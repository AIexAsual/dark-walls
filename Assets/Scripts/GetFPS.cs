﻿using UnityEngine;
using System.Collections;

public class GetFPS : MonoBehaviour
{
	float deltaTime = 0.0f;
	public int size = 4;
	string debugText;

	void Update()
	{
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		//Debug.Log(debugText + " is debug text");
	}
	
	void OnGUI()
	{
		int w = Screen.width, h = Screen.height;
		
		GUIStyle style = new GUIStyle();
		
		Rect rect = new Rect(0, 0, w, h * 2 / 100);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * size / 100;
		style.normal.textColor = new Color (0.13f, 0.81f, 0.78f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = msec.ToString("0.00") + " ms " + fps.ToString("0.0") + " fps";
		GUI.Label(rect, text, style);
	}


}