using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour{
	
	protected PlayerController() {}
	private static PlayerController _instance = null;
	
	// Singleton pattern implementation
	public static PlayerController Instance { 
		get {
			if (PlayerController._instance == null) {
				PlayerController._instance = GameObject.Find("Player").GetComponent<PlayerController>(); 
			}  
			return PlayerController._instance;
		} 
	}
	
	TextMesh ScreenText;
	public float angleTurn;
	Loader loader;
	GoTo GoToPos;
	Light myLight;
	GameObject userSpotlight;
	GameObject lightings;
	GameObject _loader;
	GameObject goToPos;
	GameObject playerHead;
	GameObject CardBoardMain;
	Transform oldPos;
	Rigidbody rb;
	Collider col;
	Vector3 targetPos;
	AudioSource maleVoice;
	int changeDialog;
	float t = 0;
	float t2 = 0;
	int i = 0;
	int tempI = 0;
	bool isDestinationReady;
	bool readytoGO = false;
	bool FLFlicker = false;
	string MyDestination;
	string MyCurrentPosition = "GoTo_Guard Table";
	string MyTempDestination;
	string LookingAt;
	string[] monologues = {
		"",
		"- Why here of all places?",
		"- I'll be needing this for later, I suppose.",  
		"- Who even goes to this place anymore?.",
		"- Was hoping to listen to some tunes.",
		"- Wish it had more battery.",
		"- I can't believe I even took this job.",
		"- Could be useful for taking notes",
		"- I think this radio's broken.", 
		"- I need to get that flashlight.", 
		"- There’s something on that middle bed.", 
		"- Where did that bear come from?!", 
		"-there's something in the hallway?", 
		"- These lights are beginning to annoy me.", 
		"- What the?!", 
		"- Hey, wait… Where are you going? Hey!", 
		"- Maybe I’m just seeing things.", 
		"- Why is the faucet open. Must turn it off.",
		"- Something is not right...", 
		"What’s that beside that incubator?!", 
		"- What a mess.", 
		"- Hello?! Who’s there?!", 
		"- Oh God, the Smell!",
		"- I must go back!!",
		"- Arghh!", 
		"- Hey kid!", 
		"TO BE CONTINUED...", 
		"- What the hell is this?!?", 
		"- Hey you! Stop!", 
		"- Something's inside...", 
		"- No! Please! I don't want to die!", 
		"- Got to get out of here.",
		"- What the hell is going on!?",
		"- What the hell is this?!",
		"- No! Please! I don't want to die!",
		"- !!!?!"

	};
	
	
	Hashtable moveCounts = new Hashtable();
	Hashtable lookAt = new Hashtable();
	Hashtable nextMove = new Hashtable();
	
	// Use this for initialization
	void Start () {
		changeDialog = 0;
		ScreenText = GameObject.Find ("Dialogs").GetComponent<TextMesh> ();
		maleVoice = GetComponent<AudioSource>();
		//ScreenText = ScreenText.GetComponent<TextMesh> ();
		gameObject.GetComponent<NavMeshAgent> ().updateRotation = false;
		lightings = GameObject.Find ("Lightings");
		_loader = GameObject.Find ("Loader");
		loader = _loader.GetComponent<Loader>();
		playerHead = GameObject.Find("Player/CardboardMain/Head");
		moveCounts["playerMoves"] = 0;
		userSpotlight = GameObject.Find("Player/CardboardMain/Head/Spot light");
		myLight = userSpotlight.GetComponent<Light>();
		CardBoardMain = GameObject.Find("Player/CardboardMain");
		col = CardBoardMain.AddComponent<BoxCollider>();
		oldPos = CardBoardMain.transform;
		if(col != null){
			col.enabled = false;
		}
	}


	
	// Update is called once per frame
	void Update () {
		
		if(angleTurn != playerHead.transform.eulerAngles.y){
			
			if(playerHead.transform.eulerAngles.y >= 340f || playerHead.transform.eulerAngles.y <= 20f){
				lookAt["Directions"] = "North";
			}else if(playerHead.transform.eulerAngles.y >= 70f && playerHead.transform.eulerAngles.y <= 110f){
				lookAt["Directions"] = "East";
			}else if(playerHead.transform.eulerAngles.y >= 160f && playerHead.transform.eulerAngles.y <= 200f){
				lookAt["Directions"] = "South";
			}else if(playerHead.transform.eulerAngles.y >= 250f && playerHead.transform.eulerAngles.y <= 290f){
				lookAt["Directions"] = "West";
			}
			
			lookAt["PlayerAngle"] = angleTurn;
			
			PlayerEventManager.Instance.dispatchEvent(PlayerEvent.ON_PLAYER_LOOKING_AT, lookAt);
			
			angleTurn = playerHead.transform.eulerAngles.y;
			
		}
		
		if(FLFlicker){
			t2 += Time.deltaTime * 1;
			if(t2 < 3f){
				myLight.intensity = Random.Range(0f,8f);
			}else{
				t2 = 0;
				myLight.intensity = 6f;
				FLFlicker = false;
			}

		}
		
		if(i > tempI){
			if((transform.position.x - targetPos.x) == 0f){
				if(readytoGO){
					nextMove ["isDestinationReady"] = isDestinationReady;
					PlayerEventManager.Instance.dispatchEvent(PlayerEvent.ON_PLAYER_NEW_POS, nextMove);
					tempI = i;
				}

			}
		}
	}
	
	public void goTo(){
		i++;
		moveCounts ["playerMoves"] = i;
		PlayerEventManager.Instance.dispatchEvent(PlayerEvent.ON_PLAYER_MOVE, moveCounts);
	}

	public void StartPos(float delay){
		StartCoroutine(WaitAndStartPos(delay));

	}

	IEnumerator WaitAndStartPos(float delay){
		yield return new WaitForSeconds(delay);
		myLight.cookie = Resources.Load("FlashlightCookie") as Texture;
		myLight.spotAngle = 45f;
		myLight.intensity = 0f;
		InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.START);
	}
	
	public void MoveTo(float delay,Vector3 val, bool isReady){
//		gameObject.GetComponent<NavMeshAgent> ().SetDestination (val);
		targetPos = val;
		isDestinationReady = isReady;
		StartCoroutine (WaitAndMoveTo (delay, val, isReady));
	}

	IEnumerator WaitAndMoveTo(float delay, Vector3 val, bool isReady){
		yield return new WaitForSeconds (delay);
		gameObject.GetComponent<NavMeshAgent> ().SetDestination (val);
//		targetPos = val;
//		isDestinationReady = isReady;
	}

	IEnumerator StandUp(float delay){
		yield return new WaitForSeconds (delay);
		InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.OFF_DIZZY);
		gameObject.GetComponent<NavMeshAgent> ().enabled = false;
		gameObject.transform.position = new Vector3(6.25f, 0f, -16.48f);
		rb = CardBoardMain.GetComponent<Rigidbody>();
		rb.isKinematic = true;
	}
	public void checkDestination(){
			loader.Load ();
	}

	
	public void GetFlashLight(float time, float val){
		StartCoroutine(WaitAndGetFlashLight(time,val));
	}	
	
	IEnumerator WaitAndGetFlashLight(float time, float val){
		yield return new WaitForSeconds(time);
		SoundManager.Instance.SoundEffects(ObjectSFX.FlashLight,true, 0.2f);	
		myLight.intensity = val;
	}
	
	public void OffFlashLight(){
		myLight.intensity = 0f;
	}
	
	public void flashLightFlicker(bool val){
		FLFlicker = val;
	}
	public void ChangeDialog(int  val){
		changeDialog = val;
	}
	
	public void Unload(){
		loader.Unload();
	}

	public void ReadToGO(bool val){
		readytoGO = val;
	}

	public void DeadState(){
		rb = CardBoardMain.AddComponent<Rigidbody>();
		col.enabled = true;
		rb.isKinematic = false;
	}
	
	public void MyMonologue(float time,int val){
		StartCoroutine(WaitAndTalk(time,val));
	}
	
	IEnumerator WaitAndTalk(float time, int val){

		yield return new WaitForSeconds(time);
		ScreenText.text = monologues [val];	
	}
	
	IEnumerator WaitAndSpace(float time){
		yield return new WaitForSeconds(time);
		ScreenText.text = "";
	}

	IEnumerator WaitAndRestartApp(float delay){
		yield return new WaitForSeconds (delay);
		InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.GAME_EXIT);

	}

	public void CallRestart(float delay){
		StartCoroutine (WaitAndRestartApp(delay));
		StartCoroutine (StandUp(4f));
	}



	
	void OnTriggerEnter(Collider other) {
		
		if(other.GetComponent<Collider>().name == "Right Arm"){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.PLAYER_HIT);
		}
	}
	
	
}
