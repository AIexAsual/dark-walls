﻿using UnityEngine;
using System.Collections;

public class InteractiveDroppingObject : InteractiveObject {
	Rigidbody rigBod;

	// Use this for initialization
	void Start () {
		rigBod = gameObject.GetComponent<Rigidbody>();
	}

	public void fallObject(){
		GetComponent<Renderer> ().enabled = true;
		rigBod.isKinematic = false;
	}
}
