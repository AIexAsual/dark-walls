﻿using UnityEngine;
using System.Collections;

public class DoorController : MonoBehaviour {
	string door;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenDoors(float rot,float time, float delay, string val){
		iTween.RotateTo (gameObject, iTween.Hash("y", rot, "time", time, "delay", delay, "easetype", iTween.EaseType.linear, "onComplete", "CloseDoors", "onCompleteTarget", gameObject));
		door = val;
		Invoke ("PlayAudio", delay);
	}

	void PlayAudio(){
		if(!GetComponent<AudioSource> ().isPlaying){
			GetComponent<AudioSource> ().Play ();
		}
	}
	

	public void CloseDoors(){
		if(door == "ER"){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ER_EXIT);
			iTween.RotateTo (gameObject, iTween.Hash("y", 0f, "time", 5f, "delay", 3f, "easetype", iTween.EaseType.easeOutElastic));
		}else if(door == "CUBICLE"){
			Rigidbody rigi = GetComponent<Rigidbody>();
			rigi.isKinematic = false;
		}

	}
}
