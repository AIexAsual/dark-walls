﻿using UnityEngine;
using System.Collections;

public class InteractiveBaby : MonoBehaviour {


	public Transform targetPos;
	Transform origPos;
	NavMeshAgent navMeshAgent;
	Animator anim;
	bool goToPos = false;
	AICharacterControl aiControl;
	GameObject[] baby_skin;

	// Use this for initialization
	void Start () {
		navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
		anim = gameObject.GetComponent<Animator>();
		aiControl = gameObject.GetComponent<AICharacterControl>();
		baby_skin = GameObject.FindGameObjectsWithTag("Baby Skin");

	}
	
	// Update is called once per frame
	void Update () {
		origPos = GameObject.Find("Baby Way Point").transform;

		if(goToPos){
			//aiControl.SetTarget(origPos);
			navMeshAgent.destination = origPos.position;
			if((gameObject.transform.position.x - origPos.position.x) == 0f){
				EnableBaby(false);
				anim.SetTrigger("toIdle");
				goToPos = false;
			}
		}
		
	}
	
	
	public void EnableBaby(bool val){
		navMeshAgent.enabled = val;
	}

	public void HideBaby(bool val){
		for(int i = 0; i < baby_skin.Length; i++){
			baby_skin[i].GetComponent<SkinnedMeshRenderer>().enabled = val;
		}
		Collider col = gameObject.GetComponent<Collider>();
		col.enabled = val;
	}
	
	public void BabyWalk(){
		anim.SetTrigger("toWalk");
		EnableBaby(true);
		goToPos = true;
	}
	
	public void BabyIdle(){
		Debug.Log("Go here");
	}
	
	
	public void BabyPos(Vector3 val){
		gameObject.transform.position = val;
	}
	
	public void BabyScream(){
		anim.SetTrigger("toScream");
	}
	
}
