﻿using UnityEngine;
using System.Collections;

public class FadeTexture : MonoBehaviour {
	Renderer mat;
	float val = 1;

	// Use this for initialization
	void Start () {
		mat = GetComponent<Renderer>();
		mat.material.SetFloat ("_Blend", 1f);
		//FadeMat ();
	}

	public void FadeMat(){
		iTween.ValueTo (gameObject, iTween.Hash ("from", 1f, "to", 0, "time", 0.8f, "delay", 5f, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.none, "onupdate", "FadeText"));
	}
	
	// Update is called once per frame
	void FadeText(float newValue){
		//apply the value of newValue:
		val = newValue;
	}

	void Update(){
		//if(val != 1)
			mat.material.SetFloat ("_Blend", val);
	}

}
