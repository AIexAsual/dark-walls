﻿using UnityEngine;
using System.Collections;

public class Reticle : MonoBehaviour {
	public GameObject myCam;
	Camera CameraFacing;
	bool gotCam;
	private Vector3 originalScale;
	
	// Use this for initialization
	void Start () {

		originalScale = transform.localScale;
		CameraFacing = myCam.GetComponent<Camera>();
	
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		float distance;
			if (Physics.Raycast (new Ray (CameraFacing.transform.position,
			                              CameraFacing.transform.rotation * Vector3.forward),
			                     out hit)) {
				distance = hit.distance;
			} else {
				distance = CameraFacing.farClipPlane * 0.95f;
			}
			if (gameObject.name.ToString() != "Loader") {
				transform.position = new Vector3 (CameraFacing.transform.position.x, 1.5f, CameraFacing.transform.position.z) +
					CameraFacing.transform.rotation * Vector3.forward * 1f;
				transform.LookAt (CameraFacing.transform.position);
				transform.localScale = originalScale;
				transform.Rotate (0.0f, 180.0f, 0.0f);
				if (distance < 10.0f) {
					distance *= 1 + 5 * Mathf.Exp (-distance);
				}
			} else {
				transform.position = new Vector3(CameraFacing.transform.position.x, CameraFacing.transform.position.y, CameraFacing.transform.position.z) +
					CameraFacing.transform.rotation * Vector3.forward * 1f;
				transform.LookAt (CameraFacing.transform.position);
			}
		}
}