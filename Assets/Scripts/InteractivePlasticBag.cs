﻿using UnityEngine;
using System.Collections;

public class InteractivePlasticBag : MonoBehaviour {
	Cloth cloth;
	Collider col;
	// Use this for initialization
	void Start () {
		cloth = gameObject.GetComponent<Cloth>();
		col = gameObject.GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Wiggle(){
		StartCoroutine(WaitAndWiggle(2f));
//		cloth.useGravity = true;
//		cloth.externalAcceleration = new Vector3(0f,10.83f,0f);
//		cloth.randomAcceleration = new Vector3(0f,3f,0f);
//		col.enabled = true;
	}

	IEnumerator WaitAndWiggle(float time){
		yield return new WaitForSeconds(time);
		cloth.useGravity = true;
		cloth.externalAcceleration = new Vector3(0f,10.83f,0f);
		cloth.randomAcceleration = new Vector3(0f,3f,0f);
		col.enabled = true;
	}

	public void StopWiggle(){
		//StartCoroutine(WaitAndStopWiggle(2f));
		cloth.useGravity = false;
		cloth.externalAcceleration = new Vector3(0f,0f,0f);
		cloth.randomAcceleration = new Vector3(0f,0f,0f);
		col.enabled = false;
	}

//	IEnumerator WaitAndStopWiggle(float time){
//		yield return new WaitForSeconds(time);
//		cloth.useGravity = false;
//		cloth.externalAcceleration = new Vector3(0f,0f,0f);
//		cloth.randomAcceleration = new Vector3(0f,0f,0f);
//		col.enabled = false;
//	}
}
