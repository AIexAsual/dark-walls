﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class InteractiveRadio : InteractiveObject {
	public AudioClip[] radioClips;
	AudioSource radioSound;

	// Use this for initialization
	void Awake () {
		radioSound = gameObject.GetComponent<AudioSource>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void SetGazedAt (bool gazeAt){
		//specific

		GetComponent<AudioSource>().volume = gazeAt ? 0.1f : 0.05f;

		base.SetGazedAt (gazeAt);
		
	}

	public void changeClip(int val){
		//radioSound.Stop ();
		radioSound.clip = radioClips [val];
		GetComponent<AudioSource>().volume = 0.8f;
		if(!radioSound.isPlaying){
			radioSound.Play ();
		}
	}
}
