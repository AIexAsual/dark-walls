﻿using UnityEngine;
using System.Collections;

public class SceneLoader : MonoBehaviour {
	string sceneCount;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void SceneChange(string val){
		sceneCount = val;
		StartCoroutine(WaitAndLoad(2.0f));
	}

	IEnumerator WaitAndLoad(float time){
		yield return new WaitForSeconds(time);
		Application.LoadLevel(sceneCount);
		if(sceneCount == "Project Lithium HW"){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ENTER_HALLWAY);
		}
	}
}
