﻿	using UnityEngine;
using System.Collections;

public class IKHandler : MonoBehaviour {

	GameObject Left_Arm_IK;
	GameObject Right_Arm_IK;
	GameObject Head_IK;
	GameObject player;
	float IKWeight = 1f;
	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		Left_Arm_IK = GameObject.Find("Left Arm Target");
		Right_Arm_IK = GameObject.Find("Right Arm Target");
		Head_IK = GameObject.Find("Head Target");
		player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnAnimatorIK(){
		anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, IKWeight);
		anim.SetIKPositionWeight(AvatarIKGoal.RightHand, IKWeight);

		anim.SetIKPosition(AvatarIKGoal.LeftHand, Left_Arm_IK.transform.position);
		anim.SetIKPosition(AvatarIKGoal.RightHand, Right_Arm_IK.transform.position);

		anim.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, IKWeight);
		anim.SetIKHintPositionWeight(AvatarIKHint.RightElbow, IKWeight);

		anim.SetLookAtPosition(player.transform.position);
//
//		anim.SetIKHintPosition(AvatarIKHint.LeftElbow, Left_Arm_IK.transform.position);
//		anim.SetIKHintPosition(AvatarIKHint.RightElbow, Right_Arm_IK.transform.position);
//
//		anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, IKWeight);
//		anim.SetIKRotationWeight(AvatarIKGoal.RightHand, IKWeight);
//		
//		anim.SetIKRotation(AvatarIKGoal.LeftHand, Left_Arm_IK.transform.rotation);
//		anim.SetIKRotation(AvatarIKGoal.RightHand, Right_Arm_IK.transform.rotation);



	}
}
