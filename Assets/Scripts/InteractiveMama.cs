﻿using UnityEngine;
using System.Collections;

public class InteractiveMama : MonoBehaviour {

	public static GameObject targetPos;
	//Transform targetPos;
	NavMeshAgent navMeshAgent;
	Animator anim;
	bool goToPos = false;
	AICharacterControl aiControl;
	GameObject[] mama_skin;
	GameObject head;
	Transform targetLook;
	Transform myTransform;

	// Use this for initialization
	void Start () {
		myTransform = gameObject.transform;
		navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
		anim = gameObject.GetComponent<Animator>();
		AICharacterControl aiControl = gameObject.GetComponent<AICharacterControl>();
		mama_skin = GameObject.FindGameObjectsWithTag("mama skin");
		head = GameObject.Find("Interactive Objects/mama/mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head");
	}
	
	// Update is called once per frame
	void Update () {

		if (targetPos != null) {
			if(goToPos){
				if(Vector3.Distance(gameObject.transform.position, targetPos.transform.position) <= 1.2f){
					anim.SetTrigger("toIdle");
					//EnableMama(false);
					goToPos = false;
				}
			}
		}


	}

	public void MamaHeadTurn(float time, float delay,float val){
		iTween.RotateTo(head, iTween.Hash("x", val, "time", time, "delay", delay, "EaseType",iTween.EaseType.easeOutQuad));

	}

	public void EnableMama(float delay, bool val){
//		NavMeshAgent navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
//		navMeshAgent.enabled = val;
		StartCoroutine (WaitAndEnableMama(delay, val));
	}

	IEnumerator WaitAndEnableMama(float delay, bool val){
		yield return new WaitForSeconds (delay);
		NavMeshAgent navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
		navMeshAgent.enabled = val;
	}


	public void MamaAction(string val){
		if(val != "toScream" || val != "toIdle"){
			EnableMama(0.2f, true);
			goToPos = true;
		}

		anim.SetTrigger(val);
	}

	public void MamaGoto(string target){
		AICharacterControl aiControl = gameObject.GetComponent<AICharacterControl>();
		targetPos = GameObject.Find(target);
		//aiControl.SetTarget(targetPos.transform);
		navMeshAgent.destination = targetPos.transform.position;
	}

	public void MamaPos(float delay, Vector3 val){
		StartCoroutine (WaitAndMamaPos(delay,val));
//		gameObject.transform.position = val;
	}

	IEnumerator WaitAndMamaPos(float delay, Vector3 val){
		yield return new WaitForSeconds (delay);
		gameObject.transform.position = val;
	}

	public void MamaScream(){
		anim.SetTrigger("toScream");
	}

	public void MamaLookAt(Transform val){
		Transform myTransform = this.gameObject.transform;
		myTransform.LookAt (val);
	
	}

	public void HideMama(float delay, bool val){
//		for(int i = 0; i < mama_skin.Length; i++){
//			mama_skin[i].GetComponent<SkinnedMeshRenderer>().enabled = val;
//		}
//		Collider col = gameObject.GetComponent<Collider>();
//		col.enabled = val;
		StartCoroutine (WaitAndHideMama (delay, val));
	}

	IEnumerator WaitAndHideMama(float delay, bool val){
		yield return new WaitForSeconds (delay);
		for(int i = 0; i < mama_skin.Length; i++){
			mama_skin[i].GetComponent<SkinnedMeshRenderer>().enabled = val;
		}
		Collider col = gameObject.GetComponent<Collider>();
		col.enabled = val;
	}

}
