﻿using UnityEngine;
using System.Collections;

public class CoroutineScript : MonoBehaviour {
	public float CoroutineTime{
		get{
			return coroutineTime;
		}
		set{
			coroutineTime = value;
			
			StopCoroutine("testCor");
			StartCoroutine("testCor", coroutineTime);
			
		}
	}
	
	private float coroutineTime;
	
	
	IEnumerator testCor(float time){
		
		yield return new WaitForSeconds (time);
		Debug.Log ("Working!");

	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
