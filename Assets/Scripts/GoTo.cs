﻿using UnityEngine;
using System.Collections;

public class GoTo : MonoBehaviour {


	protected GoTo() {}
	private static GoTo _instance = null;
	
	public static GoTo Instance { 
		get {
			if (GoTo._instance == null) {
				GoTo._instance = GameObject.Find("Destinations/GoTo_").GetComponent<GoTo>(); 
			}
			return GoTo._instance;
		} 
	}

	Vector3[] pos;
	public Vector3 nextPosition;
	Light light;
	Collider col;
	int count;

	// Use this for initialization
	void Start () {
		count = 0;

		pos = new []{
			new Vector3(1.54f, 1f, 6.49f),
			new Vector3(-0.37f, 1f, 11f),
			new Vector3(-1.2f, 1f, 28.71f),
			new Vector3(-10.11f, 1f, 27.74f),
			new Vector3(-7.68f, 1f, 25.57f),
			new Vector3(-4.07f, 1f, 28.81f),
			new Vector3(9.24f, 1f, 28.81f),
			new Vector3(14f, 1f, 28f),
			new Vector3(24.31f, 1f, 23.51f),
			new Vector3(29.72f, 1f, 15.44f),
			new Vector3(22.7f, 1f, 13.34f),
			new Vector3(25.65f, 1f, 7.42f),
			new Vector3(22.03f, 1f, 9.68f),
			new Vector3(26.03f, 1f, 16.19f),
			new Vector3(32.04f, 1f, 37.37f),
			new Vector3(32.04f, 1f, 55.13f),
			new Vector3(48.08f, 1f, 55.13f),
			new Vector3(53.02f, 1f, 47.81f),
			new Vector3(58f, 1f, 54.66f),
			new Vector3(53f, 1f, 48.26f),
			new Vector3(53f, 1f, 48.26f),
			new Vector3(62f, 1f, 48.28f),
			new Vector3(76.18f ,1f, 48.41f),
			new Vector3(81.79f, 1f, 48.69f),
			new Vector3(0f, 1f, 0f)
		};
		nextPosition = pos[0];
		light = GetComponent<Light>();
		col = GetComponent<Collider>();
		iTween.ValueTo (gameObject, iTween.Hash ("from", 0f, "to", 8f, "time", 3f, "delay", 0.2f, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.pingPong, "onupdate", "myValue" ));

	}
	

	public void GotoPos(){
		count++;
		transform.position = pos[count];
		GoHere(0f,false);
	}

	public void GoHere(float time,bool val){
		nextPosition = pos[count];
		StartCoroutine(WaitAndEnable(time,val));
	}

	IEnumerator WaitAndEnable(float time, bool val){

		yield return new WaitForSeconds(time);
		col.enabled = val;
		light.enabled = val;
	}

	void myValue(float v){
		light.intensity = v;
	}
}
