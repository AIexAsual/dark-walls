﻿using UnityEngine;
using System.Collections;

public class InteractiveFlashlight : InteractiveObject {
	GameObject mySpotlight;
	Light lights;

	bool isLightOn = false;

	// Use this for initialization
	void Start () {
//		mySpotlight = GameObject.Find ("Emergency/Flash Light/Spotlight");
//		lights = mySpotlight.GetComponent<Light> ();
		SetGazedAt (true);

	}


	public override void SetGazedAt (bool gazeAt){
		//specific
//		if (!isLightOn) {
//			lights.intensity = gazeAt ? 8f : 0f;
//		} 

		base.SetGazedAt (gazeAt);

	}

	public void LightsOn(){
		mySpotlight = GameObject.Find ("Emergency/Flash Light/Spotlight");
		lights = mySpotlight.GetComponent<Light> ();
		isLightOn = true;
		lights.range = 75f;
		lights.intensity = 8f;

	}

	public void LightsOff(){
		mySpotlight = GameObject.Find ("Emergency/Flash Light/Spotlight");
		lights = mySpotlight.GetComponent<Light> ();
		lights.intensity = 0f;
	}

	public void GetFlashlight(){
		gameObject.SetActive (false);
	}


}
