﻿using UnityEngine;
using System.Collections;


public class LightController : MonoBehaviour {
	Light lights;
	float myLightsIntensity;
	float t;
	float t2 = 100f;
	float t3;
	float timeSpeed;
	float timeDelay;
	float fadeTime = 50f;
	bool isFlicker;
	bool isFade;
	bool ExitFade = false;
	string fadeState;
	InteractiveFlashlight flashLight;

	// Array of random values for the intensity.
	private float[] smoothing = new float[20];

	// Use this for initialization
	void Start () {
		lights = gameObject.GetComponent<Light>();
		myLightsIntensity = lights.intensity;
		isFlicker = false;
	}

	
	void Update () {

		if (isFlicker) {
				t += Time.deltaTime * 1f;
				if (t >= t3) {
					t = 0;
					isFlicker = false;
					if(gameObject.name == "ER Room Light"){
					InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.LIGHTS_OFF);
					}
					else if(gameObject.name == "Room Light"){
					InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.MORGUE_LIGHTSOFF);
					}
				}
			}

		if(ExitFade){
			t2 -= Time.deltaTime * 20f;
			if(t2 < -1f){
				ExitFade = false;
			}else{
				RenderSettings.fogEndDistance = t2;
			}
		}

		if(isFade){
			if(fadeState == "In"){
				fadeTime -= Time.deltaTime * timeSpeed;
				if(fadeTime < 0f){
					isFade = false;
				}
			}else if(fadeState == "Out"){	
				fadeTime += Time.deltaTime * timeSpeed;
				if(fadeTime > 100f){
					isFade = false;
				}
			}
			RenderSettings.fogEndDistance = fadeTime;
		}
	}


	public void OffGameLight(){
		ExitFade = true;
	}

	IEnumerator FlickerMe(bool val, float time){
		//lights.intensity = Random.Range(0f,8f);
		RenderSettings.fogEndDistance = Random.Range(10f,100f);
		//lights.intensity = Random.Range(10f,100f);
		yield return new WaitForSeconds(Random.Range(0,time));
		if(isFlicker){
			StartCoroutine(FlickerMe(val,Random.Range(0,time)));
			//Debug.Log(val.ToString() + t3.ToString());
		}else{
			RenderSettings.fogEndDistance = 20f;
		}
	}

	IEnumerator WaitAndSetFlicker(float time, bool val){
		yield return new WaitForSeconds(time);
		t3 = time;
		isFlicker = val;
	}

	public void FlickerLights(bool val, float time, float delay){
		StartCoroutine(FlickerMe(val , delay));
		t3 = time;
		isFlicker = val;
		//StartCoroutine(WaitAndSetFlicker(time, val));
	}

	public void OnFlicker(bool val){
		isFlicker = val;
	}

	public void OnLights(){
		if (gameObject.name.StartsWith("Hallway")) {
			lights.intensity = 3f;
		} else {
			//lights.intensity = 8f;
			RenderSettings.fogEndDistance = 100f;
		}
	}

	public void OffLights(){
		if (gameObject.name.StartsWith("Hallway")) {
			lights.intensity = 0f;
		} else {
			RenderSettings.fogEndDistance = 20f;
		}

	}

	public void FogState(float val){
		RenderSettings.fogEndDistance = val;
	}

	public void OnFade(bool val, float delay, float speed, string fadestate){
		isFade = val;
		timeSpeed = speed;
		timeDelay = delay;
		fadeState = fadestate;
	}
}
