﻿using UnityEngine;
using System.Collections;

public enum SceneEvent { 
	EMERGENCY_ROOM,
	HALLWAY
	
}

public class SceneManager {
	protected SceneManager() {}
	private static SceneManager _instance = null;
	
	// Singleton pattern implementation
	public static SceneManager Instance { 
		get {
			if (SceneManager._instance == null) {
				SceneManager._instance = new SceneManager(); 
			}  
			return SceneManager._instance;
		} 
	}
	
	public delegate void OnStateChangeHandler(SceneEvent ev);
	public event OnStateChangeHandler OnStateChangeEvent;
	public SceneEvent sceneState;
	
	public void dispatchEvent(SceneEvent stateEvent) {
		sceneState = stateEvent;
		OnStateChangeEvent (stateEvent);
	}


	SceneLoader sceneLoader;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ChangeScene(string val){
		sceneLoader = GameObject.Find("Game Manager").GetComponent<SceneLoader>();
		sceneLoader.SceneChange(val);
	}
}
