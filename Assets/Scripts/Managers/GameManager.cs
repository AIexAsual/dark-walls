﻿using UnityEngine;
using System.Collections;

public class Stages{
	public static string INTRO_ROOM = "myIntro";
	public static string EMERGENCY_ROOM = "Emergency";
	public static string HALLWAY_ROOM = "Hallway";
	public static string COMFORT_ROOM = "Comfort";
	public static string LOBBY_ROOM = "Lobby";
	public static string NURSERY_ROOM = "Nursery";
	public static string MORGUE_ROOM = "Morgue";
	public static string WARD_ROOM = "Ward";
	public static string EMERGENCY_OBJECTS = "ER Pooling Objects";
	public static string HALLWAY_OBJECTS = "HW Pooling Objects";
	public static string COMFORT_OBJECTS = "CR Pooling Objects";
	public static string LOBBY_OBJECTS = "L Pooling Objects";
	public static string NURSERY_OBJECTS = "N Pooling Objects";
	public static string MORGUE_OBJECTS = "M Pooling Objects";
	public static string WARD_OBJECTS = "W Pooling Objects";

}


public class GameManager : MonoBehaviour {
	
	protected GameManager() {}
	private static GameManager _instance = null;
	
	public static GameManager Instance { 
		get {
			if (GameManager._instance == null) {
				GameManager._instance = new GameManager(); 
			}
			return GameManager._instance;
		} 
	}
	
	
	public InteractiveObject[] interactiveObject;
	public InteractiveEvent IE;
	public static GameObject INTRO;
	public static GameObject EMERGENCY_ROOM;	
	public static GameObject HALLWAY_ROOM;
	public static GameObject COMFORT_ROOM;
	public static GameObject LOBBY_ROOM;
	public static GameObject NURSERY_ROOM;
	public static GameObject MORGUE_ROOM;
	public static GameObject WARD_ROOM;
	public static GameObject EMERGENCY_OBJECTS;	
	public static GameObject HALLWAY_OBJECTS;
	public static GameObject COMFORT_OBJECTS;
	public static GameObject LOBBY_OBJECTS;
	public static GameObject NURSERY_OBJECTS;
	public static GameObject MORGUE_OBJECTS;
	public static GameObject WARD_OBJECTS;
	private static string debugger = "";
	public static string Debugger{
		get{return debugger;}
		set{debugger = value;}
	}
	private static int countRestart = 0;

	public string apdroidAppUrl = "market://details?id=com.Nuworks.DarkWalls";
	bool hasChildLaugh = false;
	bool hwLights = false;
	bool isStarring = false;
	bool isReady;
	bool isRestarting;
	bool isFlicker = false;
	bool isQuitting = false;
	string appPackageName;
	string oldHitObject = "";
	string state = "";
	string DebugText;
	GameObject thisObject;
	GameObject Cursor;
	GameObject hidingObjects;
	GameObject behindUser;
	GameObject myFlashLight;
	GameObject Line;
	GameObject mamawaypoint;
	GameObject myLightIndicator;
	GameObject myDestination;
	GameObject[] IntroOBjects;
	Collider col;
	Vector3 targetPos;
	GoTo GoTo;
	Animator Cursor_Anim;
	PlayerController PlayerController; 
	float t;
	float tStare;
	float deltaTime = 0.0f;

	void Start() {
	
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Application.targetFrameRate = 60;
		if(gameObject != null){
			InteractiveEventManager.Instance.OnStateChangeEvent += OnEventChange;
			GazeInputManager.Instance.OnStateChangeEvent += OnGazeInput;
			PlayerEventManager.Instance.OnStateChangeEvent += OnPlayerStateChange;
			thisObject = null;
			isRestarting = false;
			isReady = false;
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.NULL_STATE);
			//apdroidAppUrl = 
		}else{
			Debug.Log("This is null");
		}
	}


	void FindObjects(){
		interactiveObject[0] = GameObject.Find("Emergency/Guard Hat").GetComponent<InteractiveObject>();
		interactiveObject[1] = GameObject.Find("Emergency/Flash Light").GetComponent<InteractiveObject>();
		interactiveObject[2] = GameObject.Find("Emergency/radio").GetComponent<InteractiveObject>();
		interactiveObject[3] = GameObject.Find("Emergency/Record Book").GetComponent<InteractiveObject>();
		mamawaypoint = GameObject.Find("Interactive Objects/Mama Way Point");
		behindUser = GameObject.Find("Player/CardboardMain/Head/Behind User");
		myLightIndicator = GameObject.Find ("Destinations/GoTo_");
		Cursor = GameObject.Find ("Player/CardboardMain/Head/GazerPointer");
		myFlashLight = GameObject.Find("Player/CardboardMain/Head/Spot light");
		myDestination = GameObject.Find("Destinations/GoTo_");

		INTRO = GameObject.Find(Stages.INTRO_ROOM);
		EMERGENCY_ROOM = GameObject.Find(Stages.EMERGENCY_ROOM);
		HALLWAY_ROOM = GameObject.Find(Stages.HALLWAY_ROOM);
		COMFORT_ROOM = GameObject.Find(Stages.COMFORT_ROOM);
		LOBBY_ROOM = GameObject.Find(Stages.LOBBY_ROOM);
		NURSERY_ROOM = GameObject.Find(Stages.NURSERY_ROOM);
		MORGUE_ROOM = GameObject.Find(Stages.MORGUE_ROOM);
		WARD_ROOM = GameObject.Find(Stages.WARD_ROOM);

		EMERGENCY_OBJECTS = GameObject.Find(Stages.EMERGENCY_OBJECTS);
		HALLWAY_OBJECTS = GameObject.Find(Stages.HALLWAY_OBJECTS);
		COMFORT_OBJECTS = GameObject.Find(Stages.COMFORT_OBJECTS);
		LOBBY_OBJECTS = GameObject.Find(Stages.LOBBY_OBJECTS);
		NURSERY_OBJECTS = GameObject.Find(Stages.NURSERY_OBJECTS);
		MORGUE_OBJECTS = GameObject.Find(Stages.MORGUE_OBJECTS);
		WARD_OBJECTS = GameObject.Find(Stages.WARD_OBJECTS);

	}

//	IEnumerator ObjectDestroyer (string name){
//		go = GameObject.FindGameObjectsWithTag (name);
//		yield return null;
//		for(int i = 0; i < go.Length; i++){
//			Destroy(go[i]);
//		}
//	
//	}

	IEnumerator ObjectSpawner(GameObject go, string name){
		yield return null;
		GameObject instance = Instantiate(Resources.Load(name, typeof(GameObject))) as GameObject;
		go = instance;
		instance.name = name;
		instance.SetActive(false);
	}

	IEnumerator MyGameObjectHandler(string name, bool val){
		yield return null;
		GameObject go = GameObject.Find(name);
		go.SetActive(val);
	}

	void OnGUI(){
		int size = 4;
		int w = Screen.width, h = Screen.height;
		GUIStyle style = new GUIStyle();

		Rect rect = new Rect(0, 0, w, h * 2 / 100);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * size / 100;
		style.normal.textColor = new Color (0.13f, 0.81f, 0.78f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = msec.ToString("0.00") + " ms " + fps.ToString("0.0") + Debugger ;
//		GUI.Label(rect, text, style);

	}

	void Update () {
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		Debugger = "State: " + state;

		if(Input.GetKeyUp(KeyCode.R)){
			isRestarting = true;
			isReady = false;
			Application.LoadLevelAsync(0);
		}
		else if(Input.GetKeyUp(KeyCode.S)){
			InteractiveEventManager.Instance.dispatchEvent(IE);
			//PlayerController.Instance.DeadState();
		}



		if(gameObject != null){
			if(hwLights){	
				t += Time.deltaTime * 1;
				//InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find(WayPoints.PLAYER).transform);
				if(t.ToString("0") == "3"){
					PlayerController.Instance.GetFlashLight(0.2f, 0f);
					LightManager.Instance.LightsOff(myLights.HW_LIGHTS3);
					PlayerController.Instance.MyMonologue(1f, 13);
					InteractiveObjectManager.Instance.HideMama(0f, false);
					InteractiveObjectManager.Instance.EnableMama(0f, false);
					//InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find(WayPoints.PLAYER).transform);
				
				}else if(t.ToString("0") == "4"){
					LightManager.Instance.LightsOff(myLights.HW_LIGHTS2);
					SoundManager.Instance.SoundEffects(ObjectSFX.LightFlicker,true, 0.2f);
				}else if(t.ToString("0") == "5"){
					LightManager.Instance.LightsOff(myLights.HW_LIGHTS1);
				}else if(t.ToString("0") == "6"){
					InteractiveObjectManager.Instance.MoveMama(0f, new Vector3(0f, 0f, 28.32f));
				}else if(t.ToString("0") == "7"){
					//InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.SCREAM);
					InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find(WayPoints.PLAYER).transform);
					InteractiveObjectManager.Instance.HideMama(0f, true);
					LightManager.Instance.LightsOn(myLights.HW_LIGHTS3);
					SoundManager.Instance.SoundEffects(ObjectSFX.PianoSlam,true, 0.2f);	
					SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 1f);
				}else if(t.ToString("0") == "8"){
					LightManager.Instance.LightsOff(myLights.HW_LIGHTS3);
					InteractiveObjectManager.Instance.MoveMama(0f, new Vector3(0f,0f,0f));
					//InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.IDLE);
					PlayerController.Instance.MyMonologue(1.5f, 14);
				}
				else if(t.ToString("0") == "11"){
					LightManager.Instance.LightsOff(myLights.HW_LIGHTS1);
				}else if(t.ToString("0") == "13"){
					PlayerController.Instance.GetFlashLight(0f,6f);
					//MovingObjectManager.Instance.MoveDoors(Doors.CR_DOOR1,"CR",-180f,5f);
					//PlayerController.Instance.MyMonologue(2.5f, 15);
					PlayerController.Instance.MyMonologue(3,16);
				}else if(t.ToString("0") == "15"){
					InteractiveObjectManager.Instance.GoHere(0f,true);
					PlayerController.Instance.GetFlashLight(0.2f, 6f);
					SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 0.01f);
					hwLights = false;
				}
			}

			
			if(Input.touchCount > 1){
				isQuitting = false;
			}

			if (Input.GetKeyDown(KeyCode.Escape) && isQuitting == true){
				Application.Quit();
			}else if(Input.GetKeyDown(KeyCode.Escape) && isQuitting == false){
				OnMessageClose();
			}


			if(Input.anyKey){
				if (Input.GetKey(KeyCode.Escape)){
					isQuitting = true;
				}	else{
					isQuitting = false;
				} 
			}
		}

	}

	private void OnMessageClose() {
		
		new MobileNativeMessage("Message", "Double tap to Exit.");
	}

	private void OnRatePopUpClose(MNDialogResult result) {
		
		//parsing result
		switch(result) {
		case MNDialogResult.RATED:
			//Debug.Log ("Rate Option pickied");
			Application.OpenURL("www.nuworks.ph");
			break;
		case MNDialogResult.REMIND:
			//Debug.Log ("Remind Option pickied");
			break;
		case MNDialogResult.DECLINED:
			//Debug.Log ("Declined Option pickied");
			break;
		}
		
		//new MobileNativeMessage("Result", result.ToString() + " button pressed");
		
	}


	void OnEventChange(InteractiveEvent ev){
	//	DebugText = InteractiveEventManager.Instance.interactiveState.ToString();
//		Debug.Log(DebugText);
		switch (ev) {
		case InteractiveEvent.NULL_STATE:

			InteractiveObjectManager.Instance.Start();
			LightManager.Instance.Start ();
			FindObjects();
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleHumming, true, 0.2f);

			break;
		case InteractiveEvent.START:

			if(countRestart >= 2){
				MobileNativeRateUs ratePopUp =  new MobileNativeRateUs("Like this game?", "Please rate to support future updates!");
				ratePopUp.SetAndroidAppUrl(apdroidAppUrl);
				ratePopUp.OnComplete += OnRatePopUpClose;
				ratePopUp.Start();
			}else{
				countRestart += 1;
			}

			LightManager.Instance.LightFade(myLights.ER_LIGHTS, true, 2f, 20f, "Out");
			SoundManager.Instance.SoundEffects(ObjectSFX.RadioSound, true, 0.2f);
			SoundManager.Instance.SoundEffects(ObjectSFX.AmbientSound, true, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.BGM, true, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleHumming, false, 0.2f);
			PlayerController.Instance.ReadToGO(true);
			myDestination.transform.position = new Vector3(1.54f, 1f, 6.49f);
			EMERGENCY_ROOM.SetActive(true);
			EMERGENCY_OBJECTS.SetActive(true);
			HALLWAY_ROOM.SetActive(false);
			HALLWAY_OBJECTS.SetActive(false);
			COMFORT_ROOM.SetActive(false);
			COMFORT_OBJECTS.SetActive(false);
			LOBBY_ROOM.SetActive(false);
			LOBBY_OBJECTS.SetActive(false);
			NURSERY_ROOM.SetActive(false);
			NURSERY_OBJECTS.SetActive(false);
			MORGUE_ROOM.SetActive(false);
			MORGUE_OBJECTS.SetActive(false);
			WARD_ROOM.SetActive(false);
			WARD_OBJECTS.SetActive(false);
			INTRO.SetActive(false);
			break;
		case InteractiveEvent.LIGHTS_FLICKER:
			if(state == "Emergency Flicker"){
				LightManager.Instance.FlickerLights(myLights.ER_LIGHTS, true, 3f, 0.5f);
				InteractiveObjectManager.Instance.RadioChange(1);
			}
			else if(state == "Ready Scare"){
				LightManager.Instance.FlickerLights(myLights.N_LIGHTS, true, 3f, 0.5f);
			}
			else if(state == "Jump Scare 2"){
				LightManager.Instance.FlickerLights(myLights.MORGUE_LIGHTS1, true, 3f, 0.5f);
				state = "Morgue Lights Off";
			}
			break;
		case InteractiveEvent.LIGHTS_OFF:
			isReady = true;
			LightManager.Instance.LightsOff(myLights.ER_LIGHTS);
			LightManager.Instance.FlashlightsOn();
			InteractiveObjectManager.Instance.DropObject(DroppingObjects.OXYGEN_TANK);
			SoundManager.Instance.SoundEffects(ObjectSFX.OxygenTank,true, 0.2f);	
			PlayerController.Instance.MyMonologue(1f,9);
			break;
		case InteractiveEvent.ON_GET_FLASHLIGHT:
			EnableGo(0.2f,true);
			PlayerController.Instance.GetFlashLight(2.0f, 6f);
			PlayerController.Instance.MyMonologue(2f,5);

			LightManager.Instance.GetFlashlight();
			break;
		case InteractiveEvent.ER_MESSAGE:
			InteractiveObjectManager.Instance.TextEffects("Emergency/ERMessage",1f, 0.2f);
			InteractiveObjectManager.Instance.SetRagdoll();
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.LAUGHING_CHILD);
			EnableGo(2f,true);
			break;
		case InteractiveEvent.LAUGHING_CHILD:
			Debug.Log("haha");
			state = "Laughing Child";
			HALLWAY_ROOM.SetActive(true);
			hasChildLaugh = true;

			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds1,true, 0.2f);	
			EnableGo(2f,true);
			break;
		case InteractiveEvent.OPEN_HWDOOR:
			HALLWAY_OBJECTS.SetActive(true);
			COMFORT_ROOM.SetActive(true);
			MovingObjectManager.Instance.MoveDoors(Doors.ER_DOOR1,"ER",70f,1f,0.8f);
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds2,true, 0.2f);	
			break;
		case InteractiveEvent.ER_EXIT:
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ENTER_HALLWAY);
			break;
		case InteractiveEvent.ENTER_HALLWAY:
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.HALLWAY_LIGHTSOFF);
			break;
		case InteractiveEvent.HALLWAY_LIGHTSOFF:
			EMERGENCY_OBJECTS.SetActive(false);
			LightManager.Instance.FogState(myLights.ER_LIGHTS, 100);
			hwLights = true;
			break;
		case InteractiveEvent.WINDOW_MAMA:
			InteractiveObjectManager.Instance.EnableMama(0f, true);
			InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.WALK);
			InteractiveObjectManager.Instance.MamaGoto(WayPoints.MAMAWAY_POINT);
			break;
		case InteractiveEvent.ENTER_CR:
			PlayerController.Instance.MyMonologue(2f, 17);
			//PlayerController.Instance.OffFlashLight();
			SoundManager.Instance.SoundEffects(ObjectSFX.Faucet,true, 0.2f);	
			InteractiveObjectManager.Instance.EnableMama(0f, false);
			break;
		case InteractiveEvent.CR_NOICE:
			SoundManager.Instance.SoundEffects(ObjectSFX.FaucetOn,true, 0.2f);	
			MovingObjectManager.Instance.MoveDoors(Doors.CUBICLE,"CUBICLE", 120f,1f, 0f);
			InteractiveObjectManager.Instance.MamaLookAt(mamawaypoint.transform);
			InteractiveObjectManager.Instance.MoveMama(0f, new Vector3(-1.2f,0f, 21.42f));
			InteractiveObjectManager.Instance.EnableMama(0f, true);

			EnableGo(3f,true);
			break;
		case InteractiveEvent.CR_MESSAGE:
			InteractiveObjectManager.Instance.TextEffects("Comfort/CRMessage",1f, 0.2f);
			SoundManager.Instance.SoundEffects(ObjectSFX.ISeeYou,true, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.DoorPounding, true, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds3, true, 0.2f);	
			SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 1f);
			EnableGo(2f,true);
			break;
			
		case InteractiveEvent.ENTER_LOBBY:
			
			MovingObjectManager.Instance.MoveDoors(Doors.LOBBY_DOOR1,"Lobby", 150f, 2f,2f);
			MovingObjectManager.Instance.MoveDoors(Doors.LOBBY_DOOR2,"Lobby", -150f,2f,2f);
			MovingObjectManager.Instance.MoveDoors(Doors.LOBBY_DOOR1,"Lobby", 0f,0.8f,6.8f);
			MovingObjectManager.Instance.MoveDoors(Doors.LOBBY_DOOR2,"Lobby", 0f,0.8f,6.8f);
			PlayerController.Instance.MyMonologue(2f, 20);
			COMFORT_ROOM.SetActive(false);
			COMFORT_OBJECTS.SetActive(false);
			NURSERY_ROOM.SetActive(true);

			break;
		case InteractiveEvent.LOBBY_MESSAGE:
			InteractiveObjectManager.Instance.TextEffects("Lobby/LOBBYMessage",1f, 0.2f);
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds8,true, 0.2f);	
			HALLWAY_ROOM.SetActive(false);
			HALLWAY_OBJECTS.SetActive(false);
			break;
		case InteractiveEvent.EAST_LOBBY:
			MovingObjectManager.Instance.MoveWheels(true);
			SoundManager.Instance.SoundEffects(ObjectSFX.Wheels,true, 0.2f);	
			break;
		case InteractiveEvent.BABY_IN:
			InteractiveObjectManager.Instance.MoveBaby(new Vector3(24f, 0f, 22f));
			InteractiveObjectManager.Instance.HideBaby(true);
			state = "Baby In";
			EnableGo(0.2f,true);
			NURSERY_OBJECTS.SetActive(true);
			break;
			
		case InteractiveEvent.ENTER_NURSERY:
			PlayerController.Instance.MyMonologue(2f, 18);
			InteractiveObjectManager.Instance.EnableBaby(false);
			InteractiveObjectManager.Instance.EnableMama(0f, false);
			InteractiveObjectManager.Instance.HideBaby(false);
			InteractiveObjectManager.Instance.MoveBaby(new Vector3(0,0,0));
			break;
		case InteractiveEvent.EXPLORE_NURSERY:
			InteractiveObjectManager.Instance.HideBaby(false);
			mamawaypoint.transform.position = new Vector3(30.3f, 0f, 7f);
			InteractiveObjectManager.Instance.MoveMama(0f, mamawaypoint.transform.position);
			InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.CRY);
			InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find("Nursery/Baby Bag").transform);
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleHumming, true, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.BabyCry, true, 0.2f);	
			state = "Ready Scare";
			break;
		case InteractiveEvent.HIDE_MAMA:
			PlayerController.Instance.MoveTo(7f, new Vector3(30f,0f,7f), false);
			InteractiveObjectManager.Instance.HideMama(7f, false);
			InteractiveObjectManager.Instance.EnableMama(7f, false);
			InteractiveObjectManager.Instance.MoveMama(7f, new Vector3(0f, 0f, 7f));
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleBreathing1, false, 10.8f);
			InteractiveObjectManager.Instance.Wiggle();
			break;
		case InteractiveEvent.MAMA_ATTACK:
			InteractiveObjectManager.Instance.EnableMama(0f, true);
			InteractiveObjectManager.Instance.MoveMama(0f, new Vector3(29f, 0f, 7f));
			InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.ATTACK);
			InteractiveObjectManager.Instance.MamaGoto(WayPoints.PLAYER);
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleScream, true, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleHumming, false, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds4, true, 0.2f);	
			SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 1f);
			break;
		case InteractiveEvent.PLAYER_HIT:
			if(state == "Dull"){
				SetClear();
				DizzyMode(true);
				SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds7, true, 0.2f);	
				EnableGo(0.2f,true);
				SoundManager.Instance.SoundVol(ObjectSFX.AmbientSound, 0.01f);
			}else if(state == "Last Bite"){
				LightManager.Instance.OffGameLight(myLights.WARD_LIGHT);
				PlayerController.Instance.DeadState();
				PlayerController.Instance.CallRestart(30f);
				SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 5f);
				SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds17, true, 0.2f);
			}

			break;
		case InteractiveEvent.INCUBATOR:
			InteractiveObjectManager.Instance.StopWiggle();
			InteractiveObjectManager.Instance.HideMama(0f, true);
			InteractiveObjectManager.Instance.MoveMama(0f, new Vector3(28.32f, 0, 7.22f));
			InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find("Player").transform);
			InteractiveObjectManager.Instance.EnableMama(0f, true);
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleBreathing1, true, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.BabyCry, false, 0.2f);	
			SoundManager.Instance.SoundEffects(ObjectSFX.BabyStopCry, true, 0.2f);	
			state = "Jump Scare 1";
			break;

		case InteractiveEvent.EXIT_NURSERY:
			DizzyMode(false);
			MORGUE_ROOM.SetActive(true);
			break;
		case InteractiveEvent.REENTER_LOBBY:
			SoundManager.Instance.SoundEffects(ObjectSFX.MetalPounding,true, 0.2f);	
			SoundManager.Instance.SoundVol(ObjectSFX.AmbientSound, 0.2f);
			MORGUE_OBJECTS.SetActive(true);
			NURSERY_ROOM.SetActive(false);
			NURSERY_OBJECTS.SetActive(false);
			PlayerController.Instance.MyMonologue(2f, 21);
			break;
		case InteractiveEvent.ENTER_MORGUE:
			LightManager.Instance.LightsOff(myLights.MORGUE_LIGHTS2);
			LOBBY_ROOM.SetActive(false);
			LOBBY_OBJECTS.SetActive(false);

			break;
		case InteractiveEvent.EXPLORE_MORGUE:

			MovingObjectManager.Instance.MoveDoors(Doors.MORGUE_DOOR,"Morgue", -10f,2f, 0.8f);
			SoundManager.Instance.SoundEffects(ObjectSFX.Whisper, true, 0.8f);
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds10, true, 2f);
			break;
		case InteractiveEvent.MORGUE_LIGHTSOFF:
			MovingObjectManager.Instance.ShakeCabinet(MovingGameObjects.CABINET);
			LightManager.Instance.LightsOff(myLights.MORGUE_LIGHTS1);
			PlayerController.Instance.OffFlashLight();
			PlayerController.Instance.GetFlashLight(5.0f, 6f);
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds11, true, 0.2f);
			EnableGo(8f, true);
			state = "Cabinet Surprise";
			break;
		case InteractiveEvent.CABINET_SURPRISE:
			MovingObjectManager.Instance.MoveTo(MovingGameObjects.CABINET, new Vector3(61.43631f,0.72f,54f), 2f, 3f, iTween.EaseType.easeOutExpo ,iTween.LoopType.none);
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds5,true, 0.2f);	
			InteractiveObjectManager.Instance.LookAtMeMom();
			PlayerController.Instance.MyMonologue(4f, 24);
			SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 1f);
			break;
		case InteractiveEvent.FEMALE_LOOKED:
			state = "Jump Scare 3";
			PlayerController.Instance.flashLightFlicker(true);
			//PlayerController.Instance.GetFlashLight(1.5f, 0f);
			break;
		case InteractiveEvent.FEMALE_GONE:
			PlayerController.Instance.flashLightFlicker(false);
			hidingObjects = GameObject.Find("Morgue/Cabinet/mamaOld");
			hidingObjects.transform.localPosition = new Vector3(hidingObjects.transform.position.x, -2f, hidingObjects.transform.position.z);
			PlayerController.Instance.GetFlashLight(0.2f, 8f);
			InteractiveObjectManager.Instance.EnableMama(0f, false);
			SoundManager.Instance.SoundEffects(ObjectSFX.Barefoot, true, 0.8f);
			PlayerController.Instance.MyMonologue(2f, 32);
			break;
		case InteractiveEvent.GUARDS_PANIC:
			PlayerController.Instance.GetFlashLight(0.2f, 8f);
			InteractiveObjectManager.Instance.MoveMama(0f, new Vector3(31.39f, 0f, 55.4f));
			InteractiveObjectManager.Instance.EnableMama(0f, true);
			InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find(WayPoints.PLAYER).transform);
			PlayerController.Instance.MyMonologue(3f, 23);
			LightManager.Instance.LightsOn(myLights.MORGUE_LIGHTS2);
			InteractiveObjectManager.Instance.HideMama(0f, true);
			SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 0.01f);
			state = "Jump Scare 4";
			break;
		case InteractiveEvent.MAMA_CHASE:
			SoundManager.Instance.SoundEffects(ObjectSFX.CreepyCrawl, true, 0.2f);
			mamawaypoint.transform.position = new Vector3(47.26f,0f,55.12f);
			InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.CRAWL);
			InteractiveObjectManager.Instance.MamaGoto(WayPoints.MAMAWAY_POINT);
			PlayerController.Instance.MyMonologue(2f, 30);
			LightManager.Instance.FlickerLights(myLights.MORGUE_LIGHTS2, true, 2f, 0.8f);
			HideMama();
			break;
		case InteractiveEvent.WARD_HALL:
			InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.IDLE);
			SoundManager.Instance.SoundEffects(ObjectSFX.ChildWhispers, true, 0.2f);
			MovingObjectManager.Instance.MoveDoors(Doors.MORGUE_DD1, "Morgue", -180f, 1f, 0.8f);
			MovingObjectManager.Instance.MoveDoors(Doors.MORGUE_DD2, "Morgue", 0f, 1f, 0.8f);
			PlayerController.Instance.MyMonologue(3f, 31);
			WARD_ROOM.SetActive(true);
			WARD_OBJECTS.SetActive(true);

			break;
		case InteractiveEvent.ENTER_WARD:
			LightManager.Instance.FogState(myLights.WARD_LIGHT, 100f);
			MovingObjectManager.Instance.MoveDoors(Doors.WARD_DOOR1, "Ward", 90f, 1f, 0.8f);
			MovingObjectManager.Instance.MoveDoors(Doors.WARD_DOOR2, "Ward", -90f, 1f, 0.8f);
			MORGUE_ROOM.SetActive(false);
			MORGUE_OBJECTS.SetActive(false);
			LightManager.Instance.LightsOff(myLights.WARD_LIGHT);
			break;
		case InteractiveEvent.WARD_EXPLORE:
			InteractiveObjectManager.Instance.TextFade();
			LightManager.Instance.FlickerLights(myLights.WARD_LIGHT,true,3f,2f);
			SoundManager.Instance.SoundEffects(ObjectSFX.FemaleBreathing2,true, 0.2f);
//			camDrunkR.enabled = true;
//			camDrunkL.enabled = true;
			DizzyMode(true);

			break;
		case InteractiveEvent.GAME_EXIT:
			isRestarting = true;
			isReady = false;
			Application.LoadLevelAsync(0);
			break;
		case InteractiveEvent.OFF_DIZZY:
			DizzyMode(false);
			InteractiveObjectManager.Instance.TextEffects("Player/CardboardMain/Head/Ending Message 1", 1f, 5f);
			InteractiveObjectManager.Instance.TextEffects("Player/CardboardMain/Head/Ending Message 2", 1f, 11f);
			InteractiveObjectManager.Instance.TextEffects("Player/CardboardMain/Head/Ending Message 3", 1f, 16f);
			InteractiveObjectManager.Instance.TextEffects("Player/CardboardMain/Head/Logo", 1f, 20f);

			break;
		}
	}
	
	void OnGazeInput(GazeInputEvent ev, string data) {
		int val = 0;
		if (!isRestarting) {
			switch (ev) {
			case GazeInputEvent.ON_GAZE_START:
				Line = GameObject.Find("Line");
				thisObject = GameObject.Find(data);
				//Debug.Log(thisObject.name);
				if (thisObject != null) {
					if (thisObject.tag == "Interactive Objects") {
						Line.transform.position = thisObject.transform.position;
						iTween.ScaleTo(Line, iTween.Hash("y", 1f, "time", 0.2f, "delay", 0.2f, "easetype", iTween.EaseType.linear));//, "onComplete", "ObjectMonologue", "onCompleteTarget", gameObject, "oncompleteparams", data));
						
						if(isReady){
							if(data == "Flash Light") {
								InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ON_GET_FLASHLIGHT);
							}
							else if(data == "teddy"){
								val = 11;
							}
							else if(data == "Guard Hat"){
								val = 3;
							}
							else if(data == "Flash Light"){
								val = 5;
							}
							else if(data == "Record Book"){
								val = 7;
							}
							else if(data == "radio"){
								val = 8;
							}
							else if(data == "Oxygen Tank"){
								val = 10;
							}
							else if (data == "Faucet"){
								EnableGo(2f,true);
								SoundManager.Instance.SoundEffects(ObjectSFX.Faucet,false, 0.2f);	
							}else if(data == "mama"){
								val = 28;

								if(state == "Jump Scare 4"){
									InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.MAMA_CHASE);
									SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds13, true, 0.2f);
									state = "Jump Scare 4 Done";
								}
							}
							else if(data == "Baby Bag"){
								val = 27;
							}
							else if(data == "Corps"){
								val = 22;
							}else if (data == "mama") {
								PlayerController.Instance.flashLightFlicker (true);
							} else {
								PlayerController.Instance.flashLightFlicker (false);
							}

						}
						else
						{
							if(data == "Guard Hat"){
								val = 1;
							}
							else if(data == "Flash Light"){
								SoundManager.Instance.SoundEffects(ObjectSFX.FlashLight,true, 0.2f);	
								val = 2;
							}
							else if(data == "Record Book"){
								val = 6;
							}
							else if(data == "radio"){
								val = 4;
							}

							if (interactiveObject [0].lookedAt && interactiveObject [1].lookedAt && 
							    interactiveObject [2].lookedAt && interactiveObject [3].lookedAt) {
								state = "Emergency Flicker";
								InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.LIGHTS_FLICKER);
								for (int i = 0; i <= 3; i++) {
									interactiveObject [i].lookedAt = false;
								}
							}
						}
						
					}else 
					{
						Line.transform.localScale = new Vector3(Line.transform.localScale.x, 0, Line.transform.localScale.z);
					}

					if (data == "ERMessage") {
						InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.ER_MESSAGE);
					} else if (data == "CRMessage") {
						InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.CR_MESSAGE);
					} else if (data == "LOBBYMessage") {
						InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.LOBBY_MESSAGE);
					}else if(data == "HWMessage"){
						InteractiveObjectManager.Instance.TextEffects("Hallway/HWMessage",1f, 0.2f);
					}else if(data == "WARDMessage"){

						InteractiveObjectManager.Instance.TextEffects("Ward/WARDMessage",3f, 0.2f);
						InteractiveObjectManager.Instance.TextEffects("Ward/WARDMessage 2",5f, 0.2f);
						InteractiveObjectManager.Instance.TextEffects("Ward/WARDMessage 3",6f, 0.2f);
						InteractiveObjectManager.Instance.TextEffects("Ward/WARDMessage 4",7f, 0.2f);
						InteractiveObjectManager.Instance.TextEffects("Ward/WARDMessage 5",3f, 0.2f);

					}

					if (data.StartsWith ("GoTo_")) {
						PlayerController.Instance.checkDestination ();
					} else {
						PlayerController.Instance.Unload ();
					}
					
				}
			

				PlayerController.Instance.MyMonologue(0.2f,val);

				break;
			case GazeInputEvent.IS_STARRING_AT:

				if (data != null) {

					if (data.ToString() == "Baby Bag") {
						InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.INCUBATOR);
					}else if(data.ToString() == "Cabinet"){
						InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.CABINET_SURPRISE);
					}
					else if(data.ToString() == "mama"){


					}
					if (data.StartsWith ("GoTo_")) {

						if(isReady){
							PlayerController.Instance.goTo ();
							GoTo.Instance.GotoPos();
						}
						else
						{
							EnableGo(0.1f, false);
							PlayerController.Instance.MoveTo( 3f, new Vector3(-4.2f, 1f, 4.3f), false);
							PlayerController.Instance.StartPos(10f);
							MovingObjectManager.Instance.MoveDoors(Doors.ER_DOOR2,"INTRO", 85f,1f,5f);
							MovingObjectManager.Instance.MoveDoors(Doors.ER_DOOR3,"INTRO", -85f,1f,5f);
							MovingObjectManager.Instance.MoveDoors(Doors.ER_DOOR2,"INTRO", 0f,1f,5f);
							MovingObjectManager.Instance.MoveDoors(Doors.ER_DOOR3,"INTRO", 0f,1f,5f);
							SoundManager.Instance.SoundEffects(ObjectSFX.Intro, true, 1f);
							LightManager.Instance.LightFade(myLights.ER_LIGHTS, true, 3f, 9f, "In");
							InteractiveObjectManager.Instance.TextEffects("myIntro/IntroText", 2f, 0.2f);
							//Invoke("StartGame", 3f);
							//DebugText += " start pos Call";
						}
					}
				}
				break;
			}
		}
	}


	void OnPlayerStateChange(PlayerEvent pe, Hashtable data){
		switch (pe) {

		case PlayerEvent.ON_PLAYER_MOVE:

			Debug.Log(data["playerMoves"].ToString());
			if (data ["playerMoves"].ToString()	 == "0") {
				//PlayerController.Instance.MoveTo(0f, new Vector3(1.95f, 0f, 6.81f),false);
				//InteractiveObjectManager.Instance.RadioChange(2);
				Debug.Log("Intro");
			}else if (data ["playerMoves"].ToString()  == "1") {
				PlayerController.Instance.MoveTo(0f, new Vector3(1.95f, 0f, 6.81f),false);
				InteractiveObjectManager.Instance.RadioChange(2);
			}else if (data ["playerMoves"].ToString()	 == "2" && hasChildLaugh) {
				PlayerController.Instance.MoveTo(0f, new Vector3(-0.37f, 1f, 12.62f),false);
				InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.OPEN_HWDOOR);
				PlayerController.Instance.MyMonologue(2f, 12);
			}
			else if(data ["playerMoves"].ToString() == "3"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
				MovingObjectManager.Instance.MoveDoors(Doors.CR_DOOR1,"CR",-180f,1f,5f);
				COMFORT_OBJECTS.SetActive(true);

			}else if(data ["playerMoves"].ToString() == "4"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,false);
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ENTER_CR);
				EMERGENCY_ROOM.SetActive(false);
			}
			else if(data ["playerMoves"].ToString() == "5"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,false);

			}
			else if(data ["playerMoves"].ToString() == "6"){
				//InteractiveObjectManager.Instance.MoveMama(new Vector3(-0.319f,0f, 23f));
				mamawaypoint.transform.position = new Vector3(6f,0.06f, 33f);
				InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find(WayPoints.MAMAWAY_POINT).transform);
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
				InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.WALK);
				InteractiveObjectManager.Instance.MamaGoto(WayPoints.MAMAWAY_POINT);
				SoundManager.Instance.SoundEffects(ObjectSFX.CreepyCrawl,true, 0.2f);		
				LOBBY_ROOM.SetActive(true);
				//Spool.Spawn(Stages.LOBBY_ROOM);
			}
			else if(data ["playerMoves"].ToString() == "7"){
				MovingObjectManager.Instance.MoveDoors(Doors.CR_DOOR1,"CR", 270f,1f,0.8f);
				MovingObjectManager.Instance.MoveDoors(Doors.HALLWAY,"Hallway Door", 360f, 1f, 3f);
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
				SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds15, true, 1f);
				SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 1f);
				LOBBY_OBJECTS.SetActive(true);
			}
			else if(data ["playerMoves"].ToString() == "8"){
				PlayerController.Instance.MoveTo(0f, new Vector3(17.54f,0f,28.24f),true);
				SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 0.01f);
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ENTER_LOBBY);

			}
			else if(data ["playerMoves"].ToString() == "9"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,false);

				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ENTER_NURSERY);
			}
			else if(data ["playerMoves"].ToString() == "10"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.EXPLORE_NURSERY);
			}
			else if(data ["playerMoves"].ToString() == "11"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);

			}
			else if(data ["playerMoves"].ToString() == "12"){
				//Invoke("HideMama", 5f);
				HideMama();
				PlayerController.Instance.MyMonologue(4f, 28);
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,false);
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.LIGHTS_FLICKER);
				//Rooms[3].SetActive(false);
			}
			else if(data ["playerMoves"].ToString() == "13"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.EXIT_NURSERY);
				SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 0.01f);
				//EnableGo(0.2f,true);
			}
			else if(data ["playerMoves"].ToString() == "14"){
				SoundManager.Instance.SoundEffects(ObjectSFX.FemaleCry,true, 0.2f);	
				PlayerController.Instance.MoveTo(0f, new Vector3(30f,0f,29f),true);
			}
			else if(data ["playerMoves"].ToString() == "15"){
				SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds14, true, 1f);
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.REENTER_LOBBY);
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
			}
			else if(data ["playerMoves"].ToString() == "16"){
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ENTER_MORGUE);
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
			}
			else if(data ["playerMoves"].ToString() == "17"){
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.EXPLORE_MORGUE);
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
			}
			else if(data ["playerMoves"].ToString() == "18"){
				state = "Jump Scare 2";
				PlayerController.Instance.MoveTo(0f, new Vector3(56.06f,0,51.34f),false);
				//Invoke("SetFlicker", 2f);
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.LIGHTS_FLICKER);
				PlayerController.Instance.MyMonologue(3f, 27);
			}
			else if(data ["playerMoves"].ToString() == "19"){
				//InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.CABINET_SURPRISE);
				PlayerController.Instance.MoveTo(0f, new Vector3(59.4f, 0f, 55.45f),true);
			}
			else if(data ["playerMoves"].ToString() == "20"){
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.GUARDS_PANIC);
				PlayerController.Instance.MoveTo(0f, new Vector3(48.11f,0,55f),false);
			}
			else if(data ["playerMoves"].ToString() == "21"){
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
			}
			else if(data ["playerMoves"].ToString() == "22"){
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.WARD_HALL);
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,true);
			}
			else if(data ["playerMoves"].ToString() == "23"){
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.ENTER_WARD);
				PlayerController.Instance.MoveTo(0f, new Vector3(78.18f, 0f, 48.41f),true);
			}
			else if(data ["playerMoves"].ToString() == "24"){
				state = "Jump Scare 5";
				PlayerController.Instance.MoveTo(0f, GoTo.Instance.nextPosition,false);
			}
			else if(data ["playerMoves"].ToString() == "25"){
				//PlayerController.Instance.MoveTo(GoTo.Instance.nextPosition,true);
			}
			else if(data ["playerMoves"].ToString() == "26"){
				//PlayerController.Instance.MoveTo(GoTo.Instance.nextPosition,true);
			}

			break;
			
			
		case PlayerEvent.ON_PLAYER_LOOKING_AT:
			float Angle;
			Angle = (float)data["PlayerAngle"];

			if((data["Directions"] == "East" && Angle <= 30f || data["Directions"] == "West" && Angle <= 300f)  && state == "Laughing Child"){
				//move curtain
				SoundManager.Instance.SoundEffects(ObjectSFX.Child,true, 0.2f);	
				MovingObjectManager.Instance.MoveTo(MovingGameObjects.CURTAIN_MOVER, new Vector3(-1f, 0.6f, 9.74f), 0.8f, 0.2f, iTween.EaseType.linear, iTween.LoopType.none);
				state = "Curtain Moved";
			}else if(data["Directions"] == "West" && state == "Jump Scare 1"){
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.MAMA_ATTACK);
				state = "Dull";
			}
			else if((data["Directions"] == "East" && Angle >= 133f || data["Directions"] == "West" && Angle <= 205f) && state == "Baby In" ){
				InteractiveObjectManager.Instance.EnableBaby(true);
				InteractiveObjectManager.Instance.BabyWalk();
				SoundManager.Instance.SoundEffects(ObjectSFX.ChildRun,true, 0.2f);	
				SoundManager.Instance.SoundEffects(ObjectSFX.Child,true, 0.2f);	
				SoundManager.Instance.SoundEffects(ObjectSFX.Child1,true, 0.2f);
				SoundManager.Instance.SoundEffects(ObjectSFX.Child2,true, 0.2f);	
				SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds6,true, 0.2f);	
				PlayerController.Instance.MyMonologue(2f,25);
				state = "Baby out";
			}
			else if((data["Directions"] == "South" && Angle >= 170f || data["Directions"] == "North" && Angle <= 350f) && state == "Jump Scare 3"){
				InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.FEMALE_GONE);
				state = "mama Gone";
			}else if((data["Directions"] == "West" && Angle <= 300f || data["Directions"] == "West" && Angle >= 250f) && state == "Jump Scare 5"){
				SoundManager.Instance.SoundEffects(ObjectSFX.FemaleBreathing2, false, 0.2f);
				InteractiveObjectManager.Instance.EnableMama(0f, false);
				InteractiveObjectManager.Instance.MoveMama(0f, new Vector3(82.97f, 0f, 48.69f));
				mamawaypoint.transform.position = new Vector3(76.94f, 0f, 48.5f);
				InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find(WayPoints.PLAYER).transform);
				InteractiveObjectManager.Instance.EnableMama(0.2f, true);
				MovingObjectManager.Instance.spawObject(ObjectSFX.FemaleScream, new Vector3(81.46f, 0f, 49f), 0.2f);
				state = "Jump Scare 5 Done";
			}
			else if((data["Directions"] == "South" && Angle <= 150f || data["Directions"] == "North" && Angle >= 35f) && state == "Jump Scare 5 Done"){
				SoundManager.Instance.SoundEffects(ObjectSFX.FemaleScream, true, 0.2f);
				InteractiveObjectManager.Instance.MamaLookAt(GameObject.Find(WayPoints.PLAYER).transform);
				InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.BITE);
				InteractiveObjectManager.Instance.MamaGoto(WayPoints.PLAYER);
				//PlayerController.Instance.MyMonologue(2f, 36);
				state = "Last Bite";
			} 

//			Debug.Log(data["Directions"].ToString() + " " + Angle.ToString() + state);
			break;
			
		case PlayerEvent.ON_PLAYER_NEW_POS:
			if(data["isDestinationReady"].ToString() == "True"){
				EnableGo(0.2f,true);
			}else if (data["isDestinationReady"].ToString() == "False"){
				EnableGo(0.2f,false);
			}
			break;
		}
		
	}
	
	
	void EnableGo(float time, bool val){
		InteractiveObjectManager.Instance.GoHere(time,val);
	}
	

	
	void SetClear(){
		//LightManager.Instance.LightFade("HWLights");
		InteractiveObjectManager.Instance.EnableMama(2f, false);
		InteractiveObjectManager.Instance.MoveMama(2f, new Vector3(0f, 0f, 0f));
		InteractiveObjectManager.Instance.MamaGoto(WayPoints.MAMA);
		InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.IDLE);
	}

	void DizzyMode(bool val){
		GameObject[] cam;
		CameraFilterPack_FX_Drunk DizzyCam;
		cam = GameObject.FindGameObjectsWithTag ("DizzyCam");

		foreach(GameObject camCount in cam){
			DizzyCam = camCount.GetComponent<CameraFilterPack_FX_Drunk>();
			DizzyCam.enabled = val;
		}


		//CameraFilterPack_FX_Drunk DizzyCam = GameObject.FindGameObjectWithTag("DizzyCam").GetComponent<CameraFilterPack_FX_Drunk>();

	}
	
	void SetHWLights(){
		InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.HALLWAY_LIGHTSOFF);
	}
	
//	void SetFlicker() {
//		InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.LIGHTS_FLICKER);
//	}

	void HideMama(){
	
		if(state == "Ready Scare"){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.HIDE_MAMA);
		}
		else if(state == "Jump Scare 4"){
			InteractiveObjectManager.Instance.EnableMama(2f, false);
			InteractiveObjectManager.Instance.MoveMama(2f, new Vector3(0f,0f,0f));
			SoundManager.Instance.SoundEffects(ObjectSFX.EerieSounds16, true, 0.2f);
			SoundManager.Instance.SoundVol(ObjectSFX.PlayerHeartBeat, 1f);
			//InteractiveObjectManager.Instance.MamaAction(MamaActionTriggers.IDLE);
			EnableGo(2f, true);
		}
	}
	
	void InstantiateObject(string name){
		
		GameObject gameObj = Instantiate(Resources.Load(name)as GameObject);
		gameObj.name =  name;
		
	}
	
	void StartGame(){
		InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.START);

	}
	
}
