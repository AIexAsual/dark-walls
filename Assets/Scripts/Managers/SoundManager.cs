﻿using UnityEngine;
using System.Collections;

public class ObjectSFX{
	public static string EerieSounds1 = "SFX/EerieSounds1";
	public static string EerieSounds2 = "SFX/EerieSounds2";
	public static string EerieSounds3 = "SFX/EerieSounds3";
	public static string EerieSounds4 = "SFX/EerieSounds4";
	public static string EerieSounds5 = "SFX/EerieSounds5";
	public static string EerieSounds6 = "SFX/EerieSounds6";
	public static string EerieSounds7 = "SFX/EerieSounds7";
	public static string EerieSounds8 = "Lobby/LOBBYMessage";
	public static string EerieSounds9 = "SFX/EerieSounds9";
	public static string EerieSounds10 = "SFX/EerieSounds10";
	public static string EerieSounds11 = "SFX/EerieSounds11";
	public static string EerieSounds12 = "SFX/EerieSounds12";
	public static string EerieSounds13 = "SFX/EerieSounds13";
	public static string EerieSounds14 = "SFX/EerieSounds14";
	public static string EerieSounds15 = "SFX/EerieSounds15";
	public static string EerieSounds16 = "SFX/EerieSounds16";
	public static string EerieSounds17 = "SFX/EerieSounds17";
	public static string RadioSound = "Emergency/radio";
	public static string Faucet = "SFX/Faucet";
	public static string AmbientSound = "SFX/Ambient Sound";
	public static string Child = "SFX/Child";
	public static string Child1 = "SFX/Child low";
	public static string Child2 = "SFX/Child high";
	public static string ChildRun = "Interactive Objects/Baby/SFX";
	public static string FemaleCry = "Player/CardboardMain/Head/Behind Cry";
	public static string FemaleCough = "SFX/Female_Coughing";
	public static string FemaleScream = "SFX/Female Scream";
	public static string FemaleHumming = "SFX/Humming";
	public static string FemaleBreathing1 = "Interactive Objects/mama";
	public static string FemaleBreathing2 = "SFX/Female_Breathing"; 
	public static string DoorPounding  = "SFX/Door Pounding";
	public static string ChildWhispers = "SFX/Child Whispers";
	public static string BabyStopCry = "SFX/Baby Stop Cry";
	public static string CreepyCrawl = "Interactive Objects/mama/CreepyCrawl";
	public static string MetalPounding = "SFX/Metal Pounding";
	public static string LightFlicker = "SFX/Light Bulb Flicker";
	public static string PianoSlam = "SFX/Piano Slam";
	public static string FlashLight = "Player/CardboardMain/Head/Spot light";
	public static string OxygenTank = "Emergency/Oxygen Tank";
	public static string FaucetOn = "Comfort/Faucet";
	public static string Wheels = "Lobby/Wheel Chair";
	public static string BabyCry = "Nursery/Baby Bag";
	public static string OpenCabinet = "Morgue/Cabinet";
	public static string CorpsCloth = "Morgue/Corps";
	public static string Barefoot = "SFX/Barefootstep2";
	public static string ISeeYou = "SFX/I See You";
	public static string BGM = "SFX/BGM";
	public static string Whisper = "SFX/Whispers";
	public static string Intro = "SFX/Intro";
	public static string PlayerHeartBeat = "Player";
}

	public class SoundManager {
		protected SoundManager() {}
		private static SoundManager _instance = null;
		
		// Singleton pattern implementation
		public static SoundManager Instance { 
			get {
				if (SoundManager._instance == null) {
					SoundManager._instance = new SoundManager(); 
				}  
				return SoundManager._instance;
			} 
		}


	SFX sfx;

	public void SoundEffects(string name, bool val, float time){
		GameObject mySFX = GameObject.Find (name);
		sfx = mySFX.GetComponent<SFX>();
		sfx.TriggerSound(val,time);
	}



	public void SoundVol(string name, float val){
		GameObject mySFX = GameObject.Find (name);
		sfx = mySFX.GetComponent<SFX>();
		sfx.SoundVol(val);
	}
}
