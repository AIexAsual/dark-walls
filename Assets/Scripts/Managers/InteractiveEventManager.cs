﻿using UnityEngine;
using System.Collections;


public enum InteractiveEvent { 
		NULL_STATE, 
		INTRO,
		START,
		LIGHTS_FLICKER,
		LIGHTS_OFF,
		ON_GET_FLASHLIGHT,
		ER_MESSAGE,
		LAUGHING_CHILD,
		OPEN_HWDOOR,
		ER_EXIT,
		ENTER_HALLWAY,
		HALLWAY_LIGHTSOFF,
		WINDOW_MAMA,
		ENTER_CR,
		CR_NOICE,
		CR_MESSAGE,
		ENTER_LOBBY,
		LOBBY_MESSAGE,
		EAST_LOBBY,
		BABY_IN,
		ENTER_NURSERY,
		EXPLORE_NURSERY,
		HIDE_MAMA,
		MAMA_ATTACK,
		PLAYER_HIT,
		INCUBATOR,
		MORGUE_NOISE,
		EXIT_NURSERY,
		REENTER_LOBBY,
		ENTER_MORGUE,
		EXPLORE_MORGUE,
		MORGUE_LIGHTSOFF,
		MORGUE_JUMPSCARE,
		FEMALE_GONE,
		FEMALE_LOOKED,
		CABINET_SURPRISE,
		GUARDS_PANIC,
		MAMA_CHASE,
		MORGUE_EXIT,
		WARD_HALL,
		ENTER_WARD,
		WARD_EXPLORE,
		GAME_EXIT,
		OFF_DIZZY
	}
	
	public class InteractiveEventManager {
	protected InteractiveEventManager() {}
	private static InteractiveEventManager _instance = null;
		
		// Singleton pattern implementation
		public static InteractiveEventManager Instance { 
			get {
				if (InteractiveEventManager._instance == null) {
					InteractiveEventManager._instance = new InteractiveEventManager(); 
					//InteractiveEventManager.Instance =  GameObject.Find("Game Manager").GetComponent<InteractiveEventManager>();
				}  
				return InteractiveEventManager._instance;
			} 
		}

		public delegate void OnStateChangeHandler(InteractiveEvent ev);
		public event OnStateChangeHandler OnStateChangeEvent;
		public InteractiveEvent interactiveState;
		
		public void dispatchEvent(InteractiveEvent stateEvent) {
		interactiveState = stateEvent;
		OnStateChangeEvent (stateEvent);
		}
		
		
		// Use this for initialization
		public void Start () {
			
		}
		// Update is called once per frame
		public void Update () {
			
		}
		
	}