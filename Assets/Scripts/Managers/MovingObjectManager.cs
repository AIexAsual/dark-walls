﻿using UnityEngine;
using System.Collections;


public class Doors{
	public static string ER_DOOR1 = "Door Pivot 1";
	public static string CR_DOOR1 = "CR Door";
	public static string CUBICLE = "Cubicle Door Pivot";
	public static string HALLWAY = "Hallway Troll Door";
	public static string LOBBY_DOOR1 = "Lobby Door 1";
	public static string LOBBY_DOOR2 = "Lobby Door 2";
	public static string MORGUE_DOOR = "Morgue Door";
	public static string MORGUE_DD1 = "Morgue D-Door 1";
	public static string MORGUE_DD2 = "Morgue D-Door 2";
	public static string WARD_DOOR1 = "Ward Door 1";
	public static string WARD_DOOR2 = "Ward Door 2";
	public static string ER_DOOR2 = "ER Door1";
	public static string ER_DOOR3 = "ER Door2";

}

public class MovingGameObjects{
	public static string CABINET = "Morgue/Cabinet";
	public static string CURTAIN_MOVER = "ER Pooling Objects/Curtain Mover";
}

public class WardObjects{
	public static string WALL = "Ward/Wall Main";
	public static string CEILING = "Ward/Ceiling";
	public static string FLOOR = "Ward/Floor";
	public static string CURTAIN = "Ward/Curtain";
	public static string BED = "Ward/Bed";
	public static string DOOR1 = "Ward/Door1";
	public static string DOOR2 = "Ward/Door2";
}

public class MovingObjectManager {

	protected MovingObjectManager() {}  
	private static MovingObjectManager _instance = null;
	
	// Singleton pattern implementation
	public static MovingObjectManager Instance { 
		get {
			if (MovingObjectManager._instance == null) {
				MovingObjectManager._instance = new MovingObjectManager(); 
			}  
			return MovingObjectManager._instance;
		} 
	}

	MovingObjects movingObjects;
	DoorController doorController;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShakeCabinet(string name){
		MovingObjects movingObjects = GameObject.Find (name).GetComponent<MovingObjects>();
		movingObjects.ShakeCabinet();
	}

	public void MoveTo(string name, Vector3 val, float time, float delay, iTween.EaseType iE, iTween.LoopType iL){
		MovingObjects movingObjects = GameObject.Find (name).GetComponent<MovingObjects>();
		movingObjects.MoveTo (val,time,delay,iE,iL);
	}

	public void MoveWheels(bool val){
		MovingObjects movewheels = GameObject.Find("Lobby/Wheel Chair").GetComponent<MovingObjects>();
		movewheels.MoveWheelChair(val);
	}

	public void MoveDoors(string name, string val, float rot,float time, float delay){
		DoorController doorController = GameObject.Find (name).GetComponent<DoorController>();
		doorController.OpenDoors (rot,time,delay,val);
	}

	public void spawObject(string name, Vector3 val, float delay){
		MovingObjects movingObjects = GameObject.Find(name).GetComponent<MovingObjects>();

	}


}
