﻿using UnityEngine;
using System.Collections;

public enum PlayerEvent{
	ON_PLAYER_MOVE,
	ON_PLAYER_NEW_POS,
	ON_PLAYER_LOOKING_AT

}
	public class PlayerEventManager {
		protected PlayerEventManager() {}
		private static PlayerEventManager _instance = null;
		
		// Singleton pattern implementation
		public static PlayerEventManager Instance { 
			get {
				if (PlayerEventManager._instance == null) {
					PlayerEventManager._instance = new PlayerEventManager(); 
				}  
				return PlayerEventManager._instance;
			} 
		} 

	public delegate void OnStateChangeHandler(PlayerEvent ev, Hashtable data);
	public event OnStateChangeHandler OnStateChangeEvent;
	public PlayerEvent playerState;
	
	public void dispatchEvent(PlayerEvent stateEvent, Hashtable data) {
		playerState = stateEvent;
		OnStateChangeEvent (stateEvent,data);
	
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
