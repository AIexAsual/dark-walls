﻿using UnityEngine;
using System.Collections;

public enum AppEvent { 
	NULL_STATE, 
	ER_INTRO,
	ER_LIGHTS_OFF,
	ER_OUTRO
}

public class AppManager {
	protected AppManager() {}
	private static AppManager _instance = null;
	
	// Singleton pattern implementation
	public static AppManager Instance { 
		get {
			if (AppManager._instance == null) {
				AppManager._instance = new AppManager(); 
			}  
			return AppManager._instance;
		} 
	}
	
	public delegate void OnStateChangeHandler(AppEvent ev, Hashtable i);
	public event OnStateChangeHandler OnStateChangeEvent;
	public AppEvent gameState;
	
	public void dispatchEvent(AppEvent gameStateEvent, Hashtable data) {
		gameState = gameStateEvent;
		OnStateChangeEvent (gameStateEvent, data);
	}
	

	// Use this for initialization
	public void Start () {
	
	}
	// Update is called once per frame
	public void Update () {
		
	}
	
}