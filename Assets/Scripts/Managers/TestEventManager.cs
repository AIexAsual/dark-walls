﻿using UnityEngine;
using System.Collections;

public enum testClass{
	START,
	RESTART
}

	public class TestEventManager {
		protected TestEventManager() {}
		private static TestEventManager _instance = null;
		
		// Singleton pattern implementation
	public static TestEventManager Instance { 
			get {
			if (TestEventManager._instance == null) {
				TestEventManager._instance = new TestEventManager(); 
				}  
			return TestEventManager._instance;
			} 
		}

	public delegate void OnStateChangeHandler(testClass tc);
	public event OnStateChangeHandler OnStateChangeEvent;
	public testClass testState;

	public void dispatchEvent(testClass stateEvent) {
		testState = stateEvent;
		OnStateChangeEvent (stateEvent);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
