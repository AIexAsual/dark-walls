using UnityEngine;
using System.Collections;

public class myLights{
	public static string FlashLight = "Emergency/Flash Light";
	public static string ER_LIGHTS = "Lightings/ER Lights/ER Room Light";
	public static string HW_LIGHTS1 = "Lightings/HW Lights/Hallway Lights 1";
	public static string HW_LIGHTS2 = "Lightings/HW Lights/Hallway Lights 2";
	public static string HW_LIGHTS3 = "Lightings/HW Lights/Hallway Lights 3";
	public static string N_LIGHTS = "Lightings/N Lights/N Room Light";
	public static string MORGUE_LIGHTS1 = "Lightings/M Lights/Room Light";
	public static string MORGUE_LIGHTS2 = "Lightings/M Lights/Hallway Light";
	public static string WARD_LIGHT = "Lightings/W Lights/Room Light 1";
}

public class LightManager {

	protected LightManager() {}
	private static LightManager _instance = null;
	
	// Singleton pattern implementation
	public static LightManager Instance { 
		get {
			if (LightManager._instance == null) {
				LightManager._instance = new LightManager(); 
			}  
			return LightManager._instance;
		} 
	}

	GameObject[] lightControllers;

	GameObject myFlashlight;
	InteractiveFlashlight mySpotlight;

	// Use this for initialization
	public void Start () {
		myFlashlight = GameObject.Find ("Emergency/Flash Light");
		mySpotlight = myFlashlight.GetComponent<InteractiveFlashlight>();

	}


	public void OffGameLight(string val){
		LightController myLight = GameObject.Find (val).GetComponent<LightController>();
		myLight.OffGameLight();
	}

	public void LightFlicker(string val){
		lightControllers = GameObject.FindGameObjectsWithTag (val);
		for(int i = 0; i < lightControllers.Length; i++){
			LightController lightController = lightControllers[i].GetComponent<LightController>();
			lightController.OnFlicker(true);
		}
	}

	public void FlickerLights(string name, bool val, float time, float delay){
		LightController lightController = GameObject.Find(name).GetComponent<LightController>();
		lightController.FlickerLights(val, time ,delay);
	}

	public void FlashlightsOn(){
		InteractiveFlashlight mySpotlight = GameObject.Find ("Emergency/Flash Light").GetComponent<InteractiveFlashlight>();
		mySpotlight.LightsOn ();

	}

	public void FlashlightOff(){
		InteractiveFlashlight mySpotlight = GameObject.Find ("Emergency/Flash Light").GetComponent<InteractiveFlashlight>();
		mySpotlight.LightsOff ();
	}

	public void GetFlashlight(){
		//InteractiveFlashlight mySpotlight = GameObject.Find ("Emergency/Flash Light").GetComponent<InteractiveFlashlight>();
		mySpotlight.GetFlashlight ();
	}

	public void LightsOn(string val){
		LightController myLight = GameObject.Find (val).GetComponent<LightController>();
		myLight.OnLights ();
	}

	public void LightsOff(string val){
		LightController myLight = GameObject.Find (val).GetComponent<LightController>();
		myLight.OffLights ();
	}

	public void LightFade(string name, bool val, float delay, float speed, string fadeState){
		LightController myLight = GameObject.Find (name).GetComponent<LightController>();
		myLight.OnFade(val,delay,speed,fadeState);

	}

	public void FogState(string name, float val){
		LightController lightController = GameObject.Find (name).GetComponent<LightController>();
		lightController.FogState (val);
	}
}
