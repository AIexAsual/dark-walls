﻿using UnityEngine;
using System.Collections;

public class DroppingObjects{
	public static string OXYGEN_TANK = "Emergency/Oxygen Tank";
}

public class WayPoints{
	public static string MAMAWAY_POINT = "Interactive Objects/Mama Way Point";
	public static string MAMA = "Interactive Objects/mama";
	public static string PLAYER = "Player";
}

public class MamaActionTriggers{
	public static string IDLE = "toIdle";
	public static string WALK = "toWalk";
	public static string SCREAM = "toScream";
	public static string CRAWL = "toCrawl";
	public static string CRY = "toCry";
	public static string ATTACK = "toAttack";
	public static string BITE = "toBite";
	public static string APPROACH = "toApproach";
}

public class InteractiveObjectManager {
		protected InteractiveObjectManager() {}
		private static InteractiveObjectManager _instance = null;
		
		// Singleton pattern implementation
		public static InteractiveObjectManager Instance { 
			get {
				if (InteractiveObjectManager._instance == null) {
					InteractiveObjectManager._instance = new InteractiveObjectManager(); 
				}  
				return InteractiveObjectManager._instance;
			} 
		}

	InteractiveObject interactiveObjects;
	InteractiveDroppingObject interactiveDroppingObject;
	InteractiveFlashlight interactiveFlashlight;
	InteractiveRadio interactiveRadio;
	InteractiveMama interactiveMama;
	InteractiveBaby interactiveBaby;
	InteractiveBurnMama interactiveBurnMama;
	GoTo goTo;
	Teddy interactiveTeddy;
	InteractiveParticles particle;
	TextAppearance textApp;
	InteractivePlasticBag interactivePlasticBag;
	FadeTexture fadeTexture;

	// Use this for initialization
	public void Start () {

		InteractiveMama	interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		particle = GameObject.Find("Comfort/Faucet").GetComponent<InteractiveParticles>();
		goTo = GameObject.Find("Destinations/GoTo_").GetComponent<GoTo>();
		interactiveRadio = GameObject.Find("Emergency/radio").GetComponent<InteractiveRadio>();
		interactiveTeddy = GameObject.Find("Emergency/teddy").GetComponent<Teddy>();
		interactiveBaby = GameObject.Find ("Interactive Objects/Baby").GetComponent<InteractiveBaby>();
	
		interactivePlasticBag = GameObject.Find("Nursery/Baby Bag").GetComponent<InteractivePlasticBag>();
	}


	// Update is called once per frame
	public void Update () {
		
	}

	public void GoHere(float time,bool val){
		goTo.GoHere(time,val);
	}

	public void DropObject(string name){
		//InteractiveDroppingObject dropobject =  GameObject.Find (name).GetComponent<InteractiveDroppingObject>();
		interactiveDroppingObject =  GameObject.Find (name).GetComponent<InteractiveDroppingObject>();
		interactiveDroppingObject.fallObject ();
	}

	public void RadioChange(int val){

		interactiveRadio.changeClip (val);
	}

	public void SetRagdoll(){

		interactiveTeddy.TeddyLook ();
	}

	public void MamaAction(string val){
		InteractiveMama	interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		interactiveMama.MamaAction (val);
	}

	public void MamaGoto(string val){
		InteractiveMama	interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		interactiveMama.MamaGoto (val);
	}


	public void MoveMama(float delay, Vector3 val){
		InteractiveMama interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		interactiveMama.MamaPos(delay, val);

	}

	public void MamaLookAt(Transform val){
		InteractiveMama	interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		interactiveMama.MamaLookAt(val);
	}

	public void MamaScream(){
		InteractiveMama	interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		interactiveMama.MamaScream();
	}

	public void EnableMama(float delay, bool val){
		InteractiveMama	interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		interactiveMama.EnableMama(delay, val);
	}

	public void HideMama(float delay, bool val){
		InteractiveMama	interactiveMama = GameObject.Find ("Interactive Objects/mama").GetComponent<InteractiveMama>();
		interactiveMama.HideMama (delay, val);
	}

	public void LookAtMeMom(){
		InteractiveBurnMama	interactiveBurnMama = GameObject.Find("Morgue/Cabinet/mamaOld").GetComponent<InteractiveBurnMama>();
		interactiveBurnMama.lookAtMeMom();

	}

	public void TextFade(){
		GameObject[] RoomFadeTexture = GameObject.FindGameObjectsWithTag ("TextFadeRoom");
		for(int i = 0; i < RoomFadeTexture.Length; i++){
			FadeTexture fadeTexture = RoomFadeTexture[i].GetComponent<FadeTexture>();
			fadeTexture.FadeMat();
		}

	}

	public void HideBaby(bool val){

		interactiveBaby.HideBaby(val);
	}

	public void BabyWalk(){

		interactiveBaby.BabyWalk ();
	}
	
	
	public void MoveBaby(Vector3 val){

		interactiveBaby.BabyPos(val);
		
	}
	
	
	public void BabyScream(){

		interactiveBaby.BabyScream();
	}
	
	public void EnableBaby(bool val){

		interactiveBaby.EnableBaby(val);
	}


	public void OpenFaucet(bool val){

		particle.Emitter(val);
	}

	public void TextEffects(string name, float val, float delay){
		GameObject textObject;
		textObject = GameObject.Find(name);
		TextAppearance textApp = textObject.GetComponent<TextAppearance> ();
		textApp.Fade (val, delay, name);
	}

	
	public void TextEnable(string name, bool val){
		TextAppearance textApp = GameObject.Find(name).GetComponent<TextAppearance>();
		textApp.EnableCollision(val);
	}

	public void Wiggle(){

		interactivePlasticBag.Wiggle();
	}

	
	public void StopWiggle(){

		interactivePlasticBag.StopWiggle();
	}

	}