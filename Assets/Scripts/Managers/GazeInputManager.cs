﻿using UnityEngine;
using System.Collections;

public enum GazeInputEvent { 
	NULL_STATE, 
	ON_GAZE_START,
	IS_STARRING_AT
}

public class GazeInputManager {
	protected GazeInputManager() {}
	private static GazeInputManager _instance = null;
	
	// Singleton pattern implementation
	public static GazeInputManager Instance { 
		get {
			if (GazeInputManager._instance == null) {
				GazeInputManager._instance = new GazeInputManager(); 
			}  
			return GazeInputManager._instance;
		} 
	}
	
	public delegate void OnStateChangeHandler(GazeInputEvent ev, string i);
	public event OnStateChangeHandler OnStateChangeEvent;
	public GazeInputEvent gameState;
	
	public void dispatchEvent(GazeInputEvent gameStateEvent, string data) {
		gameState = gameStateEvent;
		OnStateChangeEvent (gameStateEvent, data);
	}
	

	// Use this for initialization
	public void Start () {
	
	}
	// Update is called once per frame
	public void Update () {
		
	}
	
}