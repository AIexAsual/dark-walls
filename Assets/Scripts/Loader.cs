﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour {
	Animator anim;
	public bool isDoneload = false;
	float val = 1;
	bool goLoad = false;
	float t;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Unload(){
		anim.SetBool("isLoading", false);
	}

	public void Load(){
		anim.SetBool("isLoading", true);
	}

	void changeVal(float value){
		val = value;
	}

	void doneLoading(){
		isDoneload = true;
	}
	
}
