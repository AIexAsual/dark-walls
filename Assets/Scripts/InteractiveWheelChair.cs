﻿using UnityEngine;
using System.Collections;

public class InteractiveWheelChair : MonoBehaviour {

	public GameObject[] wheels;
	float turn;
	Vector3 oldPos;
	Vector3 curPos;
	Vector3 movementPos;
	Vector3 fwd;

	// Use this for initialization
	void Start () {
		oldPos = gameObject.transform.position;

	}
	
	// Update is called once per frame
	void Update () {
		curPos = gameObject.transform.position;
		movementPos = curPos - oldPos;

		for(int i = 0; i < 2; i++){
			wheels[i].transform.Rotate(Vector3.forward * Time.deltaTime * Vector3.Dot(fwd, movementPos) * 80000);
		}

		}

	void LateUpdate(){
		oldPos = transform.position;
		fwd = transform.forward;
	}


	void OnCollisionEnter(Collision collision) {
		if(collision.collider.name == "Stopper"){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.BABY_IN);

		}


			
	}

}
