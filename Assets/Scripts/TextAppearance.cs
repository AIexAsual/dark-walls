﻿using UnityEngine;
using System.Collections;

public class TextAppearance : MonoBehaviour {

	string room;

	// Use this for initialization
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Fade(float val, float delay, string name){
		iTween.FadeTo(gameObject, iTween.Hash("alpha", val, "time", val, "delay", delay, "easetype", iTween.EaseType.linear, "onComplete", "Appeared", "onCompleteTarget", gameObject));
		room = name;
		EnableCollision(false);
	}

	void Appeared(){

//		if(room.StartsWith("CR")){
//			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.EXIT_CR);
//		}else
		if(room.StartsWith("Lobby")){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.EAST_LOBBY);
		}else if(room.StartsWith("Ward")){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.WARD_EXPLORE);
		}else if(room.StartsWith("Player")){
			iTween.FadeTo(gameObject, iTween.Hash ("alpha", 0f, "time", 1f, "delay", 5f, "easetype", iTween.EaseType.easeOutExpo));
		}
		EnableCollision(false);
	}

	public void EnableCollision(bool val){

		if (GetComponent<Collider>() == null)
			return;
		Collider col;
		col = this.GetComponent<Collider> ();
		col.enabled = val;
	}

}
