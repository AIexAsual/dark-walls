﻿using UnityEngine;
using System.Collections;

public class Teddy : RagdollScript {
	Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent <Rigidbody>();
		rb.Sleep();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TeddyLook(){
		GameObject joint = GameObject.Find ("Emergency/teddy/Body/Joint");
		GameObject neck = GameObject.Find ("Emergency/teddy/Body/Joint/Neck");
		iTween.RotateTo (joint, iTween.Hash("y", -260f, "time", 5f,"delay", 2f, "easetype", iTween.EaseType.linear));
		iTween.RotateTo (neck, iTween.Hash("y", -275f, "time", 5f,"delay", 2.8f, "easetype", iTween.EaseType.linear, "onComplete", "DoneLook", "onCompleteTarget", gameObject));
		if(!GetComponent<AudioSource>().isPlaying){
			GetComponent<AudioSource>().Play();
		}
	}

	void DoneLook(){
		rb.isKinematic = false;
		Collider col = gameObject.GetComponent<Collider>();
		rb.AddForce(-transform.forward * 10f);
	}

//	IEnumerator WaitAndGo(float time){
//		yield return new WaitForSeconds(time);
//		Collider col = gameObject.GetComponent<Collider>();
//		col.enabled = false;
//	}
}
