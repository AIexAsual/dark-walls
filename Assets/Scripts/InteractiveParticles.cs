﻿using UnityEngine;
using System.Collections;

public class InteractiveParticles : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetGazedAt (bool gazeAt){
		if(gazeAt)
			Emitter(false);				
	}

	public void Emitter(bool val){
		ParticleSystem particle = GetComponent<ParticleSystem>();
		particle.enableEmission= val;
		if(gameObject.name == "Faucet"){
			InteractiveEventManager.Instance.dispatchEvent(InteractiveEvent.CR_NOICE);
			GetComponent<Collider>().enabled = false;
		}
	}




}
