﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Collider))]
public class InteractiveObject : MonoBehaviour {
	public Material objectMaterial;
	public bool lookedAt = false;

	void Start () {

		SetGazedAt(false);
	}

	public virtual void SetGazedAt(bool gazedAt) {
		if (gazedAt)
			lookedAt = true;
	}

	// Update is called once per frame
	void Update () {

	}
}
