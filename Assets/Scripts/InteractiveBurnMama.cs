﻿using UnityEngine;
using System.Collections;

public class InteractiveBurnMama : MonoBehaviour {
	GameObject head;
	bool isRoll = false;
	GameObject[] bodies;
	Rigidbody rb;
	Animator anim;

	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator>();
		//anim.enabled = false;
		SetKinematic(true);

	}
	
	// Update is called once per frame
	void Update () {

//		if(isRoll){
//			head.transform.localEulerAngles = new Vector3((Random.Range(280f,300f)),0,(Random.Range(10f,15f)));
//			}

	}

	public void lookAtMeMom(){
		SetKinematic(true);
		anim.enabled = true;
		Invoke("waitAndTurn", 4f);
		Invoke("waitAndRagdoll", 8.5f);
	}


	void waitAndTurn(){
		anim.SetTrigger("toSurprise");
		InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.FEMALE_LOOKED);
	}

	void waitAndRagdoll(){
		anim.enabled = false;
		SetKinematic(false);
		//InteractiveEventManager.Instance.dispatchEvent (InteractiveEvent.FEMALE_LOOKED);
	}

	void SetKinematic(bool newValue)
	{
		//Get an array of components that are of type Rigidbody
		Rigidbody[] bodies=GetComponentsInChildren<Rigidbody>();
		
		//For each of the components in the array, treat the component as a Rigidbody and set its isKinematic property
		foreach (Rigidbody rb in bodies)
		{
			rb.isKinematic=newValue;
		}
	}
}
