﻿using UnityEngine;
using System.Collections;

public class MovingObjects : MonoBehaviour {

	bool IsMoveWheels = false; 
	Rigidbody rb;
	Animator animations;
	Collider col;
	// Use this for initialization
	void Start () {
	
		rb = GetComponent<Rigidbody>();
		col = GetComponent<Collider>();
		if(rb != null)
		rb.Sleep();
	}
	
	// Update is called once per frame
	void Update () {

		if(IsMoveWheels){
			transform.Translate(Vector3.back * Time.deltaTime * 1, Space.World);
			rb.AddForce(Vector3.back * 1f);
		}


	}

	public void MoveTo(Vector3 val, float t, float d, iTween.EaseType iE, iTween.LoopType iL){
		if(gameObject.name == "Cabinet"){
			Animator animations = GetComponent<Animator>();
			animations.SetBool("Shake", false);
			animations.SetTrigger("Open");
		}else{
			iTween.MoveTo (gameObject, iTween.Hash ("x", val.x, "z", val.z, "time", t, "delay", d, "easetype", iE, "looptype", iL));
		}
		col.enabled = false;
	}

	public void spawnHere(Vector3 val, float delay){
		StartCoroutine(WaitAndSpawnHere(delay,val));
	}

	IEnumerator WaitAndSpawnHere(float delay, Vector3 val){
		yield return new WaitForSeconds(delay);
		gameObject.transform.position = val;
	}

	public void ShakeCabinet(){
		if(gameObject.name == "Cabinet"){
			Animator animations = GetComponent<Animator>();
			animations.SetBool("Shake", true);
		}
	}

	public void MoveWheelChair(bool val){

		rb.AddForce(Vector3.back * 100f);
	}
}
