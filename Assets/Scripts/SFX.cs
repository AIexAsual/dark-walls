﻿using UnityEngine;
using System.Collections;

public class SFX : MonoBehaviour {
	AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TriggerSound(bool val, float time){
		if(val){
			StartCoroutine(WaitAndPlaySounds(time));
		}else{
			audioSource.Stop();	
		}

	}

	IEnumerator WaitAndPlaySounds(float time){
		yield return new WaitForSeconds(time);
		if(!audioSource.isPlaying){
			audioSource.Play ();
		}
		
	}

	public void SoundVol(float val){
		audioSource.volume = val;
	}
}
