%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Baby
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Character1_Ctrl_Reference
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_ChestEndEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_ChestOriginEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_HeadEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_LeftUpLeg
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_LeftUpLeg/Character1_Ctrl_LeftLeg
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_LeftUpLeg/Character1_Ctrl_LeftLeg/Character1_Ctrl_LeftFoot
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_RightUpLeg
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_RightUpLeg/Character1_Ctrl_RightLeg
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_RightUpLeg/Character1_Ctrl_RightLeg/Character1_Ctrl_RightFoot
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine/Character1_Ctrl_Head
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine/Character1_Ctrl_LeftArm
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine/Character1_Ctrl_LeftArm/Character1_Ctrl_LeftForeArm
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine/Character1_Ctrl_LeftArm/Character1_Ctrl_LeftForeArm/Character1_Ctrl_LeftHand
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine/Character1_Ctrl_RightArm
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine/Character1_Ctrl_RightArm/Character1_Ctrl_RightForeArm
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_Hips/Character1_Ctrl_Spine/Character1_Ctrl_RightArm/Character1_Ctrl_RightForeArm/Character1_Ctrl_RightHand
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_HipsEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_LeftAnkleEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_LeftElbowEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_LeftHipEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_LeftKneeEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_LeftShoulderEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_LeftWristEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_RightAnkleEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_RightElbowEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_RightHipEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_RightKneeEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_RightShoulderEffector
    m_Weight: 0
  - m_Path: Character1_Ctrl_Reference/Character1_Ctrl_RightWristEffector
    m_Weight: 0
  - m_Path: high-polyMesh
    m_Weight: 0
  - m_Path: male1591Mesh
    m_Weight: 0
  - m_Path: python
    m_Weight: 1
  - m_Path: python/Hips
    m_Weight: 1
  - m_Path: python/Hips/LeftUpLeg
    m_Weight: 1
  - m_Path: python/Hips/LeftUpLeg/LeftLeg
    m_Weight: 1
  - m_Path: python/Hips/LeftUpLeg/LeftLeg/LeftFoot
    m_Weight: 1
  - m_Path: python/Hips/LeftUpLeg/LeftLeg/LeftFoot/LeftToeBase
    m_Weight: 1
  - m_Path: python/Hips/RightUpLeg
    m_Weight: 1
  - m_Path: python/Hips/RightUpLeg/RightLeg
    m_Weight: 1
  - m_Path: python/Hips/RightUpLeg/RightLeg/RightFoot
    m_Weight: 1
  - m_Path: python/Hips/RightUpLeg/RightLeg/RightFoot/RightToeBase
    m_Weight: 1
  - m_Path: python/Hips/Spine
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftHandThumb1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftHandThumb1/LeftHandThumb2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftHandThumb1/LeftHandThumb2/LeftHandThumb3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandIndex
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandIndex/LeftHandIndex1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandIndex/LeftHandIndex1/LeftHandIndex2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandIndex/LeftHandIndex1/LeftHandIndex2/LeftHandIndex3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandMiddle
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandMiddle/LeftHandMiddle1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandMiddle/LeftHandMiddle1/LeftHandMiddle2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandMiddle/LeftHandMiddle1/LeftHandMiddle2/LeftHandMiddle3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandPinky
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandPinky/LeftHandPinky1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandPinky/LeftHandPinky1/LeftHandPinky2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandPinky/LeftHandPinky1/LeftHandPinky2/LeftHandPinky3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandRing
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandRing/LeftHandRing1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandRing/LeftHandRing1/LeftHandRing2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/LeftShoulder/LeftShoulderExtra/LeftArm/LeftForeArm/LeftHand/LeftInHandRing/LeftHandRing1/LeftHandRing2/LeftHandRing3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/Neck
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/Neck/Head
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/Neck/Head/EyeLeft
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/Neck/Head/EyeRight
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightHandThumb1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightHandThumb1/RightHandThumb2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightHandThumb1/RightHandThumb2/RightHandThumb3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandIndex
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandIndex/RightHandIndex1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandIndex/RightHandIndex1/RightHandIndex2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandIndex/RightHandIndex1/RightHandIndex2/RightHandIndex3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandMiddle
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandMiddle/RightHandMiddle1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandMiddle/RightHandMiddle1/RightHandMiddle2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandMiddle/RightHandMiddle1/RightHandMiddle2/RightHandMiddle3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandPinky
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandPinky/RightHandPinky1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandPinky/RightHandPinky1/RightHandPinky2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandPinky/RightHandPinky1/RightHandPinky2/RightHandPinky3
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandRing
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandRing/RightHandRing1
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandRing/RightHandRing1/RightHandRing2
    m_Weight: 1
  - m_Path: python/Hips/Spine/Spine1/Spine2/Spine3/RightShoulder/RightShoulderExtra/RightArm/RightForeArm/RightHand/RightInHandRing/RightHandRing1/RightHandRing2/RightHandRing3
    m_Weight: 1
