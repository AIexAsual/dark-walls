﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Drawing_Toon
struct  CameraFilterPack_Drawing_Toon_t109  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Drawing_Toon::SCShader
	Shader_t1 * ___SCShader_2;
	// UnityEngine.Material CameraFilterPack_Drawing_Toon::SCMaterial
	Material_t2 * ___SCMaterial_3;
	// System.Single CameraFilterPack_Drawing_Toon::TimeX
	float ___TimeX_4;
	// System.Single CameraFilterPack_Drawing_Toon::Threshold
	float ___Threshold_5;
	// System.Single CameraFilterPack_Drawing_Toon::DotSize
	float ___DotSize_6;
};
struct CameraFilterPack_Drawing_Toon_t109_StaticFields{
	// System.Single CameraFilterPack_Drawing_Toon::ChangeThreshold
	float ___ChangeThreshold_7;
	// System.Single CameraFilterPack_Drawing_Toon::ChangeDotSize
	float ___ChangeDotSize_8;
};
