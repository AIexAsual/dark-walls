﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Glitch3
struct CameraFilterPack_FX_Glitch3_t130;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Glitch3::.ctor()
extern "C" void CameraFilterPack_FX_Glitch3__ctor_m840 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Glitch3::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Glitch3_get_material_m841 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch3::Start()
extern "C" void CameraFilterPack_FX_Glitch3_Start_m842 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch3::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Glitch3_OnRenderImage_m843 (CameraFilterPack_FX_Glitch3_t130 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch3::OnValidate()
extern "C" void CameraFilterPack_FX_Glitch3_OnValidate_m844 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch3::Update()
extern "C" void CameraFilterPack_FX_Glitch3_Update_m845 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch3::OnDisable()
extern "C" void CameraFilterPack_FX_Glitch3_OnDisable_m846 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
