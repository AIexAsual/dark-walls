﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FtpRequestCreator
struct FtpRequestCreator_t1402;
// System.Net.WebRequest
struct WebRequest_t1364;
// System.Uri
struct Uri_t237;

// System.Void System.Net.FtpRequestCreator::.ctor()
extern "C" void FtpRequestCreator__ctor_m7665 (FtpRequestCreator_t1402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FtpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1364 * FtpRequestCreator_Create_m7666 (FtpRequestCreator_t1402 * __this, Uri_t237 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
