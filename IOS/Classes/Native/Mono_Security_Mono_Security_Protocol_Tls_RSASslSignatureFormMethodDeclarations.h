﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RSASslSignatureFormatter
struct RSASslSignatureFormatter_t1290;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1322;
// System.Byte[]
struct ByteU5BU5D_t469;
// System.String
struct String_t;

// System.Void Mono.Security.Protocol.Tls.RSASslSignatureFormatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSASslSignatureFormatter__ctor_m7167 (RSASslSignatureFormatter_t1290 * __this, AsymmetricAlgorithm_t1322 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RSASslSignatureFormatter::CreateSignature(System.Byte[])
extern "C" ByteU5BU5D_t469* RSASslSignatureFormatter_CreateSignature_m7168 (RSASslSignatureFormatter_t1290 * __this, ByteU5BU5D_t469* ___rgbHash, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RSASslSignatureFormatter::SetHashAlgorithm(System.String)
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m7169 (RSASslSignatureFormatter_t1290 * __this, String_t* ___strName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RSASslSignatureFormatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSASslSignatureFormatter_SetKey_m7170 (RSASslSignatureFormatter_t1290 * __this, AsymmetricAlgorithm_t1322 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
