﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RadialUndistortionEffect
struct  RadialUndistortionEffect_t263  : public MonoBehaviour_t4
{
	// UnityEngine.Material RadialUndistortionEffect::material
	Material_t2 * ___material_2;
};
