﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1376;
// System.String
struct String_t;

// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m7563 (MonoTODOAttribute_t1376 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MonoTODOAttribute::.ctor(System.String)
extern "C" void MonoTODOAttribute__ctor_m7564 (MonoTODOAttribute_t1376 * __this, String_t* ___comment, const MethodInfo* method) IL2CPP_METHOD_ATTR;
