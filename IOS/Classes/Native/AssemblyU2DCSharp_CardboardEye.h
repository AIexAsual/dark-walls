﻿#pragma once
#include <stdint.h>
// StereoController
struct StereoController_t235;
// StereoRenderEffect
struct StereoRenderEffect_t239;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Cardboard/Eye
#include "AssemblyU2DCSharp_Cardboard_Eye.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CardboardEye
struct  CardboardEye_t240  : public MonoBehaviour_t4
{
	// Cardboard/Eye CardboardEye::eye
	int32_t ___eye_2;
	// UnityEngine.LayerMask CardboardEye::toggleCullingMask
	LayerMask_t241  ___toggleCullingMask_3;
	// StereoController CardboardEye::controller
	StereoController_t235 * ___controller_4;
	// StereoRenderEffect CardboardEye::stereoEffect
	StereoRenderEffect_t239 * ___stereoEffect_5;
	// UnityEngine.Camera CardboardEye::monoCamera
	Camera_t14 * ___monoCamera_6;
	// UnityEngine.Matrix4x4 CardboardEye::realProj
	Matrix4x4_t242  ___realProj_7;
	// UnityEngine.Vector4 CardboardEye::projvec
	Vector4_t5  ___projvec_8;
	// UnityEngine.Vector4 CardboardEye::unprojvec
	Vector4_t5  ___unprojvec_9;
	// System.Single CardboardEye::interpPosition
	float ___interpPosition_10;
	// UnityEngine.Camera CardboardEye::<camera>k__BackingField
	Camera_t14 * ___U3CcameraU3Ek__BackingField_11;
};
