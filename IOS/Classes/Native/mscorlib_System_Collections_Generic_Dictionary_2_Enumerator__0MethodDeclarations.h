﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Enumerator_t785;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t257;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct Dictionary_2_t619;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9MethodDeclarations.h"
#define Enumerator__ctor_m17646(__this, ___dictionary, method) (( void (*) (Enumerator_t785 *, Dictionary_2_t619 *, const MethodInfo*))Enumerator__ctor_m17559_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17647(__this, method) (( Object_t * (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17560_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17648(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17649(__this, method) (( Object_t * (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17562_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17650(__this, method) (( Object_t * (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17563_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m4624(__this, method) (( bool (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_MoveNext_m17564_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m4621(__this, method) (( KeyValuePair_2_t784  (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_get_Current_m17565_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17651(__this, method) (( int32_t (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_get_CurrentKey_m17566_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17652(__this, method) (( PointerEventData_t257 * (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_get_CurrentValue_m17567_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m17653(__this, method) (( void (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_VerifyState_m17568_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17654(__this, method) (( void (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_VerifyCurrent_m17569_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m17655(__this, method) (( void (*) (Enumerator_t785 *, const MethodInfo*))Enumerator_Dispose_m17570_gshared)(__this, method)
