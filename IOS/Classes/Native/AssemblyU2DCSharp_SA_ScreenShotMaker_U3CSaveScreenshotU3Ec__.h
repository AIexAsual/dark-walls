﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture2D
struct Texture2D_t9;
// System.Object
struct Object_t;
// SA_ScreenShotMaker
struct SA_ScreenShotMaker_t300;
// System.Object
#include "mscorlib_System_Object.h"
// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3
struct  U3CSaveScreenshotU3Ec__Iterator3_t301  : public Object_t
{
	// System.Int32 SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::<height>__1
	int32_t ___U3CheightU3E__1_1;
	// UnityEngine.Texture2D SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::<tex>__2
	Texture2D_t9 * ___U3CtexU3E__2_2;
	// System.Int32 SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::$PC
	int32_t ___U24PC_3;
	// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::$current
	Object_t * ___U24current_4;
	// SA_ScreenShotMaker SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::<>f__this
	SA_ScreenShotMaker_t300 * ___U3CU3Ef__this_5;
};
