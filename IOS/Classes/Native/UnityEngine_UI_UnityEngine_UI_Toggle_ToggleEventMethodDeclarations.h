﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t718;

// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
extern "C" void ToggleEvent__ctor_m4305 (ToggleEvent_t718 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
