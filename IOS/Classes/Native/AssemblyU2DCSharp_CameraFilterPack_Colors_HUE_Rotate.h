﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Colors_HUE_Rotate
struct  CameraFilterPack_Colors_HUE_Rotate_t77  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Colors_HUE_Rotate::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_HUE_Rotate::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Colors_HUE_Rotate::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Colors_HUE_Rotate::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Colors_HUE_Rotate::Speed
	float ___Speed_6;
};
struct CameraFilterPack_Colors_HUE_Rotate_t77_StaticFields{
	// System.Single CameraFilterPack_Colors_HUE_Rotate::ChangeSpeed
	float ___ChangeSpeed_7;
};
