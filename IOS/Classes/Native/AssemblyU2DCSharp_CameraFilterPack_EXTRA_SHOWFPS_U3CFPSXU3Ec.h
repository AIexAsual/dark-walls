﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// CameraFilterPack_EXTRA_SHOWFPS
struct CameraFilterPack_EXTRA_SHOWFPS_t110;
// System.Object
#include "mscorlib_System_Object.h"
// CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0
struct  U3CFPSXU3Ec__Iterator0_t111  : public Object_t
{
	// System.Single CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::<fps>__0
	float ___U3CfpsU3E__0_0;
	// System.Int32 CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::$PC
	int32_t ___U24PC_1;
	// System.Object CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::$current
	Object_t * ___U24current_2;
	// CameraFilterPack_EXTRA_SHOWFPS CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::<>f__this
	CameraFilterPack_EXTRA_SHOWFPS_t110 * ___U3CU3Ef__this_3;
};
