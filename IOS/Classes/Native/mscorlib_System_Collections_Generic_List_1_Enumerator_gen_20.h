﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t585;
// UnityEngine.AudioSource
struct AudioSource_t339;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
struct  Enumerator_t2477 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::l
	List_1_t585 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::current
	AudioSource_t339 * ___current_3;
};
