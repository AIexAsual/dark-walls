﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// MNPopup
#include "AssemblyU2DCSharp_MNPopup.h"
// MNAndroidRateUsPopUp
struct  MNAndroidRateUsPopUp_t287  : public MNPopup_t278
{
	// System.String MNAndroidRateUsPopUp::yes
	String_t* ___yes_6;
	// System.String MNAndroidRateUsPopUp::later
	String_t* ___later_7;
	// System.String MNAndroidRateUsPopUp::no
	String_t* ___no_8;
	// System.String MNAndroidRateUsPopUp::url
	String_t* ___url_9;
};
