﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_RGB
struct CameraFilterPack_Color_RGB_t66;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_RGB::.ctor()
extern "C" void CameraFilterPack_Color_RGB__ctor_m413 (CameraFilterPack_Color_RGB_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_RGB::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_RGB_get_material_m414 (CameraFilterPack_Color_RGB_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::Start()
extern "C" void CameraFilterPack_Color_RGB_Start_m415 (CameraFilterPack_Color_RGB_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_RGB_OnRenderImage_m416 (CameraFilterPack_Color_RGB_t66 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::OnValidate()
extern "C" void CameraFilterPack_Color_RGB_OnValidate_m417 (CameraFilterPack_Color_RGB_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::Update()
extern "C" void CameraFilterPack_Color_RGB_Update_m418 (CameraFilterPack_Color_RGB_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_RGB::OnDisable()
extern "C" void CameraFilterPack_Color_RGB_OnDisable_m419 (CameraFilterPack_Color_RGB_t66 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
