﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_TV_Old
struct  CameraFilterPack_TV_Old_t187  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_Old::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_Old::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_Old::Distortion
	float ___Distortion_4;
	// UnityEngine.Material CameraFilterPack_TV_Old::SCMaterial
	Material_t2 * ___SCMaterial_5;
};
struct CameraFilterPack_TV_Old_t187_StaticFields{
	// System.Single CameraFilterPack_TV_Old::ChangeDistortion
	float ___ChangeDistortion_6;
};
