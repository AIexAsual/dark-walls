﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_t628;

// System.Void UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback::.ctor()
extern "C" void ColorTweenCallback__ctor_m3645 (ColorTweenCallback_t628 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
