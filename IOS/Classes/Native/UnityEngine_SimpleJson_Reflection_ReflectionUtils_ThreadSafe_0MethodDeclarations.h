﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionary_2_t1138;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t3003;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t3210;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t995;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionaryValueFactory_2_t1137;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t3211;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t3212;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"
#define ThreadSafeDictionary_2__ctor_m6448(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t1138 *, ThreadSafeDictionaryValueFactory_2_t1137 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m21058_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m21059(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t1138 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m21060_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m21061(__this, ___key, method) (( ConstructorDelegate_t995 * (*) (ThreadSafeDictionary_2_t1138 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m21062_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m21063(__this, ___key, method) (( ConstructorDelegate_t995 * (*) (ThreadSafeDictionary_2_t1138 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m21064_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m21065(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1138 *, Type_t *, ConstructorDelegate_t995 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m21066_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m21067(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1138 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m21068_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m21069(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t1138 *, Type_t *, ConstructorDelegate_t995 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m21070_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m21071(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1138 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m21072_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m21073(__this, ___key, method) (( ConstructorDelegate_t995 * (*) (ThreadSafeDictionary_2_t1138 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m21074_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m21075(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1138 *, Type_t *, ConstructorDelegate_t995 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m21076_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m21077(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t1138 *, KeyValuePair_2_t2770 , const MethodInfo*))ThreadSafeDictionary_2_Add_m21078_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m21079(__this, method) (( void (*) (ThreadSafeDictionary_2_t1138 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m21080_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m21081(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1138 *, KeyValuePair_2_t2770 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m21082_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m21083(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t1138 *, KeyValuePair_2U5BU5D_t3211*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m21084_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m21085(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t1138 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m21086_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m21087(__this, method) (( bool (*) (ThreadSafeDictionary_2_t1138 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m21088_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m21089(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1138 *, KeyValuePair_2_t2770 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m21090_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m21091(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1138 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m21092_gshared)(__this, method)
