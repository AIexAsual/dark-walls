﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Drunk
struct CameraFilterPack_FX_Drunk_t124;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Drunk::.ctor()
extern "C" void CameraFilterPack_FX_Drunk__ctor_m797 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraFilterPack_FX_Drunk::get_IsDizzy()
extern "C" bool CameraFilterPack_FX_Drunk_get_IsDizzy_m798 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::set_IsDizzy(System.Boolean)
extern "C" void CameraFilterPack_FX_Drunk_set_IsDizzy_m799 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Drunk::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Drunk_get_material_m800 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::Start()
extern "C" void CameraFilterPack_FX_Drunk_Start_m801 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Drunk_OnRenderImage_m802 (CameraFilterPack_FX_Drunk_t124 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::OnValidate()
extern "C" void CameraFilterPack_FX_Drunk_OnValidate_m803 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::Update()
extern "C" void CameraFilterPack_FX_Drunk_Update_m804 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk::OnDisable()
extern "C" void CameraFilterPack_FX_Drunk_OnDisable_m805 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
