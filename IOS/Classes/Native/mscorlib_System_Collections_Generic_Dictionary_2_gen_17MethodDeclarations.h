﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
struct Dictionary_2_t2327;
// System.Collections.Generic.ICollection`1<iTweenEvent/TweenType>
struct ICollection_1_t2991;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1094;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>
struct KeyCollection_t2331;
// System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Object>
struct ValueCollection_t2335;
// System.Collections.Generic.IEqualityComparer`1<iTweenEvent/TweenType>
struct IEqualityComparer_1_t2318;
// System.Collections.Generic.IDictionary`2<iTweenEvent/TweenType,System.Object>
struct IDictionary_2_t2999;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>[]
struct KeyValuePair_2U5BU5D_t3000;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>>
struct IEnumerator_1_t3001;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m14631_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m14631(__this, method) (( void (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2__ctor_m14631_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m14633_gshared (Dictionary_2_t2327 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m14633(__this, ___comparer, method) (( void (*) (Dictionary_2_t2327 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14633_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m14635_gshared (Dictionary_2_t2327 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m14635(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2327 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14635_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m14637_gshared (Dictionary_2_t2327 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m14637(__this, ___capacity, method) (( void (*) (Dictionary_2_t2327 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14637_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m14639_gshared (Dictionary_2_t2327 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m14639(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2327 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14639_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m14641_gshared (Dictionary_2_t2327 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m14641(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2327 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m14641_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14643_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14643(__this, method) (( Object_t* (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14643_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14645_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14645(__this, method) (( Object_t* (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14645_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m14647_gshared (Dictionary_2_t2327 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m14647(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2327 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14647_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m14649_gshared (Dictionary_2_t2327 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m14649(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2327 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14649_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m14651_gshared (Dictionary_2_t2327 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m14651(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2327 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14651_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m14653_gshared (Dictionary_2_t2327 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m14653(__this, ___key, method) (( bool (*) (Dictionary_2_t2327 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14653_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m14655_gshared (Dictionary_2_t2327 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m14655(__this, ___key, method) (( void (*) (Dictionary_2_t2327 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14655_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14657_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14657(__this, method) (( bool (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14657_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14659_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14659(__this, method) (( Object_t * (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14659_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14661_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14661(__this, method) (( bool (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14661_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14663_gshared (Dictionary_2_t2327 * __this, KeyValuePair_2_t2328  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14663(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2327 *, KeyValuePair_2_t2328 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14663_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14665_gshared (Dictionary_2_t2327 * __this, KeyValuePair_2_t2328  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14665(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2327 *, KeyValuePair_2_t2328 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14665_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14667_gshared (Dictionary_2_t2327 * __this, KeyValuePair_2U5BU5D_t3000* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14667(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2327 *, KeyValuePair_2U5BU5D_t3000*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14667_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14669_gshared (Dictionary_2_t2327 * __this, KeyValuePair_2_t2328  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14669(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2327 *, KeyValuePair_2_t2328 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14669_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m14671_gshared (Dictionary_2_t2327 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m14671(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2327 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14671_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14673_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14673(__this, method) (( Object_t * (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14673_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14675_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14675(__this, method) (( Object_t* (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14675_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14677_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14677(__this, method) (( Object_t * (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14677_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m14679_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m14679(__this, method) (( int32_t (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_get_Count_m14679_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m14681_gshared (Dictionary_2_t2327 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m14681(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2327 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m14681_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m14683_gshared (Dictionary_2_t2327 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m14683(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2327 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m14683_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m14685_gshared (Dictionary_2_t2327 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m14685(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2327 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14685_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m14687_gshared (Dictionary_2_t2327 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m14687(__this, ___size, method) (( void (*) (Dictionary_2_t2327 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14687_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m14689_gshared (Dictionary_2_t2327 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m14689(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2327 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14689_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2328  Dictionary_2_make_pair_m14691_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m14691(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2328  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m14691_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m14693_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m14693(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m14693_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m14695_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m14695(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m14695_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m14697_gshared (Dictionary_2_t2327 * __this, KeyValuePair_2U5BU5D_t3000* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m14697(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2327 *, KeyValuePair_2U5BU5D_t3000*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14697_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m14699_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m14699(__this, method) (( void (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_Resize_m14699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m14701_gshared (Dictionary_2_t2327 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m14701(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2327 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m14701_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m14703_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m14703(__this, method) (( void (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_Clear_m14703_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m14705_gshared (Dictionary_2_t2327 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m14705(__this, ___key, method) (( bool (*) (Dictionary_2_t2327 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m14705_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m14707_gshared (Dictionary_2_t2327 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m14707(__this, ___value, method) (( bool (*) (Dictionary_2_t2327 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m14707_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m14709_gshared (Dictionary_2_t2327 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m14709(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2327 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m14709_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m14711_gshared (Dictionary_2_t2327 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m14711(__this, ___sender, method) (( void (*) (Dictionary_2_t2327 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14711_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m14713_gshared (Dictionary_2_t2327 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m14713(__this, ___key, method) (( bool (*) (Dictionary_2_t2327 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m14713_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m14715_gshared (Dictionary_2_t2327 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m14715(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2327 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m14715_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::get_Keys()
extern "C" KeyCollection_t2331 * Dictionary_2_get_Keys_m14717_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m14717(__this, method) (( KeyCollection_t2331 * (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_get_Keys_m14717_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::get_Values()
extern "C" ValueCollection_t2335 * Dictionary_2_get_Values_m14719_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m14719(__this, method) (( ValueCollection_t2335 * (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_get_Values_m14719_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m14721_gshared (Dictionary_2_t2327 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m14721(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2327 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14721_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m14723_gshared (Dictionary_2_t2327 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m14723(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2327 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14723_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m14725_gshared (Dictionary_2_t2327 * __this, KeyValuePair_2_t2328  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m14725(__this, ___pair, method) (( bool (*) (Dictionary_2_t2327 *, KeyValuePair_2_t2328 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14725_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::GetEnumerator()
extern "C" Enumerator_t2333  Dictionary_2_GetEnumerator_m14727_gshared (Dictionary_2_t2327 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m14727(__this, method) (( Enumerator_t2333  (*) (Dictionary_2_t2327 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14727_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t552  Dictionary_2_U3CCopyToU3Em__0_m14729_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m14729(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14729_gshared)(__this /* static, unused */, ___key, ___value, method)
