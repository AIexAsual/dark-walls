﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RenderSettings
struct RenderSettings_t851;

// System.Void UnityEngine.RenderSettings::set_fogEndDistance(System.Single)
extern "C" void RenderSettings_set_fogEndDistance_m3157 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
