﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t577;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Boolean>
struct  Enumerator_t2411 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Boolean>::l
	List_1_t577 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Boolean>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Boolean>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Boolean>::current
	bool ___current_3;
};
