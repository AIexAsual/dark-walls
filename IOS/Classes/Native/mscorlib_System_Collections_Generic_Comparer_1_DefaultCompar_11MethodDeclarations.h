﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t2692;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m20179_gshared (DefaultComparer_t2692 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m20179(__this, method) (( void (*) (DefaultComparer_t2692 *, const MethodInfo*))DefaultComparer__ctor_m20179_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m20180_gshared (DefaultComparer_t2692 * __this, UICharInfo_t811  ___x, UICharInfo_t811  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m20180(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2692 *, UICharInfo_t811 , UICharInfo_t811 , const MethodInfo*))DefaultComparer_Compare_m20180_gshared)(__this, ___x, ___y, method)
