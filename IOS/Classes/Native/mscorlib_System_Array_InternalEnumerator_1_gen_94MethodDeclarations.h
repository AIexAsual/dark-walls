﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>
struct InternalEnumerator_1_t2925;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22764_gshared (InternalEnumerator_1_t2925 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22764(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2925 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22764_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22765_gshared (InternalEnumerator_1_t2925 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22765(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2925 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22765_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22766_gshared (InternalEnumerator_1_t2925 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22766(__this, method) (( void (*) (InternalEnumerator_1_t2925 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22766_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22767_gshared (InternalEnumerator_1_t2925 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22767(__this, method) (( bool (*) (InternalEnumerator_1_t2925 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22767_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern "C" Slot_t1657  InternalEnumerator_1_get_Current_m22768_gshared (InternalEnumerator_1_t2925 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22768(__this, method) (( Slot_t1657  (*) (InternalEnumerator_1_t2925 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22768_gshared)(__this, method)
