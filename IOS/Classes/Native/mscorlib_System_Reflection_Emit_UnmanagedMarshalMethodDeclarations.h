﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1738;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1556;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
extern "C" MarshalAsAttribute_t1556 * UnmanagedMarshal_ToMarshalAsAttribute_m10943 (UnmanagedMarshal_t1738 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
