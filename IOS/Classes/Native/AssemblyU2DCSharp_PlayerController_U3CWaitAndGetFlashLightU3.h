﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// PlayerController
struct PlayerController_t356;
// System.Object
#include "mscorlib_System_Object.h"
// PlayerController/<WaitAndGetFlashLight>c__Iterator13
struct  U3CWaitAndGetFlashLightU3Ec__Iterator13_t393  : public Object_t
{
	// System.Single PlayerController/<WaitAndGetFlashLight>c__Iterator13::time
	float ___time_0;
	// System.Single PlayerController/<WaitAndGetFlashLight>c__Iterator13::val
	float ___val_1;
	// System.Int32 PlayerController/<WaitAndGetFlashLight>c__Iterator13::$PC
	int32_t ___U24PC_2;
	// System.Object PlayerController/<WaitAndGetFlashLight>c__Iterator13::$current
	Object_t * ___U24current_3;
	// System.Single PlayerController/<WaitAndGetFlashLight>c__Iterator13::<$>time
	float ___U3CU24U3Etime_4;
	// System.Single PlayerController/<WaitAndGetFlashLight>c__Iterator13::<$>val
	float ___U3CU24U3Eval_5;
	// PlayerController PlayerController/<WaitAndGetFlashLight>c__Iterator13::<>f__this
	PlayerController_t356 * ___U3CU3Ef__this_6;
};
