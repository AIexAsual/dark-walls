﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// CameraFilterPack_Color_RGB
struct  CameraFilterPack_Color_RGB_t66  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Color_RGB::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Color_RGB::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Color_RGB::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Color_RGB::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// UnityEngine.Color CameraFilterPack_Color_RGB::ColorRGB
	Color_t6  ___ColorRGB_6;
};
struct CameraFilterPack_Color_RGB_t66_StaticFields{
	// UnityEngine.Color CameraFilterPack_Color_RGB::ChangeColorRGB
	Color_t6  ___ChangeColorRGB_7;
};
