﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<iTween/LoopType>
struct Collection_1_t2460;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// iTween/LoopType[]
struct LoopTypeU5BU5D_t452;
// System.Collections.Generic.IEnumerator`1<iTween/LoopType>
struct IEnumerator_1_t3043;
// System.Collections.Generic.IList`1<iTween/LoopType>
struct IList_1_t570;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::.ctor()
extern "C" void Collection_1__ctor_m16584_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16584(__this, method) (( void (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1__ctor_m16584_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16585_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16585(__this, method) (( bool (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16585_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16586_gshared (Collection_1_t2460 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16586(__this, ___array, ___index, method) (( void (*) (Collection_1_t2460 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16586_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16587_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16587(__this, method) (( Object_t * (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16587_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16588_gshared (Collection_1_t2460 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16588(__this, ___value, method) (( int32_t (*) (Collection_1_t2460 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16588_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16589_gshared (Collection_1_t2460 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16589(__this, ___value, method) (( bool (*) (Collection_1_t2460 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16589_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16590_gshared (Collection_1_t2460 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16590(__this, ___value, method) (( int32_t (*) (Collection_1_t2460 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16590_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16591_gshared (Collection_1_t2460 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16591(__this, ___index, ___value, method) (( void (*) (Collection_1_t2460 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16591_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16592_gshared (Collection_1_t2460 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16592(__this, ___value, method) (( void (*) (Collection_1_t2460 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16592_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16593_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16593(__this, method) (( bool (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16593_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16594_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16594(__this, method) (( Object_t * (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16594_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16595_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16595(__this, method) (( bool (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16595_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16596_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16596(__this, method) (( bool (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16596_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16597_gshared (Collection_1_t2460 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16597(__this, ___index, method) (( Object_t * (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16597_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16598_gshared (Collection_1_t2460 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16598(__this, ___index, ___value, method) (( void (*) (Collection_1_t2460 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16598_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::Add(T)
extern "C" void Collection_1_Add_m16599_gshared (Collection_1_t2460 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m16599(__this, ___item, method) (( void (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_Add_m16599_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::Clear()
extern "C" void Collection_1_Clear_m16600_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16600(__this, method) (( void (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_Clear_m16600_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::ClearItems()
extern "C" void Collection_1_ClearItems_m16601_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16601(__this, method) (( void (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_ClearItems_m16601_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::Contains(T)
extern "C" bool Collection_1_Contains_m16602_gshared (Collection_1_t2460 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m16602(__this, ___item, method) (( bool (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_Contains_m16602_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16603_gshared (Collection_1_t2460 * __this, LoopTypeU5BU5D_t452* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16603(__this, ___array, ___index, method) (( void (*) (Collection_1_t2460 *, LoopTypeU5BU5D_t452*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16603_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<iTween/LoopType>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16604_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16604(__this, method) (( Object_t* (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_GetEnumerator_m16604_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/LoopType>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16605_gshared (Collection_1_t2460 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16605(__this, ___item, method) (( int32_t (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m16605_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16606_gshared (Collection_1_t2460 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m16606(__this, ___index, ___item, method) (( void (*) (Collection_1_t2460 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m16606_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16607_gshared (Collection_1_t2460 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16607(__this, ___index, ___item, method) (( void (*) (Collection_1_t2460 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m16607_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::Remove(T)
extern "C" bool Collection_1_Remove_m16608_gshared (Collection_1_t2460 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m16608(__this, ___item, method) (( bool (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_Remove_m16608_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16609_gshared (Collection_1_t2460 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16609(__this, ___index, method) (( void (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16609_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16610_gshared (Collection_1_t2460 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16610(__this, ___index, method) (( void (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16610_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/LoopType>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16611_gshared (Collection_1_t2460 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16611(__this, method) (( int32_t (*) (Collection_1_t2460 *, const MethodInfo*))Collection_1_get_Count_m16611_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<iTween/LoopType>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m16612_gshared (Collection_1_t2460 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16612(__this, ___index, method) (( int32_t (*) (Collection_1_t2460 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16612_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16613_gshared (Collection_1_t2460 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16613(__this, ___index, ___value, method) (( void (*) (Collection_1_t2460 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m16613_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16614_gshared (Collection_1_t2460 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16614(__this, ___index, ___item, method) (( void (*) (Collection_1_t2460 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m16614_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16615_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16615(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16615_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<iTween/LoopType>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m16616_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16616(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16616_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/LoopType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16617_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16617(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16617_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16618_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16618(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16618_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/LoopType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16619_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16619(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16619_gshared)(__this /* static, unused */, ___list, method)
