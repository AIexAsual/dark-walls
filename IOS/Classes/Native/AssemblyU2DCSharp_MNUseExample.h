﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// MNFeaturePreview
#include "AssemblyU2DCSharp_MNFeaturePreview.h"
// MNUseExample
struct  MNUseExample_t275  : public MNFeaturePreview_t274
{
	// System.String MNUseExample::appleId
	String_t* ___appleId_12;
	// System.String MNUseExample::apdroidAppUrl
	String_t* ___apdroidAppUrl_13;
};
