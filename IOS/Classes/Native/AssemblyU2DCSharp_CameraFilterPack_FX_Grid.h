﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_FX_Grid
struct  CameraFilterPack_FX_Grid_t131  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_Grid::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Grid::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_FX_Grid::SCMaterial
	Material_t2 * ___SCMaterial_4;
	// System.Single CameraFilterPack_FX_Grid::Distortion
	float ___Distortion_5;
};
struct CameraFilterPack_FX_Grid_t131_StaticFields{
	// System.Single CameraFilterPack_FX_Grid::ChangeDistortion
	float ___ChangeDistortion_6;
};
