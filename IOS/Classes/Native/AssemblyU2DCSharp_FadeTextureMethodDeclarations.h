﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// FadeTexture
struct FadeTexture_t313;

// System.Void FadeTexture::.ctor()
extern "C" void FadeTexture__ctor_m1881 (FadeTexture_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeTexture::Start()
extern "C" void FadeTexture_Start_m1882 (FadeTexture_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeTexture::FadeMat()
extern "C" void FadeTexture_FadeMat_m1883 (FadeTexture_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeTexture::FadeText(System.Single)
extern "C" void FadeTexture_FadeText_m1884 (FadeTexture_t313 * __this, float ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeTexture::Update()
extern "C" void FadeTexture_Update_m1885 (FadeTexture_t313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
