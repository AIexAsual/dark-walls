﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collision
struct Collision_t471;
// UnityEngine.Rigidbody
struct Rigidbody_t325;
// UnityEngine.Collider
struct Collider_t319;
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern "C" Vector3_t215  Collision_get_relativeVelocity_m3227 (Collision_t471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C" Rigidbody_t325 * Collision_get_rigidbody_m5757 (Collision_t471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.Collision::get_collider()
extern "C" Collider_t319 * Collision_get_collider_m3155 (Collision_t471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Collision::get_transform()
extern "C" Transform_t243 * Collision_get_transform_m3226 (Collision_t471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern "C" GameObject_t256 * Collision_get_gameObject_m3228 (Collision_t471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
