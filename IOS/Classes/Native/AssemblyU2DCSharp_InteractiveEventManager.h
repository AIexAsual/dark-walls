﻿#pragma once
#include <stdint.h>
// InteractiveEventManager
struct InteractiveEventManager_t362;
// InteractiveEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t361;
// System.Object
#include "mscorlib_System_Object.h"
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"
// InteractiveEventManager
struct  InteractiveEventManager_t362  : public Object_t
{
	// InteractiveEvent InteractiveEventManager::interactiveState
	int32_t ___interactiveState_1;
	// InteractiveEventManager/OnStateChangeHandler InteractiveEventManager::OnStateChangeEvent
	OnStateChangeHandler_t361 * ___OnStateChangeEvent_2;
};
struct InteractiveEventManager_t362_StaticFields{
	// InteractiveEventManager InteractiveEventManager::_instance
	InteractiveEventManager_t362 * ____instance_0;
};
