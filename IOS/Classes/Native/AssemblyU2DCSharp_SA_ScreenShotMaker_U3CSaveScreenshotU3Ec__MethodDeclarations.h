﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3
struct U3CSaveScreenshotU3Ec__Iterator3_t301;
// System.Object
struct Object_t;

// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::.ctor()
extern "C" void U3CSaveScreenshotU3Ec__Iterator3__ctor_m1835 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1836 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1837 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::MoveNext()
extern "C" bool U3CSaveScreenshotU3Ec__Iterator3_MoveNext_m1838 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::Dispose()
extern "C" void U3CSaveScreenshotU3Ec__Iterator3_Dispose_m1839 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::Reset()
extern "C" void U3CSaveScreenshotU3Ec__Iterator3_Reset_m1840 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
