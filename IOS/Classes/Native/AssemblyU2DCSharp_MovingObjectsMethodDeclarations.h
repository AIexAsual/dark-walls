﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MovingObjects
struct MovingObjects_t375;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void MovingObjects::.ctor()
extern "C" void MovingObjects__ctor_m2222 (MovingObjects_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects::Start()
extern "C" void MovingObjects_Start_m2223 (MovingObjects_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects::Update()
extern "C" void MovingObjects_Update_m2224 (MovingObjects_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects::MoveTo(UnityEngine.Vector3,System.Single,System.Single,iTween/EaseType,iTween/LoopType)
extern "C" void MovingObjects_MoveTo_m2225 (MovingObjects_t375 * __this, Vector3_t215  ___val, float ___t, float ___d, int32_t ___iE, int32_t ___iL, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects::spawnHere(UnityEngine.Vector3,System.Single)
extern "C" void MovingObjects_spawnHere_m2226 (MovingObjects_t375 * __this, Vector3_t215  ___val, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MovingObjects::WaitAndSpawnHere(System.Single,UnityEngine.Vector3)
extern "C" Object_t * MovingObjects_WaitAndSpawnHere_m2227 (MovingObjects_t375 * __this, float ___delay, Vector3_t215  ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects::ShakeCabinet()
extern "C" void MovingObjects_ShakeCabinet_m2228 (MovingObjects_t375 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects::MoveWheelChair(System.Boolean)
extern "C" void MovingObjects_MoveWheelChair_m2229 (MovingObjects_t375 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
