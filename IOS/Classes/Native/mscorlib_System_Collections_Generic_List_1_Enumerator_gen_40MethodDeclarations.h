﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>
struct Enumerator_t2735;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.MatchDesc
struct MatchDesc_t974;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>
struct List_1_t975;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m20755(__this, ___l, method) (( void (*) (Enumerator_t2735 *, List_1_t975 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20756(__this, method) (( Object_t * (*) (Enumerator_t2735 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>::Dispose()
#define Enumerator_Dispose_m20757(__this, method) (( void (*) (Enumerator_t2735 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>::VerifyState()
#define Enumerator_VerifyState_m20758(__this, method) (( void (*) (Enumerator_t2735 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>::MoveNext()
#define Enumerator_MoveNext_m20759(__this, method) (( bool (*) (Enumerator_t2735 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>::get_Current()
#define Enumerator_get_Current_m20760(__this, method) (( MatchDesc_t974 * (*) (Enumerator_t2735 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
