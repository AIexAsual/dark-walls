﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CardboardHead
struct CardboardHead_t244;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"

// System.Void CardboardHead::.ctor()
extern "C" void CardboardHead__ctor_m1527 (CardboardHead_t244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray CardboardHead::get_Gaze()
extern "C" Ray_t465  CardboardHead_get_Gaze_m1528 (CardboardHead_t244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardHead::Update()
extern "C" void CardboardHead_Update_m1529 (CardboardHead_t244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardHead::LateUpdate()
extern "C" void CardboardHead_LateUpdate_m1530 (CardboardHead_t244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardHead::UpdateHead()
extern "C" void CardboardHead_UpdateHead_m1531 (CardboardHead_t244 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
