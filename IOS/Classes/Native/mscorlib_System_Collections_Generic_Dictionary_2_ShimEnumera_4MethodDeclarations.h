﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct ShimEnumerator_t2754;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2742;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m20946_gshared (ShimEnumerator_t2754 * __this, Dictionary_2_t2742 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m20946(__this, ___host, method) (( void (*) (ShimEnumerator_t2754 *, Dictionary_2_t2742 *, const MethodInfo*))ShimEnumerator__ctor_m20946_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m20947_gshared (ShimEnumerator_t2754 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m20947(__this, method) (( bool (*) (ShimEnumerator_t2754 *, const MethodInfo*))ShimEnumerator_MoveNext_m20947_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m20948_gshared (ShimEnumerator_t2754 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m20948(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2754 *, const MethodInfo*))ShimEnumerator_get_Entry_m20948_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m20949_gshared (ShimEnumerator_t2754 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m20949(__this, method) (( Object_t * (*) (ShimEnumerator_t2754 *, const MethodInfo*))ShimEnumerator_get_Key_m20949_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m20950_gshared (ShimEnumerator_t2754 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m20950(__this, method) (( Object_t * (*) (ShimEnumerator_t2754 *, const MethodInfo*))ShimEnumerator_get_Value_m20950_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m20951_gshared (ShimEnumerator_t2754 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m20951(__this, method) (( Object_t * (*) (ShimEnumerator_t2754 *, const MethodInfo*))ShimEnumerator_get_Current_m20951_gshared)(__this, method)
