﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<ArrayIndexes>
struct Enumerator_t2481;
// System.Object
struct Object_t;
// ArrayIndexes
struct ArrayIndexes_t441;
// System.Collections.Generic.List`1<ArrayIndexes>
struct List_1_t586;

// System.Void System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m17000(__this, ___l, method) (( void (*) (Enumerator_t2481 *, List_1_t586 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17001(__this, method) (( Object_t * (*) (Enumerator_t2481 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::Dispose()
#define Enumerator_Dispose_m17002(__this, method) (( void (*) (Enumerator_t2481 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::VerifyState()
#define Enumerator_VerifyState_m17003(__this, method) (( void (*) (Enumerator_t2481 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::MoveNext()
#define Enumerator_MoveNext_m17004(__this, method) (( bool (*) (Enumerator_t2481 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::get_Current()
#define Enumerator_get_Current_m17005(__this, method) (( ArrayIndexes_t441 * (*) (Enumerator_t2481 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
