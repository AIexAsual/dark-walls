﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t457;
// System.Collections.Generic.Dictionary`2<System.String,iTweenPath>
struct Dictionary_2_t458;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// iTweenPath
struct  iTweenPath_t459  : public MonoBehaviour_t4
{
	// System.String iTweenPath::pathName
	String_t* ___pathName_2;
	// UnityEngine.Color iTweenPath::pathColor
	Color_t6  ___pathColor_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> iTweenPath::nodes
	List_1_t457 * ___nodes_4;
	// System.Int32 iTweenPath::nodeCount
	int32_t ___nodeCount_5;
	// System.Boolean iTweenPath::initialized
	bool ___initialized_7;
	// System.String iTweenPath::initialName
	String_t* ___initialName_8;
};
struct iTweenPath_t459_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,iTweenPath> iTweenPath::paths
	Dictionary_2_t458 * ___paths_6;
};
