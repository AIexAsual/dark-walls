﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Vision_Tunnel
struct CameraFilterPack_Vision_Tunnel_t209;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Vision_Tunnel::.ctor()
extern "C" void CameraFilterPack_Vision_Tunnel__ctor_m1359 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Tunnel::get_material()
extern "C" Material_t2 * CameraFilterPack_Vision_Tunnel_get_material_m1360 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::Start()
extern "C" void CameraFilterPack_Vision_Tunnel_Start_m1361 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Vision_Tunnel_OnRenderImage_m1362 (CameraFilterPack_Vision_Tunnel_t209 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::OnValidate()
extern "C" void CameraFilterPack_Vision_Tunnel_OnValidate_m1363 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::Update()
extern "C" void CameraFilterPack_Vision_Tunnel_Update_m1364 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Tunnel::OnDisable()
extern "C" void CameraFilterPack_Vision_Tunnel_OnDisable_m1365 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
