﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_WideScreenHV
struct CameraFilterPack_TV_WideScreenHV_t201;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_WideScreenHV::.ctor()
extern "C" void CameraFilterPack_TV_WideScreenHV__ctor_m1303 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_WideScreenHV::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_WideScreenHV_get_material_m1304 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::Start()
extern "C" void CameraFilterPack_TV_WideScreenHV_Start_m1305 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_WideScreenHV_OnRenderImage_m1306 (CameraFilterPack_TV_WideScreenHV_t201 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::OnValidate()
extern "C" void CameraFilterPack_TV_WideScreenHV_OnValidate_m1307 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::Update()
extern "C" void CameraFilterPack_TV_WideScreenHV_Update_m1308 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenHV::OnDisable()
extern "C" void CameraFilterPack_TV_WideScreenHV_OnDisable_m1309 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
