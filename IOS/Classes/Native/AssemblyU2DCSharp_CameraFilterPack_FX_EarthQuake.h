﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_FX_EarthQuake
struct  CameraFilterPack_FX_EarthQuake_t126  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_EarthQuake::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_EarthQuake::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_EarthQuake::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_EarthQuake::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_FX_EarthQuake::Speed
	float ___Speed_6;
	// System.Single CameraFilterPack_FX_EarthQuake::X
	float ___X_7;
	// System.Single CameraFilterPack_FX_EarthQuake::Y
	float ___Y_8;
	// System.Single CameraFilterPack_FX_EarthQuake::Value4
	float ___Value4_9;
};
struct CameraFilterPack_FX_EarthQuake_t126_StaticFields{
	// System.Single CameraFilterPack_FX_EarthQuake::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_FX_EarthQuake::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_FX_EarthQuake::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_FX_EarthQuake::ChangeValue4
	float ___ChangeValue4_13;
};
