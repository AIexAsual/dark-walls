﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t2;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// StereoRenderEffect
struct  StereoRenderEffect_t239  : public MonoBehaviour_t4
{
	// UnityEngine.Material StereoRenderEffect::material
	Material_t2 * ___material_2;
	// UnityEngine.Camera StereoRenderEffect::camera
	Camera_t14 * ___camera_3;
};
