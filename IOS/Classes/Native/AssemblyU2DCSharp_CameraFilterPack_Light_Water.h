﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Light_Water
struct  CameraFilterPack_Light_Water_t157  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Light_Water::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Light_Water::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Light_Water::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Light_Water::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Light_Water::Size
	float ___Size_6;
	// System.Single CameraFilterPack_Light_Water::Alpha
	float ___Alpha_7;
	// System.Single CameraFilterPack_Light_Water::Distance
	float ___Distance_8;
	// System.Single CameraFilterPack_Light_Water::Speed
	float ___Speed_9;
};
struct CameraFilterPack_Light_Water_t157_StaticFields{
	// System.Single CameraFilterPack_Light_Water::ChangeAlpha
	float ___ChangeAlpha_10;
	// System.Single CameraFilterPack_Light_Water::ChangeDistance
	float ___ChangeDistance_11;
	// System.Single CameraFilterPack_Light_Water::ChangeSize
	float ___ChangeSize_12;
};
