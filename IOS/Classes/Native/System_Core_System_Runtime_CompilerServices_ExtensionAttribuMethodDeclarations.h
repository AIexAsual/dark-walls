﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t1172;

// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern "C" void ExtensionAttribute__ctor_m6502 (ExtensionAttribute_t1172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
