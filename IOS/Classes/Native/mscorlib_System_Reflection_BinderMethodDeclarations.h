﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Binder
struct Binder_t1160;
// System.Reflection.MethodBase
struct MethodBase_t1163;
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t2179;
// System.Object[]
struct ObjectU5BU5D_t470;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1161;
// System.Globalization.CultureInfo
struct CultureInfo_t1129;
// System.String[]
struct StringU5BU5D_t398;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t485;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1156;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1153;
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"

// System.Void System.Reflection.Binder::.ctor()
extern "C" void Binder__ctor_m11016 (Binder_t1160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Binder::.cctor()
extern "C" void Binder__cctor_m11017 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Binder System.Reflection.Binder::get_DefaultBinder()
extern "C" Binder_t1160 * Binder_get_DefaultBinder_m11018 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Binder::ConvertArgs(System.Reflection.Binder,System.Object[],System.Reflection.ParameterInfo[],System.Globalization.CultureInfo)
extern "C" bool Binder_ConvertArgs_m11019 (Object_t * __this /* static, unused */, Binder_t1160 * ___binder, ObjectU5BU5D_t470* ___args, ParameterInfoU5BU5D_t1153* ___pinfo, CultureInfo_t1129 * ___culture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Reflection.Binder::GetDerivedLevel(System.Type)
extern "C" int32_t Binder_GetDerivedLevel_m11020 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodBase System.Reflection.Binder::FindMostDerivedMatch(System.Reflection.MethodBase[])
extern "C" MethodBase_t1163 * Binder_FindMostDerivedMatch_m11021 (Object_t * __this /* static, unused */, MethodBaseU5BU5D_t2179* ___match, const MethodInfo* method) IL2CPP_METHOD_ATTR;
