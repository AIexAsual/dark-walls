﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// CameraFilterPack_Drawing_EnhancedComics
struct  CameraFilterPack_Drawing_EnhancedComics_t99  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Drawing_EnhancedComics::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_Drawing_EnhancedComics::SCMaterial
	Material_t2 * ___SCMaterial_4;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::DotSize
	float ___DotSize_5;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_ColorR
	float ____ColorR_6;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_ColorG
	float ____ColorG_7;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_ColorB
	float ____ColorB_8;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_Blood
	float ____Blood_9;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_SmoothStart
	float ____SmoothStart_10;
	// System.Single CameraFilterPack_Drawing_EnhancedComics::_SmoothEnd
	float ____SmoothEnd_11;
	// UnityEngine.Color CameraFilterPack_Drawing_EnhancedComics::ColorRGB
	Color_t6  ___ColorRGB_12;
};
struct CameraFilterPack_Drawing_EnhancedComics_t99_StaticFields{
	// System.Single CameraFilterPack_Drawing_EnhancedComics::ChangeDotSize
	float ___ChangeDotSize_13;
};
