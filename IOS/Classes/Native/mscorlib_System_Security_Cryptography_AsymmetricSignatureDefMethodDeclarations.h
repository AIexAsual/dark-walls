﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricSignatureDeformatter
struct AsymmetricSignatureDeformatter_t1289;
// System.String
struct String_t;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1322;
// System.Byte[]
struct ByteU5BU5D_t469;

// System.Void System.Security.Cryptography.AsymmetricSignatureDeformatter::.ctor()
extern "C" void AsymmetricSignatureDeformatter__ctor_m7540 (AsymmetricSignatureDeformatter_t1289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
