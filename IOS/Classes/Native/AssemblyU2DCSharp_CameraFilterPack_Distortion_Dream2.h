﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Dream2
struct  CameraFilterPack_Distortion_Dream2_t84  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Dream2::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Dream2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Dream2::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Dream2::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Dream2::Distortion
	float ___Distortion_6;
	// System.Single CameraFilterPack_Distortion_Dream2::Speed
	float ___Speed_7;
};
struct CameraFilterPack_Distortion_Dream2_t84_StaticFields{
	// System.Single CameraFilterPack_Distortion_Dream2::ChangeDistortion
	float ___ChangeDistortion_8;
	// System.Single CameraFilterPack_Distortion_Dream2::ChangeSpeed
	float ___ChangeSpeed_9;
};
