﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LightController/<FlickerMe>c__IteratorB
struct U3CFlickerMeU3Ec__IteratorB_t344;
// System.Object
struct Object_t;

// System.Void LightController/<FlickerMe>c__IteratorB::.ctor()
extern "C" void U3CFlickerMeU3Ec__IteratorB__ctor_m2002 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightController/<FlickerMe>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFlickerMeU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2003 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LightController/<FlickerMe>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFlickerMeU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m2004 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LightController/<FlickerMe>c__IteratorB::MoveNext()
extern "C" bool U3CFlickerMeU3Ec__IteratorB_MoveNext_m2005 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController/<FlickerMe>c__IteratorB::Dispose()
extern "C" void U3CFlickerMeU3Ec__IteratorB_Dispose_m2006 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController/<FlickerMe>c__IteratorB::Reset()
extern "C" void U3CFlickerMeU3Ec__IteratorB_Reset_m2007 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
