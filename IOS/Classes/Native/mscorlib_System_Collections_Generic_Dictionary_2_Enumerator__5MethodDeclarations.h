﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_t2354;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2349;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14807_gshared (Enumerator_t2354 * __this, Dictionary_2_t2349 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m14807(__this, ___dictionary, method) (( void (*) (Enumerator_t2354 *, Dictionary_2_t2349 *, const MethodInfo*))Enumerator__ctor_m14807_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14809_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14809(__this, method) (( Object_t * (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14811_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14811(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14811_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14813_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14813(__this, method) (( Object_t * (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14815_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14815(__this, method) (( Object_t * (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14815_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14817_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14817(__this, method) (( bool (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_MoveNext_m14817_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2350  Enumerator_get_Current_m14819_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14819(__this, method) (( KeyValuePair_2_t2350  (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_get_Current_m14819_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m14821_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m14821(__this, method) (( Object_t * (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_get_CurrentKey_m14821_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m14823_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m14823(__this, method) (( Object_t * (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_get_CurrentValue_m14823_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m14825_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14825(__this, method) (( void (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_VerifyState_m14825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m14827_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m14827(__this, method) (( void (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_VerifyCurrent_m14827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14829_gshared (Enumerator_t2354 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14829(__this, method) (( void (*) (Enumerator_t2354 *, const MethodInfo*))Enumerator_Dispose_m14829_gshared)(__this, method)
