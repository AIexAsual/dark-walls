﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Metadata.SoapParameterAttribute
struct SoapParameterAttribute_t1891;

// System.Void System.Runtime.Remoting.Metadata.SoapParameterAttribute::.ctor()
extern "C" void SoapParameterAttribute__ctor_m11497 (SoapParameterAttribute_t1891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
