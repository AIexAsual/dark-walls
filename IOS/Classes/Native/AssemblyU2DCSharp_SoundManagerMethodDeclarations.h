﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SoundManager
struct SoundManager_t384;
// System.String
struct String_t;

// System.Void SoundManager::.ctor()
extern "C" void SoundManager__ctor_m2199 (SoundManager_t384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::.cctor()
extern "C" void SoundManager__cctor_m2200 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundManager SoundManager::get_Instance()
extern "C" SoundManager_t384 * SoundManager_get_Instance_m2201 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SoundEffects(System.String,System.Boolean,System.Single)
extern "C" void SoundManager_SoundEffects_m2202 (SoundManager_t384 * __this, String_t* ___name, bool ___val, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundManager::SoundVol(System.String,System.Single)
extern "C" void SoundManager_SoundVol_m2203 (SoundManager_t384 * __this, String_t* ___name, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
