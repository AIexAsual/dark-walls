﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Light
struct Light_t318;
// UnityEngine.Texture
struct Texture_t221;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Color UnityEngine.Light::get_color()
extern "C" Color_t6  Light_get_color_m3304 (Light_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern "C" void Light_set_color_m3306 (Light_t318 * __this, Color_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_get_color_m5670 (Light_t318 * __this, Color_t6 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_set_color_m5671 (Light_t318 * __this, Color_t6 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Light::get_intensity()
extern "C" float Light_get_intensity_m3158 (Light_t318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C" void Light_set_intensity_m3124 (Light_t318 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_range(System.Single)
extern "C" void Light_set_range_m3141 (Light_t318 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_spotAngle(System.Single)
extern "C" void Light_set_spotAngle_m3199 (Light_t318 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Light::set_cookie(UnityEngine.Texture)
extern "C" void Light_set_cookie_m3198 (Light_t318 * __this, Texture_t221 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
