﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTimeOffset
struct  DateTimeOffset_t1148 
{
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t871  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t1437  ___utc_offset_3;
};
struct DateTimeOffset_t1148_StaticFields{
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t1148  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t1148  ___MinValue_1;
};
