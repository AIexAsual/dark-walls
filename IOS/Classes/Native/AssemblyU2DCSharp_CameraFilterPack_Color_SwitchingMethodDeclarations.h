﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_Switching
struct CameraFilterPack_Color_Switching_t68;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_Switching::.ctor()
extern "C" void CameraFilterPack_Color_Switching__ctor_m426 (CameraFilterPack_Color_Switching_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Switching::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_Switching_get_material_m427 (CameraFilterPack_Color_Switching_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::Start()
extern "C" void CameraFilterPack_Color_Switching_Start_m428 (CameraFilterPack_Color_Switching_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_Switching_OnRenderImage_m429 (CameraFilterPack_Color_Switching_t68 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::OnValidate()
extern "C" void CameraFilterPack_Color_Switching_OnValidate_m430 (CameraFilterPack_Color_Switching_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::Update()
extern "C" void CameraFilterPack_Color_Switching_Update_m431 (CameraFilterPack_Color_Switching_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Switching::OnDisable()
extern "C" void CameraFilterPack_Color_Switching_OnDisable_m432 (CameraFilterPack_Color_Switching_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
