﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Oculus_ThermaVision
struct CameraFilterPack_Oculus_ThermaVision_t166;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Oculus_ThermaVision::.ctor()
extern "C" void CameraFilterPack_Oculus_ThermaVision__ctor_m1072 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_ThermaVision::get_material()
extern "C" Material_t2 * CameraFilterPack_Oculus_ThermaVision_get_material_m1073 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::Start()
extern "C" void CameraFilterPack_Oculus_ThermaVision_Start_m1074 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Oculus_ThermaVision_OnRenderImage_m1075 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::OnValidate()
extern "C" void CameraFilterPack_Oculus_ThermaVision_OnValidate_m1076 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::Update()
extern "C" void CameraFilterPack_Oculus_ThermaVision_Update_m1077 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_ThermaVision::OnDisable()
extern "C" void CameraFilterPack_Oculus_ThermaVision_OnDisable_m1078 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
