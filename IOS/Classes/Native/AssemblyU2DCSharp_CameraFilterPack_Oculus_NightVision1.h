﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Oculus_NightVision1
struct  CameraFilterPack_Oculus_NightVision1_t162  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Oculus_NightVision1::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Oculus_NightVision1::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Oculus_NightVision1::Distortion
	float ___Distortion_4;
	// UnityEngine.Material CameraFilterPack_Oculus_NightVision1::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// UnityEngine.Vector4 CameraFilterPack_Oculus_NightVision1::ScreenResolution
	Vector4_t5  ___ScreenResolution_6;
	// System.Single CameraFilterPack_Oculus_NightVision1::Vignette
	float ___Vignette_7;
	// System.Single CameraFilterPack_Oculus_NightVision1::Linecount
	float ___Linecount_8;
};
struct CameraFilterPack_Oculus_NightVision1_t162_StaticFields{
	// System.Single CameraFilterPack_Oculus_NightVision1::ChangeVignette
	float ___ChangeVignette_9;
	// System.Single CameraFilterPack_Oculus_NightVision1::ChangeLinecount
	float ___ChangeLinecount_10;
};
