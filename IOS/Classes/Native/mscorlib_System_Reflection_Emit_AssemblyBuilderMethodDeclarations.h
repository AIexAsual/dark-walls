﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1731;
// System.String
struct String_t;
// System.Reflection.Module[]
struct ModuleU5BU5D_t1730;
// System.Type[]
struct TypeU5BU5D_t485;
// System.Exception
struct Exception_t520;
// System.Reflection.AssemblyName
struct AssemblyName_t1779;

// System.String System.Reflection.Emit.AssemblyBuilder::get_Location()
extern "C" String_t* AssemblyBuilder_get_Location_m10669 (AssemblyBuilder_t1731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::GetModulesInternal()
extern "C" ModuleU5BU5D_t1730* AssemblyBuilder_GetModulesInternal_m10670 (AssemblyBuilder_t1731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type[] System.Reflection.Emit.AssemblyBuilder::GetTypes(System.Boolean)
extern "C" TypeU5BU5D_t485* AssemblyBuilder_GetTypes_m10671 (AssemblyBuilder_t1731 * __this, bool ___exportedOnly, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsCompilerContext()
extern "C" bool AssemblyBuilder_get_IsCompilerContext_m10672 (AssemblyBuilder_t1731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.AssemblyBuilder::not_supported()
extern "C" Exception_t520 * AssemblyBuilder_not_supported_m10673 (AssemblyBuilder_t1731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Emit.AssemblyBuilder::UnprotectedGetName()
extern "C" AssemblyName_t1779 * AssemblyBuilder_UnprotectedGetName_m10674 (AssemblyBuilder_t1731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
