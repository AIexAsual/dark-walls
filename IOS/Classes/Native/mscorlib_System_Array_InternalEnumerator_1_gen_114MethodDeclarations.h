﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Decimal>
struct InternalEnumerator_1_t2947;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"

// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22872_gshared (InternalEnumerator_1_t2947 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22872(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2947 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22872_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22873_gshared (InternalEnumerator_1_t2947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22873(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2947 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22873_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22874_gshared (InternalEnumerator_1_t2947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22874(__this, method) (( void (*) (InternalEnumerator_1_t2947 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22874_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22875_gshared (InternalEnumerator_1_t2947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22875(__this, method) (( bool (*) (InternalEnumerator_1_t2947 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22875_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern "C" Decimal_t1133  InternalEnumerator_1_get_Current_m22876_gshared (InternalEnumerator_1_t2947 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22876(__this, method) (( Decimal_t1133  (*) (InternalEnumerator_1_t2947 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22876_gshared)(__this, method)
