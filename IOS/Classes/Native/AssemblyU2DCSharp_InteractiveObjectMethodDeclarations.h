﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveObject
struct InteractiveObject_t328;

// System.Void InteractiveObject::.ctor()
extern "C" void InteractiveObject__ctor_m1967 (InteractiveObject_t328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObject::Start()
extern "C" void InteractiveObject_Start_m1968 (InteractiveObject_t328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObject::SetGazedAt(System.Boolean)
extern "C" void InteractiveObject_SetGazedAt_m1969 (InteractiveObject_t328 * __this, bool ___gazedAt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObject::Update()
extern "C" void InteractiveObject_Update_m1970 (InteractiveObject_t328 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
