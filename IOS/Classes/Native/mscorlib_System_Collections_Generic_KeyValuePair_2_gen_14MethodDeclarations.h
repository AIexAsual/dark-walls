﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
struct KeyValuePair_2_t2584;
// UnityEngine.UI.Graphic
struct Graphic_t654;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18620(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2584 *, Graphic_t654 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m15218_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m18621(__this, method) (( Graphic_t654 * (*) (KeyValuePair_2_t2584 *, const MethodInfo*))KeyValuePair_2_get_Key_m15219_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18622(__this, ___value, method) (( void (*) (KeyValuePair_2_t2584 *, Graphic_t654 *, const MethodInfo*))KeyValuePair_2_set_Key_m15220_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m18623(__this, method) (( int32_t (*) (KeyValuePair_2_t2584 *, const MethodInfo*))KeyValuePair_2_get_Value_m15221_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18624(__this, ___value, method) (( void (*) (KeyValuePair_2_t2584 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m15222_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m18625(__this, method) (( String_t* (*) (KeyValuePair_2_t2584 *, const MethodInfo*))KeyValuePair_2_ToString_m15223_gshared)(__this, method)
