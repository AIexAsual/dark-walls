﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Artefact
struct CameraFilterPack_TV_Artefact_t179;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Artefact::.ctor()
extern "C" void CameraFilterPack_TV_Artefact__ctor_m1158 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Artefact::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Artefact_get_material_m1159 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::Start()
extern "C" void CameraFilterPack_TV_Artefact_Start_m1160 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Artefact_OnRenderImage_m1161 (CameraFilterPack_TV_Artefact_t179 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::Update()
extern "C" void CameraFilterPack_TV_Artefact_Update_m1162 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Artefact::OnDisable()
extern "C" void CameraFilterPack_TV_Artefact_OnDisable_m1163 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
