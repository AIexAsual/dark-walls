﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Vision_Blood
struct  CameraFilterPack_Vision_Blood_t205  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Vision_Blood::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Vision_Blood::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Vision_Blood::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Vision_Blood::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Vision_Blood::HoleSize
	float ___HoleSize_6;
	// System.Single CameraFilterPack_Vision_Blood::HoleSmooth
	float ___HoleSmooth_7;
	// System.Single CameraFilterPack_Vision_Blood::Color1
	float ___Color1_8;
	// System.Single CameraFilterPack_Vision_Blood::Color2
	float ___Color2_9;
};
struct CameraFilterPack_Vision_Blood_t205_StaticFields{
	// System.Single CameraFilterPack_Vision_Blood::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Vision_Blood::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Vision_Blood::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Vision_Blood::ChangeValue4
	float ___ChangeValue4_13;
};
