﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Teleport
struct  Teleport_t214  : public MonoBehaviour_t4
{
	// UnityEngine.Vector3 Teleport::startingPosition
	Vector3_t215  ___startingPosition_2;
};
