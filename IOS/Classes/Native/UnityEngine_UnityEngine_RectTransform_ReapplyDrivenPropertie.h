﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t648;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct  ReapplyDrivenProperties_t822  : public MulticastDelegate_t219
{
};
