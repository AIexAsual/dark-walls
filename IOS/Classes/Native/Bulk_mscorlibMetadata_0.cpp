﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "mscorlib_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1539_il2cpp_TypeInfo;
// <Module>
#include "mscorlib_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CModuleU3E_t1539_0_0_0;
extern const Il2CppType U3CModuleU3E_t1539_1_0_0;
struct U3CModuleU3E_t1539;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1539_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t1539_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t1539_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1539_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1539_1_0_0/* this_arg */
	, &U3CModuleU3E_t1539_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1539)/* instance_size */
	, sizeof (U3CModuleU3E_t1539)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Object
#include "mscorlib_System_Object.h"
// Metadata Definition System.Object
extern TypeInfo Object_t_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
static const EncodedMethodIndex Object_t_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_0;
struct Object_t;
const Il2CppTypeDefinitionMetadata Object_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, Object_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8089/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Object_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Object"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Object_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2423/* custom_attributes_cache */
	, &Object_t_0_0_0/* byval_arg */
	, &Object_t_1_0_0/* this_arg */
	, &Object_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Object_t)/* instance_size */
	, sizeof (Object_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ValueType
#include "mscorlib_System_ValueType.h"
// Metadata Definition System.ValueType
extern TypeInfo ValueType_t1540_il2cpp_TypeInfo;
// System.ValueType
#include "mscorlib_System_ValueTypeMethodDeclarations.h"
static const EncodedMethodIndex ValueType_t1540_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ValueType_t1540_0_0_0;
extern const Il2CppType ValueType_t1540_1_0_0;
struct ValueType_t1540;
const Il2CppTypeDefinitionMetadata ValueType_t1540_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ValueType_t1540_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8099/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ValueType_t1540_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ValueType"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ValueType_t1540_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2427/* custom_attributes_cache */
	, &ValueType_t1540_0_0_0/* byval_arg */
	, &ValueType_t1540_1_0_0/* this_arg */
	, &ValueType_t1540_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ValueType_t1540)/* instance_size */
	, sizeof (ValueType_t1540)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Attribute
#include "mscorlib_System_Attribute.h"
// Metadata Definition System.Attribute
extern TypeInfo Attribute_t903_il2cpp_TypeInfo;
// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
static const EncodedMethodIndex Attribute_t903_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static const Il2CppType* Attribute_t903_InterfacesTypeInfos[] = 
{
	&_Attribute_t3429_0_0_0,
};
static Il2CppInterfaceOffsetPair Attribute_t903_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Attribute_t903_0_0_0;
extern const Il2CppType Attribute_t903_1_0_0;
struct Attribute_t903;
const Il2CppTypeDefinitionMetadata Attribute_t903_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Attribute_t903_InterfacesTypeInfos/* implementedInterfaces */
	, Attribute_t903_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Attribute_t903_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8106/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Attribute_t903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Attribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Attribute_t903_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2428/* custom_attributes_cache */
	, &Attribute_t903_0_0_0/* byval_arg */
	, &Attribute_t903_1_0_0/* this_arg */
	, &Attribute_t903_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Attribute_t903)/* instance_size */
	, sizeof (Attribute_t903)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Attribute
extern TypeInfo _Attribute_t3429_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Attribute_t3429_1_0_0;
struct _Attribute_t3429;
const Il2CppTypeDefinitionMetadata _Attribute_t3429_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Attribute_t3429_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Attribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Attribute_t3429_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2429/* custom_attributes_cache */
	, &_Attribute_t3429_0_0_0/* byval_arg */
	, &_Attribute_t3429_1_0_0/* this_arg */
	, &_Attribute_t3429_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Int32
#include "mscorlib_System_Int32.h"
// Metadata Definition System.Int32
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
static const EncodedMethodIndex Int32_t478_VTable[25] = 
{
	2243,
	601,
	2244,
	2245,
	2246,
	2247,
	2248,
	2249,
	2250,
	2251,
	2252,
	2253,
	2254,
	2255,
	2256,
	2257,
	2258,
	2259,
	2260,
	2261,
	2262,
	2263,
	2264,
	2265,
	2266,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
extern const Il2CppType IComparable_1_t3729_0_0_0;
extern const Il2CppType IEquatable_1_t3730_0_0_0;
static const Il2CppType* Int32_t478_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3729_0_0_0,
	&IEquatable_1_t3730_0_0_0,
};
static Il2CppInterfaceOffsetPair Int32_t478_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3729_0_0_0, 22},
	{ &IEquatable_1_t3730_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Int32_t478_0_0_0;
extern const Il2CppType Int32_t478_1_0_0;
const Il2CppTypeDefinitionMetadata Int32_t478_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Int32_t478_InterfacesTypeInfos/* implementedInterfaces */
	, Int32_t478_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Int32_t478_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6386/* fieldStart */
	, 8116/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Int32_t478_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Int32"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2430/* custom_attributes_cache */
	, &Int32_t478_0_0_0/* byval_arg */
	, &Int32_t478_1_0_0/* this_arg */
	, &Int32_t478_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Int32_t478)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Int32_t478)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 41/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.IFormattable
extern TypeInfo IFormattable_t2190_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormattable_t2190_1_0_0;
struct IFormattable_t2190;
const Il2CppTypeDefinitionMetadata IFormattable_t2190_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8157/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormattable_t2190_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormattable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IFormattable_t2190_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2431/* custom_attributes_cache */
	, &IFormattable_t2190_0_0_0/* byval_arg */
	, &IFormattable_t2190_1_0_0/* this_arg */
	, &IFormattable_t2190_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IConvertible
extern TypeInfo IConvertible_t2193_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IConvertible_t2193_1_0_0;
struct IConvertible_t2193;
const Il2CppTypeDefinitionMetadata IConvertible_t2193_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8158/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IConvertible_t2193_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConvertible"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IConvertible_t2193_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2432/* custom_attributes_cache */
	, &IConvertible_t2193_0_0_0/* byval_arg */
	, &IConvertible_t2193_1_0_0/* this_arg */
	, &IConvertible_t2193_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IComparable
extern TypeInfo IComparable_t2192_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparable_t2192_1_0_0;
struct IComparable_t2192;
const Il2CppTypeDefinitionMetadata IComparable_t2192_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8174/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparable_t2192_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IComparable_t2192_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2433/* custom_attributes_cache */
	, &IComparable_t2192_0_0_0/* byval_arg */
	, &IComparable_t2192_1_0_0/* this_arg */
	, &IComparable_t2192_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IComparable`1
extern TypeInfo IComparable_1_t3430_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IComparable_1_t3430_0_0_0;
extern const Il2CppType IComparable_1_t3430_1_0_0;
struct IComparable_1_t3430;
const Il2CppTypeDefinitionMetadata IComparable_1_t3430_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8175/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IComparable_1_t3430_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IComparable_1_t3430_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IComparable_1_t3430_0_0_0/* byval_arg */
	, &IComparable_1_t3430_1_0_0/* this_arg */
	, &IComparable_1_t3430_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 79/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.SerializableAttribute
#include "mscorlib_System_SerializableAttribute.h"
// Metadata Definition System.SerializableAttribute
extern TypeInfo SerializableAttribute_t1541_il2cpp_TypeInfo;
// System.SerializableAttribute
#include "mscorlib_System_SerializableAttributeMethodDeclarations.h"
static const EncodedMethodIndex SerializableAttribute_t1541_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair SerializableAttribute_t1541_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SerializableAttribute_t1541_0_0_0;
extern const Il2CppType SerializableAttribute_t1541_1_0_0;
struct SerializableAttribute_t1541;
const Il2CppTypeDefinitionMetadata SerializableAttribute_t1541_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SerializableAttribute_t1541_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SerializableAttribute_t1541_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8176/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SerializableAttribute_t1541_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SerializableAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &SerializableAttribute_t1541_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2434/* custom_attributes_cache */
	, &SerializableAttribute_t1541_0_0_0/* byval_arg */
	, &SerializableAttribute_t1541_1_0_0/* this_arg */
	, &SerializableAttribute_t1541_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SerializableAttribute_t1541)/* instance_size */
	, sizeof (SerializableAttribute_t1541)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// Metadata Definition System.AttributeUsageAttribute
extern TypeInfo AttributeUsageAttribute_t1542_il2cpp_TypeInfo;
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
static const EncodedMethodIndex AttributeUsageAttribute_t1542_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AttributeUsageAttribute_t1542_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeUsageAttribute_t1542_0_0_0;
extern const Il2CppType AttributeUsageAttribute_t1542_1_0_0;
struct AttributeUsageAttribute_t1542;
const Il2CppTypeDefinitionMetadata AttributeUsageAttribute_t1542_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AttributeUsageAttribute_t1542_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AttributeUsageAttribute_t1542_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6389/* fieldStart */
	, 8177/* methodStart */
	, -1/* eventStart */
	, 1666/* propertyStart */

};
TypeInfo AttributeUsageAttribute_t1542_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeUsageAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AttributeUsageAttribute_t1542_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2435/* custom_attributes_cache */
	, &AttributeUsageAttribute_t1542_0_0_0/* byval_arg */
	, &AttributeUsageAttribute_t1542_1_0_0/* this_arg */
	, &AttributeUsageAttribute_t1542_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeUsageAttribute_t1542)/* instance_size */
	, sizeof (AttributeUsageAttribute_t1542)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// Metadata Definition System.Runtime.InteropServices.ComVisibleAttribute
extern TypeInfo ComVisibleAttribute_t1543_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
static const EncodedMethodIndex ComVisibleAttribute_t1543_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ComVisibleAttribute_t1543_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComVisibleAttribute_t1543_0_0_0;
extern const Il2CppType ComVisibleAttribute_t1543_1_0_0;
struct ComVisibleAttribute_t1543;
const Il2CppTypeDefinitionMetadata ComVisibleAttribute_t1543_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComVisibleAttribute_t1543_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ComVisibleAttribute_t1543_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6392/* fieldStart */
	, 8182/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ComVisibleAttribute_t1543_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComVisibleAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &ComVisibleAttribute_t1543_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2436/* custom_attributes_cache */
	, &ComVisibleAttribute_t1543_0_0_0/* byval_arg */
	, &ComVisibleAttribute_t1543_1_0_0/* this_arg */
	, &ComVisibleAttribute_t1543_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComVisibleAttribute_t1543)/* instance_size */
	, sizeof (ComVisibleAttribute_t1543)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.IEquatable`1
extern TypeInfo IEquatable_1_t3431_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEquatable_1_t3431_0_0_0;
extern const Il2CppType IEquatable_1_t3431_1_0_0;
struct IEquatable_1_t3431;
const Il2CppTypeDefinitionMetadata IEquatable_1_t3431_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8183/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEquatable_1_t3431_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IEquatable_1_t3431_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEquatable_1_t3431_0_0_0/* byval_arg */
	, &IEquatable_1_t3431_1_0_0/* this_arg */
	, &IEquatable_1_t3431_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 80/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Int64
#include "mscorlib_System_Int64.h"
// Metadata Definition System.Int64
extern TypeInfo Int64_t1131_il2cpp_TypeInfo;
// System.Int64
#include "mscorlib_System_Int64MethodDeclarations.h"
static const EncodedMethodIndex Int64_t1131_VTable[24] = 
{
	2267,
	601,
	2268,
	2269,
	2270,
	2271,
	2272,
	2273,
	2274,
	2275,
	2276,
	2277,
	2278,
	2279,
	2280,
	2281,
	2282,
	2283,
	2284,
	2285,
	2286,
	2287,
	2288,
	2289,
};
extern const Il2CppType IComparable_1_t3731_0_0_0;
extern const Il2CppType IEquatable_1_t3732_0_0_0;
static const Il2CppType* Int64_t1131_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3731_0_0_0,
	&IEquatable_1_t3732_0_0_0,
};
static Il2CppInterfaceOffsetPair Int64_t1131_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3731_0_0_0, 22},
	{ &IEquatable_1_t3732_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Int64_t1131_0_0_0;
extern const Il2CppType Int64_t1131_1_0_0;
const Il2CppTypeDefinitionMetadata Int64_t1131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Int64_t1131_InterfacesTypeInfos/* implementedInterfaces */
	, Int64_t1131_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Int64_t1131_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6393/* fieldStart */
	, 8184/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Int64_t1131_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Int64"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int64_t1131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2437/* custom_attributes_cache */
	, &Int64_t1131_0_0_0/* byval_arg */
	, &Int64_t1131_1_0_0/* this_arg */
	, &Int64_t1131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Int64_t1131)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Int64_t1131)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 31/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.UInt32
#include "mscorlib_System_UInt32.h"
// Metadata Definition System.UInt32
extern TypeInfo UInt32_t1124_il2cpp_TypeInfo;
// System.UInt32
#include "mscorlib_System_UInt32MethodDeclarations.h"
static const EncodedMethodIndex UInt32_t1124_VTable[24] = 
{
	2290,
	601,
	2291,
	2292,
	2293,
	2294,
	2295,
	2296,
	2297,
	2298,
	2299,
	2300,
	2301,
	2302,
	2303,
	2304,
	2305,
	2306,
	2307,
	2308,
	2309,
	2310,
	2311,
	2312,
};
extern const Il2CppType IComparable_1_t3733_0_0_0;
extern const Il2CppType IEquatable_1_t3734_0_0_0;
static const Il2CppType* UInt32_t1124_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3733_0_0_0,
	&IEquatable_1_t3734_0_0_0,
};
static Il2CppInterfaceOffsetPair UInt32_t1124_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3733_0_0_0, 22},
	{ &IEquatable_1_t3734_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UInt32_t1124_0_0_0;
extern const Il2CppType UInt32_t1124_1_0_0;
const Il2CppTypeDefinitionMetadata UInt32_t1124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UInt32_t1124_InterfacesTypeInfos/* implementedInterfaces */
	, UInt32_t1124_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, UInt32_t1124_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6394/* fieldStart */
	, 8215/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UInt32_t1124_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UInt32"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UInt32_t1124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2438/* custom_attributes_cache */
	, &UInt32_t1124_0_0_0/* byval_arg */
	, &UInt32_t1124_1_0_0/* this_arg */
	, &UInt32_t1124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UInt32_t1124)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UInt32_t1124)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// Metadata Definition System.CLSCompliantAttribute
extern TypeInfo CLSCompliantAttribute_t1544_il2cpp_TypeInfo;
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
static const EncodedMethodIndex CLSCompliantAttribute_t1544_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair CLSCompliantAttribute_t1544_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CLSCompliantAttribute_t1544_0_0_0;
extern const Il2CppType CLSCompliantAttribute_t1544_1_0_0;
struct CLSCompliantAttribute_t1544;
const Il2CppTypeDefinitionMetadata CLSCompliantAttribute_t1544_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CLSCompliantAttribute_t1544_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, CLSCompliantAttribute_t1544_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6395/* fieldStart */
	, 8245/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CLSCompliantAttribute_t1544_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CLSCompliantAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CLSCompliantAttribute_t1544_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2443/* custom_attributes_cache */
	, &CLSCompliantAttribute_t1544_0_0_0/* byval_arg */
	, &CLSCompliantAttribute_t1544_1_0_0/* this_arg */
	, &CLSCompliantAttribute_t1544_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CLSCompliantAttribute_t1544)/* instance_size */
	, sizeof (CLSCompliantAttribute_t1544)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.UInt64
#include "mscorlib_System_UInt64.h"
// Metadata Definition System.UInt64
extern TypeInfo UInt64_t1134_il2cpp_TypeInfo;
// System.UInt64
#include "mscorlib_System_UInt64MethodDeclarations.h"
static const EncodedMethodIndex UInt64_t1134_VTable[24] = 
{
	2313,
	601,
	2314,
	2315,
	2316,
	2317,
	2318,
	2319,
	2320,
	2321,
	2322,
	2323,
	2324,
	2325,
	2326,
	2327,
	2328,
	2329,
	2330,
	2331,
	2332,
	2333,
	2334,
	2335,
};
extern const Il2CppType IComparable_1_t3735_0_0_0;
extern const Il2CppType IEquatable_1_t3736_0_0_0;
static const Il2CppType* UInt64_t1134_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3735_0_0_0,
	&IEquatable_1_t3736_0_0_0,
};
static Il2CppInterfaceOffsetPair UInt64_t1134_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3735_0_0_0, 22},
	{ &IEquatable_1_t3736_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UInt64_t1134_0_0_0;
extern const Il2CppType UInt64_t1134_1_0_0;
const Il2CppTypeDefinitionMetadata UInt64_t1134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UInt64_t1134_InterfacesTypeInfos/* implementedInterfaces */
	, UInt64_t1134_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, UInt64_t1134_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6396/* fieldStart */
	, 8246/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UInt64_t1134_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UInt64"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UInt64_t1134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2444/* custom_attributes_cache */
	, &UInt64_t1134_0_0_0/* byval_arg */
	, &UInt64_t1134_1_0_0/* this_arg */
	, &UInt64_t1134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UInt64_t1134)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UInt64_t1134)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Byte
#include "mscorlib_System_Byte.h"
// Metadata Definition System.Byte
extern TypeInfo Byte_t1117_il2cpp_TypeInfo;
// System.Byte
#include "mscorlib_System_ByteMethodDeclarations.h"
static const EncodedMethodIndex Byte_t1117_VTable[24] = 
{
	2336,
	601,
	2337,
	2338,
	2339,
	2340,
	2341,
	2342,
	2343,
	2344,
	2345,
	2346,
	2347,
	2348,
	2349,
	2350,
	2351,
	2352,
	2353,
	2354,
	2355,
	2356,
	2357,
	2358,
};
extern const Il2CppType IComparable_1_t3737_0_0_0;
extern const Il2CppType IEquatable_1_t3738_0_0_0;
static const Il2CppType* Byte_t1117_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3737_0_0_0,
	&IEquatable_1_t3738_0_0_0,
};
static Il2CppInterfaceOffsetPair Byte_t1117_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3737_0_0_0, 22},
	{ &IEquatable_1_t3738_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Byte_t1117_0_0_0;
extern const Il2CppType Byte_t1117_1_0_0;
const Il2CppTypeDefinitionMetadata Byte_t1117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Byte_t1117_InterfacesTypeInfos/* implementedInterfaces */
	, Byte_t1117_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Byte_t1117_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6397/* fieldStart */
	, 8274/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Byte_t1117_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Byte"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2448/* custom_attributes_cache */
	, &Byte_t1117_0_0_0/* byval_arg */
	, &Byte_t1117_1_0_0/* this_arg */
	, &Byte_t1117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Byte_t1117)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Byte_t1117)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 29/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.SByte
#include "mscorlib_System_SByte.h"
// Metadata Definition System.SByte
extern TypeInfo SByte_t1135_il2cpp_TypeInfo;
// System.SByte
#include "mscorlib_System_SByteMethodDeclarations.h"
static const EncodedMethodIndex SByte_t1135_VTable[24] = 
{
	2359,
	601,
	2360,
	2361,
	2362,
	2363,
	2364,
	2365,
	2366,
	2367,
	2368,
	2369,
	2370,
	2371,
	2372,
	2373,
	2374,
	2375,
	2376,
	2377,
	2378,
	2379,
	2380,
	2381,
};
extern const Il2CppType IComparable_1_t3739_0_0_0;
extern const Il2CppType IEquatable_1_t3740_0_0_0;
static const Il2CppType* SByte_t1135_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3739_0_0_0,
	&IEquatable_1_t3740_0_0_0,
};
static Il2CppInterfaceOffsetPair SByte_t1135_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3739_0_0_0, 22},
	{ &IEquatable_1_t3740_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SByte_t1135_0_0_0;
extern const Il2CppType SByte_t1135_1_0_0;
const Il2CppTypeDefinitionMetadata SByte_t1135_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SByte_t1135_InterfacesTypeInfos/* implementedInterfaces */
	, SByte_t1135_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, SByte_t1135_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6400/* fieldStart */
	, 8303/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SByte_t1135_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SByte"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &SByte_t1135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2449/* custom_attributes_cache */
	, &SByte_t1135_0_0_0/* byval_arg */
	, &SByte_t1135_1_0_0/* this_arg */
	, &SByte_t1135_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SByte_t1135)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SByte_t1135)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Int16
#include "mscorlib_System_Int16.h"
// Metadata Definition System.Int16
extern TypeInfo Int16_t1136_il2cpp_TypeInfo;
// System.Int16
#include "mscorlib_System_Int16MethodDeclarations.h"
static const EncodedMethodIndex Int16_t1136_VTable[24] = 
{
	2382,
	601,
	2383,
	2384,
	2385,
	2386,
	2387,
	2388,
	2389,
	2390,
	2391,
	2392,
	2393,
	2394,
	2395,
	2396,
	2397,
	2398,
	2399,
	2400,
	2401,
	2402,
	2403,
	2404,
};
extern const Il2CppType IComparable_1_t3741_0_0_0;
extern const Il2CppType IEquatable_1_t3742_0_0_0;
static const Il2CppType* Int16_t1136_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3741_0_0_0,
	&IEquatable_1_t3742_0_0_0,
};
static Il2CppInterfaceOffsetPair Int16_t1136_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3741_0_0_0, 22},
	{ &IEquatable_1_t3742_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Int16_t1136_0_0_0;
extern const Il2CppType Int16_t1136_1_0_0;
const Il2CppTypeDefinitionMetadata Int16_t1136_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Int16_t1136_InterfacesTypeInfos/* implementedInterfaces */
	, Int16_t1136_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Int16_t1136_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6401/* fieldStart */
	, 8331/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Int16_t1136_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Int16"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int16_t1136_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2453/* custom_attributes_cache */
	, &Int16_t1136_0_0_0/* byval_arg */
	, &Int16_t1136_1_0_0/* this_arg */
	, &Int16_t1136_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Int16_t1136)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Int16_t1136)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 29/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.UInt16
#include "mscorlib_System_UInt16.h"
// Metadata Definition System.UInt16
extern TypeInfo UInt16_t1122_il2cpp_TypeInfo;
// System.UInt16
#include "mscorlib_System_UInt16MethodDeclarations.h"
static const EncodedMethodIndex UInt16_t1122_VTable[24] = 
{
	2405,
	601,
	2406,
	2407,
	2408,
	2409,
	2410,
	2411,
	2412,
	2413,
	2414,
	2415,
	2416,
	2417,
	2418,
	2419,
	2420,
	2421,
	2422,
	2423,
	2424,
	2425,
	2426,
	2427,
};
extern const Il2CppType IComparable_1_t3743_0_0_0;
extern const Il2CppType IEquatable_1_t3744_0_0_0;
static const Il2CppType* UInt16_t1122_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3743_0_0_0,
	&IEquatable_1_t3744_0_0_0,
};
static Il2CppInterfaceOffsetPair UInt16_t1122_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3743_0_0_0, 22},
	{ &IEquatable_1_t3744_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UInt16_t1122_0_0_0;
extern const Il2CppType UInt16_t1122_1_0_0;
const Il2CppTypeDefinitionMetadata UInt16_t1122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UInt16_t1122_InterfacesTypeInfos/* implementedInterfaces */
	, UInt16_t1122_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, UInt16_t1122_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6402/* fieldStart */
	, 8360/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UInt16_t1122_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UInt16"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UInt16_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2454/* custom_attributes_cache */
	, &UInt16_t1122_0_0_0/* byval_arg */
	, &UInt16_t1122_1_0_0/* this_arg */
	, &UInt16_t1122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UInt16_t1122)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UInt16_t1122)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Collections.IEnumerator
extern TypeInfo IEnumerator_t464_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IEnumerator_t464_1_0_0;
struct IEnumerator_t464;
const Il2CppTypeDefinitionMetadata IEnumerator_t464_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8388/* methodStart */
	, -1/* eventStart */
	, 1668/* propertyStart */

};
TypeInfo IEnumerator_t464_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IEnumerator_t464_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2459/* custom_attributes_cache */
	, &IEnumerator_t464_0_0_0/* byval_arg */
	, &IEnumerator_t464_1_0_0/* this_arg */
	, &IEnumerator_t464_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IEnumerable
extern TypeInfo IEnumerable_t1097_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerable_t1097_0_0_0;
extern const Il2CppType IEnumerable_t1097_1_0_0;
struct IEnumerable_t1097;
const Il2CppTypeDefinitionMetadata IEnumerable_t1097_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8390/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEnumerable_t1097_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IEnumerable_t1097_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2460/* custom_attributes_cache */
	, &IEnumerable_t1097_0_0_0/* byval_arg */
	, &IEnumerable_t1097_1_0_0/* this_arg */
	, &IEnumerable_t1097_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IDisposable
extern TypeInfo IDisposable_t538_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDisposable_t538_0_0_0;
extern const Il2CppType IDisposable_t538_1_0_0;
struct IDisposable_t538;
const Il2CppTypeDefinitionMetadata IDisposable_t538_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8391/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IDisposable_t538_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDisposable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IDisposable_t538_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2462/* custom_attributes_cache */
	, &IDisposable_t538_0_0_0/* byval_arg */
	, &IDisposable_t538_1_0_0/* this_arg */
	, &IDisposable_t538_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IEnumerator`1
extern TypeInfo IEnumerator_1_t3432_il2cpp_TypeInfo;
static const Il2CppType* IEnumerator_1_t3432_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerator_1_t3432_0_0_0;
extern const Il2CppType IEnumerator_1_t3432_1_0_0;
struct IEnumerator_1_t3432;
const Il2CppTypeDefinitionMetadata IEnumerator_1_t3432_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IEnumerator_1_t3432_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8392/* methodStart */
	, -1/* eventStart */
	, 1669/* propertyStart */

};
TypeInfo IEnumerator_1_t3432_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IEnumerator_1_t3432_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEnumerator_1_t3432_0_0_0/* byval_arg */
	, &IEnumerator_1_t3432_1_0_0/* this_arg */
	, &IEnumerator_1_t3432_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 81/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Char
#include "mscorlib_System_Char.h"
// Metadata Definition System.Char
extern TypeInfo Char_t526_il2cpp_TypeInfo;
// System.Char
#include "mscorlib_System_CharMethodDeclarations.h"
static const EncodedMethodIndex Char_t526_VTable[24] = 
{
	2428,
	601,
	2429,
	2430,
	2431,
	2432,
	2433,
	2434,
	2435,
	2436,
	2437,
	2438,
	2439,
	2440,
	2441,
	2442,
	2443,
	2444,
	2445,
	2446,
	2447,
	2448,
	2449,
	2450,
};
extern const Il2CppType IComparable_1_t3745_0_0_0;
extern const Il2CppType IEquatable_1_t3746_0_0_0;
static const Il2CppType* Char_t526_InterfacesTypeInfos[] = 
{
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3745_0_0_0,
	&IEquatable_1_t3746_0_0_0,
};
static Il2CppInterfaceOffsetPair Char_t526_InterfacesOffsets[] = 
{
	{ &IConvertible_t2193_0_0_0, 4},
	{ &IComparable_t2192_0_0_0, 20},
	{ &IComparable_1_t3745_0_0_0, 21},
	{ &IEquatable_1_t3746_0_0_0, 22},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Char_t526_0_0_0;
extern const Il2CppType Char_t526_1_0_0;
const Il2CppTypeDefinitionMetadata Char_t526_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Char_t526_InterfacesTypeInfos/* implementedInterfaces */
	, Char_t526_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Char_t526_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6405/* fieldStart */
	, 8393/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Char_t526_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Char"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Char_t526_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2463/* custom_attributes_cache */
	, &Char_t526_0_0_0/* byval_arg */
	, &Char_t526_1_0_0/* this_arg */
	, &Char_t526_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Char_t526)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Char_t526)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, 1/* native_size */
	, sizeof(Char_t526_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 41/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.String
#include "mscorlib_System_String.h"
// Metadata Definition System.String
extern TypeInfo String_t_il2cpp_TypeInfo;
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
static const EncodedMethodIndex String_t_VTable[25] = 
{
	2451,
	601,
	2452,
	2453,
	2454,
	2455,
	2456,
	2457,
	2458,
	2459,
	2460,
	2461,
	2462,
	2463,
	2464,
	2465,
	2466,
	2467,
	2468,
	2469,
	2470,
	2471,
	2472,
	2473,
	2474,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType IComparable_1_t3747_0_0_0;
extern const Il2CppType IEquatable_1_t3748_0_0_0;
extern const Il2CppType IEnumerable_1_t527_0_0_0;
static const Il2CppType* String_t_InterfacesTypeInfos[] = 
{
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IEnumerable_t1097_0_0_0,
	&ICloneable_t3433_0_0_0,
	&IComparable_1_t3747_0_0_0,
	&IEquatable_1_t3748_0_0_0,
	&IEnumerable_1_t527_0_0_0,
};
static Il2CppInterfaceOffsetPair String_t_InterfacesOffsets[] = 
{
	{ &IConvertible_t2193_0_0_0, 4},
	{ &IComparable_t2192_0_0_0, 20},
	{ &IEnumerable_t1097_0_0_0, 21},
	{ &ICloneable_t3433_0_0_0, 22},
	{ &IComparable_1_t3747_0_0_0, 22},
	{ &IEquatable_1_t3748_0_0_0, 23},
	{ &IEnumerable_1_t527_0_0_0, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_1_0_0;
struct String_t;
const Il2CppTypeDefinitionMetadata String_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, String_t_InterfacesTypeInfos/* implementedInterfaces */
	, String_t_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, String_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6415/* fieldStart */
	, 8434/* methodStart */
	, -1/* eventStart */
	, 1670/* propertyStart */

};
TypeInfo String_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "String"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &String_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2464/* custom_attributes_cache */
	, &String_t_0_0_0/* byval_arg */
	, &String_t_1_0_0/* this_arg */
	, &String_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (String_t)/* instance_size */
	, sizeof (String_t)/* actualSize */
	, 0/* element_size */
	, sizeof(char*)/* native_size */
	, sizeof(String_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 141/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
// Metadata Definition System.ICloneable
extern TypeInfo ICloneable_t3433_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICloneable_t3433_1_0_0;
struct ICloneable_t3433;
const Il2CppTypeDefinitionMetadata ICloneable_t3433_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICloneable_t3433_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICloneable"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ICloneable_t3433_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2481/* custom_attributes_cache */
	, &ICloneable_t3433_0_0_0/* byval_arg */
	, &ICloneable_t3433_1_0_0/* this_arg */
	, &ICloneable_t3433_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IEnumerable`1
extern TypeInfo IEnumerable_1_t3434_il2cpp_TypeInfo;
static const Il2CppType* IEnumerable_1_t3434_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnumerable_1_t3434_0_0_0;
extern const Il2CppType IEnumerable_1_t3434_1_0_0;
struct IEnumerable_1_t3434;
const Il2CppTypeDefinitionMetadata IEnumerable_1_t3434_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IEnumerable_1_t3434_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8575/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEnumerable_1_t3434_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IEnumerable_1_t3434_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEnumerable_1_t3434_0_0_0/* byval_arg */
	, &IEnumerable_1_t3434_1_0_0/* this_arg */
	, &IEnumerable_1_t3434_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 82/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Single
#include "mscorlib_System_Single.h"
// Metadata Definition System.Single
extern TypeInfo Single_t531_il2cpp_TypeInfo;
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
static const EncodedMethodIndex Single_t531_VTable[25] = 
{
	2475,
	601,
	2476,
	2477,
	2478,
	2479,
	2480,
	2481,
	2482,
	2483,
	2484,
	2485,
	2486,
	2487,
	2488,
	2489,
	2490,
	2491,
	2492,
	2493,
	2494,
	2495,
	2496,
	2497,
	2498,
};
extern const Il2CppType IComparable_1_t3749_0_0_0;
extern const Il2CppType IEquatable_1_t3750_0_0_0;
static const Il2CppType* Single_t531_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3749_0_0_0,
	&IEquatable_1_t3750_0_0_0,
};
static Il2CppInterfaceOffsetPair Single_t531_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3749_0_0_0, 22},
	{ &IEquatable_1_t3750_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Single_t531_0_0_0;
extern const Il2CppType Single_t531_1_0_0;
const Il2CppTypeDefinitionMetadata Single_t531_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Single_t531_InterfacesTypeInfos/* implementedInterfaces */
	, Single_t531_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Single_t531_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6419/* fieldStart */
	, 8576/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Single_t531_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Single"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Single_t531_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2482/* custom_attributes_cache */
	, &Single_t531_0_0_0/* byval_arg */
	, &Single_t531_1_0_0/* this_arg */
	, &Single_t531_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Single_t531)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Single_t531)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(float)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 30/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Double
#include "mscorlib_System_Double.h"
// Metadata Definition System.Double
extern TypeInfo Double_t553_il2cpp_TypeInfo;
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"
static const EncodedMethodIndex Double_t553_VTable[24] = 
{
	2499,
	601,
	2500,
	2501,
	2502,
	2503,
	2504,
	2505,
	2506,
	2507,
	2508,
	2509,
	2510,
	2511,
	2512,
	2513,
	2514,
	2515,
	2516,
	2517,
	2518,
	2519,
	2520,
	2521,
};
extern const Il2CppType IComparable_1_t3751_0_0_0;
extern const Il2CppType IEquatable_1_t3752_0_0_0;
static const Il2CppType* Double_t553_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3751_0_0_0,
	&IEquatable_1_t3752_0_0_0,
};
static Il2CppInterfaceOffsetPair Double_t553_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3751_0_0_0, 22},
	{ &IEquatable_1_t3752_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Double_t553_0_0_0;
extern const Il2CppType Double_t553_1_0_0;
const Il2CppTypeDefinitionMetadata Double_t553_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Double_t553_InterfacesTypeInfos/* implementedInterfaces */
	, Double_t553_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Double_t553_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6427/* fieldStart */
	, 8606/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Double_t553_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Double"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Double_t553_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2484/* custom_attributes_cache */
	, &Double_t553_0_0_0/* byval_arg */
	, &Double_t553_1_0_0/* this_arg */
	, &Double_t553_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Double_t553)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Double_t553)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(double)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 34/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Decimal
#include "mscorlib_System_Decimal.h"
// Metadata Definition System.Decimal
extern TypeInfo Decimal_t1133_il2cpp_TypeInfo;
// System.Decimal
#include "mscorlib_System_DecimalMethodDeclarations.h"
static const EncodedMethodIndex Decimal_t1133_VTable[24] = 
{
	2522,
	601,
	2523,
	2524,
	2525,
	2526,
	2527,
	2528,
	2529,
	2530,
	2531,
	2532,
	2533,
	2534,
	2535,
	2536,
	2537,
	2538,
	2539,
	2540,
	2541,
	2542,
	2543,
	2544,
};
extern const Il2CppType IComparable_1_t3753_0_0_0;
extern const Il2CppType IEquatable_1_t3754_0_0_0;
static const Il2CppType* Decimal_t1133_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3753_0_0_0,
	&IEquatable_1_t3754_0_0_0,
};
static Il2CppInterfaceOffsetPair Decimal_t1133_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3753_0_0_0, 22},
	{ &IEquatable_1_t3754_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Decimal_t1133_0_0_0;
extern const Il2CppType Decimal_t1133_1_0_0;
const Il2CppTypeDefinitionMetadata Decimal_t1133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Decimal_t1133_InterfacesTypeInfos/* implementedInterfaces */
	, Decimal_t1133_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Decimal_t1133_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6441/* fieldStart */
	, 8640/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Decimal_t1133_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Decimal"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Decimal_t1133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2486/* custom_attributes_cache */
	, &Decimal_t1133_0_0_0/* byval_arg */
	, &Decimal_t1133_1_0_0/* this_arg */
	, &Decimal_t1133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Decimal_t1133)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Decimal_t1133)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Decimal_t1133 )/* native_size */
	, sizeof(Decimal_t1133_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 86/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Boolean
#include "mscorlib_System_Boolean.h"
// Metadata Definition System.Boolean
extern TypeInfo Boolean_t536_il2cpp_TypeInfo;
// System.Boolean
#include "mscorlib_System_BooleanMethodDeclarations.h"
static const EncodedMethodIndex Boolean_t536_VTable[24] = 
{
	2545,
	601,
	2546,
	2547,
	2548,
	2549,
	2550,
	2551,
	2552,
	2553,
	2554,
	2555,
	2556,
	2557,
	2558,
	2559,
	2560,
	2561,
	2562,
	2563,
	2564,
	2565,
	2566,
	2567,
};
extern const Il2CppType IComparable_1_t3755_0_0_0;
extern const Il2CppType IEquatable_1_t3756_0_0_0;
static const Il2CppType* Boolean_t536_InterfacesTypeInfos[] = 
{
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3755_0_0_0,
	&IEquatable_1_t3756_0_0_0,
};
static Il2CppInterfaceOffsetPair Boolean_t536_InterfacesOffsets[] = 
{
	{ &IConvertible_t2193_0_0_0, 4},
	{ &IComparable_t2192_0_0_0, 20},
	{ &IComparable_1_t3755_0_0_0, 21},
	{ &IEquatable_1_t3756_0_0_0, 22},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Boolean_t536_0_0_0;
extern const Il2CppType Boolean_t536_1_0_0;
const Il2CppTypeDefinitionMetadata Boolean_t536_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Boolean_t536_InterfacesTypeInfos/* implementedInterfaces */
	, Boolean_t536_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Boolean_t536_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6450/* fieldStart */
	, 8726/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Boolean_t536_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Boolean"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Boolean_t536_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2502/* custom_attributes_cache */
	, &Boolean_t536_0_0_0/* byval_arg */
	, &Boolean_t536_1_0_0/* this_arg */
	, &Boolean_t536_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Boolean_t536)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Boolean_t536)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, 4/* native_size */
	, sizeof(Boolean_t536_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Metadata Definition System.IntPtr
extern TypeInfo IntPtr_t_il2cpp_TypeInfo;
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
static const EncodedMethodIndex IntPtr_t_VTable[5] = 
{
	2568,
	601,
	2569,
	2570,
	2571,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
static const Il2CppType* IntPtr_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair IntPtr_t_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_1_0_0;
const Il2CppTypeDefinitionMetadata IntPtr_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IntPtr_t_InterfacesTypeInfos/* implementedInterfaces */
	, IntPtr_t_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, IntPtr_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6453/* fieldStart */
	, 8751/* methodStart */
	, -1/* eventStart */
	, 1672/* propertyStart */

};
TypeInfo IntPtr_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntPtr"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IntPtr_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2503/* custom_attributes_cache */
	, &IntPtr_t_0_0_0/* byval_arg */
	, &IntPtr_t_1_0_0/* this_arg */
	, &IntPtr_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntPtr_t)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (IntPtr_t)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(IntPtr_t)/* native_size */
	, sizeof(IntPtr_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 18/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Serialization.ISerializable
extern TypeInfo ISerializable_t2210_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializable_t2210_1_0_0;
struct ISerializable_t2210;
const Il2CppTypeDefinitionMetadata ISerializable_t2210_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8769/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializable_t2210_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializable"/* name */
	, "System.Runtime.Serialization"/* namespaze */
	, NULL/* methods */
	, &ISerializable_t2210_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2515/* custom_attributes_cache */
	, &ISerializable_t2210_0_0_0/* byval_arg */
	, &ISerializable_t2210_1_0_0/* this_arg */
	, &ISerializable_t2210_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
// Metadata Definition System.UIntPtr
extern TypeInfo UIntPtr_t_il2cpp_TypeInfo;
// System.UIntPtr
#include "mscorlib_System_UIntPtrMethodDeclarations.h"
static const EncodedMethodIndex UIntPtr_t_VTable[5] = 
{
	2572,
	601,
	2573,
	2574,
	2575,
};
static const Il2CppType* UIntPtr_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair UIntPtr_t_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UIntPtr_t_0_0_0;
extern const Il2CppType UIntPtr_t_1_0_0;
const Il2CppTypeDefinitionMetadata UIntPtr_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UIntPtr_t_InterfacesTypeInfos/* implementedInterfaces */
	, UIntPtr_t_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, UIntPtr_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6455/* fieldStart */
	, 8770/* methodStart */
	, -1/* eventStart */
	, 1673/* propertyStart */

};
TypeInfo UIntPtr_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UIntPtr"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UIntPtr_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2516/* custom_attributes_cache */
	, &UIntPtr_t_0_0_0/* byval_arg */
	, &UIntPtr_t_1_0_0/* this_arg */
	, &UIntPtr_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UIntPtr_t)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UIntPtr_t)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(UIntPtr_t )/* native_size */
	, sizeof(UIntPtr_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 20/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Metadata Definition System.MulticastDelegate
extern TypeInfo MulticastDelegate_t219_il2cpp_TypeInfo;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegateMethodDeclarations.h"
static const EncodedMethodIndex MulticastDelegate_t219_VTable[10] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
};
static Il2CppInterfaceOffsetPair MulticastDelegate_t219_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
extern const Il2CppType MulticastDelegate_t219_1_0_0;
extern const Il2CppType Delegate_t480_0_0_0;
struct MulticastDelegate_t219;
const Il2CppTypeDefinitionMetadata MulticastDelegate_t219_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MulticastDelegate_t219_InterfacesOffsets/* interfaceOffsets */
	, &Delegate_t480_0_0_0/* parent */
	, MulticastDelegate_t219_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6457/* fieldStart */
	, 8790/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MulticastDelegate_t219_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MulticastDelegate"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MulticastDelegate_t219_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2521/* custom_attributes_cache */
	, &MulticastDelegate_t219_0_0_0/* byval_arg */
	, &MulticastDelegate_t219_1_0_0/* this_arg */
	, &MulticastDelegate_t219_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MulticastDelegate_t219)/* instance_size */
	, sizeof (MulticastDelegate_t219)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Delegate
#include "mscorlib_System_Delegate.h"
// Metadata Definition System.Delegate
extern TypeInfo Delegate_t480_il2cpp_TypeInfo;
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
static const EncodedMethodIndex Delegate_t480_VTable[10] = 
{
	2576,
	601,
	2577,
	628,
	2578,
	637,
	2578,
	2579,
	2580,
	2581,
};
static const Il2CppType* Delegate_t480_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair Delegate_t480_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Delegate_t480_1_0_0;
struct Delegate_t480;
const Il2CppTypeDefinitionMetadata Delegate_t480_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Delegate_t480_InterfacesTypeInfos/* implementedInterfaces */
	, Delegate_t480_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Delegate_t480_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6459/* fieldStart */
	, 8798/* methodStart */
	, -1/* eventStart */
	, 1674/* propertyStart */

};
TypeInfo Delegate_t480_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Delegate"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Delegate_t480_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2522/* custom_attributes_cache */
	, &Delegate_t480_0_0_0/* byval_arg */
	, &Delegate_t480_1_0_0/* this_arg */
	, &Delegate_t480_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Delegate_t480)/* instance_size */
	, sizeof (Delegate_t480)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 2/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Enum
#include "mscorlib_System_Enum.h"
// Metadata Definition System.Enum
extern TypeInfo Enum_t554_il2cpp_TypeInfo;
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
static const EncodedMethodIndex Enum_t554_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static const Il2CppType* Enum_t554_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
};
static Il2CppInterfaceOffsetPair Enum_t554_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enum_t554_0_0_0;
extern const Il2CppType Enum_t554_1_0_0;
struct Enum_t554;
const Il2CppTypeDefinitionMetadata Enum_t554_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Enum_t554_InterfacesTypeInfos/* implementedInterfaces */
	, Enum_t554_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Enum_t554_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6468/* fieldStart */
	, 8823/* methodStart */
	, -1/* eventStart */
	, 1676/* propertyStart */

};
TypeInfo Enum_t554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enum"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Enum_t554_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2525/* custom_attributes_cache */
	, &Enum_t554_0_0_0/* byval_arg */
	, &Enum_t554_1_0_0/* this_arg */
	, &Enum_t554_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enum_t554)/* instance_size */
	, sizeof (Enum_t554)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Enum_t554_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 49/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array
#include "mscorlib_System_Array.h"
// Metadata Definition System.Array
extern TypeInfo Array_t_il2cpp_TypeInfo;
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern const Il2CppType InternalEnumerator_1_t3757_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__IEnumerable_GetEnumerator_m24453_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 7109 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5522 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Array_InternalArray__ICollection_Contains_m24456_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__ICollection_Contains_m24456_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4738 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Array_InternalArray__IndexOf_m24459_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__IndexOf_m24459_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4742 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Array_InternalArray__set_Item_m24461_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Array_InternalArray__set_Item_m24461_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4744 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24465_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5523 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24466_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5524 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24467_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5525 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24468_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5526 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24469_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5527 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24470_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5528 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24471_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5529 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TKeyU5BU5D_t3761_0_0_0;
extern const Il2CppRGCTXDefinition Array_Sort_m24472_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5530 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4774 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5531 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24473_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5532 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Sort_m24474_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5533 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_qsort_m24475_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5534 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5535 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5536 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType IComparer_1_t3762_0_0_0;
extern const Il2CppType Array_compare_m24476_gp_0_0_0_0;
extern const Il2CppType IComparable_1_t3764_0_0_0;
extern const Il2CppRGCTXDefinition Array_compare_m24476_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4791 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4790 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7110 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, 4790 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_qsort_m24477_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5537 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5538 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5539 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Resize_m24480_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5540 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TU5BU5D_t3765_0_0_0;
extern const Il2CppRGCTXDefinition Array_Resize_m24481_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4805 }/* Array */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_TrueForAll_m24482_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5541 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_ForEach_m24483_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5542 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TOutputU5BU5D_t3766_0_0_0;
extern const Il2CppRGCTXDefinition Array_ConvertAll_m24484_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4817 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5543 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLastIndex_m24485_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5544 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLastIndex_m24486_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5545 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLastIndex_m24487_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5546 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindIndex_m24488_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5547 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindIndex_m24489_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5548 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindIndex_m24490_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5549 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_BinarySearch_m24491_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5550 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_BinarySearch_m24492_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5551 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_BinarySearch_m24493_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5552 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Comparer_1_t3767_0_0_0;
extern const Il2CppType IComparer_1_t3768_0_0_0;
extern const Il2CppRGCTXDefinition Array_BinarySearch_m24494_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5553 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7111 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4845 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_IndexOf_m24495_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5554 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_IndexOf_m24496_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5555 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType EqualityComparer_1_t3769_0_0_0;
extern const Il2CppRGCTXDefinition Array_IndexOf_m24497_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5556 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7112 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5557 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_LastIndexOf_m24498_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5558 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_LastIndexOf_m24499_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5559 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType EqualityComparer_1_t3770_0_0_0;
extern const Il2CppRGCTXDefinition Array_LastIndexOf_m24500_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5560 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7113 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5561 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType TU5BU5D_t3771_0_0_0;
extern const Il2CppRGCTXDefinition Array_FindAll_m24501_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4858 }/* Array */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5562 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5563 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Exists_m24502_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5564 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ArrayReadOnlyList_1_t3772_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t3773_0_0_0;
extern const Il2CppRGCTXDefinition Array_AsReadOnly_m24503_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 7114 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5565 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4866 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5566 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_Find_m24504_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5567 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Array_FindLast_m24505_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5568 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType InternalEnumerator_1_t3435_0_0_0;
extern const Il2CppType SimpleEnumerator_t1546_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t3436_0_0_0;
extern const Il2CppType Swapper_t1547_0_0_0;
static const Il2CppType* Array_t_il2cpp_TypeInfo__nestedTypes[4] =
{
	&InternalEnumerator_1_t3435_0_0_0,
	&SimpleEnumerator_t1546_0_0_0,
	&ArrayReadOnlyList_1_t3436_0_0_0,
	&Swapper_t1547_0_0_0,
};
static const EncodedMethodIndex Array_t_VTable[21] = 
{
	626,
	601,
	627,
	628,
	2582,
	2583,
	2584,
	2585,
	2586,
	2587,
	2588,
	2589,
	2590,
	2591,
	2592,
	2593,
	2594,
	2595,
	2596,
	2597,
	2598,
};
extern const Il2CppType ICollection_t1528_0_0_0;
extern const Il2CppType IList_t1487_0_0_0;
static const Il2CppType* Array_t_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICloneable_t3433_0_0_0,
	&ICollection_t1528_0_0_0,
	&IList_t1487_0_0_0,
};
static Il2CppInterfaceOffsetPair Array_t_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Array_t_0_0_0;
extern const Il2CppType Array_t_1_0_0;
struct Array_t;
const Il2CppTypeDefinitionMetadata Array_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Array_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Array_t_InterfacesTypeInfos/* implementedInterfaces */
	, Array_t_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Array_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 8872/* methodStart */
	, -1/* eventStart */
	, 1677/* propertyStart */

};
TypeInfo Array_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Array"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Array_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2542/* custom_attributes_cache */
	, &Array_t_0_0_0/* byval_arg */
	, &Array_t_1_0_0/* this_arg */
	, &Array_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Array_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 152/* method_count */
	, 9/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 21/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Array/InternalEnumerator`1
extern TypeInfo InternalEnumerator_1_t3435_il2cpp_TypeInfo;
static const EncodedMethodIndex InternalEnumerator_1_t3435_VTable[8] = 
{
	652,
	601,
	653,
	654,
	2599,
	2600,
	2601,
	2602,
};
extern const Il2CppType IEnumerator_1_t3774_0_0_0;
static const Il2CppType* InternalEnumerator_1_t3435_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3774_0_0_0,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3435_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3774_0_0_0, 7},
};
extern const Il2CppType InternalEnumerator_1_t3435_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition InternalEnumerator_1_t3435_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5569 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4873 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5570 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InternalEnumerator_1_t3435_1_0_0;
const Il2CppTypeDefinitionMetadata InternalEnumerator_1_t3435_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, InternalEnumerator_1_t3435_InterfacesTypeInfos/* implementedInterfaces */
	, InternalEnumerator_1_t3435_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, InternalEnumerator_1_t3435_VTable/* vtableMethods */
	, InternalEnumerator_1_t3435_RGCTXData/* rgctxDefinition */
	, 6469/* fieldStart */
	, 9024/* methodStart */
	, -1/* eventStart */
	, 1686/* propertyStart */

};
TypeInfo InternalEnumerator_1_t3435_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InternalEnumerator_1_t3435_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InternalEnumerator_1_t3435_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3435_1_0_0/* this_arg */
	, &InternalEnumerator_1_t3435_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 136/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array/SimpleEnumerator
#include "mscorlib_System_Array_SimpleEnumerator.h"
// Metadata Definition System.Array/SimpleEnumerator
extern TypeInfo SimpleEnumerator_t1546_il2cpp_TypeInfo;
// System.Array/SimpleEnumerator
#include "mscorlib_System_Array_SimpleEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex SimpleEnumerator_t1546_VTable[6] = 
{
	626,
	601,
	627,
	628,
	2603,
	2604,
};
static const Il2CppType* SimpleEnumerator_t1546_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair SimpleEnumerator_t1546_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SimpleEnumerator_t1546_1_0_0;
struct SimpleEnumerator_t1546;
const Il2CppTypeDefinitionMetadata SimpleEnumerator_t1546_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, SimpleEnumerator_t1546_InterfacesTypeInfos/* implementedInterfaces */
	, SimpleEnumerator_t1546_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleEnumerator_t1546_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6471/* fieldStart */
	, 9029/* methodStart */
	, -1/* eventStart */
	, 1688/* propertyStart */

};
TypeInfo SimpleEnumerator_t1546_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SimpleEnumerator_t1546_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleEnumerator_t1546_0_0_0/* byval_arg */
	, &SimpleEnumerator_t1546_1_0_0/* this_arg */
	, &SimpleEnumerator_t1546_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleEnumerator_t1546)/* instance_size */
	, sizeof (SimpleEnumerator_t1546)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Array/ArrayReadOnlyList`1
extern TypeInfo ArrayReadOnlyList_1_t3436_il2cpp_TypeInfo;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3437_0_0_0;
static const Il2CppType* ArrayReadOnlyList_1_t3436_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CGetEnumeratorU3Ec__Iterator0_t3437_0_0_0,
};
static const EncodedMethodIndex ArrayReadOnlyList_1_t3436_VTable[18] = 
{
	626,
	601,
	627,
	628,
	2605,
	2606,
	2607,
	2608,
	2609,
	2610,
	2611,
	2612,
	2613,
	2614,
	2615,
	2616,
	2617,
	2618,
};
extern const Il2CppType IList_1_t3776_0_0_0;
extern const Il2CppType ICollection_1_t3777_0_0_0;
extern const Il2CppType IEnumerable_1_t3778_0_0_0;
static const Il2CppType* ArrayReadOnlyList_1_t3436_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&IList_1_t3776_0_0_0,
	&ICollection_1_t3777_0_0_0,
	&IEnumerable_1_t3778_0_0_0,
};
static Il2CppInterfaceOffsetPair ArrayReadOnlyList_1_t3436_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &IList_1_t3776_0_0_0, 5},
	{ &ICollection_1_t3777_0_0_0, 10},
	{ &IEnumerable_1_t3778_0_0_0, 17},
};
extern const Il2CppType ArrayReadOnlyList_1_t3779_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3780_0_0_0;
extern const Il2CppRGCTXDefinition ArrayReadOnlyList_1_t3436_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5571 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5572 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7125 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5573 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 7126 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5574 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayReadOnlyList_1_t3436_1_0_0;
struct ArrayReadOnlyList_1_t3436;
const Il2CppTypeDefinitionMetadata ArrayReadOnlyList_1_t3436_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, ArrayReadOnlyList_1_t3436_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ArrayReadOnlyList_1_t3436_InterfacesTypeInfos/* implementedInterfaces */
	, ArrayReadOnlyList_1_t3436_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArrayReadOnlyList_1_t3436_VTable/* vtableMethods */
	, ArrayReadOnlyList_1_t3436_RGCTXData/* rgctxDefinition */
	, 6474/* fieldStart */
	, 9032/* methodStart */
	, -1/* eventStart */
	, 1689/* propertyStart */

};
TypeInfo ArrayReadOnlyList_1_t3436_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayReadOnlyList`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ArrayReadOnlyList_1_t3436_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2605/* custom_attributes_cache */
	, &ArrayReadOnlyList_1_t3436_0_0_0/* byval_arg */
	, &ArrayReadOnlyList_1_t3436_1_0_0/* this_arg */
	, &ArrayReadOnlyList_1_t3436_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 137/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0
extern TypeInfo U3CGetEnumeratorU3Ec__Iterator0_t3437_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CGetEnumeratorU3Ec__Iterator0_t3437_VTable[8] = 
{
	626,
	601,
	627,
	628,
	2619,
	2620,
	2621,
	2622,
};
extern const Il2CppType IEnumerator_1_t3781_0_0_0;
static const Il2CppType* U3CGetEnumeratorU3Ec__Iterator0_t3437_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerator_1_t3781_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CGetEnumeratorU3Ec__Iterator0_t3437_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerator_1_t3781_0_0_0, 7},
};
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3437_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition U3CGetEnumeratorU3Ec__Iterator0_t3437_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4879 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3437_1_0_0;
struct U3CGetEnumeratorU3Ec__Iterator0_t3437;
const Il2CppTypeDefinitionMetadata U3CGetEnumeratorU3Ec__Iterator0_t3437_DefinitionMetadata = 
{
	&ArrayReadOnlyList_1_t3436_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CGetEnumeratorU3Ec__Iterator0_t3437_InterfacesTypeInfos/* implementedInterfaces */
	, U3CGetEnumeratorU3Ec__Iterator0_t3437_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetEnumeratorU3Ec__Iterator0_t3437_VTable/* vtableMethods */
	, U3CGetEnumeratorU3Ec__Iterator0_t3437_RGCTXData/* rgctxDefinition */
	, 6475/* fieldStart */
	, 9048/* methodStart */
	, -1/* eventStart */
	, 1692/* propertyStart */

};
TypeInfo U3CGetEnumeratorU3Ec__Iterator0_t3437_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetEnumerator>c__Iterator0"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetEnumeratorU3Ec__Iterator0_t3437_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2607/* custom_attributes_cache */
	, &U3CGetEnumeratorU3Ec__Iterator0_t3437_0_0_0/* byval_arg */
	, &U3CGetEnumeratorU3Ec__Iterator0_t3437_1_0_0/* this_arg */
	, &U3CGetEnumeratorU3Ec__Iterator0_t3437_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 138/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array/Swapper
#include "mscorlib_System_Array_Swapper.h"
// Metadata Definition System.Array/Swapper
extern TypeInfo Swapper_t1547_il2cpp_TypeInfo;
// System.Array/Swapper
#include "mscorlib_System_Array_SwapperMethodDeclarations.h"
static const EncodedMethodIndex Swapper_t1547_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	2623,
	2624,
	2625,
};
static Il2CppInterfaceOffsetPair Swapper_t1547_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Swapper_t1547_1_0_0;
struct Swapper_t1547;
const Il2CppTypeDefinitionMetadata Swapper_t1547_DefinitionMetadata = 
{
	&Array_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Swapper_t1547_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Swapper_t1547_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9053/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Swapper_t1547_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Swapper"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Swapper_t1547_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Swapper_t1547_0_0_0/* byval_arg */
	, &Swapper_t1547_1_0_0/* this_arg */
	, &Swapper_t1547_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_Swapper_t1547/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Swapper_t1547)/* instance_size */
	, sizeof (Swapper_t1547)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Collections.ICollection
extern TypeInfo ICollection_t1528_il2cpp_TypeInfo;
static const Il2CppType* ICollection_t1528_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICollection_t1528_1_0_0;
struct ICollection_t1528;
const Il2CppTypeDefinitionMetadata ICollection_t1528_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ICollection_t1528_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9057/* methodStart */
	, -1/* eventStart */
	, 1694/* propertyStart */

};
TypeInfo ICollection_t1528_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &ICollection_t1528_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2611/* custom_attributes_cache */
	, &ICollection_t1528_0_0_0/* byval_arg */
	, &ICollection_t1528_1_0_0/* this_arg */
	, &ICollection_t1528_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IList
extern TypeInfo IList_t1487_il2cpp_TypeInfo;
static const Il2CppType* IList_t1487_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IList_t1487_1_0_0;
struct IList_t1487;
const Il2CppTypeDefinitionMetadata IList_t1487_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IList_t1487_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9061/* methodStart */
	, -1/* eventStart */
	, 1697/* propertyStart */

};
TypeInfo IList_t1487_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IList_t1487_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2612/* custom_attributes_cache */
	, &IList_t1487_0_0_0/* byval_arg */
	, &IList_t1487_1_0_0/* this_arg */
	, &IList_t1487_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.IList`1
extern TypeInfo IList_1_t3438_il2cpp_TypeInfo;
extern const Il2CppType ICollection_1_t3783_0_0_0;
extern const Il2CppType IEnumerable_1_t3784_0_0_0;
static const Il2CppType* IList_1_t3438_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_1_t3783_0_0_0,
	&IEnumerable_1_t3784_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IList_1_t3438_0_0_0;
extern const Il2CppType IList_1_t3438_1_0_0;
struct IList_1_t3438;
const Il2CppTypeDefinitionMetadata IList_1_t3438_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IList_1_t3438_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9072/* methodStart */
	, -1/* eventStart */
	, 1700/* propertyStart */

};
TypeInfo IList_1_t3438_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &IList_1_t3438_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2613/* custom_attributes_cache */
	, &IList_1_t3438_0_0_0/* byval_arg */
	, &IList_1_t3438_1_0_0/* this_arg */
	, &IList_1_t3438_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 139/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.ICollection`1
extern TypeInfo ICollection_1_t3439_il2cpp_TypeInfo;
extern const Il2CppType IEnumerable_1_t3785_0_0_0;
static const Il2CppType* ICollection_1_t3439_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&IEnumerable_1_t3785_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICollection_1_t3439_0_0_0;
extern const Il2CppType ICollection_1_t3439_1_0_0;
struct ICollection_1_t3439;
const Il2CppTypeDefinitionMetadata ICollection_1_t3439_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ICollection_1_t3439_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9077/* methodStart */
	, -1/* eventStart */
	, 1701/* propertyStart */

};
TypeInfo ICollection_1_t3439_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, NULL/* methods */
	, &ICollection_1_t3439_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICollection_1_t3439_0_0_0/* byval_arg */
	, &ICollection_1_t3439_1_0_0/* this_arg */
	, &ICollection_1_t3439_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 140/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Void
#include "mscorlib_System_Void.h"
// Metadata Definition System.Void
extern TypeInfo Void_t1548_il2cpp_TypeInfo;
// System.Void
#include "mscorlib_System_VoidMethodDeclarations.h"
static const EncodedMethodIndex Void_t1548_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Void_t1548_0_0_0;
extern const Il2CppType Void_t1548_1_0_0;
const Il2CppTypeDefinitionMetadata Void_t1548_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Void_t1548_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Void_t1548_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Void"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Void_t1548_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2614/* custom_attributes_cache */
	, &Void_t1548_0_0_0/* byval_arg */
	, &Void_t1548_1_0_0/* this_arg */
	, &Void_t1548_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Void_t1548)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Void_t1548)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, 1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Type
#include "mscorlib_System_Type.h"
// Metadata Definition System.Type
extern TypeInfo Type_t_il2cpp_TypeInfo;
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
static const EncodedMethodIndex Type_t_VTable[81] = 
{
	2626,
	601,
	2627,
	2628,
	2629,
	2630,
	2631,
	2632,
	0,
	2633,
	0,
	0,
	0,
	0,
	0,
	0,
	2634,
	0,
	0,
	2635,
	2636,
	2637,
	2638,
	2639,
	2640,
	2641,
	2642,
	2643,
	2644,
	2645,
	2646,
	2647,
	2648,
	2649,
	0,
	2650,
	0,
	2651,
	2652,
	0,
	2653,
	2654,
	0,
	0,
	0,
	0,
	2655,
	2656,
	2657,
	2658,
	0,
	0,
	0,
	2659,
	2660,
	2661,
	2662,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	2663,
	2664,
	2665,
	2666,
	2667,
	2668,
	2669,
	0,
	0,
	2670,
	2671,
	2672,
	2673,
	2674,
	2675,
	2676,
};
extern const Il2CppType IReflect_t3442_0_0_0;
extern const Il2CppType _Type_t3440_0_0_0;
static const Il2CppType* Type_t_InterfacesTypeInfos[] = 
{
	&IReflect_t3442_0_0_0,
	&_Type_t3440_0_0_0,
};
extern const Il2CppType ICustomAttributeProvider_t2188_0_0_0;
extern const Il2CppType _MemberInfo_t3441_0_0_0;
static Il2CppInterfaceOffsetPair Type_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &IReflect_t3442_0_0_0, 14},
	{ &_Type_t3440_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_1_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
struct Type_t;
const Il2CppTypeDefinitionMetadata Type_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Type_t_InterfacesTypeInfos/* implementedInterfaces */
	, Type_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, Type_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6479/* fieldStart */
	, 9084/* methodStart */
	, -1/* eventStart */
	, 1703/* propertyStart */

};
TypeInfo Type_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Type"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Type_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2615/* custom_attributes_cache */
	, &Type_t_0_0_0/* byval_arg */
	, &Type_t_1_0_0/* this_arg */
	, &Type_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Type_t)/* instance_size */
	, sizeof (Type_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Type_t_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 96/* method_count */
	, 33/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 81/* vtable_count */
	, 2/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// Metadata Definition System.Reflection.MemberInfo
extern TypeInfo MemberInfo_t_il2cpp_TypeInfo;
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
static const EncodedMethodIndex MemberInfo_t_VTable[14] = 
{
	626,
	601,
	627,
	628,
	2629,
	2630,
	0,
	0,
	0,
	0,
	2677,
	0,
	0,
	0,
};
static const Il2CppType* MemberInfo_t_InterfacesTypeInfos[] = 
{
	&ICustomAttributeProvider_t2188_0_0_0,
	&_MemberInfo_t3441_0_0_0,
};
static Il2CppInterfaceOffsetPair MemberInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberInfo_t_1_0_0;
struct MemberInfo_t;
const Il2CppTypeDefinitionMetadata MemberInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MemberInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, MemberInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MemberInfo_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9180/* methodStart */
	, -1/* eventStart */
	, 1736/* propertyStart */

};
TypeInfo MemberInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MemberInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2623/* custom_attributes_cache */
	, &MemberInfo_t_0_0_0/* byval_arg */
	, &MemberInfo_t_1_0_0/* this_arg */
	, &MemberInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberInfo_t)/* instance_size */
	, sizeof (MemberInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.ICustomAttributeProvider
extern TypeInfo ICustomAttributeProvider_t2188_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICustomAttributeProvider_t2188_1_0_0;
struct ICustomAttributeProvider_t2188;
const Il2CppTypeDefinitionMetadata ICustomAttributeProvider_t2188_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9189/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICustomAttributeProvider_t2188_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICustomAttributeProvider"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &ICustomAttributeProvider_t2188_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2624/* custom_attributes_cache */
	, &ICustomAttributeProvider_t2188_0_0_0/* byval_arg */
	, &ICustomAttributeProvider_t2188_1_0_0/* this_arg */
	, &ICustomAttributeProvider_t2188_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MemberInfo
extern TypeInfo _MemberInfo_t3441_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MemberInfo_t3441_1_0_0;
struct _MemberInfo_t3441;
const Il2CppTypeDefinitionMetadata _MemberInfo_t3441_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MemberInfo_t3441_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MemberInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MemberInfo_t3441_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2625/* custom_attributes_cache */
	, &_MemberInfo_t3441_0_0_0/* byval_arg */
	, &_MemberInfo_t3441_1_0_0/* this_arg */
	, &_MemberInfo_t3441_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Reflection.IReflect
extern TypeInfo IReflect_t3442_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IReflect_t3442_1_0_0;
struct IReflect_t3442;
const Il2CppTypeDefinitionMetadata IReflect_t3442_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IReflect_t3442_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IReflect"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &IReflect_t3442_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2626/* custom_attributes_cache */
	, &IReflect_t3442_0_0_0/* byval_arg */
	, &IReflect_t3442_1_0_0/* this_arg */
	, &IReflect_t3442_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Type
extern TypeInfo _Type_t3440_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Type_t3440_1_0_0;
struct _Type_t3440;
const Il2CppTypeDefinitionMetadata _Type_t3440_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Type_t3440_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Type"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Type_t3440_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2627/* custom_attributes_cache */
	, &_Type_t3440_0_0_0/* byval_arg */
	, &_Type_t3440_1_0_0/* this_arg */
	, &_Type_t3440_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Exception
#include "mscorlib_System_Exception.h"
// Metadata Definition System.Exception
extern TypeInfo Exception_t520_il2cpp_TypeInfo;
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
static const EncodedMethodIndex Exception_t520_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType _Exception_t3443_0_0_0;
static const Il2CppType* Exception_t520_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&_Exception_t3443_0_0_0,
};
static Il2CppInterfaceOffsetPair Exception_t520_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Exception_t520_0_0_0;
extern const Il2CppType Exception_t520_1_0_0;
struct Exception_t520;
const Il2CppTypeDefinitionMetadata Exception_t520_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Exception_t520_InterfacesTypeInfos/* implementedInterfaces */
	, Exception_t520_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Exception_t520_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6487/* fieldStart */
	, 9191/* methodStart */
	, -1/* eventStart */
	, 1741/* propertyStart */

};
TypeInfo Exception_t520_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Exception"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Exception_t520_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2628/* custom_attributes_cache */
	, &Exception_t520_0_0_0/* byval_arg */
	, &Exception_t520_1_0_0/* this_arg */
	, &Exception_t520_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Exception_t520)/* instance_size */
	, sizeof (Exception_t520)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 6/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Exception
extern TypeInfo _Exception_t3443_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Exception_t3443_1_0_0;
struct _Exception_t3443;
const Il2CppTypeDefinitionMetadata _Exception_t3443_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Exception_t3443_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Exception"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Exception_t3443_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2629/* custom_attributes_cache */
	, &_Exception_t3443_0_0_0/* byval_arg */
	, &_Exception_t3443_1_0_0/* this_arg */
	, &_Exception_t3443_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// Metadata Definition System.RuntimeFieldHandle
extern TypeInfo RuntimeFieldHandle_t1551_il2cpp_TypeInfo;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeFieldHandle_t1551_VTable[5] = 
{
	2678,
	601,
	2679,
	654,
	2680,
};
static const Il2CppType* RuntimeFieldHandle_t1551_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeFieldHandle_t1551_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeFieldHandle_t1551_0_0_0;
extern const Il2CppType RuntimeFieldHandle_t1551_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeFieldHandle_t1551_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeFieldHandle_t1551_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeFieldHandle_t1551_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RuntimeFieldHandle_t1551_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6498/* fieldStart */
	, 9205/* methodStart */
	, -1/* eventStart */
	, 1747/* propertyStart */

};
TypeInfo RuntimeFieldHandle_t1551_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeFieldHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeFieldHandle_t1551_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2630/* custom_attributes_cache */
	, &RuntimeFieldHandle_t1551_0_0_0/* byval_arg */
	, &RuntimeFieldHandle_t1551_1_0_0/* this_arg */
	, &RuntimeFieldHandle_t1551_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeFieldHandle_t1551)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeFieldHandle_t1551)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeFieldHandle_t1551 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// Metadata Definition System.RuntimeTypeHandle
extern TypeInfo RuntimeTypeHandle_t1550_il2cpp_TypeInfo;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeTypeHandle_t1550_VTable[5] = 
{
	2681,
	601,
	2682,
	654,
	2683,
};
static const Il2CppType* RuntimeTypeHandle_t1550_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeTypeHandle_t1550_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeTypeHandle_t1550_0_0_0;
extern const Il2CppType RuntimeTypeHandle_t1550_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeTypeHandle_t1550_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeTypeHandle_t1550_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeTypeHandle_t1550_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RuntimeTypeHandle_t1550_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6499/* fieldStart */
	, 9210/* methodStart */
	, -1/* eventStart */
	, 1748/* propertyStart */

};
TypeInfo RuntimeTypeHandle_t1550_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeTypeHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeTypeHandle_t1550_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2632/* custom_attributes_cache */
	, &RuntimeTypeHandle_t1550_0_0_0/* byval_arg */
	, &RuntimeTypeHandle_t1550_1_0_0/* this_arg */
	, &RuntimeTypeHandle_t1550_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeTypeHandle_t1550)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeTypeHandle_t1550)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeTypeHandle_t1550 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// Metadata Definition System.ParamArrayAttribute
extern TypeInfo ParamArrayAttribute_t1552_il2cpp_TypeInfo;
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
static const EncodedMethodIndex ParamArrayAttribute_t1552_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ParamArrayAttribute_t1552_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParamArrayAttribute_t1552_0_0_0;
extern const Il2CppType ParamArrayAttribute_t1552_1_0_0;
struct ParamArrayAttribute_t1552;
const Il2CppTypeDefinitionMetadata ParamArrayAttribute_t1552_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ParamArrayAttribute_t1552_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ParamArrayAttribute_t1552_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9215/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ParamArrayAttribute_t1552_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParamArrayAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ParamArrayAttribute_t1552_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2634/* custom_attributes_cache */
	, &ParamArrayAttribute_t1552_0_0_0/* byval_arg */
	, &ParamArrayAttribute_t1552_1_0_0/* this_arg */
	, &ParamArrayAttribute_t1552_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParamArrayAttribute_t1552)/* instance_size */
	, sizeof (ParamArrayAttribute_t1552)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.OutAttribute
#include "mscorlib_System_Runtime_InteropServices_OutAttribute.h"
// Metadata Definition System.Runtime.InteropServices.OutAttribute
extern TypeInfo OutAttribute_t1553_il2cpp_TypeInfo;
// System.Runtime.InteropServices.OutAttribute
#include "mscorlib_System_Runtime_InteropServices_OutAttributeMethodDeclarations.h"
static const EncodedMethodIndex OutAttribute_t1553_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair OutAttribute_t1553_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutAttribute_t1553_0_0_0;
extern const Il2CppType OutAttribute_t1553_1_0_0;
struct OutAttribute_t1553;
const Il2CppTypeDefinitionMetadata OutAttribute_t1553_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutAttribute_t1553_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, OutAttribute_t1553_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9216/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OutAttribute_t1553_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &OutAttribute_t1553_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2635/* custom_attributes_cache */
	, &OutAttribute_t1553_0_0_0/* byval_arg */
	, &OutAttribute_t1553_1_0_0/* this_arg */
	, &OutAttribute_t1553_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutAttribute_t1553)/* instance_size */
	, sizeof (OutAttribute_t1553)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// Metadata Definition System.ObsoleteAttribute
extern TypeInfo ObsoleteAttribute_t1554_il2cpp_TypeInfo;
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
static const EncodedMethodIndex ObsoleteAttribute_t1554_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ObsoleteAttribute_t1554_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObsoleteAttribute_t1554_0_0_0;
extern const Il2CppType ObsoleteAttribute_t1554_1_0_0;
struct ObsoleteAttribute_t1554;
const Il2CppTypeDefinitionMetadata ObsoleteAttribute_t1554_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObsoleteAttribute_t1554_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ObsoleteAttribute_t1554_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6500/* fieldStart */
	, 9217/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObsoleteAttribute_t1554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObsoleteAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ObsoleteAttribute_t1554_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2636/* custom_attributes_cache */
	, &ObsoleteAttribute_t1554_0_0_0/* byval_arg */
	, &ObsoleteAttribute_t1554_1_0_0/* this_arg */
	, &ObsoleteAttribute_t1554_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObsoleteAttribute_t1554)/* instance_size */
	, sizeof (ObsoleteAttribute_t1554)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.DllImportAttribute
#include "mscorlib_System_Runtime_InteropServices_DllImportAttribute.h"
// Metadata Definition System.Runtime.InteropServices.DllImportAttribute
extern TypeInfo DllImportAttribute_t1555_il2cpp_TypeInfo;
// System.Runtime.InteropServices.DllImportAttribute
#include "mscorlib_System_Runtime_InteropServices_DllImportAttributeMethodDeclarations.h"
static const EncodedMethodIndex DllImportAttribute_t1555_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DllImportAttribute_t1555_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DllImportAttribute_t1555_0_0_0;
extern const Il2CppType DllImportAttribute_t1555_1_0_0;
struct DllImportAttribute_t1555;
const Il2CppTypeDefinitionMetadata DllImportAttribute_t1555_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DllImportAttribute_t1555_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DllImportAttribute_t1555_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6502/* fieldStart */
	, 9220/* methodStart */
	, -1/* eventStart */
	, 1749/* propertyStart */

};
TypeInfo DllImportAttribute_t1555_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DllImportAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &DllImportAttribute_t1555_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2637/* custom_attributes_cache */
	, &DllImportAttribute_t1555_0_0_0/* byval_arg */
	, &DllImportAttribute_t1555_1_0_0/* this_arg */
	, &DllImportAttribute_t1555_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DllImportAttribute_t1555)/* instance_size */
	, sizeof (DllImportAttribute_t1555)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.MarshalAsAttribute
#include "mscorlib_System_Runtime_InteropServices_MarshalAsAttribute.h"
// Metadata Definition System.Runtime.InteropServices.MarshalAsAttribute
extern TypeInfo MarshalAsAttribute_t1556_il2cpp_TypeInfo;
// System.Runtime.InteropServices.MarshalAsAttribute
#include "mscorlib_System_Runtime_InteropServices_MarshalAsAttributeMethodDeclarations.h"
static const EncodedMethodIndex MarshalAsAttribute_t1556_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair MarshalAsAttribute_t1556_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalAsAttribute_t1556_0_0_0;
extern const Il2CppType MarshalAsAttribute_t1556_1_0_0;
struct MarshalAsAttribute_t1556;
const Il2CppTypeDefinitionMetadata MarshalAsAttribute_t1556_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarshalAsAttribute_t1556_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, MarshalAsAttribute_t1556_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6511/* fieldStart */
	, 9222/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MarshalAsAttribute_t1556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalAsAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &MarshalAsAttribute_t1556_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2638/* custom_attributes_cache */
	, &MarshalAsAttribute_t1556_0_0_0/* byval_arg */
	, &MarshalAsAttribute_t1556_1_0_0/* this_arg */
	, &MarshalAsAttribute_t1556_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalAsAttribute_t1556)/* instance_size */
	, sizeof (MarshalAsAttribute_t1556)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.InAttribute
#include "mscorlib_System_Runtime_InteropServices_InAttribute.h"
// Metadata Definition System.Runtime.InteropServices.InAttribute
extern TypeInfo InAttribute_t1557_il2cpp_TypeInfo;
// System.Runtime.InteropServices.InAttribute
#include "mscorlib_System_Runtime_InteropServices_InAttributeMethodDeclarations.h"
static const EncodedMethodIndex InAttribute_t1557_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair InAttribute_t1557_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InAttribute_t1557_0_0_0;
extern const Il2CppType InAttribute_t1557_1_0_0;
struct InAttribute_t1557;
const Il2CppTypeDefinitionMetadata InAttribute_t1557_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InAttribute_t1557_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, InAttribute_t1557_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9223/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InAttribute_t1557_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &InAttribute_t1557_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2641/* custom_attributes_cache */
	, &InAttribute_t1557_0_0_0/* byval_arg */
	, &InAttribute_t1557_1_0_0/* this_arg */
	, &InAttribute_t1557_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InAttribute_t1557)/* instance_size */
	, sizeof (InAttribute_t1557)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttribute.h"
// Metadata Definition System.Diagnostics.ConditionalAttribute
extern TypeInfo ConditionalAttribute_t1558_il2cpp_TypeInfo;
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttributeMethodDeclarations.h"
static const EncodedMethodIndex ConditionalAttribute_t1558_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ConditionalAttribute_t1558_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConditionalAttribute_t1558_0_0_0;
extern const Il2CppType ConditionalAttribute_t1558_1_0_0;
struct ConditionalAttribute_t1558;
const Il2CppTypeDefinitionMetadata ConditionalAttribute_t1558_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConditionalAttribute_t1558_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ConditionalAttribute_t1558_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6518/* fieldStart */
	, 9224/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConditionalAttribute_t1558_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConditionalAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &ConditionalAttribute_t1558_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2642/* custom_attributes_cache */
	, &ConditionalAttribute_t1558_0_0_0/* byval_arg */
	, &ConditionalAttribute_t1558_1_0_0/* this_arg */
	, &ConditionalAttribute_t1558_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConditionalAttribute_t1558)/* instance_size */
	, sizeof (ConditionalAttribute_t1558)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// Metadata Definition System.Runtime.InteropServices.GuidAttribute
extern TypeInfo GuidAttribute_t1559_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
static const EncodedMethodIndex GuidAttribute_t1559_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair GuidAttribute_t1559_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GuidAttribute_t1559_0_0_0;
extern const Il2CppType GuidAttribute_t1559_1_0_0;
struct GuidAttribute_t1559;
const Il2CppTypeDefinitionMetadata GuidAttribute_t1559_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GuidAttribute_t1559_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, GuidAttribute_t1559_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6519/* fieldStart */
	, 9225/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GuidAttribute_t1559_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GuidAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &GuidAttribute_t1559_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2643/* custom_attributes_cache */
	, &GuidAttribute_t1559_0_0_0/* byval_arg */
	, &GuidAttribute_t1559_1_0_0/* this_arg */
	, &GuidAttribute_t1559_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GuidAttribute_t1559)/* instance_size */
	, sizeof (GuidAttribute_t1559)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComImportAttribute
#include "mscorlib_System_Runtime_InteropServices_ComImportAttribute.h"
// Metadata Definition System.Runtime.InteropServices.ComImportAttribute
extern TypeInfo ComImportAttribute_t1560_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComImportAttribute
#include "mscorlib_System_Runtime_InteropServices_ComImportAttributeMethodDeclarations.h"
static const EncodedMethodIndex ComImportAttribute_t1560_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ComImportAttribute_t1560_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComImportAttribute_t1560_0_0_0;
extern const Il2CppType ComImportAttribute_t1560_1_0_0;
struct ComImportAttribute_t1560;
const Il2CppTypeDefinitionMetadata ComImportAttribute_t1560_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComImportAttribute_t1560_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ComImportAttribute_t1560_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9226/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ComImportAttribute_t1560_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComImportAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &ComImportAttribute_t1560_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2644/* custom_attributes_cache */
	, &ComImportAttribute_t1560_0_0_0/* byval_arg */
	, &ComImportAttribute_t1560_1_0_0/* this_arg */
	, &ComImportAttribute_t1560_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComImportAttribute_t1560)/* instance_size */
	, sizeof (ComImportAttribute_t1560)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.OptionalAttribute
#include "mscorlib_System_Runtime_InteropServices_OptionalAttribute.h"
// Metadata Definition System.Runtime.InteropServices.OptionalAttribute
extern TypeInfo OptionalAttribute_t1561_il2cpp_TypeInfo;
// System.Runtime.InteropServices.OptionalAttribute
#include "mscorlib_System_Runtime_InteropServices_OptionalAttributeMethodDeclarations.h"
static const EncodedMethodIndex OptionalAttribute_t1561_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair OptionalAttribute_t1561_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OptionalAttribute_t1561_0_0_0;
extern const Il2CppType OptionalAttribute_t1561_1_0_0;
struct OptionalAttribute_t1561;
const Il2CppTypeDefinitionMetadata OptionalAttribute_t1561_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OptionalAttribute_t1561_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, OptionalAttribute_t1561_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9227/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OptionalAttribute_t1561_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OptionalAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &OptionalAttribute_t1561_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2645/* custom_attributes_cache */
	, &OptionalAttribute_t1561_0_0_0/* byval_arg */
	, &OptionalAttribute_t1561_1_0_0/* this_arg */
	, &OptionalAttribute_t1561_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OptionalAttribute_t1561)/* instance_size */
	, sizeof (OptionalAttribute_t1561)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// Metadata Definition System.Runtime.CompilerServices.CompilerGeneratedAttribute
extern TypeInfo CompilerGeneratedAttribute_t1562_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
static const EncodedMethodIndex CompilerGeneratedAttribute_t1562_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair CompilerGeneratedAttribute_t1562_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilerGeneratedAttribute_t1562_0_0_0;
extern const Il2CppType CompilerGeneratedAttribute_t1562_1_0_0;
struct CompilerGeneratedAttribute_t1562;
const Il2CppTypeDefinitionMetadata CompilerGeneratedAttribute_t1562_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilerGeneratedAttribute_t1562_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, CompilerGeneratedAttribute_t1562_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9228/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompilerGeneratedAttribute_t1562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilerGeneratedAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &CompilerGeneratedAttribute_t1562_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2646/* custom_attributes_cache */
	, &CompilerGeneratedAttribute_t1562_0_0_0/* byval_arg */
	, &CompilerGeneratedAttribute_t1562_1_0_0/* this_arg */
	, &CompilerGeneratedAttribute_t1562_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilerGeneratedAttribute_t1562)/* instance_size */
	, sizeof (CompilerGeneratedAttribute_t1562)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// Metadata Definition System.Runtime.CompilerServices.InternalsVisibleToAttribute
extern TypeInfo InternalsVisibleToAttribute_t1563_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
static const EncodedMethodIndex InternalsVisibleToAttribute_t1563_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair InternalsVisibleToAttribute_t1563_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InternalsVisibleToAttribute_t1563_0_0_0;
extern const Il2CppType InternalsVisibleToAttribute_t1563_1_0_0;
struct InternalsVisibleToAttribute_t1563;
const Il2CppTypeDefinitionMetadata InternalsVisibleToAttribute_t1563_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InternalsVisibleToAttribute_t1563_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, InternalsVisibleToAttribute_t1563_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6520/* fieldStart */
	, 9229/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InternalsVisibleToAttribute_t1563_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalsVisibleToAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &InternalsVisibleToAttribute_t1563_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2647/* custom_attributes_cache */
	, &InternalsVisibleToAttribute_t1563_0_0_0/* byval_arg */
	, &InternalsVisibleToAttribute_t1563_1_0_0/* this_arg */
	, &InternalsVisibleToAttribute_t1563_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalsVisibleToAttribute_t1563)/* instance_size */
	, sizeof (InternalsVisibleToAttribute_t1563)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// Metadata Definition System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
extern TypeInfo RuntimeCompatibilityAttribute_t1564_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
static const EncodedMethodIndex RuntimeCompatibilityAttribute_t1564_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair RuntimeCompatibilityAttribute_t1564_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeCompatibilityAttribute_t1564_0_0_0;
extern const Il2CppType RuntimeCompatibilityAttribute_t1564_1_0_0;
struct RuntimeCompatibilityAttribute_t1564;
const Il2CppTypeDefinitionMetadata RuntimeCompatibilityAttribute_t1564_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RuntimeCompatibilityAttribute_t1564_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, RuntimeCompatibilityAttribute_t1564_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6522/* fieldStart */
	, 9230/* methodStart */
	, -1/* eventStart */
	, 1750/* propertyStart */

};
TypeInfo RuntimeCompatibilityAttribute_t1564_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeCompatibilityAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &RuntimeCompatibilityAttribute_t1564_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2648/* custom_attributes_cache */
	, &RuntimeCompatibilityAttribute_t1564_0_0_0/* byval_arg */
	, &RuntimeCompatibilityAttribute_t1564_1_0_0/* this_arg */
	, &RuntimeCompatibilityAttribute_t1564_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeCompatibilityAttribute_t1564)/* instance_size */
	, sizeof (RuntimeCompatibilityAttribute_t1564)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerHiddenAttribute
extern TypeInfo DebuggerHiddenAttribute_t1565_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerHiddenAttribute_t1565_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DebuggerHiddenAttribute_t1565_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerHiddenAttribute_t1565_0_0_0;
extern const Il2CppType DebuggerHiddenAttribute_t1565_1_0_0;
struct DebuggerHiddenAttribute_t1565;
const Il2CppTypeDefinitionMetadata DebuggerHiddenAttribute_t1565_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerHiddenAttribute_t1565_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DebuggerHiddenAttribute_t1565_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9232/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggerHiddenAttribute_t1565_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerHiddenAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerHiddenAttribute_t1565_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2649/* custom_attributes_cache */
	, &DebuggerHiddenAttribute_t1565_0_0_0/* byval_arg */
	, &DebuggerHiddenAttribute_t1565_1_0_0/* this_arg */
	, &DebuggerHiddenAttribute_t1565_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerHiddenAttribute_t1565)/* instance_size */
	, sizeof (DebuggerHiddenAttribute_t1565)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// Metadata Definition System.Reflection.DefaultMemberAttribute
extern TypeInfo DefaultMemberAttribute_t1566_il2cpp_TypeInfo;
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
static const EncodedMethodIndex DefaultMemberAttribute_t1566_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DefaultMemberAttribute_t1566_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultMemberAttribute_t1566_0_0_0;
extern const Il2CppType DefaultMemberAttribute_t1566_1_0_0;
struct DefaultMemberAttribute_t1566;
const Il2CppTypeDefinitionMetadata DefaultMemberAttribute_t1566_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultMemberAttribute_t1566_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DefaultMemberAttribute_t1566_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6523/* fieldStart */
	, 9233/* methodStart */
	, -1/* eventStart */
	, 1751/* propertyStart */

};
TypeInfo DefaultMemberAttribute_t1566_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultMemberAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &DefaultMemberAttribute_t1566_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2650/* custom_attributes_cache */
	, &DefaultMemberAttribute_t1566_0_0_0/* byval_arg */
	, &DefaultMemberAttribute_t1566_1_0_0/* this_arg */
	, &DefaultMemberAttribute_t1566_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultMemberAttribute_t1566)/* instance_size */
	, sizeof (DefaultMemberAttribute_t1566)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttr.h"
// Metadata Definition System.Runtime.CompilerServices.DecimalConstantAttribute
extern TypeInfo DecimalConstantAttribute_t1567_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttrMethodDeclarations.h"
static const EncodedMethodIndex DecimalConstantAttribute_t1567_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DecimalConstantAttribute_t1567_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecimalConstantAttribute_t1567_0_0_0;
extern const Il2CppType DecimalConstantAttribute_t1567_1_0_0;
struct DecimalConstantAttribute_t1567;
const Il2CppTypeDefinitionMetadata DecimalConstantAttribute_t1567_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DecimalConstantAttribute_t1567_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DecimalConstantAttribute_t1567_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6524/* fieldStart */
	, 9235/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DecimalConstantAttribute_t1567_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecimalConstantAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &DecimalConstantAttribute_t1567_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2651/* custom_attributes_cache */
	, &DecimalConstantAttribute_t1567_0_0_0/* byval_arg */
	, &DecimalConstantAttribute_t1567_1_0_0/* this_arg */
	, &DecimalConstantAttribute_t1567_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecimalConstantAttribute_t1567)/* instance_size */
	, sizeof (DecimalConstantAttribute_t1567)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.FieldOffsetAttribute
#include "mscorlib_System_Runtime_InteropServices_FieldOffsetAttribute.h"
// Metadata Definition System.Runtime.InteropServices.FieldOffsetAttribute
extern TypeInfo FieldOffsetAttribute_t1568_il2cpp_TypeInfo;
// System.Runtime.InteropServices.FieldOffsetAttribute
#include "mscorlib_System_Runtime_InteropServices_FieldOffsetAttributeMethodDeclarations.h"
static const EncodedMethodIndex FieldOffsetAttribute_t1568_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair FieldOffsetAttribute_t1568_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldOffsetAttribute_t1568_0_0_0;
extern const Il2CppType FieldOffsetAttribute_t1568_1_0_0;
struct FieldOffsetAttribute_t1568;
const Il2CppTypeDefinitionMetadata FieldOffsetAttribute_t1568_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldOffsetAttribute_t1568_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, FieldOffsetAttribute_t1568_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6529/* fieldStart */
	, 9236/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FieldOffsetAttribute_t1568_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldOffsetAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &FieldOffsetAttribute_t1568_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2653/* custom_attributes_cache */
	, &FieldOffsetAttribute_t1568_0_0_0/* byval_arg */
	, &FieldOffsetAttribute_t1568_1_0_0/* this_arg */
	, &FieldOffsetAttribute_t1568_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldOffsetAttribute_t1568)/* instance_size */
	, sizeof (FieldOffsetAttribute_t1568)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
// Metadata Definition System.RuntimeArgumentHandle
extern TypeInfo RuntimeArgumentHandle_t1569_il2cpp_TypeInfo;
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeArgumentHandle_t1569_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeArgumentHandle_t1569_0_0_0;
extern const Il2CppType RuntimeArgumentHandle_t1569_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeArgumentHandle_t1569_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RuntimeArgumentHandle_t1569_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6530/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimeArgumentHandle_t1569_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeArgumentHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeArgumentHandle_t1569_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2654/* custom_attributes_cache */
	, &RuntimeArgumentHandle_t1569_0_0_0/* byval_arg */
	, &RuntimeArgumentHandle_t1569_1_0_0/* this_arg */
	, &RuntimeArgumentHandle_t1569_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeArgumentHandle_t1569)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeArgumentHandle_t1569)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeArgumentHandle_t1569 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"
// Metadata Definition System.AsyncCallback
extern TypeInfo AsyncCallback_t217_il2cpp_TypeInfo;
// System.AsyncCallback
#include "mscorlib_System_AsyncCallbackMethodDeclarations.h"
static const EncodedMethodIndex AsyncCallback_t217_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	2684,
	2685,
	2686,
};
static Il2CppInterfaceOffsetPair AsyncCallback_t217_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsyncCallback_t217_0_0_0;
extern const Il2CppType AsyncCallback_t217_1_0_0;
struct AsyncCallback_t217;
const Il2CppTypeDefinitionMetadata AsyncCallback_t217_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AsyncCallback_t217_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, AsyncCallback_t217_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9237/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AsyncCallback_t217_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncCallback"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AsyncCallback_t217_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2655/* custom_attributes_cache */
	, &AsyncCallback_t217_0_0_0/* byval_arg */
	, &AsyncCallback_t217_1_0_0/* this_arg */
	, &AsyncCallback_t217_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AsyncCallback_t217/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncCallback_t217)/* instance_size */
	, sizeof (AsyncCallback_t217)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.IAsyncResult
extern TypeInfo IAsyncResult_t216_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IAsyncResult_t216_0_0_0;
extern const Il2CppType IAsyncResult_t216_1_0_0;
struct IAsyncResult_t216;
const Il2CppTypeDefinitionMetadata IAsyncResult_t216_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9241/* methodStart */
	, -1/* eventStart */
	, 1752/* propertyStart */

};
TypeInfo IAsyncResult_t216_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAsyncResult"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IAsyncResult_t216_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2656/* custom_attributes_cache */
	, &IAsyncResult_t216_0_0_0/* byval_arg */
	, &IAsyncResult_t216_1_0_0/* this_arg */
	, &IAsyncResult_t216_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.TypedReference
#include "mscorlib_System_TypedReference.h"
// Metadata Definition System.TypedReference
extern TypeInfo TypedReference_t1570_il2cpp_TypeInfo;
// System.TypedReference
#include "mscorlib_System_TypedReferenceMethodDeclarations.h"
static const EncodedMethodIndex TypedReference_t1570_VTable[4] = 
{
	2687,
	601,
	2688,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypedReference_t1570_0_0_0;
extern const Il2CppType TypedReference_t1570_1_0_0;
const Il2CppTypeDefinitionMetadata TypedReference_t1570_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, TypedReference_t1570_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6531/* fieldStart */
	, 9244/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypedReference_t1570_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypedReference"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TypedReference_t1570_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2657/* custom_attributes_cache */
	, &TypedReference_t1570_0_0_0/* byval_arg */
	, &TypedReference_t1570_1_0_0/* this_arg */
	, &TypedReference_t1570_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypedReference_t1570)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypedReference_t1570)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TypedReference_t1570 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ArgIterator
#include "mscorlib_System_ArgIterator.h"
// Metadata Definition System.ArgIterator
extern TypeInfo ArgIterator_t1571_il2cpp_TypeInfo;
// System.ArgIterator
#include "mscorlib_System_ArgIteratorMethodDeclarations.h"
static const EncodedMethodIndex ArgIterator_t1571_VTable[4] = 
{
	2689,
	601,
	2690,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgIterator_t1571_0_0_0;
extern const Il2CppType ArgIterator_t1571_1_0_0;
const Il2CppTypeDefinitionMetadata ArgIterator_t1571_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, ArgIterator_t1571_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6534/* fieldStart */
	, 9246/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgIterator_t1571_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgIterator"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgIterator_t1571_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgIterator_t1571_0_0_0/* byval_arg */
	, &ArgIterator_t1571_1_0_0/* this_arg */
	, &ArgIterator_t1571_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgIterator_t1571)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgIterator_t1571)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MarshalByRefObject
#include "mscorlib_System_MarshalByRefObject.h"
// Metadata Definition System.MarshalByRefObject
extern TypeInfo MarshalByRefObject_t1415_il2cpp_TypeInfo;
// System.MarshalByRefObject
#include "mscorlib_System_MarshalByRefObjectMethodDeclarations.h"
static const EncodedMethodIndex MarshalByRefObject_t1415_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalByRefObject_t1415_0_0_0;
extern const Il2CppType MarshalByRefObject_t1415_1_0_0;
struct MarshalByRefObject_t1415;
const Il2CppTypeDefinitionMetadata MarshalByRefObject_t1415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MarshalByRefObject_t1415_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6538/* fieldStart */
	, 9248/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MarshalByRefObject_t1415_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalByRefObject"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MarshalByRefObject_t1415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2658/* custom_attributes_cache */
	, &MarshalByRefObject_t1415_0_0_0/* byval_arg */
	, &MarshalByRefObject_t1415_1_0_0/* this_arg */
	, &MarshalByRefObject_t1415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalByRefObject_t1415)/* instance_size */
	, sizeof (MarshalByRefObject_t1415)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Nullable`1
extern TypeInfo Nullable_1_t2194_il2cpp_TypeInfo;
static const EncodedMethodIndex Nullable_1_t2194_VTable[4] = 
{
	2691,
	601,
	2692,
	2693,
};
extern const Il2CppType Nullable_1_t3786_0_0_0;
extern const Il2CppType Nullable_1_t2194_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Nullable_1_t2194_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4903 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5575 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4902 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Nullable_1_t2194_0_0_0;
extern const Il2CppType Nullable_1_t2194_1_0_0;
const Il2CppTypeDefinitionMetadata Nullable_1_t2194_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Nullable_1_t2194_VTable/* vtableMethods */
	, Nullable_1_t2194_RGCTXData/* rgctxDefinition */
	, 6539/* fieldStart */
	, 9249/* methodStart */
	, -1/* eventStart */
	, 1755/* propertyStart */

};
TypeInfo Nullable_1_t2194_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Nullable`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Nullable_1_t2194_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Nullable_1_t2194_0_0_0/* byval_arg */
	, &Nullable_1_t2194_1_0_0/* this_arg */
	, &Nullable_1_t2194_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 141/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpers.h"
// Metadata Definition System.Runtime.CompilerServices.RuntimeHelpers
extern TypeInfo RuntimeHelpers_t1573_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
static const EncodedMethodIndex RuntimeHelpers_t1573_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeHelpers_t1573_0_0_0;
extern const Il2CppType RuntimeHelpers_t1573_1_0_0;
struct RuntimeHelpers_t1573;
const Il2CppTypeDefinitionMetadata RuntimeHelpers_t1573_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RuntimeHelpers_t1573_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9256/* methodStart */
	, -1/* eventStart */
	, 1757/* propertyStart */

};
TypeInfo RuntimeHelpers_t1573_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeHelpers"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &RuntimeHelpers_t1573_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimeHelpers_t1573_0_0_0/* byval_arg */
	, &RuntimeHelpers_t1573_1_0_0/* this_arg */
	, &RuntimeHelpers_t1573_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeHelpers_t1573)/* instance_size */
	, sizeof (RuntimeHelpers_t1573)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Locale
#include "mscorlib_Locale.h"
// Metadata Definition Locale
extern TypeInfo Locale_t1574_il2cpp_TypeInfo;
// Locale
#include "mscorlib_LocaleMethodDeclarations.h"
static const EncodedMethodIndex Locale_t1574_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Locale_t1574_0_0_0;
extern const Il2CppType Locale_t1574_1_0_0;
struct Locale_t1574;
const Il2CppTypeDefinitionMetadata Locale_t1574_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Locale_t1574_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9259/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Locale_t1574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Locale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Locale_t1574_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Locale_t1574_0_0_0/* byval_arg */
	, &Locale_t1574_1_0_0/* this_arg */
	, &Locale_t1574_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Locale_t1574)/* instance_size */
	, sizeof (Locale_t1574)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttribute.h"
// Metadata Definition System.MonoTODOAttribute
extern TypeInfo MonoTODOAttribute_t1575_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttributeMethodDeclarations.h"
static const EncodedMethodIndex MonoTODOAttribute_t1575_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t1575_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTODOAttribute_t1575_0_0_0;
extern const Il2CppType MonoTODOAttribute_t1575_1_0_0;
struct MonoTODOAttribute_t1575;
const Il2CppTypeDefinitionMetadata MonoTODOAttribute_t1575_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoTODOAttribute_t1575_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, MonoTODOAttribute_t1575_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6541/* fieldStart */
	, 9261/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTODOAttribute_t1575_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTODOAttribute_t1575_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2660/* custom_attributes_cache */
	, &MonoTODOAttribute_t1575_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t1575_1_0_0/* this_arg */
	, &MonoTODOAttribute_t1575_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t1575)/* instance_size */
	, sizeof (MonoTODOAttribute_t1575)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttribute.h"
// Metadata Definition System.MonoDocumentationNoteAttribute
extern TypeInfo MonoDocumentationNoteAttribute_t1576_il2cpp_TypeInfo;
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttributeMethodDeclarations.h"
static const EncodedMethodIndex MonoDocumentationNoteAttribute_t1576_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair MonoDocumentationNoteAttribute_t1576_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoDocumentationNoteAttribute_t1576_0_0_0;
extern const Il2CppType MonoDocumentationNoteAttribute_t1576_1_0_0;
struct MonoDocumentationNoteAttribute_t1576;
const Il2CppTypeDefinitionMetadata MonoDocumentationNoteAttribute_t1576_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoDocumentationNoteAttribute_t1576_InterfacesOffsets/* interfaceOffsets */
	, &MonoTODOAttribute_t1575_0_0_0/* parent */
	, MonoDocumentationNoteAttribute_t1576_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9263/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoDocumentationNoteAttribute_t1576_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoDocumentationNoteAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoDocumentationNoteAttribute_t1576_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2661/* custom_attributes_cache */
	, &MonoDocumentationNoteAttribute_t1576_0_0_0/* byval_arg */
	, &MonoDocumentationNoteAttribute_t1576_1_0_0/* this_arg */
	, &MonoDocumentationNoteAttribute_t1576_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoDocumentationNoteAttribute_t1576)/* instance_size */
	, sizeof (MonoDocumentationNoteAttribute_t1576)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOn.h"
// Metadata Definition Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
extern TypeInfo SafeHandleZeroOrMinusOneIsInvalid_t1577_il2cpp_TypeInfo;
// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOnMethodDeclarations.h"
static const EncodedMethodIndex SafeHandleZeroOrMinusOneIsInvalid_t1577_VTable[8] = 
{
	626,
	2694,
	627,
	628,
	2695,
	2696,
	0,
	2697,
};
static const Il2CppType* SafeHandleZeroOrMinusOneIsInvalid_t1577_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair SafeHandleZeroOrMinusOneIsInvalid_t1577_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeHandleZeroOrMinusOneIsInvalid_t1577_0_0_0;
extern const Il2CppType SafeHandleZeroOrMinusOneIsInvalid_t1577_1_0_0;
extern const Il2CppType SafeHandle_t1578_0_0_0;
struct SafeHandleZeroOrMinusOneIsInvalid_t1577;
const Il2CppTypeDefinitionMetadata SafeHandleZeroOrMinusOneIsInvalid_t1577_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SafeHandleZeroOrMinusOneIsInvalid_t1577_InterfacesTypeInfos/* implementedInterfaces */
	, SafeHandleZeroOrMinusOneIsInvalid_t1577_InterfacesOffsets/* interfaceOffsets */
	, &SafeHandle_t1578_0_0_0/* parent */
	, SafeHandleZeroOrMinusOneIsInvalid_t1577_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9264/* methodStart */
	, -1/* eventStart */
	, 1758/* propertyStart */

};
TypeInfo SafeHandleZeroOrMinusOneIsInvalid_t1577_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeHandleZeroOrMinusOneIsInvalid"/* name */
	, "Microsoft.Win32.SafeHandles"/* namespaze */
	, NULL/* methods */
	, &SafeHandleZeroOrMinusOneIsInvalid_t1577_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeHandleZeroOrMinusOneIsInvalid_t1577_0_0_0/* byval_arg */
	, &SafeHandleZeroOrMinusOneIsInvalid_t1577_1_0_0/* this_arg */
	, &SafeHandleZeroOrMinusOneIsInvalid_t1577_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeHandleZeroOrMinusOneIsInvalid_t1577)/* instance_size */
	, sizeof (SafeHandleZeroOrMinusOneIsInvalid_t1577)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Microsoft.Win32.SafeHandles.SafeWaitHandle
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeWaitHandle.h"
// Metadata Definition Microsoft.Win32.SafeHandles.SafeWaitHandle
extern TypeInfo SafeWaitHandle_t1579_il2cpp_TypeInfo;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
#include "mscorlib_Microsoft_Win32_SafeHandles_SafeWaitHandleMethodDeclarations.h"
static const EncodedMethodIndex SafeWaitHandle_t1579_VTable[8] = 
{
	626,
	2694,
	627,
	628,
	2695,
	2696,
	2698,
	2697,
};
static Il2CppInterfaceOffsetPair SafeWaitHandle_t1579_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeWaitHandle_t1579_0_0_0;
extern const Il2CppType SafeWaitHandle_t1579_1_0_0;
struct SafeWaitHandle_t1579;
const Il2CppTypeDefinitionMetadata SafeWaitHandle_t1579_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SafeWaitHandle_t1579_InterfacesOffsets/* interfaceOffsets */
	, &SafeHandleZeroOrMinusOneIsInvalid_t1577_0_0_0/* parent */
	, SafeWaitHandle_t1579_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 9266/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SafeWaitHandle_t1579_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeWaitHandle"/* name */
	, "Microsoft.Win32.SafeHandles"/* namespaze */
	, NULL/* methods */
	, &SafeWaitHandle_t1579_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeWaitHandle_t1579_0_0_0/* byval_arg */
	, &SafeWaitHandle_t1579_1_0_0/* this_arg */
	, &SafeWaitHandle_t1579_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeWaitHandle_t1579)/* instance_size */
	, sizeof (SafeWaitHandle_t1579)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
// Metadata Definition Mono.Globalization.Unicode.CodePointIndexer
extern TypeInfo CodePointIndexer_t1582_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexerMethodDeclarations.h"
extern const Il2CppType TableRange_t1580_0_0_0;
static const Il2CppType* CodePointIndexer_t1582_il2cpp_TypeInfo__nestedTypes[1] =
{
	&TableRange_t1580_0_0_0,
};
static const EncodedMethodIndex CodePointIndexer_t1582_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CodePointIndexer_t1582_0_0_0;
extern const Il2CppType CodePointIndexer_t1582_1_0_0;
struct CodePointIndexer_t1582;
const Il2CppTypeDefinitionMetadata CodePointIndexer_t1582_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CodePointIndexer_t1582_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CodePointIndexer_t1582_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6542/* fieldStart */
	, 9268/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CodePointIndexer_t1582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CodePointIndexer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &CodePointIndexer_t1582_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CodePointIndexer_t1582_0_0_0/* byval_arg */
	, &CodePointIndexer_t1582_1_0_0/* this_arg */
	, &CodePointIndexer_t1582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CodePointIndexer_t1582)/* instance_size */
	, sizeof (CodePointIndexer_t1582)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
// Metadata Definition Mono.Globalization.Unicode.CodePointIndexer/TableRange
extern TypeInfo TableRange_t1580_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRaMethodDeclarations.h"
static const EncodedMethodIndex TableRange_t1580_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TableRange_t1580_1_0_0;
const Il2CppTypeDefinitionMetadata TableRange_t1580_DefinitionMetadata = 
{
	&CodePointIndexer_t1582_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, TableRange_t1580_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6546/* fieldStart */
	, 9270/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TableRange_t1580_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TableRange"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TableRange_t1580_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TableRange_t1580_0_0_0/* byval_arg */
	, &TableRange_t1580_1_0_0/* this_arg */
	, &TableRange_t1580_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TableRange_t1580)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TableRange_t1580)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TableRange_t1580 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057037/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.TailoringInfo
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfo.h"
// Metadata Definition Mono.Globalization.Unicode.TailoringInfo
extern TypeInfo TailoringInfo_t1583_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.TailoringInfo
#include "mscorlib_Mono_Globalization_Unicode_TailoringInfoMethodDeclarations.h"
static const EncodedMethodIndex TailoringInfo_t1583_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TailoringInfo_t1583_0_0_0;
extern const Il2CppType TailoringInfo_t1583_1_0_0;
struct TailoringInfo_t1583;
const Il2CppTypeDefinitionMetadata TailoringInfo_t1583_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TailoringInfo_t1583_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6551/* fieldStart */
	, 9271/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TailoringInfo_t1583_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TailoringInfo"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &TailoringInfo_t1583_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TailoringInfo_t1583_0_0_0/* byval_arg */
	, &TailoringInfo_t1583_1_0_0/* this_arg */
	, &TailoringInfo_t1583_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TailoringInfo_t1583)/* instance_size */
	, sizeof (TailoringInfo_t1583)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
// Metadata Definition Mono.Globalization.Unicode.Contraction
extern TypeInfo Contraction_t1584_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_ContractionMethodDeclarations.h"
static const EncodedMethodIndex Contraction_t1584_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Contraction_t1584_0_0_0;
extern const Il2CppType Contraction_t1584_1_0_0;
struct Contraction_t1584;
const Il2CppTypeDefinitionMetadata Contraction_t1584_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Contraction_t1584_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6555/* fieldStart */
	, 9272/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Contraction_t1584_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Contraction"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &Contraction_t1584_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Contraction_t1584_0_0_0/* byval_arg */
	, &Contraction_t1584_1_0_0/* this_arg */
	, &Contraction_t1584_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Contraction_t1584)/* instance_size */
	, sizeof (Contraction_t1584)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.ContractionComparer
#include "mscorlib_Mono_Globalization_Unicode_ContractionComparer.h"
// Metadata Definition Mono.Globalization.Unicode.ContractionComparer
extern TypeInfo ContractionComparer_t1585_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.ContractionComparer
#include "mscorlib_Mono_Globalization_Unicode_ContractionComparerMethodDeclarations.h"
static const EncodedMethodIndex ContractionComparer_t1585_VTable[5] = 
{
	626,
	601,
	627,
	628,
	2699,
};
extern const Il2CppType IComparer_t416_0_0_0;
static const Il2CppType* ContractionComparer_t1585_InterfacesTypeInfos[] = 
{
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair ContractionComparer_t1585_InterfacesOffsets[] = 
{
	{ &IComparer_t416_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContractionComparer_t1585_0_0_0;
extern const Il2CppType ContractionComparer_t1585_1_0_0;
struct ContractionComparer_t1585;
const Il2CppTypeDefinitionMetadata ContractionComparer_t1585_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContractionComparer_t1585_InterfacesTypeInfos/* implementedInterfaces */
	, ContractionComparer_t1585_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContractionComparer_t1585_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6558/* fieldStart */
	, 9273/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContractionComparer_t1585_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContractionComparer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &ContractionComparer_t1585_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContractionComparer_t1585_0_0_0/* byval_arg */
	, &ContractionComparer_t1585_1_0_0/* this_arg */
	, &ContractionComparer_t1585_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContractionComparer_t1585)/* instance_size */
	, sizeof (ContractionComparer_t1585)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ContractionComparer_t1585_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Globalization.Unicode.Level2Map
#include "mscorlib_Mono_Globalization_Unicode_Level2Map.h"
// Metadata Definition Mono.Globalization.Unicode.Level2Map
extern TypeInfo Level2Map_t1586_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.Level2Map
#include "mscorlib_Mono_Globalization_Unicode_Level2MapMethodDeclarations.h"
static const EncodedMethodIndex Level2Map_t1586_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Level2Map_t1586_0_0_0;
extern const Il2CppType Level2Map_t1586_1_0_0;
struct Level2Map_t1586;
const Il2CppTypeDefinitionMetadata Level2Map_t1586_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Level2Map_t1586_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6559/* fieldStart */
	, 9276/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Level2Map_t1586_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Level2Map"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &Level2Map_t1586_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Level2Map_t1586_0_0_0/* byval_arg */
	, &Level2Map_t1586_1_0_0/* this_arg */
	, &Level2Map_t1586_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Level2Map_t1586)/* instance_size */
	, sizeof (Level2Map_t1586)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.Level2MapComparer
#include "mscorlib_Mono_Globalization_Unicode_Level2MapComparer.h"
// Metadata Definition Mono.Globalization.Unicode.Level2MapComparer
extern TypeInfo Level2MapComparer_t1587_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.Level2MapComparer
#include "mscorlib_Mono_Globalization_Unicode_Level2MapComparerMethodDeclarations.h"
static const EncodedMethodIndex Level2MapComparer_t1587_VTable[5] = 
{
	626,
	601,
	627,
	628,
	2700,
};
static const Il2CppType* Level2MapComparer_t1587_InterfacesTypeInfos[] = 
{
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair Level2MapComparer_t1587_InterfacesOffsets[] = 
{
	{ &IComparer_t416_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Level2MapComparer_t1587_0_0_0;
extern const Il2CppType Level2MapComparer_t1587_1_0_0;
struct Level2MapComparer_t1587;
const Il2CppTypeDefinitionMetadata Level2MapComparer_t1587_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Level2MapComparer_t1587_InterfacesTypeInfos/* implementedInterfaces */
	, Level2MapComparer_t1587_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Level2MapComparer_t1587_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6561/* fieldStart */
	, 9277/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Level2MapComparer_t1587_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Level2MapComparer"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &Level2MapComparer_t1587_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Level2MapComparer_t1587_0_0_0/* byval_arg */
	, &Level2MapComparer_t1587_1_0_0/* this_arg */
	, &Level2MapComparer_t1587_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Level2MapComparer_t1587)/* instance_size */
	, sizeof (Level2MapComparer_t1587)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Level2MapComparer_t1587_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Globalization.Unicode.MSCompatUnicodeTable
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTable.h"
// Metadata Definition Mono.Globalization.Unicode.MSCompatUnicodeTable
extern TypeInfo MSCompatUnicodeTable_t1589_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.MSCompatUnicodeTable
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTableMethodDeclarations.h"
static const EncodedMethodIndex MSCompatUnicodeTable_t1589_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MSCompatUnicodeTable_t1589_0_0_0;
extern const Il2CppType MSCompatUnicodeTable_t1589_1_0_0;
struct MSCompatUnicodeTable_t1589;
const Il2CppTypeDefinitionMetadata MSCompatUnicodeTable_t1589_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MSCompatUnicodeTable_t1589_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6562/* fieldStart */
	, 9280/* methodStart */
	, -1/* eventStart */
	, 1759/* propertyStart */

};
TypeInfo MSCompatUnicodeTable_t1589_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MSCompatUnicodeTable"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &MSCompatUnicodeTable_t1589_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MSCompatUnicodeTable_t1589_0_0_0/* byval_arg */
	, &MSCompatUnicodeTable_t1589_1_0_0/* this_arg */
	, &MSCompatUnicodeTable_t1589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MSCompatUnicodeTable_t1589)/* instance_size */
	, sizeof (MSCompatUnicodeTable_t1589)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MSCompatUnicodeTable_t1589_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTableUtil.h"
// Metadata Definition Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
extern TypeInfo MSCompatUnicodeTableUtil_t1590_il2cpp_TypeInfo;
// Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
#include "mscorlib_Mono_Globalization_Unicode_MSCompatUnicodeTableUtilMethodDeclarations.h"
static const EncodedMethodIndex MSCompatUnicodeTableUtil_t1590_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MSCompatUnicodeTableUtil_t1590_0_0_0;
extern const Il2CppType MSCompatUnicodeTableUtil_t1590_1_0_0;
struct MSCompatUnicodeTableUtil_t1590;
const Il2CppTypeDefinitionMetadata MSCompatUnicodeTableUtil_t1590_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MSCompatUnicodeTableUtil_t1590_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6584/* fieldStart */
	, 9301/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MSCompatUnicodeTableUtil_t1590_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MSCompatUnicodeTableUtil"/* name */
	, "Mono.Globalization.Unicode"/* namespaze */
	, NULL/* methods */
	, &MSCompatUnicodeTableUtil_t1590_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MSCompatUnicodeTableUtil_t1590_0_0_0/* byval_arg */
	, &MSCompatUnicodeTableUtil_t1590_1_0_0/* this_arg */
	, &MSCompatUnicodeTableUtil_t1590_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MSCompatUnicodeTableUtil_t1590)/* instance_size */
	, sizeof (MSCompatUnicodeTableUtil_t1590)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MSCompatUnicodeTableUtil_t1590_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
