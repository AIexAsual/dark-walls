﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t268;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t2986;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Int32[]
struct Int32U5BU5D_t269;
// System.Collections.Generic.Queue`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Queue`1<System.Int32>::.ctor()
extern "C" void Queue_1__ctor_m3036_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1__ctor_m3036(__this, method) (( void (*) (Queue_1_t268 *, const MethodInfo*))Queue_1__ctor_m3036_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m14295_gshared (Queue_1_t268 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m14295(__this, ___array, ___idx, method) (( void (*) (Queue_1_t268 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m14295_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m14296_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m14296(__this, method) (( bool (*) (Queue_1_t268 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m14296_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m14297_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m14297(__this, method) (( Object_t * (*) (Queue_1_t268 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m14297_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14298_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14298(__this, method) (( Object_t* (*) (Queue_1_t268 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14298_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m14299_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m14299(__this, method) (( Object_t * (*) (Queue_1_t268 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m14299_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::Clear()
extern "C" void Queue_1_Clear_m3046_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1_Clear_m3046(__this, method) (( void (*) (Queue_1_t268 *, const MethodInfo*))Queue_1_Clear_m3046_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m3045_gshared (Queue_1_t268 * __this, Int32U5BU5D_t269* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m3045(__this, ___array, ___idx, method) (( void (*) (Queue_1_t268 *, Int32U5BU5D_t269*, int32_t, const MethodInfo*))Queue_1_CopyTo_m3045_gshared)(__this, ___array, ___idx, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m3048_gshared (Queue_1_t268 * __this, int32_t ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m3048(__this, ___item, method) (( void (*) (Queue_1_t268 *, int32_t, const MethodInfo*))Queue_1_Enqueue_m3048_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Int32>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m14300_gshared (Queue_1_t268 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m14300(__this, ___new_size, method) (( void (*) (Queue_1_t268 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m14300_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Int32>::get_Count()
extern "C" int32_t Queue_1_get_Count_m14301_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m14301(__this, method) (( int32_t (*) (Queue_1_t268 *, const MethodInfo*))Queue_1_get_Count_m14301_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t2284  Queue_1_GetEnumerator_m14302_gshared (Queue_1_t268 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m14302(__this, method) (( Enumerator_t2284  (*) (Queue_1_t268 *, const MethodInfo*))Queue_1_GetEnumerator_m14302_gshared)(__this, method)
