﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct KeyCollection_t2344;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Dictionary_2_t420;
// System.Collections.Generic.IEnumerator`1<iTweenEvent/TweenType>
struct IEnumerator_1_t3002;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// iTweenEvent/TweenType[]
struct TweenTypeU5BU5D_t2316;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_39.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_1MethodDeclarations.h"
#define KeyCollection__ctor_m15040(__this, ___dictionary, method) (( void (*) (KeyCollection_t2344 *, Dictionary_2_t420 *, const MethodInfo*))KeyCollection__ctor_m14846_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15041(__this, ___item, method) (( void (*) (KeyCollection_t2344 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m14847_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15042(__this, method) (( void (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m14848_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15043(__this, ___item, method) (( bool (*) (KeyCollection_t2344 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m14849_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15044(__this, ___item, method) (( bool (*) (KeyCollection_t2344 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m14850_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15045(__this, method) (( Object_t* (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m14851_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m15046(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2344 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m14852_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15047(__this, method) (( Object_t * (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m14853_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15048(__this, method) (( bool (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m14854_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15049(__this, method) (( bool (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m14855_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m15050(__this, method) (( Object_t * (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m14856_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m15051(__this, ___array, ___index, method) (( void (*) (KeyCollection_t2344 *, TweenTypeU5BU5D_t2316*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m14857_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m15052(__this, method) (( Enumerator_t3007  (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_GetEnumerator_m14858_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Count()
#define KeyCollection_get_Count_m15053(__this, method) (( int32_t (*) (KeyCollection_t2344 *, const MethodInfo*))KeyCollection_get_Count_m14859_gshared)(__this, method)
