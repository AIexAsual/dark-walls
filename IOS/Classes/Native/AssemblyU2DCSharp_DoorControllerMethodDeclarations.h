﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DoorController
struct DoorController_t311;
// System.String
struct String_t;

// System.Void DoorController::.ctor()
extern "C" void DoorController__ctor_m1875 (DoorController_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::Start()
extern "C" void DoorController_Start_m1876 (DoorController_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::Update()
extern "C" void DoorController_Update_m1877 (DoorController_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::OpenDoors(System.Single,System.Single,System.Single,System.String)
extern "C" void DoorController_OpenDoors_m1878 (DoorController_t311 * __this, float ___rot, float ___time, float ___delay, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::PlayAudio()
extern "C" void DoorController_PlayAudio_m1879 (DoorController_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DoorController::CloseDoors()
extern "C" void DoorController_CloseDoors_m1880 (DoorController_t311 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
