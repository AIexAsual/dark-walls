﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_Desert
struct CameraFilterPack_Gradients_Desert_t146;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_Desert::.ctor()
extern "C" void CameraFilterPack_Gradients_Desert__ctor_m947 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Desert::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_Desert_get_material_m948 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::Start()
extern "C" void CameraFilterPack_Gradients_Desert_Start_m949 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_Desert_OnRenderImage_m950 (CameraFilterPack_Gradients_Desert_t146 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::Update()
extern "C" void CameraFilterPack_Gradients_Desert_Update_m951 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Desert::OnDisable()
extern "C" void CameraFilterPack_Gradients_Desert_OnDisable_m952 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
