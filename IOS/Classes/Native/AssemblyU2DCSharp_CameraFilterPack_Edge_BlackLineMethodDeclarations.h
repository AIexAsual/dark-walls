﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Edge_BlackLine
struct CameraFilterPack_Edge_BlackLine_t112;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Edge_BlackLine::.ctor()
extern "C" void CameraFilterPack_Edge_BlackLine__ctor_m719 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_BlackLine::get_material()
extern "C" Material_t2 * CameraFilterPack_Edge_BlackLine_get_material_m720 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::Start()
extern "C" void CameraFilterPack_Edge_BlackLine_Start_m721 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Edge_BlackLine_OnRenderImage_m722 (CameraFilterPack_Edge_BlackLine_t112 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::Update()
extern "C" void CameraFilterPack_Edge_BlackLine_Update_m723 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_BlackLine::OnDisable()
extern "C" void CameraFilterPack_Edge_BlackLine_OnDisable_m724 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
