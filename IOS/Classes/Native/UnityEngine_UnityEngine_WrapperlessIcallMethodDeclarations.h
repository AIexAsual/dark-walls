﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t1004;

// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C" void WrapperlessIcall__ctor_m6148 (WrapperlessIcall_t1004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
