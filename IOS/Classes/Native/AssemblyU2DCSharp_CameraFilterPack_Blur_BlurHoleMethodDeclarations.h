﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_BlurHole
struct CameraFilterPack_Blur_BlurHole_t48;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_BlurHole::.ctor()
extern "C" void CameraFilterPack_Blur_BlurHole__ctor_m289 (CameraFilterPack_Blur_BlurHole_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_BlurHole::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_BlurHole_get_material_m290 (CameraFilterPack_Blur_BlurHole_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::Start()
extern "C" void CameraFilterPack_Blur_BlurHole_Start_m291 (CameraFilterPack_Blur_BlurHole_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_BlurHole_OnRenderImage_m292 (CameraFilterPack_Blur_BlurHole_t48 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::OnValidate()
extern "C" void CameraFilterPack_Blur_BlurHole_OnValidate_m293 (CameraFilterPack_Blur_BlurHole_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::Update()
extern "C" void CameraFilterPack_Blur_BlurHole_Update_m294 (CameraFilterPack_Blur_BlurHole_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_BlurHole::OnDisable()
extern "C" void CameraFilterPack_Blur_BlurHole_OnDisable_m295 (CameraFilterPack_Blur_BlurHole_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
