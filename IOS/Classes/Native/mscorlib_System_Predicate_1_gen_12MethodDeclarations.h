﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<System.Boolean>
struct Predicate_1_t2417;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m15916_gshared (Predicate_1_t2417 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m15916(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2417 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15916_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Boolean>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m15917_gshared (Predicate_1_t2417 * __this, bool ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m15917(__this, ___obj, method) (( bool (*) (Predicate_1_t2417 *, bool, const MethodInfo*))Predicate_1_Invoke_m15917_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m15918_gshared (Predicate_1_t2417 * __this, bool ___obj, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m15918(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2417 *, bool, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15918_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m15919_gshared (Predicate_1_t2417 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m15919(__this, ___result, method) (( bool (*) (Predicate_1_t2417 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15919_gshared)(__this, ___result, method)
