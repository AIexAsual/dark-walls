﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t829;
struct AsyncOperation_t829_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m5569 (AsyncOperation_t829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m5570 (AsyncOperation_t829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m5571 (AsyncOperation_t829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AsyncOperation_t829_marshal(const AsyncOperation_t829& unmarshaled, AsyncOperation_t829_marshaled& marshaled);
void AsyncOperation_t829_marshal_back(const AsyncOperation_t829_marshaled& marshaled, AsyncOperation_t829& unmarshaled);
void AsyncOperation_t829_marshal_cleanup(AsyncOperation_t829_marshaled& marshaled);
