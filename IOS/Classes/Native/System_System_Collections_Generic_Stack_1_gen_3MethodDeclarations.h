﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct Stack_1_t2631;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
struct IEnumerator_1_t3142;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t777;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m19254(__this, method) (( void (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1__ctor_m14012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m19255(__this, method) (( bool (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m14013_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m19256(__this, method) (( Object_t * (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m14014_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m19257(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2631 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m14015_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19258(__this, method) (( Object_t* (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14016_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m19259(__this, method) (( Object_t * (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m14017_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Peek()
#define Stack_1_Peek_m19260(__this, method) (( List_1_t777 * (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_Peek_m14018_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Pop()
#define Stack_1_Pop_m19261(__this, method) (( List_1_t777 * (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_Pop_m14019_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Push(T)
#define Stack_1_Push_m19262(__this, ___t, method) (( void (*) (Stack_1_t2631 *, List_1_t777 *, const MethodInfo*))Stack_1_Push_m14020_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Count()
#define Stack_1_get_Count_m19263(__this, method) (( int32_t (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_get_Count_m14021_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::GetEnumerator()
#define Stack_1_GetEnumerator_m19264(__this, method) (( Enumerator_t3143  (*) (Stack_1_t2631 *, const MethodInfo*))Stack_1_GetEnumerator_m14022_gshared)(__this, method)
