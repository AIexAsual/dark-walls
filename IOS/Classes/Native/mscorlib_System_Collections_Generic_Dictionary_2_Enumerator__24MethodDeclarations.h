﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t2842;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t481;
struct Event_t481_marshaled;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1058;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23MethodDeclarations.h"
#define Enumerator__ctor_m22011(__this, ___dictionary, method) (( void (*) (Enumerator_t2842 *, Dictionary_2_t1058 *, const MethodInfo*))Enumerator__ctor_m21912_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22012(__this, method) (( Object_t * (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21913_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22013(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21914_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22014(__this, method) (( Object_t * (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21915_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22015(__this, method) (( Object_t * (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21916_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m22016(__this, method) (( bool (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_MoveNext_m21917_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m22017(__this, method) (( KeyValuePair_2_t2839  (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_get_Current_m21918_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22018(__this, method) (( Event_t481 * (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_get_CurrentKey_m21919_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22019(__this, method) (( int32_t (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_get_CurrentValue_m21920_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m22020(__this, method) (( void (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_VerifyState_m21921_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22021(__this, method) (( void (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_VerifyCurrent_m21922_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m22022(__this, method) (( void (*) (Enumerator_t2842 *, const MethodInfo*))Enumerator_Dispose_m21923_gshared)(__this, method)
