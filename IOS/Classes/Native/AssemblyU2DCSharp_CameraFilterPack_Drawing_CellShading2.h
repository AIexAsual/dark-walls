﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Drawing_CellShading2
struct  CameraFilterPack_Drawing_CellShading2_t96  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Drawing_CellShading2::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_CellShading2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Drawing_CellShading2::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Drawing_CellShading2::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Drawing_CellShading2::EdgeSize
	float ___EdgeSize_6;
	// System.Single CameraFilterPack_Drawing_CellShading2::ColorLevel
	float ___ColorLevel_7;
	// System.Single CameraFilterPack_Drawing_CellShading2::Blur
	float ___Blur_8;
};
struct CameraFilterPack_Drawing_CellShading2_t96_StaticFields{
	// System.Single CameraFilterPack_Drawing_CellShading2::ChangeEdgeSize
	float ___ChangeEdgeSize_9;
	// System.Single CameraFilterPack_Drawing_CellShading2::ChangeColorLevel
	float ___ChangeColorLevel_10;
};
