﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2705;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1094;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t3182;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>
struct KeyCollection_t2709;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>
struct ValueCollection_t2713;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2347;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int64>
struct IDictionary_2_t3186;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct KeyValuePair_2U5BU5D_t3187;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>
struct IEnumerator_1_t3188;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor()
extern "C" void Dictionary_2__ctor_m20329_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m20329(__this, method) (( void (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2__ctor_m20329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m20331_gshared (Dictionary_2_t2705 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m20331(__this, ___comparer, method) (( void (*) (Dictionary_2_t2705 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20331_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m20333_gshared (Dictionary_2_t2705 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m20333(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2705 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20333_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m20335_gshared (Dictionary_2_t2705 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m20335(__this, ___capacity, method) (( void (*) (Dictionary_2_t2705 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m20335_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m20337_gshared (Dictionary_2_t2705 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m20337(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2705 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20337_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m20339_gshared (Dictionary_2_t2705 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m20339(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2705 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m20339_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20341_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20341(__this, method) (( Object_t* (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20341_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20343_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20343(__this, method) (( Object_t* (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20343_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m20345_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20345(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m20345_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20347_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20347(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2705 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m20347_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20349_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m20349(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2705 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m20349_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m20351_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m20351(__this, ___key, method) (( bool (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m20351_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20353_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m20353(__this, ___key, method) (( void (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m20353_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20355_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20355(__this, method) (( bool (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20355_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20357_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20357(__this, method) (( Object_t * (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20357_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20359_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20359(__this, method) (( bool (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20359_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20361_gshared (Dictionary_2_t2705 * __this, KeyValuePair_2_t2706  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20361(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2705 *, KeyValuePair_2_t2706 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20361_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20363_gshared (Dictionary_2_t2705 * __this, KeyValuePair_2_t2706  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20363(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2705 *, KeyValuePair_2_t2706 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20363_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20365_gshared (Dictionary_2_t2705 * __this, KeyValuePair_2U5BU5D_t3187* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20365(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2705 *, KeyValuePair_2U5BU5D_t3187*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20365_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20367_gshared (Dictionary_2_t2705 * __this, KeyValuePair_2_t2706  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20367(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2705 *, KeyValuePair_2_t2706 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20367_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20369_gshared (Dictionary_2_t2705 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20369(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2705 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m20369_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20371_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20371(__this, method) (( Object_t * (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20371_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20373_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20373(__this, method) (( Object_t* (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20373_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20375_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20375(__this, method) (( Object_t * (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20375_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m20377_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m20377(__this, method) (( int32_t (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_get_Count_m20377_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Item(TKey)
extern "C" int64_t Dictionary_2_get_Item_m20379_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m20379(__this, ___key, method) (( int64_t (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m20379_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m20381_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m20381(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2705 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_set_Item_m20381_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m20383_gshared (Dictionary_2_t2705 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m20383(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2705 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m20383_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m20385_gshared (Dictionary_2_t2705 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m20385(__this, ___size, method) (( void (*) (Dictionary_2_t2705 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m20385_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m20387_gshared (Dictionary_2_t2705 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m20387(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2705 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m20387_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2706  Dictionary_2_make_pair_m20389_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m20389(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2706  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_make_pair_m20389_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m20391_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m20391(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_key_m20391_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_value(TKey,TValue)
extern "C" int64_t Dictionary_2_pick_value_m20393_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m20393(__this /* static, unused */, ___key, ___value, method) (( int64_t (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_value_m20393_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m20395_gshared (Dictionary_2_t2705 * __this, KeyValuePair_2U5BU5D_t3187* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m20395(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2705 *, KeyValuePair_2U5BU5D_t3187*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m20395_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Resize()
extern "C" void Dictionary_2_Resize_m20397_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m20397(__this, method) (( void (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_Resize_m20397_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m20399_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m20399(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2705 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_Add_m20399_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Clear()
extern "C" void Dictionary_2_Clear_m20401_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m20401(__this, method) (( void (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_Clear_m20401_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m20403_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m20403(__this, ___key, method) (( bool (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m20403_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m20405_gshared (Dictionary_2_t2705 * __this, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m20405(__this, ___value, method) (( bool (*) (Dictionary_2_t2705 *, int64_t, const MethodInfo*))Dictionary_2_ContainsValue_m20405_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m20407_gshared (Dictionary_2_t2705 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m20407(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2705 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m20407_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m20409_gshared (Dictionary_2_t2705 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m20409(__this, ___sender, method) (( void (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m20409_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m20411_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m20411(__this, ___key, method) (( bool (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m20411_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m20413_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, int64_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m20413(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2705 *, Object_t *, int64_t*, const MethodInfo*))Dictionary_2_TryGetValue_m20413_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Keys()
extern "C" KeyCollection_t2709 * Dictionary_2_get_Keys_m20415_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m20415(__this, method) (( KeyCollection_t2709 * (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_get_Keys_m20415_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Values()
extern "C" ValueCollection_t2713 * Dictionary_2_get_Values_m20417_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m20417(__this, method) (( ValueCollection_t2713 * (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_get_Values_m20417_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m20419_gshared (Dictionary_2_t2705 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m20419(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m20419_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTValue(System.Object)
extern "C" int64_t Dictionary_2_ToTValue_m20421_gshared (Dictionary_2_t2705 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m20421(__this, ___value, method) (( int64_t (*) (Dictionary_2_t2705 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m20421_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m20423_gshared (Dictionary_2_t2705 * __this, KeyValuePair_2_t2706  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m20423(__this, ___pair, method) (( bool (*) (Dictionary_2_t2705 *, KeyValuePair_2_t2706 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m20423_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetEnumerator()
extern "C" Enumerator_t2711  Dictionary_2_GetEnumerator_m20425_gshared (Dictionary_2_t2705 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m20425(__this, method) (( Enumerator_t2711  (*) (Dictionary_2_t2705 *, const MethodInfo*))Dictionary_2_GetEnumerator_m20425_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t552  Dictionary_2_U3CCopyToU3Em__0_m20427_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m20427(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m20427_gshared)(__this /* static, unused */, ___key, ___value, method)
