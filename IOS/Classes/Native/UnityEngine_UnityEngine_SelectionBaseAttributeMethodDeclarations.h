﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t1051;

// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C" void SelectionBaseAttribute__ctor_m6247 (SelectionBaseAttribute_t1051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
