﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t243;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// RagdollHelper/BodyPart
struct  BodyPart_t400  : public Object_t
{
	// UnityEngine.Transform RagdollHelper/BodyPart::transform
	Transform_t243 * ___transform_0;
	// UnityEngine.Vector3 RagdollHelper/BodyPart::storedPosition
	Vector3_t215  ___storedPosition_1;
	// UnityEngine.Quaternion RagdollHelper/BodyPart::storedRotation
	Quaternion_t261  ___storedRotation_2;
};
