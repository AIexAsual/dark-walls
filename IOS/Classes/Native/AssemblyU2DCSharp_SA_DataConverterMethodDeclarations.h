﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SA_DataConverter
struct SA_DataConverter_t296;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t398;

// System.Void SA_DataConverter::.ctor()
extern "C" void SA_DataConverter__ctor_m1827 (SA_DataConverter_t296 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_DataConverter::SerializeArray(System.String[],System.String)
extern "C" String_t* SA_DataConverter_SerializeArray_m1828 (Object_t * __this /* static, unused */, StringU5BU5D_t398* ___array, String_t* ___splitter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] SA_DataConverter::ParseArray(System.String,System.String)
extern "C" StringU5BU5D_t398* SA_DataConverter_ParseArray_m1829 (Object_t * __this /* static, unused */, String_t* ___arrayData, String_t* ___splitter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
