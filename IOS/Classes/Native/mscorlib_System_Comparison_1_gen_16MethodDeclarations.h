﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<iTween/EaseType>
struct Comparison_1_t2457;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Comparison`1<iTween/EaseType>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m16496_gshared (Comparison_1_t2457 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m16496(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2457 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m16496_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<iTween/EaseType>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m16497_gshared (Comparison_1_t2457 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m16497(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2457 *, int32_t, int32_t, const MethodInfo*))Comparison_1_Invoke_m16497_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<iTween/EaseType>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m16498_gshared (Comparison_1_t2457 * __this, int32_t ___x, int32_t ___y, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m16498(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2457 *, int32_t, int32_t, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m16498_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<iTween/EaseType>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m16499_gshared (Comparison_1_t2457 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m16499(__this, ___result, method) (( int32_t (*) (Comparison_1_t2457 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m16499_gshared)(__this, ___result, method)
