﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
struct InternalEnumerator_1_t2818;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21772_gshared (InternalEnumerator_1_t2818 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21772(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2818 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21772_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21773_gshared (InternalEnumerator_1_t2818 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21773(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2818 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21773_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21774_gshared (InternalEnumerator_1_t2818 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21774(__this, method) (( void (*) (InternalEnumerator_1_t2818 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21774_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21775_gshared (InternalEnumerator_1_t2818 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21775(__this, method) (( bool (*) (InternalEnumerator_1_t2818 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21775_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t1039  InternalEnumerator_1_get_Current_m21776_gshared (InternalEnumerator_1_t2818 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21776(__this, method) (( HitInfo_t1039  (*) (InternalEnumerator_1_t2818 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21776_gshared)(__this, method)
