﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<iTween/EaseType>
struct Enumerator_t2449;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<iTween/EaseType>
struct List_1_t580;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m16406_gshared (Enumerator_t2449 * __this, List_1_t580 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m16406(__this, ___l, method) (( void (*) (Enumerator_t2449 *, List_1_t580 *, const MethodInfo*))Enumerator__ctor_m16406_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16407_gshared (Enumerator_t2449 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16407(__this, method) (( Object_t * (*) (Enumerator_t2449 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16407_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::Dispose()
extern "C" void Enumerator_Dispose_m16408_gshared (Enumerator_t2449 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16408(__this, method) (( void (*) (Enumerator_t2449 *, const MethodInfo*))Enumerator_Dispose_m16408_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::VerifyState()
extern "C" void Enumerator_VerifyState_m16409_gshared (Enumerator_t2449 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16409(__this, method) (( void (*) (Enumerator_t2449 *, const MethodInfo*))Enumerator_VerifyState_m16409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16410_gshared (Enumerator_t2449 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16410(__this, method) (( bool (*) (Enumerator_t2449 *, const MethodInfo*))Enumerator_MoveNext_m16410_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::get_Current()
extern "C" int32_t Enumerator_get_Current_m16411_gshared (Enumerator_t2449 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16411(__this, method) (( int32_t (*) (Enumerator_t2449 *, const MethodInfo*))Enumerator_get_Current_m16411_gshared)(__this, method)
