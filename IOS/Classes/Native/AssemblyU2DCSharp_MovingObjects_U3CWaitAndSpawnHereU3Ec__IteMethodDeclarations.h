﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MovingObjects/<WaitAndSpawnHere>c__IteratorF
struct U3CWaitAndSpawnHereU3Ec__IteratorF_t389;
// System.Object
struct Object_t;

// System.Void MovingObjects/<WaitAndSpawnHere>c__IteratorF::.ctor()
extern "C" void U3CWaitAndSpawnHereU3Ec__IteratorF__ctor_m2216 (U3CWaitAndSpawnHereU3Ec__IteratorF_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MovingObjects/<WaitAndSpawnHere>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndSpawnHereU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2217 (U3CWaitAndSpawnHereU3Ec__IteratorF_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MovingObjects/<WaitAndSpawnHere>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndSpawnHereU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m2218 (U3CWaitAndSpawnHereU3Ec__IteratorF_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MovingObjects/<WaitAndSpawnHere>c__IteratorF::MoveNext()
extern "C" bool U3CWaitAndSpawnHereU3Ec__IteratorF_MoveNext_m2219 (U3CWaitAndSpawnHereU3Ec__IteratorF_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects/<WaitAndSpawnHere>c__IteratorF::Dispose()
extern "C" void U3CWaitAndSpawnHereU3Ec__IteratorF_Dispose_m2220 (U3CWaitAndSpawnHereU3Ec__IteratorF_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjects/<WaitAndSpawnHere>c__IteratorF::Reset()
extern "C" void U3CWaitAndSpawnHereU3Ec__IteratorF_Reset_m2221 (U3CWaitAndSpawnHereU3Ec__IteratorF_t389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
