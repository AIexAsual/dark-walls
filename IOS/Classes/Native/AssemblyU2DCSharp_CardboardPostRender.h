﻿#pragma once
#include <stdint.h>
// UnityEngine.Mesh
struct Mesh_t245;
// UnityEngine.Material
struct Material_t2;
// System.Single[]
struct SingleU5BU5D_t72;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// CardboardPostRender
struct  CardboardPostRender_t246  : public MonoBehaviour_t4
{
	// UnityEngine.Mesh CardboardPostRender::distortionMesh
	Mesh_t245 * ___distortionMesh_14;
	// UnityEngine.Material CardboardPostRender::meshMaterial
	Material_t2 * ___meshMaterial_15;
	// UnityEngine.Material CardboardPostRender::uiMaterial
	Material_t2 * ___uiMaterial_16;
	// System.Single CardboardPostRender::centerWidthPx
	float ___centerWidthPx_17;
	// System.Single CardboardPostRender::buttonWidthPx
	float ___buttonWidthPx_18;
	// System.Single CardboardPostRender::xScale
	float ___xScale_19;
	// System.Single CardboardPostRender::yScale
	float ___yScale_20;
	// UnityEngine.Matrix4x4 CardboardPostRender::xfm
	Matrix4x4_t242  ___xfm_21;
	// UnityEngine.Camera CardboardPostRender::<camera>k__BackingField
	Camera_t14 * ___U3CcameraU3Ek__BackingField_23;
};
struct CardboardPostRender_t246_StaticFields{
	// System.Single[] CardboardPostRender::Angles
	SingleU5BU5D_t72* ___Angles_22;
};
