﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Glitch1
struct CameraFilterPack_FX_Glitch1_t128;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Glitch1::.ctor()
extern "C" void CameraFilterPack_FX_Glitch1__ctor_m826 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Glitch1::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Glitch1_get_material_m827 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::Start()
extern "C" void CameraFilterPack_FX_Glitch1_Start_m828 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Glitch1_OnRenderImage_m829 (CameraFilterPack_FX_Glitch1_t128 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::OnValidate()
extern "C" void CameraFilterPack_FX_Glitch1_OnValidate_m830 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::Update()
extern "C" void CameraFilterPack_FX_Glitch1_Update_m831 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch1::OnDisable()
extern "C" void CameraFilterPack_FX_Glitch1_OnDisable_m832 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
