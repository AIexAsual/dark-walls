﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_t2843;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct  InvokableCall_2_t2844  : public BaseInvokableCall_t1061
{
	// UnityEngine.Events.UnityAction`2<T1,T2> UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Delegate
	UnityAction_2_t2843 * ___Delegate_0;
};
