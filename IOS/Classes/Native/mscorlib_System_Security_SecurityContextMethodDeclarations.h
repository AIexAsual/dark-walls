﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SecurityContext
struct SecurityContext_t2016;
// System.Threading.CompressedStack
struct CompressedStack_t2015;

// System.Void System.Security.SecurityContext::.ctor()
extern "C" void SecurityContext__ctor_m12205 (SecurityContext_t2016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.SecurityContext::.ctor(System.Security.SecurityContext)
extern "C" void SecurityContext__ctor_m12206 (SecurityContext_t2016 * __this, SecurityContext_t2016 * ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.SecurityContext System.Security.SecurityContext::Capture()
extern "C" SecurityContext_t2016 * SecurityContext_Capture_m12207 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.SecurityContext::get_FlowSuppressed()
extern "C" bool SecurityContext_get_FlowSuppressed_m12208 (SecurityContext_t2016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.CompressedStack System.Security.SecurityContext::get_CompressedStack()
extern "C" CompressedStack_t2015 * SecurityContext_get_CompressedStack_m12209 (SecurityContext_t2016 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
