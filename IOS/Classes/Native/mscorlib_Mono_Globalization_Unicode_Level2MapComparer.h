﻿#pragma once
#include <stdint.h>
// Mono.Globalization.Unicode.Level2MapComparer
struct Level2MapComparer_t1587;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Globalization.Unicode.Level2MapComparer
struct  Level2MapComparer_t1587  : public Object_t
{
};
struct Level2MapComparer_t1587_StaticFields{
	// Mono.Globalization.Unicode.Level2MapComparer Mono.Globalization.Unicode.Level2MapComparer::Instance
	Level2MapComparer_t1587 * ___Instance_0;
};
