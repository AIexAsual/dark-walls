﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Noise
struct CameraFilterPack_Distortion_Noise_t91;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Noise::.ctor()
extern "C" void CameraFilterPack_Distortion_Noise__ctor_m574 (CameraFilterPack_Distortion_Noise_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Noise::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Noise_get_material_m575 (CameraFilterPack_Distortion_Noise_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::Start()
extern "C" void CameraFilterPack_Distortion_Noise_Start_m576 (CameraFilterPack_Distortion_Noise_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Noise_OnRenderImage_m577 (CameraFilterPack_Distortion_Noise_t91 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::OnValidate()
extern "C" void CameraFilterPack_Distortion_Noise_OnValidate_m578 (CameraFilterPack_Distortion_Noise_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::Update()
extern "C" void CameraFilterPack_Distortion_Noise_Update_m579 (CameraFilterPack_Distortion_Noise_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Noise::OnDisable()
extern "C" void CameraFilterPack_Distortion_Noise_OnDisable_m580 (CameraFilterPack_Distortion_Noise_t91 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
