﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Grid
struct CameraFilterPack_FX_Grid_t131;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Grid::.ctor()
extern "C" void CameraFilterPack_FX_Grid__ctor_m847 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Grid::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Grid_get_material_m848 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::Start()
extern "C" void CameraFilterPack_FX_Grid_Start_m849 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Grid_OnRenderImage_m850 (CameraFilterPack_FX_Grid_t131 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::OnValidate()
extern "C" void CameraFilterPack_FX_Grid_OnValidate_m851 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::Update()
extern "C" void CameraFilterPack_FX_Grid_Update_m852 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Grid::OnDisable()
extern "C" void CameraFilterPack_FX_Grid_OnDisable_m853 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
