﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// MNPopup
#include "AssemblyU2DCSharp_MNPopup.h"
// MNIOSRateUsPopUp
struct  MNIOSRateUsPopUp_t290  : public MNPopup_t278
{
	// System.String MNIOSRateUsPopUp::rate
	String_t* ___rate_6;
	// System.String MNIOSRateUsPopUp::remind
	String_t* ___remind_7;
	// System.String MNIOSRateUsPopUp::declined
	String_t* ___declined_8;
	// System.String MNIOSRateUsPopUp::appleId
	String_t* ___appleId_9;
};
