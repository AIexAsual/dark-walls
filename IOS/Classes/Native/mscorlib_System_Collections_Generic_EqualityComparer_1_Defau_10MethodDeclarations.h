﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/LoopType>
struct DefaultComparer_t2462;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/LoopType>::.ctor()
extern "C" void DefaultComparer__ctor_m16625_gshared (DefaultComparer_t2462 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16625(__this, method) (( void (*) (DefaultComparer_t2462 *, const MethodInfo*))DefaultComparer__ctor_m16625_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/LoopType>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m16626_gshared (DefaultComparer_t2462 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m16626(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2462 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m16626_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTween/LoopType>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m16627_gshared (DefaultComparer_t2462 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m16627(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2462 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m16627_gshared)(__this, ___x, ___y, method)
