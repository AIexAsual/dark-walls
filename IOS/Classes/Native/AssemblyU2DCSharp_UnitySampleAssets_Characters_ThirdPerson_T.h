﻿#pragma once
#include <stdint.h>
// UnityEngine.PhysicMaterial
struct PhysicMaterial_t410;
// System.Object
#include "mscorlib_System_Object.h"
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings
struct  AdvancedSettings_t411  : public Object_t
{
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::stationaryTurnSpeed
	float ___stationaryTurnSpeed_0;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::movingTurnSpeed
	float ___movingTurnSpeed_1;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::headLookResponseSpeed
	float ___headLookResponseSpeed_2;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::crouchHeightFactor
	float ___crouchHeightFactor_3;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::crouchChangeSpeed
	float ___crouchChangeSpeed_4;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::autoTurnThresholdAngle
	float ___autoTurnThresholdAngle_5;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::autoTurnSpeed
	float ___autoTurnSpeed_6;
	// UnityEngine.PhysicMaterial UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::zeroFrictionMaterial
	PhysicMaterial_t410 * ___zeroFrictionMaterial_7;
	// UnityEngine.PhysicMaterial UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::highFrictionMaterial
	PhysicMaterial_t410 * ___highFrictionMaterial_8;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::jumpRepeatDelayTime
	float ___jumpRepeatDelayTime_9;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::runCycleLegOffset
	float ___runCycleLegOffset_10;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings::groundStickyEffect
	float ___groundStickyEffect_11;
};
