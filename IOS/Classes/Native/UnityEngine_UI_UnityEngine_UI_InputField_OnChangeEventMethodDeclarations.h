﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t677;

// System.Void UnityEngine.UI.InputField/OnChangeEvent::.ctor()
extern "C" void OnChangeEvent__ctor_m3863 (OnChangeEvent_t677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
