﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_VHS_Rewind
struct CameraFilterPack_TV_VHS_Rewind_t195;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_VHS_Rewind::.ctor()
extern "C" void CameraFilterPack_TV_VHS_Rewind__ctor_m1263 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_VHS_Rewind::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_VHS_Rewind_get_material_m1264 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::Start()
extern "C" void CameraFilterPack_TV_VHS_Rewind_Start_m1265 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_VHS_Rewind_OnRenderImage_m1266 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::OnValidate()
extern "C" void CameraFilterPack_TV_VHS_Rewind_OnValidate_m1267 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::Update()
extern "C" void CameraFilterPack_TV_VHS_Rewind_Update_m1268 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS_Rewind::OnDisable()
extern "C" void CameraFilterPack_TV_VHS_Rewind_OnDisable_m1269 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
