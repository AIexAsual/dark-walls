﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_ShockWave
struct CameraFilterPack_Distortion_ShockWave_t92;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_ShockWave::.ctor()
extern "C" void CameraFilterPack_Distortion_ShockWave__ctor_m581 (CameraFilterPack_Distortion_ShockWave_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_ShockWave::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_ShockWave_get_material_m582 (CameraFilterPack_Distortion_ShockWave_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::Start()
extern "C" void CameraFilterPack_Distortion_ShockWave_Start_m583 (CameraFilterPack_Distortion_ShockWave_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_ShockWave_OnRenderImage_m584 (CameraFilterPack_Distortion_ShockWave_t92 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::OnValidate()
extern "C" void CameraFilterPack_Distortion_ShockWave_OnValidate_m585 (CameraFilterPack_Distortion_ShockWave_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::Update()
extern "C" void CameraFilterPack_Distortion_ShockWave_Update_m586 (CameraFilterPack_Distortion_ShockWave_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_ShockWave::OnDisable()
extern "C" void CameraFilterPack_Distortion_ShockWave_OnDisable_m587 (CameraFilterPack_Distortion_ShockWave_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
