﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t480;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.EventInfo/AddEventAdapter
struct  AddEventAdapter_t1788  : public MulticastDelegate_t219
{
};
