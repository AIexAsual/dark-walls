﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.GenericStack
struct GenericStack_t870;

// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C" void GenericStack__ctor_m6342 (GenericStack_t870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
