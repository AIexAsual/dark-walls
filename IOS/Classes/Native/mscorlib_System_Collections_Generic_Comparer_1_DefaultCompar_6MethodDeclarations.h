﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color>
struct DefaultComparer_t2438;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color>::.ctor()
extern "C" void DefaultComparer__ctor_m16210_gshared (DefaultComparer_t2438 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16210(__this, method) (( void (*) (DefaultComparer_t2438 *, const MethodInfo*))DefaultComparer__ctor_m16210_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Color>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m16211_gshared (DefaultComparer_t2438 * __this, Color_t6  ___x, Color_t6  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m16211(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2438 *, Color_t6 , Color_t6 , const MethodInfo*))DefaultComparer_Compare_m16211_gshared)(__this, ___x, ___y, method)
