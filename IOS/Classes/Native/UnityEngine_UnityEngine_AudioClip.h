﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t936;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t937;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.AudioClip
struct  AudioClip_t472  : public Object_t473
{
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t936 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t937 * ___m_PCMSetPositionCallback_3;
};
