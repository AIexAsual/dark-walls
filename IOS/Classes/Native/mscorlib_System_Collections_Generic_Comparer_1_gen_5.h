﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Vector3>
struct Comparer_1_t2428;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Vector3>
struct  Comparer_1_t2428  : public Object_t
{
};
struct Comparer_1_t2428_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::_default
	Comparer_1_t2428 * ____default_0;
};
