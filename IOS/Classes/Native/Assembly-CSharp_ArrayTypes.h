﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// CardboardOnGUIWindow[]
// CardboardOnGUIWindow[]
struct  CardboardOnGUIWindowU5BU5D_t479  : public Array_t
{
};
// CardboardEye[]
// CardboardEye[]
struct  CardboardEyeU5BU5D_t265  : public Array_t
{
};
// InteractiveObject[]
// InteractiveObject[]
struct  InteractiveObjectU5BU5D_t355  : public Array_t
{
};
// RagdollHelper/BodyPart[]
// RagdollHelper/BodyPart[]
struct  BodyPartU5BU5D_t2307  : public Array_t
{
};
// iTweenEvent/TweenType[]
// iTweenEvent/TweenType[]
struct  TweenTypeU5BU5D_t2316  : public Array_t
{
};
// iTween/EaseType[]
// iTween/EaseType[]
struct  EaseTypeU5BU5D_t451  : public Array_t
{
};
// iTween/LoopType[]
// iTween/LoopType[]
struct  LoopTypeU5BU5D_t452  : public Array_t
{
};
// ArrayIndexes[]
// ArrayIndexes[]
struct  ArrayIndexesU5BU5D_t454  : public Array_t
{
};
// iTweenPath[]
// iTweenPath[]
struct  iTweenPathU5BU5D_t455  : public Array_t
{
};
struct iTweenPathU5BU5D_t455_StaticFields{
};
// iTweenEvent[]
// iTweenEvent[]
struct  iTweenEventU5BU5D_t557  : public Array_t
{
};
