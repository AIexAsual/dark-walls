﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoEnumInfo/IntComparer
struct IntComparer_t2100;
// System.Object
struct Object_t;

// System.Void System.MonoEnumInfo/IntComparer::.ctor()
extern "C" void IntComparer__ctor_m12997 (IntComparer_t2100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Object,System.Object)
extern "C" int32_t IntComparer_Compare_m12998 (IntComparer_t2100 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Int32,System.Int32)
extern "C" int32_t IntComparer_Compare_m12999 (IntComparer_t2100 * __this, int32_t ___ix, int32_t ___iy, const MethodInfo* method) IL2CPP_METHOD_ATTR;
