﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.IEventSystemHandler
struct IEventSystemHandler_t2261;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.IEventSystemHandler>
struct  Predicate_1_t2262  : public MulticastDelegate_t219
{
};
