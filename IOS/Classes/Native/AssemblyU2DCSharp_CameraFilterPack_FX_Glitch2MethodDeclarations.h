﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Glitch2
struct CameraFilterPack_FX_Glitch2_t129;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Glitch2::.ctor()
extern "C" void CameraFilterPack_FX_Glitch2__ctor_m833 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Glitch2::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Glitch2_get_material_m834 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch2::Start()
extern "C" void CameraFilterPack_FX_Glitch2_Start_m835 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Glitch2_OnRenderImage_m836 (CameraFilterPack_FX_Glitch2_t129 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch2::OnValidate()
extern "C" void CameraFilterPack_FX_Glitch2_OnValidate_m837 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch2::Update()
extern "C" void CameraFilterPack_FX_Glitch2_Update_m838 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Glitch2::OnDisable()
extern "C" void CameraFilterPack_FX_Glitch2_OnDisable_m839 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
