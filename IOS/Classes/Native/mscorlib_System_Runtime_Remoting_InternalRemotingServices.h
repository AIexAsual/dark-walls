﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t348;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.InternalRemotingServices
struct  InternalRemotingServices_t1907  : public Object_t
{
};
struct InternalRemotingServices_t1907_StaticFields{
	// System.Collections.Hashtable System.Runtime.Remoting.InternalRemotingServices::_soapAttributes
	Hashtable_t348 * ____soapAttributes_0;
};
