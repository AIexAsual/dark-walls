﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t1341;
// System.Byte[]
struct ByteU5BU5D_t469;

// System.Void System.Security.Cryptography.HMACSHA1::.ctor()
extern "C" void HMACSHA1__ctor_m11903 (HMACSHA1_t1341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[])
extern "C" void HMACSHA1__ctor_m11904 (HMACSHA1_t1341 * __this, ByteU5BU5D_t469* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
