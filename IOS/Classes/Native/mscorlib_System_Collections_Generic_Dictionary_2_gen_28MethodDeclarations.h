﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2824;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1094;
// System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
struct ICollection_1_t3245;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t2828;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t2832;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2347;
// System.Collections.Generic.IDictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct IDictionary_2_t3249;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3250;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t3251;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C" void Dictionary_2__ctor_m21778_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m21778(__this, method) (( void (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2__ctor_m21778_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21780_gshared (Dictionary_2_t2824 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21780(__this, ___comparer, method) (( void (*) (Dictionary_2_t2824 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21780_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m21782_gshared (Dictionary_2_t2824 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m21782(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2824 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21782_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m21784_gshared (Dictionary_2_t2824 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m21784(__this, ___capacity, method) (( void (*) (Dictionary_2_t2824 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21784_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21786_gshared (Dictionary_2_t2824 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21786(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2824 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21786_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m21788_gshared (Dictionary_2_t2824 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m21788(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2824 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m21788_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21790_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21790(__this, method) (( Object_t* (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21790_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21792_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21792(__this, method) (( Object_t* (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21792_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m21794_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21794(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21794_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21796_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21796(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2824 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m21796_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21798_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m21798(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2824 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m21798_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m21800_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m21800(__this, ___key, method) (( bool (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m21800_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21802_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m21802(__this, ___key, method) (( void (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m21802_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21804_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21804(__this, method) (( bool (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21804_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21806_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21806(__this, method) (( Object_t * (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21806_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21808_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21808(__this, method) (( bool (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21808_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21810_gshared (Dictionary_2_t2824 * __this, KeyValuePair_2_t2825  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21810(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2824 *, KeyValuePair_2_t2825 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21810_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21812_gshared (Dictionary_2_t2824 * __this, KeyValuePair_2_t2825  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21812(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2824 *, KeyValuePair_2_t2825 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21812_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814_gshared (Dictionary_2_t2824 * __this, KeyValuePair_2U5BU5D_t3250* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2824 *, KeyValuePair_2U5BU5D_t3250*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21816_gshared (Dictionary_2_t2824 * __this, KeyValuePair_2_t2825  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21816(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2824 *, KeyValuePair_2_t2825 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21816_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21818_gshared (Dictionary_2_t2824 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21818(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2824 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m21818_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21820_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21820(__this, method) (( Object_t * (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21820_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21822_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21822(__this, method) (( Object_t* (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21822_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21824_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21824(__this, method) (( Object_t * (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21824_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m21826_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m21826(__this, method) (( int32_t (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_get_Count_m21826_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m21828_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m21828(__this, ___key, method) (( int32_t (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m21828_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m21830_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m21830(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2824 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m21830_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m21832_gshared (Dictionary_2_t2824 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m21832(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2824 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m21832_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m21834_gshared (Dictionary_2_t2824 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m21834(__this, ___size, method) (( void (*) (Dictionary_2_t2824 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m21834_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m21836_gshared (Dictionary_2_t2824 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m21836(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2824 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m21836_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2825  Dictionary_2_make_pair_m21838_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m21838(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2825  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m21838_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m21840_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m21840(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m21840_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m21842_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m21842(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m21842_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m21844_gshared (Dictionary_2_t2824 * __this, KeyValuePair_2U5BU5D_t3250* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m21844(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2824 *, KeyValuePair_2U5BU5D_t3250*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m21844_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Resize()
extern "C" void Dictionary_2_Resize_m21846_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m21846(__this, method) (( void (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_Resize_m21846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m21848_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m21848(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2824 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m21848_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Clear()
extern "C" void Dictionary_2_Clear_m21850_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m21850(__this, method) (( void (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_Clear_m21850_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m21852_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m21852(__this, ___key, method) (( bool (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m21852_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m21854_gshared (Dictionary_2_t2824 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m21854(__this, ___value, method) (( bool (*) (Dictionary_2_t2824 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m21854_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m21856_gshared (Dictionary_2_t2824 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m21856(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2824 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m21856_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m21858_gshared (Dictionary_2_t2824 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m21858(__this, ___sender, method) (( void (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m21858_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m21860_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m21860(__this, ___key, method) (( bool (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m21860_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m21862_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m21862(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2824 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m21862_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Keys()
extern "C" KeyCollection_t2828 * Dictionary_2_get_Keys_m21864_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m21864(__this, method) (( KeyCollection_t2828 * (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_get_Keys_m21864_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Values()
extern "C" ValueCollection_t2832 * Dictionary_2_get_Values_m21866_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m21866(__this, method) (( ValueCollection_t2832 * (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_get_Values_m21866_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m21868_gshared (Dictionary_2_t2824 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m21868(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m21868_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m21870_gshared (Dictionary_2_t2824 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m21870(__this, ___value, method) (( int32_t (*) (Dictionary_2_t2824 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m21870_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m21872_gshared (Dictionary_2_t2824 * __this, KeyValuePair_2_t2825  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m21872(__this, ___pair, method) (( bool (*) (Dictionary_2_t2824 *, KeyValuePair_2_t2825 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m21872_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
extern "C" Enumerator_t2830  Dictionary_2_GetEnumerator_m21874_gshared (Dictionary_2_t2824 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m21874(__this, method) (( Enumerator_t2830  (*) (Dictionary_2_t2824 *, const MethodInfo*))Dictionary_2_GetEnumerator_m21874_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t552  Dictionary_2_U3CCopyToU3Em__0_m21876_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m21876(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m21876_gshared)(__this /* static, unused */, ___key, ___value, method)
