﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t348;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.SoapServices/TypeInfo
struct  TypeInfo_t1922  : public Object_t
{
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices/TypeInfo::Attributes
	Hashtable_t348 * ___Attributes_0;
	// System.Collections.Hashtable System.Runtime.Remoting.SoapServices/TypeInfo::Elements
	Hashtable_t348 * ___Elements_1;
};
