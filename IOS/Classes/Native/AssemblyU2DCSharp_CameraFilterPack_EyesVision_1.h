﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.Texture2D
struct Texture2D_t9;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_EyesVision_1
struct  CameraFilterPack_EyesVision_1_t118  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_EyesVision_1::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_EyesVision_1::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_EyesVision_1::_EyeWave
	float ____EyeWave_4;
	// System.Single CameraFilterPack_EyesVision_1::_EyeSpeed
	float ____EyeSpeed_5;
	// System.Single CameraFilterPack_EyesVision_1::_EyeMove
	float ____EyeMove_6;
	// System.Single CameraFilterPack_EyesVision_1::_EyeBlink
	float ____EyeBlink_7;
	// UnityEngine.Material CameraFilterPack_EyesVision_1::SCMaterial
	Material_t2 * ___SCMaterial_8;
	// UnityEngine.Texture2D CameraFilterPack_EyesVision_1::Texture2
	Texture2D_t9 * ___Texture2_9;
};
