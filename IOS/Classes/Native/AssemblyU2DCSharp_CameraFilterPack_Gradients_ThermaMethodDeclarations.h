﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_Therma
struct CameraFilterPack_Gradients_Therma_t154;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_Therma::.ctor()
extern "C" void CameraFilterPack_Gradients_Therma__ctor_m995 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Therma::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_Therma_get_material_m996 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::Start()
extern "C" void CameraFilterPack_Gradients_Therma_Start_m997 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_Therma_OnRenderImage_m998 (CameraFilterPack_Gradients_Therma_t154 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::Update()
extern "C" void CameraFilterPack_Gradients_Therma_Update_m999 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Therma::OnDisable()
extern "C" void CameraFilterPack_Gradients_Therma_OnDisable_m1000 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
