﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// LightController
struct LightController_t343;
// System.Object
#include "mscorlib_System_Object.h"
// LightController/<WaitAndSetFlicker>c__IteratorC
struct  U3CWaitAndSetFlickerU3Ec__IteratorC_t345  : public Object_t
{
	// System.Single LightController/<WaitAndSetFlicker>c__IteratorC::time
	float ___time_0;
	// System.Boolean LightController/<WaitAndSetFlicker>c__IteratorC::val
	bool ___val_1;
	// System.Int32 LightController/<WaitAndSetFlicker>c__IteratorC::$PC
	int32_t ___U24PC_2;
	// System.Object LightController/<WaitAndSetFlicker>c__IteratorC::$current
	Object_t * ___U24current_3;
	// System.Single LightController/<WaitAndSetFlicker>c__IteratorC::<$>time
	float ___U3CU24U3Etime_4;
	// System.Boolean LightController/<WaitAndSetFlicker>c__IteratorC::<$>val
	bool ___U3CU24U3Eval_5;
	// LightController LightController/<WaitAndSetFlicker>c__IteratorC::<>f__this
	LightController_t343 * ___U3CU3Ef__this_6;
};
