﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t647;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t653;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t687;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m4682(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t647 *, UnityAction_1_t653 *, UnityAction_1_t653 *, const MethodInfo*))ObjectPool_1__ctor_m13999_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m18102(__this, method) (( int32_t (*) (ObjectPool_1_t647 *, const MethodInfo*))ObjectPool_1_get_countAll_m14001_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m18103(__this, ___value, method) (( void (*) (ObjectPool_1_t647 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m14003_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m18104(__this, method) (( int32_t (*) (ObjectPool_1_t647 *, const MethodInfo*))ObjectPool_1_get_countActive_m14005_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m18105(__this, method) (( int32_t (*) (ObjectPool_1_t647 *, const MethodInfo*))ObjectPool_1_get_countInactive_m14007_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m4689(__this, method) (( List_1_t687 * (*) (ObjectPool_1_t647 *, const MethodInfo*))ObjectPool_1_Get_m14009_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m4692(__this, ___element, method) (( void (*) (ObjectPool_1_t647 *, List_1_t687 *, const MethodInfo*))ObjectPool_1_Release_m14011_gshared)(__this, ___element, method)
