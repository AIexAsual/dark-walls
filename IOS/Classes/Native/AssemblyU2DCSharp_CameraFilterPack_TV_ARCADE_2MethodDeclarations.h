﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_ARCADE_2
struct CameraFilterPack_TV_ARCADE_2_t178;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_ARCADE_2::.ctor()
extern "C" void CameraFilterPack_TV_ARCADE_2__ctor_m1151 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_ARCADE_2::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_ARCADE_2_get_material_m1152 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::Start()
extern "C" void CameraFilterPack_TV_ARCADE_2_Start_m1153 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_ARCADE_2_OnRenderImage_m1154 (CameraFilterPack_TV_ARCADE_2_t178 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::OnValidate()
extern "C" void CameraFilterPack_TV_ARCADE_2_OnValidate_m1155 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::Update()
extern "C" void CameraFilterPack_TV_ARCADE_2_Update_m1156 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE_2::OnDisable()
extern "C" void CameraFilterPack_TV_ARCADE_2_OnDisable_m1157 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
