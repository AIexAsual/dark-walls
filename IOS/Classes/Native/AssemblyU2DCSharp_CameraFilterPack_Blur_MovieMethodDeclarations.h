﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Movie
struct CameraFilterPack_Blur_Movie_t54;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Movie::.ctor()
extern "C" void CameraFilterPack_Blur_Movie__ctor_m331 (CameraFilterPack_Blur_Movie_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Movie::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Movie_get_material_m332 (CameraFilterPack_Blur_Movie_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::Start()
extern "C" void CameraFilterPack_Blur_Movie_Start_m333 (CameraFilterPack_Blur_Movie_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Movie_OnRenderImage_m334 (CameraFilterPack_Blur_Movie_t54 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::OnValidate()
extern "C" void CameraFilterPack_Blur_Movie_OnValidate_m335 (CameraFilterPack_Blur_Movie_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::Update()
extern "C" void CameraFilterPack_Blur_Movie_Update_m336 (CameraFilterPack_Blur_Movie_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Movie::OnDisable()
extern "C" void CameraFilterPack_Blur_Movie_OnDisable_m337 (CameraFilterPack_Blur_Movie_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
