﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Vintage
struct CameraFilterPack_TV_Vintage_t199;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Vintage::.ctor()
extern "C" void CameraFilterPack_TV_Vintage__ctor_m1289 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Vintage::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Vintage_get_material_m1290 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::Start()
extern "C" void CameraFilterPack_TV_Vintage_Start_m1291 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Vintage_OnRenderImage_m1292 (CameraFilterPack_TV_Vintage_t199 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::OnValidate()
extern "C" void CameraFilterPack_TV_Vintage_OnValidate_m1293 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::Update()
extern "C" void CameraFilterPack_TV_Vintage_Update_m1294 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vintage::OnDisable()
extern "C" void CameraFilterPack_TV_Vintage_OnDisable_m1295 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
