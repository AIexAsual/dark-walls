﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>
struct Enumerator_t2473;
// System.Object
struct Object_t;
// UnityEngine.AudioClip
struct AudioClip_t472;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t584;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m16816(__this, ___l, method) (( void (*) (Enumerator_t2473 *, List_1_t584 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16817(__this, method) (( Object_t * (*) (Enumerator_t2473 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>::Dispose()
#define Enumerator_Dispose_m16818(__this, method) (( void (*) (Enumerator_t2473 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>::VerifyState()
#define Enumerator_VerifyState_m16819(__this, method) (( void (*) (Enumerator_t2473 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>::MoveNext()
#define Enumerator_MoveNext_m16820(__this, method) (( bool (*) (Enumerator_t2473 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>::get_Current()
#define Enumerator_get_Current_m16821(__this, method) (( AudioClip_t472 * (*) (Enumerator_t2473 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
