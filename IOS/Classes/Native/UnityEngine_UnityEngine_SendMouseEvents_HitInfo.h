﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.Camera
struct Camera_t14;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t1039 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t256 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t14 * ___camera_1;
};
