﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Light_Water2
struct  CameraFilterPack_Light_Water2_t158  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Light_Water2::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Light_Water2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Light_Water2::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Light_Water2::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Light_Water2::Speed
	float ___Speed_6;
	// System.Single CameraFilterPack_Light_Water2::Speed_X
	float ___Speed_X_7;
	// System.Single CameraFilterPack_Light_Water2::Speed_Y
	float ___Speed_Y_8;
	// System.Single CameraFilterPack_Light_Water2::Intensity
	float ___Intensity_9;
};
struct CameraFilterPack_Light_Water2_t158_StaticFields{
	// System.Single CameraFilterPack_Light_Water2::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Light_Water2::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Light_Water2::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Light_Water2::ChangeValue4
	float ___ChangeValue4_13;
};
