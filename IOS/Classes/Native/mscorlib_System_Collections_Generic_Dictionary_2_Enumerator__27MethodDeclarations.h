﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
struct Enumerator_t2900;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1463;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_31.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22625_gshared (Enumerator_t2900 * __this, Dictionary_2_t1463 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22625(__this, ___dictionary, method) (( void (*) (Enumerator_t2900 *, Dictionary_2_t1463 *, const MethodInfo*))Enumerator__ctor_m22625_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22626_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22626(__this, method) (( Object_t * (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22627_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22627(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22627_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22628_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22628(__this, method) (( Object_t * (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22628_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22629_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22629(__this, method) (( Object_t * (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22630_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22630(__this, method) (( bool (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_MoveNext_m22630_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2896  Enumerator_get_Current_m22631_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22631(__this, method) (( KeyValuePair_2_t2896  (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_get_Current_m22631_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m22632_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22632(__this, method) (( int32_t (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_get_CurrentKey_m22632_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m22633_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22633(__this, method) (( int32_t (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_get_CurrentValue_m22633_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m22634_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22634(__this, method) (( void (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_VerifyState_m22634_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22635_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22635(__this, method) (( void (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_VerifyCurrent_m22635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m22636_gshared (Enumerator_t2900 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22636(__this, method) (( void (*) (Enumerator_t2900 *, const MethodInfo*))Enumerator_Dispose_m22636_gshared)(__this, method)
