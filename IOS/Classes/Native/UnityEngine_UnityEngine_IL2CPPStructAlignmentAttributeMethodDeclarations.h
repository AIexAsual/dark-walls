﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.IL2CPPStructAlignmentAttribute
struct IL2CPPStructAlignmentAttribute_t1005;

// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m6149 (IL2CPPStructAlignmentAttribute_t1005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
