﻿#pragma once
#include <stdint.h>
// UnityEngine.Cloth
struct Cloth_t337;
// UnityEngine.Collider
struct Collider_t319;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InteractivePlasticBag
struct  InteractivePlasticBag_t335  : public MonoBehaviour_t4
{
	// UnityEngine.Cloth InteractivePlasticBag::cloth
	Cloth_t337 * ___cloth_2;
	// UnityEngine.Collider InteractivePlasticBag::col
	Collider_t319 * ___col_3;
};
