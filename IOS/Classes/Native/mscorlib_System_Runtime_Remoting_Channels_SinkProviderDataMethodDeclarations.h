﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Channels.SinkProviderData
struct SinkProviderData_t1856;
// System.Collections.IList
struct IList_t1487;
// System.Collections.IDictionary
struct IDictionary_t1462;
// System.String
struct String_t;

// System.Void System.Runtime.Remoting.Channels.SinkProviderData::.ctor(System.String)
extern "C" void SinkProviderData__ctor_m11323 (SinkProviderData_t1856 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList System.Runtime.Remoting.Channels.SinkProviderData::get_Children()
extern "C" Object_t * SinkProviderData_get_Children_m11324 (SinkProviderData_t1856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary System.Runtime.Remoting.Channels.SinkProviderData::get_Properties()
extern "C" Object_t * SinkProviderData_get_Properties_m11325 (SinkProviderData_t1856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
