﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Edge_Golden
struct CameraFilterPack_Edge_Golden_t114;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Edge_Golden::.ctor()
extern "C" void CameraFilterPack_Edge_Golden__ctor_m732 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Golden::get_material()
extern "C" Material_t2 * CameraFilterPack_Edge_Golden_get_material_m733 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::Start()
extern "C" void CameraFilterPack_Edge_Golden_Start_m734 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Edge_Golden_OnRenderImage_m735 (CameraFilterPack_Edge_Golden_t114 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::Update()
extern "C" void CameraFilterPack_Edge_Golden_Update_m736 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Golden::OnDisable()
extern "C" void CameraFilterPack_Edge_Golden_OnDisable_m737 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
