﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// testScript
struct testScript_t418;

// System.Void testScript::.ctor()
extern "C" void testScript__ctor_m2392 (testScript_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testScript::.cctor()
extern "C" void testScript__cctor_m2393 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// testScript testScript::get_Instance()
extern "C" testScript_t418 * testScript_get_Instance_m2394 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testScript::Update()
extern "C" void testScript_Update_m2395 (testScript_t418 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
