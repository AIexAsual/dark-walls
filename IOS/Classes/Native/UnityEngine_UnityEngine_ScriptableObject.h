﻿#pragma once
#include <stdint.h>
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.ScriptableObject
struct  ScriptableObject_t838  : public Object_t473
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t838_marshaled
{
};
