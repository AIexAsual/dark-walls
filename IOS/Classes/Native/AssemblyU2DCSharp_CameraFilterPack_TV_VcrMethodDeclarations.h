﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Vcr
struct CameraFilterPack_TV_Vcr_t196;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Vcr::.ctor()
extern "C" void CameraFilterPack_TV_Vcr__ctor_m1270 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Vcr::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Vcr_get_material_m1271 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::Start()
extern "C" void CameraFilterPack_TV_Vcr_Start_m1272 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Vcr_OnRenderImage_m1273 (CameraFilterPack_TV_Vcr_t196 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::OnValidate()
extern "C" void CameraFilterPack_TV_Vcr_OnValidate_m1274 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::Update()
extern "C" void CameraFilterPack_TV_Vcr_Update_m1275 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Vcr::OnDisable()
extern "C" void CameraFilterPack_TV_Vcr_OnDisable_m1276 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
