﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Single[]
struct SingleU5BU5D_t72;
// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t268;
// System.Int32[]
struct Int32U5BU5D_t269;
// BaseVRDevice
#include "AssemblyU2DCSharp_BaseVRDevice.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// BaseCardboardDevice
struct  BaseCardboardDevice_t270  : public BaseVRDevice_t236
{
	// System.Single[] BaseCardboardDevice::headData
	SingleU5BU5D_t72* ___headData_24;
	// System.Single[] BaseCardboardDevice::viewData
	SingleU5BU5D_t72* ___viewData_25;
	// System.Single[] BaseCardboardDevice::profileData
	SingleU5BU5D_t72* ___profileData_26;
	// UnityEngine.Matrix4x4 BaseCardboardDevice::headView
	Matrix4x4_t242  ___headView_27;
	// UnityEngine.Matrix4x4 BaseCardboardDevice::leftEyeView
	Matrix4x4_t242  ___leftEyeView_28;
	// UnityEngine.Matrix4x4 BaseCardboardDevice::rightEyeView
	Matrix4x4_t242  ___rightEyeView_29;
	// System.Collections.Generic.Queue`1<System.Int32> BaseCardboardDevice::eventQueue
	Queue_1_t268 * ___eventQueue_30;
	// System.Boolean BaseCardboardDevice::debugDisableNativeProjections
	bool ___debugDisableNativeProjections_31;
	// System.Boolean BaseCardboardDevice::debugDisableNativeDistortion
	bool ___debugDisableNativeDistortion_32;
	// System.Boolean BaseCardboardDevice::debugDisableNativeUILayer
	bool ___debugDisableNativeUILayer_33;
	// System.Int32[] BaseCardboardDevice::events
	Int32U5BU5D_t269* ___events_34;
};
