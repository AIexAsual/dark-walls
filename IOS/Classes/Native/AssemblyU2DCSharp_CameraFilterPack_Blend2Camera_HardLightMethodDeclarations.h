﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_HardLight
struct CameraFilterPack_Blend2Camera_HardLight_t27;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_HardLight::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_HardLight__ctor_m129 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_HardLight::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_HardLight_get_material_m130 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::Start()
extern "C" void CameraFilterPack_Blend2Camera_HardLight_Start_m131 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_HardLight_OnRenderImage_m132 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_HardLight_OnValidate_m133 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::Update()
extern "C" void CameraFilterPack_Blend2Camera_HardLight_Update_m134 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_HardLight_OnEnable_m135 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardLight::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_HardLight_OnDisable_m136 (CameraFilterPack_Blend2Camera_HardLight_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
