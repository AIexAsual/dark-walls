﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>
struct Enumerator_t2724;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t964;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16MethodDeclarations.h"
#define Enumerator__ctor_m20565(__this, ___dictionary, method) (( void (*) (Enumerator_t2724 *, Dictionary_2_t964 *, const MethodInfo*))Enumerator__ctor_m20463_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20566(__this, method) (( Object_t * (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20464_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20567(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20465_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20568(__this, method) (( Object_t * (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20466_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20569(__this, method) (( Object_t * (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20467_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m20570(__this, method) (( bool (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_MoveNext_m20468_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m20571(__this, method) (( KeyValuePair_2_t2721  (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_get_Current_m20469_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20572(__this, method) (( String_t* (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_get_CurrentKey_m20470_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20573(__this, method) (( int64_t (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_get_CurrentValue_m20471_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m20574(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_VerifyState_m20472_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20575(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_VerifyCurrent_m20473_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m20576(__this, method) (( void (*) (Enumerator_t2724 *, const MethodInfo*))Enumerator_Dispose_m20474_gshared)(__this, method)
