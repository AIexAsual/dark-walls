﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Space>
struct  Predicate_1_t2445  : public MulticastDelegate_t219
{
};
