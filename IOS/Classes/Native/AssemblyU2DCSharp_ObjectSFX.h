﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// ObjectSFX
struct  ObjectSFX_t383  : public Object_t
{
};
struct ObjectSFX_t383_StaticFields{
	// System.String ObjectSFX::EerieSounds1
	String_t* ___EerieSounds1_0;
	// System.String ObjectSFX::EerieSounds2
	String_t* ___EerieSounds2_1;
	// System.String ObjectSFX::EerieSounds3
	String_t* ___EerieSounds3_2;
	// System.String ObjectSFX::EerieSounds4
	String_t* ___EerieSounds4_3;
	// System.String ObjectSFX::EerieSounds5
	String_t* ___EerieSounds5_4;
	// System.String ObjectSFX::EerieSounds6
	String_t* ___EerieSounds6_5;
	// System.String ObjectSFX::EerieSounds7
	String_t* ___EerieSounds7_6;
	// System.String ObjectSFX::EerieSounds8
	String_t* ___EerieSounds8_7;
	// System.String ObjectSFX::EerieSounds9
	String_t* ___EerieSounds9_8;
	// System.String ObjectSFX::EerieSounds10
	String_t* ___EerieSounds10_9;
	// System.String ObjectSFX::EerieSounds11
	String_t* ___EerieSounds11_10;
	// System.String ObjectSFX::EerieSounds12
	String_t* ___EerieSounds12_11;
	// System.String ObjectSFX::EerieSounds13
	String_t* ___EerieSounds13_12;
	// System.String ObjectSFX::EerieSounds14
	String_t* ___EerieSounds14_13;
	// System.String ObjectSFX::EerieSounds15
	String_t* ___EerieSounds15_14;
	// System.String ObjectSFX::EerieSounds16
	String_t* ___EerieSounds16_15;
	// System.String ObjectSFX::EerieSounds17
	String_t* ___EerieSounds17_16;
	// System.String ObjectSFX::RadioSound
	String_t* ___RadioSound_17;
	// System.String ObjectSFX::Faucet
	String_t* ___Faucet_18;
	// System.String ObjectSFX::AmbientSound
	String_t* ___AmbientSound_19;
	// System.String ObjectSFX::Child
	String_t* ___Child_20;
	// System.String ObjectSFX::Child1
	String_t* ___Child1_21;
	// System.String ObjectSFX::Child2
	String_t* ___Child2_22;
	// System.String ObjectSFX::ChildRun
	String_t* ___ChildRun_23;
	// System.String ObjectSFX::FemaleCry
	String_t* ___FemaleCry_24;
	// System.String ObjectSFX::FemaleCough
	String_t* ___FemaleCough_25;
	// System.String ObjectSFX::FemaleScream
	String_t* ___FemaleScream_26;
	// System.String ObjectSFX::FemaleHumming
	String_t* ___FemaleHumming_27;
	// System.String ObjectSFX::FemaleBreathing1
	String_t* ___FemaleBreathing1_28;
	// System.String ObjectSFX::FemaleBreathing2
	String_t* ___FemaleBreathing2_29;
	// System.String ObjectSFX::DoorPounding
	String_t* ___DoorPounding_30;
	// System.String ObjectSFX::ChildWhispers
	String_t* ___ChildWhispers_31;
	// System.String ObjectSFX::BabyStopCry
	String_t* ___BabyStopCry_32;
	// System.String ObjectSFX::CreepyCrawl
	String_t* ___CreepyCrawl_33;
	// System.String ObjectSFX::MetalPounding
	String_t* ___MetalPounding_34;
	// System.String ObjectSFX::LightFlicker
	String_t* ___LightFlicker_35;
	// System.String ObjectSFX::PianoSlam
	String_t* ___PianoSlam_36;
	// System.String ObjectSFX::FlashLight
	String_t* ___FlashLight_37;
	// System.String ObjectSFX::OxygenTank
	String_t* ___OxygenTank_38;
	// System.String ObjectSFX::FaucetOn
	String_t* ___FaucetOn_39;
	// System.String ObjectSFX::Wheels
	String_t* ___Wheels_40;
	// System.String ObjectSFX::BabyCry
	String_t* ___BabyCry_41;
	// System.String ObjectSFX::OpenCabinet
	String_t* ___OpenCabinet_42;
	// System.String ObjectSFX::CorpsCloth
	String_t* ___CorpsCloth_43;
	// System.String ObjectSFX::Barefoot
	String_t* ___Barefoot_44;
	// System.String ObjectSFX::ISeeYou
	String_t* ___ISeeYou_45;
	// System.String ObjectSFX::BGM
	String_t* ___BGM_46;
	// System.String ObjectSFX::Whisper
	String_t* ___Whisper_47;
	// System.String ObjectSFX::Intro
	String_t* ___Intro_48;
	// System.String ObjectSFX::PlayerHeartBeat
	String_t* ___PlayerHeartBeat_49;
};
