﻿#pragma once
#include <stdint.h>
// System.Single[]
struct SingleU5BU5D_t72;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.AudioClip/PCMReaderCallback
struct  PCMReaderCallback_t936  : public MulticastDelegate_t219
{
};
