﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Ascii
struct CameraFilterPack_FX_Ascii_t122;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Ascii::.ctor()
extern "C" void CameraFilterPack_FX_Ascii__ctor_m784 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Ascii::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Ascii_get_material_m785 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Ascii::Start()
extern "C" void CameraFilterPack_FX_Ascii_Start_m786 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Ascii::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Ascii_OnRenderImage_m787 (CameraFilterPack_FX_Ascii_t122 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Ascii::Update()
extern "C" void CameraFilterPack_FX_Ascii_Update_m788 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Ascii::OnDisable()
extern "C" void CameraFilterPack_FX_Ascii_OnDisable_m789 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
