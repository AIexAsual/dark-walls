﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Manga2
struct CameraFilterPack_Drawing_Manga2_t103;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Manga2::.ctor()
extern "C" void CameraFilterPack_Drawing_Manga2__ctor_m657 (CameraFilterPack_Drawing_Manga2_t103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga2::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Manga2_get_material_m658 (CameraFilterPack_Drawing_Manga2_t103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga2::Start()
extern "C" void CameraFilterPack_Drawing_Manga2_Start_m659 (CameraFilterPack_Drawing_Manga2_t103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Manga2_OnRenderImage_m660 (CameraFilterPack_Drawing_Manga2_t103 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga2::OnValidate()
extern "C" void CameraFilterPack_Drawing_Manga2_OnValidate_m661 (CameraFilterPack_Drawing_Manga2_t103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga2::Update()
extern "C" void CameraFilterPack_Drawing_Manga2_Update_m662 (CameraFilterPack_Drawing_Manga2_t103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga2::OnDisable()
extern "C" void CameraFilterPack_Drawing_Manga2_OnDisable_m663 (CameraFilterPack_Drawing_Manga2_t103 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
