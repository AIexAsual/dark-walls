﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t377;
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t348;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// PlayerEvent
#include "AssemblyU2DCSharp_PlayerEvent.h"

// System.Void PlayerEventManager/OnStateChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void OnStateChangeHandler__ctor_m2172 (OnStateChangeHandler_t377 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager/OnStateChangeHandler::Invoke(PlayerEvent,System.Collections.Hashtable)
extern "C" void OnStateChangeHandler_Invoke_m2173 (OnStateChangeHandler_t377 * __this, int32_t ___ev, Hashtable_t348 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnStateChangeHandler_t377(Il2CppObject* delegate, int32_t ___ev, Hashtable_t348 * ___data);
// System.IAsyncResult PlayerEventManager/OnStateChangeHandler::BeginInvoke(PlayerEvent,System.Collections.Hashtable,System.AsyncCallback,System.Object)
extern "C" Object_t * OnStateChangeHandler_BeginInvoke_m2174 (OnStateChangeHandler_t377 * __this, int32_t ___ev, Hashtable_t348 * ___data, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager/OnStateChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void OnStateChangeHandler_EndInvoke_m2175 (OnStateChangeHandler_t377 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
