﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GameManager
struct GameManager_t354;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.GameObject
struct GameObject_t256;
// System.Collections.Hashtable
struct Hashtable_t348;
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEvent.h"
// PlayerEvent
#include "AssemblyU2DCSharp_PlayerEvent.h"

// System.Void GameManager::.ctor()
extern "C" void GameManager__ctor_m2059 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::.cctor()
extern "C" void GameManager__cctor_m2060 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameManager GameManager::get_Instance()
extern "C" GameManager_t354 * GameManager_get_Instance_m2061 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameManager::get_Debugger()
extern "C" String_t* GameManager_get_Debugger_m2062 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::set_Debugger(System.String)
extern "C" void GameManager_set_Debugger_m2063 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Start()
extern "C" void GameManager_Start_m2064 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::FindObjects()
extern "C" void GameManager_FindObjects_m2065 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager::ObjectSpawner(UnityEngine.GameObject,System.String)
extern "C" Object_t * GameManager_ObjectSpawner_m2066 (GameManager_t354 * __this, GameObject_t256 * ___go, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager::MyGameObjectHandler(System.String,System.Boolean)
extern "C" Object_t * GameManager_MyGameObjectHandler_m2067 (GameManager_t354 * __this, String_t* ___name, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::OnGUI()
extern "C" void GameManager_OnGUI_m2068 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Update()
extern "C" void GameManager_Update_m2069 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::OnMessageClose()
extern "C" void GameManager_OnMessageClose_m2070 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::OnRatePopUpClose(MNDialogResult)
extern "C" void GameManager_OnRatePopUpClose_m2071 (GameManager_t354 * __this, int32_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::OnEventChange(InteractiveEvent)
extern "C" void GameManager_OnEventChange_m2072 (GameManager_t354 * __this, int32_t ___ev, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::OnGazeInput(GazeInputEvent,System.String)
extern "C" void GameManager_OnGazeInput_m2073 (GameManager_t354 * __this, int32_t ___ev, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::OnPlayerStateChange(PlayerEvent,System.Collections.Hashtable)
extern "C" void GameManager_OnPlayerStateChange_m2074 (GameManager_t354 * __this, int32_t ___pe, Hashtable_t348 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::EnableGo(System.Single,System.Boolean)
extern "C" void GameManager_EnableGo_m2075 (GameManager_t354 * __this, float ___time, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::SetClear()
extern "C" void GameManager_SetClear_m2076 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::DizzyMode(System.Boolean)
extern "C" void GameManager_DizzyMode_m2077 (GameManager_t354 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::SetHWLights()
extern "C" void GameManager_SetHWLights_m2078 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::HideMama()
extern "C" void GameManager_HideMama_m2079 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::InstantiateObject(System.String)
extern "C" void GameManager_InstantiateObject_m2080 (GameManager_t354 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::StartGame()
extern "C" void GameManager_StartGame_m2081 (GameManager_t354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
