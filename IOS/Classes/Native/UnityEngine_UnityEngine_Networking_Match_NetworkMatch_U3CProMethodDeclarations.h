﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t2761;
// System.Object
struct Object_t;

// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m21018_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m21018(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m21018_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m21019_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m21019(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m21019_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m21020_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m21020(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m21020_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m21021_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m21021(__this, method) (( bool (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m21021_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m21022_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m21022(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2761 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m21022_gshared)(__this, method)
