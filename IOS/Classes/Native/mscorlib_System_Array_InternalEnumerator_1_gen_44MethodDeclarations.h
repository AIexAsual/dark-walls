﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>
struct InternalEnumerator_1_t2640;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t1032;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m19326(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2640 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19327(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2640 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::Dispose()
#define InternalEnumerator_1_Dispose_m19328(__this, method) (( void (*) (InternalEnumerator_1_t2640 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::MoveNext()
#define InternalEnumerator_1_MoveNext_m19329(__this, method) (( bool (*) (InternalEnumerator_1_t2640 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.Impl.UserProfile>::get_Current()
#define InternalEnumerator_1_get_Current_m19330(__this, method) (( UserProfile_t1032 * (*) (InternalEnumerator_1_t2640 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
