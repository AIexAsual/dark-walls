﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_Adjust_ColorRGB
struct CameraFilterPack_Colors_Adjust_ColorRGB_t69;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::.ctor()
extern "C" void CameraFilterPack_Colors_Adjust_ColorRGB__ctor_m433 (CameraFilterPack_Colors_Adjust_ColorRGB_t69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Adjust_ColorRGB::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_Adjust_ColorRGB_get_material_m434 (CameraFilterPack_Colors_Adjust_ColorRGB_t69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::Start()
extern "C" void CameraFilterPack_Colors_Adjust_ColorRGB_Start_m435 (CameraFilterPack_Colors_Adjust_ColorRGB_t69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_Adjust_ColorRGB_OnRenderImage_m436 (CameraFilterPack_Colors_Adjust_ColorRGB_t69 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::OnValidate()
extern "C" void CameraFilterPack_Colors_Adjust_ColorRGB_OnValidate_m437 (CameraFilterPack_Colors_Adjust_ColorRGB_t69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::Update()
extern "C" void CameraFilterPack_Colors_Adjust_ColorRGB_Update_m438 (CameraFilterPack_Colors_Adjust_ColorRGB_t69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Adjust_ColorRGB::OnDisable()
extern "C" void CameraFilterPack_Colors_Adjust_ColorRGB_OnDisable_m439 (CameraFilterPack_Colors_Adjust_ColorRGB_t69 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
