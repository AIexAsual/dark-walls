﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1463;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3012;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t2898;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t2902;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t2510;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t3270;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3271;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t3272;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_31.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m22545_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m22545(__this, method) (( void (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2__ctor_m22545_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22546_gshared (Dictionary_2_t1463 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22546(__this, ___comparer, method) (( void (*) (Dictionary_2_t1463 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22546_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m22547_gshared (Dictionary_2_t1463 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m22547(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1463 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22547_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22548_gshared (Dictionary_2_t1463 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m22548(__this, ___capacity, method) (( void (*) (Dictionary_2_t1463 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22548_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22549_gshared (Dictionary_2_t1463 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22549(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1463 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22549_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22550_gshared (Dictionary_2_t1463 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m22550(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1463 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m22550_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22551_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22551(__this, method) (( Object_t* (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22551_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22552_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22552(__this, method) (( Object_t* (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22552_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22553_gshared (Dictionary_2_t1463 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22553(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1463 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22553_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22554_gshared (Dictionary_2_t1463 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22554(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1463 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22554_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22555_gshared (Dictionary_2_t1463 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22555(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1463 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22555_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22556_gshared (Dictionary_2_t1463 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22556(__this, ___key, method) (( bool (*) (Dictionary_2_t1463 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22556_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22557_gshared (Dictionary_2_t1463 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22557(__this, ___key, method) (( void (*) (Dictionary_2_t1463 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22557_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22558_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22558(__this, method) (( bool (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22558_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22559_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22559(__this, method) (( Object_t * (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22560_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22560(__this, method) (( bool (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22560_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22561_gshared (Dictionary_2_t1463 * __this, KeyValuePair_2_t2896  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22561(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1463 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22561_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22562_gshared (Dictionary_2_t1463 * __this, KeyValuePair_2_t2896  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22562(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1463 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22562_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22563_gshared (Dictionary_2_t1463 * __this, KeyValuePair_2U5BU5D_t3271* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22563(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1463 *, KeyValuePair_2U5BU5D_t3271*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22563_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22564_gshared (Dictionary_2_t1463 * __this, KeyValuePair_2_t2896  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22564(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1463 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22564_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22565_gshared (Dictionary_2_t1463 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22565(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1463 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22565_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22566_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22566(__this, method) (( Object_t * (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22566_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22567_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22567(__this, method) (( Object_t* (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22567_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22568_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22568(__this, method) (( Object_t * (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22568_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22569_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22569(__this, method) (( int32_t (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_get_Count_m22569_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m22570_gshared (Dictionary_2_t1463 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22570(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1463 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m22570_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22571_gshared (Dictionary_2_t1463 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22571(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1463 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m22571_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22572_gshared (Dictionary_2_t1463 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22572(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1463 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22572_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22573_gshared (Dictionary_2_t1463 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22573(__this, ___size, method) (( void (*) (Dictionary_2_t1463 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22573_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22574_gshared (Dictionary_2_t1463 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22574(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1463 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22574_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2896  Dictionary_2_make_pair_m22575_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22575(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2896  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m22575_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m22576_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22576(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m22576_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m22577_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22577(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m22577_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22578_gshared (Dictionary_2_t1463 * __this, KeyValuePair_2U5BU5D_t3271* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22578(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1463 *, KeyValuePair_2U5BU5D_t3271*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22578_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m22579_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22579(__this, method) (( void (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_Resize_m22579_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22580_gshared (Dictionary_2_t1463 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22580(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1463 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m22580_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m22581_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22581(__this, method) (( void (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_Clear_m22581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22582_gshared (Dictionary_2_t1463 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22582(__this, ___key, method) (( bool (*) (Dictionary_2_t1463 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m22582_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22583_gshared (Dictionary_2_t1463 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22583(__this, ___value, method) (( bool (*) (Dictionary_2_t1463 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m22583_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22584_gshared (Dictionary_2_t1463 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22584(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1463 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m22584_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22585_gshared (Dictionary_2_t1463 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22585(__this, ___sender, method) (( void (*) (Dictionary_2_t1463 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22585_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22586_gshared (Dictionary_2_t1463 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22586(__this, ___key, method) (( bool (*) (Dictionary_2_t1463 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m22586_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22587_gshared (Dictionary_2_t1463 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22587(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1463 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m22587_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t2898 * Dictionary_2_get_Keys_m22588_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22588(__this, method) (( KeyCollection_t2898 * (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_get_Keys_m22588_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t2902 * Dictionary_2_get_Values_m22589_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22589(__this, method) (( ValueCollection_t2902 * (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_get_Values_m22589_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m22590_gshared (Dictionary_2_t1463 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22590(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1463 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22590_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m22591_gshared (Dictionary_2_t1463 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22591(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1463 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22591_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22592_gshared (Dictionary_2_t1463 * __this, KeyValuePair_2_t2896  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22592(__this, ___pair, method) (( bool (*) (Dictionary_2_t1463 *, KeyValuePair_2_t2896 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22592_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2900  Dictionary_2_GetEnumerator_m22593_gshared (Dictionary_2_t1463 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22593(__this, method) (( Enumerator_t2900  (*) (Dictionary_2_t1463 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22593_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t552  Dictionary_2_U3CCopyToU3Em__0_m22594_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22594(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22594_gshared)(__this /* static, unused */, ___key, ___value, method)
