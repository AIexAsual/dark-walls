﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_HardMix
struct CameraFilterPack_Blend2Camera_HardMix_t28;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_HardMix::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_HardMix__ctor_m137 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_HardMix::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_HardMix_get_material_m138 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::Start()
extern "C" void CameraFilterPack_Blend2Camera_HardMix_Start_m139 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_HardMix_OnRenderImage_m140 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_HardMix_OnValidate_m141 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::Update()
extern "C" void CameraFilterPack_Blend2Camera_HardMix_Update_m142 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_HardMix_OnEnable_m143 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_HardMix::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_HardMix_OnDisable_m144 (CameraFilterPack_Blend2Camera_HardMix_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
