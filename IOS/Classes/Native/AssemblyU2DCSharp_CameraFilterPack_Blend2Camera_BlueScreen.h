﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// CameraFilterPack_Blend2Camera_BlueScreen
struct  CameraFilterPack_Blend2Camera_BlueScreen_t17  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_Blend2Camera_BlueScreen::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_BlueScreen::SCShader
	Shader_t1 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_BlueScreen::Camera2
	Camera_t14 * ___Camera2_4;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::TimeX
	float ___TimeX_5;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_BlueScreen::SCMaterial
	Material_t2 * ___SCMaterial_6;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::SwitchCameraToCamera2
	float ___SwitchCameraToCamera2_7;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::BlendFX
	float ___BlendFX_8;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::Adjust
	float ___Adjust_9;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::Precision
	float ___Precision_10;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::Luminosity
	float ___Luminosity_11;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::Change_Red
	float ___Change_Red_12;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::Change_Green
	float ___Change_Green_13;
	// System.Single CameraFilterPack_Blend2Camera_BlueScreen::Change_Blue
	float ___Change_Blue_14;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_BlueScreen::Camera2tex
	RenderTexture_t15 * ___Camera2tex_15;
	// UnityEngine.Vector2 CameraFilterPack_Blend2Camera_BlueScreen::ScreenSize
	Vector2_t7  ___ScreenSize_16;
};
