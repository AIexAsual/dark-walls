﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Light_Rainbow2
struct CameraFilterPack_Light_Rainbow2_t156;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Light_Rainbow2::.ctor()
extern "C" void CameraFilterPack_Light_Rainbow2__ctor_m1008 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Rainbow2::get_material()
extern "C" Material_t2 * CameraFilterPack_Light_Rainbow2_get_material_m1009 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::Start()
extern "C" void CameraFilterPack_Light_Rainbow2_Start_m1010 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Light_Rainbow2_OnRenderImage_m1011 (CameraFilterPack_Light_Rainbow2_t156 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::OnValidate()
extern "C" void CameraFilterPack_Light_Rainbow2_OnValidate_m1012 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::Update()
extern "C" void CameraFilterPack_Light_Rainbow2_Update_m1013 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow2::OnDisable()
extern "C" void CameraFilterPack_Light_Rainbow2_OnDisable_m1014 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
