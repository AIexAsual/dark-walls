﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.CreateMatchResponse
extern TypeInfo CreateMatchResponse_t966_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponseMethodDeclarations.h"
static const EncodedMethodIndex CreateMatchResponse_t966_VTable[5] = 
{
	626,
	601,
	627,
	1408,
	1409,
};
extern const Il2CppType IResponse_t3400_0_0_0;
static Il2CppInterfaceOffsetPair CreateMatchResponse_t966_InterfacesOffsets[] = 
{
	{ &IResponse_t3400_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CreateMatchResponse_t966_0_0_0;
extern const Il2CppType CreateMatchResponse_t966_1_0_0;
extern const Il2CppType BasicResponse_t963_0_0_0;
struct CreateMatchResponse_t966;
const Il2CppTypeDefinitionMetadata CreateMatchResponse_t966_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CreateMatchResponse_t966_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t963_0_0_0/* parent */
	, CreateMatchResponse_t966_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4596/* fieldStart */
	, 5628/* methodStart */
	, -1/* eventStart */
	, 1106/* propertyStart */

};
TypeInfo CreateMatchResponse_t966_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CreateMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &CreateMatchResponse_t966_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CreateMatchResponse_t966_0_0_0/* byval_arg */
	, &CreateMatchResponse_t966_1_0_0/* this_arg */
	, &CreateMatchResponse_t966_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CreateMatchResponse_t966)/* instance_size */
	, sizeof (CreateMatchResponse_t966)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.JoinMatchRequest
extern TypeInfo JoinMatchRequest_t967_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex JoinMatchRequest_t967_VTable[4] = 
{
	626,
	601,
	627,
	1410,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JoinMatchRequest_t967_0_0_0;
extern const Il2CppType JoinMatchRequest_t967_1_0_0;
extern const Il2CppType Request_t960_0_0_0;
struct JoinMatchRequest_t967;
const Il2CppTypeDefinitionMetadata JoinMatchRequest_t967_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t960_0_0_0/* parent */
	, JoinMatchRequest_t967_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4602/* fieldStart */
	, 5643/* methodStart */
	, -1/* eventStart */
	, 1112/* propertyStart */

};
TypeInfo JoinMatchRequest_t967_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JoinMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &JoinMatchRequest_t967_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &JoinMatchRequest_t967_0_0_0/* byval_arg */
	, &JoinMatchRequest_t967_1_0_0/* this_arg */
	, &JoinMatchRequest_t967_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JoinMatchRequest_t967)/* instance_size */
	, sizeof (JoinMatchRequest_t967)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.JoinMatchResponse
extern TypeInfo JoinMatchResponse_t968_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponseMethodDeclarations.h"
static const EncodedMethodIndex JoinMatchResponse_t968_VTable[5] = 
{
	626,
	601,
	627,
	1411,
	1412,
};
static Il2CppInterfaceOffsetPair JoinMatchResponse_t968_InterfacesOffsets[] = 
{
	{ &IResponse_t3400_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JoinMatchResponse_t968_0_0_0;
extern const Il2CppType JoinMatchResponse_t968_1_0_0;
struct JoinMatchResponse_t968;
const Il2CppTypeDefinitionMetadata JoinMatchResponse_t968_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, JoinMatchResponse_t968_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t963_0_0_0/* parent */
	, JoinMatchResponse_t968_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4604/* fieldStart */
	, 5649/* methodStart */
	, -1/* eventStart */
	, 1114/* propertyStart */

};
TypeInfo JoinMatchResponse_t968_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JoinMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &JoinMatchResponse_t968_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &JoinMatchResponse_t968_0_0_0/* byval_arg */
	, &JoinMatchResponse_t968_1_0_0/* this_arg */
	, &JoinMatchResponse_t968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JoinMatchResponse_t968)/* instance_size */
	, sizeof (JoinMatchResponse_t968)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Match.DestroyMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.DestroyMatchRequest
extern TypeInfo DestroyMatchRequest_t969_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.DestroyMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex DestroyMatchRequest_t969_VTable[4] = 
{
	626,
	601,
	627,
	1413,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DestroyMatchRequest_t969_0_0_0;
extern const Il2CppType DestroyMatchRequest_t969_1_0_0;
struct DestroyMatchRequest_t969;
const Il2CppTypeDefinitionMetadata DestroyMatchRequest_t969_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t960_0_0_0/* parent */
	, DestroyMatchRequest_t969_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4610/* fieldStart */
	, 5664/* methodStart */
	, -1/* eventStart */
	, 1120/* propertyStart */

};
TypeInfo DestroyMatchRequest_t969_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DestroyMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &DestroyMatchRequest_t969_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DestroyMatchRequest_t969_0_0_0/* byval_arg */
	, &DestroyMatchRequest_t969_1_0_0/* this_arg */
	, &DestroyMatchRequest_t969_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DestroyMatchRequest_t969)/* instance_size */
	, sizeof (DestroyMatchRequest_t969)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.DropConnectionRequest
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionReque.h"
// Metadata Definition UnityEngine.Networking.Match.DropConnectionRequest
extern TypeInfo DropConnectionRequest_t970_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.DropConnectionRequest
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionRequeMethodDeclarations.h"
static const EncodedMethodIndex DropConnectionRequest_t970_VTable[4] = 
{
	626,
	601,
	627,
	1414,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DropConnectionRequest_t970_0_0_0;
extern const Il2CppType DropConnectionRequest_t970_1_0_0;
struct DropConnectionRequest_t970;
const Il2CppTypeDefinitionMetadata DropConnectionRequest_t970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t960_0_0_0/* parent */
	, DropConnectionRequest_t970_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4611/* fieldStart */
	, 5668/* methodStart */
	, -1/* eventStart */
	, 1121/* propertyStart */

};
TypeInfo DropConnectionRequest_t970_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DropConnectionRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &DropConnectionRequest_t970_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DropConnectionRequest_t970_0_0_0/* byval_arg */
	, &DropConnectionRequest_t970_1_0_0/* this_arg */
	, &DropConnectionRequest_t970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DropConnectionRequest_t970)/* instance_size */
	, sizeof (DropConnectionRequest_t970)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ListMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequest.h"
// Metadata Definition UnityEngine.Networking.Match.ListMatchRequest
extern TypeInfo ListMatchRequest_t971_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ListMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequestMethodDeclarations.h"
static const EncodedMethodIndex ListMatchRequest_t971_VTable[4] = 
{
	626,
	601,
	627,
	1415,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ListMatchRequest_t971_0_0_0;
extern const Il2CppType ListMatchRequest_t971_1_0_0;
struct ListMatchRequest_t971;
const Il2CppTypeDefinitionMetadata ListMatchRequest_t971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Request_t960_0_0_0/* parent */
	, ListMatchRequest_t971_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4613/* fieldStart */
	, 5674/* methodStart */
	, -1/* eventStart */
	, 1123/* propertyStart */

};
TypeInfo ListMatchRequest_t971_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListMatchRequest"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &ListMatchRequest_t971_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ListMatchRequest_t971_0_0_0/* byval_arg */
	, &ListMatchRequest_t971_1_0_0/* this_arg */
	, &ListMatchRequest_t971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListMatchRequest_t971)/* instance_size */
	, sizeof (ListMatchRequest_t971)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.MatchDirectConnectInfo
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectI.h"
// Metadata Definition UnityEngine.Networking.Match.MatchDirectConnectInfo
extern TypeInfo MatchDirectConnectInfo_t972_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectIMethodDeclarations.h"
static const EncodedMethodIndex MatchDirectConnectInfo_t972_VTable[5] = 
{
	626,
	601,
	627,
	1416,
	1417,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MatchDirectConnectInfo_t972_0_0_0;
extern const Il2CppType MatchDirectConnectInfo_t972_1_0_0;
extern const Il2CppType ResponseBase_t961_0_0_0;
struct MatchDirectConnectInfo_t972;
const Il2CppTypeDefinitionMetadata MatchDirectConnectInfo_t972_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ResponseBase_t961_0_0_0/* parent */
	, MatchDirectConnectInfo_t972_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4619/* fieldStart */
	, 5685/* methodStart */
	, -1/* eventStart */
	, 1129/* propertyStart */

};
TypeInfo MatchDirectConnectInfo_t972_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchDirectConnectInfo"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &MatchDirectConnectInfo_t972_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchDirectConnectInfo_t972_0_0_0/* byval_arg */
	, &MatchDirectConnectInfo_t972_1_0_0/* this_arg */
	, &MatchDirectConnectInfo_t972_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchDirectConnectInfo_t972)/* instance_size */
	, sizeof (MatchDirectConnectInfo_t972)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.MatchDesc
#include "UnityEngine_UnityEngine_Networking_Match_MatchDesc.h"
// Metadata Definition UnityEngine.Networking.Match.MatchDesc
extern TypeInfo MatchDesc_t974_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.MatchDesc
#include "UnityEngine_UnityEngine_Networking_Match_MatchDescMethodDeclarations.h"
static const EncodedMethodIndex MatchDesc_t974_VTable[5] = 
{
	626,
	601,
	627,
	1418,
	1419,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MatchDesc_t974_0_0_0;
extern const Il2CppType MatchDesc_t974_1_0_0;
struct MatchDesc_t974;
const Il2CppTypeDefinitionMetadata MatchDesc_t974_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ResponseBase_t961_0_0_0/* parent */
	, MatchDesc_t974_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4622/* fieldStart */
	, 5694/* methodStart */
	, -1/* eventStart */
	, 1132/* propertyStart */

};
TypeInfo MatchDesc_t974_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatchDesc"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &MatchDesc_t974_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatchDesc_t974_0_0_0/* byval_arg */
	, &MatchDesc_t974_1_0_0/* this_arg */
	, &MatchDesc_t974_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatchDesc_t974)/* instance_size */
	, sizeof (MatchDesc_t974)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 9/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.ListMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponse.h"
// Metadata Definition UnityEngine.Networking.Match.ListMatchResponse
extern TypeInfo ListMatchResponse_t976_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.ListMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponseMethodDeclarations.h"
static const EncodedMethodIndex ListMatchResponse_t976_VTable[5] = 
{
	626,
	601,
	627,
	1420,
	1421,
};
static Il2CppInterfaceOffsetPair ListMatchResponse_t976_InterfacesOffsets[] = 
{
	{ &IResponse_t3400_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ListMatchResponse_t976_0_0_0;
extern const Il2CppType ListMatchResponse_t976_1_0_0;
struct ListMatchResponse_t976;
const Il2CppTypeDefinitionMetadata ListMatchResponse_t976_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ListMatchResponse_t976_InterfacesOffsets/* interfaceOffsets */
	, &BasicResponse_t963_0_0_0/* parent */
	, ListMatchResponse_t976_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4631/* fieldStart */
	, 5712/* methodStart */
	, -1/* eventStart */
	, 1141/* propertyStart */

};
TypeInfo ListMatchResponse_t976_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ListMatchResponse"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &ListMatchResponse_t976_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ListMatchResponse_t976_0_0_0/* byval_arg */
	, &ListMatchResponse_t976_1_0_0/* this_arg */
	, &ListMatchResponse_t976_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ListMatchResponse_t976)/* instance_size */
	, sizeof (ListMatchResponse_t976)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
// Metadata Definition UnityEngine.Networking.Types.AppID
extern TypeInfo AppID_t977_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppIDMethodDeclarations.h"
static const EncodedMethodIndex AppID_t977_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair AppID_t977_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AppID_t977_0_0_0;
extern const Il2CppType AppID_t977_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.UInt64
#include "mscorlib_System_UInt64.h"
extern TypeInfo UInt64_t1134_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AppID_t977_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppID_t977_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AppID_t977_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4632/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppID_t977_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt64_t1134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2180/* custom_attributes_cache */
	, &AppID_t977_0_0_0/* byval_arg */
	, &AppID_t977_1_0_0/* this_arg */
	, &AppID_t977_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppID_t977)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AppID_t977)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
// Metadata Definition UnityEngine.Networking.Types.SourceID
extern TypeInfo SourceID_t978_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceIDMethodDeclarations.h"
static const EncodedMethodIndex SourceID_t978_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SourceID_t978_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SourceID_t978_0_0_0;
extern const Il2CppType SourceID_t978_1_0_0;
const Il2CppTypeDefinitionMetadata SourceID_t978_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SourceID_t978_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SourceID_t978_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4634/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SourceID_t978_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SourceID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt64_t1134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2181/* custom_attributes_cache */
	, &SourceID_t978_0_0_0/* byval_arg */
	, &SourceID_t978_1_0_0/* this_arg */
	, &SourceID_t978_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SourceID_t978)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SourceID_t978)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
// Metadata Definition UnityEngine.Networking.Types.NetworkID
extern TypeInfo NetworkID_t979_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkIDMethodDeclarations.h"
static const EncodedMethodIndex NetworkID_t979_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair NetworkID_t979_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkID_t979_0_0_0;
extern const Il2CppType NetworkID_t979_1_0_0;
const Il2CppTypeDefinitionMetadata NetworkID_t979_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NetworkID_t979_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, NetworkID_t979_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4636/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NetworkID_t979_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt64_t1134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2182/* custom_attributes_cache */
	, &NetworkID_t979_0_0_0/* byval_arg */
	, &NetworkID_t979_1_0_0/* this_arg */
	, &NetworkID_t979_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkID_t979)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NetworkID_t979)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint64_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
// Metadata Definition UnityEngine.Networking.Types.NodeID
extern TypeInfo NodeID_t980_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeIDMethodDeclarations.h"
static const EncodedMethodIndex NodeID_t980_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair NodeID_t980_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NodeID_t980_0_0_0;
extern const Il2CppType NodeID_t980_1_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t1122_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata NodeID_t980_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NodeID_t980_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, NodeID_t980_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4638/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NodeID_t980_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NodeID"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &UInt16_t1122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2183/* custom_attributes_cache */
	, &NodeID_t980_0_0_0/* byval_arg */
	, &NodeID_t980_1_0_0/* this_arg */
	, &NodeID_t980_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NodeID_t980)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NodeID_t980)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint16_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Networking.Types.NetworkAccessToken
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessToken.h"
// Metadata Definition UnityEngine.Networking.Types.NetworkAccessToken
extern TypeInfo NetworkAccessToken_t981_il2cpp_TypeInfo;
// UnityEngine.Networking.Types.NetworkAccessToken
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessTokenMethodDeclarations.h"
static const EncodedMethodIndex NetworkAccessToken_t981_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkAccessToken_t981_0_0_0;
extern const Il2CppType NetworkAccessToken_t981_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct NetworkAccessToken_t981;
const Il2CppTypeDefinitionMetadata NetworkAccessToken_t981_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NetworkAccessToken_t981_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4640/* fieldStart */
	, 5717/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NetworkAccessToken_t981_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkAccessToken"/* name */
	, "UnityEngine.Networking.Types"/* namespaze */
	, NULL/* methods */
	, &NetworkAccessToken_t981_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NetworkAccessToken_t981_0_0_0/* byval_arg */
	, &NetworkAccessToken_t981_1_0_0/* this_arg */
	, &NetworkAccessToken_t981_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkAccessToken_t981)/* instance_size */
	, sizeof (NetworkAccessToken_t981)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Utility
#include "UnityEngine_UnityEngine_Networking_Utility.h"
// Metadata Definition UnityEngine.Networking.Utility
extern TypeInfo Utility_t983_il2cpp_TypeInfo;
// UnityEngine.Networking.Utility
#include "UnityEngine_UnityEngine_Networking_UtilityMethodDeclarations.h"
static const EncodedMethodIndex Utility_t983_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Utility_t983_0_0_0;
extern const Il2CppType Utility_t983_1_0_0;
struct Utility_t983;
const Il2CppTypeDefinitionMetadata Utility_t983_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Utility_t983_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4641/* fieldStart */
	, 5719/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Utility_t983_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Utility"/* name */
	, "UnityEngine.Networking"/* namespaze */
	, NULL/* methods */
	, &Utility_t983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Utility_t983_0_0_0/* byval_arg */
	, &Utility_t983_1_0_0/* this_arg */
	, &Utility_t983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Utility_t983)/* instance_size */
	, sizeof (Utility_t983)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Utility_t983_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Networking.Match.NetworkMatch
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch.h"
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch
extern TypeInfo NetworkMatch_t984_il2cpp_TypeInfo;
// UnityEngine.Networking.Match.NetworkMatch
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatchMethodDeclarations.h"
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t3652_0_0_0;
extern const Il2CppRGCTXDefinition NetworkMatch_ProcessMatchResponse_m24199_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6538 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5454 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ResponseDelegate_1_t3401_0_0_0;
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_0_0_0;
static const Il2CppType* NetworkMatch_t984_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ResponseDelegate_1_t3401_0_0_0,
	&U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_0_0_0,
};
static const EncodedMethodIndex NetworkMatch_t984_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType NetworkMatch_t984_0_0_0;
extern const Il2CppType NetworkMatch_t984_1_0_0;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
struct NetworkMatch_t984;
const Il2CppTypeDefinitionMetadata NetworkMatch_t984_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NetworkMatch_t984_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, NetworkMatch_t984_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4646/* fieldStart */
	, 5724/* methodStart */
	, -1/* eventStart */
	, 1142/* propertyStart */

};
TypeInfo NetworkMatch_t984_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetworkMatch"/* name */
	, "UnityEngine.Networking.Match"/* namespaze */
	, NULL/* methods */
	, &NetworkMatch_t984_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NetworkMatch_t984_0_0_0/* byval_arg */
	, &NetworkMatch_t984_1_0_0/* this_arg */
	, &NetworkMatch_t984_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetworkMatch_t984)/* instance_size */
	, sizeof (NetworkMatch_t984)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1
extern TypeInfo ResponseDelegate_1_t3401_il2cpp_TypeInfo;
static const EncodedMethodIndex ResponseDelegate_1_t3401_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1422,
	1423,
	1424,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair ResponseDelegate_1_t3401_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ResponseDelegate_1_t3401_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct ResponseDelegate_1_t3401;
const Il2CppTypeDefinitionMetadata ResponseDelegate_1_t3401_DefinitionMetadata = 
{
	&NetworkMatch_t984_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResponseDelegate_1_t3401_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ResponseDelegate_1_t3401_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5739/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResponseDelegate_1_t3401_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResponseDelegate`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ResponseDelegate_1_t3401_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResponseDelegate_1_t3401_0_0_0/* byval_arg */
	, &ResponseDelegate_1_t3401_1_0_0/* this_arg */
	, &ResponseDelegate_1_t3401_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 40/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1
extern TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_VTable[8] = 
{
	626,
	601,
	627,
	628,
	1425,
	1426,
	1427,
	1428,
};
extern const Il2CppType IDisposable_t538_0_0_0;
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IEnumerator_1_t2280_0_0_0;
static const Il2CppType* U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &IEnumerator_t464_0_0_0, 5},
	{ &IEnumerator_1_t2280_0_0_0, 7},
};
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4171 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5455 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5456 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_1_0_0;
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t3402;
const Il2CppTypeDefinitionMetadata U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_DefinitionMetadata = 
{
	&NetworkMatch_t984_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_InterfacesTypeInfos/* implementedInterfaces */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_VTable/* vtableMethods */
	, U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_RGCTXData/* rgctxDefinition */
	, 4648/* fieldStart */
	, 5743/* methodStart */
	, -1/* eventStart */
	, 1143/* propertyStart */

};
TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ProcessMatchResponse>c__Iterator0`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2185/* custom_attributes_cache */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_0_0_0/* byval_arg */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_1_0_0/* this_arg */
	, &U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 41/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// SimpleJson.JsonArray
#include "UnityEngine_SimpleJson_JsonArray.h"
// Metadata Definition SimpleJson.JsonArray
extern TypeInfo JsonArray_t985_il2cpp_TypeInfo;
// SimpleJson.JsonArray
#include "UnityEngine_SimpleJson_JsonArrayMethodDeclarations.h"
static const EncodedMethodIndex JsonArray_t985_VTable[33] = 
{
	626,
	601,
	627,
	1429,
	2147485078,
	2147485079,
	2147485080,
	2147485081,
	2147485082,
	2147485083,
	2147485084,
	2147485085,
	2147485086,
	2147485087,
	2147485088,
	2147485089,
	2147485090,
	2147485091,
	2147485092,
	2147485093,
	2147485079,
	2147485094,
	2147485095,
	2147485088,
	2147485096,
	2147485097,
	2147485098,
	2147485099,
	2147485100,
	2147485101,
	2147485093,
	2147485102,
	2147485103,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
extern const Il2CppType ICollection_t1528_0_0_0;
extern const Il2CppType IList_t1487_0_0_0;
extern const Il2CppType ICollection_1_t1094_0_0_0;
extern const Il2CppType IEnumerable_1_t515_0_0_0;
extern const Il2CppType IList_1_t565_0_0_0;
static Il2CppInterfaceOffsetPair JsonArray_t985_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
	{ &ICollection_1_t1094_0_0_0, 20},
	{ &IEnumerable_1_t515_0_0_0, 27},
	{ &IList_1_t565_0_0_0, 28},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JsonArray_t985_0_0_0;
extern const Il2CppType JsonArray_t985_1_0_0;
extern const Il2CppType List_1_t779_0_0_0;
struct JsonArray_t985;
const Il2CppTypeDefinitionMetadata JsonArray_t985_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, JsonArray_t985_InterfacesOffsets/* interfaceOffsets */
	, &List_1_t779_0_0_0/* parent */
	, JsonArray_t985_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5748/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo JsonArray_t985_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JsonArray"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &JsonArray_t985_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2189/* custom_attributes_cache */
	, &JsonArray_t985_0_0_0/* byval_arg */
	, &JsonArray_t985_1_0_0/* this_arg */
	, &JsonArray_t985_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JsonArray_t985)/* instance_size */
	, sizeof (JsonArray_t985)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 6/* interface_offsets_count */

};
// SimpleJson.JsonObject
#include "UnityEngine_SimpleJson_JsonObject.h"
// Metadata Definition SimpleJson.JsonObject
extern TypeInfo JsonObject_t986_il2cpp_TypeInfo;
// SimpleJson.JsonObject
#include "UnityEngine_SimpleJson_JsonObjectMethodDeclarations.h"
static const EncodedMethodIndex JsonObject_t986_VTable[19] = 
{
	626,
	601,
	627,
	1456,
	1457,
	1458,
	1459,
	1460,
	1461,
	1462,
	1463,
	1464,
	1465,
	1466,
	1467,
	1468,
	1469,
	1470,
	1471,
};
extern const Il2CppType IDictionary_2_t1088_0_0_0;
extern const Il2CppType IEnumerable_1_t3654_0_0_0;
extern const Il2CppType ICollection_1_t3655_0_0_0;
static const Il2CppType* JsonObject_t986_InterfacesTypeInfos[] = 
{
	&IDictionary_2_t1088_0_0_0,
	&IEnumerable_1_t3654_0_0_0,
	&IEnumerable_t1097_0_0_0,
	&ICollection_1_t3655_0_0_0,
};
static Il2CppInterfaceOffsetPair JsonObject_t986_InterfacesOffsets[] = 
{
	{ &IDictionary_2_t1088_0_0_0, 4},
	{ &IEnumerable_1_t3654_0_0_0, 10},
	{ &IEnumerable_t1097_0_0_0, 11},
	{ &ICollection_1_t3655_0_0_0, 12},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType JsonObject_t986_0_0_0;
extern const Il2CppType JsonObject_t986_1_0_0;
struct JsonObject_t986;
const Il2CppTypeDefinitionMetadata JsonObject_t986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, JsonObject_t986_InterfacesTypeInfos/* implementedInterfaces */
	, JsonObject_t986_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, JsonObject_t986_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4658/* fieldStart */
	, 5750/* methodStart */
	, -1/* eventStart */
	, 1145/* propertyStart */

};
TypeInfo JsonObject_t986_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "JsonObject"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &JsonObject_t986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2190/* custom_attributes_cache */
	, &JsonObject_t986_0_0_0/* byval_arg */
	, &JsonObject_t986_1_0_0/* this_arg */
	, &JsonObject_t986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (JsonObject_t986)/* instance_size */
	, sizeof (JsonObject_t986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 5/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.SimpleJson
#include "UnityEngine_SimpleJson_SimpleJson.h"
// Metadata Definition SimpleJson.SimpleJson
extern TypeInfo SimpleJson_t989_il2cpp_TypeInfo;
// SimpleJson.SimpleJson
#include "UnityEngine_SimpleJson_SimpleJsonMethodDeclarations.h"
static const EncodedMethodIndex SimpleJson_t989_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SimpleJson_t989_0_0_0;
extern const Il2CppType SimpleJson_t989_1_0_0;
struct SimpleJson_t989;
const Il2CppTypeDefinitionMetadata SimpleJson_t989_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SimpleJson_t989_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4659/* fieldStart */
	, 5767/* methodStart */
	, -1/* eventStart */
	, 1150/* propertyStart */

};
TypeInfo SimpleJson_t989_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleJson"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &SimpleJson_t989_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2191/* custom_attributes_cache */
	, &SimpleJson_t989_0_0_0/* byval_arg */
	, &SimpleJson_t989_1_0_0/* this_arg */
	, &SimpleJson_t989_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleJson_t989)/* instance_size */
	, sizeof (SimpleJson_t989)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SimpleJson_t989_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition SimpleJson.IJsonSerializerStrategy
extern TypeInfo IJsonSerializerStrategy_t987_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IJsonSerializerStrategy_t987_0_0_0;
extern const Il2CppType IJsonSerializerStrategy_t987_1_0_0;
struct IJsonSerializerStrategy_t987;
const Il2CppTypeDefinitionMetadata IJsonSerializerStrategy_t987_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5788/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IJsonSerializerStrategy_t987_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IJsonSerializerStrategy"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &IJsonSerializerStrategy_t987_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2195/* custom_attributes_cache */
	, &IJsonSerializerStrategy_t987_0_0_0/* byval_arg */
	, &IJsonSerializerStrategy_t987_1_0_0/* this_arg */
	, &IJsonSerializerStrategy_t987_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy.h"
// Metadata Definition SimpleJson.PocoJsonSerializerStrategy
extern TypeInfo PocoJsonSerializerStrategy_t988_il2cpp_TypeInfo;
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategyMethodDeclarations.h"
static const EncodedMethodIndex PocoJsonSerializerStrategy_t988_VTable[13] = 
{
	626,
	601,
	627,
	628,
	1472,
	1473,
	1474,
	1475,
	1476,
	1472,
	1477,
	1478,
	1479,
};
static const Il2CppType* PocoJsonSerializerStrategy_t988_InterfacesTypeInfos[] = 
{
	&IJsonSerializerStrategy_t987_0_0_0,
};
static Il2CppInterfaceOffsetPair PocoJsonSerializerStrategy_t988_InterfacesOffsets[] = 
{
	{ &IJsonSerializerStrategy_t987_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PocoJsonSerializerStrategy_t988_0_0_0;
extern const Il2CppType PocoJsonSerializerStrategy_t988_1_0_0;
struct PocoJsonSerializerStrategy_t988;
const Il2CppTypeDefinitionMetadata PocoJsonSerializerStrategy_t988_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PocoJsonSerializerStrategy_t988_InterfacesTypeInfos/* implementedInterfaces */
	, PocoJsonSerializerStrategy_t988_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PocoJsonSerializerStrategy_t988_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4661/* fieldStart */
	, 5789/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PocoJsonSerializerStrategy_t988_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PocoJsonSerializerStrategy"/* name */
	, "SimpleJson"/* namespaze */
	, NULL/* methods */
	, &PocoJsonSerializerStrategy_t988_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2197/* custom_attributes_cache */
	, &PocoJsonSerializerStrategy_t988_0_0_0/* byval_arg */
	, &PocoJsonSerializerStrategy_t988_1_0_0/* this_arg */
	, &PocoJsonSerializerStrategy_t988_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PocoJsonSerializerStrategy_t988)/* instance_size */
	, sizeof (PocoJsonSerializerStrategy_t988)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PocoJsonSerializerStrategy_t988_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils
extern TypeInfo ReflectionUtils_t1002_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtilsMethodDeclarations.h"
extern const Il2CppType ThreadSafeDictionary_2_t3403_0_0_0;
extern const Il2CppType GetDelegate_t993_0_0_0;
extern const Il2CppType SetDelegate_t994_0_0_0;
extern const Il2CppType ConstructorDelegate_t995_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t3404_0_0_0;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_0_0_0;
static const Il2CppType* ReflectionUtils_t1002_il2cpp_TypeInfo__nestedTypes[10] =
{
	&ThreadSafeDictionary_2_t3403_0_0_0,
	&GetDelegate_t993_0_0_0,
	&SetDelegate_t994_0_0_0,
	&ConstructorDelegate_t995_0_0_0,
	&ThreadSafeDictionaryValueFactory_2_t3404_0_0_0,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_0_0_0,
};
static const EncodedMethodIndex ReflectionUtils_t1002_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionUtils_t1002_0_0_0;
extern const Il2CppType ReflectionUtils_t1002_1_0_0;
struct ReflectionUtils_t1002;
const Il2CppTypeDefinitionMetadata ReflectionUtils_t1002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ReflectionUtils_t1002_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReflectionUtils_t1002_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4667/* fieldStart */
	, 5799/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReflectionUtils_t1002_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionUtils"/* name */
	, "SimpleJson.Reflection"/* namespaze */
	, NULL/* methods */
	, &ReflectionUtils_t1002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2200/* custom_attributes_cache */
	, &ReflectionUtils_t1002_0_0_0/* byval_arg */
	, &ReflectionUtils_t1002_1_0_0/* this_arg */
	, &ReflectionUtils_t1002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionUtils_t1002)/* instance_size */
	, sizeof (ReflectionUtils_t1002)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ReflectionUtils_t1002_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 10/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2
extern TypeInfo ThreadSafeDictionary_2_t3403_il2cpp_TypeInfo;
static const EncodedMethodIndex ThreadSafeDictionary_2_t3403_VTable[19] = 
{
	626,
	601,
	627,
	628,
	1480,
	1481,
	1482,
	1483,
	1484,
	1485,
	1486,
	1487,
	1488,
	1489,
	1490,
	1491,
	1492,
	1493,
	1494,
};
extern const Il2CppType IDictionary_2_t3656_0_0_0;
extern const Il2CppType ICollection_1_t3657_0_0_0;
extern const Il2CppType IEnumerable_1_t3658_0_0_0;
static const Il2CppType* ThreadSafeDictionary_2_t3403_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&IDictionary_2_t3656_0_0_0,
	&ICollection_1_t3657_0_0_0,
	&IEnumerable_1_t3658_0_0_0,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionary_2_t3403_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &IDictionary_2_t3656_0_0_0, 5},
	{ &ICollection_1_t3657_0_0_0, 11},
	{ &IEnumerable_1_t3658_0_0_0, 18},
};
extern const Il2CppType Enumerator_t3659_0_0_0;
extern const Il2CppType Dictionary_2_t3660_0_0_0;
extern const Il2CppRGCTXDefinition ThreadSafeDictionary_2_t3403_RGCTXData[15] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5457 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6556 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5458 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5459 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5460 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6557 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5461 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5462 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5463 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5464 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5465 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5466 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5467 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5468 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionary_2_t3403_1_0_0;
struct ThreadSafeDictionary_2_t3403;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionary_2_t3403_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ThreadSafeDictionary_2_t3403_InterfacesTypeInfos/* implementedInterfaces */
	, ThreadSafeDictionary_2_t3403_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ThreadSafeDictionary_2_t3403_VTable/* vtableMethods */
	, ThreadSafeDictionary_2_t3403_RGCTXData/* rgctxDefinition */
	, 4668/* fieldStart */
	, 5817/* methodStart */
	, -1/* eventStart */
	, 1152/* propertyStart */

};
TypeInfo ThreadSafeDictionary_2_t3403_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionary`2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ThreadSafeDictionary_2_t3403_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2204/* custom_attributes_cache */
	, &ThreadSafeDictionary_2_t3403_0_0_0/* byval_arg */
	, &ThreadSafeDictionary_2_t3403_1_0_0/* this_arg */
	, &ThreadSafeDictionary_2_t3403_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 42/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048834/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/GetDelegate
extern TypeInfo GetDelegate_t993_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegatMethodDeclarations.h"
static const EncodedMethodIndex GetDelegate_t993_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1495,
	1496,
	1497,
};
static Il2CppInterfaceOffsetPair GetDelegate_t993_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GetDelegate_t993_1_0_0;
struct GetDelegate_t993;
const Il2CppTypeDefinitionMetadata GetDelegate_t993_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetDelegate_t993_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, GetDelegate_t993_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5835/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GetDelegate_t993_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GetDelegate_t993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetDelegate_t993_0_0_0/* byval_arg */
	, &GetDelegate_t993_1_0_0/* this_arg */
	, &GetDelegate_t993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetDelegate_t993/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetDelegate_t993)/* instance_size */
	, sizeof (GetDelegate_t993)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/SetDelegate
extern TypeInfo SetDelegate_t994_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegatMethodDeclarations.h"
static const EncodedMethodIndex SetDelegate_t994_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1498,
	1499,
	1500,
};
static Il2CppInterfaceOffsetPair SetDelegate_t994_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetDelegate_t994_1_0_0;
struct SetDelegate_t994;
const Il2CppTypeDefinitionMetadata SetDelegate_t994_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SetDelegate_t994_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, SetDelegate_t994_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5839/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SetDelegate_t994_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SetDelegate_t994_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetDelegate_t994_0_0_0/* byval_arg */
	, &SetDelegate_t994_1_0_0/* this_arg */
	, &SetDelegate_t994_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_SetDelegate_t994/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetDelegate_t994)/* instance_size */
	, sizeof (SetDelegate_t994)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_Constructo.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
extern TypeInfo ConstructorDelegate_t995_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ConstructoMethodDeclarations.h"
static const EncodedMethodIndex ConstructorDelegate_t995_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1501,
	1502,
	1503,
};
static Il2CppInterfaceOffsetPair ConstructorDelegate_t995_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ConstructorDelegate_t995_1_0_0;
struct ConstructorDelegate_t995;
const Il2CppTypeDefinitionMetadata ConstructorDelegate_t995_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructorDelegate_t995_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ConstructorDelegate_t995_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5843/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConstructorDelegate_t995_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ConstructorDelegate_t995_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructorDelegate_t995_0_0_0/* byval_arg */
	, &ConstructorDelegate_t995_1_0_0/* this_arg */
	, &ConstructorDelegate_t995_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ConstructorDelegate_t995/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorDelegate_t995)/* instance_size */
	, sizeof (ConstructorDelegate_t995)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t3404_il2cpp_TypeInfo;
static const EncodedMethodIndex ThreadSafeDictionaryValueFactory_2_t3404_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1504,
	1505,
	1506,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionaryValueFactory_2_t3404_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t3404_1_0_0;
struct ThreadSafeDictionaryValueFactory_2_t3404;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionaryValueFactory_2_t3404_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadSafeDictionaryValueFactory_2_t3404_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ThreadSafeDictionaryValueFactory_2_t3404_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5847/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadSafeDictionaryValueFactory_2_t3404_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionaryValueFactory`2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ThreadSafeDictionaryValueFactory_2_t3404_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThreadSafeDictionaryValueFactory_2_t3404_0_0_0/* byval_arg */
	, &ThreadSafeDictionaryValueFactory_2_t3404_1_0_0/* this_arg */
	, &ThreadSafeDictionaryValueFactory_2_t3404_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 43/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetCons.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
extern TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetConsMethodDeclarations.h"
static const EncodedMethodIndex U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_1_0_0;
struct U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997;
const Il2CppTypeDefinitionMetadata U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4671/* fieldStart */
	, 5851/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetConstructorByReflection>c__AnonStorey1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2207/* custom_attributes_cache */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_0_0_0/* byval_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_1_0_0/* this_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997)/* instance_size */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetMMethodDeclarations.h"
static const EncodedMethodIndex U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4672/* fieldStart */
	, 5853/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2208/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t998)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0MethodDeclarations.h"
static const EncodedMethodIndex U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4673/* fieldStart */
	, 5855/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey3"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2209/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t999)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetMMethodDeclarations.h"
static const EncodedMethodIndex U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4674/* fieldStart */
	, 5857/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey4"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2210/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1000)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0MethodDeclarations.h"
static const EncodedMethodIndex U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_DefinitionMetadata = 
{
	&ReflectionUtils_t1002_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4675/* fieldStart */
	, 5859/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey5"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2211/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1001)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AOT.MonoPInvokeCallbackAttribute
#include "UnityEngine_AOT_MonoPInvokeCallbackAttribute.h"
// Metadata Definition AOT.MonoPInvokeCallbackAttribute
extern TypeInfo MonoPInvokeCallbackAttribute_t1003_il2cpp_TypeInfo;
// AOT.MonoPInvokeCallbackAttribute
#include "UnityEngine_AOT_MonoPInvokeCallbackAttributeMethodDeclarations.h"
static const EncodedMethodIndex MonoPInvokeCallbackAttribute_t1003_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair MonoPInvokeCallbackAttribute_t1003_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType MonoPInvokeCallbackAttribute_t1003_0_0_0;
extern const Il2CppType MonoPInvokeCallbackAttribute_t1003_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct MonoPInvokeCallbackAttribute_t1003;
const Il2CppTypeDefinitionMetadata MonoPInvokeCallbackAttribute_t1003_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoPInvokeCallbackAttribute_t1003_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, MonoPInvokeCallbackAttribute_t1003_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5861/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoPInvokeCallbackAttribute_t1003_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoPInvokeCallbackAttribute"/* name */
	, "AOT"/* namespaze */
	, NULL/* methods */
	, &MonoPInvokeCallbackAttribute_t1003_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2212/* custom_attributes_cache */
	, &MonoPInvokeCallbackAttribute_t1003_0_0_0/* byval_arg */
	, &MonoPInvokeCallbackAttribute_t1003_1_0_0/* this_arg */
	, &MonoPInvokeCallbackAttribute_t1003_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoPInvokeCallbackAttribute_t1003)/* instance_size */
	, sizeof (MonoPInvokeCallbackAttribute_t1003)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// Metadata Definition UnityEngine.WrapperlessIcall
extern TypeInfo WrapperlessIcall_t1004_il2cpp_TypeInfo;
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
static const EncodedMethodIndex WrapperlessIcall_t1004_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair WrapperlessIcall_t1004_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WrapperlessIcall_t1004_0_0_0;
extern const Il2CppType WrapperlessIcall_t1004_1_0_0;
struct WrapperlessIcall_t1004;
const Il2CppTypeDefinitionMetadata WrapperlessIcall_t1004_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WrapperlessIcall_t1004_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, WrapperlessIcall_t1004_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5862/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WrapperlessIcall_t1004_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WrapperlessIcall"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WrapperlessIcall_t1004_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WrapperlessIcall_t1004_0_0_0/* byval_arg */
	, &WrapperlessIcall_t1004_1_0_0/* this_arg */
	, &WrapperlessIcall_t1004_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WrapperlessIcall_t1004)/* instance_size */
	, sizeof (WrapperlessIcall_t1004)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// Metadata Definition UnityEngine.IL2CPPStructAlignmentAttribute
extern TypeInfo IL2CPPStructAlignmentAttribute_t1005_il2cpp_TypeInfo;
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
static const EncodedMethodIndex IL2CPPStructAlignmentAttribute_t1005_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair IL2CPPStructAlignmentAttribute_t1005_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t1005_0_0_0;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t1005_1_0_0;
struct IL2CPPStructAlignmentAttribute_t1005;
const Il2CppTypeDefinitionMetadata IL2CPPStructAlignmentAttribute_t1005_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IL2CPPStructAlignmentAttribute_t1005_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, IL2CPPStructAlignmentAttribute_t1005_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4676/* fieldStart */
	, 5863/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IL2CPPStructAlignmentAttribute_t1005_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IL2CPPStructAlignmentAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &IL2CPPStructAlignmentAttribute_t1005_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2213/* custom_attributes_cache */
	, &IL2CPPStructAlignmentAttribute_t1005_0_0_0/* byval_arg */
	, &IL2CPPStructAlignmentAttribute_t1005_1_0_0/* this_arg */
	, &IL2CPPStructAlignmentAttribute_t1005_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IL2CPPStructAlignmentAttribute_t1005)/* instance_size */
	, sizeof (IL2CPPStructAlignmentAttribute_t1005)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// Metadata Definition UnityEngine.AttributeHelperEngine
extern TypeInfo AttributeHelperEngine_t1009_il2cpp_TypeInfo;
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
static const EncodedMethodIndex AttributeHelperEngine_t1009_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AttributeHelperEngine_t1009_0_0_0;
extern const Il2CppType AttributeHelperEngine_t1009_1_0_0;
struct AttributeHelperEngine_t1009;
const Il2CppTypeDefinitionMetadata AttributeHelperEngine_t1009_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeHelperEngine_t1009_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4677/* fieldStart */
	, 5864/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AttributeHelperEngine_t1009_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeHelperEngine"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AttributeHelperEngine_t1009_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeHelperEngine_t1009_0_0_0/* byval_arg */
	, &AttributeHelperEngine_t1009_1_0_0/* this_arg */
	, &AttributeHelperEngine_t1009_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeHelperEngine_t1009)/* instance_size */
	, sizeof (AttributeHelperEngine_t1009)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AttributeHelperEngine_t1009_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// Metadata Definition UnityEngine.DisallowMultipleComponent
extern TypeInfo DisallowMultipleComponent_t1010_il2cpp_TypeInfo;
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
static const EncodedMethodIndex DisallowMultipleComponent_t1010_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DisallowMultipleComponent_t1010_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisallowMultipleComponent_t1010_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t1010_1_0_0;
struct DisallowMultipleComponent_t1010;
const Il2CppTypeDefinitionMetadata DisallowMultipleComponent_t1010_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisallowMultipleComponent_t1010_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DisallowMultipleComponent_t1010_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5868/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DisallowMultipleComponent_t1010_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisallowMultipleComponent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &DisallowMultipleComponent_t1010_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2214/* custom_attributes_cache */
	, &DisallowMultipleComponent_t1010_0_0_0/* byval_arg */
	, &DisallowMultipleComponent_t1010_1_0_0/* this_arg */
	, &DisallowMultipleComponent_t1010_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisallowMultipleComponent_t1010)/* instance_size */
	, sizeof (DisallowMultipleComponent_t1010)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// Metadata Definition UnityEngine.RequireComponent
extern TypeInfo RequireComponent_t1011_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
static const EncodedMethodIndex RequireComponent_t1011_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair RequireComponent_t1011_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RequireComponent_t1011_0_0_0;
extern const Il2CppType RequireComponent_t1011_1_0_0;
struct RequireComponent_t1011;
const Il2CppTypeDefinitionMetadata RequireComponent_t1011_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RequireComponent_t1011_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, RequireComponent_t1011_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4680/* fieldStart */
	, 5869/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RequireComponent_t1011_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RequireComponent"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RequireComponent_t1011_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2215/* custom_attributes_cache */
	, &RequireComponent_t1011_0_0_0/* byval_arg */
	, &RequireComponent_t1011_1_0_0/* this_arg */
	, &RequireComponent_t1011_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RequireComponent_t1011)/* instance_size */
	, sizeof (RequireComponent_t1011)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// Metadata Definition UnityEngine.AddComponentMenu
extern TypeInfo AddComponentMenu_t1012_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
static const EncodedMethodIndex AddComponentMenu_t1012_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AddComponentMenu_t1012_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AddComponentMenu_t1012_0_0_0;
extern const Il2CppType AddComponentMenu_t1012_1_0_0;
struct AddComponentMenu_t1012;
const Il2CppTypeDefinitionMetadata AddComponentMenu_t1012_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddComponentMenu_t1012_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AddComponentMenu_t1012_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4683/* fieldStart */
	, 5870/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AddComponentMenu_t1012_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddComponentMenu"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AddComponentMenu_t1012_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddComponentMenu_t1012_0_0_0/* byval_arg */
	, &AddComponentMenu_t1012_1_0_0/* this_arg */
	, &AddComponentMenu_t1012_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddComponentMenu_t1012)/* instance_size */
	, sizeof (AddComponentMenu_t1012)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// Metadata Definition UnityEngine.ExecuteInEditMode
extern TypeInfo ExecuteInEditMode_t1013_il2cpp_TypeInfo;
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
static const EncodedMethodIndex ExecuteInEditMode_t1013_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ExecuteInEditMode_t1013_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExecuteInEditMode_t1013_0_0_0;
extern const Il2CppType ExecuteInEditMode_t1013_1_0_0;
struct ExecuteInEditMode_t1013;
const Il2CppTypeDefinitionMetadata ExecuteInEditMode_t1013_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecuteInEditMode_t1013_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ExecuteInEditMode_t1013_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5872/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExecuteInEditMode_t1013_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecuteInEditMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &ExecuteInEditMode_t1013_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecuteInEditMode_t1013_0_0_0/* byval_arg */
	, &ExecuteInEditMode_t1013_1_0_0/* this_arg */
	, &ExecuteInEditMode_t1013_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecuteInEditMode_t1013)/* instance_size */
	, sizeof (ExecuteInEditMode_t1013)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// Metadata Definition UnityEngine.HideInInspector
extern TypeInfo HideInInspector_t1014_il2cpp_TypeInfo;
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
static const EncodedMethodIndex HideInInspector_t1014_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair HideInInspector_t1014_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HideInInspector_t1014_0_0_0;
extern const Il2CppType HideInInspector_t1014_1_0_0;
struct HideInInspector_t1014;
const Il2CppTypeDefinitionMetadata HideInInspector_t1014_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HideInInspector_t1014_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, HideInInspector_t1014_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5873/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HideInInspector_t1014_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideInInspector"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &HideInInspector_t1014_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HideInInspector_t1014_0_0_0/* byval_arg */
	, &HideInInspector_t1014_1_0_0/* this_arg */
	, &HideInInspector_t1014_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideInInspector_t1014)/* instance_size */
	, sizeof (HideInInspector_t1014)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.CastHelper`1
extern TypeInfo CastHelper_1_t3405_il2cpp_TypeInfo;
static const EncodedMethodIndex CastHelper_1_t3405_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CastHelper_1_t3405_0_0_0;
extern const Il2CppType CastHelper_1_t3405_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata CastHelper_1_t3405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, CastHelper_1_t3405_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4685/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CastHelper_1_t3405_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &CastHelper_1_t3405_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CastHelper_1_t3405_0_0_0/* byval_arg */
	, &CastHelper_1_t3405_1_0_0/* this_arg */
	, &CastHelper_1_t3405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 44/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
// Metadata Definition UnityEngine.SetupCoroutine
extern TypeInfo SetupCoroutine_t1015_il2cpp_TypeInfo;
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
static const EncodedMethodIndex SetupCoroutine_t1015_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetupCoroutine_t1015_0_0_0;
extern const Il2CppType SetupCoroutine_t1015_1_0_0;
struct SetupCoroutine_t1015;
const Il2CppTypeDefinitionMetadata SetupCoroutine_t1015_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetupCoroutine_t1015_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5874/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SetupCoroutine_t1015_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetupCoroutine"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SetupCoroutine_t1015_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetupCoroutine_t1015_0_0_0/* byval_arg */
	, &SetupCoroutine_t1015_1_0_0/* this_arg */
	, &SetupCoroutine_t1015_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetupCoroutine_t1015)/* instance_size */
	, sizeof (SetupCoroutine_t1015)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// Metadata Definition UnityEngine.WritableAttribute
extern TypeInfo WritableAttribute_t1016_il2cpp_TypeInfo;
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
static const EncodedMethodIndex WritableAttribute_t1016_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair WritableAttribute_t1016_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WritableAttribute_t1016_0_0_0;
extern const Il2CppType WritableAttribute_t1016_1_0_0;
struct WritableAttribute_t1016;
const Il2CppTypeDefinitionMetadata WritableAttribute_t1016_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WritableAttribute_t1016_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, WritableAttribute_t1016_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5877/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WritableAttribute_t1016_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WritableAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &WritableAttribute_t1016_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2216/* custom_attributes_cache */
	, &WritableAttribute_t1016_0_0_0/* byval_arg */
	, &WritableAttribute_t1016_1_0_0/* this_arg */
	, &WritableAttribute_t1016_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WritableAttribute_t1016)/* instance_size */
	, sizeof (WritableAttribute_t1016)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
// Metadata Definition UnityEngine.AssemblyIsEditorAssembly
extern TypeInfo AssemblyIsEditorAssembly_t1017_il2cpp_TypeInfo;
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
static const EncodedMethodIndex AssemblyIsEditorAssembly_t1017_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyIsEditorAssembly_t1017_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssemblyIsEditorAssembly_t1017_0_0_0;
extern const Il2CppType AssemblyIsEditorAssembly_t1017_1_0_0;
struct AssemblyIsEditorAssembly_t1017;
const Il2CppTypeDefinitionMetadata AssemblyIsEditorAssembly_t1017_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyIsEditorAssembly_t1017_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyIsEditorAssembly_t1017_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5878/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyIsEditorAssembly_t1017_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyIsEditorAssembly"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &AssemblyIsEditorAssembly_t1017_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2217/* custom_attributes_cache */
	, &AssemblyIsEditorAssembly_t1017_0_0_0/* byval_arg */
	, &AssemblyIsEditorAssembly_t1017_1_0_0/* this_arg */
	, &AssemblyIsEditorAssembly_t1017_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyIsEditorAssembly_t1017)/* instance_size */
	, sizeof (AssemblyIsEditorAssembly_t1017)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern TypeInfo GcUserProfileData_t1018_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
static const EncodedMethodIndex GcUserProfileData_t1018_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcUserProfileData_t1018_0_0_0;
extern const Il2CppType GcUserProfileData_t1018_1_0_0;
const Il2CppTypeDefinitionMetadata GcUserProfileData_t1018_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, GcUserProfileData_t1018_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4687/* fieldStart */
	, 5879/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcUserProfileData_t1018_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcUserProfileData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcUserProfileData_t1018_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcUserProfileData_t1018_0_0_0/* byval_arg */
	, &GcUserProfileData_t1018_1_0_0/* this_arg */
	, &GcUserProfileData_t1018_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcUserProfileData_t1018)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcUserProfileData_t1018)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern TypeInfo GcAchievementDescriptionData_t1019_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
static const EncodedMethodIndex GcAchievementDescriptionData_t1019_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementDescriptionData_t1019_0_0_0;
extern const Il2CppType GcAchievementDescriptionData_t1019_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementDescriptionData_t1019_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, GcAchievementDescriptionData_t1019_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4691/* fieldStart */
	, 5881/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcAchievementDescriptionData_t1019_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementDescriptionData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcAchievementDescriptionData_t1019_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementDescriptionData_t1019_0_0_0/* byval_arg */
	, &GcAchievementDescriptionData_t1019_1_0_0/* this_arg */
	, &GcAchievementDescriptionData_t1019_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcAchievementDescriptionData_t1019)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementDescriptionData_t1019)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern TypeInfo GcAchievementData_t1020_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
static const EncodedMethodIndex GcAchievementData_t1020_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementData_t1020_0_0_0;
extern const Il2CppType GcAchievementData_t1020_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementData_t1020_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, GcAchievementData_t1020_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4698/* fieldStart */
	, 5882/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcAchievementData_t1020_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcAchievementData_t1020_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementData_t1020_0_0_0/* byval_arg */
	, &GcAchievementData_t1020_1_0_0/* this_arg */
	, &GcAchievementData_t1020_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcAchievementData_t1020_marshal/* marshal_to_native_func */
	, (methodPointerType)GcAchievementData_t1020_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcAchievementData_t1020_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcAchievementData_t1020)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementData_t1020)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcAchievementData_t1020_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern TypeInfo GcScoreData_t1021_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
static const EncodedMethodIndex GcScoreData_t1021_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcScoreData_t1021_0_0_0;
extern const Il2CppType GcScoreData_t1021_1_0_0;
const Il2CppTypeDefinitionMetadata GcScoreData_t1021_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, GcScoreData_t1021_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4703/* fieldStart */
	, 5883/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GcScoreData_t1021_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, NULL/* methods */
	, &GcScoreData_t1021_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcScoreData_t1021_0_0_0/* byval_arg */
	, &GcScoreData_t1021_1_0_0/* this_arg */
	, &GcScoreData_t1021_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcScoreData_t1021_marshal/* marshal_to_native_func */
	, (methodPointerType)GcScoreData_t1021_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcScoreData_t1021_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcScoreData_t1021)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcScoreData_t1021)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcScoreData_t1021_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_Resolution.h"
// Metadata Definition UnityEngine.Resolution
extern TypeInfo Resolution_t1022_il2cpp_TypeInfo;
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
static const EncodedMethodIndex Resolution_t1022_VTable[4] = 
{
	652,
	601,
	653,
	1507,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resolution_t1022_0_0_0;
extern const Il2CppType Resolution_t1022_1_0_0;
const Il2CppTypeDefinitionMetadata Resolution_t1022_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Resolution_t1022_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4710/* fieldStart */
	, 5884/* methodStart */
	, -1/* eventStart */
	, 1157/* propertyStart */

};
TypeInfo Resolution_t1022_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resolution"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Resolution_t1022_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resolution_t1022_0_0_0/* byval_arg */
	, &Resolution_t1022_1_0_0/* this_arg */
	, &Resolution_t1022_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resolution_t1022)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Resolution_t1022)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Resolution_t1022 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// Metadata Definition UnityEngine.RenderBuffer
extern TypeInfo RenderBuffer_t1023_il2cpp_TypeInfo;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
static const EncodedMethodIndex RenderBuffer_t1023_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderBuffer_t1023_0_0_0;
extern const Il2CppType RenderBuffer_t1023_1_0_0;
const Il2CppTypeDefinitionMetadata RenderBuffer_t1023_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RenderBuffer_t1023_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4713/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RenderBuffer_t1023_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderBuffer"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RenderBuffer_t1023_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderBuffer_t1023_0_0_0/* byval_arg */
	, &RenderBuffer_t1023_1_0_0/* this_arg */
	, &RenderBuffer_t1023_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderBuffer_t1023)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderBuffer_t1023)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RenderBuffer_t1023 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// Metadata Definition UnityEngine.CameraClearFlags
extern TypeInfo CameraClearFlags_t1024_il2cpp_TypeInfo;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
static const EncodedMethodIndex CameraClearFlags_t1024_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CameraClearFlags_t1024_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraClearFlags_t1024_0_0_0;
extern const Il2CppType CameraClearFlags_t1024_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata CameraClearFlags_t1024_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraClearFlags_t1024_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CameraClearFlags_t1024_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4715/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CameraClearFlags_t1024_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraClearFlags"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraClearFlags_t1024_0_0_0/* byval_arg */
	, &CameraClearFlags_t1024_1_0_0/* this_arg */
	, &CameraClearFlags_t1024_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraClearFlags_t1024)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CameraClearFlags_t1024)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"
// Metadata Definition UnityEngine.ColorSpace
extern TypeInfo ColorSpace_t1025_il2cpp_TypeInfo;
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpaceMethodDeclarations.h"
static const EncodedMethodIndex ColorSpace_t1025_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ColorSpace_t1025_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ColorSpace_t1025_0_0_0;
extern const Il2CppType ColorSpace_t1025_1_0_0;
const Il2CppTypeDefinitionMetadata ColorSpace_t1025_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ColorSpace_t1025_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ColorSpace_t1025_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4721/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ColorSpace_t1025_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorSpace"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorSpace_t1025_0_0_0/* byval_arg */
	, &ColorSpace_t1025_1_0_0/* this_arg */
	, &ColorSpace_t1025_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorSpace_t1025)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorSpace_t1025)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// Metadata Definition UnityEngine.FilterMode
extern TypeInfo FilterMode_t1026_il2cpp_TypeInfo;
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterModeMethodDeclarations.h"
static const EncodedMethodIndex FilterMode_t1026_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FilterMode_t1026_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FilterMode_t1026_0_0_0;
extern const Il2CppType FilterMode_t1026_1_0_0;
const Il2CppTypeDefinitionMetadata FilterMode_t1026_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FilterMode_t1026_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FilterMode_t1026_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4725/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FilterMode_t1026_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FilterMode"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FilterMode_t1026_0_0_0/* byval_arg */
	, &FilterMode_t1026_1_0_0/* this_arg */
	, &FilterMode_t1026_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FilterMode_t1026)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FilterMode_t1026)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// Metadata Definition UnityEngine.TextureFormat
extern TypeInfo TextureFormat_t1027_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
static const EncodedMethodIndex TextureFormat_t1027_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TextureFormat_t1027_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureFormat_t1027_0_0_0;
extern const Il2CppType TextureFormat_t1027_1_0_0;
const Il2CppTypeDefinitionMetadata TextureFormat_t1027_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureFormat_t1027_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TextureFormat_t1027_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4729/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextureFormat_t1027_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureFormat_t1027_0_0_0/* byval_arg */
	, &TextureFormat_t1027_1_0_0/* this_arg */
	, &TextureFormat_t1027_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t1027)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureFormat_t1027)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// Metadata Definition UnityEngine.RenderTextureFormat
extern TypeInfo RenderTextureFormat_t1028_il2cpp_TypeInfo;
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormatMethodDeclarations.h"
static const EncodedMethodIndex RenderTextureFormat_t1028_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair RenderTextureFormat_t1028_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureFormat_t1028_0_0_0;
extern const Il2CppType RenderTextureFormat_t1028_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureFormat_t1028_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureFormat_t1028_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, RenderTextureFormat_t1028_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4774/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RenderTextureFormat_t1028_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureFormat_t1028_0_0_0/* byval_arg */
	, &RenderTextureFormat_t1028_1_0_0/* this_arg */
	, &RenderTextureFormat_t1028_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureFormat_t1028)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureFormat_t1028)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
// Metadata Definition UnityEngine.RenderTextureReadWrite
extern TypeInfo RenderTextureReadWrite_t1029_il2cpp_TypeInfo;
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWriteMethodDeclarations.h"
static const EncodedMethodIndex RenderTextureReadWrite_t1029_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair RenderTextureReadWrite_t1029_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureReadWrite_t1029_0_0_0;
extern const Il2CppType RenderTextureReadWrite_t1029_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureReadWrite_t1029_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureReadWrite_t1029_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, RenderTextureReadWrite_t1029_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4794/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RenderTextureReadWrite_t1029_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureReadWrite"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureReadWrite_t1029_0_0_0/* byval_arg */
	, &RenderTextureReadWrite_t1029_1_0_0/* this_arg */
	, &RenderTextureReadWrite_t1029_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureReadWrite_t1029)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureReadWrite_t1029)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern TypeInfo ReflectionProbeBlendInfo_t1030_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
static const EncodedMethodIndex ReflectionProbeBlendInfo_t1030_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbeBlendInfo_t1030_0_0_0;
extern const Il2CppType ReflectionProbeBlendInfo_t1030_1_0_0;
const Il2CppTypeDefinitionMetadata ReflectionProbeBlendInfo_t1030_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, ReflectionProbeBlendInfo_t1030_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4798/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReflectionProbeBlendInfo_t1030_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, NULL/* methods */
	, &ReflectionProbeBlendInfo_t1030_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t1030_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t1030_1_0_0/* this_arg */
	, &ReflectionProbeBlendInfo_t1030_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t1030)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReflectionProbeBlendInfo_t1030)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern TypeInfo LocalUser_t846_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
static const EncodedMethodIndex LocalUser_t846_VTable[9] = 
{
	626,
	601,
	627,
	1508,
	1509,
	1510,
	1511,
	1512,
	1513,
};
extern const Il2CppType ILocalUser_t1078_0_0_0;
extern const Il2CppType IUserProfile_t3147_0_0_0;
static const Il2CppType* LocalUser_t846_InterfacesTypeInfos[] = 
{
	&ILocalUser_t1078_0_0_0,
	&IUserProfile_t3147_0_0_0,
};
static Il2CppInterfaceOffsetPair LocalUser_t846_InterfacesOffsets[] = 
{
	{ &IUserProfile_t3147_0_0_0, 4},
	{ &ILocalUser_t1078_0_0_0, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LocalUser_t846_0_0_0;
extern const Il2CppType LocalUser_t846_1_0_0;
extern const Il2CppType UserProfile_t1032_0_0_0;
struct LocalUser_t846;
const Il2CppTypeDefinitionMetadata LocalUser_t846_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LocalUser_t846_InterfacesTypeInfos/* implementedInterfaces */
	, LocalUser_t846_InterfacesOffsets/* interfaceOffsets */
	, &UserProfile_t1032_0_0_0/* parent */
	, LocalUser_t846_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4800/* fieldStart */
	, 5891/* methodStart */
	, -1/* eventStart */
	, 1160/* propertyStart */

};
TypeInfo LocalUser_t846_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &LocalUser_t846_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LocalUser_t846_0_0_0/* byval_arg */
	, &LocalUser_t846_1_0_0/* this_arg */
	, &LocalUser_t846_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t846)/* instance_size */
	, sizeof (LocalUser_t846)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern TypeInfo UserProfile_t1032_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
static const EncodedMethodIndex UserProfile_t1032_VTable[8] = 
{
	626,
	601,
	627,
	1508,
	1509,
	1510,
	1511,
	1512,
};
static const Il2CppType* UserProfile_t1032_InterfacesTypeInfos[] = 
{
	&IUserProfile_t3147_0_0_0,
};
static Il2CppInterfaceOffsetPair UserProfile_t1032_InterfacesOffsets[] = 
{
	{ &IUserProfile_t3147_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserProfile_t1032_1_0_0;
struct UserProfile_t1032;
const Il2CppTypeDefinitionMetadata UserProfile_t1032_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UserProfile_t1032_InterfacesTypeInfos/* implementedInterfaces */
	, UserProfile_t1032_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UserProfile_t1032_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4803/* fieldStart */
	, 5896/* methodStart */
	, -1/* eventStart */
	, 1161/* propertyStart */

};
TypeInfo UserProfile_t1032_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &UserProfile_t1032_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserProfile_t1032_0_0_0/* byval_arg */
	, &UserProfile_t1032_1_0_0/* this_arg */
	, &UserProfile_t1032_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t1032)/* instance_size */
	, sizeof (UserProfile_t1032)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern TypeInfo Achievement_t1033_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
static const EncodedMethodIndex Achievement_t1033_VTable[11] = 
{
	626,
	601,
	627,
	1514,
	1515,
	1516,
	1517,
	1518,
	1519,
	1520,
	1521,
};
extern const Il2CppType IAchievement_t1082_0_0_0;
static const Il2CppType* Achievement_t1033_InterfacesTypeInfos[] = 
{
	&IAchievement_t1082_0_0_0,
};
static Il2CppInterfaceOffsetPair Achievement_t1033_InterfacesOffsets[] = 
{
	{ &IAchievement_t1082_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Achievement_t1033_0_0_0;
extern const Il2CppType Achievement_t1033_1_0_0;
struct Achievement_t1033;
const Il2CppTypeDefinitionMetadata Achievement_t1033_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Achievement_t1033_InterfacesTypeInfos/* implementedInterfaces */
	, Achievement_t1033_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Achievement_t1033_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4808/* fieldStart */
	, 5906/* methodStart */
	, -1/* eventStart */
	, 1165/* propertyStart */

};
TypeInfo Achievement_t1033_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &Achievement_t1033_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Achievement_t1033_0_0_0/* byval_arg */
	, &Achievement_t1033_1_0_0/* this_arg */
	, &Achievement_t1033_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t1033)/* instance_size */
	, sizeof (Achievement_t1033)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern TypeInfo AchievementDescription_t1034_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
static const EncodedMethodIndex AchievementDescription_t1034_VTable[11] = 
{
	626,
	601,
	627,
	1522,
	1523,
	1524,
	1525,
	1526,
	1527,
	1528,
	1529,
};
extern const Il2CppType IAchievementDescription_t3146_0_0_0;
static const Il2CppType* AchievementDescription_t1034_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t3146_0_0_0,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t1034_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t3146_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AchievementDescription_t1034_0_0_0;
extern const Il2CppType AchievementDescription_t1034_1_0_0;
struct AchievementDescription_t1034;
const Il2CppTypeDefinitionMetadata AchievementDescription_t1034_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AchievementDescription_t1034_InterfacesTypeInfos/* implementedInterfaces */
	, AchievementDescription_t1034_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AchievementDescription_t1034_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4813/* fieldStart */
	, 5917/* methodStart */
	, -1/* eventStart */
	, 1170/* propertyStart */

};
TypeInfo AchievementDescription_t1034_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &AchievementDescription_t1034_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AchievementDescription_t1034_0_0_0/* byval_arg */
	, &AchievementDescription_t1034_1_0_0/* this_arg */
	, &AchievementDescription_t1034_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t1034)/* instance_size */
	, sizeof (AchievementDescription_t1034)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern TypeInfo Score_t1035_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
static const EncodedMethodIndex Score_t1035_VTable[8] = 
{
	626,
	601,
	627,
	1530,
	1531,
	1532,
	1533,
	1534,
};
extern const Il2CppType IScore_t1036_0_0_0;
static const Il2CppType* Score_t1035_InterfacesTypeInfos[] = 
{
	&IScore_t1036_0_0_0,
};
static Il2CppInterfaceOffsetPair Score_t1035_InterfacesOffsets[] = 
{
	{ &IScore_t1036_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Score_t1035_0_0_0;
extern const Il2CppType Score_t1035_1_0_0;
struct Score_t1035;
const Il2CppTypeDefinitionMetadata Score_t1035_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Score_t1035_InterfacesTypeInfos/* implementedInterfaces */
	, Score_t1035_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Score_t1035_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4820/* fieldStart */
	, 5927/* methodStart */
	, -1/* eventStart */
	, 1176/* propertyStart */

};
TypeInfo Score_t1035_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &Score_t1035_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Score_t1035_0_0_0/* byval_arg */
	, &Score_t1035_1_0_0/* this_arg */
	, &Score_t1035_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t1035)/* instance_size */
	, sizeof (Score_t1035)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern TypeInfo Leaderboard_t849_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
static const EncodedMethodIndex Leaderboard_t849_VTable[12] = 
{
	626,
	601,
	627,
	1535,
	1536,
	1537,
	1538,
	1539,
	1540,
	1541,
	1542,
	1543,
};
extern const Il2CppType ILeaderboard_t1081_0_0_0;
static const Il2CppType* Leaderboard_t849_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t1081_0_0_0,
};
static Il2CppInterfaceOffsetPair Leaderboard_t849_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t1081_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Leaderboard_t849_0_0_0;
extern const Il2CppType Leaderboard_t849_1_0_0;
struct Leaderboard_t849;
const Il2CppTypeDefinitionMetadata Leaderboard_t849_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Leaderboard_t849_InterfacesTypeInfos/* implementedInterfaces */
	, Leaderboard_t849_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Leaderboard_t849_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4826/* fieldStart */
	, 5934/* methodStart */
	, -1/* eventStart */
	, 1178/* propertyStart */

};
TypeInfo Leaderboard_t849_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, NULL/* methods */
	, &Leaderboard_t849_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Leaderboard_t849_0_0_0/* byval_arg */
	, &Leaderboard_t849_1_0_0/* this_arg */
	, &Leaderboard_t849_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t849)/* instance_size */
	, sizeof (Leaderboard_t849)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// Metadata Definition UnityEngine.SendMouseEvents
extern TypeInfo SendMouseEvents_t1042_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
extern const Il2CppType HitInfo_t1039_0_0_0;
static const Il2CppType* SendMouseEvents_t1042_il2cpp_TypeInfo__nestedTypes[1] =
{
	&HitInfo_t1039_0_0_0,
};
static const EncodedMethodIndex SendMouseEvents_t1042_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMouseEvents_t1042_0_0_0;
extern const Il2CppType SendMouseEvents_t1042_1_0_0;
struct SendMouseEvents_t1042;
const Il2CppTypeDefinitionMetadata SendMouseEvents_t1042_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SendMouseEvents_t1042_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendMouseEvents_t1042_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4836/* fieldStart */
	, 5949/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SendMouseEvents_t1042_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SendMouseEvents_t1042_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMouseEvents_t1042_0_0_0/* byval_arg */
	, &SendMouseEvents_t1042_1_0_0/* this_arg */
	, &SendMouseEvents_t1042_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t1042)/* instance_size */
	, sizeof (SendMouseEvents_t1042)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t1042_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern TypeInfo HitInfo_t1039_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
static const EncodedMethodIndex HitInfo_t1039_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HitInfo_t1039_1_0_0;
const Il2CppTypeDefinitionMetadata HitInfo_t1039_DefinitionMetadata = 
{
	&SendMouseEvents_t1042_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, HitInfo_t1039_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4843/* fieldStart */
	, 5952/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HitInfo_t1039_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &HitInfo_t1039_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HitInfo_t1039_0_0_0/* byval_arg */
	, &HitInfo_t1039_1_0_0/* this_arg */
	, &HitInfo_t1039_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t1039)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HitInfo_t1039)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern TypeInfo ISocialPlatform_t3406_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISocialPlatform_t3406_0_0_0;
extern const Il2CppType ISocialPlatform_t3406_1_0_0;
struct ISocialPlatform_t3406;
const Il2CppTypeDefinitionMetadata ISocialPlatform_t3406_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5955/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISocialPlatform_t3406_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &ISocialPlatform_t3406_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISocialPlatform_t3406_0_0_0/* byval_arg */
	, &ISocialPlatform_t3406_1_0_0/* this_arg */
	, &ISocialPlatform_t3406_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern TypeInfo ILocalUser_t1078_il2cpp_TypeInfo;
static const Il2CppType* ILocalUser_t1078_InterfacesTypeInfos[] = 
{
	&IUserProfile_t3147_0_0_0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILocalUser_t1078_1_0_0;
struct ILocalUser_t1078;
const Il2CppTypeDefinitionMetadata ILocalUser_t1078_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILocalUser_t1078_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5957/* methodStart */
	, -1/* eventStart */
	, 1182/* propertyStart */

};
TypeInfo ILocalUser_t1078_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &ILocalUser_t1078_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILocalUser_t1078_0_0_0/* byval_arg */
	, &ILocalUser_t1078_1_0_0/* this_arg */
	, &ILocalUser_t1078_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern TypeInfo UserState_t1043_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
static const EncodedMethodIndex UserState_t1043_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair UserState_t1043_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserState_t1043_0_0_0;
extern const Il2CppType UserState_t1043_1_0_0;
const Il2CppTypeDefinitionMetadata UserState_t1043_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserState_t1043_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, UserState_t1043_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4845/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UserState_t1043_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserState_t1043_0_0_0/* byval_arg */
	, &UserState_t1043_1_0_0/* this_arg */
	, &UserState_t1043_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t1043)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserState_t1043)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
extern TypeInfo IUserProfile_t3147_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IUserProfile_t3147_1_0_0;
struct IUserProfile_t3147;
const Il2CppTypeDefinitionMetadata IUserProfile_t3147_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IUserProfile_t3147_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IUserProfile_t3147_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUserProfile_t3147_0_0_0/* byval_arg */
	, &IUserProfile_t3147_1_0_0/* this_arg */
	, &IUserProfile_t3147_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
extern TypeInfo IAchievement_t1082_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievement_t1082_1_0_0;
struct IAchievement_t1082;
const Il2CppTypeDefinitionMetadata IAchievement_t1082_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IAchievement_t1082_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IAchievement_t1082_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievement_t1082_0_0_0/* byval_arg */
	, &IAchievement_t1082_1_0_0/* this_arg */
	, &IAchievement_t1082_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
extern TypeInfo IAchievementDescription_t3146_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievementDescription_t3146_1_0_0;
struct IAchievementDescription_t3146;
const Il2CppTypeDefinitionMetadata IAchievementDescription_t3146_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IAchievementDescription_t3146_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IAchievementDescription_t3146_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievementDescription_t3146_0_0_0/* byval_arg */
	, &IAchievementDescription_t3146_1_0_0/* this_arg */
	, &IAchievementDescription_t3146_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IScore
extern TypeInfo IScore_t1036_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IScore_t1036_1_0_0;
struct IScore_t1036;
const Il2CppTypeDefinitionMetadata IScore_t1036_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IScore_t1036_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &IScore_t1036_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScore_t1036_0_0_0/* byval_arg */
	, &IScore_t1036_1_0_0/* this_arg */
	, &IScore_t1036_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern TypeInfo UserScope_t1044_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
static const EncodedMethodIndex UserScope_t1044_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair UserScope_t1044_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserScope_t1044_0_0_0;
extern const Il2CppType UserScope_t1044_1_0_0;
const Il2CppTypeDefinitionMetadata UserScope_t1044_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserScope_t1044_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, UserScope_t1044_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4851/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UserScope_t1044_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserScope_t1044_0_0_0/* byval_arg */
	, &UserScope_t1044_1_0_0/* this_arg */
	, &UserScope_t1044_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t1044)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserScope_t1044)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern TypeInfo TimeScope_t1045_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
static const EncodedMethodIndex TimeScope_t1045_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TimeScope_t1045_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TimeScope_t1045_0_0_0;
extern const Il2CppType TimeScope_t1045_1_0_0;
const Il2CppTypeDefinitionMetadata TimeScope_t1045_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimeScope_t1045_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TimeScope_t1045_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4854/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TimeScope_t1045_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimeScope_t1045_0_0_0/* byval_arg */
	, &TimeScope_t1045_1_0_0/* this_arg */
	, &TimeScope_t1045_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t1045)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeScope_t1045)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern TypeInfo Range_t1038_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
static const EncodedMethodIndex Range_t1038_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Range_t1038_0_0_0;
extern const Il2CppType Range_t1038_1_0_0;
const Il2CppTypeDefinitionMetadata Range_t1038_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Range_t1038_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4858/* fieldStart */
	, 5958/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Range_t1038_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &Range_t1038_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Range_t1038_0_0_0/* byval_arg */
	, &Range_t1038_1_0_0/* this_arg */
	, &Range_t1038_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t1038)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Range_t1038)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Range_t1038 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern TypeInfo ILeaderboard_t1081_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILeaderboard_t1081_1_0_0;
struct ILeaderboard_t1081;
const Il2CppTypeDefinitionMetadata ILeaderboard_t1081_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5959/* methodStart */
	, -1/* eventStart */
	, 1183/* propertyStart */

};
TypeInfo ILeaderboard_t1081_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, NULL/* methods */
	, &ILeaderboard_t1081_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILeaderboard_t1081_0_0_0/* byval_arg */
	, &ILeaderboard_t1081_1_0_0/* this_arg */
	, &ILeaderboard_t1081_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// Metadata Definition UnityEngine.PropertyAttribute
extern TypeInfo PropertyAttribute_t1046_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
static const EncodedMethodIndex PropertyAttribute_t1046_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair PropertyAttribute_t1046_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PropertyAttribute_t1046_0_0_0;
extern const Il2CppType PropertyAttribute_t1046_1_0_0;
struct PropertyAttribute_t1046;
const Il2CppTypeDefinitionMetadata PropertyAttribute_t1046_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttribute_t1046_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, PropertyAttribute_t1046_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5963/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PropertyAttribute_t1046_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &PropertyAttribute_t1046_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2245/* custom_attributes_cache */
	, &PropertyAttribute_t1046_0_0_0/* byval_arg */
	, &PropertyAttribute_t1046_1_0_0/* this_arg */
	, &PropertyAttribute_t1046_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t1046)/* instance_size */
	, sizeof (PropertyAttribute_t1046)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// Metadata Definition UnityEngine.TooltipAttribute
extern TypeInfo TooltipAttribute_t1047_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
static const EncodedMethodIndex TooltipAttribute_t1047_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t1047_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TooltipAttribute_t1047_0_0_0;
extern const Il2CppType TooltipAttribute_t1047_1_0_0;
struct TooltipAttribute_t1047;
const Il2CppTypeDefinitionMetadata TooltipAttribute_t1047_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TooltipAttribute_t1047_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1046_0_0_0/* parent */
	, TooltipAttribute_t1047_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4860/* fieldStart */
	, 5964/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TooltipAttribute_t1047_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TooltipAttribute_t1047_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2246/* custom_attributes_cache */
	, &TooltipAttribute_t1047_0_0_0/* byval_arg */
	, &TooltipAttribute_t1047_1_0_0/* this_arg */
	, &TooltipAttribute_t1047_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t1047)/* instance_size */
	, sizeof (TooltipAttribute_t1047)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// Metadata Definition UnityEngine.SpaceAttribute
extern TypeInfo SpaceAttribute_t1048_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
static const EncodedMethodIndex SpaceAttribute_t1048_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t1048_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SpaceAttribute_t1048_0_0_0;
extern const Il2CppType SpaceAttribute_t1048_1_0_0;
struct SpaceAttribute_t1048;
const Il2CppTypeDefinitionMetadata SpaceAttribute_t1048_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpaceAttribute_t1048_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1046_0_0_0/* parent */
	, SpaceAttribute_t1048_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4861/* fieldStart */
	, 5965/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SpaceAttribute_t1048_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SpaceAttribute_t1048_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2247/* custom_attributes_cache */
	, &SpaceAttribute_t1048_0_0_0/* byval_arg */
	, &SpaceAttribute_t1048_1_0_0/* this_arg */
	, &SpaceAttribute_t1048_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t1048)/* instance_size */
	, sizeof (SpaceAttribute_t1048)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// Metadata Definition UnityEngine.RangeAttribute
extern TypeInfo RangeAttribute_t1049_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
static const EncodedMethodIndex RangeAttribute_t1049_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair RangeAttribute_t1049_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RangeAttribute_t1049_0_0_0;
extern const Il2CppType RangeAttribute_t1049_1_0_0;
struct RangeAttribute_t1049;
const Il2CppTypeDefinitionMetadata RangeAttribute_t1049_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RangeAttribute_t1049_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1046_0_0_0/* parent */
	, RangeAttribute_t1049_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4862/* fieldStart */
	, 5966/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RangeAttribute_t1049_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &RangeAttribute_t1049_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2248/* custom_attributes_cache */
	, &RangeAttribute_t1049_0_0_0/* byval_arg */
	, &RangeAttribute_t1049_1_0_0/* this_arg */
	, &RangeAttribute_t1049_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t1049)/* instance_size */
	, sizeof (RangeAttribute_t1049)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// Metadata Definition UnityEngine.TextAreaAttribute
extern TypeInfo TextAreaAttribute_t1050_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
static const EncodedMethodIndex TextAreaAttribute_t1050_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t1050_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextAreaAttribute_t1050_0_0_0;
extern const Il2CppType TextAreaAttribute_t1050_1_0_0;
struct TextAreaAttribute_t1050;
const Il2CppTypeDefinitionMetadata TextAreaAttribute_t1050_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAreaAttribute_t1050_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1046_0_0_0/* parent */
	, TextAreaAttribute_t1050_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4864/* fieldStart */
	, 5967/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextAreaAttribute_t1050_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextAreaAttribute_t1050_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2249/* custom_attributes_cache */
	, &TextAreaAttribute_t1050_0_0_0/* byval_arg */
	, &TextAreaAttribute_t1050_1_0_0/* this_arg */
	, &TextAreaAttribute_t1050_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t1050)/* instance_size */
	, sizeof (TextAreaAttribute_t1050)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern TypeInfo SelectionBaseAttribute_t1051_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
static const EncodedMethodIndex SelectionBaseAttribute_t1051_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t1051_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SelectionBaseAttribute_t1051_0_0_0;
extern const Il2CppType SelectionBaseAttribute_t1051_1_0_0;
struct SelectionBaseAttribute_t1051;
const Il2CppTypeDefinitionMetadata SelectionBaseAttribute_t1051_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionBaseAttribute_t1051_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SelectionBaseAttribute_t1051_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5968/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SelectionBaseAttribute_t1051_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SelectionBaseAttribute_t1051_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2250/* custom_attributes_cache */
	, &SelectionBaseAttribute_t1051_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t1051_1_0_0/* this_arg */
	, &SelectionBaseAttribute_t1051_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t1051)/* instance_size */
	, sizeof (SelectionBaseAttribute_t1051)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// Metadata Definition UnityEngine.SliderState
extern TypeInfo SliderState_t1052_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
static const EncodedMethodIndex SliderState_t1052_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SliderState_t1052_0_0_0;
extern const Il2CppType SliderState_t1052_1_0_0;
struct SliderState_t1052;
const Il2CppTypeDefinitionMetadata SliderState_t1052_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SliderState_t1052_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4866/* fieldStart */
	, 5969/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SliderState_t1052_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SliderState_t1052_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderState_t1052_0_0_0/* byval_arg */
	, &SliderState_t1052_1_0_0/* this_arg */
	, &SliderState_t1052_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t1052)/* instance_size */
	, sizeof (SliderState_t1052)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// Metadata Definition UnityEngine.StackTraceUtility
extern TypeInfo StackTraceUtility_t1053_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
static const EncodedMethodIndex StackTraceUtility_t1053_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StackTraceUtility_t1053_0_0_0;
extern const Il2CppType StackTraceUtility_t1053_1_0_0;
struct StackTraceUtility_t1053;
const Il2CppTypeDefinitionMetadata StackTraceUtility_t1053_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTraceUtility_t1053_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4869/* fieldStart */
	, 5970/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StackTraceUtility_t1053_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &StackTraceUtility_t1053_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StackTraceUtility_t1053_0_0_0/* byval_arg */
	, &StackTraceUtility_t1053_1_0_0/* this_arg */
	, &StackTraceUtility_t1053_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t1053)/* instance_size */
	, sizeof (StackTraceUtility_t1053)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t1053_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// Metadata Definition UnityEngine.UnityException
extern TypeInfo UnityException_t804_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
static const EncodedMethodIndex UnityException_t804_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair UnityException_t804_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityException_t804_0_0_0;
extern const Il2CppType UnityException_t804_1_0_0;
extern const Il2CppType Exception_t520_0_0_0;
struct UnityException_t804;
const Il2CppTypeDefinitionMetadata UnityException_t804_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityException_t804_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, UnityException_t804_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4870/* fieldStart */
	, 5979/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityException_t804_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UnityException_t804_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityException_t804_0_0_0/* byval_arg */
	, &UnityException_t804_1_0_0/* this_arg */
	, &UnityException_t804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t804)/* instance_size */
	, sizeof (UnityException_t804)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern TypeInfo SharedBetweenAnimatorsAttribute_t1054_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
static const EncodedMethodIndex SharedBetweenAnimatorsAttribute_t1054_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t1054_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t1054_0_0_0;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t1054_1_0_0;
struct SharedBetweenAnimatorsAttribute_t1054;
const Il2CppTypeDefinitionMetadata SharedBetweenAnimatorsAttribute_t1054_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SharedBetweenAnimatorsAttribute_t1054_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SharedBetweenAnimatorsAttribute_t1054_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5983/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SharedBetweenAnimatorsAttribute_t1054_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &SharedBetweenAnimatorsAttribute_t1054_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2254/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t1054_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t1054_1_0_0/* this_arg */
	, &SharedBetweenAnimatorsAttribute_t1054_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t1054)/* instance_size */
	, sizeof (SharedBetweenAnimatorsAttribute_t1054)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// Metadata Definition UnityEngine.StateMachineBehaviour
extern TypeInfo StateMachineBehaviour_t1055_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
static const EncodedMethodIndex StateMachineBehaviour_t1055_VTable[11] = 
{
	600,
	601,
	602,
	603,
	1544,
	1545,
	1546,
	1547,
	1548,
	1549,
	1550,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StateMachineBehaviour_t1055_0_0_0;
extern const Il2CppType StateMachineBehaviour_t1055_1_0_0;
extern const Il2CppType ScriptableObject_t838_0_0_0;
struct StateMachineBehaviour_t1055;
const Il2CppTypeDefinitionMetadata StateMachineBehaviour_t1055_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t838_0_0_0/* parent */
	, StateMachineBehaviour_t1055_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 5984/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StateMachineBehaviour_t1055_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &StateMachineBehaviour_t1055_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StateMachineBehaviour_t1055_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t1055_1_0_0/* this_arg */
	, &StateMachineBehaviour_t1055_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t1055)/* instance_size */
	, sizeof (StateMachineBehaviour_t1055)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// Metadata Definition UnityEngine.TextEditor
extern TypeInfo TextEditor_t806_il2cpp_TypeInfo;
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
extern const Il2CppType DblClickSnapping_t1056_0_0_0;
extern const Il2CppType TextEditOp_t1057_0_0_0;
static const Il2CppType* TextEditor_t806_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DblClickSnapping_t1056_0_0_0,
	&TextEditOp_t1057_0_0_0,
};
static const EncodedMethodIndex TextEditor_t806_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditor_t806_0_0_0;
extern const Il2CppType TextEditor_t806_1_0_0;
struct TextEditor_t806;
const Il2CppTypeDefinitionMetadata TextEditor_t806_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextEditor_t806_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextEditor_t806_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4872/* fieldStart */
	, 5992/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextEditor_t806_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextEditor_t806_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditor_t806_0_0_0/* byval_arg */
	, &TextEditor_t806_1_0_0/* this_arg */
	, &TextEditor_t806_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t806)/* instance_size */
	, sizeof (TextEditor_t806)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t806_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern TypeInfo DblClickSnapping_t1056_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
static const EncodedMethodIndex DblClickSnapping_t1056_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t1056_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DblClickSnapping_t1056_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t1117_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DblClickSnapping_t1056_DefinitionMetadata = 
{
	&TextEditor_t806_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DblClickSnapping_t1056_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DblClickSnapping_t1056_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4896/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DblClickSnapping_t1056_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DblClickSnapping_t1056_0_0_0/* byval_arg */
	, &DblClickSnapping_t1056_1_0_0/* this_arg */
	, &DblClickSnapping_t1056_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t1056)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DblClickSnapping_t1056)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern TypeInfo TextEditOp_t1057_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
static const EncodedMethodIndex TextEditOp_t1057_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TextEditOp_t1057_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditOp_t1057_1_0_0;
const Il2CppTypeDefinitionMetadata TextEditOp_t1057_DefinitionMetadata = 
{
	&TextEditor_t806_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextEditOp_t1057_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TextEditOp_t1057_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4899/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextEditOp_t1057_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditOp_t1057_0_0_0/* byval_arg */
	, &TextEditOp_t1057_1_0_0/* this_arg */
	, &TextEditOp_t1057_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t1057)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextEditOp_t1057)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// Metadata Definition UnityEngine.TextGenerationSettings
extern TypeInfo TextGenerationSettings_t773_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
static const EncodedMethodIndex TextGenerationSettings_t773_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerationSettings_t773_0_0_0;
extern const Il2CppType TextGenerationSettings_t773_1_0_0;
const Il2CppTypeDefinitionMetadata TextGenerationSettings_t773_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, TextGenerationSettings_t773_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4950/* fieldStart */
	, 6002/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextGenerationSettings_t773_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TextGenerationSettings_t773_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerationSettings_t773_0_0_0/* byval_arg */
	, &TextGenerationSettings_t773_1_0_0/* this_arg */
	, &TextGenerationSettings_t773_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t773)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextGenerationSettings_t773)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// Metadata Definition UnityEngine.TrackedReference
extern TypeInfo TrackedReference_t945_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
static const EncodedMethodIndex TrackedReference_t945_VTable[4] = 
{
	1393,
	601,
	1394,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TrackedReference_t945_0_0_0;
extern const Il2CppType TrackedReference_t945_1_0_0;
struct TrackedReference_t945;
const Il2CppTypeDefinitionMetadata TrackedReference_t945_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackedReference_t945_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4967/* fieldStart */
	, 6005/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TrackedReference_t945_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &TrackedReference_t945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrackedReference_t945_0_0_0/* byval_arg */
	, &TrackedReference_t945_1_0_0/* this_arg */
	, &TrackedReference_t945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t945_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t945_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t945_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t945)/* instance_size */
	, sizeof (TrackedReference_t945)/* actualSize */
	, 0/* element_size */
	, sizeof(TrackedReference_t945_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern TypeInfo PersistentListenerMode_t1059_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
static const EncodedMethodIndex PersistentListenerMode_t1059_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t1059_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentListenerMode_t1059_0_0_0;
extern const Il2CppType PersistentListenerMode_t1059_1_0_0;
const Il2CppTypeDefinitionMetadata PersistentListenerMode_t1059_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PersistentListenerMode_t1059_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, PersistentListenerMode_t1059_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4968/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PersistentListenerMode_t1059_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentListenerMode_t1059_0_0_0/* byval_arg */
	, &PersistentListenerMode_t1059_1_0_0/* this_arg */
	, &PersistentListenerMode_t1059_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t1059)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PersistentListenerMode_t1059)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// Metadata Definition UnityEngine.Events.ArgumentCache
extern TypeInfo ArgumentCache_t1060_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
static const EncodedMethodIndex ArgumentCache_t1060_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1551,
	1552,
};
extern const Il2CppType ISerializationCallbackReceiver_t3399_0_0_0;
static const Il2CppType* ArgumentCache_t1060_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t3399_0_0_0,
};
static Il2CppInterfaceOffsetPair ArgumentCache_t1060_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ArgumentCache_t1060_0_0_0;
extern const Il2CppType ArgumentCache_t1060_1_0_0;
struct ArgumentCache_t1060;
const Il2CppTypeDefinitionMetadata ArgumentCache_t1060_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ArgumentCache_t1060_InterfacesTypeInfos/* implementedInterfaces */
	, ArgumentCache_t1060_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgumentCache_t1060_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4976/* fieldStart */
	, 6008/* methodStart */
	, -1/* eventStart */
	, 1187/* propertyStart */

};
TypeInfo ArgumentCache_t1060_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &ArgumentCache_t1060_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgumentCache_t1060_0_0_0/* byval_arg */
	, &ArgumentCache_t1060_1_0_0/* this_arg */
	, &ArgumentCache_t1060_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t1060)/* instance_size */
	, sizeof (ArgumentCache_t1060)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern TypeInfo BaseInvokableCall_t1061_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
extern const Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m24240_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m24240_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6641 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, 6641 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex BaseInvokableCall_t1061_VTable[6] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BaseInvokableCall_t1061_0_0_0;
extern const Il2CppType BaseInvokableCall_t1061_1_0_0;
struct BaseInvokableCall_t1061;
const Il2CppTypeDefinitionMetadata BaseInvokableCall_t1061_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseInvokableCall_t1061_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6018/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BaseInvokableCall_t1061_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &BaseInvokableCall_t1061_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseInvokableCall_t1061_0_0_0/* byval_arg */
	, &BaseInvokableCall_t1061_1_0_0/* this_arg */
	, &BaseInvokableCall_t1061_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t1061)/* instance_size */
	, sizeof (BaseInvokableCall_t1061)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
// Metadata Definition UnityEngine.Events.InvokableCall
extern TypeInfo InvokableCall_t1062_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
static const EncodedMethodIndex InvokableCall_t1062_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1553,
	1554,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_t1062_0_0_0;
extern const Il2CppType InvokableCall_t1062_1_0_0;
struct InvokableCall_t1062;
const Il2CppTypeDefinitionMetadata InvokableCall_t1062_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1061_0_0_0/* parent */
	, InvokableCall_t1062_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4982/* fieldStart */
	, 6024/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvokableCall_t1062_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &InvokableCall_t1062_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_t1062_0_0_0/* byval_arg */
	, &InvokableCall_t1062_1_0_0/* this_arg */
	, &InvokableCall_t1062_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t1062)/* instance_size */
	, sizeof (InvokableCall_t1062)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`1
extern TypeInfo InvokableCall_1_t3407_il2cpp_TypeInfo;
static const EncodedMethodIndex InvokableCall_1_t3407_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1555,
	1556,
};
extern const Il2CppType UnityAction_1_t3662_0_0_0;
extern const Il2CppType InvokableCall_1_t3407_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition InvokableCall_1_t3407_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 4251 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4251 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5469 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4252 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5470 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_1_t3407_0_0_0;
extern const Il2CppType InvokableCall_1_t3407_1_0_0;
struct InvokableCall_1_t3407;
const Il2CppTypeDefinitionMetadata InvokableCall_1_t3407_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1061_0_0_0/* parent */
	, InvokableCall_1_t3407_VTable/* vtableMethods */
	, InvokableCall_1_t3407_RGCTXData/* rgctxDefinition */
	, 4983/* fieldStart */
	, 6027/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvokableCall_1_t3407_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &InvokableCall_1_t3407_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_1_t3407_0_0_0/* byval_arg */
	, &InvokableCall_1_t3407_1_0_0/* this_arg */
	, &InvokableCall_1_t3407_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 46/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
