﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t2266;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2280;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C" void Stack_1__ctor_m14012_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1__ctor_m14012(__this, method) (( void (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1__ctor_m14012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m14013_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m14013(__this, method) (( bool (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m14013_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Stack_1_System_Collections_ICollection_get_SyncRoot_m14014_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m14014(__this, method) (( Object_t * (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m14014_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Stack_1_System_Collections_ICollection_CopyTo_m14015_gshared (Stack_1_t2266 * __this, Array_t * ___dest, int32_t ___idx, const MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m14015(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2266 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m14015_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14016_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14016(__this, method) (( Object_t* (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14016_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Stack_1_System_Collections_IEnumerable_GetEnumerator_m14017_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m14017(__this, method) (( Object_t * (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m14017_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Peek()
extern "C" Object_t * Stack_1_Peek_m14018_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_Peek_m14018(__this, method) (( Object_t * (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_Peek_m14018_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" Object_t * Stack_1_Pop_m14019_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_Pop_m14019(__this, method) (( Object_t * (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_Pop_m14019_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
extern "C" void Stack_1_Push_m14020_gshared (Stack_1_t2266 * __this, Object_t * ___t, const MethodInfo* method);
#define Stack_1_Push_m14020(__this, ___t, method) (( void (*) (Stack_1_t2266 *, Object_t *, const MethodInfo*))Stack_1_Push_m14020_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C" int32_t Stack_1_get_Count_m14021_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_get_Count_m14021(__this, method) (( int32_t (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_get_Count_m14021_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2269  Stack_1_GetEnumerator_m14022_gshared (Stack_1_t2266 * __this, const MethodInfo* method);
#define Stack_1_GetEnumerator_m14022(__this, method) (( Enumerator_t2269  (*) (Stack_1_t2266 *, const MethodInfo*))Stack_1_GetEnumerator_m14022_gshared)(__this, method)
