﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t470;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent
struct  UnityEvent_t633  : public UnityEventBase_t1069
{
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t470* ___m_InvokeArray_4;
};
