﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<iTween/LoopType>
struct IList_1_t570;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<iTween/LoopType>
struct  Collection_1_t2460  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<iTween/LoopType>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<iTween/LoopType>::syncRoot
	Object_t * ___syncRoot_1;
};
