﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0MethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$20
void U24ArrayTypeU2420_t2156_marshal(const U24ArrayTypeU2420_t2156& unmarshaled, U24ArrayTypeU2420_t2156_marshaled& marshaled)
{
}
void U24ArrayTypeU2420_t2156_marshal_back(const U24ArrayTypeU2420_t2156_marshaled& marshaled, U24ArrayTypeU2420_t2156& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$20
void U24ArrayTypeU2420_t2156_marshal_cleanup(U24ArrayTypeU2420_t2156_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$32
void U24ArrayTypeU2432_t2157_marshal(const U24ArrayTypeU2432_t2157& unmarshaled, U24ArrayTypeU2432_t2157_marshaled& marshaled)
{
}
void U24ArrayTypeU2432_t2157_marshal_back(const U24ArrayTypeU2432_t2157_marshaled& marshaled, U24ArrayTypeU2432_t2157& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$32
void U24ArrayTypeU2432_t2157_marshal_cleanup(U24ArrayTypeU2432_t2157_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$48
void U24ArrayTypeU2448_t2158_marshal(const U24ArrayTypeU2448_t2158& unmarshaled, U24ArrayTypeU2448_t2158_marshaled& marshaled)
{
}
void U24ArrayTypeU2448_t2158_marshal_back(const U24ArrayTypeU2448_t2158_marshaled& marshaled, U24ArrayTypeU2448_t2158& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$48
void U24ArrayTypeU2448_t2158_marshal_cleanup(U24ArrayTypeU2448_t2158_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$64
void U24ArrayTypeU2464_t2159_marshal(const U24ArrayTypeU2464_t2159& unmarshaled, U24ArrayTypeU2464_t2159_marshaled& marshaled)
{
}
void U24ArrayTypeU2464_t2159_marshal_back(const U24ArrayTypeU2464_t2159_marshaled& marshaled, U24ArrayTypeU2464_t2159& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$64
void U24ArrayTypeU2464_t2159_marshal_cleanup(U24ArrayTypeU2464_t2159_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void U24ArrayTypeU2412_t2160_marshal(const U24ArrayTypeU2412_t2160& unmarshaled, U24ArrayTypeU2412_t2160_marshaled& marshaled)
{
}
void U24ArrayTypeU2412_t2160_marshal_back(const U24ArrayTypeU2412_t2160_marshaled& marshaled, U24ArrayTypeU2412_t2160& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$12
void U24ArrayTypeU2412_t2160_marshal_cleanup(U24ArrayTypeU2412_t2160_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void U24ArrayTypeU24136_t2161_marshal(const U24ArrayTypeU24136_t2161& unmarshaled, U24ArrayTypeU24136_t2161_marshaled& marshaled)
{
}
void U24ArrayTypeU24136_t2161_marshal_back(const U24ArrayTypeU24136_t2161_marshaled& marshaled, U24ArrayTypeU24136_t2161& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$136
void U24ArrayTypeU24136_t2161_marshal_cleanup(U24ArrayTypeU24136_t2161_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$8
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$8
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$8
void U24ArrayTypeU248_t2162_marshal(const U24ArrayTypeU248_t2162& unmarshaled, U24ArrayTypeU248_t2162_marshaled& marshaled)
{
}
void U24ArrayTypeU248_t2162_marshal_back(const U24ArrayTypeU248_t2162_marshaled& marshaled, U24ArrayTypeU248_t2162& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$8
void U24ArrayTypeU248_t2162_marshal_cleanup(U24ArrayTypeU248_t2162_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$72
void U24ArrayTypeU2472_t2163_marshal(const U24ArrayTypeU2472_t2163& unmarshaled, U24ArrayTypeU2472_t2163_marshaled& marshaled)
{
}
void U24ArrayTypeU2472_t2163_marshal_back(const U24ArrayTypeU2472_t2163_marshaled& marshaled, U24ArrayTypeU2472_t2163& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$72
void U24ArrayTypeU2472_t2163_marshal_cleanup(U24ArrayTypeU2472_t2163_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$124
void U24ArrayTypeU24124_t2164_marshal(const U24ArrayTypeU24124_t2164& unmarshaled, U24ArrayTypeU24124_t2164_marshaled& marshaled)
{
}
void U24ArrayTypeU24124_t2164_marshal_back(const U24ArrayTypeU24124_t2164_marshaled& marshaled, U24ArrayTypeU24124_t2164& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$124
void U24ArrayTypeU24124_t2164_marshal_cleanup(U24ArrayTypeU24124_t2164_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$96
void U24ArrayTypeU2496_t2165_marshal(const U24ArrayTypeU2496_t2165& unmarshaled, U24ArrayTypeU2496_t2165_marshaled& marshaled)
{
}
void U24ArrayTypeU2496_t2165_marshal_back(const U24ArrayTypeU2496_t2165_marshaled& marshaled, U24ArrayTypeU2496_t2165& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$96
void U24ArrayTypeU2496_t2165_marshal_cleanup(U24ArrayTypeU2496_t2165_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$2048
void U24ArrayTypeU242048_t2166_marshal(const U24ArrayTypeU242048_t2166& unmarshaled, U24ArrayTypeU242048_t2166_marshaled& marshaled)
{
}
void U24ArrayTypeU242048_t2166_marshal_back(const U24ArrayTypeU242048_t2166_marshaled& marshaled, U24ArrayTypeU242048_t2166& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$2048
void U24ArrayTypeU242048_t2166_marshal_cleanup(U24ArrayTypeU242048_t2166_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$256
void U24ArrayTypeU24256_t2167_marshal(const U24ArrayTypeU24256_t2167& unmarshaled, U24ArrayTypeU24256_t2167_marshaled& marshaled)
{
}
void U24ArrayTypeU24256_t2167_marshal_back(const U24ArrayTypeU24256_t2167_marshaled& marshaled, U24ArrayTypeU24256_t2167& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$256
void U24ArrayTypeU24256_t2167_marshal_cleanup(U24ArrayTypeU24256_t2167_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$1024
void U24ArrayTypeU241024_t2168_marshal(const U24ArrayTypeU241024_t2168& unmarshaled, U24ArrayTypeU241024_t2168_marshaled& marshaled)
{
}
void U24ArrayTypeU241024_t2168_marshal_back(const U24ArrayTypeU241024_t2168_marshaled& marshaled, U24ArrayTypeU241024_t2168& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$1024
void U24ArrayTypeU241024_t2168_marshal_cleanup(U24ArrayTypeU241024_t2168_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$640
void U24ArrayTypeU24640_t2169_marshal(const U24ArrayTypeU24640_t2169& unmarshaled, U24ArrayTypeU24640_t2169_marshaled& marshaled)
{
}
void U24ArrayTypeU24640_t2169_marshal_back(const U24ArrayTypeU24640_t2169_marshaled& marshaled, U24ArrayTypeU24640_t2169& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$640
void U24ArrayTypeU24640_t2169_marshal_cleanup(U24ArrayTypeU24640_t2169_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$128
void U24ArrayTypeU24128_t2170_marshal(const U24ArrayTypeU24128_t2170& unmarshaled, U24ArrayTypeU24128_t2170_marshaled& marshaled)
{
}
void U24ArrayTypeU24128_t2170_marshal_back(const U24ArrayTypeU24128_t2170_marshaled& marshaled, U24ArrayTypeU24128_t2170& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$128
void U24ArrayTypeU24128_t2170_marshal_cleanup(U24ArrayTypeU24128_t2170_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$52
void U24ArrayTypeU2452_t2171_marshal(const U24ArrayTypeU2452_t2171& unmarshaled, U24ArrayTypeU2452_t2171_marshaled& marshaled)
{
}
void U24ArrayTypeU2452_t2171_marshal_back(const U24ArrayTypeU2452_t2171_marshaled& marshaled, U24ArrayTypeU2452_t2171& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$52
void U24ArrayTypeU2452_t2171_marshal_cleanup(U24ArrayTypeU2452_t2171_marshaled& marshaled)
{
}
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"



#ifdef __clang__
#pragma clang diagnostic pop
#endif
