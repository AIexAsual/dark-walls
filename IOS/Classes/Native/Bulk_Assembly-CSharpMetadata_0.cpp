﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType U3CModuleU3E_t0_0_0_0;
extern const Il2CppType U3CModuleU3E_t0_1_0_0;
struct U3CModuleU3E_t0;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t0_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t0_0_0_0/* byval_arg */
	, &U3CModuleU3E_t0_1_0_0/* this_arg */
	, &U3CModuleU3E_t0_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t0)/* instance_size */
	, sizeof (U3CModuleU3E_t0)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_AAA_SuperComputer
#include "AssemblyU2DCSharp_CameraFilterPack_AAA_SuperComputer.h"
// Metadata Definition CameraFilterPack_AAA_SuperComputer
extern TypeInfo CameraFilterPack_AAA_SuperComputer_t3_il2cpp_TypeInfo;
// CameraFilterPack_AAA_SuperComputer
#include "AssemblyU2DCSharp_CameraFilterPack_AAA_SuperComputerMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_AAA_SuperComputer_t3_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_AAA_SuperComputer_t3_0_0_0;
extern const Il2CppType CameraFilterPack_AAA_SuperComputer_t3_1_0_0;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
struct CameraFilterPack_AAA_SuperComputer_t3;
const Il2CppTypeDefinitionMetadata CameraFilterPack_AAA_SuperComputer_t3_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_AAA_SuperComputer_t3_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */
	, 0/* methodStart */
	, -1/* eventStart */
	, 0/* propertyStart */

};
TypeInfo CameraFilterPack_AAA_SuperComputer_t3_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_AAA_SuperComputer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_AAA_SuperComputer_t3_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &CameraFilterPack_AAA_SuperComputer_t3_0_0_0/* byval_arg */
	, &CameraFilterPack_AAA_SuperComputer_t3_1_0_0/* this_arg */
	, &CameraFilterPack_AAA_SuperComputer_t3_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_AAA_SuperComputer_t3)/* instance_size */
	, sizeof (CameraFilterPack_AAA_SuperComputer_t3)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_AAA_SuperComputer_t3_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_AAA_SuperHexagon
#include "AssemblyU2DCSharp_CameraFilterPack_AAA_SuperHexagon.h"
// Metadata Definition CameraFilterPack_AAA_SuperHexagon
extern TypeInfo CameraFilterPack_AAA_SuperHexagon_t8_il2cpp_TypeInfo;
// CameraFilterPack_AAA_SuperHexagon
#include "AssemblyU2DCSharp_CameraFilterPack_AAA_SuperHexagonMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_AAA_SuperHexagon_t8_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_AAA_SuperHexagon_t8_0_0_0;
extern const Il2CppType CameraFilterPack_AAA_SuperHexagon_t8_1_0_0;
struct CameraFilterPack_AAA_SuperHexagon_t8;
const Il2CppTypeDefinitionMetadata CameraFilterPack_AAA_SuperHexagon_t8_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_AAA_SuperHexagon_t8_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 20/* fieldStart */
	, 8/* methodStart */
	, -1/* eventStart */
	, 1/* propertyStart */

};
TypeInfo CameraFilterPack_AAA_SuperHexagon_t8_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_AAA_SuperHexagon"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_AAA_SuperHexagon_t8_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 7/* custom_attributes_cache */
	, &CameraFilterPack_AAA_SuperHexagon_t8_0_0_0/* byval_arg */
	, &CameraFilterPack_AAA_SuperHexagon_t8_1_0_0/* this_arg */
	, &CameraFilterPack_AAA_SuperHexagon_t8_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_AAA_SuperHexagon_t8)/* instance_size */
	, sizeof (CameraFilterPack_AAA_SuperHexagon_t8)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_AAA_SuperHexagon_t8_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_AAA_WaterDrop
#include "AssemblyU2DCSharp_CameraFilterPack_AAA_WaterDrop.h"
// Metadata Definition CameraFilterPack_AAA_WaterDrop
extern TypeInfo CameraFilterPack_AAA_WaterDrop_t10_il2cpp_TypeInfo;
// CameraFilterPack_AAA_WaterDrop
#include "AssemblyU2DCSharp_CameraFilterPack_AAA_WaterDropMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_AAA_WaterDrop_t10_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_AAA_WaterDrop_t10_0_0_0;
extern const Il2CppType CameraFilterPack_AAA_WaterDrop_t10_1_0_0;
struct CameraFilterPack_AAA_WaterDrop_t10;
const Il2CppTypeDefinitionMetadata CameraFilterPack_AAA_WaterDrop_t10_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_AAA_WaterDrop_t10_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 40/* fieldStart */
	, 16/* methodStart */
	, -1/* eventStart */
	, 2/* propertyStart */

};
TypeInfo CameraFilterPack_AAA_WaterDrop_t10_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_AAA_WaterDrop"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_AAA_WaterDrop_t10_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 10/* custom_attributes_cache */
	, &CameraFilterPack_AAA_WaterDrop_t10_0_0_0/* byval_arg */
	, &CameraFilterPack_AAA_WaterDrop_t10_1_0_0/* this_arg */
	, &CameraFilterPack_AAA_WaterDrop_t10_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_AAA_WaterDrop_t10)/* instance_size */
	, sizeof (CameraFilterPack_AAA_WaterDrop_t10)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_AAA_WaterDrop_t10_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Alien_Vision
#include "AssemblyU2DCSharp_CameraFilterPack_Alien_Vision.h"
// Metadata Definition CameraFilterPack_Alien_Vision
extern TypeInfo CameraFilterPack_Alien_Vision_t11_il2cpp_TypeInfo;
// CameraFilterPack_Alien_Vision
#include "AssemblyU2DCSharp_CameraFilterPack_Alien_VisionMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Alien_Vision_t11_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Alien_Vision_t11_0_0_0;
extern const Il2CppType CameraFilterPack_Alien_Vision_t11_1_0_0;
struct CameraFilterPack_Alien_Vision_t11;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Alien_Vision_t11_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Alien_Vision_t11_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 52/* fieldStart */
	, 23/* methodStart */
	, -1/* eventStart */
	, 3/* propertyStart */

};
TypeInfo CameraFilterPack_Alien_Vision_t11_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Alien_Vision"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Alien_Vision_t11_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 15/* custom_attributes_cache */
	, &CameraFilterPack_Alien_Vision_t11_0_0_0/* byval_arg */
	, &CameraFilterPack_Alien_Vision_t11_1_0_0/* this_arg */
	, &CameraFilterPack_Alien_Vision_t11_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Alien_Vision_t11)/* instance_size */
	, sizeof (CameraFilterPack_Alien_Vision_t11)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Alien_Vision_t11_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Antialiasing_FXAA
#include "AssemblyU2DCSharp_CameraFilterPack_Antialiasing_FXAA.h"
// Metadata Definition CameraFilterPack_Antialiasing_FXAA
extern TypeInfo CameraFilterPack_Antialiasing_FXAA_t12_il2cpp_TypeInfo;
// CameraFilterPack_Antialiasing_FXAA
#include "AssemblyU2DCSharp_CameraFilterPack_Antialiasing_FXAAMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Antialiasing_FXAA_t12_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Antialiasing_FXAA_t12_0_0_0;
extern const Il2CppType CameraFilterPack_Antialiasing_FXAA_t12_1_0_0;
struct CameraFilterPack_Antialiasing_FXAA_t12;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Antialiasing_FXAA_t12_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Antialiasing_FXAA_t12_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 64/* fieldStart */
	, 30/* methodStart */
	, -1/* eventStart */
	, 4/* propertyStart */

};
TypeInfo CameraFilterPack_Antialiasing_FXAA_t12_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Antialiasing_FXAA"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Antialiasing_FXAA_t12_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 20/* custom_attributes_cache */
	, &CameraFilterPack_Antialiasing_FXAA_t12_0_0_0/* byval_arg */
	, &CameraFilterPack_Antialiasing_FXAA_t12_1_0_0/* this_arg */
	, &CameraFilterPack_Antialiasing_FXAA_t12_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Antialiasing_FXAA_t12)/* instance_size */
	, sizeof (CameraFilterPack_Antialiasing_FXAA_t12)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Atmosphere_Snow_8bits
#include "AssemblyU2DCSharp_CameraFilterPack_Atmosphere_Snow_8bits.h"
// Metadata Definition CameraFilterPack_Atmosphere_Snow_8bits
extern TypeInfo CameraFilterPack_Atmosphere_Snow_8bits_t13_il2cpp_TypeInfo;
// CameraFilterPack_Atmosphere_Snow_8bits
#include "AssemblyU2DCSharp_CameraFilterPack_Atmosphere_Snow_8bitsMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Atmosphere_Snow_8bits_t13_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Atmosphere_Snow_8bits_t13_0_0_0;
extern const Il2CppType CameraFilterPack_Atmosphere_Snow_8bits_t13_1_0_0;
struct CameraFilterPack_Atmosphere_Snow_8bits_t13;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Atmosphere_Snow_8bits_t13_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Atmosphere_Snow_8bits_t13_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 68/* fieldStart */
	, 36/* methodStart */
	, -1/* eventStart */
	, 5/* propertyStart */

};
TypeInfo CameraFilterPack_Atmosphere_Snow_8bits_t13_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Atmosphere_Snow_8bits"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Atmosphere_Snow_8bits_t13_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 21/* custom_attributes_cache */
	, &CameraFilterPack_Atmosphere_Snow_8bits_t13_0_0_0/* byval_arg */
	, &CameraFilterPack_Atmosphere_Snow_8bits_t13_1_0_0/* this_arg */
	, &CameraFilterPack_Atmosphere_Snow_8bits_t13_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Atmosphere_Snow_8bits_t13)/* instance_size */
	, sizeof (CameraFilterPack_Atmosphere_Snow_8bits_t13)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Atmosphere_Snow_8bits_t13_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Blend
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Blend.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Blend
extern TypeInfo CameraFilterPack_Blend2Camera_Blend_t16_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Blend
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_BlendMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Blend_t16_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Blend_t16_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Blend_t16_1_0_0;
struct CameraFilterPack_Blend2Camera_Blend_t16;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Blend_t16_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Blend_t16_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 80/* fieldStart */
	, 43/* methodStart */
	, -1/* eventStart */
	, 6/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Blend_t16_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Blend"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Blend_t16_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 26/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Blend_t16_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Blend_t16_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Blend_t16_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Blend_t16)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Blend_t16)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Blend_t16_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_BlueScreen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_BlueScreen.h"
// Metadata Definition CameraFilterPack_Blend2Camera_BlueScreen
extern TypeInfo CameraFilterPack_Blend2Camera_BlueScreen_t17_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_BlueScreen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_BlueScreenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_BlueScreen_t17_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_BlueScreen_t17_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_BlueScreen_t17_1_0_0;
struct CameraFilterPack_Blend2Camera_BlueScreen_t17;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_BlueScreen_t17_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_BlueScreen_t17_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 89/* fieldStart */
	, 51/* methodStart */
	, -1/* eventStart */
	, 7/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_BlueScreen_t17_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_BlueScreen"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_BlueScreen_t17_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 28/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_BlueScreen_t17_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_BlueScreen_t17_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_BlueScreen_t17_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_BlueScreen_t17)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_BlueScreen_t17)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Color
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Color.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Color
extern TypeInfo CameraFilterPack_Blend2Camera_Color_t18_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Color
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_ColorMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Color_t18_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Color_t18_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Color_t18_1_0_0;
struct CameraFilterPack_Blend2Camera_Color_t18;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Color_t18_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Color_t18_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 104/* fieldStart */
	, 58/* methodStart */
	, -1/* eventStart */
	, 8/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Color_t18_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Color"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Color_t18_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 36/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Color_t18_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Color_t18_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Color_t18_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Color_t18)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Color_t18)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Color_t18_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_ColorBurn
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_ColorBurn.h"
// Metadata Definition CameraFilterPack_Blend2Camera_ColorBurn
extern TypeInfo CameraFilterPack_Blend2Camera_ColorBurn_t19_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_ColorBurn
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_ColorBurnMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_ColorBurn_t19_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_ColorBurn_t19_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_ColorBurn_t19_1_0_0;
struct CameraFilterPack_Blend2Camera_ColorBurn_t19;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_ColorBurn_t19_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_ColorBurn_t19_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 115/* fieldStart */
	, 66/* methodStart */
	, -1/* eventStart */
	, 9/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_ColorBurn_t19_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_ColorBurn"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_ColorBurn_t19_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 39/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_ColorBurn_t19_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_ColorBurn_t19_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_ColorBurn_t19_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_ColorBurn_t19)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_ColorBurn_t19)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_ColorBurn_t19_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_ColorDodge
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_ColorDodge.h"
// Metadata Definition CameraFilterPack_Blend2Camera_ColorDodge
extern TypeInfo CameraFilterPack_Blend2Camera_ColorDodge_t20_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_ColorDodge
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_ColorDodgeMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_ColorDodge_t20_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_ColorDodge_t20_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_ColorDodge_t20_1_0_0;
struct CameraFilterPack_Blend2Camera_ColorDodge_t20;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_ColorDodge_t20_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_ColorDodge_t20_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 126/* fieldStart */
	, 74/* methodStart */
	, -1/* eventStart */
	, 10/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_ColorDodge_t20_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_ColorDodge"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_ColorDodge_t20_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 42/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_ColorDodge_t20_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_ColorDodge_t20_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_ColorDodge_t20_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_ColorDodge_t20)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_ColorDodge_t20)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_ColorDodge_t20_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Darken
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Darken.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Darken
extern TypeInfo CameraFilterPack_Blend2Camera_Darken_t21_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Darken
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_DarkenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Darken_t21_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Darken_t21_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Darken_t21_1_0_0;
struct CameraFilterPack_Blend2Camera_Darken_t21;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Darken_t21_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Darken_t21_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 137/* fieldStart */
	, 82/* methodStart */
	, -1/* eventStart */
	, 11/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Darken_t21_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Darken"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Darken_t21_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 45/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Darken_t21_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Darken_t21_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Darken_t21_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Darken_t21)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Darken_t21)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Darken_t21_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_DarkerColor
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_DarkerColor.h"
// Metadata Definition CameraFilterPack_Blend2Camera_DarkerColor
extern TypeInfo CameraFilterPack_Blend2Camera_DarkerColor_t22_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_DarkerColor
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_DarkerColorMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_DarkerColor_t22_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_DarkerColor_t22_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_DarkerColor_t22_1_0_0;
struct CameraFilterPack_Blend2Camera_DarkerColor_t22;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_DarkerColor_t22_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_DarkerColor_t22_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 148/* fieldStart */
	, 90/* methodStart */
	, -1/* eventStart */
	, 12/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_DarkerColor_t22_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_DarkerColor"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_DarkerColor_t22_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 48/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_DarkerColor_t22_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_DarkerColor_t22_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_DarkerColor_t22_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_DarkerColor_t22)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_DarkerColor_t22)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_DarkerColor_t22_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Difference
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Difference.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Difference
extern TypeInfo CameraFilterPack_Blend2Camera_Difference_t23_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Difference
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_DifferenceMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Difference_t23_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Difference_t23_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Difference_t23_1_0_0;
struct CameraFilterPack_Blend2Camera_Difference_t23;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Difference_t23_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Difference_t23_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 159/* fieldStart */
	, 98/* methodStart */
	, -1/* eventStart */
	, 13/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Difference_t23_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Difference"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Difference_t23_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 51/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Difference_t23_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Difference_t23_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Difference_t23_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Difference_t23)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Difference_t23)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Difference_t23_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Divide
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Divide.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Divide
extern TypeInfo CameraFilterPack_Blend2Camera_Divide_t24_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Divide
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_DivideMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Divide_t24_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Divide_t24_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Divide_t24_1_0_0;
struct CameraFilterPack_Blend2Camera_Divide_t24;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Divide_t24_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Divide_t24_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 170/* fieldStart */
	, 106/* methodStart */
	, -1/* eventStart */
	, 14/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Divide_t24_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Divide"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Divide_t24_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 54/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Divide_t24_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Divide_t24_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Divide_t24_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Divide_t24)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Divide_t24)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Divide_t24_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Exclusion
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Exclusion.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Exclusion
extern TypeInfo CameraFilterPack_Blend2Camera_Exclusion_t25_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Exclusion
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_ExclusionMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Exclusion_t25_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Exclusion_t25_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Exclusion_t25_1_0_0;
struct CameraFilterPack_Blend2Camera_Exclusion_t25;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Exclusion_t25_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Exclusion_t25_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 181/* fieldStart */
	, 114/* methodStart */
	, -1/* eventStart */
	, 15/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Exclusion_t25_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Exclusion"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Exclusion_t25_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 57/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Exclusion_t25_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Exclusion_t25_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Exclusion_t25_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Exclusion_t25)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Exclusion_t25)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Exclusion_t25_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_GreenScreen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_GreenScreen.h"
// Metadata Definition CameraFilterPack_Blend2Camera_GreenScreen
extern TypeInfo CameraFilterPack_Blend2Camera_GreenScreen_t26_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_GreenScreen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_GreenScreenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_GreenScreen_t26_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_GreenScreen_t26_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_GreenScreen_t26_1_0_0;
struct CameraFilterPack_Blend2Camera_GreenScreen_t26;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_GreenScreen_t26_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_GreenScreen_t26_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 192/* fieldStart */
	, 122/* methodStart */
	, -1/* eventStart */
	, 16/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_GreenScreen_t26_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_GreenScreen"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_GreenScreen_t26_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 60/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_GreenScreen_t26_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_GreenScreen_t26_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_GreenScreen_t26_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_GreenScreen_t26)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_GreenScreen_t26)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_HardLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_HardLight.h"
// Metadata Definition CameraFilterPack_Blend2Camera_HardLight
extern TypeInfo CameraFilterPack_Blend2Camera_HardLight_t27_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_HardLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_HardLightMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_HardLight_t27_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_HardLight_t27_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_HardLight_t27_1_0_0;
struct CameraFilterPack_Blend2Camera_HardLight_t27;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_HardLight_t27_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_HardLight_t27_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 207/* fieldStart */
	, 129/* methodStart */
	, -1/* eventStart */
	, 17/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_HardLight_t27_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_HardLight"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_HardLight_t27_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 68/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_HardLight_t27_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_HardLight_t27_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_HardLight_t27_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_HardLight_t27)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_HardLight_t27)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_HardLight_t27_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_HardMix
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_HardMix.h"
// Metadata Definition CameraFilterPack_Blend2Camera_HardMix
extern TypeInfo CameraFilterPack_Blend2Camera_HardMix_t28_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_HardMix
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_HardMixMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_HardMix_t28_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_HardMix_t28_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_HardMix_t28_1_0_0;
struct CameraFilterPack_Blend2Camera_HardMix_t28;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_HardMix_t28_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_HardMix_t28_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 218/* fieldStart */
	, 137/* methodStart */
	, -1/* eventStart */
	, 18/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_HardMix_t28_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_HardMix"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_HardMix_t28_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 71/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_HardMix_t28_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_HardMix_t28_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_HardMix_t28_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_HardMix_t28)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_HardMix_t28)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_HardMix_t28_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Hue
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Hue.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Hue
extern TypeInfo CameraFilterPack_Blend2Camera_Hue_t29_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Hue
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_HueMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Hue_t29_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Hue_t29_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Hue_t29_1_0_0;
struct CameraFilterPack_Blend2Camera_Hue_t29;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Hue_t29_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Hue_t29_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 229/* fieldStart */
	, 145/* methodStart */
	, -1/* eventStart */
	, 19/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Hue_t29_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Hue"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Hue_t29_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 74/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Hue_t29_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Hue_t29_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Hue_t29_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Hue_t29)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Hue_t29)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Hue_t29_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Lighten
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Lighten.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Lighten
extern TypeInfo CameraFilterPack_Blend2Camera_Lighten_t30_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Lighten
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LightenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Lighten_t30_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Lighten_t30_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Lighten_t30_1_0_0;
struct CameraFilterPack_Blend2Camera_Lighten_t30;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Lighten_t30_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Lighten_t30_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 240/* fieldStart */
	, 153/* methodStart */
	, -1/* eventStart */
	, 20/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Lighten_t30_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Lighten"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Lighten_t30_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 77/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Lighten_t30_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Lighten_t30_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Lighten_t30_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Lighten_t30)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Lighten_t30)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Lighten_t30_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_LighterColor
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LighterColor.h"
// Metadata Definition CameraFilterPack_Blend2Camera_LighterColor
extern TypeInfo CameraFilterPack_Blend2Camera_LighterColor_t31_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_LighterColor
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LighterColorMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_LighterColor_t31_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_LighterColor_t31_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_LighterColor_t31_1_0_0;
struct CameraFilterPack_Blend2Camera_LighterColor_t31;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_LighterColor_t31_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_LighterColor_t31_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 251/* fieldStart */
	, 161/* methodStart */
	, -1/* eventStart */
	, 21/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_LighterColor_t31_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_LighterColor"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_LighterColor_t31_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 80/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_LighterColor_t31_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_LighterColor_t31_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_LighterColor_t31_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_LighterColor_t31)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_LighterColor_t31)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_LighterColor_t31_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_LinearBurn
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LinearBurn.h"
// Metadata Definition CameraFilterPack_Blend2Camera_LinearBurn
extern TypeInfo CameraFilterPack_Blend2Camera_LinearBurn_t32_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_LinearBurn
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LinearBurnMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_LinearBurn_t32_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_LinearBurn_t32_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_LinearBurn_t32_1_0_0;
struct CameraFilterPack_Blend2Camera_LinearBurn_t32;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_LinearBurn_t32_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_LinearBurn_t32_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 262/* fieldStart */
	, 169/* methodStart */
	, -1/* eventStart */
	, 22/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_LinearBurn_t32_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_LinearBurn"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_LinearBurn_t32_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 83/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_LinearBurn_t32_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_LinearBurn_t32_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_LinearBurn_t32_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_LinearBurn_t32)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_LinearBurn_t32)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_LinearBurn_t32_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_LinearDodge
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LinearDodge.h"
// Metadata Definition CameraFilterPack_Blend2Camera_LinearDodge
extern TypeInfo CameraFilterPack_Blend2Camera_LinearDodge_t33_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_LinearDodge
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LinearDodgeMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_LinearDodge_t33_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_LinearDodge_t33_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_LinearDodge_t33_1_0_0;
struct CameraFilterPack_Blend2Camera_LinearDodge_t33;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_LinearDodge_t33_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_LinearDodge_t33_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 273/* fieldStart */
	, 177/* methodStart */
	, -1/* eventStart */
	, 23/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_LinearDodge_t33_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_LinearDodge"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_LinearDodge_t33_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 86/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_LinearDodge_t33_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_LinearDodge_t33_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_LinearDodge_t33_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_LinearDodge_t33)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_LinearDodge_t33)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_LinearDodge_t33_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_LinearLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LinearLight.h"
// Metadata Definition CameraFilterPack_Blend2Camera_LinearLight
extern TypeInfo CameraFilterPack_Blend2Camera_LinearLight_t34_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_LinearLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LinearLightMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_LinearLight_t34_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_LinearLight_t34_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_LinearLight_t34_1_0_0;
struct CameraFilterPack_Blend2Camera_LinearLight_t34;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_LinearLight_t34_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_LinearLight_t34_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 284/* fieldStart */
	, 185/* methodStart */
	, -1/* eventStart */
	, 24/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_LinearLight_t34_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_LinearLight"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_LinearLight_t34_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 89/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_LinearLight_t34_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_LinearLight_t34_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_LinearLight_t34_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_LinearLight_t34)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_LinearLight_t34)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_LinearLight_t34_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Luminosity
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Luminosity.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Luminosity
extern TypeInfo CameraFilterPack_Blend2Camera_Luminosity_t35_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Luminosity
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_LuminosityMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Luminosity_t35_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Luminosity_t35_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Luminosity_t35_1_0_0;
struct CameraFilterPack_Blend2Camera_Luminosity_t35;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Luminosity_t35_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Luminosity_t35_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 295/* fieldStart */
	, 193/* methodStart */
	, -1/* eventStart */
	, 25/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Luminosity_t35_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Luminosity"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Luminosity_t35_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 92/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Luminosity_t35_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Luminosity_t35_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Luminosity_t35_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Luminosity_t35)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Luminosity_t35)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Luminosity_t35_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Multiply
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Multiply.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Multiply
extern TypeInfo CameraFilterPack_Blend2Camera_Multiply_t36_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Multiply
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_MultiplyMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Multiply_t36_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Multiply_t36_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Multiply_t36_1_0_0;
struct CameraFilterPack_Blend2Camera_Multiply_t36;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Multiply_t36_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Multiply_t36_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 306/* fieldStart */
	, 201/* methodStart */
	, -1/* eventStart */
	, 26/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Multiply_t36_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Multiply"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Multiply_t36_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 95/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Multiply_t36_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Multiply_t36_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Multiply_t36_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Multiply_t36)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Multiply_t36)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Multiply_t36_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Overlay
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Overlay.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Overlay
extern TypeInfo CameraFilterPack_Blend2Camera_Overlay_t37_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Overlay
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_OverlayMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Overlay_t37_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Overlay_t37_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Overlay_t37_1_0_0;
struct CameraFilterPack_Blend2Camera_Overlay_t37;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Overlay_t37_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Overlay_t37_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 317/* fieldStart */
	, 209/* methodStart */
	, -1/* eventStart */
	, 27/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Overlay_t37_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Overlay"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Overlay_t37_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 98/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Overlay_t37_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Overlay_t37_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Overlay_t37_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Overlay_t37)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Overlay_t37)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Overlay_t37_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_PhotoshopFilters
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_PhotoshopFil_0.h"
// Metadata Definition CameraFilterPack_Blend2Camera_PhotoshopFilters
extern TypeInfo CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_PhotoshopFilters
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_PhotoshopFil_0MethodDeclarations.h"
extern const Il2CppType filters_t38_0_0_0;
static const Il2CppType* CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_il2cpp_TypeInfo__nestedTypes[1] =
{
	&filters_t38_0_0_0,
};
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_1_0_0;
struct CameraFilterPack_Blend2Camera_PhotoshopFilters_t39;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 328/* fieldStart */
	, 217/* methodStart */
	, -1/* eventStart */
	, 28/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_PhotoshopFilters"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 101/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_PhotoshopFilters/filters
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_PhotoshopFil.h"
// Metadata Definition CameraFilterPack_Blend2Camera_PhotoshopFilters/filters
extern TypeInfo filters_t38_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_PhotoshopFilters/filters
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_PhotoshopFilMethodDeclarations.h"
static const EncodedMethodIndex filters_t38_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair filters_t38_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType filters_t38_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata filters_t38_DefinitionMetadata = 
{
	&CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, filters_t38_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, filters_t38_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 341/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo filters_t38_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "filters"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &filters_t38_0_0_0/* byval_arg */
	, &filters_t38_1_0_0/* this_arg */
	, &filters_t38_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (filters_t38)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (filters_t38)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_PinLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_PinLight.h"
// Metadata Definition CameraFilterPack_Blend2Camera_PinLight
extern TypeInfo CameraFilterPack_Blend2Camera_PinLight_t40_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_PinLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_PinLightMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_PinLight_t40_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_PinLight_t40_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_PinLight_t40_1_0_0;
struct CameraFilterPack_Blend2Camera_PinLight_t40;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_PinLight_t40_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_PinLight_t40_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 367/* fieldStart */
	, 226/* methodStart */
	, -1/* eventStart */
	, 29/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_PinLight_t40_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_PinLight"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_PinLight_t40_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 104/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_PinLight_t40_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_PinLight_t40_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_PinLight_t40_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_PinLight_t40)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_PinLight_t40)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_PinLight_t40_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Saturation
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Saturation.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Saturation
extern TypeInfo CameraFilterPack_Blend2Camera_Saturation_t41_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Saturation
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_SaturationMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Saturation_t41_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Saturation_t41_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Saturation_t41_1_0_0;
struct CameraFilterPack_Blend2Camera_Saturation_t41;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Saturation_t41_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Saturation_t41_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 378/* fieldStart */
	, 234/* methodStart */
	, -1/* eventStart */
	, 30/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Saturation_t41_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Saturation"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Saturation_t41_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 107/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Saturation_t41_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Saturation_t41_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Saturation_t41_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Saturation_t41)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Saturation_t41)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Saturation_t41_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Screen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Screen.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Screen
extern TypeInfo CameraFilterPack_Blend2Camera_Screen_t42_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Screen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_ScreenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Screen_t42_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Screen_t42_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Screen_t42_1_0_0;
struct CameraFilterPack_Blend2Camera_Screen_t42;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Screen_t42_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Screen_t42_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 389/* fieldStart */
	, 242/* methodStart */
	, -1/* eventStart */
	, 31/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Screen_t42_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Screen"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Screen_t42_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 110/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Screen_t42_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Screen_t42_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Screen_t42_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Screen_t42)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Screen_t42)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Screen_t42_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_SoftLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_SoftLight.h"
// Metadata Definition CameraFilterPack_Blend2Camera_SoftLight
extern TypeInfo CameraFilterPack_Blend2Camera_SoftLight_t43_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_SoftLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_SoftLightMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_SoftLight_t43_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_SoftLight_t43_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_SoftLight_t43_1_0_0;
struct CameraFilterPack_Blend2Camera_SoftLight_t43;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_SoftLight_t43_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_SoftLight_t43_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 400/* fieldStart */
	, 250/* methodStart */
	, -1/* eventStart */
	, 32/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_SoftLight_t43_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_SoftLight"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_SoftLight_t43_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 113/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_SoftLight_t43_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_SoftLight_t43_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_SoftLight_t43_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_SoftLight_t43)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_SoftLight_t43)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_SoftLight_t43_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_SplitScreen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_SplitScreen.h"
// Metadata Definition CameraFilterPack_Blend2Camera_SplitScreen
extern TypeInfo CameraFilterPack_Blend2Camera_SplitScreen_t44_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_SplitScreen
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_SplitScreenMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_SplitScreen_t44_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_SplitScreen_t44_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_SplitScreen_t44_1_0_0;
struct CameraFilterPack_Blend2Camera_SplitScreen_t44;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_SplitScreen_t44_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_SplitScreen_t44_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 411/* fieldStart */
	, 258/* methodStart */
	, -1/* eventStart */
	, 33/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_SplitScreen_t44_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_SplitScreen"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_SplitScreen_t44_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 116/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_SplitScreen_t44_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_SplitScreen_t44_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_SplitScreen_t44_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_SplitScreen_t44)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_SplitScreen_t44)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_SplitScreen_t44_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_Subtract
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_Subtract.h"
// Metadata Definition CameraFilterPack_Blend2Camera_Subtract
extern TypeInfo CameraFilterPack_Blend2Camera_Subtract_t45_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_Subtract
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_SubtractMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_Subtract_t45_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_Subtract_t45_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_Subtract_t45_1_0_0;
struct CameraFilterPack_Blend2Camera_Subtract_t45;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_Subtract_t45_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_Subtract_t45_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 432/* fieldStart */
	, 266/* methodStart */
	, -1/* eventStart */
	, 34/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_Subtract_t45_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_Subtract"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_Subtract_t45_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 123/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_Subtract_t45_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_Subtract_t45_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_Subtract_t45_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_Subtract_t45)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_Subtract_t45)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_Subtract_t45_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blend2Camera_VividLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_VividLight.h"
// Metadata Definition CameraFilterPack_Blend2Camera_VividLight
extern TypeInfo CameraFilterPack_Blend2Camera_VividLight_t46_il2cpp_TypeInfo;
// CameraFilterPack_Blend2Camera_VividLight
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_VividLightMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blend2Camera_VividLight_t46_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blend2Camera_VividLight_t46_0_0_0;
extern const Il2CppType CameraFilterPack_Blend2Camera_VividLight_t46_1_0_0;
struct CameraFilterPack_Blend2Camera_VividLight_t46;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blend2Camera_VividLight_t46_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blend2Camera_VividLight_t46_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 443/* fieldStart */
	, 274/* methodStart */
	, -1/* eventStart */
	, 35/* propertyStart */

};
TypeInfo CameraFilterPack_Blend2Camera_VividLight_t46_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blend2Camera_VividLight"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blend2Camera_VividLight_t46_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 126/* custom_attributes_cache */
	, &CameraFilterPack_Blend2Camera_VividLight_t46_0_0_0/* byval_arg */
	, &CameraFilterPack_Blend2Camera_VividLight_t46_1_0_0/* this_arg */
	, &CameraFilterPack_Blend2Camera_VividLight_t46_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blend2Camera_VividLight_t46)/* instance_size */
	, sizeof (CameraFilterPack_Blend2Camera_VividLight_t46)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blend2Camera_VividLight_t46_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Bloom
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Bloom.h"
// Metadata Definition CameraFilterPack_Blur_Bloom
extern TypeInfo CameraFilterPack_Blur_Bloom_t47_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Bloom
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_BloomMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Bloom_t47_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Bloom_t47_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Bloom_t47_1_0_0;
struct CameraFilterPack_Blur_Bloom_t47;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Bloom_t47_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Bloom_t47_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 454/* fieldStart */
	, 282/* methodStart */
	, -1/* eventStart */
	, 36/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Bloom_t47_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Bloom"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Bloom_t47_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 129/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Bloom_t47_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Bloom_t47_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Bloom_t47_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Bloom_t47)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Bloom_t47)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Bloom_t47_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_BlurHole
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_BlurHole.h"
// Metadata Definition CameraFilterPack_Blur_BlurHole
extern TypeInfo CameraFilterPack_Blur_BlurHole_t48_il2cpp_TypeInfo;
// CameraFilterPack_Blur_BlurHole
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_BlurHoleMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_BlurHole_t48_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_BlurHole_t48_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_BlurHole_t48_1_0_0;
struct CameraFilterPack_Blur_BlurHole_t48;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_BlurHole_t48_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_BlurHole_t48_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 462/* fieldStart */
	, 289/* methodStart */
	, -1/* eventStart */
	, 37/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_BlurHole_t48_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_BlurHole"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_BlurHole_t48_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 132/* custom_attributes_cache */
	, &CameraFilterPack_Blur_BlurHole_t48_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_BlurHole_t48_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_BlurHole_t48_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_BlurHole_t48)/* instance_size */
	, sizeof (CameraFilterPack_Blur_BlurHole_t48)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_BlurHole_t48_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Blurry
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Blurry.h"
// Metadata Definition CameraFilterPack_Blur_Blurry
extern TypeInfo CameraFilterPack_Blur_Blurry_t49_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Blurry
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_BlurryMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Blurry_t49_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Blurry_t49_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Blurry_t49_1_0_0;
struct CameraFilterPack_Blur_Blurry_t49;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Blurry_t49_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Blurry_t49_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 474/* fieldStart */
	, 296/* methodStart */
	, -1/* eventStart */
	, 38/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Blurry_t49_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Blurry"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Blurry_t49_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 140/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Blurry_t49_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Blurry_t49_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Blurry_t49_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Blurry_t49)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Blurry_t49)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Blurry_t49_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_DitherOffset
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_DitherOffset.h"
// Metadata Definition CameraFilterPack_Blur_DitherOffset
extern TypeInfo CameraFilterPack_Blur_DitherOffset_t50_il2cpp_TypeInfo;
// CameraFilterPack_Blur_DitherOffset
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_DitherOffsetMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_DitherOffset_t50_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_DitherOffset_t50_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_DitherOffset_t50_1_0_0;
struct CameraFilterPack_Blur_DitherOffset_t50;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_DitherOffset_t50_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_DitherOffset_t50_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 482/* fieldStart */
	, 303/* methodStart */
	, -1/* eventStart */
	, 39/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_DitherOffset_t50_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_DitherOffset"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_DitherOffset_t50_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 143/* custom_attributes_cache */
	, &CameraFilterPack_Blur_DitherOffset_t50_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_DitherOffset_t50_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_DitherOffset_t50_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_DitherOffset_t50)/* instance_size */
	, sizeof (CameraFilterPack_Blur_DitherOffset_t50)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_DitherOffset_t50_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Dithering2x2
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Dithering2x2.h"
// Metadata Definition CameraFilterPack_Blur_Dithering2x2
extern TypeInfo CameraFilterPack_Blur_Dithering2x2_t51_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Dithering2x2
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Dithering2x2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Dithering2x2_t51_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Dithering2x2_t51_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Dithering2x2_t51_1_0_0;
struct CameraFilterPack_Blur_Dithering2x2_t51;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Dithering2x2_t51_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Dithering2x2_t51_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 490/* fieldStart */
	, 310/* methodStart */
	, -1/* eventStart */
	, 40/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Dithering2x2_t51_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Dithering2x2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Dithering2x2_t51_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 145/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Dithering2x2_t51_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Dithering2x2_t51_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Dithering2x2_t51_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Dithering2x2_t51)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Dithering2x2_t51)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Dithering2x2_t51_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Focus
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Focus.h"
// Metadata Definition CameraFilterPack_Blur_Focus
extern TypeInfo CameraFilterPack_Blur_Focus_t52_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Focus
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_FocusMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Focus_t52_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Focus_t52_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Focus_t52_1_0_0;
struct CameraFilterPack_Blur_Focus_t52;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Focus_t52_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Focus_t52_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 498/* fieldStart */
	, 317/* methodStart */
	, -1/* eventStart */
	, 41/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Focus_t52_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Focus"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Focus_t52_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 147/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Focus_t52_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Focus_t52_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Focus_t52_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Focus_t52)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Focus_t52)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Focus_t52_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_GaussianBlur
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_GaussianBlur.h"
// Metadata Definition CameraFilterPack_Blur_GaussianBlur
extern TypeInfo CameraFilterPack_Blur_GaussianBlur_t53_il2cpp_TypeInfo;
// CameraFilterPack_Blur_GaussianBlur
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_GaussianBlurMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_GaussianBlur_t53_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_GaussianBlur_t53_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_GaussianBlur_t53_1_0_0;
struct CameraFilterPack_Blur_GaussianBlur_t53;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_GaussianBlur_t53_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_GaussianBlur_t53_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 510/* fieldStart */
	, 324/* methodStart */
	, -1/* eventStart */
	, 42/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_GaussianBlur_t53_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_GaussianBlur"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_GaussianBlur_t53_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 152/* custom_attributes_cache */
	, &CameraFilterPack_Blur_GaussianBlur_t53_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_GaussianBlur_t53_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_GaussianBlur_t53_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_GaussianBlur_t53)/* instance_size */
	, sizeof (CameraFilterPack_Blur_GaussianBlur_t53)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_GaussianBlur_t53_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Movie
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Movie.h"
// Metadata Definition CameraFilterPack_Blur_Movie
extern TypeInfo CameraFilterPack_Blur_Movie_t54_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Movie
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_MovieMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Movie_t54_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Movie_t54_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Movie_t54_1_0_0;
struct CameraFilterPack_Blur_Movie_t54;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Movie_t54_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Movie_t54_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 516/* fieldStart */
	, 331/* methodStart */
	, -1/* eventStart */
	, 43/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Movie_t54_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Movie"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Movie_t54_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 154/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Movie_t54_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Movie_t54_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Movie_t54_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Movie_t54)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Movie_t54)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Movie_t54_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Noise.h"
// Metadata Definition CameraFilterPack_Blur_Noise
extern TypeInfo CameraFilterPack_Blur_Noise_t55_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_NoiseMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Noise_t55_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Noise_t55_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Noise_t55_1_0_0;
struct CameraFilterPack_Blur_Noise_t55;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Noise_t55_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Noise_t55_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 527/* fieldStart */
	, 338/* methodStart */
	, -1/* eventStart */
	, 44/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Noise_t55_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Noise"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Noise_t55_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 158/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Noise_t55_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Noise_t55_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Noise_t55_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Noise_t55)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Noise_t55)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Noise_t55_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Radial
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Radial.h"
// Metadata Definition CameraFilterPack_Blur_Radial
extern TypeInfo CameraFilterPack_Blur_Radial_t56_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Radial
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_RadialMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Radial_t56_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Radial_t56_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Radial_t56_1_0_0;
struct CameraFilterPack_Blur_Radial_t56;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Radial_t56_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Radial_t56_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 535/* fieldStart */
	, 345/* methodStart */
	, -1/* eventStart */
	, 45/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Radial_t56_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Radial"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Radial_t56_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 160/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Radial_t56_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Radial_t56_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Radial_t56_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Radial_t56)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Radial_t56)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Radial_t56_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Radial_Fast
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Radial_Fast.h"
// Metadata Definition CameraFilterPack_Blur_Radial_Fast
extern TypeInfo CameraFilterPack_Blur_Radial_Fast_t57_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Radial_Fast
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Radial_FastMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Radial_Fast_t57_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Radial_Fast_t57_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Radial_Fast_t57_1_0_0;
struct CameraFilterPack_Blur_Radial_Fast_t57;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Radial_Fast_t57_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Radial_Fast_t57_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 547/* fieldStart */
	, 352/* methodStart */
	, -1/* eventStart */
	, 46/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Radial_Fast_t57_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Radial_Fast"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Radial_Fast_t57_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 165/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Radial_Fast_t57_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Radial_Fast_t57_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Radial_Fast_t57_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Radial_Fast_t57)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Radial_Fast_t57)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Radial_Fast_t57_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Regular
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Regular.h"
// Metadata Definition CameraFilterPack_Blur_Regular
extern TypeInfo CameraFilterPack_Blur_Regular_t58_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Regular
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_RegularMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Regular_t58_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Regular_t58_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Regular_t58_1_0_0;
struct CameraFilterPack_Blur_Regular_t58;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Regular_t58_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Regular_t58_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 559/* fieldStart */
	, 359/* methodStart */
	, -1/* eventStart */
	, 47/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Regular_t58_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Regular"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Regular_t58_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 170/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Regular_t58_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Regular_t58_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Regular_t58_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Regular_t58)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Regular_t58)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Regular_t58_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Blur_Steam
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_Steam.h"
// Metadata Definition CameraFilterPack_Blur_Steam
extern TypeInfo CameraFilterPack_Blur_Steam_t59_il2cpp_TypeInfo;
// CameraFilterPack_Blur_Steam
#include "AssemblyU2DCSharp_CameraFilterPack_Blur_SteamMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Blur_Steam_t59_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Blur_Steam_t59_0_0_0;
extern const Il2CppType CameraFilterPack_Blur_Steam_t59_1_0_0;
struct CameraFilterPack_Blur_Steam_t59;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Blur_Steam_t59_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Blur_Steam_t59_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 567/* fieldStart */
	, 366/* methodStart */
	, -1/* eventStart */
	, 48/* propertyStart */

};
TypeInfo CameraFilterPack_Blur_Steam_t59_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Blur_Steam"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Blur_Steam_t59_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 172/* custom_attributes_cache */
	, &CameraFilterPack_Blur_Steam_t59_0_0_0/* byval_arg */
	, &CameraFilterPack_Blur_Steam_t59_1_0_0/* this_arg */
	, &CameraFilterPack_Blur_Steam_t59_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Blur_Steam_t59)/* instance_size */
	, sizeof (CameraFilterPack_Blur_Steam_t59)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Blur_Steam_t59_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_BrightContrastSaturation
#include "AssemblyU2DCSharp_CameraFilterPack_Color_BrightContrastSatur.h"
// Metadata Definition CameraFilterPack_Color_BrightContrastSaturation
extern TypeInfo CameraFilterPack_Color_BrightContrastSaturation_t60_il2cpp_TypeInfo;
// CameraFilterPack_Color_BrightContrastSaturation
#include "AssemblyU2DCSharp_CameraFilterPack_Color_BrightContrastSaturMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_BrightContrastSaturation_t60_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_BrightContrastSaturation_t60_0_0_0;
extern const Il2CppType CameraFilterPack_Color_BrightContrastSaturation_t60_1_0_0;
struct CameraFilterPack_Color_BrightContrastSaturation_t60;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_BrightContrastSaturation_t60_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_BrightContrastSaturation_t60_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 575/* fieldStart */
	, 373/* methodStart */
	, -1/* eventStart */
	, 49/* propertyStart */

};
TypeInfo CameraFilterPack_Color_BrightContrastSaturation_t60_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_BrightContrastSaturation"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_BrightContrastSaturation_t60_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 175/* custom_attributes_cache */
	, &CameraFilterPack_Color_BrightContrastSaturation_t60_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_BrightContrastSaturation_t60_1_0_0/* this_arg */
	, &CameraFilterPack_Color_BrightContrastSaturation_t60_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_BrightContrastSaturation_t60)/* instance_size */
	, sizeof (CameraFilterPack_Color_BrightContrastSaturation_t60)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Color_BrightContrastSaturation_t60_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_Chromatic_Aberration
#include "AssemblyU2DCSharp_CameraFilterPack_Color_Chromatic_Aberratio.h"
// Metadata Definition CameraFilterPack_Color_Chromatic_Aberration
extern TypeInfo CameraFilterPack_Color_Chromatic_Aberration_t61_il2cpp_TypeInfo;
// CameraFilterPack_Color_Chromatic_Aberration
#include "AssemblyU2DCSharp_CameraFilterPack_Color_Chromatic_AberratioMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_Chromatic_Aberration_t61_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_Chromatic_Aberration_t61_0_0_0;
extern const Il2CppType CameraFilterPack_Color_Chromatic_Aberration_t61_1_0_0;
struct CameraFilterPack_Color_Chromatic_Aberration_t61;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_Chromatic_Aberration_t61_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_Chromatic_Aberration_t61_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 585/* fieldStart */
	, 380/* methodStart */
	, -1/* eventStart */
	, 50/* propertyStart */

};
TypeInfo CameraFilterPack_Color_Chromatic_Aberration_t61_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_Chromatic_Aberration"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_Chromatic_Aberration_t61_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 179/* custom_attributes_cache */
	, &CameraFilterPack_Color_Chromatic_Aberration_t61_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_Chromatic_Aberration_t61_1_0_0/* this_arg */
	, &CameraFilterPack_Color_Chromatic_Aberration_t61_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_Chromatic_Aberration_t61)/* instance_size */
	, sizeof (CameraFilterPack_Color_Chromatic_Aberration_t61)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Color_Chromatic_Aberration_t61_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_Contrast
#include "AssemblyU2DCSharp_CameraFilterPack_Color_Contrast.h"
// Metadata Definition CameraFilterPack_Color_Contrast
extern TypeInfo CameraFilterPack_Color_Contrast_t62_il2cpp_TypeInfo;
// CameraFilterPack_Color_Contrast
#include "AssemblyU2DCSharp_CameraFilterPack_Color_ContrastMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_Contrast_t62_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_Contrast_t62_0_0_0;
extern const Il2CppType CameraFilterPack_Color_Contrast_t62_1_0_0;
struct CameraFilterPack_Color_Contrast_t62;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_Contrast_t62_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_Contrast_t62_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 591/* fieldStart */
	, 387/* methodStart */
	, -1/* eventStart */
	, 51/* propertyStart */

};
TypeInfo CameraFilterPack_Color_Contrast_t62_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_Contrast"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_Contrast_t62_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 181/* custom_attributes_cache */
	, &CameraFilterPack_Color_Contrast_t62_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_Contrast_t62_1_0_0/* this_arg */
	, &CameraFilterPack_Color_Contrast_t62_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_Contrast_t62)/* instance_size */
	, sizeof (CameraFilterPack_Color_Contrast_t62)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Color_Contrast_t62_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_GrayScale
#include "AssemblyU2DCSharp_CameraFilterPack_Color_GrayScale.h"
// Metadata Definition CameraFilterPack_Color_GrayScale
extern TypeInfo CameraFilterPack_Color_GrayScale_t63_il2cpp_TypeInfo;
// CameraFilterPack_Color_GrayScale
#include "AssemblyU2DCSharp_CameraFilterPack_Color_GrayScaleMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_GrayScale_t63_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_GrayScale_t63_0_0_0;
extern const Il2CppType CameraFilterPack_Color_GrayScale_t63_1_0_0;
struct CameraFilterPack_Color_GrayScale_t63;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_GrayScale_t63_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_GrayScale_t63_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 597/* fieldStart */
	, 394/* methodStart */
	, -1/* eventStart */
	, 52/* propertyStart */

};
TypeInfo CameraFilterPack_Color_GrayScale_t63_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_GrayScale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_GrayScale_t63_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 183/* custom_attributes_cache */
	, &CameraFilterPack_Color_GrayScale_t63_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_GrayScale_t63_1_0_0/* this_arg */
	, &CameraFilterPack_Color_GrayScale_t63_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_GrayScale_t63)/* instance_size */
	, sizeof (CameraFilterPack_Color_GrayScale_t63)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_Invert
#include "AssemblyU2DCSharp_CameraFilterPack_Color_Invert.h"
// Metadata Definition CameraFilterPack_Color_Invert
extern TypeInfo CameraFilterPack_Color_Invert_t64_il2cpp_TypeInfo;
// CameraFilterPack_Color_Invert
#include "AssemblyU2DCSharp_CameraFilterPack_Color_InvertMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_Invert_t64_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_Invert_t64_0_0_0;
extern const Il2CppType CameraFilterPack_Color_Invert_t64_1_0_0;
struct CameraFilterPack_Color_Invert_t64;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_Invert_t64_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_Invert_t64_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 601/* fieldStart */
	, 400/* methodStart */
	, -1/* eventStart */
	, 53/* propertyStart */

};
TypeInfo CameraFilterPack_Color_Invert_t64_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_Invert"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_Invert_t64_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 184/* custom_attributes_cache */
	, &CameraFilterPack_Color_Invert_t64_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_Invert_t64_1_0_0/* this_arg */
	, &CameraFilterPack_Color_Invert_t64_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_Invert_t64)/* instance_size */
	, sizeof (CameraFilterPack_Color_Invert_t64)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_Color_Noise.h"
// Metadata Definition CameraFilterPack_Color_Noise
extern TypeInfo CameraFilterPack_Color_Noise_t65_il2cpp_TypeInfo;
// CameraFilterPack_Color_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_Color_NoiseMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_Noise_t65_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_Noise_t65_0_0_0;
extern const Il2CppType CameraFilterPack_Color_Noise_t65_1_0_0;
struct CameraFilterPack_Color_Noise_t65;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_Noise_t65_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_Noise_t65_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 605/* fieldStart */
	, 406/* methodStart */
	, -1/* eventStart */
	, 54/* propertyStart */

};
TypeInfo CameraFilterPack_Color_Noise_t65_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_Noise"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_Noise_t65_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 185/* custom_attributes_cache */
	, &CameraFilterPack_Color_Noise_t65_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_Noise_t65_1_0_0/* this_arg */
	, &CameraFilterPack_Color_Noise_t65_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_Noise_t65)/* instance_size */
	, sizeof (CameraFilterPack_Color_Noise_t65)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Color_Noise_t65_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_RGB
#include "AssemblyU2DCSharp_CameraFilterPack_Color_RGB.h"
// Metadata Definition CameraFilterPack_Color_RGB
extern TypeInfo CameraFilterPack_Color_RGB_t66_il2cpp_TypeInfo;
// CameraFilterPack_Color_RGB
#include "AssemblyU2DCSharp_CameraFilterPack_Color_RGBMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_RGB_t66_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_RGB_t66_0_0_0;
extern const Il2CppType CameraFilterPack_Color_RGB_t66_1_0_0;
struct CameraFilterPack_Color_RGB_t66;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_RGB_t66_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_RGB_t66_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 611/* fieldStart */
	, 413/* methodStart */
	, -1/* eventStart */
	, 55/* propertyStart */

};
TypeInfo CameraFilterPack_Color_RGB_t66_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_RGB"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_RGB_t66_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 187/* custom_attributes_cache */
	, &CameraFilterPack_Color_RGB_t66_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_RGB_t66_1_0_0/* this_arg */
	, &CameraFilterPack_Color_RGB_t66_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_RGB_t66)/* instance_size */
	, sizeof (CameraFilterPack_Color_RGB_t66)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Color_RGB_t66_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_Sepia
#include "AssemblyU2DCSharp_CameraFilterPack_Color_Sepia.h"
// Metadata Definition CameraFilterPack_Color_Sepia
extern TypeInfo CameraFilterPack_Color_Sepia_t67_il2cpp_TypeInfo;
// CameraFilterPack_Color_Sepia
#include "AssemblyU2DCSharp_CameraFilterPack_Color_SepiaMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_Sepia_t67_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_Sepia_t67_0_0_0;
extern const Il2CppType CameraFilterPack_Color_Sepia_t67_1_0_0;
struct CameraFilterPack_Color_Sepia_t67;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_Sepia_t67_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_Sepia_t67_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 617/* fieldStart */
	, 420/* methodStart */
	, -1/* eventStart */
	, 56/* propertyStart */

};
TypeInfo CameraFilterPack_Color_Sepia_t67_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_Sepia"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_Sepia_t67_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 188/* custom_attributes_cache */
	, &CameraFilterPack_Color_Sepia_t67_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_Sepia_t67_1_0_0/* this_arg */
	, &CameraFilterPack_Color_Sepia_t67_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_Sepia_t67)/* instance_size */
	, sizeof (CameraFilterPack_Color_Sepia_t67)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Color_Switching
#include "AssemblyU2DCSharp_CameraFilterPack_Color_Switching.h"
// Metadata Definition CameraFilterPack_Color_Switching
extern TypeInfo CameraFilterPack_Color_Switching_t68_il2cpp_TypeInfo;
// CameraFilterPack_Color_Switching
#include "AssemblyU2DCSharp_CameraFilterPack_Color_SwitchingMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Color_Switching_t68_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Color_Switching_t68_0_0_0;
extern const Il2CppType CameraFilterPack_Color_Switching_t68_1_0_0;
struct CameraFilterPack_Color_Switching_t68;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Color_Switching_t68_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Color_Switching_t68_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 621/* fieldStart */
	, 426/* methodStart */
	, -1/* eventStart */
	, 57/* propertyStart */

};
TypeInfo CameraFilterPack_Color_Switching_t68_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Color_Switching"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Color_Switching_t68_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 189/* custom_attributes_cache */
	, &CameraFilterPack_Color_Switching_t68_0_0_0/* byval_arg */
	, &CameraFilterPack_Color_Switching_t68_1_0_0/* this_arg */
	, &CameraFilterPack_Color_Switching_t68_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Color_Switching_t68)/* instance_size */
	, sizeof (CameraFilterPack_Color_Switching_t68)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Color_Switching_t68_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_Adjust_ColorRGB
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_ColorRGB.h"
// Metadata Definition CameraFilterPack_Colors_Adjust_ColorRGB
extern TypeInfo CameraFilterPack_Colors_Adjust_ColorRGB_t69_il2cpp_TypeInfo;
// CameraFilterPack_Colors_Adjust_ColorRGB
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_ColorRGBMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_Adjust_ColorRGB_t69_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_Adjust_ColorRGB_t69_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_Adjust_ColorRGB_t69_1_0_0;
struct CameraFilterPack_Colors_Adjust_ColorRGB_t69;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_Adjust_ColorRGB_t69_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_Adjust_ColorRGB_t69_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 627/* fieldStart */
	, 433/* methodStart */
	, -1/* eventStart */
	, 58/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_Adjust_ColorRGB_t69_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_Adjust_ColorRGB"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_Adjust_ColorRGB_t69_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 191/* custom_attributes_cache */
	, &CameraFilterPack_Colors_Adjust_ColorRGB_t69_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_Adjust_ColorRGB_t69_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_Adjust_ColorRGB_t69_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_Adjust_ColorRGB_t69)/* instance_size */
	, sizeof (CameraFilterPack_Colors_Adjust_ColorRGB_t69)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Colors_Adjust_ColorRGB_t69_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_Adjust_FullColors
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_FullColors.h"
// Metadata Definition CameraFilterPack_Colors_Adjust_FullColors
extern TypeInfo CameraFilterPack_Colors_Adjust_FullColors_t70_il2cpp_TypeInfo;
// CameraFilterPack_Colors_Adjust_FullColors
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_FullColorsMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_Adjust_FullColors_t70_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_Adjust_FullColors_t70_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_Adjust_FullColors_t70_1_0_0;
struct CameraFilterPack_Colors_Adjust_FullColors_t70;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_Adjust_FullColors_t70_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_Adjust_FullColors_t70_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 639/* fieldStart */
	, 440/* methodStart */
	, -1/* eventStart */
	, 59/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_Adjust_FullColors_t70_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_Adjust_FullColors"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_Adjust_FullColors_t70_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 196/* custom_attributes_cache */
	, &CameraFilterPack_Colors_Adjust_FullColors_t70_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_Adjust_FullColors_t70_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_Adjust_FullColors_t70_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_Adjust_FullColors_t70)/* instance_size */
	, sizeof (CameraFilterPack_Colors_Adjust_FullColors_t70)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_Adjust_PreFilters
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_PreFilters.h"
// Metadata Definition CameraFilterPack_Colors_Adjust_PreFilters
extern TypeInfo CameraFilterPack_Colors_Adjust_PreFilters_t73_il2cpp_TypeInfo;
// CameraFilterPack_Colors_Adjust_PreFilters
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_PreFiltersMethodDeclarations.h"
extern const Il2CppType filters_t71_0_0_0;
static const Il2CppType* CameraFilterPack_Colors_Adjust_PreFilters_t73_il2cpp_TypeInfo__nestedTypes[1] =
{
	&filters_t71_0_0_0,
};
static const EncodedMethodIndex CameraFilterPack_Colors_Adjust_PreFilters_t73_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_Adjust_PreFilters_t73_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_Adjust_PreFilters_t73_1_0_0;
struct CameraFilterPack_Colors_Adjust_PreFilters_t73;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_Adjust_PreFilters_t73_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CameraFilterPack_Colors_Adjust_PreFilters_t73_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_Adjust_PreFilters_t73_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 655/* fieldStart */
	, 446/* methodStart */
	, -1/* eventStart */
	, 60/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_Adjust_PreFilters_t73_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_Adjust_PreFilters"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_Adjust_PreFilters_t73_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 209/* custom_attributes_cache */
	, &CameraFilterPack_Colors_Adjust_PreFilters_t73_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_Adjust_PreFilters_t73_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_Adjust_PreFilters_t73_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_Adjust_PreFilters_t73)/* instance_size */
	, sizeof (CameraFilterPack_Colors_Adjust_PreFilters_t73)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_Adjust_PreFilters/filters
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_PreFilters_.h"
// Metadata Definition CameraFilterPack_Colors_Adjust_PreFilters/filters
extern TypeInfo filters_t71_il2cpp_TypeInfo;
// CameraFilterPack_Colors_Adjust_PreFilters/filters
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_PreFilters_MethodDeclarations.h"
static const EncodedMethodIndex filters_t71_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair filters_t71_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType filters_t71_1_0_0;
const Il2CppTypeDefinitionMetadata filters_t71_DefinitionMetadata = 
{
	&CameraFilterPack_Colors_Adjust_PreFilters_t73_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, filters_t71_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, filters_t71_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 665/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo filters_t71_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "filters"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &filters_t71_0_0_0/* byval_arg */
	, &filters_t71_1_0_0/* this_arg */
	, &filters_t71_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (filters_t71)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (filters_t71)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 33/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// CameraFilterPack_Colors_Brightness
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Brightness.h"
// Metadata Definition CameraFilterPack_Colors_Brightness
extern TypeInfo CameraFilterPack_Colors_Brightness_t74_il2cpp_TypeInfo;
// CameraFilterPack_Colors_Brightness
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_BrightnessMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_Brightness_t74_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_Brightness_t74_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_Brightness_t74_1_0_0;
struct CameraFilterPack_Colors_Brightness_t74;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_Brightness_t74_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_Brightness_t74_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 698/* fieldStart */
	, 454/* methodStart */
	, -1/* eventStart */
	, 61/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_Brightness_t74_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_Brightness"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_Brightness_t74_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 211/* custom_attributes_cache */
	, &CameraFilterPack_Colors_Brightness_t74_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_Brightness_t74_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_Brightness_t74_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_Brightness_t74)/* instance_size */
	, sizeof (CameraFilterPack_Colors_Brightness_t74)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Colors_Brightness_t74_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_DarkColor
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_DarkColor.h"
// Metadata Definition CameraFilterPack_Colors_DarkColor
extern TypeInfo CameraFilterPack_Colors_DarkColor_t75_il2cpp_TypeInfo;
// CameraFilterPack_Colors_DarkColor
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_DarkColorMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_DarkColor_t75_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_DarkColor_t75_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_DarkColor_t75_1_0_0;
struct CameraFilterPack_Colors_DarkColor_t75;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_DarkColor_t75_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_DarkColor_t75_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 702/* fieldStart */
	, 461/* methodStart */
	, -1/* eventStart */
	, 62/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_DarkColor_t75_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_DarkColor"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_DarkColor_t75_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 213/* custom_attributes_cache */
	, &CameraFilterPack_Colors_DarkColor_t75_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_DarkColor_t75_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_DarkColor_t75_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_DarkColor_t75)/* instance_size */
	, sizeof (CameraFilterPack_Colors_DarkColor_t75)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Colors_DarkColor_t75_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_HSV
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_HSV.h"
// Metadata Definition CameraFilterPack_Colors_HSV
extern TypeInfo CameraFilterPack_Colors_HSV_t76_il2cpp_TypeInfo;
// CameraFilterPack_Colors_HSV
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_HSVMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_HSV_t76_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_HSV_t76_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_HSV_t76_1_0_0;
struct CameraFilterPack_Colors_HSV_t76;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_HSV_t76_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_HSV_t76_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 714/* fieldStart */
	, 468/* methodStart */
	, -1/* eventStart */
	, 63/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_HSV_t76_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_HSV"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_HSV_t76_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 218/* custom_attributes_cache */
	, &CameraFilterPack_Colors_HSV_t76_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_HSV_t76_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_HSV_t76_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_HSV_t76)/* instance_size */
	, sizeof (CameraFilterPack_Colors_HSV_t76)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Colors_HSV_t76_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_HUE_Rotate
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_HUE_Rotate.h"
// Metadata Definition CameraFilterPack_Colors_HUE_Rotate
extern TypeInfo CameraFilterPack_Colors_HUE_Rotate_t77_il2cpp_TypeInfo;
// CameraFilterPack_Colors_HUE_Rotate
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_HUE_RotateMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_HUE_Rotate_t77_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_HUE_Rotate_t77_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_HUE_Rotate_t77_1_0_0;
struct CameraFilterPack_Colors_HUE_Rotate_t77;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_HUE_Rotate_t77_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_HUE_Rotate_t77_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 722/* fieldStart */
	, 475/* methodStart */
	, -1/* eventStart */
	, 64/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_HUE_Rotate_t77_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_HUE_Rotate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_HUE_Rotate_t77_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 222/* custom_attributes_cache */
	, &CameraFilterPack_Colors_HUE_Rotate_t77_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_HUE_Rotate_t77_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_HUE_Rotate_t77_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_HUE_Rotate_t77)/* instance_size */
	, sizeof (CameraFilterPack_Colors_HUE_Rotate_t77)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Colors_HUE_Rotate_t77_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_NewPosterize
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_NewPosterize.h"
// Metadata Definition CameraFilterPack_Colors_NewPosterize
extern TypeInfo CameraFilterPack_Colors_NewPosterize_t78_il2cpp_TypeInfo;
// CameraFilterPack_Colors_NewPosterize
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_NewPosterizeMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_NewPosterize_t78_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_NewPosterize_t78_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_NewPosterize_t78_1_0_0;
struct CameraFilterPack_Colors_NewPosterize_t78;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_NewPosterize_t78_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_NewPosterize_t78_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 728/* fieldStart */
	, 482/* methodStart */
	, -1/* eventStart */
	, 65/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_NewPosterize_t78_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_NewPosterize"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_NewPosterize_t78_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 224/* custom_attributes_cache */
	, &CameraFilterPack_Colors_NewPosterize_t78_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_NewPosterize_t78_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_NewPosterize_t78_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_NewPosterize_t78)/* instance_size */
	, sizeof (CameraFilterPack_Colors_NewPosterize_t78)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Colors_NewPosterize_t78_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Colors_Threshold
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Threshold.h"
// Metadata Definition CameraFilterPack_Colors_Threshold
extern TypeInfo CameraFilterPack_Colors_Threshold_t79_il2cpp_TypeInfo;
// CameraFilterPack_Colors_Threshold
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_ThresholdMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Colors_Threshold_t79_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Colors_Threshold_t79_0_0_0;
extern const Il2CppType CameraFilterPack_Colors_Threshold_t79_1_0_0;
struct CameraFilterPack_Colors_Threshold_t79;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Colors_Threshold_t79_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Colors_Threshold_t79_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 740/* fieldStart */
	, 489/* methodStart */
	, -1/* eventStart */
	, 66/* propertyStart */

};
TypeInfo CameraFilterPack_Colors_Threshold_t79_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Colors_Threshold"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Colors_Threshold_t79_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 229/* custom_attributes_cache */
	, &CameraFilterPack_Colors_Threshold_t79_0_0_0/* byval_arg */
	, &CameraFilterPack_Colors_Threshold_t79_1_0_0/* this_arg */
	, &CameraFilterPack_Colors_Threshold_t79_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Colors_Threshold_t79)/* instance_size */
	, sizeof (CameraFilterPack_Colors_Threshold_t79)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Colors_Threshold_t79_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_BigFace
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_BigFace.h"
// Metadata Definition CameraFilterPack_Distortion_BigFace
extern TypeInfo CameraFilterPack_Distortion_BigFace_t80_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_BigFace
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_BigFaceMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_BigFace_t80_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_BigFace_t80_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_BigFace_t80_1_0_0;
struct CameraFilterPack_Distortion_BigFace_t80;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_BigFace_t80_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_BigFace_t80_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 745/* fieldStart */
	, 496/* methodStart */
	, -1/* eventStart */
	, 67/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_BigFace_t80_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_BigFace"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_BigFace_t80_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 231/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_BigFace_t80_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_BigFace_t80_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_BigFace_t80_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_BigFace_t80)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_BigFace_t80)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_BigFace_t80_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_BlackHole
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_BlackHole.h"
// Metadata Definition CameraFilterPack_Distortion_BlackHole
extern TypeInfo CameraFilterPack_Distortion_BlackHole_t81_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_BlackHole
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_BlackHoleMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_BlackHole_t81_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_BlackHole_t81_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_BlackHole_t81_1_0_0;
struct CameraFilterPack_Distortion_BlackHole_t81;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_BlackHole_t81_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_BlackHole_t81_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 753/* fieldStart */
	, 503/* methodStart */
	, -1/* eventStart */
	, 68/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_BlackHole_t81_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_BlackHole"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_BlackHole_t81_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 233/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_BlackHole_t81_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_BlackHole_t81_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_BlackHole_t81_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_BlackHole_t81)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_BlackHole_t81)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_BlackHole_t81_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Dissipation
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Dissipation.h"
// Metadata Definition CameraFilterPack_Distortion_Dissipation
extern TypeInfo CameraFilterPack_Distortion_Dissipation_t82_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Dissipation
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_DissipationMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Dissipation_t82_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Dissipation_t82_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Dissipation_t82_1_0_0;
struct CameraFilterPack_Distortion_Dissipation_t82;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Dissipation_t82_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Dissipation_t82_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 765/* fieldStart */
	, 510/* methodStart */
	, -1/* eventStart */
	, 69/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Dissipation_t82_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Dissipation"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Dissipation_t82_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 238/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Dissipation_t82_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Dissipation_t82_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Dissipation_t82_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Dissipation_t82)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Dissipation_t82)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Dissipation_t82_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Dream
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Dream.h"
// Metadata Definition CameraFilterPack_Distortion_Dream
extern TypeInfo CameraFilterPack_Distortion_Dream_t83_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Dream
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_DreamMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Dream_t83_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Dream_t83_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Dream_t83_1_0_0;
struct CameraFilterPack_Distortion_Dream_t83;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Dream_t83_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Dream_t83_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 777/* fieldStart */
	, 517/* methodStart */
	, -1/* eventStart */
	, 70/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Dream_t83_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Dream"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Dream_t83_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 243/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Dream_t83_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Dream_t83_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Dream_t83_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Dream_t83)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Dream_t83)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Dream_t83_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Dream2
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Dream2.h"
// Metadata Definition CameraFilterPack_Distortion_Dream2
extern TypeInfo CameraFilterPack_Distortion_Dream2_t84_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Dream2
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Dream2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Dream2_t84_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Dream2_t84_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Dream2_t84_1_0_0;
struct CameraFilterPack_Distortion_Dream2_t84;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Dream2_t84_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Dream2_t84_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 782/* fieldStart */
	, 524/* methodStart */
	, -1/* eventStart */
	, 71/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Dream2_t84_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Dream2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Dream2_t84_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 245/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Dream2_t84_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Dream2_t84_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Dream2_t84_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Dream2_t84)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Dream2_t84)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Dream2_t84_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_FishEye
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_FishEye.h"
// Metadata Definition CameraFilterPack_Distortion_FishEye
extern TypeInfo CameraFilterPack_Distortion_FishEye_t85_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_FishEye
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_FishEyeMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_FishEye_t85_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_FishEye_t85_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_FishEye_t85_1_0_0;
struct CameraFilterPack_Distortion_FishEye_t85;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_FishEye_t85_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_FishEye_t85_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 790/* fieldStart */
	, 531/* methodStart */
	, -1/* eventStart */
	, 72/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_FishEye_t85_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_FishEye"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_FishEye_t85_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 248/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_FishEye_t85_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_FishEye_t85_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_FishEye_t85_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_FishEye_t85)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_FishEye_t85)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_FishEye_t85_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Flag
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Flag.h"
// Metadata Definition CameraFilterPack_Distortion_Flag
extern TypeInfo CameraFilterPack_Distortion_Flag_t86_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Flag
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_FlagMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Flag_t86_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Flag_t86_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Flag_t86_1_0_0;
struct CameraFilterPack_Distortion_Flag_t86;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Flag_t86_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Flag_t86_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 796/* fieldStart */
	, 539/* methodStart */
	, -1/* eventStart */
	, 73/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Flag_t86_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Flag"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Flag_t86_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 250/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Flag_t86_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Flag_t86_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Flag_t86_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Flag_t86)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Flag_t86)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Flag_t86_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Flush
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Flush.h"
// Metadata Definition CameraFilterPack_Distortion_Flush
extern TypeInfo CameraFilterPack_Distortion_Flush_t87_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Flush
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_FlushMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Flush_t87_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Flush_t87_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Flush_t87_1_0_0;
struct CameraFilterPack_Distortion_Flush_t87;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Flush_t87_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Flush_t87_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 802/* fieldStart */
	, 546/* methodStart */
	, -1/* eventStart */
	, 74/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Flush_t87_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Flush"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Flush_t87_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 252/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Flush_t87_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Flush_t87_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Flush_t87_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Flush_t87)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Flush_t87)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Flush_t87_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Half_Sphere
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Half_Sphere.h"
// Metadata Definition CameraFilterPack_Distortion_Half_Sphere
extern TypeInfo CameraFilterPack_Distortion_Half_Sphere_t88_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Half_Sphere
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Half_SphereMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Half_Sphere_t88_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Half_Sphere_t88_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Half_Sphere_t88_1_0_0;
struct CameraFilterPack_Distortion_Half_Sphere_t88;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Half_Sphere_t88_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Half_Sphere_t88_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 814/* fieldStart */
	, 553/* methodStart */
	, -1/* eventStart */
	, 75/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Half_Sphere_t88_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Half_Sphere"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Half_Sphere_t88_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 257/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Half_Sphere_t88_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Half_Sphere_t88_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Half_Sphere_t88_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Half_Sphere_t88)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Half_Sphere_t88)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Half_Sphere_t88_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Heat
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Heat.h"
// Metadata Definition CameraFilterPack_Distortion_Heat
extern TypeInfo CameraFilterPack_Distortion_Heat_t89_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Heat
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_HeatMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Heat_t89_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Heat_t89_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Heat_t89_1_0_0;
struct CameraFilterPack_Distortion_Heat_t89;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Heat_t89_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Heat_t89_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 826/* fieldStart */
	, 560/* methodStart */
	, -1/* eventStart */
	, 76/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Heat_t89_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Heat"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Heat_t89_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 262/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Heat_t89_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Heat_t89_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Heat_t89_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Heat_t89)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Heat_t89)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Heat_t89_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Lens
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Lens.h"
// Metadata Definition CameraFilterPack_Distortion_Lens
extern TypeInfo CameraFilterPack_Distortion_Lens_t90_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Lens
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_LensMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Lens_t90_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Lens_t90_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Lens_t90_1_0_0;
struct CameraFilterPack_Distortion_Lens_t90;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Lens_t90_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Lens_t90_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 832/* fieldStart */
	, 567/* methodStart */
	, -1/* eventStart */
	, 77/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Lens_t90_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Lens"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Lens_t90_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 264/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Lens_t90_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Lens_t90_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Lens_t90_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Lens_t90)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Lens_t90)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Lens_t90_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Noise.h"
// Metadata Definition CameraFilterPack_Distortion_Noise
extern TypeInfo CameraFilterPack_Distortion_Noise_t91_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_NoiseMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Noise_t91_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Noise_t91_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Noise_t91_1_0_0;
struct CameraFilterPack_Distortion_Noise_t91;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Noise_t91_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Noise_t91_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 842/* fieldStart */
	, 574/* methodStart */
	, -1/* eventStart */
	, 78/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Noise_t91_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Noise"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Noise_t91_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 268/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Noise_t91_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Noise_t91_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Noise_t91_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Noise_t91)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Noise_t91)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Noise_t91_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_ShockWave
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_ShockWave.h"
// Metadata Definition CameraFilterPack_Distortion_ShockWave
extern TypeInfo CameraFilterPack_Distortion_ShockWave_t92_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_ShockWave
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_ShockWaveMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_ShockWave_t92_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_ShockWave_t92_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_ShockWave_t92_1_0_0;
struct CameraFilterPack_Distortion_ShockWave_t92;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_ShockWave_t92_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_ShockWave_t92_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 848/* fieldStart */
	, 581/* methodStart */
	, -1/* eventStart */
	, 79/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_ShockWave_t92_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_ShockWave"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_ShockWave_t92_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 270/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_ShockWave_t92_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_ShockWave_t92_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_ShockWave_t92_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_ShockWave_t92)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_ShockWave_t92)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_ShockWave_t92_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Water_Drop
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Water_Drop.h"
// Metadata Definition CameraFilterPack_Distortion_Water_Drop
extern TypeInfo CameraFilterPack_Distortion_Water_Drop_t93_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Water_Drop
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Water_DropMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Water_Drop_t93_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Water_Drop_t93_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Water_Drop_t93_1_0_0;
struct CameraFilterPack_Distortion_Water_Drop_t93;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Water_Drop_t93_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Water_Drop_t93_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 860/* fieldStart */
	, 588/* methodStart */
	, -1/* eventStart */
	, 80/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Water_Drop_t93_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Water_Drop"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Water_Drop_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 275/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Water_Drop_t93_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Water_Drop_t93_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Water_Drop_t93_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Water_Drop_t93)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Water_Drop_t93)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Water_Drop_t93_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Distortion_Wave_Horizontal
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Wave_Horizonta.h"
// Metadata Definition CameraFilterPack_Distortion_Wave_Horizontal
extern TypeInfo CameraFilterPack_Distortion_Wave_Horizontal_t94_il2cpp_TypeInfo;
// CameraFilterPack_Distortion_Wave_Horizontal
#include "AssemblyU2DCSharp_CameraFilterPack_Distortion_Wave_HorizontaMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Distortion_Wave_Horizontal_t94_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Distortion_Wave_Horizontal_t94_0_0_0;
extern const Il2CppType CameraFilterPack_Distortion_Wave_Horizontal_t94_1_0_0;
struct CameraFilterPack_Distortion_Wave_Horizontal_t94;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Distortion_Wave_Horizontal_t94_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Distortion_Wave_Horizontal_t94_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 872/* fieldStart */
	, 595/* methodStart */
	, -1/* eventStart */
	, 81/* propertyStart */

};
TypeInfo CameraFilterPack_Distortion_Wave_Horizontal_t94_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Distortion_Wave_Horizontal"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Distortion_Wave_Horizontal_t94_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 280/* custom_attributes_cache */
	, &CameraFilterPack_Distortion_Wave_Horizontal_t94_0_0_0/* byval_arg */
	, &CameraFilterPack_Distortion_Wave_Horizontal_t94_1_0_0/* this_arg */
	, &CameraFilterPack_Distortion_Wave_Horizontal_t94_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Distortion_Wave_Horizontal_t94)/* instance_size */
	, sizeof (CameraFilterPack_Distortion_Wave_Horizontal_t94)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Distortion_Wave_Horizontal_t94_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_CellShading
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_CellShading.h"
// Metadata Definition CameraFilterPack_Drawing_CellShading
extern TypeInfo CameraFilterPack_Drawing_CellShading_t95_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_CellShading
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_CellShadingMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_CellShading_t95_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_CellShading_t95_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_CellShading_t95_1_0_0;
struct CameraFilterPack_Drawing_CellShading_t95;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_CellShading_t95_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_CellShading_t95_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 878/* fieldStart */
	, 602/* methodStart */
	, -1/* eventStart */
	, 82/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_CellShading_t95_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_CellShading"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_CellShading_t95_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 282/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_CellShading_t95_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_CellShading_t95_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_CellShading_t95_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_CellShading_t95)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_CellShading_t95)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_CellShading_t95_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_CellShading2
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_CellShading2.h"
// Metadata Definition CameraFilterPack_Drawing_CellShading2
extern TypeInfo CameraFilterPack_Drawing_CellShading2_t96_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_CellShading2
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_CellShading2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_CellShading2_t96_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_CellShading2_t96_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_CellShading2_t96_1_0_0;
struct CameraFilterPack_Drawing_CellShading2_t96;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_CellShading2_t96_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_CellShading2_t96_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 886/* fieldStart */
	, 609/* methodStart */
	, -1/* eventStart */
	, 83/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_CellShading2_t96_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_CellShading2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_CellShading2_t96_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 285/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_CellShading2_t96_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_CellShading2_t96_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_CellShading2_t96_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_CellShading2_t96)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_CellShading2_t96)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_CellShading2_t96_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Comics
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Comics.h"
// Metadata Definition CameraFilterPack_Drawing_Comics
extern TypeInfo CameraFilterPack_Drawing_Comics_t97_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Comics
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_ComicsMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Comics_t97_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Comics_t97_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Comics_t97_1_0_0;
struct CameraFilterPack_Drawing_Comics_t97;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Comics_t97_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Comics_t97_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 895/* fieldStart */
	, 616/* methodStart */
	, -1/* eventStart */
	, 84/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Comics_t97_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Comics"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Comics_t97_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 289/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Comics_t97_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Comics_t97_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Comics_t97_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Comics_t97)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Comics_t97)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Comics_t97_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Crosshatch
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Crosshatch.h"
// Metadata Definition CameraFilterPack_Drawing_Crosshatch
extern TypeInfo CameraFilterPack_Drawing_Crosshatch_t98_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Crosshatch
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_CrosshatchMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Crosshatch_t98_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Crosshatch_t98_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Crosshatch_t98_1_0_0;
struct CameraFilterPack_Drawing_Crosshatch_t98;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Crosshatch_t98_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Crosshatch_t98_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 900/* fieldStart */
	, 623/* methodStart */
	, -1/* eventStart */
	, 85/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Crosshatch_t98_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Crosshatch"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Crosshatch_t98_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 291/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Crosshatch_t98_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Crosshatch_t98_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Crosshatch_t98_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Crosshatch_t98)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Crosshatch_t98)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Crosshatch_t98_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_EnhancedComics
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_EnhancedComics.h"
// Metadata Definition CameraFilterPack_Drawing_EnhancedComics
extern TypeInfo CameraFilterPack_Drawing_EnhancedComics_t99_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_EnhancedComics
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_EnhancedComicsMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_EnhancedComics_t99_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_EnhancedComics_t99_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_EnhancedComics_t99_1_0_0;
struct CameraFilterPack_Drawing_EnhancedComics_t99;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_EnhancedComics_t99_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_EnhancedComics_t99_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 906/* fieldStart */
	, 630/* methodStart */
	, -1/* eventStart */
	, 86/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_EnhancedComics_t99_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_EnhancedComics"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_EnhancedComics_t99_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 293/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_EnhancedComics_t99_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_EnhancedComics_t99_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_EnhancedComics_t99_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_EnhancedComics_t99)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_EnhancedComics_t99)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_EnhancedComics_t99_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Halftone
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Halftone.h"
// Metadata Definition CameraFilterPack_Drawing_Halftone
extern TypeInfo CameraFilterPack_Drawing_Halftone_t100_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Halftone
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_HalftoneMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Halftone_t100_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Halftone_t100_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Halftone_t100_1_0_0;
struct CameraFilterPack_Drawing_Halftone_t100;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Halftone_t100_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Halftone_t100_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 918/* fieldStart */
	, 637/* methodStart */
	, -1/* eventStart */
	, 87/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Halftone_t100_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Halftone"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Halftone_t100_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 301/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Halftone_t100_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Halftone_t100_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Halftone_t100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Halftone_t100)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Halftone_t100)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Halftone_t100_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Laplacian
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Laplacian.h"
// Metadata Definition CameraFilterPack_Drawing_Laplacian
extern TypeInfo CameraFilterPack_Drawing_Laplacian_t101_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Laplacian
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_LaplacianMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Laplacian_t101_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Laplacian_t101_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Laplacian_t101_1_0_0;
struct CameraFilterPack_Drawing_Laplacian_t101;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Laplacian_t101_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Laplacian_t101_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 925/* fieldStart */
	, 644/* methodStart */
	, -1/* eventStart */
	, 88/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Laplacian_t101_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Laplacian"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Laplacian_t101_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 304/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Laplacian_t101_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Laplacian_t101_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Laplacian_t101_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Laplacian_t101)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Laplacian_t101)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Manga
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga.h"
// Metadata Definition CameraFilterPack_Drawing_Manga
extern TypeInfo CameraFilterPack_Drawing_Manga_t102_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Manga
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_MangaMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Manga_t102_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Manga_t102_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Manga_t102_1_0_0;
struct CameraFilterPack_Drawing_Manga_t102;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Manga_t102_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Manga_t102_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 929/* fieldStart */
	, 650/* methodStart */
	, -1/* eventStart */
	, 89/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Manga_t102_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Manga"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Manga_t102_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 305/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Manga_t102_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Manga_t102_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Manga_t102_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Manga_t102)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Manga_t102)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Manga_t102_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Manga2
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga2.h"
// Metadata Definition CameraFilterPack_Drawing_Manga2
extern TypeInfo CameraFilterPack_Drawing_Manga2_t103_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Manga2
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga2MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Manga2_t103_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Manga2_t103_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Manga2_t103_1_0_0;
struct CameraFilterPack_Drawing_Manga2_t103;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Manga2_t103_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Manga2_t103_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 934/* fieldStart */
	, 657/* methodStart */
	, -1/* eventStart */
	, 90/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Manga2_t103_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Manga2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Manga2_t103_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 307/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Manga2_t103_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Manga2_t103_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Manga2_t103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Manga2_t103)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Manga2_t103)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Manga2_t103_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Manga3
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga3.h"
// Metadata Definition CameraFilterPack_Drawing_Manga3
extern TypeInfo CameraFilterPack_Drawing_Manga3_t104_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Manga3
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga3MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Manga3_t104_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Manga3_t104_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Manga3_t104_1_0_0;
struct CameraFilterPack_Drawing_Manga3_t104;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Manga3_t104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Manga3_t104_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 939/* fieldStart */
	, 664/* methodStart */
	, -1/* eventStart */
	, 91/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Manga3_t104_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Manga3"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Manga3_t104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 309/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Manga3_t104_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Manga3_t104_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Manga3_t104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Manga3_t104)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Manga3_t104)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Manga3_t104_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Manga4
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga4.h"
// Metadata Definition CameraFilterPack_Drawing_Manga4
extern TypeInfo CameraFilterPack_Drawing_Manga4_t105_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Manga4
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga4MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Manga4_t105_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Manga4_t105_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Manga4_t105_1_0_0;
struct CameraFilterPack_Drawing_Manga4_t105;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Manga4_t105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Manga4_t105_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 944/* fieldStart */
	, 671/* methodStart */
	, -1/* eventStart */
	, 92/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Manga4_t105_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Manga4"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Manga4_t105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 311/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Manga4_t105_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Manga4_t105_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Manga4_t105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Manga4_t105)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Manga4_t105)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Manga4_t105_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Manga5
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga5.h"
// Metadata Definition CameraFilterPack_Drawing_Manga5
extern TypeInfo CameraFilterPack_Drawing_Manga5_t106_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Manga5
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga5MethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Manga5_t106_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Manga5_t106_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Manga5_t106_1_0_0;
struct CameraFilterPack_Drawing_Manga5_t106;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Manga5_t106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Manga5_t106_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 949/* fieldStart */
	, 678/* methodStart */
	, -1/* eventStart */
	, 93/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Manga5_t106_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Manga5"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Manga5_t106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 313/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Manga5_t106_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Manga5_t106_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Manga5_t106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Manga5_t106)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Manga5_t106)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Manga5_t106_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Manga_Color
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga_Color.h"
// Metadata Definition CameraFilterPack_Drawing_Manga_Color
extern TypeInfo CameraFilterPack_Drawing_Manga_Color_t107_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Manga_Color
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Manga_ColorMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Manga_Color_t107_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Manga_Color_t107_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Manga_Color_t107_1_0_0;
struct CameraFilterPack_Drawing_Manga_Color_t107;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Manga_Color_t107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Manga_Color_t107_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 954/* fieldStart */
	, 685/* methodStart */
	, -1/* eventStart */
	, 94/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Manga_Color_t107_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Manga_Color"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Manga_Color_t107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 315/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Manga_Color_t107_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Manga_Color_t107_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Manga_Color_t107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Manga_Color_t107)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Manga_Color_t107)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Manga_Color_t107_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_NewCellShading
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_NewCellShading.h"
// Metadata Definition CameraFilterPack_Drawing_NewCellShading
extern TypeInfo CameraFilterPack_Drawing_NewCellShading_t108_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_NewCellShading
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_NewCellShadingMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_NewCellShading_t108_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_NewCellShading_t108_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_NewCellShading_t108_1_0_0;
struct CameraFilterPack_Drawing_NewCellShading_t108;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_NewCellShading_t108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_NewCellShading_t108_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 959/* fieldStart */
	, 692/* methodStart */
	, -1/* eventStart */
	, 95/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_NewCellShading_t108_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_NewCellShading"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_NewCellShading_t108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 317/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_NewCellShading_t108_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_NewCellShading_t108_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_NewCellShading_t108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_NewCellShading_t108)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_NewCellShading_t108)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// CameraFilterPack_Drawing_Toon
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_Toon.h"
// Metadata Definition CameraFilterPack_Drawing_Toon
extern TypeInfo CameraFilterPack_Drawing_Toon_t109_il2cpp_TypeInfo;
// CameraFilterPack_Drawing_Toon
#include "AssemblyU2DCSharp_CameraFilterPack_Drawing_ToonMethodDeclarations.h"
static const EncodedMethodIndex CameraFilterPack_Drawing_Toon_t109_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern const Il2CppType CameraFilterPack_Drawing_Toon_t109_0_0_0;
extern const Il2CppType CameraFilterPack_Drawing_Toon_t109_1_0_0;
struct CameraFilterPack_Drawing_Toon_t109;
const Il2CppTypeDefinitionMetadata CameraFilterPack_Drawing_Toon_t109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, CameraFilterPack_Drawing_Toon_t109_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 964/* fieldStart */
	, 698/* methodStart */
	, -1/* eventStart */
	, 96/* propertyStart */

};
TypeInfo CameraFilterPack_Drawing_Toon_t109_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraFilterPack_Drawing_Toon"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CameraFilterPack_Drawing_Toon_t109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 319/* custom_attributes_cache */
	, &CameraFilterPack_Drawing_Toon_t109_0_0_0/* byval_arg */
	, &CameraFilterPack_Drawing_Toon_t109_1_0_0/* this_arg */
	, &CameraFilterPack_Drawing_Toon_t109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraFilterPack_Drawing_Toon_t109)/* instance_size */
	, sizeof (CameraFilterPack_Drawing_Toon_t109)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CameraFilterPack_Drawing_Toon_t109_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
