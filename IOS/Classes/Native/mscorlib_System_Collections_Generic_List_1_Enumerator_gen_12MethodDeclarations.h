﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Boolean>
struct Enumerator_t2411;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t577;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15833_gshared (Enumerator_t2411 * __this, List_1_t577 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15833(__this, ___l, method) (( void (*) (Enumerator_t2411 *, List_1_t577 *, const MethodInfo*))Enumerator__ctor_m15833_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15834_gshared (Enumerator_t2411 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15834(__this, method) (( Object_t * (*) (Enumerator_t2411 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15834_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m15835_gshared (Enumerator_t2411 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15835(__this, method) (( void (*) (Enumerator_t2411 *, const MethodInfo*))Enumerator_Dispose_m15835_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m15836_gshared (Enumerator_t2411 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15836(__this, method) (( void (*) (Enumerator_t2411 *, const MethodInfo*))Enumerator_VerifyState_m15836_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15837_gshared (Enumerator_t2411 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15837(__this, method) (( bool (*) (Enumerator_t2411 *, const MethodInfo*))Enumerator_MoveNext_m15837_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Boolean>::get_Current()
extern "C" bool Enumerator_get_Current_m15838_gshared (Enumerator_t2411 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15838(__this, method) (( bool (*) (Enumerator_t2411 *, const MethodInfo*))Enumerator_get_Current_m15838_gshared)(__this, method)
