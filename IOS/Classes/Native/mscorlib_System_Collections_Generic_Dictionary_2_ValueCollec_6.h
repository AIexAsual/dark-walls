﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>
struct  Enumerator_t2336 
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>::host_enumerator
	Enumerator_t2333  ___host_enumerator_0;
};
