﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<ArrayIndexes>
struct List_1_t586;
// ArrayIndexes
struct ArrayIndexes_t441;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<ArrayIndexes>
struct  Enumerator_t2481 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::l
	List_1_t586 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<ArrayIndexes>::current
	ArrayIndexes_t441 * ___current_3;
};
