﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_BlueScreen
struct CameraFilterPack_Blend2Camera_BlueScreen_t17;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_BlueScreen::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_BlueScreen__ctor_m51 (CameraFilterPack_Blend2Camera_BlueScreen_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_BlueScreen::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_BlueScreen_get_material_m52 (CameraFilterPack_Blend2Camera_BlueScreen_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::Start()
extern "C" void CameraFilterPack_Blend2Camera_BlueScreen_Start_m53 (CameraFilterPack_Blend2Camera_BlueScreen_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_BlueScreen_OnRenderImage_m54 (CameraFilterPack_Blend2Camera_BlueScreen_t17 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::Update()
extern "C" void CameraFilterPack_Blend2Camera_BlueScreen_Update_m55 (CameraFilterPack_Blend2Camera_BlueScreen_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_BlueScreen_OnEnable_m56 (CameraFilterPack_Blend2Camera_BlueScreen_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_BlueScreen::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_BlueScreen_OnDisable_m57 (CameraFilterPack_Blend2Camera_BlueScreen_t17 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
