﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t1711;
struct MonoIOStat_t1711_marshaled;

void MonoIOStat_t1711_marshal(const MonoIOStat_t1711& unmarshaled, MonoIOStat_t1711_marshaled& marshaled);
void MonoIOStat_t1711_marshal_back(const MonoIOStat_t1711_marshaled& marshaled, MonoIOStat_t1711& unmarshaled);
void MonoIOStat_t1711_marshal_cleanup(MonoIOStat_t1711_marshaled& marshaled);
