﻿#pragma once
#include <stdint.h>
// System.Action`1<MNDialogResult>
struct Action_1_t277;
// System.Object
#include "mscorlib_System_Object.h"
// MobileNativeDialog
struct  MobileNativeDialog_t279  : public Object_t
{
	// System.Action`1<MNDialogResult> MobileNativeDialog::OnComplete
	Action_1_t277 * ___OnComplete_0;
};
struct MobileNativeDialog_t279_StaticFields{
	// System.Action`1<MNDialogResult> MobileNativeDialog::<>f__am$cache1
	Action_1_t277 * ___U3CU3Ef__amU24cache1_1;
};
