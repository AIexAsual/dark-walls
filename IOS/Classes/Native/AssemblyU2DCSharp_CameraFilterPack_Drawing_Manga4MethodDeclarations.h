﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Manga4
struct CameraFilterPack_Drawing_Manga4_t105;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Manga4::.ctor()
extern "C" void CameraFilterPack_Drawing_Manga4__ctor_m671 (CameraFilterPack_Drawing_Manga4_t105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga4::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Manga4_get_material_m672 (CameraFilterPack_Drawing_Manga4_t105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::Start()
extern "C" void CameraFilterPack_Drawing_Manga4_Start_m673 (CameraFilterPack_Drawing_Manga4_t105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Manga4_OnRenderImage_m674 (CameraFilterPack_Drawing_Manga4_t105 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::OnValidate()
extern "C" void CameraFilterPack_Drawing_Manga4_OnValidate_m675 (CameraFilterPack_Drawing_Manga4_t105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::Update()
extern "C" void CameraFilterPack_Drawing_Manga4_Update_m676 (CameraFilterPack_Drawing_Manga4_t105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga4::OnDisable()
extern "C" void CameraFilterPack_Drawing_Manga4_OnDisable_m677 (CameraFilterPack_Drawing_Manga4_t105 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
