﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.Space>
struct IList_1_t568;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<UnityEngine.Space>
struct  Collection_1_t2442  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::syncRoot
	Object_t * ___syncRoot_1;
};
