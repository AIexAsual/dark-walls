﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// StereoLensFlare
struct StereoLensFlare_t227;

// System.Void StereoLensFlare::.ctor()
extern "C" void StereoLensFlare__ctor_m1416 (StereoLensFlare_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoLensFlare::Awake()
extern "C" void StereoLensFlare_Awake_m1417 (StereoLensFlare_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
