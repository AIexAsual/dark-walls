﻿#pragma once
#include <stdint.h>
// UnityEngine.MeshRenderer
struct MeshRenderer_t223;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// CardboardOnGUIWindow
struct  CardboardOnGUIWindow_t224  : public MonoBehaviour_t4
{
	// UnityEngine.MeshRenderer CardboardOnGUIWindow::meshRenderer
	MeshRenderer_t223 * ___meshRenderer_2;
	// UnityEngine.Rect CardboardOnGUIWindow::rect
	Rect_t225  ___rect_3;
};
