﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.Color>
struct IList_1_t567;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>
struct  ReadOnlyCollection_1_t2432  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::list
	Object_t* ___list_0;
};
