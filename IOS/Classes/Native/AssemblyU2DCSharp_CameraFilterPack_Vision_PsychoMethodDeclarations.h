﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Vision_Psycho
struct CameraFilterPack_Vision_Psycho_t208;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Vision_Psycho::.ctor()
extern "C" void CameraFilterPack_Vision_Psycho__ctor_m1352 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Psycho::get_material()
extern "C" Material_t2 * CameraFilterPack_Vision_Psycho_get_material_m1353 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::Start()
extern "C" void CameraFilterPack_Vision_Psycho_Start_m1354 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Vision_Psycho_OnRenderImage_m1355 (CameraFilterPack_Vision_Psycho_t208 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::OnValidate()
extern "C" void CameraFilterPack_Vision_Psycho_OnValidate_m1356 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::Update()
extern "C" void CameraFilterPack_Vision_Psycho_Update_m1357 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Psycho::OnDisable()
extern "C" void CameraFilterPack_Vision_Psycho_OnDisable_m1358 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
