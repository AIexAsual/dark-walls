﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Edge_Sigmoid
struct  CameraFilterPack_Edge_Sigmoid_t116  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Edge_Sigmoid::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Edge_Sigmoid::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Edge_Sigmoid::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Edge_Sigmoid::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Edge_Sigmoid::Gain
	float ___Gain_6;
};
struct CameraFilterPack_Edge_Sigmoid_t116_StaticFields{
	// System.Single CameraFilterPack_Edge_Sigmoid::ChangeGain
	float ___ChangeGain_7;
};
