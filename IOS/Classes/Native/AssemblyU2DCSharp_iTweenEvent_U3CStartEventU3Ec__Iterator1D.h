﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t348;
// System.Object
struct Object_t;
// iTweenEvent
struct iTweenEvent_t443;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
// iTweenEvent/<StartEvent>c__Iterator1D
struct  U3CStartEventU3Ec__Iterator1D_t444  : public Object_t
{
	// System.Collections.Hashtable iTweenEvent/<StartEvent>c__Iterator1D::<optionsHash>__0
	Hashtable_t348 * ___U3CoptionsHashU3E__0_0;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object> iTweenEvent/<StartEvent>c__Iterator1D::<$s_68>__1
	Enumerator_t445  ___U3CU24s_68U3E__1_1;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Object> iTweenEvent/<StartEvent>c__Iterator1D::<pair>__2
	KeyValuePair_2_t446  ___U3CpairU3E__2_2;
	// System.Int32 iTweenEvent/<StartEvent>c__Iterator1D::$PC
	int32_t ___U24PC_3;
	// System.Object iTweenEvent/<StartEvent>c__Iterator1D::$current
	Object_t * ___U24current_4;
	// iTweenEvent iTweenEvent/<StartEvent>c__Iterator1D::<>f__this
	iTweenEvent_t443 * ___U3CU3Ef__this_5;
};
