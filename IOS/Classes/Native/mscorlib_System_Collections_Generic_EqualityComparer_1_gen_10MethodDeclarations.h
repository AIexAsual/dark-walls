﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<iTween/LoopType>
struct EqualityComparer_1_t2461;
// System.Object
struct Object_t;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Collections.Generic.EqualityComparer`1<iTween/LoopType>::.ctor()
extern "C" void EqualityComparer_1__ctor_m16620_gshared (EqualityComparer_1_t2461 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m16620(__this, method) (( void (*) (EqualityComparer_1_t2461 *, const MethodInfo*))EqualityComparer_1__ctor_m16620_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<iTween/LoopType>::.cctor()
extern "C" void EqualityComparer_1__cctor_m16621_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m16621(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m16621_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<iTween/LoopType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622_gshared (EqualityComparer_1_t2461 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2461 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16622_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<iTween/LoopType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623_gshared (EqualityComparer_1_t2461 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2461 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16623_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<iTween/LoopType>::get_Default()
extern "C" EqualityComparer_1_t2461 * EqualityComparer_1_get_Default_m16624_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m16624(__this /* static, unused */, method) (( EqualityComparer_1_t2461 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m16624_gshared)(__this /* static, unused */, method)
