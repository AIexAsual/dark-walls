﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t2829;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2824;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21907_gshared (Enumerator_t2829 * __this, Dictionary_2_t2824 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21907(__this, ___host, method) (( void (*) (Enumerator_t2829 *, Dictionary_2_t2824 *, const MethodInfo*))Enumerator__ctor_m21907_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21908_gshared (Enumerator_t2829 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21908(__this, method) (( Object_t * (*) (Enumerator_t2829 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21908_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m21909_gshared (Enumerator_t2829 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21909(__this, method) (( void (*) (Enumerator_t2829 *, const MethodInfo*))Enumerator_Dispose_m21909_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21910_gshared (Enumerator_t2829 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21910(__this, method) (( bool (*) (Enumerator_t2829 *, const MethodInfo*))Enumerator_MoveNext_m21910_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m21911_gshared (Enumerator_t2829 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21911(__this, method) (( Object_t * (*) (Enumerator_t2829 *, const MethodInfo*))Enumerator_get_Current_m21911_gshared)(__this, method)
