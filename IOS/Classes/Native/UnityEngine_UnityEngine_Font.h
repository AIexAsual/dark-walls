﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t793;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t956;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct  Font_t643  : public Object_t473
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t956 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t643_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t793 * ___textureRebuilt_2;
};
