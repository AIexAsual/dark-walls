﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct Enumerator_t2392;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t560;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15550_gshared (Enumerator_t2392 * __this, List_1_t560 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15550(__this, ___l, method) (( void (*) (Enumerator_t2392 *, List_1_t560 *, const MethodInfo*))Enumerator__ctor_m15550_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15551_gshared (Enumerator_t2392 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15551(__this, method) (( Object_t * (*) (Enumerator_t2392 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15551_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15552_gshared (Enumerator_t2392 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15552(__this, method) (( void (*) (Enumerator_t2392 *, const MethodInfo*))Enumerator_Dispose_m15552_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m15553_gshared (Enumerator_t2392 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15553(__this, method) (( void (*) (Enumerator_t2392 *, const MethodInfo*))Enumerator_VerifyState_m15553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15554_gshared (Enumerator_t2392 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15554(__this, method) (( bool (*) (Enumerator_t2392 *, const MethodInfo*))Enumerator_MoveNext_m15554_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m15555_gshared (Enumerator_t2392 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15555(__this, method) (( int32_t (*) (Enumerator_t2392 *, const MethodInfo*))Enumerator_get_Current_m15555_gshared)(__this, method)
