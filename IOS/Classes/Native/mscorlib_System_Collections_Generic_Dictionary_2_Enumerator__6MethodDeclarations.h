﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
struct Enumerator_t2368;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t2363;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15243_gshared (Enumerator_t2368 * __this, Dictionary_2_t2363 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15243(__this, ___dictionary, method) (( void (*) (Enumerator_t2368 *, Dictionary_2_t2363 *, const MethodInfo*))Enumerator__ctor_m15243_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15244_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15244(__this, method) (( Object_t * (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15244_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15245_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15245(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15245_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15246_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15246(__this, method) (( Object_t * (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15246_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15247_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15247(__this, method) (( Object_t * (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15247_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15248_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15248(__this, method) (( bool (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_MoveNext_m15248_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t2364  Enumerator_get_Current_m15249_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15249(__this, method) (( KeyValuePair_2_t2364  (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_get_Current_m15249_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m15250_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15250(__this, method) (( Object_t * (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_get_CurrentKey_m15250_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m15251_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15251(__this, method) (( int32_t (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_get_CurrentValue_m15251_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m15252_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15252(__this, method) (( void (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_VerifyState_m15252_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15253_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15253(__this, method) (( void (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_VerifyCurrent_m15253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15254_gshared (Enumerator_t2368 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15254(__this, method) (( void (*) (Enumerator_t2368 *, const MethodInfo*))Enumerator_Dispose_m15254_gshared)(__this, method)
