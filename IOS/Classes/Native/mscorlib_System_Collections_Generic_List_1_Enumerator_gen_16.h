﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<iTween/EaseType>
struct List_1_t580;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// System.Collections.Generic.List`1/Enumerator<iTween/EaseType>
struct  Enumerator_t2449 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::l
	List_1_t580 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<iTween/EaseType>::current
	int32_t ___current_3;
};
