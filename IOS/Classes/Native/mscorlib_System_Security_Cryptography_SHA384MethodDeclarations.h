﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA384
struct SHA384_t1991;

// System.Void System.Security.Cryptography.SHA384::.ctor()
extern "C" void SHA384__ctor_m12080 (SHA384_t1991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
