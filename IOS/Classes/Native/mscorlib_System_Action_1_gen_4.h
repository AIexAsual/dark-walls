﻿#pragma once
#include <stdint.h>
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t1109;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct  Action_1_t841  : public MulticastDelegate_t219
{
};
