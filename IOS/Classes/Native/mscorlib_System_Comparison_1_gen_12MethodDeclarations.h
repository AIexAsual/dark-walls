﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<System.Boolean>
struct Comparison_1_t2421;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m15928_gshared (Comparison_1_t2421 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m15928(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2421 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m15928_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.Boolean>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m15929_gshared (Comparison_1_t2421 * __this, bool ___x, bool ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m15929(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2421 *, bool, bool, const MethodInfo*))Comparison_1_Invoke_m15929_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.Boolean>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m15930_gshared (Comparison_1_t2421 * __this, bool ___x, bool ___y, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m15930(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2421 *, bool, bool, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15930_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m15931_gshared (Comparison_1_t2421 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m15931(__this, ___result, method) (( int32_t (*) (Comparison_1_t2421 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15931_gshared)(__this, ___result, method)
