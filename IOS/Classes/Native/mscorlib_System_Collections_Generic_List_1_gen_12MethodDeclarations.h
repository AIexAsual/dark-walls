﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t584;
// System.Object
struct Object_t;
// UnityEngine.AudioClip
struct AudioClip_t472;
// System.Collections.Generic.IEnumerable`1<UnityEngine.AudioClip>
struct IEnumerable_1_t3048;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AudioClip>
struct IEnumerator_1_t3049;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>
struct ICollection_1_t3050;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>
struct ReadOnlyCollection_1_t2471;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t338;
// System.Predicate`1<UnityEngine.AudioClip>
struct Predicate_1_t2472;
// System.Comparison`1<UnityEngine.AudioClip>
struct Comparison_1_t2474;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioClip>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_19.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m3399(__this, method) (( void (*) (List_1_t584 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16734(__this, ___collection, method) (( void (*) (List_1_t584 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::.ctor(System.Int32)
#define List_1__ctor_m16735(__this, ___capacity, method) (( void (*) (List_1_t584 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::.cctor()
#define List_1__cctor_m16736(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16737(__this, method) (( Object_t* (*) (List_1_t584 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16738(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t584 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16739(__this, method) (( Object_t * (*) (List_1_t584 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16740(__this, ___item, method) (( int32_t (*) (List_1_t584 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16741(__this, ___item, method) (( bool (*) (List_1_t584 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16742(__this, ___item, method) (( int32_t (*) (List_1_t584 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16743(__this, ___index, ___item, method) (( void (*) (List_1_t584 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16744(__this, ___item, method) (( void (*) (List_1_t584 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16745(__this, method) (( bool (*) (List_1_t584 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16746(__this, method) (( bool (*) (List_1_t584 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16747(__this, method) (( Object_t * (*) (List_1_t584 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16748(__this, method) (( bool (*) (List_1_t584 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16749(__this, method) (( bool (*) (List_1_t584 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16750(__this, ___index, method) (( Object_t * (*) (List_1_t584 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16751(__this, ___index, ___value, method) (( void (*) (List_1_t584 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Add(T)
#define List_1_Add_m16752(__this, ___item, method) (( void (*) (List_1_t584 *, AudioClip_t472 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16753(__this, ___newCount, method) (( void (*) (List_1_t584 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16754(__this, ___collection, method) (( void (*) (List_1_t584 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16755(__this, ___enumerable, method) (( void (*) (List_1_t584 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16756(__this, ___collection, method) (( void (*) (List_1_t584 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.AudioClip>::AsReadOnly()
#define List_1_AsReadOnly_m16757(__this, method) (( ReadOnlyCollection_1_t2471 * (*) (List_1_t584 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Clear()
#define List_1_Clear_m16758(__this, method) (( void (*) (List_1_t584 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::Contains(T)
#define List_1_Contains_m16759(__this, ___item, method) (( bool (*) (List_1_t584 *, AudioClip_t472 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16760(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t584 *, AudioClipU5BU5D_t338*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioClip>::Find(System.Predicate`1<T>)
#define List_1_Find_m16761(__this, ___match, method) (( AudioClip_t472 * (*) (List_1_t584 *, Predicate_1_t2472 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16762(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2472 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16763(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t584 *, int32_t, int32_t, Predicate_1_t2472 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.AudioClip>::GetEnumerator()
#define List_1_GetEnumerator_m16764(__this, method) (( Enumerator_t2473  (*) (List_1_t584 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::IndexOf(T)
#define List_1_IndexOf_m16765(__this, ___item, method) (( int32_t (*) (List_1_t584 *, AudioClip_t472 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16766(__this, ___start, ___delta, method) (( void (*) (List_1_t584 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16767(__this, ___index, method) (( void (*) (List_1_t584 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Insert(System.Int32,T)
#define List_1_Insert_m16768(__this, ___index, ___item, method) (( void (*) (List_1_t584 *, int32_t, AudioClip_t472 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16769(__this, ___collection, method) (( void (*) (List_1_t584 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioClip>::Remove(T)
#define List_1_Remove_m16770(__this, ___item, method) (( bool (*) (List_1_t584 *, AudioClip_t472 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16771(__this, ___match, method) (( int32_t (*) (List_1_t584 *, Predicate_1_t2472 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16772(__this, ___index, method) (( void (*) (List_1_t584 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Reverse()
#define List_1_Reverse_m16773(__this, method) (( void (*) (List_1_t584 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Sort()
#define List_1_Sort_m16774(__this, method) (( void (*) (List_1_t584 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16775(__this, ___comparison, method) (( void (*) (List_1_t584 *, Comparison_1_t2474 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.AudioClip>::ToArray()
#define List_1_ToArray_m3412(__this, method) (( AudioClipU5BU5D_t338* (*) (List_1_t584 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::TrimExcess()
#define List_1_TrimExcess_m16776(__this, method) (( void (*) (List_1_t584 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Capacity()
#define List_1_get_Capacity_m16777(__this, method) (( int32_t (*) (List_1_t584 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16778(__this, ___value, method) (( void (*) (List_1_t584 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Count()
#define List_1_get_Count_m16779(__this, method) (( int32_t (*) (List_1_t584 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioClip>::get_Item(System.Int32)
#define List_1_get_Item_m16780(__this, ___index, method) (( AudioClip_t472 * (*) (List_1_t584 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioClip>::set_Item(System.Int32,T)
#define List_1_set_Item_m16781(__this, ___index, ___value, method) (( void (*) (List_1_t584 *, int32_t, AudioClip_t472 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
