﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.PointerInputModule/ButtonState
struct ButtonState_t616;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
struct  Comparison_1_t2531  : public MulticastDelegate_t219
{
};
