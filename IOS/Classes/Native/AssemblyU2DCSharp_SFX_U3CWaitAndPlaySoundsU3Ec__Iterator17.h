﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SFX
struct SFX_t385;
// System.Object
#include "mscorlib_System_Object.h"
// SFX/<WaitAndPlaySounds>c__Iterator17
struct  U3CWaitAndPlaySoundsU3Ec__Iterator17_t407  : public Object_t
{
	// System.Single SFX/<WaitAndPlaySounds>c__Iterator17::time
	float ___time_0;
	// System.Int32 SFX/<WaitAndPlaySounds>c__Iterator17::$PC
	int32_t ___U24PC_1;
	// System.Object SFX/<WaitAndPlaySounds>c__Iterator17::$current
	Object_t * ___U24current_2;
	// System.Single SFX/<WaitAndPlaySounds>c__Iterator17::<$>time
	float ___U3CU24U3Etime_3;
	// SFX SFX/<WaitAndPlaySounds>c__Iterator17::<>f__this
	SFX_t385 * ___U3CU3Ef__this_4;
};
