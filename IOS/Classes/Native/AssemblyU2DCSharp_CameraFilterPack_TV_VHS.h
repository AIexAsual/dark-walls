﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_VHS
struct  CameraFilterPack_TV_VHS_t194  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_VHS::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_VHS::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_TV_VHS::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_TV_VHS::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_TV_VHS::Cryptage
	float ___Cryptage_6;
	// System.Single CameraFilterPack_TV_VHS::Parasite
	float ___Parasite_7;
	// System.Single CameraFilterPack_TV_VHS::Calibrage
	float ___Calibrage_8;
	// System.Single CameraFilterPack_TV_VHS::WhiteParasite
	float ___WhiteParasite_9;
};
struct CameraFilterPack_TV_VHS_t194_StaticFields{
	// System.Single CameraFilterPack_TV_VHS::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_TV_VHS::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_TV_VHS::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_TV_VHS::ChangeValue4
	float ___ChangeValue4_13;
};
