﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Film_Grain
struct CameraFilterPack_Film_Grain_t144;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Film_Grain::.ctor()
extern "C" void CameraFilterPack_Film_Grain__ctor_m934 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Film_Grain::get_material()
extern "C" Material_t2 * CameraFilterPack_Film_Grain_get_material_m935 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::Start()
extern "C" void CameraFilterPack_Film_Grain_Start_m936 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Film_Grain_OnRenderImage_m937 (CameraFilterPack_Film_Grain_t144 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::OnValidate()
extern "C" void CameraFilterPack_Film_Grain_OnValidate_m938 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::Update()
extern "C" void CameraFilterPack_Film_Grain_Update_m939 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_Grain::OnDisable()
extern "C" void CameraFilterPack_Film_Grain_OnDisable_m940 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
