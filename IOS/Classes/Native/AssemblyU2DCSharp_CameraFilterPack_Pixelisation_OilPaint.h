﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Pixelisation_OilPaint
struct  CameraFilterPack_Pixelisation_OilPaint_t171  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Pixelisation_OilPaint::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Pixelisation_OilPaint::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Pixelisation_OilPaint::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Pixelisation_OilPaint::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Pixelisation_OilPaint::Value
	float ___Value_6;
};
struct CameraFilterPack_Pixelisation_OilPaint_t171_StaticFields{
	// System.Single CameraFilterPack_Pixelisation_OilPaint::ChangeValue
	float ___ChangeValue_7;
};
