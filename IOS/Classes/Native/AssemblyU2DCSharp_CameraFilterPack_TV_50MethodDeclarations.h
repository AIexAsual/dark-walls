﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_50
struct CameraFilterPack_TV_50_t175;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_50::.ctor()
extern "C" void CameraFilterPack_TV_50__ctor_m1133 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_50::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_50_get_material_m1134 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::Start()
extern "C" void CameraFilterPack_TV_50_Start_m1135 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_50_OnRenderImage_m1136 (CameraFilterPack_TV_50_t175 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::Update()
extern "C" void CameraFilterPack_TV_50_Update_m1137 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_50::OnDisable()
extern "C" void CameraFilterPack_TV_50_OnDisable_m1138 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
