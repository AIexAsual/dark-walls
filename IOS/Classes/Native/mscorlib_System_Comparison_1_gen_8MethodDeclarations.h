﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<System.Char>
struct Comparison_1_t2300;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m14481_gshared (Comparison_1_t2300 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m14481(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2300 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m14481_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.Char>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m14482_gshared (Comparison_1_t2300 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m14482(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2300 *, uint16_t, uint16_t, const MethodInfo*))Comparison_1_Invoke_m14482_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.Char>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m14483_gshared (Comparison_1_t2300 * __this, uint16_t ___x, uint16_t ___y, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m14483(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2300 *, uint16_t, uint16_t, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m14483_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m14484_gshared (Comparison_1_t2300 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m14484(__this, ___result, method) (( int32_t (*) (Comparison_1_t2300 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m14484_gshared)(__this, ___result, method)
