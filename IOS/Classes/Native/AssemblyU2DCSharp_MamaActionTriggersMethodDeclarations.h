﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MamaActionTriggers
struct MamaActionTriggers_t365;

// System.Void MamaActionTriggers::.ctor()
extern "C" void MamaActionTriggers__ctor_m2110 (MamaActionTriggers_t365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MamaActionTriggers::.cctor()
extern "C" void MamaActionTriggers__cctor_m2111 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
