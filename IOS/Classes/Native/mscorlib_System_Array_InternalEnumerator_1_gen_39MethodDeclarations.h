﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievementDescription>
struct InternalEnumerator_1_t2635;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.IAchievementDescription
struct IAchievementDescription_t3146;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievementDescription>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m19292(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2635 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievementDescription>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19293(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2635 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievementDescription>::Dispose()
#define InternalEnumerator_1_Dispose_m19294(__this, method) (( void (*) (InternalEnumerator_1_t2635 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievementDescription>::MoveNext()
#define InternalEnumerator_1_MoveNext_m19295(__this, method) (( bool (*) (InternalEnumerator_1_t2635 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IAchievementDescription>::get_Current()
#define InternalEnumerator_1_get_Current_m19296(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2635 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
