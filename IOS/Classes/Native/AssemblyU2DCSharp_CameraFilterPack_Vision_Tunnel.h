﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Vision_Tunnel
struct  CameraFilterPack_Vision_Tunnel_t209  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Vision_Tunnel::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Vision_Tunnel::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Vision_Tunnel::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Vision_Tunnel::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Vision_Tunnel::Value
	float ___Value_6;
	// System.Single CameraFilterPack_Vision_Tunnel::Value2
	float ___Value2_7;
	// System.Single CameraFilterPack_Vision_Tunnel::Intensity
	float ___Intensity_8;
	// System.Single CameraFilterPack_Vision_Tunnel::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Vision_Tunnel_t209_StaticFields{
	// System.Single CameraFilterPack_Vision_Tunnel::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Vision_Tunnel::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Vision_Tunnel::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Vision_Tunnel::ChangeValue4
	float ___ChangeValue4_13;
};
