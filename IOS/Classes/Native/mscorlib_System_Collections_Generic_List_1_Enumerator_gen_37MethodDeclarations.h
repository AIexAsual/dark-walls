﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
struct Enumerator_t2685;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t957;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m20091_gshared (Enumerator_t2685 * __this, List_1_t957 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m20091(__this, ___l, method) (( void (*) (Enumerator_t2685 *, List_1_t957 *, const MethodInfo*))Enumerator__ctor_m20091_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20092_gshared (Enumerator_t2685 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20092(__this, method) (( Object_t * (*) (Enumerator_t2685 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20092_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m20093_gshared (Enumerator_t2685 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20093(__this, method) (( void (*) (Enumerator_t2685 *, const MethodInfo*))Enumerator_Dispose_m20093_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m20094_gshared (Enumerator_t2685 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20094(__this, method) (( void (*) (Enumerator_t2685 *, const MethodInfo*))Enumerator_VerifyState_m20094_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20095_gshared (Enumerator_t2685 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20095(__this, method) (( bool (*) (Enumerator_t2685 *, const MethodInfo*))Enumerator_MoveNext_m20095_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t811  Enumerator_get_Current_m20096_gshared (Enumerator_t2685 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20096(__this, method) (( UICharInfo_t811  (*) (Enumerator_t2685 *, const MethodInfo*))Enumerator_get_Current_m20096_gshared)(__this, method)
