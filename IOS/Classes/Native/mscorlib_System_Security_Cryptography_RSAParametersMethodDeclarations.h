﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t1307;
struct RSAParameters_t1307_marshaled;

void RSAParameters_t1307_marshal(const RSAParameters_t1307& unmarshaled, RSAParameters_t1307_marshaled& marshaled);
void RSAParameters_t1307_marshal_back(const RSAParameters_t1307_marshaled& marshaled, RSAParameters_t1307& unmarshaled);
void RSAParameters_t1307_marshal_cleanup(RSAParameters_t1307_marshaled& marshaled);
