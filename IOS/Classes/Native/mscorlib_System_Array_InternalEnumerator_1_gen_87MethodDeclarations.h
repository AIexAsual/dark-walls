﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.SByte>
struct InternalEnumerator_1_t2911;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22699_gshared (InternalEnumerator_1_t2911 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22699(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2911 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22699_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22700_gshared (InternalEnumerator_1_t2911 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22700(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2911 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22700_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22701_gshared (InternalEnumerator_1_t2911 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22701(__this, method) (( void (*) (InternalEnumerator_1_t2911 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22701_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22702_gshared (InternalEnumerator_1_t2911 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22702(__this, method) (( bool (*) (InternalEnumerator_1_t2911 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22702_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m22703_gshared (InternalEnumerator_1_t2911 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22703(__this, method) (( int8_t (*) (InternalEnumerator_1_t2911 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22703_gshared)(__this, method)
