﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int64>
struct InternalEnumerator_1_t2708;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20439_gshared (InternalEnumerator_1_t2708 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20439(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2708 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20439_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20440_gshared (InternalEnumerator_1_t2708 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20440(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2708 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20440_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20441_gshared (InternalEnumerator_1_t2708 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20441(__this, method) (( void (*) (InternalEnumerator_1_t2708 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20441_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20442_gshared (InternalEnumerator_1_t2708 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20442(__this, method) (( bool (*) (InternalEnumerator_1_t2708 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20442_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m20443_gshared (InternalEnumerator_1_t2708 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20443(__this, method) (( int64_t (*) (InternalEnumerator_1_t2708 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20443_gshared)(__this, method)
