﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>
struct InternalEnumerator_1_t2593;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18721_gshared (InternalEnumerator_1_t2593 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18721(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2593 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18721_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18722_gshared (InternalEnumerator_1_t2593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18722(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2593 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18722_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18723_gshared (InternalEnumerator_1_t2593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18723(__this, method) (( void (*) (InternalEnumerator_1_t2593 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18723_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18724_gshared (InternalEnumerator_1_t2593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18724(__this, method) (( bool (*) (InternalEnumerator_1_t2593 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18724_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t809  InternalEnumerator_1_get_Current_m18725_gshared (InternalEnumerator_1_t2593 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18725(__this, method) (( UILineInfo_t809  (*) (InternalEnumerator_1_t2593 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18725_gshared)(__this, method)
