﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,iTweenPath>
struct Dictionary_2_t458;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iTweenPath>
struct  KeyCollection_t2485  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,iTweenPath>::dictionary
	Dictionary_2_t458 * ___dictionary_0;
};
