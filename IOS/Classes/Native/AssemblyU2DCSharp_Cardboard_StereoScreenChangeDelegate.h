﻿#pragma once
#include <stdint.h>
// UnityEngine.RenderTexture
struct RenderTexture_t15;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Cardboard/StereoScreenChangeDelegate
struct  StereoScreenChangeDelegate_t232  : public MulticastDelegate_t219
{
};
