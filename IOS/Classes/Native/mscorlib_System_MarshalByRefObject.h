﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1572;
// System.Object
#include "mscorlib_System_Object.h"
// System.MarshalByRefObject
struct  MarshalByRefObject_t1415  : public Object_t
{
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1572 * ____identity_0;
};
