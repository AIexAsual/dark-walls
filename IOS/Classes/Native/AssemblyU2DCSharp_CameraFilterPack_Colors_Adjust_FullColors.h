﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Colors_Adjust_FullColors
struct  CameraFilterPack_Colors_Adjust_FullColors_t70  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Colors_Adjust_FullColors::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Colors_Adjust_FullColors::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Colors_Adjust_FullColors::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_R
	float ___Red_R_6;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_G
	float ___Red_G_7;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_B
	float ___Red_B_8;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Red_Constant
	float ___Red_Constant_9;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_R
	float ___Green_R_10;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_G
	float ___Green_G_11;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_B
	float ___Green_B_12;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Green_Constant
	float ___Green_Constant_13;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_R
	float ___Blue_R_14;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_G
	float ___Blue_G_15;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_B
	float ___Blue_B_16;
	// System.Single CameraFilterPack_Colors_Adjust_FullColors::Blue_Constant
	float ___Blue_Constant_17;
};
