﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractivePlasticBag/<WaitAndWiggle>c__IteratorA
struct U3CWaitAndWiggleU3Ec__IteratorA_t336;
// System.Object
struct Object_t;

// System.Void InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::.ctor()
extern "C" void U3CWaitAndWiggleU3Ec__IteratorA__ctor_m1976 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndWiggleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1977 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndWiggleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1978 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::MoveNext()
extern "C" bool U3CWaitAndWiggleU3Ec__IteratorA_MoveNext_m1979 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::Dispose()
extern "C" void U3CWaitAndWiggleU3Ec__IteratorA_Dispose_m1980 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::Reset()
extern "C" void U3CWaitAndWiggleU3Ec__IteratorA_Reset_m1981 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
