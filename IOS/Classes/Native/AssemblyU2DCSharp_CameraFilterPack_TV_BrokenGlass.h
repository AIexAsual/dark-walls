﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.Texture2D
struct Texture2D_t9;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_TV_BrokenGlass
struct  CameraFilterPack_TV_BrokenGlass_t180  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_BrokenGlass::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_BrokenGlass::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_Small
	float ___Broken_Small_4;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_Medium
	float ___Broken_Medium_5;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_High
	float ___Broken_High_6;
	// System.Single CameraFilterPack_TV_BrokenGlass::Broken_Big
	float ___Broken_Big_7;
	// System.Single CameraFilterPack_TV_BrokenGlass::LightReflect
	float ___LightReflect_8;
	// UnityEngine.Material CameraFilterPack_TV_BrokenGlass::SCMaterial
	Material_t2 * ___SCMaterial_9;
	// UnityEngine.Texture2D CameraFilterPack_TV_BrokenGlass::Texture2
	Texture2D_t9 * ___Texture2_10;
};
