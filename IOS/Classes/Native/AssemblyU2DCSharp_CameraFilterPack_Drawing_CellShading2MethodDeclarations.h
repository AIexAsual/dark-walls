﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_CellShading2
struct CameraFilterPack_Drawing_CellShading2_t96;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_CellShading2::.ctor()
extern "C" void CameraFilterPack_Drawing_CellShading2__ctor_m609 (CameraFilterPack_Drawing_CellShading2_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_CellShading2::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_CellShading2_get_material_m610 (CameraFilterPack_Drawing_CellShading2_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::Start()
extern "C" void CameraFilterPack_Drawing_CellShading2_Start_m611 (CameraFilterPack_Drawing_CellShading2_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_CellShading2_OnRenderImage_m612 (CameraFilterPack_Drawing_CellShading2_t96 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::OnValidate()
extern "C" void CameraFilterPack_Drawing_CellShading2_OnValidate_m613 (CameraFilterPack_Drawing_CellShading2_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::Update()
extern "C" void CameraFilterPack_Drawing_CellShading2_Update_m614 (CameraFilterPack_Drawing_CellShading2_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading2::OnDisable()
extern "C" void CameraFilterPack_Drawing_CellShading2_OnDisable_m615 (CameraFilterPack_Drawing_CellShading2_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
