﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$8
struct U24ArrayTypeU248_t2162;
struct U24ArrayTypeU248_t2162_marshaled;

void U24ArrayTypeU248_t2162_marshal(const U24ArrayTypeU248_t2162& unmarshaled, U24ArrayTypeU248_t2162_marshaled& marshaled);
void U24ArrayTypeU248_t2162_marshal_back(const U24ArrayTypeU248_t2162_marshaled& marshaled, U24ArrayTypeU248_t2162& unmarshaled);
void U24ArrayTypeU248_t2162_marshal_cleanup(U24ArrayTypeU248_t2162_marshaled& marshaled);
