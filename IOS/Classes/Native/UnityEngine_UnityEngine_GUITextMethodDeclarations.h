﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIText
struct GUIText_t549;
// UnityEngine.Material
struct Material_t2;

// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C" Material_t2 * GUIText_get_material_m3302 (GUIText_t549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
