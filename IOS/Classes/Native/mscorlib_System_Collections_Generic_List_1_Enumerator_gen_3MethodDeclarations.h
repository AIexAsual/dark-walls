﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t2227;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t779;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m13586_gshared (Enumerator_t2227 * __this, List_1_t779 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m13586(__this, ___l, method) (( void (*) (Enumerator_t2227 *, List_1_t779 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared (Enumerator_t2227 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13587(__this, method) (( Object_t * (*) (Enumerator_t2227 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m13588_gshared (Enumerator_t2227 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13588(__this, method) (( void (*) (Enumerator_t2227 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m13589_gshared (Enumerator_t2227 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13589(__this, method) (( void (*) (Enumerator_t2227 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13590_gshared (Enumerator_t2227 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13590(__this, method) (( bool (*) (Enumerator_t2227 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m13591_gshared (Enumerator_t2227 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13591(__this, method) (( Object_t * (*) (Enumerator_t2227 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
