﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "UnityEngine_UI_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t587_il2cpp_TypeInfo;
// <Module>
#include "UnityEngine_UI_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType U3CModuleU3E_t587_0_0_0;
extern const Il2CppType U3CModuleU3E_t587_1_0_0;
struct U3CModuleU3E_t587;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t587_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t587_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t587_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t587_0_0_0/* byval_arg */
	, &U3CModuleU3E_t587_1_0_0/* this_arg */
	, &U3CModuleU3E_t587_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t587)/* instance_size */
	, sizeof (U3CModuleU3E_t587)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.EventHandle
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle.h"
// Metadata Definition UnityEngine.EventSystems.EventHandle
extern TypeInfo EventHandle_t588_il2cpp_TypeInfo;
// UnityEngine.EventSystems.EventHandle
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandleMethodDeclarations.h"
static const EncodedMethodIndex EventHandle_t588_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair EventHandle_t588_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType EventHandle_t588_0_0_0;
extern const Il2CppType EventHandle_t588_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata EventHandle_t588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventHandle_t588_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EventHandle_t588_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2936/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventHandle_t588_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventHandle"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 996/* custom_attributes_cache */
	, &EventHandle_t588_0_0_0/* byval_arg */
	, &EventHandle_t588_1_0_0/* this_arg */
	, &EventHandle_t588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventHandle_t588)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventHandle_t588)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IEventSystemHandler
extern TypeInfo IEventSystemHandler_t2261_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IEventSystemHandler_t2261_0_0_0;
extern const Il2CppType IEventSystemHandler_t2261_1_0_0;
struct IEventSystemHandler_t2261;
const Il2CppTypeDefinitionMetadata IEventSystemHandler_t2261_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEventSystemHandler_t2261_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEventSystemHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IEventSystemHandler_t2261_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEventSystemHandler_t2261_0_0_0/* byval_arg */
	, &IEventSystemHandler_t2261_1_0_0/* this_arg */
	, &IEventSystemHandler_t2261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IPointerEnterHandler
extern TypeInfo IPointerEnterHandler_t760_il2cpp_TypeInfo;
static const Il2CppType* IPointerEnterHandler_t760_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IPointerEnterHandler_t760_0_0_0;
extern const Il2CppType IPointerEnterHandler_t760_1_0_0;
struct IPointerEnterHandler_t760;
const Il2CppTypeDefinitionMetadata IPointerEnterHandler_t760_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IPointerEnterHandler_t760_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2741/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPointerEnterHandler_t760_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPointerEnterHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IPointerEnterHandler_t760_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IPointerEnterHandler_t760_0_0_0/* byval_arg */
	, &IPointerEnterHandler_t760_1_0_0/* this_arg */
	, &IPointerEnterHandler_t760_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IPointerExitHandler
extern TypeInfo IPointerExitHandler_t761_il2cpp_TypeInfo;
static const Il2CppType* IPointerExitHandler_t761_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IPointerExitHandler_t761_0_0_0;
extern const Il2CppType IPointerExitHandler_t761_1_0_0;
struct IPointerExitHandler_t761;
const Il2CppTypeDefinitionMetadata IPointerExitHandler_t761_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IPointerExitHandler_t761_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2742/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPointerExitHandler_t761_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPointerExitHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IPointerExitHandler_t761_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IPointerExitHandler_t761_0_0_0/* byval_arg */
	, &IPointerExitHandler_t761_1_0_0/* this_arg */
	, &IPointerExitHandler_t761_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IPointerDownHandler
extern TypeInfo IPointerDownHandler_t505_il2cpp_TypeInfo;
static const Il2CppType* IPointerDownHandler_t505_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IPointerDownHandler_t505_0_0_0;
extern const Il2CppType IPointerDownHandler_t505_1_0_0;
struct IPointerDownHandler_t505;
const Il2CppTypeDefinitionMetadata IPointerDownHandler_t505_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IPointerDownHandler_t505_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2743/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPointerDownHandler_t505_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPointerDownHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IPointerDownHandler_t505_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IPointerDownHandler_t505_0_0_0/* byval_arg */
	, &IPointerDownHandler_t505_1_0_0/* this_arg */
	, &IPointerDownHandler_t505_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IPointerUpHandler
extern TypeInfo IPointerUpHandler_t495_il2cpp_TypeInfo;
static const Il2CppType* IPointerUpHandler_t495_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IPointerUpHandler_t495_0_0_0;
extern const Il2CppType IPointerUpHandler_t495_1_0_0;
struct IPointerUpHandler_t495;
const Il2CppTypeDefinitionMetadata IPointerUpHandler_t495_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IPointerUpHandler_t495_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2744/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPointerUpHandler_t495_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPointerUpHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IPointerUpHandler_t495_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IPointerUpHandler_t495_0_0_0/* byval_arg */
	, &IPointerUpHandler_t495_1_0_0/* this_arg */
	, &IPointerUpHandler_t495_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IPointerClickHandler
extern TypeInfo IPointerClickHandler_t499_il2cpp_TypeInfo;
static const Il2CppType* IPointerClickHandler_t499_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IPointerClickHandler_t499_0_0_0;
extern const Il2CppType IPointerClickHandler_t499_1_0_0;
struct IPointerClickHandler_t499;
const Il2CppTypeDefinitionMetadata IPointerClickHandler_t499_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IPointerClickHandler_t499_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2745/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPointerClickHandler_t499_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPointerClickHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IPointerClickHandler_t499_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IPointerClickHandler_t499_0_0_0/* byval_arg */
	, &IPointerClickHandler_t499_1_0_0/* this_arg */
	, &IPointerClickHandler_t499_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IBeginDragHandler
extern TypeInfo IBeginDragHandler_t493_il2cpp_TypeInfo;
static const Il2CppType* IBeginDragHandler_t493_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IBeginDragHandler_t493_0_0_0;
extern const Il2CppType IBeginDragHandler_t493_1_0_0;
struct IBeginDragHandler_t493;
const Il2CppTypeDefinitionMetadata IBeginDragHandler_t493_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IBeginDragHandler_t493_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2746/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IBeginDragHandler_t493_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IBeginDragHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IBeginDragHandler_t493_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IBeginDragHandler_t493_0_0_0/* byval_arg */
	, &IBeginDragHandler_t493_1_0_0/* this_arg */
	, &IBeginDragHandler_t493_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IInitializePotentialDragHandler
extern TypeInfo IInitializePotentialDragHandler_t507_il2cpp_TypeInfo;
static const Il2CppType* IInitializePotentialDragHandler_t507_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IInitializePotentialDragHandler_t507_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t507_1_0_0;
struct IInitializePotentialDragHandler_t507;
const Il2CppTypeDefinitionMetadata IInitializePotentialDragHandler_t507_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IInitializePotentialDragHandler_t507_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2747/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IInitializePotentialDragHandler_t507_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IInitializePotentialDragHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IInitializePotentialDragHandler_t507_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IInitializePotentialDragHandler_t507_0_0_0/* byval_arg */
	, &IInitializePotentialDragHandler_t507_1_0_0/* this_arg */
	, &IInitializePotentialDragHandler_t507_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IDragHandler
extern TypeInfo IDragHandler_t497_il2cpp_TypeInfo;
static const Il2CppType* IDragHandler_t497_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IDragHandler_t497_0_0_0;
extern const Il2CppType IDragHandler_t497_1_0_0;
struct IDragHandler_t497;
const Il2CppTypeDefinitionMetadata IDragHandler_t497_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDragHandler_t497_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2748/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IDragHandler_t497_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDragHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IDragHandler_t497_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IDragHandler_t497_0_0_0/* byval_arg */
	, &IDragHandler_t497_1_0_0/* this_arg */
	, &IDragHandler_t497_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IEndDragHandler
extern TypeInfo IEndDragHandler_t503_il2cpp_TypeInfo;
static const Il2CppType* IEndDragHandler_t503_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IEndDragHandler_t503_0_0_0;
extern const Il2CppType IEndDragHandler_t503_1_0_0;
struct IEndDragHandler_t503;
const Il2CppTypeDefinitionMetadata IEndDragHandler_t503_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IEndDragHandler_t503_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2749/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEndDragHandler_t503_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEndDragHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IEndDragHandler_t503_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IEndDragHandler_t503_0_0_0/* byval_arg */
	, &IEndDragHandler_t503_1_0_0/* this_arg */
	, &IEndDragHandler_t503_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IDropHandler
extern TypeInfo IDropHandler_t501_il2cpp_TypeInfo;
static const Il2CppType* IDropHandler_t501_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IDropHandler_t501_0_0_0;
extern const Il2CppType IDropHandler_t501_1_0_0;
struct IDropHandler_t501;
const Il2CppTypeDefinitionMetadata IDropHandler_t501_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDropHandler_t501_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2750/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IDropHandler_t501_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDropHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IDropHandler_t501_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IDropHandler_t501_0_0_0/* byval_arg */
	, &IDropHandler_t501_1_0_0/* this_arg */
	, &IDropHandler_t501_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IScrollHandler
extern TypeInfo IScrollHandler_t762_il2cpp_TypeInfo;
static const Il2CppType* IScrollHandler_t762_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IScrollHandler_t762_0_0_0;
extern const Il2CppType IScrollHandler_t762_1_0_0;
struct IScrollHandler_t762;
const Il2CppTypeDefinitionMetadata IScrollHandler_t762_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IScrollHandler_t762_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2751/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IScrollHandler_t762_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScrollHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IScrollHandler_t762_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScrollHandler_t762_0_0_0/* byval_arg */
	, &IScrollHandler_t762_1_0_0/* this_arg */
	, &IScrollHandler_t762_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IUpdateSelectedHandler
extern TypeInfo IUpdateSelectedHandler_t489_il2cpp_TypeInfo;
static const Il2CppType* IUpdateSelectedHandler_t489_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IUpdateSelectedHandler_t489_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t489_1_0_0;
struct IUpdateSelectedHandler_t489;
const Il2CppTypeDefinitionMetadata IUpdateSelectedHandler_t489_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IUpdateSelectedHandler_t489_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2752/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IUpdateSelectedHandler_t489_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUpdateSelectedHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IUpdateSelectedHandler_t489_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUpdateSelectedHandler_t489_0_0_0/* byval_arg */
	, &IUpdateSelectedHandler_t489_1_0_0/* this_arg */
	, &IUpdateSelectedHandler_t489_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.ISelectHandler
extern TypeInfo ISelectHandler_t487_il2cpp_TypeInfo;
static const Il2CppType* ISelectHandler_t487_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ISelectHandler_t487_0_0_0;
extern const Il2CppType ISelectHandler_t487_1_0_0;
struct ISelectHandler_t487;
const Il2CppTypeDefinitionMetadata ISelectHandler_t487_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ISelectHandler_t487_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2753/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISelectHandler_t487_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISelectHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &ISelectHandler_t487_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISelectHandler_t487_0_0_0/* byval_arg */
	, &ISelectHandler_t487_1_0_0/* this_arg */
	, &ISelectHandler_t487_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IDeselectHandler
extern TypeInfo IDeselectHandler_t763_il2cpp_TypeInfo;
static const Il2CppType* IDeselectHandler_t763_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IDeselectHandler_t763_0_0_0;
extern const Il2CppType IDeselectHandler_t763_1_0_0;
struct IDeselectHandler_t763;
const Il2CppTypeDefinitionMetadata IDeselectHandler_t763_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDeselectHandler_t763_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2754/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IDeselectHandler_t763_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDeselectHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IDeselectHandler_t763_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IDeselectHandler_t763_0_0_0/* byval_arg */
	, &IDeselectHandler_t763_1_0_0/* this_arg */
	, &IDeselectHandler_t763_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.IMoveHandler
extern TypeInfo IMoveHandler_t764_il2cpp_TypeInfo;
static const Il2CppType* IMoveHandler_t764_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMoveHandler_t764_0_0_0;
extern const Il2CppType IMoveHandler_t764_1_0_0;
struct IMoveHandler_t764;
const Il2CppTypeDefinitionMetadata IMoveHandler_t764_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMoveHandler_t764_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2755/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMoveHandler_t764_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMoveHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &IMoveHandler_t764_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMoveHandler_t764_0_0_0/* byval_arg */
	, &IMoveHandler_t764_1_0_0/* this_arg */
	, &IMoveHandler_t764_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.ISubmitHandler
extern TypeInfo ISubmitHandler_t765_il2cpp_TypeInfo;
static const Il2CppType* ISubmitHandler_t765_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ISubmitHandler_t765_0_0_0;
extern const Il2CppType ISubmitHandler_t765_1_0_0;
struct ISubmitHandler_t765;
const Il2CppTypeDefinitionMetadata ISubmitHandler_t765_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ISubmitHandler_t765_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2756/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISubmitHandler_t765_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISubmitHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &ISubmitHandler_t765_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISubmitHandler_t765_0_0_0/* byval_arg */
	, &ISubmitHandler_t765_1_0_0/* this_arg */
	, &ISubmitHandler_t765_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.ICancelHandler
extern TypeInfo ICancelHandler_t766_il2cpp_TypeInfo;
static const Il2CppType* ICancelHandler_t766_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ICancelHandler_t766_0_0_0;
extern const Il2CppType ICancelHandler_t766_1_0_0;
struct ICancelHandler_t766;
const Il2CppTypeDefinitionMetadata ICancelHandler_t766_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ICancelHandler_t766_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2757/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICancelHandler_t766_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICancelHandler"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &ICancelHandler_t766_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICancelHandler_t766_0_0_0/* byval_arg */
	, &ICancelHandler_t766_1_0_0/* this_arg */
	, &ICancelHandler_t766_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
// Metadata Definition UnityEngine.EventSystems.EventSystem
extern TypeInfo EventSystem_t509_il2cpp_TypeInfo;
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystemMethodDeclarations.h"
static const EncodedMethodIndex EventSystem_t509_VTable[17] = 
{
	600,
	601,
	602,
	892,
	655,
	893,
	657,
	894,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	895,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType EventSystem_t509_0_0_0;
extern const Il2CppType EventSystem_t509_1_0_0;
extern const Il2CppType UIBehaviour_t591_0_0_0;
struct EventSystem_t509;
const Il2CppTypeDefinitionMetadata EventSystem_t509_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, EventSystem_t509_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2939/* fieldStart */
	, 2758/* methodStart */
	, -1/* eventStart */
	, 332/* propertyStart */

};
TypeInfo EventSystem_t509_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventSystem"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &EventSystem_t509_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 997/* custom_attributes_cache */
	, &EventSystem_t509_0_0_0/* byval_arg */
	, &EventSystem_t509_1_0_0/* this_arg */
	, &EventSystem_t509_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventSystem_t509)/* instance_size */
	, sizeof (EventSystem_t509)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EventSystem_t509_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 9/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.EventTrigger
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger.h"
// Metadata Definition UnityEngine.EventSystems.EventTrigger
extern TypeInfo EventTrigger_t596_il2cpp_TypeInfo;
// UnityEngine.EventSystems.EventTrigger
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTriggerMethodDeclarations.h"
extern const Il2CppType TriggerEvent_t592_0_0_0;
extern const Il2CppType Entry_t594_0_0_0;
static const Il2CppType* EventTrigger_t596_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TriggerEvent_t592_0_0_0,
	&Entry_t594_0_0_0,
};
static const EncodedMethodIndex EventTrigger_t596_VTable[38] = 
{
	600,
	601,
	602,
	603,
	896,
	897,
	898,
	899,
	900,
	901,
	902,
	903,
	904,
	905,
	906,
	907,
	908,
	909,
	910,
	911,
	912,
	896,
	897,
	903,
	905,
	898,
	899,
	900,
	908,
	909,
	906,
	910,
	907,
	902,
	901,
	904,
	911,
	912,
};
static const Il2CppType* EventTrigger_t596_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IPointerEnterHandler_t760_0_0_0,
	&IPointerExitHandler_t761_0_0_0,
	&IPointerDownHandler_t505_0_0_0,
	&IPointerUpHandler_t495_0_0_0,
	&IPointerClickHandler_t499_0_0_0,
	&IBeginDragHandler_t493_0_0_0,
	&IInitializePotentialDragHandler_t507_0_0_0,
	&IDragHandler_t497_0_0_0,
	&IEndDragHandler_t503_0_0_0,
	&IDropHandler_t501_0_0_0,
	&IScrollHandler_t762_0_0_0,
	&IUpdateSelectedHandler_t489_0_0_0,
	&ISelectHandler_t487_0_0_0,
	&IDeselectHandler_t763_0_0_0,
	&IMoveHandler_t764_0_0_0,
	&ISubmitHandler_t765_0_0_0,
	&ICancelHandler_t766_0_0_0,
};
static Il2CppInterfaceOffsetPair EventTrigger_t596_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 4},
	{ &IPointerEnterHandler_t760_0_0_0, 4},
	{ &IPointerExitHandler_t761_0_0_0, 5},
	{ &IPointerDownHandler_t505_0_0_0, 6},
	{ &IPointerUpHandler_t495_0_0_0, 7},
	{ &IPointerClickHandler_t499_0_0_0, 8},
	{ &IBeginDragHandler_t493_0_0_0, 9},
	{ &IInitializePotentialDragHandler_t507_0_0_0, 10},
	{ &IDragHandler_t497_0_0_0, 11},
	{ &IEndDragHandler_t503_0_0_0, 12},
	{ &IDropHandler_t501_0_0_0, 13},
	{ &IScrollHandler_t762_0_0_0, 14},
	{ &IUpdateSelectedHandler_t489_0_0_0, 15},
	{ &ISelectHandler_t487_0_0_0, 16},
	{ &IDeselectHandler_t763_0_0_0, 17},
	{ &IMoveHandler_t764_0_0_0, 18},
	{ &ISubmitHandler_t765_0_0_0, 19},
	{ &ICancelHandler_t766_0_0_0, 20},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType EventTrigger_t596_0_0_0;
extern const Il2CppType EventTrigger_t596_1_0_0;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
struct EventTrigger_t596;
const Il2CppTypeDefinitionMetadata EventTrigger_t596_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EventTrigger_t596_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, EventTrigger_t596_InterfacesTypeInfos/* implementedInterfaces */
	, EventTrigger_t596_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, EventTrigger_t596_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2949/* fieldStart */
	, 2786/* methodStart */
	, -1/* eventStart */
	, 341/* propertyStart */

};
TypeInfo EventTrigger_t596_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventTrigger"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &EventTrigger_t596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1005/* custom_attributes_cache */
	, &EventTrigger_t596_0_0_0/* byval_arg */
	, &EventTrigger_t596_1_0_0/* this_arg */
	, &EventTrigger_t596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventTrigger_t596)/* instance_size */
	, sizeof (EventTrigger_t596)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 38/* vtable_count */
	, 18/* interfaces_count */
	, 18/* interface_offsets_count */

};
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger_Trigger.h"
// Metadata Definition UnityEngine.EventSystems.EventTrigger/TriggerEvent
extern TypeInfo TriggerEvent_t592_il2cpp_TypeInfo;
// UnityEngine.EventSystems.EventTrigger/TriggerEvent
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger_TriggerMethodDeclarations.h"
static const EncodedMethodIndex TriggerEvent_t592_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484564,
	2147484565,
};
extern const Il2CppType ISerializationCallbackReceiver_t3399_0_0_0;
static Il2CppInterfaceOffsetPair TriggerEvent_t592_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType TriggerEvent_t592_1_0_0;
extern const Il2CppType UnityEvent_1_t593_0_0_0;
struct TriggerEvent_t592;
const Il2CppTypeDefinitionMetadata TriggerEvent_t592_DefinitionMetadata = 
{
	&EventTrigger_t596_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TriggerEvent_t592_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t593_0_0_0/* parent */
	, TriggerEvent_t592_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2807/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TriggerEvent_t592_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "TriggerEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TriggerEvent_t592_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TriggerEvent_t592_0_0_0/* byval_arg */
	, &TriggerEvent_t592_1_0_0/* this_arg */
	, &TriggerEvent_t592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TriggerEvent_t592)/* instance_size */
	, sizeof (TriggerEvent_t592)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.EventSystems.EventTrigger/Entry
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger_Entry.h"
// Metadata Definition UnityEngine.EventSystems.EventTrigger/Entry
extern TypeInfo Entry_t594_il2cpp_TypeInfo;
// UnityEngine.EventSystems.EventTrigger/Entry
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger_EntryMethodDeclarations.h"
static const EncodedMethodIndex Entry_t594_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Entry_t594_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Entry_t594;
const Il2CppTypeDefinitionMetadata Entry_t594_DefinitionMetadata = 
{
	&EventTrigger_t596_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Entry_t594_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2951/* fieldStart */
	, 2808/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Entry_t594_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Entry"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Entry_t594_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Entry_t594_0_0_0/* byval_arg */
	, &Entry_t594_1_0_0/* this_arg */
	, &Entry_t594_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Entry_t594)/* instance_size */
	, sizeof (Entry_t594)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.EventTriggerType
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTriggerType.h"
// Metadata Definition UnityEngine.EventSystems.EventTriggerType
extern TypeInfo EventTriggerType_t597_il2cpp_TypeInfo;
// UnityEngine.EventSystems.EventTriggerType
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTriggerTypeMethodDeclarations.h"
static const EncodedMethodIndex EventTriggerType_t597_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EventTriggerType_t597_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType EventTriggerType_t597_0_0_0;
extern const Il2CppType EventTriggerType_t597_1_0_0;
const Il2CppTypeDefinitionMetadata EventTriggerType_t597_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventTriggerType_t597_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EventTriggerType_t597_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2953/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventTriggerType_t597_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventTriggerType"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EventTriggerType_t597_0_0_0/* byval_arg */
	, &EventTriggerType_t597_1_0_0/* this_arg */
	, &EventTriggerType_t597_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventTriggerType_t597)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventTriggerType_t597)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventSystems.ExecuteEvents
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents.h"
// Metadata Definition UnityEngine.EventSystems.ExecuteEvents
extern TypeInfo ExecuteEvents_t488_il2cpp_TypeInfo;
// UnityEngine.EventSystems.ExecuteEvents
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEventsMethodDeclarations.h"
extern const Il2CppType ExecuteEvents_ValidateEventData_m24097_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ExecuteEvents_ValidateEventData_m24097_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3667 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3667 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ExecuteEvents_Execute_m24098_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ExecuteEvents_Execute_m24098_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5407 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3669 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, 3669 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5408 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition ExecuteEvents_ExecuteHierarchy_m24099_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5409 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ExecuteEvents_ShouldSendToComponent_m24100_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ExecuteEvents_ShouldSendToComponent_m24100_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6105 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition ExecuteEvents_GetEventList_m24101_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5410 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition ExecuteEvents_CanHandleEvent_m24102_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5411 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition ExecuteEvents_GetEventHandler_m24103_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5412 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType EventFunction_1_t3393_0_0_0;
static const Il2CppType* ExecuteEvents_t488_il2cpp_TypeInfo__nestedTypes[1] =
{
	&EventFunction_1_t3393_0_0_0,
};
static const EncodedMethodIndex ExecuteEvents_t488_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ExecuteEvents_t488_0_0_0;
extern const Il2CppType ExecuteEvents_t488_1_0_0;
struct ExecuteEvents_t488;
const Il2CppTypeDefinitionMetadata ExecuteEvents_t488_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ExecuteEvents_t488_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ExecuteEvents_t488_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2971/* fieldStart */
	, 2809/* methodStart */
	, -1/* eventStart */
	, 342/* propertyStart */

};
TypeInfo ExecuteEvents_t488_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecuteEvents"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &ExecuteEvents_t488_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecuteEvents_t488_0_0_0/* byval_arg */
	, &ExecuteEvents_t488_1_0_0/* this_arg */
	, &ExecuteEvents_t488_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecuteEvents_t488)/* instance_size */
	, sizeof (ExecuteEvents_t488)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ExecuteEvents_t488_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 44/* method_count */
	, 17/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1
extern TypeInfo EventFunction_1_t3393_il2cpp_TypeInfo;
static const EncodedMethodIndex EventFunction_1_t3393_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	918,
	919,
	920,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair EventFunction_1_t3393_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType EventFunction_1_t3393_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct EventFunction_1_t3393;
const Il2CppTypeDefinitionMetadata EventFunction_1_t3393_DefinitionMetadata = 
{
	&ExecuteEvents_t488_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventFunction_1_t3393_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, EventFunction_1_t3393_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2853/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventFunction_1_t3393_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventFunction`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EventFunction_1_t3393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EventFunction_1_t3393_0_0_0/* byval_arg */
	, &EventFunction_1_t3393_1_0_0/* this_arg */
	, &EventFunction_1_t3393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 12/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
// Metadata Definition UnityEngine.EventSystems.MoveDirection
extern TypeInfo MoveDirection_t608_il2cpp_TypeInfo;
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirectionMethodDeclarations.h"
static const EncodedMethodIndex MoveDirection_t608_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MoveDirection_t608_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MoveDirection_t608_0_0_0;
extern const Il2CppType MoveDirection_t608_1_0_0;
const Il2CppTypeDefinitionMetadata MoveDirection_t608_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MoveDirection_t608_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MoveDirection_t608_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2991/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MoveDirection_t608_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MoveDirection"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MoveDirection_t608_0_0_0/* byval_arg */
	, &MoveDirection_t608_1_0_0/* this_arg */
	, &MoveDirection_t608_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MoveDirection_t608)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MoveDirection_t608)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventSystems.RaycasterManager
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterManager.h"
// Metadata Definition UnityEngine.EventSystems.RaycasterManager
extern TypeInfo RaycasterManager_t610_il2cpp_TypeInfo;
// UnityEngine.EventSystems.RaycasterManager
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterManagerMethodDeclarations.h"
static const EncodedMethodIndex RaycasterManager_t610_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType RaycasterManager_t610_0_0_0;
extern const Il2CppType RaycasterManager_t610_1_0_0;
struct RaycasterManager_t610;
const Il2CppTypeDefinitionMetadata RaycasterManager_t610_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RaycasterManager_t610_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2997/* fieldStart */
	, 2857/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RaycasterManager_t610_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "RaycasterManager"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &RaycasterManager_t610_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RaycasterManager_t610_0_0_0/* byval_arg */
	, &RaycasterManager_t610_1_0_0/* this_arg */
	, &RaycasterManager_t610_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RaycasterManager_t610)/* instance_size */
	, sizeof (RaycasterManager_t610)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RaycasterManager_t610_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// Metadata Definition UnityEngine.EventSystems.RaycastResult
extern TypeInfo RaycastResult_t511_il2cpp_TypeInfo;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResultMethodDeclarations.h"
static const EncodedMethodIndex RaycastResult_t511_VTable[4] = 
{
	652,
	601,
	653,
	921,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType RaycastResult_t511_0_0_0;
extern const Il2CppType RaycastResult_t511_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata RaycastResult_t511_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RaycastResult_t511_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 2998/* fieldStart */
	, 2861/* methodStart */
	, -1/* eventStart */
	, 359/* propertyStart */

};
TypeInfo RaycastResult_t511_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "RaycastResult"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &RaycastResult_t511_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RaycastResult_t511_0_0_0/* byval_arg */
	, &RaycastResult_t511_1_0_0/* this_arg */
	, &RaycastResult_t511_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RaycastResult_t511)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RaycastResult_t511)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// Metadata Definition UnityEngine.EventSystems.UIBehaviour
extern TypeInfo UIBehaviour_t591_il2cpp_TypeInfo;
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviourMethodDeclarations.h"
static const EncodedMethodIndex UIBehaviour_t591_VTable[16] = 
{
	600,
	601,
	602,
	603,
	655,
	922,
	657,
	923,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType UIBehaviour_t591_1_0_0;
struct UIBehaviour_t591;
const Il2CppTypeDefinitionMetadata UIBehaviour_t591_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, UIBehaviour_t591_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 2866/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UIBehaviour_t591_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "UIBehaviour"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &UIBehaviour_t591_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UIBehaviour_t591_0_0_0/* byval_arg */
	, &UIBehaviour_t591_1_0_0/* this_arg */
	, &UIBehaviour_t591_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UIBehaviour_t591)/* instance_size */
	, sizeof (UIBehaviour_t591)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.AxisEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventData.h"
// Metadata Definition UnityEngine.EventSystems.AxisEventData
extern TypeInfo AxisEventData_t612_il2cpp_TypeInfo;
// UnityEngine.EventSystems.AxisEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventDataMethodDeclarations.h"
static const EncodedMethodIndex AxisEventData_t612_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AxisEventData_t612_0_0_0;
extern const Il2CppType AxisEventData_t612_1_0_0;
extern const Il2CppType BaseEventData_t490_0_0_0;
struct AxisEventData_t612;
const Il2CppTypeDefinitionMetadata AxisEventData_t612_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseEventData_t490_0_0_0/* parent */
	, AxisEventData_t612_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3007/* fieldStart */
	, 2880/* methodStart */
	, -1/* eventStart */
	, 361/* propertyStart */

};
TypeInfo AxisEventData_t612_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisEventData"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &AxisEventData_t612_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AxisEventData_t612_0_0_0/* byval_arg */
	, &AxisEventData_t612_1_0_0/* this_arg */
	, &AxisEventData_t612_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisEventData_t612)/* instance_size */
	, sizeof (AxisEventData_t612)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.BaseEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventData.h"
// Metadata Definition UnityEngine.EventSystems.BaseEventData
extern TypeInfo BaseEventData_t490_il2cpp_TypeInfo;
// UnityEngine.EventSystems.BaseEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventDataMethodDeclarations.h"
static const EncodedMethodIndex BaseEventData_t490_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseEventData_t490_1_0_0;
struct BaseEventData_t490;
const Il2CppTypeDefinitionMetadata BaseEventData_t490_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseEventData_t490_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3009/* fieldStart */
	, 2885/* methodStart */
	, -1/* eventStart */
	, 363/* propertyStart */

};
TypeInfo BaseEventData_t490_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseEventData"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &BaseEventData_t490_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseEventData_t490_0_0_0/* byval_arg */
	, &BaseEventData_t490_1_0_0/* this_arg */
	, &BaseEventData_t490_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseEventData_t490)/* instance_size */
	, sizeof (BaseEventData_t490)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
// Metadata Definition UnityEngine.EventSystems.PointerEventData
extern TypeInfo PointerEventData_t257_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"
extern const Il2CppType InputButton_t613_0_0_0;
extern const Il2CppType FramePressState_t614_0_0_0;
static const Il2CppType* PointerEventData_t257_il2cpp_TypeInfo__nestedTypes[2] =
{
	&InputButton_t613_0_0_0,
	&FramePressState_t614_0_0_0,
};
static const EncodedMethodIndex PointerEventData_t257_VTable[4] = 
{
	626,
	601,
	627,
	924,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PointerEventData_t257_0_0_0;
extern const Il2CppType PointerEventData_t257_1_0_0;
struct PointerEventData_t257;
const Il2CppTypeDefinitionMetadata PointerEventData_t257_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PointerEventData_t257_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseEventData_t490_0_0_0/* parent */
	, PointerEventData_t257_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3011/* fieldStart */
	, 2892/* methodStart */
	, -1/* eventStart */
	, 366/* propertyStart */

};
TypeInfo PointerEventData_t257_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PointerEventData"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &PointerEventData_t257_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PointerEventData_t257_0_0_0/* byval_arg */
	, &PointerEventData_t257_1_0_0/* this_arg */
	, &PointerEventData_t257_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PointerEventData_t257)/* instance_size */
	, sizeof (PointerEventData_t257)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 46/* method_count */
	, 22/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
// Metadata Definition UnityEngine.EventSystems.PointerEventData/InputButton
extern TypeInfo InputButton_t613_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_InpMethodDeclarations.h"
static const EncodedMethodIndex InputButton_t613_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair InputButton_t613_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType InputButton_t613_1_0_0;
const Il2CppTypeDefinitionMetadata InputButton_t613_DefinitionMetadata = 
{
	&PointerEventData_t257_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InputButton_t613_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, InputButton_t613_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3032/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InputButton_t613_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "InputButton"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InputButton_t613_0_0_0/* byval_arg */
	, &InputButton_t613_1_0_0/* this_arg */
	, &InputButton_t613_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InputButton_t613)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (InputButton_t613)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
// Metadata Definition UnityEngine.EventSystems.PointerEventData/FramePressState
extern TypeInfo FramePressState_t614_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_FraMethodDeclarations.h"
static const EncodedMethodIndex FramePressState_t614_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FramePressState_t614_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FramePressState_t614_1_0_0;
const Il2CppTypeDefinitionMetadata FramePressState_t614_DefinitionMetadata = 
{
	&PointerEventData_t257_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FramePressState_t614_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FramePressState_t614_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3036/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FramePressState_t614_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FramePressState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FramePressState_t614_0_0_0/* byval_arg */
	, &FramePressState_t614_1_0_0/* this_arg */
	, &FramePressState_t614_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FramePressState_t614)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FramePressState_t614)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventSystems.BaseInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputModule.h"
// Metadata Definition UnityEngine.EventSystems.BaseInputModule
extern TypeInfo BaseInputModule_t259_il2cpp_TypeInfo;
// UnityEngine.EventSystems.BaseInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputModuleMethodDeclarations.h"
static const EncodedMethodIndex BaseInputModule_t259_VTable[25] = 
{
	600,
	601,
	602,
	603,
	655,
	656,
	657,
	658,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	0,
	668,
	669,
	925,
	926,
	927,
	673,
	674,
	675,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseInputModule_t259_0_0_0;
extern const Il2CppType BaseInputModule_t259_1_0_0;
struct BaseInputModule_t259;
const Il2CppTypeDefinitionMetadata BaseInputModule_t259_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, BaseInputModule_t259_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3041/* fieldStart */
	, 2938/* methodStart */
	, -1/* eventStart */
	, 388/* propertyStart */

};
TypeInfo BaseInputModule_t259_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInputModule"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &BaseInputModule_t259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1075/* custom_attributes_cache */
	, &BaseInputModule_t259_0_0_0/* byval_arg */
	, &BaseInputModule_t259_1_0_0/* this_arg */
	, &BaseInputModule_t259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInputModule_t259)/* instance_size */
	, sizeof (BaseInputModule_t259)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.PointerInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule.h"
// Metadata Definition UnityEngine.EventSystems.PointerInputModule
extern TypeInfo PointerInputModule_t620_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PointerInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModuleMethodDeclarations.h"
extern const Il2CppType ButtonState_t616_0_0_0;
extern const Il2CppType MouseState_t618_0_0_0;
extern const Il2CppType MouseButtonEventData_t615_0_0_0;
static const Il2CppType* PointerInputModule_t620_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ButtonState_t616_0_0_0,
	&MouseState_t618_0_0_0,
	&MouseButtonEventData_t615_0_0_0,
};
static const EncodedMethodIndex PointerInputModule_t620_VTable[28] = 
{
	600,
	601,
	602,
	928,
	655,
	656,
	657,
	658,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	0,
	668,
	669,
	929,
	926,
	927,
	673,
	674,
	675,
	930,
	931,
	932,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PointerInputModule_t620_0_0_0;
extern const Il2CppType PointerInputModule_t620_1_0_0;
struct PointerInputModule_t620;
const Il2CppTypeDefinitionMetadata PointerInputModule_t620_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PointerInputModule_t620_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInputModule_t259_0_0_0/* parent */
	, PointerInputModule_t620_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3045/* fieldStart */
	, 2956/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PointerInputModule_t620_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PointerInputModule"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &PointerInputModule_t620_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PointerInputModule_t620_0_0_0/* byval_arg */
	, &PointerInputModule_t620_1_0_0/* this_arg */
	, &PointerInputModule_t620_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PointerInputModule_t620)/* instance_size */
	, sizeof (PointerInputModule_t620)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.PointerInputModule/ButtonState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_B.h"
// Metadata Definition UnityEngine.EventSystems.PointerInputModule/ButtonState
extern TypeInfo ButtonState_t616_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PointerInputModule/ButtonState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_BMethodDeclarations.h"
static const EncodedMethodIndex ButtonState_t616_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ButtonState_t616_1_0_0;
struct ButtonState_t616;
const Il2CppTypeDefinitionMetadata ButtonState_t616_DefinitionMetadata = 
{
	&PointerInputModule_t620_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ButtonState_t616_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3051/* fieldStart */
	, 2971/* methodStart */
	, -1/* eventStart */
	, 389/* propertyStart */

};
TypeInfo ButtonState_t616_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ButtonState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ButtonState_t616_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ButtonState_t616_0_0_0/* byval_arg */
	, &ButtonState_t616_1_0_0/* this_arg */
	, &ButtonState_t616_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ButtonState_t616)/* instance_size */
	, sizeof (ButtonState_t616)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048580/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.PointerInputModule/MouseState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_M.h"
// Metadata Definition UnityEngine.EventSystems.PointerInputModule/MouseState
extern TypeInfo MouseState_t618_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PointerInputModule/MouseState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_MMethodDeclarations.h"
static const EncodedMethodIndex MouseState_t618_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MouseState_t618_1_0_0;
struct MouseState_t618;
const Il2CppTypeDefinitionMetadata MouseState_t618_DefinitionMetadata = 
{
	&PointerInputModule_t620_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MouseState_t618_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3053/* fieldStart */
	, 2976/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MouseState_t618_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MouseState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MouseState_t618_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MouseState_t618_0_0_0/* byval_arg */
	, &MouseState_t618_1_0_0/* this_arg */
	, &MouseState_t618_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MouseState_t618)/* instance_size */
	, sizeof (MouseState_t618)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048580/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_M_0.h"
// Metadata Definition UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
extern TypeInfo MouseButtonEventData_t615_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule_M_0MethodDeclarations.h"
static const EncodedMethodIndex MouseButtonEventData_t615_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MouseButtonEventData_t615_1_0_0;
struct MouseButtonEventData_t615;
const Il2CppTypeDefinitionMetadata MouseButtonEventData_t615_DefinitionMetadata = 
{
	&PointerInputModule_t620_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MouseButtonEventData_t615_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3054/* fieldStart */
	, 2981/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MouseButtonEventData_t615_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MouseButtonEventData"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MouseButtonEventData_t615_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MouseButtonEventData_t615_0_0_0/* byval_arg */
	, &MouseButtonEventData_t615_1_0_0/* this_arg */
	, &MouseButtonEventData_t615_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MouseButtonEventData_t615)/* instance_size */
	, sizeof (MouseButtonEventData_t615)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.StandaloneInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul_0.h"
// Metadata Definition UnityEngine.EventSystems.StandaloneInputModule
extern TypeInfo StandaloneInputModule_t622_il2cpp_TypeInfo;
// UnityEngine.EventSystems.StandaloneInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul_0MethodDeclarations.h"
extern const Il2CppType InputMode_t621_0_0_0;
static const Il2CppType* StandaloneInputModule_t622_il2cpp_TypeInfo__nestedTypes[1] =
{
	&InputMode_t621_0_0_0,
};
static const EncodedMethodIndex StandaloneInputModule_t622_VTable[28] = 
{
	600,
	601,
	602,
	928,
	655,
	656,
	657,
	658,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	933,
	668,
	669,
	929,
	934,
	935,
	936,
	937,
	938,
	930,
	931,
	932,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType StandaloneInputModule_t622_0_0_0;
extern const Il2CppType StandaloneInputModule_t622_1_0_0;
struct StandaloneInputModule_t622;
const Il2CppTypeDefinitionMetadata StandaloneInputModule_t622_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StandaloneInputModule_t622_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PointerInputModule_t620_0_0_0/* parent */
	, StandaloneInputModule_t622_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3056/* fieldStart */
	, 2984/* methodStart */
	, -1/* eventStart */
	, 391/* propertyStart */

};
TypeInfo StandaloneInputModule_t622_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "StandaloneInputModule"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &StandaloneInputModule_t622_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1076/* custom_attributes_cache */
	, &StandaloneInputModule_t622_0_0_0/* byval_arg */
	, &StandaloneInputModule_t622_1_0_0/* this_arg */
	, &StandaloneInputModule_t622_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StandaloneInputModule_t622)/* instance_size */
	, sizeof (StandaloneInputModule_t622)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 8/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
// Metadata Definition UnityEngine.EventSystems.StandaloneInputModule/InputMode
extern TypeInfo InputMode_t621_il2cpp_TypeInfo;
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModulMethodDeclarations.h"
static const EncodedMethodIndex InputMode_t621_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair InputMode_t621_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType InputMode_t621_1_0_0;
const Il2CppTypeDefinitionMetadata InputMode_t621_DefinitionMetadata = 
{
	&StandaloneInputModule_t622_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InputMode_t621_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, InputMode_t621_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3068/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InputMode_t621_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "InputMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1085/* custom_attributes_cache */
	, &InputMode_t621_0_0_0/* byval_arg */
	, &InputMode_t621_1_0_0/* this_arg */
	, &InputMode_t621_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InputMode_t621)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (InputMode_t621)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.EventSystems.TouchInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputModule.h"
// Metadata Definition UnityEngine.EventSystems.TouchInputModule
extern TypeInfo TouchInputModule_t623_il2cpp_TypeInfo;
// UnityEngine.EventSystems.TouchInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputModuleMethodDeclarations.h"
static const EncodedMethodIndex TouchInputModule_t623_VTable[28] = 
{
	600,
	601,
	602,
	939,
	655,
	656,
	657,
	658,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	940,
	668,
	669,
	929,
	941,
	942,
	673,
	943,
	944,
	930,
	931,
	932,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType TouchInputModule_t623_0_0_0;
extern const Il2CppType TouchInputModule_t623_1_0_0;
struct TouchInputModule_t623;
const Il2CppTypeDefinitionMetadata TouchInputModule_t623_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PointerInputModule_t620_0_0_0/* parent */
	, TouchInputModule_t623_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3071/* fieldStart */
	, 3012/* methodStart */
	, -1/* eventStart */
	, 399/* propertyStart */

};
TypeInfo TouchInputModule_t623_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchInputModule"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &TouchInputModule_t623_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1086/* custom_attributes_cache */
	, &TouchInputModule_t623_0_0_0/* byval_arg */
	, &TouchInputModule_t623_1_0_0/* this_arg */
	, &TouchInputModule_t623_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TouchInputModule_t623)/* instance_size */
	, sizeof (TouchInputModule_t623)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.BaseRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycaster.h"
// Metadata Definition UnityEngine.EventSystems.BaseRaycaster
extern TypeInfo BaseRaycaster_t611_il2cpp_TypeInfo;
// UnityEngine.EventSystems.BaseRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycasterMethodDeclarations.h"
static const EncodedMethodIndex BaseRaycaster_t611_VTable[21] = 
{
	600,
	601,
	602,
	603,
	655,
	945,
	657,
	946,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	0,
	0,
	947,
	948,
	949,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseRaycaster_t611_0_0_0;
extern const Il2CppType BaseRaycaster_t611_1_0_0;
struct BaseRaycaster_t611;
const Il2CppTypeDefinitionMetadata BaseRaycaster_t611_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, BaseRaycaster_t611_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3025/* methodStart */
	, -1/* eventStart */
	, 400/* propertyStart */

};
TypeInfo BaseRaycaster_t611_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseRaycaster"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &BaseRaycaster_t611_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseRaycaster_t611_0_0_0/* byval_arg */
	, &BaseRaycaster_t611_1_0_0/* this_arg */
	, &BaseRaycaster_t611_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseRaycaster_t611)/* instance_size */
	, sizeof (BaseRaycaster_t611)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 21/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.Physics2DRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DRaycaster.h"
// Metadata Definition UnityEngine.EventSystems.Physics2DRaycaster
extern TypeInfo Physics2DRaycaster_t624_il2cpp_TypeInfo;
// UnityEngine.EventSystems.Physics2DRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DRaycasterMethodDeclarations.h"
static const EncodedMethodIndex Physics2DRaycaster_t624_VTable[22] = 
{
	600,
	601,
	602,
	603,
	655,
	945,
	657,
	946,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	950,
	951,
	947,
	948,
	949,
	952,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Physics2DRaycaster_t624_0_0_0;
extern const Il2CppType Physics2DRaycaster_t624_1_0_0;
extern const Il2CppType PhysicsRaycaster_t625_0_0_0;
struct Physics2DRaycaster_t624;
const Il2CppTypeDefinitionMetadata Physics2DRaycaster_t624_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PhysicsRaycaster_t625_0_0_0/* parent */
	, Physics2DRaycaster_t624_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3033/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Physics2DRaycaster_t624_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Physics2DRaycaster"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &Physics2DRaycaster_t624_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1089/* custom_attributes_cache */
	, &Physics2DRaycaster_t624_0_0_0/* byval_arg */
	, &Physics2DRaycaster_t624_1_0_0/* this_arg */
	, &Physics2DRaycaster_t624_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Physics2DRaycaster_t624)/* instance_size */
	, sizeof (Physics2DRaycaster_t624)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 22/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.EventSystems.PhysicsRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRaycaster.h"
// Metadata Definition UnityEngine.EventSystems.PhysicsRaycaster
extern TypeInfo PhysicsRaycaster_t625_il2cpp_TypeInfo;
// UnityEngine.EventSystems.PhysicsRaycaster
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRaycasterMethodDeclarations.h"
static const EncodedMethodIndex PhysicsRaycaster_t625_VTable[22] = 
{
	600,
	601,
	602,
	603,
	655,
	945,
	657,
	946,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	953,
	951,
	947,
	948,
	949,
	952,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PhysicsRaycaster_t625_1_0_0;
struct PhysicsRaycaster_t625;
const Il2CppTypeDefinitionMetadata PhysicsRaycaster_t625_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseRaycaster_t611_0_0_0/* parent */
	, PhysicsRaycaster_t625_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3074/* fieldStart */
	, 3035/* methodStart */
	, -1/* eventStart */
	, 404/* propertyStart */

};
TypeInfo PhysicsRaycaster_t625_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PhysicsRaycaster"/* name */
	, "UnityEngine.EventSystems"/* namespaze */
	, NULL/* methods */
	, &PhysicsRaycaster_t625_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1090/* custom_attributes_cache */
	, &PhysicsRaycaster_t625_0_0_0/* byval_arg */
	, &PhysicsRaycaster_t625_1_0_0/* this_arg */
	, &PhysicsRaycaster_t625_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PhysicsRaycaster_t625)/* instance_size */
	, sizeof (PhysicsRaycaster_t625)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PhysicsRaycaster_t625_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 22/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.CoroutineTween.ITweenValue
extern TypeInfo ITweenValue_t3394_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ITweenValue_t3394_0_0_0;
extern const Il2CppType ITweenValue_t3394_1_0_0;
struct ITweenValue_t3394;
const Il2CppTypeDefinitionMetadata ITweenValue_t3394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3043/* methodStart */
	, -1/* eventStart */
	, 408/* propertyStart */

};
TypeInfo ITweenValue_t3394_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITweenValue"/* name */
	, "UnityEngine.UI.CoroutineTween"/* namespaze */
	, NULL/* methods */
	, &ITweenValue_t3394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ITweenValue_t3394_0_0_0/* byval_arg */
	, &ITweenValue_t3394_1_0_0/* this_arg */
	, &ITweenValue_t3394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
// Metadata Definition UnityEngine.UI.CoroutineTween.ColorTween
extern TypeInfo ColorTween_t630_il2cpp_TypeInfo;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTweenMethodDeclarations.h"
extern const Il2CppType ColorTweenMode_t627_0_0_0;
extern const Il2CppType ColorTweenCallback_t628_0_0_0;
static const Il2CppType* ColorTween_t630_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ColorTweenMode_t627_0_0_0,
	&ColorTweenCallback_t628_0_0_0,
};
static const EncodedMethodIndex ColorTween_t630_VTable[8] = 
{
	652,
	601,
	653,
	654,
	954,
	955,
	956,
	957,
};
static const Il2CppType* ColorTween_t630_InterfacesTypeInfos[] = 
{
	&ITweenValue_t3394_0_0_0,
};
static Il2CppInterfaceOffsetPair ColorTween_t630_InterfacesOffsets[] = 
{
	{ &ITweenValue_t3394_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ColorTween_t630_0_0_0;
extern const Il2CppType ColorTween_t630_1_0_0;
const Il2CppTypeDefinitionMetadata ColorTween_t630_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ColorTween_t630_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ColorTween_t630_InterfacesTypeInfos/* implementedInterfaces */
	, ColorTween_t630_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, ColorTween_t630_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3078/* fieldStart */
	, 3047/* methodStart */
	, -1/* eventStart */
	, 410/* propertyStart */

};
TypeInfo ColorTween_t630_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorTween"/* name */
	, "UnityEngine.UI.CoroutineTween"/* namespaze */
	, NULL/* methods */
	, &ColorTween_t630_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorTween_t630_0_0_0/* byval_arg */
	, &ColorTween_t630_1_0_0/* this_arg */
	, &ColorTween_t630_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorTween_t630)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorTween_t630)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 5/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
// Metadata Definition UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
extern TypeInfo ColorTweenMode_t627_il2cpp_TypeInfo;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_ColoMethodDeclarations.h"
static const EncodedMethodIndex ColorTweenMode_t627_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ColorTweenMode_t627_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ColorTweenMode_t627_1_0_0;
const Il2CppTypeDefinitionMetadata ColorTweenMode_t627_DefinitionMetadata = 
{
	&ColorTween_t630_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ColorTweenMode_t627_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ColorTweenMode_t627_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3084/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ColorTweenMode_t627_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorTweenMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorTweenMode_t627_0_0_0/* byval_arg */
	, &ColorTweenMode_t627_1_0_0/* this_arg */
	, &ColorTweenMode_t627_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorTweenMode_t627)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorTweenMode_t627)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo_0.h"
// Metadata Definition UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
extern TypeInfo ColorTweenCallback_t628_il2cpp_TypeInfo;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo_0MethodDeclarations.h"
static const EncodedMethodIndex ColorTweenCallback_t628_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484606,
	2147484607,
};
static Il2CppInterfaceOffsetPair ColorTweenCallback_t628_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ColorTweenCallback_t628_1_0_0;
extern const Il2CppType UnityEvent_1_t629_0_0_0;
struct ColorTweenCallback_t628;
const Il2CppTypeDefinitionMetadata ColorTweenCallback_t628_DefinitionMetadata = 
{
	&ColorTween_t630_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ColorTweenCallback_t628_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t629_0_0_0/* parent */
	, ColorTweenCallback_t628_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3062/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ColorTweenCallback_t628_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorTweenCallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ColorTweenCallback_t628_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorTweenCallback_t628_0_0_0/* byval_arg */
	, &ColorTweenCallback_t628_1_0_0/* this_arg */
	, &ColorTweenCallback_t628_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorTweenCallback_t628)/* instance_size */
	, sizeof (ColorTweenCallback_t628)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.CoroutineTween.TweenRunner`1
extern TypeInfo TweenRunner_1_t3395_il2cpp_TypeInfo;
extern const Il2CppType U3CStartU3Ec__Iterator0_t3396_0_0_0;
static const Il2CppType* TweenRunner_1_t3395_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U3CStartU3Ec__Iterator0_t3396_0_0_0,
};
static const EncodedMethodIndex TweenRunner_1_t3395_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern const Il2CppType U3CStartU3Ec__Iterator0_t3619_0_0_0;
extern const Il2CppType TweenRunner_1_t3395_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t3621_0_0_0;
extern const Il2CppRGCTXDefinition TweenRunner_1_t3395_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6141 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5413 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3730 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5414 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6142 }/* Static */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType TweenRunner_1_t3395_0_0_0;
extern const Il2CppType TweenRunner_1_t3395_1_0_0;
struct TweenRunner_1_t3395;
const Il2CppTypeDefinitionMetadata TweenRunner_1_t3395_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TweenRunner_1_t3395_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TweenRunner_1_t3395_VTable/* vtableMethods */
	, TweenRunner_1_t3395_RGCTXData/* rgctxDefinition */
	, 3088/* fieldStart */
	, 3063/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TweenRunner_1_t3395_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenRunner`1"/* name */
	, "UnityEngine.UI.CoroutineTween"/* namespaze */
	, NULL/* methods */
	, &TweenRunner_1_t3395_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenRunner_1_t3395_0_0_0/* byval_arg */
	, &TweenRunner_1_t3395_1_0_0/* this_arg */
	, &TweenRunner_1_t3395_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 13/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0
extern TypeInfo U3CStartU3Ec__Iterator0_t3396_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CStartU3Ec__Iterator0_t3396_VTable[9] = 
{
	626,
	601,
	627,
	628,
	960,
	961,
	962,
	963,
	964,
};
extern const Il2CppType IDisposable_t538_0_0_0;
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IEnumerator_1_t2280_0_0_0;
static const Il2CppType* U3CStartU3Ec__Iterator0_t3396_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CStartU3Ec__Iterator0_t3396_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &IEnumerator_t464_0_0_0, 5},
	{ &IEnumerator_1_t2280_0_0_0, 7},
};
extern const Il2CppType U3CStartU3Ec__Iterator0_t3396_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition U3CStartU3Ec__Iterator0_t3396_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6145 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType U3CStartU3Ec__Iterator0_t3396_1_0_0;
struct U3CStartU3Ec__Iterator0_t3396;
const Il2CppTypeDefinitionMetadata U3CStartU3Ec__Iterator0_t3396_DefinitionMetadata = 
{
	&TweenRunner_1_t3395_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CStartU3Ec__Iterator0_t3396_InterfacesTypeInfos/* implementedInterfaces */
	, U3CStartU3Ec__Iterator0_t3396_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CStartU3Ec__Iterator0_t3396_VTable/* vtableMethods */
	, U3CStartU3Ec__Iterator0_t3396_RGCTXData/* rgctxDefinition */
	, 3090/* fieldStart */
	, 3067/* methodStart */
	, -1/* eventStart */
	, 415/* propertyStart */

};
TypeInfo U3CStartU3Ec__Iterator0_t3396_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Start>c__Iterator0"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CStartU3Ec__Iterator0_t3396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1095/* custom_attributes_cache */
	, &U3CStartU3Ec__Iterator0_t3396_0_0_0/* byval_arg */
	, &U3CStartU3Ec__Iterator0_t3396_1_0_0/* this_arg */
	, &U3CStartU3Ec__Iterator0_t3396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 14/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.AnimationTriggers
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers.h"
// Metadata Definition UnityEngine.UI.AnimationTriggers
extern TypeInfo AnimationTriggers_t631_il2cpp_TypeInfo;
// UnityEngine.UI.AnimationTriggers
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggersMethodDeclarations.h"
static const EncodedMethodIndex AnimationTriggers_t631_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AnimationTriggers_t631_0_0_0;
extern const Il2CppType AnimationTriggers_t631_1_0_0;
struct AnimationTriggers_t631;
const Il2CppTypeDefinitionMetadata AnimationTriggers_t631_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AnimationTriggers_t631_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3096/* fieldStart */
	, 3073/* methodStart */
	, -1/* eventStart */
	, 417/* propertyStart */

};
TypeInfo AnimationTriggers_t631_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimationTriggers"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &AnimationTriggers_t631_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AnimationTriggers_t631_0_0_0/* byval_arg */
	, &AnimationTriggers_t631_1_0_0/* this_arg */
	, &AnimationTriggers_t631_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimationTriggers_t631)/* instance_size */
	, sizeof (AnimationTriggers_t631)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 4/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Button
#include "UnityEngine_UI_UnityEngine_UI_Button.h"
// Metadata Definition UnityEngine.UI.Button
extern TypeInfo Button_t634_il2cpp_TypeInfo;
// UnityEngine.UI.Button
#include "UnityEngine_UI_UnityEngine_UI_ButtonMethodDeclarations.h"
extern const Il2CppType ButtonClickedEvent_t632_0_0_0;
extern const Il2CppType U3COnFinishSubmitU3Ec__Iterator1_t635_0_0_0;
static const Il2CppType* Button_t634_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ButtonClickedEvent_t632_0_0_0,
	&U3COnFinishSubmitU3Ec__Iterator1_t635_0_0_0,
};
static const EncodedMethodIndex Button_t634_VTable[42] = 
{
	600,
	601,
	602,
	603,
	965,
	966,
	657,
	967,
	659,
	660,
	661,
	662,
	663,
	968,
	969,
	666,
	970,
	971,
	972,
	973,
	974,
	975,
	976,
	977,
	978,
	979,
	980,
	981,
	982,
	983,
	976,
	972,
	973,
	970,
	971,
	974,
	975,
	984,
	985,
	986,
	985,
	986,
};
static const Il2CppType* Button_t634_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IPointerClickHandler_t499_0_0_0,
	&ISubmitHandler_t765_0_0_0,
};
static Il2CppInterfaceOffsetPair Button_t634_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 16},
	{ &IPointerEnterHandler_t760_0_0_0, 16},
	{ &IPointerExitHandler_t761_0_0_0, 17},
	{ &IPointerDownHandler_t505_0_0_0, 18},
	{ &IPointerUpHandler_t495_0_0_0, 19},
	{ &ISelectHandler_t487_0_0_0, 20},
	{ &IDeselectHandler_t763_0_0_0, 21},
	{ &IMoveHandler_t764_0_0_0, 22},
	{ &IPointerClickHandler_t499_0_0_0, 38},
	{ &ISubmitHandler_t765_0_0_0, 39},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Button_t634_0_0_0;
extern const Il2CppType Button_t634_1_0_0;
extern const Il2CppType Selectable_t636_0_0_0;
struct Button_t634;
const Il2CppTypeDefinitionMetadata Button_t634_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Button_t634_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Button_t634_InterfacesTypeInfos/* implementedInterfaces */
	, Button_t634_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t636_0_0_0/* parent */
	, Button_t634_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3104/* fieldStart */
	, 3082/* methodStart */
	, -1/* eventStart */
	, 421/* propertyStart */

};
TypeInfo Button_t634_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Button"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Button_t634_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1104/* custom_attributes_cache */
	, &Button_t634_0_0_0/* byval_arg */
	, &Button_t634_1_0_0/* this_arg */
	, &Button_t634_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Button_t634)/* instance_size */
	, sizeof (Button_t634)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 42/* vtable_count */
	, 3/* interfaces_count */
	, 10/* interface_offsets_count */

};
// UnityEngine.UI.Button/ButtonClickedEvent
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedEvent.h"
// Metadata Definition UnityEngine.UI.Button/ButtonClickedEvent
extern TypeInfo ButtonClickedEvent_t632_il2cpp_TypeInfo;
// UnityEngine.UI.Button/ButtonClickedEvent
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedEventMethodDeclarations.h"
static const EncodedMethodIndex ButtonClickedEvent_t632_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	987,
	988,
};
static Il2CppInterfaceOffsetPair ButtonClickedEvent_t632_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ButtonClickedEvent_t632_1_0_0;
extern const Il2CppType UnityEvent_t633_0_0_0;
struct ButtonClickedEvent_t632;
const Il2CppTypeDefinitionMetadata ButtonClickedEvent_t632_DefinitionMetadata = 
{
	&Button_t634_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ButtonClickedEvent_t632_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_t633_0_0_0/* parent */
	, ButtonClickedEvent_t632_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3089/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ButtonClickedEvent_t632_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ButtonClickedEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ButtonClickedEvent_t632_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ButtonClickedEvent_t632_0_0_0/* byval_arg */
	, &ButtonClickedEvent_t632_1_0_0/* this_arg */
	, &ButtonClickedEvent_t632_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ButtonClickedEvent_t632)/* instance_size */
	, sizeof (ButtonClickedEvent_t632)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSubmitU3Ec__.h"
// Metadata Definition UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1
extern TypeInfo U3COnFinishSubmitU3Ec__Iterator1_t635_il2cpp_TypeInfo;
// UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSubmitU3Ec__MethodDeclarations.h"
static const EncodedMethodIndex U3COnFinishSubmitU3Ec__Iterator1_t635_VTable[9] = 
{
	626,
	601,
	627,
	628,
	989,
	990,
	991,
	992,
	993,
};
static const Il2CppType* U3COnFinishSubmitU3Ec__Iterator1_t635_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
};
static Il2CppInterfaceOffsetPair U3COnFinishSubmitU3Ec__Iterator1_t635_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &IEnumerator_t464_0_0_0, 5},
	{ &IEnumerator_1_t2280_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType U3COnFinishSubmitU3Ec__Iterator1_t635_1_0_0;
struct U3COnFinishSubmitU3Ec__Iterator1_t635;
const Il2CppTypeDefinitionMetadata U3COnFinishSubmitU3Ec__Iterator1_t635_DefinitionMetadata = 
{
	&Button_t634_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3COnFinishSubmitU3Ec__Iterator1_t635_InterfacesTypeInfos/* implementedInterfaces */
	, U3COnFinishSubmitU3Ec__Iterator1_t635_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3COnFinishSubmitU3Ec__Iterator1_t635_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3105/* fieldStart */
	, 3090/* methodStart */
	, -1/* eventStart */
	, 422/* propertyStart */

};
TypeInfo U3COnFinishSubmitU3Ec__Iterator1_t635_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "<OnFinishSubmit>c__Iterator1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3COnFinishSubmitU3Ec__Iterator1_t635_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1107/* custom_attributes_cache */
	, &U3COnFinishSubmitU3Ec__Iterator1_t635_0_0_0/* byval_arg */
	, &U3COnFinishSubmitU3Ec__Iterator1_t635_1_0_0/* this_arg */
	, &U3COnFinishSubmitU3Ec__Iterator1_t635_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3COnFinishSubmitU3Ec__Iterator1_t635)/* instance_size */
	, sizeof (U3COnFinishSubmitU3Ec__Iterator1_t635)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"
// Metadata Definition UnityEngine.UI.CanvasUpdate
extern TypeInfo CanvasUpdate_t637_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateMethodDeclarations.h"
static const EncodedMethodIndex CanvasUpdate_t637_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CanvasUpdate_t637_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasUpdate_t637_0_0_0;
extern const Il2CppType CanvasUpdate_t637_1_0_0;
const Il2CppTypeDefinitionMetadata CanvasUpdate_t637_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CanvasUpdate_t637_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CanvasUpdate_t637_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3110/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CanvasUpdate_t637_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasUpdate"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasUpdate_t637_0_0_0/* byval_arg */
	, &CanvasUpdate_t637_1_0_0/* this_arg */
	, &CanvasUpdate_t637_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasUpdate_t637)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CanvasUpdate_t637)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ICanvasElement
extern TypeInfo ICanvasElement_t770_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ICanvasElement_t770_0_0_0;
extern const Il2CppType ICanvasElement_t770_1_0_0;
struct ICanvasElement_t770;
const Il2CppTypeDefinitionMetadata ICanvasElement_t770_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3096/* methodStart */
	, -1/* eventStart */
	, 424/* propertyStart */

};
TypeInfo ICanvasElement_t770_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICanvasElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ICanvasElement_t770_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ICanvasElement_t770_0_0_0/* byval_arg */
	, &ICanvasElement_t770_1_0_0/* this_arg */
	, &ICanvasElement_t770_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.CanvasUpdateRegistry
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry.h"
// Metadata Definition UnityEngine.UI.CanvasUpdateRegistry
extern TypeInfo CanvasUpdateRegistry_t638_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasUpdateRegistry
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistryMethodDeclarations.h"
static const EncodedMethodIndex CanvasUpdateRegistry_t638_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasUpdateRegistry_t638_0_0_0;
extern const Il2CppType CanvasUpdateRegistry_t638_1_0_0;
struct CanvasUpdateRegistry_t638;
const Il2CppTypeDefinitionMetadata CanvasUpdateRegistry_t638_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CanvasUpdateRegistry_t638_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3117/* fieldStart */
	, 3099/* methodStart */
	, -1/* eventStart */
	, 425/* propertyStart */

};
TypeInfo CanvasUpdateRegistry_t638_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasUpdateRegistry"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &CanvasUpdateRegistry_t638_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasUpdateRegistry_t638_0_0_0/* byval_arg */
	, &CanvasUpdateRegistry_t638_1_0_0/* this_arg */
	, &CanvasUpdateRegistry_t638_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasUpdateRegistry_t638)/* instance_size */
	, sizeof (CanvasUpdateRegistry_t638)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CanvasUpdateRegistry_t638_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
// Metadata Definition UnityEngine.UI.ColorBlock
extern TypeInfo ColorBlock_t642_il2cpp_TypeInfo;
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlockMethodDeclarations.h"
static const EncodedMethodIndex ColorBlock_t642_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ColorBlock_t642_0_0_0;
extern const Il2CppType ColorBlock_t642_1_0_0;
const Il2CppTypeDefinitionMetadata ColorBlock_t642_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, ColorBlock_t642_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3125/* fieldStart */
	, 3117/* methodStart */
	, -1/* eventStart */
	, 426/* propertyStart */

};
TypeInfo ColorBlock_t642_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorBlock"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ColorBlock_t642_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorBlock_t642_0_0_0/* byval_arg */
	, &ColorBlock_t642_1_0_0/* this_arg */
	, &ColorBlock_t642_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorBlock_t642)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorBlock_t642)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(ColorBlock_t642 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 13/* method_count */
	, 7/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.FontData
#include "UnityEngine_UI_UnityEngine_UI_FontData.h"
// Metadata Definition UnityEngine.UI.FontData
extern TypeInfo FontData_t644_il2cpp_TypeInfo;
// UnityEngine.UI.FontData
#include "UnityEngine_UI_UnityEngine_UI_FontDataMethodDeclarations.h"
static const EncodedMethodIndex FontData_t644_VTable[6] = 
{
	626,
	601,
	627,
	628,
	994,
	995,
};
static const Il2CppType* FontData_t644_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t3399_0_0_0,
};
static Il2CppInterfaceOffsetPair FontData_t644_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FontData_t644_0_0_0;
extern const Il2CppType FontData_t644_1_0_0;
struct FontData_t644;
const Il2CppTypeDefinitionMetadata FontData_t644_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FontData_t644_InterfacesTypeInfos/* implementedInterfaces */
	, FontData_t644_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FontData_t644_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3131/* fieldStart */
	, 3130/* methodStart */
	, -1/* eventStart */
	, 433/* propertyStart */

};
TypeInfo FontData_t644_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FontData"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &FontData_t644_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FontData_t644_0_0_0/* byval_arg */
	, &FontData_t644_1_0_0/* this_arg */
	, &FontData_t644_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FontData_t644)/* instance_size */
	, sizeof (FontData_t644)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 12/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.FontUpdateTracker
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker.h"
// Metadata Definition UnityEngine.UI.FontUpdateTracker
extern TypeInfo FontUpdateTracker_t646_il2cpp_TypeInfo;
// UnityEngine.UI.FontUpdateTracker
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTrackerMethodDeclarations.h"
static const EncodedMethodIndex FontUpdateTracker_t646_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FontUpdateTracker_t646_0_0_0;
extern const Il2CppType FontUpdateTracker_t646_1_0_0;
struct FontUpdateTracker_t646;
const Il2CppTypeDefinitionMetadata FontUpdateTracker_t646_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FontUpdateTracker_t646_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3142/* fieldStart */
	, 3156/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FontUpdateTracker_t646_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FontUpdateTracker"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &FontUpdateTracker_t646_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FontUpdateTracker_t646_0_0_0/* byval_arg */
	, &FontUpdateTracker_t646_1_0_0/* this_arg */
	, &FontUpdateTracker_t646_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FontUpdateTracker_t646)/* instance_size */
	, sizeof (FontUpdateTracker_t646)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(FontUpdateTracker_t646_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
// Metadata Definition UnityEngine.UI.Graphic
extern TypeInfo Graphic_t654_il2cpp_TypeInfo;
// UnityEngine.UI.Graphic
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
static const EncodedMethodIndex Graphic_t654_VTable[36] = 
{
	600,
	601,
	602,
	603,
	655,
	996,
	657,
	997,
	659,
	660,
	998,
	999,
	1000,
	1001,
	665,
	1002,
	1003,
	1004,
	1005,
	1006,
	1007,
	1008,
	1009,
	1010,
	1011,
	1012,
	1013,
	1014,
	1003,
	1015,
	1016,
	1017,
	1018,
	1019,
	1005,
	1004,
};
static const Il2CppType* Graphic_t654_InterfacesTypeInfos[] = 
{
	&ICanvasElement_t770_0_0_0,
};
static Il2CppInterfaceOffsetPair Graphic_t654_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t770_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Graphic_t654_0_0_0;
extern const Il2CppType Graphic_t654_1_0_0;
struct Graphic_t654;
const Il2CppTypeDefinitionMetadata Graphic_t654_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Graphic_t654_InterfacesTypeInfos/* implementedInterfaces */
	, Graphic_t654_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, Graphic_t654_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3143/* fieldStart */
	, 3160/* methodStart */
	, -1/* eventStart */
	, 445/* propertyStart */

};
TypeInfo Graphic_t654_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Graphic"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Graphic_t654_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1133/* custom_attributes_cache */
	, &Graphic_t654_0_0_0/* byval_arg */
	, &Graphic_t654_1_0_0/* this_arg */
	, &Graphic_t654_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Graphic_t654)/* instance_size */
	, sizeof (Graphic_t654)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Graphic_t654_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 49/* method_count */
	, 10/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 36/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster.h"
// Metadata Definition UnityEngine.UI.GraphicRaycaster
extern TypeInfo GraphicRaycaster_t658_il2cpp_TypeInfo;
// UnityEngine.UI.GraphicRaycaster
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycasterMethodDeclarations.h"
extern const Il2CppType BlockingObjects_t655_0_0_0;
static const Il2CppType* GraphicRaycaster_t658_il2cpp_TypeInfo__nestedTypes[1] =
{
	&BlockingObjects_t655_0_0_0,
};
static const EncodedMethodIndex GraphicRaycaster_t658_VTable[21] = 
{
	600,
	601,
	602,
	603,
	655,
	945,
	657,
	946,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	1020,
	1021,
	947,
	1022,
	1023,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType GraphicRaycaster_t658_0_0_0;
extern const Il2CppType GraphicRaycaster_t658_1_0_0;
struct GraphicRaycaster_t658;
const Il2CppTypeDefinitionMetadata GraphicRaycaster_t658_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GraphicRaycaster_t658_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseRaycaster_t611_0_0_0/* parent */
	, GraphicRaycaster_t658_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3159/* fieldStart */
	, 3209/* methodStart */
	, -1/* eventStart */
	, 455/* propertyStart */

};
TypeInfo GraphicRaycaster_t658_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GraphicRaycaster"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &GraphicRaycaster_t658_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1140/* custom_attributes_cache */
	, &GraphicRaycaster_t658_0_0_0/* byval_arg */
	, &GraphicRaycaster_t658_1_0_0/* this_arg */
	, &GraphicRaycaster_t658_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GraphicRaycaster_t658)/* instance_size */
	, sizeof (GraphicRaycaster_t658)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GraphicRaycaster_t658_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 6/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 21/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
// Metadata Definition UnityEngine.UI.GraphicRaycaster/BlockingObjects
extern TypeInfo BlockingObjects_t655_il2cpp_TypeInfo;
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjecMethodDeclarations.h"
static const EncodedMethodIndex BlockingObjects_t655_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair BlockingObjects_t655_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BlockingObjects_t655_1_0_0;
const Il2CppTypeDefinitionMetadata BlockingObjects_t655_DefinitionMetadata = 
{
	&GraphicRaycaster_t658_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BlockingObjects_t655_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, BlockingObjects_t655_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3167/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BlockingObjects_t655_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BlockingObjects"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BlockingObjects_t655_0_0_0/* byval_arg */
	, &BlockingObjects_t655_1_0_0/* this_arg */
	, &BlockingObjects_t655_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BlockingObjects_t655)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BlockingObjects_t655)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GraphicRegistry
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry.h"
// Metadata Definition UnityEngine.UI.GraphicRegistry
extern TypeInfo GraphicRegistry_t659_il2cpp_TypeInfo;
// UnityEngine.UI.GraphicRegistry
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistryMethodDeclarations.h"
static const EncodedMethodIndex GraphicRegistry_t659_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType GraphicRegistry_t659_0_0_0;
extern const Il2CppType GraphicRegistry_t659_1_0_0;
struct GraphicRegistry_t659;
const Il2CppTypeDefinitionMetadata GraphicRegistry_t659_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GraphicRegistry_t659_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3172/* fieldStart */
	, 3222/* methodStart */
	, -1/* eventStart */
	, 461/* propertyStart */

};
TypeInfo GraphicRegistry_t659_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GraphicRegistry"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &GraphicRegistry_t659_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GraphicRegistry_t659_0_0_0/* byval_arg */
	, &GraphicRegistry_t659_1_0_0/* this_arg */
	, &GraphicRegistry_t659_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GraphicRegistry_t659)/* instance_size */
	, sizeof (GraphicRegistry_t659)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GraphicRegistry_t659_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IGraphicEnabledDisabled
extern TypeInfo IGraphicEnabledDisabled_t795_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IGraphicEnabledDisabled_t795_0_0_0;
extern const Il2CppType IGraphicEnabledDisabled_t795_1_0_0;
struct IGraphicEnabledDisabled_t795;
const Il2CppTypeDefinitionMetadata IGraphicEnabledDisabled_t795_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3228/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IGraphicEnabledDisabled_t795_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IGraphicEnabledDisabled"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &IGraphicEnabledDisabled_t795_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IGraphicEnabledDisabled_t795_0_0_0/* byval_arg */
	, &IGraphicEnabledDisabled_t795_1_0_0/* this_arg */
	, &IGraphicEnabledDisabled_t795_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Image
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
// Metadata Definition UnityEngine.UI.Image
extern TypeInfo Image_t669_il2cpp_TypeInfo;
// UnityEngine.UI.Image
#include "UnityEngine_UI_UnityEngine_UI_ImageMethodDeclarations.h"
extern const Il2CppType Type_t661_0_0_0;
extern const Il2CppType FillMethod_t662_0_0_0;
extern const Il2CppType OriginHorizontal_t663_0_0_0;
extern const Il2CppType OriginVertical_t664_0_0_0;
extern const Il2CppType Origin90_t665_0_0_0;
extern const Il2CppType Origin180_t666_0_0_0;
extern const Il2CppType Origin360_t667_0_0_0;
static const Il2CppType* Image_t669_il2cpp_TypeInfo__nestedTypes[7] =
{
	&Type_t661_0_0_0,
	&FillMethod_t662_0_0_0,
	&OriginHorizontal_t663_0_0_0,
	&OriginVertical_t664_0_0_0,
	&Origin90_t665_0_0_0,
	&Origin180_t666_0_0_0,
	&Origin360_t667_0_0_0,
};
static const EncodedMethodIndex Image_t669_VTable[62] = 
{
	600,
	601,
	602,
	603,
	655,
	1024,
	657,
	1025,
	659,
	660,
	998,
	999,
	1026,
	1001,
	665,
	1002,
	1003,
	1004,
	1005,
	1006,
	1007,
	1008,
	1027,
	1010,
	1028,
	1029,
	1013,
	1030,
	1003,
	1015,
	1016,
	1031,
	1032,
	1019,
	1005,
	1004,
	1033,
	1033,
	1034,
	1035,
	1036,
	1037,
	1038,
	1039,
	1040,
	1041,
	1042,
	1043,
	1044,
	1045,
	1035,
	1036,
	1037,
	1038,
	1039,
	1040,
	1041,
	1042,
	1043,
	1044,
	1045,
	1034,
};
extern const Il2CppType ICanvasRaycastFilter_t798_0_0_0;
extern const Il2CppType ILayoutElement_t776_0_0_0;
static const Il2CppType* Image_t669_InterfacesTypeInfos[] = 
{
	&ICanvasRaycastFilter_t798_0_0_0,
	&ISerializationCallbackReceiver_t3399_0_0_0,
	&ILayoutElement_t776_0_0_0,
};
extern const Il2CppType IMaskable_t826_0_0_0;
static Il2CppInterfaceOffsetPair Image_t669_InterfacesOffsets[] = 
{
	{ &IMaskable_t826_0_0_0, 36},
	{ &ICanvasElement_t770_0_0_0, 16},
	{ &ICanvasRaycastFilter_t798_0_0_0, 38},
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 39},
	{ &ILayoutElement_t776_0_0_0, 41},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Image_t669_0_0_0;
extern const Il2CppType Image_t669_1_0_0;
extern const Il2CppType MaskableGraphic_t670_0_0_0;
struct Image_t669;
const Il2CppTypeDefinitionMetadata Image_t669_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Image_t669_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Image_t669_InterfacesTypeInfos/* implementedInterfaces */
	, Image_t669_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t670_0_0_0/* parent */
	, Image_t669_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3175/* fieldStart */
	, 3229/* methodStart */
	, -1/* eventStart */
	, 462/* propertyStart */

};
TypeInfo Image_t669_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Image"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Image_t669_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1146/* custom_attributes_cache */
	, &Image_t669_0_0_0/* byval_arg */
	, &Image_t669_1_0_0/* this_arg */
	, &Image_t669_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Image_t669)/* instance_size */
	, sizeof (Image_t669)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Image_t669_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 49/* method_count */
	, 20/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 7/* nested_type_count */
	, 62/* vtable_count */
	, 3/* interfaces_count */
	, 5/* interface_offsets_count */

};
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
// Metadata Definition UnityEngine.UI.Image/Type
extern TypeInfo Type_t661_il2cpp_TypeInfo;
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_TypeMethodDeclarations.h"
static const EncodedMethodIndex Type_t661_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Type_t661_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Type_t661_1_0_0;
const Il2CppTypeDefinitionMetadata Type_t661_DefinitionMetadata = 
{
	&Image_t669_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Type_t661_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Type_t661_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3189/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Type_t661_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Type"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Type_t661_0_0_0/* byval_arg */
	, &Type_t661_1_0_0/* this_arg */
	, &Type_t661_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Type_t661)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Type_t661)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
// Metadata Definition UnityEngine.UI.Image/FillMethod
extern TypeInfo FillMethod_t662_il2cpp_TypeInfo;
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethodMethodDeclarations.h"
static const EncodedMethodIndex FillMethod_t662_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FillMethod_t662_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FillMethod_t662_1_0_0;
const Il2CppTypeDefinitionMetadata FillMethod_t662_DefinitionMetadata = 
{
	&Image_t669_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FillMethod_t662_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FillMethod_t662_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3194/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FillMethod_t662_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FillMethod"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FillMethod_t662_0_0_0/* byval_arg */
	, &FillMethod_t662_1_0_0/* this_arg */
	, &FillMethod_t662_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FillMethod_t662)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FillMethod_t662)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Image/OriginHorizontal
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizontal.h"
// Metadata Definition UnityEngine.UI.Image/OriginHorizontal
extern TypeInfo OriginHorizontal_t663_il2cpp_TypeInfo;
// UnityEngine.UI.Image/OriginHorizontal
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizontalMethodDeclarations.h"
static const EncodedMethodIndex OriginHorizontal_t663_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair OriginHorizontal_t663_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType OriginHorizontal_t663_1_0_0;
const Il2CppTypeDefinitionMetadata OriginHorizontal_t663_DefinitionMetadata = 
{
	&Image_t669_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OriginHorizontal_t663_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, OriginHorizontal_t663_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3200/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OriginHorizontal_t663_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "OriginHorizontal"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OriginHorizontal_t663_0_0_0/* byval_arg */
	, &OriginHorizontal_t663_1_0_0/* this_arg */
	, &OriginHorizontal_t663_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OriginHorizontal_t663)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OriginHorizontal_t663)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Image/OriginVertical
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical.h"
// Metadata Definition UnityEngine.UI.Image/OriginVertical
extern TypeInfo OriginVertical_t664_il2cpp_TypeInfo;
// UnityEngine.UI.Image/OriginVertical
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVerticalMethodDeclarations.h"
static const EncodedMethodIndex OriginVertical_t664_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair OriginVertical_t664_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType OriginVertical_t664_1_0_0;
const Il2CppTypeDefinitionMetadata OriginVertical_t664_DefinitionMetadata = 
{
	&Image_t669_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OriginVertical_t664_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, OriginVertical_t664_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3203/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OriginVertical_t664_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "OriginVertical"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OriginVertical_t664_0_0_0/* byval_arg */
	, &OriginVertical_t664_1_0_0/* this_arg */
	, &OriginVertical_t664_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OriginVertical_t664)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OriginVertical_t664)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Image/Origin90
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin90.h"
// Metadata Definition UnityEngine.UI.Image/Origin90
extern TypeInfo Origin90_t665_il2cpp_TypeInfo;
// UnityEngine.UI.Image/Origin90
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin90MethodDeclarations.h"
static const EncodedMethodIndex Origin90_t665_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Origin90_t665_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Origin90_t665_1_0_0;
const Il2CppTypeDefinitionMetadata Origin90_t665_DefinitionMetadata = 
{
	&Image_t669_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Origin90_t665_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Origin90_t665_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3206/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Origin90_t665_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Origin90"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Origin90_t665_0_0_0/* byval_arg */
	, &Origin90_t665_1_0_0/* this_arg */
	, &Origin90_t665_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Origin90_t665)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Origin90_t665)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Image/Origin180
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin180.h"
// Metadata Definition UnityEngine.UI.Image/Origin180
extern TypeInfo Origin180_t666_il2cpp_TypeInfo;
// UnityEngine.UI.Image/Origin180
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin180MethodDeclarations.h"
static const EncodedMethodIndex Origin180_t666_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Origin180_t666_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Origin180_t666_1_0_0;
const Il2CppTypeDefinitionMetadata Origin180_t666_DefinitionMetadata = 
{
	&Image_t669_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Origin180_t666_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Origin180_t666_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3211/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Origin180_t666_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Origin180"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Origin180_t666_0_0_0/* byval_arg */
	, &Origin180_t666_1_0_0/* this_arg */
	, &Origin180_t666_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Origin180_t666)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Origin180_t666)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Image/Origin360
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360.h"
// Metadata Definition UnityEngine.UI.Image/Origin360
extern TypeInfo Origin360_t667_il2cpp_TypeInfo;
// UnityEngine.UI.Image/Origin360
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360MethodDeclarations.h"
static const EncodedMethodIndex Origin360_t667_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Origin360_t667_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Origin360_t667_1_0_0;
const Il2CppTypeDefinitionMetadata Origin360_t667_DefinitionMetadata = 
{
	&Image_t669_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Origin360_t667_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Origin360_t667_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3216/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Origin360_t667_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Origin360"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Origin360_t667_0_0_0/* byval_arg */
	, &Origin360_t667_1_0_0/* this_arg */
	, &Origin360_t667_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Origin360_t667)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Origin360_t667)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMask
extern TypeInfo IMask_t814_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMask_t814_0_0_0;
extern const Il2CppType IMask_t814_1_0_0;
struct IMask_t814;
const Il2CppTypeDefinitionMetadata IMask_t814_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3278/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMask_t814_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMask"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &IMask_t814_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMask_t814_0_0_0/* byval_arg */
	, &IMask_t814_1_0_0/* this_arg */
	, &IMask_t814_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMaskable
extern TypeInfo IMaskable_t826_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMaskable_t826_1_0_0;
struct IMaskable_t826;
const Il2CppTypeDefinitionMetadata IMaskable_t826_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3279/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMaskable_t826_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMaskable"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &IMaskable_t826_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMaskable_t826_0_0_0/* byval_arg */
	, &IMaskable_t826_1_0_0/* this_arg */
	, &IMaskable_t826_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.InputField
#include "UnityEngine_UI_UnityEngine_UI_InputField.h"
// Metadata Definition UnityEngine.UI.InputField
extern TypeInfo InputField_t680_il2cpp_TypeInfo;
// UnityEngine.UI.InputField
#include "UnityEngine_UI_UnityEngine_UI_InputFieldMethodDeclarations.h"
extern const Il2CppType ContentType_t671_0_0_0;
extern const Il2CppType InputType_t672_0_0_0;
extern const Il2CppType CharacterValidation_t673_0_0_0;
extern const Il2CppType LineType_t674_0_0_0;
extern const Il2CppType SubmitEvent_t675_0_0_0;
extern const Il2CppType OnChangeEvent_t677_0_0_0;
extern const Il2CppType EditState_t678_0_0_0;
extern const Il2CppType OnValidateInput_t679_0_0_0;
extern const Il2CppType U3CCaretBlinkU3Ec__Iterator2_t681_0_0_0;
extern const Il2CppType U3CMouseDragOutsideRectU3Ec__Iterator3_t682_0_0_0;
static const Il2CppType* InputField_t680_il2cpp_TypeInfo__nestedTypes[10] =
{
	&ContentType_t671_0_0_0,
	&InputType_t672_0_0_0,
	&CharacterValidation_t673_0_0_0,
	&LineType_t674_0_0_0,
	&SubmitEvent_t675_0_0_0,
	&OnChangeEvent_t677_0_0_0,
	&EditState_t678_0_0_0,
	&OnValidateInput_t679_0_0_0,
	&U3CCaretBlinkU3Ec__Iterator2_t681_0_0_0,
	&U3CMouseDragOutsideRectU3Ec__Iterator3_t682_0_0_0,
};
static const EncodedMethodIndex InputField_t680_VTable[59] = 
{
	600,
	601,
	602,
	603,
	965,
	1046,
	657,
	1047,
	659,
	660,
	661,
	662,
	663,
	968,
	969,
	666,
	970,
	971,
	1048,
	973,
	1049,
	1050,
	976,
	977,
	978,
	1051,
	980,
	981,
	982,
	983,
	976,
	1048,
	973,
	970,
	971,
	1049,
	1050,
	984,
	1052,
	1053,
	1054,
	1055,
	1056,
	1057,
	1058,
	1059,
	1060,
	1061,
	1053,
	1054,
	1055,
	1056,
	1062,
	1063,
	1058,
	1052,
	1057,
	1060,
	1059,
};
static const Il2CppType* InputField_t680_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IPointerClickHandler_t499_0_0_0,
	&IBeginDragHandler_t493_0_0_0,
	&IDragHandler_t497_0_0_0,
	&IEndDragHandler_t503_0_0_0,
	&IUpdateSelectedHandler_t489_0_0_0,
	&ISubmitHandler_t765_0_0_0,
	&ICanvasElement_t770_0_0_0,
};
static Il2CppInterfaceOffsetPair InputField_t680_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 16},
	{ &IPointerEnterHandler_t760_0_0_0, 16},
	{ &IPointerExitHandler_t761_0_0_0, 17},
	{ &IPointerDownHandler_t505_0_0_0, 18},
	{ &IPointerUpHandler_t495_0_0_0, 19},
	{ &ISelectHandler_t487_0_0_0, 20},
	{ &IDeselectHandler_t763_0_0_0, 21},
	{ &IMoveHandler_t764_0_0_0, 22},
	{ &IPointerClickHandler_t499_0_0_0, 38},
	{ &IBeginDragHandler_t493_0_0_0, 39},
	{ &IDragHandler_t497_0_0_0, 40},
	{ &IEndDragHandler_t503_0_0_0, 41},
	{ &IUpdateSelectedHandler_t489_0_0_0, 42},
	{ &ISubmitHandler_t765_0_0_0, 43},
	{ &ICanvasElement_t770_0_0_0, 44},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType InputField_t680_0_0_0;
extern const Il2CppType InputField_t680_1_0_0;
struct InputField_t680;
const Il2CppTypeDefinitionMetadata InputField_t680_DefinitionMetadata = 
{
	NULL/* declaringType */
	, InputField_t680_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, InputField_t680_InterfacesTypeInfos/* implementedInterfaces */
	, InputField_t680_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t636_0_0_0/* parent */
	, InputField_t680_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3221/* fieldStart */
	, 3280/* methodStart */
	, -1/* eventStart */
	, 482/* propertyStart */

};
TypeInfo InputField_t680_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "InputField"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &InputField_t680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1155/* custom_attributes_cache */
	, &InputField_t680_0_0_0/* byval_arg */
	, &InputField_t680_1_0_0/* this_arg */
	, &InputField_t680_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InputField_t680)/* instance_size */
	, sizeof (InputField_t680)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(InputField_t680_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 125/* method_count */
	, 27/* property_count */
	, 43/* field_count */
	, 0/* event_count */
	, 10/* nested_type_count */
	, 59/* vtable_count */
	, 8/* interfaces_count */
	, 15/* interface_offsets_count */

};
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
// Metadata Definition UnityEngine.UI.InputField/ContentType
extern TypeInfo ContentType_t671_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTypeMethodDeclarations.h"
static const EncodedMethodIndex ContentType_t671_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ContentType_t671_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ContentType_t671_1_0_0;
const Il2CppTypeDefinitionMetadata ContentType_t671_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ContentType_t671_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ContentType_t671_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3264/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContentType_t671_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContentType_t671_0_0_0/* byval_arg */
	, &ContentType_t671_1_0_0/* this_arg */
	, &ContentType_t671_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentType_t671)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ContentType_t671)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
// Metadata Definition UnityEngine.UI.InputField/InputType
extern TypeInfo InputType_t672_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputTypeMethodDeclarations.h"
static const EncodedMethodIndex InputType_t672_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair InputType_t672_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType InputType_t672_1_0_0;
const Il2CppTypeDefinitionMetadata InputType_t672_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InputType_t672_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, InputType_t672_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3275/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InputType_t672_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "InputType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InputType_t672_0_0_0/* byval_arg */
	, &InputType_t672_1_0_0/* this_arg */
	, &InputType_t672_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InputType_t672)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (InputType_t672)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
// Metadata Definition UnityEngine.UI.InputField/CharacterValidation
extern TypeInfo CharacterValidation_t673_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidationMethodDeclarations.h"
static const EncodedMethodIndex CharacterValidation_t673_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CharacterValidation_t673_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CharacterValidation_t673_1_0_0;
const Il2CppTypeDefinitionMetadata CharacterValidation_t673_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CharacterValidation_t673_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CharacterValidation_t673_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3279/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CharacterValidation_t673_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharacterValidation"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CharacterValidation_t673_0_0_0/* byval_arg */
	, &CharacterValidation_t673_1_0_0/* this_arg */
	, &CharacterValidation_t673_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharacterValidation_t673)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CharacterValidation_t673)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
// Metadata Definition UnityEngine.UI.InputField/LineType
extern TypeInfo LineType_t674_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineTypeMethodDeclarations.h"
static const EncodedMethodIndex LineType_t674_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair LineType_t674_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LineType_t674_1_0_0;
const Il2CppTypeDefinitionMetadata LineType_t674_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LineType_t674_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, LineType_t674_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3286/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LineType_t674_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LineType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LineType_t674_0_0_0/* byval_arg */
	, &LineType_t674_1_0_0/* this_arg */
	, &LineType_t674_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LineType_t674)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LineType_t674)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.InputField/SubmitEvent
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEvent.h"
// Metadata Definition UnityEngine.UI.InputField/SubmitEvent
extern TypeInfo SubmitEvent_t675_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/SubmitEvent
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEventMethodDeclarations.h"
static const EncodedMethodIndex SubmitEvent_t675_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484712,
	2147484713,
};
static Il2CppInterfaceOffsetPair SubmitEvent_t675_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SubmitEvent_t675_1_0_0;
extern const Il2CppType UnityEvent_1_t676_0_0_0;
struct SubmitEvent_t675;
const Il2CppTypeDefinitionMetadata SubmitEvent_t675_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SubmitEvent_t675_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t676_0_0_0/* parent */
	, SubmitEvent_t675_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3405/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SubmitEvent_t675_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SubmitEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SubmitEvent_t675_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SubmitEvent_t675_0_0_0/* byval_arg */
	, &SubmitEvent_t675_1_0_0/* this_arg */
	, &SubmitEvent_t675_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SubmitEvent_t675)/* instance_size */
	, sizeof (SubmitEvent_t675)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.InputField/OnChangeEvent
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeEvent.h"
// Metadata Definition UnityEngine.UI.InputField/OnChangeEvent
extern TypeInfo OnChangeEvent_t677_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/OnChangeEvent
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeEventMethodDeclarations.h"
static const EncodedMethodIndex OnChangeEvent_t677_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484712,
	2147484713,
};
static Il2CppInterfaceOffsetPair OnChangeEvent_t677_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType OnChangeEvent_t677_1_0_0;
struct OnChangeEvent_t677;
const Il2CppTypeDefinitionMetadata OnChangeEvent_t677_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnChangeEvent_t677_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t676_0_0_0/* parent */
	, OnChangeEvent_t677_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3406/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnChangeEvent_t677_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnChangeEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnChangeEvent_t677_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnChangeEvent_t677_0_0_0/* byval_arg */
	, &OnChangeEvent_t677_1_0_0/* this_arg */
	, &OnChangeEvent_t677_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnChangeEvent_t677)/* instance_size */
	, sizeof (OnChangeEvent_t677)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
// Metadata Definition UnityEngine.UI.InputField/EditState
extern TypeInfo EditState_t678_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditStateMethodDeclarations.h"
static const EncodedMethodIndex EditState_t678_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EditState_t678_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType EditState_t678_1_0_0;
const Il2CppTypeDefinitionMetadata EditState_t678_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EditState_t678_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EditState_t678_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3290/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EditState_t678_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "EditState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EditState_t678_0_0_0/* byval_arg */
	, &EditState_t678_1_0_0/* this_arg */
	, &EditState_t678_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EditState_t678)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EditState_t678)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 260/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.InputField/OnValidateInput
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidateInput.h"
// Metadata Definition UnityEngine.UI.InputField/OnValidateInput
extern TypeInfo OnValidateInput_t679_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/OnValidateInput
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidateInputMethodDeclarations.h"
static const EncodedMethodIndex OnValidateInput_t679_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1066,
	1067,
	1068,
};
static Il2CppInterfaceOffsetPair OnValidateInput_t679_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType OnValidateInput_t679_1_0_0;
struct OnValidateInput_t679;
const Il2CppTypeDefinitionMetadata OnValidateInput_t679_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OnValidateInput_t679_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, OnValidateInput_t679_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3407/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OnValidateInput_t679_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "OnValidateInput"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &OnValidateInput_t679_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OnValidateInput_t679_0_0_0/* byval_arg */
	, &OnValidateInput_t679_1_0_0/* this_arg */
	, &OnValidateInput_t679_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_OnValidateInput_t679/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OnValidateInput_t679)/* instance_size */
	, sizeof (OnValidateInput_t679)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.InputField/<CaretBlink>c__Iterator2
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretBlinkU3Ec__.h"
// Metadata Definition UnityEngine.UI.InputField/<CaretBlink>c__Iterator2
extern TypeInfo U3CCaretBlinkU3Ec__Iterator2_t681_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/<CaretBlink>c__Iterator2
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretBlinkU3Ec__MethodDeclarations.h"
static const EncodedMethodIndex U3CCaretBlinkU3Ec__Iterator2_t681_VTable[9] = 
{
	626,
	601,
	627,
	628,
	1069,
	1070,
	1071,
	1072,
	1073,
};
static const Il2CppType* U3CCaretBlinkU3Ec__Iterator2_t681_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCaretBlinkU3Ec__Iterator2_t681_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &IEnumerator_t464_0_0_0, 5},
	{ &IEnumerator_1_t2280_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType U3CCaretBlinkU3Ec__Iterator2_t681_1_0_0;
struct U3CCaretBlinkU3Ec__Iterator2_t681;
const Il2CppTypeDefinitionMetadata U3CCaretBlinkU3Ec__Iterator2_t681_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCaretBlinkU3Ec__Iterator2_t681_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCaretBlinkU3Ec__Iterator2_t681_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCaretBlinkU3Ec__Iterator2_t681_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3293/* fieldStart */
	, 3411/* methodStart */
	, -1/* eventStart */
	, 509/* propertyStart */

};
TypeInfo U3CCaretBlinkU3Ec__Iterator2_t681_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CaretBlink>c__Iterator2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CCaretBlinkU3Ec__Iterator2_t681_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1175/* custom_attributes_cache */
	, &U3CCaretBlinkU3Ec__Iterator2_t681_0_0_0/* byval_arg */
	, &U3CCaretBlinkU3Ec__Iterator2_t681_1_0_0/* this_arg */
	, &U3CCaretBlinkU3Ec__Iterator2_t681_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CCaretBlinkU3Ec__Iterator2_t681)/* instance_size */
	, sizeof (U3CCaretBlinkU3Ec__Iterator2_t681)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseDragOutside.h"
// Metadata Definition UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3
extern TypeInfo U3CMouseDragOutsideRectU3Ec__Iterator3_t682_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/<MouseDragOutsideRect>c__Iterator3
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseDragOutsideMethodDeclarations.h"
static const EncodedMethodIndex U3CMouseDragOutsideRectU3Ec__Iterator3_t682_VTable[9] = 
{
	626,
	601,
	627,
	628,
	1074,
	1075,
	1076,
	1077,
	1078,
};
static const Il2CppType* U3CMouseDragOutsideRectU3Ec__Iterator3_t682_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CMouseDragOutsideRectU3Ec__Iterator3_t682_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &IEnumerator_t464_0_0_0, 5},
	{ &IEnumerator_1_t2280_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType U3CMouseDragOutsideRectU3Ec__Iterator3_t682_1_0_0;
struct U3CMouseDragOutsideRectU3Ec__Iterator3_t682;
const Il2CppTypeDefinitionMetadata U3CMouseDragOutsideRectU3Ec__Iterator3_t682_DefinitionMetadata = 
{
	&InputField_t680_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CMouseDragOutsideRectU3Ec__Iterator3_t682_InterfacesTypeInfos/* implementedInterfaces */
	, U3CMouseDragOutsideRectU3Ec__Iterator3_t682_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CMouseDragOutsideRectU3Ec__Iterator3_t682_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3298/* fieldStart */
	, 3417/* methodStart */
	, -1/* eventStart */
	, 511/* propertyStart */

};
TypeInfo U3CMouseDragOutsideRectU3Ec__Iterator3_t682_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "<MouseDragOutsideRect>c__Iterator3"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CMouseDragOutsideRectU3Ec__Iterator3_t682_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1180/* custom_attributes_cache */
	, &U3CMouseDragOutsideRectU3Ec__Iterator3_t682_0_0_0/* byval_arg */
	, &U3CMouseDragOutsideRectU3Ec__Iterator3_t682_1_0_0/* this_arg */
	, &U3CMouseDragOutsideRectU3Ec__Iterator3_t682_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CMouseDragOutsideRectU3Ec__Iterator3_t682)/* instance_size */
	, sizeof (U3CMouseDragOutsideRectU3Ec__Iterator3_t682)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// Metadata Definition UnityEngine.UI.MaskableGraphic
extern TypeInfo MaskableGraphic_t670_il2cpp_TypeInfo;
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphicMethodDeclarations.h"
static const EncodedMethodIndex MaskableGraphic_t670_VTable[38] = 
{
	600,
	601,
	602,
	603,
	655,
	1024,
	657,
	1025,
	659,
	660,
	998,
	999,
	1026,
	1001,
	665,
	1002,
	1003,
	1004,
	1005,
	1006,
	1007,
	1008,
	1027,
	1010,
	1028,
	1029,
	1013,
	1014,
	1003,
	1015,
	1016,
	1017,
	1018,
	1019,
	1005,
	1004,
	1033,
	1033,
};
static const Il2CppType* MaskableGraphic_t670_InterfacesTypeInfos[] = 
{
	&IMaskable_t826_0_0_0,
};
static Il2CppInterfaceOffsetPair MaskableGraphic_t670_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t770_0_0_0, 16},
	{ &IMaskable_t826_0_0_0, 36},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MaskableGraphic_t670_1_0_0;
struct MaskableGraphic_t670;
const Il2CppTypeDefinitionMetadata MaskableGraphic_t670_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MaskableGraphic_t670_InterfacesTypeInfos/* implementedInterfaces */
	, MaskableGraphic_t670_InterfacesOffsets/* interfaceOffsets */
	, &Graphic_t654_0_0_0/* parent */
	, MaskableGraphic_t670_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3306/* fieldStart */
	, 3423/* methodStart */
	, -1/* eventStart */
	, 513/* propertyStart */

};
TypeInfo MaskableGraphic_t670_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MaskableGraphic"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &MaskableGraphic_t670_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MaskableGraphic_t670_0_0_0/* byval_arg */
	, &MaskableGraphic_t670_1_0_0/* this_arg */
	, &MaskableGraphic_t670_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MaskableGraphic_t670)/* instance_size */
	, sizeof (MaskableGraphic_t670)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 38/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.Misc
#include "UnityEngine_UI_UnityEngine_UI_Misc.h"
// Metadata Definition UnityEngine.UI.Misc
extern TypeInfo Misc_t688_il2cpp_TypeInfo;
// UnityEngine.UI.Misc
#include "UnityEngine_UI_UnityEngine_UI_MiscMethodDeclarations.h"
static const EncodedMethodIndex Misc_t688_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Misc_t688_0_0_0;
extern const Il2CppType Misc_t688_1_0_0;
struct Misc_t688;
const Il2CppTypeDefinitionMetadata Misc_t688_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Misc_t688_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3436/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Misc_t688_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Misc"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Misc_t688_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Misc_t688_0_0_0/* byval_arg */
	, &Misc_t688_1_0_0/* this_arg */
	, &Misc_t688_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Misc_t688)/* instance_size */
	, sizeof (Misc_t688)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
// Metadata Definition UnityEngine.UI.Navigation
extern TypeInfo Navigation_t690_il2cpp_TypeInfo;
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_NavigationMethodDeclarations.h"
extern const Il2CppType Mode_t689_0_0_0;
static const Il2CppType* Navigation_t690_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Mode_t689_0_0_0,
};
static const EncodedMethodIndex Navigation_t690_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Navigation_t690_0_0_0;
extern const Il2CppType Navigation_t690_1_0_0;
const Il2CppTypeDefinitionMetadata Navigation_t690_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Navigation_t690_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Navigation_t690_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3311/* fieldStart */
	, 3438/* methodStart */
	, -1/* eventStart */
	, 515/* propertyStart */

};
TypeInfo Navigation_t690_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Navigation"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Navigation_t690_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Navigation_t690_0_0_0/* byval_arg */
	, &Navigation_t690_1_0_0/* this_arg */
	, &Navigation_t690_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Navigation_t690)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Navigation_t690)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
// Metadata Definition UnityEngine.UI.Navigation/Mode
extern TypeInfo Mode_t689_il2cpp_TypeInfo;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_ModeMethodDeclarations.h"
static const EncodedMethodIndex Mode_t689_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Mode_t689_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Mode_t689_1_0_0;
const Il2CppTypeDefinitionMetadata Mode_t689_DefinitionMetadata = 
{
	&Navigation_t690_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mode_t689_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Mode_t689_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3316/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Mode_t689_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1190/* custom_attributes_cache */
	, &Mode_t689_0_0_0/* byval_arg */
	, &Mode_t689_1_0_0/* this_arg */
	, &Mode_t689_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mode_t689)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Mode_t689)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.RawImage
#include "UnityEngine_UI_UnityEngine_UI_RawImage.h"
// Metadata Definition UnityEngine.UI.RawImage
extern TypeInfo RawImage_t691_il2cpp_TypeInfo;
// UnityEngine.UI.RawImage
#include "UnityEngine_UI_UnityEngine_UI_RawImageMethodDeclarations.h"
static const EncodedMethodIndex RawImage_t691_VTable[38] = 
{
	600,
	601,
	602,
	603,
	655,
	1024,
	657,
	1025,
	659,
	660,
	998,
	999,
	1026,
	1001,
	665,
	1002,
	1003,
	1004,
	1005,
	1006,
	1007,
	1008,
	1027,
	1010,
	1028,
	1029,
	1013,
	1079,
	1003,
	1015,
	1016,
	1080,
	1081,
	1019,
	1005,
	1004,
	1033,
	1033,
};
static Il2CppInterfaceOffsetPair RawImage_t691_InterfacesOffsets[] = 
{
	{ &IMaskable_t826_0_0_0, 36},
	{ &ICanvasElement_t770_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType RawImage_t691_0_0_0;
extern const Il2CppType RawImage_t691_1_0_0;
struct RawImage_t691;
const Il2CppTypeDefinitionMetadata RawImage_t691_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RawImage_t691_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t670_0_0_0/* parent */
	, RawImage_t691_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3322/* fieldStart */
	, 3449/* methodStart */
	, -1/* eventStart */
	, 521/* propertyStart */

};
TypeInfo RawImage_t691_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "RawImage"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &RawImage_t691_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1191/* custom_attributes_cache */
	, &RawImage_t691_0_0_0/* byval_arg */
	, &RawImage_t691_1_0_0/* this_arg */
	, &RawImage_t691_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RawImage_t691)/* instance_size */
	, sizeof (RawImage_t691)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 38/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.Scrollbar
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar.h"
// Metadata Definition UnityEngine.UI.Scrollbar
extern TypeInfo Scrollbar_t696_il2cpp_TypeInfo;
// UnityEngine.UI.Scrollbar
#include "UnityEngine_UI_UnityEngine_UI_ScrollbarMethodDeclarations.h"
extern const Il2CppType Direction_t692_0_0_0;
extern const Il2CppType ScrollEvent_t693_0_0_0;
extern const Il2CppType Axis_t695_0_0_0;
extern const Il2CppType U3CClickRepeatU3Ec__Iterator4_t697_0_0_0;
static const Il2CppType* Scrollbar_t696_il2cpp_TypeInfo__nestedTypes[4] =
{
	&Direction_t692_0_0_0,
	&ScrollEvent_t693_0_0_0,
	&Axis_t695_0_0_0,
	&U3CClickRepeatU3Ec__Iterator4_t697_0_0_0,
};
static const EncodedMethodIndex Scrollbar_t696_VTable[50] = 
{
	600,
	601,
	602,
	603,
	965,
	1082,
	657,
	1083,
	659,
	660,
	1084,
	662,
	663,
	968,
	969,
	666,
	970,
	971,
	1085,
	1086,
	974,
	975,
	1087,
	977,
	978,
	979,
	1088,
	1089,
	1090,
	1091,
	1087,
	1085,
	1086,
	970,
	971,
	974,
	975,
	984,
	1092,
	1093,
	1094,
	1095,
	1096,
	1097,
	1095,
	1092,
	1094,
	1093,
	1097,
	1096,
};
static const Il2CppType* Scrollbar_t696_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IBeginDragHandler_t493_0_0_0,
	&IInitializePotentialDragHandler_t507_0_0_0,
	&IDragHandler_t497_0_0_0,
	&ICanvasElement_t770_0_0_0,
};
static Il2CppInterfaceOffsetPair Scrollbar_t696_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 16},
	{ &IPointerEnterHandler_t760_0_0_0, 16},
	{ &IPointerExitHandler_t761_0_0_0, 17},
	{ &IPointerDownHandler_t505_0_0_0, 18},
	{ &IPointerUpHandler_t495_0_0_0, 19},
	{ &ISelectHandler_t487_0_0_0, 20},
	{ &IDeselectHandler_t763_0_0_0, 21},
	{ &IMoveHandler_t764_0_0_0, 22},
	{ &IBeginDragHandler_t493_0_0_0, 38},
	{ &IInitializePotentialDragHandler_t507_0_0_0, 39},
	{ &IDragHandler_t497_0_0_0, 40},
	{ &ICanvasElement_t770_0_0_0, 41},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Scrollbar_t696_0_0_0;
extern const Il2CppType Scrollbar_t696_1_0_0;
struct Scrollbar_t696;
const Il2CppTypeDefinitionMetadata Scrollbar_t696_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Scrollbar_t696_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Scrollbar_t696_InterfacesTypeInfos/* implementedInterfaces */
	, Scrollbar_t696_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t636_0_0_0/* parent */
	, Scrollbar_t696_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3324/* fieldStart */
	, 3457/* methodStart */
	, -1/* eventStart */
	, 524/* propertyStart */

};
TypeInfo Scrollbar_t696_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Scrollbar"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Scrollbar_t696_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1194/* custom_attributes_cache */
	, &Scrollbar_t696_0_0_0/* byval_arg */
	, &Scrollbar_t696_1_0_0/* this_arg */
	, &Scrollbar_t696_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Scrollbar_t696)/* instance_size */
	, sizeof (Scrollbar_t696)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 40/* method_count */
	, 9/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 50/* vtable_count */
	, 5/* interfaces_count */
	, 12/* interface_offsets_count */

};
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
// Metadata Definition UnityEngine.UI.Scrollbar/Direction
extern TypeInfo Direction_t692_il2cpp_TypeInfo;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_DirectionMethodDeclarations.h"
static const EncodedMethodIndex Direction_t692_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Direction_t692_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Direction_t692_1_0_0;
const Il2CppTypeDefinitionMetadata Direction_t692_DefinitionMetadata = 
{
	&Scrollbar_t696_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t692_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Direction_t692_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3335/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Direction_t692_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t692_0_0_0/* byval_arg */
	, &Direction_t692_1_0_0/* this_arg */
	, &Direction_t692_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t692)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t692)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Scrollbar/ScrollEvent
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEvent.h"
// Metadata Definition UnityEngine.UI.Scrollbar/ScrollEvent
extern TypeInfo ScrollEvent_t693_il2cpp_TypeInfo;
// UnityEngine.UI.Scrollbar/ScrollEvent
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEventMethodDeclarations.h"
static const EncodedMethodIndex ScrollEvent_t693_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484746,
	2147484747,
};
static Il2CppInterfaceOffsetPair ScrollEvent_t693_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollEvent_t693_1_0_0;
extern const Il2CppType UnityEvent_1_t694_0_0_0;
struct ScrollEvent_t693;
const Il2CppTypeDefinitionMetadata ScrollEvent_t693_DefinitionMetadata = 
{
	&Scrollbar_t696_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrollEvent_t693_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t694_0_0_0/* parent */
	, ScrollEvent_t693_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3497/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScrollEvent_t693_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ScrollEvent_t693_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollEvent_t693_0_0_0/* byval_arg */
	, &ScrollEvent_t693_1_0_0/* this_arg */
	, &ScrollEvent_t693_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollEvent_t693)/* instance_size */
	, sizeof (ScrollEvent_t693)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
// Metadata Definition UnityEngine.UI.Scrollbar/Axis
extern TypeInfo Axis_t695_il2cpp_TypeInfo;
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_AxisMethodDeclarations.h"
static const EncodedMethodIndex Axis_t695_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Axis_t695_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t695_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t695_DefinitionMetadata = 
{
	&Scrollbar_t696_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t695_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Axis_t695_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3340/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Axis_t695_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t695_0_0_0/* byval_arg */
	, &Axis_t695_1_0_0/* this_arg */
	, &Axis_t695_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t695)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t695)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRepeatU3Ec__.h"
// Metadata Definition UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4
extern TypeInfo U3CClickRepeatU3Ec__Iterator4_t697_il2cpp_TypeInfo;
// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator4
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRepeatU3Ec__MethodDeclarations.h"
static const EncodedMethodIndex U3CClickRepeatU3Ec__Iterator4_t697_VTable[9] = 
{
	626,
	601,
	627,
	628,
	1100,
	1101,
	1102,
	1103,
	1104,
};
static const Il2CppType* U3CClickRepeatU3Ec__Iterator4_t697_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&IEnumerator_t464_0_0_0,
	&IEnumerator_1_t2280_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CClickRepeatU3Ec__Iterator4_t697_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &IEnumerator_t464_0_0_0, 5},
	{ &IEnumerator_1_t2280_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType U3CClickRepeatU3Ec__Iterator4_t697_1_0_0;
struct U3CClickRepeatU3Ec__Iterator4_t697;
const Il2CppTypeDefinitionMetadata U3CClickRepeatU3Ec__Iterator4_t697_DefinitionMetadata = 
{
	&Scrollbar_t696_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CClickRepeatU3Ec__Iterator4_t697_InterfacesTypeInfos/* implementedInterfaces */
	, U3CClickRepeatU3Ec__Iterator4_t697_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CClickRepeatU3Ec__Iterator4_t697_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3343/* fieldStart */
	, 3498/* methodStart */
	, -1/* eventStart */
	, 533/* propertyStart */

};
TypeInfo U3CClickRepeatU3Ec__Iterator4_t697_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "<ClickRepeat>c__Iterator4"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CClickRepeatU3Ec__Iterator4_t697_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1202/* custom_attributes_cache */
	, &U3CClickRepeatU3Ec__Iterator4_t697_0_0_0/* byval_arg */
	, &U3CClickRepeatU3Ec__Iterator4_t697_1_0_0/* this_arg */
	, &U3CClickRepeatU3Ec__Iterator4_t697_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CClickRepeatU3Ec__Iterator4_t697)/* instance_size */
	, sizeof (U3CClickRepeatU3Ec__Iterator4_t697)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
// Metadata Definition UnityEngine.UI.ScrollRect
extern TypeInfo ScrollRect_t702_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRectMethodDeclarations.h"
extern const Il2CppType MovementType_t699_0_0_0;
extern const Il2CppType ScrollRectEvent_t700_0_0_0;
static const Il2CppType* ScrollRect_t702_il2cpp_TypeInfo__nestedTypes[2] =
{
	&MovementType_t699_0_0_0,
	&ScrollRectEvent_t700_0_0_0,
};
static const EncodedMethodIndex ScrollRect_t702_VTable[35] = 
{
	600,
	601,
	602,
	603,
	655,
	1105,
	657,
	1106,
	659,
	1107,
	661,
	662,
	663,
	664,
	665,
	666,
	1108,
	1109,
	1110,
	1111,
	1112,
	1113,
	1114,
	1115,
	1113,
	1116,
	1112,
	1109,
	1108,
	1111,
	1110,
	1117,
	1118,
	1115,
	1114,
};
static const Il2CppType* ScrollRect_t702_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IBeginDragHandler_t493_0_0_0,
	&IInitializePotentialDragHandler_t507_0_0_0,
	&IDragHandler_t497_0_0_0,
	&IEndDragHandler_t503_0_0_0,
	&IScrollHandler_t762_0_0_0,
	&ICanvasElement_t770_0_0_0,
};
static Il2CppInterfaceOffsetPair ScrollRect_t702_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 16},
	{ &IBeginDragHandler_t493_0_0_0, 16},
	{ &IInitializePotentialDragHandler_t507_0_0_0, 17},
	{ &IDragHandler_t497_0_0_0, 18},
	{ &IEndDragHandler_t503_0_0_0, 19},
	{ &IScrollHandler_t762_0_0_0, 20},
	{ &ICanvasElement_t770_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRect_t702_0_0_0;
extern const Il2CppType ScrollRect_t702_1_0_0;
struct ScrollRect_t702;
const Il2CppTypeDefinitionMetadata ScrollRect_t702_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScrollRect_t702_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ScrollRect_t702_InterfacesTypeInfos/* implementedInterfaces */
	, ScrollRect_t702_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, ScrollRect_t702_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3350/* fieldStart */
	, 3504/* methodStart */
	, -1/* eventStart */
	, 535/* propertyStart */

};
TypeInfo ScrollRect_t702_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ScrollRect_t702_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1207/* custom_attributes_cache */
	, &ScrollRect_t702_0_0_0/* byval_arg */
	, &ScrollRect_t702_1_0_0/* this_arg */
	, &ScrollRect_t702_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRect_t702)/* instance_size */
	, sizeof (ScrollRect_t702)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 56/* method_count */
	, 16/* property_count */
	, 23/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 35/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
