﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AppManager
struct AppManager_t350;
// AppManager/OnStateChangeHandler
struct OnStateChangeHandler_t349;
// System.Collections.Hashtable
struct Hashtable_t348;
// AppEvent
#include "AssemblyU2DCSharp_AppEvent.h"

// System.Void AppManager::.ctor()
extern "C" void AppManager__ctor_m2037 (AppManager_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::.cctor()
extern "C" void AppManager__cctor_m2038 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::add_OnStateChangeEvent(AppManager/OnStateChangeHandler)
extern "C" void AppManager_add_OnStateChangeEvent_m2039 (AppManager_t350 * __this, OnStateChangeHandler_t349 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::remove_OnStateChangeEvent(AppManager/OnStateChangeHandler)
extern "C" void AppManager_remove_OnStateChangeEvent_m2040 (AppManager_t350 * __this, OnStateChangeHandler_t349 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AppManager AppManager::get_Instance()
extern "C" AppManager_t350 * AppManager_get_Instance_m2041 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::dispatchEvent(AppEvent,System.Collections.Hashtable)
extern "C" void AppManager_dispatchEvent_m2042 (AppManager_t350 * __this, int32_t ___gameStateEvent, Hashtable_t348 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::Start()
extern "C" void AppManager_Start_m2043 (AppManager_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppManager::Update()
extern "C" void AppManager_Update_m2044 (AppManager_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
