﻿#pragma once
#include <stdint.h>
// UnityEngine.WWW
struct WWW_t304;
// System.Object
struct Object_t;
// WWWTextureLoader
struct WWWTextureLoader_t305;
// System.Object
#include "mscorlib_System_Object.h"
// WWWTextureLoader/<LoadCoroutin>c__Iterator4
struct  U3CLoadCoroutinU3Ec__Iterator4_t306  : public Object_t
{
	// UnityEngine.WWW WWWTextureLoader/<LoadCoroutin>c__Iterator4::<www>__0
	WWW_t304 * ___U3CwwwU3E__0_0;
	// System.Int32 WWWTextureLoader/<LoadCoroutin>c__Iterator4::$PC
	int32_t ___U24PC_1;
	// System.Object WWWTextureLoader/<LoadCoroutin>c__Iterator4::$current
	Object_t * ___U24current_2;
	// WWWTextureLoader WWWTextureLoader/<LoadCoroutin>c__Iterator4::<>f__this
	WWWTextureLoader_t305 * ___U3CU3Ef__this_3;
};
