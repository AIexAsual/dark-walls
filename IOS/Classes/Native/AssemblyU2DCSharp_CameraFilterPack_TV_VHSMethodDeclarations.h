﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_VHS
struct CameraFilterPack_TV_VHS_t194;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_VHS::.ctor()
extern "C" void CameraFilterPack_TV_VHS__ctor_m1256 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_VHS::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_VHS_get_material_m1257 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::Start()
extern "C" void CameraFilterPack_TV_VHS_Start_m1258 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_VHS_OnRenderImage_m1259 (CameraFilterPack_TV_VHS_t194 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::OnValidate()
extern "C" void CameraFilterPack_TV_VHS_OnValidate_m1260 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::Update()
extern "C" void CameraFilterPack_TV_VHS_Update_m1261 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_VHS::OnDisable()
extern "C" void CameraFilterPack_TV_VHS_OnDisable_m1262 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
