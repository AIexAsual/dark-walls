﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int16>
struct InternalEnumerator_1_t2910;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22694_gshared (InternalEnumerator_1_t2910 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22694(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2910 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22694_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22695_gshared (InternalEnumerator_1_t2910 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22695(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2910 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22695_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22696_gshared (InternalEnumerator_1_t2910 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22696(__this, method) (( void (*) (InternalEnumerator_1_t2910 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22696_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22697_gshared (InternalEnumerator_1_t2910 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22697(__this, method) (( bool (*) (InternalEnumerator_1_t2910 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22697_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C" int16_t InternalEnumerator_1_get_Current_m22698_gshared (InternalEnumerator_1_t2910 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22698(__this, method) (( int16_t (*) (InternalEnumerator_1_t2910 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22698_gshared)(__this, method)
