﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>
struct ReadOnlyCollection_1_t2309;
// RagdollHelper/BodyPart
struct BodyPart_t400;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<RagdollHelper/BodyPart>
struct IList_1_t2308;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// RagdollHelper/BodyPart[]
struct BodyPartU5BU5D_t2307;
// System.Collections.Generic.IEnumerator`1<RagdollHelper/BodyPart>
struct IEnumerator_1_t2989;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m14578(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2309 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m13592_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14579(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2309 *, BodyPart_t400 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13593_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14580(__this, method) (( void (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13594_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14581(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2309 *, int32_t, BodyPart_t400 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13595_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14582(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2309 *, BodyPart_t400 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13596_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14583(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2309 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13597_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14584(__this, ___index, method) (( BodyPart_t400 * (*) (ReadOnlyCollection_1_t2309 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13598_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14585(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2309 *, int32_t, BodyPart_t400 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13599_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14586(__this, method) (( bool (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13600_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14587(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2309 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13601_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14588(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13602_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m14589(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2309 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m13603_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m14590(__this, method) (( void (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m13604_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m14591(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2309 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m13605_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14592(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2309 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m13606_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m14593(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2309 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m13607_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m14594(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2309 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m13608_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14595(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2309 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m13609_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14596(__this, method) (( bool (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m13610_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14597(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m13611_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14598(__this, method) (( bool (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m13612_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14599(__this, method) (( bool (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m13613_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m14600(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2309 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m13614_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m14601(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2309 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m13615_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::Contains(T)
#define ReadOnlyCollection_1_Contains_m14602(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2309 *, BodyPart_t400 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m13616_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m14603(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2309 *, BodyPartU5BU5D_t2307*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m13617_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m14604(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m13618_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m14605(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2309 *, BodyPart_t400 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m13619_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::get_Count()
#define ReadOnlyCollection_1_get_Count_m14606(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2309 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m13620_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<RagdollHelper/BodyPart>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m14607(__this, ___index, method) (( BodyPart_t400 * (*) (ReadOnlyCollection_1_t2309 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m13621_gshared)(__this, ___index, method)
