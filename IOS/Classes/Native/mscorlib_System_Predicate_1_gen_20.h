﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t339;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.AudioSource>
struct  Predicate_1_t2476  : public MulticastDelegate_t219
{
};
