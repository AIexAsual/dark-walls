﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_DitherOffset
struct CameraFilterPack_Blur_DitherOffset_t50;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_DitherOffset::.ctor()
extern "C" void CameraFilterPack_Blur_DitherOffset__ctor_m303 (CameraFilterPack_Blur_DitherOffset_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_DitherOffset::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_DitherOffset_get_material_m304 (CameraFilterPack_Blur_DitherOffset_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::Start()
extern "C" void CameraFilterPack_Blur_DitherOffset_Start_m305 (CameraFilterPack_Blur_DitherOffset_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_DitherOffset_OnRenderImage_m306 (CameraFilterPack_Blur_DitherOffset_t50 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::OnValidate()
extern "C" void CameraFilterPack_Blur_DitherOffset_OnValidate_m307 (CameraFilterPack_Blur_DitherOffset_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::Update()
extern "C" void CameraFilterPack_Blur_DitherOffset_Update_m308 (CameraFilterPack_Blur_DitherOffset_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_DitherOffset::OnDisable()
extern "C" void CameraFilterPack_Blur_DitherOffset_OnDisable_m309 (CameraFilterPack_Blur_DitherOffset_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
