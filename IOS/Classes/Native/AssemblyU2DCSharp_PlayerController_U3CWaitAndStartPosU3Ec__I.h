﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// PlayerController
struct PlayerController_t356;
// System.Object
#include "mscorlib_System_Object.h"
// PlayerController/<WaitAndStartPos>c__Iterator10
struct  U3CWaitAndStartPosU3Ec__Iterator10_t390  : public Object_t
{
	// System.Single PlayerController/<WaitAndStartPos>c__Iterator10::delay
	float ___delay_0;
	// System.Int32 PlayerController/<WaitAndStartPos>c__Iterator10::$PC
	int32_t ___U24PC_1;
	// System.Object PlayerController/<WaitAndStartPos>c__Iterator10::$current
	Object_t * ___U24current_2;
	// System.Single PlayerController/<WaitAndStartPos>c__Iterator10::<$>delay
	float ___U3CU24U3Edelay_3;
	// PlayerController PlayerController/<WaitAndStartPos>c__Iterator10::<>f__this
	PlayerController_t356 * ___U3CU3Ef__this_4;
};
