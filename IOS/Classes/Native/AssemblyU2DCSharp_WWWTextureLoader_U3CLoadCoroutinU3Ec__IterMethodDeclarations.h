﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WWWTextureLoader/<LoadCoroutin>c__Iterator4
struct U3CLoadCoroutinU3Ec__Iterator4_t306;
// System.Object
struct Object_t;

// System.Void WWWTextureLoader/<LoadCoroutin>c__Iterator4::.ctor()
extern "C" void U3CLoadCoroutinU3Ec__Iterator4__ctor_m1844 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WWWTextureLoader/<LoadCoroutin>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLoadCoroutinU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1845 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object WWWTextureLoader/<LoadCoroutin>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLoadCoroutinU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1846 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean WWWTextureLoader/<LoadCoroutin>c__Iterator4::MoveNext()
extern "C" bool U3CLoadCoroutinU3Ec__Iterator4_MoveNext_m1847 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader/<LoadCoroutin>c__Iterator4::Dispose()
extern "C" void U3CLoadCoroutinU3Ec__Iterator4_Dispose_m1848 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader/<LoadCoroutin>c__Iterator4::Reset()
extern "C" void U3CLoadCoroutinU3Ec__Iterator4_Reset_m1849 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
