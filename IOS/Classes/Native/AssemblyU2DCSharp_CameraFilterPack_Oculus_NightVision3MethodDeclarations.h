﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Oculus_NightVision3
struct CameraFilterPack_Oculus_NightVision3_t164;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Oculus_NightVision3::.ctor()
extern "C" void CameraFilterPack_Oculus_NightVision3__ctor_m1057 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision3::get_material()
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision3_get_material_m1058 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::Start()
extern "C" void CameraFilterPack_Oculus_NightVision3_Start_m1059 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Oculus_NightVision3_OnRenderImage_m1060 (CameraFilterPack_Oculus_NightVision3_t164 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::OnValidate()
extern "C" void CameraFilterPack_Oculus_NightVision3_OnValidate_m1061 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::Update()
extern "C" void CameraFilterPack_Oculus_NightVision3_Update_m1062 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision3::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision3_OnDisable_m1063 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
