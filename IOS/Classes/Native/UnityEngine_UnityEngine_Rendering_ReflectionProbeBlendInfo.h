﻿#pragma once
#include <stdint.h>
// UnityEngine.ReflectionProbe
struct ReflectionProbe_t860;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Rendering.ReflectionProbeBlendInfo
struct  ReflectionProbeBlendInfo_t1030 
{
	// UnityEngine.ReflectionProbe UnityEngine.Rendering.ReflectionProbeBlendInfo::probe
	ReflectionProbe_t860 * ___probe_0;
	// System.Single UnityEngine.Rendering.ReflectionProbeBlendInfo::weight
	float ___weight_1;
};
