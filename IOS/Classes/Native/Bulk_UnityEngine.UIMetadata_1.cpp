﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
// Metadata Definition UnityEngine.UI.ScrollRect/MovementType
extern TypeInfo MovementType_t699_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementTypeMethodDeclarations.h"
static const EncodedMethodIndex MovementType_t699_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair MovementType_t699_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MovementType_t699_0_0_0;
extern const Il2CppType MovementType_t699_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
extern TypeInfo ScrollRect_t702_il2cpp_TypeInfo;
extern const Il2CppType ScrollRect_t702_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata MovementType_t699_DefinitionMetadata = 
{
	&ScrollRect_t702_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MovementType_t699_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MovementType_t699_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3373/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MovementType_t699_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MovementType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MovementType_t699_0_0_0/* byval_arg */
	, &MovementType_t699_1_0_0/* this_arg */
	, &MovementType_t699_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MovementType_t699)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MovementType_t699)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEvent.h"
// Metadata Definition UnityEngine.UI.ScrollRect/ScrollRectEvent
extern TypeInfo ScrollRectEvent_t700_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEventMethodDeclarations.h"
static const EncodedMethodIndex ScrollRectEvent_t700_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484767,
	2147484768,
};
extern const Il2CppType ISerializationCallbackReceiver_t3399_0_0_0;
static Il2CppInterfaceOffsetPair ScrollRectEvent_t700_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRectEvent_t700_0_0_0;
extern const Il2CppType ScrollRectEvent_t700_1_0_0;
extern const Il2CppType UnityEvent_1_t701_0_0_0;
struct ScrollRectEvent_t700;
const Il2CppTypeDefinitionMetadata ScrollRectEvent_t700_DefinitionMetadata = 
{
	&ScrollRect_t702_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrollRectEvent_t700_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t701_0_0_0/* parent */
	, ScrollRectEvent_t700_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3560/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScrollRectEvent_t700_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRectEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ScrollRectEvent_t700_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollRectEvent_t700_0_0_0/* byval_arg */
	, &ScrollRectEvent_t700_1_0_0/* this_arg */
	, &ScrollRectEvent_t700_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRectEvent_t700)/* instance_size */
	, sizeof (ScrollRectEvent_t700)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// Metadata Definition UnityEngine.UI.Selectable
extern TypeInfo Selectable_t636_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
extern const Il2CppType Transition_t704_0_0_0;
extern const Il2CppType SelectionState_t705_0_0_0;
static const Il2CppType* Selectable_t636_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Transition_t704_0_0_0,
	&SelectionState_t705_0_0_0,
};
static const EncodedMethodIndex Selectable_t636_VTable[38] = 
{
	600,
	601,
	602,
	603,
	965,
	966,
	657,
	967,
	659,
	660,
	661,
	662,
	663,
	968,
	969,
	666,
	970,
	971,
	972,
	973,
	974,
	975,
	976,
	977,
	978,
	979,
	980,
	981,
	982,
	983,
	976,
	972,
	973,
	970,
	971,
	974,
	975,
	984,
};
extern const Il2CppType IEventSystemHandler_t2261_0_0_0;
extern const Il2CppType IPointerEnterHandler_t760_0_0_0;
extern const Il2CppType IPointerExitHandler_t761_0_0_0;
extern const Il2CppType IPointerDownHandler_t505_0_0_0;
extern const Il2CppType IPointerUpHandler_t495_0_0_0;
extern const Il2CppType ISelectHandler_t487_0_0_0;
extern const Il2CppType IDeselectHandler_t763_0_0_0;
extern const Il2CppType IMoveHandler_t764_0_0_0;
static const Il2CppType* Selectable_t636_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IPointerEnterHandler_t760_0_0_0,
	&IPointerExitHandler_t761_0_0_0,
	&IPointerDownHandler_t505_0_0_0,
	&IPointerUpHandler_t495_0_0_0,
	&ISelectHandler_t487_0_0_0,
	&IDeselectHandler_t763_0_0_0,
	&IMoveHandler_t764_0_0_0,
};
static Il2CppInterfaceOffsetPair Selectable_t636_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 16},
	{ &IPointerEnterHandler_t760_0_0_0, 16},
	{ &IPointerExitHandler_t761_0_0_0, 17},
	{ &IPointerDownHandler_t505_0_0_0, 18},
	{ &IPointerUpHandler_t495_0_0_0, 19},
	{ &ISelectHandler_t487_0_0_0, 20},
	{ &IDeselectHandler_t763_0_0_0, 21},
	{ &IMoveHandler_t764_0_0_0, 22},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Selectable_t636_0_0_0;
extern const Il2CppType Selectable_t636_1_0_0;
extern const Il2CppType UIBehaviour_t591_0_0_0;
struct Selectable_t636;
const Il2CppTypeDefinitionMetadata Selectable_t636_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Selectable_t636_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Selectable_t636_InterfacesTypeInfos/* implementedInterfaces */
	, Selectable_t636_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, Selectable_t636_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3377/* fieldStart */
	, 3561/* methodStart */
	, -1/* eventStart */
	, 551/* propertyStart */

};
TypeInfo Selectable_t636_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Selectable"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Selectable_t636_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1219/* custom_attributes_cache */
	, &Selectable_t636_0_0_0/* byval_arg */
	, &Selectable_t636_1_0_0/* this_arg */
	, &Selectable_t636_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Selectable_t636)/* instance_size */
	, sizeof (Selectable_t636)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Selectable_t636_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 60/* method_count */
	, 14/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 38/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
// Metadata Definition UnityEngine.UI.Selectable/Transition
extern TypeInfo Transition_t704_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_TransitionMethodDeclarations.h"
static const EncodedMethodIndex Transition_t704_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Transition_t704_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Transition_t704_1_0_0;
const Il2CppTypeDefinitionMetadata Transition_t704_DefinitionMetadata = 
{
	&Selectable_t636_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transition_t704_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Transition_t704_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3391/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Transition_t704_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transition"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transition_t704_0_0_0/* byval_arg */
	, &Transition_t704_1_0_0/* this_arg */
	, &Transition_t704_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transition_t704)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Transition_t704)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
// Metadata Definition UnityEngine.UI.Selectable/SelectionState
extern TypeInfo SelectionState_t705_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionStateMethodDeclarations.h"
static const EncodedMethodIndex SelectionState_t705_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SelectionState_t705_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SelectionState_t705_1_0_0;
const Il2CppTypeDefinitionMetadata SelectionState_t705_DefinitionMetadata = 
{
	&Selectable_t636_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionState_t705_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SelectionState_t705_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3396/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SelectionState_t705_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionState"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SelectionState_t705_0_0_0/* byval_arg */
	, &SelectionState_t705_1_0_0/* this_arg */
	, &SelectionState_t705_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionState_t705)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SelectionState_t705)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 260/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// Metadata Definition UnityEngine.UI.SetPropertyUtility
extern TypeInfo SetPropertyUtility_t709_il2cpp_TypeInfo;
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
extern const Il2CppType SetPropertyUtility_SetStruct_m24131_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition SetPropertyUtility_SetStruct_m24131_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3855 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType SetPropertyUtility_SetClass_m24132_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition SetPropertyUtility_SetClass_m24132_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3857 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex SetPropertyUtility_t709_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SetPropertyUtility_t709_0_0_0;
extern const Il2CppType SetPropertyUtility_t709_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SetPropertyUtility_t709;
const Il2CppTypeDefinitionMetadata SetPropertyUtility_t709_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetPropertyUtility_t709_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3621/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SetPropertyUtility_t709_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetPropertyUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &SetPropertyUtility_t709_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetPropertyUtility_t709_0_0_0/* byval_arg */
	, &SetPropertyUtility_t709_1_0_0/* this_arg */
	, &SetPropertyUtility_t709_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetPropertyUtility_t709)/* instance_size */
	, sizeof (SetPropertyUtility_t709)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// Metadata Definition UnityEngine.UI.Slider
extern TypeInfo Slider_t713_il2cpp_TypeInfo;
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
extern const Il2CppType Direction_t710_0_0_0;
extern const Il2CppType SliderEvent_t711_0_0_0;
extern const Il2CppType Axis_t712_0_0_0;
static const Il2CppType* Slider_t713_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Direction_t710_0_0_0,
	&SliderEvent_t711_0_0_0,
	&Axis_t712_0_0_0,
};
static const EncodedMethodIndex Slider_t713_VTable[48] = 
{
	600,
	601,
	602,
	603,
	965,
	1121,
	657,
	1122,
	659,
	660,
	1123,
	662,
	663,
	968,
	969,
	666,
	970,
	971,
	1124,
	973,
	974,
	975,
	1125,
	977,
	978,
	979,
	1126,
	1127,
	1128,
	1129,
	1125,
	1124,
	973,
	970,
	971,
	974,
	975,
	984,
	1130,
	1131,
	1132,
	1133,
	1134,
	1132,
	1131,
	1130,
	1134,
	1133,
};
extern const Il2CppType IInitializePotentialDragHandler_t507_0_0_0;
extern const Il2CppType IDragHandler_t497_0_0_0;
extern const Il2CppType ICanvasElement_t770_0_0_0;
static const Il2CppType* Slider_t713_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IInitializePotentialDragHandler_t507_0_0_0,
	&IDragHandler_t497_0_0_0,
	&ICanvasElement_t770_0_0_0,
};
static Il2CppInterfaceOffsetPair Slider_t713_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 16},
	{ &IPointerEnterHandler_t760_0_0_0, 16},
	{ &IPointerExitHandler_t761_0_0_0, 17},
	{ &IPointerDownHandler_t505_0_0_0, 18},
	{ &IPointerUpHandler_t495_0_0_0, 19},
	{ &ISelectHandler_t487_0_0_0, 20},
	{ &IDeselectHandler_t763_0_0_0, 21},
	{ &IMoveHandler_t764_0_0_0, 22},
	{ &IInitializePotentialDragHandler_t507_0_0_0, 38},
	{ &IDragHandler_t497_0_0_0, 39},
	{ &ICanvasElement_t770_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Slider_t713_0_0_0;
extern const Il2CppType Slider_t713_1_0_0;
struct Slider_t713;
const Il2CppTypeDefinitionMetadata Slider_t713_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Slider_t713_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Slider_t713_InterfacesTypeInfos/* implementedInterfaces */
	, Slider_t713_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t636_0_0_0/* parent */
	, Slider_t713_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3401/* fieldStart */
	, 3624/* methodStart */
	, -1/* eventStart */
	, 565/* propertyStart */

};
TypeInfo Slider_t713_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slider"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Slider_t713_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1237/* custom_attributes_cache */
	, &Slider_t713_0_0_0/* byval_arg */
	, &Slider_t713_1_0_0/* this_arg */
	, &Slider_t713_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slider_t713)/* instance_size */
	, sizeof (Slider_t713)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 43/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
// Metadata Definition UnityEngine.UI.Slider/Direction
extern TypeInfo Direction_t710_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_DirectionMethodDeclarations.h"
static const EncodedMethodIndex Direction_t710_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Direction_t710_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Direction_t710_1_0_0;
const Il2CppTypeDefinitionMetadata Direction_t710_DefinitionMetadata = 
{
	&Slider_t713_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t710_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Direction_t710_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3416/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Direction_t710_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t710_0_0_0/* byval_arg */
	, &Direction_t710_1_0_0/* this_arg */
	, &Direction_t710_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t710)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t710)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent.h"
// Metadata Definition UnityEngine.UI.Slider/SliderEvent
extern TypeInfo SliderEvent_t711_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEventMethodDeclarations.h"
static const EncodedMethodIndex SliderEvent_t711_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484746,
	2147484747,
};
static Il2CppInterfaceOffsetPair SliderEvent_t711_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SliderEvent_t711_1_0_0;
extern const Il2CppType UnityEvent_1_t694_0_0_0;
struct SliderEvent_t711;
const Il2CppTypeDefinitionMetadata SliderEvent_t711_DefinitionMetadata = 
{
	&Slider_t713_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SliderEvent_t711_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t694_0_0_0/* parent */
	, SliderEvent_t711_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3667/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SliderEvent_t711_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SliderEvent_t711_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderEvent_t711_0_0_0/* byval_arg */
	, &SliderEvent_t711_1_0_0/* this_arg */
	, &SliderEvent_t711_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderEvent_t711)/* instance_size */
	, sizeof (SliderEvent_t711)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
// Metadata Definition UnityEngine.UI.Slider/Axis
extern TypeInfo Axis_t712_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_AxisMethodDeclarations.h"
static const EncodedMethodIndex Axis_t712_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Axis_t712_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t712_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t712_DefinitionMetadata = 
{
	&Slider_t713_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t712_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Axis_t712_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3421/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Axis_t712_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t712_0_0_0/* byval_arg */
	, &Axis_t712_1_0_0/* this_arg */
	, &Axis_t712_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t712)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t712)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// Metadata Definition UnityEngine.UI.SpriteState
extern TypeInfo SpriteState_t708_il2cpp_TypeInfo;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
static const EncodedMethodIndex SpriteState_t708_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SpriteState_t708_0_0_0;
extern const Il2CppType SpriteState_t708_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata SpriteState_t708_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, SpriteState_t708_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3424/* fieldStart */
	, 3668/* methodStart */
	, -1/* eventStart */
	, 577/* propertyStart */

};
TypeInfo SpriteState_t708_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpriteState"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &SpriteState_t708_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpriteState_t708_0_0_0/* byval_arg */
	, &SpriteState_t708_1_0_0/* this_arg */
	, &SpriteState_t708_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpriteState_t708)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpriteState_t708)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// Metadata Definition UnityEngine.UI.StencilMaterial
extern TypeInfo StencilMaterial_t716_il2cpp_TypeInfo;
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
extern const Il2CppType MatEntry_t714_0_0_0;
static const Il2CppType* StencilMaterial_t716_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatEntry_t714_0_0_0,
};
static const EncodedMethodIndex StencilMaterial_t716_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType StencilMaterial_t716_0_0_0;
extern const Il2CppType StencilMaterial_t716_1_0_0;
struct StencilMaterial_t716;
const Il2CppTypeDefinitionMetadata StencilMaterial_t716_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StencilMaterial_t716_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StencilMaterial_t716_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3427/* fieldStart */
	, 3674/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StencilMaterial_t716_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "StencilMaterial"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &StencilMaterial_t716_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StencilMaterial_t716_0_0_0/* byval_arg */
	, &StencilMaterial_t716_1_0_0/* this_arg */
	, &StencilMaterial_t716_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StencilMaterial_t716)/* instance_size */
	, sizeof (StencilMaterial_t716)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StencilMaterial_t716_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry
extern TypeInfo MatEntry_t714_il2cpp_TypeInfo;
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
static const EncodedMethodIndex MatEntry_t714_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MatEntry_t714_1_0_0;
struct MatEntry_t714;
const Il2CppTypeDefinitionMetadata MatEntry_t714_DefinitionMetadata = 
{
	&StencilMaterial_t716_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatEntry_t714_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3428/* fieldStart */
	, 3677/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MatEntry_t714_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatEntry"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &MatEntry_t714_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatEntry_t714_0_0_0/* byval_arg */
	, &MatEntry_t714_1_0_0/* this_arg */
	, &MatEntry_t714_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatEntry_t714)/* instance_size */
	, sizeof (MatEntry_t714)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// Metadata Definition UnityEngine.UI.Text
extern TypeInfo Text_t212_il2cpp_TypeInfo;
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
static const EncodedMethodIndex Text_t212_VTable[58] = 
{
	600,
	601,
	602,
	603,
	655,
	1135,
	657,
	1136,
	659,
	660,
	998,
	999,
	1026,
	1001,
	665,
	1002,
	1003,
	1004,
	1005,
	1006,
	1007,
	1008,
	1027,
	1137,
	1028,
	1029,
	1013,
	1138,
	1003,
	1139,
	1016,
	1140,
	1018,
	1019,
	1005,
	1004,
	1033,
	1033,
	1141,
	1142,
	1143,
	1144,
	1145,
	1146,
	1147,
	1148,
	1149,
	1150,
	1151,
	1141,
	1142,
	1143,
	1144,
	1145,
	1146,
	1147,
	1148,
	1149,
};
extern const Il2CppType ILayoutElement_t776_0_0_0;
static const Il2CppType* Text_t212_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t776_0_0_0,
};
extern const Il2CppType IMaskable_t826_0_0_0;
static Il2CppInterfaceOffsetPair Text_t212_InterfacesOffsets[] = 
{
	{ &IMaskable_t826_0_0_0, 36},
	{ &ICanvasElement_t770_0_0_0, 16},
	{ &ILayoutElement_t776_0_0_0, 38},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Text_t212_0_0_0;
extern const Il2CppType Text_t212_1_0_0;
extern const Il2CppType MaskableGraphic_t670_0_0_0;
struct Text_t212;
const Il2CppTypeDefinitionMetadata Text_t212_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Text_t212_InterfacesTypeInfos/* implementedInterfaces */
	, Text_t212_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t670_0_0_0/* parent */
	, Text_t212_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3432/* fieldStart */
	, 3678/* methodStart */
	, -1/* eventStart */
	, 580/* propertyStart */

};
TypeInfo Text_t212_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Text"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Text_t212_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1249/* custom_attributes_cache */
	, &Text_t212_0_0_0/* byval_arg */
	, &Text_t212_1_0_0/* this_arg */
	, &Text_t212_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Text_t212)/* instance_size */
	, sizeof (Text_t212)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Text_t212_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 24/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 58/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// Metadata Definition UnityEngine.UI.Toggle
extern TypeInfo Toggle_t721_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern const Il2CppType ToggleTransition_t717_0_0_0;
extern const Il2CppType ToggleEvent_t718_0_0_0;
static const Il2CppType* Toggle_t721_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ToggleTransition_t717_0_0_0,
	&ToggleEvent_t718_0_0_0,
};
static const EncodedMethodIndex Toggle_t721_VTable[48] = 
{
	600,
	601,
	602,
	603,
	965,
	1152,
	1153,
	1154,
	659,
	660,
	661,
	662,
	663,
	968,
	969,
	666,
	970,
	971,
	972,
	973,
	974,
	975,
	976,
	977,
	978,
	979,
	980,
	981,
	982,
	983,
	976,
	972,
	973,
	970,
	971,
	974,
	975,
	984,
	1155,
	1156,
	1157,
	1158,
	1159,
	1157,
	1155,
	1156,
	1159,
	1158,
};
extern const Il2CppType IPointerClickHandler_t499_0_0_0;
extern const Il2CppType ISubmitHandler_t765_0_0_0;
static const Il2CppType* Toggle_t721_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t2261_0_0_0,
	&IPointerClickHandler_t499_0_0_0,
	&ISubmitHandler_t765_0_0_0,
	&ICanvasElement_t770_0_0_0,
};
static Il2CppInterfaceOffsetPair Toggle_t721_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t2261_0_0_0, 16},
	{ &IPointerEnterHandler_t760_0_0_0, 16},
	{ &IPointerExitHandler_t761_0_0_0, 17},
	{ &IPointerDownHandler_t505_0_0_0, 18},
	{ &IPointerUpHandler_t495_0_0_0, 19},
	{ &ISelectHandler_t487_0_0_0, 20},
	{ &IDeselectHandler_t763_0_0_0, 21},
	{ &IMoveHandler_t764_0_0_0, 22},
	{ &IPointerClickHandler_t499_0_0_0, 38},
	{ &ISubmitHandler_t765_0_0_0, 39},
	{ &ICanvasElement_t770_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Toggle_t721_0_0_0;
extern const Il2CppType Toggle_t721_1_0_0;
struct Toggle_t721;
const Il2CppTypeDefinitionMetadata Toggle_t721_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Toggle_t721_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Toggle_t721_InterfacesTypeInfos/* implementedInterfaces */
	, Toggle_t721_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t636_0_0_0/* parent */
	, Toggle_t721_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3438/* fieldStart */
	, 3725/* methodStart */
	, -1/* eventStart */
	, 604/* propertyStart */

};
TypeInfo Toggle_t721_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Toggle"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Toggle_t721_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1252/* custom_attributes_cache */
	, &Toggle_t721_0_0_0/* byval_arg */
	, &Toggle_t721_1_0_0/* this_arg */
	, &Toggle_t721_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Toggle_t721)/* instance_size */
	, sizeof (Toggle_t721)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition
extern TypeInfo ToggleTransition_t717_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
static const EncodedMethodIndex ToggleTransition_t717_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ToggleTransition_t717_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleTransition_t717_1_0_0;
const Il2CppTypeDefinitionMetadata ToggleTransition_t717_DefinitionMetadata = 
{
	&Toggle_t721_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleTransition_t717_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ToggleTransition_t717_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3443/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ToggleTransition_t717_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleTransition"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleTransition_t717_0_0_0/* byval_arg */
	, &ToggleTransition_t717_1_0_0/* this_arg */
	, &ToggleTransition_t717_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleTransition_t717)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ToggleTransition_t717)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleEvent
extern TypeInfo ToggleEvent_t718_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
static const EncodedMethodIndex ToggleEvent_t718_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	2147484808,
	2147484809,
};
static Il2CppInterfaceOffsetPair ToggleEvent_t718_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleEvent_t718_1_0_0;
extern const Il2CppType UnityEvent_1_t719_0_0_0;
struct ToggleEvent_t718;
const Il2CppTypeDefinitionMetadata ToggleEvent_t718_DefinitionMetadata = 
{
	&Toggle_t721_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleEvent_t718_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t719_0_0_0/* parent */
	, ToggleEvent_t718_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3743/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ToggleEvent_t718_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleEvent"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ToggleEvent_t718_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleEvent_t718_0_0_0/* byval_arg */
	, &ToggleEvent_t718_1_0_0/* this_arg */
	, &ToggleEvent_t718_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleEvent_t718)/* instance_size */
	, sizeof (ToggleEvent_t718)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// Metadata Definition UnityEngine.UI.ToggleGroup
extern TypeInfo ToggleGroup_t720_il2cpp_TypeInfo;
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
static const EncodedMethodIndex ToggleGroup_t720_VTable[16] = 
{
	600,
	601,
	602,
	603,
	655,
	922,
	657,
	923,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleGroup_t720_0_0_0;
extern const Il2CppType ToggleGroup_t720_1_0_0;
struct ToggleGroup_t720;
const Il2CppTypeDefinitionMetadata ToggleGroup_t720_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, ToggleGroup_t720_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3446/* fieldStart */
	, 3744/* methodStart */
	, -1/* eventStart */
	, 606/* propertyStart */

};
TypeInfo ToggleGroup_t720_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ToggleGroup_t720_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1255/* custom_attributes_cache */
	, &ToggleGroup_t720_0_0_0/* byval_arg */
	, &ToggleGroup_t720_1_0_0/* this_arg */
	, &ToggleGroup_t720_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleGroup_t720)/* instance_size */
	, sizeof (ToggleGroup_t720)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ToggleGroup_t720_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter
extern TypeInfo AspectRatioFitter_t726_il2cpp_TypeInfo;
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
extern const Il2CppType AspectMode_t725_0_0_0;
static const Il2CppType* AspectRatioFitter_t726_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AspectMode_t725_0_0_0,
};
static const EncodedMethodIndex AspectRatioFitter_t726_VTable[20] = 
{
	600,
	601,
	602,
	603,
	655,
	1162,
	657,
	1163,
	659,
	660,
	1164,
	662,
	663,
	664,
	665,
	666,
	1165,
	1166,
	1165,
	1166,
};
extern const Il2CppType ILayoutController_t823_0_0_0;
extern const Il2CppType ILayoutSelfController_t824_0_0_0;
static const Il2CppType* AspectRatioFitter_t726_InterfacesTypeInfos[] = 
{
	&ILayoutController_t823_0_0_0,
	&ILayoutSelfController_t824_0_0_0,
};
static Il2CppInterfaceOffsetPair AspectRatioFitter_t726_InterfacesOffsets[] = 
{
	{ &ILayoutController_t823_0_0_0, 16},
	{ &ILayoutSelfController_t824_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectRatioFitter_t726_0_0_0;
extern const Il2CppType AspectRatioFitter_t726_1_0_0;
struct AspectRatioFitter_t726;
const Il2CppTypeDefinitionMetadata AspectRatioFitter_t726_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AspectRatioFitter_t726_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, AspectRatioFitter_t726_InterfacesTypeInfos/* implementedInterfaces */
	, AspectRatioFitter_t726_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, AspectRatioFitter_t726_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3450/* fieldStart */
	, 3756/* methodStart */
	, -1/* eventStart */
	, 607/* propertyStart */

};
TypeInfo AspectRatioFitter_t726_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectRatioFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &AspectRatioFitter_t726_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1261/* custom_attributes_cache */
	, &AspectRatioFitter_t726_0_0_0/* byval_arg */
	, &AspectRatioFitter_t726_1_0_0/* this_arg */
	, &AspectRatioFitter_t726_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectRatioFitter_t726)/* instance_size */
	, sizeof (AspectRatioFitter_t726)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode
extern TypeInfo AspectMode_t725_il2cpp_TypeInfo;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
static const EncodedMethodIndex AspectMode_t725_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AspectMode_t725_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectMode_t725_1_0_0;
const Il2CppTypeDefinitionMetadata AspectMode_t725_DefinitionMetadata = 
{
	&AspectRatioFitter_t726_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AspectMode_t725_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AspectMode_t725_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3454/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AspectMode_t725_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AspectMode_t725_0_0_0/* byval_arg */
	, &AspectMode_t725_1_0_0/* this_arg */
	, &AspectMode_t725_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectMode_t725)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AspectMode_t725)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// Metadata Definition UnityEngine.UI.CanvasScaler
extern TypeInfo CanvasScaler_t730_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
extern const Il2CppType ScaleMode_t727_0_0_0;
extern const Il2CppType ScreenMatchMode_t728_0_0_0;
extern const Il2CppType Unit_t729_0_0_0;
static const Il2CppType* CanvasScaler_t730_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ScaleMode_t727_0_0_0,
	&ScreenMatchMode_t728_0_0_0,
	&Unit_t729_0_0_0,
};
static const EncodedMethodIndex CanvasScaler_t730_VTable[22] = 
{
	600,
	601,
	602,
	603,
	655,
	1167,
	657,
	1168,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	1169,
	1170,
	1171,
	1172,
	1173,
	1174,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasScaler_t730_0_0_0;
extern const Il2CppType CanvasScaler_t730_1_0_0;
struct CanvasScaler_t730;
const Il2CppTypeDefinitionMetadata CanvasScaler_t730_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CanvasScaler_t730_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, CanvasScaler_t730_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3460/* fieldStart */
	, 3771/* methodStart */
	, -1/* eventStart */
	, 610/* propertyStart */

};
TypeInfo CanvasScaler_t730_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasScaler"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &CanvasScaler_t730_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1264/* custom_attributes_cache */
	, &CanvasScaler_t730_0_0_0/* byval_arg */
	, &CanvasScaler_t730_1_0_0/* this_arg */
	, &CanvasScaler_t730_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasScaler_t730)/* instance_size */
	, sizeof (CanvasScaler_t730)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 10/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 22/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode
extern TypeInfo ScaleMode_t727_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
static const EncodedMethodIndex ScaleMode_t727_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ScaleMode_t727_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScaleMode_t727_1_0_0;
const Il2CppTypeDefinitionMetadata ScaleMode_t727_DefinitionMetadata = 
{
	&CanvasScaler_t730_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScaleMode_t727_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ScaleMode_t727_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3474/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScaleMode_t727_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScaleMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScaleMode_t727_0_0_0/* byval_arg */
	, &ScaleMode_t727_1_0_0/* this_arg */
	, &ScaleMode_t727_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScaleMode_t727)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScaleMode_t727)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode
extern TypeInfo ScreenMatchMode_t728_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
static const EncodedMethodIndex ScreenMatchMode_t728_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ScreenMatchMode_t728_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScreenMatchMode_t728_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenMatchMode_t728_DefinitionMetadata = 
{
	&CanvasScaler_t730_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenMatchMode_t728_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ScreenMatchMode_t728_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3478/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ScreenMatchMode_t728_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenMatchMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenMatchMode_t728_0_0_0/* byval_arg */
	, &ScreenMatchMode_t728_1_0_0/* this_arg */
	, &ScreenMatchMode_t728_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenMatchMode_t728)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenMatchMode_t728)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/Unit
extern TypeInfo Unit_t729_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
static const EncodedMethodIndex Unit_t729_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Unit_t729_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Unit_t729_1_0_0;
const Il2CppTypeDefinitionMetadata Unit_t729_DefinitionMetadata = 
{
	&CanvasScaler_t730_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Unit_t729_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Unit_t729_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3482/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Unit_t729_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Unit"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Unit_t729_0_0_0/* byval_arg */
	, &Unit_t729_1_0_0/* this_arg */
	, &Unit_t729_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Unit_t729)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Unit_t729)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter
extern TypeInfo ContentSizeFitter_t732_il2cpp_TypeInfo;
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
extern const Il2CppType FitMode_t731_0_0_0;
static const Il2CppType* ContentSizeFitter_t732_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FitMode_t731_0_0_0,
};
static const EncodedMethodIndex ContentSizeFitter_t732_VTable[20] = 
{
	600,
	601,
	602,
	603,
	655,
	1175,
	657,
	1176,
	659,
	660,
	1177,
	662,
	663,
	664,
	665,
	666,
	1178,
	1179,
	1178,
	1179,
};
static const Il2CppType* ContentSizeFitter_t732_InterfacesTypeInfos[] = 
{
	&ILayoutController_t823_0_0_0,
	&ILayoutSelfController_t824_0_0_0,
};
static Il2CppInterfaceOffsetPair ContentSizeFitter_t732_InterfacesOffsets[] = 
{
	{ &ILayoutController_t823_0_0_0, 16},
	{ &ILayoutSelfController_t824_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ContentSizeFitter_t732_0_0_0;
extern const Il2CppType ContentSizeFitter_t732_1_0_0;
struct ContentSizeFitter_t732;
const Il2CppTypeDefinitionMetadata ContentSizeFitter_t732_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ContentSizeFitter_t732_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ContentSizeFitter_t732_InterfacesTypeInfos/* implementedInterfaces */
	, ContentSizeFitter_t732_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, ContentSizeFitter_t732_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3488/* fieldStart */
	, 3802/* methodStart */
	, -1/* eventStart */
	, 620/* propertyStart */

};
TypeInfo ContentSizeFitter_t732_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentSizeFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ContentSizeFitter_t732_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1275/* custom_attributes_cache */
	, &ContentSizeFitter_t732_0_0_0/* byval_arg */
	, &ContentSizeFitter_t732_1_0_0/* this_arg */
	, &ContentSizeFitter_t732_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentSizeFitter_t732)/* instance_size */
	, sizeof (ContentSizeFitter_t732)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode
extern TypeInfo FitMode_t731_il2cpp_TypeInfo;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
static const EncodedMethodIndex FitMode_t731_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FitMode_t731_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FitMode_t731_1_0_0;
const Il2CppTypeDefinitionMetadata FitMode_t731_DefinitionMetadata = 
{
	&ContentSizeFitter_t732_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FitMode_t731_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FitMode_t731_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3492/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FitMode_t731_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FitMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FitMode_t731_0_0_0/* byval_arg */
	, &FitMode_t731_1_0_0/* this_arg */
	, &FitMode_t731_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FitMode_t731)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FitMode_t731)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup
extern TypeInfo GridLayoutGroup_t736_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
extern const Il2CppType Corner_t733_0_0_0;
extern const Il2CppType Axis_t734_0_0_0;
extern const Il2CppType Constraint_t735_0_0_0;
static const Il2CppType* GridLayoutGroup_t736_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Corner_t733_0_0_0,
	&Axis_t734_0_0_0,
	&Constraint_t735_0_0_0,
};
static const EncodedMethodIndex GridLayoutGroup_t736_VTable[39] = 
{
	600,
	601,
	602,
	603,
	655,
	1180,
	657,
	1181,
	659,
	660,
	1182,
	662,
	663,
	1183,
	665,
	666,
	1184,
	1185,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1193,
	1194,
	1184,
	1185,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1193,
	1194,
	1195,
};
extern const Il2CppType ILayoutGroup_t821_0_0_0;
static Il2CppInterfaceOffsetPair GridLayoutGroup_t736_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t776_0_0_0, 16},
	{ &ILayoutController_t823_0_0_0, 25},
	{ &ILayoutGroup_t821_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType GridLayoutGroup_t736_0_0_0;
extern const Il2CppType GridLayoutGroup_t736_1_0_0;
extern const Il2CppType LayoutGroup_t737_0_0_0;
struct GridLayoutGroup_t736;
const Il2CppTypeDefinitionMetadata GridLayoutGroup_t736_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GridLayoutGroup_t736_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GridLayoutGroup_t736_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t737_0_0_0/* parent */
	, GridLayoutGroup_t736_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3496/* fieldStart */
	, 3815/* methodStart */
	, -1/* eventStart */
	, 623/* propertyStart */

};
TypeInfo GridLayoutGroup_t736_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GridLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &GridLayoutGroup_t736_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1278/* custom_attributes_cache */
	, &GridLayoutGroup_t736_0_0_0/* byval_arg */
	, &GridLayoutGroup_t736_1_0_0/* this_arg */
	, &GridLayoutGroup_t736_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GridLayoutGroup_t736)/* instance_size */
	, sizeof (GridLayoutGroup_t736)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner
extern TypeInfo Corner_t733_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
static const EncodedMethodIndex Corner_t733_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Corner_t733_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Corner_t733_1_0_0;
const Il2CppTypeDefinitionMetadata Corner_t733_DefinitionMetadata = 
{
	&GridLayoutGroup_t736_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Corner_t733_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Corner_t733_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3502/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Corner_t733_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Corner"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Corner_t733_0_0_0/* byval_arg */
	, &Corner_t733_1_0_0/* this_arg */
	, &Corner_t733_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Corner_t733)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Corner_t733)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis
extern TypeInfo Axis_t734_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
static const EncodedMethodIndex Axis_t734_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Axis_t734_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t734_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t734_DefinitionMetadata = 
{
	&GridLayoutGroup_t736_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t734_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Axis_t734_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3507/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Axis_t734_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t734_0_0_0/* byval_arg */
	, &Axis_t734_1_0_0/* this_arg */
	, &Axis_t734_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t734)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t734)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint
extern TypeInfo Constraint_t735_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
static const EncodedMethodIndex Constraint_t735_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Constraint_t735_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Constraint_t735_1_0_0;
const Il2CppTypeDefinitionMetadata Constraint_t735_DefinitionMetadata = 
{
	&GridLayoutGroup_t736_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Constraint_t735_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Constraint_t735_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3510/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Constraint_t735_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Constraint"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Constraint_t735_0_0_0/* byval_arg */
	, &Constraint_t735_1_0_0/* this_arg */
	, &Constraint_t735_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Constraint_t735)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Constraint_t735)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup
extern TypeInfo HorizontalLayoutGroup_t738_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
static const EncodedMethodIndex HorizontalLayoutGroup_t738_VTable[39] = 
{
	600,
	601,
	602,
	603,
	655,
	1180,
	657,
	1181,
	659,
	660,
	1182,
	662,
	663,
	1183,
	665,
	666,
	1196,
	1197,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1198,
	1199,
	1196,
	1197,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1198,
	1199,
	1195,
};
static Il2CppInterfaceOffsetPair HorizontalLayoutGroup_t738_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t776_0_0_0, 16},
	{ &ILayoutController_t823_0_0_0, 25},
	{ &ILayoutGroup_t821_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalLayoutGroup_t738_0_0_0;
extern const Il2CppType HorizontalLayoutGroup_t738_1_0_0;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t739_0_0_0;
struct HorizontalLayoutGroup_t738;
const Il2CppTypeDefinitionMetadata HorizontalLayoutGroup_t738_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalLayoutGroup_t738_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t739_0_0_0/* parent */
	, HorizontalLayoutGroup_t738_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3833/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HorizontalLayoutGroup_t738_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &HorizontalLayoutGroup_t738_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1285/* custom_attributes_cache */
	, &HorizontalLayoutGroup_t738_0_0_0/* byval_arg */
	, &HorizontalLayoutGroup_t738_1_0_0/* this_arg */
	, &HorizontalLayoutGroup_t738_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalLayoutGroup_t738)/* instance_size */
	, sizeof (HorizontalLayoutGroup_t738)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup
extern TypeInfo HorizontalOrVerticalLayoutGroup_t739_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
static const EncodedMethodIndex HorizontalOrVerticalLayoutGroup_t739_VTable[39] = 
{
	600,
	601,
	602,
	603,
	655,
	1180,
	657,
	1181,
	659,
	660,
	1182,
	662,
	663,
	1183,
	665,
	666,
	1200,
	1201,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1202,
	1203,
	1200,
	0,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	0,
	0,
	1195,
};
static Il2CppInterfaceOffsetPair HorizontalOrVerticalLayoutGroup_t739_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t776_0_0_0, 16},
	{ &ILayoutController_t823_0_0_0, 25},
	{ &ILayoutGroup_t821_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t739_1_0_0;
struct HorizontalOrVerticalLayoutGroup_t739;
const Il2CppTypeDefinitionMetadata HorizontalOrVerticalLayoutGroup_t739_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalOrVerticalLayoutGroup_t739_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t737_0_0_0/* parent */
	, HorizontalOrVerticalLayoutGroup_t739_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3514/* fieldStart */
	, 3838/* methodStart */
	, -1/* eventStart */
	, 629/* propertyStart */

};
TypeInfo HorizontalOrVerticalLayoutGroup_t739_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalOrVerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &HorizontalOrVerticalLayoutGroup_t739_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalOrVerticalLayoutGroup_t739_0_0_0/* byval_arg */
	, &HorizontalOrVerticalLayoutGroup_t739_1_0_0/* this_arg */
	, &HorizontalOrVerticalLayoutGroup_t739_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalOrVerticalLayoutGroup_t739)/* instance_size */
	, sizeof (HorizontalOrVerticalLayoutGroup_t739)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutElement
extern TypeInfo ILayoutElement_t776_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutElement_t776_1_0_0;
struct ILayoutElement_t776;
const Il2CppTypeDefinitionMetadata ILayoutElement_t776_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3847/* methodStart */
	, -1/* eventStart */
	, 632/* propertyStart */

};
TypeInfo ILayoutElement_t776_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ILayoutElement_t776_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutElement_t776_0_0_0/* byval_arg */
	, &ILayoutElement_t776_1_0_0/* this_arg */
	, &ILayoutElement_t776_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutController
extern TypeInfo ILayoutController_t823_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutController_t823_1_0_0;
struct ILayoutController_t823;
const Il2CppTypeDefinitionMetadata ILayoutController_t823_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3856/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ILayoutController_t823_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ILayoutController_t823_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutController_t823_0_0_0/* byval_arg */
	, &ILayoutController_t823_1_0_0/* this_arg */
	, &ILayoutController_t823_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutGroup
extern TypeInfo ILayoutGroup_t821_il2cpp_TypeInfo;
static const Il2CppType* ILayoutGroup_t821_InterfacesTypeInfos[] = 
{
	&ILayoutController_t823_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutGroup_t821_1_0_0;
struct ILayoutGroup_t821;
const Il2CppTypeDefinitionMetadata ILayoutGroup_t821_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutGroup_t821_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ILayoutGroup_t821_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ILayoutGroup_t821_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutGroup_t821_0_0_0/* byval_arg */
	, &ILayoutGroup_t821_1_0_0/* this_arg */
	, &ILayoutGroup_t821_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutSelfController
extern TypeInfo ILayoutSelfController_t824_il2cpp_TypeInfo;
static const Il2CppType* ILayoutSelfController_t824_InterfacesTypeInfos[] = 
{
	&ILayoutController_t823_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutSelfController_t824_1_0_0;
struct ILayoutSelfController_t824;
const Il2CppTypeDefinitionMetadata ILayoutSelfController_t824_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutSelfController_t824_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ILayoutSelfController_t824_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutSelfController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ILayoutSelfController_t824_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutSelfController_t824_0_0_0/* byval_arg */
	, &ILayoutSelfController_t824_1_0_0/* this_arg */
	, &ILayoutSelfController_t824_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutIgnorer
extern TypeInfo ILayoutIgnorer_t820_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutIgnorer_t820_0_0_0;
extern const Il2CppType ILayoutIgnorer_t820_1_0_0;
struct ILayoutIgnorer_t820;
const Il2CppTypeDefinitionMetadata ILayoutIgnorer_t820_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3858/* methodStart */
	, -1/* eventStart */
	, 639/* propertyStart */

};
TypeInfo ILayoutIgnorer_t820_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutIgnorer"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ILayoutIgnorer_t820_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutIgnorer_t820_0_0_0/* byval_arg */
	, &ILayoutIgnorer_t820_1_0_0/* this_arg */
	, &ILayoutIgnorer_t820_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// Metadata Definition UnityEngine.UI.LayoutElement
extern TypeInfo LayoutElement_t740_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
static const EncodedMethodIndex LayoutElement_t740_VTable[43] = 
{
	600,
	601,
	602,
	603,
	655,
	1204,
	657,
	1205,
	659,
	660,
	661,
	1206,
	1207,
	1208,
	665,
	666,
	1209,
	1210,
	1211,
	1212,
	1213,
	1214,
	1215,
	1216,
	1217,
	1218,
	1218,
	1219,
	1209,
	1210,
	1211,
	1220,
	1214,
	1221,
	1212,
	1222,
	1215,
	1223,
	1213,
	1224,
	1216,
	1225,
	1217,
};
static const Il2CppType* LayoutElement_t740_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t776_0_0_0,
	&ILayoutIgnorer_t820_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutElement_t740_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t776_0_0_0, 16},
	{ &ILayoutIgnorer_t820_0_0_0, 25},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutElement_t740_0_0_0;
extern const Il2CppType LayoutElement_t740_1_0_0;
struct LayoutElement_t740;
const Il2CppTypeDefinitionMetadata LayoutElement_t740_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutElement_t740_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutElement_t740_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, LayoutElement_t740_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3517/* fieldStart */
	, 3859/* methodStart */
	, -1/* eventStart */
	, 640/* propertyStart */

};
TypeInfo LayoutElement_t740_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &LayoutElement_t740_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1289/* custom_attributes_cache */
	, &LayoutElement_t740_0_0_0/* byval_arg */
	, &LayoutElement_t740_1_0_0/* this_arg */
	, &LayoutElement_t740_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutElement_t740)/* instance_size */
	, sizeof (LayoutElement_t740)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 8/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 43/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// Metadata Definition UnityEngine.UI.LayoutGroup
extern TypeInfo LayoutGroup_t737_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern const Il2CppType LayoutGroup_SetProperty_m24148_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition LayoutGroup_SetProperty_m24148_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 3895 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex LayoutGroup_t737_VTable[39] = 
{
	600,
	601,
	602,
	603,
	655,
	1180,
	657,
	1181,
	659,
	660,
	1182,
	662,
	663,
	1183,
	665,
	666,
	1200,
	1201,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1202,
	1203,
	1200,
	0,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	0,
	0,
	1195,
};
static const Il2CppType* LayoutGroup_t737_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t776_0_0_0,
	&ILayoutController_t823_0_0_0,
	&ILayoutGroup_t821_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutGroup_t737_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t776_0_0_0, 16},
	{ &ILayoutController_t823_0_0_0, 25},
	{ &ILayoutGroup_t821_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutGroup_t737_1_0_0;
struct LayoutGroup_t737;
const Il2CppTypeDefinitionMetadata LayoutGroup_t737_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutGroup_t737_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutGroup_t737_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, LayoutGroup_t737_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3524/* fieldStart */
	, 3883/* methodStart */
	, -1/* eventStart */
	, 648/* propertyStart */

};
TypeInfo LayoutGroup_t737_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &LayoutGroup_t737_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1297/* custom_attributes_cache */
	, &LayoutGroup_t737_0_0_0/* byval_arg */
	, &LayoutGroup_t737_1_0_0/* this_arg */
	, &LayoutGroup_t737_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutGroup_t737)/* instance_size */
	, sizeof (LayoutGroup_t737)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 32/* method_count */
	, 12/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// Metadata Definition UnityEngine.UI.LayoutRebuilder
extern TypeInfo LayoutRebuilder_t745_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
static const EncodedMethodIndex LayoutRebuilder_t745_VTable[8] = 
{
	652,
	601,
	1226,
	1227,
	1228,
	1229,
	1230,
	1231,
};
extern const Il2CppType IEquatable_1_t3626_0_0_0;
static const Il2CppType* LayoutRebuilder_t745_InterfacesTypeInfos[] = 
{
	&ICanvasElement_t770_0_0_0,
	&IEquatable_1_t3626_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutRebuilder_t745_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t770_0_0_0, 4},
	{ &IEquatable_1_t3626_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutRebuilder_t745_0_0_0;
extern const Il2CppType LayoutRebuilder_t745_1_0_0;
const Il2CppTypeDefinitionMetadata LayoutRebuilder_t745_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutRebuilder_t745_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutRebuilder_t745_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, LayoutRebuilder_t745_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3532/* fieldStart */
	, 3915/* methodStart */
	, -1/* eventStart */
	, 660/* propertyStart */

};
TypeInfo LayoutRebuilder_t745_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutRebuilder"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &LayoutRebuilder_t745_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutRebuilder_t745_0_0_0/* byval_arg */
	, &LayoutRebuilder_t745_1_0_0/* this_arg */
	, &LayoutRebuilder_t745_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutRebuilder_t745)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayoutRebuilder_t745)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutRebuilder_t745_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 265/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// Metadata Definition UnityEngine.UI.LayoutUtility
extern TypeInfo LayoutUtility_t747_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
static const EncodedMethodIndex LayoutUtility_t747_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutUtility_t747_0_0_0;
extern const Il2CppType LayoutUtility_t747_1_0_0;
struct LayoutUtility_t747;
const Il2CppTypeDefinitionMetadata LayoutUtility_t747_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutUtility_t747_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3539/* fieldStart */
	, 3936/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LayoutUtility_t747_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &LayoutUtility_t747_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutUtility_t747_0_0_0/* byval_arg */
	, &LayoutUtility_t747_1_0_0/* this_arg */
	, &LayoutUtility_t747_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutUtility_t747)/* instance_size */
	, sizeof (LayoutUtility_t747)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutUtility_t747_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.VerticalLayoutGroup
extern TypeInfo VerticalLayoutGroup_t748_il2cpp_TypeInfo;
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
static const EncodedMethodIndex VerticalLayoutGroup_t748_VTable[39] = 
{
	600,
	601,
	602,
	603,
	655,
	1180,
	657,
	1181,
	659,
	660,
	1182,
	662,
	663,
	1183,
	665,
	666,
	1232,
	1233,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1234,
	1235,
	1232,
	1233,
	1186,
	1187,
	1188,
	1189,
	1190,
	1191,
	1192,
	1234,
	1235,
	1195,
};
static Il2CppInterfaceOffsetPair VerticalLayoutGroup_t748_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t776_0_0_0, 16},
	{ &ILayoutController_t823_0_0_0, 25},
	{ &ILayoutGroup_t821_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType VerticalLayoutGroup_t748_0_0_0;
extern const Il2CppType VerticalLayoutGroup_t748_1_0_0;
struct VerticalLayoutGroup_t748;
const Il2CppTypeDefinitionMetadata VerticalLayoutGroup_t748_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalLayoutGroup_t748_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t739_0_0_0/* parent */
	, VerticalLayoutGroup_t748_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3955/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo VerticalLayoutGroup_t748_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &VerticalLayoutGroup_t748_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1326/* custom_attributes_cache */
	, &VerticalLayoutGroup_t748_0_0_0/* byval_arg */
	, &VerticalLayoutGroup_t748_1_0_0/* this_arg */
	, &VerticalLayoutGroup_t748_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalLayoutGroup_t748)/* instance_size */
	, sizeof (VerticalLayoutGroup_t748)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMaterialModifier
extern TypeInfo IMaterialModifier_t794_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMaterialModifier_t794_0_0_0;
extern const Il2CppType IMaterialModifier_t794_1_0_0;
struct IMaterialModifier_t794;
const Il2CppTypeDefinitionMetadata IMaterialModifier_t794_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 3960/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMaterialModifier_t794_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMaterialModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &IMaterialModifier_t794_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMaterialModifier_t794_0_0_0/* byval_arg */
	, &IMaterialModifier_t794_1_0_0/* this_arg */
	, &IMaterialModifier_t794_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
// Metadata Definition UnityEngine.UI.Mask
extern TypeInfo Mask_t749_il2cpp_TypeInfo;
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_MaskMethodDeclarations.h"
static const EncodedMethodIndex Mask_t749_VTable[24] = 
{
	600,
	601,
	602,
	603,
	655,
	1236,
	657,
	1237,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	1238,
	1239,
	1240,
	1241,
	1239,
	1238,
	1240,
	1241,
};
extern const Il2CppType IGraphicEnabledDisabled_t795_0_0_0;
extern const Il2CppType IMask_t814_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t798_0_0_0;
static const Il2CppType* Mask_t749_InterfacesTypeInfos[] = 
{
	&IGraphicEnabledDisabled_t795_0_0_0,
	&IMask_t814_0_0_0,
	&ICanvasRaycastFilter_t798_0_0_0,
	&IMaterialModifier_t794_0_0_0,
};
static Il2CppInterfaceOffsetPair Mask_t749_InterfacesOffsets[] = 
{
	{ &IGraphicEnabledDisabled_t795_0_0_0, 16},
	{ &IMask_t814_0_0_0, 17},
	{ &ICanvasRaycastFilter_t798_0_0_0, 18},
	{ &IMaterialModifier_t794_0_0_0, 19},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Mask_t749_0_0_0;
extern const Il2CppType Mask_t749_1_0_0;
struct Mask_t749;
const Il2CppTypeDefinitionMetadata Mask_t749_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Mask_t749_InterfacesTypeInfos/* implementedInterfaces */
	, Mask_t749_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, Mask_t749_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3547/* fieldStart */
	, 3961/* methodStart */
	, -1/* eventStart */
	, 661/* propertyStart */

};
TypeInfo Mask_t749_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mask"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Mask_t749_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1327/* custom_attributes_cache */
	, &Mask_t749_0_0_0/* byval_arg */
	, &Mask_t749_1_0_0/* this_arg */
	, &Mask_t749_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mask_t749)/* instance_size */
	, sizeof (Mask_t749)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1
extern TypeInfo IndexedSet_1_t3397_il2cpp_TypeInfo;
static const EncodedMethodIndex IndexedSet_1_t3397_VTable[18] = 
{
	626,
	601,
	627,
	628,
	1242,
	1243,
	1244,
	1245,
	1246,
	1247,
	1248,
	1249,
	1250,
	1251,
	1252,
	1253,
	1254,
	1255,
};
extern const Il2CppType IList_1_t3627_0_0_0;
extern const Il2CppType ICollection_1_t3628_0_0_0;
extern const Il2CppType IEnumerable_1_t3629_0_0_0;
extern const Il2CppType IEnumerable_t1097_0_0_0;
static const Il2CppType* IndexedSet_1_t3397_InterfacesTypeInfos[] = 
{
	&IList_1_t3627_0_0_0,
	&ICollection_1_t3628_0_0_0,
	&IEnumerable_1_t3629_0_0_0,
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair IndexedSet_1_t3397_InterfacesOffsets[] = 
{
	{ &IList_1_t3627_0_0_0, 4},
	{ &ICollection_1_t3628_0_0_0, 9},
	{ &IEnumerable_1_t3629_0_0_0, 16},
	{ &IEnumerable_t1097_0_0_0, 17},
};
extern const Il2CppType List_1_t3630_0_0_0;
extern const Il2CppType Dictionary_2_t3631_0_0_0;
extern const Il2CppRGCTXDefinition IndexedSet_1_t3397_RGCTXData[23] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6264 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5417 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6265 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5418 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5419 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5420 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5421 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5422 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5423 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5424 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5425 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5426 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5427 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5428 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5429 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5430 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5431 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5432 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5433 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5434 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5435 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5436 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IndexedSet_1_t3397_0_0_0;
extern const Il2CppType IndexedSet_1_t3397_1_0_0;
struct IndexedSet_1_t3397;
const Il2CppTypeDefinitionMetadata IndexedSet_1_t3397_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IndexedSet_1_t3397_InterfacesTypeInfos/* implementedInterfaces */
	, IndexedSet_1_t3397_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IndexedSet_1_t3397_VTable/* vtableMethods */
	, IndexedSet_1_t3397_RGCTXData/* rgctxDefinition */
	, 3551/* fieldStart */
	, 3974/* methodStart */
	, -1/* eventStart */
	, 664/* propertyStart */

};
TypeInfo IndexedSet_1_t3397_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexedSet`1"/* name */
	, "UnityEngine.UI.Collections"/* namespaze */
	, NULL/* methods */
	, &IndexedSet_1_t3397_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1329/* custom_attributes_cache */
	, &IndexedSet_1_t3397_0_0_0/* byval_arg */
	, &IndexedSet_1_t3397_1_0_0/* this_arg */
	, &IndexedSet_1_t3397_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 18/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPool.h"
// Metadata Definition UnityEngine.UI.CanvasListPool
extern TypeInfo CanvasListPool_t752_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPoolMethodDeclarations.h"
static const EncodedMethodIndex CanvasListPool_t752_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasListPool_t752_0_0_0;
extern const Il2CppType CanvasListPool_t752_1_0_0;
struct CanvasListPool_t752;
const Il2CppTypeDefinitionMetadata CanvasListPool_t752_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CanvasListPool_t752_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3553/* fieldStart */
	, 3991/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CanvasListPool_t752_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &CanvasListPool_t752_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasListPool_t752_0_0_0/* byval_arg */
	, &CanvasListPool_t752_1_0_0/* this_arg */
	, &CanvasListPool_t752_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasListPool_t752)/* instance_size */
	, sizeof (CanvasListPool_t752)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CanvasListPool_t752_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPool.h"
// Metadata Definition UnityEngine.UI.ComponentListPool
extern TypeInfo ComponentListPool_t755_il2cpp_TypeInfo;
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPoolMethodDeclarations.h"
static const EncodedMethodIndex ComponentListPool_t755_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ComponentListPool_t755_0_0_0;
extern const Il2CppType ComponentListPool_t755_1_0_0;
struct ComponentListPool_t755;
const Il2CppTypeDefinitionMetadata ComponentListPool_t755_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ComponentListPool_t755_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3555/* fieldStart */
	, 3995/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ComponentListPool_t755_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ComponentListPool_t755_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentListPool_t755_0_0_0/* byval_arg */
	, &ComponentListPool_t755_1_0_0/* this_arg */
	, &ComponentListPool_t755_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentListPool_t755)/* instance_size */
	, sizeof (ComponentListPool_t755)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ComponentListPool_t755_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ObjectPool`1
extern TypeInfo ObjectPool_1_t3398_il2cpp_TypeInfo;
static const EncodedMethodIndex ObjectPool_1_t3398_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern const Il2CppType Stack_1_t3632_0_0_0;
extern const Il2CppType ObjectPool_1_t3398_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition ObjectPool_1_t3398_RGCTXData[13] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6270 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5437 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5438 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5439 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5440 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 3918 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5441 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5442 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5443 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5444 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5445 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5446 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ObjectPool_1_t3398_0_0_0;
extern const Il2CppType ObjectPool_1_t3398_1_0_0;
struct ObjectPool_1_t3398;
const Il2CppTypeDefinitionMetadata ObjectPool_1_t3398_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectPool_1_t3398_VTable/* vtableMethods */
	, ObjectPool_1_t3398_RGCTXData/* rgctxDefinition */
	, 3557/* fieldStart */
	, 3999/* methodStart */
	, -1/* eventStart */
	, 667/* propertyStart */

};
TypeInfo ObjectPool_1_t3398_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &ObjectPool_1_t3398_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectPool_1_t3398_0_0_0/* byval_arg */
	, &ObjectPool_1_t3398_1_0_0/* this_arg */
	, &ObjectPool_1_t3398_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 19/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// Metadata Definition UnityEngine.UI.BaseVertexEffect
extern TypeInfo BaseVertexEffect_t756_il2cpp_TypeInfo;
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
static const EncodedMethodIndex BaseVertexEffect_t756_VTable[18] = 
{
	600,
	601,
	602,
	603,
	655,
	1256,
	657,
	1257,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	1258,
	0,
};
extern const Il2CppType IVertexModifier_t796_0_0_0;
static const Il2CppType* BaseVertexEffect_t756_InterfacesTypeInfos[] = 
{
	&IVertexModifier_t796_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseVertexEffect_t756_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t796_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseVertexEffect_t756_0_0_0;
extern const Il2CppType BaseVertexEffect_t756_1_0_0;
struct BaseVertexEffect_t756;
const Il2CppTypeDefinitionMetadata BaseVertexEffect_t756_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BaseVertexEffect_t756_InterfacesTypeInfos/* implementedInterfaces */
	, BaseVertexEffect_t756_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t591_0_0_0/* parent */
	, BaseVertexEffect_t756_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3561/* fieldStart */
	, 4006/* methodStart */
	, -1/* eventStart */
	, 670/* propertyStart */

};
TypeInfo BaseVertexEffect_t756_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseVertexEffect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &BaseVertexEffect_t756_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1337/* custom_attributes_cache */
	, &BaseVertexEffect_t756_0_0_0/* byval_arg */
	, &BaseVertexEffect_t756_1_0_0/* this_arg */
	, &BaseVertexEffect_t756_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseVertexEffect_t756)/* instance_size */
	, sizeof (BaseVertexEffect_t756)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IVertexModifier
extern TypeInfo IVertexModifier_t796_il2cpp_TypeInfo;
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IVertexModifier_t796_1_0_0;
struct IVertexModifier_t796;
const Il2CppTypeDefinitionMetadata IVertexModifier_t796_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4011/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IVertexModifier_t796_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IVertexModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &IVertexModifier_t796_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IVertexModifier_t796_0_0_0/* byval_arg */
	, &IVertexModifier_t796_1_0_0/* this_arg */
	, &IVertexModifier_t796_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// Metadata Definition UnityEngine.UI.Outline
extern TypeInfo Outline_t757_il2cpp_TypeInfo;
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
static const EncodedMethodIndex Outline_t757_VTable[18] = 
{
	600,
	601,
	602,
	603,
	655,
	1256,
	657,
	1257,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	1259,
	1259,
};
static Il2CppInterfaceOffsetPair Outline_t757_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t796_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Outline_t757_0_0_0;
extern const Il2CppType Outline_t757_1_0_0;
extern const Il2CppType Shadow_t758_0_0_0;
struct Outline_t757;
const Il2CppTypeDefinitionMetadata Outline_t757_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Outline_t757_InterfacesOffsets/* interfaceOffsets */
	, &Shadow_t758_0_0_0/* parent */
	, Outline_t757_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4012/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Outline_t757_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Outline"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Outline_t757_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1338/* custom_attributes_cache */
	, &Outline_t757_0_0_0/* byval_arg */
	, &Outline_t757_1_0_0/* this_arg */
	, &Outline_t757_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Outline_t757)/* instance_size */
	, sizeof (Outline_t757)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// Metadata Definition UnityEngine.UI.PositionAsUV1
extern TypeInfo PositionAsUV1_t759_il2cpp_TypeInfo;
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
static const EncodedMethodIndex PositionAsUV1_t759_VTable[18] = 
{
	600,
	601,
	602,
	603,
	655,
	1256,
	657,
	1257,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	1260,
	1260,
};
static Il2CppInterfaceOffsetPair PositionAsUV1_t759_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t796_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PositionAsUV1_t759_0_0_0;
extern const Il2CppType PositionAsUV1_t759_1_0_0;
struct PositionAsUV1_t759;
const Il2CppTypeDefinitionMetadata PositionAsUV1_t759_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PositionAsUV1_t759_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t756_0_0_0/* parent */
	, PositionAsUV1_t759_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 4014/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PositionAsUV1_t759_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAsUV1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &PositionAsUV1_t759_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1339/* custom_attributes_cache */
	, &PositionAsUV1_t759_0_0_0/* byval_arg */
	, &PositionAsUV1_t759_1_0_0/* this_arg */
	, &PositionAsUV1_t759_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAsUV1_t759)/* instance_size */
	, sizeof (PositionAsUV1_t759)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// Metadata Definition UnityEngine.UI.Shadow
extern TypeInfo Shadow_t758_il2cpp_TypeInfo;
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
static const EncodedMethodIndex Shadow_t758_VTable[18] = 
{
	600,
	601,
	602,
	603,
	655,
	1256,
	657,
	1257,
	659,
	660,
	661,
	662,
	663,
	664,
	665,
	666,
	1261,
	1261,
};
static Il2CppInterfaceOffsetPair Shadow_t758_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t796_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Shadow_t758_1_0_0;
struct Shadow_t758;
const Il2CppTypeDefinitionMetadata Shadow_t758_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Shadow_t758_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t756_0_0_0/* parent */
	, Shadow_t758_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 3562/* fieldStart */
	, 4016/* methodStart */
	, -1/* eventStart */
	, 671/* propertyStart */

};
TypeInfo Shadow_t758_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Shadow"/* name */
	, "UnityEngine.UI"/* namespaze */
	, NULL/* methods */
	, &Shadow_t758_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 1340/* custom_attributes_cache */
	, &Shadow_t758_0_0_0/* byval_arg */
	, &Shadow_t758_1_0_0/* this_arg */
	, &Shadow_t758_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Shadow_t758)/* instance_size */
	, sizeof (Shadow_t758)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
