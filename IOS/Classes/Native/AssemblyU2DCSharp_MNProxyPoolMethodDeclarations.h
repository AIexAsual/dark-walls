﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNProxyPool
struct MNProxyPool_t284;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t470;

// System.Void MNProxyPool::.ctor()
extern "C" void MNProxyPool__ctor_m1762 (MNProxyPool_t284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNProxyPool::CallStatic(System.String,System.String,System.Object[])
extern "C" void MNProxyPool_CallStatic_m1763 (Object_t * __this /* static, unused */, String_t* ___className, String_t* ___methodName, ObjectU5BU5D_t470* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
