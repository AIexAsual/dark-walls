﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Transform>
struct Predicate_1_t2276;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t243;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m14237(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2276 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m13671_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Transform>::Invoke(T)
#define Predicate_1_Invoke_m14238(__this, ___obj, method) (( bool (*) (Predicate_1_t2276 *, Transform_t243 *, const MethodInfo*))Predicate_1_Invoke_m13672_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Transform>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m14239(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2276 *, Transform_t243 *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m13673_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m14240(__this, ___result, method) (( bool (*) (Predicate_1_t2276 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m13674_gshared)(__this, ___result, method)
