﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t803;
// UnityEngine.UI.Graphic
struct Graphic_t654;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t3113;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t2571;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t2573;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t657;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m4722(__this, method) (( void (*) (IndexedSet_1_t803 *, const MethodInfo*))IndexedSet_1__ctor_m17771_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m18554(__this, method) (( Object_t * (*) (IndexedSet_1_t803 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17773_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m18555(__this, ___item, method) (( void (*) (IndexedSet_1_t803 *, Graphic_t654 *, const MethodInfo*))IndexedSet_1_Add_m17775_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m18556(__this, ___item, method) (( bool (*) (IndexedSet_1_t803 *, Graphic_t654 *, const MethodInfo*))IndexedSet_1_Remove_m17777_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m18557(__this, method) (( Object_t* (*) (IndexedSet_1_t803 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m17779_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m18558(__this, method) (( void (*) (IndexedSet_1_t803 *, const MethodInfo*))IndexedSet_1_Clear_m17781_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m18559(__this, ___item, method) (( bool (*) (IndexedSet_1_t803 *, Graphic_t654 *, const MethodInfo*))IndexedSet_1_Contains_m17783_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m18560(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t803 *, GraphicU5BU5D_t2571*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m17785_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m18561(__this, method) (( int32_t (*) (IndexedSet_1_t803 *, const MethodInfo*))IndexedSet_1_get_Count_m17787_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m18562(__this, method) (( bool (*) (IndexedSet_1_t803 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m17789_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m18563(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t803 *, Graphic_t654 *, const MethodInfo*))IndexedSet_1_IndexOf_m17791_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m18564(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t803 *, int32_t, Graphic_t654 *, const MethodInfo*))IndexedSet_1_Insert_m17793_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m18565(__this, ___index, method) (( void (*) (IndexedSet_1_t803 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m17795_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m18566(__this, ___index, method) (( Graphic_t654 * (*) (IndexedSet_1_t803 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m17797_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m18567(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t803 *, int32_t, Graphic_t654 *, const MethodInfo*))IndexedSet_1_set_Item_m17799_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m18568(__this, ___match, method) (( void (*) (IndexedSet_1_t803 *, Predicate_1_t2573 *, const MethodInfo*))IndexedSet_1_RemoveAll_m17800_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m18569(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t803 *, Comparison_1_t657 *, const MethodInfo*))IndexedSet_1_Sort_m17801_gshared)(__this, ___sortLayoutFunction, method)
