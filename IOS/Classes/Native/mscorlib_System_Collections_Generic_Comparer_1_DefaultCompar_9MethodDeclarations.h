﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<iTween/LoopType>
struct DefaultComparer_t2465;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<iTween/LoopType>::.ctor()
extern "C" void DefaultComparer__ctor_m16636_gshared (DefaultComparer_t2465 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16636(__this, method) (( void (*) (DefaultComparer_t2465 *, const MethodInfo*))DefaultComparer__ctor_m16636_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<iTween/LoopType>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m16637_gshared (DefaultComparer_t2465 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m16637(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2465 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m16637_gshared)(__this, ___x, ___y, method)
