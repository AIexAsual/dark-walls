﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t606;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct UnityAction_1_t607;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t767;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m4599(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t606 *, UnityAction_1_t607 *, UnityAction_1_t607 *, const MethodInfo*))ObjectPool_1__ctor_m13999_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countAll()
#define ObjectPool_1_get_countAll_m14000(__this, method) (( int32_t (*) (ObjectPool_1_t606 *, const MethodInfo*))ObjectPool_1_get_countAll_m14001_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m14002(__this, ___value, method) (( void (*) (ObjectPool_1_t606 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m14003_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countActive()
#define ObjectPool_1_get_countActive_m14004(__this, method) (( int32_t (*) (ObjectPool_1_t606 *, const MethodInfo*))ObjectPool_1_get_countActive_m14005_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m14006(__this, method) (( int32_t (*) (ObjectPool_1_t606 *, const MethodInfo*))ObjectPool_1_get_countInactive_m14007_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Get()
#define ObjectPool_1_Get_m14008(__this, method) (( List_1_t767 * (*) (ObjectPool_1_t606 *, const MethodInfo*))ObjectPool_1_Get_m14009_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Release(T)
#define ObjectPool_1_Release_m14010(__this, ___element, method) (( void (*) (ObjectPool_1_t606 *, List_1_t767 *, const MethodInfo*))ObjectPool_1_Release_m14011_gshared)(__this, ___element, method)
