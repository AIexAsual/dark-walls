﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
struct Enumerator_t2710;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2705;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20458_gshared (Enumerator_t2710 * __this, Dictionary_2_t2705 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m20458(__this, ___host, method) (( void (*) (Enumerator_t2710 *, Dictionary_2_t2705 *, const MethodInfo*))Enumerator__ctor_m20458_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20459_gshared (Enumerator_t2710 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20459(__this, method) (( Object_t * (*) (Enumerator_t2710 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20459_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m20460_gshared (Enumerator_t2710 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20460(__this, method) (( void (*) (Enumerator_t2710 *, const MethodInfo*))Enumerator_Dispose_m20460_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20461_gshared (Enumerator_t2710 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20461(__this, method) (( bool (*) (Enumerator_t2710 *, const MethodInfo*))Enumerator_MoveNext_m20461_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m20462_gshared (Enumerator_t2710 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20462(__this, method) (( Object_t * (*) (Enumerator_t2710 *, const MethodInfo*))Enumerator_get_Current_m20462_gshared)(__this, method)
