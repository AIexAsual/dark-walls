﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_Sepia
struct CameraFilterPack_Color_Sepia_t67;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_Sepia::.ctor()
extern "C" void CameraFilterPack_Color_Sepia__ctor_m420 (CameraFilterPack_Color_Sepia_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Sepia::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_Sepia_get_material_m421 (CameraFilterPack_Color_Sepia_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::Start()
extern "C" void CameraFilterPack_Color_Sepia_Start_m422 (CameraFilterPack_Color_Sepia_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_Sepia_OnRenderImage_m423 (CameraFilterPack_Color_Sepia_t67 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::Update()
extern "C" void CameraFilterPack_Color_Sepia_Update_m424 (CameraFilterPack_Color_Sepia_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Sepia::OnDisable()
extern "C" void CameraFilterPack_Color_Sepia_OnDisable_m425 (CameraFilterPack_Color_Sepia_t67 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
