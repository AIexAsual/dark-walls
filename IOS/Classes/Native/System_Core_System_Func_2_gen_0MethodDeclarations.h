﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<CardboardEye,CardboardHead>
struct Func_2_t266;
// System.Object
struct Object_t;
// CardboardHead
struct CardboardHead_t244;
// CardboardEye
struct CardboardEye_t240;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<CardboardEye,CardboardHead>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_6MethodDeclarations.h"
#define Func_2__ctor_m3030(__this, ___object, ___method, method) (( void (*) (Func_2_t266 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m14279_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<CardboardEye,CardboardHead>::Invoke(T)
#define Func_2_Invoke_m14280(__this, ___arg1, method) (( CardboardHead_t244 * (*) (Func_2_t266 *, CardboardEye_t240 *, const MethodInfo*))Func_2_Invoke_m14281_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<CardboardEye,CardboardHead>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m14282(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t266 *, CardboardEye_t240 *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m14283_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<CardboardEye,CardboardHead>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m14284(__this, ___result, method) (( CardboardHead_t244 * (*) (Func_2_t266 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m14285_gshared)(__this, ___result, method)
