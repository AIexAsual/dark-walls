﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIWordWrapSizer
struct GUIWordWrapSizer_t880;
// UnityEngine.GUIStyle
struct GUIStyle_t273;
// UnityEngine.GUIContent
struct GUIContent_t807;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t544;

// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C" void GUIWordWrapSizer__ctor_m5206 (GUIWordWrapSizer_t880 * __this, GUIStyle_t273 * ____style, GUIContent_t807 * ____content, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m5207 (GUIWordWrapSizer_t880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m5208 (GUIWordWrapSizer_t880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
