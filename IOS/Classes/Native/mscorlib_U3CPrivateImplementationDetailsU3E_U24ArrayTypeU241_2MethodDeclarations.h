﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$136
struct U24ArrayTypeU24136_t2161;
struct U24ArrayTypeU24136_t2161_marshaled;

void U24ArrayTypeU24136_t2161_marshal(const U24ArrayTypeU24136_t2161& unmarshaled, U24ArrayTypeU24136_t2161_marshaled& marshaled);
void U24ArrayTypeU24136_t2161_marshal_back(const U24ArrayTypeU24136_t2161_marshaled& marshaled, U24ArrayTypeU24136_t2161& unmarshaled);
void U24ArrayTypeU24136_t2161_marshal_cleanup(U24ArrayTypeU24136_t2161_marshaled& marshaled);
