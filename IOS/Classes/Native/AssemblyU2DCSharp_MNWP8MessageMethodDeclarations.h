﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNWP8Message
struct MNWP8Message_t293;
// System.String
struct String_t;

// System.Void MNWP8Message::.ctor()
extern "C" void MNWP8Message__ctor_m1817 (MNWP8Message_t293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNWP8Message MNWP8Message::Create(System.String,System.String)
extern "C" MNWP8Message_t293 * MNWP8Message_Create_m1818 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Message::init()
extern "C" void MNWP8Message_init_m1819 (MNWP8Message_t293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8Message::onPopUpCallBack()
extern "C" void MNWP8Message_onPopUpCallBack_m1820 (MNWP8Message_t293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
