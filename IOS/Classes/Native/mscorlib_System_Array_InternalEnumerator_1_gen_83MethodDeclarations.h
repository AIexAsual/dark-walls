﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Uri/UriScheme>
struct InternalEnumerator_1_t2907;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"

// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22679_gshared (InternalEnumerator_1_t2907 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22679(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2907 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22679_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22680_gshared (InternalEnumerator_1_t2907 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22680(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2907 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22680_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22681_gshared (InternalEnumerator_1_t2907 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22681(__this, method) (( void (*) (InternalEnumerator_1_t2907 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22681_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22682_gshared (InternalEnumerator_1_t2907 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22682(__this, method) (( bool (*) (InternalEnumerator_1_t2907 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22682_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern "C" UriScheme_t1516  InternalEnumerator_1_get_Current_m22683_gshared (InternalEnumerator_1_t2907 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22683(__this, method) (( UriScheme_t1516  (*) (InternalEnumerator_1_t2907 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22683_gshared)(__this, method)
