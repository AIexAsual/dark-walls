﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t221;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UserAuthorizationDialog
struct  UserAuthorizationDialog_t1070  : public MonoBehaviour_t4
{
	// UnityEngine.Rect UnityEngine.UserAuthorizationDialog::windowRect
	Rect_t225  ___windowRect_4;
	// UnityEngine.Texture UnityEngine.UserAuthorizationDialog::warningIcon
	Texture_t221 * ___warningIcon_5;
};
