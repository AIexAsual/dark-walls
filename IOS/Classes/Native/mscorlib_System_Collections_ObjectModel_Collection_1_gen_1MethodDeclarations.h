﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Char>
struct Collection_1_t2292;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Char[]
struct CharU5BU5D_t530;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t2174;
// System.Collections.Generic.IList`1<System.Char>
struct IList_1_t2290;

// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::.ctor()
extern "C" void Collection_1__ctor_m14422_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1__ctor_m14422(__this, method) (( void (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1__ctor_m14422_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14423_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14423(__this, method) (( bool (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14423_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m14424_gshared (Collection_1_t2292 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m14424(__this, ___array, ___index, method) (( void (*) (Collection_1_t2292 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m14424_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m14425_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m14425(__this, method) (( Object_t * (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m14425_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m14426_gshared (Collection_1_t2292 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m14426(__this, ___value, method) (( int32_t (*) (Collection_1_t2292 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m14426_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m14427_gshared (Collection_1_t2292 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m14427(__this, ___value, method) (( bool (*) (Collection_1_t2292 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m14427_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m14428_gshared (Collection_1_t2292 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m14428(__this, ___value, method) (( int32_t (*) (Collection_1_t2292 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m14428_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m14429_gshared (Collection_1_t2292 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m14429(__this, ___index, ___value, method) (( void (*) (Collection_1_t2292 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m14429_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m14430_gshared (Collection_1_t2292 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m14430(__this, ___value, method) (( void (*) (Collection_1_t2292 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m14430_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m14431_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m14431(__this, method) (( bool (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m14431_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m14432_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m14432(__this, method) (( Object_t * (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m14432_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m14433_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m14433(__this, method) (( bool (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m14433_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m14434_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m14434(__this, method) (( bool (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m14434_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m14435_gshared (Collection_1_t2292 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m14435(__this, ___index, method) (( Object_t * (*) (Collection_1_t2292 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m14435_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m14436_gshared (Collection_1_t2292 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m14436(__this, ___index, ___value, method) (( void (*) (Collection_1_t2292 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m14436_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::Add(T)
extern "C" void Collection_1_Add_m14437_gshared (Collection_1_t2292 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Add_m14437(__this, ___item, method) (( void (*) (Collection_1_t2292 *, uint16_t, const MethodInfo*))Collection_1_Add_m14437_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::Clear()
extern "C" void Collection_1_Clear_m14438_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_Clear_m14438(__this, method) (( void (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_Clear_m14438_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::ClearItems()
extern "C" void Collection_1_ClearItems_m14439_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m14439(__this, method) (( void (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_ClearItems_m14439_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::Contains(T)
extern "C" bool Collection_1_Contains_m14440_gshared (Collection_1_t2292 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m14440(__this, ___item, method) (( bool (*) (Collection_1_t2292 *, uint16_t, const MethodInfo*))Collection_1_Contains_m14440_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m14441_gshared (Collection_1_t2292 * __this, CharU5BU5D_t530* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m14441(__this, ___array, ___index, method) (( void (*) (Collection_1_t2292 *, CharU5BU5D_t530*, int32_t, const MethodInfo*))Collection_1_CopyTo_m14441_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Char>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m14442_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m14442(__this, method) (( Object_t* (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_GetEnumerator_m14442_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Char>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m14443_gshared (Collection_1_t2292 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m14443(__this, ___item, method) (( int32_t (*) (Collection_1_t2292 *, uint16_t, const MethodInfo*))Collection_1_IndexOf_m14443_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m14444_gshared (Collection_1_t2292 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m14444(__this, ___index, ___item, method) (( void (*) (Collection_1_t2292 *, int32_t, uint16_t, const MethodInfo*))Collection_1_Insert_m14444_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m14445_gshared (Collection_1_t2292 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m14445(__this, ___index, ___item, method) (( void (*) (Collection_1_t2292 *, int32_t, uint16_t, const MethodInfo*))Collection_1_InsertItem_m14445_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::Remove(T)
extern "C" bool Collection_1_Remove_m14446_gshared (Collection_1_t2292 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m14446(__this, ___item, method) (( bool (*) (Collection_1_t2292 *, uint16_t, const MethodInfo*))Collection_1_Remove_m14446_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m14447_gshared (Collection_1_t2292 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m14447(__this, ___index, method) (( void (*) (Collection_1_t2292 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m14447_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m14448_gshared (Collection_1_t2292 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m14448(__this, ___index, method) (( void (*) (Collection_1_t2292 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m14448_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Char>::get_Count()
extern "C" int32_t Collection_1_get_Count_m14449_gshared (Collection_1_t2292 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m14449(__this, method) (( int32_t (*) (Collection_1_t2292 *, const MethodInfo*))Collection_1_get_Count_m14449_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Char>::get_Item(System.Int32)
extern "C" uint16_t Collection_1_get_Item_m14450_gshared (Collection_1_t2292 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m14450(__this, ___index, method) (( uint16_t (*) (Collection_1_t2292 *, int32_t, const MethodInfo*))Collection_1_get_Item_m14450_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m14451_gshared (Collection_1_t2292 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m14451(__this, ___index, ___value, method) (( void (*) (Collection_1_t2292 *, int32_t, uint16_t, const MethodInfo*))Collection_1_set_Item_m14451_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m14452_gshared (Collection_1_t2292 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m14452(__this, ___index, ___item, method) (( void (*) (Collection_1_t2292 *, int32_t, uint16_t, const MethodInfo*))Collection_1_SetItem_m14452_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m14453_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m14453(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m14453_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Char>::ConvertItem(System.Object)
extern "C" uint16_t Collection_1_ConvertItem_m14454_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m14454(__this /* static, unused */, ___item, method) (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m14454_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Char>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m14455_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m14455(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m14455_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m14456_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m14456(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m14456_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Char>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m14457_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m14457(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m14457_gshared)(__this /* static, unused */, ___list, method)
