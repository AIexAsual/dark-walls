﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IntPtr>
struct InternalEnumerator_1_t2676;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19934_gshared (InternalEnumerator_1_t2676 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19934(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2676 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19934_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19935_gshared (InternalEnumerator_1_t2676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19935(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2676 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19935_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19936_gshared (InternalEnumerator_1_t2676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19936(__this, method) (( void (*) (InternalEnumerator_1_t2676 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19936_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19937_gshared (InternalEnumerator_1_t2676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19937(__this, method) (( bool (*) (InternalEnumerator_1_t2676 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19937_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C" IntPtr_t InternalEnumerator_1_get_Current_m19938_gshared (InternalEnumerator_1_t2676 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19938(__this, method) (( IntPtr_t (*) (InternalEnumerator_1_t2676 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19938_gshared)(__this, method)
