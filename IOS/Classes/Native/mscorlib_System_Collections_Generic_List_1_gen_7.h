﻿#pragma once
#include <stdint.h>
// UnityEngine.Space[]
struct SpaceU5BU5D_t450;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Space>
struct  List_1_t579  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Space>::_items
	SpaceU5BU5D_t450* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::_version
	int32_t ____version_3;
};
struct List_1_t579_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Space>::EmptyArray
	SpaceU5BU5D_t450* ___EmptyArray_4;
};
