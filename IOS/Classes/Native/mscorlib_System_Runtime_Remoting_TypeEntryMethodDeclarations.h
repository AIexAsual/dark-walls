﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.TypeEntry
struct TypeEntry_t1901;
// System.String
struct String_t;

// System.Void System.Runtime.Remoting.TypeEntry::.ctor()
extern "C" void TypeEntry__ctor_m11663 (TypeEntry_t1901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.TypeEntry::get_AssemblyName()
extern "C" String_t* TypeEntry_get_AssemblyName_m11664 (TypeEntry_t1901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.TypeEntry::set_AssemblyName(System.String)
extern "C" void TypeEntry_set_AssemblyName_m11665 (TypeEntry_t1901 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.TypeEntry::get_TypeName()
extern "C" String_t* TypeEntry_get_TypeName_m11666 (TypeEntry_t1901 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.TypeEntry::set_TypeName(System.String)
extern "C" void TypeEntry_set_TypeName_m11667 (TypeEntry_t1901 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
