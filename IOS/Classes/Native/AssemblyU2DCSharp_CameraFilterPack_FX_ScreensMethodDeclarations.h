﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Screens
struct CameraFilterPack_FX_Screens_t139;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Screens::.ctor()
extern "C" void CameraFilterPack_FX_Screens__ctor_m900 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Screens::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Screens_get_material_m901 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::Start()
extern "C" void CameraFilterPack_FX_Screens_Start_m902 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Screens_OnRenderImage_m903 (CameraFilterPack_FX_Screens_t139 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::OnValidate()
extern "C" void CameraFilterPack_FX_Screens_OnValidate_m904 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::Update()
extern "C" void CameraFilterPack_FX_Screens_Update_m905 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Screens::OnDisable()
extern "C" void CameraFilterPack_FX_Screens_OnDisable_m906 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
