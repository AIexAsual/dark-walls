﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_ShockWave
struct  CameraFilterPack_Distortion_ShockWave_t92  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_ShockWave::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_ShockWave::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_ShockWave::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_ShockWave::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_ShockWave::PosX
	float ___PosX_6;
	// System.Single CameraFilterPack_Distortion_ShockWave::PosY
	float ___PosY_7;
	// System.Single CameraFilterPack_Distortion_ShockWave::Speed
	float ___Speed_8;
	// System.Single CameraFilterPack_Distortion_ShockWave::Size
	float ___Size_9;
};
struct CameraFilterPack_Distortion_ShockWave_t92_StaticFields{
	// System.Single CameraFilterPack_Distortion_ShockWave::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Distortion_ShockWave::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Distortion_ShockWave::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Distortion_ShockWave::ChangeValue4
	float ___ChangeValue4_13;
};
