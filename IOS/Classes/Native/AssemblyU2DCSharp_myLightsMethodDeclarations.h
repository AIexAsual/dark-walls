﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// myLights
struct myLights_t369;

// System.Void myLights::.ctor()
extern "C" void myLights__ctor_m2140 (myLights_t369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myLights::.cctor()
extern "C" void myLights__cctor_m2141 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
