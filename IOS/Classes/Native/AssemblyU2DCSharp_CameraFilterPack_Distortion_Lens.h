﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Lens
struct  CameraFilterPack_Distortion_Lens_t90  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Lens::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Lens::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Lens::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Lens::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Lens::CenterX
	float ___CenterX_6;
	// System.Single CameraFilterPack_Distortion_Lens::CenterY
	float ___CenterY_7;
	// System.Single CameraFilterPack_Distortion_Lens::Distortion
	float ___Distortion_8;
};
struct CameraFilterPack_Distortion_Lens_t90_StaticFields{
	// System.Single CameraFilterPack_Distortion_Lens::ChangeCenterX
	float ___ChangeCenterX_9;
	// System.Single CameraFilterPack_Distortion_Lens::ChangeCenterY
	float ___ChangeCenterY_10;
	// System.Single CameraFilterPack_Distortion_Lens::ChangeDistortion
	float ___ChangeDistortion_11;
};
