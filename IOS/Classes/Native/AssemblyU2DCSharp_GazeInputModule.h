﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t257;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.EventSystems.BaseInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputModule.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// GazeInputModule
struct  GazeInputModule_t258  : public BaseInputModule_t259
{
	// System.Boolean GazeInputModule::vrModeOnly
	bool ___vrModeOnly_6;
	// UnityEngine.GameObject GazeInputModule::cursor
	GameObject_t256 * ___cursor_7;
	// System.Boolean GazeInputModule::showCursor
	bool ___showCursor_8;
	// System.Boolean GazeInputModule::scaleCursorSize
	bool ___scaleCursorSize_9;
	// System.Single GazeInputModule::clickTime
	float ___clickTime_10;
	// UnityEngine.Vector2 GazeInputModule::hotspot
	Vector2_t7  ___hotspot_11;
	// UnityEngine.EventSystems.PointerEventData GazeInputModule::pointerData
	PointerEventData_t257 * ___pointerData_12;
	// UnityEngine.Vector2 GazeInputModule::lastHeadPose
	Vector2_t7  ___lastHeadPose_13;
	// UnityEngine.Camera GazeInputModule::CameraFacing
	Camera_t14 * ___CameraFacing_15;
	// UnityEngine.Vector3 GazeInputModule::originalScale
	Vector3_t215  ___originalScale_16;
	// System.String GazeInputModule::oldHitObject
	String_t* ___oldHitObject_17;
	// System.Single GazeInputModule::tStare
	float ___tStare_18;
	// System.Boolean GazeInputModule::isStarring
	bool ___isStarring_19;
};
struct GazeInputModule_t258_StaticFields{
	// System.String GazeInputModule::hitObject
	String_t* ___hitObject_14;
};
