﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Edge_Sobel
struct CameraFilterPack_Edge_Sobel_t117;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Edge_Sobel::.ctor()
extern "C" void CameraFilterPack_Edge_Sobel__ctor_m752 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Edge_Sobel::get_material()
extern "C" Material_t2 * CameraFilterPack_Edge_Sobel_get_material_m753 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::Start()
extern "C" void CameraFilterPack_Edge_Sobel_Start_m754 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Edge_Sobel_OnRenderImage_m755 (CameraFilterPack_Edge_Sobel_t117 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::Update()
extern "C" void CameraFilterPack_Edge_Sobel_Update_m756 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Edge_Sobel::OnDisable()
extern "C" void CameraFilterPack_Edge_Sobel_OnDisable_m757 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
