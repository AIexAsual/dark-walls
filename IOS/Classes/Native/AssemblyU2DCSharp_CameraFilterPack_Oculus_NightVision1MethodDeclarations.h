﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Oculus_NightVision1
struct CameraFilterPack_Oculus_NightVision1_t162;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Oculus_NightVision1::.ctor()
extern "C" void CameraFilterPack_Oculus_NightVision1__ctor_m1043 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision1::get_material()
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision1_get_material_m1044 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::Start()
extern "C" void CameraFilterPack_Oculus_NightVision1_Start_m1045 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Oculus_NightVision1_OnRenderImage_m1046 (CameraFilterPack_Oculus_NightVision1_t162 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::Update()
extern "C" void CameraFilterPack_Oculus_NightVision1_Update_m1047 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision1::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision1_OnDisable_m1048 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
