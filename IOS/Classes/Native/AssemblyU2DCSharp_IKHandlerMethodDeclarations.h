﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// IKHandler
struct IKHandler_t322;

// System.Void IKHandler::.ctor()
extern "C" void IKHandler__ctor_m1906 (IKHandler_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IKHandler::Start()
extern "C" void IKHandler_Start_m1907 (IKHandler_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IKHandler::Update()
extern "C" void IKHandler_Update_m1908 (IKHandler_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IKHandler::OnAnimatorIK()
extern "C" void IKHandler_OnAnimatorIK_m1909 (IKHandler_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
