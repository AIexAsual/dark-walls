﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_WideScreenVertical
struct CameraFilterPack_TV_WideScreenVertical_t203;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_WideScreenVertical::.ctor()
extern "C" void CameraFilterPack_TV_WideScreenVertical__ctor_m1317 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_WideScreenVertical::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_WideScreenVertical_get_material_m1318 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::Start()
extern "C" void CameraFilterPack_TV_WideScreenVertical_Start_m1319 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_WideScreenVertical_OnRenderImage_m1320 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::OnValidate()
extern "C" void CameraFilterPack_TV_WideScreenVertical_OnValidate_m1321 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::Update()
extern "C" void CameraFilterPack_TV_WideScreenVertical_Update_m1322 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_WideScreenVertical::OnDisable()
extern "C" void CameraFilterPack_TV_WideScreenVertical_OnDisable_m1323 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
