﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Heat
struct CameraFilterPack_Distortion_Heat_t89;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Heat::.ctor()
extern "C" void CameraFilterPack_Distortion_Heat__ctor_m560 (CameraFilterPack_Distortion_Heat_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Heat::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Heat_get_material_m561 (CameraFilterPack_Distortion_Heat_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::Start()
extern "C" void CameraFilterPack_Distortion_Heat_Start_m562 (CameraFilterPack_Distortion_Heat_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Heat_OnRenderImage_m563 (CameraFilterPack_Distortion_Heat_t89 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::OnValidate()
extern "C" void CameraFilterPack_Distortion_Heat_OnValidate_m564 (CameraFilterPack_Distortion_Heat_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::Update()
extern "C" void CameraFilterPack_Distortion_Heat_Update_m565 (CameraFilterPack_Distortion_Heat_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Heat::OnDisable()
extern "C" void CameraFilterPack_Distortion_Heat_OnDisable_m566 (CameraFilterPack_Distortion_Heat_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
