﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTweenEvent/TweenType>
struct DefaultComparer_t2342;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTweenEvent/TweenType>::.ctor()
extern "C" void DefaultComparer__ctor_m14928_gshared (DefaultComparer_t2342 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m14928(__this, method) (( void (*) (DefaultComparer_t2342 *, const MethodInfo*))DefaultComparer__ctor_m14928_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTweenEvent/TweenType>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m14929_gshared (DefaultComparer_t2342 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m14929(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2342 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m14929_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<iTweenEvent/TweenType>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m14930_gshared (DefaultComparer_t2342 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m14930(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2342 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m14930_gshared)(__this, ___x, ___y, method)
