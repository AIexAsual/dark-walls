﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>
struct ValueCollection_t2713;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2705;
// System.Collections.Generic.IEnumerator`1<System.Int64>
struct IEnumerator_1_t3189;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Int64[]
struct Int64U5BU5D_t2176;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_23.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ValueCollection__ctor_m20479_gshared (ValueCollection_t2713 * __this, Dictionary_2_t2705 * ___dictionary, const MethodInfo* method);
#define ValueCollection__ctor_m20479(__this, ___dictionary, method) (( void (*) (ValueCollection_t2713 *, Dictionary_2_t2705 *, const MethodInfo*))ValueCollection__ctor_m20479_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20480_gshared (ValueCollection_t2713 * __this, int64_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20480(__this, ___item, method) (( void (*) (ValueCollection_t2713 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m20480_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20481_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20481(__this, method) (( void (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m20481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20482_gshared (ValueCollection_t2713 * __this, int64_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20482(__this, ___item, method) (( bool (*) (ValueCollection_t2713 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m20482_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20483_gshared (ValueCollection_t2713 * __this, int64_t ___item, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20483(__this, ___item, method) (( bool (*) (ValueCollection_t2713 *, int64_t, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m20483_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20484_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20484(__this, method) (( Object_t* (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m20484_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m20485_gshared (ValueCollection_t2713 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m20485(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2713 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m20485_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20486_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20486(__this, method) (( Object_t * (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m20486_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20487_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20487(__this, method) (( bool (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m20487_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20488_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20488(__this, method) (( bool (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m20488_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m20489_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m20489(__this, method) (( Object_t * (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m20489_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m20490_gshared (ValueCollection_t2713 * __this, Int64U5BU5D_t2176* ___array, int32_t ___index, const MethodInfo* method);
#define ValueCollection_CopyTo_m20490(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2713 *, Int64U5BU5D_t2176*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m20490_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::GetEnumerator()
extern "C" Enumerator_t2714  ValueCollection_GetEnumerator_m20491_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m20491(__this, method) (( Enumerator_t2714  (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_GetEnumerator_m20491_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m20492_gshared (ValueCollection_t2713 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m20492(__this, method) (( int32_t (*) (ValueCollection_t2713 *, const MethodInfo*))ValueCollection_get_Count_m20492_gshared)(__this, method)
