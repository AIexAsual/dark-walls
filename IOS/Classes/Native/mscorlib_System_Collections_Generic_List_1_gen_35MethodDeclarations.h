﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t957;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t3177;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3178;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t3179;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t2686;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1086;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t2690;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t2693;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_37.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m20042_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1__ctor_m20042(__this, method) (( void (*) (List_1_t957 *, const MethodInfo*))List_1__ctor_m20042_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m20043_gshared (List_1_t957 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m20043(__this, ___collection, method) (( void (*) (List_1_t957 *, Object_t*, const MethodInfo*))List_1__ctor_m20043_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m6400_gshared (List_1_t957 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m6400(__this, ___capacity, method) (( void (*) (List_1_t957 *, int32_t, const MethodInfo*))List_1__ctor_m6400_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m20044_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m20044(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m20044_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20045_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20045(__this, method) (( Object_t* (*) (List_1_t957 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20045_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m20046_gshared (List_1_t957 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m20046(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t957 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m20046_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m20047_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20047(__this, method) (( Object_t * (*) (List_1_t957 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m20047_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m20048_gshared (List_1_t957 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m20048(__this, ___item, method) (( int32_t (*) (List_1_t957 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m20048_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m20049_gshared (List_1_t957 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m20049(__this, ___item, method) (( bool (*) (List_1_t957 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m20049_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m20050_gshared (List_1_t957 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m20050(__this, ___item, method) (( int32_t (*) (List_1_t957 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m20050_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m20051_gshared (List_1_t957 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m20051(__this, ___index, ___item, method) (( void (*) (List_1_t957 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m20051_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m20052_gshared (List_1_t957 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m20052(__this, ___item, method) (( void (*) (List_1_t957 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m20052_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20053_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20053(__this, method) (( bool (*) (List_1_t957 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20053_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m20054_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20054(__this, method) (( bool (*) (List_1_t957 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m20054_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m20055_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m20055(__this, method) (( Object_t * (*) (List_1_t957 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m20055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m20056_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m20056(__this, method) (( bool (*) (List_1_t957 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m20056_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m20057_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m20057(__this, method) (( bool (*) (List_1_t957 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m20057_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m20058_gshared (List_1_t957 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m20058(__this, ___index, method) (( Object_t * (*) (List_1_t957 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m20058_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m20059_gshared (List_1_t957 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m20059(__this, ___index, ___value, method) (( void (*) (List_1_t957 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m20059_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m20060_gshared (List_1_t957 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define List_1_Add_m20060(__this, ___item, method) (( void (*) (List_1_t957 *, UICharInfo_t811 , const MethodInfo*))List_1_Add_m20060_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m20061_gshared (List_1_t957 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m20061(__this, ___newCount, method) (( void (*) (List_1_t957 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m20061_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m20062_gshared (List_1_t957 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m20062(__this, ___collection, method) (( void (*) (List_1_t957 *, Object_t*, const MethodInfo*))List_1_AddCollection_m20062_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m20063_gshared (List_1_t957 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m20063(__this, ___enumerable, method) (( void (*) (List_1_t957 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m20063_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m20064_gshared (List_1_t957 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m20064(__this, ___collection, method) (( void (*) (List_1_t957 *, Object_t*, const MethodInfo*))List_1_AddRange_m20064_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2686 * List_1_AsReadOnly_m20065_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m20065(__this, method) (( ReadOnlyCollection_1_t2686 * (*) (List_1_t957 *, const MethodInfo*))List_1_AsReadOnly_m20065_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m20066_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_Clear_m20066(__this, method) (( void (*) (List_1_t957 *, const MethodInfo*))List_1_Clear_m20066_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m20067_gshared (List_1_t957 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define List_1_Contains_m20067(__this, ___item, method) (( bool (*) (List_1_t957 *, UICharInfo_t811 , const MethodInfo*))List_1_Contains_m20067_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m20068_gshared (List_1_t957 * __this, UICharInfoU5BU5D_t1086* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m20068(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t957 *, UICharInfoU5BU5D_t1086*, int32_t, const MethodInfo*))List_1_CopyTo_m20068_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t811  List_1_Find_m20069_gshared (List_1_t957 * __this, Predicate_1_t2690 * ___match, const MethodInfo* method);
#define List_1_Find_m20069(__this, ___match, method) (( UICharInfo_t811  (*) (List_1_t957 *, Predicate_1_t2690 *, const MethodInfo*))List_1_Find_m20069_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m20070_gshared (Object_t * __this /* static, unused */, Predicate_1_t2690 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m20070(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2690 *, const MethodInfo*))List_1_CheckMatch_m20070_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m20071_gshared (List_1_t957 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2690 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m20071(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t957 *, int32_t, int32_t, Predicate_1_t2690 *, const MethodInfo*))List_1_GetIndex_m20071_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t2685  List_1_GetEnumerator_m20072_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m20072(__this, method) (( Enumerator_t2685  (*) (List_1_t957 *, const MethodInfo*))List_1_GetEnumerator_m20072_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m20073_gshared (List_1_t957 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define List_1_IndexOf_m20073(__this, ___item, method) (( int32_t (*) (List_1_t957 *, UICharInfo_t811 , const MethodInfo*))List_1_IndexOf_m20073_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m20074_gshared (List_1_t957 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m20074(__this, ___start, ___delta, method) (( void (*) (List_1_t957 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m20074_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m20075_gshared (List_1_t957 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m20075(__this, ___index, method) (( void (*) (List_1_t957 *, int32_t, const MethodInfo*))List_1_CheckIndex_m20075_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m20076_gshared (List_1_t957 * __this, int32_t ___index, UICharInfo_t811  ___item, const MethodInfo* method);
#define List_1_Insert_m20076(__this, ___index, ___item, method) (( void (*) (List_1_t957 *, int32_t, UICharInfo_t811 , const MethodInfo*))List_1_Insert_m20076_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m20077_gshared (List_1_t957 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m20077(__this, ___collection, method) (( void (*) (List_1_t957 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m20077_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m20078_gshared (List_1_t957 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define List_1_Remove_m20078(__this, ___item, method) (( bool (*) (List_1_t957 *, UICharInfo_t811 , const MethodInfo*))List_1_Remove_m20078_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m20079_gshared (List_1_t957 * __this, Predicate_1_t2690 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m20079(__this, ___match, method) (( int32_t (*) (List_1_t957 *, Predicate_1_t2690 *, const MethodInfo*))List_1_RemoveAll_m20079_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m20080_gshared (List_1_t957 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m20080(__this, ___index, method) (( void (*) (List_1_t957 *, int32_t, const MethodInfo*))List_1_RemoveAt_m20080_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m20081_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_Reverse_m20081(__this, method) (( void (*) (List_1_t957 *, const MethodInfo*))List_1_Reverse_m20081_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m20082_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_Sort_m20082(__this, method) (( void (*) (List_1_t957 *, const MethodInfo*))List_1_Sort_m20082_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m20083_gshared (List_1_t957 * __this, Comparison_1_t2693 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m20083(__this, ___comparison, method) (( void (*) (List_1_t957 *, Comparison_1_t2693 *, const MethodInfo*))List_1_Sort_m20083_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t1086* List_1_ToArray_m20084_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_ToArray_m20084(__this, method) (( UICharInfoU5BU5D_t1086* (*) (List_1_t957 *, const MethodInfo*))List_1_ToArray_m20084_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m20085_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m20085(__this, method) (( void (*) (List_1_t957 *, const MethodInfo*))List_1_TrimExcess_m20085_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m20086_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m20086(__this, method) (( int32_t (*) (List_1_t957 *, const MethodInfo*))List_1_get_Capacity_m20086_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m20087_gshared (List_1_t957 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m20087(__this, ___value, method) (( void (*) (List_1_t957 *, int32_t, const MethodInfo*))List_1_set_Capacity_m20087_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m20088_gshared (List_1_t957 * __this, const MethodInfo* method);
#define List_1_get_Count_m20088(__this, method) (( int32_t (*) (List_1_t957 *, const MethodInfo*))List_1_get_Count_m20088_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t811  List_1_get_Item_m20089_gshared (List_1_t957 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m20089(__this, ___index, method) (( UICharInfo_t811  (*) (List_1_t957 *, int32_t, const MethodInfo*))List_1_get_Item_m20089_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m20090_gshared (List_1_t957 * __this, int32_t ___index, UICharInfo_t811  ___value, const MethodInfo* method);
#define List_1_set_Item_m20090(__this, ___index, ___value, method) (( void (*) (List_1_t957 *, int32_t, UICharInfo_t811 , const MethodInfo*))List_1_set_Item_m20090_gshared)(__this, ___index, ___value, method)
