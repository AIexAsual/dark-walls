﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct KeyValuePair_2_t2743;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m20869_gshared (KeyValuePair_2_t2743 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m20869(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2743 *, uint64_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m20869_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Key()
extern "C" uint64_t KeyValuePair_2_get_Key_m20870_gshared (KeyValuePair_2_t2743 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m20870(__this, method) (( uint64_t (*) (KeyValuePair_2_t2743 *, const MethodInfo*))KeyValuePair_2_get_Key_m20870_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m20871_gshared (KeyValuePair_2_t2743 * __this, uint64_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m20871(__this, ___value, method) (( void (*) (KeyValuePair_2_t2743 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m20871_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m20872_gshared (KeyValuePair_2_t2743 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m20872(__this, method) (( Object_t * (*) (KeyValuePair_2_t2743 *, const MethodInfo*))KeyValuePair_2_get_Value_m20872_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m20873_gshared (KeyValuePair_2_t2743 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m20873(__this, ___value, method) (( void (*) (KeyValuePair_2_t2743 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m20873_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m20874_gshared (KeyValuePair_2_t2743 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m20874(__this, method) (( String_t* (*) (KeyValuePair_2_t2743 *, const MethodInfo*))KeyValuePair_2_ToString_m20874_gshared)(__this, method)
