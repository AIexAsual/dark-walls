﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Atmosphere_Snow_8bits
struct CameraFilterPack_Atmosphere_Snow_8bits_t13;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Atmosphere_Snow_8bits::.ctor()
extern "C" void CameraFilterPack_Atmosphere_Snow_8bits__ctor_m36 (CameraFilterPack_Atmosphere_Snow_8bits_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Atmosphere_Snow_8bits::get_material()
extern "C" Material_t2 * CameraFilterPack_Atmosphere_Snow_8bits_get_material_m37 (CameraFilterPack_Atmosphere_Snow_8bits_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::Start()
extern "C" void CameraFilterPack_Atmosphere_Snow_8bits_Start_m38 (CameraFilterPack_Atmosphere_Snow_8bits_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Atmosphere_Snow_8bits_OnRenderImage_m39 (CameraFilterPack_Atmosphere_Snow_8bits_t13 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::OnValidate()
extern "C" void CameraFilterPack_Atmosphere_Snow_8bits_OnValidate_m40 (CameraFilterPack_Atmosphere_Snow_8bits_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::Update()
extern "C" void CameraFilterPack_Atmosphere_Snow_8bits_Update_m41 (CameraFilterPack_Atmosphere_Snow_8bits_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Atmosphere_Snow_8bits::OnDisable()
extern "C" void CameraFilterPack_Atmosphere_Snow_8bits_OnDisable_m42 (CameraFilterPack_Atmosphere_Snow_8bits_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
