﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_EXTRA_SHOWFPS
struct  CameraFilterPack_EXTRA_SHOWFPS_t110  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_EXTRA_SHOWFPS::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_EXTRA_SHOWFPS::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_EXTRA_SHOWFPS::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::Size
	float ___Size_6;
	// System.Int32 CameraFilterPack_EXTRA_SHOWFPS::FPS
	int32_t ___FPS_7;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::Value3
	float ___Value3_8;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::Value4
	float ___Value4_9;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::deltaTime
	float ___deltaTime_10;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::accum
	float ___accum_15;
	// System.Int32 CameraFilterPack_EXTRA_SHOWFPS::frames
	int32_t ___frames_16;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::frequency
	float ___frequency_17;
};
struct CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields{
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::ChangeValue
	float ___ChangeValue_11;
	// System.Int32 CameraFilterPack_EXTRA_SHOWFPS::ChangeValue2
	int32_t ___ChangeValue2_12;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::ChangeValue3
	float ___ChangeValue3_13;
	// System.Single CameraFilterPack_EXTRA_SHOWFPS::ChangeValue4
	float ___ChangeValue4_14;
};
