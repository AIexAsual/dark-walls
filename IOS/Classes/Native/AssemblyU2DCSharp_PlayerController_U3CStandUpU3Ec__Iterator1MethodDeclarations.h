﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController/<StandUp>c__Iterator12
struct U3CStandUpU3Ec__Iterator12_t392;
// System.Object
struct Object_t;

// System.Void PlayerController/<StandUp>c__Iterator12::.ctor()
extern "C" void U3CStandUpU3Ec__Iterator12__ctor_m2242 (U3CStandUpU3Ec__Iterator12_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<StandUp>c__Iterator12::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStandUpU3Ec__Iterator12_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2243 (U3CStandUpU3Ec__Iterator12_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<StandUp>c__Iterator12::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStandUpU3Ec__Iterator12_System_Collections_IEnumerator_get_Current_m2244 (U3CStandUpU3Ec__Iterator12_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController/<StandUp>c__Iterator12::MoveNext()
extern "C" bool U3CStandUpU3Ec__Iterator12_MoveNext_m2245 (U3CStandUpU3Ec__Iterator12_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<StandUp>c__Iterator12::Dispose()
extern "C" void U3CStandUpU3Ec__Iterator12_Dispose_m2246 (U3CStandUpU3Ec__Iterator12_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<StandUp>c__Iterator12::Reset()
extern "C" void U3CStandUpU3Ec__Iterator12_Reset_m2247 (U3CStandUpU3Ec__Iterator12_t392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
