﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_NewPosterize
struct CameraFilterPack_Colors_NewPosterize_t78;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_NewPosterize::.ctor()
extern "C" void CameraFilterPack_Colors_NewPosterize__ctor_m482 (CameraFilterPack_Colors_NewPosterize_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_NewPosterize::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_NewPosterize_get_material_m483 (CameraFilterPack_Colors_NewPosterize_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::Start()
extern "C" void CameraFilterPack_Colors_NewPosterize_Start_m484 (CameraFilterPack_Colors_NewPosterize_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_NewPosterize_OnRenderImage_m485 (CameraFilterPack_Colors_NewPosterize_t78 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::OnValidate()
extern "C" void CameraFilterPack_Colors_NewPosterize_OnValidate_m486 (CameraFilterPack_Colors_NewPosterize_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::Update()
extern "C" void CameraFilterPack_Colors_NewPosterize_Update_m487 (CameraFilterPack_Colors_NewPosterize_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_NewPosterize::OnDisable()
extern "C" void CameraFilterPack_Colors_NewPosterize_OnDisable_m488 (CameraFilterPack_Colors_NewPosterize_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
