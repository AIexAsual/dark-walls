﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_LED
struct CameraFilterPack_TV_LED_t185;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_LED::.ctor()
extern "C" void CameraFilterPack_TV_LED__ctor_m1196 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_LED::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_LED_get_material_m1197 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::Start()
extern "C" void CameraFilterPack_TV_LED_Start_m1198 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_LED_OnRenderImage_m1199 (CameraFilterPack_TV_LED_t185 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::Update()
extern "C" void CameraFilterPack_TV_LED_Update_m1200 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_LED::OnDisable()
extern "C" void CameraFilterPack_TV_LED_OnDisable_m1201 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
