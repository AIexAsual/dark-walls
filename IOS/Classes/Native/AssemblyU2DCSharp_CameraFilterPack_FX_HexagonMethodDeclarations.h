﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Hexagon
struct CameraFilterPack_FX_Hexagon_t132;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Hexagon::.ctor()
extern "C" void CameraFilterPack_FX_Hexagon__ctor_m854 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Hexagon::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Hexagon_get_material_m855 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::Start()
extern "C" void CameraFilterPack_FX_Hexagon_Start_m856 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Hexagon_OnRenderImage_m857 (CameraFilterPack_FX_Hexagon_t132 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::Update()
extern "C" void CameraFilterPack_FX_Hexagon_Update_m858 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon::OnDisable()
extern "C" void CameraFilterPack_FX_Hexagon_OnDisable_m859 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
