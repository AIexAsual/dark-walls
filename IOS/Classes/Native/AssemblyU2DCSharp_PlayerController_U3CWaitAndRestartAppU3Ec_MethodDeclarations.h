﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController/<WaitAndRestartApp>c__Iterator16
struct U3CWaitAndRestartAppU3Ec__Iterator16_t396;
// System.Object
struct Object_t;

// System.Void PlayerController/<WaitAndRestartApp>c__Iterator16::.ctor()
extern "C" void U3CWaitAndRestartAppU3Ec__Iterator16__ctor_m2266 (U3CWaitAndRestartAppU3Ec__Iterator16_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndRestartApp>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndRestartAppU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2267 (U3CWaitAndRestartAppU3Ec__Iterator16_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndRestartApp>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndRestartAppU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m2268 (U3CWaitAndRestartAppU3Ec__Iterator16_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController/<WaitAndRestartApp>c__Iterator16::MoveNext()
extern "C" bool U3CWaitAndRestartAppU3Ec__Iterator16_MoveNext_m2269 (U3CWaitAndRestartAppU3Ec__Iterator16_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndRestartApp>c__Iterator16::Dispose()
extern "C" void U3CWaitAndRestartAppU3Ec__Iterator16_Dispose_m2270 (U3CWaitAndRestartAppU3Ec__Iterator16_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndRestartApp>c__Iterator16::Reset()
extern "C" void U3CWaitAndRestartAppU3Ec__Iterator16_Reset_m2271 (U3CWaitAndRestartAppU3Ec__Iterator16_t396 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
