﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t2357;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2349;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15094_gshared (Enumerator_t2357 * __this, Dictionary_2_t2349 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15094(__this, ___host, method) (( void (*) (Enumerator_t2357 *, Dictionary_2_t2349 *, const MethodInfo*))Enumerator__ctor_m15094_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15095_gshared (Enumerator_t2357 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15095(__this, method) (( Object_t * (*) (Enumerator_t2357 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15096_gshared (Enumerator_t2357 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15096(__this, method) (( void (*) (Enumerator_t2357 *, const MethodInfo*))Enumerator_Dispose_m15096_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15097_gshared (Enumerator_t2357 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15097(__this, method) (( bool (*) (Enumerator_t2357 *, const MethodInfo*))Enumerator_MoveNext_m15097_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15098_gshared (Enumerator_t2357 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15098(__this, method) (( Object_t * (*) (Enumerator_t2357 *, const MethodInfo*))Enumerator_get_Current_m15098_gshared)(__this, method)
