﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>
struct EqualityComparer_1_t2341;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>
struct  EqualityComparer_1_t2341  : public Object_t
{
};
struct EqualityComparer_1_t2341_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>::_default
	EqualityComparer_1_t2341 * ____default_0;
};
