﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// SceneLoader
struct SceneLoader_t382;
// System.Object
#include "mscorlib_System_Object.h"
// SceneLoader/<WaitAndLoad>c__Iterator18
struct  U3CWaitAndLoadU3Ec__Iterator18_t408  : public Object_t
{
	// System.Single SceneLoader/<WaitAndLoad>c__Iterator18::time
	float ___time_0;
	// System.Int32 SceneLoader/<WaitAndLoad>c__Iterator18::$PC
	int32_t ___U24PC_1;
	// System.Object SceneLoader/<WaitAndLoad>c__Iterator18::$current
	Object_t * ___U24current_2;
	// System.Single SceneLoader/<WaitAndLoad>c__Iterator18::<$>time
	float ___U3CU24U3Etime_3;
	// SceneLoader SceneLoader/<WaitAndLoad>c__Iterator18::<>f__this
	SceneLoader_t382 * ___U3CU3Ef__this_4;
};
