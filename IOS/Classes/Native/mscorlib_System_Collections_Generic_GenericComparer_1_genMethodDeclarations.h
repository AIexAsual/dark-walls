﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2215;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C" void GenericComparer_1__ctor_m13463_gshared (GenericComparer_1_t2215 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m13463(__this, method) (( void (*) (GenericComparer_1_t2215 *, const MethodInfo*))GenericComparer_1__ctor_m13463_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m22985_gshared (GenericComparer_1_t2215 * __this, DateTime_t871  ___x, DateTime_t871  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m22985(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2215 *, DateTime_t871 , DateTime_t871 , const MethodInfo*))GenericComparer_1_Compare_m22985_gshared)(__this, ___x, ___y, method)
