﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Halftone
struct CameraFilterPack_Drawing_Halftone_t100;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Halftone::.ctor()
extern "C" void CameraFilterPack_Drawing_Halftone__ctor_m637 (CameraFilterPack_Drawing_Halftone_t100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Halftone::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Halftone_get_material_m638 (CameraFilterPack_Drawing_Halftone_t100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::Start()
extern "C" void CameraFilterPack_Drawing_Halftone_Start_m639 (CameraFilterPack_Drawing_Halftone_t100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Halftone_OnRenderImage_m640 (CameraFilterPack_Drawing_Halftone_t100 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::OnValidate()
extern "C" void CameraFilterPack_Drawing_Halftone_OnValidate_m641 (CameraFilterPack_Drawing_Halftone_t100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::Update()
extern "C" void CameraFilterPack_Drawing_Halftone_Update_m642 (CameraFilterPack_Drawing_Halftone_t100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Halftone::OnDisable()
extern "C" void CameraFilterPack_Drawing_Halftone_OnDisable_m643 (CameraFilterPack_Drawing_Halftone_t100 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
