﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Dithering2x2
struct CameraFilterPack_Blur_Dithering2x2_t51;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Dithering2x2::.ctor()
extern "C" void CameraFilterPack_Blur_Dithering2x2__ctor_m310 (CameraFilterPack_Blur_Dithering2x2_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Dithering2x2::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Dithering2x2_get_material_m311 (CameraFilterPack_Blur_Dithering2x2_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Dithering2x2::Start()
extern "C" void CameraFilterPack_Blur_Dithering2x2_Start_m312 (CameraFilterPack_Blur_Dithering2x2_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Dithering2x2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Dithering2x2_OnRenderImage_m313 (CameraFilterPack_Blur_Dithering2x2_t51 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Dithering2x2::OnValidate()
extern "C" void CameraFilterPack_Blur_Dithering2x2_OnValidate_m314 (CameraFilterPack_Blur_Dithering2x2_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Dithering2x2::Update()
extern "C" void CameraFilterPack_Blur_Dithering2x2_Update_m315 (CameraFilterPack_Blur_Dithering2x2_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Dithering2x2::OnDisable()
extern "C" void CameraFilterPack_Blur_Dithering2x2_OnDisable_m316 (CameraFilterPack_Blur_Dithering2x2_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
