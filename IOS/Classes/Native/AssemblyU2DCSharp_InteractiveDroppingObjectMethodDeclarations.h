﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveDroppingObject
struct InteractiveDroppingObject_t327;

// System.Void InteractiveDroppingObject::.ctor()
extern "C" void InteractiveDroppingObject__ctor_m1926 (InteractiveDroppingObject_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveDroppingObject::Start()
extern "C" void InteractiveDroppingObject_Start_m1927 (InteractiveDroppingObject_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveDroppingObject::fallObject()
extern "C" void InteractiveDroppingObject_fallObject_m1928 (InteractiveDroppingObject_t327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
