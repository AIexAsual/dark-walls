﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t2414;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.ctor()
extern "C" void EqualityComparer_1__ctor_m15905_gshared (EqualityComparer_1_t2414 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m15905(__this, method) (( void (*) (EqualityComparer_1_t2414 *, const MethodInfo*))EqualityComparer_1__ctor_m15905_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Boolean>::.cctor()
extern "C" void EqualityComparer_1__cctor_m15906_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m15906(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m15906_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15907_gshared (EqualityComparer_1_t2414 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15907(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2414 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15907_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Boolean>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15908_gshared (EqualityComparer_1_t2414 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15908(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2414 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15908_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::get_Default()
extern "C" EqualityComparer_1_t2414 * EqualityComparer_1_get_Default_m15909_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m15909(__this /* static, unused */, method) (( EqualityComparer_1_t2414 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m15909_gshared)(__this /* static, unused */, method)
