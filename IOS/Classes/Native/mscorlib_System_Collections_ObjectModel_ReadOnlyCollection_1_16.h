﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.AudioClip>
struct IList_1_t573;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>
struct  ReadOnlyCollection_1_t2471  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioClip>::list
	Object_t* ___list_0;
};
