﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t348;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// PlayerEvent
#include "AssemblyU2DCSharp_PlayerEvent.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// PlayerEventManager/OnStateChangeHandler
struct  OnStateChangeHandler_t377  : public MulticastDelegate_t219
{
};
