﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>
struct Enumerator_t2603;
// System.Object
struct Object_t;
// UnityEngine.UI.Selectable
struct Selectable_t636;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t706;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m18846(__this, ___l, method) (( void (*) (Enumerator_t2603 *, List_1_t706 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18847(__this, method) (( Object_t * (*) (Enumerator_t2603 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::Dispose()
#define Enumerator_Dispose_m18848(__this, method) (( void (*) (Enumerator_t2603 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::VerifyState()
#define Enumerator_VerifyState_m18849(__this, method) (( void (*) (Enumerator_t2603 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::MoveNext()
#define Enumerator_MoveNext_m18850(__this, method) (( bool (*) (Enumerator_t2603 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::get_Current()
#define Enumerator_get_Current_m18851(__this, method) (( Selectable_t636 * (*) (Enumerator_t2603 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
