﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Char>
struct GenericComparer_1_t2298;

// System.Void System.Collections.Generic.GenericComparer`1<System.Char>::.ctor()
extern "C" void GenericComparer_1__ctor_m14477_gshared (GenericComparer_1_t2298 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m14477(__this, method) (( void (*) (GenericComparer_1_t2298 *, const MethodInfo*))GenericComparer_1__ctor_m14477_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Char>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m14478_gshared (GenericComparer_1_t2298 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m14478(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2298 *, uint16_t, uint16_t, const MethodInfo*))GenericComparer_1_Compare_m14478_gshared)(__this, ___x, ___y, method)
