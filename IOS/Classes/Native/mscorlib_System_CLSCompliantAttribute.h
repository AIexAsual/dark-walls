﻿#pragma once
#include <stdint.h>
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.CLSCompliantAttribute
struct  CLSCompliantAttribute_t1544  : public Attribute_t903
{
	// System.Boolean System.CLSCompliantAttribute::is_compliant
	bool ___is_compliant_0;
};
