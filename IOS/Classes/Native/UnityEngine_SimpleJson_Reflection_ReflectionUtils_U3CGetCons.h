﻿#pragma once
#include <stdint.h>
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t996;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
struct  U3CGetConstructorByReflectionU3Ec__AnonStorey1_t997  : public Object_t
{
	// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::constructorInfo
	ConstructorInfo_t996 * ___constructorInfo_0;
};
