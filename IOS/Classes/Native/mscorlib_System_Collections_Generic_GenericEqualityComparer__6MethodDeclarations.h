﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>
struct GenericEqualityComparer_1_t2415;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m15910_gshared (GenericEqualityComparer_1_t2415 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m15910(__this, method) (( void (*) (GenericEqualityComparer_1_t2415 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m15910_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m15911_gshared (GenericEqualityComparer_1_t2415 * __this, bool ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m15911(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2415 *, bool, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m15911_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Boolean>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m15912_gshared (GenericEqualityComparer_1_t2415 * __this, bool ___x, bool ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m15912(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2415 *, bool, bool, const MethodInfo*))GenericEqualityComparer_1_Equals_m15912_gshared)(__this, ___x, ___y, method)
