﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>
struct InternalEnumerator_1_t2707;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20428_gshared (InternalEnumerator_1_t2707 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20428(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2707 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20428_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20429_gshared (InternalEnumerator_1_t2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20429(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2707 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20429_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20430_gshared (InternalEnumerator_1_t2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20430(__this, method) (( void (*) (InternalEnumerator_1_t2707 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20430_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20431_gshared (InternalEnumerator_1_t2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20431(__this, method) (( bool (*) (InternalEnumerator_1_t2707 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20431_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::get_Current()
extern "C" KeyValuePair_2_t2706  InternalEnumerator_1_get_Current_m20432_gshared (InternalEnumerator_1_t2707 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20432(__this, method) (( KeyValuePair_2_t2706  (*) (InternalEnumerator_1_t2707 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20432_gshared)(__this, method)
