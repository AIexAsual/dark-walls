﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RagdollHelper/BodyPart
struct BodyPart_t400;

// System.Void RagdollHelper/BodyPart::.ctor()
extern "C" void BodyPart__ctor_m2298 (BodyPart_t400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
