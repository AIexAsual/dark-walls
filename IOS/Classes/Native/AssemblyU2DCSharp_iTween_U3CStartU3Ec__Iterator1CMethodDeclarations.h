﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTween/<Start>c__Iterator1C
struct U3CStartU3Ec__Iterator1C_t435;
// System.Object
struct Object_t;

// System.Void iTween/<Start>c__Iterator1C::.ctor()
extern "C" void U3CStartU3Ec__Iterator1C__ctor_m2427 (U3CStartU3Ec__Iterator1C_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator1C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2428 (U3CStartU3Ec__Iterator1C_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<Start>c__Iterator1C::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator1C_System_Collections_IEnumerator_get_Current_m2429 (U3CStartU3Ec__Iterator1C_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<Start>c__Iterator1C::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator1C_MoveNext_m2430 (U3CStartU3Ec__Iterator1C_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator1C::Dispose()
extern "C" void U3CStartU3Ec__Iterator1C_Dispose_m2431 (U3CStartU3Ec__Iterator1C_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<Start>c__Iterator1C::Reset()
extern "C" void U3CStartU3Ec__Iterator1C_Reset_m2432 (U3CStartU3Ec__Iterator1C_t435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
