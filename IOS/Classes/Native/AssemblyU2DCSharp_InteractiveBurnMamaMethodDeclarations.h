﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveBurnMama
struct InteractiveBurnMama_t326;

// System.Void InteractiveBurnMama::.ctor()
extern "C" void InteractiveBurnMama__ctor_m1919 (InteractiveBurnMama_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBurnMama::Start()
extern "C" void InteractiveBurnMama_Start_m1920 (InteractiveBurnMama_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBurnMama::Update()
extern "C" void InteractiveBurnMama_Update_m1921 (InteractiveBurnMama_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBurnMama::lookAtMeMom()
extern "C" void InteractiveBurnMama_lookAtMeMom_m1922 (InteractiveBurnMama_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBurnMama::waitAndTurn()
extern "C" void InteractiveBurnMama_waitAndTurn_m1923 (InteractiveBurnMama_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBurnMama::waitAndRagdoll()
extern "C" void InteractiveBurnMama_waitAndRagdoll_m1924 (InteractiveBurnMama_t326 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBurnMama::SetKinematic(System.Boolean)
extern "C" void InteractiveBurnMama_SetKinematic_m1925 (InteractiveBurnMama_t326 * __this, bool ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
