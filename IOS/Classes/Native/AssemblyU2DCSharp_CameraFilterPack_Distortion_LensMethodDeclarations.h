﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Lens
struct CameraFilterPack_Distortion_Lens_t90;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Lens::.ctor()
extern "C" void CameraFilterPack_Distortion_Lens__ctor_m567 (CameraFilterPack_Distortion_Lens_t90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Lens::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Lens_get_material_m568 (CameraFilterPack_Distortion_Lens_t90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::Start()
extern "C" void CameraFilterPack_Distortion_Lens_Start_m569 (CameraFilterPack_Distortion_Lens_t90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Lens_OnRenderImage_m570 (CameraFilterPack_Distortion_Lens_t90 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::OnValidate()
extern "C" void CameraFilterPack_Distortion_Lens_OnValidate_m571 (CameraFilterPack_Distortion_Lens_t90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::Update()
extern "C" void CameraFilterPack_Distortion_Lens_Update_m572 (CameraFilterPack_Distortion_Lens_t90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Lens::OnDisable()
extern "C" void CameraFilterPack_Distortion_Lens_OnDisable_m573 (CameraFilterPack_Distortion_Lens_t90 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
