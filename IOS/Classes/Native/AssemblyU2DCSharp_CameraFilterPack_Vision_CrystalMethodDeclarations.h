﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Vision_Crystal
struct CameraFilterPack_Vision_Crystal_t206;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Vision_Crystal::.ctor()
extern "C" void CameraFilterPack_Vision_Crystal__ctor_m1338 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Crystal::get_material()
extern "C" Material_t2 * CameraFilterPack_Vision_Crystal_get_material_m1339 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::Start()
extern "C" void CameraFilterPack_Vision_Crystal_Start_m1340 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Vision_Crystal_OnRenderImage_m1341 (CameraFilterPack_Vision_Crystal_t206 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::OnValidate()
extern "C" void CameraFilterPack_Vision_Crystal_OnValidate_m1342 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::Update()
extern "C" void CameraFilterPack_Vision_Crystal_Update_m1343 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Crystal::OnDisable()
extern "C" void CameraFilterPack_Vision_Crystal_OnDisable_m1344 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
