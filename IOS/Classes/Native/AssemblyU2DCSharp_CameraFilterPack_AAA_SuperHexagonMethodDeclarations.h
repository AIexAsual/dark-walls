﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_AAA_SuperHexagon
struct CameraFilterPack_AAA_SuperHexagon_t8;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_AAA_SuperHexagon::.ctor()
extern "C" void CameraFilterPack_AAA_SuperHexagon__ctor_m8 (CameraFilterPack_AAA_SuperHexagon_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::.cctor()
extern "C" void CameraFilterPack_AAA_SuperHexagon__cctor_m9 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_AAA_SuperHexagon::get_material()
extern "C" Material_t2 * CameraFilterPack_AAA_SuperHexagon_get_material_m10 (CameraFilterPack_AAA_SuperHexagon_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::Start()
extern "C" void CameraFilterPack_AAA_SuperHexagon_Start_m11 (CameraFilterPack_AAA_SuperHexagon_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_AAA_SuperHexagon_OnRenderImage_m12 (CameraFilterPack_AAA_SuperHexagon_t8 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::OnValidate()
extern "C" void CameraFilterPack_AAA_SuperHexagon_OnValidate_m13 (CameraFilterPack_AAA_SuperHexagon_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::Update()
extern "C" void CameraFilterPack_AAA_SuperHexagon_Update_m14 (CameraFilterPack_AAA_SuperHexagon_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperHexagon::OnDisable()
extern "C" void CameraFilterPack_AAA_SuperHexagon_OnDisable_m15 (CameraFilterPack_AAA_SuperHexagon_t8 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
