﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct  RaycastResultU5BU5D_t2247  : public Array_t
{
};
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct  IEventSystemHandlerU5BU5D_t2258  : public Array_t
{
};
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct  BaseInputModuleU5BU5D_t2488  : public Array_t
{
};
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct  BaseRaycasterU5BU5D_t2494  : public Array_t
{
};
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct  EntryU5BU5D_t2500  : public Array_t
{
};
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct  PointerEventDataU5BU5D_t2509  : public Array_t
{
};
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct  ButtonStateU5BU5D_t2526  : public Array_t
{
};
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct  ICanvasElementU5BU5D_t2536  : public Array_t
{
};
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct  TextU5BU5D_t2543  : public Array_t
{
};
struct TextU5BU5D_t2543_StaticFields{
};
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct  GraphicU5BU5D_t2571  : public Array_t
{
};
struct GraphicU5BU5D_t2571_StaticFields{
};
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct  IndexedSet_1U5BU5D_t2575  : public Array_t
{
};
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct  ContentTypeU5BU5D_t772  : public Array_t
{
};
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct  SelectableU5BU5D_t2599  : public Array_t
{
};
struct SelectableU5BU5D_t2599_StaticFields{
};
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct  MatEntryU5BU5D_t2611  : public Array_t
{
};
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct  ToggleU5BU5D_t2619  : public Array_t
{
};
