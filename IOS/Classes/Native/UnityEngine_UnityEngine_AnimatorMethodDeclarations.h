﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Animator
struct Animator_t321;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t816;
// UnityEngine.Avatar
struct Avatar_t543;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.AvatarIKGoal
#include "UnityEngine_UnityEngine_AvatarIKGoal.h"
// UnityEngine.AvatarIKHint
#include "UnityEngine_UnityEngine_AvatarIKHint.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.HumanBodyBones
#include "UnityEngine_UnityEngine_HumanBodyBones.h"

// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
extern "C" void Animator_SetFloat_m3275 (Animator_t321 * __this, String_t* ___name, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single,System.Single,System.Single)
extern "C" void Animator_SetFloat_m3274 (Animator_t321 * __this, String_t* ___name, float ___value, float ___dampTime, float ___deltaTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C" void Animator_SetBool_m3160 (Animator_t321 * __this, String_t* ___name, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" void Animator_SetTrigger_m3137 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" void Animator_ResetTrigger_m4895 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C" Vector3_t215  Animator_get_deltaPosition_m3284 (Animator_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
extern "C" void Animator_INTERNAL_get_deltaPosition_m5832 (Animator_t321 * __this, Vector3_t215 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C" Quaternion_t261  Animator_get_rootRotation_m3282 (Animator_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
extern "C" void Animator_INTERNAL_get_rootRotation_m5833 (Animator_t321 * __this, Quaternion_t261 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C" void Animator_set_applyRootMotion_m3273 (Animator_t321 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C" void Animator_SetIKPosition_m3127 (Animator_t321 * __this, int32_t ___goal, Vector3_t215  ___goalPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionInternal(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C" void Animator_SetIKPositionInternal_m5834 (Animator_t321 * __this, int32_t ___goal, Vector3_t215  ___goalPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C" void Animator_INTERNAL_CALL_SetIKPositionInternal_m5835 (Object_t * __this /* static, unused */, Animator_t321 * ___self, int32_t ___goal, Vector3_t215 * ___goalPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C" void Animator_SetIKPositionWeight_m3126 (Animator_t321 * __this, int32_t ___goal, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C" void Animator_SetIKPositionWeightInternal_m5836 (Animator_t321 * __this, int32_t ___goal, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKHintPositionWeight(UnityEngine.AvatarIKHint,System.Single)
extern "C" void Animator_SetIKHintPositionWeight_m3128 (Animator_t321 * __this, int32_t ___hint, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetIKHintPositionWeightInternal(UnityEngine.AvatarIKHint,System.Single)
extern "C" void Animator_SetIKHintPositionWeightInternal_m5837 (Animator_t321 * __this, int32_t ___hint, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C" void Animator_SetLookAtPosition_m3129 (Animator_t321 * __this, Vector3_t215  ___lookAtPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C" void Animator_SetLookAtPositionInternal_m5838 (Animator_t321 * __this, Vector3_t215  ___lookAtPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C" void Animator_INTERNAL_CALL_SetLookAtPositionInternal_m5839 (Object_t * __this /* static, unused */, Animator_t321 * ___self, Vector3_t215 * ___lookAtPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C" void Animator_SetLookAtWeight_m3279 (Animator_t321 * __this, float ___weight, float ___bodyWeight, float ___headWeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" void Animator_SetLookAtWeight_m5840 (Animator_t321 * __this, float ___weight, float ___bodyWeight, float ___headWeight, float ___eyesWeight, float ___clampWeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" void Animator_SetLookAtWeightInternal_m5841 (Animator_t321 * __this, float ___weight, float ___bodyWeight, float ___headWeight, float ___eyesWeight, float ___clampWeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C" AnimatorStateInfo_t542  Animator_GetCurrentAnimatorStateInfo_m3270 (Animator_t321 * __this, int32_t ___layerIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C" void Animator_set_speed_m3278 (Animator_t321 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C" Transform_t243 * Animator_GetBoneTransform_m3215 (Animator_t321 * __this, int32_t ___humanBoneId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C" RuntimeAnimatorController_t816 * Animator_get_runtimeAnimatorController_m4894 (Animator_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m5842 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Avatar UnityEngine.Animator::get_avatar()
extern "C" Avatar_t543 * Animator_get_avatar_m3280 (Animator_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::set_avatar(UnityEngine.Avatar)
extern "C" void Animator_set_avatar_m3281 (Animator_t321 * __this, Avatar_t543 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern "C" void Animator_CheckIfInIKPass_m5843 (Animator_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
extern "C" bool Animator_CheckIfInIKPassInternal_m5844 (Animator_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
extern "C" void Animator_SetFloatString_m5845 (Animator_t321 * __this, String_t* ___name, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C" void Animator_SetBoolString_m5846 (Animator_t321 * __this, String_t* ___name, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m5847 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" void Animator_ResetTriggerString_m5848 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)
extern "C" void Animator_SetFloatStringDamp_m5849 (Animator_t321 * __this, String_t* ___name, float ___value, float ___dampTime, float ___deltaTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C" bool Animator_get_logWarnings_m5850 (Animator_t321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
