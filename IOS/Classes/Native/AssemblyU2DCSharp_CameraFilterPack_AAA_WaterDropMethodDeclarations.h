﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_AAA_WaterDrop
struct CameraFilterPack_AAA_WaterDrop_t10;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_AAA_WaterDrop::.ctor()
extern "C" void CameraFilterPack_AAA_WaterDrop__ctor_m16 (CameraFilterPack_AAA_WaterDrop_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_AAA_WaterDrop::get_material()
extern "C" Material_t2 * CameraFilterPack_AAA_WaterDrop_get_material_m17 (CameraFilterPack_AAA_WaterDrop_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::Start()
extern "C" void CameraFilterPack_AAA_WaterDrop_Start_m18 (CameraFilterPack_AAA_WaterDrop_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_AAA_WaterDrop_OnRenderImage_m19 (CameraFilterPack_AAA_WaterDrop_t10 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::OnValidate()
extern "C" void CameraFilterPack_AAA_WaterDrop_OnValidate_m20 (CameraFilterPack_AAA_WaterDrop_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::Update()
extern "C" void CameraFilterPack_AAA_WaterDrop_Update_m21 (CameraFilterPack_AAA_WaterDrop_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_WaterDrop::OnDisable()
extern "C" void CameraFilterPack_AAA_WaterDrop_OnDisable_m22 (CameraFilterPack_AAA_WaterDrop_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
