﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Pixel_Pixelisation
struct CameraFilterPack_Pixel_Pixelisation_t169;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Pixel_Pixelisation::.ctor()
extern "C" void CameraFilterPack_Pixel_Pixelisation__ctor_m1091 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixel_Pixelisation::get_material()
extern "C" Material_t2 * CameraFilterPack_Pixel_Pixelisation_get_material_m1092 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::Start()
extern "C" void CameraFilterPack_Pixel_Pixelisation_Start_m1093 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Pixel_Pixelisation_OnRenderImage_m1094 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::OnValidate()
extern "C" void CameraFilterPack_Pixel_Pixelisation_OnValidate_m1095 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::Update()
extern "C" void CameraFilterPack_Pixel_Pixelisation_Update_m1096 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixel_Pixelisation::OnDisable()
extern "C" void CameraFilterPack_Pixel_Pixelisation_OnDisable_m1097 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
