﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Pixel_Pixelisation
struct  CameraFilterPack_Pixel_Pixelisation_t169  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Pixel_Pixelisation::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Pixel_Pixelisation::_Pixelisation
	float ____Pixelisation_3;
	// System.Single CameraFilterPack_Pixel_Pixelisation::_SizeX
	float ____SizeX_4;
	// System.Single CameraFilterPack_Pixel_Pixelisation::_SizeY
	float ____SizeY_5;
	// UnityEngine.Material CameraFilterPack_Pixel_Pixelisation::SCMaterial
	Material_t2 * ___SCMaterial_6;
};
struct CameraFilterPack_Pixel_Pixelisation_t169_StaticFields{
	// System.Single CameraFilterPack_Pixel_Pixelisation::ChangePixel
	float ___ChangePixel_7;
	// System.Single CameraFilterPack_Pixel_Pixelisation::ChangePixelX
	float ___ChangePixelX_8;
	// System.Single CameraFilterPack_Pixel_Pixelisation::ChangePixelY
	float ___ChangePixelY_9;
};
