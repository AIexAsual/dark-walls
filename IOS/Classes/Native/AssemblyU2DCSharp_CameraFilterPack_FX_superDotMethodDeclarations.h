﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_superDot
struct CameraFilterPack_FX_superDot_t142;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_superDot::.ctor()
extern "C" void CameraFilterPack_FX_superDot__ctor_m921 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_superDot::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_superDot_get_material_m922 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_superDot::Start()
extern "C" void CameraFilterPack_FX_superDot_Start_m923 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_superDot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_superDot_OnRenderImage_m924 (CameraFilterPack_FX_superDot_t142 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_superDot::Update()
extern "C" void CameraFilterPack_FX_superDot_Update_m925 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_superDot::OnDisable()
extern "C" void CameraFilterPack_FX_superDot_OnDisable_m926 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
