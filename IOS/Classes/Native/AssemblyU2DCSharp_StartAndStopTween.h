﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// StartAndStopTween
struct  StartAndStopTween_t419  : public MonoBehaviour_t4
{
	// UnityEngine.GameObject StartAndStopTween::target
	GameObject_t256 * ___target_2;
};
