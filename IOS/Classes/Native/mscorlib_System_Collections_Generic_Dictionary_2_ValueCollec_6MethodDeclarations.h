﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>
struct Enumerator_t2336;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
struct Dictionary_2_t2327;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14895_gshared (Enumerator_t2336 * __this, Dictionary_2_t2327 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m14895(__this, ___host, method) (( void (*) (Enumerator_t2336 *, Dictionary_2_t2327 *, const MethodInfo*))Enumerator__ctor_m14895_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14896_gshared (Enumerator_t2336 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14896(__this, method) (( Object_t * (*) (Enumerator_t2336 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14896_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14897_gshared (Enumerator_t2336 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14897(__this, method) (( void (*) (Enumerator_t2336 *, const MethodInfo*))Enumerator_Dispose_m14897_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14898_gshared (Enumerator_t2336 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14898(__this, method) (( bool (*) (Enumerator_t2336 *, const MethodInfo*))Enumerator_MoveNext_m14898_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m14899_gshared (Enumerator_t2336 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14899(__this, method) (( Object_t * (*) (Enumerator_t2336 *, const MethodInfo*))Enumerator_get_Current_m14899_gshared)(__this, method)
