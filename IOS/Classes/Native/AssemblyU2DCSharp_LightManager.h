﻿#pragma once
#include <stdint.h>
// LightManager
struct LightManager_t370;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t323;
// UnityEngine.GameObject
struct GameObject_t256;
// InteractiveFlashlight
struct InteractiveFlashlight_t329;
// System.Object
#include "mscorlib_System_Object.h"
// LightManager
struct  LightManager_t370  : public Object_t
{
	// UnityEngine.GameObject[] LightManager::lightControllers
	GameObjectU5BU5D_t323* ___lightControllers_1;
	// UnityEngine.GameObject LightManager::myFlashlight
	GameObject_t256 * ___myFlashlight_2;
	// InteractiveFlashlight LightManager::mySpotlight
	InteractiveFlashlight_t329 * ___mySpotlight_3;
};
struct LightManager_t370_StaticFields{
	// LightManager LightManager::_instance
	LightManager_t370 * ____instance_0;
};
