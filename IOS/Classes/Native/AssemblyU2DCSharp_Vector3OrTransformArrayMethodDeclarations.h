﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vector3OrTransformArray
struct Vector3OrTransformArray_t424;

// System.Void Vector3OrTransformArray::.ctor()
extern "C" void Vector3OrTransformArray__ctor_m2402 (Vector3OrTransformArray_t424 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vector3OrTransformArray::.cctor()
extern "C" void Vector3OrTransformArray__cctor_m2403 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
