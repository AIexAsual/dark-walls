﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GradientColorKey
struct GradientColorKey_t863;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C" void GradientColorKey__ctor_m5130 (GradientColorKey_t863 * __this, Color_t6  ___col, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
