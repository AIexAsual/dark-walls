﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Manga5
struct CameraFilterPack_Drawing_Manga5_t106;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Manga5::.ctor()
extern "C" void CameraFilterPack_Drawing_Manga5__ctor_m678 (CameraFilterPack_Drawing_Manga5_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga5::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Manga5_get_material_m679 (CameraFilterPack_Drawing_Manga5_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::Start()
extern "C" void CameraFilterPack_Drawing_Manga5_Start_m680 (CameraFilterPack_Drawing_Manga5_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Manga5_OnRenderImage_m681 (CameraFilterPack_Drawing_Manga5_t106 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::OnValidate()
extern "C" void CameraFilterPack_Drawing_Manga5_OnValidate_m682 (CameraFilterPack_Drawing_Manga5_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::Update()
extern "C" void CameraFilterPack_Drawing_Manga5_Update_m683 (CameraFilterPack_Drawing_Manga5_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga5::OnDisable()
extern "C" void CameraFilterPack_Drawing_Manga5_OnDisable_m684 (CameraFilterPack_Drawing_Manga5_t106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
