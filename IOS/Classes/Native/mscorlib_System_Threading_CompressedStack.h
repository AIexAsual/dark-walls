﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t436;
// System.Object
#include "mscorlib_System_Object.h"
// System.Threading.CompressedStack
struct  CompressedStack_t2015  : public Object_t
{
	// System.Collections.ArrayList System.Threading.CompressedStack::_list
	ArrayList_t436 * ____list_0;
};
