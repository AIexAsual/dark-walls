﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Single>
struct Collection_1_t2402;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Single[]
struct SingleU5BU5D_t72;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3025;
// System.Collections.Generic.IList`1<System.Single>
struct IList_1_t562;

// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::.ctor()
extern "C" void Collection_1__ctor_m15722_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15722(__this, method) (( void (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1__ctor_m15722_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15723_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15723(__this, method) (( bool (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15723_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15724_gshared (Collection_1_t2402 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15724(__this, ___array, ___index, method) (( void (*) (Collection_1_t2402 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15724_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15725_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15725(__this, method) (( Object_t * (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15725_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15726_gshared (Collection_1_t2402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15726(__this, ___value, method) (( int32_t (*) (Collection_1_t2402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15726_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15727_gshared (Collection_1_t2402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15727(__this, ___value, method) (( bool (*) (Collection_1_t2402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15727_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15728_gshared (Collection_1_t2402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15728(__this, ___value, method) (( int32_t (*) (Collection_1_t2402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15728_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15729_gshared (Collection_1_t2402 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15729(__this, ___index, ___value, method) (( void (*) (Collection_1_t2402 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15729_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15730_gshared (Collection_1_t2402 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15730(__this, ___value, method) (( void (*) (Collection_1_t2402 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15730_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15731_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15731(__this, method) (( bool (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15731_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15732_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15732(__this, method) (( Object_t * (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15732_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15733_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15733(__this, method) (( bool (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15733_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15734_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15734(__this, method) (( bool (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15734_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15735_gshared (Collection_1_t2402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15735(__this, ___index, method) (( Object_t * (*) (Collection_1_t2402 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15735_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15736_gshared (Collection_1_t2402 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15736(__this, ___index, ___value, method) (( void (*) (Collection_1_t2402 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15736_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::Add(T)
extern "C" void Collection_1_Add_m15737_gshared (Collection_1_t2402 * __this, float ___item, const MethodInfo* method);
#define Collection_1_Add_m15737(__this, ___item, method) (( void (*) (Collection_1_t2402 *, float, const MethodInfo*))Collection_1_Add_m15737_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::Clear()
extern "C" void Collection_1_Clear_m15738_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15738(__this, method) (( void (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_Clear_m15738_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::ClearItems()
extern "C" void Collection_1_ClearItems_m15739_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15739(__this, method) (( void (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_ClearItems_m15739_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::Contains(T)
extern "C" bool Collection_1_Contains_m15740_gshared (Collection_1_t2402 * __this, float ___item, const MethodInfo* method);
#define Collection_1_Contains_m15740(__this, ___item, method) (( bool (*) (Collection_1_t2402 *, float, const MethodInfo*))Collection_1_Contains_m15740_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15741_gshared (Collection_1_t2402 * __this, SingleU5BU5D_t72* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15741(__this, ___array, ___index, method) (( void (*) (Collection_1_t2402 *, SingleU5BU5D_t72*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15741_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Single>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15742_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15742(__this, method) (( Object_t* (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_GetEnumerator_m15742_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Single>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15743_gshared (Collection_1_t2402 * __this, float ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15743(__this, ___item, method) (( int32_t (*) (Collection_1_t2402 *, float, const MethodInfo*))Collection_1_IndexOf_m15743_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15744_gshared (Collection_1_t2402 * __this, int32_t ___index, float ___item, const MethodInfo* method);
#define Collection_1_Insert_m15744(__this, ___index, ___item, method) (( void (*) (Collection_1_t2402 *, int32_t, float, const MethodInfo*))Collection_1_Insert_m15744_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15745_gshared (Collection_1_t2402 * __this, int32_t ___index, float ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15745(__this, ___index, ___item, method) (( void (*) (Collection_1_t2402 *, int32_t, float, const MethodInfo*))Collection_1_InsertItem_m15745_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::Remove(T)
extern "C" bool Collection_1_Remove_m15746_gshared (Collection_1_t2402 * __this, float ___item, const MethodInfo* method);
#define Collection_1_Remove_m15746(__this, ___item, method) (( bool (*) (Collection_1_t2402 *, float, const MethodInfo*))Collection_1_Remove_m15746_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15747_gshared (Collection_1_t2402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15747(__this, ___index, method) (( void (*) (Collection_1_t2402 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15747_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15748_gshared (Collection_1_t2402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15748(__this, ___index, method) (( void (*) (Collection_1_t2402 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15748_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Single>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15749_gshared (Collection_1_t2402 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15749(__this, method) (( int32_t (*) (Collection_1_t2402 *, const MethodInfo*))Collection_1_get_Count_m15749_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Single>::get_Item(System.Int32)
extern "C" float Collection_1_get_Item_m15750_gshared (Collection_1_t2402 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15750(__this, ___index, method) (( float (*) (Collection_1_t2402 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15750_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15751_gshared (Collection_1_t2402 * __this, int32_t ___index, float ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15751(__this, ___index, ___value, method) (( void (*) (Collection_1_t2402 *, int32_t, float, const MethodInfo*))Collection_1_set_Item_m15751_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15752_gshared (Collection_1_t2402 * __this, int32_t ___index, float ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15752(__this, ___index, ___item, method) (( void (*) (Collection_1_t2402 *, int32_t, float, const MethodInfo*))Collection_1_SetItem_m15752_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15753_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15753(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15753_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Single>::ConvertItem(System.Object)
extern "C" float Collection_1_ConvertItem_m15754_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15754(__this /* static, unused */, ___item, method) (( float (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15754_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Single>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15755_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15755(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15755_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15756_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15756(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15756_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Single>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15757_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15757(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15757_gshared)(__this /* static, unused */, ___list, method)
