﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<iTween/EaseType>
struct Collection_1_t2451;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// iTween/EaseType[]
struct EaseTypeU5BU5D_t451;
// System.Collections.Generic.IEnumerator`1<iTween/EaseType>
struct IEnumerator_1_t3040;
// System.Collections.Generic.IList`1<iTween/EaseType>
struct IList_1_t569;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"

// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::.ctor()
extern "C" void Collection_1__ctor_m16442_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16442(__this, method) (( void (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1__ctor_m16442_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16443_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16443(__this, method) (( bool (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16443_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16444_gshared (Collection_1_t2451 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16444(__this, ___array, ___index, method) (( void (*) (Collection_1_t2451 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16444_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16445_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16445(__this, method) (( Object_t * (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16445_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16446_gshared (Collection_1_t2451 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16446(__this, ___value, method) (( int32_t (*) (Collection_1_t2451 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16446_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16447_gshared (Collection_1_t2451 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16447(__this, ___value, method) (( bool (*) (Collection_1_t2451 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16447_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16448_gshared (Collection_1_t2451 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16448(__this, ___value, method) (( int32_t (*) (Collection_1_t2451 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16448_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16449_gshared (Collection_1_t2451 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16449(__this, ___index, ___value, method) (( void (*) (Collection_1_t2451 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16449_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16450_gshared (Collection_1_t2451 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16450(__this, ___value, method) (( void (*) (Collection_1_t2451 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16450_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16451_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16451(__this, method) (( bool (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16451_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16452_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16452(__this, method) (( Object_t * (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16452_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16453_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16453(__this, method) (( bool (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16453_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16454_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16454(__this, method) (( bool (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16454_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16455_gshared (Collection_1_t2451 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16455(__this, ___index, method) (( Object_t * (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16455_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16456_gshared (Collection_1_t2451 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16456(__this, ___index, ___value, method) (( void (*) (Collection_1_t2451 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16456_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::Add(T)
extern "C" void Collection_1_Add_m16457_gshared (Collection_1_t2451 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m16457(__this, ___item, method) (( void (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_Add_m16457_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::Clear()
extern "C" void Collection_1_Clear_m16458_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16458(__this, method) (( void (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_Clear_m16458_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::ClearItems()
extern "C" void Collection_1_ClearItems_m16459_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16459(__this, method) (( void (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_ClearItems_m16459_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::Contains(T)
extern "C" bool Collection_1_Contains_m16460_gshared (Collection_1_t2451 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m16460(__this, ___item, method) (( bool (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_Contains_m16460_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16461_gshared (Collection_1_t2451 * __this, EaseTypeU5BU5D_t451* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16461(__this, ___array, ___index, method) (( void (*) (Collection_1_t2451 *, EaseTypeU5BU5D_t451*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16461_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<iTween/EaseType>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16462_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16462(__this, method) (( Object_t* (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_GetEnumerator_m16462_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/EaseType>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16463_gshared (Collection_1_t2451 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16463(__this, ___item, method) (( int32_t (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m16463_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16464_gshared (Collection_1_t2451 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m16464(__this, ___index, ___item, method) (( void (*) (Collection_1_t2451 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m16464_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16465_gshared (Collection_1_t2451 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16465(__this, ___index, ___item, method) (( void (*) (Collection_1_t2451 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m16465_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::Remove(T)
extern "C" bool Collection_1_Remove_m16466_gshared (Collection_1_t2451 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m16466(__this, ___item, method) (( bool (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_Remove_m16466_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16467_gshared (Collection_1_t2451 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16467(__this, ___index, method) (( void (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16467_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16468_gshared (Collection_1_t2451 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16468(__this, ___index, method) (( void (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16468_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<iTween/EaseType>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16469_gshared (Collection_1_t2451 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16469(__this, method) (( int32_t (*) (Collection_1_t2451 *, const MethodInfo*))Collection_1_get_Count_m16469_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<iTween/EaseType>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m16470_gshared (Collection_1_t2451 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16470(__this, ___index, method) (( int32_t (*) (Collection_1_t2451 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16470_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16471_gshared (Collection_1_t2451 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16471(__this, ___index, ___value, method) (( void (*) (Collection_1_t2451 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m16471_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16472_gshared (Collection_1_t2451 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16472(__this, ___index, ___item, method) (( void (*) (Collection_1_t2451 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m16472_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16473_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16473(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16473_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<iTween/EaseType>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m16474_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16474(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16474_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<iTween/EaseType>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16475_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16475(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16475_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16476_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16476(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16476_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<iTween/EaseType>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16477_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16477(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16477_gshared)(__this /* static, unused */, ___list, method)
