﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// goHere
struct goHere_t417;

// System.Void goHere::.ctor()
extern "C" void goHere__ctor_m2388 (goHere_t417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void goHere::Start()
extern "C" void goHere_Start_m2389 (goHere_t417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void goHere::Update()
extern "C" void goHere_Update_m2390 (goHere_t417 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void goHere::myValue(System.Single)
extern "C" void goHere_myValue_m2391 (goHere_t417 * __this, float ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
