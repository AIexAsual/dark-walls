﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// GoTo
struct GoTo_t315;
// System.Object
#include "mscorlib_System_Object.h"
// GoTo/<WaitAndEnable>c__Iterator6
struct  U3CWaitAndEnableU3Ec__Iterator6_t316  : public Object_t
{
	// System.Single GoTo/<WaitAndEnable>c__Iterator6::time
	float ___time_0;
	// System.Boolean GoTo/<WaitAndEnable>c__Iterator6::val
	bool ___val_1;
	// System.Int32 GoTo/<WaitAndEnable>c__Iterator6::$PC
	int32_t ___U24PC_2;
	// System.Object GoTo/<WaitAndEnable>c__Iterator6::$current
	Object_t * ___U24current_3;
	// System.Single GoTo/<WaitAndEnable>c__Iterator6::<$>time
	float ___U3CU24U3Etime_4;
	// System.Boolean GoTo/<WaitAndEnable>c__Iterator6::<$>val
	bool ___U3CU24U3Eval_5;
	// GoTo GoTo/<WaitAndEnable>c__Iterator6::<>f__this
	GoTo_t315 * ___U3CU3Ef__this_6;
};
