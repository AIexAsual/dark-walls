﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_50
struct  CameraFilterPack_TV_50_t175  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_50::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_50::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_TV_50::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_TV_50::SCMaterial
	Material_t2 * ___SCMaterial_5;
};
