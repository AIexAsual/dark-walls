﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Darken
struct CameraFilterPack_Blend2Camera_Darken_t21;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Darken::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Darken__ctor_m82 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Darken::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Darken_get_material_m83 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::Start()
extern "C" void CameraFilterPack_Blend2Camera_Darken_Start_m84 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Darken_OnRenderImage_m85 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Darken_OnValidate_m86 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::Update()
extern "C" void CameraFilterPack_Blend2Camera_Darken_Update_m87 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Darken_OnEnable_m88 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Darken::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Darken_OnDisable_m89 (CameraFilterPack_Blend2Camera_Darken_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
