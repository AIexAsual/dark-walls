﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Dot_Circle
struct CameraFilterPack_FX_Dot_Circle_t123;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Dot_Circle::.ctor()
extern "C" void CameraFilterPack_FX_Dot_Circle__ctor_m790 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Dot_Circle::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Dot_Circle_get_material_m791 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::Start()
extern "C" void CameraFilterPack_FX_Dot_Circle_Start_m792 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Dot_Circle_OnRenderImage_m793 (CameraFilterPack_FX_Dot_Circle_t123 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::OnValidate()
extern "C" void CameraFilterPack_FX_Dot_Circle_OnValidate_m794 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::Update()
extern "C" void CameraFilterPack_FX_Dot_Circle_Update_m795 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Dot_Circle::OnDisable()
extern "C" void CameraFilterPack_FX_Dot_Circle_OnDisable_m796 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
