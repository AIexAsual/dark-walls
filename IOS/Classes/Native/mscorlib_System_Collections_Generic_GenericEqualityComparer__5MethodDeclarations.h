﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Single>
struct GenericEqualityComparer_1_t2404;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Single>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m15763_gshared (GenericEqualityComparer_1_t2404 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m15763(__this, method) (( void (*) (GenericEqualityComparer_1_t2404 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m15763_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Single>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m15764_gshared (GenericEqualityComparer_1_t2404 * __this, float ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m15764(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2404 *, float, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m15764_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Single>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m15765_gshared (GenericEqualityComparer_1_t2404 * __this, float ___x, float ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m15765(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2404 *, float, float, const MethodInfo*))GenericEqualityComparer_1_Equals_m15765_gshared)(__this, ___x, ___y, method)
