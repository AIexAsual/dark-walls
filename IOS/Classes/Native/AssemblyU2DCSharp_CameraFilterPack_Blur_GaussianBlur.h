﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blur_GaussianBlur
struct  CameraFilterPack_Blur_GaussianBlur_t53  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Blur_GaussianBlur::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_GaussianBlur::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Blur_GaussianBlur::Size
	float ___Size_4;
	// UnityEngine.Vector4 CameraFilterPack_Blur_GaussianBlur::ScreenResolution
	Vector4_t5  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_Blur_GaussianBlur::SCMaterial
	Material_t2 * ___SCMaterial_6;
};
struct CameraFilterPack_Blur_GaussianBlur_t53_StaticFields{
	// System.Single CameraFilterPack_Blur_GaussianBlur::ChangeSize
	float ___ChangeSize_7;
};
