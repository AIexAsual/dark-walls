﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Dictionary_2_t420;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct  ValueCollection_t2345  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::dictionary
	Dictionary_2_t420 * ___dictionary_0;
};
