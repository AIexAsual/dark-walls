﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTweenEvent/<StartEvent>c__Iterator1D
struct U3CStartEventU3Ec__Iterator1D_t444;
// System.Object
struct Object_t;

// System.Void iTweenEvent/<StartEvent>c__Iterator1D::.ctor()
extern "C" void U3CStartEventU3Ec__Iterator1D__ctor_m2688 (U3CStartEventU3Ec__Iterator1D_t444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTweenEvent/<StartEvent>c__Iterator1D::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartEventU3Ec__Iterator1D_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2689 (U3CStartEventU3Ec__Iterator1D_t444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTweenEvent/<StartEvent>c__Iterator1D::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartEventU3Ec__Iterator1D_System_Collections_IEnumerator_get_Current_m2690 (U3CStartEventU3Ec__Iterator1D_t444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTweenEvent/<StartEvent>c__Iterator1D::MoveNext()
extern "C" bool U3CStartEventU3Ec__Iterator1D_MoveNext_m2691 (U3CStartEventU3Ec__Iterator1D_t444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent/<StartEvent>c__Iterator1D::Dispose()
extern "C" void U3CStartEventU3Ec__Iterator1D_Dispose_m2692 (U3CStartEventU3Ec__Iterator1D_t444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent/<StartEvent>c__Iterator1D::Reset()
extern "C" void U3CStartEventU3Ec__Iterator1D_Reset_m2693 (U3CStartEventU3Ec__Iterator1D_t444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
