﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.BitArray/BitArrayEnumerator
struct BitArrayEnumerator_t1654;
// System.Object
struct Object_t;
// System.Collections.BitArray
struct BitArray_t1510;

// System.Void System.Collections.BitArray/BitArrayEnumerator::.ctor(System.Collections.BitArray)
extern "C" void BitArrayEnumerator__ctor_m9986 (BitArrayEnumerator_t1654 * __this, BitArray_t1510 * ___ba, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray/BitArrayEnumerator::get_Current()
extern "C" Object_t * BitArrayEnumerator_get_Current_m9987 (BitArrayEnumerator_t1654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray/BitArrayEnumerator::MoveNext()
extern "C" bool BitArrayEnumerator_MoveNext_m9988 (BitArrayEnumerator_t1654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray/BitArrayEnumerator::checkVersion()
extern "C" void BitArrayEnumerator_checkVersion_m9989 (BitArrayEnumerator_t1654 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
