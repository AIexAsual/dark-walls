﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<System.Char>
struct Predicate_1_t2296;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m14469_gshared (Predicate_1_t2296 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m14469(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2296 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m14469_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m14470_gshared (Predicate_1_t2296 * __this, uint16_t ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m14470(__this, ___obj, method) (( bool (*) (Predicate_1_t2296 *, uint16_t, const MethodInfo*))Predicate_1_Invoke_m14470_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m14471_gshared (Predicate_1_t2296 * __this, uint16_t ___obj, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m14471(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2296 *, uint16_t, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m14471_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m14472_gshared (Predicate_1_t2296 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m14472(__this, ___result, method) (( bool (*) (Predicate_1_t2296 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m14472_gshared)(__this, ___result, method)
