﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1/Enumerator<System.Int32>
struct Enumerator_t2284;
// System.Object
struct Object_t;
// System.Collections.Generic.Queue`1<System.Int32>
struct Queue_1_t268;

// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C" void Enumerator__ctor_m14303_gshared (Enumerator_t2284 * __this, Queue_1_t268 * ___q, const MethodInfo* method);
#define Enumerator__ctor_m14303(__this, ___q, method) (( void (*) (Enumerator_t2284 *, Queue_1_t268 *, const MethodInfo*))Enumerator__ctor_m14303_gshared)(__this, ___q, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14304_gshared (Enumerator_t2284 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14304(__this, method) (( Object_t * (*) (Enumerator_t2284 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14304_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m14305_gshared (Enumerator_t2284 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14305(__this, method) (( void (*) (Enumerator_t2284 *, const MethodInfo*))Enumerator_Dispose_m14305_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14306_gshared (Enumerator_t2284 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14306(__this, method) (( bool (*) (Enumerator_t2284 *, const MethodInfo*))Enumerator_MoveNext_m14306_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m14307_gshared (Enumerator_t2284 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14307(__this, method) (( int32_t (*) (Enumerator_t2284 *, const MethodInfo*))Enumerator_get_Current_m14307_gshared)(__this, method)
