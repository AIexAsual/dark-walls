﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t323;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// InteractiveWheelChair
struct  InteractiveWheelChair_t342  : public MonoBehaviour_t4
{
	// UnityEngine.GameObject[] InteractiveWheelChair::wheels
	GameObjectU5BU5D_t323* ___wheels_2;
	// System.Single InteractiveWheelChair::turn
	float ___turn_3;
	// UnityEngine.Vector3 InteractiveWheelChair::oldPos
	Vector3_t215  ___oldPos_4;
	// UnityEngine.Vector3 InteractiveWheelChair::curPos
	Vector3_t215  ___curPos_5;
	// UnityEngine.Vector3 InteractiveWheelChair::movementPos
	Vector3_t215  ___movementPos_6;
	// UnityEngine.Vector3 InteractiveWheelChair::fwd
	Vector3_t215  ___fwd_7;
};
