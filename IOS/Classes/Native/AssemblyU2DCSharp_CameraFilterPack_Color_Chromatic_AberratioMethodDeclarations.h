﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_Chromatic_Aberration
struct CameraFilterPack_Color_Chromatic_Aberration_t61;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_Chromatic_Aberration::.ctor()
extern "C" void CameraFilterPack_Color_Chromatic_Aberration__ctor_m380 (CameraFilterPack_Color_Chromatic_Aberration_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Chromatic_Aberration::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_Chromatic_Aberration_get_material_m381 (CameraFilterPack_Color_Chromatic_Aberration_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::Start()
extern "C" void CameraFilterPack_Color_Chromatic_Aberration_Start_m382 (CameraFilterPack_Color_Chromatic_Aberration_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_Chromatic_Aberration_OnRenderImage_m383 (CameraFilterPack_Color_Chromatic_Aberration_t61 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::OnValidate()
extern "C" void CameraFilterPack_Color_Chromatic_Aberration_OnValidate_m384 (CameraFilterPack_Color_Chromatic_Aberration_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::Update()
extern "C" void CameraFilterPack_Color_Chromatic_Aberration_Update_m385 (CameraFilterPack_Color_Chromatic_Aberration_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Chromatic_Aberration::OnDisable()
extern "C" void CameraFilterPack_Color_Chromatic_Aberration_OnDisable_m386 (CameraFilterPack_Color_Chromatic_Aberration_t61 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
