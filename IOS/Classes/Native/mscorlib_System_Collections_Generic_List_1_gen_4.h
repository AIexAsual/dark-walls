﻿#pragma once
#include <stdint.h>
// System.Boolean[]
struct BooleanU5BU5D_t448;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Boolean>
struct  List_1_t577  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Boolean>::_items
	BooleanU5BU5D_t448* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Boolean>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Boolean>::_version
	int32_t ____version_3;
};
struct List_1_t577_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Boolean>::EmptyArray
	BooleanU5BU5D_t448* ___EmptyArray_4;
};
