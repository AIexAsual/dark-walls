﻿#pragma once
#include <stdint.h>
// System.OperatingSystem
struct OperatingSystem_t2104;
// System.Object
#include "mscorlib_System_Object.h"
// System.Environment
struct  Environment_t2105  : public Object_t
{
};
struct Environment_t2105_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t2104 * ___os_0;
};
