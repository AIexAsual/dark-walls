﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Metadata Definition UnityEngine.Events.InvokableCall`2
extern TypeInfo InvokableCall_2_t3408_il2cpp_TypeInfo;
static const EncodedMethodIndex InvokableCall_2_t3408_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1557,
	1558,
};
extern const Il2CppType UnityAction_2_t3664_0_0_0;
extern const Il2CppType InvokableCall_2_t3408_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3408_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition InvokableCall_2_t3408_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 6646 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6646 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5471 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5472 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4255 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4256 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5473 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_2_t3408_0_0_0;
extern const Il2CppType InvokableCall_2_t3408_1_0_0;
extern const Il2CppType BaseInvokableCall_t1061_0_0_0;
struct InvokableCall_2_t3408;
const Il2CppTypeDefinitionMetadata InvokableCall_2_t3408_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1061_0_0_0/* parent */
	, InvokableCall_2_t3408_VTable/* vtableMethods */
	, InvokableCall_2_t3408_RGCTXData/* rgctxDefinition */
	, 4984/* fieldStart */
	, 6031/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvokableCall_2_t3408_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &InvokableCall_2_t3408_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_2_t3408_0_0_0/* byval_arg */
	, &InvokableCall_2_t3408_1_0_0/* this_arg */
	, &InvokableCall_2_t3408_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 47/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`3
extern TypeInfo InvokableCall_3_t3409_il2cpp_TypeInfo;
static const EncodedMethodIndex InvokableCall_3_t3409_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1559,
	1560,
};
extern const Il2CppType UnityAction_3_t3667_0_0_0;
extern const Il2CppType InvokableCall_3_t3409_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3409_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3409_gp_2_0_0_0;
extern const Il2CppRGCTXDefinition InvokableCall_3_t3409_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 6649 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6649 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5474 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5475 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5476 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4258 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4259 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4260 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5477 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_3_t3409_0_0_0;
extern const Il2CppType InvokableCall_3_t3409_1_0_0;
struct InvokableCall_3_t3409;
const Il2CppTypeDefinitionMetadata InvokableCall_3_t3409_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1061_0_0_0/* parent */
	, InvokableCall_3_t3409_VTable/* vtableMethods */
	, InvokableCall_3_t3409_RGCTXData/* rgctxDefinition */
	, 4985/* fieldStart */
	, 6034/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvokableCall_3_t3409_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &InvokableCall_3_t3409_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_3_t3409_0_0_0/* byval_arg */
	, &InvokableCall_3_t3409_1_0_0/* this_arg */
	, &InvokableCall_3_t3409_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 48/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`4
extern TypeInfo InvokableCall_4_t3410_il2cpp_TypeInfo;
static const EncodedMethodIndex InvokableCall_4_t3410_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1561,
	1562,
};
extern const Il2CppType UnityAction_4_t3671_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_gp_3_0_0_0;
extern const Il2CppRGCTXDefinition InvokableCall_4_t3410_RGCTXData[12] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 6652 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6652 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5478 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5479 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5480 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5481 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4262 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4263 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4264 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4265 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5482 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_4_t3410_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_1_0_0;
struct InvokableCall_4_t3410;
const Il2CppTypeDefinitionMetadata InvokableCall_4_t3410_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1061_0_0_0/* parent */
	, InvokableCall_4_t3410_VTable/* vtableMethods */
	, InvokableCall_4_t3410_RGCTXData/* rgctxDefinition */
	, 4986/* fieldStart */
	, 6037/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvokableCall_4_t3410_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &InvokableCall_4_t3410_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_4_t3410_0_0_0/* byval_arg */
	, &InvokableCall_4_t3410_1_0_0/* this_arg */
	, &InvokableCall_4_t3410_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 49/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1
extern TypeInfo CachedInvokableCall_1_t1168_il2cpp_TypeInfo;
static const EncodedMethodIndex CachedInvokableCall_1_t1168_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1563,
	2147485212,
};
extern const Il2CppType InvokableCall_1_t3676_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1168_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition CachedInvokableCall_1_t1168_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5484 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6655 }/* Static */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4266 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5485 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CachedInvokableCall_1_t1168_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1168_1_0_0;
struct CachedInvokableCall_1_t1168;
const Il2CppTypeDefinitionMetadata CachedInvokableCall_1_t1168_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InvokableCall_1_t3676_0_0_0/* parent */
	, CachedInvokableCall_1_t1168_VTable/* vtableMethods */
	, CachedInvokableCall_1_t1168_RGCTXData/* rgctxDefinition */
	, 4987/* fieldStart */
	, 6040/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CachedInvokableCall_1_t1168_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &CachedInvokableCall_1_t1168_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CachedInvokableCall_1_t1168_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t1168_1_0_0/* this_arg */
	, &CachedInvokableCall_1_t1168_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 50/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// Metadata Definition UnityEngine.Events.UnityEventCallState
extern TypeInfo UnityEventCallState_t1063_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
static const EncodedMethodIndex UnityEventCallState_t1063_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair UnityEventCallState_t1063_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventCallState_t1063_0_0_0;
extern const Il2CppType UnityEventCallState_t1063_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UnityEventCallState_t1063_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEventCallState_t1063_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, UnityEventCallState_t1063_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4988/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEventCallState_t1063_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventCallState_t1063_0_0_0/* byval_arg */
	, &UnityEventCallState_t1063_1_0_0/* this_arg */
	, &UnityEventCallState_t1063_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t1063)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityEventCallState_t1063)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// Metadata Definition UnityEngine.Events.PersistentCall
extern TypeInfo PersistentCall_t1064_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
static const EncodedMethodIndex PersistentCall_t1064_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCall_t1064_0_0_0;
extern const Il2CppType PersistentCall_t1064_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct PersistentCall_t1064;
const Il2CppTypeDefinitionMetadata PersistentCall_t1064_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCall_t1064_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4992/* fieldStart */
	, 6042/* methodStart */
	, -1/* eventStart */
	, 1193/* propertyStart */

};
TypeInfo PersistentCall_t1064_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &PersistentCall_t1064_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCall_t1064_0_0_0/* byval_arg */
	, &PersistentCall_t1064_1_0_0/* this_arg */
	, &PersistentCall_t1064_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t1064)/* instance_size */
	, sizeof (PersistentCall_t1064)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern TypeInfo PersistentCallGroup_t1066_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
static const EncodedMethodIndex PersistentCallGroup_t1066_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCallGroup_t1066_0_0_0;
extern const Il2CppType PersistentCallGroup_t1066_1_0_0;
struct PersistentCallGroup_t1066;
const Il2CppTypeDefinitionMetadata PersistentCallGroup_t1066_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCallGroup_t1066_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4997/* fieldStart */
	, 6050/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PersistentCallGroup_t1066_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &PersistentCallGroup_t1066_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCallGroup_t1066_0_0_0/* byval_arg */
	, &PersistentCallGroup_t1066_1_0_0/* this_arg */
	, &PersistentCallGroup_t1066_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t1066)/* instance_size */
	, sizeof (PersistentCallGroup_t1066)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// Metadata Definition UnityEngine.Events.InvokableCallList
extern TypeInfo InvokableCallList_t1068_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
static const EncodedMethodIndex InvokableCallList_t1068_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCallList_t1068_0_0_0;
extern const Il2CppType InvokableCallList_t1068_1_0_0;
struct InvokableCallList_t1068;
const Il2CppTypeDefinitionMetadata InvokableCallList_t1068_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InvokableCallList_t1068_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 4998/* fieldStart */
	, 6052/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvokableCallList_t1068_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &InvokableCallList_t1068_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCallList_t1068_0_0_0/* byval_arg */
	, &InvokableCallList_t1068_1_0_0/* this_arg */
	, &InvokableCallList_t1068_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t1068)/* instance_size */
	, sizeof (InvokableCallList_t1068)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// Metadata Definition UnityEngine.Events.UnityEventBase
extern TypeInfo UnityEventBase_t1069_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
static const EncodedMethodIndex UnityEventBase_t1069_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	0,
	0,
};
extern const Il2CppType ISerializationCallbackReceiver_t3399_0_0_0;
static const Il2CppType* UnityEventBase_t1069_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t3399_0_0_0,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t1069_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventBase_t1069_0_0_0;
extern const Il2CppType UnityEventBase_t1069_1_0_0;
struct UnityEventBase_t1069;
const Il2CppTypeDefinitionMetadata UnityEventBase_t1069_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UnityEventBase_t1069_InterfacesTypeInfos/* implementedInterfaces */
	, UnityEventBase_t1069_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityEventBase_t1069_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5001/* fieldStart */
	, 6058/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEventBase_t1069_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEventBase_t1069_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventBase_t1069_0_0_0/* byval_arg */
	, &UnityEventBase_t1069_1_0_0/* this_arg */
	, &UnityEventBase_t1069_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t1069)/* instance_size */
	, sizeof (UnityEventBase_t1069)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// Metadata Definition UnityEngine.Events.UnityEvent
extern TypeInfo UnityEvent_t633_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
static const EncodedMethodIndex UnityEvent_t633_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	987,
	988,
};
static Il2CppInterfaceOffsetPair UnityEvent_t633_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_t633_0_0_0;
extern const Il2CppType UnityEvent_t633_1_0_0;
struct UnityEvent_t633;
const Il2CppTypeDefinitionMetadata UnityEvent_t633_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_t633_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1069_0_0_0/* parent */
	, UnityEvent_t633_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5005/* fieldStart */
	, 6072/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_t633_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_t633_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_t633_0_0_0/* byval_arg */
	, &UnityEvent_t633_1_0_0/* this_arg */
	, &UnityEvent_t633_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t633)/* instance_size */
	, sizeof (UnityEvent_t633)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`1
extern TypeInfo UnityEvent_1_t3411_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_1_t3411_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	1565,
	1566,
};
static Il2CppInterfaceOffsetPair UnityEvent_1_t3411_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern const Il2CppType UnityEvent_1_t3678_0_0_0;
extern const Il2CppType UnityEvent_1_t3411_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t3680_0_0_0;
extern const Il2CppRGCTXDefinition UnityEvent_1_t3411_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5486 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6664 }/* Static */,
	{ IL2CPP_RGCTX_DATA_TYPE, 4279 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6665 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5487 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5488 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4279 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_1_t3411_0_0_0;
extern const Il2CppType UnityEvent_1_t3411_1_0_0;
struct UnityEvent_1_t3411;
const Il2CppTypeDefinitionMetadata UnityEvent_1_t3411_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_1_t3411_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1069_0_0_0/* parent */
	, UnityEvent_1_t3411_VTable/* vtableMethods */
	, UnityEvent_1_t3411_RGCTXData/* rgctxDefinition */
	, 5006/* fieldStart */
	, 6076/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_1_t3411_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_1_t3411_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_1_t3411_0_0_0/* byval_arg */
	, &UnityEvent_1_t3411_1_0_0/* this_arg */
	, &UnityEvent_1_t3411_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 51/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`2
extern TypeInfo UnityEvent_2_t3412_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_2_t3412_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	1567,
	1568,
};
static Il2CppInterfaceOffsetPair UnityEvent_2_t3412_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern const Il2CppType UnityEvent_2_t3412_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t3412_gp_1_0_0_0;
extern const Il2CppType InvokableCall_2_t3683_0_0_0;
extern const Il2CppRGCTXDefinition UnityEvent_2_t3412_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 6668 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 6669 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6670 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5489 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_2_t3412_0_0_0;
extern const Il2CppType UnityEvent_2_t3412_1_0_0;
struct UnityEvent_2_t3412;
const Il2CppTypeDefinitionMetadata UnityEvent_2_t3412_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_2_t3412_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1069_0_0_0/* parent */
	, UnityEvent_2_t3412_VTable/* vtableMethods */
	, UnityEvent_2_t3412_RGCTXData/* rgctxDefinition */
	, 5007/* fieldStart */
	, 6083/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_2_t3412_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_2_t3412_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_2_t3412_0_0_0/* byval_arg */
	, &UnityEvent_2_t3412_1_0_0/* this_arg */
	, &UnityEvent_2_t3412_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 52/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`3
extern TypeInfo UnityEvent_3_t3413_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_3_t3413_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	1569,
	1570,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t3413_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern const Il2CppType UnityEvent_3_t3413_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3413_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3413_gp_2_0_0_0;
extern const Il2CppType InvokableCall_3_t3687_0_0_0;
extern const Il2CppRGCTXDefinition UnityEvent_3_t3413_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 6673 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 6674 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 6675 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6676 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5490 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_3_t3413_0_0_0;
extern const Il2CppType UnityEvent_3_t3413_1_0_0;
struct UnityEvent_3_t3413;
const Il2CppTypeDefinitionMetadata UnityEvent_3_t3413_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_3_t3413_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1069_0_0_0/* parent */
	, UnityEvent_3_t3413_VTable/* vtableMethods */
	, UnityEvent_3_t3413_RGCTXData/* rgctxDefinition */
	, 5008/* fieldStart */
	, 6086/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_3_t3413_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_3_t3413_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_3_t3413_0_0_0/* byval_arg */
	, &UnityEvent_3_t3413_1_0_0/* this_arg */
	, &UnityEvent_3_t3413_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 53/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`4
extern TypeInfo UnityEvent_4_t3414_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityEvent_4_t3414_VTable[8] = 
{
	626,
	601,
	627,
	913,
	914,
	915,
	1571,
	1572,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t3414_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t3399_0_0_0, 4},
};
extern const Il2CppType UnityEvent_4_t3414_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t3414_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t3414_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t3414_gp_3_0_0_0;
extern const Il2CppType InvokableCall_4_t3692_0_0_0;
extern const Il2CppRGCTXDefinition UnityEvent_4_t3414_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 6679 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 6680 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 6681 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, 6682 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6683 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5491 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_4_t3414_0_0_0;
extern const Il2CppType UnityEvent_4_t3414_1_0_0;
struct UnityEvent_4_t3414;
const Il2CppTypeDefinitionMetadata UnityEvent_4_t3414_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_4_t3414_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1069_0_0_0/* parent */
	, UnityEvent_4_t3414_VTable/* vtableMethods */
	, UnityEvent_4_t3414_RGCTXData/* rgctxDefinition */
	, 5009/* fieldStart */
	, 6089/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityEvent_4_t3414_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityEvent_4_t3414_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_4_t3414_0_0_0/* byval_arg */
	, &UnityEvent_4_t3414_1_0_0/* this_arg */
	, &UnityEvent_4_t3414_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 54/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern TypeInfo UserAuthorizationDialog_t1070_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
static const EncodedMethodIndex UserAuthorizationDialog_t1070_VTable[4] = 
{
	600,
	601,
	602,
	603,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserAuthorizationDialog_t1070_0_0_0;
extern const Il2CppType UserAuthorizationDialog_t1070_1_0_0;
extern const Il2CppType MonoBehaviour_t4_0_0_0;
struct UserAuthorizationDialog_t1070;
const Il2CppTypeDefinitionMetadata UserAuthorizationDialog_t1070_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t4_0_0_0/* parent */
	, UserAuthorizationDialog_t1070_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5010/* fieldStart */
	, 6092/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UserAuthorizationDialog_t1070_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, NULL/* methods */
	, &UserAuthorizationDialog_t1070_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2269/* custom_attributes_cache */
	, &UserAuthorizationDialog_t1070_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t1070_1_0_0/* this_arg */
	, &UserAuthorizationDialog_t1070_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t1070)/* instance_size */
	, sizeof (UserAuthorizationDialog_t1070)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t1071_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
static const EncodedMethodIndex DefaultValueAttribute_t1071_VTable[4] = 
{
	1573,
	601,
	1574,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1071_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DefaultValueAttribute_t1071_0_0_0;
extern const Il2CppType DefaultValueAttribute_t1071_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct DefaultValueAttribute_t1071;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t1071_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t1071_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DefaultValueAttribute_t1071_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5014/* fieldStart */
	, 6096/* methodStart */
	, -1/* eventStart */
	, 1197/* propertyStart */

};
TypeInfo DefaultValueAttribute_t1071_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, NULL/* methods */
	, &DefaultValueAttribute_t1071_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2270/* custom_attributes_cache */
	, &DefaultValueAttribute_t1071_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1071_1_0_0/* this_arg */
	, &DefaultValueAttribute_t1071_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1071)/* instance_size */
	, sizeof (DefaultValueAttribute_t1071)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern TypeInfo ExcludeFromDocsAttribute_t1072_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
static const EncodedMethodIndex ExcludeFromDocsAttribute_t1072_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t1072_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExcludeFromDocsAttribute_t1072_0_0_0;
extern const Il2CppType ExcludeFromDocsAttribute_t1072_1_0_0;
struct ExcludeFromDocsAttribute_t1072;
const Il2CppTypeDefinitionMetadata ExcludeFromDocsAttribute_t1072_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExcludeFromDocsAttribute_t1072_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ExcludeFromDocsAttribute_t1072_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6100/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExcludeFromDocsAttribute_t1072_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, NULL/* methods */
	, &ExcludeFromDocsAttribute_t1072_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2271/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t1072_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t1072_1_0_0/* this_arg */
	, &ExcludeFromDocsAttribute_t1072_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t1072)/* instance_size */
	, sizeof (ExcludeFromDocsAttribute_t1072)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern TypeInfo FormerlySerializedAsAttribute_t1073_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
static const EncodedMethodIndex FormerlySerializedAsAttribute_t1073_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t1073_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FormerlySerializedAsAttribute_t1073_0_0_0;
extern const Il2CppType FormerlySerializedAsAttribute_t1073_1_0_0;
struct FormerlySerializedAsAttribute_t1073;
const Il2CppTypeDefinitionMetadata FormerlySerializedAsAttribute_t1073_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormerlySerializedAsAttribute_t1073_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, FormerlySerializedAsAttribute_t1073_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5015/* fieldStart */
	, 6101/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormerlySerializedAsAttribute_t1073_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, NULL/* methods */
	, &FormerlySerializedAsAttribute_t1073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2272/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t1073_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t1073_1_0_0/* this_arg */
	, &FormerlySerializedAsAttribute_t1073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t1073)/* instance_size */
	, sizeof (FormerlySerializedAsAttribute_t1073)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern TypeInfo TypeInferenceRules_t1074_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
static const EncodedMethodIndex TypeInferenceRules_t1074_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t1074_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRules_t1074_0_0_0;
extern const Il2CppType TypeInferenceRules_t1074_1_0_0;
const Il2CppTypeDefinitionMetadata TypeInferenceRules_t1074_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRules_t1074_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TypeInferenceRules_t1074_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5016/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeInferenceRules_t1074_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInferenceRules_t1074_0_0_0/* byval_arg */
	, &TypeInferenceRules_t1074_1_0_0/* this_arg */
	, &TypeInferenceRules_t1074_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t1074)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeInferenceRules_t1074)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern TypeInfo TypeInferenceRuleAttribute_t1075_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
static const EncodedMethodIndex TypeInferenceRuleAttribute_t1075_VTable[4] = 
{
	1366,
	601,
	1367,
	1575,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t1075_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRuleAttribute_t1075_0_0_0;
extern const Il2CppType TypeInferenceRuleAttribute_t1075_1_0_0;
struct TypeInferenceRuleAttribute_t1075;
const Il2CppTypeDefinitionMetadata TypeInferenceRuleAttribute_t1075_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRuleAttribute_t1075_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, TypeInferenceRuleAttribute_t1075_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5021/* fieldStart */
	, 6102/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeInferenceRuleAttribute_t1075_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &TypeInferenceRuleAttribute_t1075_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2273/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t1075_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t1075_1_0_0/* this_arg */
	, &TypeInferenceRuleAttribute_t1075_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t1075)/* instance_size */
	, sizeof (TypeInferenceRuleAttribute_t1075)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// Metadata Definition UnityEngineInternal.GenericStack
extern TypeInfo GenericStack_t870_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
static const EncodedMethodIndex GenericStack_t870_VTable[18] = 
{
	626,
	601,
	627,
	628,
	1576,
	1577,
	1578,
	1579,
	1580,
	1577,
	1578,
	1579,
	1581,
	1580,
	1576,
	1582,
	1583,
	1584,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ICollection_t1528_0_0_0;
static Il2CppInterfaceOffsetPair GenericStack_t870_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GenericStack_t870_0_0_0;
extern const Il2CppType GenericStack_t870_1_0_0;
extern const Il2CppType Stack_t1076_0_0_0;
struct GenericStack_t870;
const Il2CppTypeDefinitionMetadata GenericStack_t870_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericStack_t870_InterfacesOffsets/* interfaceOffsets */
	, &Stack_t1076_0_0_0/* parent */
	, GenericStack_t870_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6105/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GenericStack_t870_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, NULL/* methods */
	, &GenericStack_t870_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericStack_t870_0_0_0/* byval_arg */
	, &GenericStack_t870_1_0_0/* this_arg */
	, &GenericStack_t870_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t870)/* instance_size */
	, sizeof (GenericStack_t870)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// Metadata Definition UnityEngine.Events.UnityAction
extern TypeInfo UnityAction_t651_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
static const EncodedMethodIndex UnityAction_t651_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1585,
	1586,
	1587,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair UnityAction_t651_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_t651_0_0_0;
extern const Il2CppType UnityAction_t651_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct UnityAction_t651;
const Il2CppTypeDefinitionMetadata UnityAction_t651_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_t651_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, UnityAction_t651_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6106/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityAction_t651_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityAction_t651_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_t651_0_0_0/* byval_arg */
	, &UnityAction_t651_1_0_0/* this_arg */
	, &UnityAction_t651_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t651/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t651)/* instance_size */
	, sizeof (UnityAction_t651)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`1
extern TypeInfo UnityAction_1_t3415_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityAction_1_t3415_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1588,
	1589,
	1590,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3415_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_1_t3415_0_0_0;
extern const Il2CppType UnityAction_1_t3415_1_0_0;
struct UnityAction_1_t3415;
const Il2CppTypeDefinitionMetadata UnityAction_1_t3415_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_1_t3415_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, UnityAction_1_t3415_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6110/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityAction_1_t3415_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityAction_1_t3415_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_1_t3415_0_0_0/* byval_arg */
	, &UnityAction_1_t3415_1_0_0/* this_arg */
	, &UnityAction_1_t3415_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 55/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`2
extern TypeInfo UnityAction_2_t3416_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityAction_2_t3416_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1591,
	1592,
	1593,
};
static Il2CppInterfaceOffsetPair UnityAction_2_t3416_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_2_t3416_0_0_0;
extern const Il2CppType UnityAction_2_t3416_1_0_0;
struct UnityAction_2_t3416;
const Il2CppTypeDefinitionMetadata UnityAction_2_t3416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_2_t3416_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, UnityAction_2_t3416_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6114/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityAction_2_t3416_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityAction_2_t3416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_2_t3416_0_0_0/* byval_arg */
	, &UnityAction_2_t3416_1_0_0/* this_arg */
	, &UnityAction_2_t3416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 56/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`3
extern TypeInfo UnityAction_3_t3417_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityAction_3_t3417_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1594,
	1595,
	1596,
};
static Il2CppInterfaceOffsetPair UnityAction_3_t3417_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_3_t3417_0_0_0;
extern const Il2CppType UnityAction_3_t3417_1_0_0;
struct UnityAction_3_t3417;
const Il2CppTypeDefinitionMetadata UnityAction_3_t3417_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_3_t3417_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, UnityAction_3_t3417_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6118/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityAction_3_t3417_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityAction_3_t3417_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_3_t3417_0_0_0/* byval_arg */
	, &UnityAction_3_t3417_1_0_0/* this_arg */
	, &UnityAction_3_t3417_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 57/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`4
extern TypeInfo UnityAction_4_t3418_il2cpp_TypeInfo;
static const EncodedMethodIndex UnityAction_4_t3418_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1597,
	1598,
	1599,
};
static Il2CppInterfaceOffsetPair UnityAction_4_t3418_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_4_t3418_0_0_0;
extern const Il2CppType UnityAction_4_t3418_1_0_0;
struct UnityAction_4_t3418;
const Il2CppTypeDefinitionMetadata UnityAction_4_t3418_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_4_t3418_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, UnityAction_4_t3418_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6122/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityAction_4_t3418_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, NULL/* methods */
	, &UnityAction_4_t3418_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_4_t3418_0_0_0/* byval_arg */
	, &UnityAction_4_t3418_1_0_0/* this_arg */
	, &UnityAction_4_t3418_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 58/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
