﻿#pragma once
#include <stdint.h>
// UnityEngine.Renderer
struct Renderer_t312;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// FadeTexture
struct  FadeTexture_t313  : public MonoBehaviour_t4
{
	// UnityEngine.Renderer FadeTexture::mat
	Renderer_t312 * ___mat_2;
	// System.Single FadeTexture::val
	float ___val_3;
};
