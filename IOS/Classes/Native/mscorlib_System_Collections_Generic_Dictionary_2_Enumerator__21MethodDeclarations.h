﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t2791;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2788;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21516_gshared (Enumerator_t2791 * __this, Dictionary_2_t2788 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m21516(__this, ___dictionary, method) (( void (*) (Enumerator_t2791 *, Dictionary_2_t2788 *, const MethodInfo*))Enumerator__ctor_m21516_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21517_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21517(__this, method) (( Object_t * (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21517_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21518_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21518(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21518_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21519_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21519(__this, method) (( Object_t * (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21519_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21520_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21520(__this, method) (( Object_t * (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21520_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21521_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21521(__this, method) (( bool (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_MoveNext_m21521_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t2762  Enumerator_get_Current_m21522_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21522(__this, method) (( KeyValuePair_2_t2762  (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_get_Current_m21522_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m21523_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m21523(__this, method) (( Object_t * (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_get_CurrentKey_m21523_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_CurrentValue()
extern "C" KeyValuePair_2_t2350  Enumerator_get_CurrentValue_m21524_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m21524(__this, method) (( KeyValuePair_2_t2350  (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_get_CurrentValue_m21524_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyState()
extern "C" void Enumerator_VerifyState_m21525_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m21525(__this, method) (( void (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_VerifyState_m21525_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m21526_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m21526(__this, method) (( void (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_VerifyCurrent_m21526_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m21527_gshared (Enumerator_t2791 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21527(__this, method) (( void (*) (Enumerator_t2791 *, const MethodInfo*))Enumerator_Dispose_m21527_gshared)(__this, method)
