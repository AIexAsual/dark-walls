﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t323;
// UnityEngine.Rigidbody
struct Rigidbody_t325;
// UnityEngine.Animator
struct Animator_t321;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InteractiveBurnMama
struct  InteractiveBurnMama_t326  : public MonoBehaviour_t4
{
	// UnityEngine.GameObject InteractiveBurnMama::head
	GameObject_t256 * ___head_2;
	// System.Boolean InteractiveBurnMama::isRoll
	bool ___isRoll_3;
	// UnityEngine.GameObject[] InteractiveBurnMama::bodies
	GameObjectU5BU5D_t323* ___bodies_4;
	// UnityEngine.Rigidbody InteractiveBurnMama::rb
	Rigidbody_t325 * ___rb_5;
	// UnityEngine.Animator InteractiveBurnMama::anim
	Animator_t321 * ___anim_6;
};
