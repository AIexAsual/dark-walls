﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// WardObjects
struct  WardObjects_t373  : public Object_t
{
};
struct WardObjects_t373_StaticFields{
	// System.String WardObjects::WALL
	String_t* ___WALL_0;
	// System.String WardObjects::CEILING
	String_t* ___CEILING_1;
	// System.String WardObjects::FLOOR
	String_t* ___FLOOR_2;
	// System.String WardObjects::CURTAIN
	String_t* ___CURTAIN_3;
	// System.String WardObjects::BED
	String_t* ___BED_4;
	// System.String WardObjects::DOOR1
	String_t* ___DOOR1_5;
	// System.String WardObjects::DOOR2
	String_t* ___DOOR2_6;
};
