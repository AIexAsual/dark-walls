﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t934;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AudioSettings
struct  AudioSettings_t935  : public Object_t
{
};
struct AudioSettings_t935_StaticFields{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_t934 * ___OnAudioConfigurationChanged_0;
};
