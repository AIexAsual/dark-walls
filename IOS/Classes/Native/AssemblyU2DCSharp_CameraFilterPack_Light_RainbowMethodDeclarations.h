﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Light_Rainbow
struct CameraFilterPack_Light_Rainbow_t155;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Light_Rainbow::.ctor()
extern "C" void CameraFilterPack_Light_Rainbow__ctor_m1001 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Rainbow::get_material()
extern "C" Material_t2 * CameraFilterPack_Light_Rainbow_get_material_m1002 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::Start()
extern "C" void CameraFilterPack_Light_Rainbow_Start_m1003 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Light_Rainbow_OnRenderImage_m1004 (CameraFilterPack_Light_Rainbow_t155 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::OnValidate()
extern "C" void CameraFilterPack_Light_Rainbow_OnValidate_m1005 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::Update()
extern "C" void CameraFilterPack_Light_Rainbow_Update_m1006 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Rainbow::OnDisable()
extern "C" void CameraFilterPack_Light_Rainbow_OnDisable_m1007 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
