﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_BrokenGlass
struct CameraFilterPack_TV_BrokenGlass_t180;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_BrokenGlass::.ctor()
extern "C" void CameraFilterPack_TV_BrokenGlass__ctor_m1164 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_BrokenGlass::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_BrokenGlass_get_material_m1165 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::Start()
extern "C" void CameraFilterPack_TV_BrokenGlass_Start_m1166 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_BrokenGlass_OnRenderImage_m1167 (CameraFilterPack_TV_BrokenGlass_t180 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::Update()
extern "C" void CameraFilterPack_TV_BrokenGlass_Update_m1168 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_BrokenGlass::OnDisable()
extern "C" void CameraFilterPack_TV_BrokenGlass_OnDisable_m1169 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
