﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_EXTRA_SHOWFPS
struct CameraFilterPack_EXTRA_SHOWFPS_t110;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void CameraFilterPack_EXTRA_SHOWFPS::.ctor()
extern "C" void CameraFilterPack_EXTRA_SHOWFPS__ctor_m711 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_EXTRA_SHOWFPS::get_material()
extern "C" Material_t2 * CameraFilterPack_EXTRA_SHOWFPS_get_material_m712 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EXTRA_SHOWFPS::Start()
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_Start_m713 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EXTRA_SHOWFPS::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_OnRenderImage_m714 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EXTRA_SHOWFPS::OnValidate()
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_OnValidate_m715 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CameraFilterPack_EXTRA_SHOWFPS::FPSX()
extern "C" Object_t * CameraFilterPack_EXTRA_SHOWFPS_FPSX_m716 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EXTRA_SHOWFPS::Update()
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_Update_m717 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EXTRA_SHOWFPS::OnDisable()
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_OnDisable_m718 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
