﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t1371;
// System.Text.StringBuilder
struct StringBuilder_t780;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
struct  MatchAppendEvaluator_t1452  : public MulticastDelegate_t219
{
};
