﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_DarkerColor
struct CameraFilterPack_Blend2Camera_DarkerColor_t22;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_DarkerColor::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_DarkerColor__ctor_m90 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_DarkerColor::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_DarkerColor_get_material_m91 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::Start()
extern "C" void CameraFilterPack_Blend2Camera_DarkerColor_Start_m92 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_DarkerColor_OnRenderImage_m93 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_DarkerColor_OnValidate_m94 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::Update()
extern "C" void CameraFilterPack_Blend2Camera_DarkerColor_Update_m95 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_DarkerColor_OnEnable_m96 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_DarkerColor::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_DarkerColor_OnDisable_m97 (CameraFilterPack_Blend2Camera_DarkerColor_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
