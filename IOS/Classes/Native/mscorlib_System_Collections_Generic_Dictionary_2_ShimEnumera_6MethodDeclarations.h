﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t2836;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2824;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m21959_gshared (ShimEnumerator_t2836 * __this, Dictionary_2_t2824 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m21959(__this, ___host, method) (( void (*) (ShimEnumerator_t2836 *, Dictionary_2_t2824 *, const MethodInfo*))ShimEnumerator__ctor_m21959_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m21960_gshared (ShimEnumerator_t2836 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m21960(__this, method) (( bool (*) (ShimEnumerator_t2836 *, const MethodInfo*))ShimEnumerator_MoveNext_m21960_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m21961_gshared (ShimEnumerator_t2836 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m21961(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2836 *, const MethodInfo*))ShimEnumerator_get_Entry_m21961_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m21962_gshared (ShimEnumerator_t2836 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m21962(__this, method) (( Object_t * (*) (ShimEnumerator_t2836 *, const MethodInfo*))ShimEnumerator_get_Key_m21962_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m21963_gshared (ShimEnumerator_t2836 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m21963(__this, method) (( Object_t * (*) (ShimEnumerator_t2836 *, const MethodInfo*))ShimEnumerator_get_Value_m21963_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m21964_gshared (ShimEnumerator_t2836 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m21964(__this, method) (( Object_t * (*) (ShimEnumerator_t2836 *, const MethodInfo*))ShimEnumerator_get_Current_m21964_gshared)(__this, method)
