﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<iTween/EaseType>
struct List_1_t580;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<iTween/EaseType>
struct IEnumerable_1_t3039;
// System.Collections.Generic.IEnumerator`1<iTween/EaseType>
struct IEnumerator_1_t3040;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<iTween/EaseType>
struct ICollection_1_t3041;
// System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/EaseType>
struct ReadOnlyCollection_1_t2450;
// iTween/EaseType[]
struct EaseTypeU5BU5D_t451;
// System.Predicate`1<iTween/EaseType>
struct Predicate_1_t2454;
// System.Comparison`1<iTween/EaseType>
struct Comparison_1_t2457;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// System.Collections.Generic.List`1/Enumerator<iTween/EaseType>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"

// System.Void System.Collections.Generic.List`1<iTween/EaseType>::.ctor()
extern "C" void List_1__ctor_m3395_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1__ctor_m3395(__this, method) (( void (*) (List_1_t580 *, const MethodInfo*))List_1__ctor_m3395_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m16358_gshared (List_1_t580 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m16358(__this, ___collection, method) (( void (*) (List_1_t580 *, Object_t*, const MethodInfo*))List_1__ctor_m16358_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::.ctor(System.Int32)
extern "C" void List_1__ctor_m16359_gshared (List_1_t580 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m16359(__this, ___capacity, method) (( void (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1__ctor_m16359_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::.cctor()
extern "C" void List_1__cctor_m16360_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m16360(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m16360_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16361_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16361(__this, method) (( Object_t* (*) (List_1_t580 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16361_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m16362_gshared (List_1_t580 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m16362(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t580 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m16362_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m16363_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16363(__this, method) (( Object_t * (*) (List_1_t580 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m16363_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m16364_gshared (List_1_t580 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m16364(__this, ___item, method) (( int32_t (*) (List_1_t580 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m16364_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m16365_gshared (List_1_t580 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m16365(__this, ___item, method) (( bool (*) (List_1_t580 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m16365_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m16366_gshared (List_1_t580 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m16366(__this, ___item, method) (( int32_t (*) (List_1_t580 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m16366_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m16367_gshared (List_1_t580 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m16367(__this, ___index, ___item, method) (( void (*) (List_1_t580 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m16367_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m16368_gshared (List_1_t580 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m16368(__this, ___item, method) (( void (*) (List_1_t580 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m16368_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16369_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16369(__this, method) (( bool (*) (List_1_t580 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16369_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m16370_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16370(__this, method) (( bool (*) (List_1_t580 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m16370_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m16371_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m16371(__this, method) (( Object_t * (*) (List_1_t580 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m16371_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m16372_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m16372(__this, method) (( bool (*) (List_1_t580 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m16372_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m16373_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m16373(__this, method) (( bool (*) (List_1_t580 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m16373_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m16374_gshared (List_1_t580 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m16374(__this, ___index, method) (( Object_t * (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m16374_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m16375_gshared (List_1_t580 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m16375(__this, ___index, ___value, method) (( void (*) (List_1_t580 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m16375_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::Add(T)
extern "C" void List_1_Add_m16376_gshared (List_1_t580 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m16376(__this, ___item, method) (( void (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_Add_m16376_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m16377_gshared (List_1_t580 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m16377(__this, ___newCount, method) (( void (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m16377_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m16378_gshared (List_1_t580 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m16378(__this, ___collection, method) (( void (*) (List_1_t580 *, Object_t*, const MethodInfo*))List_1_AddCollection_m16378_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m16379_gshared (List_1_t580 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m16379(__this, ___enumerable, method) (( void (*) (List_1_t580 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m16379_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m16380_gshared (List_1_t580 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m16380(__this, ___collection, method) (( void (*) (List_1_t580 *, Object_t*, const MethodInfo*))List_1_AddRange_m16380_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<iTween/EaseType>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2450 * List_1_AsReadOnly_m16381_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m16381(__this, method) (( ReadOnlyCollection_1_t2450 * (*) (List_1_t580 *, const MethodInfo*))List_1_AsReadOnly_m16381_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::Clear()
extern "C" void List_1_Clear_m16382_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_Clear_m16382(__this, method) (( void (*) (List_1_t580 *, const MethodInfo*))List_1_Clear_m16382_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<iTween/EaseType>::Contains(T)
extern "C" bool List_1_Contains_m16383_gshared (List_1_t580 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m16383(__this, ___item, method) (( bool (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_Contains_m16383_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m16384_gshared (List_1_t580 * __this, EaseTypeU5BU5D_t451* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m16384(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t580 *, EaseTypeU5BU5D_t451*, int32_t, const MethodInfo*))List_1_CopyTo_m16384_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<iTween/EaseType>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m16385_gshared (List_1_t580 * __this, Predicate_1_t2454 * ___match, const MethodInfo* method);
#define List_1_Find_m16385(__this, ___match, method) (( int32_t (*) (List_1_t580 *, Predicate_1_t2454 *, const MethodInfo*))List_1_Find_m16385_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m16386_gshared (Object_t * __this /* static, unused */, Predicate_1_t2454 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m16386(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2454 *, const MethodInfo*))List_1_CheckMatch_m16386_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m16387_gshared (List_1_t580 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2454 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m16387(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t580 *, int32_t, int32_t, Predicate_1_t2454 *, const MethodInfo*))List_1_GetIndex_m16387_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<iTween/EaseType>::GetEnumerator()
extern "C" Enumerator_t2449  List_1_GetEnumerator_m16388_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m16388(__this, method) (( Enumerator_t2449  (*) (List_1_t580 *, const MethodInfo*))List_1_GetEnumerator_m16388_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m16389_gshared (List_1_t580 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m16389(__this, ___item, method) (( int32_t (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_IndexOf_m16389_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m16390_gshared (List_1_t580 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m16390(__this, ___start, ___delta, method) (( void (*) (List_1_t580 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m16390_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m16391_gshared (List_1_t580 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m16391(__this, ___index, method) (( void (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_CheckIndex_m16391_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m16392_gshared (List_1_t580 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m16392(__this, ___index, ___item, method) (( void (*) (List_1_t580 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m16392_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m16393_gshared (List_1_t580 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m16393(__this, ___collection, method) (( void (*) (List_1_t580 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m16393_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<iTween/EaseType>::Remove(T)
extern "C" bool List_1_Remove_m16394_gshared (List_1_t580 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m16394(__this, ___item, method) (( bool (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_Remove_m16394_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m16395_gshared (List_1_t580 * __this, Predicate_1_t2454 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m16395(__this, ___match, method) (( int32_t (*) (List_1_t580 *, Predicate_1_t2454 *, const MethodInfo*))List_1_RemoveAll_m16395_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m16396_gshared (List_1_t580 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m16396(__this, ___index, method) (( void (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_RemoveAt_m16396_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::Reverse()
extern "C" void List_1_Reverse_m16397_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_Reverse_m16397(__this, method) (( void (*) (List_1_t580 *, const MethodInfo*))List_1_Reverse_m16397_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::Sort()
extern "C" void List_1_Sort_m16398_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_Sort_m16398(__this, method) (( void (*) (List_1_t580 *, const MethodInfo*))List_1_Sort_m16398_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m16399_gshared (List_1_t580 * __this, Comparison_1_t2457 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m16399(__this, ___comparison, method) (( void (*) (List_1_t580 *, Comparison_1_t2457 *, const MethodInfo*))List_1_Sort_m16399_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<iTween/EaseType>::ToArray()
extern "C" EaseTypeU5BU5D_t451* List_1_ToArray_m3408_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_ToArray_m3408(__this, method) (( EaseTypeU5BU5D_t451* (*) (List_1_t580 *, const MethodInfo*))List_1_ToArray_m3408_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::TrimExcess()
extern "C" void List_1_TrimExcess_m16400_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m16400(__this, method) (( void (*) (List_1_t580 *, const MethodInfo*))List_1_TrimExcess_m16400_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m16401_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m16401(__this, method) (( int32_t (*) (List_1_t580 *, const MethodInfo*))List_1_get_Capacity_m16401_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m16402_gshared (List_1_t580 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m16402(__this, ___value, method) (( void (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_set_Capacity_m16402_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::get_Count()
extern "C" int32_t List_1_get_Count_m16403_gshared (List_1_t580 * __this, const MethodInfo* method);
#define List_1_get_Count_m16403(__this, method) (( int32_t (*) (List_1_t580 *, const MethodInfo*))List_1_get_Count_m16403_gshared)(__this, method)
// T System.Collections.Generic.List`1<iTween/EaseType>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m16404_gshared (List_1_t580 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m16404(__this, ___index, method) (( int32_t (*) (List_1_t580 *, int32_t, const MethodInfo*))List_1_get_Item_m16404_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<iTween/EaseType>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m16405_gshared (List_1_t580 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m16405(__this, ___index, ___value, method) (( void (*) (List_1_t580 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m16405_gshared)(__this, ___index, ___value, method)
