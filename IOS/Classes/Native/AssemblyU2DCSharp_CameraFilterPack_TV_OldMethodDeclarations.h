﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Old
struct CameraFilterPack_TV_Old_t187;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Old::.ctor()
extern "C" void CameraFilterPack_TV_Old__ctor_m1209 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Old::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Old_get_material_m1210 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::Start()
extern "C" void CameraFilterPack_TV_Old_Start_m1211 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Old_OnRenderImage_m1212 (CameraFilterPack_TV_Old_t187 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::OnValidate()
extern "C" void CameraFilterPack_TV_Old_OnValidate_m1213 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::Update()
extern "C" void CameraFilterPack_TV_Old_Update_m1214 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Old::OnDisable()
extern "C" void CameraFilterPack_TV_Old_OnDisable_m1215 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
