﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t828;
// UnityEngine.AssetBundle
struct AssetBundle_t830;

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m4969 (AssetBundleCreateRequest_t828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t830 * AssetBundleCreateRequest_get_assetBundle_m4970 (AssetBundleCreateRequest_t828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m4971 (AssetBundleCreateRequest_t828 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
