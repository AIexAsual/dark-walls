﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Dissipation
struct CameraFilterPack_Distortion_Dissipation_t82;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Dissipation::.ctor()
extern "C" void CameraFilterPack_Distortion_Dissipation__ctor_m510 (CameraFilterPack_Distortion_Dissipation_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Dissipation::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Dissipation_get_material_m511 (CameraFilterPack_Distortion_Dissipation_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::Start()
extern "C" void CameraFilterPack_Distortion_Dissipation_Start_m512 (CameraFilterPack_Distortion_Dissipation_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Dissipation_OnRenderImage_m513 (CameraFilterPack_Distortion_Dissipation_t82 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::OnValidate()
extern "C" void CameraFilterPack_Distortion_Dissipation_OnValidate_m514 (CameraFilterPack_Distortion_Dissipation_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::Update()
extern "C" void CameraFilterPack_Distortion_Dissipation_Update_m515 (CameraFilterPack_Distortion_Dissipation_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dissipation::OnDisable()
extern "C" void CameraFilterPack_Distortion_Dissipation_OnDisable_m516 (CameraFilterPack_Distortion_Dissipation_t82 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
