﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_Rainbow
struct CameraFilterPack_Gradients_Rainbow_t151;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_Rainbow::.ctor()
extern "C" void CameraFilterPack_Gradients_Rainbow__ctor_m977 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Rainbow::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_Rainbow_get_material_m978 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::Start()
extern "C" void CameraFilterPack_Gradients_Rainbow_Start_m979 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_Rainbow_OnRenderImage_m980 (CameraFilterPack_Gradients_Rainbow_t151 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::Update()
extern "C" void CameraFilterPack_Gradients_Rainbow_Update_m981 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Rainbow::OnDisable()
extern "C" void CameraFilterPack_Gradients_Rainbow_OnDisable_m982 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
