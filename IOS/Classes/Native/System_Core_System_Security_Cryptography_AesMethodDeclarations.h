﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.Aes
struct Aes_t1180;

// System.Void System.Security.Cryptography.Aes::.ctor()
extern "C" void Aes__ctor_m6530 (Aes_t1180 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
