﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
struct Action_1_t841;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t1109;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_7MethodDeclarations.h"
#define Action_1__ctor_m19297(__this, ___object, ___method, method) (( void (*) (Action_1_t841 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m14500_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::Invoke(T)
#define Action_1_Invoke_m6350(__this, ___obj, method) (( void (*) (Action_1_t841 *, IAchievementU5BU5D_t1109*, const MethodInfo*))Action_1_Invoke_m14501_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m19298(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t841 *, IAchievementU5BU5D_t1109*, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m14503_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m19299(__this, ___result, method) (( void (*) (Action_1_t841 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m14505_gshared)(__this, ___result, method)
