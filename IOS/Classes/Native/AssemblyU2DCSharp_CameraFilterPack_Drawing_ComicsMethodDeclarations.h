﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Comics
struct CameraFilterPack_Drawing_Comics_t97;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Comics::.ctor()
extern "C" void CameraFilterPack_Drawing_Comics__ctor_m616 (CameraFilterPack_Drawing_Comics_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Comics::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Comics_get_material_m617 (CameraFilterPack_Drawing_Comics_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::Start()
extern "C" void CameraFilterPack_Drawing_Comics_Start_m618 (CameraFilterPack_Drawing_Comics_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Comics_OnRenderImage_m619 (CameraFilterPack_Drawing_Comics_t97 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::OnValidate()
extern "C" void CameraFilterPack_Drawing_Comics_OnValidate_m620 (CameraFilterPack_Drawing_Comics_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::Update()
extern "C" void CameraFilterPack_Drawing_Comics_Update_m621 (CameraFilterPack_Drawing_Comics_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Comics::OnDisable()
extern "C" void CameraFilterPack_Drawing_Comics_OnDisable_m622 (CameraFilterPack_Drawing_Comics_t97 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
