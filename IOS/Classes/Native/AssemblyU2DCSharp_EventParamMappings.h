﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Dictionary_2_t420;
// System.Object
#include "mscorlib_System_Object.h"
// EventParamMappings
struct  EventParamMappings_t421  : public Object_t
{
};
struct EventParamMappings_t421_StaticFields{
	// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>> EventParamMappings::mappings
	Dictionary_2_t420 * ___mappings_0;
};
