﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// System.Single[]
struct SingleU5BU5D_t72;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Colors_Adjust_PreFilters/filters
#include "AssemblyU2DCSharp_CameraFilterPack_Colors_Adjust_PreFilters_.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Colors_Adjust_PreFilters
struct  CameraFilterPack_Colors_Adjust_PreFilters_t73  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_Colors_Adjust_PreFilters::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Colors_Adjust_PreFilters::SCShader
	Shader_t1 * ___SCShader_3;
	// CameraFilterPack_Colors_Adjust_PreFilters/filters CameraFilterPack_Colors_Adjust_PreFilters::filterchoice
	int32_t ___filterchoice_4;
	// System.Single CameraFilterPack_Colors_Adjust_PreFilters::FadeFX
	float ___FadeFX_5;
	// CameraFilterPack_Colors_Adjust_PreFilters/filters CameraFilterPack_Colors_Adjust_PreFilters::filterchoicememo
	int32_t ___filterchoicememo_6;
	// System.Single CameraFilterPack_Colors_Adjust_PreFilters::TimeX
	float ___TimeX_7;
	// UnityEngine.Vector4 CameraFilterPack_Colors_Adjust_PreFilters::ScreenResolution
	Vector4_t5  ___ScreenResolution_8;
	// UnityEngine.Material CameraFilterPack_Colors_Adjust_PreFilters::SCMaterial
	Material_t2 * ___SCMaterial_9;
	// System.Single[] CameraFilterPack_Colors_Adjust_PreFilters::Matrix9
	SingleU5BU5D_t72* ___Matrix9_10;
	// System.Single CameraFilterPack_Colors_Adjust_PreFilters::Brightness
	float ___Brightness_11;
};
