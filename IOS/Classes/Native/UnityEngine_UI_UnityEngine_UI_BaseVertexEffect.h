﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t654;
// UnityEngine.EventSystems.UIBehaviour
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviour.h"
// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t756  : public UIBehaviour_t591
{
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::m_Graphic
	Graphic_t654 * ___m_Graphic_2;
};
