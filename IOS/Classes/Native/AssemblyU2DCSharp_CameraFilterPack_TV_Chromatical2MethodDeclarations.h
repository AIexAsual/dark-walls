﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Chromatical2
struct CameraFilterPack_TV_Chromatical2_t182;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Chromatical2::.ctor()
extern "C" void CameraFilterPack_TV_Chromatical2__ctor_m1176 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Chromatical2::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Chromatical2_get_material_m1177 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::Start()
extern "C" void CameraFilterPack_TV_Chromatical2_Start_m1178 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Chromatical2_OnRenderImage_m1179 (CameraFilterPack_TV_Chromatical2_t182 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::OnValidate()
extern "C" void CameraFilterPack_TV_Chromatical2_OnValidate_m1180 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::Update()
extern "C" void CameraFilterPack_TV_Chromatical2_Update_m1181 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical2::OnDisable()
extern "C" void CameraFilterPack_TV_Chromatical2_OnDisable_m1182 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
