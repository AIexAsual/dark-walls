﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture2D
struct Texture2D_t9;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.Texture2D>
struct  Action_1_t302  : public MulticastDelegate_t219
{
};
