﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.Single>
struct EqualityComparer_1_t2403;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.ctor()
extern "C" void EqualityComparer_1__ctor_m15758_gshared (EqualityComparer_1_t2403 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m15758(__this, method) (( void (*) (EqualityComparer_1_t2403 *, const MethodInfo*))EqualityComparer_1__ctor_m15758_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Single>::.cctor()
extern "C" void EqualityComparer_1__cctor_m15759_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m15759(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m15759_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15760_gshared (EqualityComparer_1_t2403 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15760(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2403 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m15760_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15761_gshared (EqualityComparer_1_t2403 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15761(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2403 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m15761_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::get_Default()
extern "C" EqualityComparer_1_t2403 * EqualityComparer_1_get_Default_m15762_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m15762(__this /* static, unused */, method) (( EqualityComparer_1_t2403 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m15762_gshared)(__this /* static, unused */, method)
