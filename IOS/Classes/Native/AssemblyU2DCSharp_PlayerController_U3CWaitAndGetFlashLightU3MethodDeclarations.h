﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController/<WaitAndGetFlashLight>c__Iterator13
struct U3CWaitAndGetFlashLightU3Ec__Iterator13_t393;
// System.Object
struct Object_t;

// System.Void PlayerController/<WaitAndGetFlashLight>c__Iterator13::.ctor()
extern "C" void U3CWaitAndGetFlashLightU3Ec__Iterator13__ctor_m2248 (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndGetFlashLight>c__Iterator13::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndGetFlashLightU3Ec__Iterator13_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2249 (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndGetFlashLight>c__Iterator13::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndGetFlashLightU3Ec__Iterator13_System_Collections_IEnumerator_get_Current_m2250 (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController/<WaitAndGetFlashLight>c__Iterator13::MoveNext()
extern "C" bool U3CWaitAndGetFlashLightU3Ec__Iterator13_MoveNext_m2251 (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndGetFlashLight>c__Iterator13::Dispose()
extern "C" void U3CWaitAndGetFlashLightU3Ec__Iterator13_Dispose_m2252 (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndGetFlashLight>c__Iterator13::Reset()
extern "C" void U3CWaitAndGetFlashLightU3Ec__Iterator13_Reset_m2253 (U3CWaitAndGetFlashLightU3Ec__Iterator13_t393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
