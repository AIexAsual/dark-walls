﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GameManager/<MyGameObjectHandler>c__IteratorE
struct U3CMyGameObjectHandlerU3Ec__IteratorE_t353;
// System.Object
struct Object_t;

// System.Void GameManager/<MyGameObjectHandler>c__IteratorE::.ctor()
extern "C" void U3CMyGameObjectHandlerU3Ec__IteratorE__ctor_m2053 (U3CMyGameObjectHandlerU3Ec__IteratorE_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<MyGameObjectHandler>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CMyGameObjectHandlerU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2054 (U3CMyGameObjectHandlerU3Ec__IteratorE_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<MyGameObjectHandler>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CMyGameObjectHandlerU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m2055 (U3CMyGameObjectHandlerU3Ec__IteratorE_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<MyGameObjectHandler>c__IteratorE::MoveNext()
extern "C" bool U3CMyGameObjectHandlerU3Ec__IteratorE_MoveNext_m2056 (U3CMyGameObjectHandlerU3Ec__IteratorE_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<MyGameObjectHandler>c__IteratorE::Dispose()
extern "C" void U3CMyGameObjectHandlerU3Ec__IteratorE_Dispose_m2057 (U3CMyGameObjectHandlerU3Ec__IteratorE_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<MyGameObjectHandler>c__IteratorE::Reset()
extern "C" void U3CMyGameObjectHandlerU3Ec__IteratorE_Reset_m2058 (U3CMyGameObjectHandlerU3Ec__IteratorE_t353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
