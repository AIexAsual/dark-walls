﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Dissipation
struct  CameraFilterPack_Distortion_Dissipation_t82  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Dissipation::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Dissipation::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Dissipation::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Dissipation::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Dissipation::Dissipation
	float ___Dissipation_6;
	// System.Single CameraFilterPack_Distortion_Dissipation::Colors
	float ___Colors_7;
	// System.Single CameraFilterPack_Distortion_Dissipation::Green_Mod
	float ___Green_Mod_8;
	// System.Single CameraFilterPack_Distortion_Dissipation::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Distortion_Dissipation_t82_StaticFields{
	// System.Single CameraFilterPack_Distortion_Dissipation::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Distortion_Dissipation::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Distortion_Dissipation::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Distortion_Dissipation::ChangeValue4
	float ___ChangeValue4_13;
};
