﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// StereoRenderEffect
struct StereoRenderEffect_t239;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void StereoRenderEffect::.ctor()
extern "C" void StereoRenderEffect__ctor_m1626 (StereoRenderEffect_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoRenderEffect::Awake()
extern "C" void StereoRenderEffect_Awake_m1627 (StereoRenderEffect_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoRenderEffect::Start()
extern "C" void StereoRenderEffect_Start_m1628 (StereoRenderEffect_t239 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StereoRenderEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void StereoRenderEffect_OnRenderImage_m1629 (StereoRenderEffect_t239 * __this, RenderTexture_t15 * ___source, RenderTexture_t15 * ___dest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
