﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Char>
struct DefaultComparer_t2299;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Char>::.ctor()
extern "C" void DefaultComparer__ctor_m14479_gshared (DefaultComparer_t2299 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m14479(__this, method) (( void (*) (DefaultComparer_t2299 *, const MethodInfo*))DefaultComparer__ctor_m14479_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Char>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m14480_gshared (DefaultComparer_t2299 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m14480(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2299 *, uint16_t, uint16_t, const MethodInfo*))DefaultComparer_Compare_m14480_gshared)(__this, ___x, ___y, method)
