﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_BrightContrastSaturation
struct CameraFilterPack_Color_BrightContrastSaturation_t60;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_BrightContrastSaturation::.ctor()
extern "C" void CameraFilterPack_Color_BrightContrastSaturation__ctor_m373 (CameraFilterPack_Color_BrightContrastSaturation_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_BrightContrastSaturation::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_BrightContrastSaturation_get_material_m374 (CameraFilterPack_Color_BrightContrastSaturation_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::Start()
extern "C" void CameraFilterPack_Color_BrightContrastSaturation_Start_m375 (CameraFilterPack_Color_BrightContrastSaturation_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_BrightContrastSaturation_OnRenderImage_m376 (CameraFilterPack_Color_BrightContrastSaturation_t60 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::OnValidate()
extern "C" void CameraFilterPack_Color_BrightContrastSaturation_OnValidate_m377 (CameraFilterPack_Color_BrightContrastSaturation_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::Update()
extern "C" void CameraFilterPack_Color_BrightContrastSaturation_Update_m378 (CameraFilterPack_Color_BrightContrastSaturation_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_BrightContrastSaturation::OnDisable()
extern "C" void CameraFilterPack_Color_BrightContrastSaturation_OnDisable_m379 (CameraFilterPack_Color_BrightContrastSaturation_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
