﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Film_ColorPerfection
struct CameraFilterPack_Film_ColorPerfection_t143;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Film_ColorPerfection::.ctor()
extern "C" void CameraFilterPack_Film_ColorPerfection__ctor_m927 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Film_ColorPerfection::get_material()
extern "C" Material_t2 * CameraFilterPack_Film_ColorPerfection_get_material_m928 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::Start()
extern "C" void CameraFilterPack_Film_ColorPerfection_Start_m929 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Film_ColorPerfection_OnRenderImage_m930 (CameraFilterPack_Film_ColorPerfection_t143 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::OnValidate()
extern "C" void CameraFilterPack_Film_ColorPerfection_OnValidate_m931 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::Update()
extern "C" void CameraFilterPack_Film_ColorPerfection_Update_m932 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Film_ColorPerfection::OnDisable()
extern "C" void CameraFilterPack_Film_ColorPerfection_OnDisable_m933 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
