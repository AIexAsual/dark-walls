﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t338;
// UnityEngine.AudioSource
struct AudioSource_t339;
// InteractiveObject
#include "AssemblyU2DCSharp_InteractiveObject.h"
// InteractiveRadio
struct  InteractiveRadio_t340  : public InteractiveObject_t328
{
	// UnityEngine.AudioClip[] InteractiveRadio::radioClips
	AudioClipU5BU5D_t338* ___radioClips_4;
	// UnityEngine.AudioSource InteractiveRadio::radioSound
	AudioSource_t339 * ___radioSound_5;
};
