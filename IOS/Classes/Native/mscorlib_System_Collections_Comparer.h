﻿#pragma once
#include <stdint.h>
// System.Collections.Comparer
struct Comparer_t1656;
// System.Globalization.CompareInfo
struct CompareInfo_t1358;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Comparer
struct  Comparer_t1656  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t1358 * ___m_compareInfo_2;
};
struct Comparer_t1656_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t1656 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t1656 * ___DefaultInvariant_1;
};
