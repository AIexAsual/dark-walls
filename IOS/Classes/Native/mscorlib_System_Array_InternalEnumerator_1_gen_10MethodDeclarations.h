﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<CardboardEye>
struct InternalEnumerator_1_t2279;
// System.Object
struct Object_t;
// CardboardEye
struct CardboardEye_t240;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<CardboardEye>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14260(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2279 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<CardboardEye>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14261(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2279 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<CardboardEye>::Dispose()
#define InternalEnumerator_1_Dispose_m14262(__this, method) (( void (*) (InternalEnumerator_1_t2279 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<CardboardEye>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14263(__this, method) (( bool (*) (InternalEnumerator_1_t2279 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<CardboardEye>::get_Current()
#define InternalEnumerator_1_get_Current_m14264(__this, method) (( CardboardEye_t240 * (*) (InternalEnumerator_1_t2279 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
