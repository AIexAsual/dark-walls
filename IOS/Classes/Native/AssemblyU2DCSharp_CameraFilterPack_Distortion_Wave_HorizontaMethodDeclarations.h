﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Wave_Horizontal
struct CameraFilterPack_Distortion_Wave_Horizontal_t94;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Wave_Horizontal::.ctor()
extern "C" void CameraFilterPack_Distortion_Wave_Horizontal__ctor_m595 (CameraFilterPack_Distortion_Wave_Horizontal_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Wave_Horizontal::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Wave_Horizontal_get_material_m596 (CameraFilterPack_Distortion_Wave_Horizontal_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::Start()
extern "C" void CameraFilterPack_Distortion_Wave_Horizontal_Start_m597 (CameraFilterPack_Distortion_Wave_Horizontal_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Wave_Horizontal_OnRenderImage_m598 (CameraFilterPack_Distortion_Wave_Horizontal_t94 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::OnValidate()
extern "C" void CameraFilterPack_Distortion_Wave_Horizontal_OnValidate_m599 (CameraFilterPack_Distortion_Wave_Horizontal_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::Update()
extern "C" void CameraFilterPack_Distortion_Wave_Horizontal_Update_m600 (CameraFilterPack_Distortion_Wave_Horizontal_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Wave_Horizontal::OnDisable()
extern "C" void CameraFilterPack_Distortion_Wave_Horizontal_OnDisable_m601 (CameraFilterPack_Distortion_Wave_Horizontal_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
