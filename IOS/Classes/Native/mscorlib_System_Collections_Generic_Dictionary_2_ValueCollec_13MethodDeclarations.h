﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>
struct ValueCollection_t2486;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,iTweenPath>
struct Dictionary_2_t458;
// iTweenPath
struct iTweenPath_t459;
// System.Collections.Generic.IEnumerator`1<iTweenPath>
struct IEnumerator_1_t3062;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// iTweenPath[]
struct iTweenPathU5BU5D_t455;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,iTweenPath>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_44.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_8MethodDeclarations.h"
#define ValueCollection__ctor_m17083(__this, ___dictionary, method) (( void (*) (ValueCollection_t2486 *, Dictionary_2_t458 *, const MethodInfo*))ValueCollection__ctor_m14779_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m17084(__this, ___item, method) (( void (*) (ValueCollection_t2486 *, iTweenPath_t459 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m14781_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m17085(__this, method) (( void (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m14783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m17086(__this, ___item, method) (( bool (*) (ValueCollection_t2486 *, iTweenPath_t459 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m14785_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m17087(__this, ___item, method) (( bool (*) (ValueCollection_t2486 *, iTweenPath_t459 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m14787_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m17088(__this, method) (( Object_t* (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m14789_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m17089(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2486 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m14791_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m17090(__this, method) (( Object_t * (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m14793_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m17091(__this, method) (( bool (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m14795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m17092(__this, method) (( bool (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m14797_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m17093(__this, method) (( Object_t * (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m14799_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m17094(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2486 *, iTweenPathU5BU5D_t455*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m14801_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::GetEnumerator()
#define ValueCollection_GetEnumerator_m17095(__this, method) (( Enumerator_t3063  (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_GetEnumerator_m14803_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,iTweenPath>::get_Count()
#define ValueCollection_get_Count_m17096(__this, method) (( int32_t (*) (ValueCollection_t2486 *, const MethodInfo*))ValueCollection_get_Count_m14805_gshared)(__this, method)
