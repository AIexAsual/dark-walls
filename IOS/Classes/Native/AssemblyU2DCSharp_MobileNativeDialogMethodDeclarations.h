﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MobileNativeDialog
struct MobileNativeDialog_t279;
// System.String
struct String_t;
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"

// System.Void MobileNativeDialog::.ctor(System.String,System.String)
extern "C" void MobileNativeDialog__ctor_m1735 (MobileNativeDialog_t279 * __this, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::.ctor(System.String,System.String,System.String,System.String)
extern "C" void MobileNativeDialog__ctor_m1736 (MobileNativeDialog_t279 * __this, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::init(System.String,System.String,System.String,System.String)
extern "C" void MobileNativeDialog_init_m1737 (MobileNativeDialog_t279 * __this, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::OnCompleteListener(MNDialogResult)
extern "C" void MobileNativeDialog_OnCompleteListener_m1738 (MobileNativeDialog_t279 * __this, int32_t ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeDialog::<OnComplete>m__4(MNDialogResult)
extern "C" void MobileNativeDialog_U3COnCompleteU3Em__4_m1739 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
