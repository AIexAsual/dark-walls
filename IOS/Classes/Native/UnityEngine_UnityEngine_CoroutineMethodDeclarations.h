﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t547;
struct Coroutine_t547_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m4984 (Coroutine_t547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m4985 (Coroutine_t547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m4986 (Coroutine_t547 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t547_marshal(const Coroutine_t547& unmarshaled, Coroutine_t547_marshaled& marshaled);
void Coroutine_t547_marshal_back(const Coroutine_t547_marshaled& marshaled, Coroutine_t547& unmarshaled);
void Coroutine_t547_marshal_cleanup(Coroutine_t547_marshaled& marshaled);
