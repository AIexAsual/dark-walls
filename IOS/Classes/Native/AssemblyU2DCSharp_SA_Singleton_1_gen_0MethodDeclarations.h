﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SA_Singleton`1<System.Object>
struct SA_Singleton_1_t2301;
// System.Object
struct Object_t;

// System.Void SA_Singleton`1<System.Object>::.ctor()
extern "C" void SA_Singleton_1__ctor_m14485_gshared (SA_Singleton_1_t2301 * __this, const MethodInfo* method);
#define SA_Singleton_1__ctor_m14485(__this, method) (( void (*) (SA_Singleton_1_t2301 *, const MethodInfo*))SA_Singleton_1__ctor_m14485_gshared)(__this, method)
// System.Void SA_Singleton`1<System.Object>::.cctor()
extern "C" void SA_Singleton_1__cctor_m14487_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_1__cctor_m14487(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1__cctor_m14487_gshared)(__this /* static, unused */, method)
// T SA_Singleton`1<System.Object>::get_instance()
extern "C" Object_t * SA_Singleton_1_get_instance_m14489_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_1_get_instance_m14489(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_instance_m14489_gshared)(__this /* static, unused */, method)
// T SA_Singleton`1<System.Object>::get_Instance()
extern "C" Object_t * SA_Singleton_1_get_Instance_m14491_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_1_get_Instance_m14491(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_Instance_m14491_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton`1<System.Object>::get_HasInstance()
extern "C" bool SA_Singleton_1_get_HasInstance_m14493_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_1_get_HasInstance_m14493(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_HasInstance_m14493_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton`1<System.Object>::get_IsDestroyed()
extern "C" bool SA_Singleton_1_get_IsDestroyed_m14495_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define SA_Singleton_1_get_IsDestroyed_m14495(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_IsDestroyed_m14495_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton`1<System.Object>::OnDestroy()
extern "C" void SA_Singleton_1_OnDestroy_m14497_gshared (SA_Singleton_1_t2301 * __this, const MethodInfo* method);
#define SA_Singleton_1_OnDestroy_m14497(__this, method) (( void (*) (SA_Singleton_1_t2301 *, const MethodInfo*))SA_Singleton_1_OnDestroy_m14497_gshared)(__this, method)
// System.Void SA_Singleton`1<System.Object>::OnApplicationQuit()
extern "C" void SA_Singleton_1_OnApplicationQuit_m14499_gshared (SA_Singleton_1_t2301 * __this, const MethodInfo* method);
#define SA_Singleton_1_OnApplicationQuit_m14499(__this, method) (( void (*) (SA_Singleton_1_t2301 *, const MethodInfo*))SA_Singleton_1_OnApplicationQuit_m14499_gshared)(__this, method)
