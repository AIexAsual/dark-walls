﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SkyboxMesh
struct SkyboxMesh_t226;

// System.Void SkyboxMesh::.ctor()
extern "C" void SkyboxMesh__ctor_m1414 (SkyboxMesh_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkyboxMesh::Awake()
extern "C" void SkyboxMesh_Awake_m1415 (SkyboxMesh_t226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
