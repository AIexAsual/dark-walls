﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OptionalAttribute
struct OptionalAttribute_t1561;

// System.Void System.Runtime.InteropServices.OptionalAttribute::.ctor()
extern "C" void OptionalAttribute__ctor_m9354 (OptionalAttribute_t1561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
