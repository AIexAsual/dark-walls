﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RadialUndistortionEffect
struct RadialUndistortionEffect_t263;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void RadialUndistortionEffect::.ctor()
extern "C" void RadialUndistortionEffect__ctor_m1599 (RadialUndistortionEffect_t263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialUndistortionEffect::Awake()
extern "C" void RadialUndistortionEffect_Awake_m1600 (RadialUndistortionEffect_t263 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadialUndistortionEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void RadialUndistortionEffect_OnRenderImage_m1601 (RadialUndistortionEffect_t263 * __this, RenderTexture_t15 * ___source, RenderTexture_t15 * ___dest, const MethodInfo* method) IL2CPP_METHOD_ATTR;
