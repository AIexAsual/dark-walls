﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t339;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SFX
struct  SFX_t385  : public MonoBehaviour_t4
{
	// UnityEngine.AudioSource SFX::audioSource
	AudioSource_t339 * ___audioSource_2;
};
