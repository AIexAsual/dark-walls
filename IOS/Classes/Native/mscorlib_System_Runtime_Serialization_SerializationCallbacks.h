﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Runtime.Serialization.SerializationCallbacks/CallbackHandler
struct  CallbackHandler_t1957  : public MulticastDelegate_t219
{
};
