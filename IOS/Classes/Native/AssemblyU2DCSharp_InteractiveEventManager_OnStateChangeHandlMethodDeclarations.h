﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t361;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"

// System.Void InteractiveEventManager/OnStateChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void OnStateChangeHandler__ctor_m2094 (OnStateChangeHandler_t361 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager/OnStateChangeHandler::Invoke(InteractiveEvent)
extern "C" void OnStateChangeHandler_Invoke_m2095 (OnStateChangeHandler_t361 * __this, int32_t ___ev, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnStateChangeHandler_t361(Il2CppObject* delegate, int32_t ___ev);
// System.IAsyncResult InteractiveEventManager/OnStateChangeHandler::BeginInvoke(InteractiveEvent,System.AsyncCallback,System.Object)
extern "C" Object_t * OnStateChangeHandler_BeginInvoke_m2096 (OnStateChangeHandler_t361 * __this, int32_t ___ev, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveEventManager/OnStateChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void OnStateChangeHandler_EndInvoke_m2097 (OnStateChangeHandler_t361 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
