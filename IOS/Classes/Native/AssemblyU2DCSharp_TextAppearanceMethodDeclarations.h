﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TextAppearance
struct TextAppearance_t368;
// System.String
struct String_t;

// System.Void TextAppearance::.ctor()
extern "C" void TextAppearance__ctor_m2352 (TextAppearance_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextAppearance::Awake()
extern "C" void TextAppearance_Awake_m2353 (TextAppearance_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextAppearance::Update()
extern "C" void TextAppearance_Update_m2354 (TextAppearance_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextAppearance::Fade(System.Single,System.Single,System.String)
extern "C" void TextAppearance_Fade_m2355 (TextAppearance_t368 * __this, float ___val, float ___delay, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextAppearance::Appeared()
extern "C" void TextAppearance_Appeared_m2356 (TextAppearance_t368 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextAppearance::EnableCollision(System.Boolean)
extern "C" void TextAppearance_EnableCollision_m2357 (TextAppearance_t368 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
