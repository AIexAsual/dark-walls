﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GazeInputManager
struct GazeInputManager_t359;
// GazeInputManager/OnStateChangeHandler
struct OnStateChangeHandler_t358;
// System.String
struct String_t;
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEvent.h"

// System.Void GazeInputManager::.ctor()
extern "C" void GazeInputManager__ctor_m2086 (GazeInputManager_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager::.cctor()
extern "C" void GazeInputManager__cctor_m2087 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager::add_OnStateChangeEvent(GazeInputManager/OnStateChangeHandler)
extern "C" void GazeInputManager_add_OnStateChangeEvent_m2088 (GazeInputManager_t359 * __this, OnStateChangeHandler_t358 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager::remove_OnStateChangeEvent(GazeInputManager/OnStateChangeHandler)
extern "C" void GazeInputManager_remove_OnStateChangeEvent_m2089 (GazeInputManager_t359 * __this, OnStateChangeHandler_t358 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GazeInputManager GazeInputManager::get_Instance()
extern "C" GazeInputManager_t359 * GazeInputManager_get_Instance_m2090 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager::dispatchEvent(GazeInputEvent,System.String)
extern "C" void GazeInputManager_dispatchEvent_m2091 (GazeInputManager_t359 * __this, int32_t ___gameStateEvent, String_t* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager::Start()
extern "C" void GazeInputManager_Start_m2092 (GazeInputManager_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GazeInputManager::Update()
extern "C" void GazeInputManager_Update_m2093 (GazeInputManager_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
