﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
struct Action_1_t843;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t1031;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_7MethodDeclarations.h"
#define Action_1__ctor_m19313(__this, ___object, ___method, method) (( void (*) (Action_1_t843 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m14500_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::Invoke(T)
#define Action_1_Invoke_m6355(__this, ___obj, method) (( void (*) (Action_1_t843 *, IUserProfileU5BU5D_t1031*, const MethodInfo*))Action_1_Invoke_m14501_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m19314(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t843 *, IUserProfileU5BU5D_t1031*, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m14503_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m19315(__this, ___result, method) (( void (*) (Action_1_t843 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m14505_gshared)(__this, ___result, method)
