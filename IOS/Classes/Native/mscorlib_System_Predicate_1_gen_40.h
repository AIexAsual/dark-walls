﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t972;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct  Predicate_1_t2728  : public MulticastDelegate_t219
{
};
