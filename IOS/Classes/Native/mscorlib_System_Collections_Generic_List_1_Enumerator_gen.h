﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<RagdollHelper/BodyPart>
struct List_1_t401;
// RagdollHelper/BodyPart
struct BodyPart_t400;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>
struct  Enumerator_t537 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::l
	List_1_t401 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::current
	BodyPart_t400 * ___current_3;
};
