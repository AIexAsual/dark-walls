﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Colors_Threshold
struct  CameraFilterPack_Colors_Threshold_t79  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Colors_Threshold::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_Threshold::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Colors_Threshold::Threshold
	float ___Threshold_4;
	// UnityEngine.Material CameraFilterPack_Colors_Threshold::SCMaterial
	Material_t2 * ___SCMaterial_5;
};
struct CameraFilterPack_Colors_Threshold_t79_StaticFields{
	// System.Single CameraFilterPack_Colors_Threshold::ChangeThreshold
	float ___ChangeThreshold_6;
};
