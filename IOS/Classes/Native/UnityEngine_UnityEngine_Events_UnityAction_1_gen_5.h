﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t775;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct  UnityAction_1_t754  : public MulticastDelegate_t219
{
};
