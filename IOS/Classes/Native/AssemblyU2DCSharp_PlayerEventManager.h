﻿#pragma once
#include <stdint.h>
// PlayerEventManager
struct PlayerEventManager_t378;
// PlayerEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t377;
// System.Object
#include "mscorlib_System_Object.h"
// PlayerEvent
#include "AssemblyU2DCSharp_PlayerEvent.h"
// PlayerEventManager
struct  PlayerEventManager_t378  : public Object_t
{
	// PlayerEvent PlayerEventManager::playerState
	int32_t ___playerState_1;
	// PlayerEventManager/OnStateChangeHandler PlayerEventManager::OnStateChangeEvent
	OnStateChangeHandler_t377 * ___OnStateChangeEvent_2;
};
struct PlayerEventManager_t378_StaticFields{
	// PlayerEventManager PlayerEventManager::_instance
	PlayerEventManager_t378 * ____instance_0;
};
