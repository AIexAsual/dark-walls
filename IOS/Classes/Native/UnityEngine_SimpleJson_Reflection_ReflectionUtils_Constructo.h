﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t470;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct  ConstructorDelegate_t995  : public MulticastDelegate_t219
{
};
