﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.TimeZone
#include "mscorlib_System_TimeZone.h"
// Metadata Definition System.TimeZone
extern TypeInfo TimeZone_t2138_il2cpp_TypeInfo;
// System.TimeZone
#include "mscorlib_System_TimeZoneMethodDeclarations.h"
static const EncodedMethodIndex TimeZone_t2138_VTable[9] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	4371,
	4372,
	4373,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeZone_t2138_0_0_0;
extern const Il2CppType TimeZone_t2138_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct TimeZone_t2138;
const Il2CppTypeDefinitionMetadata TimeZone_t2138_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimeZone_t2138_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9322/* fieldStart */
	, 13905/* methodStart */
	, -1/* eventStart */
	, 2634/* propertyStart */

};
TypeInfo TimeZone_t2138_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeZone"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TimeZone_t2138_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3395/* custom_attributes_cache */
	, &TimeZone_t2138_0_0_0/* byval_arg */
	, &TimeZone_t2138_1_0_0/* this_arg */
	, &TimeZone_t2138_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeZone_t2138)/* instance_size */
	, sizeof (TimeZone_t2138)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TimeZone_t2138_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZone.h"
// Metadata Definition System.CurrentSystemTimeZone
extern TypeInfo CurrentSystemTimeZone_t2139_il2cpp_TypeInfo;
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZoneMethodDeclarations.h"
static const EncodedMethodIndex CurrentSystemTimeZone_t2139_VTable[10] = 
{
	626,
	601,
	627,
	628,
	4374,
	4375,
	4371,
	4372,
	4373,
	4376,
};
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
static const Il2CppType* CurrentSystemTimeZone_t2139_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair CurrentSystemTimeZone_t2139_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t2213_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CurrentSystemTimeZone_t2139_0_0_0;
extern const Il2CppType CurrentSystemTimeZone_t2139_1_0_0;
struct CurrentSystemTimeZone_t2139;
const Il2CppTypeDefinitionMetadata CurrentSystemTimeZone_t2139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CurrentSystemTimeZone_t2139_InterfacesTypeInfos/* implementedInterfaces */
	, CurrentSystemTimeZone_t2139_InterfacesOffsets/* interfaceOffsets */
	, &TimeZone_t2138_0_0_0/* parent */
	, CurrentSystemTimeZone_t2139_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9323/* fieldStart */
	, 13916/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CurrentSystemTimeZone_t2139_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CurrentSystemTimeZone"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CurrentSystemTimeZone_t2139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CurrentSystemTimeZone_t2139_0_0_0/* byval_arg */
	, &CurrentSystemTimeZone_t2139_1_0_0/* this_arg */
	, &CurrentSystemTimeZone_t2139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CurrentSystemTimeZone_t2139)/* instance_size */
	, sizeof (CurrentSystemTimeZone_t2139)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CurrentSystemTimeZone_t2139_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
// Metadata Definition System.TypeCode
extern TypeInfo TypeCode_t2140_il2cpp_TypeInfo;
// System.TypeCode
#include "mscorlib_System_TypeCodeMethodDeclarations.h"
static const EncodedMethodIndex TypeCode_t2140_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair TypeCode_t2140_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeCode_t2140_0_0_0;
extern const Il2CppType TypeCode_t2140_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata TypeCode_t2140_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeCode_t2140_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TypeCode_t2140_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9331/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeCode_t2140_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeCode"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3396/* custom_attributes_cache */
	, &TypeCode_t2140_0_0_0/* byval_arg */
	, &TypeCode_t2140_1_0_0/* this_arg */
	, &TypeCode_t2140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeCode_t2140)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeCode_t2140)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 19/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationException.h"
// Metadata Definition System.TypeInitializationException
extern TypeInfo TypeInitializationException_t2141_il2cpp_TypeInfo;
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationExceptionMethodDeclarations.h"
static const EncodedMethodIndex TypeInitializationException_t2141_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4377,
	1318,
	1319,
	1320,
	1321,
	4377,
	1322,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair TypeInitializationException_t2141_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInitializationException_t2141_0_0_0;
extern const Il2CppType TypeInitializationException_t2141_1_0_0;
extern const Il2CppType SystemException_t1536_0_0_0;
struct TypeInitializationException_t2141;
const Il2CppTypeDefinitionMetadata TypeInitializationException_t2141_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInitializationException_t2141_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, TypeInitializationException_t2141_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9350/* fieldStart */
	, 13924/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeInitializationException_t2141_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInitializationException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TypeInitializationException_t2141_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3397/* custom_attributes_cache */
	, &TypeInitializationException_t2141_0_0_0/* byval_arg */
	, &TypeInitializationException_t2141_1_0_0/* this_arg */
	, &TypeInitializationException_t2141_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInitializationException_t2141)/* instance_size */
	, sizeof (TypeInitializationException_t2141)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.TypeLoadException
#include "mscorlib_System_TypeLoadException.h"
// Metadata Definition System.TypeLoadException
extern TypeInfo TypeLoadException_t2096_il2cpp_TypeInfo;
// System.TypeLoadException
#include "mscorlib_System_TypeLoadExceptionMethodDeclarations.h"
static const EncodedMethodIndex TypeLoadException_t2096_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4283,
	1318,
	4284,
	1320,
	1321,
	4283,
	1322,
};
static Il2CppInterfaceOffsetPair TypeLoadException_t2096_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLoadException_t2096_0_0_0;
extern const Il2CppType TypeLoadException_t2096_1_0_0;
struct TypeLoadException_t2096;
const Il2CppTypeDefinitionMetadata TypeLoadException_t2096_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLoadException_t2096_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, TypeLoadException_t2096_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9351/* fieldStart */
	, 13926/* methodStart */
	, -1/* eventStart */
	, 2635/* propertyStart */

};
TypeInfo TypeLoadException_t2096_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLoadException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TypeLoadException_t2096_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3398/* custom_attributes_cache */
	, &TypeLoadException_t2096_0_0_0/* byval_arg */
	, &TypeLoadException_t2096_1_0_0/* this_arg */
	, &TypeLoadException_t2096_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLoadException_t2096)/* instance_size */
	, sizeof (TypeLoadException_t2096)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessException.h"
// Metadata Definition System.UnauthorizedAccessException
extern TypeInfo UnauthorizedAccessException_t2142_il2cpp_TypeInfo;
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessExceptionMethodDeclarations.h"
static const EncodedMethodIndex UnauthorizedAccessException_t2142_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair UnauthorizedAccessException_t2142_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnauthorizedAccessException_t2142_0_0_0;
extern const Il2CppType UnauthorizedAccessException_t2142_1_0_0;
struct UnauthorizedAccessException_t2142;
const Il2CppTypeDefinitionMetadata UnauthorizedAccessException_t2142_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnauthorizedAccessException_t2142_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, UnauthorizedAccessException_t2142_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13931/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnauthorizedAccessException_t2142_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnauthorizedAccessException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UnauthorizedAccessException_t2142_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3399/* custom_attributes_cache */
	, &UnauthorizedAccessException_t2142_0_0_0/* byval_arg */
	, &UnauthorizedAccessException_t2142_1_0_0/* this_arg */
	, &UnauthorizedAccessException_t2142_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnauthorizedAccessException_t2142)/* instance_size */
	, sizeof (UnauthorizedAccessException_t2142)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
// Metadata Definition System.UnhandledExceptionEventArgs
extern TypeInfo UnhandledExceptionEventArgs_t2143_il2cpp_TypeInfo;
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
static const EncodedMethodIndex UnhandledExceptionEventArgs_t2143_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventArgs_t2143_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2143_1_0_0;
extern const Il2CppType EventArgs_t1223_0_0_0;
struct UnhandledExceptionEventArgs_t2143;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventArgs_t2143_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1223_0_0_0/* parent */
	, UnhandledExceptionEventArgs_t2143_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9354/* fieldStart */
	, 13934/* methodStart */
	, -1/* eventStart */
	, 2636/* propertyStart */

};
TypeInfo UnhandledExceptionEventArgs_t2143_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventArgs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UnhandledExceptionEventArgs_t2143_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3400/* custom_attributes_cache */
	, &UnhandledExceptionEventArgs_t2143_0_0_0/* byval_arg */
	, &UnhandledExceptionEventArgs_t2143_1_0_0/* this_arg */
	, &UnhandledExceptionEventArgs_t2143_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventArgs_t2143)/* instance_size */
	, sizeof (UnhandledExceptionEventArgs_t2143)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolder.h"
// Metadata Definition System.UnitySerializationHolder
extern TypeInfo UnitySerializationHolder_t2145_il2cpp_TypeInfo;
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolderMethodDeclarations.h"
extern const Il2CppType UnityType_t2144_0_0_0;
static const Il2CppType* UnitySerializationHolder_t2145_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UnityType_t2144_0_0_0,
};
static const EncodedMethodIndex UnitySerializationHolder_t2145_VTable[8] = 
{
	626,
	601,
	627,
	628,
	4378,
	4379,
	4378,
	4379,
};
extern const Il2CppType IObjectReference_t2211_0_0_0;
static const Il2CppType* UnitySerializationHolder_t2145_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IObjectReference_t2211_0_0_0,
};
static Il2CppInterfaceOffsetPair UnitySerializationHolder_t2145_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IObjectReference_t2211_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnitySerializationHolder_t2145_0_0_0;
extern const Il2CppType UnitySerializationHolder_t2145_1_0_0;
struct UnitySerializationHolder_t2145;
const Il2CppTypeDefinitionMetadata UnitySerializationHolder_t2145_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UnitySerializationHolder_t2145_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, UnitySerializationHolder_t2145_InterfacesTypeInfos/* implementedInterfaces */
	, UnitySerializationHolder_t2145_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnitySerializationHolder_t2145_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9356/* fieldStart */
	, 13937/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnitySerializationHolder_t2145_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnitySerializationHolder"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UnitySerializationHolder_t2145_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnitySerializationHolder_t2145_0_0_0/* byval_arg */
	, &UnitySerializationHolder_t2145_1_0_0/* this_arg */
	, &UnitySerializationHolder_t2145_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnitySerializationHolder_t2145)/* instance_size */
	, sizeof (UnitySerializationHolder_t2145)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
// Metadata Definition System.UnitySerializationHolder/UnityType
extern TypeInfo UnityType_t2144_il2cpp_TypeInfo;
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityTypeMethodDeclarations.h"
static const EncodedMethodIndex UnityType_t2144_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair UnityType_t2144_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnityType_t2144_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t1117_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UnityType_t2144_DefinitionMetadata = 
{
	&UnitySerializationHolder_t2145_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityType_t2144_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, UnityType_t2144_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9359/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnityType_t2144_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityType"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityType_t2144_0_0_0/* byval_arg */
	, &UnityType_t2144_1_0_0/* this_arg */
	, &UnityType_t2144_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityType_t2144)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityType_t2144)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Version
#include "mscorlib_System_Version.h"
// Metadata Definition System.Version
extern TypeInfo Version_t522_il2cpp_TypeInfo;
// System.Version
#include "mscorlib_System_VersionMethodDeclarations.h"
static const EncodedMethodIndex Version_t522_VTable[7] = 
{
	4380,
	601,
	4381,
	4382,
	4383,
	4384,
	4385,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType IComparable_1_t3902_0_0_0;
extern const Il2CppType IEquatable_1_t3903_0_0_0;
static const Il2CppType* Version_t522_InterfacesTypeInfos[] = 
{
	&IComparable_t2192_0_0_0,
	&ICloneable_t3433_0_0_0,
	&IComparable_1_t3902_0_0_0,
	&IEquatable_1_t3903_0_0_0,
};
static Il2CppInterfaceOffsetPair Version_t522_InterfacesOffsets[] = 
{
	{ &IComparable_t2192_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &IComparable_1_t3902_0_0_0, 5},
	{ &IEquatable_1_t3903_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Version_t522_0_0_0;
extern const Il2CppType Version_t522_1_0_0;
struct Version_t522;
const Il2CppTypeDefinitionMetadata Version_t522_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Version_t522_InterfacesTypeInfos/* implementedInterfaces */
	, Version_t522_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Version_t522_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9364/* fieldStart */
	, 13943/* methodStart */
	, -1/* eventStart */
	, 2638/* propertyStart */

};
TypeInfo Version_t522_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Version"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Version_t522_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3403/* custom_attributes_cache */
	, &Version_t522_0_0_0/* byval_arg */
	, &Version_t522_1_0_0/* this_arg */
	, &Version_t522_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Version_t522)/* instance_size */
	, sizeof (Version_t522)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.WeakReference
#include "mscorlib_System_WeakReference.h"
// Metadata Definition System.WeakReference
extern TypeInfo WeakReference_t1905_il2cpp_TypeInfo;
// System.WeakReference
#include "mscorlib_System_WeakReferenceMethodDeclarations.h"
static const EncodedMethodIndex WeakReference_t1905_VTable[8] = 
{
	626,
	4386,
	627,
	628,
	4387,
	4388,
	4389,
	4387,
};
static const Il2CppType* WeakReference_t1905_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair WeakReference_t1905_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WeakReference_t1905_0_0_0;
extern const Il2CppType WeakReference_t1905_1_0_0;
struct WeakReference_t1905;
const Il2CppTypeDefinitionMetadata WeakReference_t1905_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WeakReference_t1905_InterfacesTypeInfos/* implementedInterfaces */
	, WeakReference_t1905_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WeakReference_t1905_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9369/* fieldStart */
	, 13962/* methodStart */
	, -1/* eventStart */
	, 2642/* propertyStart */

};
TypeInfo WeakReference_t1905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WeakReference"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &WeakReference_t1905_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3404/* custom_attributes_cache */
	, &WeakReference_t1905_0_0_0/* byval_arg */
	, &WeakReference_t1905_1_0_0/* this_arg */
	, &WeakReference_t1905_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WeakReference_t1905)/* instance_size */
	, sizeof (WeakReference_t1905)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t2146_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
static const EncodedMethodIndex PrimalityTest_t2146_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4390,
	4391,
	4392,
};
static Il2CppInterfaceOffsetPair PrimalityTest_t2146_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimalityTest_t2146_0_0_0;
extern const Il2CppType PrimalityTest_t2146_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct PrimalityTest_t2146;
const Il2CppTypeDefinitionMetadata PrimalityTest_t2146_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t2146_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, PrimalityTest_t2146_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13971/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrimalityTest_t2146_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &PrimalityTest_t2146_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t2146_0_0_0/* byval_arg */
	, &PrimalityTest_t2146_1_0_0/* this_arg */
	, &PrimalityTest_t2146_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t2146/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t2146)/* instance_size */
	, sizeof (PrimalityTest_t2146)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilter.h"
// Metadata Definition System.Reflection.MemberFilter
extern TypeInfo MemberFilter_t1549_il2cpp_TypeInfo;
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilterMethodDeclarations.h"
static const EncodedMethodIndex MemberFilter_t1549_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4393,
	4394,
	4395,
};
static Il2CppInterfaceOffsetPair MemberFilter_t1549_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberFilter_t1549_0_0_0;
extern const Il2CppType MemberFilter_t1549_1_0_0;
struct MemberFilter_t1549;
const Il2CppTypeDefinitionMetadata MemberFilter_t1549_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberFilter_t1549_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, MemberFilter_t1549_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13975/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MemberFilter_t1549_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberFilter"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MemberFilter_t1549_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3405/* custom_attributes_cache */
	, &MemberFilter_t1549_0_0_0/* byval_arg */
	, &MemberFilter_t1549_1_0_0/* this_arg */
	, &MemberFilter_t1549_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MemberFilter_t1549/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberFilter_t1549)/* instance_size */
	, sizeof (MemberFilter_t1549)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilter.h"
// Metadata Definition System.Reflection.TypeFilter
extern TypeInfo TypeFilter_t1795_il2cpp_TypeInfo;
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilterMethodDeclarations.h"
static const EncodedMethodIndex TypeFilter_t1795_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4396,
	4397,
	4398,
};
static Il2CppInterfaceOffsetPair TypeFilter_t1795_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilter_t1795_0_0_0;
extern const Il2CppType TypeFilter_t1795_1_0_0;
struct TypeFilter_t1795;
const Il2CppTypeDefinitionMetadata TypeFilter_t1795_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilter_t1795_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, TypeFilter_t1795_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13979/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeFilter_t1795_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilter"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &TypeFilter_t1795_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3406/* custom_attributes_cache */
	, &TypeFilter_t1795_0_0_0/* byval_arg */
	, &TypeFilter_t1795_1_0_0/* this_arg */
	, &TypeFilter_t1795_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TypeFilter_t1795/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilter_t1795)/* instance_size */
	, sizeof (TypeFilter_t1795)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
// Metadata Definition System.Runtime.Remoting.Messaging.HeaderHandler
extern TypeInfo HeaderHandler_t2148_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandlerMethodDeclarations.h"
static const EncodedMethodIndex HeaderHandler_t2148_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4399,
	4400,
	4401,
};
static Il2CppInterfaceOffsetPair HeaderHandler_t2148_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HeaderHandler_t2148_0_0_0;
extern const Il2CppType HeaderHandler_t2148_1_0_0;
struct HeaderHandler_t2148;
const Il2CppTypeDefinitionMetadata HeaderHandler_t2148_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HeaderHandler_t2148_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, HeaderHandler_t2148_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13983/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HeaderHandler_t2148_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HeaderHandler"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &HeaderHandler_t2148_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3407/* custom_attributes_cache */
	, &HeaderHandler_t2148_0_0_0/* byval_arg */
	, &HeaderHandler_t2148_1_0_0/* this_arg */
	, &HeaderHandler_t2148_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_HeaderHandler_t2148/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HeaderHandler_t2148)/* instance_size */
	, sizeof (HeaderHandler_t2148)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.ThreadStart
#include "mscorlib_System_Threading_ThreadStart.h"
// Metadata Definition System.Threading.ThreadStart
extern TypeInfo ThreadStart_t2149_il2cpp_TypeInfo;
// System.Threading.ThreadStart
#include "mscorlib_System_Threading_ThreadStartMethodDeclarations.h"
static const EncodedMethodIndex ThreadStart_t2149_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4402,
	4403,
	4404,
};
static Il2CppInterfaceOffsetPair ThreadStart_t2149_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStart_t2149_0_0_0;
extern const Il2CppType ThreadStart_t2149_1_0_0;
struct ThreadStart_t2149;
const Il2CppTypeDefinitionMetadata ThreadStart_t2149_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStart_t2149_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ThreadStart_t2149_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13987/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadStart_t2149_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStart"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadStart_t2149_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3408/* custom_attributes_cache */
	, &ThreadStart_t2149_0_0_0/* byval_arg */
	, &ThreadStart_t2149_1_0_0/* this_arg */
	, &ThreadStart_t2149_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ThreadStart_t2149/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStart_t2149)/* instance_size */
	, sizeof (ThreadStart_t2149)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.TimerCallback
#include "mscorlib_System_Threading_TimerCallback.h"
// Metadata Definition System.Threading.TimerCallback
extern TypeInfo TimerCallback_t2068_il2cpp_TypeInfo;
// System.Threading.TimerCallback
#include "mscorlib_System_Threading_TimerCallbackMethodDeclarations.h"
static const EncodedMethodIndex TimerCallback_t2068_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	581,
	4405,
	4406,
};
static Il2CppInterfaceOffsetPair TimerCallback_t2068_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimerCallback_t2068_0_0_0;
extern const Il2CppType TimerCallback_t2068_1_0_0;
struct TimerCallback_t2068;
const Il2CppTypeDefinitionMetadata TimerCallback_t2068_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimerCallback_t2068_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, TimerCallback_t2068_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13991/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TimerCallback_t2068_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimerCallback"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &TimerCallback_t2068_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3409/* custom_attributes_cache */
	, &TimerCallback_t2068_0_0_0/* byval_arg */
	, &TimerCallback_t2068_1_0_0/* this_arg */
	, &TimerCallback_t2068_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TimerCallback_t2068/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimerCallback_t2068)/* instance_size */
	, sizeof (TimerCallback_t2068)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.WaitCallback
#include "mscorlib_System_Threading_WaitCallback.h"
// Metadata Definition System.Threading.WaitCallback
extern TypeInfo WaitCallback_t2150_il2cpp_TypeInfo;
// System.Threading.WaitCallback
#include "mscorlib_System_Threading_WaitCallbackMethodDeclarations.h"
static const EncodedMethodIndex WaitCallback_t2150_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4407,
	4408,
	4409,
};
static Il2CppInterfaceOffsetPair WaitCallback_t2150_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WaitCallback_t2150_0_0_0;
extern const Il2CppType WaitCallback_t2150_1_0_0;
struct WaitCallback_t2150;
const Il2CppTypeDefinitionMetadata WaitCallback_t2150_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WaitCallback_t2150_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, WaitCallback_t2150_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13995/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WaitCallback_t2150_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitCallback"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &WaitCallback_t2150_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3410/* custom_attributes_cache */
	, &WaitCallback_t2150_0_0_0/* byval_arg */
	, &WaitCallback_t2150_1_0_0/* this_arg */
	, &WaitCallback_t2150_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WaitCallback_t2150/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitCallback_t2150)/* instance_size */
	, sizeof (WaitCallback_t2150)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`1
extern TypeInfo Action_1_t3501_il2cpp_TypeInfo;
static const EncodedMethodIndex Action_1_t3501_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4410,
	4411,
	4412,
};
static Il2CppInterfaceOffsetPair Action_1_t3501_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Action_1_t3501_0_0_0;
extern const Il2CppType Action_1_t3501_1_0_0;
struct Action_1_t3501;
const Il2CppTypeDefinitionMetadata Action_1_t3501_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_1_t3501_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Action_1_t3501_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13999/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Action_1_t3501_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Action_1_t3501_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_1_t3501_0_0_0/* byval_arg */
	, &Action_1_t3501_1_0_0/* this_arg */
	, &Action_1_t3501_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 173/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializer.h"
// Metadata Definition System.AppDomainInitializer
extern TypeInfo AppDomainInitializer_t2078_il2cpp_TypeInfo;
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializerMethodDeclarations.h"
static const EncodedMethodIndex AppDomainInitializer_t2078_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4413,
	4414,
	4415,
};
static Il2CppInterfaceOffsetPair AppDomainInitializer_t2078_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainInitializer_t2078_0_0_0;
extern const Il2CppType AppDomainInitializer_t2078_1_0_0;
struct AppDomainInitializer_t2078;
const Il2CppTypeDefinitionMetadata AppDomainInitializer_t2078_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppDomainInitializer_t2078_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, AppDomainInitializer_t2078_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14003/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppDomainInitializer_t2078_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainInitializer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AppDomainInitializer_t2078_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3411/* custom_attributes_cache */
	, &AppDomainInitializer_t2078_0_0_0/* byval_arg */
	, &AppDomainInitializer_t2078_1_0_0/* this_arg */
	, &AppDomainInitializer_t2078_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AppDomainInitializer_t2078/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainInitializer_t2078)/* instance_size */
	, sizeof (AppDomainInitializer_t2078)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandler.h"
// Metadata Definition System.AssemblyLoadEventHandler
extern TypeInfo AssemblyLoadEventHandler_t2074_il2cpp_TypeInfo;
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandlerMethodDeclarations.h"
static const EncodedMethodIndex AssemblyLoadEventHandler_t2074_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4416,
	4417,
	4418,
};
static Il2CppInterfaceOffsetPair AssemblyLoadEventHandler_t2074_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyLoadEventHandler_t2074_0_0_0;
extern const Il2CppType AssemblyLoadEventHandler_t2074_1_0_0;
struct AssemblyLoadEventHandler_t2074;
const Il2CppTypeDefinitionMetadata AssemblyLoadEventHandler_t2074_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyLoadEventHandler_t2074_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, AssemblyLoadEventHandler_t2074_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14007/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyLoadEventHandler_t2074_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyLoadEventHandler"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AssemblyLoadEventHandler_t2074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3412/* custom_attributes_cache */
	, &AssemblyLoadEventHandler_t2074_0_0_0/* byval_arg */
	, &AssemblyLoadEventHandler_t2074_1_0_0/* this_arg */
	, &AssemblyLoadEventHandler_t2074_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2074/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyLoadEventHandler_t2074)/* instance_size */
	, sizeof (AssemblyLoadEventHandler_t2074)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Comparison`1
extern TypeInfo Comparison_1_t3502_il2cpp_TypeInfo;
static const EncodedMethodIndex Comparison_1_t3502_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4419,
	4420,
	4421,
};
static Il2CppInterfaceOffsetPair Comparison_1_t3502_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparison_1_t3502_0_0_0;
extern const Il2CppType Comparison_1_t3502_1_0_0;
struct Comparison_1_t3502;
const Il2CppTypeDefinitionMetadata Comparison_1_t3502_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Comparison_1_t3502_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Comparison_1_t3502_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14011/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Comparison_1_t3502_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Comparison_1_t3502_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Comparison_1_t3502_0_0_0/* byval_arg */
	, &Comparison_1_t3502_1_0_0/* this_arg */
	, &Comparison_1_t3502_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 174/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Converter`2
extern TypeInfo Converter_2_t3503_il2cpp_TypeInfo;
static const EncodedMethodIndex Converter_2_t3503_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4422,
	4423,
	4424,
};
static Il2CppInterfaceOffsetPair Converter_2_t3503_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Converter_2_t3503_0_0_0;
extern const Il2CppType Converter_2_t3503_1_0_0;
struct Converter_2_t3503;
const Il2CppTypeDefinitionMetadata Converter_2_t3503_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Converter_2_t3503_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Converter_2_t3503_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14015/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Converter_2_t3503_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Converter`2"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Converter_2_t3503_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Converter_2_t3503_0_0_0/* byval_arg */
	, &Converter_2_t3503_1_0_0/* this_arg */
	, &Converter_2_t3503_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 175/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.EventHandler
#include "mscorlib_System_EventHandler.h"
// Metadata Definition System.EventHandler
extern TypeInfo EventHandler_t2076_il2cpp_TypeInfo;
// System.EventHandler
#include "mscorlib_System_EventHandlerMethodDeclarations.h"
static const EncodedMethodIndex EventHandler_t2076_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4425,
	4426,
	4427,
};
static Il2CppInterfaceOffsetPair EventHandler_t2076_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventHandler_t2076_0_0_0;
extern const Il2CppType EventHandler_t2076_1_0_0;
struct EventHandler_t2076;
const Il2CppTypeDefinitionMetadata EventHandler_t2076_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventHandler_t2076_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, EventHandler_t2076_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14019/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventHandler_t2076_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventHandler"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &EventHandler_t2076_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3413/* custom_attributes_cache */
	, &EventHandler_t2076_0_0_0/* byval_arg */
	, &EventHandler_t2076_1_0_0/* this_arg */
	, &EventHandler_t2076_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EventHandler_t2076/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventHandler_t2076)/* instance_size */
	, sizeof (EventHandler_t2076)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Predicate`1
extern TypeInfo Predicate_1_t3504_il2cpp_TypeInfo;
static const EncodedMethodIndex Predicate_1_t3504_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4428,
	4429,
	4430,
};
static Il2CppInterfaceOffsetPair Predicate_1_t3504_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Predicate_1_t3504_0_0_0;
extern const Il2CppType Predicate_1_t3504_1_0_0;
struct Predicate_1_t3504;
const Il2CppTypeDefinitionMetadata Predicate_1_t3504_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Predicate_1_t3504_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Predicate_1_t3504_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14023/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Predicate_1_t3504_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Predicate`1"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Predicate_1_t3504_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Predicate_1_t3504_0_0_0/* byval_arg */
	, &Predicate_1_t3504_1_0_0/* this_arg */
	, &Predicate_1_t3504_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 176/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandler.h"
// Metadata Definition System.ResolveEventHandler
extern TypeInfo ResolveEventHandler_t2075_il2cpp_TypeInfo;
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandlerMethodDeclarations.h"
static const EncodedMethodIndex ResolveEventHandler_t2075_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4431,
	4432,
	4433,
};
static Il2CppInterfaceOffsetPair ResolveEventHandler_t2075_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventHandler_t2075_0_0_0;
extern const Il2CppType ResolveEventHandler_t2075_1_0_0;
struct ResolveEventHandler_t2075;
const Il2CppTypeDefinitionMetadata ResolveEventHandler_t2075_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResolveEventHandler_t2075_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ResolveEventHandler_t2075_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14027/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResolveEventHandler_t2075_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventHandler"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ResolveEventHandler_t2075_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3414/* custom_attributes_cache */
	, &ResolveEventHandler_t2075_0_0_0/* byval_arg */
	, &ResolveEventHandler_t2075_1_0_0/* this_arg */
	, &ResolveEventHandler_t2075_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ResolveEventHandler_t2075/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventHandler_t2075)/* instance_size */
	, sizeof (ResolveEventHandler_t2075)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
// Metadata Definition System.UnhandledExceptionEventHandler
extern TypeInfo UnhandledExceptionEventHandler_t2077_il2cpp_TypeInfo;
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
static const EncodedMethodIndex UnhandledExceptionEventHandler_t2077_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	4434,
	4435,
	4436,
};
static Il2CppInterfaceOffsetPair UnhandledExceptionEventHandler_t2077_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventHandler_t2077_0_0_0;
extern const Il2CppType UnhandledExceptionEventHandler_t2077_1_0_0;
struct UnhandledExceptionEventHandler_t2077;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventHandler_t2077_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnhandledExceptionEventHandler_t2077_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, UnhandledExceptionEventHandler_t2077_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 14031/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnhandledExceptionEventHandler_t2077_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventHandler"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &UnhandledExceptionEventHandler_t2077_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3415/* custom_attributes_cache */
	, &UnhandledExceptionEventHandler_t2077_0_0_0/* byval_arg */
	, &UnhandledExceptionEventHandler_t2077_1_0_0/* this_arg */
	, &UnhandledExceptionEventHandler_t2077_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t2077/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventHandler_t2077)/* instance_size */
	, sizeof (UnhandledExceptionEventHandler_t2077)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2172_il2cpp_TypeInfo;
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
extern const Il2CppType U24ArrayTypeU2456_t2151_0_0_0;
extern const Il2CppType U24ArrayTypeU2424_t2152_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t2153_0_0_0;
extern const Il2CppType U24ArrayTypeU24120_t2154_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t2155_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t2156_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t2157_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t2158_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t2159_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t2160_0_0_0;
extern const Il2CppType U24ArrayTypeU24136_t2161_0_0_0;
extern const Il2CppType U24ArrayTypeU248_t2162_0_0_0;
extern const Il2CppType U24ArrayTypeU2472_t2163_0_0_0;
extern const Il2CppType U24ArrayTypeU24124_t2164_0_0_0;
extern const Il2CppType U24ArrayTypeU2496_t2165_0_0_0;
extern const Il2CppType U24ArrayTypeU242048_t2166_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t2167_0_0_0;
extern const Il2CppType U24ArrayTypeU241024_t2168_0_0_0;
extern const Il2CppType U24ArrayTypeU24640_t2169_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t2170_0_0_0;
extern const Il2CppType U24ArrayTypeU2452_t2171_0_0_0;
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t2172_il2cpp_TypeInfo__nestedTypes[21] =
{
	&U24ArrayTypeU2456_t2151_0_0_0,
	&U24ArrayTypeU2424_t2152_0_0_0,
	&U24ArrayTypeU2416_t2153_0_0_0,
	&U24ArrayTypeU24120_t2154_0_0_0,
	&U24ArrayTypeU243132_t2155_0_0_0,
	&U24ArrayTypeU2420_t2156_0_0_0,
	&U24ArrayTypeU2432_t2157_0_0_0,
	&U24ArrayTypeU2448_t2158_0_0_0,
	&U24ArrayTypeU2464_t2159_0_0_0,
	&U24ArrayTypeU2412_t2160_0_0_0,
	&U24ArrayTypeU24136_t2161_0_0_0,
	&U24ArrayTypeU248_t2162_0_0_0,
	&U24ArrayTypeU2472_t2163_0_0_0,
	&U24ArrayTypeU24124_t2164_0_0_0,
	&U24ArrayTypeU2496_t2165_0_0_0,
	&U24ArrayTypeU242048_t2166_0_0_0,
	&U24ArrayTypeU24256_t2167_0_0_0,
	&U24ArrayTypeU241024_t2168_0_0_0,
	&U24ArrayTypeU24640_t2169_0_0_0,
	&U24ArrayTypeU24128_t2170_0_0_0,
	&U24ArrayTypeU2452_t2171_0_0_0,
};
static const EncodedMethodIndex U3CPrivateImplementationDetailsU3E_t2172_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2172_0_0_0;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2172_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t2172;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t2172_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t2172_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t2172_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9371/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t2172_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CPrivateImplementationDetailsU3E_t2172_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3416/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t2172_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t2172_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2172)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2172)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t2172_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 53/* field_count */
	, 0/* event_count */
	, 21/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$56
extern TypeInfo U24ArrayTypeU2456_t2151_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2456_t2151_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2456_t2151_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2456_t2151_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2456_t2151_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2456_t2151_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$56"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2456_t2151_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2456_t2151_0_0_0/* byval_arg */
	, &U24ArrayTypeU2456_t2151_1_0_0/* this_arg */
	, &U24ArrayTypeU2456_t2151_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2456_t2151_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2151_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2151_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2456_t2151)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2456_t2151)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2456_t2151_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$24
extern TypeInfo U24ArrayTypeU2424_t2152_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2424_t2152_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2424_t2152_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2424_t2152_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2424_t2152_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2424_t2152_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$24"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2424_t2152_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2424_t2152_0_0_0/* byval_arg */
	, &U24ArrayTypeU2424_t2152_1_0_0/* this_arg */
	, &U24ArrayTypeU2424_t2152_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2424_t2152_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2152_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2152_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2424_t2152)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2424_t2152)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2424_t2152_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t2153_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2416_t2153_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t2153_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t2153_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2416_t2153_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2416_t2153_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2416_t2153_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t2153_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t2153_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t2153_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t2153_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2153_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2153_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t2153)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t2153)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t2153_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
extern TypeInfo U24ArrayTypeU24120_t2154_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24120_t2154_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24120_t2154_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24120_t2154_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24120_t2154_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24120_t2154_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$120"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24120_t2154_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24120_t2154_0_0_0/* byval_arg */
	, &U24ArrayTypeU24120_t2154_1_0_0/* this_arg */
	, &U24ArrayTypeU24120_t2154_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24120_t2154_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2154_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2154_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24120_t2154)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24120_t2154)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24120_t2154_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t2155_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU243132_t2155_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t2155_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t2155_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU243132_t2155_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU243132_t2155_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU243132_t2155_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t2155_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t2155_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t2155_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t2155_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2155_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2155_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t2155)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t2155)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t2155_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t2156_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2420_t2156_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t2156_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t2156_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2420_t2156_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2420_t2156_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2420_t2156_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t2156_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t2156_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t2156_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t2156_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2156_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2156_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t2156)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t2156)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t2156_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t2157_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2432_t2157_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t2157_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t2157_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2432_t2157_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2432_t2157_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2432_t2157_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t2157_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t2157_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t2157_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t2157_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2157_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2157_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t2157)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t2157)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t2157_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t2158_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2448_t2158_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t2158_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t2158_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2448_t2158_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2448_t2158_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2448_t2158_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t2158_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t2158_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t2158_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t2158_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2158_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2158_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t2158)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t2158)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t2158_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t2159_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2464_t2159_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t2159_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t2159_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2464_t2159_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2464_t2159_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2464_t2159_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t2159_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t2159_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t2159_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t2159_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2159_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2159_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t2159)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t2159)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t2159_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t2160_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2412_t2160_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t2160_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t2160_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2412_t2160_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2412_t2160_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2412_t2160_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t2160_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t2160_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t2160_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t2160_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2160_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2160_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t2160)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t2160)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t2160_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t2161_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24136_t2161_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24136_t2161_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t2161_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24136_t2161_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24136_t2161_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24136_t2161_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t2161_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t2161_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t2161_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t2161_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2161_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2161_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t2161)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t2161)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t2161_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$8
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$8
extern TypeInfo U24ArrayTypeU248_t2162_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$8
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU248MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU248_t2162_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU248_t2162_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU248_t2162_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU248_t2162_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU248_t2162_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$8"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU248_t2162_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU248_t2162_0_0_0/* byval_arg */
	, &U24ArrayTypeU248_t2162_1_0_0/* this_arg */
	, &U24ArrayTypeU248_t2162_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU248_t2162_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU248_t2162_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU248_t2162_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU248_t2162)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU248_t2162)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU248_t2162_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$72
extern TypeInfo U24ArrayTypeU2472_t2163_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2472_t2163_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2472_t2163_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2472_t2163_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2472_t2163_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2472_t2163_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$72"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2472_t2163_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2472_t2163_0_0_0/* byval_arg */
	, &U24ArrayTypeU2472_t2163_1_0_0/* this_arg */
	, &U24ArrayTypeU2472_t2163_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2472_t2163_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2163_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2163_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2472_t2163)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2472_t2163)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2472_t2163_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$124
extern TypeInfo U24ArrayTypeU24124_t2164_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24124_t2164_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24124_t2164_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24124_t2164_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24124_t2164_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24124_t2164_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$124"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24124_t2164_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24124_t2164_0_0_0/* byval_arg */
	, &U24ArrayTypeU24124_t2164_1_0_0/* this_arg */
	, &U24ArrayTypeU24124_t2164_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24124_t2164_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2164_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2164_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24124_t2164)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24124_t2164)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24124_t2164_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$96
extern TypeInfo U24ArrayTypeU2496_t2165_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2496_t2165_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2496_t2165_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2496_t2165_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2496_t2165_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2496_t2165_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$96"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2496_t2165_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2496_t2165_0_0_0/* byval_arg */
	, &U24ArrayTypeU2496_t2165_1_0_0/* this_arg */
	, &U24ArrayTypeU2496_t2165_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2496_t2165_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2165_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2165_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2496_t2165)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2496_t2165)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2496_t2165_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$2048
extern TypeInfo U24ArrayTypeU242048_t2166_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU242048_t2166_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU242048_t2166_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU242048_t2166_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU242048_t2166_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU242048_t2166_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$2048"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU242048_t2166_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU242048_t2166_0_0_0/* byval_arg */
	, &U24ArrayTypeU242048_t2166_1_0_0/* this_arg */
	, &U24ArrayTypeU242048_t2166_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU242048_t2166_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2166_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2166_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU242048_t2166)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU242048_t2166)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU242048_t2166_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t2167_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24256_t2167_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t2167_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t2167_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24256_t2167_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24256_t2167_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24256_t2167_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t2167_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t2167_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t2167_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t2167_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2167_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2167_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t2167)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t2167)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t2167_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
extern TypeInfo U24ArrayTypeU241024_t2168_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU241024_t2168_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU241024_t2168_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU241024_t2168_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU241024_t2168_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU241024_t2168_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$1024"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU241024_t2168_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU241024_t2168_0_0_0/* byval_arg */
	, &U24ArrayTypeU241024_t2168_1_0_0/* this_arg */
	, &U24ArrayTypeU241024_t2168_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU241024_t2168_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2168_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2168_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU241024_t2168)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU241024_t2168)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU241024_t2168_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$640
extern TypeInfo U24ArrayTypeU24640_t2169_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24640_t2169_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24640_t2169_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24640_t2169_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24640_t2169_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24640_t2169_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$640"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24640_t2169_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24640_t2169_0_0_0/* byval_arg */
	, &U24ArrayTypeU24640_t2169_1_0_0/* this_arg */
	, &U24ArrayTypeU24640_t2169_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24640_t2169_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2169_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2169_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24640_t2169)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24640_t2169)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24640_t2169_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t2170_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24128_t2170_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t2170_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t2170_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24128_t2170_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24128_t2170_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24128_t2170_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t2170_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t2170_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t2170_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t2170_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2170_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2170_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t2170)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t2170)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t2170_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$52
extern TypeInfo U24ArrayTypeU2452_t2171_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2452_t2171_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2452_t2171_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2452_t2171_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2172_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2452_t2171_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2452_t2171_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$52"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2452_t2171_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2452_t2171_0_0_0/* byval_arg */
	, &U24ArrayTypeU2452_t2171_1_0_0/* this_arg */
	, &U24ArrayTypeU2452_t2171_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2452_t2171_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2171_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2171_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2452_t2171)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2452_t2171)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2452_t2171_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
