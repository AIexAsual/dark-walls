﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t2905;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1463;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22668_gshared (ShimEnumerator_t2905 * __this, Dictionary_2_t1463 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22668(__this, ___host, method) (( void (*) (ShimEnumerator_t2905 *, Dictionary_2_t1463 *, const MethodInfo*))ShimEnumerator__ctor_m22668_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22669_gshared (ShimEnumerator_t2905 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22669(__this, method) (( bool (*) (ShimEnumerator_t2905 *, const MethodInfo*))ShimEnumerator_MoveNext_m22669_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m22670_gshared (ShimEnumerator_t2905 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22670(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2905 *, const MethodInfo*))ShimEnumerator_get_Entry_m22670_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22671_gshared (ShimEnumerator_t2905 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22671(__this, method) (( Object_t * (*) (ShimEnumerator_t2905 *, const MethodInfo*))ShimEnumerator_get_Key_m22671_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22672_gshared (ShimEnumerator_t2905 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22672(__this, method) (( Object_t * (*) (ShimEnumerator_t2905 *, const MethodInfo*))ShimEnumerator_get_Value_m22672_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22673_gshared (ShimEnumerator_t2905 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22673(__this, method) (( Object_t * (*) (ShimEnumerator_t2905 *, const MethodInfo*))ShimEnumerator_get_Current_m22673_gshared)(__this, method)
