﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.Animator
struct Animator_t321;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// IKHandler
struct  IKHandler_t322  : public MonoBehaviour_t4
{
	// UnityEngine.GameObject IKHandler::Left_Arm_IK
	GameObject_t256 * ___Left_Arm_IK_2;
	// UnityEngine.GameObject IKHandler::Right_Arm_IK
	GameObject_t256 * ___Right_Arm_IK_3;
	// UnityEngine.GameObject IKHandler::Head_IK
	GameObject_t256 * ___Head_IK_4;
	// UnityEngine.GameObject IKHandler::player
	GameObject_t256 * ___player_5;
	// System.Single IKHandler::IKWeight
	float ___IKWeight_6;
	// UnityEngine.Animator IKHandler::anim
	Animator_t321 * ___anim_7;
};
