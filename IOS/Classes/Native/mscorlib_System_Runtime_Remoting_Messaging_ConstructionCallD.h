﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t398;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t440;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
struct  ConstructionCallDictionary_t1873  : public MethodDictionary_t1874
{
};
struct ConstructionCallDictionary_t1873_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.ConstructionCallDictionary::InternalKeys
	StringU5BU5D_t398* ___InternalKeys_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Remoting.Messaging.ConstructionCallDictionary::<>f__switch$map23
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map23_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Runtime.Remoting.Messaging.ConstructionCallDictionary::<>f__switch$map24
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map24_8;
};
