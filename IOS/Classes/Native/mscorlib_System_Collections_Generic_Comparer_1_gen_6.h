﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Color>
struct Comparer_1_t2437;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Color>
struct  Comparer_1_t2437  : public Object_t
{
};
struct Comparer_1_t2437_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Color>::_default
	Comparer_1_t2437 * ____default_0;
};
