﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Enumerator_t2552;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t643;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t792;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t645;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m18088(__this, ___dictionary, method) (( void (*) (Enumerator_t2552 *, Dictionary_2_t645 *, const MethodInfo*))Enumerator__ctor_m14807_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18089(__this, method) (( Object_t * (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18090(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14811_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18091(__this, method) (( Object_t * (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18092(__this, method) (( Object_t * (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14815_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define Enumerator_MoveNext_m18093(__this, method) (( bool (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_MoveNext_m14817_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
#define Enumerator_get_Current_m18094(__this, method) (( KeyValuePair_2_t2549  (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_get_Current_m14819_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18095(__this, method) (( Font_t643 * (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_get_CurrentKey_m14821_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18096(__this, method) (( List_1_t792 * (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_get_CurrentValue_m14823_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyState()
#define Enumerator_VerifyState_m18097(__this, method) (( void (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_VerifyState_m14825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18098(__this, method) (( void (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_VerifyCurrent_m14827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Dispose()
#define Enumerator_Dispose_m18099(__this, method) (( void (*) (Enumerator_t2552 *, const MethodInfo*))Enumerator_Dispose_m14829_gshared)(__this, method)
