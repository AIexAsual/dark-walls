﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CapsuleCollider
struct CapsuleCollider_t415;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern "C" Vector3_t215  CapsuleCollider_get_center_m3260 (CapsuleCollider_t415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern "C" void CapsuleCollider_set_center_m3250 (CapsuleCollider_t415 * __this, Vector3_t215  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CapsuleCollider::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C" void CapsuleCollider_INTERNAL_get_center_m5753 (CapsuleCollider_t415 * __this, Vector3_t215 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CapsuleCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C" void CapsuleCollider_INTERNAL_set_center_m5754 (CapsuleCollider_t415 * __this, Vector3_t215 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CapsuleCollider::get_radius()
extern "C" float CapsuleCollider_get_radius_m3255 (CapsuleCollider_t415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CapsuleCollider::get_height()
extern "C" float CapsuleCollider_get_height_m3248 (CapsuleCollider_t415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern "C" void CapsuleCollider_set_height_m3259 (CapsuleCollider_t415 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
