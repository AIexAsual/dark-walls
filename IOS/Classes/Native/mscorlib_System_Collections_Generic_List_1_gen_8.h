﻿#pragma once
#include <stdint.h>
// iTween/EaseType[]
struct EaseTypeU5BU5D_t451;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<iTween/EaseType>
struct  List_1_t580  : public Object_t
{
	// T[] System.Collections.Generic.List`1<iTween/EaseType>::_items
	EaseTypeU5BU5D_t451* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<iTween/EaseType>::_version
	int32_t ____version_3;
};
struct List_1_t580_StaticFields{
	// T[] System.Collections.Generic.List`1<iTween/EaseType>::EmptyArray
	EaseTypeU5BU5D_t451* ___EmptyArray_4;
};
