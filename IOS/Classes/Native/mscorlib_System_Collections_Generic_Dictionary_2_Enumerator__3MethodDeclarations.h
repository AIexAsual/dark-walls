﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>
struct Enumerator_t2333;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
struct Dictionary_2_t2327;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m14865_gshared (Enumerator_t2333 * __this, Dictionary_2_t2327 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m14865(__this, ___dictionary, method) (( void (*) (Enumerator_t2333 *, Dictionary_2_t2327 *, const MethodInfo*))Enumerator__ctor_m14865_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14866_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14866(__this, method) (( Object_t * (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14866_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14867_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14867(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14867_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14868_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14868(__this, method) (( Object_t * (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14868_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14869_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14869(__this, method) (( Object_t * (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14869_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14870_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14870(__this, method) (( bool (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_MoveNext_m14870_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2328  Enumerator_get_Current_m14871_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14871(__this, method) (( KeyValuePair_2_t2328  (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_get_Current_m14871_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m14872_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m14872(__this, method) (( int32_t (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_get_CurrentKey_m14872_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m14873_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m14873(__this, method) (( Object_t * (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_get_CurrentValue_m14873_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m14874_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14874(__this, method) (( void (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_VerifyState_m14874_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m14875_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m14875(__this, method) (( void (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_VerifyCurrent_m14875_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m14876_gshared (Enumerator_t2333 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14876(__this, method) (( void (*) (Enumerator_t2333 *, const MethodInfo*))Enumerator_Dispose_m14876_gshared)(__this, method)
