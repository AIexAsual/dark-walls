﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_EyesVision_1
struct CameraFilterPack_EyesVision_1_t118;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_EyesVision_1::.ctor()
extern "C" void CameraFilterPack_EyesVision_1__ctor_m758 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_EyesVision_1::get_material()
extern "C" Material_t2 * CameraFilterPack_EyesVision_1_get_material_m759 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::Start()
extern "C" void CameraFilterPack_EyesVision_1_Start_m760 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_EyesVision_1_OnRenderImage_m761 (CameraFilterPack_EyesVision_1_t118 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::Update()
extern "C" void CameraFilterPack_EyesVision_1_Update_m762 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EyesVision_1::OnDisable()
extern "C" void CameraFilterPack_EyesVision_1_OnDisable_m763 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
