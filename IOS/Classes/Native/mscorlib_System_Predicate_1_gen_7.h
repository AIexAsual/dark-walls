﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t243;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Transform>
struct  Predicate_1_t2276  : public MulticastDelegate_t219
{
};
