﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveBaby
struct InteractiveBaby_t324;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void InteractiveBaby::.ctor()
extern "C" void InteractiveBaby__ctor_m1910 (InteractiveBaby_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::Start()
extern "C" void InteractiveBaby_Start_m1911 (InteractiveBaby_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::Update()
extern "C" void InteractiveBaby_Update_m1912 (InteractiveBaby_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::EnableBaby(System.Boolean)
extern "C" void InteractiveBaby_EnableBaby_m1913 (InteractiveBaby_t324 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::HideBaby(System.Boolean)
extern "C" void InteractiveBaby_HideBaby_m1914 (InteractiveBaby_t324 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::BabyWalk()
extern "C" void InteractiveBaby_BabyWalk_m1915 (InteractiveBaby_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::BabyIdle()
extern "C" void InteractiveBaby_BabyIdle_m1916 (InteractiveBaby_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::BabyPos(UnityEngine.Vector3)
extern "C" void InteractiveBaby_BabyPos_m1917 (InteractiveBaby_t324 * __this, Vector3_t215  ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveBaby::BabyScream()
extern "C" void InteractiveBaby_BabyScream_m1918 (InteractiveBaby_t324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
