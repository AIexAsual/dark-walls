﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_HSV
struct CameraFilterPack_Colors_HSV_t76;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_HSV::.ctor()
extern "C" void CameraFilterPack_Colors_HSV__ctor_m468 (CameraFilterPack_Colors_HSV_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_HSV::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_HSV_get_material_m469 (CameraFilterPack_Colors_HSV_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::Start()
extern "C" void CameraFilterPack_Colors_HSV_Start_m470 (CameraFilterPack_Colors_HSV_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_HSV_OnRenderImage_m471 (CameraFilterPack_Colors_HSV_t76 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::OnValidate()
extern "C" void CameraFilterPack_Colors_HSV_OnValidate_m472 (CameraFilterPack_Colors_HSV_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::Update()
extern "C" void CameraFilterPack_Colors_HSV_Update_m473 (CameraFilterPack_Colors_HSV_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HSV::OnDisable()
extern "C" void CameraFilterPack_Colors_HSV_OnDisable_m474 (CameraFilterPack_Colors_HSV_t76 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
