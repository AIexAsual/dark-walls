﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Manga
struct CameraFilterPack_Drawing_Manga_t102;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Manga::.ctor()
extern "C" void CameraFilterPack_Drawing_Manga__ctor_m650 (CameraFilterPack_Drawing_Manga_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Manga_get_material_m651 (CameraFilterPack_Drawing_Manga_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::Start()
extern "C" void CameraFilterPack_Drawing_Manga_Start_m652 (CameraFilterPack_Drawing_Manga_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Manga_OnRenderImage_m653 (CameraFilterPack_Drawing_Manga_t102 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::OnValidate()
extern "C" void CameraFilterPack_Drawing_Manga_OnValidate_m654 (CameraFilterPack_Drawing_Manga_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::Update()
extern "C" void CameraFilterPack_Drawing_Manga_Update_m655 (CameraFilterPack_Drawing_Manga_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga::OnDisable()
extern "C" void CameraFilterPack_Drawing_Manga_OnDisable_m656 (CameraFilterPack_Drawing_Manga_t102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
