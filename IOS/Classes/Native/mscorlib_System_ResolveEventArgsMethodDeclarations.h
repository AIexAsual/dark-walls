﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ResolveEventArgs
struct ResolveEventArgs_t2131;
// System.String
struct String_t;

// System.Void System.ResolveEventArgs::.ctor(System.String)
extern "C" void ResolveEventArgs__ctor_m13284 (ResolveEventArgs_t2131 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
