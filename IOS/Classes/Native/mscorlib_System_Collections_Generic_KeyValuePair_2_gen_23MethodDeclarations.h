﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct KeyValuePair_2_t2770;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t995;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21143(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2770 *, Type_t *, ConstructorDelegate_t995 *, const MethodInfo*))KeyValuePair_2__ctor_m14739_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m21144(__this, method) (( Type_t * (*) (KeyValuePair_2_t2770 *, const MethodInfo*))KeyValuePair_2_get_Key_m14741_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21145(__this, ___value, method) (( void (*) (KeyValuePair_2_t2770 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m14743_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m21146(__this, method) (( ConstructorDelegate_t995 * (*) (KeyValuePair_2_t2770 *, const MethodInfo*))KeyValuePair_2_get_Value_m14745_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21147(__this, ___value, method) (( void (*) (KeyValuePair_2_t2770 *, ConstructorDelegate_t995 *, const MethodInfo*))KeyValuePair_2_set_Value_m14747_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToString()
#define KeyValuePair_2_ToString_m21148(__this, method) (( String_t* (*) (KeyValuePair_2_t2770 *, const MethodInfo*))KeyValuePair_2_ToString_m14749_gshared)(__this, method)
