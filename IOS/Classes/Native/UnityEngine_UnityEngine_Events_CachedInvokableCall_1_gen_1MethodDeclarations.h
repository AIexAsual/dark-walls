﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct CachedInvokableCall_1_t1166;
// UnityEngine.Object
struct Object_t473;
struct Object_t473_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t470;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m6489(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1166 *, Object_t473 *, MethodInfo_t *, String_t*, const MethodInfo*))CachedInvokableCall_1__ctor_m22044_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m22056(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1166 *, ObjectU5BU5D_t470*, const MethodInfo*))CachedInvokableCall_1_Invoke_m22045_gshared)(__this, ___args, method)
