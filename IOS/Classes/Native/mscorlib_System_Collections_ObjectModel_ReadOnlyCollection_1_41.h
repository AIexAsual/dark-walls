﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.Events.PersistentCall>
struct IList_1_t2854;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Events.PersistentCall>
struct  ReadOnlyCollection_1_t2855  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Events.PersistentCall>::list
	Object_t* ___list_0;
};
