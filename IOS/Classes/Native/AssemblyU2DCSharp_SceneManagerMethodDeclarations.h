﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SceneManager
struct SceneManager_t381;
// SceneManager/OnStateChangeHandler
struct OnStateChangeHandler_t380;
// System.String
struct String_t;
// SceneEvent
#include "AssemblyU2DCSharp_SceneEvent.h"

// System.Void SceneManager::.ctor()
extern "C" void SceneManager__ctor_m2188 (SceneManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneManager::.cctor()
extern "C" void SceneManager__cctor_m2189 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneManager::add_OnStateChangeEvent(SceneManager/OnStateChangeHandler)
extern "C" void SceneManager_add_OnStateChangeEvent_m2190 (SceneManager_t381 * __this, OnStateChangeHandler_t380 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneManager::remove_OnStateChangeEvent(SceneManager/OnStateChangeHandler)
extern "C" void SceneManager_remove_OnStateChangeEvent_m2191 (SceneManager_t381 * __this, OnStateChangeHandler_t380 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneManager SceneManager::get_Instance()
extern "C" SceneManager_t381 * SceneManager_get_Instance_m2192 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneManager::dispatchEvent(SceneEvent)
extern "C" void SceneManager_dispatchEvent_m2193 (SceneManager_t381 * __this, int32_t ___stateEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneManager::Start()
extern "C" void SceneManager_Start_m2194 (SceneManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneManager::Update()
extern "C" void SceneManager_Update_m2195 (SceneManager_t381 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneManager::ChangeScene(System.String)
extern "C" void SceneManager_ChangeScene_m2196 (SceneManager_t381 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
