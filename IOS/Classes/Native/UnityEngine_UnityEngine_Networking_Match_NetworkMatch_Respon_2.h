﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.ListMatchResponse
struct ListMatchResponse_t976;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>
struct  ResponseDelegate_1_t1092  : public MulticastDelegate_t219
{
};
