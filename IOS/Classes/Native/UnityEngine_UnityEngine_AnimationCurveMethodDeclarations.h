﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t944;
struct AnimationCurve_t944_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1085;

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m5811 (AnimationCurve_t944 * __this, KeyframeU5BU5D_t1085* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m5812 (AnimationCurve_t944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m5813 (AnimationCurve_t944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m5814 (AnimationCurve_t944 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m5815 (AnimationCurve_t944 * __this, KeyframeU5BU5D_t1085* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void AnimationCurve_t944_marshal(const AnimationCurve_t944& unmarshaled, AnimationCurve_t944_marshaled& marshaled);
void AnimationCurve_t944_marshal_back(const AnimationCurve_t944_marshaled& marshaled, AnimationCurve_t944& unmarshaled);
void AnimationCurve_t944_marshal_cleanup(AnimationCurve_t944_marshaled& marshaled);
