﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Vision_Crystal
struct  CameraFilterPack_Vision_Crystal_t206  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Vision_Crystal::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Vision_Crystal::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Vision_Crystal::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Vision_Crystal::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Vision_Crystal::Value
	float ___Value_6;
	// System.Single CameraFilterPack_Vision_Crystal::X
	float ___X_7;
	// System.Single CameraFilterPack_Vision_Crystal::Y
	float ___Y_8;
	// System.Single CameraFilterPack_Vision_Crystal::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Vision_Crystal_t206_StaticFields{
	// System.Single CameraFilterPack_Vision_Crystal::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Vision_Crystal::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Vision_Crystal::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Vision_Crystal::ChangeValue4
	float ___ChangeValue4_13;
};
