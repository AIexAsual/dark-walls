﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_InverChromiLum
struct CameraFilterPack_FX_InverChromiLum_t135;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_InverChromiLum::.ctor()
extern "C" void CameraFilterPack_FX_InverChromiLum__ctor_m874 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_InverChromiLum::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_InverChromiLum_get_material_m875 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::Start()
extern "C" void CameraFilterPack_FX_InverChromiLum_Start_m876 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_InverChromiLum_OnRenderImage_m877 (CameraFilterPack_FX_InverChromiLum_t135 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::Update()
extern "C" void CameraFilterPack_FX_InverChromiLum_Update_m878 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_InverChromiLum::OnDisable()
extern "C" void CameraFilterPack_FX_InverChromiLum_OnDisable_m879 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
