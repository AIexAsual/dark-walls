﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>
struct EqualityComparer_1_t2443;
// System.Object
struct Object_t;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>::.ctor()
extern "C" void EqualityComparer_1__ctor_m16336_gshared (EqualityComparer_1_t2443 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m16336(__this, method) (( void (*) (EqualityComparer_1_t2443 *, const MethodInfo*))EqualityComparer_1__ctor_m16336_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>::.cctor()
extern "C" void EqualityComparer_1__cctor_m16337_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m16337(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m16337_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16338_gshared (EqualityComparer_1_t2443 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16338(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2443 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16338_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16339_gshared (EqualityComparer_1_t2443 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16339(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2443 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16339_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>::get_Default()
extern "C" EqualityComparer_1_t2443 * EqualityComparer_1_get_Default_m16340_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m16340(__this /* static, unused */, method) (( EqualityComparer_1_t2443 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m16340_gshared)(__this /* static, unused */, method)
