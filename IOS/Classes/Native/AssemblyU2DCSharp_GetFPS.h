﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// GetFPS
struct  GetFPS_t314  : public MonoBehaviour_t4
{
	// System.Single GetFPS::deltaTime
	float ___deltaTime_2;
	// System.Int32 GetFPS::size
	int32_t ___size_3;
	// System.String GetFPS::debugText
	String_t* ___debugText_4;
};
