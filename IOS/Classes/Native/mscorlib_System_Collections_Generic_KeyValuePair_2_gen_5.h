﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>
struct  KeyValuePair_2_t2328 
{
	// TKey System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::value
	Object_t * ___value_1;
};
