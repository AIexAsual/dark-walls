﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t398;
// System.Int32[]
struct Int32U5BU5D_t269;
// System.Single[]
struct SingleU5BU5D_t72;
// System.Boolean[]
struct BooleanU5BU5D_t448;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// UnityEngine.Color[]
struct ColorU5BU5D_t449;
// UnityEngine.Space[]
struct SpaceU5BU5D_t450;
// iTween/EaseType[]
struct EaseTypeU5BU5D_t451;
// iTween/LoopType[]
struct LoopTypeU5BU5D_t452;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t323;
// UnityEngine.Transform[]
struct TransformU5BU5D_t423;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t338;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t453;
// ArrayIndexes[]
struct ArrayIndexesU5BU5D_t454;
// iTweenPath[]
struct iTweenPathU5BU5D_t455;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t456;
// iTween
struct iTween_t432;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
// iTweenEvent
struct  iTweenEvent_t443  : public MonoBehaviour_t4
{
	// System.String iTweenEvent::tweenName
	String_t* ___tweenName_3;
	// System.Boolean iTweenEvent::playAutomatically
	bool ___playAutomatically_4;
	// System.Single iTweenEvent::delay
	float ___delay_5;
	// iTweenEvent/TweenType iTweenEvent::type
	int32_t ___type_6;
	// System.Boolean iTweenEvent::showIconInInspector
	bool ___showIconInInspector_7;
	// System.String[] iTweenEvent::keys
	StringU5BU5D_t398* ___keys_8;
	// System.Int32[] iTweenEvent::indexes
	Int32U5BU5D_t269* ___indexes_9;
	// System.String[] iTweenEvent::metadatas
	StringU5BU5D_t398* ___metadatas_10;
	// System.Int32[] iTweenEvent::ints
	Int32U5BU5D_t269* ___ints_11;
	// System.Single[] iTweenEvent::floats
	SingleU5BU5D_t72* ___floats_12;
	// System.Boolean[] iTweenEvent::bools
	BooleanU5BU5D_t448* ___bools_13;
	// System.String[] iTweenEvent::strings
	StringU5BU5D_t398* ___strings_14;
	// UnityEngine.Vector3[] iTweenEvent::vector3s
	Vector3U5BU5D_t317* ___vector3s_15;
	// UnityEngine.Color[] iTweenEvent::colors
	ColorU5BU5D_t449* ___colors_16;
	// UnityEngine.Space[] iTweenEvent::spaces
	SpaceU5BU5D_t450* ___spaces_17;
	// iTween/EaseType[] iTweenEvent::easeTypes
	EaseTypeU5BU5D_t451* ___easeTypes_18;
	// iTween/LoopType[] iTweenEvent::loopTypes
	LoopTypeU5BU5D_t452* ___loopTypes_19;
	// UnityEngine.GameObject[] iTweenEvent::gameObjects
	GameObjectU5BU5D_t323* ___gameObjects_20;
	// UnityEngine.Transform[] iTweenEvent::transforms
	TransformU5BU5D_t423* ___transforms_21;
	// UnityEngine.AudioClip[] iTweenEvent::audioClips
	AudioClipU5BU5D_t338* ___audioClips_22;
	// UnityEngine.AudioSource[] iTweenEvent::audioSources
	AudioSourceU5BU5D_t453* ___audioSources_23;
	// ArrayIndexes[] iTweenEvent::vector3Arrays
	ArrayIndexesU5BU5D_t454* ___vector3Arrays_24;
	// ArrayIndexes[] iTweenEvent::transformArrays
	ArrayIndexesU5BU5D_t454* ___transformArrays_25;
	// iTweenPath[] iTweenEvent::paths
	iTweenPathU5BU5D_t455* ___paths_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> iTweenEvent::values
	Dictionary_2_t456 * ___values_27;
	// System.Boolean iTweenEvent::stopped
	bool ___stopped_28;
	// iTween iTweenEvent::instantiatedTween
	iTween_t432 * ___instantiatedTween_29;
	// System.String iTweenEvent::internalName
	String_t* ___internalName_30;
};
