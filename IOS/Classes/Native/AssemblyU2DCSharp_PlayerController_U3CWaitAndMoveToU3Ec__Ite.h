﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// PlayerController
struct PlayerController_t356;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// PlayerController/<WaitAndMoveTo>c__Iterator11
struct  U3CWaitAndMoveToU3Ec__Iterator11_t391  : public Object_t
{
	// System.Single PlayerController/<WaitAndMoveTo>c__Iterator11::delay
	float ___delay_0;
	// UnityEngine.Vector3 PlayerController/<WaitAndMoveTo>c__Iterator11::val
	Vector3_t215  ___val_1;
	// System.Int32 PlayerController/<WaitAndMoveTo>c__Iterator11::$PC
	int32_t ___U24PC_2;
	// System.Object PlayerController/<WaitAndMoveTo>c__Iterator11::$current
	Object_t * ___U24current_3;
	// System.Single PlayerController/<WaitAndMoveTo>c__Iterator11::<$>delay
	float ___U3CU24U3Edelay_4;
	// UnityEngine.Vector3 PlayerController/<WaitAndMoveTo>c__Iterator11::<$>val
	Vector3_t215  ___U3CU24U3Eval_5;
	// PlayerController PlayerController/<WaitAndMoveTo>c__Iterator11::<>f__this
	PlayerController_t356 * ___U3CU3Ef__this_6;
};
