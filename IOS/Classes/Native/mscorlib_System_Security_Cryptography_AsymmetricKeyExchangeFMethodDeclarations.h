﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
struct AsymmetricKeyExchangeFormatter_t1964;
// System.Byte[]
struct ByteU5BU5D_t469;

// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m11828 (AsymmetricKeyExchangeFormatter_t1964 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
