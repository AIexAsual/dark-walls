﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>
struct EqualityComparer_1_t2443;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>
struct  EqualityComparer_1_t2443  : public Object_t
{
};
struct EqualityComparer_1_t2443_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Space>::_default
	EqualityComparer_1_t2443 * ____default_0;
};
