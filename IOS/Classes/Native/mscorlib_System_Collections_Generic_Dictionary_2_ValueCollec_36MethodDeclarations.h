﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>
struct Enumerator_t2884;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2876;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22457_gshared (Enumerator_t2884 * __this, Dictionary_2_t2876 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22457(__this, ___host, method) (( void (*) (Enumerator_t2884 *, Dictionary_2_t2876 *, const MethodInfo*))Enumerator__ctor_m22457_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22458_gshared (Enumerator_t2884 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22458(__this, method) (( Object_t * (*) (Enumerator_t2884 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22458_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m22459_gshared (Enumerator_t2884 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22459(__this, method) (( void (*) (Enumerator_t2884 *, const MethodInfo*))Enumerator_Dispose_m22459_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22460_gshared (Enumerator_t2884 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22460(__this, method) (( bool (*) (Enumerator_t2884 *, const MethodInfo*))Enumerator_MoveNext_m22460_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" bool Enumerator_get_Current_m22461_gshared (Enumerator_t2884 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22461(__this, method) (( bool (*) (Enumerator_t2884 *, const MethodInfo*))Enumerator_get_Current_m22461_gshared)(__this, method)
