﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MobileNativeRateUs
struct MobileNativeRateUs_t281;
// System.String
struct String_t;
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"

// System.Void MobileNativeRateUs::.ctor(System.String,System.String)
extern "C" void MobileNativeRateUs__ctor_m1745 (MobileNativeRateUs_t281 * __this, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::.ctor(System.String,System.String,System.String,System.String,System.String)
extern "C" void MobileNativeRateUs__ctor_m1746 (MobileNativeRateUs_t281 * __this, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___later, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::SetAndroidAppUrl(System.String)
extern "C" void MobileNativeRateUs_SetAndroidAppUrl_m1747 (MobileNativeRateUs_t281 * __this, String_t* ____url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::SetAppleId(System.String)
extern "C" void MobileNativeRateUs_SetAppleId_m1748 (MobileNativeRateUs_t281 * __this, String_t* ____appleId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::Start()
extern "C" void MobileNativeRateUs_Start_m1749 (MobileNativeRateUs_t281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::OnCompleteListener(MNDialogResult)
extern "C" void MobileNativeRateUs_OnCompleteListener_m1750 (MobileNativeRateUs_t281 * __this, int32_t ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeRateUs::<OnComplete>m__6(MNDialogResult)
extern "C" void MobileNativeRateUs_U3COnCompleteU3Em__6_m1751 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
