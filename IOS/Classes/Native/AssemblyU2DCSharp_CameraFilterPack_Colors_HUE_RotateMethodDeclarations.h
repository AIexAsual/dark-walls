﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_HUE_Rotate
struct CameraFilterPack_Colors_HUE_Rotate_t77;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_HUE_Rotate::.ctor()
extern "C" void CameraFilterPack_Colors_HUE_Rotate__ctor_m475 (CameraFilterPack_Colors_HUE_Rotate_t77 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_HUE_Rotate::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_HUE_Rotate_get_material_m476 (CameraFilterPack_Colors_HUE_Rotate_t77 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::Start()
extern "C" void CameraFilterPack_Colors_HUE_Rotate_Start_m477 (CameraFilterPack_Colors_HUE_Rotate_t77 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_HUE_Rotate_OnRenderImage_m478 (CameraFilterPack_Colors_HUE_Rotate_t77 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::OnValidate()
extern "C" void CameraFilterPack_Colors_HUE_Rotate_OnValidate_m479 (CameraFilterPack_Colors_HUE_Rotate_t77 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::Update()
extern "C" void CameraFilterPack_Colors_HUE_Rotate_Update_m480 (CameraFilterPack_Colors_HUE_Rotate_t77 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_HUE_Rotate::OnDisable()
extern "C" void CameraFilterPack_Colors_HUE_Rotate_OnDisable_m481 (CameraFilterPack_Colors_HUE_Rotate_t77 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
