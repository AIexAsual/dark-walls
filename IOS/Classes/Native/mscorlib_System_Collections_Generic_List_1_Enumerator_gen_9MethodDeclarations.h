﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Char>
struct Enumerator_t2289;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t2288;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m14386_gshared (Enumerator_t2289 * __this, List_1_t2288 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m14386(__this, ___l, method) (( void (*) (Enumerator_t2289 *, List_1_t2288 *, const MethodInfo*))Enumerator__ctor_m14386_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14387_gshared (Enumerator_t2289 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14387(__this, method) (( Object_t * (*) (Enumerator_t2289 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14387_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::Dispose()
extern "C" void Enumerator_Dispose_m14388_gshared (Enumerator_t2289 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14388(__this, method) (( void (*) (Enumerator_t2289 *, const MethodInfo*))Enumerator_Dispose_m14388_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::VerifyState()
extern "C" void Enumerator_VerifyState_m14389_gshared (Enumerator_t2289 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14389(__this, method) (( void (*) (Enumerator_t2289 *, const MethodInfo*))Enumerator_VerifyState_m14389_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14390_gshared (Enumerator_t2289 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14390(__this, method) (( bool (*) (Enumerator_t2289 *, const MethodInfo*))Enumerator_MoveNext_m14390_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
extern "C" uint16_t Enumerator_get_Current_m14391_gshared (Enumerator_t2289 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14391(__this, method) (( uint16_t (*) (Enumerator_t2289 *, const MethodInfo*))Enumerator_get_Current_m14391_gshared)(__this, method)
