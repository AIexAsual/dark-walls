﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.NavMeshAgent
struct NavMeshAgent_t307;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Boolean UnityEngine.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern "C" bool NavMeshAgent_SetDestination_m3200 (NavMeshAgent_t307 * __this, Vector3_t215  ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_SetDestination(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
extern "C" bool NavMeshAgent_INTERNAL_CALL_SetDestination_m5767 (Object_t * __this /* static, unused */, NavMeshAgent_t307 * ___self, Vector3_t215 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_destination(UnityEngine.Vector3)
extern "C" void NavMeshAgent_set_destination_m3136 (NavMeshAgent_t307 * __this, Vector3_t215  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_destination(UnityEngine.Vector3&)
extern "C" void NavMeshAgent_INTERNAL_set_destination_m5768 (NavMeshAgent_t307 * __this, Vector3_t215 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshAgent::set_updateRotation(System.Boolean)
extern "C" void NavMeshAgent_set_updateRotation_m3208 (NavMeshAgent_t307 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
