﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t632;

// System.Void UnityEngine.UI.Button/ButtonClickedEvent::.ctor()
extern "C" void ButtonClickedEvent__ctor_m3670 (ButtonClickedEvent_t632 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
