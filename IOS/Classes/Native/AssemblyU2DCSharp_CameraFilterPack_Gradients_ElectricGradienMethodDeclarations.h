﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_ElectricGradient
struct CameraFilterPack_Gradients_ElectricGradient_t147;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_ElectricGradient::.ctor()
extern "C" void CameraFilterPack_Gradients_ElectricGradient__ctor_m953 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_ElectricGradient::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_ElectricGradient_get_material_m954 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::Start()
extern "C" void CameraFilterPack_Gradients_ElectricGradient_Start_m955 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_ElectricGradient_OnRenderImage_m956 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::Update()
extern "C" void CameraFilterPack_Gradients_ElectricGradient_Update_m957 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_ElectricGradient::OnDisable()
extern "C" void CameraFilterPack_Gradients_ElectricGradient_OnDisable_m958 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
