﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Flush
struct CameraFilterPack_Distortion_Flush_t87;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Flush::.ctor()
extern "C" void CameraFilterPack_Distortion_Flush__ctor_m546 (CameraFilterPack_Distortion_Flush_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Flush::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Flush_get_material_m547 (CameraFilterPack_Distortion_Flush_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::Start()
extern "C" void CameraFilterPack_Distortion_Flush_Start_m548 (CameraFilterPack_Distortion_Flush_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Flush_OnRenderImage_m549 (CameraFilterPack_Distortion_Flush_t87 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::OnValidate()
extern "C" void CameraFilterPack_Distortion_Flush_OnValidate_m550 (CameraFilterPack_Distortion_Flush_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::Update()
extern "C" void CameraFilterPack_Distortion_Flush_Update_m551 (CameraFilterPack_Distortion_Flush_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flush::OnDisable()
extern "C" void CameraFilterPack_Distortion_Flush_OnDisable_m552 (CameraFilterPack_Distortion_Flush_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
