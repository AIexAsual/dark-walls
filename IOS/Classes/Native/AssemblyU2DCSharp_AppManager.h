﻿#pragma once
#include <stdint.h>
// AppManager
struct AppManager_t350;
// AppManager/OnStateChangeHandler
struct OnStateChangeHandler_t349;
// System.Object
#include "mscorlib_System_Object.h"
// AppEvent
#include "AssemblyU2DCSharp_AppEvent.h"
// AppManager
struct  AppManager_t350  : public Object_t
{
	// AppEvent AppManager::gameState
	int32_t ___gameState_1;
	// AppManager/OnStateChangeHandler AppManager::OnStateChangeEvent
	OnStateChangeHandler_t349 * ___OnStateChangeEvent_2;
};
struct AppManager_t350_StaticFields{
	// AppManager AppManager::_instance
	AppManager_t350 * ____instance_0;
};
