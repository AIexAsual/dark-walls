﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t831;
// UnityEngine.Object
struct Object_t473;
struct Object_t473_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1077;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m4972 (AssetBundleRequest_t831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t473 * AssetBundleRequest_get_asset_m4973 (AssetBundleRequest_t831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t1077* AssetBundleRequest_get_allAssets_m4974 (AssetBundleRequest_t831 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
