﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Text_t212_0_0_0;
static const Il2CppType* GenInst_Text_t212_0_0_0_Types[] = { &Text_t212_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t212_0_0_0 = { 1, GenInst_Text_t212_0_0_0_Types };
extern const Il2CppType Renderer_t312_0_0_0;
static const Il2CppType* GenInst_Renderer_t312_0_0_0_Types[] = { &Renderer_t312_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t312_0_0_0 = { 1, GenInst_Renderer_t312_0_0_0_Types };
extern const Il2CppType CardboardOnGUIWindow_t224_0_0_0;
static const Il2CppType* GenInst_CardboardOnGUIWindow_t224_0_0_0_Types[] = { &CardboardOnGUIWindow_t224_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardOnGUIWindow_t224_0_0_0 = { 1, GenInst_CardboardOnGUIWindow_t224_0_0_0_Types };
extern const Il2CppType CardboardOnGUIMouse_t222_0_0_0;
static const Il2CppType* GenInst_CardboardOnGUIMouse_t222_0_0_0_Types[] = { &CardboardOnGUIMouse_t222_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardOnGUIMouse_t222_0_0_0 = { 1, GenInst_CardboardOnGUIMouse_t222_0_0_0_Types };
extern const Il2CppType Collider_t319_0_0_0;
static const Il2CppType* GenInst_Collider_t319_0_0_0_Types[] = { &Collider_t319_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t319_0_0_0 = { 1, GenInst_Collider_t319_0_0_0_Types };
extern const Il2CppType MeshRenderer_t223_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t223_0_0_0_Types[] = { &MeshRenderer_t223_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t223_0_0_0 = { 1, GenInst_MeshRenderer_t223_0_0_0_Types };
extern const Il2CppType Cardboard_t233_0_0_0;
static const Il2CppType* GenInst_Cardboard_t233_0_0_0_Types[] = { &Cardboard_t233_0_0_0 };
extern const Il2CppGenericInst GenInst_Cardboard_t233_0_0_0 = { 1, GenInst_Cardboard_t233_0_0_0_Types };
extern const Il2CppType StereoController_t235_0_0_0;
static const Il2CppType* GenInst_StereoController_t235_0_0_0_Types[] = { &StereoController_t235_0_0_0 };
extern const Il2CppGenericInst GenInst_StereoController_t235_0_0_0 = { 1, GenInst_StereoController_t235_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType CardboardPreRender_t247_0_0_0;
static const Il2CppType* GenInst_CardboardPreRender_t247_0_0_0_Types[] = { &CardboardPreRender_t247_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardPreRender_t247_0_0_0 = { 1, GenInst_CardboardPreRender_t247_0_0_0_Types };
extern const Il2CppType CardboardPostRender_t246_0_0_0;
static const Il2CppType* GenInst_CardboardPostRender_t246_0_0_0_Types[] = { &CardboardPostRender_t246_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardPostRender_t246_0_0_0 = { 1, GenInst_CardboardPostRender_t246_0_0_0_Types };
extern const Il2CppType CardboardHead_t244_0_0_0;
static const Il2CppType* GenInst_CardboardHead_t244_0_0_0_Types[] = { &CardboardHead_t244_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardHead_t244_0_0_0 = { 1, GenInst_CardboardHead_t244_0_0_0_Types };
extern const Il2CppType Camera_t14_0_0_0;
static const Il2CppType* GenInst_Camera_t14_0_0_0_Types[] = { &Camera_t14_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t14_0_0_0 = { 1, GenInst_Camera_t14_0_0_0_Types };
extern const Il2CppType StereoRenderEffect_t239_0_0_0;
static const Il2CppType* GenInst_StereoRenderEffect_t239_0_0_0_Types[] = { &StereoRenderEffect_t239_0_0_0 };
extern const Il2CppGenericInst GenInst_StereoRenderEffect_t239_0_0_0 = { 1, GenInst_StereoRenderEffect_t239_0_0_0_Types };
extern const Il2CppType ISelectHandler_t487_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t487_0_0_0_Types[] = { &ISelectHandler_t487_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t487_0_0_0 = { 1, GenInst_ISelectHandler_t487_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t489_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t489_0_0_0_Types[] = { &IUpdateSelectedHandler_t489_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t489_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t489_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t493_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t493_0_0_0_Types[] = { &IBeginDragHandler_t493_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t493_0_0_0 = { 1, GenInst_IBeginDragHandler_t493_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t495_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t495_0_0_0_Types[] = { &IPointerUpHandler_t495_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t495_0_0_0 = { 1, GenInst_IPointerUpHandler_t495_0_0_0_Types };
extern const Il2CppType IDragHandler_t497_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t497_0_0_0_Types[] = { &IDragHandler_t497_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t497_0_0_0 = { 1, GenInst_IDragHandler_t497_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t499_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t499_0_0_0_Types[] = { &IPointerClickHandler_t499_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t499_0_0_0 = { 1, GenInst_IPointerClickHandler_t499_0_0_0_Types };
extern const Il2CppType IDropHandler_t501_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t501_0_0_0_Types[] = { &IDropHandler_t501_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t501_0_0_0 = { 1, GenInst_IDropHandler_t501_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t503_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t503_0_0_0_Types[] = { &IEndDragHandler_t503_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t503_0_0_0 = { 1, GenInst_IEndDragHandler_t503_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t505_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t505_0_0_0_Types[] = { &IPointerDownHandler_t505_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t505_0_0_0 = { 1, GenInst_IPointerDownHandler_t505_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t507_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t507_0_0_0_Types[] = { &IInitializePotentialDragHandler_t507_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t507_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t507_0_0_0_Types };
extern const Il2CppType CardboardEye_t240_0_0_0;
static const Il2CppType* GenInst_CardboardEye_t240_0_0_0_Types[] = { &CardboardEye_t240_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardEye_t240_0_0_0 = { 1, GenInst_CardboardEye_t240_0_0_0_Types };
extern const Il2CppType Boolean_t536_0_0_0;
static const Il2CppType* GenInst_CardboardEye_t240_0_0_0_Boolean_t536_0_0_0_Types[] = { &CardboardEye_t240_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardEye_t240_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_CardboardEye_t240_0_0_0_Boolean_t536_0_0_0_Types };
static const Il2CppType* GenInst_CardboardEye_t240_0_0_0_CardboardHead_t244_0_0_0_Types[] = { &CardboardEye_t240_0_0_0, &CardboardHead_t244_0_0_0 };
extern const Il2CppGenericInst GenInst_CardboardEye_t240_0_0_0_CardboardHead_t244_0_0_0 = { 2, GenInst_CardboardEye_t240_0_0_0_CardboardHead_t244_0_0_0_Types };
extern const Il2CppType Int32_t478_0_0_0;
static const Il2CppType* GenInst_Int32_t478_0_0_0_Types[] = { &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0 = { 1, GenInst_Int32_t478_0_0_0_Types };
extern const Il2CppType MNDialogResult_t282_0_0_0;
static const Il2CppType* GenInst_MNDialogResult_t282_0_0_0_Types[] = { &MNDialogResult_t282_0_0_0 };
extern const Il2CppGenericInst GenInst_MNDialogResult_t282_0_0_0 = { 1, GenInst_MNDialogResult_t282_0_0_0_Types };
extern const Il2CppType MNAndroidDialog_t285_0_0_0;
static const Il2CppType* GenInst_MNAndroidDialog_t285_0_0_0_Types[] = { &MNAndroidDialog_t285_0_0_0 };
extern const Il2CppGenericInst GenInst_MNAndroidDialog_t285_0_0_0 = { 1, GenInst_MNAndroidDialog_t285_0_0_0_Types };
extern const Il2CppType MNAndroidMessage_t286_0_0_0;
static const Il2CppType* GenInst_MNAndroidMessage_t286_0_0_0_Types[] = { &MNAndroidMessage_t286_0_0_0 };
extern const Il2CppGenericInst GenInst_MNAndroidMessage_t286_0_0_0 = { 1, GenInst_MNAndroidMessage_t286_0_0_0_Types };
extern const Il2CppType MNAndroidRateUsPopUp_t287_0_0_0;
static const Il2CppType* GenInst_MNAndroidRateUsPopUp_t287_0_0_0_Types[] = { &MNAndroidRateUsPopUp_t287_0_0_0 };
extern const Il2CppGenericInst GenInst_MNAndroidRateUsPopUp_t287_0_0_0 = { 1, GenInst_MNAndroidRateUsPopUp_t287_0_0_0_Types };
extern const Il2CppType MNIOSDialog_t288_0_0_0;
static const Il2CppType* GenInst_MNIOSDialog_t288_0_0_0_Types[] = { &MNIOSDialog_t288_0_0_0 };
extern const Il2CppGenericInst GenInst_MNIOSDialog_t288_0_0_0 = { 1, GenInst_MNIOSDialog_t288_0_0_0_Types };
extern const Il2CppType MNIOSMessage_t289_0_0_0;
static const Il2CppType* GenInst_MNIOSMessage_t289_0_0_0_Types[] = { &MNIOSMessage_t289_0_0_0 };
extern const Il2CppGenericInst GenInst_MNIOSMessage_t289_0_0_0 = { 1, GenInst_MNIOSMessage_t289_0_0_0_Types };
extern const Il2CppType MNIOSRateUsPopUp_t290_0_0_0;
static const Il2CppType* GenInst_MNIOSRateUsPopUp_t290_0_0_0_Types[] = { &MNIOSRateUsPopUp_t290_0_0_0 };
extern const Il2CppGenericInst GenInst_MNIOSRateUsPopUp_t290_0_0_0 = { 1, GenInst_MNIOSRateUsPopUp_t290_0_0_0_Types };
extern const Il2CppType MNWP8Dialog_t292_0_0_0;
static const Il2CppType* GenInst_MNWP8Dialog_t292_0_0_0_Types[] = { &MNWP8Dialog_t292_0_0_0 };
extern const Il2CppGenericInst GenInst_MNWP8Dialog_t292_0_0_0 = { 1, GenInst_MNWP8Dialog_t292_0_0_0_Types };
extern const Il2CppType MNWP8Message_t293_0_0_0;
static const Il2CppType* GenInst_MNWP8Message_t293_0_0_0_Types[] = { &MNWP8Message_t293_0_0_0 };
extern const Il2CppGenericInst GenInst_MNWP8Message_t293_0_0_0 = { 1, GenInst_MNWP8Message_t293_0_0_0_Types };
extern const Il2CppType MNWP8RateUsPopUp_t294_0_0_0;
static const Il2CppType* GenInst_MNWP8RateUsPopUp_t294_0_0_0_Types[] = { &MNWP8RateUsPopUp_t294_0_0_0 };
extern const Il2CppGenericInst GenInst_MNWP8RateUsPopUp_t294_0_0_0 = { 1, GenInst_MNWP8RateUsPopUp_t294_0_0_0_Types };
extern const Il2CppType Char_t526_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Char_t526_0_0_0_Types[] = { &String_t_0_0_0, &Char_t526_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Char_t526_0_0_0 = { 2, GenInst_String_t_0_0_0_Char_t526_0_0_0_Types };
static const Il2CppType* GenInst_Char_t526_0_0_0_Types[] = { &Char_t526_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t526_0_0_0 = { 1, GenInst_Char_t526_0_0_0_Types };
extern const Il2CppType Texture2D_t9_0_0_0;
static const Il2CppType* GenInst_Texture2D_t9_0_0_0_Types[] = { &Texture2D_t9_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t9_0_0_0 = { 1, GenInst_Texture2D_t9_0_0_0_Types };
extern const Il2CppType SA_ScreenShotMaker_t300_0_0_0;
static const Il2CppType* GenInst_SA_ScreenShotMaker_t300_0_0_0_Types[] = { &SA_ScreenShotMaker_t300_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_ScreenShotMaker_t300_0_0_0 = { 1, GenInst_SA_ScreenShotMaker_t300_0_0_0_Types };
extern const Il2CppType WWWTextureLoader_t305_0_0_0;
static const Il2CppType* GenInst_WWWTextureLoader_t305_0_0_0_Types[] = { &WWWTextureLoader_t305_0_0_0 };
extern const Il2CppGenericInst GenInst_WWWTextureLoader_t305_0_0_0 = { 1, GenInst_WWWTextureLoader_t305_0_0_0_Types };
extern const Il2CppType NavMeshAgent_t307_0_0_0;
static const Il2CppType* GenInst_NavMeshAgent_t307_0_0_0_Types[] = { &NavMeshAgent_t307_0_0_0 };
extern const Il2CppGenericInst GenInst_NavMeshAgent_t307_0_0_0 = { 1, GenInst_NavMeshAgent_t307_0_0_0_Types };
extern const Il2CppType AudioSource_t339_0_0_0;
static const Il2CppType* GenInst_AudioSource_t339_0_0_0_Types[] = { &AudioSource_t339_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t339_0_0_0 = { 1, GenInst_AudioSource_t339_0_0_0_Types };
extern const Il2CppType Rigidbody_t325_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t325_0_0_0_Types[] = { &Rigidbody_t325_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t325_0_0_0 = { 1, GenInst_Rigidbody_t325_0_0_0_Types };
extern const Il2CppType GoTo_t315_0_0_0;
static const Il2CppType* GenInst_GoTo_t315_0_0_0_Types[] = { &GoTo_t315_0_0_0 };
extern const Il2CppGenericInst GenInst_GoTo_t315_0_0_0 = { 1, GenInst_GoTo_t315_0_0_0_Types };
extern const Il2CppType Light_t318_0_0_0;
static const Il2CppType* GenInst_Light_t318_0_0_0_Types[] = { &Light_t318_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t318_0_0_0 = { 1, GenInst_Light_t318_0_0_0_Types };
extern const Il2CppType Animator_t321_0_0_0;
static const Il2CppType* GenInst_Animator_t321_0_0_0_Types[] = { &Animator_t321_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t321_0_0_0 = { 1, GenInst_Animator_t321_0_0_0_Types };
extern const Il2CppType AICharacterControl_t308_0_0_0;
static const Il2CppType* GenInst_AICharacterControl_t308_0_0_0_Types[] = { &AICharacterControl_t308_0_0_0 };
extern const Il2CppGenericInst GenInst_AICharacterControl_t308_0_0_0 = { 1, GenInst_AICharacterControl_t308_0_0_0_Types };
extern const Il2CppType SkinnedMeshRenderer_t532_0_0_0;
static const Il2CppType* GenInst_SkinnedMeshRenderer_t532_0_0_0_Types[] = { &SkinnedMeshRenderer_t532_0_0_0 };
extern const Il2CppGenericInst GenInst_SkinnedMeshRenderer_t532_0_0_0 = { 1, GenInst_SkinnedMeshRenderer_t532_0_0_0_Types };
extern const Il2CppType ParticleSystem_t534_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t534_0_0_0_Types[] = { &ParticleSystem_t534_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t534_0_0_0 = { 1, GenInst_ParticleSystem_t534_0_0_0_Types };
extern const Il2CppType Cloth_t337_0_0_0;
static const Il2CppType* GenInst_Cloth_t337_0_0_0_Types[] = { &Cloth_t337_0_0_0 };
extern const Il2CppGenericInst GenInst_Cloth_t337_0_0_0 = { 1, GenInst_Cloth_t337_0_0_0_Types };
extern const Il2CppType InteractiveObject_t328_0_0_0;
static const Il2CppType* GenInst_InteractiveObject_t328_0_0_0_Types[] = { &InteractiveObject_t328_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveObject_t328_0_0_0 = { 1, GenInst_InteractiveObject_t328_0_0_0_Types };
extern const Il2CppType CameraFilterPack_FX_Drunk_t124_0_0_0;
static const Il2CppType* GenInst_CameraFilterPack_FX_Drunk_t124_0_0_0_Types[] = { &CameraFilterPack_FX_Drunk_t124_0_0_0 };
extern const Il2CppGenericInst GenInst_CameraFilterPack_FX_Drunk_t124_0_0_0 = { 1, GenInst_CameraFilterPack_FX_Drunk_t124_0_0_0_Types };
extern const Il2CppType GameObject_t256_0_0_0;
static const Il2CppType* GenInst_GameObject_t256_0_0_0_Types[] = { &GameObject_t256_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t256_0_0_0 = { 1, GenInst_GameObject_t256_0_0_0_Types };
extern const Il2CppType InteractiveMama_t330_0_0_0;
static const Il2CppType* GenInst_InteractiveMama_t330_0_0_0_Types[] = { &InteractiveMama_t330_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveMama_t330_0_0_0 = { 1, GenInst_InteractiveMama_t330_0_0_0_Types };
extern const Il2CppType InteractiveParticles_t334_0_0_0;
static const Il2CppType* GenInst_InteractiveParticles_t334_0_0_0_Types[] = { &InteractiveParticles_t334_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveParticles_t334_0_0_0 = { 1, GenInst_InteractiveParticles_t334_0_0_0_Types };
extern const Il2CppType InteractiveRadio_t340_0_0_0;
static const Il2CppType* GenInst_InteractiveRadio_t340_0_0_0_Types[] = { &InteractiveRadio_t340_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveRadio_t340_0_0_0 = { 1, GenInst_InteractiveRadio_t340_0_0_0_Types };
extern const Il2CppType Teddy_t367_0_0_0;
static const Il2CppType* GenInst_Teddy_t367_0_0_0_Types[] = { &Teddy_t367_0_0_0 };
extern const Il2CppGenericInst GenInst_Teddy_t367_0_0_0 = { 1, GenInst_Teddy_t367_0_0_0_Types };
extern const Il2CppType InteractiveBaby_t324_0_0_0;
static const Il2CppType* GenInst_InteractiveBaby_t324_0_0_0_Types[] = { &InteractiveBaby_t324_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveBaby_t324_0_0_0 = { 1, GenInst_InteractiveBaby_t324_0_0_0_Types };
extern const Il2CppType InteractivePlasticBag_t335_0_0_0;
static const Il2CppType* GenInst_InteractivePlasticBag_t335_0_0_0_Types[] = { &InteractivePlasticBag_t335_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractivePlasticBag_t335_0_0_0 = { 1, GenInst_InteractivePlasticBag_t335_0_0_0_Types };
extern const Il2CppType InteractiveDroppingObject_t327_0_0_0;
static const Il2CppType* GenInst_InteractiveDroppingObject_t327_0_0_0_Types[] = { &InteractiveDroppingObject_t327_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveDroppingObject_t327_0_0_0 = { 1, GenInst_InteractiveDroppingObject_t327_0_0_0_Types };
extern const Il2CppType InteractiveBurnMama_t326_0_0_0;
static const Il2CppType* GenInst_InteractiveBurnMama_t326_0_0_0_Types[] = { &InteractiveBurnMama_t326_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveBurnMama_t326_0_0_0 = { 1, GenInst_InteractiveBurnMama_t326_0_0_0_Types };
extern const Il2CppType FadeTexture_t313_0_0_0;
static const Il2CppType* GenInst_FadeTexture_t313_0_0_0_Types[] = { &FadeTexture_t313_0_0_0 };
extern const Il2CppGenericInst GenInst_FadeTexture_t313_0_0_0 = { 1, GenInst_FadeTexture_t313_0_0_0_Types };
extern const Il2CppType TextAppearance_t368_0_0_0;
static const Il2CppType* GenInst_TextAppearance_t368_0_0_0_Types[] = { &TextAppearance_t368_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAppearance_t368_0_0_0 = { 1, GenInst_TextAppearance_t368_0_0_0_Types };
extern const Il2CppType InteractiveFlashlight_t329_0_0_0;
static const Il2CppType* GenInst_InteractiveFlashlight_t329_0_0_0_Types[] = { &InteractiveFlashlight_t329_0_0_0 };
extern const Il2CppGenericInst GenInst_InteractiveFlashlight_t329_0_0_0 = { 1, GenInst_InteractiveFlashlight_t329_0_0_0_Types };
extern const Il2CppType LightController_t343_0_0_0;
static const Il2CppType* GenInst_LightController_t343_0_0_0_Types[] = { &LightController_t343_0_0_0 };
extern const Il2CppGenericInst GenInst_LightController_t343_0_0_0 = { 1, GenInst_LightController_t343_0_0_0_Types };
extern const Il2CppType MovingObjects_t375_0_0_0;
static const Il2CppType* GenInst_MovingObjects_t375_0_0_0_Types[] = { &MovingObjects_t375_0_0_0 };
extern const Il2CppGenericInst GenInst_MovingObjects_t375_0_0_0 = { 1, GenInst_MovingObjects_t375_0_0_0_Types };
extern const Il2CppType DoorController_t311_0_0_0;
static const Il2CppType* GenInst_DoorController_t311_0_0_0_Types[] = { &DoorController_t311_0_0_0 };
extern const Il2CppGenericInst GenInst_DoorController_t311_0_0_0 = { 1, GenInst_DoorController_t311_0_0_0_Types };
extern const Il2CppType SceneLoader_t382_0_0_0;
static const Il2CppType* GenInst_SceneLoader_t382_0_0_0_Types[] = { &SceneLoader_t382_0_0_0 };
extern const Il2CppGenericInst GenInst_SceneLoader_t382_0_0_0 = { 1, GenInst_SceneLoader_t382_0_0_0_Types };
extern const Il2CppType SFX_t385_0_0_0;
static const Il2CppType* GenInst_SFX_t385_0_0_0_Types[] = { &SFX_t385_0_0_0 };
extern const Il2CppGenericInst GenInst_SFX_t385_0_0_0 = { 1, GenInst_SFX_t385_0_0_0_Types };
extern const Il2CppType PlayerController_t356_0_0_0;
static const Il2CppType* GenInst_PlayerController_t356_0_0_0_Types[] = { &PlayerController_t356_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerController_t356_0_0_0 = { 1, GenInst_PlayerController_t356_0_0_0_Types };
extern const Il2CppType TextMesh_t397_0_0_0;
static const Il2CppType* GenInst_TextMesh_t397_0_0_0_Types[] = { &TextMesh_t397_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMesh_t397_0_0_0 = { 1, GenInst_TextMesh_t397_0_0_0_Types };
extern const Il2CppType Loader_t346_0_0_0;
static const Il2CppType* GenInst_Loader_t346_0_0_0_Types[] = { &Loader_t346_0_0_0 };
extern const Il2CppGenericInst GenInst_Loader_t346_0_0_0 = { 1, GenInst_Loader_t346_0_0_0_Types };
extern const Il2CppType BoxCollider_t535_0_0_0;
static const Il2CppType* GenInst_BoxCollider_t535_0_0_0_Types[] = { &BoxCollider_t535_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider_t535_0_0_0 = { 1, GenInst_BoxCollider_t535_0_0_0_Types };
extern const Il2CppType BodyPart_t400_0_0_0;
static const Il2CppType* GenInst_BodyPart_t400_0_0_0_Types[] = { &BodyPart_t400_0_0_0 };
extern const Il2CppGenericInst GenInst_BodyPart_t400_0_0_0 = { 1, GenInst_BodyPart_t400_0_0_0_Types };
extern const Il2CppType RagdollPartScript_t404_0_0_0;
static const Il2CppType* GenInst_RagdollPartScript_t404_0_0_0_Types[] = { &RagdollPartScript_t404_0_0_0 };
extern const Il2CppGenericInst GenInst_RagdollPartScript_t404_0_0_0 = { 1, GenInst_RagdollPartScript_t404_0_0_0_Types };
extern const Il2CppType RagdollHelper_t402_0_0_0;
static const Il2CppType* GenInst_RagdollHelper_t402_0_0_0_Types[] = { &RagdollHelper_t402_0_0_0 };
extern const Il2CppGenericInst GenInst_RagdollHelper_t402_0_0_0 = { 1, GenInst_RagdollHelper_t402_0_0_0_Types };
extern const Il2CppType TweenType_t442_0_0_0;
extern const Il2CppType Dictionary_2_t545_0_0_0;
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0_Types[] = { &TweenType_t442_0_0_0, &Dictionary_2_t545_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0 = { 2, GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Type_t_0_0_0_Types };
extern const Il2CppType GUITexture_t548_0_0_0;
static const Il2CppType* GenInst_GUITexture_t548_0_0_0_Types[] = { &GUITexture_t548_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t548_0_0_0 = { 1, GenInst_GUITexture_t548_0_0_0_Types };
extern const Il2CppType GUIText_t549_0_0_0;
static const Il2CppType* GenInst_GUIText_t549_0_0_0_Types[] = { &GUIText_t549_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t549_0_0_0 = { 1, GenInst_GUIText_t549_0_0_0_Types };
extern const Il2CppType Vector3_t215_0_0_0;
static const Il2CppType* GenInst_Vector3_t215_0_0_0_Types[] = { &Vector3_t215_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t215_0_0_0 = { 1, GenInst_Vector3_t215_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t478_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t478_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t478_0_0_0_Types };
extern const Il2CppType iTween_t432_0_0_0;
static const Il2CppType* GenInst_iTween_t432_0_0_0_Types[] = { &iTween_t432_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t432_0_0_0 = { 1, GenInst_iTween_t432_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType iTweenEvent_t443_0_0_0;
static const Il2CppType* GenInst_iTweenEvent_t443_0_0_0_Types[] = { &iTweenEvent_t443_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenEvent_t443_0_0_0 = { 1, GenInst_iTweenEvent_t443_0_0_0_Types };
static const Il2CppType* GenInst_iTweenEvent_t443_0_0_0_Boolean_t536_0_0_0_Types[] = { &iTweenEvent_t443_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenEvent_t443_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_iTweenEvent_t443_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType Single_t531_0_0_0;
static const Il2CppType* GenInst_Single_t531_0_0_0_Types[] = { &Single_t531_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t531_0_0_0 = { 1, GenInst_Single_t531_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t536_0_0_0_Types[] = { &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t536_0_0_0 = { 1, GenInst_Boolean_t536_0_0_0_Types };
extern const Il2CppType Color_t6_0_0_0;
static const Il2CppType* GenInst_Color_t6_0_0_0_Types[] = { &Color_t6_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t6_0_0_0 = { 1, GenInst_Color_t6_0_0_0_Types };
extern const Il2CppType Space_t546_0_0_0;
static const Il2CppType* GenInst_Space_t546_0_0_0_Types[] = { &Space_t546_0_0_0 };
extern const Il2CppGenericInst GenInst_Space_t546_0_0_0 = { 1, GenInst_Space_t546_0_0_0_Types };
extern const Il2CppType EaseType_t425_0_0_0;
static const Il2CppType* GenInst_EaseType_t425_0_0_0_Types[] = { &EaseType_t425_0_0_0 };
extern const Il2CppGenericInst GenInst_EaseType_t425_0_0_0 = { 1, GenInst_EaseType_t425_0_0_0_Types };
extern const Il2CppType LoopType_t426_0_0_0;
static const Il2CppType* GenInst_LoopType_t426_0_0_0_Types[] = { &LoopType_t426_0_0_0 };
extern const Il2CppGenericInst GenInst_LoopType_t426_0_0_0 = { 1, GenInst_LoopType_t426_0_0_0_Types };
extern const Il2CppType Transform_t243_0_0_0;
static const Il2CppType* GenInst_Transform_t243_0_0_0_Types[] = { &Transform_t243_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t243_0_0_0 = { 1, GenInst_Transform_t243_0_0_0_Types };
extern const Il2CppType AudioClip_t472_0_0_0;
static const Il2CppType* GenInst_AudioClip_t472_0_0_0_Types[] = { &AudioClip_t472_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioClip_t472_0_0_0 = { 1, GenInst_AudioClip_t472_0_0_0_Types };
extern const Il2CppType ArrayIndexes_t441_0_0_0;
static const Il2CppType* GenInst_ArrayIndexes_t441_0_0_0_Types[] = { &ArrayIndexes_t441_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayIndexes_t441_0_0_0 = { 1, GenInst_ArrayIndexes_t441_0_0_0_Types };
extern const Il2CppType iTweenPath_t459_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0_Types[] = { &String_t_0_0_0, &iTweenPath_t459_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0 = { 2, GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0_Types };
extern const Il2CppType BaseInputModule_t259_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t259_0_0_0_Types[] = { &BaseInputModule_t259_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t259_0_0_0 = { 1, GenInst_BaseInputModule_t259_0_0_0_Types };
extern const Il2CppType RaycastResult_t511_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t511_0_0_0_Types[] = { &RaycastResult_t511_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t511_0_0_0 = { 1, GenInst_RaycastResult_t511_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t763_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t763_0_0_0_Types[] = { &IDeselectHandler_t763_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t763_0_0_0 = { 1, GenInst_IDeselectHandler_t763_0_0_0_Types };
extern const Il2CppType BaseEventData_t490_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t490_0_0_0_Types[] = { &BaseEventData_t490_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t490_0_0_0 = { 1, GenInst_BaseEventData_t490_0_0_0_Types };
extern const Il2CppType Entry_t594_0_0_0;
static const Il2CppType* GenInst_Entry_t594_0_0_0_Types[] = { &Entry_t594_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t594_0_0_0 = { 1, GenInst_Entry_t594_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t760_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t760_0_0_0_Types[] = { &IPointerEnterHandler_t760_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t760_0_0_0 = { 1, GenInst_IPointerEnterHandler_t760_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t761_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t761_0_0_0_Types[] = { &IPointerExitHandler_t761_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t761_0_0_0 = { 1, GenInst_IPointerExitHandler_t761_0_0_0_Types };
extern const Il2CppType IScrollHandler_t762_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t762_0_0_0_Types[] = { &IScrollHandler_t762_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t762_0_0_0 = { 1, GenInst_IScrollHandler_t762_0_0_0_Types };
extern const Il2CppType IMoveHandler_t764_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t764_0_0_0_Types[] = { &IMoveHandler_t764_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t764_0_0_0 = { 1, GenInst_IMoveHandler_t764_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t765_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t765_0_0_0_Types[] = { &ISubmitHandler_t765_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t765_0_0_0 = { 1, GenInst_ISubmitHandler_t765_0_0_0_Types };
extern const Il2CppType ICancelHandler_t766_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t766_0_0_0_Types[] = { &ICancelHandler_t766_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t766_0_0_0 = { 1, GenInst_ICancelHandler_t766_0_0_0_Types };
extern const Il2CppType List_1_t767_0_0_0;
static const Il2CppType* GenInst_List_1_t767_0_0_0_Types[] = { &List_1_t767_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t767_0_0_0 = { 1, GenInst_List_1_t767_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t2261_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t2261_0_0_0_Types[] = { &IEventSystemHandler_t2261_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2261_0_0_0 = { 1, GenInst_IEventSystemHandler_t2261_0_0_0_Types };
extern const Il2CppType PointerEventData_t257_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t257_0_0_0_Types[] = { &PointerEventData_t257_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t257_0_0_0 = { 1, GenInst_PointerEventData_t257_0_0_0_Types };
extern const Il2CppType AxisEventData_t612_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t612_0_0_0_Types[] = { &AxisEventData_t612_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t612_0_0_0 = { 1, GenInst_AxisEventData_t612_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t611_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t611_0_0_0_Types[] = { &BaseRaycaster_t611_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t611_0_0_0 = { 1, GenInst_BaseRaycaster_t611_0_0_0_Types };
extern const Il2CppType EventSystem_t509_0_0_0;
static const Il2CppType* GenInst_EventSystem_t509_0_0_0_Types[] = { &EventSystem_t509_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t509_0_0_0 = { 1, GenInst_EventSystem_t509_0_0_0_Types };
extern const Il2CppType ButtonState_t616_0_0_0;
static const Il2CppType* GenInst_ButtonState_t616_0_0_0_Types[] = { &ButtonState_t616_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t616_0_0_0 = { 1, GenInst_ButtonState_t616_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0_Types[] = { &Int32_t478_0_0_0, &PointerEventData_t257_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0 = { 2, GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t786_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t786_0_0_0_Types[] = { &SpriteRenderer_t786_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t786_0_0_0 = { 1, GenInst_SpriteRenderer_t786_0_0_0_Types };
extern const Il2CppType RaycastHit_t482_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t482_0_0_0_Types[] = { &RaycastHit_t482_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t482_0_0_0 = { 1, GenInst_RaycastHit_t482_0_0_0_Types };
extern const Il2CppType ICanvasElement_t770_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t770_0_0_0_Types[] = { &ICanvasElement_t770_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t770_0_0_0 = { 1, GenInst_ICanvasElement_t770_0_0_0_Types };
extern const Il2CppType Font_t643_0_0_0;
extern const Il2CppType List_1_t792_0_0_0;
static const Il2CppType* GenInst_Font_t643_0_0_0_List_1_t792_0_0_0_Types[] = { &Font_t643_0_0_0, &List_1_t792_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t643_0_0_0_List_1_t792_0_0_0 = { 2, GenInst_Font_t643_0_0_0_List_1_t792_0_0_0_Types };
static const Il2CppType* GenInst_Font_t643_0_0_0_Types[] = { &Font_t643_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t643_0_0_0 = { 1, GenInst_Font_t643_0_0_0_Types };
extern const Il2CppType ColorTween_t630_0_0_0;
static const Il2CppType* GenInst_ColorTween_t630_0_0_0_Types[] = { &ColorTween_t630_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t630_0_0_0 = { 1, GenInst_ColorTween_t630_0_0_0_Types };
extern const Il2CppType List_1_t687_0_0_0;
static const Il2CppType* GenInst_List_1_t687_0_0_0_Types[] = { &List_1_t687_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t687_0_0_0 = { 1, GenInst_List_1_t687_0_0_0_Types };
extern const Il2CppType UIVertex_t685_0_0_0;
static const Il2CppType* GenInst_UIVertex_t685_0_0_0_Types[] = { &UIVertex_t685_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t685_0_0_0 = { 1, GenInst_UIVertex_t685_0_0_0_Types };
extern const Il2CppType RectTransform_t648_0_0_0;
static const Il2CppType* GenInst_RectTransform_t648_0_0_0_Types[] = { &RectTransform_t648_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t648_0_0_0 = { 1, GenInst_RectTransform_t648_0_0_0_Types };
extern const Il2CppType Canvas_t650_0_0_0;
static const Il2CppType* GenInst_Canvas_t650_0_0_0_Types[] = { &Canvas_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t650_0_0_0 = { 1, GenInst_Canvas_t650_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t649_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t649_0_0_0_Types[] = { &CanvasRenderer_t649_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t649_0_0_0 = { 1, GenInst_CanvasRenderer_t649_0_0_0_Types };
extern const Il2CppType Component_t477_0_0_0;
static const Il2CppType* GenInst_Component_t477_0_0_0_Types[] = { &Component_t477_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t477_0_0_0 = { 1, GenInst_Component_t477_0_0_0_Types };
extern const Il2CppType Graphic_t654_0_0_0;
static const Il2CppType* GenInst_Graphic_t654_0_0_0_Types[] = { &Graphic_t654_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t654_0_0_0 = { 1, GenInst_Graphic_t654_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t803_0_0_0;
static const Il2CppType* GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0_Types[] = { &Canvas_t650_0_0_0, &IndexedSet_1_t803_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0 = { 2, GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0_Types };
extern const Il2CppType Sprite_t668_0_0_0;
static const Il2CppType* GenInst_Sprite_t668_0_0_0_Types[] = { &Sprite_t668_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t668_0_0_0 = { 1, GenInst_Sprite_t668_0_0_0_Types };
extern const Il2CppType Type_t661_0_0_0;
static const Il2CppType* GenInst_Type_t661_0_0_0_Types[] = { &Type_t661_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t661_0_0_0 = { 1, GenInst_Type_t661_0_0_0_Types };
extern const Il2CppType FillMethod_t662_0_0_0;
static const Il2CppType* GenInst_FillMethod_t662_0_0_0_Types[] = { &FillMethod_t662_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t662_0_0_0 = { 1, GenInst_FillMethod_t662_0_0_0_Types };
extern const Il2CppType SubmitEvent_t675_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t675_0_0_0_Types[] = { &SubmitEvent_t675_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t675_0_0_0 = { 1, GenInst_SubmitEvent_t675_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t677_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t677_0_0_0_Types[] = { &OnChangeEvent_t677_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t677_0_0_0 = { 1, GenInst_OnChangeEvent_t677_0_0_0_Types };
extern const Il2CppType OnValidateInput_t679_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t679_0_0_0_Types[] = { &OnValidateInput_t679_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t679_0_0_0 = { 1, GenInst_OnValidateInput_t679_0_0_0_Types };
extern const Il2CppType ContentType_t671_0_0_0;
static const Il2CppType* GenInst_ContentType_t671_0_0_0_Types[] = { &ContentType_t671_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t671_0_0_0 = { 1, GenInst_ContentType_t671_0_0_0_Types };
extern const Il2CppType LineType_t674_0_0_0;
static const Il2CppType* GenInst_LineType_t674_0_0_0_Types[] = { &LineType_t674_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t674_0_0_0 = { 1, GenInst_LineType_t674_0_0_0_Types };
extern const Il2CppType InputType_t672_0_0_0;
static const Il2CppType* GenInst_InputType_t672_0_0_0_Types[] = { &InputType_t672_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t672_0_0_0 = { 1, GenInst_InputType_t672_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t805_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t805_0_0_0_Types[] = { &TouchScreenKeyboardType_t805_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t805_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t805_0_0_0_Types };
extern const Il2CppType CharacterValidation_t673_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t673_0_0_0_Types[] = { &CharacterValidation_t673_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t673_0_0_0 = { 1, GenInst_CharacterValidation_t673_0_0_0_Types };
extern const Il2CppType UILineInfo_t809_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t809_0_0_0_Types[] = { &UILineInfo_t809_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t809_0_0_0 = { 1, GenInst_UILineInfo_t809_0_0_0_Types };
extern const Il2CppType UICharInfo_t811_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t811_0_0_0_Types[] = { &UICharInfo_t811_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t811_0_0_0 = { 1, GenInst_UICharInfo_t811_0_0_0_Types };
extern const Il2CppType LayoutElement_t740_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t740_0_0_0_Types[] = { &LayoutElement_t740_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t740_0_0_0 = { 1, GenInst_LayoutElement_t740_0_0_0_Types };
extern const Il2CppType Direction_t692_0_0_0;
static const Il2CppType* GenInst_Direction_t692_0_0_0_Types[] = { &Direction_t692_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t692_0_0_0 = { 1, GenInst_Direction_t692_0_0_0_Types };
extern const Il2CppType Vector2_t7_0_0_0;
static const Il2CppType* GenInst_Vector2_t7_0_0_0_Types[] = { &Vector2_t7_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t7_0_0_0 = { 1, GenInst_Vector2_t7_0_0_0_Types };
extern const Il2CppType CanvasGroup_t797_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t797_0_0_0_Types[] = { &CanvasGroup_t797_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t797_0_0_0 = { 1, GenInst_CanvasGroup_t797_0_0_0_Types };
extern const Il2CppType Selectable_t636_0_0_0;
static const Il2CppType* GenInst_Selectable_t636_0_0_0_Types[] = { &Selectable_t636_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t636_0_0_0 = { 1, GenInst_Selectable_t636_0_0_0_Types };
extern const Il2CppType Navigation_t690_0_0_0;
static const Il2CppType* GenInst_Navigation_t690_0_0_0_Types[] = { &Navigation_t690_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t690_0_0_0 = { 1, GenInst_Navigation_t690_0_0_0_Types };
extern const Il2CppType Transition_t704_0_0_0;
static const Il2CppType* GenInst_Transition_t704_0_0_0_Types[] = { &Transition_t704_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t704_0_0_0 = { 1, GenInst_Transition_t704_0_0_0_Types };
extern const Il2CppType ColorBlock_t642_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t642_0_0_0_Types[] = { &ColorBlock_t642_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t642_0_0_0 = { 1, GenInst_ColorBlock_t642_0_0_0_Types };
extern const Il2CppType SpriteState_t708_0_0_0;
static const Il2CppType* GenInst_SpriteState_t708_0_0_0_Types[] = { &SpriteState_t708_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t708_0_0_0 = { 1, GenInst_SpriteState_t708_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t631_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t631_0_0_0_Types[] = { &AnimationTriggers_t631_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t631_0_0_0 = { 1, GenInst_AnimationTriggers_t631_0_0_0_Types };
extern const Il2CppType Direction_t710_0_0_0;
static const Il2CppType* GenInst_Direction_t710_0_0_0_Types[] = { &Direction_t710_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t710_0_0_0 = { 1, GenInst_Direction_t710_0_0_0_Types };
extern const Il2CppType Image_t669_0_0_0;
static const Il2CppType* GenInst_Image_t669_0_0_0_Types[] = { &Image_t669_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t669_0_0_0 = { 1, GenInst_Image_t669_0_0_0_Types };
extern const Il2CppType MatEntry_t714_0_0_0;
static const Il2CppType* GenInst_MatEntry_t714_0_0_0_Types[] = { &MatEntry_t714_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t714_0_0_0 = { 1, GenInst_MatEntry_t714_0_0_0_Types };
extern const Il2CppType Toggle_t721_0_0_0;
static const Il2CppType* GenInst_Toggle_t721_0_0_0_Types[] = { &Toggle_t721_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t721_0_0_0 = { 1, GenInst_Toggle_t721_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t721_0_0_0_Boolean_t536_0_0_0_Types[] = { &Toggle_t721_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t721_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_Toggle_t721_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType AspectMode_t725_0_0_0;
static const Il2CppType* GenInst_AspectMode_t725_0_0_0_Types[] = { &AspectMode_t725_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t725_0_0_0 = { 1, GenInst_AspectMode_t725_0_0_0_Types };
extern const Il2CppType FitMode_t731_0_0_0;
static const Il2CppType* GenInst_FitMode_t731_0_0_0_Types[] = { &FitMode_t731_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t731_0_0_0 = { 1, GenInst_FitMode_t731_0_0_0_Types };
extern const Il2CppType Corner_t733_0_0_0;
static const Il2CppType* GenInst_Corner_t733_0_0_0_Types[] = { &Corner_t733_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t733_0_0_0 = { 1, GenInst_Corner_t733_0_0_0_Types };
extern const Il2CppType Axis_t734_0_0_0;
static const Il2CppType* GenInst_Axis_t734_0_0_0_Types[] = { &Axis_t734_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t734_0_0_0 = { 1, GenInst_Axis_t734_0_0_0_Types };
extern const Il2CppType Constraint_t735_0_0_0;
static const Il2CppType* GenInst_Constraint_t735_0_0_0_Types[] = { &Constraint_t735_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t735_0_0_0 = { 1, GenInst_Constraint_t735_0_0_0_Types };
extern const Il2CppType RectOffset_t741_0_0_0;
static const Il2CppType* GenInst_RectOffset_t741_0_0_0_Types[] = { &RectOffset_t741_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t741_0_0_0 = { 1, GenInst_RectOffset_t741_0_0_0_Types };
extern const Il2CppType TextAnchor_t819_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t819_0_0_0_Types[] = { &TextAnchor_t819_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t819_0_0_0 = { 1, GenInst_TextAnchor_t819_0_0_0_Types };
extern const Il2CppType ILayoutElement_t776_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t776_0_0_0_Single_t531_0_0_0_Types[] = { &ILayoutElement_t776_0_0_0, &Single_t531_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t776_0_0_0_Single_t531_0_0_0 = { 2, GenInst_ILayoutElement_t776_0_0_0_Single_t531_0_0_0_Types };
extern const Il2CppType List_1_t777_0_0_0;
static const Il2CppType* GenInst_List_1_t777_0_0_0_Types[] = { &List_1_t777_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t777_0_0_0 = { 1, GenInst_List_1_t777_0_0_0_Types };
extern const Il2CppType List_1_t775_0_0_0;
static const Il2CppType* GenInst_List_1_t775_0_0_0_Types[] = { &List_1_t775_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t775_0_0_0 = { 1, GenInst_List_1_t775_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t850_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t850_0_0_0_Types[] = { &GcLeaderboard_t850_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t850_0_0_0 = { 1, GenInst_GcLeaderboard_t850_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t1107_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t1107_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t1107_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1107_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t1107_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1109_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1109_0_0_0_Types[] = { &IAchievementU5BU5D_t1109_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1109_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1109_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t1037_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t1037_0_0_0_Types[] = { &IScoreU5BU5D_t1037_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t1037_0_0_0 = { 1, GenInst_IScoreU5BU5D_t1037_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t1031_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t1031_0_0_0_Types[] = { &IUserProfileU5BU5D_t1031_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t1031_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t1031_0_0_0_Types };
extern const Il2CppType LayoutCache_t874_0_0_0;
static const Il2CppType* GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0_Types[] = { &Int32_t478_0_0_0, &LayoutCache_t874_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0 = { 2, GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t877_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t877_0_0_0_Types[] = { &GUILayoutEntry_t877_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t877_0_0_0 = { 1, GenInst_GUILayoutEntry_t877_0_0_0_Types };
extern const Il2CppType GUIStyle_t273_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t273_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t469_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t469_0_0_0_Types[] = { &ByteU5BU5D_t469_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t469_0_0_0 = { 1, GenInst_ByteU5BU5D_t469_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t933_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t933_0_0_0_Types[] = { &Rigidbody2D_t933_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t933_0_0_0 = { 1, GenInst_Rigidbody2D_t933_0_0_0_Types };
extern const Il2CppType MatchDirectConnectInfo_t972_0_0_0;
static const Il2CppType* GenInst_MatchDirectConnectInfo_t972_0_0_0_Types[] = { &MatchDirectConnectInfo_t972_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchDirectConnectInfo_t972_0_0_0 = { 1, GenInst_MatchDirectConnectInfo_t972_0_0_0_Types };
extern const Il2CppType MatchDesc_t974_0_0_0;
static const Il2CppType* GenInst_MatchDesc_t974_0_0_0_Types[] = { &MatchDesc_t974_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchDesc_t974_0_0_0 = { 1, GenInst_MatchDesc_t974_0_0_0_Types };
extern const Il2CppType NetworkID_t979_0_0_0;
extern const Il2CppType NetworkAccessToken_t981_0_0_0;
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &NetworkAccessToken_t981_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0 = { 2, GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0_Types };
extern const Il2CppType CreateMatchResponse_t966_0_0_0;
static const Il2CppType* GenInst_CreateMatchResponse_t966_0_0_0_Types[] = { &CreateMatchResponse_t966_0_0_0 };
extern const Il2CppGenericInst GenInst_CreateMatchResponse_t966_0_0_0 = { 1, GenInst_CreateMatchResponse_t966_0_0_0_Types };
extern const Il2CppType JoinMatchResponse_t968_0_0_0;
static const Il2CppType* GenInst_JoinMatchResponse_t968_0_0_0_Types[] = { &JoinMatchResponse_t968_0_0_0 };
extern const Il2CppGenericInst GenInst_JoinMatchResponse_t968_0_0_0 = { 1, GenInst_JoinMatchResponse_t968_0_0_0_Types };
extern const Il2CppType BasicResponse_t963_0_0_0;
static const Il2CppType* GenInst_BasicResponse_t963_0_0_0_Types[] = { &BasicResponse_t963_0_0_0 };
extern const Il2CppGenericInst GenInst_BasicResponse_t963_0_0_0 = { 1, GenInst_BasicResponse_t963_0_0_0_Types };
extern const Il2CppType ListMatchResponse_t976_0_0_0;
static const Il2CppType* GenInst_ListMatchResponse_t976_0_0_0_Types[] = { &ListMatchResponse_t976_0_0_0 };
extern const Il2CppGenericInst GenInst_ListMatchResponse_t976_0_0_0 = { 1, GenInst_ListMatchResponse_t976_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t446_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t446_0_0_0_Types[] = { &KeyValuePair_2_t446_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t446_0_0_0 = { 1, GenInst_KeyValuePair_2_t446_0_0_0_Types };
extern const Il2CppType ConstructorDelegate_t995_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t995_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0 = { 2, GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0_Types };
extern const Il2CppType IDictionary_2_t1098_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t1098_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0_Types };
extern const Il2CppType GetDelegate_t993_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t993_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0 = { 2, GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0_Types };
extern const Il2CppType IDictionary_2_t1099_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t1099_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1147_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t1147_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0 = { 2, GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0_Types };
extern const Il2CppType SetDelegate_t994_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_SetDelegate_t994_0_0_0_Types[] = { &Type_t_0_0_0, &SetDelegate_t994_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t994_0_0_0 = { 2, GenInst_Type_t_0_0_0_SetDelegate_t994_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1149_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1149_0_0_0_Types[] = { &KeyValuePair_2_t1149_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1149_0_0_0 = { 1, GenInst_KeyValuePair_2_t1149_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t996_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t996_0_0_0_Types[] = { &ConstructorInfo_t996_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t996_0_0_0 = { 1, GenInst_ConstructorInfo_t996_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t862_0_0_0;
static const Il2CppType* GenInst_GUILayer_t862_0_0_0_Types[] = { &GUILayer_t862_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t862_0_0_0 = { 1, GenInst_GUILayer_t862_0_0_0_Types };
extern const Il2CppType PersistentCall_t1064_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t1064_0_0_0_Types[] = { &PersistentCall_t1064_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t1064_0_0_0 = { 1, GenInst_PersistentCall_t1064_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t1061_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t1061_0_0_0_Types[] = { &BaseInvokableCall_t1061_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t1061_0_0_0 = { 1, GenInst_BaseInvokableCall_t1061_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t536_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType StrongName_t2011_0_0_0;
static const Il2CppType* GenInst_StrongName_t2011_0_0_0_Types[] = { &StrongName_t2011_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t2011_0_0_0 = { 1, GenInst_StrongName_t2011_0_0_0_Types };
extern const Il2CppType DateTime_t871_0_0_0;
static const Il2CppType* GenInst_DateTime_t871_0_0_0_Types[] = { &DateTime_t871_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t871_0_0_0 = { 1, GenInst_DateTime_t871_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1148_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1148_0_0_0_Types[] = { &DateTimeOffset_t1148_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1148_0_0_0 = { 1, GenInst_DateTimeOffset_t1148_0_0_0_Types };
extern const Il2CppType TimeSpan_t1437_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t1437_0_0_0_Types[] = { &TimeSpan_t1437_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t1437_0_0_0 = { 1, GenInst_TimeSpan_t1437_0_0_0_Types };
extern const Il2CppType Guid_t555_0_0_0;
static const Il2CppType* GenInst_Guid_t555_0_0_0_Types[] = { &Guid_t555_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t555_0_0_0 = { 1, GenInst_Guid_t555_0_0_0_Types };
extern const Il2CppType IFormattable_t2190_0_0_0;
static const Il2CppType* GenInst_IFormattable_t2190_0_0_0_Types[] = { &IFormattable_t2190_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t2190_0_0_0 = { 1, GenInst_IFormattable_t2190_0_0_0_Types };
extern const Il2CppType IConvertible_t2193_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2193_0_0_0_Types[] = { &IConvertible_t2193_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2193_0_0_0 = { 1, GenInst_IConvertible_t2193_0_0_0_Types };
extern const Il2CppType IComparable_t2192_0_0_0;
static const Il2CppType* GenInst_IComparable_t2192_0_0_0_Types[] = { &IComparable_t2192_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t2192_0_0_0 = { 1, GenInst_IComparable_t2192_0_0_0_Types };
extern const Il2CppType IComparable_1_t3749_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3749_0_0_0_Types[] = { &IComparable_1_t3749_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3749_0_0_0 = { 1, GenInst_IComparable_1_t3749_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3750_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3750_0_0_0_Types[] = { &IEquatable_1_t3750_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3750_0_0_0 = { 1, GenInst_IEquatable_1_t3750_0_0_0_Types };
extern const Il2CppType ValueType_t1540_0_0_0;
static const Il2CppType* GenInst_ValueType_t1540_0_0_0_Types[] = { &ValueType_t1540_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1540_0_0_0 = { 1, GenInst_ValueType_t1540_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t4_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t4_0_0_0_Types[] = { &MonoBehaviour_t4_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t4_0_0_0 = { 1, GenInst_MonoBehaviour_t4_0_0_0_Types };
extern const Il2CppType Behaviour_t825_0_0_0;
static const Il2CppType* GenInst_Behaviour_t825_0_0_0_Types[] = { &Behaviour_t825_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t825_0_0_0 = { 1, GenInst_Behaviour_t825_0_0_0_Types };
extern const Il2CppType Object_t473_0_0_0;
static const Il2CppType* GenInst_Object_t473_0_0_0_Types[] = { &Object_t473_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t473_0_0_0 = { 1, GenInst_Object_t473_0_0_0_Types };
extern const Il2CppType IReflect_t3442_0_0_0;
static const Il2CppType* GenInst_IReflect_t3442_0_0_0_Types[] = { &IReflect_t3442_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t3442_0_0_0 = { 1, GenInst_IReflect_t3442_0_0_0_Types };
extern const Il2CppType _Type_t3440_0_0_0;
static const Il2CppType* GenInst__Type_t3440_0_0_0_Types[] = { &_Type_t3440_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t3440_0_0_0 = { 1, GenInst__Type_t3440_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t2188_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t2188_0_0_0_Types[] = { &ICustomAttributeProvider_t2188_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2188_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t2188_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3441_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3441_0_0_0_Types[] = { &_MemberInfo_t3441_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3441_0_0_0 = { 1, GenInst__MemberInfo_t3441_0_0_0_Types };
extern const Il2CppType IComparable_1_t3729_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3729_0_0_0_Types[] = { &IComparable_1_t3729_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3729_0_0_0 = { 1, GenInst_IComparable_1_t3729_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3730_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3730_0_0_0_Types[] = { &IEquatable_1_t3730_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3730_0_0_0 = { 1, GenInst_IEquatable_1_t3730_0_0_0_Types };
extern const Il2CppType Double_t553_0_0_0;
static const Il2CppType* GenInst_Double_t553_0_0_0_Types[] = { &Double_t553_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t553_0_0_0 = { 1, GenInst_Double_t553_0_0_0_Types };
extern const Il2CppType IComparable_1_t3751_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3751_0_0_0_Types[] = { &IComparable_1_t3751_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3751_0_0_0 = { 1, GenInst_IComparable_1_t3751_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3752_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3752_0_0_0_Types[] = { &IEquatable_1_t3752_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3752_0_0_0 = { 1, GenInst_IEquatable_1_t3752_0_0_0_Types };
extern const Il2CppType IComparable_1_t3745_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3745_0_0_0_Types[] = { &IComparable_1_t3745_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3745_0_0_0 = { 1, GenInst_IComparable_1_t3745_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3746_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3746_0_0_0_Types[] = { &IEquatable_1_t3746_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3746_0_0_0 = { 1, GenInst_IEquatable_1_t3746_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType Byte_t1117_0_0_0;
static const Il2CppType* GenInst_Byte_t1117_0_0_0_Types[] = { &Byte_t1117_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1117_0_0_0 = { 1, GenInst_Byte_t1117_0_0_0_Types };
extern const Il2CppType IComparable_1_t3737_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3737_0_0_0_Types[] = { &IComparable_1_t3737_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3737_0_0_0 = { 1, GenInst_IComparable_1_t3737_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3738_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3738_0_0_0_Types[] = { &IEquatable_1_t3738_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3738_0_0_0 = { 1, GenInst_IEquatable_1_t3738_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Char_t526_0_0_0_Types[] = { &Object_t_0_0_0, &Char_t526_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Char_t526_0_0_0 = { 2, GenInst_Object_t_0_0_0_Char_t526_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t882_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t882_0_0_0_Types[] = { &GUILayoutOption_t882_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t882_0_0_0 = { 1, GenInst_GUILayoutOption_t882_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t552_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &Type_t_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2322_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2322_0_0_0_Types[] = { &KeyValuePair_2_t2322_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2322_0_0_0 = { 1, GenInst_KeyValuePair_2_t2322_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_Types[] = { &TweenType_t442_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_Object_t_0_0_0 = { 2, GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2328_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2328_0_0_0_Types[] = { &KeyValuePair_2_t2328_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2328_0_0_0 = { 1, GenInst_KeyValuePair_2_t2328_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Types[] = { &TweenType_t442_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0 = { 1, GenInst_TweenType_t442_0_0_0_Types };
extern const Il2CppType Enum_t554_0_0_0;
static const Il2CppType* GenInst_Enum_t554_0_0_0_Types[] = { &Enum_t554_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t554_0_0_0 = { 1, GenInst_Enum_t554_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_TweenType_t442_0_0_0_Types[] = { &TweenType_t442_0_0_0, &Object_t_0_0_0, &TweenType_t442_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_TweenType_t442_0_0_0 = { 3, GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_TweenType_t442_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &TweenType_t442_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &TweenType_t442_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t552_0_0_0_Types[] = { &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t552_0_0_0 = { 1, GenInst_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2328_0_0_0_Types[] = { &TweenType_t442_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2328_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2328_0_0_0 = { 3, GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2328_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &TweenType_t442_0_0_0, &Dictionary_2_t545_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t545_0_0_0_Types[] = { &Dictionary_2_t545_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t545_0_0_0 = { 1, GenInst_Dictionary_2_t545_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2343_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2343_0_0_0_Types[] = { &KeyValuePair_2_t2343_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2343_0_0_0 = { 1, GenInst_KeyValuePair_2_t2343_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2350_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2350_0_0_0_Types[] = { &KeyValuePair_2_t2350_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2350_0_0_0 = { 1, GenInst_KeyValuePair_2_t2350_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2350_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Types };
extern const Il2CppType Rect_t225_0_0_0;
static const Il2CppType* GenInst_Rect_t225_0_0_0_Types[] = { &Rect_t225_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t225_0_0_0 = { 1, GenInst_Rect_t225_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t478_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2364_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2364_0_0_0_Types[] = { &KeyValuePair_2_t2364_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2364_0_0_0 = { 1, GenInst_KeyValuePair_2_t2364_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t478_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t478_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t478_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2364_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t478_0_0_0, &KeyValuePair_2_t2364_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2364_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2364_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t478_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2378_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2378_0_0_0_Types[] = { &KeyValuePair_2_t2378_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2378_0_0_0 = { 1, GenInst_KeyValuePair_2_t2378_0_0_0_Types };
extern const Il2CppType Material_t2_0_0_0;
static const Il2CppType* GenInst_Material_t2_0_0_0_Types[] = { &Material_t2_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t2_0_0_0 = { 1, GenInst_Material_t2_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType IComparable_1_t3755_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3755_0_0_0_Types[] = { &IComparable_1_t3755_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3755_0_0_0 = { 1, GenInst_IComparable_1_t3755_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3756_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3756_0_0_0_Types[] = { &IEquatable_1_t3756_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3756_0_0_0 = { 1, GenInst_IEquatable_1_t3756_0_0_0_Types };
static const Il2CppType* GenInst_iTweenPath_t459_0_0_0_Types[] = { &iTweenPath_t459_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenPath_t459_0_0_0 = { 1, GenInst_iTweenPath_t459_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &iTweenPath_t459_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2484_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2484_0_0_0_Types[] = { &KeyValuePair_2_t2484_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2484_0_0_0 = { 1, GenInst_KeyValuePair_2_t2484_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t478_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2514_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2514_0_0_0_Types[] = { &KeyValuePair_2_t2514_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2514_0_0_0 = { 1, GenInst_KeyValuePair_2_t2514_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Int32_t478_0_0_0_Types[] = { &Int32_t478_0_0_0, &Object_t_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Int32_t478_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Int32_t478_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t478_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Int32_t478_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2514_0_0_0_Types[] = { &Int32_t478_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2514_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2514_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2514_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Int32_t478_0_0_0, &PointerEventData_t257_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t784_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t784_0_0_0_Types[] = { &KeyValuePair_2_t784_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t784_0_0_0 = { 1, GenInst_KeyValuePair_2_t784_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t789_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t789_0_0_0_Types[] = { &RaycastHit2D_t789_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t789_0_0_0 = { 1, GenInst_RaycastHit2D_t789_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0_Types[] = { &ICanvasElement_t770_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0 = { 2, GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &ICanvasElement_t770_0_0_0, &Int32_t478_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Font_t643_0_0_0_List_1_t792_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Font_t643_0_0_0, &List_1_t792_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t643_0_0_0_List_1_t792_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Font_t643_0_0_0_List_1_t792_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t792_0_0_0_Types[] = { &List_1_t792_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t792_0_0_0 = { 1, GenInst_List_1_t792_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2549_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2549_0_0_0_Types[] = { &KeyValuePair_2_t2549_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2549_0_0_0 = { 1, GenInst_KeyValuePair_2_t2549_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0_Types[] = { &Graphic_t654_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0 = { 2, GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Graphic_t654_0_0_0, &Int32_t478_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Canvas_t650_0_0_0, &IndexedSet_1_t803_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t803_0_0_0_Types[] = { &IndexedSet_1_t803_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t803_0_0_0 = { 1, GenInst_IndexedSet_1_t803_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2580_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2580_0_0_0_Types[] = { &KeyValuePair_2_t2580_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2580_0_0_0 = { 1, GenInst_KeyValuePair_2_t2580_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2584_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2584_0_0_0_Types[] = { &KeyValuePair_2_t2584_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2584_0_0_0 = { 1, GenInst_KeyValuePair_2_t2584_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2588_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2588_0_0_0_Types[] = { &KeyValuePair_2_t2588_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2588_0_0_0 = { 1, GenInst_KeyValuePair_2_t2588_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t745_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t745_0_0_0_Types[] = { &LayoutRebuilder_t745_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t745_0_0_0 = { 1, GenInst_LayoutRebuilder_t745_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t531_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t531_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t531_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t531_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t3146_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t3146_0_0_0_Types[] = { &IAchievementDescription_t3146_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3146_0_0_0 = { 1, GenInst_IAchievementDescription_t3146_0_0_0_Types };
extern const Il2CppType IAchievement_t1082_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1082_0_0_0_Types[] = { &IAchievement_t1082_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1082_0_0_0 = { 1, GenInst_IAchievement_t1082_0_0_0_Types };
extern const Il2CppType IScore_t1036_0_0_0;
static const Il2CppType* GenInst_IScore_t1036_0_0_0_Types[] = { &IScore_t1036_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t1036_0_0_0 = { 1, GenInst_IScore_t1036_0_0_0_Types };
extern const Il2CppType IUserProfile_t3147_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t3147_0_0_0_Types[] = { &IUserProfile_t3147_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t3147_0_0_0 = { 1, GenInst_IUserProfile_t3147_0_0_0_Types };
extern const Il2CppType AchievementDescription_t1034_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t1034_0_0_0_Types[] = { &AchievementDescription_t1034_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t1034_0_0_0 = { 1, GenInst_AchievementDescription_t1034_0_0_0_Types };
extern const Il2CppType UserProfile_t1032_0_0_0;
static const Il2CppType* GenInst_UserProfile_t1032_0_0_0_Types[] = { &UserProfile_t1032_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t1032_0_0_0 = { 1, GenInst_UserProfile_t1032_0_0_0_Types };
extern const Il2CppType GcAchievementData_t1020_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t1020_0_0_0_Types[] = { &GcAchievementData_t1020_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1020_0_0_0 = { 1, GenInst_GcAchievementData_t1020_0_0_0_Types };
extern const Il2CppType Achievement_t1033_0_0_0;
static const Il2CppType* GenInst_Achievement_t1033_0_0_0_Types[] = { &Achievement_t1033_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t1033_0_0_0 = { 1, GenInst_Achievement_t1033_0_0_0_Types };
extern const Il2CppType GcScoreData_t1021_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t1021_0_0_0_Types[] = { &GcScoreData_t1021_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t1021_0_0_0 = { 1, GenInst_GcScoreData_t1021_0_0_0_Types };
extern const Il2CppType Score_t1035_0_0_0;
static const Il2CppType* GenInst_Score_t1035_0_0_0_Types[] = { &Score_t1035_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t1035_0_0_0 = { 1, GenInst_Score_t1035_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Int32_t478_0_0_0, &LayoutCache_t874_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t874_0_0_0_Types[] = { &LayoutCache_t874_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t874_0_0_0 = { 1, GenInst_LayoutCache_t874_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2652_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2652_0_0_0_Types[] = { &KeyValuePair_2_t2652_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2652_0_0_0 = { 1, GenInst_KeyValuePair_2_t2652_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t273_0_0_0_Types[] = { &GUIStyle_t273_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t273_0_0_0 = { 1, GenInst_GUIStyle_t273_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t273_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2663_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2663_0_0_0_Types[] = { &KeyValuePair_2_t2663_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2663_0_0_0 = { 1, GenInst_KeyValuePair_2_t2663_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1118_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1118_0_0_0_Types[] = { &KeyValuePair_2_t1118_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1118_0_0_0 = { 1, GenInst_KeyValuePair_2_t1118_0_0_0_Types };
extern const Il2CppType Display_t918_0_0_0;
static const Il2CppType* GenInst_Display_t918_0_0_0_Types[] = { &Display_t918_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t918_0_0_0 = { 1, GenInst_Display_t918_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t2210_0_0_0;
static const Il2CppType* GenInst_ISerializable_t2210_0_0_0_Types[] = { &ISerializable_t2210_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t2210_0_0_0 = { 1, GenInst_ISerializable_t2210_0_0_0_Types };
extern const Il2CppType ContactPoint_t930_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t930_0_0_0_Types[] = { &ContactPoint_t930_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t930_0_0_0 = { 1, GenInst_ContactPoint_t930_0_0_0_Types };
extern const Il2CppType Keyframe_t943_0_0_0;
static const Il2CppType* GenInst_Keyframe_t943_0_0_0_Types[] = { &Keyframe_t943_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t943_0_0_0 = { 1, GenInst_Keyframe_t943_0_0_0_Types };
extern const Il2CppType Int64_t1131_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t1131_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t1131_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1131_0_0_0 = { 2, GenInst_String_t_0_0_0_Int64_t1131_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t1131_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1131_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2706_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2706_0_0_0_Types[] = { &KeyValuePair_2_t2706_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2706_0_0_0 = { 1, GenInst_KeyValuePair_2_t2706_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t1131_0_0_0_Types[] = { &Int64_t1131_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1131_0_0_0 = { 1, GenInst_Int64_t1131_0_0_0_Types };
extern const Il2CppType IComparable_1_t3731_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3731_0_0_0_Types[] = { &IComparable_1_t3731_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3731_0_0_0 = { 1, GenInst_IComparable_1_t3731_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3732_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3732_0_0_0_Types[] = { &IEquatable_1_t3732_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3732_0_0_0 = { 1, GenInst_IEquatable_1_t3732_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t1131_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Int64_t1131_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t1131_0_0_0, &Int64_t1131_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Int64_t1131_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Int64_t1131_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t1131_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_KeyValuePair_2_t2706_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t1131_0_0_0, &KeyValuePair_2_t2706_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_KeyValuePair_2_t2706_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_KeyValuePair_2_t2706_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t1131_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2721_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2721_0_0_0_Types[] = { &KeyValuePair_2_t2721_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2721_0_0_0 = { 1, GenInst_KeyValuePair_2_t2721_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0 = { 2, GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2743_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2743_0_0_0_Types[] = { &KeyValuePair_2_t2743_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2743_0_0_0 = { 1, GenInst_KeyValuePair_2_t2743_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_Types[] = { &NetworkID_t979_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0 = { 1, GenInst_NetworkID_t979_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_NetworkID_t979_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &Object_t_0_0_0, &NetworkID_t979_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_NetworkID_t979_0_0_0 = { 3, GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_NetworkID_t979_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2743_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2743_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2743_0_0_0 = { 3, GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2743_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &NetworkAccessToken_t981_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_NetworkAccessToken_t981_0_0_0_Types[] = { &NetworkAccessToken_t981_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkAccessToken_t981_0_0_0 = { 1, GenInst_NetworkAccessToken_t981_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2757_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2757_0_0_0_Types[] = { &KeyValuePair_2_t2757_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2757_0_0_0 = { 1, GenInst_KeyValuePair_2_t2757_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorDelegate_t995_0_0_0_Types[] = { &ConstructorDelegate_t995_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t995_0_0_0 = { 1, GenInst_ConstructorDelegate_t995_0_0_0_Types };
static const Il2CppType* GenInst_GetDelegate_t993_0_0_0_Types[] = { &GetDelegate_t993_0_0_0 };
extern const Il2CppGenericInst GenInst_GetDelegate_t993_0_0_0 = { 1, GenInst_GetDelegate_t993_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t1098_0_0_0_Types[] = { &IDictionary_2_t1098_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1098_0_0_0 = { 1, GenInst_IDictionary_2_t1098_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1147_0_0_0_Types[] = { &KeyValuePair_2_t1147_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1147_0_0_0 = { 1, GenInst_KeyValuePair_2_t1147_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t1099_0_0_0_Types[] = { &IDictionary_2_t1099_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1099_0_0_0 = { 1, GenInst_IDictionary_2_t1099_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t2350_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0 = { 2, GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2762_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2762_0_0_0_Types[] = { &KeyValuePair_2_t2762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2762_0_0_0 = { 1, GenInst_KeyValuePair_2_t2762_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t995_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2770_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2770_0_0_0_Types[] = { &KeyValuePair_2_t2770_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2770_0_0_0 = { 1, GenInst_KeyValuePair_2_t2770_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t1098_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2774_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2774_0_0_0_Types[] = { &KeyValuePair_2_t2774_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2774_0_0_0 = { 1, GenInst_KeyValuePair_2_t2774_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t1099_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2778_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2778_0_0_0_Types[] = { &KeyValuePair_2_t2778_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2778_0_0_0 = { 1, GenInst_KeyValuePair_2_t2778_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t993_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t2350_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t2350_0_0_0, &KeyValuePair_2_t2350_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t2350_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2762_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t2350_0_0_0, &KeyValuePair_2_t2762_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2762_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2762_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t1147_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2800_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2800_0_0_0_Types[] = { &KeyValuePair_2_t2800_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2800_0_0_0 = { 1, GenInst_KeyValuePair_2_t2800_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3478_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3478_0_0_0_Types[] = { &_ConstructorInfo_t3478_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3478_0_0_0 = { 1, GenInst__ConstructorInfo_t3478_0_0_0_Types };
extern const Il2CppType MethodBase_t1163_0_0_0;
static const Il2CppType* GenInst_MethodBase_t1163_0_0_0_Types[] = { &MethodBase_t1163_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t1163_0_0_0 = { 1, GenInst_MethodBase_t1163_0_0_0_Types };
extern const Il2CppType _MethodBase_t3481_0_0_0;
static const Il2CppType* GenInst__MethodBase_t3481_0_0_0_Types[] = { &_MethodBase_t3481_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t3481_0_0_0 = { 1, GenInst__MethodBase_t3481_0_0_0_Types };
extern const Il2CppType ParameterInfo_t1154_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t1154_0_0_0_Types[] = { &ParameterInfo_t1154_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t1154_0_0_0 = { 1, GenInst_ParameterInfo_t1154_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t3484_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t3484_0_0_0_Types[] = { &_ParameterInfo_t3484_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t3484_0_0_0 = { 1, GenInst__ParameterInfo_t3484_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t3485_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t3485_0_0_0_Types[] = { &_PropertyInfo_t3485_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3485_0_0_0 = { 1, GenInst__PropertyInfo_t3485_0_0_0_Types };
extern const Il2CppType _FieldInfo_t3480_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t3480_0_0_0_Types[] = { &_FieldInfo_t3480_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t3480_0_0_0 = { 1, GenInst__FieldInfo_t3480_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t1010_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t1010_0_0_0_Types[] = { &DisallowMultipleComponent_t1010_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t1010_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t1010_0_0_0_Types };
extern const Il2CppType Attribute_t903_0_0_0;
static const Il2CppType* GenInst_Attribute_t903_0_0_0_Types[] = { &Attribute_t903_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t903_0_0_0 = { 1, GenInst_Attribute_t903_0_0_0_Types };
extern const Il2CppType _Attribute_t3429_0_0_0;
static const Il2CppType* GenInst__Attribute_t3429_0_0_0_Types[] = { &_Attribute_t3429_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t3429_0_0_0 = { 1, GenInst__Attribute_t3429_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t1013_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t1013_0_0_0_Types[] = { &ExecuteInEditMode_t1013_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t1013_0_0_0 = { 1, GenInst_ExecuteInEditMode_t1013_0_0_0_Types };
extern const Il2CppType RequireComponent_t1011_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t1011_0_0_0_Types[] = { &RequireComponent_t1011_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t1011_0_0_0 = { 1, GenInst_RequireComponent_t1011_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1805_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1805_0_0_0_Types[] = { &ParameterModifier_t1805_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1805_0_0_0 = { 1, GenInst_ParameterModifier_t1805_0_0_0_Types };
extern const Il2CppType HitInfo_t1039_0_0_0;
static const Il2CppType* GenInst_HitInfo_t1039_0_0_0_Types[] = { &HitInfo_t1039_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t1039_0_0_0 = { 1, GenInst_HitInfo_t1039_0_0_0_Types };
extern const Il2CppType Event_t481_0_0_0;
extern const Il2CppType TextEditOp_t1057_0_0_0;
static const Il2CppType* GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0_Types[] = { &Event_t481_0_0_0, &TextEditOp_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0 = { 2, GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2825_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2825_0_0_0_Types[] = { &KeyValuePair_2_t2825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2825_0_0_0 = { 1, GenInst_KeyValuePair_2_t2825_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1057_0_0_0_Types[] = { &TextEditOp_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1057_0_0_0 = { 1, GenInst_TextEditOp_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1057_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1057_0_0_0, &TextEditOp_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1057_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_KeyValuePair_2_t2825_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t1057_0_0_0, &KeyValuePair_2_t2825_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_KeyValuePair_2_t2825_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_KeyValuePair_2_t2825_0_0_0_Types };
static const Il2CppType* GenInst_Event_t481_0_0_0_Types[] = { &Event_t481_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t481_0_0_0 = { 1, GenInst_Event_t481_0_0_0_Types };
static const Il2CppType* GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Event_t481_0_0_0, &TextEditOp_t1057_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2839_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2839_0_0_0_Types[] = { &KeyValuePair_2_t2839_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2839_0_0_0 = { 1, GenInst_KeyValuePair_2_t2839_0_0_0_Types };
extern const Il2CppType KeySizes_t1195_0_0_0;
static const Il2CppType* GenInst_KeySizes_t1195_0_0_0_Types[] = { &KeySizes_t1195_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t1195_0_0_0 = { 1, GenInst_KeySizes_t1195_0_0_0_Types };
extern const Il2CppType UInt32_t1124_0_0_0;
static const Il2CppType* GenInst_UInt32_t1124_0_0_0_Types[] = { &UInt32_t1124_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t1124_0_0_0 = { 1, GenInst_UInt32_t1124_0_0_0_Types };
extern const Il2CppType IComparable_1_t3733_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3733_0_0_0_Types[] = { &IComparable_1_t3733_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3733_0_0_0 = { 1, GenInst_IComparable_1_t3733_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3734_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3734_0_0_0_Types[] = { &IEquatable_1_t3734_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3734_0_0_0 = { 1, GenInst_IEquatable_1_t3734_0_0_0_Types };
extern const Il2CppType BigInteger_t1199_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1199_0_0_0_Types[] = { &BigInteger_t1199_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1199_0_0_0 = { 1, GenInst_BigInteger_t1199_0_0_0_Types };
extern const Il2CppType X509Certificate_t1304_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t1304_0_0_0_Types[] = { &X509Certificate_t1304_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t1304_0_0_0 = { 1, GenInst_X509Certificate_t1304_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t2213_0_0_0_Types[] = { &IDeserializationCallback_t2213_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t2213_0_0_0 = { 1, GenInst_IDeserializationCallback_t2213_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1308_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1308_0_0_0_Types[] = { &ClientCertificateType_t1308_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1308_0_0_0 = { 1, GenInst_ClientCertificateType_t1308_0_0_0_Types };
extern const Il2CppType UInt16_t1122_0_0_0;
static const Il2CppType* GenInst_UInt16_t1122_0_0_0_Types[] = { &UInt16_t1122_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t1122_0_0_0 = { 1, GenInst_UInt16_t1122_0_0_0_Types };
extern const Il2CppType IComparable_1_t3743_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3743_0_0_0_Types[] = { &IComparable_1_t3743_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3743_0_0_0 = { 1, GenInst_IComparable_1_t3743_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3744_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3744_0_0_0_Types[] = { &IEquatable_1_t3744_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3744_0_0_0 = { 1, GenInst_IEquatable_1_t3744_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2877_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2877_0_0_0_Types[] = { &KeyValuePair_2_t2877_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2877_0_0_0 = { 1, GenInst_KeyValuePair_2_t2877_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t536_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Boolean_t536_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t536_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Boolean_t536_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Boolean_t536_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t536_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_KeyValuePair_2_t2877_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t536_0_0_0, &KeyValuePair_2_t2877_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_KeyValuePair_2_t2877_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_KeyValuePair_2_t2877_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t536_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2888_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2888_0_0_0_Types[] = { &KeyValuePair_2_t2888_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2888_0_0_0 = { 1, GenInst_KeyValuePair_2_t2888_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t1432_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t1432_0_0_0_Types[] = { &X509ChainStatus_t1432_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1432_0_0_0 = { 1, GenInst_X509ChainStatus_t1432_0_0_0_Types };
extern const Il2CppType Capture_t1454_0_0_0;
static const Il2CppType* GenInst_Capture_t1454_0_0_0_Types[] = { &Capture_t1454_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t1454_0_0_0 = { 1, GenInst_Capture_t1454_0_0_0_Types };
extern const Il2CppType Group_t1373_0_0_0;
static const Il2CppType* GenInst_Group_t1373_0_0_0_Types[] = { &Group_t1373_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t1373_0_0_0 = { 1, GenInst_Group_t1373_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_Types[] = { &Int32_t478_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0 = { 2, GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2896_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2896_0_0_0_Types[] = { &KeyValuePair_2_t2896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2896_0_0_0 = { 1, GenInst_KeyValuePair_2_t2896_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0_Types[] = { &Int32_t478_0_0_0, &Int32_t478_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Int32_t478_0_0_0, &Int32_t478_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2896_0_0_0_Types[] = { &Int32_t478_0_0_0, &Int32_t478_0_0_0, &KeyValuePair_2_t2896_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2896_0_0_0 = { 3, GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2896_0_0_0_Types };
extern const Il2CppType Mark_t1479_0_0_0;
static const Il2CppType* GenInst_Mark_t1479_0_0_0_Types[] = { &Mark_t1479_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t1479_0_0_0 = { 1, GenInst_Mark_t1479_0_0_0_Types };
extern const Il2CppType UriScheme_t1516_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1516_0_0_0_Types[] = { &UriScheme_t1516_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1516_0_0_0 = { 1, GenInst_UriScheme_t1516_0_0_0_Types };
extern const Il2CppType UInt64_t1134_0_0_0;
static const Il2CppType* GenInst_UInt64_t1134_0_0_0_Types[] = { &UInt64_t1134_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t1134_0_0_0 = { 1, GenInst_UInt64_t1134_0_0_0_Types };
extern const Il2CppType SByte_t1135_0_0_0;
static const Il2CppType* GenInst_SByte_t1135_0_0_0_Types[] = { &SByte_t1135_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1135_0_0_0 = { 1, GenInst_SByte_t1135_0_0_0_Types };
extern const Il2CppType Int16_t1136_0_0_0;
static const Il2CppType* GenInst_Int16_t1136_0_0_0_Types[] = { &Int16_t1136_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1136_0_0_0 = { 1, GenInst_Int16_t1136_0_0_0_Types };
extern const Il2CppType Decimal_t1133_0_0_0;
static const Il2CppType* GenInst_Decimal_t1133_0_0_0_Types[] = { &Decimal_t1133_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1133_0_0_0 = { 1, GenInst_Decimal_t1133_0_0_0_Types };
extern const Il2CppType Delegate_t480_0_0_0;
static const Il2CppType* GenInst_Delegate_t480_0_0_0_Types[] = { &Delegate_t480_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t480_0_0_0 = { 1, GenInst_Delegate_t480_0_0_0_Types };
extern const Il2CppType ICloneable_t3433_0_0_0;
static const Il2CppType* GenInst_ICloneable_t3433_0_0_0_Types[] = { &ICloneable_t3433_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t3433_0_0_0 = { 1, GenInst_ICloneable_t3433_0_0_0_Types };
extern const Il2CppType IComparable_1_t3735_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3735_0_0_0_Types[] = { &IComparable_1_t3735_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3735_0_0_0 = { 1, GenInst_IComparable_1_t3735_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3736_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3736_0_0_0_Types[] = { &IEquatable_1_t3736_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3736_0_0_0 = { 1, GenInst_IEquatable_1_t3736_0_0_0_Types };
extern const Il2CppType IComparable_1_t3741_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3741_0_0_0_Types[] = { &IComparable_1_t3741_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3741_0_0_0 = { 1, GenInst_IComparable_1_t3741_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3742_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3742_0_0_0_Types[] = { &IEquatable_1_t3742_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3742_0_0_0 = { 1, GenInst_IEquatable_1_t3742_0_0_0_Types };
extern const Il2CppType IComparable_1_t3739_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3739_0_0_0_Types[] = { &IComparable_1_t3739_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3739_0_0_0 = { 1, GenInst_IComparable_1_t3739_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3740_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3740_0_0_0_Types[] = { &IEquatable_1_t3740_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3740_0_0_0 = { 1, GenInst_IEquatable_1_t3740_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3482_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3482_0_0_0_Types[] = { &_MethodInfo_t3482_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3482_0_0_0 = { 1, GenInst__MethodInfo_t3482_0_0_0_Types };
extern const Il2CppType TableRange_t1580_0_0_0;
static const Il2CppType* GenInst_TableRange_t1580_0_0_0_Types[] = { &TableRange_t1580_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1580_0_0_0 = { 1, GenInst_TableRange_t1580_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1583_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1583_0_0_0_Types[] = { &TailoringInfo_t1583_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1583_0_0_0 = { 1, GenInst_TailoringInfo_t1583_0_0_0_Types };
extern const Il2CppType Contraction_t1584_0_0_0;
static const Il2CppType* GenInst_Contraction_t1584_0_0_0_Types[] = { &Contraction_t1584_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1584_0_0_0 = { 1, GenInst_Contraction_t1584_0_0_0_Types };
extern const Il2CppType Level2Map_t1586_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1586_0_0_0_Types[] = { &Level2Map_t1586_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1586_0_0_0 = { 1, GenInst_Level2Map_t1586_0_0_0_Types };
extern const Il2CppType BigInteger_t1606_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1606_0_0_0_Types[] = { &BigInteger_t1606_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1606_0_0_0 = { 1, GenInst_BigInteger_t1606_0_0_0_Types };
extern const Il2CppType Slot_t1657_0_0_0;
static const Il2CppType* GenInst_Slot_t1657_0_0_0_Types[] = { &Slot_t1657_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1657_0_0_0 = { 1, GenInst_Slot_t1657_0_0_0_Types };
extern const Il2CppType Slot_t1664_0_0_0;
static const Il2CppType* GenInst_Slot_t1664_0_0_0_Types[] = { &Slot_t1664_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1664_0_0_0 = { 1, GenInst_Slot_t1664_0_0_0_Types };
extern const Il2CppType StackFrame_t1162_0_0_0;
static const Il2CppType* GenInst_StackFrame_t1162_0_0_0_Types[] = { &StackFrame_t1162_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t1162_0_0_0 = { 1, GenInst_StackFrame_t1162_0_0_0_Types };
extern const Il2CppType Calendar_t1678_0_0_0;
static const Il2CppType* GenInst_Calendar_t1678_0_0_0_Types[] = { &Calendar_t1678_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1678_0_0_0 = { 1, GenInst_Calendar_t1678_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1754_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1754_0_0_0_Types[] = { &ModuleBuilder_t1754_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1754_0_0_0 = { 1, GenInst_ModuleBuilder_t1754_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t3472_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t3472_0_0_0_Types[] = { &_ModuleBuilder_t3472_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t3472_0_0_0 = { 1, GenInst__ModuleBuilder_t3472_0_0_0_Types };
extern const Il2CppType Module_t1748_0_0_0;
static const Il2CppType* GenInst_Module_t1748_0_0_0_Types[] = { &Module_t1748_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1748_0_0_0 = { 1, GenInst_Module_t1748_0_0_0_Types };
extern const Il2CppType _Module_t3483_0_0_0;
static const Il2CppType* GenInst__Module_t3483_0_0_0_Types[] = { &_Module_t3483_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t3483_0_0_0 = { 1, GenInst__Module_t3483_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1758_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1758_0_0_0_Types[] = { &ParameterBuilder_t1758_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1758_0_0_0 = { 1, GenInst_ParameterBuilder_t1758_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t3473_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t3473_0_0_0_Types[] = { &_ParameterBuilder_t3473_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t3473_0_0_0 = { 1, GenInst__ParameterBuilder_t3473_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t485_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t485_0_0_0_Types[] = { &TypeU5BU5D_t485_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t485_0_0_0 = { 1, GenInst_TypeU5BU5D_t485_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType IEnumerable_t1097_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t1097_0_0_0_Types[] = { &IEnumerable_t1097_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t1097_0_0_0 = { 1, GenInst_IEnumerable_t1097_0_0_0_Types };
extern const Il2CppType ICollection_t1528_0_0_0;
static const Il2CppType* GenInst_ICollection_t1528_0_0_0_Types[] = { &ICollection_t1528_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1528_0_0_0 = { 1, GenInst_ICollection_t1528_0_0_0_Types };
extern const Il2CppType IList_t1487_0_0_0;
static const Il2CppType* GenInst_IList_t1487_0_0_0_Types[] = { &IList_t1487_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1487_0_0_0 = { 1, GenInst_IList_t1487_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1742_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1742_0_0_0_Types[] = { &ILTokenInfo_t1742_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1742_0_0_0 = { 1, GenInst_ILTokenInfo_t1742_0_0_0_Types };
extern const Il2CppType LabelData_t1744_0_0_0;
static const Il2CppType* GenInst_LabelData_t1744_0_0_0_Types[] = { &LabelData_t1744_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1744_0_0_0 = { 1, GenInst_LabelData_t1744_0_0_0_Types };
extern const Il2CppType LabelFixup_t1743_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1743_0_0_0_Types[] = { &LabelFixup_t1743_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1743_0_0_0 = { 1, GenInst_LabelFixup_t1743_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1741_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1741_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1741_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1741_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1741_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1733_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1733_0_0_0_Types[] = { &TypeBuilder_t1733_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1733_0_0_0 = { 1, GenInst_TypeBuilder_t1733_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t3475_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t3475_0_0_0_Types[] = { &_TypeBuilder_t3475_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t3475_0_0_0 = { 1, GenInst__TypeBuilder_t3475_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1740_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1740_0_0_0_Types[] = { &MethodBuilder_t1740_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1740_0_0_0 = { 1, GenInst_MethodBuilder_t1740_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t3471_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t3471_0_0_0_Types[] = { &_MethodBuilder_t3471_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3471_0_0_0 = { 1, GenInst__MethodBuilder_t3471_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1736_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1736_0_0_0_Types[] = { &ConstructorBuilder_t1736_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1736_0_0_0 = { 1, GenInst_ConstructorBuilder_t1736_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t3467_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t3467_0_0_0_Types[] = { &_ConstructorBuilder_t3467_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t3467_0_0_0 = { 1, GenInst__ConstructorBuilder_t3467_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t1759_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t1759_0_0_0_Types[] = { &PropertyBuilder_t1759_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t1759_0_0_0 = { 1, GenInst_PropertyBuilder_t1759_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t3474_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t3474_0_0_0_Types[] = { &_PropertyBuilder_t3474_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3474_0_0_0 = { 1, GenInst__PropertyBuilder_t3474_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1739_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1739_0_0_0_Types[] = { &FieldBuilder_t1739_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1739_0_0_0 = { 1, GenInst_FieldBuilder_t1739_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t3469_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t3469_0_0_0_Types[] = { &_FieldBuilder_t3469_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t3469_0_0_0 = { 1, GenInst__FieldBuilder_t3469_0_0_0_Types };
extern const Il2CppType Header_t1876_0_0_0;
static const Il2CppType* GenInst_Header_t1876_0_0_0_Types[] = { &Header_t1876_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1876_0_0_0 = { 1, GenInst_Header_t1876_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t2205_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t2205_0_0_0_Types[] = { &ITrackingHandler_t2205_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2205_0_0_0 = { 1, GenInst_ITrackingHandler_t2205_0_0_0_Types };
extern const Il2CppType IContextAttribute_t2197_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t2197_0_0_0_Types[] = { &IContextAttribute_t2197_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2197_0_0_0 = { 1, GenInst_IContextAttribute_t2197_0_0_0_Types };
extern const Il2CppType IComparable_1_t3890_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3890_0_0_0_Types[] = { &IComparable_1_t3890_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3890_0_0_0 = { 1, GenInst_IComparable_1_t3890_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3891_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3891_0_0_0_Types[] = { &IEquatable_1_t3891_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3891_0_0_0 = { 1, GenInst_IEquatable_1_t3891_0_0_0_Types };
extern const Il2CppType IComparable_1_t3753_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3753_0_0_0_Types[] = { &IComparable_1_t3753_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3753_0_0_0 = { 1, GenInst_IComparable_1_t3753_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3754_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3754_0_0_0_Types[] = { &IEquatable_1_t3754_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3754_0_0_0 = { 1, GenInst_IEquatable_1_t3754_0_0_0_Types };
extern const Il2CppType IComparable_1_t3900_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3900_0_0_0_Types[] = { &IComparable_1_t3900_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3900_0_0_0 = { 1, GenInst_IComparable_1_t3900_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3901_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3901_0_0_0_Types[] = { &IEquatable_1_t3901_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3901_0_0_0 = { 1, GenInst_IEquatable_1_t3901_0_0_0_Types };
extern const Il2CppType TypeTag_t1930_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1930_0_0_0_Types[] = { &TypeTag_t1930_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1930_0_0_0 = { 1, GenInst_TypeTag_t1930_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t522_0_0_0;
static const Il2CppType* GenInst_Version_t522_0_0_0_Types[] = { &Version_t522_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t522_0_0_0 = { 1, GenInst_Version_t522_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t511_0_0_0_RaycastResult_t511_0_0_0_Types[] = { &RaycastResult_t511_0_0_0, &RaycastResult_t511_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t511_0_0_0_RaycastResult_t511_0_0_0 = { 2, GenInst_RaycastResult_t511_0_0_0_RaycastResult_t511_0_0_0_Types };
static const Il2CppType* GenInst_Char_t526_0_0_0_Char_t526_0_0_0_Types[] = { &Char_t526_0_0_0, &Char_t526_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t526_0_0_0_Char_t526_0_0_0 = { 2, GenInst_Char_t526_0_0_0_Char_t526_0_0_0_Types };
static const Il2CppType* GenInst_TweenType_t442_0_0_0_TweenType_t442_0_0_0_Types[] = { &TweenType_t442_0_0_0, &TweenType_t442_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenType_t442_0_0_0_TweenType_t442_0_0_0 = { 2, GenInst_TweenType_t442_0_0_0_TweenType_t442_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t552_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &DictionaryEntry_t552_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t552_0_0_0_DictionaryEntry_t552_0_0_0 = { 2, GenInst_DictionaryEntry_t552_0_0_0_DictionaryEntry_t552_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2328_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2328_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2328_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2328_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2328_0_0_0_KeyValuePair_2_t2328_0_0_0_Types[] = { &KeyValuePair_2_t2328_0_0_0, &KeyValuePair_2_t2328_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2328_0_0_0_KeyValuePair_2_t2328_0_0_0 = { 2, GenInst_KeyValuePair_2_t2328_0_0_0_KeyValuePair_2_t2328_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2350_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0_Types[] = { &KeyValuePair_2_t2350_0_0_0, &KeyValuePair_2_t2350_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0 = { 2, GenInst_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2364_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2364_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2364_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2364_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2364_0_0_0_KeyValuePair_2_t2364_0_0_0_Types[] = { &KeyValuePair_2_t2364_0_0_0, &KeyValuePair_2_t2364_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2364_0_0_0_KeyValuePair_2_t2364_0_0_0 = { 2, GenInst_KeyValuePair_2_t2364_0_0_0_KeyValuePair_2_t2364_0_0_0_Types };
static const Il2CppType* GenInst_Single_t531_0_0_0_Single_t531_0_0_0_Types[] = { &Single_t531_0_0_0, &Single_t531_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t531_0_0_0_Single_t531_0_0_0 = { 2, GenInst_Single_t531_0_0_0_Single_t531_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t536_0_0_0_Boolean_t536_0_0_0_Types[] = { &Boolean_t536_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t536_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_Boolean_t536_0_0_0_Boolean_t536_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t215_0_0_0_Vector3_t215_0_0_0_Types[] = { &Vector3_t215_0_0_0, &Vector3_t215_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t215_0_0_0_Vector3_t215_0_0_0 = { 2, GenInst_Vector3_t215_0_0_0_Vector3_t215_0_0_0_Types };
static const Il2CppType* GenInst_Color_t6_0_0_0_Color_t6_0_0_0_Types[] = { &Color_t6_0_0_0, &Color_t6_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t6_0_0_0_Color_t6_0_0_0 = { 2, GenInst_Color_t6_0_0_0_Color_t6_0_0_0_Types };
static const Il2CppType* GenInst_Space_t546_0_0_0_Space_t546_0_0_0_Types[] = { &Space_t546_0_0_0, &Space_t546_0_0_0 };
extern const Il2CppGenericInst GenInst_Space_t546_0_0_0_Space_t546_0_0_0 = { 2, GenInst_Space_t546_0_0_0_Space_t546_0_0_0_Types };
static const Il2CppType* GenInst_EaseType_t425_0_0_0_EaseType_t425_0_0_0_Types[] = { &EaseType_t425_0_0_0, &EaseType_t425_0_0_0 };
extern const Il2CppGenericInst GenInst_EaseType_t425_0_0_0_EaseType_t425_0_0_0 = { 2, GenInst_EaseType_t425_0_0_0_EaseType_t425_0_0_0_Types };
static const Il2CppType* GenInst_LoopType_t426_0_0_0_LoopType_t426_0_0_0_Types[] = { &LoopType_t426_0_0_0, &LoopType_t426_0_0_0 };
extern const Il2CppGenericInst GenInst_LoopType_t426_0_0_0_LoopType_t426_0_0_0 = { 2, GenInst_LoopType_t426_0_0_0_LoopType_t426_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2514_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2514_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2514_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2514_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2514_0_0_0_KeyValuePair_2_t2514_0_0_0_Types[] = { &KeyValuePair_2_t2514_0_0_0, &KeyValuePair_2_t2514_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2514_0_0_0_KeyValuePair_2_t2514_0_0_0 = { 2, GenInst_KeyValuePair_2_t2514_0_0_0_KeyValuePair_2_t2514_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t685_0_0_0_UIVertex_t685_0_0_0_Types[] = { &UIVertex_t685_0_0_0, &UIVertex_t685_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t685_0_0_0_UIVertex_t685_0_0_0 = { 2, GenInst_UIVertex_t685_0_0_0_UIVertex_t685_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t811_0_0_0_UICharInfo_t811_0_0_0_Types[] = { &UICharInfo_t811_0_0_0, &UICharInfo_t811_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t811_0_0_0_UICharInfo_t811_0_0_0 = { 2, GenInst_UICharInfo_t811_0_0_0_UICharInfo_t811_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t809_0_0_0_UILineInfo_t809_0_0_0_Types[] = { &UILineInfo_t809_0_0_0, &UILineInfo_t809_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t809_0_0_0_UILineInfo_t809_0_0_0 = { 2, GenInst_UILineInfo_t809_0_0_0_UILineInfo_t809_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t1131_0_0_0_Object_t_0_0_0_Types[] = { &Int64_t1131_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1131_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int64_t1131_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t1131_0_0_0_Int64_t1131_0_0_0_Types[] = { &Int64_t1131_0_0_0, &Int64_t1131_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t1131_0_0_0_Int64_t1131_0_0_0 = { 2, GenInst_Int64_t1131_0_0_0_Int64_t1131_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2706_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2706_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2706_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2706_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2706_0_0_0_KeyValuePair_2_t2706_0_0_0_Types[] = { &KeyValuePair_2_t2706_0_0_0, &KeyValuePair_2_t2706_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2706_0_0_0_KeyValuePair_2_t2706_0_0_0 = { 2, GenInst_KeyValuePair_2_t2706_0_0_0_KeyValuePair_2_t2706_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t979_0_0_0_NetworkID_t979_0_0_0_Types[] = { &NetworkID_t979_0_0_0, &NetworkID_t979_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t979_0_0_0_NetworkID_t979_0_0_0 = { 2, GenInst_NetworkID_t979_0_0_0_NetworkID_t979_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2743_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2743_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2743_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2743_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2743_0_0_0_KeyValuePair_2_t2743_0_0_0_Types[] = { &KeyValuePair_2_t2743_0_0_0, &KeyValuePair_2_t2743_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2743_0_0_0_KeyValuePair_2_t2743_0_0_0 = { 2, GenInst_KeyValuePair_2_t2743_0_0_0_KeyValuePair_2_t2743_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2762_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2762_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2762_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2762_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2762_0_0_0_KeyValuePair_2_t2762_0_0_0_Types[] = { &KeyValuePair_2_t2762_0_0_0, &KeyValuePair_2_t2762_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2762_0_0_0_KeyValuePair_2_t2762_0_0_0 = { 2, GenInst_KeyValuePair_2_t2762_0_0_0_KeyValuePair_2_t2762_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1057_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t1057_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1057_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t1057_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0_Types[] = { &TextEditOp_t1057_0_0_0, &TextEditOp_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0 = { 2, GenInst_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2825_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2825_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2825_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2825_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2825_0_0_0_KeyValuePair_2_t2825_0_0_0_Types[] = { &KeyValuePair_2_t2825_0_0_0, &KeyValuePair_2_t2825_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2825_0_0_0_KeyValuePair_2_t2825_0_0_0 = { 2, GenInst_KeyValuePair_2_t2825_0_0_0_KeyValuePair_2_t2825_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t536_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t536_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t536_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t536_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2877_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2877_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2877_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2877_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2877_0_0_0_KeyValuePair_2_t2877_0_0_0_Types[] = { &KeyValuePair_2_t2877_0_0_0, &KeyValuePair_2_t2877_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2877_0_0_0_KeyValuePair_2_t2877_0_0_0 = { 2, GenInst_KeyValuePair_2_t2877_0_0_0_KeyValuePair_2_t2877_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2896_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0_Types[] = { &KeyValuePair_2_t2896_0_0_0, &KeyValuePair_2_t2896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0 = { 2, GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0_Types };
extern const Il2CppType iTweenEvent_AddToList_m24076_gp_0_0_0_0;
static const Il2CppType* GenInst_iTweenEvent_AddToList_m24076_gp_0_0_0_0_Types[] = { &iTweenEvent_AddToList_m24076_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenEvent_AddToList_m24076_gp_0_0_0_0 = { 1, GenInst_iTweenEvent_AddToList_m24076_gp_0_0_0_0_Types };
extern const Il2CppType iTweenEvent_AddToList_m24077_gp_0_0_0_0;
static const Il2CppType* GenInst_iTweenEvent_AddToList_m24077_gp_0_0_0_0_Types[] = { &iTweenEvent_AddToList_m24077_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenEvent_AddToList_m24077_gp_0_0_0_0 = { 1, GenInst_iTweenEvent_AddToList_m24077_gp_0_0_0_0_Types };
extern const Il2CppType iTweenEvent_AddToList_m24078_gp_0_0_0_0;
static const Il2CppType* GenInst_iTweenEvent_AddToList_m24078_gp_0_0_0_0_Types[] = { &iTweenEvent_AddToList_m24078_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenEvent_AddToList_m24078_gp_0_0_0_0 = { 1, GenInst_iTweenEvent_AddToList_m24078_gp_0_0_0_0_Types };
extern const Il2CppType iTweenEvent_AddToList_m24079_gp_0_0_0_0;
static const Il2CppType* GenInst_iTweenEvent_AddToList_m24079_gp_0_0_0_0_Types[] = { &iTweenEvent_AddToList_m24079_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenEvent_AddToList_m24079_gp_0_0_0_0 = { 1, GenInst_iTweenEvent_AddToList_m24079_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m24098_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m24098_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m24098_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m24098_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m24098_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m24099_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m24099_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m24099_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m24099_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m24099_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3397_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t3397_gp_0_0_0_0_Types[] = { &IndexedSet_1_t3397_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3397_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t3397_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t3397_gp_0_0_0_0_Int32_t478_0_0_0_Types[] = { &IndexedSet_1_t3397_gp_0_0_0_0, &Int32_t478_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3397_gp_0_0_0_0_Int32_t478_0_0_0 = { 2, GenInst_IndexedSet_1_t3397_gp_0_0_0_0_Int32_t478_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t3398_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t3398_gp_0_0_0_0_Types[] = { &ObjectPool_1_t3398_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t3398_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t3398_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m24184_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m24184_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m24184_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m24184_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m24184_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m24186_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m24186_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m24186_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m24186_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m24186_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m24188_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m24188_gp_0_0_0_0_Types[] = { &Component_GetComponents_m24188_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m24188_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m24188_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m24191_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m24191_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m24191_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m24191_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m24191_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m24193_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m24193_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m24193_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m24193_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m24193_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m24194_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m24194_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m24194_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m24194_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m24194_gp_0_0_0_0_Types };
extern const Il2CppType ResponseBase_ParseJSONList_m24198_gp_0_0_0_0;
static const Il2CppType* GenInst_ResponseBase_ParseJSONList_m24198_gp_0_0_0_0_Types[] = { &ResponseBase_ParseJSONList_m24198_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseBase_ParseJSONList_m24198_gp_0_0_0_0 = { 1, GenInst_ResponseBase_ParseJSONList_m24198_gp_0_0_0_0_Types };
extern const Il2CppType NetworkMatch_ProcessMatchResponse_m24199_gp_0_0_0_0;
static const Il2CppType* GenInst_NetworkMatch_ProcessMatchResponse_m24199_gp_0_0_0_0_Types[] = { &NetworkMatch_ProcessMatchResponse_m24199_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkMatch_ProcessMatchResponse_m24199_gp_0_0_0_0 = { 1, GenInst_NetworkMatch_ProcessMatchResponse_m24199_gp_0_0_0_0_Types };
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_gp_0_0_0_0_Types[] = { &U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_gp_0_0_0_0 = { 1, GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_gp_0_0_0_0_Types };
extern const Il2CppType ThreadSafeDictionary_2_t3403_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t3403_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0_ThreadSafeDictionary_2_t3403_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t3403_gp_0_0_0_0, &ThreadSafeDictionary_2_t3403_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0_ThreadSafeDictionary_2_t3403_gp_1_0_0_0 = { 2, GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0_ThreadSafeDictionary_2_t3403_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0_Types[] = { &ThreadSafeDictionary_2_t3403_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t3403_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t3403_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t3403_gp_1_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t3403_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4583_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4583_0_0_0_Types[] = { &KeyValuePair_2_t4583_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4583_0_0_0 = { 1, GenInst_KeyValuePair_2_t4583_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3407_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3407_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3407_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3407_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3407_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3408_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3408_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3408_gp_0_0_0_0_InvokableCall_2_t3408_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3408_gp_0_0_0_0, &InvokableCall_2_t3408_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3408_gp_0_0_0_0_InvokableCall_2_t3408_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3408_gp_0_0_0_0_InvokableCall_2_t3408_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3409_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3409_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3409_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3409_gp_0_0_0_0_InvokableCall_3_t3409_gp_1_0_0_0_InvokableCall_3_t3409_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3409_gp_0_0_0_0, &InvokableCall_3_t3409_gp_1_0_0_0, &InvokableCall_3_t3409_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3409_gp_0_0_0_0_InvokableCall_3_t3409_gp_1_0_0_0_InvokableCall_3_t3409_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3409_gp_0_0_0_0_InvokableCall_3_t3409_gp_1_0_0_0_InvokableCall_3_t3409_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3410_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3410_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3410_gp_0_0_0_0_InvokableCall_4_t3410_gp_1_0_0_0_InvokableCall_4_t3410_gp_2_0_0_0_InvokableCall_4_t3410_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3410_gp_0_0_0_0, &InvokableCall_4_t3410_gp_1_0_0_0, &InvokableCall_4_t3410_gp_2_0_0_0, &InvokableCall_4_t3410_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3410_gp_0_0_0_0_InvokableCall_4_t3410_gp_1_0_0_0_InvokableCall_4_t3410_gp_2_0_0_0_InvokableCall_4_t3410_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3410_gp_0_0_0_0_InvokableCall_4_t3410_gp_1_0_0_0_InvokableCall_4_t3410_gp_2_0_0_0_InvokableCall_4_t3410_gp_3_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t3411_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t3411_gp_0_0_0_0_Types[] = { &UnityEvent_1_t3411_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t3411_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t3411_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m24292_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m24292_gp_0_0_0_0_Types[] = { &Enumerable_Any_m24292_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m24292_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m24292_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m24293_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m24293_gp_0_0_0_0_Types[] = { &Enumerable_First_m24293_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m24293_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m24293_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_First_m24293_gp_0_0_0_0_Boolean_t536_0_0_0_Types[] = { &Enumerable_First_m24293_gp_0_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m24293_gp_0_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_Enumerable_First_m24293_gp_0_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m24294_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m24294_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m24294_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m24294_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m24294_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m24295_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m24295_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0_Boolean_t536_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m24295_gp_0_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType Enumerable_Repeat_m24296_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Repeat_m24296_gp_0_0_0_0_Types[] = { &Enumerable_Repeat_m24296_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Repeat_m24296_gp_0_0_0_0 = { 1, GenInst_Enumerable_Repeat_m24296_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateRepeatIterator_m24297_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateRepeatIterator_m24297_gp_0_0_0_0_Types[] = { &Enumerable_CreateRepeatIterator_m24297_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateRepeatIterator_m24297_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateRepeatIterator_m24297_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m24298_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m24298_gp_0_0_0_0_Types[] = { &Enumerable_Select_m24298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m24298_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m24298_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Select_m24298_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_Select_m24298_gp_0_0_0_0_Enumerable_Select_m24298_gp_1_0_0_0_Types[] = { &Enumerable_Select_m24298_gp_0_0_0_0, &Enumerable_Select_m24298_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m24298_gp_0_0_0_0_Enumerable_Select_m24298_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m24298_gp_0_0_0_0_Enumerable_Select_m24298_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Select_m24298_gp_1_0_0_0_Types[] = { &Enumerable_Select_m24298_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m24298_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m24298_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0, &Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0_Types[] = { &Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m24300_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m24300_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m24300_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m24300_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m24300_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m24301_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m24301_gp_0_0_0_0_Types[] = { &Enumerable_Where_m24301_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m24301_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m24301_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m24301_gp_0_0_0_0_Boolean_t536_0_0_0_Types[] = { &Enumerable_Where_m24301_gp_0_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m24301_gp_0_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_Enumerable_Where_m24301_gp_0_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0_Boolean_t536_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType PredicateOf_1_t3419_gp_0_0_0_0;
static const Il2CppType* GenInst_PredicateOf_1_t3419_gp_0_0_0_0_Boolean_t536_0_0_0_Types[] = { &PredicateOf_1_t3419_gp_0_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_PredicateOf_1_t3419_gp_0_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_PredicateOf_1_t3419_gp_0_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_gp_0_0_0_0_Types[] = { &U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_gp_0_0_0_0 = { 1, GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_gp_0_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0_Types[] = { &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0_Boolean_t536_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0, &Boolean_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0_Boolean_t536_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0_Boolean_t536_0_0_0_Types };
extern const Il2CppType Queue_1_t3424_gp_0_0_0_0;
static const Il2CppType* GenInst_Queue_1_t3424_gp_0_0_0_0_Types[] = { &Queue_1_t3424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Queue_1_t3424_gp_0_0_0_0 = { 1, GenInst_Queue_1_t3424_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3425_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3425_gp_0_0_0_0_Types[] = { &Enumerator_t3425_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3425_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3425_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t3426_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t3426_gp_0_0_0_0_Types[] = { &Stack_1_t3426_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t3426_gp_0_0_0_0 = { 1, GenInst_Stack_1_t3426_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3427_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3427_gp_0_0_0_0_Types[] = { &Enumerator_t3427_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3427_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3427_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t3434_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t3434_gp_0_0_0_0_Types[] = { &IEnumerable_1_t3434_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3434_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t3434_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m24453_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m24453_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m24453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m24453_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m24453_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24467_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24467_gp_0_0_0_0_Types[] = { &Array_Sort_m24467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24467_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m24467_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24468_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24468_gp_0_0_0_0_Types[] = { &Array_Sort_m24468_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24468_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m24468_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24471_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24471_gp_0_0_0_0_Types[] = { &Array_Sort_m24471_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24471_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m24471_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24472_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24472_gp_0_0_0_0_Types[] = { &Array_Sort_m24472_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24472_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m24472_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24473_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24473_gp_0_0_0_0_Types[] = { &Array_Sort_m24473_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24473_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m24473_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24474_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24474_gp_0_0_0_0_Types[] = { &Array_Sort_m24474_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24474_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m24474_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m24475_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m24475_gp_0_0_0_0_Types[] = { &Array_qsort_m24475_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m24475_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m24475_gp_0_0_0_0_Types };
extern const Il2CppType Array_compare_m24476_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m24476_gp_0_0_0_0_Types[] = { &Array_compare_m24476_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m24476_gp_0_0_0_0 = { 1, GenInst_Array_compare_m24476_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m24477_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m24477_gp_0_0_0_0_Types[] = { &Array_qsort_m24477_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m24477_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m24477_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m24482_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m24482_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m24482_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m24482_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m24482_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m24483_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m24483_gp_0_0_0_0_Types[] = { &Array_ForEach_m24483_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m24483_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m24483_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m24484_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m24484_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m24484_gp_0_0_0_0_Array_ConvertAll_m24484_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m24484_gp_0_0_0_0, &Array_ConvertAll_m24484_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m24484_gp_0_0_0_0_Array_ConvertAll_m24484_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m24484_gp_0_0_0_0_Array_ConvertAll_m24484_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m24485_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m24485_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m24485_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m24485_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m24485_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m24486_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m24486_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m24486_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m24486_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m24486_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m24487_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m24487_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m24487_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m24487_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m24487_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m24488_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m24488_gp_0_0_0_0_Types[] = { &Array_FindIndex_m24488_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m24488_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m24488_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m24489_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m24489_gp_0_0_0_0_Types[] = { &Array_FindIndex_m24489_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m24489_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m24489_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m24490_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m24490_gp_0_0_0_0_Types[] = { &Array_FindIndex_m24490_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m24490_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m24490_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m24492_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m24492_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m24492_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m24492_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m24492_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m24494_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m24494_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m24494_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m24494_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m24494_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m24501_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m24501_gp_0_0_0_0_Types[] = { &Array_FindAll_m24501_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m24501_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m24501_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m24502_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m24502_gp_0_0_0_0_Types[] = { &Array_Exists_m24502_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m24502_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m24502_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m24503_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m24503_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m24503_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m24503_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m24503_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m24504_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m24504_gp_0_0_0_0_Types[] = { &Array_Find_m24504_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m24504_gp_0_0_0_0 = { 1, GenInst_Array_Find_m24504_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m24505_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m24505_gp_0_0_0_0_Types[] = { &Array_FindLast_m24505_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m24505_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m24505_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t3436_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t3436_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t3436_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3436_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3436_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t3437_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3437_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t3437_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3437_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3437_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t2194_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t2194_gp_0_0_0_0_Types[] = { &Nullable_1_t2194_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t2194_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t2194_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t3446_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t3446_gp_0_0_0_0_Types[] = { &Comparer_1_t3446_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t3446_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t3446_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t3391_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t3391_gp_0_0_0_0_Types[] = { &GenericComparer_1_t3391_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3391_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3391_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3448_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3448_gp_0_0_0_0_Types[] = { &Dictionary_2_t3448_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3448_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3448_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3448_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Types[] = { &Dictionary_2_t3448_gp_0_0_0_0, &Dictionary_2_t3448_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3448_gp_1_0_0_0_Types[] = { &Dictionary_2_t3448_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3448_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3448_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3820_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3820_0_0_0_Types[] = { &KeyValuePair_2_t3820_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3820_0_0_0 = { 1, GenInst_KeyValuePair_2_t3820_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m24657_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m24657_gp_0_0_0_0_Types[] = { &Dictionary_2_t3448_gp_0_0_0_0, &Dictionary_2_t3448_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m24657_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m24657_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m24657_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0_Types[] = { &Dictionary_2_t3448_gp_0_0_0_0, &Dictionary_2_t3448_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_DictionaryEntry_t552_0_0_0_Types[] = { &Dictionary_2_t3448_gp_0_0_0_0, &Dictionary_2_t3448_gp_1_0_0_0, &DictionaryEntry_t552_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_DictionaryEntry_t552_0_0_0 = { 3, GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_DictionaryEntry_t552_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3449_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3449_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3449_gp_0_0_0_0_ShimEnumerator_t3449_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3449_gp_0_0_0_0, &ShimEnumerator_t3449_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3449_gp_0_0_0_0_ShimEnumerator_t3449_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3449_gp_0_0_0_0_ShimEnumerator_t3449_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3450_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3450_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3450_gp_0_0_0_0_Enumerator_t3450_gp_1_0_0_0_Types[] = { &Enumerator_t3450_gp_0_0_0_0, &Enumerator_t3450_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3450_gp_0_0_0_0_Enumerator_t3450_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3450_gp_0_0_0_0_Enumerator_t3450_gp_1_0_0_0_Types };
extern const Il2CppType KeyCollection_t3451_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3451_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0_Types[] = { &KeyCollection_t3451_gp_0_0_0_0, &KeyCollection_t3451_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3451_gp_0_0_0_0_Types[] = { &KeyCollection_t3451_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3451_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3451_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3452_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3452_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3452_gp_0_0_0_0_Enumerator_t3452_gp_1_0_0_0_Types[] = { &Enumerator_t3452_gp_0_0_0_0, &Enumerator_t3452_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3452_gp_0_0_0_0_Enumerator_t3452_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3452_gp_0_0_0_0_Enumerator_t3452_gp_1_0_0_0_Types };
extern const Il2CppType ValueCollection_t3453_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t3453_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0_Types[] = { &ValueCollection_t3453_gp_0_0_0_0, &ValueCollection_t3453_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3453_gp_1_0_0_0_Types[] = { &ValueCollection_t3453_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3453_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t3453_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t3454_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3454_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3454_gp_0_0_0_0_Enumerator_t3454_gp_1_0_0_0_Types[] = { &Enumerator_t3454_gp_0_0_0_0, &Enumerator_t3454_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3454_gp_0_0_0_0_Enumerator_t3454_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3454_gp_0_0_0_0_Enumerator_t3454_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t3456_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t3456_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t3456_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t3456_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t3456_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t3390_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t3390_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t3390_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t3390_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t3390_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3459_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3459_gp_0_0_0_0_Types[] = { &IDictionary_2_t3459_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3459_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3459_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3459_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3459_gp_1_0_0_0_Types[] = { &IDictionary_2_t3459_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3459_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t3459_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t3462_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t3462_gp_0_0_0_0_Types[] = { &List_1_t3462_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3462_gp_0_0_0_0 = { 1, GenInst_List_1_t3462_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3463_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3463_gp_0_0_0_0_Types[] = { &Enumerator_t3463_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3463_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3463_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t3464_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t3464_gp_0_0_0_0_Types[] = { &Collection_1_t3464_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t3464_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3464_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t3465_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t3465_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t3465_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3465_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3465_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m24939_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m24939_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m24939_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m24939_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m24939_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m24939_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m24939_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m24939_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m24939_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m24939_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m24940_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m24940_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m24940_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m24940_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m24940_gp_0_0_0_0_Types };
extern const Il2CppType SA_Singleton_1_t3392_gp_0_0_0_0;
static const Il2CppType* GenInst_SA_Singleton_1_t3392_gp_0_0_0_0_Types[] = { &SA_Singleton_1_t3392_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SA_Singleton_1_t3392_gp_0_0_0_0 = { 1, GenInst_SA_Singleton_1_t3392_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m24101_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m24101_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m24101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m24101_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m24101_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m24102_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m24102_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m24102_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m24102_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m24102_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m24103_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m24103_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m24103_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m24103_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m24103_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t3395_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t3395_gp_0_0_0_0_Types[] = { &TweenRunner_1_t3395_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t3395_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t3395_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m24183_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m24183_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m24183_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m24183_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m24183_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m24185_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m24185_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m24185_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m24185_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m24185_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3408_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3408_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3408_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3408_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3408_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3408_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3408_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3408_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3409_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3409_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3409_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3409_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3409_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3409_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3409_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3409_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3409_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3409_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3409_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3409_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3410_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3410_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3410_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3410_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3410_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3410_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3410_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3410_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3410_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3410_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3410_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3410_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3410_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3410_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3410_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3410_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t1168_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t1168_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t1168_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1168_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t1168_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t3412_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t3412_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t3412_gp_0_0_0_0_UnityEvent_2_t3412_gp_1_0_0_0_Types[] = { &UnityEvent_2_t3412_gp_0_0_0_0, &UnityEvent_2_t3412_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t3412_gp_0_0_0_0_UnityEvent_2_t3412_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t3412_gp_0_0_0_0_UnityEvent_2_t3412_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t3413_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3413_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3413_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t3413_gp_0_0_0_0_UnityEvent_3_t3413_gp_1_0_0_0_UnityEvent_3_t3413_gp_2_0_0_0_Types[] = { &UnityEvent_3_t3413_gp_0_0_0_0, &UnityEvent_3_t3413_gp_1_0_0_0, &UnityEvent_3_t3413_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t3413_gp_0_0_0_0_UnityEvent_3_t3413_gp_1_0_0_0_UnityEvent_3_t3413_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t3413_gp_0_0_0_0_UnityEvent_3_t3413_gp_1_0_0_0_UnityEvent_3_t3413_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t3414_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t3414_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t3414_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t3414_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t3414_gp_0_0_0_0_UnityEvent_4_t3414_gp_1_0_0_0_UnityEvent_4_t3414_gp_2_0_0_0_UnityEvent_4_t3414_gp_3_0_0_0_Types[] = { &UnityEvent_4_t3414_gp_0_0_0_0, &UnityEvent_4_t3414_gp_1_0_0_0, &UnityEvent_4_t3414_gp_2_0_0_0, &UnityEvent_4_t3414_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t3414_gp_0_0_0_0_UnityEvent_4_t3414_gp_1_0_0_0_UnityEvent_4_t3414_gp_2_0_0_0_UnityEvent_4_t3414_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t3414_gp_0_0_0_0_UnityEvent_4_t3414_gp_1_0_0_0_UnityEvent_4_t3414_gp_2_0_0_0_UnityEvent_4_t3414_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_PredicateOf_1_t3419_gp_0_0_0_0_Types[] = { &PredicateOf_1_t3419_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PredicateOf_1_t3419_gp_0_0_0_0 = { 1, GenInst_PredicateOf_1_t3419_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24465_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24465_gp_0_0_0_0_Array_Sort_m24465_gp_0_0_0_0_Types[] = { &Array_Sort_m24465_gp_0_0_0_0, &Array_Sort_m24465_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24465_gp_0_0_0_0_Array_Sort_m24465_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m24465_gp_0_0_0_0_Array_Sort_m24465_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24466_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m24466_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24466_gp_0_0_0_0_Array_Sort_m24466_gp_1_0_0_0_Types[] = { &Array_Sort_m24466_gp_0_0_0_0, &Array_Sort_m24466_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24466_gp_0_0_0_0_Array_Sort_m24466_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m24466_gp_0_0_0_0_Array_Sort_m24466_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m24467_gp_0_0_0_0_Array_Sort_m24467_gp_0_0_0_0_Types[] = { &Array_Sort_m24467_gp_0_0_0_0, &Array_Sort_m24467_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24467_gp_0_0_0_0_Array_Sort_m24467_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m24467_gp_0_0_0_0_Array_Sort_m24467_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24468_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24468_gp_0_0_0_0_Array_Sort_m24468_gp_1_0_0_0_Types[] = { &Array_Sort_m24468_gp_0_0_0_0, &Array_Sort_m24468_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24468_gp_0_0_0_0_Array_Sort_m24468_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m24468_gp_0_0_0_0_Array_Sort_m24468_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m24469_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24469_gp_0_0_0_0_Array_Sort_m24469_gp_0_0_0_0_Types[] = { &Array_Sort_m24469_gp_0_0_0_0, &Array_Sort_m24469_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24469_gp_0_0_0_0_Array_Sort_m24469_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m24469_gp_0_0_0_0_Array_Sort_m24469_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24470_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m24470_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24470_gp_0_0_0_0_Array_Sort_m24470_gp_1_0_0_0_Types[] = { &Array_Sort_m24470_gp_0_0_0_0, &Array_Sort_m24470_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24470_gp_0_0_0_0_Array_Sort_m24470_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m24470_gp_0_0_0_0_Array_Sort_m24470_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m24471_gp_0_0_0_0_Array_Sort_m24471_gp_0_0_0_0_Types[] = { &Array_Sort_m24471_gp_0_0_0_0, &Array_Sort_m24471_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24471_gp_0_0_0_0_Array_Sort_m24471_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m24471_gp_0_0_0_0_Array_Sort_m24471_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m24472_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m24472_gp_1_0_0_0_Types[] = { &Array_Sort_m24472_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24472_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m24472_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m24472_gp_0_0_0_0_Array_Sort_m24472_gp_1_0_0_0_Types[] = { &Array_Sort_m24472_gp_0_0_0_0, &Array_Sort_m24472_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m24472_gp_0_0_0_0_Array_Sort_m24472_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m24472_gp_0_0_0_0_Array_Sort_m24472_gp_1_0_0_0_Types };
extern const Il2CppType Array_qsort_m24475_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m24475_gp_0_0_0_0_Array_qsort_m24475_gp_1_0_0_0_Types[] = { &Array_qsort_m24475_gp_0_0_0_0, &Array_qsort_m24475_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m24475_gp_0_0_0_0_Array_qsort_m24475_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m24475_gp_0_0_0_0_Array_qsort_m24475_gp_1_0_0_0_Types };
extern const Il2CppType Array_Resize_m24480_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m24480_gp_0_0_0_0_Types[] = { &Array_Resize_m24480_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m24480_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m24480_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m24491_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m24491_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m24491_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m24491_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m24491_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m24493_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m24493_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m24493_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m24493_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m24493_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m24495_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m24495_gp_0_0_0_0_Types[] = { &Array_IndexOf_m24495_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m24495_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m24495_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m24496_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m24496_gp_0_0_0_0_Types[] = { &Array_IndexOf_m24496_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m24496_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m24496_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m24497_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m24497_gp_0_0_0_0_Types[] = { &Array_IndexOf_m24497_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m24497_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m24497_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m24498_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m24498_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m24498_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m24498_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m24498_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m24499_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m24499_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m24499_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m24499_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m24499_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m24500_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m24500_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m24500_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m24500_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m24500_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t3435_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t3435_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t3435_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3435_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3435_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t3438_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t3438_gp_0_0_0_0_Types[] = { &IList_1_t3438_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3438_gp_0_0_0_0 = { 1, GenInst_IList_1_t3438_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t3439_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3439_gp_0_0_0_0_Types[] = { &ICollection_1_t3439_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3439_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t3439_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3447_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3447_gp_0_0_0_0_Types[] = { &DefaultComparer_t3447_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3447_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3447_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0_Types[] = { &Dictionary_2_t3448_gp_0_0_0_0, &Dictionary_2_t3448_gp_1_0_0_0, &KeyValuePair_2_t3820_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0 = { 3, GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0_Types[] = { &KeyValuePair_2_t3820_0_0_0, &KeyValuePair_2_t3820_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0 = { 2, GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3829_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3829_0_0_0_Types[] = { &KeyValuePair_2_t3829_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3829_0_0_0 = { 1, GenInst_KeyValuePair_2_t3829_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0_KeyCollection_t3451_gp_0_0_0_0_Types[] = { &KeyCollection_t3451_gp_0_0_0_0, &KeyCollection_t3451_gp_1_0_0_0, &KeyCollection_t3451_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0_KeyCollection_t3451_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0_KeyCollection_t3451_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_0_0_0_0_Types[] = { &KeyCollection_t3451_gp_0_0_0_0, &KeyCollection_t3451_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3452_gp_0_0_0_0_Types[] = { &Enumerator_t3452_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3452_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3452_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0_Types[] = { &ValueCollection_t3453_gp_0_0_0_0, &ValueCollection_t3453_gp_1_0_0_0, &ValueCollection_t3453_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0_Types[] = { &ValueCollection_t3453_gp_1_0_0_0, &ValueCollection_t3453_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t3454_gp_1_0_0_0_Types[] = { &Enumerator_t3454_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3454_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3454_gp_1_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3457_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3457_gp_0_0_0_0_Types[] = { &DefaultComparer_t3457_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3457_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3457_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4658_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4658_0_0_0_Types[] = { &KeyValuePair_2_t4658_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4658_0_0_0 = { 1, GenInst_KeyValuePair_2_t4658_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t3459_gp_0_0_0_0_IDictionary_2_t3459_gp_1_0_0_0_Types[] = { &IDictionary_2_t3459_gp_0_0_0_0, &IDictionary_2_t3459_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3459_gp_0_0_0_0_IDictionary_2_t3459_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3459_gp_0_0_0_0_IDictionary_2_t3459_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3461_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t3461_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3461_gp_0_0_0_0_KeyValuePair_2_t3461_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t3461_gp_0_0_0_0, &KeyValuePair_2_t3461_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3461_gp_0_0_0_0_KeyValuePair_2_t3461_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t3461_gp_0_0_0_0_KeyValuePair_2_t3461_gp_1_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[686] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Text_t212_0_0_0,
	&GenInst_Renderer_t312_0_0_0,
	&GenInst_CardboardOnGUIWindow_t224_0_0_0,
	&GenInst_CardboardOnGUIMouse_t222_0_0_0,
	&GenInst_Collider_t319_0_0_0,
	&GenInst_MeshRenderer_t223_0_0_0,
	&GenInst_Cardboard_t233_0_0_0,
	&GenInst_StereoController_t235_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_CardboardPreRender_t247_0_0_0,
	&GenInst_CardboardPostRender_t246_0_0_0,
	&GenInst_CardboardHead_t244_0_0_0,
	&GenInst_Camera_t14_0_0_0,
	&GenInst_StereoRenderEffect_t239_0_0_0,
	&GenInst_ISelectHandler_t487_0_0_0,
	&GenInst_IUpdateSelectedHandler_t489_0_0_0,
	&GenInst_IBeginDragHandler_t493_0_0_0,
	&GenInst_IPointerUpHandler_t495_0_0_0,
	&GenInst_IDragHandler_t497_0_0_0,
	&GenInst_IPointerClickHandler_t499_0_0_0,
	&GenInst_IDropHandler_t501_0_0_0,
	&GenInst_IEndDragHandler_t503_0_0_0,
	&GenInst_IPointerDownHandler_t505_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t507_0_0_0,
	&GenInst_CardboardEye_t240_0_0_0,
	&GenInst_CardboardEye_t240_0_0_0_Boolean_t536_0_0_0,
	&GenInst_CardboardEye_t240_0_0_0_CardboardHead_t244_0_0_0,
	&GenInst_Int32_t478_0_0_0,
	&GenInst_MNDialogResult_t282_0_0_0,
	&GenInst_MNAndroidDialog_t285_0_0_0,
	&GenInst_MNAndroidMessage_t286_0_0_0,
	&GenInst_MNAndroidRateUsPopUp_t287_0_0_0,
	&GenInst_MNIOSDialog_t288_0_0_0,
	&GenInst_MNIOSMessage_t289_0_0_0,
	&GenInst_MNIOSRateUsPopUp_t290_0_0_0,
	&GenInst_MNWP8Dialog_t292_0_0_0,
	&GenInst_MNWP8Message_t293_0_0_0,
	&GenInst_MNWP8RateUsPopUp_t294_0_0_0,
	&GenInst_String_t_0_0_0_Char_t526_0_0_0,
	&GenInst_Char_t526_0_0_0,
	&GenInst_Texture2D_t9_0_0_0,
	&GenInst_SA_ScreenShotMaker_t300_0_0_0,
	&GenInst_WWWTextureLoader_t305_0_0_0,
	&GenInst_NavMeshAgent_t307_0_0_0,
	&GenInst_AudioSource_t339_0_0_0,
	&GenInst_Rigidbody_t325_0_0_0,
	&GenInst_GoTo_t315_0_0_0,
	&GenInst_Light_t318_0_0_0,
	&GenInst_Animator_t321_0_0_0,
	&GenInst_AICharacterControl_t308_0_0_0,
	&GenInst_SkinnedMeshRenderer_t532_0_0_0,
	&GenInst_ParticleSystem_t534_0_0_0,
	&GenInst_Cloth_t337_0_0_0,
	&GenInst_InteractiveObject_t328_0_0_0,
	&GenInst_CameraFilterPack_FX_Drunk_t124_0_0_0,
	&GenInst_GameObject_t256_0_0_0,
	&GenInst_InteractiveMama_t330_0_0_0,
	&GenInst_InteractiveParticles_t334_0_0_0,
	&GenInst_InteractiveRadio_t340_0_0_0,
	&GenInst_Teddy_t367_0_0_0,
	&GenInst_InteractiveBaby_t324_0_0_0,
	&GenInst_InteractivePlasticBag_t335_0_0_0,
	&GenInst_InteractiveDroppingObject_t327_0_0_0,
	&GenInst_InteractiveBurnMama_t326_0_0_0,
	&GenInst_FadeTexture_t313_0_0_0,
	&GenInst_TextAppearance_t368_0_0_0,
	&GenInst_InteractiveFlashlight_t329_0_0_0,
	&GenInst_LightController_t343_0_0_0,
	&GenInst_MovingObjects_t375_0_0_0,
	&GenInst_DoorController_t311_0_0_0,
	&GenInst_SceneLoader_t382_0_0_0,
	&GenInst_SFX_t385_0_0_0,
	&GenInst_PlayerController_t356_0_0_0,
	&GenInst_TextMesh_t397_0_0_0,
	&GenInst_Loader_t346_0_0_0,
	&GenInst_BoxCollider_t535_0_0_0,
	&GenInst_BodyPart_t400_0_0_0,
	&GenInst_RagdollPartScript_t404_0_0_0,
	&GenInst_RagdollHelper_t402_0_0_0,
	&GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0,
	&GenInst_GUITexture_t548_0_0_0,
	&GenInst_GUIText_t549_0_0_0,
	&GenInst_Vector3_t215_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t478_0_0_0,
	&GenInst_iTween_t432_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0,
	&GenInst_iTweenEvent_t443_0_0_0,
	&GenInst_iTweenEvent_t443_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Single_t531_0_0_0,
	&GenInst_Boolean_t536_0_0_0,
	&GenInst_Color_t6_0_0_0,
	&GenInst_Space_t546_0_0_0,
	&GenInst_EaseType_t425_0_0_0,
	&GenInst_LoopType_t426_0_0_0,
	&GenInst_Transform_t243_0_0_0,
	&GenInst_AudioClip_t472_0_0_0,
	&GenInst_ArrayIndexes_t441_0_0_0,
	&GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0,
	&GenInst_BaseInputModule_t259_0_0_0,
	&GenInst_RaycastResult_t511_0_0_0,
	&GenInst_IDeselectHandler_t763_0_0_0,
	&GenInst_BaseEventData_t490_0_0_0,
	&GenInst_Entry_t594_0_0_0,
	&GenInst_IPointerEnterHandler_t760_0_0_0,
	&GenInst_IPointerExitHandler_t761_0_0_0,
	&GenInst_IScrollHandler_t762_0_0_0,
	&GenInst_IMoveHandler_t764_0_0_0,
	&GenInst_ISubmitHandler_t765_0_0_0,
	&GenInst_ICancelHandler_t766_0_0_0,
	&GenInst_List_1_t767_0_0_0,
	&GenInst_IEventSystemHandler_t2261_0_0_0,
	&GenInst_PointerEventData_t257_0_0_0,
	&GenInst_AxisEventData_t612_0_0_0,
	&GenInst_BaseRaycaster_t611_0_0_0,
	&GenInst_EventSystem_t509_0_0_0,
	&GenInst_ButtonState_t616_0_0_0,
	&GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0,
	&GenInst_SpriteRenderer_t786_0_0_0,
	&GenInst_RaycastHit_t482_0_0_0,
	&GenInst_ICanvasElement_t770_0_0_0,
	&GenInst_Font_t643_0_0_0_List_1_t792_0_0_0,
	&GenInst_Font_t643_0_0_0,
	&GenInst_ColorTween_t630_0_0_0,
	&GenInst_List_1_t687_0_0_0,
	&GenInst_UIVertex_t685_0_0_0,
	&GenInst_RectTransform_t648_0_0_0,
	&GenInst_Canvas_t650_0_0_0,
	&GenInst_CanvasRenderer_t649_0_0_0,
	&GenInst_Component_t477_0_0_0,
	&GenInst_Graphic_t654_0_0_0,
	&GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0,
	&GenInst_Sprite_t668_0_0_0,
	&GenInst_Type_t661_0_0_0,
	&GenInst_FillMethod_t662_0_0_0,
	&GenInst_SubmitEvent_t675_0_0_0,
	&GenInst_OnChangeEvent_t677_0_0_0,
	&GenInst_OnValidateInput_t679_0_0_0,
	&GenInst_ContentType_t671_0_0_0,
	&GenInst_LineType_t674_0_0_0,
	&GenInst_InputType_t672_0_0_0,
	&GenInst_TouchScreenKeyboardType_t805_0_0_0,
	&GenInst_CharacterValidation_t673_0_0_0,
	&GenInst_UILineInfo_t809_0_0_0,
	&GenInst_UICharInfo_t811_0_0_0,
	&GenInst_LayoutElement_t740_0_0_0,
	&GenInst_Direction_t692_0_0_0,
	&GenInst_Vector2_t7_0_0_0,
	&GenInst_CanvasGroup_t797_0_0_0,
	&GenInst_Selectable_t636_0_0_0,
	&GenInst_Navigation_t690_0_0_0,
	&GenInst_Transition_t704_0_0_0,
	&GenInst_ColorBlock_t642_0_0_0,
	&GenInst_SpriteState_t708_0_0_0,
	&GenInst_AnimationTriggers_t631_0_0_0,
	&GenInst_Direction_t710_0_0_0,
	&GenInst_Image_t669_0_0_0,
	&GenInst_MatEntry_t714_0_0_0,
	&GenInst_Toggle_t721_0_0_0,
	&GenInst_Toggle_t721_0_0_0_Boolean_t536_0_0_0,
	&GenInst_AspectMode_t725_0_0_0,
	&GenInst_FitMode_t731_0_0_0,
	&GenInst_Corner_t733_0_0_0,
	&GenInst_Axis_t734_0_0_0,
	&GenInst_Constraint_t735_0_0_0,
	&GenInst_RectOffset_t741_0_0_0,
	&GenInst_TextAnchor_t819_0_0_0,
	&GenInst_ILayoutElement_t776_0_0_0_Single_t531_0_0_0,
	&GenInst_List_1_t777_0_0_0,
	&GenInst_List_1_t775_0_0_0,
	&GenInst_GcLeaderboard_t850_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t1107_0_0_0,
	&GenInst_IAchievementU5BU5D_t1109_0_0_0,
	&GenInst_IScoreU5BU5D_t1037_0_0_0,
	&GenInst_IUserProfileU5BU5D_t1031_0_0_0,
	&GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0,
	&GenInst_GUILayoutEntry_t877_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_ByteU5BU5D_t469_0_0_0,
	&GenInst_Rigidbody2D_t933_0_0_0,
	&GenInst_MatchDirectConnectInfo_t972_0_0_0,
	&GenInst_MatchDesc_t974_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0,
	&GenInst_CreateMatchResponse_t966_0_0_0,
	&GenInst_JoinMatchResponse_t968_0_0_0,
	&GenInst_BasicResponse_t963_0_0_0,
	&GenInst_ListMatchResponse_t976_0_0_0,
	&GenInst_KeyValuePair_2_t446_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0,
	&GenInst_Type_t_0_0_0_SetDelegate_t994_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst_KeyValuePair_2_t1149_0_0_0,
	&GenInst_ConstructorInfo_t996_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t862_0_0_0,
	&GenInst_PersistentCall_t1064_0_0_0,
	&GenInst_BaseInvokableCall_t1061_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t536_0_0_0,
	&GenInst_StrongName_t2011_0_0_0,
	&GenInst_DateTime_t871_0_0_0,
	&GenInst_DateTimeOffset_t1148_0_0_0,
	&GenInst_TimeSpan_t1437_0_0_0,
	&GenInst_Guid_t555_0_0_0,
	&GenInst_IFormattable_t2190_0_0_0,
	&GenInst_IConvertible_t2193_0_0_0,
	&GenInst_IComparable_t2192_0_0_0,
	&GenInst_IComparable_1_t3749_0_0_0,
	&GenInst_IEquatable_1_t3750_0_0_0,
	&GenInst_ValueType_t1540_0_0_0,
	&GenInst_MonoBehaviour_t4_0_0_0,
	&GenInst_Behaviour_t825_0_0_0,
	&GenInst_Object_t473_0_0_0,
	&GenInst_IReflect_t3442_0_0_0,
	&GenInst__Type_t3440_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t2188_0_0_0,
	&GenInst__MemberInfo_t3441_0_0_0,
	&GenInst_IComparable_1_t3729_0_0_0,
	&GenInst_IEquatable_1_t3730_0_0_0,
	&GenInst_Double_t553_0_0_0,
	&GenInst_IComparable_1_t3751_0_0_0,
	&GenInst_IEquatable_1_t3752_0_0_0,
	&GenInst_IComparable_1_t3745_0_0_0,
	&GenInst_IEquatable_1_t3746_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Byte_t1117_0_0_0,
	&GenInst_IComparable_1_t3737_0_0_0,
	&GenInst_IEquatable_1_t3738_0_0_0,
	&GenInst_Object_t_0_0_0_Char_t526_0_0_0,
	&GenInst_GUILayoutOption_t882_0_0_0,
	&GenInst_String_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2322_0_0_0,
	&GenInst_TweenType_t442_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2328_0_0_0,
	&GenInst_TweenType_t442_0_0_0,
	&GenInst_Enum_t554_0_0_0,
	&GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_TweenType_t442_0_0_0,
	&GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_DictionaryEntry_t552_0_0_0,
	&GenInst_TweenType_t442_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2328_0_0_0,
	&GenInst_TweenType_t442_0_0_0_Dictionary_2_t545_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Dictionary_2_t545_0_0_0,
	&GenInst_KeyValuePair_2_t2343_0_0_0,
	&GenInst_KeyValuePair_2_t2350_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0,
	&GenInst_Rect_t225_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t478_0_0_0,
	&GenInst_KeyValuePair_2_t2364_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2364_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2378_0_0_0,
	&GenInst_Material_t2_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_IComparable_1_t3755_0_0_0,
	&GenInst_IEquatable_1_t3756_0_0_0,
	&GenInst_iTweenPath_t459_0_0_0,
	&GenInst_String_t_0_0_0_iTweenPath_t459_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2484_0_0_0,
	&GenInst_Int32_t478_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2514_0_0_0,
	&GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Int32_t478_0_0_0,
	&GenInst_Int32_t478_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t478_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Int32_t478_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2514_0_0_0,
	&GenInst_Int32_t478_0_0_0_PointerEventData_t257_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t784_0_0_0,
	&GenInst_RaycastHit2D_t789_0_0_0,
	&GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0,
	&GenInst_ICanvasElement_t770_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Font_t643_0_0_0_List_1_t792_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_List_1_t792_0_0_0,
	&GenInst_KeyValuePair_2_t2549_0_0_0,
	&GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0,
	&GenInst_Graphic_t654_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Canvas_t650_0_0_0_IndexedSet_1_t803_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_IndexedSet_1_t803_0_0_0,
	&GenInst_KeyValuePair_2_t2580_0_0_0,
	&GenInst_KeyValuePair_2_t2584_0_0_0,
	&GenInst_KeyValuePair_2_t2588_0_0_0,
	&GenInst_LayoutRebuilder_t745_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t531_0_0_0,
	&GenInst_IAchievementDescription_t3146_0_0_0,
	&GenInst_IAchievement_t1082_0_0_0,
	&GenInst_IScore_t1036_0_0_0,
	&GenInst_IUserProfile_t3147_0_0_0,
	&GenInst_AchievementDescription_t1034_0_0_0,
	&GenInst_UserProfile_t1032_0_0_0,
	&GenInst_GcAchievementData_t1020_0_0_0,
	&GenInst_Achievement_t1033_0_0_0,
	&GenInst_GcScoreData_t1021_0_0_0,
	&GenInst_Score_t1035_0_0_0,
	&GenInst_Int32_t478_0_0_0_LayoutCache_t874_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_LayoutCache_t874_0_0_0,
	&GenInst_KeyValuePair_2_t2652_0_0_0,
	&GenInst_GUIStyle_t273_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t273_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2663_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t1118_0_0_0,
	&GenInst_Display_t918_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t2210_0_0_0,
	&GenInst_ContactPoint_t930_0_0_0,
	&GenInst_Keyframe_t943_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t1131_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t1131_0_0_0,
	&GenInst_KeyValuePair_2_t2706_0_0_0,
	&GenInst_Int64_t1131_0_0_0,
	&GenInst_IComparable_1_t3731_0_0_0,
	&GenInst_IEquatable_1_t3732_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_Int64_t1131_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t1131_0_0_0_KeyValuePair_2_t2706_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t1131_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2721_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2743_0_0_0,
	&GenInst_NetworkID_t979_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_NetworkID_t979_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2743_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_NetworkAccessToken_t981_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_NetworkAccessToken_t981_0_0_0,
	&GenInst_KeyValuePair_2_t2757_0_0_0,
	&GenInst_ConstructorDelegate_t995_0_0_0,
	&GenInst_GetDelegate_t993_0_0_0,
	&GenInst_IDictionary_2_t1098_0_0_0,
	&GenInst_KeyValuePair_2_t1147_0_0_0,
	&GenInst_IDictionary_2_t1099_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0,
	&GenInst_KeyValuePair_2_t2762_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t995_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2770_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t1098_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2774_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t1099_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2778_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t993_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2762_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t1147_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2800_0_0_0,
	&GenInst__ConstructorInfo_t3478_0_0_0,
	&GenInst_MethodBase_t1163_0_0_0,
	&GenInst__MethodBase_t3481_0_0_0,
	&GenInst_ParameterInfo_t1154_0_0_0,
	&GenInst__ParameterInfo_t3484_0_0_0,
	&GenInst__PropertyInfo_t3485_0_0_0,
	&GenInst__FieldInfo_t3480_0_0_0,
	&GenInst_DisallowMultipleComponent_t1010_0_0_0,
	&GenInst_Attribute_t903_0_0_0,
	&GenInst__Attribute_t3429_0_0_0,
	&GenInst_ExecuteInEditMode_t1013_0_0_0,
	&GenInst_RequireComponent_t1011_0_0_0,
	&GenInst_ParameterModifier_t1805_0_0_0,
	&GenInst_HitInfo_t1039_0_0_0,
	&GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2825_0_0_0,
	&GenInst_TextEditOp_t1057_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t1057_0_0_0_KeyValuePair_2_t2825_0_0_0,
	&GenInst_Event_t481_0_0_0,
	&GenInst_Event_t481_0_0_0_TextEditOp_t1057_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2839_0_0_0,
	&GenInst_KeySizes_t1195_0_0_0,
	&GenInst_UInt32_t1124_0_0_0,
	&GenInst_IComparable_1_t3733_0_0_0,
	&GenInst_IEquatable_1_t3734_0_0_0,
	&GenInst_BigInteger_t1199_0_0_0,
	&GenInst_X509Certificate_t1304_0_0_0,
	&GenInst_IDeserializationCallback_t2213_0_0_0,
	&GenInst_ClientCertificateType_t1308_0_0_0,
	&GenInst_UInt16_t1122_0_0_0,
	&GenInst_IComparable_1_t3743_0_0_0,
	&GenInst_IEquatable_1_t3744_0_0_0,
	&GenInst_KeyValuePair_2_t2877_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t536_0_0_0_KeyValuePair_2_t2877_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t536_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2888_0_0_0,
	&GenInst_X509ChainStatus_t1432_0_0_0,
	&GenInst_Capture_t1454_0_0_0,
	&GenInst_Group_t1373_0_0_0,
	&GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0,
	&GenInst_KeyValuePair_2_t2896_0_0_0,
	&GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_Int32_t478_0_0_0,
	&GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_Int32_t478_0_0_0_Int32_t478_0_0_0_KeyValuePair_2_t2896_0_0_0,
	&GenInst_Mark_t1479_0_0_0,
	&GenInst_UriScheme_t1516_0_0_0,
	&GenInst_UInt64_t1134_0_0_0,
	&GenInst_SByte_t1135_0_0_0,
	&GenInst_Int16_t1136_0_0_0,
	&GenInst_Decimal_t1133_0_0_0,
	&GenInst_Delegate_t480_0_0_0,
	&GenInst_ICloneable_t3433_0_0_0,
	&GenInst_IComparable_1_t3735_0_0_0,
	&GenInst_IEquatable_1_t3736_0_0_0,
	&GenInst_IComparable_1_t3741_0_0_0,
	&GenInst_IEquatable_1_t3742_0_0_0,
	&GenInst_IComparable_1_t3739_0_0_0,
	&GenInst_IEquatable_1_t3740_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3482_0_0_0,
	&GenInst_TableRange_t1580_0_0_0,
	&GenInst_TailoringInfo_t1583_0_0_0,
	&GenInst_Contraction_t1584_0_0_0,
	&GenInst_Level2Map_t1586_0_0_0,
	&GenInst_BigInteger_t1606_0_0_0,
	&GenInst_Slot_t1657_0_0_0,
	&GenInst_Slot_t1664_0_0_0,
	&GenInst_StackFrame_t1162_0_0_0,
	&GenInst_Calendar_t1678_0_0_0,
	&GenInst_ModuleBuilder_t1754_0_0_0,
	&GenInst__ModuleBuilder_t3472_0_0_0,
	&GenInst_Module_t1748_0_0_0,
	&GenInst__Module_t3483_0_0_0,
	&GenInst_ParameterBuilder_t1758_0_0_0,
	&GenInst__ParameterBuilder_t3473_0_0_0,
	&GenInst_TypeU5BU5D_t485_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_IEnumerable_t1097_0_0_0,
	&GenInst_ICollection_t1528_0_0_0,
	&GenInst_IList_t1487_0_0_0,
	&GenInst_ILTokenInfo_t1742_0_0_0,
	&GenInst_LabelData_t1744_0_0_0,
	&GenInst_LabelFixup_t1743_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1741_0_0_0,
	&GenInst_TypeBuilder_t1733_0_0_0,
	&GenInst__TypeBuilder_t3475_0_0_0,
	&GenInst_MethodBuilder_t1740_0_0_0,
	&GenInst__MethodBuilder_t3471_0_0_0,
	&GenInst_ConstructorBuilder_t1736_0_0_0,
	&GenInst__ConstructorBuilder_t3467_0_0_0,
	&GenInst_PropertyBuilder_t1759_0_0_0,
	&GenInst__PropertyBuilder_t3474_0_0_0,
	&GenInst_FieldBuilder_t1739_0_0_0,
	&GenInst__FieldBuilder_t3469_0_0_0,
	&GenInst_Header_t1876_0_0_0,
	&GenInst_ITrackingHandler_t2205_0_0_0,
	&GenInst_IContextAttribute_t2197_0_0_0,
	&GenInst_IComparable_1_t3890_0_0_0,
	&GenInst_IEquatable_1_t3891_0_0_0,
	&GenInst_IComparable_1_t3753_0_0_0,
	&GenInst_IEquatable_1_t3754_0_0_0,
	&GenInst_IComparable_1_t3900_0_0_0,
	&GenInst_IEquatable_1_t3901_0_0_0,
	&GenInst_TypeTag_t1930_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t522_0_0_0,
	&GenInst_RaycastResult_t511_0_0_0_RaycastResult_t511_0_0_0,
	&GenInst_Char_t526_0_0_0_Char_t526_0_0_0,
	&GenInst_TweenType_t442_0_0_0_TweenType_t442_0_0_0,
	&GenInst_DictionaryEntry_t552_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_KeyValuePair_2_t2328_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2328_0_0_0_KeyValuePair_2_t2328_0_0_0,
	&GenInst_KeyValuePair_2_t2350_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2350_0_0_0_KeyValuePair_2_t2350_0_0_0,
	&GenInst_KeyValuePair_2_t2364_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2364_0_0_0_KeyValuePair_2_t2364_0_0_0,
	&GenInst_Single_t531_0_0_0_Single_t531_0_0_0,
	&GenInst_Boolean_t536_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Vector3_t215_0_0_0_Vector3_t215_0_0_0,
	&GenInst_Color_t6_0_0_0_Color_t6_0_0_0,
	&GenInst_Space_t546_0_0_0_Space_t546_0_0_0,
	&GenInst_EaseType_t425_0_0_0_EaseType_t425_0_0_0,
	&GenInst_LoopType_t426_0_0_0_LoopType_t426_0_0_0,
	&GenInst_KeyValuePair_2_t2514_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2514_0_0_0_KeyValuePair_2_t2514_0_0_0,
	&GenInst_UIVertex_t685_0_0_0_UIVertex_t685_0_0_0,
	&GenInst_UICharInfo_t811_0_0_0_UICharInfo_t811_0_0_0,
	&GenInst_UILineInfo_t809_0_0_0_UILineInfo_t809_0_0_0,
	&GenInst_Int64_t1131_0_0_0_Object_t_0_0_0,
	&GenInst_Int64_t1131_0_0_0_Int64_t1131_0_0_0,
	&GenInst_KeyValuePair_2_t2706_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2706_0_0_0_KeyValuePair_2_t2706_0_0_0,
	&GenInst_NetworkID_t979_0_0_0_NetworkID_t979_0_0_0,
	&GenInst_KeyValuePair_2_t2743_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2743_0_0_0_KeyValuePair_2_t2743_0_0_0,
	&GenInst_KeyValuePair_2_t2762_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2762_0_0_0_KeyValuePair_2_t2762_0_0_0,
	&GenInst_TextEditOp_t1057_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t1057_0_0_0_TextEditOp_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2825_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2825_0_0_0_KeyValuePair_2_t2825_0_0_0,
	&GenInst_Boolean_t536_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2877_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2877_0_0_0_KeyValuePair_2_t2877_0_0_0,
	&GenInst_KeyValuePair_2_t2896_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2896_0_0_0_KeyValuePair_2_t2896_0_0_0,
	&GenInst_iTweenEvent_AddToList_m24076_gp_0_0_0_0,
	&GenInst_iTweenEvent_AddToList_m24077_gp_0_0_0_0,
	&GenInst_iTweenEvent_AddToList_m24078_gp_0_0_0_0,
	&GenInst_iTweenEvent_AddToList_m24079_gp_0_0_0_0,
	&GenInst_ExecuteEvents_Execute_m24098_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m24099_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3397_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t3397_gp_0_0_0_0_Int32_t478_0_0_0,
	&GenInst_ObjectPool_1_t3398_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m24184_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m24186_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m24188_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m24191_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m24193_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m24194_gp_0_0_0_0,
	&GenInst_ResponseBase_ParseJSONList_m24198_gp_0_0_0_0,
	&GenInst_NetworkMatch_ProcessMatchResponse_m24199_gp_0_0_0_0,
	&GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0_ThreadSafeDictionary_2_t3403_gp_1_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t3403_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t3403_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4583_0_0_0,
	&GenInst_InvokableCall_1_t3407_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3408_gp_0_0_0_0_InvokableCall_2_t3408_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3409_gp_0_0_0_0_InvokableCall_3_t3409_gp_1_0_0_0_InvokableCall_3_t3409_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3410_gp_0_0_0_0_InvokableCall_4_t3410_gp_1_0_0_0_InvokableCall_4_t3410_gp_2_0_0_0_InvokableCall_4_t3410_gp_3_0_0_0,
	&GenInst_UnityEvent_1_t3411_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m24292_gp_0_0_0_0,
	&GenInst_Enumerable_First_m24293_gp_0_0_0_0,
	&GenInst_Enumerable_First_m24293_gp_0_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m24294_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m24295_gp_0_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Enumerable_Repeat_m24296_gp_0_0_0_0,
	&GenInst_Enumerable_CreateRepeatIterator_m24297_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m24298_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m24298_gp_0_0_0_0_Enumerable_Select_m24298_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m24298_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m24299_gp_0_0_0_0_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m24299_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m24300_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m24301_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m24301_gp_0_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m24302_gp_0_0_0_0_Boolean_t536_0_0_0,
	&GenInst_PredicateOf_1_t3419_gp_0_0_0_0_Boolean_t536_0_0_0,
	&GenInst_U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0_Boolean_t536_0_0_0,
	&GenInst_Queue_1_t3424_gp_0_0_0_0,
	&GenInst_Enumerator_t3425_gp_0_0_0_0,
	&GenInst_Stack_1_t3426_gp_0_0_0_0,
	&GenInst_Enumerator_t3427_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t3434_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m24453_gp_0_0_0_0,
	&GenInst_Array_Sort_m24467_gp_0_0_0_0,
	&GenInst_Array_Sort_m24468_gp_0_0_0_0,
	&GenInst_Array_Sort_m24471_gp_0_0_0_0,
	&GenInst_Array_Sort_m24472_gp_0_0_0_0,
	&GenInst_Array_Sort_m24473_gp_0_0_0_0,
	&GenInst_Array_Sort_m24474_gp_0_0_0_0,
	&GenInst_Array_qsort_m24475_gp_0_0_0_0,
	&GenInst_Array_compare_m24476_gp_0_0_0_0,
	&GenInst_Array_qsort_m24477_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m24482_gp_0_0_0_0,
	&GenInst_Array_ForEach_m24483_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m24484_gp_0_0_0_0_Array_ConvertAll_m24484_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m24485_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m24486_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m24487_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m24488_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m24489_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m24490_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m24492_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m24494_gp_0_0_0_0,
	&GenInst_Array_FindAll_m24501_gp_0_0_0_0,
	&GenInst_Array_Exists_m24502_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m24503_gp_0_0_0_0,
	&GenInst_Array_Find_m24504_gp_0_0_0_0,
	&GenInst_Array_FindLast_m24505_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3436_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t3437_gp_0_0_0_0,
	&GenInst_Nullable_1_t2194_gp_0_0_0_0,
	&GenInst_Comparer_1_t3446_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3391_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3448_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0,
	&GenInst_Dictionary_2_t3448_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3820_0_0_0,
	&GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m24657_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_DictionaryEntry_t552_0_0_0,
	&GenInst_ShimEnumerator_t3449_gp_0_0_0_0_ShimEnumerator_t3449_gp_1_0_0_0,
	&GenInst_Enumerator_t3450_gp_0_0_0_0_Enumerator_t3450_gp_1_0_0_0,
	&GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0,
	&GenInst_KeyCollection_t3451_gp_0_0_0_0,
	&GenInst_Enumerator_t3452_gp_0_0_0_0_Enumerator_t3452_gp_1_0_0_0,
	&GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0,
	&GenInst_ValueCollection_t3453_gp_1_0_0_0,
	&GenInst_Enumerator_t3454_gp_0_0_0_0_Enumerator_t3454_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t3456_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t3390_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3459_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3459_gp_1_0_0_0,
	&GenInst_List_1_t3462_gp_0_0_0_0,
	&GenInst_Enumerator_t3463_gp_0_0_0_0,
	&GenInst_Collection_1_t3464_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3465_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m24939_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m24939_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m24940_gp_0_0_0_0,
	&GenInst_SA_Singleton_1_t3392_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m24101_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m24102_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m24103_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t3395_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m24183_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m24185_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3408_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3408_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3409_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3409_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3409_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3410_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3410_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3410_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3410_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t1168_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t3412_gp_0_0_0_0_UnityEvent_2_t3412_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t3413_gp_0_0_0_0_UnityEvent_3_t3413_gp_1_0_0_0_UnityEvent_3_t3413_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t3414_gp_0_0_0_0_UnityEvent_4_t3414_gp_1_0_0_0_UnityEvent_4_t3414_gp_2_0_0_0_UnityEvent_4_t3414_gp_3_0_0_0,
	&GenInst_PredicateOf_1_t3419_gp_0_0_0_0,
	&GenInst_Array_Sort_m24465_gp_0_0_0_0_Array_Sort_m24465_gp_0_0_0_0,
	&GenInst_Array_Sort_m24466_gp_0_0_0_0_Array_Sort_m24466_gp_1_0_0_0,
	&GenInst_Array_Sort_m24467_gp_0_0_0_0_Array_Sort_m24467_gp_0_0_0_0,
	&GenInst_Array_Sort_m24468_gp_0_0_0_0_Array_Sort_m24468_gp_1_0_0_0,
	&GenInst_Array_Sort_m24469_gp_0_0_0_0_Array_Sort_m24469_gp_0_0_0_0,
	&GenInst_Array_Sort_m24470_gp_0_0_0_0_Array_Sort_m24470_gp_1_0_0_0,
	&GenInst_Array_Sort_m24471_gp_0_0_0_0_Array_Sort_m24471_gp_0_0_0_0,
	&GenInst_Array_Sort_m24472_gp_1_0_0_0,
	&GenInst_Array_Sort_m24472_gp_0_0_0_0_Array_Sort_m24472_gp_1_0_0_0,
	&GenInst_Array_qsort_m24475_gp_0_0_0_0_Array_qsort_m24475_gp_1_0_0_0,
	&GenInst_Array_Resize_m24480_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m24491_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m24493_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m24495_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m24496_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m24497_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m24498_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m24499_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m24500_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3435_gp_0_0_0_0,
	&GenInst_IList_1_t3438_gp_0_0_0_0,
	&GenInst_ICollection_1_t3439_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3447_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m24662_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t3448_gp_0_0_0_0_Dictionary_2_t3448_gp_1_0_0_0_KeyValuePair_2_t3820_0_0_0,
	&GenInst_KeyValuePair_2_t3820_0_0_0_KeyValuePair_2_t3820_0_0_0,
	&GenInst_KeyValuePair_2_t3829_0_0_0,
	&GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_1_0_0_0_KeyCollection_t3451_gp_0_0_0_0,
	&GenInst_KeyCollection_t3451_gp_0_0_0_0_KeyCollection_t3451_gp_0_0_0_0,
	&GenInst_Enumerator_t3452_gp_0_0_0_0,
	&GenInst_ValueCollection_t3453_gp_0_0_0_0_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0,
	&GenInst_ValueCollection_t3453_gp_1_0_0_0_ValueCollection_t3453_gp_1_0_0_0,
	&GenInst_Enumerator_t3454_gp_1_0_0_0,
	&GenInst_DefaultComparer_t3457_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4658_0_0_0,
	&GenInst_IDictionary_2_t3459_gp_0_0_0_0_IDictionary_2_t3459_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3461_gp_0_0_0_0_KeyValuePair_2_t3461_gp_1_0_0_0,
};
