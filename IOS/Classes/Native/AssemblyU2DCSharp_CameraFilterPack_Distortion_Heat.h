﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Heat
struct  CameraFilterPack_Distortion_Heat_t89  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Heat::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Heat::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Heat::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Heat::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Heat::Distortion
	float ___Distortion_6;
};
struct CameraFilterPack_Distortion_Heat_t89_StaticFields{
	// System.Single CameraFilterPack_Distortion_Heat::ChangeDistortion
	float ___ChangeDistortion_7;
};
