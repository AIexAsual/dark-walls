﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Misc
struct Misc_t688;
// UnityEngine.Object
struct Object_t473;
struct Object_t473_marshaled;

// System.Void UnityEngine.UI.Misc::Destroy(UnityEngine.Object)
extern "C" void Misc_Destroy_m4018 (Object_t * __this /* static, unused */, Object_t473 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Misc::DestroyImmediate(UnityEngine.Object)
extern "C" void Misc_DestroyImmediate_m4019 (Object_t * __this /* static, unused */, Object_t473 * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
