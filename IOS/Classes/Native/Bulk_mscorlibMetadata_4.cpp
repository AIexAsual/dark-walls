﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Metadata Definition System.Runtime.InteropServices._EnumBuilder
extern TypeInfo _EnumBuilder_t3468_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EnumBuilder_t3468_0_0_0;
extern const Il2CppType _EnumBuilder_t3468_1_0_0;
struct _EnumBuilder_t3468;
const Il2CppTypeDefinitionMetadata _EnumBuilder_t3468_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _EnumBuilder_t3468_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EnumBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_EnumBuilder_t3468_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2917/* custom_attributes_cache */
	, &_EnumBuilder_t3468_0_0_0/* byval_arg */
	, &_EnumBuilder_t3468_1_0_0/* this_arg */
	, &_EnumBuilder_t3468_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._EventInfo
extern TypeInfo _EventInfo_t3479_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _EventInfo_t3479_0_0_0;
extern const Il2CppType _EventInfo_t3479_1_0_0;
struct _EventInfo_t3479;
const Il2CppTypeDefinitionMetadata _EventInfo_t3479_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _EventInfo_t3479_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_EventInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_EventInfo_t3479_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2918/* custom_attributes_cache */
	, &_EventInfo_t3479_0_0_0/* byval_arg */
	, &_EventInfo_t3479_1_0_0/* this_arg */
	, &_EventInfo_t3479_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldBuilder
extern TypeInfo _FieldBuilder_t3469_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldBuilder_t3469_0_0_0;
extern const Il2CppType _FieldBuilder_t3469_1_0_0;
struct _FieldBuilder_t3469;
const Il2CppTypeDefinitionMetadata _FieldBuilder_t3469_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _FieldBuilder_t3469_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_FieldBuilder_t3469_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2919/* custom_attributes_cache */
	, &_FieldBuilder_t3469_0_0_0/* byval_arg */
	, &_FieldBuilder_t3469_1_0_0/* this_arg */
	, &_FieldBuilder_t3469_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._FieldInfo
extern TypeInfo _FieldInfo_t3480_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _FieldInfo_t3480_0_0_0;
extern const Il2CppType _FieldInfo_t3480_1_0_0;
struct _FieldInfo_t3480;
const Il2CppTypeDefinitionMetadata _FieldInfo_t3480_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _FieldInfo_t3480_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_FieldInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_FieldInfo_t3480_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2920/* custom_attributes_cache */
	, &_FieldInfo_t3480_0_0_0/* byval_arg */
	, &_FieldInfo_t3480_1_0_0/* this_arg */
	, &_FieldInfo_t3480_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ILGenerator
extern TypeInfo _ILGenerator_t3470_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ILGenerator_t3470_0_0_0;
extern const Il2CppType _ILGenerator_t3470_1_0_0;
struct _ILGenerator_t3470;
const Il2CppTypeDefinitionMetadata _ILGenerator_t3470_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ILGenerator_t3470_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ILGenerator"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ILGenerator_t3470_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2921/* custom_attributes_cache */
	, &_ILGenerator_t3470_0_0_0/* byval_arg */
	, &_ILGenerator_t3470_1_0_0/* this_arg */
	, &_ILGenerator_t3470_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBase
extern TypeInfo _MethodBase_t3481_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBase_t3481_0_0_0;
extern const Il2CppType _MethodBase_t3481_1_0_0;
struct _MethodBase_t3481;
const Il2CppTypeDefinitionMetadata _MethodBase_t3481_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MethodBase_t3481_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBase"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MethodBase_t3481_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2922/* custom_attributes_cache */
	, &_MethodBase_t3481_0_0_0/* byval_arg */
	, &_MethodBase_t3481_1_0_0/* this_arg */
	, &_MethodBase_t3481_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodBuilder
extern TypeInfo _MethodBuilder_t3471_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodBuilder_t3471_0_0_0;
extern const Il2CppType _MethodBuilder_t3471_1_0_0;
struct _MethodBuilder_t3471;
const Il2CppTypeDefinitionMetadata _MethodBuilder_t3471_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MethodBuilder_t3471_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MethodBuilder_t3471_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2923/* custom_attributes_cache */
	, &_MethodBuilder_t3471_0_0_0/* byval_arg */
	, &_MethodBuilder_t3471_1_0_0/* this_arg */
	, &_MethodBuilder_t3471_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._MethodInfo
extern TypeInfo _MethodInfo_t3482_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _MethodInfo_t3482_0_0_0;
extern const Il2CppType _MethodInfo_t3482_1_0_0;
struct _MethodInfo_t3482;
const Il2CppTypeDefinitionMetadata _MethodInfo_t3482_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _MethodInfo_t3482_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_MethodInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_MethodInfo_t3482_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2924/* custom_attributes_cache */
	, &_MethodInfo_t3482_0_0_0/* byval_arg */
	, &_MethodInfo_t3482_1_0_0/* this_arg */
	, &_MethodInfo_t3482_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Module
extern TypeInfo _Module_t3483_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Module_t3483_0_0_0;
extern const Il2CppType _Module_t3483_1_0_0;
struct _Module_t3483;
const Il2CppTypeDefinitionMetadata _Module_t3483_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Module_t3483_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Module"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Module_t3483_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2925/* custom_attributes_cache */
	, &_Module_t3483_0_0_0/* byval_arg */
	, &_Module_t3483_1_0_0/* this_arg */
	, &_Module_t3483_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ModuleBuilder
extern TypeInfo _ModuleBuilder_t3472_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ModuleBuilder_t3472_0_0_0;
extern const Il2CppType _ModuleBuilder_t3472_1_0_0;
struct _ModuleBuilder_t3472;
const Il2CppTypeDefinitionMetadata _ModuleBuilder_t3472_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ModuleBuilder_t3472_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ModuleBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ModuleBuilder_t3472_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2926/* custom_attributes_cache */
	, &_ModuleBuilder_t3472_0_0_0/* byval_arg */
	, &_ModuleBuilder_t3472_1_0_0/* this_arg */
	, &_ModuleBuilder_t3472_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterBuilder
extern TypeInfo _ParameterBuilder_t3473_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterBuilder_t3473_0_0_0;
extern const Il2CppType _ParameterBuilder_t3473_1_0_0;
struct _ParameterBuilder_t3473;
const Il2CppTypeDefinitionMetadata _ParameterBuilder_t3473_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ParameterBuilder_t3473_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ParameterBuilder_t3473_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2927/* custom_attributes_cache */
	, &_ParameterBuilder_t3473_0_0_0/* byval_arg */
	, &_ParameterBuilder_t3473_1_0_0/* this_arg */
	, &_ParameterBuilder_t3473_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ParameterInfo
extern TypeInfo _ParameterInfo_t3484_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ParameterInfo_t3484_0_0_0;
extern const Il2CppType _ParameterInfo_t3484_1_0_0;
struct _ParameterInfo_t3484;
const Il2CppTypeDefinitionMetadata _ParameterInfo_t3484_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ParameterInfo_t3484_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ParameterInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ParameterInfo_t3484_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2928/* custom_attributes_cache */
	, &_ParameterInfo_t3484_0_0_0/* byval_arg */
	, &_ParameterInfo_t3484_1_0_0/* this_arg */
	, &_ParameterInfo_t3484_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyBuilder
extern TypeInfo _PropertyBuilder_t3474_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyBuilder_t3474_0_0_0;
extern const Il2CppType _PropertyBuilder_t3474_1_0_0;
struct _PropertyBuilder_t3474;
const Il2CppTypeDefinitionMetadata _PropertyBuilder_t3474_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _PropertyBuilder_t3474_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_PropertyBuilder_t3474_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2929/* custom_attributes_cache */
	, &_PropertyBuilder_t3474_0_0_0/* byval_arg */
	, &_PropertyBuilder_t3474_1_0_0/* this_arg */
	, &_PropertyBuilder_t3474_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._PropertyInfo
extern TypeInfo _PropertyInfo_t3485_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _PropertyInfo_t3485_0_0_0;
extern const Il2CppType _PropertyInfo_t3485_1_0_0;
struct _PropertyInfo_t3485;
const Il2CppTypeDefinitionMetadata _PropertyInfo_t3485_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _PropertyInfo_t3485_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_PropertyInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_PropertyInfo_t3485_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2930/* custom_attributes_cache */
	, &_PropertyInfo_t3485_0_0_0/* byval_arg */
	, &_PropertyInfo_t3485_1_0_0/* this_arg */
	, &_PropertyInfo_t3485_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Thread
extern TypeInfo _Thread_t3487_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Thread_t3487_0_0_0;
extern const Il2CppType _Thread_t3487_1_0_0;
struct _Thread_t3487;
const Il2CppTypeDefinitionMetadata _Thread_t3487_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Thread_t3487_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Thread"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Thread_t3487_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2931/* custom_attributes_cache */
	, &_Thread_t3487_0_0_0/* byval_arg */
	, &_Thread_t3487_1_0_0/* this_arg */
	, &_Thread_t3487_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._TypeBuilder
extern TypeInfo _TypeBuilder_t3475_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _TypeBuilder_t3475_0_0_0;
extern const Il2CppType _TypeBuilder_t3475_1_0_0;
struct _TypeBuilder_t3475;
const Il2CppTypeDefinitionMetadata _TypeBuilder_t3475_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _TypeBuilder_t3475_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_TypeBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_TypeBuilder_t3475_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2932/* custom_attributes_cache */
	, &_TypeBuilder_t3475_0_0_0/* byval_arg */
	, &_TypeBuilder_t3475_1_0_0/* this_arg */
	, &_TypeBuilder_t3475_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServic.h"
// Metadata Definition System.Runtime.Remoting.Activation.ActivationServices
extern TypeInfo ActivationServices_t1843_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ActivationServices
#include "mscorlib_System_Runtime_Remoting_Activation_ActivationServicMethodDeclarations.h"
static const EncodedMethodIndex ActivationServices_t1843_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationServices_t1843_0_0_0;
extern const Il2CppType ActivationServices_t1843_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct ActivationServices_t1843;
const Il2CppTypeDefinitionMetadata ActivationServices_t1843_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationServices_t1843_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8178/* fieldStart */
	, 11572/* methodStart */
	, -1/* eventStart */
	, 2276/* propertyStart */

};
TypeInfo ActivationServices_t1843_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationServices"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &ActivationServices_t1843_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ActivationServices_t1843_0_0_0/* byval_arg */
	, &ActivationServices_t1843_1_0_0/* this_arg */
	, &ActivationServices_t1843_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationServices_t1843)/* instance_size */
	, sizeof (ActivationServices_t1843)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ActivationServices_t1843_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAc.h"
// Metadata Definition System.Runtime.Remoting.Activation.AppDomainLevelActivator
extern TypeInfo AppDomainLevelActivator_t1844_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.AppDomainLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_AppDomainLevelAcMethodDeclarations.h"
static const EncodedMethodIndex AppDomainLevelActivator_t1844_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern const Il2CppType IActivator_t1842_0_0_0;
static const Il2CppType* AppDomainLevelActivator_t1844_InterfacesTypeInfos[] = 
{
	&IActivator_t1842_0_0_0,
};
static Il2CppInterfaceOffsetPair AppDomainLevelActivator_t1844_InterfacesOffsets[] = 
{
	{ &IActivator_t1842_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainLevelActivator_t1844_0_0_0;
extern const Il2CppType AppDomainLevelActivator_t1844_1_0_0;
struct AppDomainLevelActivator_t1844;
const Il2CppTypeDefinitionMetadata AppDomainLevelActivator_t1844_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AppDomainLevelActivator_t1844_InterfacesTypeInfos/* implementedInterfaces */
	, AppDomainLevelActivator_t1844_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainLevelActivator_t1844_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8179/* fieldStart */
	, 11577/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppDomainLevelActivator_t1844_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &AppDomainLevelActivator_t1844_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AppDomainLevelActivator_t1844_0_0_0/* byval_arg */
	, &AppDomainLevelActivator_t1844_1_0_0/* this_arg */
	, &AppDomainLevelActivator_t1844_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainLevelActivator_t1844)/* instance_size */
	, sizeof (AppDomainLevelActivator_t1844)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeve.h"
// Metadata Definition System.Runtime.Remoting.Activation.ConstructionLevelActivator
extern TypeInfo ConstructionLevelActivator_t1845_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ConstructionLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ConstructionLeveMethodDeclarations.h"
static const EncodedMethodIndex ConstructionLevelActivator_t1845_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
static const Il2CppType* ConstructionLevelActivator_t1845_InterfacesTypeInfos[] = 
{
	&IActivator_t1842_0_0_0,
};
static Il2CppInterfaceOffsetPair ConstructionLevelActivator_t1845_InterfacesOffsets[] = 
{
	{ &IActivator_t1842_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionLevelActivator_t1845_0_0_0;
extern const Il2CppType ConstructionLevelActivator_t1845_1_0_0;
struct ConstructionLevelActivator_t1845;
const Il2CppTypeDefinitionMetadata ConstructionLevelActivator_t1845_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionLevelActivator_t1845_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionLevelActivator_t1845_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConstructionLevelActivator_t1845_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11578/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConstructionLevelActivator_t1845_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &ConstructionLevelActivator_t1845_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionLevelActivator_t1845_0_0_0/* byval_arg */
	, &ConstructionLevelActivator_t1845_1_0_0/* this_arg */
	, &ConstructionLevelActivator_t1845_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionLevelActivator_t1845)/* instance_size */
	, sizeof (ConstructionLevelActivator_t1845)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActi.h"
// Metadata Definition System.Runtime.Remoting.Activation.ContextLevelActivator
extern TypeInfo ContextLevelActivator_t1846_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.ContextLevelActivator
#include "mscorlib_System_Runtime_Remoting_Activation_ContextLevelActiMethodDeclarations.h"
static const EncodedMethodIndex ContextLevelActivator_t1846_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
static const Il2CppType* ContextLevelActivator_t1846_InterfacesTypeInfos[] = 
{
	&IActivator_t1842_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextLevelActivator_t1846_InterfacesOffsets[] = 
{
	{ &IActivator_t1842_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextLevelActivator_t1846_0_0_0;
extern const Il2CppType ContextLevelActivator_t1846_1_0_0;
struct ContextLevelActivator_t1846;
const Il2CppTypeDefinitionMetadata ContextLevelActivator_t1846_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextLevelActivator_t1846_InterfacesTypeInfos/* implementedInterfaces */
	, ContextLevelActivator_t1846_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContextLevelActivator_t1846_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8181/* fieldStart */
	, 11579/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContextLevelActivator_t1846_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextLevelActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &ContextLevelActivator_t1846_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContextLevelActivator_t1846_0_0_0/* byval_arg */
	, &ContextLevelActivator_t1846_1_0_0/* this_arg */
	, &ContextLevelActivator_t1846_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextLevelActivator_t1846)/* instance_size */
	, sizeof (ContextLevelActivator_t1846)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IActivator
extern TypeInfo IActivator_t1842_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IActivator_t1842_1_0_0;
struct IActivator_t1842;
const Il2CppTypeDefinitionMetadata IActivator_t1842_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IActivator_t1842_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &IActivator_t1842_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2933/* custom_attributes_cache */
	, &IActivator_t1842_0_0_0/* byval_arg */
	, &IActivator_t1842_1_0_0/* this_arg */
	, &IActivator_t1842_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Activation.IConstructionCallMessage
extern TypeInfo IConstructionCallMessage_t2180_il2cpp_TypeInfo;
extern const Il2CppType IMessage_t1869_0_0_0;
extern const Il2CppType IMethodCallMessage_t2185_0_0_0;
extern const Il2CppType IMethodMessage_t1881_0_0_0;
static const Il2CppType* IConstructionCallMessage_t2180_InterfacesTypeInfos[] = 
{
	&IMessage_t1869_0_0_0,
	&IMethodCallMessage_t2185_0_0_0,
	&IMethodMessage_t1881_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IConstructionCallMessage_t2180_0_0_0;
extern const Il2CppType IConstructionCallMessage_t2180_1_0_0;
struct IConstructionCallMessage_t2180;
const Il2CppTypeDefinitionMetadata IConstructionCallMessage_t2180_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IConstructionCallMessage_t2180_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11580/* methodStart */
	, -1/* eventStart */
	, 2277/* propertyStart */

};
TypeInfo IConstructionCallMessage_t2180_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IConstructionCallMessage"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &IConstructionCallMessage_t2180_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2934/* custom_attributes_cache */
	, &IConstructionCallMessage_t2180_0_0_0/* byval_arg */
	, &IConstructionCallMessage_t2180_1_0_0/* this_arg */
	, &IConstructionCallMessage_t2180_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivator.h"
// Metadata Definition System.Runtime.Remoting.Activation.RemoteActivator
extern TypeInfo RemoteActivator_t1847_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.RemoteActivator
#include "mscorlib_System_Runtime_Remoting_Activation_RemoteActivatorMethodDeclarations.h"
static const EncodedMethodIndex RemoteActivator_t1847_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
static const Il2CppType* RemoteActivator_t1847_InterfacesTypeInfos[] = 
{
	&IActivator_t1842_0_0_0,
};
static Il2CppInterfaceOffsetPair RemoteActivator_t1847_InterfacesOffsets[] = 
{
	{ &IActivator_t1842_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemoteActivator_t1847_0_0_0;
extern const Il2CppType RemoteActivator_t1847_1_0_0;
extern const Il2CppType MarshalByRefObject_t1415_0_0_0;
struct RemoteActivator_t1847;
const Il2CppTypeDefinitionMetadata RemoteActivator_t1847_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemoteActivator_t1847_InterfacesTypeInfos/* implementedInterfaces */
	, RemoteActivator_t1847_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, RemoteActivator_t1847_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemoteActivator_t1847_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemoteActivator"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &RemoteActivator_t1847_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemoteActivator_t1847_0_0_0/* byval_arg */
	, &RemoteActivator_t1847_1_0_0/* this_arg */
	, &RemoteActivator_t1847_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemoteActivator_t1847)/* instance_size */
	, sizeof (RemoteActivator_t1847)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttribute.h"
// Metadata Definition System.Runtime.Remoting.Activation.UrlAttribute
extern TypeInfo UrlAttribute_t1848_il2cpp_TypeInfo;
// System.Runtime.Remoting.Activation.UrlAttribute
#include "mscorlib_System_Runtime_Remoting_Activation_UrlAttributeMethodDeclarations.h"
static const EncodedMethodIndex UrlAttribute_t1848_VTable[10] = 
{
	3747,
	601,
	3748,
	628,
	3749,
	3750,
	3751,
	3751,
	3749,
	3750,
};
extern const Il2CppType IContextAttribute_t2197_0_0_0;
extern const Il2CppType IContextProperty_t2183_0_0_0;
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair UrlAttribute_t1848_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2197_0_0_0, 4},
	{ &IContextProperty_t2183_0_0_0, 6},
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UrlAttribute_t1848_0_0_0;
extern const Il2CppType UrlAttribute_t1848_1_0_0;
extern const Il2CppType ContextAttribute_t1849_0_0_0;
struct UrlAttribute_t1848;
const Il2CppTypeDefinitionMetadata UrlAttribute_t1848_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UrlAttribute_t1848_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1849_0_0_0/* parent */
	, UrlAttribute_t1848_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8182/* fieldStart */
	, 11586/* methodStart */
	, -1/* eventStart */
	, 2282/* propertyStart */

};
TypeInfo UrlAttribute_t1848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UrlAttribute"/* name */
	, "System.Runtime.Remoting.Activation"/* namespaze */
	, NULL/* methods */
	, &UrlAttribute_t1848_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2935/* custom_attributes_cache */
	, &UrlAttribute_t1848_0_0_0/* byval_arg */
	, &UrlAttribute_t1848_1_0_0/* this_arg */
	, &UrlAttribute_t1848_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UrlAttribute_t1848)/* instance_size */
	, sizeof (UrlAttribute_t1848)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfo.h"
// Metadata Definition System.Runtime.Remoting.ChannelInfo
extern TypeInfo ChannelInfo_t1850_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelInfo
#include "mscorlib_System_Runtime_Remoting_ChannelInfoMethodDeclarations.h"
static const EncodedMethodIndex ChannelInfo_t1850_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3752,
};
extern const Il2CppType IChannelInfo_t1908_0_0_0;
static const Il2CppType* ChannelInfo_t1850_InterfacesTypeInfos[] = 
{
	&IChannelInfo_t1908_0_0_0,
};
static Il2CppInterfaceOffsetPair ChannelInfo_t1850_InterfacesOffsets[] = 
{
	{ &IChannelInfo_t1908_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelInfo_t1850_0_0_0;
extern const Il2CppType ChannelInfo_t1850_1_0_0;
struct ChannelInfo_t1850;
const Il2CppTypeDefinitionMetadata ChannelInfo_t1850_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ChannelInfo_t1850_InterfacesTypeInfos/* implementedInterfaces */
	, ChannelInfo_t1850_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelInfo_t1850_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8183/* fieldStart */
	, 11591/* methodStart */
	, -1/* eventStart */
	, 2283/* propertyStart */

};
TypeInfo ChannelInfo_t1850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ChannelInfo_t1850_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelInfo_t1850_0_0_0/* byval_arg */
	, &ChannelInfo_t1850_1_0_0/* this_arg */
	, &ChannelInfo_t1850_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelInfo_t1850)/* instance_size */
	, sizeof (ChannelInfo_t1850)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServices.h"
// Metadata Definition System.Runtime.Remoting.Channels.ChannelServices
extern TypeInfo ChannelServices_t1852_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.ChannelServices
#include "mscorlib_System_Runtime_Remoting_Channels_ChannelServicesMethodDeclarations.h"
static const EncodedMethodIndex ChannelServices_t1852_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelServices_t1852_0_0_0;
extern const Il2CppType ChannelServices_t1852_1_0_0;
struct ChannelServices_t1852;
const Il2CppTypeDefinitionMetadata ChannelServices_t1852_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelServices_t1852_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8184/* fieldStart */
	, 11593/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ChannelServices_t1852_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelServices"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &ChannelServices_t1852_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2938/* custom_attributes_cache */
	, &ChannelServices_t1852_0_0_0/* byval_arg */
	, &ChannelServices_t1852_1_0_0/* this_arg */
	, &ChannelServices_t1852_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelServices_t1852)/* instance_size */
	, sizeof (ChannelServices_t1852)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ChannelServices_t1852_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainData.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainData
extern TypeInfo CrossAppDomainData_t1853_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainData
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainDataMethodDeclarations.h"
static const EncodedMethodIndex CrossAppDomainData_t1853_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainData_t1853_0_0_0;
extern const Il2CppType CrossAppDomainData_t1853_1_0_0;
struct CrossAppDomainData_t1853;
const Il2CppTypeDefinitionMetadata CrossAppDomainData_t1853_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainData_t1853_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8189/* fieldStart */
	, 11601/* methodStart */
	, -1/* eventStart */
	, 2284/* propertyStart */

};
TypeInfo CrossAppDomainData_t1853_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &CrossAppDomainData_t1853_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainData_t1853_0_0_0/* byval_arg */
	, &CrossAppDomainData_t1853_1_0_0/* this_arg */
	, &CrossAppDomainData_t1853_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainData_t1853)/* instance_size */
	, sizeof (CrossAppDomainData_t1853)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChan.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainChannel
extern TypeInfo CrossAppDomainChannel_t1854_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainChannel
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainChanMethodDeclarations.h"
static const EncodedMethodIndex CrossAppDomainChannel_t1854_VTable[14] = 
{
	626,
	601,
	627,
	628,
	3753,
	3754,
	3755,
	3756,
	3757,
	3753,
	3754,
	3755,
	3756,
	3757,
};
extern const Il2CppType IChannel_t2182_0_0_0;
extern const Il2CppType IChannelReceiver_t2200_0_0_0;
extern const Il2CppType IChannelSender_t2181_0_0_0;
static const Il2CppType* CrossAppDomainChannel_t1854_InterfacesTypeInfos[] = 
{
	&IChannel_t2182_0_0_0,
	&IChannelReceiver_t2200_0_0_0,
	&IChannelSender_t2181_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainChannel_t1854_InterfacesOffsets[] = 
{
	{ &IChannel_t2182_0_0_0, 4},
	{ &IChannelReceiver_t2200_0_0_0, 6},
	{ &IChannelSender_t2181_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainChannel_t1854_0_0_0;
extern const Il2CppType CrossAppDomainChannel_t1854_1_0_0;
struct CrossAppDomainChannel_t1854;
const Il2CppTypeDefinitionMetadata CrossAppDomainChannel_t1854_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainChannel_t1854_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainChannel_t1854_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainChannel_t1854_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8192/* fieldStart */
	, 11604/* methodStart */
	, -1/* eventStart */
	, 2286/* propertyStart */

};
TypeInfo CrossAppDomainChannel_t1854_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &CrossAppDomainChannel_t1854_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossAppDomainChannel_t1854_0_0_0/* byval_arg */
	, &CrossAppDomainChannel_t1854_1_0_0/* this_arg */
	, &CrossAppDomainChannel_t1854_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainChannel_t1854)/* instance_size */
	, sizeof (CrossAppDomainChannel_t1854)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainChannel_t1854_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSink.h"
// Metadata Definition System.Runtime.Remoting.Channels.CrossAppDomainSink
extern TypeInfo CrossAppDomainSink_t1855_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.CrossAppDomainSink
#include "mscorlib_System_Runtime_Remoting_Channels_CrossAppDomainSinkMethodDeclarations.h"
static const EncodedMethodIndex CrossAppDomainSink_t1855_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern const Il2CppType IMessageSink_t1897_0_0_0;
static const Il2CppType* CrossAppDomainSink_t1855_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1897_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossAppDomainSink_t1855_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1897_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossAppDomainSink_t1855_0_0_0;
extern const Il2CppType CrossAppDomainSink_t1855_1_0_0;
struct CrossAppDomainSink_t1855;
const Il2CppTypeDefinitionMetadata CrossAppDomainSink_t1855_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossAppDomainSink_t1855_InterfacesTypeInfos/* implementedInterfaces */
	, CrossAppDomainSink_t1855_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossAppDomainSink_t1855_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8193/* fieldStart */
	, 11612/* methodStart */
	, -1/* eventStart */
	, 2289/* propertyStart */

};
TypeInfo CrossAppDomainSink_t1855_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossAppDomainSink"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &CrossAppDomainSink_t1855_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2940/* custom_attributes_cache */
	, &CrossAppDomainSink_t1855_0_0_0/* byval_arg */
	, &CrossAppDomainSink_t1855_1_0_0/* this_arg */
	, &CrossAppDomainSink_t1855_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossAppDomainSink_t1855)/* instance_size */
	, sizeof (CrossAppDomainSink_t1855)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CrossAppDomainSink_t1855_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannel
extern TypeInfo IChannel_t2182_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannel_t2182_1_0_0;
struct IChannel_t2182;
const Il2CppTypeDefinitionMetadata IChannel_t2182_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11616/* methodStart */
	, -1/* eventStart */
	, 2290/* propertyStart */

};
TypeInfo IChannel_t2182_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannel_t2182_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2941/* custom_attributes_cache */
	, &IChannel_t2182_0_0_0/* byval_arg */
	, &IChannel_t2182_1_0_0/* this_arg */
	, &IChannel_t2182_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelDataStore
extern TypeInfo IChannelDataStore_t2198_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelDataStore_t2198_0_0_0;
extern const Il2CppType IChannelDataStore_t2198_1_0_0;
struct IChannelDataStore_t2198;
const Il2CppTypeDefinitionMetadata IChannelDataStore_t2198_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IChannelDataStore_t2198_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelDataStore"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannelDataStore_t2198_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2942/* custom_attributes_cache */
	, &IChannelDataStore_t2198_0_0_0/* byval_arg */
	, &IChannelDataStore_t2198_1_0_0/* this_arg */
	, &IChannelDataStore_t2198_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelReceiver
extern TypeInfo IChannelReceiver_t2200_il2cpp_TypeInfo;
static const Il2CppType* IChannelReceiver_t2200_InterfacesTypeInfos[] = 
{
	&IChannel_t2182_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelReceiver_t2200_1_0_0;
struct IChannelReceiver_t2200;
const Il2CppTypeDefinitionMetadata IChannelReceiver_t2200_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelReceiver_t2200_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11618/* methodStart */
	, -1/* eventStart */
	, 2292/* propertyStart */

};
TypeInfo IChannelReceiver_t2200_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelReceiver"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannelReceiver_t2200_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2943/* custom_attributes_cache */
	, &IChannelReceiver_t2200_0_0_0/* byval_arg */
	, &IChannelReceiver_t2200_1_0_0/* this_arg */
	, &IChannelReceiver_t2200_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IChannelSender
extern TypeInfo IChannelSender_t2181_il2cpp_TypeInfo;
static const Il2CppType* IChannelSender_t2181_InterfacesTypeInfos[] = 
{
	&IChannel_t2182_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelSender_t2181_1_0_0;
struct IChannelSender_t2181;
const Il2CppTypeDefinitionMetadata IChannelSender_t2181_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IChannelSender_t2181_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11620/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IChannelSender_t2181_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelSender"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IChannelSender_t2181_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2944/* custom_attributes_cache */
	, &IChannelSender_t2181_0_0_0/* byval_arg */
	, &IChannelSender_t2181_1_0_0/* this_arg */
	, &IChannelSender_t2181_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IClientChannelSinkProvider
extern TypeInfo IClientChannelSinkProvider_t2202_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IClientChannelSinkProvider_t2202_0_0_0;
extern const Il2CppType IClientChannelSinkProvider_t2202_1_0_0;
struct IClientChannelSinkProvider_t2202;
const Il2CppTypeDefinitionMetadata IClientChannelSinkProvider_t2202_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11621/* methodStart */
	, -1/* eventStart */
	, 2293/* propertyStart */

};
TypeInfo IClientChannelSinkProvider_t2202_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IClientChannelSinkProvider"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IClientChannelSinkProvider_t2202_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2945/* custom_attributes_cache */
	, &IClientChannelSinkProvider_t2202_0_0_0/* byval_arg */
	, &IClientChannelSinkProvider_t2202_1_0_0/* this_arg */
	, &IClientChannelSinkProvider_t2202_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.ISecurableChannel
extern TypeInfo ISecurableChannel_t2199_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISecurableChannel_t2199_0_0_0;
extern const Il2CppType ISecurableChannel_t2199_1_0_0;
struct ISecurableChannel_t2199;
const Il2CppTypeDefinitionMetadata ISecurableChannel_t2199_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11622/* methodStart */
	, -1/* eventStart */
	, 2294/* propertyStart */

};
TypeInfo ISecurableChannel_t2199_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurableChannel"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &ISecurableChannel_t2199_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISecurableChannel_t2199_0_0_0/* byval_arg */
	, &ISecurableChannel_t2199_1_0_0/* this_arg */
	, &ISecurableChannel_t2199_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Channels.IServerChannelSinkProvider
extern TypeInfo IServerChannelSinkProvider_t2201_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IServerChannelSinkProvider_t2201_0_0_0;
extern const Il2CppType IServerChannelSinkProvider_t2201_1_0_0;
struct IServerChannelSinkProvider_t2201;
const Il2CppTypeDefinitionMetadata IServerChannelSinkProvider_t2201_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11623/* methodStart */
	, -1/* eventStart */
	, 2295/* propertyStart */

};
TypeInfo IServerChannelSinkProvider_t2201_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IServerChannelSinkProvider"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &IServerChannelSinkProvider_t2201_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2946/* custom_attributes_cache */
	, &IServerChannelSinkProvider_t2201_0_0_0/* byval_arg */
	, &IServerChannelSinkProvider_t2201_1_0_0/* this_arg */
	, &IServerChannelSinkProvider_t2201_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Channels.SinkProviderData
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProviderData.h"
// Metadata Definition System.Runtime.Remoting.Channels.SinkProviderData
extern TypeInfo SinkProviderData_t1856_il2cpp_TypeInfo;
// System.Runtime.Remoting.Channels.SinkProviderData
#include "mscorlib_System_Runtime_Remoting_Channels_SinkProviderDataMethodDeclarations.h"
static const EncodedMethodIndex SinkProviderData_t1856_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SinkProviderData_t1856_0_0_0;
extern const Il2CppType SinkProviderData_t1856_1_0_0;
struct SinkProviderData_t1856;
const Il2CppTypeDefinitionMetadata SinkProviderData_t1856_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SinkProviderData_t1856_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8196/* fieldStart */
	, 11624/* methodStart */
	, -1/* eventStart */
	, 2296/* propertyStart */

};
TypeInfo SinkProviderData_t1856_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SinkProviderData"/* name */
	, "System.Runtime.Remoting.Channels"/* namespaze */
	, NULL/* methods */
	, &SinkProviderData_t1856_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2947/* custom_attributes_cache */
	, &SinkProviderData_t1856_0_0_0/* byval_arg */
	, &SinkProviderData_t1856_1_0_0/* this_arg */
	, &SinkProviderData_t1856_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SinkProviderData_t1856)/* instance_size */
	, sizeof (SinkProviderData_t1856)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_Context.h"
// Metadata Definition System.Runtime.Remoting.Contexts.Context
extern TypeInfo Context_t1857_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.Context
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextMethodDeclarations.h"
static const EncodedMethodIndex Context_t1857_VTable[5] = 
{
	626,
	3758,
	627,
	3759,
	3760,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Context_t1857_0_0_0;
extern const Il2CppType Context_t1857_1_0_0;
struct Context_t1857;
const Il2CppTypeDefinitionMetadata Context_t1857_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Context_t1857_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8199/* fieldStart */
	, 11627/* methodStart */
	, -1/* eventStart */
	, 2298/* propertyStart */

};
TypeInfo Context_t1857_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &Context_t1857_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2948/* custom_attributes_cache */
	, &Context_t1857_0_0_0/* byval_arg */
	, &Context_t1857_1_0_0/* this_arg */
	, &Context_t1857_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Context_t1857)/* instance_size */
	, sizeof (Context_t1857)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Context_t1857_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttribute.h"
// Metadata Definition System.Runtime.Remoting.Contexts.ContextAttribute
extern TypeInfo ContextAttribute_t1849_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.ContextAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_ContextAttributeMethodDeclarations.h"
static const EncodedMethodIndex ContextAttribute_t1849_VTable[10] = 
{
	3761,
	601,
	3762,
	628,
	3763,
	3764,
	3751,
	3751,
	3763,
	3764,
};
static const Il2CppType* ContextAttribute_t1849_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2197_0_0_0,
	&IContextProperty_t2183_0_0_0,
};
static Il2CppInterfaceOffsetPair ContextAttribute_t1849_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
	{ &IContextAttribute_t2197_0_0_0, 4},
	{ &IContextProperty_t2183_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextAttribute_t1849_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct ContextAttribute_t1849;
const Il2CppTypeDefinitionMetadata ContextAttribute_t1849_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ContextAttribute_t1849_InterfacesTypeInfos/* implementedInterfaces */
	, ContextAttribute_t1849_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ContextAttribute_t1849_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8202/* fieldStart */
	, 11633/* methodStart */
	, -1/* eventStart */
	, 2300/* propertyStart */

};
TypeInfo ContextAttribute_t1849_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &ContextAttribute_t1849_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2949/* custom_attributes_cache */
	, &ContextAttribute_t1849_0_0_0/* byval_arg */
	, &ContextAttribute_t1849_1_0_0/* this_arg */
	, &ContextAttribute_t1849_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextAttribute_t1849)/* instance_size */
	, sizeof (ContextAttribute_t1849)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 2/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanne.h"
// Metadata Definition System.Runtime.Remoting.Contexts.CrossContextChannel
extern TypeInfo CrossContextChannel_t1851_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.CrossContextChannel
#include "mscorlib_System_Runtime_Remoting_Contexts_CrossContextChanneMethodDeclarations.h"
static const EncodedMethodIndex CrossContextChannel_t1851_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
static const Il2CppType* CrossContextChannel_t1851_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1897_0_0_0,
};
static Il2CppInterfaceOffsetPair CrossContextChannel_t1851_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1897_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CrossContextChannel_t1851_0_0_0;
extern const Il2CppType CrossContextChannel_t1851_1_0_0;
struct CrossContextChannel_t1851;
const Il2CppTypeDefinitionMetadata CrossContextChannel_t1851_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CrossContextChannel_t1851_InterfacesTypeInfos/* implementedInterfaces */
	, CrossContextChannel_t1851_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CrossContextChannel_t1851_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11639/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CrossContextChannel_t1851_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CrossContextChannel"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &CrossContextChannel_t1851_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CrossContextChannel_t1851_0_0_0/* byval_arg */
	, &CrossContextChannel_t1851_1_0_0/* this_arg */
	, &CrossContextChannel_t1851_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CrossContextChannel_t1851)/* instance_size */
	, sizeof (CrossContextChannel_t1851)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextAttribute
extern TypeInfo IContextAttribute_t2197_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextAttribute_t2197_1_0_0;
struct IContextAttribute_t2197;
const Il2CppTypeDefinitionMetadata IContextAttribute_t2197_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11640/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContextAttribute_t2197_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContextAttribute_t2197_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2950/* custom_attributes_cache */
	, &IContextAttribute_t2197_0_0_0/* byval_arg */
	, &IContextAttribute_t2197_1_0_0/* this_arg */
	, &IContextAttribute_t2197_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContextProperty
extern TypeInfo IContextProperty_t2183_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContextProperty_t2183_1_0_0;
struct IContextProperty_t2183;
const Il2CppTypeDefinitionMetadata IContextProperty_t2183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11642/* methodStart */
	, -1/* eventStart */
	, 2301/* propertyStart */

};
TypeInfo IContextProperty_t2183_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContextProperty"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContextProperty_t2183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2951/* custom_attributes_cache */
	, &IContextProperty_t2183_0_0_0/* byval_arg */
	, &IContextProperty_t2183_1_0_0/* this_arg */
	, &IContextProperty_t2183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeClientContextSink
extern TypeInfo IContributeClientContextSink_t3488_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeClientContextSink_t3488_0_0_0;
extern const Il2CppType IContributeClientContextSink_t3488_1_0_0;
struct IContributeClientContextSink_t3488;
const Il2CppTypeDefinitionMetadata IContributeClientContextSink_t3488_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContributeClientContextSink_t3488_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeClientContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContributeClientContextSink_t3488_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2952/* custom_attributes_cache */
	, &IContributeClientContextSink_t3488_0_0_0/* byval_arg */
	, &IContributeClientContextSink_t3488_1_0_0/* this_arg */
	, &IContributeClientContextSink_t3488_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Contexts.IContributeServerContextSink
extern TypeInfo IContributeServerContextSink_t3489_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IContributeServerContextSink_t3489_0_0_0;
extern const Il2CppType IContributeServerContextSink_t3489_1_0_0;
struct IContributeServerContextSink_t3489;
const Il2CppTypeDefinitionMetadata IContributeServerContextSink_t3489_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IContributeServerContextSink_t3489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IContributeServerContextSink"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &IContributeServerContextSink_t3489_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2953/* custom_attributes_cache */
	, &IContributeServerContextSink_t3489_0_0_0/* byval_arg */
	, &IContributeServerContextSink_t3489_1_0_0/* this_arg */
	, &IContributeServerContextSink_t3489_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAtt.h"
// Metadata Definition System.Runtime.Remoting.Contexts.SynchronizationAttribute
extern TypeInfo SynchronizationAttribute_t1860_il2cpp_TypeInfo;
// System.Runtime.Remoting.Contexts.SynchronizationAttribute
#include "mscorlib_System_Runtime_Remoting_Contexts_SynchronizationAttMethodDeclarations.h"
static const EncodedMethodIndex SynchronizationAttribute_t1860_VTable[11] = 
{
	3761,
	601,
	3762,
	628,
	3765,
	3766,
	3751,
	3751,
	3765,
	3766,
	3767,
};
static const Il2CppType* SynchronizationAttribute_t1860_InterfacesTypeInfos[] = 
{
	&IContributeClientContextSink_t3488_0_0_0,
	&IContributeServerContextSink_t3489_0_0_0,
};
static Il2CppInterfaceOffsetPair SynchronizationAttribute_t1860_InterfacesOffsets[] = 
{
	{ &IContextAttribute_t2197_0_0_0, 4},
	{ &IContextProperty_t2183_0_0_0, 6},
	{ &_Attribute_t3429_0_0_0, 4},
	{ &IContributeClientContextSink_t3488_0_0_0, 10},
	{ &IContributeServerContextSink_t3489_0_0_0, 10},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizationAttribute_t1860_0_0_0;
extern const Il2CppType SynchronizationAttribute_t1860_1_0_0;
struct SynchronizationAttribute_t1860;
const Il2CppTypeDefinitionMetadata SynchronizationAttribute_t1860_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SynchronizationAttribute_t1860_InterfacesTypeInfos/* implementedInterfaces */
	, SynchronizationAttribute_t1860_InterfacesOffsets/* interfaceOffsets */
	, &ContextAttribute_t1849_0_0_0/* parent */
	, SynchronizationAttribute_t1860_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8203/* fieldStart */
	, 11643/* methodStart */
	, -1/* eventStart */
	, 2302/* propertyStart */

};
TypeInfo SynchronizationAttribute_t1860_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationAttribute"/* name */
	, "System.Runtime.Remoting.Contexts"/* namespaze */
	, NULL/* methods */
	, &SynchronizationAttribute_t1860_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2954/* custom_attributes_cache */
	, &SynchronizationAttribute_t1860_0_0_0/* byval_arg */
	, &SynchronizationAttribute_t1860_1_0_0/* this_arg */
	, &SynchronizationAttribute_t1860_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationAttribute_t1860)/* instance_size */
	, sizeof (SynchronizationAttribute_t1860)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.LeaseManager
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseManager.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.LeaseManager
extern TypeInfo LeaseManager_t1862_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.LeaseManager
#include "mscorlib_System_Runtime_Remoting_Lifetime_LeaseManagerMethodDeclarations.h"
static const EncodedMethodIndex LeaseManager_t1862_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LeaseManager_t1862_0_0_0;
extern const Il2CppType LeaseManager_t1862_1_0_0;
struct LeaseManager_t1862;
const Il2CppTypeDefinitionMetadata LeaseManager_t1862_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LeaseManager_t1862_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8208/* fieldStart */
	, 11651/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LeaseManager_t1862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LeaseManager"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, NULL/* methods */
	, &LeaseManager_t1862_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LeaseManager_t1862_0_0_0/* byval_arg */
	, &LeaseManager_t1862_1_0_0/* this_arg */
	, &LeaseManager_t1862_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LeaseManager_t1862)/* instance_size */
	, sizeof (LeaseManager_t1862)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Lifetime.LifetimeServices
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeServices.h"
// Metadata Definition System.Runtime.Remoting.Lifetime.LifetimeServices
extern TypeInfo LifetimeServices_t1863_il2cpp_TypeInfo;
// System.Runtime.Remoting.Lifetime.LifetimeServices
#include "mscorlib_System_Runtime_Remoting_Lifetime_LifetimeServicesMethodDeclarations.h"
static const EncodedMethodIndex LifetimeServices_t1863_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LifetimeServices_t1863_0_0_0;
extern const Il2CppType LifetimeServices_t1863_1_0_0;
struct LifetimeServices_t1863;
const Il2CppTypeDefinitionMetadata LifetimeServices_t1863_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LifetimeServices_t1863_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8210/* fieldStart */
	, 11653/* methodStart */
	, -1/* eventStart */
	, 2303/* propertyStart */

};
TypeInfo LifetimeServices_t1863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LifetimeServices"/* name */
	, "System.Runtime.Remoting.Lifetime"/* namespaze */
	, NULL/* methods */
	, &LifetimeServices_t1863_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2957/* custom_attributes_cache */
	, &LifetimeServices_t1863_0_0_0/* byval_arg */
	, &LifetimeServices_t1863_1_0_0/* this_arg */
	, &LifetimeServices_t1863_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LifetimeServices_t1863)/* instance_size */
	, sizeof (LifetimeServices_t1863)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LifetimeServices_t1863_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoType.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfoType
extern TypeInfo ArgInfoType_t1864_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfoType
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoTypeMethodDeclarations.h"
static const EncodedMethodIndex ArgInfoType_t1864_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair ArgInfoType_t1864_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfoType_t1864_0_0_0;
extern const Il2CppType ArgInfoType_t1864_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t1117_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ArgInfoType_t1864_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgInfoType_t1864_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ArgInfoType_t1864_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8215/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgInfoType_t1864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfoType"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfoType_t1864_0_0_0/* byval_arg */
	, &ArgInfoType_t1864_1_0_0/* this_arg */
	, &ArgInfoType_t1864_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfoType_t1864)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ArgInfoType_t1864)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfo.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ArgInfo
extern TypeInfo ArgInfo_t1865_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ArgInfo
#include "mscorlib_System_Runtime_Remoting_Messaging_ArgInfoMethodDeclarations.h"
static const EncodedMethodIndex ArgInfo_t1865_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgInfo_t1865_0_0_0;
extern const Il2CppType ArgInfo_t1865_1_0_0;
struct ArgInfo_t1865;
const Il2CppTypeDefinitionMetadata ArgInfo_t1865_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgInfo_t1865_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8218/* fieldStart */
	, 11658/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgInfo_t1865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgInfo"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ArgInfo_t1865_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgInfo_t1865_0_0_0/* byval_arg */
	, &ArgInfo_t1865_1_0_0/* this_arg */
	, &ArgInfo_t1865_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgInfo_t1865)/* instance_size */
	, sizeof (ArgInfo_t1865)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResult.h"
// Metadata Definition System.Runtime.Remoting.Messaging.AsyncResult
extern TypeInfo AsyncResult_t1870_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.AsyncResult
#include "mscorlib_System_Runtime_Remoting_Messaging_AsyncResultMethodDeclarations.h"
static const EncodedMethodIndex AsyncResult_t1870_VTable[17] = 
{
	626,
	601,
	627,
	628,
	3768,
	3769,
	3770,
	3768,
	3769,
	3771,
	3770,
	3772,
	3773,
	3774,
	3775,
	3776,
	3777,
};
extern const Il2CppType IAsyncResult_t216_0_0_0;
static const Il2CppType* AsyncResult_t1870_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t216_0_0_0,
	&IMessageSink_t1897_0_0_0,
};
static Il2CppInterfaceOffsetPair AsyncResult_t1870_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t216_0_0_0, 4},
	{ &IMessageSink_t1897_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AsyncResult_t1870_0_0_0;
extern const Il2CppType AsyncResult_t1870_1_0_0;
struct AsyncResult_t1870;
const Il2CppTypeDefinitionMetadata AsyncResult_t1870_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AsyncResult_t1870_InterfacesTypeInfos/* implementedInterfaces */
	, AsyncResult_t1870_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AsyncResult_t1870_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8221/* fieldStart */
	, 11660/* methodStart */
	, -1/* eventStart */
	, 2307/* propertyStart */

};
TypeInfo AsyncResult_t1870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AsyncResult"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &AsyncResult_t1870_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2958/* custom_attributes_cache */
	, &AsyncResult_t1870_0_0_0/* byval_arg */
	, &AsyncResult_t1870_1_0_0/* this_arg */
	, &AsyncResult_t1870_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AsyncResult_t1870)/* instance_size */
	, sizeof (AsyncResult_t1870)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 8/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCall
extern TypeInfo ConstructionCall_t1871_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCall
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallMethodDeclarations.h"
static const EncodedMethodIndex ConstructionCall_t1871_VTable[25] = 
{
	626,
	601,
	627,
	628,
	3778,
	3779,
	3780,
	3781,
	3782,
	3783,
	3784,
	3785,
	3786,
	3787,
	3778,
	3788,
	3789,
	3790,
	3791,
	3792,
	3793,
	3794,
	3795,
	3796,
	3797,
};
static const Il2CppType* ConstructionCall_t1871_InterfacesTypeInfos[] = 
{
	&IConstructionCallMessage_t2180_0_0_0,
	&IMessage_t1869_0_0_0,
	&IMethodCallMessage_t2185_0_0_0,
	&IMethodMessage_t1881_0_0_0,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
extern const Il2CppType IInternalMessage_t2203_0_0_0;
extern const Il2CppType ISerializationRootObject_t3491_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCall_t1871_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IInternalMessage_t2203_0_0_0, 5},
	{ &IMessage_t1869_0_0_0, 6},
	{ &IMethodCallMessage_t2185_0_0_0, 6},
	{ &IMethodMessage_t1881_0_0_0, 6},
	{ &ISerializationRootObject_t3491_0_0_0, 13},
	{ &IConstructionCallMessage_t2180_0_0_0, 19},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCall_t1871_0_0_0;
extern const Il2CppType ConstructionCall_t1871_1_0_0;
extern const Il2CppType MethodCall_t1872_0_0_0;
struct ConstructionCall_t1871;
const Il2CppTypeDefinitionMetadata ConstructionCall_t1871_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructionCall_t1871_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructionCall_t1871_InterfacesOffsets/* interfaceOffsets */
	, &MethodCall_t1872_0_0_0/* parent */
	, ConstructionCall_t1871_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8236/* fieldStart */
	, 11677/* methodStart */
	, -1/* eventStart */
	, 2315/* propertyStart */

};
TypeInfo ConstructionCall_t1871_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ConstructionCall_t1871_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2959/* custom_attributes_cache */
	, &ConstructionCall_t1871_0_0_0/* byval_arg */
	, &ConstructionCall_t1871_1_0_0/* this_arg */
	, &ConstructionCall_t1871_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCall_t1871)/* instance_size */
	, sizeof (ConstructionCall_t1871)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCall_t1871_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 4/* interfaces_count */
	, 7/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallD.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ConstructionCallDictionary
extern TypeInfo ConstructionCallDictionary_t1873_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ConstructionCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_ConstructionCallDMethodDeclarations.h"
static const EncodedMethodIndex ConstructionCallDictionary_t1873_VTable[19] = 
{
	626,
	601,
	627,
	628,
	3798,
	3799,
	3800,
	3801,
	3802,
	3803,
	3804,
	3805,
	3806,
	3807,
	3808,
	3809,
	3810,
	3811,
	3812,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
extern const Il2CppType ICollection_t1528_0_0_0;
extern const Il2CppType IDictionary_t1462_0_0_0;
static Il2CppInterfaceOffsetPair ConstructionCallDictionary_t1873_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IDictionary_t1462_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructionCallDictionary_t1873_0_0_0;
extern const Il2CppType ConstructionCallDictionary_t1873_1_0_0;
extern const Il2CppType MethodDictionary_t1874_0_0_0;
struct ConstructionCallDictionary_t1873;
const Il2CppTypeDefinitionMetadata ConstructionCallDictionary_t1873_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructionCallDictionary_t1873_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1874_0_0_0/* parent */
	, ConstructionCallDictionary_t1873_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8243/* fieldStart */
	, 11691/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConstructionCallDictionary_t1873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructionCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ConstructionCallDictionary_t1873_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructionCallDictionary_t1873_0_0_0/* byval_arg */
	, &ConstructionCallDictionary_t1873_1_0_0/* this_arg */
	, &ConstructionCallDictionary_t1873_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructionCallDictionary_t1873)/* instance_size */
	, sizeof (ConstructionCallDictionary_t1873)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructionCallDictionary_t1873_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSi.h"
// Metadata Definition System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
extern TypeInfo EnvoyTerminatorSink_t1875_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
#include "mscorlib_System_Runtime_Remoting_Messaging_EnvoyTerminatorSiMethodDeclarations.h"
static const EncodedMethodIndex EnvoyTerminatorSink_t1875_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
static const Il2CppType* EnvoyTerminatorSink_t1875_InterfacesTypeInfos[] = 
{
	&IMessageSink_t1897_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyTerminatorSink_t1875_InterfacesOffsets[] = 
{
	{ &IMessageSink_t1897_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyTerminatorSink_t1875_0_0_0;
extern const Il2CppType EnvoyTerminatorSink_t1875_1_0_0;
struct EnvoyTerminatorSink_t1875;
const Il2CppTypeDefinitionMetadata EnvoyTerminatorSink_t1875_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyTerminatorSink_t1875_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyTerminatorSink_t1875_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyTerminatorSink_t1875_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8246/* fieldStart */
	, 11695/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EnvoyTerminatorSink_t1875_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyTerminatorSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &EnvoyTerminatorSink_t1875_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyTerminatorSink_t1875_0_0_0/* byval_arg */
	, &EnvoyTerminatorSink_t1875_1_0_0/* this_arg */
	, &EnvoyTerminatorSink_t1875_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyTerminatorSink_t1875)/* instance_size */
	, sizeof (EnvoyTerminatorSink_t1875)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EnvoyTerminatorSink_t1875_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_Header.h"
// Metadata Definition System.Runtime.Remoting.Messaging.Header
extern TypeInfo Header_t1876_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.Header
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderMethodDeclarations.h"
static const EncodedMethodIndex Header_t1876_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Header_t1876_0_0_0;
extern const Il2CppType Header_t1876_1_0_0;
struct Header_t1876;
const Il2CppTypeDefinitionMetadata Header_t1876_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Header_t1876_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8247/* fieldStart */
	, 11697/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Header_t1876_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Header"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &Header_t1876_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2963/* custom_attributes_cache */
	, &Header_t1876_0_0_0/* byval_arg */
	, &Header_t1876_1_0_0/* this_arg */
	, &Header_t1876_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Header_t1876)/* instance_size */
	, sizeof (Header_t1876)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IInternalMessage
extern TypeInfo IInternalMessage_t2203_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IInternalMessage_t2203_1_0_0;
struct IInternalMessage_t2203;
const Il2CppTypeDefinitionMetadata IInternalMessage_t2203_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11700/* methodStart */
	, -1/* eventStart */
	, 2322/* propertyStart */

};
TypeInfo IInternalMessage_t2203_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IInternalMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IInternalMessage_t2203_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IInternalMessage_t2203_0_0_0/* byval_arg */
	, &IInternalMessage_t2203_1_0_0/* this_arg */
	, &IInternalMessage_t2203_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessage
extern TypeInfo IMessage_t1869_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessage_t1869_1_0_0;
struct IMessage_t1869;
const Il2CppTypeDefinitionMetadata IMessage_t1869_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMessage_t1869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMessage_t1869_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2964/* custom_attributes_cache */
	, &IMessage_t1869_0_0_0/* byval_arg */
	, &IMessage_t1869_1_0_0/* this_arg */
	, &IMessage_t1869_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageCtrl
extern TypeInfo IMessageCtrl_t1868_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageCtrl_t1868_0_0_0;
extern const Il2CppType IMessageCtrl_t1868_1_0_0;
struct IMessageCtrl_t1868;
const Il2CppTypeDefinitionMetadata IMessageCtrl_t1868_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMessageCtrl_t1868_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageCtrl"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMessageCtrl_t1868_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2965/* custom_attributes_cache */
	, &IMessageCtrl_t1868_0_0_0/* byval_arg */
	, &IMessageCtrl_t1868_1_0_0/* this_arg */
	, &IMessageCtrl_t1868_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMessageSink
extern TypeInfo IMessageSink_t1897_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMessageSink_t1897_1_0_0;
struct IMessageSink_t1897;
const Il2CppTypeDefinitionMetadata IMessageSink_t1897_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMessageSink_t1897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMessageSink"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMessageSink_t1897_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2966/* custom_attributes_cache */
	, &IMessageSink_t1897_0_0_0/* byval_arg */
	, &IMessageSink_t1897_1_0_0/* this_arg */
	, &IMessageSink_t1897_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodCallMessage
extern TypeInfo IMethodCallMessage_t2185_il2cpp_TypeInfo;
static const Il2CppType* IMethodCallMessage_t2185_InterfacesTypeInfos[] = 
{
	&IMessage_t1869_0_0_0,
	&IMethodMessage_t1881_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodCallMessage_t2185_1_0_0;
struct IMethodCallMessage_t2185;
const Il2CppTypeDefinitionMetadata IMethodCallMessage_t2185_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodCallMessage_t2185_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IMethodCallMessage_t2185_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodCallMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMethodCallMessage_t2185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2967/* custom_attributes_cache */
	, &IMethodCallMessage_t2185_0_0_0/* byval_arg */
	, &IMethodCallMessage_t2185_1_0_0/* this_arg */
	, &IMethodCallMessage_t2185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodMessage
extern TypeInfo IMethodMessage_t1881_il2cpp_TypeInfo;
static const Il2CppType* IMethodMessage_t1881_InterfacesTypeInfos[] = 
{
	&IMessage_t1869_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodMessage_t1881_1_0_0;
struct IMethodMessage_t1881;
const Il2CppTypeDefinitionMetadata IMethodMessage_t1881_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodMessage_t1881_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11701/* methodStart */
	, -1/* eventStart */
	, 2323/* propertyStart */

};
TypeInfo IMethodMessage_t1881_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMethodMessage_t1881_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2968/* custom_attributes_cache */
	, &IMethodMessage_t1881_0_0_0/* byval_arg */
	, &IMethodMessage_t1881_1_0_0/* this_arg */
	, &IMethodMessage_t1881_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IMethodReturnMessage
extern TypeInfo IMethodReturnMessage_t2184_il2cpp_TypeInfo;
static const Il2CppType* IMethodReturnMessage_t2184_InterfacesTypeInfos[] = 
{
	&IMessage_t1869_0_0_0,
	&IMethodMessage_t1881_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IMethodReturnMessage_t2184_0_0_0;
extern const Il2CppType IMethodReturnMessage_t2184_1_0_0;
struct IMethodReturnMessage_t2184;
const Il2CppTypeDefinitionMetadata IMethodReturnMessage_t2184_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IMethodReturnMessage_t2184_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11708/* methodStart */
	, -1/* eventStart */
	, 2330/* propertyStart */

};
TypeInfo IMethodReturnMessage_t2184_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMethodReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IMethodReturnMessage_t2184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2969/* custom_attributes_cache */
	, &IMethodReturnMessage_t2184_0_0_0/* byval_arg */
	, &IMethodReturnMessage_t2184_1_0_0/* this_arg */
	, &IMethodReturnMessage_t2184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.IRemotingFormatter
extern TypeInfo IRemotingFormatter_t3490_il2cpp_TypeInfo;
extern const Il2CppType IFormatter_t3492_0_0_0;
static const Il2CppType* IRemotingFormatter_t3490_InterfacesTypeInfos[] = 
{
	&IFormatter_t3492_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingFormatter_t3490_0_0_0;
extern const Il2CppType IRemotingFormatter_t3490_1_0_0;
struct IRemotingFormatter_t3490;
const Il2CppTypeDefinitionMetadata IRemotingFormatter_t3490_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IRemotingFormatter_t3490_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IRemotingFormatter_t3490_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingFormatter"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &IRemotingFormatter_t3490_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2970/* custom_attributes_cache */
	, &IRemotingFormatter_t3490_0_0_0/* byval_arg */
	, &IRemotingFormatter_t3490_1_0_0/* this_arg */
	, &IRemotingFormatter_t3490_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Messaging.ISerializationRootObject
extern TypeInfo ISerializationRootObject_t3491_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISerializationRootObject_t3491_1_0_0;
struct ISerializationRootObject_t3491;
const Il2CppTypeDefinitionMetadata ISerializationRootObject_t3491_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISerializationRootObject_t3491_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISerializationRootObject"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ISerializationRootObject_t3491_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISerializationRootObject_t3491_0_0_0/* byval_arg */
	, &ISerializationRootObject_t3491_1_0_0/* this_arg */
	, &ISerializationRootObject_t3491_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContex.h"
// Metadata Definition System.Runtime.Remoting.Messaging.LogicalCallContext
extern TypeInfo LogicalCallContext_t1878_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.LogicalCallContext
#include "mscorlib_System_Runtime_Remoting_Messaging_LogicalCallContexMethodDeclarations.h"
static const EncodedMethodIndex LogicalCallContext_t1878_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3813,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
static const Il2CppType* LogicalCallContext_t1878_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair LogicalCallContext_t1878_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LogicalCallContext_t1878_0_0_0;
extern const Il2CppType LogicalCallContext_t1878_1_0_0;
struct LogicalCallContext_t1878;
const Il2CppTypeDefinitionMetadata LogicalCallContext_t1878_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LogicalCallContext_t1878_InterfacesTypeInfos/* implementedInterfaces */
	, LogicalCallContext_t1878_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LogicalCallContext_t1878_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8251/* fieldStart */
	, 11711/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LogicalCallContext_t1878_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogicalCallContext"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &LogicalCallContext_t1878_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2971/* custom_attributes_cache */
	, &LogicalCallContext_t1878_0_0_0/* byval_arg */
	, &LogicalCallContext_t1878_1_0_0/* this_arg */
	, &LogicalCallContext_t1878_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogicalCallContext_t1878)/* instance_size */
	, sizeof (LogicalCallContext_t1878)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemoti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.CallContextRemotingData
extern TypeInfo CallContextRemotingData_t1877_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.CallContextRemotingData
#include "mscorlib_System_Runtime_Remoting_Messaging_CallContextRemotiMethodDeclarations.h"
static const EncodedMethodIndex CallContextRemotingData_t1877_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
static const Il2CppType* CallContextRemotingData_t1877_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair CallContextRemotingData_t1877_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallContextRemotingData_t1877_0_0_0;
extern const Il2CppType CallContextRemotingData_t1877_1_0_0;
struct CallContextRemotingData_t1877;
const Il2CppTypeDefinitionMetadata CallContextRemotingData_t1877_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CallContextRemotingData_t1877_InterfacesTypeInfos/* implementedInterfaces */
	, CallContextRemotingData_t1877_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CallContextRemotingData_t1877_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11715/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CallContextRemotingData_t1877_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallContextRemotingData"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &CallContextRemotingData_t1877_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CallContextRemotingData_t1877_0_0_0/* byval_arg */
	, &CallContextRemotingData_t1877_1_0_0/* this_arg */
	, &CallContextRemotingData_t1877_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallContextRemotingData_t1877)/* instance_size */
	, sizeof (CallContextRemotingData_t1877)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCall.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCall
extern TypeInfo MethodCall_t1872_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCall
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallMethodDeclarations.h"
static const EncodedMethodIndex MethodCall_t1872_VTable[19] = 
{
	626,
	601,
	627,
	628,
	3814,
	3779,
	3780,
	3781,
	3782,
	3783,
	3784,
	3785,
	3786,
	3815,
	3814,
	3816,
	3817,
	3790,
	3791,
};
static const Il2CppType* MethodCall_t1872_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IInternalMessage_t2203_0_0_0,
	&IMessage_t1869_0_0_0,
	&IMethodCallMessage_t2185_0_0_0,
	&IMethodMessage_t1881_0_0_0,
	&ISerializationRootObject_t3491_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodCall_t1872_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IInternalMessage_t2203_0_0_0, 5},
	{ &IMessage_t1869_0_0_0, 6},
	{ &IMethodCallMessage_t2185_0_0_0, 6},
	{ &IMethodMessage_t1881_0_0_0, 6},
	{ &ISerializationRootObject_t3491_0_0_0, 13},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCall_t1872_1_0_0;
struct MethodCall_t1872;
const Il2CppTypeDefinitionMetadata MethodCall_t1872_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodCall_t1872_InterfacesTypeInfos/* implementedInterfaces */
	, MethodCall_t1872_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodCall_t1872_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8253/* fieldStart */
	, 11716/* methodStart */
	, -1/* eventStart */
	, 2333/* propertyStart */

};
TypeInfo MethodCall_t1872_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCall"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodCall_t1872_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2972/* custom_attributes_cache */
	, &MethodCall_t1872_0_0_0/* byval_arg */
	, &MethodCall_t1872_1_0_0/* this_arg */
	, &MethodCall_t1872_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCall_t1872)/* instance_size */
	, sizeof (MethodCall_t1872)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCall_t1872_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 10/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDiction.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodCallDictionary
extern TypeInfo MethodCallDictionary_t1879_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodCallDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodCallDictionMethodDeclarations.h"
static const EncodedMethodIndex MethodCallDictionary_t1879_VTable[19] = 
{
	626,
	601,
	627,
	628,
	3798,
	3799,
	3800,
	3801,
	3802,
	3803,
	3804,
	3805,
	3806,
	3807,
	3808,
	3809,
	3818,
	3819,
	3812,
};
static Il2CppInterfaceOffsetPair MethodCallDictionary_t1879_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IDictionary_t1462_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodCallDictionary_t1879_0_0_0;
extern const Il2CppType MethodCallDictionary_t1879_1_0_0;
struct MethodCallDictionary_t1879;
const Il2CppTypeDefinitionMetadata MethodCallDictionary_t1879_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodCallDictionary_t1879_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1874_0_0_0/* parent */
	, MethodCallDictionary_t1879_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8264/* fieldStart */
	, 11737/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodCallDictionary_t1879_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodCallDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodCallDictionary_t1879_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodCallDictionary_t1879_0_0_0/* byval_arg */
	, &MethodCallDictionary_t1879_1_0_0/* this_arg */
	, &MethodCallDictionary_t1879_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodCallDictionary_t1879)/* instance_size */
	, sizeof (MethodCallDictionary_t1879)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodCallDictionary_t1879_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary
extern TypeInfo MethodDictionary_t1874_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionaryMethodDeclarations.h"
extern const Il2CppType DictionaryEnumerator_t1880_0_0_0;
static const Il2CppType* MethodDictionary_t1874_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DictionaryEnumerator_t1880_0_0_0,
};
static const EncodedMethodIndex MethodDictionary_t1874_VTable[19] = 
{
	626,
	601,
	627,
	628,
	3798,
	3799,
	3800,
	3801,
	3802,
	3803,
	3804,
	3805,
	3806,
	3807,
	3808,
	3809,
	3818,
	3819,
	3812,
};
static const Il2CppType* MethodDictionary_t1874_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&IDictionary_t1462_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodDictionary_t1874_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IDictionary_t1462_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodDictionary_t1874_1_0_0;
struct MethodDictionary_t1874;
const Il2CppTypeDefinitionMetadata MethodDictionary_t1874_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MethodDictionary_t1874_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MethodDictionary_t1874_InterfacesTypeInfos/* implementedInterfaces */
	, MethodDictionary_t1874_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MethodDictionary_t1874_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8265/* fieldStart */
	, 11739/* methodStart */
	, -1/* eventStart */
	, 2343/* propertyStart */

};
TypeInfo MethodDictionary_t1874_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodDictionary_t1874_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2974/* custom_attributes_cache */
	, &MethodDictionary_t1874_0_0_0/* byval_arg */
	, &MethodDictionary_t1874_1_0_0/* this_arg */
	, &MethodDictionary_t1874_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodDictionary_t1874)/* instance_size */
	, sizeof (MethodDictionary_t1874)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodDictionary_t1874_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 19/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
extern TypeInfo DictionaryEnumerator_t1880_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodDictionary/DictionaryEnumerator
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary_MethodDeclarations.h"
static const EncodedMethodIndex DictionaryEnumerator_t1880_VTable[9] = 
{
	626,
	601,
	627,
	628,
	3820,
	3821,
	3822,
	3823,
	3824,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IDictionaryEnumerator_t1527_0_0_0;
static const Il2CppType* DictionaryEnumerator_t1880_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDictionaryEnumerator_t1527_0_0_0,
};
static Il2CppInterfaceOffsetPair DictionaryEnumerator_t1880_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDictionaryEnumerator_t1527_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DictionaryEnumerator_t1880_1_0_0;
struct DictionaryEnumerator_t1880;
const Il2CppTypeDefinitionMetadata DictionaryEnumerator_t1880_DefinitionMetadata = 
{
	&MethodDictionary_t1874_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, DictionaryEnumerator_t1880_InterfacesTypeInfos/* implementedInterfaces */
	, DictionaryEnumerator_t1880_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DictionaryEnumerator_t1880_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8271/* fieldStart */
	, 11758/* methodStart */
	, -1/* eventStart */
	, 2349/* propertyStart */

};
TypeInfo DictionaryEnumerator_t1880_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DictionaryEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DictionaryEnumerator_t1880_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DictionaryEnumerator_t1880_0_0_0/* byval_arg */
	, &DictionaryEnumerator_t1880_1_0_0/* this_arg */
	, &DictionaryEnumerator_t1880_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DictionaryEnumerator_t1880)/* instance_size */
	, sizeof (DictionaryEnumerator_t1880)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 4/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDicti.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MethodReturnDictionary
extern TypeInfo MethodReturnDictionary_t1882_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MethodReturnDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodReturnDictiMethodDeclarations.h"
static const EncodedMethodIndex MethodReturnDictionary_t1882_VTable[19] = 
{
	626,
	601,
	627,
	628,
	3798,
	3799,
	3800,
	3801,
	3802,
	3803,
	3804,
	3805,
	3806,
	3807,
	3808,
	3809,
	3818,
	3819,
	3812,
};
static Il2CppInterfaceOffsetPair MethodReturnDictionary_t1882_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IDictionary_t1462_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodReturnDictionary_t1882_0_0_0;
extern const Il2CppType MethodReturnDictionary_t1882_1_0_0;
struct MethodReturnDictionary_t1882;
const Il2CppTypeDefinitionMetadata MethodReturnDictionary_t1882_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodReturnDictionary_t1882_InterfacesOffsets/* interfaceOffsets */
	, &MethodDictionary_t1874_0_0_0/* parent */
	, MethodReturnDictionary_t1882_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8274/* fieldStart */
	, 11764/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodReturnDictionary_t1882_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodReturnDictionary"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MethodReturnDictionary_t1882_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MethodReturnDictionary_t1882_0_0_0/* byval_arg */
	, &MethodReturnDictionary_t1882_1_0_0/* this_arg */
	, &MethodReturnDictionary_t1882_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodReturnDictionary_t1882)/* instance_size */
	, sizeof (MethodReturnDictionary_t1882)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MethodReturnDictionary_t1882_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 19/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.MonoMethodMessage
extern TypeInfo MonoMethodMessage_t1867_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.MonoMethodMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_MonoMethodMessageMethodDeclarations.h"
static const EncodedMethodIndex MonoMethodMessage_t1867_VTable[16] = 
{
	626,
	601,
	627,
	628,
	3825,
	3826,
	3827,
	3828,
	3829,
	3830,
	3831,
	3832,
	3833,
	3834,
	3835,
	3836,
};
static const Il2CppType* MonoMethodMessage_t1867_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2203_0_0_0,
	&IMessage_t1869_0_0_0,
	&IMethodCallMessage_t2185_0_0_0,
	&IMethodMessage_t1881_0_0_0,
	&IMethodReturnMessage_t2184_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethodMessage_t1867_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2203_0_0_0, 4},
	{ &IMessage_t1869_0_0_0, 5},
	{ &IMethodCallMessage_t2185_0_0_0, 5},
	{ &IMethodMessage_t1881_0_0_0, 5},
	{ &IMethodReturnMessage_t2184_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethodMessage_t1867_0_0_0;
extern const Il2CppType MonoMethodMessage_t1867_1_0_0;
struct MonoMethodMessage_t1867;
const Il2CppTypeDefinitionMetadata MonoMethodMessage_t1867_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethodMessage_t1867_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethodMessage_t1867_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoMethodMessage_t1867_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8276/* fieldStart */
	, 11766/* methodStart */
	, -1/* eventStart */
	, 2353/* propertyStart */

};
TypeInfo MonoMethodMessage_t1867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &MonoMethodMessage_t1867_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodMessage_t1867_0_0_0/* byval_arg */
	, &MonoMethodMessage_t1867_1_0_0/* this_arg */
	, &MonoMethodMessage_t1867_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodMessage_t1867)/* instance_size */
	, sizeof (MonoMethodMessage_t1867)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 11/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogate
extern TypeInfo RemotingSurrogate_t1883_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogateMethodDeclarations.h"
static const EncodedMethodIndex RemotingSurrogate_t1883_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3837,
	3837,
};
extern const Il2CppType ISerializationSurrogate_t1952_0_0_0;
static const Il2CppType* RemotingSurrogate_t1883_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t1952_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogate_t1883_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t1952_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogate_t1883_0_0_0;
extern const Il2CppType RemotingSurrogate_t1883_1_0_0;
struct RemotingSurrogate_t1883;
const Il2CppTypeDefinitionMetadata RemotingSurrogate_t1883_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogate_t1883_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogate_t1883_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogate_t1883_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11778/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingSurrogate_t1883_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &RemotingSurrogate_t1883_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingSurrogate_t1883_0_0_0/* byval_arg */
	, &RemotingSurrogate_t1883_1_0_0/* this_arg */
	, &RemotingSurrogate_t1883_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogate_t1883)/* instance_size */
	, sizeof (RemotingSurrogate_t1883)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogate.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ObjRefSurrogate
extern TypeInfo ObjRefSurrogate_t1884_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ObjRefSurrogate
#include "mscorlib_System_Runtime_Remoting_Messaging_ObjRefSurrogateMethodDeclarations.h"
static const EncodedMethodIndex ObjRefSurrogate_t1884_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3838,
	3838,
};
static const Il2CppType* ObjRefSurrogate_t1884_InterfacesTypeInfos[] = 
{
	&ISerializationSurrogate_t1952_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRefSurrogate_t1884_InterfacesOffsets[] = 
{
	{ &ISerializationSurrogate_t1952_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRefSurrogate_t1884_0_0_0;
extern const Il2CppType ObjRefSurrogate_t1884_1_0_0;
struct ObjRefSurrogate_t1884;
const Il2CppTypeDefinitionMetadata ObjRefSurrogate_t1884_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRefSurrogate_t1884_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRefSurrogate_t1884_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRefSurrogate_t1884_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11780/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ObjRefSurrogate_t1884_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRefSurrogate"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ObjRefSurrogate_t1884_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjRefSurrogate_t1884_0_0_0/* byval_arg */
	, &ObjRefSurrogate_t1884_1_0_0/* this_arg */
	, &ObjRefSurrogate_t1884_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRefSurrogate_t1884)/* instance_size */
	, sizeof (ObjRefSurrogate_t1884)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0.h"
// Metadata Definition System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
extern TypeInfo RemotingSurrogateSelector_t1886_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.RemotingSurrogateSelector
#include "mscorlib_System_Runtime_Remoting_Messaging_RemotingSurrogate_0MethodDeclarations.h"
static const EncodedMethodIndex RemotingSurrogateSelector_t1886_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3839,
	3839,
};
extern const Il2CppType ISurrogateSelector_t1885_0_0_0;
static const Il2CppType* RemotingSurrogateSelector_t1886_InterfacesTypeInfos[] = 
{
	&ISurrogateSelector_t1885_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingSurrogateSelector_t1886_InterfacesOffsets[] = 
{
	{ &ISurrogateSelector_t1885_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingSurrogateSelector_t1886_0_0_0;
extern const Il2CppType RemotingSurrogateSelector_t1886_1_0_0;
struct RemotingSurrogateSelector_t1886;
const Il2CppTypeDefinitionMetadata RemotingSurrogateSelector_t1886_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingSurrogateSelector_t1886_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingSurrogateSelector_t1886_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingSurrogateSelector_t1886_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8284/* fieldStart */
	, 11782/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RemotingSurrogateSelector_t1886_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingSurrogateSelector"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &RemotingSurrogateSelector_t1886_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2977/* custom_attributes_cache */
	, &RemotingSurrogateSelector_t1886_0_0_0/* byval_arg */
	, &RemotingSurrogateSelector_t1886_1_0_0/* this_arg */
	, &RemotingSurrogateSelector_t1886_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingSurrogateSelector_t1886)/* instance_size */
	, sizeof (RemotingSurrogateSelector_t1886)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingSurrogateSelector_t1886_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessage.h"
// Metadata Definition System.Runtime.Remoting.Messaging.ReturnMessage
extern TypeInfo ReturnMessage_t1887_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.ReturnMessage
#include "mscorlib_System_Runtime_Remoting_Messaging_ReturnMessageMethodDeclarations.h"
static const EncodedMethodIndex ReturnMessage_t1887_VTable[18] = 
{
	626,
	601,
	627,
	628,
	3840,
	3841,
	3842,
	3843,
	3844,
	3845,
	3846,
	3847,
	3848,
	3849,
	3850,
	3851,
	3852,
	3850,
};
static const Il2CppType* ReturnMessage_t1887_InterfacesTypeInfos[] = 
{
	&IInternalMessage_t2203_0_0_0,
	&IMessage_t1869_0_0_0,
	&IMethodMessage_t1881_0_0_0,
	&IMethodReturnMessage_t2184_0_0_0,
};
static Il2CppInterfaceOffsetPair ReturnMessage_t1887_InterfacesOffsets[] = 
{
	{ &IInternalMessage_t2203_0_0_0, 4},
	{ &IMessage_t1869_0_0_0, 5},
	{ &IMethodMessage_t1881_0_0_0, 5},
	{ &IMethodReturnMessage_t2184_0_0_0, 12},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReturnMessage_t1887_0_0_0;
extern const Il2CppType ReturnMessage_t1887_1_0_0;
struct ReturnMessage_t1887;
const Il2CppTypeDefinitionMetadata ReturnMessage_t1887_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ReturnMessage_t1887_InterfacesTypeInfos/* implementedInterfaces */
	, ReturnMessage_t1887_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReturnMessage_t1887_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8288/* fieldStart */
	, 11785/* methodStart */
	, -1/* eventStart */
	, 2364/* propertyStart */

};
TypeInfo ReturnMessage_t1887_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReturnMessage"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, NULL/* methods */
	, &ReturnMessage_t1887_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2978/* custom_attributes_cache */
	, &ReturnMessage_t1887_0_0_0/* byval_arg */
	, &ReturnMessage_t1887_1_0_0/* this_arg */
	, &ReturnMessage_t1887_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReturnMessage_t1887)/* instance_size */
	, sizeof (ReturnMessage_t1887)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 12/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttribute.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapAttribute
extern TypeInfo SoapAttribute_t1888_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapAttributeMethodDeclarations.h"
static const EncodedMethodIndex SoapAttribute_t1888_VTable[7] = 
{
	1366,
	601,
	1367,
	628,
	3853,
	3854,
	3855,
};
static Il2CppInterfaceOffsetPair SoapAttribute_t1888_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapAttribute_t1888_0_0_0;
extern const Il2CppType SoapAttribute_t1888_1_0_0;
struct SoapAttribute_t1888;
const Il2CppTypeDefinitionMetadata SoapAttribute_t1888_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapAttribute_t1888_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SoapAttribute_t1888_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8301/* fieldStart */
	, 11800/* methodStart */
	, -1/* eventStart */
	, 2376/* propertyStart */

};
TypeInfo SoapAttribute_t1888_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapAttribute_t1888_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2979/* custom_attributes_cache */
	, &SoapAttribute_t1888_0_0_0/* byval_arg */
	, &SoapAttribute_t1888_1_0_0/* this_arg */
	, &SoapAttribute_t1888_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapAttribute_t1888)/* instance_size */
	, sizeof (SoapAttribute_t1888)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapFieldAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFieldAttribute.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapFieldAttribute
extern TypeInfo SoapFieldAttribute_t1889_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapFieldAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapFieldAttributeMethodDeclarations.h"
static const EncodedMethodIndex SoapFieldAttribute_t1889_VTable[7] = 
{
	1366,
	601,
	1367,
	628,
	3853,
	3854,
	3856,
};
static Il2CppInterfaceOffsetPair SoapFieldAttribute_t1889_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapFieldAttribute_t1889_0_0_0;
extern const Il2CppType SoapFieldAttribute_t1889_1_0_0;
struct SoapFieldAttribute_t1889;
const Il2CppTypeDefinitionMetadata SoapFieldAttribute_t1889_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapFieldAttribute_t1889_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1888_0_0_0/* parent */
	, SoapFieldAttribute_t1889_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8304/* fieldStart */
	, 11804/* methodStart */
	, -1/* eventStart */
	, 2378/* propertyStart */

};
TypeInfo SoapFieldAttribute_t1889_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapFieldAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapFieldAttribute_t1889_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2980/* custom_attributes_cache */
	, &SoapFieldAttribute_t1889_0_0_0/* byval_arg */
	, &SoapFieldAttribute_t1889_1_0_0/* this_arg */
	, &SoapFieldAttribute_t1889_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapFieldAttribute_t1889)/* instance_size */
	, sizeof (SoapFieldAttribute_t1889)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapMethodAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMethodAttribut.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapMethodAttribute
extern TypeInfo SoapMethodAttribute_t1890_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapMethodAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapMethodAttributMethodDeclarations.h"
static const EncodedMethodIndex SoapMethodAttribute_t1890_VTable[7] = 
{
	1366,
	601,
	1367,
	628,
	3857,
	3858,
	3859,
};
static Il2CppInterfaceOffsetPair SoapMethodAttribute_t1890_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapMethodAttribute_t1890_0_0_0;
extern const Il2CppType SoapMethodAttribute_t1890_1_0_0;
struct SoapMethodAttribute_t1890;
const Il2CppTypeDefinitionMetadata SoapMethodAttribute_t1890_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapMethodAttribute_t1890_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1888_0_0_0/* parent */
	, SoapMethodAttribute_t1890_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8306/* fieldStart */
	, 11808/* methodStart */
	, -1/* eventStart */
	, 2379/* propertyStart */

};
TypeInfo SoapMethodAttribute_t1890_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapMethodAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapMethodAttribute_t1890_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2981/* custom_attributes_cache */
	, &SoapMethodAttribute_t1890_0_0_0/* byval_arg */
	, &SoapMethodAttribute_t1890_1_0_0/* this_arg */
	, &SoapMethodAttribute_t1890_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapMethodAttribute_t1890)/* instance_size */
	, sizeof (SoapMethodAttribute_t1890)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapParameterAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapParameterAttri.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapParameterAttribute
extern TypeInfo SoapParameterAttribute_t1891_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapParameterAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapParameterAttriMethodDeclarations.h"
static const EncodedMethodIndex SoapParameterAttribute_t1891_VTable[7] = 
{
	1366,
	601,
	1367,
	628,
	3853,
	3854,
	3855,
};
static Il2CppInterfaceOffsetPair SoapParameterAttribute_t1891_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapParameterAttribute_t1891_0_0_0;
extern const Il2CppType SoapParameterAttribute_t1891_1_0_0;
struct SoapParameterAttribute_t1891;
const Il2CppTypeDefinitionMetadata SoapParameterAttribute_t1891_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapParameterAttribute_t1891_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1888_0_0_0/* parent */
	, SoapParameterAttribute_t1891_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11812/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SoapParameterAttribute_t1891_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapParameterAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapParameterAttribute_t1891_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2982/* custom_attributes_cache */
	, &SoapParameterAttribute_t1891_0_0_0/* byval_arg */
	, &SoapParameterAttribute_t1891_1_0_0/* this_arg */
	, &SoapParameterAttribute_t1891_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapParameterAttribute_t1891)/* instance_size */
	, sizeof (SoapParameterAttribute_t1891)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Metadata.SoapTypeAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapTypeAttribute.h"
// Metadata Definition System.Runtime.Remoting.Metadata.SoapTypeAttribute
extern TypeInfo SoapTypeAttribute_t1892_il2cpp_TypeInfo;
// System.Runtime.Remoting.Metadata.SoapTypeAttribute
#include "mscorlib_System_Runtime_Remoting_Metadata_SoapTypeAttributeMethodDeclarations.h"
static const EncodedMethodIndex SoapTypeAttribute_t1892_VTable[7] = 
{
	1366,
	601,
	1367,
	628,
	3860,
	3861,
	3862,
};
static Il2CppInterfaceOffsetPair SoapTypeAttribute_t1892_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SoapTypeAttribute_t1892_0_0_0;
extern const Il2CppType SoapTypeAttribute_t1892_1_0_0;
struct SoapTypeAttribute_t1892;
const Il2CppTypeDefinitionMetadata SoapTypeAttribute_t1892_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SoapTypeAttribute_t1892_InterfacesOffsets/* interfaceOffsets */
	, &SoapAttribute_t1888_0_0_0/* parent */
	, SoapTypeAttribute_t1892_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8312/* fieldStart */
	, 11813/* methodStart */
	, -1/* eventStart */
	, 2381/* propertyStart */

};
TypeInfo SoapTypeAttribute_t1892_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SoapTypeAttribute"/* name */
	, "System.Runtime.Remoting.Metadata"/* namespaze */
	, NULL/* methods */
	, &SoapTypeAttribute_t1892_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2983/* custom_attributes_cache */
	, &SoapTypeAttribute_t1892_0_0_0/* byval_arg */
	, &SoapTypeAttribute_t1892_1_0_0/* this_arg */
	, &SoapTypeAttribute_t1892_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SoapTypeAttribute_t1892)/* instance_size */
	, sizeof (SoapTypeAttribute_t1892)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttribute.h"
// Metadata Definition System.Runtime.Remoting.Proxies.ProxyAttribute
extern TypeInfo ProxyAttribute_t1893_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.ProxyAttribute
#include "mscorlib_System_Runtime_Remoting_Proxies_ProxyAttributeMethodDeclarations.h"
static const EncodedMethodIndex ProxyAttribute_t1893_VTable[8] = 
{
	1366,
	601,
	1367,
	628,
	3863,
	3864,
	3865,
	3866,
};
static const Il2CppType* ProxyAttribute_t1893_InterfacesTypeInfos[] = 
{
	&IContextAttribute_t2197_0_0_0,
};
static Il2CppInterfaceOffsetPair ProxyAttribute_t1893_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
	{ &IContextAttribute_t2197_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProxyAttribute_t1893_0_0_0;
extern const Il2CppType ProxyAttribute_t1893_1_0_0;
struct ProxyAttribute_t1893;
const Il2CppTypeDefinitionMetadata ProxyAttribute_t1893_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ProxyAttribute_t1893_InterfacesTypeInfos/* implementedInterfaces */
	, ProxyAttribute_t1893_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ProxyAttribute_t1893_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11822/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ProxyAttribute_t1893_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProxyAttribute"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &ProxyAttribute_t1893_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2984/* custom_attributes_cache */
	, &ProxyAttribute_t1893_0_0_0/* byval_arg */
	, &ProxyAttribute_t1893_1_0_0/* this_arg */
	, &ProxyAttribute_t1893_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProxyAttribute_t1893)/* instance_size */
	, sizeof (ProxyAttribute_t1893)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.TransparentProxy
extern TypeInfo TransparentProxy_t1895_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.TransparentProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_TransparentProxyMethodDeclarations.h"
static const EncodedMethodIndex TransparentProxy_t1895_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TransparentProxy_t1895_0_0_0;
extern const Il2CppType TransparentProxy_t1895_1_0_0;
struct TransparentProxy_t1895;
const Il2CppTypeDefinitionMetadata TransparentProxy_t1895_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TransparentProxy_t1895_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8319/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TransparentProxy_t1895_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TransparentProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &TransparentProxy_t1895_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TransparentProxy_t1895_0_0_0/* byval_arg */
	, &TransparentProxy_t1895_1_0_0/* this_arg */
	, &TransparentProxy_t1895_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TransparentProxy_t1895)/* instance_size */
	, sizeof (TransparentProxy_t1895)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RealProxy
extern TypeInfo RealProxy_t1894_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RealProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RealProxyMethodDeclarations.h"
static const EncodedMethodIndex RealProxy_t1894_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3867,
	3868,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RealProxy_t1894_0_0_0;
extern const Il2CppType RealProxy_t1894_1_0_0;
struct RealProxy_t1894;
const Il2CppTypeDefinitionMetadata RealProxy_t1894_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RealProxy_t1894_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8320/* fieldStart */
	, 11826/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RealProxy_t1894_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RealProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &RealProxy_t1894_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2987/* custom_attributes_cache */
	, &RealProxy_t1894_0_0_0/* byval_arg */
	, &RealProxy_t1894_1_0_0/* this_arg */
	, &RealProxy_t1894_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RealProxy_t1894)/* instance_size */
	, sizeof (RealProxy_t1894)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxy.h"
// Metadata Definition System.Runtime.Remoting.Proxies.RemotingProxy
extern TypeInfo RemotingProxy_t1898_il2cpp_TypeInfo;
// System.Runtime.Remoting.Proxies.RemotingProxy
#include "mscorlib_System_Runtime_Remoting_Proxies_RemotingProxyMethodDeclarations.h"
static const EncodedMethodIndex RemotingProxy_t1898_VTable[7] = 
{
	626,
	3869,
	627,
	628,
	3867,
	3868,
	3870,
};
extern const Il2CppType IRemotingTypeInfo_t1909_0_0_0;
static const Il2CppType* RemotingProxy_t1898_InterfacesTypeInfos[] = 
{
	&IRemotingTypeInfo_t1909_0_0_0,
};
static Il2CppInterfaceOffsetPair RemotingProxy_t1898_InterfacesOffsets[] = 
{
	{ &IRemotingTypeInfo_t1909_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingProxy_t1898_0_0_0;
extern const Il2CppType RemotingProxy_t1898_1_0_0;
struct RemotingProxy_t1898;
const Il2CppTypeDefinitionMetadata RemotingProxy_t1898_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RemotingProxy_t1898_InterfacesTypeInfos/* implementedInterfaces */
	, RemotingProxy_t1898_InterfacesOffsets/* interfaceOffsets */
	, &RealProxy_t1894_0_0_0/* parent */
	, RemotingProxy_t1898_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8325/* fieldStart */
	, 11834/* methodStart */
	, -1/* eventStart */
	, 2388/* propertyStart */

};
TypeInfo RemotingProxy_t1898_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingProxy"/* name */
	, "System.Runtime.Remoting.Proxies"/* namespaze */
	, NULL/* methods */
	, &RemotingProxy_t1898_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RemotingProxy_t1898_0_0_0/* byval_arg */
	, &RemotingProxy_t1898_1_0_0/* this_arg */
	, &RemotingProxy_t1898_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingProxy_t1898)/* instance_size */
	, sizeof (RemotingProxy_t1898)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingProxy_t1898_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.Services.ITrackingHandler
extern TypeInfo ITrackingHandler_t2205_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ITrackingHandler_t2205_0_0_0;
extern const Il2CppType ITrackingHandler_t2205_1_0_0;
struct ITrackingHandler_t2205;
const Il2CppTypeDefinitionMetadata ITrackingHandler_t2205_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11839/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ITrackingHandler_t2205_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITrackingHandler"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, NULL/* methods */
	, &ITrackingHandler_t2205_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2988/* custom_attributes_cache */
	, &ITrackingHandler_t2205_0_0_0/* byval_arg */
	, &ITrackingHandler_t2205_1_0_0/* this_arg */
	, &ITrackingHandler_t2205_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServices.h"
// Metadata Definition System.Runtime.Remoting.Services.TrackingServices
extern TypeInfo TrackingServices_t1899_il2cpp_TypeInfo;
// System.Runtime.Remoting.Services.TrackingServices
#include "mscorlib_System_Runtime_Remoting_Services_TrackingServicesMethodDeclarations.h"
static const EncodedMethodIndex TrackingServices_t1899_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TrackingServices_t1899_0_0_0;
extern const Il2CppType TrackingServices_t1899_1_0_0;
struct TrackingServices_t1899;
const Il2CppTypeDefinitionMetadata TrackingServices_t1899_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackingServices_t1899_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8330/* fieldStart */
	, 11840/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TrackingServices_t1899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackingServices"/* name */
	, "System.Runtime.Remoting.Services"/* namespaze */
	, NULL/* methods */
	, &TrackingServices_t1899_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2989/* custom_attributes_cache */
	, &TrackingServices_t1899_0_0_0/* byval_arg */
	, &TrackingServices_t1899_1_0_0/* this_arg */
	, &TrackingServices_t1899_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TrackingServices_t1899)/* instance_size */
	, sizeof (TrackingServices_t1899)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TrackingServices_t1899_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedClientTypeEntry
extern TypeInfo ActivatedClientTypeEntry_t1900_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedClientTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedClientTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex ActivatedClientTypeEntry_t1900_VTable[4] = 
{
	626,
	601,
	627,
	3871,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivatedClientTypeEntry_t1900_0_0_0;
extern const Il2CppType ActivatedClientTypeEntry_t1900_1_0_0;
extern const Il2CppType TypeEntry_t1901_0_0_0;
struct ActivatedClientTypeEntry_t1900;
const Il2CppTypeDefinitionMetadata ActivatedClientTypeEntry_t1900_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1901_0_0_0/* parent */
	, ActivatedClientTypeEntry_t1900_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8331/* fieldStart */
	, 11842/* methodStart */
	, -1/* eventStart */
	, 2389/* propertyStart */

};
TypeInfo ActivatedClientTypeEntry_t1900_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedClientTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ActivatedClientTypeEntry_t1900_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2990/* custom_attributes_cache */
	, &ActivatedClientTypeEntry_t1900_0_0_0/* byval_arg */
	, &ActivatedClientTypeEntry_t1900_1_0_0/* this_arg */
	, &ActivatedClientTypeEntry_t1900_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedClientTypeEntry_t1900)/* instance_size */
	, sizeof (ActivatedClientTypeEntry_t1900)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ActivatedServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedServiceTypeEntry.h"
// Metadata Definition System.Runtime.Remoting.ActivatedServiceTypeEntry
extern TypeInfo ActivatedServiceTypeEntry_t1902_il2cpp_TypeInfo;
// System.Runtime.Remoting.ActivatedServiceTypeEntry
#include "mscorlib_System_Runtime_Remoting_ActivatedServiceTypeEntryMethodDeclarations.h"
static const EncodedMethodIndex ActivatedServiceTypeEntry_t1902_VTable[4] = 
{
	626,
	601,
	627,
	3872,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivatedServiceTypeEntry_t1902_0_0_0;
extern const Il2CppType ActivatedServiceTypeEntry_t1902_1_0_0;
struct ActivatedServiceTypeEntry_t1902;
const Il2CppTypeDefinitionMetadata ActivatedServiceTypeEntry_t1902_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &TypeEntry_t1901_0_0_0/* parent */
	, ActivatedServiceTypeEntry_t1902_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8333/* fieldStart */
	, 11847/* methodStart */
	, -1/* eventStart */
	, 2392/* propertyStart */

};
TypeInfo ActivatedServiceTypeEntry_t1902_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivatedServiceTypeEntry"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ActivatedServiceTypeEntry_t1902_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2991/* custom_attributes_cache */
	, &ActivatedServiceTypeEntry_t1902_0_0_0/* byval_arg */
	, &ActivatedServiceTypeEntry_t1902_1_0_0/* this_arg */
	, &ActivatedServiceTypeEntry_t1902_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivatedServiceTypeEntry_t1902)/* instance_size */
	, sizeof (ActivatedServiceTypeEntry_t1902)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfo.h"
// Metadata Definition System.Runtime.Remoting.EnvoyInfo
extern TypeInfo EnvoyInfo_t1903_il2cpp_TypeInfo;
// System.Runtime.Remoting.EnvoyInfo
#include "mscorlib_System_Runtime_Remoting_EnvoyInfoMethodDeclarations.h"
static const EncodedMethodIndex EnvoyInfo_t1903_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3873,
};
extern const Il2CppType IEnvoyInfo_t1910_0_0_0;
static const Il2CppType* EnvoyInfo_t1903_InterfacesTypeInfos[] = 
{
	&IEnvoyInfo_t1910_0_0_0,
};
static Il2CppInterfaceOffsetPair EnvoyInfo_t1903_InterfacesOffsets[] = 
{
	{ &IEnvoyInfo_t1910_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnvoyInfo_t1903_0_0_0;
extern const Il2CppType EnvoyInfo_t1903_1_0_0;
struct EnvoyInfo_t1903;
const Il2CppTypeDefinitionMetadata EnvoyInfo_t1903_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnvoyInfo_t1903_InterfacesTypeInfos/* implementedInterfaces */
	, EnvoyInfo_t1903_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EnvoyInfo_t1903_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8334/* fieldStart */
	, 11850/* methodStart */
	, -1/* eventStart */
	, 2393/* propertyStart */

};
TypeInfo EnvoyInfo_t1903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &EnvoyInfo_t1903_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnvoyInfo_t1903_0_0_0/* byval_arg */
	, &EnvoyInfo_t1903_1_0_0/* this_arg */
	, &EnvoyInfo_t1903_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnvoyInfo_t1903)/* instance_size */
	, sizeof (EnvoyInfo_t1903)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IChannelInfo
extern TypeInfo IChannelInfo_t1908_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IChannelInfo_t1908_1_0_0;
struct IChannelInfo_t1908;
const Il2CppTypeDefinitionMetadata IChannelInfo_t1908_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11852/* methodStart */
	, -1/* eventStart */
	, 2394/* propertyStart */

};
TypeInfo IChannelInfo_t1908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IChannelInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &IChannelInfo_t1908_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2992/* custom_attributes_cache */
	, &IChannelInfo_t1908_0_0_0/* byval_arg */
	, &IChannelInfo_t1908_1_0_0/* this_arg */
	, &IChannelInfo_t1908_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IEnvoyInfo
extern TypeInfo IEnvoyInfo_t1910_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEnvoyInfo_t1910_1_0_0;
struct IEnvoyInfo_t1910;
const Il2CppTypeDefinitionMetadata IEnvoyInfo_t1910_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11853/* methodStart */
	, -1/* eventStart */
	, 2395/* propertyStart */

};
TypeInfo IEnvoyInfo_t1910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnvoyInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &IEnvoyInfo_t1910_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2993/* custom_attributes_cache */
	, &IEnvoyInfo_t1910_0_0_0/* byval_arg */
	, &IEnvoyInfo_t1910_1_0_0/* this_arg */
	, &IEnvoyInfo_t1910_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.Remoting.IRemotingTypeInfo
extern TypeInfo IRemotingTypeInfo_t1909_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IRemotingTypeInfo_t1909_1_0_0;
struct IRemotingTypeInfo_t1909;
const Il2CppTypeDefinitionMetadata IRemotingTypeInfo_t1909_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11854/* methodStart */
	, -1/* eventStart */
	, 2396/* propertyStart */

};
TypeInfo IRemotingTypeInfo_t1909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IRemotingTypeInfo"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &IRemotingTypeInfo_t1909_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2994/* custom_attributes_cache */
	, &IRemotingTypeInfo_t1909_0_0_0/* byval_arg */
	, &IRemotingTypeInfo_t1909_1_0_0/* this_arg */
	, &IRemotingTypeInfo_t1909_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// Metadata Definition System.Runtime.Remoting.Identity
extern TypeInfo Identity_t1896_il2cpp_TypeInfo;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_IdentityMethodDeclarations.h"
static const EncodedMethodIndex Identity_t1896_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Identity_t1896_0_0_0;
extern const Il2CppType Identity_t1896_1_0_0;
struct Identity_t1896;
const Il2CppTypeDefinitionMetadata Identity_t1896_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Identity_t1896_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8335/* fieldStart */
	, 11855/* methodStart */
	, -1/* eventStart */
	, 2397/* propertyStart */

};
TypeInfo Identity_t1896_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Identity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &Identity_t1896_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Identity_t1896_0_0_0/* byval_arg */
	, &Identity_t1896_1_0_0/* this_arg */
	, &Identity_t1896_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Identity_t1896)/* instance_size */
	, sizeof (Identity_t1896)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentity.h"
// Metadata Definition System.Runtime.Remoting.ClientIdentity
extern TypeInfo ClientIdentity_t1906_il2cpp_TypeInfo;
// System.Runtime.Remoting.ClientIdentity
#include "mscorlib_System_Runtime_Remoting_ClientIdentityMethodDeclarations.h"
static const EncodedMethodIndex ClientIdentity_t1906_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3874,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClientIdentity_t1906_0_0_0;
extern const Il2CppType ClientIdentity_t1906_1_0_0;
struct ClientIdentity_t1906;
const Il2CppTypeDefinitionMetadata ClientIdentity_t1906_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Identity_t1896_0_0_0/* parent */
	, ClientIdentity_t1906_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8340/* fieldStart */
	, 11862/* methodStart */
	, -1/* eventStart */
	, 2400/* propertyStart */

};
TypeInfo ClientIdentity_t1906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientIdentity"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ClientIdentity_t1906_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientIdentity_t1906_0_0_0/* byval_arg */
	, &ClientIdentity_t1906_1_0_0/* this_arg */
	, &ClientIdentity_t1906_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientIdentity_t1906)/* instance_size */
	, sizeof (ClientIdentity_t1906)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.InternalRemotingServices
#include "mscorlib_System_Runtime_Remoting_InternalRemotingServices.h"
// Metadata Definition System.Runtime.Remoting.InternalRemotingServices
extern TypeInfo InternalRemotingServices_t1907_il2cpp_TypeInfo;
// System.Runtime.Remoting.InternalRemotingServices
#include "mscorlib_System_Runtime_Remoting_InternalRemotingServicesMethodDeclarations.h"
static const EncodedMethodIndex InternalRemotingServices_t1907_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InternalRemotingServices_t1907_0_0_0;
extern const Il2CppType InternalRemotingServices_t1907_1_0_0;
struct InternalRemotingServices_t1907;
const Il2CppTypeDefinitionMetadata InternalRemotingServices_t1907_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InternalRemotingServices_t1907_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8341/* fieldStart */
	, 11867/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InternalRemotingServices_t1907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalRemotingServices"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &InternalRemotingServices_t1907_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2995/* custom_attributes_cache */
	, &InternalRemotingServices_t1907_0_0_0/* byval_arg */
	, &InternalRemotingServices_t1907_1_0_0/* this_arg */
	, &InternalRemotingServices_t1907_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalRemotingServices_t1907)/* instance_size */
	, sizeof (InternalRemotingServices_t1907)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(InternalRemotingServices_t1907_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRef.h"
// Metadata Definition System.Runtime.Remoting.ObjRef
extern TypeInfo ObjRef_t1904_il2cpp_TypeInfo;
// System.Runtime.Remoting.ObjRef
#include "mscorlib_System_Runtime_Remoting_ObjRefMethodDeclarations.h"
static const EncodedMethodIndex ObjRef_t1904_VTable[15] = 
{
	626,
	601,
	627,
	628,
	3875,
	3876,
	3877,
	3878,
	3879,
	3880,
	3881,
	3882,
	3883,
	3875,
	3876,
};
extern const Il2CppType IObjectReference_t2211_0_0_0;
static const Il2CppType* ObjRef_t1904_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IObjectReference_t2211_0_0_0,
};
static Il2CppInterfaceOffsetPair ObjRef_t1904_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IObjectReference_t2211_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjRef_t1904_0_0_0;
extern const Il2CppType ObjRef_t1904_1_0_0;
struct ObjRef_t1904;
const Il2CppTypeDefinitionMetadata ObjRef_t1904_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ObjRef_t1904_InterfacesTypeInfos/* implementedInterfaces */
	, ObjRef_t1904_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjRef_t1904_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8342/* fieldStart */
	, 11869/* methodStart */
	, -1/* eventStart */
	, 2402/* propertyStart */

};
TypeInfo ObjRef_t1904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjRef"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ObjRef_t1904_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2996/* custom_attributes_cache */
	, &ObjRef_t1904_0_0_0/* byval_arg */
	, &ObjRef_t1904_1_0_0/* this_arg */
	, &ObjRef_t1904_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjRef_t1904)/* instance_size */
	, sizeof (ObjRef_t1904)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ObjRef_t1904_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfiguration.h"
// Metadata Definition System.Runtime.Remoting.RemotingConfiguration
extern TypeInfo RemotingConfiguration_t1911_il2cpp_TypeInfo;
// System.Runtime.Remoting.RemotingConfiguration
#include "mscorlib_System_Runtime_Remoting_RemotingConfigurationMethodDeclarations.h"
static const EncodedMethodIndex RemotingConfiguration_t1911_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RemotingConfiguration_t1911_0_0_0;
extern const Il2CppType RemotingConfiguration_t1911_1_0_0;
struct RemotingConfiguration_t1911;
const Il2CppTypeDefinitionMetadata RemotingConfiguration_t1911_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RemotingConfiguration_t1911_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8351/* fieldStart */
	, 11884/* methodStart */
	, -1/* eventStart */
	, 2408/* propertyStart */

};
TypeInfo RemotingConfiguration_t1911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RemotingConfiguration"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &RemotingConfiguration_t1911_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2999/* custom_attributes_cache */
	, &RemotingConfiguration_t1911_0_0_0/* byval_arg */
	, &RemotingConfiguration_t1911_1_0_0/* this_arg */
	, &RemotingConfiguration_t1911_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RemotingConfiguration_t1911)/* instance_size */
	, sizeof (RemotingConfiguration_t1911)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RemotingConfiguration_t1911_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 2/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ConfigHandler
#include "mscorlib_System_Runtime_Remoting_ConfigHandler.h"
// Metadata Definition System.Runtime.Remoting.ConfigHandler
extern TypeInfo ConfigHandler_t1913_il2cpp_TypeInfo;
// System.Runtime.Remoting.ConfigHandler
#include "mscorlib_System_Runtime_Remoting_ConfigHandlerMethodDeclarations.h"
static const EncodedMethodIndex ConfigHandler_t1913_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3884,
	3885,
	3886,
	3887,
	3888,
	3889,
	3890,
};
extern const Il2CppType IContentHandler_t1643_0_0_0;
static const Il2CppType* ConfigHandler_t1913_InterfacesTypeInfos[] = 
{
	&IContentHandler_t1643_0_0_0,
};
static Il2CppInterfaceOffsetPair ConfigHandler_t1913_InterfacesOffsets[] = 
{
	{ &IContentHandler_t1643_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConfigHandler_t1913_0_0_0;
extern const Il2CppType ConfigHandler_t1913_1_0_0;
struct ConfigHandler_t1913;
const Il2CppTypeDefinitionMetadata ConfigHandler_t1913_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConfigHandler_t1913_InterfacesTypeInfos/* implementedInterfaces */
	, ConfigHandler_t1913_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ConfigHandler_t1913_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8364/* fieldStart */
	, 11900/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConfigHandler_t1913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConfigHandler"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ConfigHandler_t1913_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConfigHandler_t1913_0_0_0/* byval_arg */
	, &ConfigHandler_t1913_1_0_0/* this_arg */
	, &ConfigHandler_t1913_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConfigHandler_t1913)/* instance_size */
	, sizeof (ConfigHandler_t1913)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConfigHandler_t1913_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Remoting.ChannelData
#include "mscorlib_System_Runtime_Remoting_ChannelData.h"
// Metadata Definition System.Runtime.Remoting.ChannelData
extern TypeInfo ChannelData_t1912_il2cpp_TypeInfo;
// System.Runtime.Remoting.ChannelData
#include "mscorlib_System_Runtime_Remoting_ChannelDataMethodDeclarations.h"
static const EncodedMethodIndex ChannelData_t1912_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ChannelData_t1912_0_0_0;
extern const Il2CppType ChannelData_t1912_1_0_0;
struct ChannelData_t1912;
const Il2CppTypeDefinitionMetadata ChannelData_t1912_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ChannelData_t1912_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8374/* fieldStart */
	, 11924/* methodStart */
	, -1/* eventStart */
	, 2410/* propertyStart */

};
TypeInfo ChannelData_t1912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ChannelData"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ChannelData_t1912_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ChannelData_t1912_0_0_0/* byval_arg */
	, &ChannelData_t1912_1_0_0/* this_arg */
	, &ChannelData_t1912_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ChannelData_t1912)/* instance_size */
	, sizeof (ChannelData_t1912)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.Remoting.ProviderData
#include "mscorlib_System_Runtime_Remoting_ProviderData.h"
// Metadata Definition System.Runtime.Remoting.ProviderData
extern TypeInfo ProviderData_t1914_il2cpp_TypeInfo;
// System.Runtime.Remoting.ProviderData
#include "mscorlib_System_Runtime_Remoting_ProviderDataMethodDeclarations.h"
static const EncodedMethodIndex ProviderData_t1914_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProviderData_t1914_0_0_0;
extern const Il2CppType ProviderData_t1914_1_0_0;
struct ProviderData_t1914;
const Il2CppTypeDefinitionMetadata ProviderData_t1914_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ProviderData_t1914_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8381/* fieldStart */
	, 11929/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ProviderData_t1914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProviderData"/* name */
	, "System.Runtime.Remoting"/* namespaze */
	, NULL/* methods */
	, &ProviderData_t1914_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ProviderData_t1914_0_0_0/* byval_arg */
	, &ProviderData_t1914_1_0_0/* this_arg */
	, &ProviderData_t1914_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProviderData_t1914)/* instance_size */
	, sizeof (ProviderData_t1914)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
