﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t1019;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t1034;

// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern "C" AchievementDescription_t1034 * GcAchievementDescriptionData_ToAchievementDescription_m6167 (GcAchievementDescriptionData_t1019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
