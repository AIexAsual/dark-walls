﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ObjectSFX
struct ObjectSFX_t383;

// System.Void ObjectSFX::.ctor()
extern "C" void ObjectSFX__ctor_m2197 (ObjectSFX_t383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ObjectSFX::.cctor()
extern "C" void ObjectSFX__cctor_m2198 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
