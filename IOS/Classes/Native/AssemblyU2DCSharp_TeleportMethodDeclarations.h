﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Teleport
struct Teleport_t214;

// System.Void Teleport::.ctor()
extern "C" void Teleport__ctor_m1383 (Teleport_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::Start()
extern "C" void Teleport_Start_m1384 (Teleport_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::SetGazedAt(System.Boolean)
extern "C" void Teleport_SetGazedAt_m1385 (Teleport_t214 * __this, bool ___gazedAt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::Reset()
extern "C" void Teleport_Reset_m1386 (Teleport_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::ToggleVRMode()
extern "C" void Teleport_ToggleVRMode_m1387 (Teleport_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Teleport::TeleportRandomly()
extern "C" void Teleport_TeleportRandomly_m1388 (Teleport_t214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
