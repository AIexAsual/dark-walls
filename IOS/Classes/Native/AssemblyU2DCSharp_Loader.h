﻿#pragma once
#include <stdint.h>
// UnityEngine.Animator
struct Animator_t321;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Loader
struct  Loader_t346  : public MonoBehaviour_t4
{
	// UnityEngine.Animator Loader::anim
	Animator_t321 * ___anim_2;
	// System.Boolean Loader::isDoneload
	bool ___isDoneload_3;
	// System.Single Loader::val
	float ___val_4;
	// System.Boolean Loader::goLoad
	bool ___goLoad_5;
	// System.Single Loader::t
	float ___t_6;
};
