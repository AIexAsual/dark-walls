﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t307;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// AICharacterControl
struct  AICharacterControl_t308  : public MonoBehaviour_t4
{
	// UnityEngine.Transform AICharacterControl::target
	Transform_t243 * ___target_2;
	// System.Single AICharacterControl::targetChangeTolerance
	float ___targetChangeTolerance_3;
	// UnityEngine.Vector3 AICharacterControl::targetPos
	Vector3_t215  ___targetPos_4;
	// UnityEngine.NavMeshAgent AICharacterControl::<agent>k__BackingField
	NavMeshAgent_t307 * ___U3CagentU3Ek__BackingField_5;
};
