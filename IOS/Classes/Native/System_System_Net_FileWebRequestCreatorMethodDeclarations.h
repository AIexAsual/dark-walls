﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FileWebRequestCreator
struct FileWebRequestCreator_t1401;
// System.Net.WebRequest
struct WebRequest_t1364;
// System.Uri
struct Uri_t237;

// System.Void System.Net.FileWebRequestCreator::.ctor()
extern "C" void FileWebRequestCreator__ctor_m7663 (FileWebRequestCreator_t1401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FileWebRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1364 * FileWebRequestCreator_Create_m7664 (FileWebRequestCreator_t1401 * __this, Uri_t237 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
