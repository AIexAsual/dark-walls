﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct Comparer_1_t2691;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Comparer_1__ctor_m20175_gshared (Comparer_1_t2691 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m20175(__this, method) (( void (*) (Comparer_1_t2691 *, const MethodInfo*))Comparer_1__ctor_m20175_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void Comparer_1__cctor_m20176_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m20176(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m20176_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m20177_gshared (Comparer_1_t2691 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m20177(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2691 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m20177_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::get_Default()
extern "C" Comparer_1_t2691 * Comparer_1_get_Default_m20178_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m20178(__this /* static, unused */, method) (( Comparer_1_t2691 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m20178_gshared)(__this /* static, unused */, method)
