﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Int32>
struct InternalEnumerator_1_t2236;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13681_gshared (InternalEnumerator_1_t2236 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13681(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2236 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13681_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13682_gshared (InternalEnumerator_1_t2236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13682(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2236 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13682_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13683_gshared (InternalEnumerator_1_t2236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13683(__this, method) (( void (*) (InternalEnumerator_1_t2236 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13683_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13684_gshared (InternalEnumerator_1_t2236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13684(__this, method) (( bool (*) (InternalEnumerator_1_t2236 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13684_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m13685_gshared (InternalEnumerator_1_t2236 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13685(__this, method) (( int32_t (*) (InternalEnumerator_1_t2236 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13685_gshared)(__this, method)
