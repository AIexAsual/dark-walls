﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNUseExample
struct MNUseExample_t275;
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"

// System.Void MNUseExample::.ctor()
extern "C" void MNUseExample__ctor_m1723 (MNUseExample_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::Awake()
extern "C" void MNUseExample_Awake_m1724 (MNUseExample_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnGUI()
extern "C" void MNUseExample_OnGUI_m1725 (MNUseExample_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnPreloaderTimeOut()
extern "C" void MNUseExample_OnPreloaderTimeOut_m1726 (MNUseExample_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnRatePopUpClose(MNDialogResult)
extern "C" void MNUseExample_OnRatePopUpClose_m1727 (MNUseExample_t275 * __this, int32_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnDialogClose(MNDialogResult)
extern "C" void MNUseExample_OnDialogClose_m1728 (MNUseExample_t275 * __this, int32_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNUseExample::OnMessageClose()
extern "C" void MNUseExample_OnMessageClose_m1729 (MNUseExample_t275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
