﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Water_Drop
struct CameraFilterPack_Distortion_Water_Drop_t93;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Water_Drop::.ctor()
extern "C" void CameraFilterPack_Distortion_Water_Drop__ctor_m588 (CameraFilterPack_Distortion_Water_Drop_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Water_Drop::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Water_Drop_get_material_m589 (CameraFilterPack_Distortion_Water_Drop_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::Start()
extern "C" void CameraFilterPack_Distortion_Water_Drop_Start_m590 (CameraFilterPack_Distortion_Water_Drop_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Water_Drop_OnRenderImage_m591 (CameraFilterPack_Distortion_Water_Drop_t93 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::OnValidate()
extern "C" void CameraFilterPack_Distortion_Water_Drop_OnValidate_m592 (CameraFilterPack_Distortion_Water_Drop_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::Update()
extern "C" void CameraFilterPack_Distortion_Water_Drop_Update_m593 (CameraFilterPack_Distortion_Water_Drop_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Water_Drop::OnDisable()
extern "C" void CameraFilterPack_Distortion_Water_Drop_OnDisable_m594 (CameraFilterPack_Distortion_Water_Drop_t93 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
