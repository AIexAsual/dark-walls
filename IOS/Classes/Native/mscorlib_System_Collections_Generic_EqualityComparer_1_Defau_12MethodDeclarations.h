﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t2689;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m20168_gshared (DefaultComparer_t2689 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m20168(__this, method) (( void (*) (DefaultComparer_t2689 *, const MethodInfo*))DefaultComparer__ctor_m20168_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m20169_gshared (DefaultComparer_t2689 * __this, UICharInfo_t811  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m20169(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2689 *, UICharInfo_t811 , const MethodInfo*))DefaultComparer_GetHashCode_m20169_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m20170_gshared (DefaultComparer_t2689 * __this, UICharInfo_t811  ___x, UICharInfo_t811  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m20170(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2689 *, UICharInfo_t811 , UICharInfo_t811 , const MethodInfo*))DefaultComparer_Equals_m20170_gshared)(__this, ___x, ___y, method)
