﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_LinearBurn
struct CameraFilterPack_Blend2Camera_LinearBurn_t32;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_LinearBurn::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_LinearBurn__ctor_m169 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_LinearBurn::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_LinearBurn_get_material_m170 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::Start()
extern "C" void CameraFilterPack_Blend2Camera_LinearBurn_Start_m171 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_LinearBurn_OnRenderImage_m172 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_LinearBurn_OnValidate_m173 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::Update()
extern "C" void CameraFilterPack_Blend2Camera_LinearBurn_Update_m174 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_LinearBurn_OnEnable_m175 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearBurn::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_LinearBurn_OnDisable_m176 (CameraFilterPack_Blend2Camera_LinearBurn_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
