﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.Light
struct Light_t318;
// InteractiveObject
#include "AssemblyU2DCSharp_InteractiveObject.h"
// InteractiveFlashlight
struct  InteractiveFlashlight_t329  : public InteractiveObject_t328
{
	// UnityEngine.GameObject InteractiveFlashlight::mySpotlight
	GameObject_t256 * ___mySpotlight_4;
	// UnityEngine.Light InteractiveFlashlight::lights
	Light_t318 * ___lights_5;
	// System.Boolean InteractiveFlashlight::isLightOn
	bool ___isLightOn_6;
};
