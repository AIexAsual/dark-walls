﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Char>
struct EqualityComparer_1_t2293;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Char>
struct  EqualityComparer_1_t2293  : public Object_t
{
};
struct EqualityComparer_1_t2293_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Char>::_default
	EqualityComparer_1_t2293 * ____default_0;
};
