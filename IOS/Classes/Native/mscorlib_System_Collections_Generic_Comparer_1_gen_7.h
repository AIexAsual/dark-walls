﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Space>
struct Comparer_1_t2446;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Space>
struct  Comparer_1_t2446  : public Object_t
{
};
struct Comparer_1_t2446_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Space>::_default
	Comparer_1_t2446 * ____default_0;
};
