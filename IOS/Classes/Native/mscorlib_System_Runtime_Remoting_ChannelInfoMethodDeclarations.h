﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t1850;
// System.Object[]
struct ObjectU5BU5D_t470;

// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
extern "C" void ChannelInfo__ctor_m11298 (ChannelInfo_t1850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
extern "C" ObjectU5BU5D_t470* ChannelInfo_get_ChannelData_m11299 (ChannelInfo_t1850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
