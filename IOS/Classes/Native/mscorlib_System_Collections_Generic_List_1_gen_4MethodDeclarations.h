﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t577;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Boolean>
struct IEnumerable_1_t3027;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t3028;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<System.Boolean>
struct ICollection_1_t3029;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>
struct ReadOnlyCollection_1_t2412;
// System.Boolean[]
struct BooleanU5BU5D_t448;
// System.Predicate`1<System.Boolean>
struct Predicate_1_t2417;
// System.Comparison`1<System.Boolean>
struct Comparison_1_t2421;
// System.Collections.Generic.List`1/Enumerator<System.Boolean>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1<System.Boolean>::.ctor()
extern "C" void List_1__ctor_m3391_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1__ctor_m3391(__this, method) (( void (*) (List_1_t577 *, const MethodInfo*))List_1__ctor_m3391_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15785_gshared (List_1_t577 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m15785(__this, ___collection, method) (( void (*) (List_1_t577 *, Object_t*, const MethodInfo*))List_1__ctor_m15785_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15786_gshared (List_1_t577 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m15786(__this, ___capacity, method) (( void (*) (List_1_t577 *, int32_t, const MethodInfo*))List_1__ctor_m15786_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::.cctor()
extern "C" void List_1__cctor_m15787_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m15787(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15787_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Boolean>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15788_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15788(__this, method) (( Object_t* (*) (List_1_t577 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15788_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15789_gshared (List_1_t577 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m15789(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t577 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15789_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m15790_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15790(__this, method) (( Object_t * (*) (List_1_t577 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15790_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m15791_gshared (List_1_t577 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m15791(__this, ___item, method) (( int32_t (*) (List_1_t577 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m15791_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m15792_gshared (List_1_t577 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m15792(__this, ___item, method) (( bool (*) (List_1_t577 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m15792_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m15793_gshared (List_1_t577 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m15793(__this, ___item, method) (( int32_t (*) (List_1_t577 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m15793_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m15794_gshared (List_1_t577 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m15794(__this, ___index, ___item, method) (( void (*) (List_1_t577 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m15794_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m15795_gshared (List_1_t577 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m15795(__this, ___item, method) (( void (*) (List_1_t577 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m15795_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15796_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15796(__this, method) (( bool (*) (List_1_t577 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m15797_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15797(__this, method) (( bool (*) (List_1_t577 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15797_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m15798_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m15798(__this, method) (( Object_t * (*) (List_1_t577 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15798_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m15799_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m15799(__this, method) (( bool (*) (List_1_t577 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15799_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m15800_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m15800(__this, method) (( bool (*) (List_1_t577 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15800_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m15801_gshared (List_1_t577 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m15801(__this, ___index, method) (( Object_t * (*) (List_1_t577 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m15801_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m15802_gshared (List_1_t577 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m15802(__this, ___index, ___value, method) (( void (*) (List_1_t577 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m15802_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::Add(T)
extern "C" void List_1_Add_m15803_gshared (List_1_t577 * __this, bool ___item, const MethodInfo* method);
#define List_1_Add_m15803(__this, ___item, method) (( void (*) (List_1_t577 *, bool, const MethodInfo*))List_1_Add_m15803_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15804_gshared (List_1_t577 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m15804(__this, ___newCount, method) (( void (*) (List_1_t577 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15804_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15805_gshared (List_1_t577 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m15805(__this, ___collection, method) (( void (*) (List_1_t577 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15805_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15806_gshared (List_1_t577 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m15806(__this, ___enumerable, method) (( void (*) (List_1_t577 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15806_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15807_gshared (List_1_t577 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m15807(__this, ___collection, method) (( void (*) (List_1_t577 *, Object_t*, const MethodInfo*))List_1_AddRange_m15807_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Boolean>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2412 * List_1_AsReadOnly_m15808_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m15808(__this, method) (( ReadOnlyCollection_1_t2412 * (*) (List_1_t577 *, const MethodInfo*))List_1_AsReadOnly_m15808_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::Clear()
extern "C" void List_1_Clear_m15809_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_Clear_m15809(__this, method) (( void (*) (List_1_t577 *, const MethodInfo*))List_1_Clear_m15809_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::Contains(T)
extern "C" bool List_1_Contains_m15810_gshared (List_1_t577 * __this, bool ___item, const MethodInfo* method);
#define List_1_Contains_m15810(__this, ___item, method) (( bool (*) (List_1_t577 *, bool, const MethodInfo*))List_1_Contains_m15810_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m15811_gshared (List_1_t577 * __this, BooleanU5BU5D_t448* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m15811(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t577 *, BooleanU5BU5D_t448*, int32_t, const MethodInfo*))List_1_CopyTo_m15811_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Boolean>::Find(System.Predicate`1<T>)
extern "C" bool List_1_Find_m15812_gshared (List_1_t577 * __this, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_Find_m15812(__this, ___match, method) (( bool (*) (List_1_t577 *, Predicate_1_t2417 *, const MethodInfo*))List_1_Find_m15812_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15813_gshared (Object_t * __this /* static, unused */, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m15813(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2417 *, const MethodInfo*))List_1_CheckMatch_m15813_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15814_gshared (List_1_t577 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m15814(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t577 *, int32_t, int32_t, Predicate_1_t2417 *, const MethodInfo*))List_1_GetIndex_m15814_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Boolean>::GetEnumerator()
extern "C" Enumerator_t2411  List_1_GetEnumerator_m15815_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m15815(__this, method) (( Enumerator_t2411  (*) (List_1_t577 *, const MethodInfo*))List_1_GetEnumerator_m15815_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m15816_gshared (List_1_t577 * __this, bool ___item, const MethodInfo* method);
#define List_1_IndexOf_m15816(__this, ___item, method) (( int32_t (*) (List_1_t577 *, bool, const MethodInfo*))List_1_IndexOf_m15816_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15817_gshared (List_1_t577 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m15817(__this, ___start, ___delta, method) (( void (*) (List_1_t577 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15817_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15818_gshared (List_1_t577 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m15818(__this, ___index, method) (( void (*) (List_1_t577 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15818_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m15819_gshared (List_1_t577 * __this, int32_t ___index, bool ___item, const MethodInfo* method);
#define List_1_Insert_m15819(__this, ___index, ___item, method) (( void (*) (List_1_t577 *, int32_t, bool, const MethodInfo*))List_1_Insert_m15819_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15820_gshared (List_1_t577 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m15820(__this, ___collection, method) (( void (*) (List_1_t577 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15820_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Boolean>::Remove(T)
extern "C" bool List_1_Remove_m15821_gshared (List_1_t577 * __this, bool ___item, const MethodInfo* method);
#define List_1_Remove_m15821(__this, ___item, method) (( bool (*) (List_1_t577 *, bool, const MethodInfo*))List_1_Remove_m15821_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15822_gshared (List_1_t577 * __this, Predicate_1_t2417 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m15822(__this, ___match, method) (( int32_t (*) (List_1_t577 *, Predicate_1_t2417 *, const MethodInfo*))List_1_RemoveAll_m15822_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m15823_gshared (List_1_t577 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m15823(__this, ___index, method) (( void (*) (List_1_t577 *, int32_t, const MethodInfo*))List_1_RemoveAt_m15823_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::Reverse()
extern "C" void List_1_Reverse_m15824_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_Reverse_m15824(__this, method) (( void (*) (List_1_t577 *, const MethodInfo*))List_1_Reverse_m15824_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::Sort()
extern "C" void List_1_Sort_m15825_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_Sort_m15825(__this, method) (( void (*) (List_1_t577 *, const MethodInfo*))List_1_Sort_m15825_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m15826_gshared (List_1_t577 * __this, Comparison_1_t2421 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m15826(__this, ___comparison, method) (( void (*) (List_1_t577 *, Comparison_1_t2421 *, const MethodInfo*))List_1_Sort_m15826_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Boolean>::ToArray()
extern "C" BooleanU5BU5D_t448* List_1_ToArray_m3404_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_ToArray_m3404(__this, method) (( BooleanU5BU5D_t448* (*) (List_1_t577 *, const MethodInfo*))List_1_ToArray_m3404_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::TrimExcess()
extern "C" void List_1_TrimExcess_m15827_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m15827(__this, method) (( void (*) (List_1_t577 *, const MethodInfo*))List_1_TrimExcess_m15827_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15828_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m15828(__this, method) (( int32_t (*) (List_1_t577 *, const MethodInfo*))List_1_get_Capacity_m15828_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15829_gshared (List_1_t577 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m15829(__this, ___value, method) (( void (*) (List_1_t577 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15829_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Boolean>::get_Count()
extern "C" int32_t List_1_get_Count_m15830_gshared (List_1_t577 * __this, const MethodInfo* method);
#define List_1_get_Count_m15830(__this, method) (( int32_t (*) (List_1_t577 *, const MethodInfo*))List_1_get_Count_m15830_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Boolean>::get_Item(System.Int32)
extern "C" bool List_1_get_Item_m15831_gshared (List_1_t577 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m15831(__this, ___index, method) (( bool (*) (List_1_t577 *, int32_t, const MethodInfo*))List_1_get_Item_m15831_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Boolean>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15832_gshared (List_1_t577 * __this, int32_t ___index, bool ___value, const MethodInfo* method);
#define List_1_set_Item_m15832(__this, ___index, ___value, method) (( void (*) (List_1_t577 *, int32_t, bool, const MethodInfo*))List_1_set_Item_m15832_gshared)(__this, ___index, ___value, method)
