﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t398;
// System.Runtime.Remoting.Messaging.MethodDictionary
#include "mscorlib_System_Runtime_Remoting_Messaging_MethodDictionary.h"
// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct  MethodCallDictionary_t1879  : public MethodDictionary_t1874
{
};
struct MethodCallDictionary_t1879_StaticFields{
	// System.String[] System.Runtime.Remoting.Messaging.MethodCallDictionary::InternalKeys
	StringU5BU5D_t398* ___InternalKeys_6;
};
