﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Blend
struct CameraFilterPack_Blend2Camera_Blend_t16;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Blend::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Blend__ctor_m43 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Blend::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Blend_get_material_m44 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::Start()
extern "C" void CameraFilterPack_Blend2Camera_Blend_Start_m45 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Blend_OnRenderImage_m46 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Blend_OnValidate_m47 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::Update()
extern "C" void CameraFilterPack_Blend2Camera_Blend_Update_m48 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Blend_OnEnable_m49 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Blend::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Blend_OnDisable_m50 (CameraFilterPack_Blend2Camera_Blend_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
