﻿#pragma once
#include <stdint.h>
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t307;
// System.Object
struct Object_t;
// InteractiveMama
struct InteractiveMama_t330;
// System.Object
#include "mscorlib_System_Object.h"
// InteractiveMama/<WaitAndEnableMama>c__Iterator7
struct  U3CWaitAndEnableMamaU3Ec__Iterator7_t331  : public Object_t
{
	// System.Single InteractiveMama/<WaitAndEnableMama>c__Iterator7::delay
	float ___delay_0;
	// UnityEngine.NavMeshAgent InteractiveMama/<WaitAndEnableMama>c__Iterator7::<navMeshAgent>__0
	NavMeshAgent_t307 * ___U3CnavMeshAgentU3E__0_1;
	// System.Boolean InteractiveMama/<WaitAndEnableMama>c__Iterator7::val
	bool ___val_2;
	// System.Int32 InteractiveMama/<WaitAndEnableMama>c__Iterator7::$PC
	int32_t ___U24PC_3;
	// System.Object InteractiveMama/<WaitAndEnableMama>c__Iterator7::$current
	Object_t * ___U24current_4;
	// System.Single InteractiveMama/<WaitAndEnableMama>c__Iterator7::<$>delay
	float ___U3CU24U3Edelay_5;
	// System.Boolean InteractiveMama/<WaitAndEnableMama>c__Iterator7::<$>val
	bool ___U3CU24U3Eval_6;
	// InteractiveMama InteractiveMama/<WaitAndEnableMama>c__Iterator7::<>f__this
	InteractiveMama_t330 * ___U3CU3Ef__this_7;
};
