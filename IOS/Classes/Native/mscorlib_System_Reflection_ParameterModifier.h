﻿#pragma once
#include <stdint.h>
// System.Boolean[]
struct BooleanU5BU5D_t448;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Reflection.ParameterModifier
struct  ParameterModifier_t1805 
{
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t448* ____byref_0;
};
// Native definition for marshalling of: System.Reflection.ParameterModifier
struct ParameterModifier_t1805_marshaled
{
	int32_t* ____byref_0;
};
