﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_BigFace
struct CameraFilterPack_Distortion_BigFace_t80;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_BigFace::.ctor()
extern "C" void CameraFilterPack_Distortion_BigFace__ctor_m496 (CameraFilterPack_Distortion_BigFace_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_BigFace::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_BigFace_get_material_m497 (CameraFilterPack_Distortion_BigFace_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::Start()
extern "C" void CameraFilterPack_Distortion_BigFace_Start_m498 (CameraFilterPack_Distortion_BigFace_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_BigFace_OnRenderImage_m499 (CameraFilterPack_Distortion_BigFace_t80 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::OnValidate()
extern "C" void CameraFilterPack_Distortion_BigFace_OnValidate_m500 (CameraFilterPack_Distortion_BigFace_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::Update()
extern "C" void CameraFilterPack_Distortion_BigFace_Update_m501 (CameraFilterPack_Distortion_BigFace_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BigFace::OnDisable()
extern "C" void CameraFilterPack_Distortion_BigFace_OnDisable_m502 (CameraFilterPack_Distortion_BigFace_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
