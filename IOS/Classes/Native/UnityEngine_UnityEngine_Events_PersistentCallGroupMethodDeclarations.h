﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1066;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1068;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1069;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C" void PersistentCallGroup__ctor_m6307 (PersistentCallGroup_t1066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C" void PersistentCallGroup_Initialize_m6308 (PersistentCallGroup_t1066 * __this, InvokableCallList_t1068 * ___invokableList, UnityEventBase_t1069 * ___unityEventBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
