﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// Cardboard
struct Cardboard_t233;
// System.Object
#include "mscorlib_System_Object.h"
// Cardboard/<EndOfFrame>c__Iterator1
struct  U3CEndOfFrameU3Ec__Iterator1_t234  : public Object_t
{
	// System.Int32 Cardboard/<EndOfFrame>c__Iterator1::$PC
	int32_t ___U24PC_0;
	// System.Object Cardboard/<EndOfFrame>c__Iterator1::$current
	Object_t * ___U24current_1;
	// Cardboard Cardboard/<EndOfFrame>c__Iterator1::<>f__this
	Cardboard_t233 * ___U3CU3Ef__this_2;
};
