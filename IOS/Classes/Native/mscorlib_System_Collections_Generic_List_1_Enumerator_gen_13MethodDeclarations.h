﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
struct Enumerator_t2422;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t457;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15980_gshared (Enumerator_t2422 * __this, List_1_t457 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15980(__this, ___l, method) (( void (*) (Enumerator_t2422 *, List_1_t457 *, const MethodInfo*))Enumerator__ctor_m15980_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15981_gshared (Enumerator_t2422 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15981(__this, method) (( Object_t * (*) (Enumerator_t2422 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15981_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C" void Enumerator_Dispose_m15982_gshared (Enumerator_t2422 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15982(__this, method) (( void (*) (Enumerator_t2422 *, const MethodInfo*))Enumerator_Dispose_m15982_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern "C" void Enumerator_VerifyState_m15983_gshared (Enumerator_t2422 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15983(__this, method) (( void (*) (Enumerator_t2422 *, const MethodInfo*))Enumerator_VerifyState_m15983_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15984_gshared (Enumerator_t2422 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15984(__this, method) (( bool (*) (Enumerator_t2422 *, const MethodInfo*))Enumerator_MoveNext_m15984_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C" Vector3_t215  Enumerator_get_Current_m15985_gshared (Enumerator_t2422 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15985(__this, method) (( Vector3_t215  (*) (Enumerator_t2422 *, const MethodInfo*))Enumerator_get_Current_m15985_gshared)(__this, method)
