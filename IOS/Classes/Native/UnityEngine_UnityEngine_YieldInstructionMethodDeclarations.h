﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t836;
struct YieldInstruction_t836_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m5711 (YieldInstruction_t836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void YieldInstruction_t836_marshal(const YieldInstruction_t836& unmarshaled, YieldInstruction_t836_marshaled& marshaled);
void YieldInstruction_t836_marshal_back(const YieldInstruction_t836_marshaled& marshaled, YieldInstruction_t836& unmarshaled);
void YieldInstruction_t836_marshal_cleanup(YieldInstruction_t836_marshaled& marshaled);
