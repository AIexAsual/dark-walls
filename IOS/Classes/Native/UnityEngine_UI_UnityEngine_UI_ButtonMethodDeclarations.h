﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t634;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t632;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t257;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t490;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void UnityEngine.UI.Button::.ctor()
extern "C" void Button__ctor_m3677 (Button_t634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C" ButtonClickedEvent_t632 * Button_get_onClick_m3678 (Button_t634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
extern "C" void Button_set_onClick_m3679 (Button_t634 * __this, ButtonClickedEvent_t632 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
extern "C" void Button_Press_m3680 (Button_t634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void Button_OnPointerClick_m3681 (Button_t634 * __this, PointerEventData_t257 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C" void Button_OnSubmit_m3682 (Button_t634 * __this, BaseEventData_t490 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
extern "C" Object_t * Button_OnFinishSubmit_m3683 (Button_t634 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
