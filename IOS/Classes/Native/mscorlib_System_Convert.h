﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Type[]
struct TypeU5BU5D_t485;
// System.Object
#include "mscorlib_System_Object.h"
// System.Convert
struct  Convert_t524  : public Object_t
{
};
struct Convert_t524_StaticFields{
	// System.Object System.Convert::DBNull
	Object_t * ___DBNull_0;
	// System.Type[] System.Convert::conversionTable
	TypeU5BU5D_t485* ___conversionTable_1;
};
