﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_NightVision_4
struct CameraFilterPack_NightVision_4_t161;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_NightVision_4::.ctor()
extern "C" void CameraFilterPack_NightVision_4__ctor_m1035 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_NightVision_4::get_material()
extern "C" Material_t2 * CameraFilterPack_NightVision_4_get_material_m1036 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::ChangeFilters()
extern "C" void CameraFilterPack_NightVision_4_ChangeFilters_m1037 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::Start()
extern "C" void CameraFilterPack_NightVision_4_Start_m1038 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_NightVision_4_OnRenderImage_m1039 (CameraFilterPack_NightVision_4_t161 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::OnValidate()
extern "C" void CameraFilterPack_NightVision_4_OnValidate_m1040 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::Update()
extern "C" void CameraFilterPack_NightVision_4_Update_m1041 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVision_4::OnDisable()
extern "C" void CameraFilterPack_NightVision_4_OnDisable_m1042 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
