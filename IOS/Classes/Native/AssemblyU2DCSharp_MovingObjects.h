﻿#pragma once
#include <stdint.h>
// UnityEngine.Rigidbody
struct Rigidbody_t325;
// UnityEngine.Animator
struct Animator_t321;
// UnityEngine.Collider
struct Collider_t319;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// MovingObjects
struct  MovingObjects_t375  : public MonoBehaviour_t4
{
	// System.Boolean MovingObjects::IsMoveWheels
	bool ___IsMoveWheels_2;
	// UnityEngine.Rigidbody MovingObjects::rb
	Rigidbody_t325 * ___rb_3;
	// UnityEngine.Animator MovingObjects::animations
	Animator_t321 * ___animations_4;
	// UnityEngine.Collider MovingObjects::col
	Collider_t319 * ___col_5;
};
