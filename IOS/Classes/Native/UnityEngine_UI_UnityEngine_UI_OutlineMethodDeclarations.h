﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Outline
struct Outline_t757;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t687;

// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m4549 (Outline_t757 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void Outline_ModifyVertices_m4550 (Outline_t757 * __this, List_1_t687 * ___verts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
