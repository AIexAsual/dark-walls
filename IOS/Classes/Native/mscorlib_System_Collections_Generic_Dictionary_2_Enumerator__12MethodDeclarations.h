﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>
struct Enumerator_t2587;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t654;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.Graphic,System.Int32>
struct Dictionary_2_t801;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m18654(__this, ___dictionary, method) (( void (*) (Enumerator_t2587 *, Dictionary_2_t801 *, const MethodInfo*))Enumerator__ctor_m15243_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18655(__this, method) (( Object_t * (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15244_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18656(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15245_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18657(__this, method) (( Object_t * (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15246_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18658(__this, method) (( Object_t * (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15247_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m18659(__this, method) (( bool (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_MoveNext_m15248_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_Current()
#define Enumerator_get_Current_m18660(__this, method) (( KeyValuePair_2_t2584  (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_get_Current_m15249_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18661(__this, method) (( Graphic_t654 * (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_get_CurrentKey_m15250_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18662(__this, method) (( int32_t (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_get_CurrentValue_m15251_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m18663(__this, method) (( void (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_VerifyState_m15252_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18664(__this, method) (( void (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_VerifyCurrent_m15253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::Dispose()
#define Enumerator_Dispose_m18665(__this, method) (( void (*) (Enumerator_t2587 *, const MethodInfo*))Enumerator_Dispose_m15254_gshared)(__this, method)
