﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_Threshold
struct CameraFilterPack_Colors_Threshold_t79;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_Threshold::.ctor()
extern "C" void CameraFilterPack_Colors_Threshold__ctor_m489 (CameraFilterPack_Colors_Threshold_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Threshold::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_Threshold_get_material_m490 (CameraFilterPack_Colors_Threshold_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::Start()
extern "C" void CameraFilterPack_Colors_Threshold_Start_m491 (CameraFilterPack_Colors_Threshold_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_Threshold_OnRenderImage_m492 (CameraFilterPack_Colors_Threshold_t79 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::OnValidate()
extern "C" void CameraFilterPack_Colors_Threshold_OnValidate_m493 (CameraFilterPack_Colors_Threshold_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::Update()
extern "C" void CameraFilterPack_Colors_Threshold_Update_m494 (CameraFilterPack_Colors_Threshold_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Threshold::OnDisable()
extern "C" void CameraFilterPack_Colors_Threshold_OnDisable_m495 (CameraFilterPack_Colors_Threshold_t79 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
