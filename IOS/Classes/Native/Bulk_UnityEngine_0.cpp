﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// <Module>
#include "UnityEngine_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "UnityEngine_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// UnityEngine.AssetBundleCreateRequest
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AssetBundleCreateRequest
#include "UnityEngine_UnityEngine_AssetBundleCreateRequestMethodDeclarations.h"

// UnityEngine.AssetBundle
#include "UnityEngine_UnityEngine_AssetBundle.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"


// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m4969 (AssetBundleCreateRequest_t828 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m5569(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t830 * AssetBundleCreateRequest_get_assetBundle_m4970 (AssetBundleCreateRequest_t828 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t830 * (*AssetBundleCreateRequest_get_assetBundle_m4970_ftn) (AssetBundleCreateRequest_t828 *);
	static AssetBundleCreateRequest_get_assetBundle_m4970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m4970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m4971 (AssetBundleCreateRequest_t828 * __this, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_DisableCompatibilityChecks_m4971_ftn) (AssetBundleCreateRequest_t828 *);
	static AssetBundleCreateRequest_DisableCompatibilityChecks_m4971_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_DisableCompatibilityChecks_m4971_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.AssetBundleRequest
#include "UnityEngine_UnityEngine_AssetBundleRequest.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AssetBundleRequest
#include "UnityEngine_UnityEngine_AssetBundleRequestMethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.Type
#include "mscorlib_System_Type.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.AssetBundle
#include "UnityEngine_UnityEngine_AssetBundleMethodDeclarations.h"


// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m4972 (AssetBundleRequest_t831 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m5569(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t473 * AssetBundleRequest_get_asset_m4973 (AssetBundleRequest_t831 * __this, const MethodInfo* method)
{
	{
		AssetBundle_t830 * L_0 = (__this->___m_AssetBundle_1);
		String_t* L_1 = (__this->___m_Path_2);
		Type_t * L_2 = (__this->___m_Type_3);
		NullCheck(L_0);
		Object_t473 * L_3 = AssetBundle_LoadAsset_m4975(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t1077* AssetBundleRequest_get_allAssets_m4974 (AssetBundleRequest_t831 * __this, const MethodInfo* method)
{
	{
		AssetBundle_t830 * L_0 = (__this->___m_AssetBundle_1);
		String_t* L_1 = (__this->___m_Path_2);
		Type_t * L_2 = (__this->___m_Type_3);
		NullCheck(L_0);
		ObjectU5BU5D_t1077* L_3 = AssetBundle_LoadAssetWithSubAssets_Internal_m4977(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.NullReferenceException
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"


// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern TypeInfo* NullReferenceException_t1106_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral867;
extern Il2CppCodeGenString* _stringLiteral868;
extern Il2CppCodeGenString* _stringLiteral869;
extern "C" Object_t473 * AssetBundle_LoadAsset_m4975 (AssetBundle_t830 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t1106_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(603);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		_stringLiteral867 = il2cpp_codegen_string_literal_from_index(867);
		_stringLiteral868 = il2cpp_codegen_string_literal_from_index(868);
		_stringLiteral869 = il2cpp_codegen_string_literal_from_index(869);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullReferenceException_t1106 * L_1 = (NullReferenceException_t1106 *)il2cpp_codegen_object_new (NullReferenceException_t1106_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m6345(L_1, _stringLiteral867, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		String_t* L_2 = ___name;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3086(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		ArgumentException_t556 * L_4 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_4, _stringLiteral868, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0027:
	{
		Type_t * L_5 = ___type;
		if (L_5)
		{
			goto IL_0038;
		}
	}
	{
		NullReferenceException_t1106 * L_6 = (NullReferenceException_t1106 *)il2cpp_codegen_object_new (NullReferenceException_t1106_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m6345(L_6, _stringLiteral869, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_0038:
	{
		String_t* L_7 = ___name;
		Type_t * L_8 = ___type;
		Object_t473 * L_9 = AssetBundle_LoadAsset_Internal_m4976(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern "C" Object_t473 * AssetBundle_LoadAsset_Internal_m4976 (AssetBundle_t830 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	typedef Object_t473 * (*AssetBundle_LoadAsset_Internal_m4976_ftn) (AssetBundle_t830 *, String_t*, Type_t *);
	static AssetBundle_LoadAsset_Internal_m4976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAsset_Internal_m4976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name, ___type);
}
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern "C" ObjectU5BU5D_t1077* AssetBundle_LoadAssetWithSubAssets_Internal_m4977 (AssetBundle_t830 * __this, String_t* ___name, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1077* (*AssetBundle_LoadAssetWithSubAssets_Internal_m4977_ftn) (AssetBundle_t830 *, String_t*, Type_t *);
	static AssetBundle_LoadAssetWithSubAssets_Internal_m4977_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAssetWithSubAssets_Internal_m4977_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name, ___type);
}
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptionsMethodDeclarations.h"



// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Space
#include "UnityEngine_UnityEngine_SpaceMethodDeclarations.h"



// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"

#include "mscorlib_ArrayTypes.h"


// System.Int32 UnityEngine.LayerMask::get_value()
extern "C" int32_t LayerMask_get_value_m2884 (LayerMask_t241 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Mask_0);
		return L_0;
	}
}
// System.Void UnityEngine.LayerMask::set_value(System.Int32)
extern "C" void LayerMask_set_value_m4978 (LayerMask_t241 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_Mask_0 = L_0;
		return;
	}
}
// System.String UnityEngine.LayerMask::LayerToName(System.Int32)
extern "C" String_t* LayerMask_LayerToName_m4979 (Object_t * __this /* static, unused */, int32_t ___layer, const MethodInfo* method)
{
	typedef String_t* (*LayerMask_LayerToName_m4979_ftn) (int32_t);
	static LayerMask_LayerToName_m4979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_LayerToName_m4979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::LayerToName(System.Int32)");
	return _il2cpp_icall_func(___layer);
}
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C" int32_t LayerMask_NameToLayer_m4980 (Object_t * __this /* static, unused */, String_t* ___layerName, const MethodInfo* method)
{
	typedef int32_t (*LayerMask_NameToLayer_m4980_ftn) (String_t*);
	static LayerMask_NameToLayer_m4980_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (LayerMask_NameToLayer_m4980_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.LayerMask::NameToLayer(System.String)");
	return _il2cpp_icall_func(___layerName);
}
// System.Int32 UnityEngine.LayerMask::GetMask(System.String[])
extern "C" int32_t LayerMask_GetMask_m4981 (Object_t * __this /* static, unused */, StringU5BU5D_t398* ___layerNames, const MethodInfo* method)
{
	int32_t V_0 = 0;
	String_t* V_1 = {0};
	StringU5BU5D_t398* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		StringU5BU5D_t398* L_0 = ___layerNames;
		V_2 = L_0;
		V_3 = 0;
		goto IL_002f;
	}

IL_000b:
	{
		StringU5BU5D_t398* L_1 = V_2;
		int32_t L_2 = V_3;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(L_1, L_3));
		String_t* L_4 = V_1;
		int32_t L_5 = LayerMask_NameToLayer_m4980(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		int32_t L_6 = V_4;
		if (!L_6)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = V_4;
		V_0 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)31)))&(int32_t)((int32_t)31)))))));
	}

IL_002b:
	{
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_10 = V_3;
		StringU5BU5D_t398* L_11 = V_2;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)(((Array_t *)L_11)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C" int32_t LayerMask_op_Implicit_m3256 (Object_t * __this /* static, unused */, LayerMask_t241  ___mask, const MethodInfo* method)
{
	{
		int32_t L_0 = ((&___mask)->___m_Mask_0);
		return L_0;
	}
}
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C" LayerMask_t241  LayerMask_op_Implicit_m2853 (Object_t * __this /* static, unused */, int32_t ___intVal, const MethodInfo* method)
{
	LayerMask_t241  V_0 = {0};
	{
		int32_t L_0 = ___intVal;
		(&V_0)->___m_Mask_0 = L_0;
		LayerMask_t241  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatformMethodDeclarations.h"



// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogTypeMethodDeclarations.h"



// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"

// System.Boolean
#include "mscorlib_System_Boolean.h"


// System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern "C" bool SystemInfo_get_supportsRenderTextures_m2780 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsRenderTextures_m2780_ftn) ();
	static SystemInfo_get_supportsRenderTextures_m2780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsRenderTextures_m2780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsRenderTextures()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern "C" bool SystemInfo_get_supportsImageEffects_m2720 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*SystemInfo_get_supportsImageEffects_m2720_ftn) ();
	static SystemInfo_get_supportsImageEffects_m2720_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_supportsImageEffects_m2720_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_supportsImageEffects()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C" String_t* SystemInfo_get_deviceUniqueIdentifier_m4982 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*SystemInfo_get_deviceUniqueIdentifier_m4982_ftn) ();
	static SystemInfo_get_deviceUniqueIdentifier_m4982_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_deviceUniqueIdentifier_m4982_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_deviceUniqueIdentifier()");
	return _il2cpp_icall_func();
}
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"


// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m2750 (WaitForSeconds_t475 * __this, float ___seconds, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m5711(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds;
		__this->___m_Seconds_0 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
void WaitForSeconds_t475_marshal(const WaitForSeconds_t475& unmarshaled, WaitForSeconds_t475_marshaled& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.___m_Seconds_0;
}
void WaitForSeconds_t475_marshal_back(const WaitForSeconds_t475_marshaled& marshaled, WaitForSeconds_t475& unmarshaled)
{
	unmarshaled.___m_Seconds_0 = marshaled.___m_Seconds_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
void WaitForSeconds_t475_marshal_cleanup(WaitForSeconds_t475_marshaled& marshaled)
{
}
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdate.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WaitForFixedUpdate
#include "UnityEngine_UnityEngine_WaitForFixedUpdateMethodDeclarations.h"



// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m4983 (WaitForFixedUpdate_t837 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m5711(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"



// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m2816 (WaitForEndOfFrame_t484 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m5711(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_CoroutineMethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m4984 (Coroutine_t547 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m5711(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m4985 (Coroutine_t547 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m4985_ftn) (Coroutine_t547 *);
	static Coroutine_ReleaseCoroutine_m4985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m4985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m4986 (Coroutine_t547 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m4985(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
void Coroutine_t547_marshal(const Coroutine_t547& unmarshaled, Coroutine_t547_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
void Coroutine_t547_marshal_back(const Coroutine_t547_marshaled& marshaled, Coroutine_t547& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
void Coroutine_t547_marshal_cleanup(Coroutine_t547_marshaled& marshaled)
{
}
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObject.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"


// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" void ScriptableObject__ctor_m4987 (ScriptableObject_t838 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m4988(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m4988 (Object_t * __this /* static, unused */, ScriptableObject_t838 * ___self, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m4988_ftn) (ScriptableObject_t838 *);
	static ScriptableObject_Internal_CreateScriptableObject_m4988_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m4988_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C" ScriptableObject_t838 * ScriptableObject_CreateInstance_m4989 (Object_t * __this /* static, unused */, String_t* ___className, const MethodInfo* method)
{
	typedef ScriptableObject_t838 * (*ScriptableObject_CreateInstance_m4989_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m4989_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m4989_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C" ScriptableObject_t838 * ScriptableObject_CreateInstance_m4990 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		ScriptableObject_t838 * L_1 = ScriptableObject_CreateInstanceFromType_m4991(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C" ScriptableObject_t838 * ScriptableObject_CreateInstanceFromType_m4991 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ScriptableObject_t838 * (*ScriptableObject_CreateInstanceFromType_m4991_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m4991_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m4991_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
void ScriptableObject_t838_marshal(const ScriptableObject_t838& unmarshaled, ScriptableObject_t838_marshaled& marshaled)
{
}
void ScriptableObject_t838_marshal_back(const ScriptableObject_t838_marshaled& marshaled, ScriptableObject_t838& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
void ScriptableObject_t838_marshal_cleanup(ScriptableObject_t838_marshaled& marshaled)
{
}
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCente.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GameCenteMethodDeclarations.h"

// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
#include "mscorlib_System_Collections_Generic_List_1_gen_31.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_2.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
#include "mscorlib_System_Action_1_gen_3.h"
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
#include "mscorlib_System_Action_1_gen_4.h"
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
#include "mscorlib_System_Action_1_gen_5.h"
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderb.h"
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
#include "mscorlib_System_Action_1_gen_6.h"
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
#include "mscorlib_System_Collections_Generic_List_1_gen_31MethodDeclarations.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_2MethodDeclarations.h"
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>
#include "mscorlib_System_Action_1_gen_4MethodDeclarations.h"
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcLeaderbMethodDeclarations.h"
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"
// System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>
#include "mscorlib_System_Action_1_gen_6MethodDeclarations.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"


// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.ctor()
extern "C" void GameCenterPlatform__ctor_m4992 (GameCenterPlatform_t848 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::.cctor()
extern TypeInfo* AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern TypeInfo* UserProfileU5BU5D_t845_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t847_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m6347_MethodInfo_var;
extern "C" void GameCenterPlatform__cctor_m4993 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		UserProfileU5BU5D_t845_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		List_1_t847_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(610);
		List_1__ctor_m6347_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484122);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t844*)SZArrayNew(AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_friends_10 = ((UserProfileU5BU5D_t845*)SZArrayNew(UserProfileU5BU5D_t845_il2cpp_TypeInfo_var, 0));
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_users_11 = ((UserProfileU5BU5D_t845*)SZArrayNew(UserProfileU5BU5D_t845_il2cpp_TypeInfo_var, 0));
		List_1_t847 * L_0 = (List_1_t847 *)il2cpp_codegen_object_new (List_1_t847_il2cpp_TypeInfo_var);
		List_1__ctor_m6347(L_0, /*hidden argument*/List_1__ctor_m6347_MethodInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m4994 (GameCenterPlatform_t848 * __this, Object_t * ___user, Action_1_t839 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t839 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1 = L_0;
		GameCenterPlatform_Internal_LoadFriends_m5002(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::UnityEngine.SocialPlatforms.ISocialPlatform.Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m4995 (GameCenterPlatform_t848 * __this, Object_t * ___user, Action_1_t839 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t839 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0 = L_0;
		GameCenterPlatform_Internal_Authenticate_m4996(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
extern "C" void GameCenterPlatform_Internal_Authenticate_m4996 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_Authenticate_m4996_ftn) ();
	static GameCenterPlatform_Internal_Authenticate_m4996_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticate_m4996_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
extern "C" bool GameCenterPlatform_Internal_Authenticated_m4997 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Authenticated_m4997_ftn) ();
	static GameCenterPlatform_Internal_Authenticated_m4997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Authenticated_m4997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
extern "C" String_t* GameCenterPlatform_Internal_UserName_m4998 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserName_m4998_ftn) ();
	static GameCenterPlatform_Internal_UserName_m4998_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserName_m4998_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
extern "C" String_t* GameCenterPlatform_Internal_UserID_m4999 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GameCenterPlatform_Internal_UserID_m4999_ftn) ();
	static GameCenterPlatform_Internal_UserID_m4999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserID_m4999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
extern "C" bool GameCenterPlatform_Internal_Underage_m5000 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*GameCenterPlatform_Internal_Underage_m5000_ftn) ();
	static GameCenterPlatform_Internal_Underage_m5000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_Underage_m5000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()");
	return _il2cpp_icall_func();
}
// UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
extern "C" Texture2D_t9 * GameCenterPlatform_Internal_UserImage_m5001 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t9 * (*GameCenterPlatform_Internal_UserImage_m5001_ftn) ();
	static GameCenterPlatform_Internal_UserImage_m5001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_UserImage_m5001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
extern "C" void GameCenterPlatform_Internal_LoadFriends_m5002 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadFriends_m5002_ftn) ();
	static GameCenterPlatform_Internal_LoadFriends_m5002_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadFriends_m5002_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m5003 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievementDescriptions_m5003_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievementDescriptions_m5003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievementDescriptions_m5003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m5004 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadAchievements_m5004_ftn) ();
	static GameCenterPlatform_Internal_LoadAchievements_m5004_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadAchievements_m5004_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
extern "C" void GameCenterPlatform_Internal_ReportProgress_m5005 (Object_t * __this /* static, unused */, String_t* ___id, double ___progress, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportProgress_m5005_ftn) (String_t*, double);
	static GameCenterPlatform_Internal_ReportProgress_m5005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportProgress_m5005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)");
	_il2cpp_icall_func(___id, ___progress);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
extern "C" void GameCenterPlatform_Internal_ReportScore_m5006 (Object_t * __this /* static, unused */, int64_t ___score, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ReportScore_m5006_ftn) (int64_t, String_t*);
	static GameCenterPlatform_Internal_ReportScore_m5006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ReportScore_m5006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)");
	_il2cpp_icall_func(___score, ___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
extern "C" void GameCenterPlatform_Internal_LoadScores_m5007 (Object_t * __this /* static, unused */, String_t* ___category, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadScores_m5007_ftn) (String_t*);
	static GameCenterPlatform_Internal_LoadScores_m5007_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadScores_m5007_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)");
	_il2cpp_icall_func(___category);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m5008 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowAchievementsUI_m5008_ftn) ();
	static GameCenterPlatform_Internal_ShowAchievementsUI_m5008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowAchievementsUI_m5008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m5009 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowLeaderboardUI_m5009_ftn) ();
	static GameCenterPlatform_Internal_ShowLeaderboardUI_m5009_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowLeaderboardUI_m5009_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
extern "C" void GameCenterPlatform_Internal_LoadUsers_m5010 (Object_t * __this /* static, unused */, StringU5BU5D_t398* ___userIds, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_LoadUsers_m5010_ftn) (StringU5BU5D_t398*);
	static GameCenterPlatform_Internal_LoadUsers_m5010_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_LoadUsers_m5010_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])");
	_il2cpp_icall_func(___userIds);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m5011 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ResetAllAchievements_m5011_ftn) ();
	static GameCenterPlatform_Internal_ResetAllAchievements_m5011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ResetAllAchievements_m5011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5012 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5012_ftn) (bool);
	static GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ResetAllAchievements(System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ResetAllAchievements_m5013 (Object_t * __this /* static, unused */, Action_1_t839 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t839 * L_0 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12 = L_0;
		GameCenterPlatform_Internal_ResetAllAchievements_m5011(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowDefaultAchievementCompletionBanner(System.Boolean)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m5014 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5012(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI(System.String,UnityEngine.SocialPlatforms.TimeScope)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m5015 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___leaderboardID;
		int32_t L_1 = ___timeScope;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5016(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5016 (Object_t * __this /* static, unused */, String_t* ___leaderboardID, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5016_ftn) (String_t*, int32_t);
	static GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5016_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5016_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)");
	_il2cpp_icall_func(___leaderboardID, ___timeScope);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearAchievementDescriptions(System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m5017 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t844* L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t844* L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_1);
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}

IL_0017:
	{
		int32_t L_3 = ___size;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9 = ((AchievementDescriptionU5BU5D_t844*)SZArrayNew(AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var, L_3));
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescription(UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData,System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetAchievementDescription_m5018 (Object_t * __this /* static, unused */, GcAchievementDescriptionData_t1019  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t844* L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_1 = ___number;
		AchievementDescription_t1034 * L_2 = GcAchievementDescriptionData_ToAchievementDescription_m6167((&___data), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((AchievementDescription_t1034 **)(AchievementDescription_t1034 **)SZArrayLdElema(L_0, L_1)) = (AchievementDescription_t1034 *)L_2;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetAchievementDescriptionImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral870;
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m5019 (Object_t * __this /* static, unused */, Texture2D_t9 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		_stringLiteral870 = il2cpp_codegen_string_literal_from_index(870);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t844* L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_0);
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) <= ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}

IL_0014:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral870, /*hidden argument*/NULL);
		return;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t844* L_3 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		int32_t L_4 = ___number;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Texture2D_t9 * L_6 = ___texture;
		NullCheck((*(AchievementDescription_t1034 **)(AchievementDescription_t1034 **)SZArrayLdElema(L_3, L_5)));
		AchievementDescription_SetImage_m6205((*(AchievementDescription_t1034 **)(AchievementDescription_t1034 **)SZArrayLdElema(L_3, L_5)), L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerAchievementDescriptionCallback()
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6348_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral871;
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m5020 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6348_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484123);
		_stringLiteral871 = il2cpp_codegen_string_literal_from_index(871);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t840 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t844* L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		AchievementDescriptionU5BU5D_t844* L_2 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_2);
		if ((((int32_t)(((Array_t *)L_2)->max_length))))
		{
			goto IL_002a;
		}
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral871, /*hidden argument*/NULL);
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t840 * L_3 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2;
		AchievementDescriptionU5BU5D_t844* L_4 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_adCache_9;
		NullCheck(L_3);
		Action_1_Invoke_m6348(L_3, (IAchievementDescriptionU5BU5D_t1107*)(IAchievementDescriptionU5BU5D_t1107*)L_4, /*hidden argument*/Action_1_Invoke_m6348_MethodInfo_var);
	}

IL_0039:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AuthenticateCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m5021 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t839 * G_B3_0 = {0};
	Action_1_t839 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	Action_1_t839 * G_B4_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m5031(NULL /*static, unused*/, /*hidden argument*/NULL);
		Action_1_t839 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AuthenticateCallback_0;
		int32_t L_2 = ___result;
		G_B2_0 = L_1;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			G_B3_0 = L_1;
			goto IL_0021;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0022:
	{
		NullCheck(G_B4_1);
		Action_1_Invoke_m6349(G_B4_1, G_B4_0, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearFriends(System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearFriends_m5022 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m5049(NULL /*static, unused*/, (&((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriends(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriends_m5023 (Object_t * __this /* static, unused */, GcUserProfileData_t1018  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6166((&___data), (&((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetFriendImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetFriendImage_m5024 (Object_t * __this /* static, unused */, Texture2D_t9 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Texture2D_t9 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m5048(NULL /*static, unused*/, (&((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_friends_10), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerFriendsCallbackWrapper(System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m5025 (Object_t * __this /* static, unused */, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t839 * G_B5_0 = {0};
	Action_1_t839 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Action_1_t839 * G_B6_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		UserProfileU5BU5D_t845* L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		LocalUser_t846 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		UserProfileU5BU5D_t845* L_2 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_friends_10;
		NullCheck(L_1);
		LocalUser_SetFriends_m6178(L_1, (IUserProfileU5BU5D_t1031*)(IUserProfileU5BU5D_t1031*)L_2, /*hidden argument*/NULL);
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_3 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_4 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_FriendsCallback_1;
		int32_t L_5 = ___result;
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			G_B5_0 = L_4;
			goto IL_0035;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0036:
	{
		NullCheck(G_B6_1);
		Action_1_Invoke_m6349(G_B6_1, G_B6_0, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
	}

IL_003b:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::AchievementCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[])
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern TypeInfo* AchievementU5BU5D_t1108_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6350_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral872;
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m5026 (Object_t * __this /* static, unused */, GcAchievementDataU5BU5D_t1079* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		AchievementU5BU5D_t1108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(615);
		Action_1_Invoke_m6350_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484125);
		_stringLiteral872 = il2cpp_codegen_string_literal_from_index(872);
		s_Il2CppMethodIntialized = true;
	}
	AchievementU5BU5D_t1108* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t841 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		GcAchievementDataU5BU5D_t1079* L_1 = ___result;
		NullCheck(L_1);
		if ((((int32_t)(((Array_t *)L_1)->max_length))))
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral872, /*hidden argument*/NULL);
	}

IL_001c:
	{
		GcAchievementDataU5BU5D_t1079* L_2 = ___result;
		NullCheck(L_2);
		V_0 = ((AchievementU5BU5D_t1108*)SZArrayNew(AchievementU5BU5D_t1108_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_2)->max_length)))));
		V_1 = 0;
		goto IL_003f;
	}

IL_002c:
	{
		AchievementU5BU5D_t1108* L_3 = V_0;
		int32_t L_4 = V_1;
		GcAchievementDataU5BU5D_t1079* L_5 = ___result;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Achievement_t1033 * L_7 = GcAchievementData_ToAchievement_m6168(((GcAchievementData_t1020 *)(GcAchievementData_t1020 *)SZArrayLdElema(L_5, L_6)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		ArrayElementTypeCheck (L_3, L_7);
		*((Achievement_t1033 **)(Achievement_t1033 **)SZArrayLdElema(L_3, L_4)) = (Achievement_t1033 *)L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		GcAchievementDataU5BU5D_t1079* L_10 = ___result;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t841 * L_11 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3;
		AchievementU5BU5D_t1108* L_12 = V_0;
		NullCheck(L_11);
		Action_1_Invoke_m6350(L_11, (IAchievementU5BU5D_t1109*)(IAchievementU5BU5D_t1109*)L_12, /*hidden argument*/Action_1_Invoke_m6350_MethodInfo_var);
	}

IL_0053:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ProgressCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m5027 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m6349(L_1, L_2, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m5028 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m6349(L_1, L_2, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ScoreLoaderCallbackWrapper(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern TypeInfo* ScoreU5BU5D_t1110_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6351_MethodInfo_var;
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m5029 (Object_t * __this /* static, unused */, GcScoreDataU5BU5D_t1080* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		ScoreU5BU5D_t1110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(619);
		Action_1_Invoke_m6351_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484126);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t1110* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t842 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		if (!L_0)
		{
			goto IL_0041;
		}
	}
	{
		GcScoreDataU5BU5D_t1080* L_1 = ___result;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t1110*)SZArrayNew(ScoreU5BU5D_t1110_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_1)->max_length)))));
		V_1 = 0;
		goto IL_002d;
	}

IL_001a:
	{
		ScoreU5BU5D_t1110* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1080* L_4 = ___result;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t1035 * L_6 = GcScoreData_ToScore_m6169(((GcScoreData_t1021 *)(GcScoreData_t1021 *)SZArrayLdElema(L_4, L_5)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t1035 **)(Score_t1035 **)SZArrayLdElema(L_2, L_3)) = (Score_t1035 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1080* L_9 = ___result;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t842 * L_10 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6;
		ScoreU5BU5D_t1110* L_11 = V_0;
		NullCheck(L_10);
		Action_1_Invoke_m6351(L_10, (IScoreU5BU5D_t1037*)(IScoreU5BU5D_t1037*)L_11, /*hidden argument*/Action_1_Invoke_m6351_MethodInfo_var);
	}

IL_0041:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILocalUser UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::get_localUser()
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern TypeInfo* LocalUser_t846_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral471;
extern "C" Object_t * GameCenterPlatform_get_localUser_m5030 (GameCenterPlatform_t848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		LocalUser_t846_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(621);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral471 = il2cpp_codegen_string_literal_from_index(471);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		LocalUser_t846 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		LocalUser_t846 * L_1 = (LocalUser_t846 *)il2cpp_codegen_object_new (LocalUser_t846_il2cpp_TypeInfo_var);
		LocalUser__ctor_m6177(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		bool L_2 = GameCenterPlatform_Internal_Authenticated_m4997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		LocalUser_t846 * L_3 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		NullCheck(L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id() */, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m2981(NULL /*static, unused*/, L_4, _stringLiteral471, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		GameCenterPlatform_PopulateLocalUser_m5031(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		LocalUser_t846 * L_6 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		return L_6;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::PopulateLocalUser()
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_PopulateLocalUser_m5031 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		LocalUser_t846 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_1 = GameCenterPlatform_Internal_Authenticated_m4997(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		LocalUser_SetAuthenticated_m6179(L_0, L_1, /*hidden argument*/NULL);
		LocalUser_t846 * L_2 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_3 = GameCenterPlatform_Internal_UserName_m4998(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		UserProfile_SetUserName_m6185(L_2, L_3, /*hidden argument*/NULL);
		LocalUser_t846 * L_4 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		String_t* L_5 = GameCenterPlatform_Internal_UserID_m4999(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		UserProfile_SetUserID_m6186(L_4, L_5, /*hidden argument*/NULL);
		LocalUser_t846 * L_6 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		bool L_7 = GameCenterPlatform_Internal_Underage_m5000(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		LocalUser_SetUnderage_m6180(L_6, L_7, /*hidden argument*/NULL);
		LocalUser_t846 * L_8 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_LocalUser_13;
		Texture2D_t9 * L_9 = GameCenterPlatform_Internal_UserImage_m5001(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		UserProfile_SetImage_m6187(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievementDescriptions(System.Action`1<UnityEngine.SocialPlatforms.IAchievementDescription[]>)
extern TypeInfo* AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6348_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m5032 (GameCenterPlatform_t848 * __this, Action_1_t840 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(605);
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6348_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484123);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t840 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m6348(L_1, (IAchievementDescriptionU5BU5D_t1107*)(IAchievementDescriptionU5BU5D_t1107*)((AchievementDescriptionU5BU5D_t844*)SZArrayNew(AchievementDescriptionU5BU5D_t844_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m6348_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t840 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AchievementDescriptionLoaderCallback_2 = L_2;
		GameCenterPlatform_Internal_LoadAchievementDescriptions_m5003(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportProgress(System.String,System.Double,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportProgress_m5033 (GameCenterPlatform_t848 * __this, String_t* ___id, double ___progress, Action_1_t839 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t839 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m6349(L_1, 0, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t839 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ProgressCallback_4 = L_2;
		String_t* L_3 = ___id;
		double L_4 = ___progress;
		GameCenterPlatform_Internal_ReportProgress_m5005(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadAchievements(System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>)
extern TypeInfo* AchievementU5BU5D_t1108_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6350_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadAchievements_m5034 (GameCenterPlatform_t848 * __this, Action_1_t841 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AchievementU5BU5D_t1108_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(615);
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6350_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484125);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t841 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m6350(L_1, (IAchievementU5BU5D_t1109*)(IAchievementU5BU5D_t1109*)((AchievementU5BU5D_t1108*)SZArrayNew(AchievementU5BU5D_t1108_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m6350_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t841 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_AchievementLoaderCallback_3 = L_2;
		GameCenterPlatform_Internal_LoadAchievements_m5004(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ReportScore(System.Int64,System.String,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_ReportScore_m5035 (GameCenterPlatform_t848 * __this, int64_t ___score, String_t* ___board, Action_1_t839 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t839 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m6349(L_1, 0, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t839 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ScoreCallback_5 = L_2;
		int64_t L_3 = ___score;
		String_t* L_4 = ___board;
		GameCenterPlatform_Internal_ReportScore_m5006(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(System.String,System.Action`1<UnityEngine.SocialPlatforms.IScore[]>)
extern TypeInfo* ScoreU5BU5D_t1110_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6351_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m5036 (GameCenterPlatform_t848 * __this, String_t* ___category, Action_1_t842 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t1110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(619);
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6351_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484126);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t842 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m6351(L_1, (IScoreU5BU5D_t1037*)(IScoreU5BU5D_t1037*)((ScoreU5BU5D_t1110*)SZArrayNew(ScoreU5BU5D_t1110_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m6351_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t842 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ScoreLoaderCallback_6 = L_2;
		String_t* L_3 = ___category;
		GameCenterPlatform_Internal_LoadScores_m5007(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadScores(UnityEngine.SocialPlatforms.ILeaderboard,System.Action`1<System.Boolean>)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t849_il2cpp_TypeInfo_var;
extern TypeInfo* GcLeaderboard_t850_il2cpp_TypeInfo_var;
extern TypeInfo* ILeaderboard_t1081_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadScores_m5037 (GameCenterPlatform_t848 * __this, Object_t * ___board, Action_1_t839 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Leaderboard_t849_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		GcLeaderboard_t850_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(604);
		ILeaderboard_t1081_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(623);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t849 * V_0 = {0};
	GcLeaderboard_t850 * V_1 = {0};
	Range_t1038  V_2 = {0};
	Range_t1038  V_3 = {0};
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t839 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m6349(L_1, 0, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
		return;
	}

IL_0013:
	{
		Action_1_t839 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7 = L_2;
		Object_t * L_3 = ___board;
		V_0 = ((Leaderboard_t849 *)Castclass(L_3, Leaderboard_t849_il2cpp_TypeInfo_var));
		Leaderboard_t849 * L_4 = V_0;
		GcLeaderboard_t850 * L_5 = (GcLeaderboard_t850 *)il2cpp_codegen_object_new (GcLeaderboard_t850_il2cpp_TypeInfo_var);
		GcLeaderboard__ctor_m5053(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		List_1_t847 * L_6 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		GcLeaderboard_t850 * L_7 = V_1;
		NullCheck(L_6);
		VirtActionInvoker1< GcLeaderboard_t850 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Add(!0) */, L_6, L_7);
		Leaderboard_t849 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t398* L_9 = Leaderboard_GetUserFilter_m6226(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		if ((((int32_t)(((int32_t)(((Array_t *)L_9)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		GcLeaderboard_t850 * L_10 = V_1;
		Object_t * L_11 = ___board;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t1081_il2cpp_TypeInfo_var, L_11);
		Object_t * L_13 = ___board;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t1081_il2cpp_TypeInfo_var, L_13);
		Leaderboard_t849 * L_15 = V_0;
		NullCheck(L_15);
		StringU5BU5D_t398* L_16 = Leaderboard_GetUserFilter_m6226(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		GcLeaderboard_Internal_LoadScoresWithUsers_m5061(L_10, L_12, L_14, L_16, /*hidden argument*/NULL);
		goto IL_0091;
	}

IL_005d:
	{
		GcLeaderboard_t850 * L_17 = V_1;
		Object_t * L_18 = ___board;
		NullCheck(L_18);
		String_t* L_19 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id() */, ILeaderboard_t1081_il2cpp_TypeInfo_var, L_18);
		Object_t * L_20 = ___board;
		NullCheck(L_20);
		Range_t1038  L_21 = (Range_t1038 )InterfaceFuncInvoker0< Range_t1038  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t1081_il2cpp_TypeInfo_var, L_20);
		V_2 = L_21;
		int32_t L_22 = ((&V_2)->___from_0);
		Object_t * L_23 = ___board;
		NullCheck(L_23);
		Range_t1038  L_24 = (Range_t1038 )InterfaceFuncInvoker0< Range_t1038  >::Invoke(2 /* UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range() */, ILeaderboard_t1081_il2cpp_TypeInfo_var, L_23);
		V_3 = L_24;
		int32_t L_25 = ((&V_3)->___count_1);
		Object_t * L_26 = ___board;
		NullCheck(L_26);
		int32_t L_27 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope() */, ILeaderboard_t1081_il2cpp_TypeInfo_var, L_26);
		Object_t * L_28 = ___board;
		NullCheck(L_28);
		int32_t L_29 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope() */, ILeaderboard_t1081_il2cpp_TypeInfo_var, L_28);
		NullCheck(L_17);
		GcLeaderboard_Internal_LoadScores_m5060(L_17, L_19, L_22, L_25, L_27, L_29, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LeaderboardCallbackWrapper(System.Boolean)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m5038 (Object_t * __this /* static, unused */, bool ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_LeaderboardCallback_7;
		bool L_2 = ___success;
		NullCheck(L_1);
		Action_1_Invoke_m6349(L_1, L_2, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::GetLoading(UnityEngine.SocialPlatforms.ILeaderboard)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern TypeInfo* Leaderboard_t849_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1111_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m6352_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6353_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6354_MethodInfo_var;
extern "C" bool GameCenterPlatform_GetLoading_m5039 (GameCenterPlatform_t848 * __this, Object_t * ___board, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Leaderboard_t849_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		Enumerator_t1111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(624);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		List_1_GetEnumerator_m6352_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484127);
		Enumerator_get_Current_m6353_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484128);
		Enumerator_MoveNext_m6354_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484129);
		s_Il2CppMethodIntialized = true;
	}
	GcLeaderboard_t850 * V_0 = {0};
	Enumerator_t1111  V_1 = {0};
	bool V_2 = false;
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		List_1_t847 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___m_GcBoards_14;
		NullCheck(L_1);
		Enumerator_t1111  L_2 = List_1_GetEnumerator_m6352(L_1, /*hidden argument*/List_1_GetEnumerator_m6352_MethodInfo_var);
		V_1 = L_2;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_001d:
		{
			GcLeaderboard_t850 * L_3 = Enumerator_get_Current_m6353((&V_1), /*hidden argument*/Enumerator_get_Current_m6353_MethodInfo_var);
			V_0 = L_3;
			GcLeaderboard_t850 * L_4 = V_0;
			Object_t * L_5 = ___board;
			NullCheck(L_4);
			bool L_6 = GcLeaderboard_Contains_m5055(L_4, ((Leaderboard_t849 *)Castclass(L_5, Leaderboard_t849_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0042;
			}
		}

IL_0036:
		{
			GcLeaderboard_t850 * L_7 = V_0;
			NullCheck(L_7);
			bool L_8 = GcLeaderboard_Loading_m5062(L_7, /*hidden argument*/NULL);
			V_2 = L_8;
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}

IL_0042:
		{
			bool L_9 = Enumerator_MoveNext_m6354((&V_1), /*hidden argument*/Enumerator_MoveNext_m6354_MethodInfo_var);
			if (L_9)
			{
				goto IL_001d;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_t1111  L_10 = V_1;
		Enumerator_t1111  L_11 = L_10;
		Object_t * L_12 = Box(Enumerator_t1111_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_12);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_12);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_005f:
	{
		return 0;
	}

IL_0061:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::VerifyAuthentication()
extern TypeInfo* ILocalUser_t1078_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral873;
extern "C" bool GameCenterPlatform_VerifyAuthentication_m5040 (GameCenterPlatform_t848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ILocalUser_t1078_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(625);
		_stringLiteral873 = il2cpp_codegen_string_literal_from_index(873);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = GameCenterPlatform_get_localUser_m5030(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated() */, ILocalUser_t1078_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral873, /*hidden argument*/NULL);
		return 0;
	}

IL_001c:
	{
		return 1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowAchievementsUI()
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowAchievementsUI_m5041 (GameCenterPlatform_t848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowAchievementsUI_m5008(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ShowLeaderboardUI()
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m5042 (GameCenterPlatform_t848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		GameCenterPlatform_Internal_ShowLeaderboardUI_m5009(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::ClearUsers(System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_ClearUsers_m5043 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		int32_t L_0 = ___size;
		GameCenterPlatform_SafeClearArray_m5049(NULL /*static, unused*/, (&((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUser(UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData,System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUser_m5044 (Object_t * __this /* static, unused */, GcUserProfileData_t1018  ___data, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		int32_t L_0 = ___number;
		GcUserProfileData_AddToArray_m6166((&___data), (&((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SetUserImage(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SetUserImage_m5045 (Object_t * __this /* static, unused */, Texture2D_t9 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Texture2D_t9 * L_0 = ___texture;
		int32_t L_1 = ___number;
		GameCenterPlatform_SafeSetUserImage_m5048(NULL /*static, unused*/, (&((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_users_11), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerUsersCallbackWrapper()
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6355_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m5046 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6355_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484130);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t843 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t843 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8;
		UserProfileU5BU5D_t845* L_2 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_users_11;
		NullCheck(L_1);
		Action_1_Invoke_m6355(L_1, (IUserProfileU5BU5D_t1031*)(IUserProfileU5BU5D_t1031*)L_2, /*hidden argument*/Action_1_Invoke_m6355_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::LoadUsers(System.String[],System.Action`1<UnityEngine.SocialPlatforms.IUserProfile[]>)
extern TypeInfo* UserProfileU5BU5D_t845_il2cpp_TypeInfo_var;
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6355_MethodInfo_var;
extern "C" void GameCenterPlatform_LoadUsers_m5047 (GameCenterPlatform_t848 * __this, StringU5BU5D_t398* ___userIds, Action_1_t843 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t845_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6355_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484130);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = GameCenterPlatform_VerifyAuthentication_m5040(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		Action_1_t843 * L_1 = ___callback;
		NullCheck(L_1);
		Action_1_Invoke_m6355(L_1, (IUserProfileU5BU5D_t1031*)(IUserProfileU5BU5D_t1031*)((UserProfileU5BU5D_t845*)SZArrayNew(UserProfileU5BU5D_t845_il2cpp_TypeInfo_var, 0)), /*hidden argument*/Action_1_Invoke_m6355_MethodInfo_var);
		return;
	}

IL_0018:
	{
		Action_1_t843 * L_2 = ___callback;
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_UsersCallback_8 = L_2;
		StringU5BU5D_t398* L_3 = ___userIds;
		GameCenterPlatform_Internal_LoadUsers_m5010(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeSetUserImage(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,UnityEngine.Texture2D,System.Int32)
extern TypeInfo* Texture2D_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral874;
extern Il2CppCodeGenString* _stringLiteral875;
extern "C" void GameCenterPlatform_SafeSetUserImage_m5048 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t845** ___array, Texture2D_t9 * ___texture, int32_t ___number, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral874 = il2cpp_codegen_string_literal_from_index(874);
		_stringLiteral875 = il2cpp_codegen_string_literal_from_index(875);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t845** L_0 = ___array;
		NullCheck((*((UserProfileU5BU5D_t845**)L_0)));
		int32_t L_1 = ___number;
		if ((((int32_t)(((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t845**)L_0)))->max_length)))) <= ((int32_t)L_1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = ___number;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0026;
		}
	}

IL_0011:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral874, /*hidden argument*/NULL);
		Texture2D_t9 * L_3 = (Texture2D_t9 *)il2cpp_codegen_object_new (Texture2D_t9_il2cpp_TypeInfo_var);
		Texture2D__ctor_m5105(L_3, ((int32_t)76), ((int32_t)76), /*hidden argument*/NULL);
		___texture = L_3;
	}

IL_0026:
	{
		UserProfileU5BU5D_t845** L_4 = ___array;
		NullCheck((*((UserProfileU5BU5D_t845**)L_4)));
		int32_t L_5 = ___number;
		if ((((int32_t)(((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t845**)L_4)))->max_length)))) <= ((int32_t)L_5)))
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_6 = ___number;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		UserProfileU5BU5D_t845** L_7 = ___array;
		int32_t L_8 = ___number;
		NullCheck((*((UserProfileU5BU5D_t845**)L_7)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((UserProfileU5BU5D_t845**)L_7)), L_8);
		int32_t L_9 = L_8;
		Texture2D_t9 * L_10 = ___texture;
		NullCheck((*(UserProfile_t1032 **)(UserProfile_t1032 **)SZArrayLdElema((*((UserProfileU5BU5D_t845**)L_7)), L_9)));
		UserProfile_SetImage_m6187((*(UserProfile_t1032 **)(UserProfile_t1032 **)SZArrayLdElema((*((UserProfileU5BU5D_t845**)L_7)), L_9)), L_10, /*hidden argument*/NULL);
		goto IL_0050;
	}

IL_0046:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral875, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::SafeClearArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern TypeInfo* UserProfileU5BU5D_t845_il2cpp_TypeInfo_var;
extern "C" void GameCenterPlatform_SafeClearArray_m5049 (Object_t * __this /* static, unused */, UserProfileU5BU5D_t845** ___array, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UserProfileU5BU5D_t845_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(608);
		s_Il2CppMethodIntialized = true;
	}
	{
		UserProfileU5BU5D_t845** L_0 = ___array;
		if (!(*((UserProfileU5BU5D_t845**)L_0)))
		{
			goto IL_0011;
		}
	}
	{
		UserProfileU5BU5D_t845** L_1 = ___array;
		NullCheck((*((UserProfileU5BU5D_t845**)L_1)));
		int32_t L_2 = ___size;
		if ((((int32_t)(((int32_t)(((Array_t *)(*((UserProfileU5BU5D_t845**)L_1)))->max_length)))) == ((int32_t)L_2)))
		{
			goto IL_0019;
		}
	}

IL_0011:
	{
		UserProfileU5BU5D_t845** L_3 = ___array;
		int32_t L_4 = ___size;
		*((Object_t **)(L_3)) = (Object_t *)((UserProfileU5BU5D_t845*)SZArrayNew(UserProfileU5BU5D_t845_il2cpp_TypeInfo_var, L_4));
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.SocialPlatforms.ILeaderboard UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateLeaderboard()
extern TypeInfo* Leaderboard_t849_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateLeaderboard_m5050 (GameCenterPlatform_t848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Leaderboard_t849_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(622);
		s_Il2CppMethodIntialized = true;
	}
	Leaderboard_t849 * V_0 = {0};
	{
		Leaderboard_t849 * L_0 = (Leaderboard_t849 *)il2cpp_codegen_object_new (Leaderboard_t849_il2cpp_TypeInfo_var);
		Leaderboard__ctor_m6220(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Leaderboard_t849 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.SocialPlatforms.IAchievement UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::CreateAchievement()
extern TypeInfo* Achievement_t1033_il2cpp_TypeInfo_var;
extern "C" Object_t * GameCenterPlatform_CreateAchievement_m5051 (GameCenterPlatform_t848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Achievement_t1033_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(616);
		s_Il2CppMethodIntialized = true;
	}
	Achievement_t1033 * V_0 = {0};
	{
		Achievement_t1033 * L_0 = (Achievement_t1033 *)il2cpp_codegen_object_new (Achievement_t1033_il2cpp_TypeInfo_var);
		Achievement__ctor_m6194(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Achievement_t1033 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::TriggerResetAchievementCallback(System.Boolean)
extern TypeInfo* GameCenterPlatform_t848_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6349_MethodInfo_var;
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m5052 (Object_t * __this /* static, unused */, bool ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameCenterPlatform_t848_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(607);
		Action_1_Invoke_m6349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484124);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_0 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameCenterPlatform_t848_il2cpp_TypeInfo_var);
		Action_1_t839 * L_1 = ((GameCenterPlatform_t848_StaticFields*)GameCenterPlatform_t848_il2cpp_TypeInfo_var->static_fields)->___s_ResetAchievements_12;
		bool L_2 = ___result;
		NullCheck(L_1);
		Action_1_Invoke_m6349(L_1, L_2, /*hidden argument*/Action_1_Invoke_m6349_MethodInfo_var);
	}

IL_0015:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.UInt32
#include "mscorlib_System_UInt32.h"


// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::.ctor(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" void GcLeaderboard__ctor_m5053 (GcLeaderboard_t850 * __this, Leaderboard_t849 * ___board, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Leaderboard_t849 * L_0 = ___board;
		__this->___m_GenericLeaderboard_1 = L_0;
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Finalize()
extern "C" void GcLeaderboard_Finalize_m5054 (GcLeaderboard_t850 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GcLeaderboard_Dispose_m5063(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Contains(UnityEngine.SocialPlatforms.Impl.Leaderboard)
extern "C" bool GcLeaderboard_Contains_m5055 (GcLeaderboard_t850 * __this, Leaderboard_t849 * ___board, const MethodInfo* method)
{
	{
		Leaderboard_t849 * L_0 = (__this->___m_GenericLeaderboard_1);
		Leaderboard_t849 * L_1 = ___board;
		return ((((Object_t*)(Leaderboard_t849 *)L_0) == ((Object_t*)(Leaderboard_t849 *)L_1))? 1 : 0);
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetScores(UnityEngine.SocialPlatforms.GameCenter.GcScoreData[])
extern TypeInfo* ScoreU5BU5D_t1110_il2cpp_TypeInfo_var;
extern "C" void GcLeaderboard_SetScores_m5056 (GcLeaderboard_t850 * __this, GcScoreDataU5BU5D_t1080* ___scoreDatas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ScoreU5BU5D_t1110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(619);
		s_Il2CppMethodIntialized = true;
	}
	ScoreU5BU5D_t1110* V_0 = {0};
	int32_t V_1 = 0;
	{
		Leaderboard_t849 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0043;
		}
	}
	{
		GcScoreDataU5BU5D_t1080* L_1 = ___scoreDatas;
		NullCheck(L_1);
		V_0 = ((ScoreU5BU5D_t1110*)SZArrayNew(ScoreU5BU5D_t1110_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_1)->max_length)))));
		V_1 = 0;
		goto IL_002e;
	}

IL_001b:
	{
		ScoreU5BU5D_t1110* L_2 = V_0;
		int32_t L_3 = V_1;
		GcScoreDataU5BU5D_t1080* L_4 = ___scoreDatas;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Score_t1035 * L_6 = GcScoreData_ToScore_m6169(((GcScoreData_t1021 *)(GcScoreData_t1021 *)SZArrayLdElema(L_4, L_5)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		ArrayElementTypeCheck (L_2, L_6);
		*((Score_t1035 **)(Score_t1035 **)SZArrayLdElema(L_2, L_3)) = (Score_t1035 *)L_6;
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		GcScoreDataU5BU5D_t1080* L_9 = ___scoreDatas;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
		{
			goto IL_001b;
		}
	}
	{
		Leaderboard_t849 * L_10 = (__this->___m_GenericLeaderboard_1);
		ScoreU5BU5D_t1110* L_11 = V_0;
		NullCheck(L_10);
		Leaderboard_SetScores_m6224(L_10, (IScoreU5BU5D_t1037*)(IScoreU5BU5D_t1037*)L_11, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetLocalScore(UnityEngine.SocialPlatforms.GameCenter.GcScoreData)
extern "C" void GcLeaderboard_SetLocalScore_m5057 (GcLeaderboard_t850 * __this, GcScoreData_t1021  ___scoreData, const MethodInfo* method)
{
	{
		Leaderboard_t849 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Leaderboard_t849 * L_1 = (__this->___m_GenericLeaderboard_1);
		Score_t1035 * L_2 = GcScoreData_ToScore_m6169((&___scoreData), /*hidden argument*/NULL);
		NullCheck(L_1);
		Leaderboard_SetLocalUserScore_m6222(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetMaxRange(System.UInt32)
extern "C" void GcLeaderboard_SetMaxRange_m5058 (GcLeaderboard_t850 * __this, uint32_t ___maxRange, const MethodInfo* method)
{
	{
		Leaderboard_t849 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t849 * L_1 = (__this->___m_GenericLeaderboard_1);
		uint32_t L_2 = ___maxRange;
		NullCheck(L_1);
		Leaderboard_SetMaxRange_m6223(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::SetTitle(System.String)
extern "C" void GcLeaderboard_SetTitle_m5059 (GcLeaderboard_t850 * __this, String_t* ___title, const MethodInfo* method)
{
	{
		Leaderboard_t849 * L_0 = (__this->___m_GenericLeaderboard_1);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Leaderboard_t849 * L_1 = (__this->___m_GenericLeaderboard_1);
		String_t* L_2 = ___title;
		NullCheck(L_1);
		Leaderboard_SetTitle_m6225(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void GcLeaderboard_Internal_LoadScores_m5060 (GcLeaderboard_t850 * __this, String_t* ___category, int32_t ___from, int32_t ___count, int32_t ___playerScope, int32_t ___timeScope, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScores_m5060_ftn) (GcLeaderboard_t850 *, String_t*, int32_t, int32_t, int32_t, int32_t);
	static GcLeaderboard_Internal_LoadScores_m5060_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScores_m5060_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___category, ___from, ___count, ___playerScope, ___timeScope);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m5061 (GcLeaderboard_t850 * __this, String_t* ___category, int32_t ___timeScope, StringU5BU5D_t398* ___userIDs, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Internal_LoadScoresWithUsers_m5061_ftn) (GcLeaderboard_t850 *, String_t*, int32_t, StringU5BU5D_t398*);
	static GcLeaderboard_Internal_LoadScoresWithUsers_m5061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Internal_LoadScoresWithUsers_m5061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])");
	_il2cpp_icall_func(__this, ___category, ___timeScope, ___userIDs);
}
// System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
extern "C" bool GcLeaderboard_Loading_m5062 (GcLeaderboard_t850 * __this, const MethodInfo* method)
{
	typedef bool (*GcLeaderboard_Loading_m5062_ftn) (GcLeaderboard_t850 *);
	static GcLeaderboard_Loading_m5062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Loading_m5062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
extern "C" void GcLeaderboard_Dispose_m5063 (GcLeaderboard_t850 * __this, const MethodInfo* method)
{
	typedef void (*GcLeaderboard_Dispose_m5063_ftn) (GcLeaderboard_t850 *);
	static GcLeaderboard_Dispose_m5063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GcLeaderboard_Dispose_m5063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.RenderSettings
#include "UnityEngine_UnityEngine_RenderSettings.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RenderSettings
#include "UnityEngine_UnityEngine_RenderSettingsMethodDeclarations.h"



// System.Void UnityEngine.RenderSettings::set_fogEndDistance(System.Single)
extern "C" void RenderSettings_set_fogEndDistance_m3157 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	typedef void (*RenderSettings_set_fogEndDistance_m3157_ftn) (float);
	static RenderSettings_set_fogEndDistance_m3157_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderSettings_set_fogEndDistance_m3157_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderSettings::set_fogEndDistance(System.Single)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.QualitySettings
#include "UnityEngine_UnityEngine_QualitySettings.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.QualitySettings
#include "UnityEngine_UnityEngine_QualitySettingsMethodDeclarations.h"

// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"


// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C" int32_t QualitySettings_get_antiAliasing_m3058 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_antiAliasing_m3058_ftn) ();
	static QualitySettings_get_antiAliasing_m3058_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_antiAliasing_m3058_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_antiAliasing()");
	return _il2cpp_icall_func();
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C" int32_t QualitySettings_get_activeColorSpace_m5064 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m5064_ftn) ();
	static QualitySettings_get_activeColorSpace_m5064_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m5064_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"



// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"

// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"


// System.Void UnityEngine.Mesh::.ctor()
extern "C" void Mesh__ctor_m2913 (Mesh_t245 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		Mesh_Internal_Create_m5065(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C" void Mesh_Internal_Create_m5065 (Object_t * __this /* static, unused */, Mesh_t245 * ___mono, const MethodInfo* method)
{
	typedef void (*Mesh_Internal_Create_m5065_ftn) (Mesh_t245 *);
	static Mesh_Internal_Create_m5065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Internal_Create_m5065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)");
	_il2cpp_icall_func(___mono);
}
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C" void Mesh_set_vertices_m2914 (Mesh_t245 * __this, Vector3U5BU5D_t317* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_vertices_m2914_ftn) (Mesh_t245 *, Vector3U5BU5D_t317*);
	static Mesh_set_vertices_m2914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_vertices_m2914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C" void Mesh_set_uv_m2915 (Mesh_t245 * __this, Vector2U5BU5D_t437* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_uv_m2915_ftn) (Mesh_t245 *, Vector2U5BU5D_t437*);
	static Mesh_set_uv_m2915_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_uv_m2915_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
extern "C" void Mesh_set_colors_m2916 (Mesh_t245 * __this, ColorU5BU5D_t449* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_colors_m2916_ftn) (Mesh_t245 *, ColorU5BU5D_t449*);
	static Mesh_set_colors_m2916_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_colors_m2916_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_colors(UnityEngine.Color[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Mesh::Optimize()
extern "C" void Mesh_Optimize_m2918 (Mesh_t245 * __this, const MethodInfo* method)
{
	typedef void (*Mesh_Optimize_m2918_ftn) (Mesh_t245 *);
	static Mesh_Optimize_m2918_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_Optimize_m2918_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::Optimize()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C" void Mesh_set_triangles_m2917 (Mesh_t245 * __this, Int32U5BU5D_t269* ___value, const MethodInfo* method)
{
	typedef void (*Mesh_set_triangles_m2917_ftn) (Mesh_t245 *, Int32U5BU5D_t269*);
	static Mesh_set_triangles_m2917_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_set_triangles_m2917_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::set_triangles(System.Int32[])");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Mesh::UploadMeshData(System.Boolean)
extern "C" void Mesh_UploadMeshData_m2919 (Mesh_t245 * __this, bool ___markNoLogerReadable, const MethodInfo* method)
{
	typedef void (*Mesh_UploadMeshData_m2919_ftn) (Mesh_t245 *, bool);
	static Mesh_UploadMeshData_m2919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Mesh_UploadMeshData_m2919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Mesh::UploadMeshData(System.Boolean)");
	_il2cpp_icall_func(__this, ___markNoLogerReadable);
}
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeightMethodDeclarations.h"

// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"


// System.Single UnityEngine.BoneWeight::get_weight0()
extern "C" float BoneWeight_get_weight0_m5066 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight0_0);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight0(System.Single)
extern "C" void BoneWeight_set_weight0_m5067 (BoneWeight_t854 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight0_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight1()
extern "C" float BoneWeight_get_weight1_m5068 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight1_1);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight1(System.Single)
extern "C" void BoneWeight_set_weight1_m5069 (BoneWeight_t854 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight1_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight2()
extern "C" float BoneWeight_get_weight2_m5070 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight2_2);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight2(System.Single)
extern "C" void BoneWeight_set_weight2_m5071 (BoneWeight_t854 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight2_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.BoneWeight::get_weight3()
extern "C" float BoneWeight_get_weight3_m5072 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Weight3_3);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_weight3(System.Single)
extern "C" void BoneWeight_set_weight3_m5073 (BoneWeight_t854 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Weight3_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex0()
extern "C" int32_t BoneWeight_get_boneIndex0_m5074 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex0_4);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex0(System.Int32)
extern "C" void BoneWeight_set_boneIndex0_m5075 (BoneWeight_t854 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex0_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex1()
extern "C" int32_t BoneWeight_get_boneIndex1_m5076 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex1_5);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex1(System.Int32)
extern "C" void BoneWeight_set_boneIndex1_m5077 (BoneWeight_t854 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex1_5 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex2()
extern "C" int32_t BoneWeight_get_boneIndex2_m5078 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex2_6);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex2(System.Int32)
extern "C" void BoneWeight_set_boneIndex2_m5079 (BoneWeight_t854 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex2_6 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::get_boneIndex3()
extern "C" int32_t BoneWeight_get_boneIndex3_m5080 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_BoneIndex3_7);
		return L_0;
	}
}
// System.Void UnityEngine.BoneWeight::set_boneIndex3(System.Int32)
extern "C" void BoneWeight_set_boneIndex3_m5081 (BoneWeight_t854 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_BoneIndex3_7 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.BoneWeight::GetHashCode()
extern "C" int32_t BoneWeight_GetHashCode_m5082 (BoneWeight_t854 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m5074(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Int32_GetHashCode_m6356((&V_0), /*hidden argument*/NULL);
		int32_t L_2 = BoneWeight_get_boneIndex1_m5076(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Int32_GetHashCode_m6356((&V_1), /*hidden argument*/NULL);
		int32_t L_4 = BoneWeight_get_boneIndex2_m5078(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Int32_GetHashCode_m6356((&V_2), /*hidden argument*/NULL);
		int32_t L_6 = BoneWeight_get_boneIndex3_m5080(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Int32_GetHashCode_m6356((&V_3), /*hidden argument*/NULL);
		float L_8 = BoneWeight_get_weight0_m5066(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		int32_t L_9 = Single_GetHashCode_m6357((&V_4), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight1_m5068(__this, /*hidden argument*/NULL);
		V_5 = L_10;
		int32_t L_11 = Single_GetHashCode_m6357((&V_5), /*hidden argument*/NULL);
		float L_12 = BoneWeight_get_weight2_m5070(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		int32_t L_13 = Single_GetHashCode_m6357((&V_6), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight3_m5072(__this, /*hidden argument*/NULL);
		V_7 = L_14;
		int32_t L_15 = Single_GetHashCode_m6357((&V_7), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))))^(int32_t)((int32_t)((int32_t)L_9<<(int32_t)5))))^(int32_t)((int32_t)((int32_t)L_11<<(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_13>>(int32_t)4))))^(int32_t)((int32_t)((int32_t)L_15>>(int32_t)3))));
	}
}
// System.Boolean UnityEngine.BoneWeight::Equals(System.Object)
extern TypeInfo* BoneWeight_t854_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t5_il2cpp_TypeInfo_var;
extern "C" bool BoneWeight_Equals_m5083 (BoneWeight_t854 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BoneWeight_t854_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(628);
		Vector4_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	BoneWeight_t854  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector4_t5  V_5 = {0};
	int32_t G_B8_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, BoneWeight_t854_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(BoneWeight_t854 *)((BoneWeight_t854 *)UnBox (L_1, BoneWeight_t854_il2cpp_TypeInfo_var))));
		int32_t L_2 = BoneWeight_get_boneIndex0_m5074(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = BoneWeight_get_boneIndex0_m5074((&V_0), /*hidden argument*/NULL);
		bool L_4 = Int32_Equals_m6358((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_5 = BoneWeight_get_boneIndex1_m5076(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = BoneWeight_get_boneIndex1_m5076((&V_0), /*hidden argument*/NULL);
		bool L_7 = Int32_Equals_m6358((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_8 = BoneWeight_get_boneIndex2_m5078(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		int32_t L_9 = BoneWeight_get_boneIndex2_m5078((&V_0), /*hidden argument*/NULL);
		bool L_10 = Int32_Equals_m6358((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00cb;
		}
	}
	{
		int32_t L_11 = BoneWeight_get_boneIndex3_m5080(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		int32_t L_12 = BoneWeight_get_boneIndex3_m5080((&V_0), /*hidden argument*/NULL);
		bool L_13 = Int32_Equals_m6358((&V_4), L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00cb;
		}
	}
	{
		float L_14 = BoneWeight_get_weight0_m5066(__this, /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight1_m5068(__this, /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight2_m5070(__this, /*hidden argument*/NULL);
		float L_17 = BoneWeight_get_weight3_m5072(__this, /*hidden argument*/NULL);
		Vector4__ctor_m2728((&V_5), L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		float L_18 = BoneWeight_get_weight0_m5066((&V_0), /*hidden argument*/NULL);
		float L_19 = BoneWeight_get_weight1_m5068((&V_0), /*hidden argument*/NULL);
		float L_20 = BoneWeight_get_weight2_m5070((&V_0), /*hidden argument*/NULL);
		float L_21 = BoneWeight_get_weight3_m5072((&V_0), /*hidden argument*/NULL);
		Vector4_t5  L_22 = {0};
		Vector4__ctor_m2728(&L_22, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		Vector4_t5  L_23 = L_22;
		Object_t * L_24 = Box(Vector4_t5_il2cpp_TypeInfo_var, &L_23);
		bool L_25 = Vector4_Equals_m5449((&V_5), L_24, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_25));
		goto IL_00cc;
	}

IL_00cb:
	{
		G_B8_0 = 0;
	}

IL_00cc:
	{
		return G_B8_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Equality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Equality_m5084 (Object_t * __this /* static, unused */, BoneWeight_t854  ___lhs, BoneWeight_t854  ___rhs, const MethodInfo* method)
{
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = BoneWeight_get_boneIndex0_m5074((&___lhs), /*hidden argument*/NULL);
		int32_t L_1 = BoneWeight_get_boneIndex0_m5074((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_2 = BoneWeight_get_boneIndex1_m5076((&___lhs), /*hidden argument*/NULL);
		int32_t L_3 = BoneWeight_get_boneIndex1_m5076((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_4 = BoneWeight_get_boneIndex2_m5078((&___lhs), /*hidden argument*/NULL);
		int32_t L_5 = BoneWeight_get_boneIndex2_m5078((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_6 = BoneWeight_get_boneIndex3_m5080((&___lhs), /*hidden argument*/NULL);
		int32_t L_7 = BoneWeight_get_boneIndex3_m5080((&___rhs), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0095;
		}
	}
	{
		float L_8 = BoneWeight_get_weight0_m5066((&___lhs), /*hidden argument*/NULL);
		float L_9 = BoneWeight_get_weight1_m5068((&___lhs), /*hidden argument*/NULL);
		float L_10 = BoneWeight_get_weight2_m5070((&___lhs), /*hidden argument*/NULL);
		float L_11 = BoneWeight_get_weight3_m5072((&___lhs), /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		float L_13 = BoneWeight_get_weight0_m5066((&___rhs), /*hidden argument*/NULL);
		float L_14 = BoneWeight_get_weight1_m5068((&___rhs), /*hidden argument*/NULL);
		float L_15 = BoneWeight_get_weight2_m5070((&___rhs), /*hidden argument*/NULL);
		float L_16 = BoneWeight_get_weight3_m5072((&___rhs), /*hidden argument*/NULL);
		Vector4_t5  L_17 = {0};
		Vector4__ctor_m2728(&L_17, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		bool L_18 = Vector4_op_Equality_m5454(NULL /*static, unused*/, L_12, L_17, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_18));
		goto IL_0096;
	}

IL_0095:
	{
		G_B6_0 = 0;
	}

IL_0096:
	{
		return G_B6_0;
	}
}
// System.Boolean UnityEngine.BoneWeight::op_Inequality(UnityEngine.BoneWeight,UnityEngine.BoneWeight)
extern "C" bool BoneWeight_op_Inequality_m5085 (Object_t * __this /* static, unused */, BoneWeight_t854  ___lhs, BoneWeight_t854  ___rhs, const MethodInfo* method)
{
	{
		BoneWeight_t854  L_0 = ___lhs;
		BoneWeight_t854  L_1 = ___rhs;
		bool L_2 = BoneWeight_op_Equality_m5084(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRendererMethodDeclarations.h"



// UnityEngine.LensFlare
#include "UnityEngine_UnityEngine_LensFlare.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.LensFlare
#include "UnityEngine_UnityEngine_LensFlareMethodDeclarations.h"



// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"


// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C" void Renderer_set_enabled_m2811 (Renderer_t312 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m2811_ftn) (Renderer_t312 *, bool);
	static Renderer_set_enabled_m2811_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m2811_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C" Material_t2 * Renderer_get_material_m2762 (Renderer_t312 * __this, const MethodInfo* method)
{
	typedef Material_t2 * (*Renderer_get_material_m2762_ftn) (Renderer_t312 *);
	static Renderer_get_material_m2762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m2762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C" void Renderer_set_material_m2810 (Renderer_t312 * __this, Material_t2 * ___value, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m2810_ftn) (Renderer_t312 *, Material_t2 *);
	static Renderer_set_material_m2810_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m2810_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C" MaterialU5BU5D_t551* Renderer_get_materials_m3319 (Renderer_t312 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t551* (*Renderer_get_materials_m3319_ftn) (Renderer_t312 *);
	static Renderer_get_materials_m3319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m3319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C" int32_t Renderer_get_sortingLayerID_m4646 (Renderer_t312 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m4646_ftn) (Renderer_t312 *);
	static Renderer_get_sortingLayerID_m4646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m4646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C" int32_t Renderer_get_sortingOrder_m4647 (Renderer_t312 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m4647_ftn) (Renderer_t312 *);
	static Renderer_get_sortingOrder_m4647_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m4647_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.InternalDrawTextureArguments
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.InternalDrawTextureArguments
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumentsMethodDeclarations.h"



// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_Graphics.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_GraphicsMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"


// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" void Graphics_DrawMeshNow_m2911 (Object_t * __this /* static, unused */, Mesh_t245 * ___mesh, Vector3_t215  ___position, Quaternion_t261  ___rotation, const MethodInfo* method)
{
	{
		Mesh_t245 * L_0 = ___mesh;
		Vector3_t215  L_1 = ___position;
		Quaternion_t261  L_2 = ___rotation;
		Graphics_Internal_DrawMeshNow1_m5086(NULL /*static, unused*/, L_0, L_1, L_2, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::Internal_DrawMeshNow1(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C" void Graphics_Internal_DrawMeshNow1_m5086 (Object_t * __this /* static, unused */, Mesh_t245 * ___mesh, Vector3_t215  ___position, Quaternion_t261  ___rotation, int32_t ___materialIndex, const MethodInfo* method)
{
	{
		Mesh_t245 * L_0 = ___mesh;
		int32_t L_1 = ___materialIndex;
		Graphics_INTERNAL_CALL_Internal_DrawMeshNow1_m5087(NULL /*static, unused*/, L_0, (&___position), (&___rotation), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow1(UnityEngine.Mesh,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
extern "C" void Graphics_INTERNAL_CALL_Internal_DrawMeshNow1_m5087 (Object_t * __this /* static, unused */, Mesh_t245 * ___mesh, Vector3_t215 * ___position, Quaternion_t261 * ___rotation, int32_t ___materialIndex, const MethodInfo* method)
{
	typedef void (*Graphics_INTERNAL_CALL_Internal_DrawMeshNow1_m5087_ftn) (Mesh_t245 *, Vector3_t215 *, Quaternion_t261 *, int32_t);
	static Graphics_INTERNAL_CALL_Internal_DrawMeshNow1_m5087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_INTERNAL_CALL_Internal_DrawMeshNow1_m5087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow1(UnityEngine.Mesh,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)");
	_il2cpp_icall_func(___mesh, ___position, ___rotation, ___materialIndex);
}
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Material)
extern "C" void Graphics_DrawTexture_m3035 (Object_t * __this /* static, unused */, Rect_t225  ___screenRect, Texture_t221 * ___texture, Material_t2 * ___mat, const MethodInfo* method)
{
	{
		Rect_t225  L_0 = ___screenRect;
		Texture_t221 * L_1 = ___texture;
		Material_t2 * L_2 = ___mat;
		Graphics_DrawTexture_m5088(NULL /*static, unused*/, L_0, L_1, 0, 0, 0, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
extern "C" void Graphics_DrawTexture_m5088 (Object_t * __this /* static, unused */, Rect_t225  ___screenRect, Texture_t221 * ___texture, int32_t ___leftBorder, int32_t ___rightBorder, int32_t ___topBorder, int32_t ___bottomBorder, Material_t2 * ___mat, const MethodInfo* method)
{
	{
		Rect_t225  L_0 = ___screenRect;
		Texture_t221 * L_1 = ___texture;
		Rect_t225  L_2 = {0};
		Rect__ctor_m2802(&L_2, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		int32_t L_3 = ___leftBorder;
		int32_t L_4 = ___rightBorder;
		int32_t L_5 = ___topBorder;
		int32_t L_6 = ___bottomBorder;
		Material_t2 * L_7 = ___mat;
		Graphics_DrawTexture_m5089(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
extern TypeInfo* InternalDrawTextureArguments_t856_il2cpp_TypeInfo_var;
extern TypeInfo* Color32_t778_il2cpp_TypeInfo_var;
extern "C" void Graphics_DrawTexture_m5089 (Object_t * __this /* static, unused */, Rect_t225  ___screenRect, Texture_t221 * ___texture, Rect_t225  ___sourceRect, int32_t ___leftBorder, int32_t ___rightBorder, int32_t ___topBorder, int32_t ___bottomBorder, Material_t2 * ___mat, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalDrawTextureArguments_t856_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		Color32_t778_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(631);
		s_Il2CppMethodIntialized = true;
	}
	InternalDrawTextureArguments_t856  V_0 = {0};
	Color32_t778  V_1 = {0};
	uint8_t V_2 = 0x0;
	{
		Initobj (InternalDrawTextureArguments_t856_il2cpp_TypeInfo_var, (&V_0));
		Rect_t225  L_0 = ___screenRect;
		(&V_0)->___screenRect_0 = L_0;
		Texture_t221 * L_1 = ___texture;
		(&V_0)->___texture_1 = L_1;
		Rect_t225  L_2 = ___sourceRect;
		(&V_0)->___sourceRect_2 = L_2;
		int32_t L_3 = ___leftBorder;
		(&V_0)->___leftBorder_3 = L_3;
		int32_t L_4 = ___rightBorder;
		(&V_0)->___rightBorder_4 = L_4;
		int32_t L_5 = ___topBorder;
		(&V_0)->___topBorder_5 = L_5;
		int32_t L_6 = ___bottomBorder;
		(&V_0)->___bottomBorder_6 = L_6;
		Initobj (Color32_t778_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_7 = ((int32_t)128);
		V_2 = L_7;
		(&V_1)->___a_3 = L_7;
		uint8_t L_8 = V_2;
		uint8_t L_9 = L_8;
		V_2 = L_9;
		(&V_1)->___b_2 = L_9;
		uint8_t L_10 = V_2;
		uint8_t L_11 = L_10;
		V_2 = L_11;
		(&V_1)->___g_1 = L_11;
		uint8_t L_12 = V_2;
		(&V_1)->___r_0 = L_12;
		Color32_t778  L_13 = V_1;
		(&V_0)->___color_7 = L_13;
		Material_t2 * L_14 = ___mat;
		(&V_0)->___mat_8 = L_14;
		Graphics_DrawTexture_m5090(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.InternalDrawTextureArguments&)
extern "C" void Graphics_DrawTexture_m5090 (Object_t * __this /* static, unused */, InternalDrawTextureArguments_t856 * ___arguments, const MethodInfo* method)
{
	typedef void (*Graphics_DrawTexture_m5090_ftn) (InternalDrawTextureArguments_t856 *);
	static Graphics_DrawTexture_m5090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_DrawTexture_m5090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::DrawTexture(UnityEngine.InternalDrawTextureArguments&)");
	_il2cpp_icall_func(___arguments);
}
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C" void Graphics_Blit_m2731 (Object_t * __this /* static, unused */, Texture_t221 * ___source, RenderTexture_t15 * ___dest, const MethodInfo* method)
{
	typedef void (*Graphics_Blit_m2731_ftn) (Texture_t221 *, RenderTexture_t15 *);
	static Graphics_Blit_m2731_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Blit_m2731_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___source, ___dest);
}
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material)
extern "C" void Graphics_Blit_m2730 (Object_t * __this /* static, unused */, Texture_t221 * ___source, RenderTexture_t15 * ___dest, Material_t2 * ___mat, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		Texture_t221 * L_0 = ___source;
		RenderTexture_t15 * L_1 = ___dest;
		Material_t2 * L_2 = ___mat;
		int32_t L_3 = V_0;
		Graphics_Blit_m5091(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C" void Graphics_Blit_m5091 (Object_t * __this /* static, unused */, Texture_t221 * ___source, RenderTexture_t15 * ___dest, Material_t2 * ___mat, int32_t ___pass, const MethodInfo* method)
{
	{
		Texture_t221 * L_0 = ___source;
		RenderTexture_t15 * L_1 = ___dest;
		Material_t2 * L_2 = ___mat;
		int32_t L_3 = ___pass;
		Graphics_Internal_BlitMaterial_m5092(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Graphics::Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean)
extern "C" void Graphics_Internal_BlitMaterial_m5092 (Object_t * __this /* static, unused */, Texture_t221 * ___source, RenderTexture_t15 * ___dest, Material_t2 * ___mat, int32_t ___pass, bool ___setRT, const MethodInfo* method)
{
	typedef void (*Graphics_Internal_BlitMaterial_m5092_ftn) (Texture_t221 *, RenderTexture_t15 *, Material_t2 *, int32_t, bool);
	static Graphics_Internal_BlitMaterial_m5092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Graphics_Internal_BlitMaterial_m5092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Graphics::Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___source, ___dest, ___mat, ___pass, ___setRT);
}
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_Screen.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"



// System.Int32 UnityEngine.Screen::get_width()
extern "C" int32_t Screen_get_width_m2737 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m2737_ftn) ();
	static Screen_get_width_m2737_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m2737_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C" int32_t Screen_get_height_m2738 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m2738_ftn) ();
	static Screen_get_height_m2738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m2738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C" float Screen_get_dpi_m2922 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m2922_ftn) ();
	static Screen_get_dpi_m2922_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m2922_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
extern "C" void Screen_set_sleepTimeout_m2837 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Screen_set_sleepTimeout_m2837_ftn) (int32_t);
	static Screen_set_sleepTimeout_m2837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_sleepTimeout_m2837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_sleepTimeout(System.Int32)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GL.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"

// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"


// System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
extern "C" void GL_Vertex3_m2929 (Object_t * __this /* static, unused */, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	typedef void (*GL_Vertex3_m2929_ftn) (float, float, float);
	static GL_Vertex3_m2929_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_Vertex3_m2929_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(___x, ___y, ___z);
}
// System.Void UnityEngine.GL::Begin(System.Int32)
extern "C" void GL_Begin_m2927 (Object_t * __this /* static, unused */, int32_t ___mode, const MethodInfo* method)
{
	typedef void (*GL_Begin_m2927_ftn) (int32_t);
	static GL_Begin_m2927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_Begin_m2927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::Begin(System.Int32)");
	_il2cpp_icall_func(___mode);
}
// System.Void UnityEngine.GL::End()
extern "C" void GL_End_m2930 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GL_End_m2930_ftn) ();
	static GL_End_m2930_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_End_m2930_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::End()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::LoadOrtho()
extern "C" void GL_LoadOrtho_m2925 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GL_LoadOrtho_m2925_ftn) ();
	static GL_LoadOrtho_m2925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_LoadOrtho_m2925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::LoadOrtho()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::LoadPixelMatrixArgs(System.Single,System.Single,System.Single,System.Single)
extern "C" void GL_LoadPixelMatrixArgs_m5093 (Object_t * __this /* static, unused */, float ___left, float ___right, float ___bottom, float ___top, const MethodInfo* method)
{
	typedef void (*GL_LoadPixelMatrixArgs_m5093_ftn) (float, float, float, float);
	static GL_LoadPixelMatrixArgs_m5093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_LoadPixelMatrixArgs_m5093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::LoadPixelMatrixArgs(System.Single,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(___left, ___right, ___bottom, ___top);
}
// System.Void UnityEngine.GL::LoadPixelMatrix(System.Single,System.Single,System.Single,System.Single)
extern "C" void GL_LoadPixelMatrix_m2932 (Object_t * __this /* static, unused */, float ___left, float ___right, float ___bottom, float ___top, const MethodInfo* method)
{
	{
		float L_0 = ___left;
		float L_1 = ___right;
		float L_2 = ___bottom;
		float L_3 = ___top;
		GL_LoadPixelMatrixArgs_m5093(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::MultMatrix(UnityEngine.Matrix4x4)
extern "C" void GL_MultMatrix_m2926 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___mat, const MethodInfo* method)
{
	{
		GL_INTERNAL_CALL_MultMatrix_m5094(NULL /*static, unused*/, (&___mat), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_MultMatrix(UnityEngine.Matrix4x4&)
extern "C" void GL_INTERNAL_CALL_MultMatrix_m5094 (Object_t * __this /* static, unused */, Matrix4x4_t242 * ___mat, const MethodInfo* method)
{
	typedef void (*GL_INTERNAL_CALL_MultMatrix_m5094_ftn) (Matrix4x4_t242 *);
	static GL_INTERNAL_CALL_MultMatrix_m5094_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_MultMatrix_m5094_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_MultMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___mat);
}
// System.Void UnityEngine.GL::PushMatrix()
extern "C" void GL_PushMatrix_m2924 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GL_PushMatrix_m2924_ftn) ();
	static GL_PushMatrix_m2924_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_PushMatrix_m2924_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::PushMatrix()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::PopMatrix()
extern "C" void GL_PopMatrix_m2931 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GL_PopMatrix_m2931_ftn) ();
	static GL_PopMatrix_m2931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_PopMatrix_m2931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::PopMatrix()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color)
extern "C" void GL_Clear_m2789 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t6  ___backgroundColor, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		bool L_0 = ___clearDepth;
		bool L_1 = ___clearColor;
		Color_t6  L_2 = ___backgroundColor;
		float L_3 = V_0;
		GL_Clear_m5095(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C" void GL_Clear_m5095 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t6  ___backgroundColor, float ___depth, const MethodInfo* method)
{
	{
		bool L_0 = ___clearDepth;
		bool L_1 = ___clearColor;
		Color_t6  L_2 = ___backgroundColor;
		float L_3 = ___depth;
		GL_Internal_Clear_m5096(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern "C" void GL_Internal_Clear_m5096 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t6  ___backgroundColor, float ___depth, const MethodInfo* method)
{
	{
		bool L_0 = ___clearDepth;
		bool L_1 = ___clearColor;
		float L_2 = ___depth;
		GL_INTERNAL_CALL_Internal_Clear_m5097(NULL /*static, unused*/, L_0, L_1, (&___backgroundColor), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
extern "C" void GL_INTERNAL_CALL_Internal_Clear_m5097 (Object_t * __this /* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t6 * ___backgroundColor, float ___depth, const MethodInfo* method)
{
	typedef void (*GL_INTERNAL_CALL_Internal_Clear_m5097_ftn) (bool, bool, Color_t6 *, float);
	static GL_INTERNAL_CALL_Internal_Clear_m5097_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_INTERNAL_CALL_Internal_Clear_m5097_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)");
	_il2cpp_icall_func(___clearDepth, ___clearColor, ___backgroundColor, ___depth);
}
// System.Void UnityEngine.GL::IssuePluginEvent(System.Int32)
extern "C" void GL_IssuePluginEvent_m3042 (Object_t * __this /* static, unused */, int32_t ___eventID, const MethodInfo* method)
{
	typedef void (*GL_IssuePluginEvent_m3042_ftn) (int32_t);
	static GL_IssuePluginEvent_m3042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GL_IssuePluginEvent_m3042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GL::IssuePluginEvent(System.Int32)");
	_il2cpp_icall_func(___eventID);
}
// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRendererMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"

// System.Exception
#include "mscorlib_System_Exception.h"
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"


// System.Void UnityEngine.Texture::.ctor()
extern "C" void Texture__ctor_m5098 (Texture_t221 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetWidth_m5099 (Object_t * __this /* static, unused */, Texture_t221 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m5099_ftn) (Texture_t221 *);
	static Texture_Internal_GetWidth_m5099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m5099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C" int32_t Texture_Internal_GetHeight_m5100 (Object_t * __this /* static, unused */, Texture_t221 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m5100_ftn) (Texture_t221 *);
	static Texture_Internal_GetHeight_m5100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m5100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	return _il2cpp_icall_func(___mono);
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C" int32_t Texture_get_width_m5101 (Texture_t221 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetWidth_m5099(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_width(System.Int32)
extern TypeInfo* Exception_t520_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral876;
extern "C" void Texture_set_width_m5102 (Texture_t221 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t520_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		_stringLiteral876 = il2cpp_codegen_string_literal_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t520 * L_0 = (Exception_t520 *)il2cpp_codegen_object_new (Exception_t520_il2cpp_TypeInfo_var);
		Exception__ctor_m6359(L_0, _stringLiteral876, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C" int32_t Texture_get_height_m5103 (Texture_t221 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Texture_Internal_GetHeight_m5100(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Texture::set_height(System.Int32)
extern TypeInfo* Exception_t520_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral876;
extern "C" void Texture_set_height_m5104 (Texture_t221 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t520_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		_stringLiteral876 = il2cpp_codegen_string_literal_from_index(876);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t520 * L_0 = (Exception_t520 *)il2cpp_codegen_object_new (Exception_t520_il2cpp_TypeInfo_var);
		Exception__ctor_m6359(L_0, _stringLiteral876, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern "C" void Texture_set_filterMode_m2745 (Texture_t221 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Texture_set_filterMode_m2745_ftn) (Texture_t221 *, int32_t);
	static Texture_set_filterMode_m2745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_filterMode_m2745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
extern "C" void Texture_set_anisoLevel_m3057 (Texture_t221 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Texture_set_anisoLevel_m3057_ftn) (Texture_t221 *, int32_t);
	static Texture_set_anisoLevel_m3057_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_anisoLevel_m3057_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_anisoLevel(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.IntPtr UnityEngine.Texture::GetNativeTexturePtr()
extern "C" IntPtr_t Texture_GetNativeTexturePtr_m3039 (Texture_t221 * __this, const MethodInfo* method)
{
	typedef IntPtr_t (*Texture_GetNativeTexturePtr_m3039_ftn) (Texture_t221 *);
	static Texture_GetNativeTexturePtr_m3039_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_GetNativeTexturePtr_m3039_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::GetNativeTexturePtr()");
	return _il2cpp_icall_func(__this);
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"


// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Texture2D__ctor_m5105 (Texture2D_t9 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(632);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m5098(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Texture2D_Internal_Create_m5106(NULL /*static, unused*/, __this, L_0, L_1, 5, 1, 0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void Texture2D__ctor_m3099 (Texture2D_t9 * __this, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(632);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture__ctor_m5098(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		int32_t L_2 = ___format;
		bool L_3 = ___mipmap;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		Texture2D_Internal_Create_m5106(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, 0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C" void Texture2D_Internal_Create_m5106 (Object_t * __this /* static, unused */, Texture2D_t9 * ___mono, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, bool ___linear, IntPtr_t ___nativeTex, const MethodInfo* method)
{
	typedef void (*Texture2D_Internal_Create_m5106_ftn) (Texture2D_t9 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m5106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m5106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono, ___width, ___height, ___format, ___mipmap, ___linear, ___nativeTex);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C" Texture2D_t9 * Texture2D_get_whiteTexture_m4687 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Texture2D_t9 * (*Texture2D_get_whiteTexture_m4687_ftn) ();
	static Texture2D_get_whiteTexture_m4687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m4687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	return _il2cpp_icall_func();
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C" Color_t6  Texture2D_GetPixelBilinear_m4752 (Texture2D_t9 * __this, float ___u, float ___v, const MethodInfo* method)
{
	typedef Color_t6  (*Texture2D_GetPixelBilinear_m4752_ftn) (Texture2D_t9 *, float, float);
	static Texture2D_GetPixelBilinear_m4752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixelBilinear_m4752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)");
	return _il2cpp_icall_func(__this, ___u, ___v);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C" void Texture2D_SetPixels_m3338 (Texture2D_t9 * __this, ColorU5BU5D_t449* ___colors, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t449* L_0 = ___colors;
		int32_t L_1 = V_0;
		Texture2D_SetPixels_m5107(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C" void Texture2D_SetPixels_m5107 (Texture2D_t9 * __this, ColorU5BU5D_t449* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		V_0 = 1;
	}

IL_0015:
	{
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 1;
	}

IL_002a:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t449* L_8 = ___colors;
		int32_t L_9 = ___miplevel;
		Texture2D_SetPixels_m5108(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C" void Texture2D_SetPixels_m5108 (Texture2D_t9 * __this, int32_t ___x, int32_t ___y, int32_t ___blockWidth, int32_t ___blockHeight, ColorU5BU5D_t449* ___colors, int32_t ___miplevel, const MethodInfo* method)
{
	typedef void (*Texture2D_SetPixels_m5108_ftn) (Texture2D_t9 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t449*, int32_t);
	static Texture2D_SetPixels_m5108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m5108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x, ___y, ___blockWidth, ___blockHeight, ___colors, ___miplevel);
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C" void Texture2D_Apply_m5109 (Texture2D_t9 * __this, bool ___updateMipmaps, bool ___makeNoLongerReadable, const MethodInfo* method)
{
	typedef void (*Texture2D_Apply_m5109_ftn) (Texture2D_t9 *, bool, bool);
	static Texture2D_Apply_m5109_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m5109_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps, ___makeNoLongerReadable);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C" void Texture2D_Apply_m3101 (Texture2D_t9 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = 0;
		V_1 = 1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m5109(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern "C" void Texture2D_ReadPixels_m3100 (Texture2D_t9 * __this, Rect_t225  ___source, int32_t ___destX, int32_t ___destY, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		int32_t L_0 = ___destX;
		int32_t L_1 = ___destY;
		bool L_2 = V_0;
		Texture2D_INTERNAL_CALL_ReadPixels_m5110(NULL /*static, unused*/, __this, (&___source), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern "C" void Texture2D_INTERNAL_CALL_ReadPixels_m5110 (Object_t * __this /* static, unused */, Texture2D_t9 * ___self, Rect_t225 * ___source, int32_t ___destX, int32_t ___destY, bool ___recalculateMipMaps, const MethodInfo* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_ReadPixels_m5110_ftn) (Texture2D_t9 *, Rect_t225 *, int32_t, int32_t, bool);
	static Texture2D_INTERNAL_CALL_ReadPixels_m5110_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_ReadPixels_m5110_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)");
	_il2cpp_icall_func(___self, ___source, ___destX, ___destY, ___recalculateMipMaps);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"

// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"


// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern "C" void RenderTexture__ctor_m2783 (RenderTexture_t15 * __this, int32_t ___width, int32_t ___height, int32_t ___depth, int32_t ___format, const MethodInfo* method)
{
	{
		Texture__ctor_m5098(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m5111(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		RenderTexture_set_width_m5118(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height;
		RenderTexture_set_height_m5119(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth;
		RenderTexture_set_depth_m5120(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format;
		RenderTexture_set_format_m5121(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = QualitySettings_get_activeColorSpace_m5064(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m5117(NULL /*static, unused*/, __this, ((((int32_t)L_4) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" void RenderTexture__ctor_m2741 (RenderTexture_t15 * __this, int32_t ___width, int32_t ___height, int32_t ___depth, const MethodInfo* method)
{
	{
		Texture__ctor_m5098(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m5111(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width;
		RenderTexture_set_width_m5118(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height;
		RenderTexture_set_height_m5119(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth;
		RenderTexture_set_depth_m5120(__this, L_2, /*hidden argument*/NULL);
		RenderTexture_set_format_m5121(__this, 7, /*hidden argument*/NULL);
		int32_t L_3 = QualitySettings_get_activeColorSpace_m5064(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_Internal_SetSRGBReadWrite_m5117(NULL /*static, unused*/, __this, ((((int32_t)L_3) == ((int32_t)1))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C" void RenderTexture_Internal_CreateRenderTexture_m5111 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___rt, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m5111_ftn) (RenderTexture_t15 *);
	static RenderTexture_Internal_CreateRenderTexture_m5111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m5111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern "C" RenderTexture_t15 * RenderTexture_GetTemporary_m5112 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, int32_t ___depthBuffer, int32_t ___format, int32_t ___readWrite, int32_t ___antiAliasing, const MethodInfo* method)
{
	typedef RenderTexture_t15 * (*RenderTexture_GetTemporary_m5112_ftn) (int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	static RenderTexture_GetTemporary_m5112_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_GetTemporary_m5112_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)");
	return _il2cpp_icall_func(___width, ___height, ___depthBuffer, ___format, ___readWrite, ___antiAliasing);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32)
extern "C" RenderTexture_t15 * RenderTexture_GetTemporary_m2744 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, int32_t ___depthBuffer, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	int32_t V_2 = {0};
	{
		V_0 = 1;
		V_1 = 0;
		V_2 = 7;
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		int32_t L_2 = ___depthBuffer;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		RenderTexture_t15 * L_6 = RenderTexture_GetTemporary_m5112(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern "C" void RenderTexture_ReleaseTemporary_m2746 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___temp, const MethodInfo* method)
{
	typedef void (*RenderTexture_ReleaseTemporary_m2746_ftn) (RenderTexture_t15 *);
	static RenderTexture_ReleaseTemporary_m2746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_ReleaseTemporary_m2746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___temp);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetWidth_m5113 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m5113_ftn) (RenderTexture_t15 *);
	static RenderTexture_Internal_GetWidth_m5113_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m5113_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C" void RenderTexture_Internal_SetWidth_m5114 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___mono, int32_t ___width, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m5114_ftn) (RenderTexture_t15 *, int32_t);
	static RenderTexture_Internal_SetWidth_m5114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m5114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono, ___width);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C" int32_t RenderTexture_Internal_GetHeight_m5115 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___mono, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m5115_ftn) (RenderTexture_t15 *);
	static RenderTexture_Internal_GetHeight_m5115_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m5115_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C" void RenderTexture_Internal_SetHeight_m5116 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___mono, int32_t ___width, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m5116_ftn) (RenderTexture_t15 *, int32_t);
	static RenderTexture_Internal_SetHeight_m5116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m5116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono, ___width);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C" void RenderTexture_Internal_SetSRGBReadWrite_m5117 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___mono, bool ___sRGB, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m5117_ftn) (RenderTexture_t15 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m5117_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m5117_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono, ___sRGB);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C" int32_t RenderTexture_get_width_m2726 (RenderTexture_t15 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m5113(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C" void RenderTexture_set_width_m5118 (RenderTexture_t15 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		RenderTexture_Internal_SetWidth_m5114(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C" int32_t RenderTexture_get_height_m2727 (RenderTexture_t15 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m5115(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C" void RenderTexture_set_height_m5119 (RenderTexture_t15 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		RenderTexture_Internal_SetHeight_m5116(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C" void RenderTexture_set_depth_m5120 (RenderTexture_t15 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m5120_ftn) (RenderTexture_t15 *, int32_t);
	static RenderTexture_set_depth_m5120_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m5120_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C" void RenderTexture_set_format_m5121 (RenderTexture_t15 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m5121_ftn) (RenderTexture_t15 *, int32_t);
	static RenderTexture_set_format_m5121_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m5121_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
extern "C" void RenderTexture_set_antiAliasing_m3059 (RenderTexture_t15 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_antiAliasing_m3059_ftn) (RenderTexture_t15 *, int32_t);
	static RenderTexture_set_antiAliasing_m3059_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_antiAliasing_m3059_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.RenderTexture::Create()
extern "C" bool RenderTexture_Create_m2784 (RenderTexture_t15 * __this, const MethodInfo* method)
{
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_Create_m5122(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C" bool RenderTexture_INTERNAL_CALL_Create_m5122 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___self, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_Create_m5122_ftn) (RenderTexture_t15 *);
	static RenderTexture_INTERNAL_CALL_Create_m5122_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Create_m5122_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self);
}
// System.Void UnityEngine.RenderTexture::Release()
extern "C" void RenderTexture_Release_m2782 (RenderTexture_t15 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_Release_m5123(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C" void RenderTexture_INTERNAL_CALL_Release_m5123 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___self, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_Release_m5123_ftn) (RenderTexture_t15 *);
	static RenderTexture_INTERNAL_CALL_Release_m5123_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Release_m5123_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self);
}
// System.Boolean UnityEngine.RenderTexture::IsCreated()
extern "C" bool RenderTexture_IsCreated_m2933 (RenderTexture_t15 * __this, const MethodInfo* method)
{
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_IsCreated_m5124(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C" bool RenderTexture_INTERNAL_CALL_IsCreated_m5124 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___self, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_IsCreated_m5124_ftn) (RenderTexture_t15 *);
	static RenderTexture_INTERNAL_CALL_IsCreated_m5124_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_IsCreated_m5124_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self);
}
// System.Void UnityEngine.RenderTexture::DiscardContents()
extern "C" void RenderTexture_DiscardContents_m2912 (RenderTexture_t15 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_DiscardContents_m5125(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)
extern "C" void RenderTexture_INTERNAL_CALL_DiscardContents_m5125 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___self, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_DiscardContents_m5125_ftn) (RenderTexture_t15 *);
	static RenderTexture_INTERNAL_CALL_DiscardContents_m5125_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_DiscardContents_m5125_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_DiscardContents(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self);
}
// UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
extern "C" RenderTexture_t15 * RenderTexture_get_active_m2787 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef RenderTexture_t15 * (*RenderTexture_get_active_m2787_ftn) ();
	static RenderTexture_get_active_m2787_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_get_active_m2787_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::get_active()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern "C" void RenderTexture_set_active_m2788 (Object_t * __this /* static, unused */, RenderTexture_t15 * ___value, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_active_m2788_ftn) (RenderTexture_t15 *);
	static RenderTexture_set_active_m2788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_active_m2788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.ReflectionProbe
#include "UnityEngine_UnityEngine_ReflectionProbe.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ReflectionProbe
#include "UnityEngine_UnityEngine_ReflectionProbeMethodDeclarations.h"



// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElement.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUIElement
#include "UnityEngine_UnityEngine_GUIElementMethodDeclarations.h"



// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITexture.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUITexture
#include "UnityEngine_UnityEngine_GUITextureMethodDeclarations.h"



// UnityEngine.Color UnityEngine.GUITexture::get_color()
extern "C" Color_t6  GUITexture_get_color_m3301 (GUITexture_t548 * __this, const MethodInfo* method)
{
	Color_t6  V_0 = {0};
	{
		GUITexture_INTERNAL_get_color_m5126(__this, (&V_0), /*hidden argument*/NULL);
		Color_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
extern "C" void GUITexture_set_color_m3305 (GUITexture_t548 * __this, Color_t6  ___value, const MethodInfo* method)
{
	{
		GUITexture_INTERNAL_set_color_m5127(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_get_color_m5126 (GUITexture_t548 * __this, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*GUITexture_INTERNAL_get_color_m5126_ftn) (GUITexture_t548 *, Color_t6 *);
	static GUITexture_INTERNAL_get_color_m5126_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_INTERNAL_get_color_m5126_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_set_color_m5127 (GUITexture_t548 * __this, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*GUITexture_INTERNAL_set_color_m5127_ftn) (GUITexture_t548 *, Color_t6 *);
	static GUITexture_INTERNAL_set_color_m5127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_INTERNAL_set_color_m5127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C" void GUITexture_set_texture_m3339 (GUITexture_t548 * __this, Texture_t221 * ___value, const MethodInfo* method)
{
	typedef void (*GUITexture_set_texture_m3339_ftn) (GUITexture_t548 *, Texture_t221 *);
	static GUITexture_set_texture_m3339_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUITexture_set_texture_m3339_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUITexture::set_texture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayer.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUILayer
#include "UnityEngine_UnityEngine_GUILayerMethodDeclarations.h"



// UnityEngine.GUIElement UnityEngine.GUILayer::HitTest(UnityEngine.Vector3)
extern "C" GUIElement_t861 * GUILayer_HitTest_m5128 (GUILayer_t862 * __this, Vector3_t215  ___screenPosition, const MethodInfo* method)
{
	{
		GUIElement_t861 * L_0 = GUILayer_INTERNAL_CALL_HitTest_m5129(NULL /*static, unused*/, __this, (&___screenPosition), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
extern "C" GUIElement_t861 * GUILayer_INTERNAL_CALL_HitTest_m5129 (Object_t * __this /* static, unused */, GUILayer_t862 * ___self, Vector3_t215 * ___screenPosition, const MethodInfo* method)
{
	typedef GUIElement_t861 * (*GUILayer_INTERNAL_CALL_HitTest_m5129_ftn) (GUILayer_t862 *, Vector3_t215 *);
	static GUILayer_INTERNAL_CALL_HitTest_m5129_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayer_INTERNAL_CALL_HitTest_m5129_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___screenPosition);
}
// UnityEngine.GradientColorKey
#include "UnityEngine_UnityEngine_GradientColorKey.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GradientColorKey
#include "UnityEngine_UnityEngine_GradientColorKeyMethodDeclarations.h"



// System.Void UnityEngine.GradientColorKey::.ctor(UnityEngine.Color,System.Single)
extern "C" void GradientColorKey__ctor_m5130 (GradientColorKey_t863 * __this, Color_t6  ___col, float ___time, const MethodInfo* method)
{
	{
		Color_t6  L_0 = ___col;
		__this->___color_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// UnityEngine.GradientAlphaKey
#include "UnityEngine_UnityEngine_GradientAlphaKey.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GradientAlphaKey
#include "UnityEngine_UnityEngine_GradientAlphaKeyMethodDeclarations.h"



// System.Void UnityEngine.GradientAlphaKey::.ctor(System.Single,System.Single)
extern "C" void GradientAlphaKey__ctor_m5131 (GradientAlphaKey_t864 * __this, float ___alpha, float ___time, const MethodInfo* method)
{
	{
		float L_0 = ___alpha;
		__this->___alpha_0 = L_0;
		float L_1 = ___time;
		__this->___time_1 = L_1;
		return;
	}
}
// UnityEngine.Gradient
#include "UnityEngine_UnityEngine_Gradient.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Gradient
#include "UnityEngine_UnityEngine_GradientMethodDeclarations.h"



// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m5132 (Gradient_t865 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Gradient_Init_m5133(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m5133 (Gradient_t865 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Init_m5133_ftn) (Gradient_t865 *);
	static Gradient_Init_m5133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Init_m5133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m5134 (Gradient_t865 * __this, const MethodInfo* method)
{
	typedef void (*Gradient_Cleanup_m5134_ftn) (Gradient_t865 *);
	static Gradient_Cleanup_m5134_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gradient_Cleanup_m5134_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gradient::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m5135 (Gradient_t865 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Gradient_Cleanup_m5134(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Gradient
void Gradient_t865_marshal(const Gradient_t865& unmarshaled, Gradient_t865_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
void Gradient_t865_marshal_back(const Gradient_t865_marshaled& marshaled, Gradient_t865& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Gradient
void Gradient_t865_marshal_cleanup(Gradient_t865_marshaled& marshaled)
{
}
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleModeMethodDeclarations.h"



// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewState.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUI/ScrollViewState
#include "UnityEngine_UnityEngine_GUI_ScrollViewStateMethodDeclarations.h"



// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
extern "C" void ScrollViewState__ctor_m5136 (ScrollViewState_t867 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"

// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.GUI/WindowFunction::.ctor(System.Object,System.IntPtr)
extern "C" void WindowFunction__ctor_m5137 (WindowFunction_t868 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUI/WindowFunction::Invoke(System.Int32)
extern "C" void WindowFunction_Invoke_m5138 (WindowFunction_t868 * __this, int32_t ___id, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WindowFunction_Invoke_m5138((WindowFunction_t868 *)__this->___prev_9,___id, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___id, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___id,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t868(Il2CppObject* delegate, int32_t ___id)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___id' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___id);

	// Marshaling cleanup of parameter '___id' native representation

}
// System.IAsyncResult UnityEngine.GUI/WindowFunction::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern "C" Object_t * WindowFunction_BeginInvoke_m5139 (WindowFunction_t868 * __this, int32_t ___id, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t478_il2cpp_TypeInfo_var, &___id);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUI/WindowFunction::EndInvoke(System.IAsyncResult)
extern "C" void WindowFunction_EndInvoke_m5140 (WindowFunction_t868 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUI.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"

// System.DateTime
#include "mscorlib_System_DateTime.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkin.h"
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOption.h"
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
// UnityEngine.GUISkin
#include "UnityEngine_UnityEngine_GUISkinMethodDeclarations.h"
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtilityMethodDeclarations.h"
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayoutMethodDeclarations.h"


// System.Void UnityEngine.GUI::.cctor()
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t870_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t871_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral877;
extern Il2CppCodeGenString* _stringLiteral878;
extern Il2CppCodeGenString* _stringLiteral879;
extern Il2CppCodeGenString* _stringLiteral880;
extern Il2CppCodeGenString* _stringLiteral881;
extern Il2CppCodeGenString* _stringLiteral882;
extern Il2CppCodeGenString* _stringLiteral883;
extern "C" void GUI__cctor_m5141 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		GenericStack_t870_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(633);
		DateTime_t871_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(634);
		_stringLiteral877 = il2cpp_codegen_string_literal_from_index(877);
		_stringLiteral878 = il2cpp_codegen_string_literal_from_index(878);
		_stringLiteral879 = il2cpp_codegen_string_literal_from_index(879);
		_stringLiteral880 = il2cpp_codegen_string_literal_from_index(880);
		_stringLiteral881 = il2cpp_codegen_string_literal_from_index(881);
		_stringLiteral882 = il2cpp_codegen_string_literal_from_index(882);
		_stringLiteral883 = il2cpp_codegen_string_literal_from_index(883);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___scrollStepSize_0 = (10.0f);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___hotTextField_2 = (-1);
		NullCheck(_stringLiteral877);
		int32_t L_0 = String_GetHashCode_m6360(_stringLiteral877, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___boxHash_5 = L_0;
		NullCheck(_stringLiteral878);
		int32_t L_1 = String_GetHashCode_m6360(_stringLiteral878, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___repeatButtonHash_6 = L_1;
		NullCheck(_stringLiteral879);
		int32_t L_2 = String_GetHashCode_m6360(_stringLiteral879, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___toggleHash_7 = L_2;
		NullCheck(_stringLiteral880);
		int32_t L_3 = String_GetHashCode_m6360(_stringLiteral880, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___buttonGridHash_8 = L_3;
		NullCheck(_stringLiteral881);
		int32_t L_4 = String_GetHashCode_m6360(_stringLiteral881, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___sliderHash_9 = L_4;
		NullCheck(_stringLiteral882);
		int32_t L_5 = String_GetHashCode_m6360(_stringLiteral882, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___beginGroupHash_10 = L_5;
		NullCheck(_stringLiteral883);
		int32_t L_6 = String_GetHashCode_m6360(_stringLiteral883, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___scrollviewHash_11 = L_6;
		GenericStack_t870 * L_7 = (GenericStack_t870 *)il2cpp_codegen_object_new (GenericStack_t870_il2cpp_TypeInfo_var);
		GenericStack__ctor_m6342(L_7, /*hidden argument*/NULL);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___s_ScrollViewStates_12 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t871_il2cpp_TypeInfo_var);
		DateTime_t871  L_8 = DateTime_get_Now_m6361(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_nextScrollStepTime_m5142(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" void GUI_set_nextScrollStepTime_m5142 (Object_t * __this /* static, unused */, DateTime_t871  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		DateTime_t871  L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___U3CnextScrollStepTimeU3Ek__BackingField_13 = L_0;
		return;
	}
}
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" void GUI_set_skin_m5143 (Object_t * __this /* static, unused */, GUISkin_t869 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m5219(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUISkin_t869 * L_0 = ___value;
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		GUISkin_t869 * L_2 = GUIUtility_GetDefaultSkin_m5213(NULL /*static, unused*/, /*hidden argument*/NULL);
		___value = L_2;
	}

IL_0017:
	{
		GUISkin_t869 * L_3 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3 = L_3;
		GUISkin_t869 * L_4 = ___value;
		NullCheck(L_4);
		GUISkin_MakeCurrent_m5278(L_4, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" GUISkin_t869 * GUI_get_skin_m5144 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m5219(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUISkin_t869 * L_0 = ((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3;
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.GUI::get_color()
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" Color_t6  GUI_get_color_m5145 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	Color_t6  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_INTERNAL_get_color_m5146(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Color_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUI_INTERNAL_get_color_m5146 (Object_t * __this /* static, unused */, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_get_color_m5146_ftn) (Color_t6 *);
	static GUI_INTERNAL_get_color_m5146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_get_color_m5146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
extern "C" void GUI_set_changed_m5147 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*GUI_set_changed_m5147_ftn) (bool);
	static GUI_set_changed_m5147_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_set_changed_m5147_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::set_changed(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m3069 (Object_t * __this /* static, unused */, Rect_t225  ___position, String_t* ___text, GUIStyle_t273 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t225  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent_t807 * L_2 = GUIContent_Temp_m5284(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUIStyle_t273 * L_3 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_Label_m5148(NULL /*static, unused*/, L_0, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" void GUI_Label_m5148 (Object_t * __this /* static, unused */, Rect_t225  ___position, GUIContent_t807 * ___content, GUIStyle_t273 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t225  L_0 = ___position;
		GUIContent_t807 * L_1 = ___content;
		GUIStyle_t273 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_DoLabel_m5149(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" void GUI_DoLabel_m5149 (Object_t * __this /* static, unused */, Rect_t225  ___position, GUIContent_t807 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t807 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_INTERNAL_CALL_DoLabel_m5150(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" void GUI_INTERNAL_CALL_DoLabel_m5150 (Object_t * __this /* static, unused */, Rect_t225 * ___position, GUIContent_t807 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef void (*GUI_INTERNAL_CALL_DoLabel_m5150_ftn) (Rect_t225 *, GUIContent_t807 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoLabel_m5150_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoLabel_m5150_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	_il2cpp_icall_func(___position, ___content, ___style);
}
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode)
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" void GUI_DrawTexture_m2803 (Object_t * __this /* static, unused */, Rect_t225  ___position, Texture_t221 * ___image, int32_t ___scaleMode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		V_0 = (0.0f);
		V_1 = 1;
		Rect_t225  L_0 = ___position;
		Texture_t221 * L_1 = ___image;
		int32_t L_2 = ___scaleMode;
		bool L_3 = V_1;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m5151(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode,System.Boolean,System.Single)
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern TypeInfo* InternalDrawTextureArguments_t856_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral884;
extern "C" void GUI_DrawTexture_m5151 (Object_t * __this /* static, unused */, Rect_t225  ___position, Texture_t221 * ___image, int32_t ___scaleMode, bool ___alphaBlend, float ___imageAspect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		InternalDrawTextureArguments_t856_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(630);
		_stringLiteral884 = il2cpp_codegen_string_literal_from_index(884);
		s_Il2CppMethodIntialized = true;
	}
	Material_t2 * V_0 = {0};
	float V_1 = 0.0f;
	InternalDrawTextureArguments_t856  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	int32_t V_7 = {0};
	Material_t2 * G_B8_0 = {0};
	{
		Event_t481 * L_0 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m2786(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)7))))
		{
			goto IL_0277;
		}
	}
	{
		Texture_t221 * L_2 = ___image;
		bool L_3 = Object_op_Equality_m2716(NULL /*static, unused*/, L_2, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		Debug_LogWarning_m2781(NULL /*static, unused*/, _stringLiteral884, /*hidden argument*/NULL);
		return;
	}

IL_0027:
	{
		float L_4 = ___imageAspect;
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		Texture_t221 * L_5 = ___image;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_5);
		Texture_t221 * L_7 = ___image;
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_7);
		___imageAspect = ((float)((float)(((float)L_6))/(float)(((float)L_8))));
	}

IL_0044:
	{
		bool L_9 = ___alphaBlend;
		if (!L_9)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		Material_t2 * L_10 = GUI_get_blendMaterial_m5152(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_10;
		goto IL_0059;
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		Material_t2 * L_11 = GUI_get_blitMaterial_m5153(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_11;
	}

IL_0059:
	{
		V_0 = G_B8_0;
		float L_12 = Rect_get_width_m2798((&___position), /*hidden argument*/NULL);
		float L_13 = Rect_get_height_m2800((&___position), /*hidden argument*/NULL);
		V_1 = ((float)((float)L_12/(float)L_13));
		Initobj (InternalDrawTextureArguments_t856_il2cpp_TypeInfo_var, (&V_2));
		Texture_t221 * L_14 = ___image;
		(&V_2)->___texture_1 = L_14;
		(&V_2)->___leftBorder_3 = 0;
		(&V_2)->___rightBorder_4 = 0;
		(&V_2)->___topBorder_5 = 0;
		(&V_2)->___bottomBorder_6 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		Color_t6  L_15 = GUI_get_color_m5145(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color32_t778  L_16 = Color32_op_Implicit_m4694(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		(&V_2)->___color_7 = L_16;
		Material_t2 * L_17 = V_0;
		(&V_2)->___mat_8 = L_17;
		int32_t L_18 = ___scaleMode;
		V_7 = L_18;
		int32_t L_19 = V_7;
		if (L_19 == 0)
		{
			goto IL_00ce;
		}
		if (L_19 == 1)
		{
			goto IL_0102;
		}
		if (L_19 == 2)
		{
			goto IL_0187;
		}
	}
	{
		goto IL_0277;
	}

IL_00ce:
	{
		Rect_t225  L_20 = ___position;
		(&V_2)->___screenRect_0 = L_20;
		Rect_t225  L_21 = {0};
		Rect__ctor_m2802(&L_21, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_21;
		Graphics_DrawTexture_m5090(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
		goto IL_0277;
	}

IL_0102:
	{
		float L_22 = V_1;
		float L_23 = ___imageAspect;
		if ((!(((float)L_22) > ((float)L_23))))
		{
			goto IL_0147;
		}
	}
	{
		float L_24 = ___imageAspect;
		float L_25 = V_1;
		V_3 = ((float)((float)L_24/(float)L_25));
		Rect_t225  L_26 = ___position;
		(&V_2)->___screenRect_0 = L_26;
		float L_27 = V_3;
		float L_28 = V_3;
		Rect_t225  L_29 = {0};
		Rect__ctor_m2802(&L_29, (0.0f), ((float)((float)((float)((float)(1.0f)-(float)L_27))*(float)(0.5f))), (1.0f), L_28, /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_29;
		Graphics_DrawTexture_m5090(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
		goto IL_0182;
	}

IL_0147:
	{
		float L_30 = V_1;
		float L_31 = ___imageAspect;
		V_4 = ((float)((float)L_30/(float)L_31));
		Rect_t225  L_32 = ___position;
		(&V_2)->___screenRect_0 = L_32;
		float L_33 = V_4;
		float L_34 = V_4;
		Rect_t225  L_35 = {0};
		Rect__ctor_m2802(&L_35, ((float)((float)(0.5f)-(float)((float)((float)L_33*(float)(0.5f))))), (0.0f), L_34, (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_35;
		Graphics_DrawTexture_m5090(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
	}

IL_0182:
	{
		goto IL_0277;
	}

IL_0187:
	{
		float L_36 = V_1;
		float L_37 = ___imageAspect;
		if ((!(((float)L_36) > ((float)L_37))))
		{
			goto IL_0203;
		}
	}
	{
		float L_38 = ___imageAspect;
		float L_39 = V_1;
		V_5 = ((float)((float)L_38/(float)L_39));
		float L_40 = Rect_get_xMin_m2797((&___position), /*hidden argument*/NULL);
		float L_41 = Rect_get_width_m2798((&___position), /*hidden argument*/NULL);
		float L_42 = V_5;
		float L_43 = Rect_get_yMin_m2799((&___position), /*hidden argument*/NULL);
		float L_44 = V_5;
		float L_45 = Rect_get_width_m2798((&___position), /*hidden argument*/NULL);
		float L_46 = Rect_get_height_m2800((&___position), /*hidden argument*/NULL);
		Rect_t225  L_47 = {0};
		Rect__ctor_m2802(&L_47, ((float)((float)L_40+(float)((float)((float)((float)((float)L_41*(float)((float)((float)(1.0f)-(float)L_42))))*(float)(0.5f))))), L_43, ((float)((float)L_44*(float)L_45)), L_46, /*hidden argument*/NULL);
		(&V_2)->___screenRect_0 = L_47;
		Rect_t225  L_48 = {0};
		Rect__ctor_m2802(&L_48, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_48;
		Graphics_DrawTexture_m5090(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
		goto IL_0272;
	}

IL_0203:
	{
		float L_49 = V_1;
		float L_50 = ___imageAspect;
		V_6 = ((float)((float)L_49/(float)L_50));
		float L_51 = Rect_get_xMin_m2797((&___position), /*hidden argument*/NULL);
		float L_52 = Rect_get_yMin_m2799((&___position), /*hidden argument*/NULL);
		float L_53 = Rect_get_height_m2800((&___position), /*hidden argument*/NULL);
		float L_54 = V_6;
		float L_55 = Rect_get_width_m2798((&___position), /*hidden argument*/NULL);
		float L_56 = V_6;
		float L_57 = Rect_get_height_m2800((&___position), /*hidden argument*/NULL);
		Rect_t225  L_58 = {0};
		Rect__ctor_m2802(&L_58, L_51, ((float)((float)L_52+(float)((float)((float)((float)((float)L_53*(float)((float)((float)(1.0f)-(float)L_54))))*(float)(0.5f))))), L_55, ((float)((float)L_56*(float)L_57)), /*hidden argument*/NULL);
		(&V_2)->___screenRect_0 = L_58;
		Rect_t225  L_59 = {0};
		Rect__ctor_m2802(&L_59, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		(&V_2)->___sourceRect_2 = L_59;
		Graphics_DrawTexture_m5090(NULL /*static, unused*/, (&V_2), /*hidden argument*/NULL);
	}

IL_0272:
	{
		goto IL_0277;
	}

IL_0277:
	{
		return;
	}
}
// UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
extern "C" Material_t2 * GUI_get_blendMaterial_m5152 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t2 * (*GUI_get_blendMaterial_m5152_ftn) ();
	static GUI_get_blendMaterial_m5152_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_blendMaterial_m5152_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_blendMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
extern "C" Material_t2 * GUI_get_blitMaterial_m5153 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t2 * (*GUI_get_blitMaterial_m5153_ftn) ();
	static GUI_get_blitMaterial_m5153_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_get_blitMaterial_m5153_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::get_blitMaterial()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m3070 (Object_t * __this /* static, unused */, Rect_t225  ___position, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t225  L_0 = ___position;
		String_t* L_1 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent_t807 * L_2 = GUIContent_Temp_m5284(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUISkin_t869 * L_3 = ((GUI_t483_StaticFields*)GUI_t483_il2cpp_TypeInfo_var->static_fields)->___s_Skin_3;
		NullCheck(L_3);
		GUIStyle_t273 * L_4 = GUISkin_get_button_m5238(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		IntPtr_t L_5 = (L_4->___m_Ptr_0);
		bool L_6 = GUI_DoButton_m5155(NULL /*static, unused*/, L_0, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" bool GUI_Button_m5154 (Object_t * __this /* static, unused */, Rect_t225  ___position, GUIContent_t807 * ___content, GUIStyle_t273 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t225  L_0 = ___position;
		GUIContent_t807 * L_1 = ___content;
		GUIStyle_t273 * L_2 = ___style;
		NullCheck(L_2);
		IntPtr_t L_3 = (L_2->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		bool L_4 = GUI_DoButton_m5155(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" bool GUI_DoButton_m5155 (Object_t * __this /* static, unused */, Rect_t225  ___position, GUIContent_t807 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t807 * L_0 = ___content;
		IntPtr_t L_1 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		bool L_2 = GUI_INTERNAL_CALL_DoButton_m5156(NULL /*static, unused*/, (&___position), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
extern "C" bool GUI_INTERNAL_CALL_DoButton_m5156 (Object_t * __this /* static, unused */, Rect_t225 * ___position, GUIContent_t807 * ___content, IntPtr_t ___style, const MethodInfo* method)
{
	typedef bool (*GUI_INTERNAL_CALL_DoButton_m5156_ftn) (Rect_t225 *, GUIContent_t807 *, IntPtr_t);
	static GUI_INTERNAL_CALL_DoButton_m5156_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUI_INTERNAL_CALL_DoButton_m5156_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)");
	return _il2cpp_icall_func(___position, ___content, ___style);
}
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOptionU5BU5D_t544_il2cpp_TypeInfo_var;
extern "C" void GUI_CallWindowDelegate_m5157 (Object_t * __this /* static, unused */, WindowFunction_t868 * ___func, int32_t ___id, GUISkin_t869 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t273 * ___style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		GUILayoutOptionU5BU5D_t544_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(363);
		s_Il2CppMethodIntialized = true;
	}
	GUISkin_t869 * V_0 = {0};
	GUILayoutOptionU5BU5D_t544* V_1 = {0};
	{
		int32_t L_0 = ___id;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m5163(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUISkin_t869 * L_1 = GUI_get_skin_m5144(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t481 * L_2 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m2786(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = ___forceRect;
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		GUILayoutOptionU5BU5D_t544* L_5 = ((GUILayoutOptionU5BU5D_t544*)SZArrayNew(GUILayoutOptionU5BU5D_t544_il2cpp_TypeInfo_var, 2));
		float L_6 = ___width;
		GUILayoutOption_t882 * L_7 = GUILayout_Width_m5159(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_7);
		*((GUILayoutOption_t882 **)(GUILayoutOption_t882 **)SZArrayLdElema(L_5, 0)) = (GUILayoutOption_t882 *)L_7;
		GUILayoutOptionU5BU5D_t544* L_8 = L_5;
		float L_9 = ___height;
		GUILayoutOption_t882 * L_10 = GUILayout_Height_m5160(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		*((GUILayoutOption_t882 **)(GUILayoutOption_t882 **)SZArrayLdElema(L_8, 1)) = (GUILayoutOption_t882 *)L_10;
		V_1 = L_8;
		int32_t L_11 = ___id;
		GUIStyle_t273 * L_12 = ___style;
		GUILayoutOptionU5BU5D_t544* L_13 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m5165(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0056;
	}

IL_004d:
	{
		int32_t L_14 = ___id;
		GUIStyle_t273 * L_15 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUILayoutUtility_BeginWindow_m5165(NULL /*static, unused*/, L_14, L_15, (GUILayoutOptionU5BU5D_t544*)(GUILayoutOptionU5BU5D_t544*)NULL, /*hidden argument*/NULL);
	}

IL_0056:
	{
		GUISkin_t869 * L_16 = ____skin;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_set_skin_m5143(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		WindowFunction_t868 * L_17 = ___func;
		int32_t L_18 = ___id;
		NullCheck(L_17);
		WindowFunction_Invoke_m5138(L_17, L_18, /*hidden argument*/NULL);
		Event_t481 * L_19 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = Event_get_type_m2786(L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_20) == ((uint32_t)8))))
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUILayoutUtility_Layout_m5166(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0078:
	{
		GUISkin_t869 * L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_set_skin_m5143(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUILayout
#include "UnityEngine_UnityEngine_GUILayout.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
// UnityEngine.GUILayoutOption
#include "UnityEngine_UnityEngine_GUILayoutOptionMethodDeclarations.h"


// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_Button_m3286 (Object_t * __this /* static, unused */, String_t* ___text, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent_t807 * L_1 = GUIContent_Temp_m5284(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUISkin_t869 * L_2 = GUI_get_skin_m5144(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyle_t273 * L_3 = GUISkin_get_button_m5238(L_2, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t544* L_4 = ___options;
		bool L_5 = GUILayout_DoButton_m5158(NULL /*static, unused*/, L_1, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.GUILayout::DoButton(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" bool GUILayout_DoButton_m5158 (Object_t * __this /* static, unused */, GUIContent_t807 * ___content, GUIStyle_t273 * ___style, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t807 * L_0 = ___content;
		GUIStyle_t273 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t544* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		Rect_t225  L_3 = GUILayoutUtility_GetRect_m5173(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		GUIContent_t807 * L_4 = ___content;
		GUIStyle_t273 * L_5 = ___style;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		bool L_6 = GUI_Button_m5154(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t882_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t882 * GUILayout_Width_m5159 (Object_t * __this /* static, unused */, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		GUILayoutOption_t882_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___width;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t531_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t882 * L_3 = (GUILayoutOption_t882 *)il2cpp_codegen_object_new (GUILayoutOption_t882_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m5209(L_3, 0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutOption_t882_il2cpp_TypeInfo_var;
extern "C" GUILayoutOption_t882 * GUILayout_Height_m5160 (Object_t * __this /* static, unused */, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		GUILayoutOption_t882_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___height;
		float L_1 = L_0;
		Object_t * L_2 = Box(Single_t531_il2cpp_TypeInfo_var, &L_1);
		GUILayoutOption_t882 * L_3 = (GUILayoutOption_t882 *)il2cpp_codegen_object_new (GUILayoutOption_t882_il2cpp_TypeInfo_var);
		GUILayoutOption__ctor_m5209(L_3, 1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUILayoutUtility/LayoutCache
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCacheMethodDeclarations.h"

// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroup.h"
// System.Collections.Stack
#include "mscorlib_System_Collections_Stack.h"
// UnityEngine.GUILayoutGroup
#include "UnityEngine_UnityEngine_GUILayoutGroupMethodDeclarations.h"
// System.Collections.Stack
#include "mscorlib_System_Collections_StackMethodDeclarations.h"


// System.Void UnityEngine.GUILayoutUtility/LayoutCache::.ctor()
extern TypeInfo* GUILayoutGroup_t873_il2cpp_TypeInfo_var;
extern TypeInfo* GenericStack_t870_il2cpp_TypeInfo_var;
extern "C" void LayoutCache__ctor_m5161 (LayoutCache_t874 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		GenericStack_t870_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(633);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUILayoutGroup_t873 * L_0 = (GUILayoutGroup_t873 *)il2cpp_codegen_object_new (GUILayoutGroup_t873_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m5189(L_0, /*hidden argument*/NULL);
		__this->___topLevel_0 = L_0;
		GenericStack_t870 * L_1 = (GenericStack_t870 *)il2cpp_codegen_object_new (GenericStack_t870_il2cpp_TypeInfo_var);
		GenericStack__ctor_m6342(L_1, /*hidden argument*/NULL);
		__this->___layoutGroups_1 = L_1;
		GUILayoutGroup_t873 * L_2 = (GUILayoutGroup_t873 *)il2cpp_codegen_object_new (GUILayoutGroup_t873_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m5189(L_2, /*hidden argument*/NULL);
		__this->___windows_2 = L_2;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		GenericStack_t870 * L_3 = (__this->___layoutGroups_1);
		GUILayoutGroup_t873 * L_4 = (__this->___topLevel_0);
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_3, L_4);
		return;
	}
}
// UnityEngine.GUILayoutUtility
#include "UnityEngine_UnityEngine_GUILayoutUtility.h"
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_32.h"
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizer.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9MethodDeclarations.h"
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntryMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_gen_32MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
// UnityEngine.GUIWordWrapSizer
#include "UnityEngine_UnityEngine_GUIWordWrapSizerMethodDeclarations.h"


// System.Void UnityEngine.GUILayoutUtility::.cctor()
extern TypeInfo* Dictionary_2_t875_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t874_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m6362_MethodInfo_var;
extern "C" void GUILayoutUtility__cctor_m5162 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t875_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(639);
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		LayoutCache_t874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(638);
		Dictionary_2__ctor_m6362_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484131);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t875 * L_0 = (Dictionary_2_t875 *)il2cpp_codegen_object_new (Dictionary_2_t875_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m6362(L_0, /*hidden argument*/Dictionary_2__ctor_m6362_MethodInfo_var);
		((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___storedLayouts_0 = L_0;
		Dictionary_2_t875 * L_1 = (Dictionary_2_t875 *)il2cpp_codegen_object_new (Dictionary_2_t875_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m6362(L_1, /*hidden argument*/Dictionary_2__ctor_m6362_MethodInfo_var);
		((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___storedWindows_1 = L_1;
		LayoutCache_t874 * L_2 = (LayoutCache_t874 *)il2cpp_codegen_object_new (LayoutCache_t874_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m5161(L_2, /*hidden argument*/NULL);
		((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2 = L_2;
		Rect_t225  L_3 = {0};
		Rect__ctor_m2802(&L_3, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3 = L_3;
		return;
	}
}
// UnityEngine.GUILayoutUtility/LayoutCache UnityEngine.GUILayoutUtility::SelectIDList(System.Int32,System.Boolean)
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* LayoutCache_t874_il2cpp_TypeInfo_var;
extern "C" LayoutCache_t874 * GUILayoutUtility_SelectIDList_m5163 (Object_t * __this /* static, unused */, int32_t ___instanceID, bool ___isWindow, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		LayoutCache_t874_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(638);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t875 * V_0 = {0};
	LayoutCache_t874 * V_1 = {0};
	Dictionary_2_t875 * G_B3_0 = {0};
	{
		bool L_0 = ___isWindow;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		Dictionary_2_t875 * L_1 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___storedWindows_1;
		G_B3_0 = L_1;
		goto IL_0015;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		Dictionary_2_t875 * L_2 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___storedLayouts_0;
		G_B3_0 = L_2;
	}

IL_0015:
	{
		V_0 = G_B3_0;
		Dictionary_2_t875 * L_3 = V_0;
		int32_t L_4 = ___instanceID;
		NullCheck(L_3);
		bool L_5 = (bool)VirtFuncInvoker2< bool, int32_t, LayoutCache_t874 ** >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::TryGetValue(!0,!1&) */, L_3, L_4, (&V_1));
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		LayoutCache_t874 * L_6 = (LayoutCache_t874 *)il2cpp_codegen_object_new (LayoutCache_t874_il2cpp_TypeInfo_var);
		LayoutCache__ctor_m5161(L_6, /*hidden argument*/NULL);
		V_1 = L_6;
		Dictionary_2_t875 * L_7 = V_0;
		int32_t L_8 = ___instanceID;
		LayoutCache_t874 * L_9 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, LayoutCache_t874 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Item(!0,!1) */, L_7, L_8, L_9);
		goto IL_0037;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_10 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_11 = V_1;
		NullCheck(L_11);
		GUILayoutGroup_t873 * L_12 = (L_11->___topLevel_0);
		NullCheck(L_10);
		L_10->___topLevel_0 = L_12;
		LayoutCache_t874 * L_13 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_14 = V_1;
		NullCheck(L_14);
		GenericStack_t870 * L_15 = (L_14->___layoutGroups_1);
		NullCheck(L_13);
		L_13->___layoutGroups_1 = L_15;
		LayoutCache_t874 * L_16 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_17 = V_1;
		NullCheck(L_17);
		GUILayoutGroup_t873 * L_18 = (L_17->___windows_2);
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		LayoutCache_t874 * L_19 = V_1;
		return L_19;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Begin(System.Int32)
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t873_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Begin_m5164 (Object_t * __this /* static, unused */, int32_t ___instanceID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		GUILayoutGroup_t873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t874 * V_0 = {0};
	GUILayoutGroup_t873 * V_1 = {0};
	{
		int32_t L_0 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_1 = GUILayoutUtility_SelectIDList_m5163(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t481 * L_2 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m2786(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0075;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_4 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_5 = V_0;
		GUILayoutGroup_t873 * L_6 = (GUILayoutGroup_t873 *)il2cpp_codegen_object_new (GUILayoutGroup_t873_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m5189(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t873 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t874 * L_9 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GenericStack_t870 * L_10 = (L_9->___layoutGroups_1);
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Collections.Stack::Clear() */, L_10);
		LayoutCache_t874 * L_11 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_11);
		GenericStack_t870 * L_12 = (L_11->___layoutGroups_1);
		LayoutCache_t874 * L_13 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_13);
		GUILayoutGroup_t873 * L_14 = (L_13->___topLevel_0);
		NullCheck(L_12);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_12, L_14);
		LayoutCache_t874 * L_15 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_16 = V_0;
		GUILayoutGroup_t873 * L_17 = (GUILayoutGroup_t873 *)il2cpp_codegen_object_new (GUILayoutGroup_t873_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m5189(L_17, /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_18 = L_17;
		V_1 = L_18;
		NullCheck(L_16);
		L_16->___windows_2 = L_18;
		GUILayoutGroup_t873 * L_19 = V_1;
		NullCheck(L_15);
		L_15->___windows_2 = L_19;
		goto IL_00a5;
	}

IL_0075:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_20 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_21 = V_0;
		NullCheck(L_21);
		GUILayoutGroup_t873 * L_22 = (L_21->___topLevel_0);
		NullCheck(L_20);
		L_20->___topLevel_0 = L_22;
		LayoutCache_t874 * L_23 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_24 = V_0;
		NullCheck(L_24);
		GenericStack_t870 * L_25 = (L_24->___layoutGroups_1);
		NullCheck(L_23);
		L_23->___layoutGroups_1 = L_25;
		LayoutCache_t874 * L_26 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_27 = V_0;
		NullCheck(L_27);
		GUILayoutGroup_t873 * L_28 = (L_27->___windows_2);
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
	}

IL_00a5:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::BeginWindow(System.Int32,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutGroup_t873_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_BeginWindow_m5165 (Object_t * __this /* static, unused */, int32_t ___windowID, GUIStyle_t273 * ___style, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		GUILayoutGroup_t873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		s_Il2CppMethodIntialized = true;
	}
	LayoutCache_t874 * V_0 = {0};
	GUILayoutGroup_t873 * V_1 = {0};
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_1 = GUILayoutUtility_SelectIDList_m5163(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		V_0 = L_1;
		Event_t481 * L_2 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Event_get_type_m2786(L_2, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_00ab;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_4 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_5 = V_0;
		GUILayoutGroup_t873 * L_6 = (GUILayoutGroup_t873 *)il2cpp_codegen_object_new (GUILayoutGroup_t873_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m5189(L_6, /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_7 = L_6;
		V_1 = L_7;
		NullCheck(L_5);
		L_5->___topLevel_0 = L_7;
		GUILayoutGroup_t873 * L_8 = V_1;
		NullCheck(L_4);
		L_4->___topLevel_0 = L_8;
		LayoutCache_t874 * L_9 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_9);
		GUILayoutGroup_t873 * L_10 = (L_9->___topLevel_0);
		GUIStyle_t273 * L_11 = ___style;
		NullCheck(L_10);
		GUILayoutEntry_set_style_m5180(L_10, L_11, /*hidden argument*/NULL);
		LayoutCache_t874 * L_12 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t873 * L_13 = (L_12->___topLevel_0);
		int32_t L_14 = ___windowID;
		NullCheck(L_13);
		L_13->___windowID_16 = L_14;
		GUILayoutOptionU5BU5D_t544* L_15 = ___options;
		if (!L_15)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_16 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_16);
		GUILayoutGroup_t873 * L_17 = (L_16->___topLevel_0);
		GUILayoutOptionU5BU5D_t544* L_18 = ___options;
		NullCheck(L_17);
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t544* >::Invoke(10 /* System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[]) */, L_17, L_18);
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_19 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_19);
		GenericStack_t870 * L_20 = (L_19->___layoutGroups_1);
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Collections.Stack::Clear() */, L_20);
		LayoutCache_t874 * L_21 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_21);
		GenericStack_t870 * L_22 = (L_21->___layoutGroups_1);
		LayoutCache_t874 * L_23 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t873 * L_24 = (L_23->___topLevel_0);
		NullCheck(L_22);
		VirtActionInvoker1< Object_t * >::Invoke(17 /* System.Void System.Collections.Stack::Push(System.Object) */, L_22, L_24);
		LayoutCache_t874 * L_25 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_26 = V_0;
		GUILayoutGroup_t873 * L_27 = (GUILayoutGroup_t873 *)il2cpp_codegen_object_new (GUILayoutGroup_t873_il2cpp_TypeInfo_var);
		GUILayoutGroup__ctor_m5189(L_27, /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_28 = L_27;
		V_1 = L_28;
		NullCheck(L_26);
		L_26->___windows_2 = L_28;
		GUILayoutGroup_t873 * L_29 = V_1;
		NullCheck(L_25);
		L_25->___windows_2 = L_29;
		goto IL_00db;
	}

IL_00ab:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_30 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_31 = V_0;
		NullCheck(L_31);
		GUILayoutGroup_t873 * L_32 = (L_31->___topLevel_0);
		NullCheck(L_30);
		L_30->___topLevel_0 = L_32;
		LayoutCache_t874 * L_33 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_34 = V_0;
		NullCheck(L_34);
		GenericStack_t870 * L_35 = (L_34->___layoutGroups_1);
		NullCheck(L_33);
		L_33->___layoutGroups_1 = L_35;
		LayoutCache_t874 * L_36 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		LayoutCache_t874 * L_37 = V_0;
		NullCheck(L_37);
		GUILayoutGroup_t873 * L_38 = (L_37->___windows_2);
		NullCheck(L_36);
		L_36->___windows_2 = L_38;
	}

IL_00db:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::Layout()
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Layout_m5166 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_0 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t873 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		int32_t L_2 = (L_1->___windowID_16);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_3 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_3);
		GUILayoutGroup_t873 * L_4 = (L_3->___topLevel_0);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_4);
		LayoutCache_t874 * L_5 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GUILayoutGroup_t873 * L_6 = (L_5->___topLevel_0);
		int32_t L_7 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t874 * L_8 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_8);
		GUILayoutGroup_t873 * L_9 = (L_8->___topLevel_0);
		NullCheck(L_9);
		float L_10 = (((GUILayoutEntry_t877 *)L_9)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Min_m3328(NULL /*static, unused*/, (((float)L_7)), L_10, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_6, (0.0f), L_11);
		LayoutCache_t874 * L_12 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_12);
		GUILayoutGroup_t873 * L_13 = (L_12->___topLevel_0);
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_13);
		LayoutCache_t874 * L_14 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_14);
		GUILayoutGroup_t873 * L_15 = (L_14->___topLevel_0);
		int32_t L_16 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		LayoutCache_t874 * L_17 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_17);
		GUILayoutGroup_t873 * L_18 = (L_17->___topLevel_0);
		NullCheck(L_18);
		float L_19 = (((GUILayoutEntry_t877 *)L_18)->___maxHeight_3);
		float L_20 = Mathf_Min_m3328(NULL /*static, unused*/, (((float)L_16)), L_19, /*hidden argument*/NULL);
		NullCheck(L_15);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_15, (0.0f), L_20);
		LayoutCache_t874 * L_21 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_21);
		GUILayoutGroup_t873 * L_22 = (L_21->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m5168(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_00a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_23 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_23);
		GUILayoutGroup_t873 * L_24 = (L_23->___topLevel_0);
		GUILayoutUtility_LayoutSingleGroup_m5169(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		LayoutCache_t874 * L_25 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_25);
		GUILayoutGroup_t873 * L_26 = (L_25->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m5168(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFromEditorWindow()
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m5167 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_0 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_0);
		GUILayoutGroup_t873 * L_1 = (L_0->___topLevel_0);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_1);
		LayoutCache_t874 * L_2 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_2);
		GUILayoutGroup_t873 * L_3 = (L_2->___topLevel_0);
		int32_t L_4 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_3, (0.0f), (((float)L_4)));
		LayoutCache_t874 * L_5 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_5);
		GUILayoutGroup_t873 * L_6 = (L_5->___topLevel_0);
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_6);
		LayoutCache_t874 * L_7 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_7);
		GUILayoutGroup_t873 * L_8 = (L_7->___topLevel_0);
		int32_t L_9 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_8, (0.0f), (((float)L_9)));
		LayoutCache_t874 * L_10 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_10);
		GUILayoutGroup_t873 * L_11 = (L_10->___windows_2);
		GUILayoutUtility_LayoutFreeGroup_m5168(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutFreeGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* GUILayoutGroup_t873_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1112_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m6363_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6364_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6365_MethodInfo_var;
extern "C" void GUILayoutUtility_LayoutFreeGroup_m5168 (Object_t * __this /* static, unused */, GUILayoutGroup_t873 * ___toplevel, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutGroup_t873_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(637);
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		Enumerator_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		List_1_GetEnumerator_m6363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		Enumerator_get_Current_m6364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		Enumerator_MoveNext_m6365_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutGroup_t873 * V_0 = {0};
	Enumerator_t1112  V_1 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUILayoutGroup_t873 * L_0 = ___toplevel;
		NullCheck(L_0);
		List_1_t878 * L_1 = (L_0->___entries_10);
		NullCheck(L_1);
		Enumerator_t1112  L_2 = List_1_GetEnumerator_m6363(L_1, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_1 = L_2;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0024;
		}

IL_0011:
		{
			GUILayoutEntry_t877 * L_3 = Enumerator_get_Current_m6364((&V_1), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_0 = ((GUILayoutGroup_t873 *)Castclass(L_3, GUILayoutGroup_t873_il2cpp_TypeInfo_var));
			GUILayoutGroup_t873 * L_4 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutSingleGroup_m5169(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		}

IL_0024:
		{
			bool L_5 = Enumerator_MoveNext_m6365((&V_1), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0030:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0035);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0035;
	}

FINALLY_0035:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_6 = V_1;
		Enumerator_t1112  L_7 = L_6;
		Object_t * L_8 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_8);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_8);
		IL2CPP_END_FINALLY(53)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(53)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0041:
	{
		GUILayoutGroup_t873 * L_9 = ___toplevel;
		NullCheck(L_9);
		GUILayoutGroup_ResetCursor_m5193(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::LayoutSingleGroup(UnityEngine.GUILayoutGroup)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_LayoutSingleGroup_m5169 (Object_t * __this /* static, unused */, GUILayoutGroup_t873 * ___i, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Rect_t225  V_4 = {0};
	{
		GUILayoutGroup_t873 * L_0 = ___i;
		NullCheck(L_0);
		bool L_1 = (L_0->___isWindow_15);
		if (L_1)
		{
			goto IL_0074;
		}
	}
	{
		GUILayoutGroup_t873 * L_2 = ___i;
		NullCheck(L_2);
		float L_3 = (((GUILayoutEntry_t877 *)L_2)->___minWidth_0);
		V_0 = L_3;
		GUILayoutGroup_t873 * L_4 = ___i;
		NullCheck(L_4);
		float L_5 = (((GUILayoutEntry_t877 *)L_4)->___maxWidth_1);
		V_1 = L_5;
		GUILayoutGroup_t873 * L_6 = ___i;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_6);
		GUILayoutGroup_t873 * L_7 = ___i;
		GUILayoutGroup_t873 * L_8 = ___i;
		NullCheck(L_8);
		Rect_t225 * L_9 = &(((GUILayoutEntry_t877 *)L_8)->___rect_4);
		float L_10 = Rect_get_x_m2861(L_9, /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_11 = ___i;
		NullCheck(L_11);
		float L_12 = (((GUILayoutEntry_t877 *)L_11)->___maxWidth_1);
		float L_13 = V_0;
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_7, L_10, L_15);
		GUILayoutGroup_t873 * L_16 = ___i;
		NullCheck(L_16);
		float L_17 = (((GUILayoutEntry_t877 *)L_16)->___minHeight_2);
		V_2 = L_17;
		GUILayoutGroup_t873 * L_18 = ___i;
		NullCheck(L_18);
		float L_19 = (((GUILayoutEntry_t877 *)L_18)->___maxHeight_3);
		V_3 = L_19;
		GUILayoutGroup_t873 * L_20 = ___i;
		NullCheck(L_20);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_20);
		GUILayoutGroup_t873 * L_21 = ___i;
		GUILayoutGroup_t873 * L_22 = ___i;
		NullCheck(L_22);
		Rect_t225 * L_23 = &(((GUILayoutEntry_t877 *)L_22)->___rect_4);
		float L_24 = Rect_get_y_m2865(L_23, /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_25 = ___i;
		NullCheck(L_25);
		float L_26 = (((GUILayoutEntry_t877 *)L_25)->___maxHeight_3);
		float L_27 = V_2;
		float L_28 = V_3;
		float L_29 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_21, L_24, L_29);
		goto IL_00e8;
	}

IL_0074:
	{
		GUILayoutGroup_t873 * L_30 = ___i;
		NullCheck(L_30);
		VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutGroup::CalcWidth() */, L_30);
		GUILayoutGroup_t873 * L_31 = ___i;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___windowID_16);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		Rect_t225  L_33 = GUILayoutUtility_Internal_GetWindowRect_m5170(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		GUILayoutGroup_t873 * L_34 = ___i;
		float L_35 = Rect_get_x_m2861((&V_4), /*hidden argument*/NULL);
		float L_36 = Rect_get_width_m2798((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_37 = ___i;
		NullCheck(L_37);
		float L_38 = (((GUILayoutEntry_t877 *)L_37)->___minWidth_0);
		GUILayoutGroup_t873 * L_39 = ___i;
		NullCheck(L_39);
		float L_40 = (((GUILayoutEntry_t877 *)L_39)->___maxWidth_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_41 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_36, L_38, L_40, /*hidden argument*/NULL);
		NullCheck(L_34);
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single) */, L_34, L_35, L_41);
		GUILayoutGroup_t873 * L_42 = ___i;
		NullCheck(L_42);
		VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutGroup::CalcHeight() */, L_42);
		GUILayoutGroup_t873 * L_43 = ___i;
		float L_44 = Rect_get_y_m2865((&V_4), /*hidden argument*/NULL);
		float L_45 = Rect_get_height_m2800((&V_4), /*hidden argument*/NULL);
		GUILayoutGroup_t873 * L_46 = ___i;
		NullCheck(L_46);
		float L_47 = (((GUILayoutEntry_t877 *)L_46)->___minHeight_2);
		GUILayoutGroup_t873 * L_48 = ___i;
		NullCheck(L_48);
		float L_49 = (((GUILayoutEntry_t877 *)L_48)->___maxHeight_3);
		float L_50 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_45, L_47, L_49, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single) */, L_43, L_44, L_50);
		GUILayoutGroup_t873 * L_51 = ___i;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___windowID_16);
		GUILayoutGroup_t873 * L_53 = ___i;
		NullCheck(L_53);
		Rect_t225  L_54 = (((GUILayoutEntry_t877 *)L_53)->___rect_4);
		GUILayoutUtility_Internal_MoveWindow_m5171(NULL /*static, unused*/, L_52, L_54, /*hidden argument*/NULL);
	}

IL_00e8:
	{
		return;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)
extern "C" Rect_t225  GUILayoutUtility_Internal_GetWindowRect_m5170 (Object_t * __this /* static, unused */, int32_t ___windowID, const MethodInfo* method)
{
	typedef Rect_t225  (*GUILayoutUtility_Internal_GetWindowRect_m5170_ftn) (int32_t);
	static GUILayoutUtility_Internal_GetWindowRect_m5170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_Internal_GetWindowRect_m5170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::Internal_GetWindowRect(System.Int32)");
	return _il2cpp_icall_func(___windowID);
}
// System.Void UnityEngine.GUILayoutUtility::Internal_MoveWindow(System.Int32,UnityEngine.Rect)
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern "C" void GUILayoutUtility_Internal_MoveWindow_m5171 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t225  ___r, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___windowID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5172(NULL /*static, unused*/, L_0, (&___r), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5172 (Object_t * __this /* static, unused */, int32_t ___windowID, Rect_t225 * ___r, const MethodInfo* method)
{
	typedef void (*GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5172_ftn) (int32_t, Rect_t225 *);
	static GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)");
	_il2cpp_icall_func(___windowID, ___r);
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::GetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern "C" Rect_t225  GUILayoutUtility_GetRect_m5173 (Object_t * __this /* static, unused */, GUIContent_t807 * ___content, GUIStyle_t273 * ___style, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIContent_t807 * L_0 = ___content;
		GUIStyle_t273 * L_1 = ___style;
		GUILayoutOptionU5BU5D_t544* L_2 = ___options;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		Rect_t225  L_3 = GUILayoutUtility_DoGetRect_m5174(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect UnityEngine.GUILayoutUtility::DoGetRect(UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* GUIWordWrapSizer_t880_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t877_il2cpp_TypeInfo_var;
extern "C" Rect_t225  GUILayoutUtility_DoGetRect_m5174 (Object_t * __this /* static, unused */, GUIContent_t807 * ___content, GUIStyle_t273 * ___style, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		GUIWordWrapSizer_t880_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(642);
		GUILayoutEntry_t877_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	int32_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		GUIUtility_CheckOnGUI_m5219(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t481 * L_0 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = Event_get_type_m2786(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)12))))
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0091;
	}

IL_0024:
	{
		GUIStyle_t273 * L_4 = ___style;
		NullCheck(L_4);
		bool L_5 = GUIStyle_get_isHeightDependantOnWidth_m5336(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_6 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_6);
		GUILayoutGroup_t873 * L_7 = (L_6->___topLevel_0);
		GUIStyle_t273 * L_8 = ___style;
		GUIContent_t807 * L_9 = ___content;
		GUILayoutOptionU5BU5D_t544* L_10 = ___options;
		GUIWordWrapSizer_t880 * L_11 = (GUIWordWrapSizer_t880 *)il2cpp_codegen_object_new (GUIWordWrapSizer_t880_il2cpp_TypeInfo_var);
		GUIWordWrapSizer__ctor_m5206(L_11, L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		GUILayoutGroup_Add_m5195(L_7, L_11, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_004b:
	{
		GUIStyle_t273 * L_12 = ___style;
		GUIContent_t807 * L_13 = ___content;
		NullCheck(L_12);
		Vector2_t7  L_14 = GUIStyle_CalcSize_m5332(L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_15 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_15);
		GUILayoutGroup_t873 * L_16 = (L_15->___topLevel_0);
		float L_17 = ((&V_0)->___x_1);
		float L_18 = ((&V_0)->___x_1);
		float L_19 = ((&V_0)->___y_2);
		float L_20 = ((&V_0)->___y_2);
		GUIStyle_t273 * L_21 = ___style;
		GUILayoutOptionU5BU5D_t544* L_22 = ___options;
		GUILayoutEntry_t877 * L_23 = (GUILayoutEntry_t877 *)il2cpp_codegen_object_new (GUILayoutEntry_t877_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m5177(L_23, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		GUILayoutGroup_Add_m5195(L_16, L_23, /*hidden argument*/NULL);
	}

IL_0085:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		Rect_t225  L_24 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_24;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		Rect_t225  L_25 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_3;
		return L_25;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		LayoutCache_t874 * L_26 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___current_2;
		NullCheck(L_26);
		GUILayoutGroup_t873 * L_27 = (L_26->___topLevel_0);
		NullCheck(L_27);
		GUILayoutEntry_t877 * L_28 = GUILayoutGroup_GetNext_m5194(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Rect_t225  L_29 = (L_28->___rect_4);
		return L_29;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutUtility::get_spaceStyle()
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t273 * GUILayoutUtility_get_spaceStyle_m5175 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_0 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t273 * L_1 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_2 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		NullCheck(L_2);
		GUIStyle_set_stretchWidth_m5322(L_2, 0, /*hidden argument*/NULL);
		GUIStyle_t273 * L_3 = ((GUILayoutUtility_t876_StaticFields*)GUILayoutUtility_t876_il2cpp_TypeInfo_var->static_fields)->___s_SpaceStyle_4;
		return L_3;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"


// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m5176 (GUILayoutEntry_t877 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t273 * ____style, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t225  L_0 = {0};
		Rect__ctor_m2802(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_1 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t273 * L_6 = ____style;
		if (L_6)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_7 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		____style = L_7;
	}

IL_005b:
	{
		GUIStyle_t273 * L_8 = ____style;
		GUILayoutEntry_set_style_m5180(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__ctor_m5177 (GUILayoutEntry_t877 * __this, float ____minWidth, float ____maxWidth, float ____minHeight, float ____maxHeight, GUIStyle_t273 * ____style, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t225  L_0 = {0};
		Rect__ctor_m2802(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rect_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_1 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_Style_7 = L_1;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		float L_2 = ____minWidth;
		__this->___minWidth_0 = L_2;
		float L_3 = ____maxWidth;
		__this->___maxWidth_1 = L_3;
		float L_4 = ____minHeight;
		__this->___minHeight_2 = L_4;
		float L_5 = ____maxHeight;
		__this->___maxHeight_3 = L_5;
		GUIStyle_t273 * L_6 = ____style;
		GUILayoutEntry_set_style_m5180(__this, L_6, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t544* L_7 = ___options;
		VirtActionInvoker1< GUILayoutOptionU5BU5D_t544* >::Invoke(10 /* System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[]) */, __this, L_7);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::.cctor()
extern TypeInfo* GUILayoutEntry_t877_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry__cctor_m5178 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t877_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t225  L_0 = {0};
		Rect__ctor_m2802(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___kDummyRect_8 = L_0;
		((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___indent_9 = 0;
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUILayoutEntry::get_style()
extern "C" GUIStyle_t273 * GUILayoutEntry_get_style_m5179 (GUILayoutEntry_t877 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_Style_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutEntry::set_style(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_set_style_m5180 (GUILayoutEntry_t877 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_Style_7 = L_0;
		GUIStyle_t273 * L_1 = ___value;
		VirtActionInvoker1< GUIStyle_t273 * >::Invoke(9 /* System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle) */, __this, L_1);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin()
extern "C" RectOffset_t741 * GUILayoutEntry_get_margin_m5181 (GUILayoutEntry_t877 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectOffset_t741 * L_1 = GUIStyle_get_margin_m5314(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcWidth()
extern "C" void GUILayoutEntry_CalcWidth_m5182 (GUILayoutEntry_t877 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::CalcHeight()
extern "C" void GUILayoutEntry_CalcHeight_m5183 (GUILayoutEntry_t877 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetHorizontal_m5184 (GUILayoutEntry_t877 * __this, float ___x, float ___width, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___rect_4);
		float L_1 = ___x;
		Rect_set_x_m2862(L_0, L_1, /*hidden argument*/NULL);
		Rect_t225 * L_2 = &(__this->___rect_4);
		float L_3 = ___width;
		Rect_set_width_m2863(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single)
extern "C" void GUILayoutEntry_SetVertical_m5185 (GUILayoutEntry_t877 * __this, float ___y, float ___height, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___rect_4);
		float L_1 = ___y;
		Rect_set_y_m2866(L_0, L_1, /*hidden argument*/NULL);
		Rect_t225 * L_2 = &(__this->___rect_4);
		float L_3 = ___height;
		Rect_set_height_m2864(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutEntry_ApplyStyleSettings_m5186 (GUILayoutEntry_t877 * __this, GUIStyle_t273 * ___style, const MethodInfo* method)
{
	GUILayoutEntry_t877 * G_B3_0 = {0};
	GUILayoutEntry_t877 * G_B1_0 = {0};
	GUILayoutEntry_t877 * G_B2_0 = {0};
	int32_t G_B4_0 = 0;
	GUILayoutEntry_t877 * G_B4_1 = {0};
	GUILayoutEntry_t877 * G_B7_0 = {0};
	GUILayoutEntry_t877 * G_B5_0 = {0};
	GUILayoutEntry_t877 * G_B6_0 = {0};
	int32_t G_B8_0 = 0;
	GUILayoutEntry_t877 * G_B8_1 = {0};
	{
		GUIStyle_t273 * L_0 = ___style;
		NullCheck(L_0);
		float L_1 = GUIStyle_get_fixedWidth_m5319(L_0, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			G_B3_0 = __this;
			goto IL_0022;
		}
	}
	{
		GUIStyle_t273 * L_2 = ___style;
		NullCheck(L_2);
		bool L_3 = GUIStyle_get_stretchWidth_m5321(L_2, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
		if (!L_3)
		{
			G_B3_0 = G_B1_0;
			goto IL_0022;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0023:
	{
		NullCheck(G_B4_1);
		G_B4_1->___stretchWidth_5 = G_B4_0;
		GUIStyle_t273 * L_4 = ___style;
		NullCheck(L_4);
		float L_5 = GUIStyle_get_fixedHeight_m5320(L_4, /*hidden argument*/NULL);
		G_B5_0 = __this;
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			G_B7_0 = __this;
			goto IL_004a;
		}
	}
	{
		GUIStyle_t273 * L_6 = ___style;
		NullCheck(L_6);
		bool L_7 = GUIStyle_get_stretchHeight_m5323(L_6, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
		if (!L_7)
		{
			G_B7_0 = G_B5_0;
			goto IL_004a;
		}
	}
	{
		G_B8_0 = 1;
		G_B8_1 = G_B6_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B8_0 = 0;
		G_B8_1 = G_B7_0;
	}

IL_004b:
	{
		NullCheck(G_B8_1);
		G_B8_1->___stretchHeight_6 = G_B8_0;
		GUIStyle_t273 * L_8 = ___style;
		__this->___m_Style_7 = L_8;
		return;
	}
}
// System.Void UnityEngine.GUILayoutEntry::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern "C" void GUILayoutEntry_ApplyOptions_m5187 (GUILayoutEntry_t877 * __this, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t882 * V_0 = {0};
	GUILayoutOptionU5BU5D_t544* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	float V_4 = 0.0f;
	{
		GUILayoutOptionU5BU5D_t544* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t544* L_1 = ___options;
		V_1 = L_1;
		V_2 = 0;
		goto IL_01a0;
	}

IL_0010:
	{
		GUILayoutOptionU5BU5D_t544* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(GUILayoutOption_t882 **)(GUILayoutOption_t882 **)SZArrayLdElema(L_2, L_4));
		GUILayoutOption_t882 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___type_0);
		V_3 = L_6;
		int32_t L_7 = V_3;
		if (L_7 == 0)
		{
			goto IL_0046;
		}
		if (L_7 == 1)
		{
			goto IL_006e;
		}
		if (L_7 == 2)
		{
			goto IL_0096;
		}
		if (L_7 == 3)
		{
			goto IL_00c9;
		}
		if (L_7 == 4)
		{
			goto IL_0103;
		}
		if (L_7 == 5)
		{
			goto IL_0136;
		}
		if (L_7 == 6)
		{
			goto IL_0170;
		}
		if (L_7 == 7)
		{
			goto IL_0186;
		}
	}
	{
		goto IL_019c;
	}

IL_0046:
	{
		GUILayoutOption_t882 * L_8 = V_0;
		NullCheck(L_8);
		Object_t * L_9 = (L_8->___value_1);
		float L_10 = ((*(float*)((float*)UnBox (L_9, Single_t531_il2cpp_TypeInfo_var))));
		V_4 = L_10;
		__this->___maxWidth_1 = L_10;
		float L_11 = V_4;
		__this->___minWidth_0 = L_11;
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_006e:
	{
		GUILayoutOption_t882 * L_12 = V_0;
		NullCheck(L_12);
		Object_t * L_13 = (L_12->___value_1);
		float L_14 = ((*(float*)((float*)UnBox (L_13, Single_t531_il2cpp_TypeInfo_var))));
		V_4 = L_14;
		__this->___maxHeight_3 = L_14;
		float L_15 = V_4;
		__this->___minHeight_2 = L_15;
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0096:
	{
		GUILayoutOption_t882 * L_16 = V_0;
		NullCheck(L_16);
		Object_t * L_17 = (L_16->___value_1);
		__this->___minWidth_0 = ((*(float*)((float*)UnBox (L_17, Single_t531_il2cpp_TypeInfo_var))));
		float L_18 = (__this->___maxWidth_1);
		float L_19 = (__this->___minWidth_0);
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_00c4;
		}
	}
	{
		float L_20 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_20;
	}

IL_00c4:
	{
		goto IL_019c;
	}

IL_00c9:
	{
		GUILayoutOption_t882 * L_21 = V_0;
		NullCheck(L_21);
		Object_t * L_22 = (L_21->___value_1);
		__this->___maxWidth_1 = ((*(float*)((float*)UnBox (L_22, Single_t531_il2cpp_TypeInfo_var))));
		float L_23 = (__this->___minWidth_0);
		float L_24 = (__this->___maxWidth_1);
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_00f7;
		}
	}
	{
		float L_25 = (__this->___maxWidth_1);
		__this->___minWidth_0 = L_25;
	}

IL_00f7:
	{
		__this->___stretchWidth_5 = 0;
		goto IL_019c;
	}

IL_0103:
	{
		GUILayoutOption_t882 * L_26 = V_0;
		NullCheck(L_26);
		Object_t * L_27 = (L_26->___value_1);
		__this->___minHeight_2 = ((*(float*)((float*)UnBox (L_27, Single_t531_il2cpp_TypeInfo_var))));
		float L_28 = (__this->___maxHeight_3);
		float L_29 = (__this->___minHeight_2);
		if ((!(((float)L_28) < ((float)L_29))))
		{
			goto IL_0131;
		}
	}
	{
		float L_30 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_30;
	}

IL_0131:
	{
		goto IL_019c;
	}

IL_0136:
	{
		GUILayoutOption_t882 * L_31 = V_0;
		NullCheck(L_31);
		Object_t * L_32 = (L_31->___value_1);
		__this->___maxHeight_3 = ((*(float*)((float*)UnBox (L_32, Single_t531_il2cpp_TypeInfo_var))));
		float L_33 = (__this->___minHeight_2);
		float L_34 = (__this->___maxHeight_3);
		if ((!(((float)L_33) > ((float)L_34))))
		{
			goto IL_0164;
		}
	}
	{
		float L_35 = (__this->___maxHeight_3);
		__this->___minHeight_2 = L_35;
	}

IL_0164:
	{
		__this->___stretchHeight_6 = 0;
		goto IL_019c;
	}

IL_0170:
	{
		GUILayoutOption_t882 * L_36 = V_0;
		NullCheck(L_36);
		Object_t * L_37 = (L_36->___value_1);
		__this->___stretchWidth_5 = ((*(int32_t*)((int32_t*)UnBox (L_37, Int32_t478_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_0186:
	{
		GUILayoutOption_t882 * L_38 = V_0;
		NullCheck(L_38);
		Object_t * L_39 = (L_38->___value_1);
		__this->___stretchHeight_6 = ((*(int32_t*)((int32_t*)UnBox (L_39, Int32_t478_il2cpp_TypeInfo_var))));
		goto IL_019c;
	}

IL_019c:
	{
		int32_t L_40 = V_2;
		V_2 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_41 = V_2;
		GUILayoutOptionU5BU5D_t544* L_42 = V_1;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_0010;
		}
	}
	{
		float L_43 = (__this->___maxWidth_1);
		if ((((float)L_43) == ((float)(0.0f))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_44 = (__this->___maxWidth_1);
		float L_45 = (__this->___minWidth_0);
		if ((!(((float)L_44) < ((float)L_45))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_46 = (__this->___minWidth_0);
		__this->___maxWidth_1 = L_46;
	}

IL_01d6:
	{
		float L_47 = (__this->___maxHeight_3);
		if ((((float)L_47) == ((float)(0.0f))))
		{
			goto IL_0203;
		}
	}
	{
		float L_48 = (__this->___maxHeight_3);
		float L_49 = (__this->___minHeight_2);
		if ((!(((float)L_48) < ((float)L_49))))
		{
			goto IL_0203;
		}
	}
	{
		float L_50 = (__this->___minHeight_2);
		__this->___maxHeight_3 = L_50;
	}

IL_0203:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutEntry::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t877_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral885;
extern Il2CppCodeGenString* _stringLiteral886;
extern Il2CppCodeGenString* _stringLiteral887;
extern Il2CppCodeGenString* _stringLiteral888;
extern Il2CppCodeGenString* _stringLiteral889;
extern Il2CppCodeGenString* _stringLiteral890;
extern Il2CppCodeGenString* _stringLiteral891;
extern "C" String_t* GUILayoutEntry_ToString_m5188 (GUILayoutEntry_t877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		GUILayoutEntry_t877_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral885 = il2cpp_codegen_string_literal_from_index(885);
		_stringLiteral886 = il2cpp_codegen_string_literal_from_index(886);
		_stringLiteral887 = il2cpp_codegen_string_literal_from_index(887);
		_stringLiteral888 = il2cpp_codegen_string_literal_from_index(888);
		_stringLiteral889 = il2cpp_codegen_string_literal_from_index(889);
		_stringLiteral890 = il2cpp_codegen_string_literal_from_index(890);
		_stringLiteral891 = il2cpp_codegen_string_literal_from_index(891);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t470* G_B5_1 = {0};
	ObjectU5BU5D_t470* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B5_4 = 0;
	ObjectU5BU5D_t470* G_B5_5 = {0};
	ObjectU5BU5D_t470* G_B5_6 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t470* G_B4_1 = {0};
	ObjectU5BU5D_t470* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B4_4 = 0;
	ObjectU5BU5D_t470* G_B4_5 = {0};
	ObjectU5BU5D_t470* G_B4_6 = {0};
	String_t* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t470* G_B6_2 = {0};
	ObjectU5BU5D_t470* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	int32_t G_B6_5 = 0;
	ObjectU5BU5D_t470* G_B6_6 = {0};
	ObjectU5BU5D_t470* G_B6_7 = {0};
	int32_t G_B8_0 = 0;
	ObjectU5BU5D_t470* G_B8_1 = {0};
	ObjectU5BU5D_t470* G_B8_2 = {0};
	int32_t G_B7_0 = 0;
	ObjectU5BU5D_t470* G_B7_1 = {0};
	ObjectU5BU5D_t470* G_B7_2 = {0};
	String_t* G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	ObjectU5BU5D_t470* G_B9_2 = {0};
	ObjectU5BU5D_t470* G_B9_3 = {0};
	int32_t G_B11_0 = 0;
	ObjectU5BU5D_t470* G_B11_1 = {0};
	ObjectU5BU5D_t470* G_B11_2 = {0};
	int32_t G_B10_0 = 0;
	ObjectU5BU5D_t470* G_B10_1 = {0};
	ObjectU5BU5D_t470* G_B10_2 = {0};
	String_t* G_B12_0 = {0};
	int32_t G_B12_1 = 0;
	ObjectU5BU5D_t470* G_B12_2 = {0};
	ObjectU5BU5D_t470* G_B12_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		goto IL_001d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3031(NULL /*static, unused*/, L_1, _stringLiteral885, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001d:
	{
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t877_il2cpp_TypeInfo_var);
		int32_t L_5 = ((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_000d;
		}
	}
	{
		ObjectU5BU5D_t470* L_6 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, ((int32_t)12)));
		String_t* L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 0)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_6;
		ObjectU5BU5D_t470* L_9 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 6));
		GUIStyle_t273 * L_10 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		G_B4_0 = 0;
		G_B4_1 = L_9;
		G_B4_2 = L_9;
		G_B4_3 = _stringLiteral886;
		G_B4_4 = 1;
		G_B4_5 = L_8;
		G_B4_6 = L_8;
		if (!L_10)
		{
			G_B5_0 = 0;
			G_B5_1 = L_9;
			G_B5_2 = L_9;
			G_B5_3 = _stringLiteral886;
			G_B5_4 = 1;
			G_B5_5 = L_8;
			G_B5_6 = L_8;
			goto IL_005d;
		}
	}
	{
		GUIStyle_t273 * L_11 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = GUIStyle_get_name_m5311(L_11, /*hidden argument*/NULL);
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		G_B6_5 = G_B4_4;
		G_B6_6 = G_B4_5;
		G_B6_7 = G_B4_6;
		goto IL_0062;
	}

IL_005d:
	{
		G_B6_0 = _stringLiteral887;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
		G_B6_5 = G_B5_4;
		G_B6_6 = G_B5_5;
		G_B6_7 = G_B5_6;
	}

IL_0062:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1)) = (Object_t *)G_B6_0;
		ObjectU5BU5D_t470* L_13 = G_B6_3;
		Type_t * L_14 = Object_GetType_m3299(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		ArrayElementTypeCheck (L_13, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 1)) = (Object_t *)L_14;
		ObjectU5BU5D_t470* L_15 = L_13;
		Rect_t225 * L_16 = &(__this->___rect_4);
		float L_17 = Rect_get_x_m2861(L_16, /*hidden argument*/NULL);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t531_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2)) = (Object_t *)L_19;
		ObjectU5BU5D_t470* L_20 = L_15;
		Rect_t225 * L_21 = &(__this->___rect_4);
		float L_22 = Rect_get_xMax_m3060(L_21, /*hidden argument*/NULL);
		float L_23 = L_22;
		Object_t * L_24 = Box(Single_t531_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 3)) = (Object_t *)L_24;
		ObjectU5BU5D_t470* L_25 = L_20;
		Rect_t225 * L_26 = &(__this->___rect_4);
		float L_27 = Rect_get_y_m2865(L_26, /*hidden argument*/NULL);
		float L_28 = L_27;
		Object_t * L_29 = Box(Single_t531_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4)) = (Object_t *)L_29;
		ObjectU5BU5D_t470* L_30 = L_25;
		Rect_t225 * L_31 = &(__this->___rect_4);
		float L_32 = Rect_get_yMax_m4751(L_31, /*hidden argument*/NULL);
		float L_33 = L_32;
		Object_t * L_34 = Box(Single_t531_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 5);
		ArrayElementTypeCheck (L_30, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 5)) = (Object_t *)L_34;
		String_t* L_35 = UnityString_Format_m5568(NULL /*static, unused*/, G_B6_4, L_30, /*hidden argument*/NULL);
		NullCheck(G_B6_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_6, G_B6_5);
		ArrayElementTypeCheck (G_B6_6, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_6, G_B6_5)) = (Object_t *)L_35;
		ObjectU5BU5D_t470* L_36 = G_B6_7;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		ArrayElementTypeCheck (L_36, _stringLiteral888);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, 2)) = (Object_t *)_stringLiteral888;
		ObjectU5BU5D_t470* L_37 = L_36;
		float L_38 = (__this->___minWidth_0);
		float L_39 = L_38;
		Object_t * L_40 = Box(Single_t531_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 3);
		ArrayElementTypeCheck (L_37, L_40);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_37, 3)) = (Object_t *)L_40;
		ObjectU5BU5D_t470* L_41 = L_37;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 4);
		ArrayElementTypeCheck (L_41, _stringLiteral889);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_41, 4)) = (Object_t *)_stringLiteral889;
		ObjectU5BU5D_t470* L_42 = L_41;
		float L_43 = (__this->___maxWidth_1);
		float L_44 = L_43;
		Object_t * L_45 = Box(Single_t531_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 5);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 5)) = (Object_t *)L_45;
		ObjectU5BU5D_t470* L_46 = L_42;
		int32_t L_47 = (__this->___stretchWidth_5);
		G_B7_0 = 6;
		G_B7_1 = L_46;
		G_B7_2 = L_46;
		if (!L_47)
		{
			G_B8_0 = 6;
			G_B8_1 = L_46;
			G_B8_2 = L_46;
			goto IL_0101;
		}
	}
	{
		G_B9_0 = _stringLiteral890;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0106;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B9_0 = L_48;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0106:
	{
		NullCheck(G_B9_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B9_2, G_B9_1);
		ArrayElementTypeCheck (G_B9_2, G_B9_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B9_2, G_B9_1)) = (Object_t *)G_B9_0;
		ObjectU5BU5D_t470* L_49 = G_B9_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, 7);
		ArrayElementTypeCheck (L_49, _stringLiteral891);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_49, 7)) = (Object_t *)_stringLiteral891;
		ObjectU5BU5D_t470* L_50 = L_49;
		float L_51 = (__this->___minHeight_2);
		float L_52 = L_51;
		Object_t * L_53 = Box(Single_t531_il2cpp_TypeInfo_var, &L_52);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 8);
		ArrayElementTypeCheck (L_50, L_53);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, 8)) = (Object_t *)L_53;
		ObjectU5BU5D_t470* L_54 = L_50;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)9));
		ArrayElementTypeCheck (L_54, _stringLiteral889);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_54, ((int32_t)9))) = (Object_t *)_stringLiteral889;
		ObjectU5BU5D_t470* L_55 = L_54;
		float L_56 = (__this->___maxHeight_3);
		float L_57 = L_56;
		Object_t * L_58 = Box(Single_t531_il2cpp_TypeInfo_var, &L_57);
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)10));
		ArrayElementTypeCheck (L_55, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, ((int32_t)10))) = (Object_t *)L_58;
		ObjectU5BU5D_t470* L_59 = L_55;
		int32_t L_60 = (__this->___stretchHeight_6);
		G_B10_0 = ((int32_t)11);
		G_B10_1 = L_59;
		G_B10_2 = L_59;
		if (!L_60)
		{
			G_B11_0 = ((int32_t)11);
			G_B11_1 = L_59;
			G_B11_2 = L_59;
			goto IL_014d;
		}
	}
	{
		G_B12_0 = _stringLiteral890;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_0152;
	}

IL_014d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_61 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B12_0 = L_61;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_0152:
	{
		NullCheck(G_B12_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B12_2, G_B12_1);
		ArrayElementTypeCheck (G_B12_2, G_B12_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B12_2, G_B12_1)) = (Object_t *)G_B12_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m3056(NULL /*static, unused*/, G_B12_3, /*hidden argument*/NULL);
		return L_62;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"


// System.Void UnityEngine.GUILayoutGroup::.ctor()
extern TypeInfo* List_1_t878_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffset_t741_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t877_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m6366_MethodInfo_var;
extern "C" void GUILayoutGroup__ctor_m5189 (GUILayoutGroup_t873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t878_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(643);
		RectOffset_t741_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(582);
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		GUILayoutEntry_t877_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		List_1__ctor_m6366_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484135);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t878 * L_0 = (List_1_t878 *)il2cpp_codegen_object_new (List_1_t878_il2cpp_TypeInfo_var);
		List_1__ctor_m6366(L_0, /*hidden argument*/List_1__ctor_m6366_MethodInfo_var);
		__this->___entries_10 = L_0;
		__this->___isVertical_11 = 1;
		__this->___sameSize_14 = 1;
		__this->___windowID_16 = (-1);
		__this->___stretchableCountX_18 = ((int32_t)100);
		__this->___stretchableCountY_19 = ((int32_t)100);
		__this->___childMinWidth_22 = (100.0f);
		__this->___childMaxWidth_23 = (100.0f);
		__this->___childMinHeight_24 = (100.0f);
		__this->___childMaxHeight_25 = (100.0f);
		RectOffset_t741 * L_1 = (RectOffset_t741 *)il2cpp_codegen_object_new (RectOffset_t741_il2cpp_TypeInfo_var);
		RectOffset__ctor_m4944(L_1, /*hidden argument*/NULL);
		__this->___m_Margin_26 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_2 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t877_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m5176(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin()
extern "C" RectOffset_t741 * GUILayoutGroup_get_margin_m5190 (GUILayoutGroup_t873 * __this, const MethodInfo* method)
{
	{
		RectOffset_t741 * L_0 = (__this->___m_Margin_26);
		return L_0;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[])
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern "C" void GUILayoutGroup_ApplyOptions_m5191 (GUILayoutGroup_t873 * __this, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutOption_t882 * V_0 = {0};
	GUILayoutOptionU5BU5D_t544* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	{
		GUILayoutOptionU5BU5D_t544* L_0 = ___options;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		return;
	}

IL_0007:
	{
		GUILayoutOptionU5BU5D_t544* L_1 = ___options;
		GUILayoutEntry_ApplyOptions_m5187(__this, L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t544* L_2 = ___options;
		V_1 = L_2;
		V_2 = 0;
		goto IL_0098;
	}

IL_0017:
	{
		GUILayoutOptionU5BU5D_t544* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_0 = (*(GUILayoutOption_t882 **)(GUILayoutOption_t882 **)SZArrayLdElema(L_3, L_5));
		GUILayoutOption_t882 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___type_0);
		V_3 = L_7;
		int32_t L_8 = V_3;
		if (L_8 == 0)
		{
			goto IL_0065;
		}
		if (L_8 == 1)
		{
			goto IL_0071;
		}
		if (L_8 == 2)
		{
			goto IL_0065;
		}
		if (L_8 == 3)
		{
			goto IL_0065;
		}
		if (L_8 == 4)
		{
			goto IL_0071;
		}
		if (L_8 == 5)
		{
			goto IL_0071;
		}
		if (L_8 == 6)
		{
			goto IL_0094;
		}
		if (L_8 == 7)
		{
			goto IL_0094;
		}
		if (L_8 == 8)
		{
			goto IL_0094;
		}
		if (L_8 == 9)
		{
			goto IL_0094;
		}
		if (L_8 == 10)
		{
			goto IL_0094;
		}
		if (L_8 == 11)
		{
			goto IL_0094;
		}
		if (L_8 == 12)
		{
			goto IL_0094;
		}
		if (L_8 == 13)
		{
			goto IL_007d;
		}
	}
	{
		goto IL_0094;
	}

IL_0065:
	{
		__this->___userSpecifiedHeight_21 = 1;
		goto IL_0094;
	}

IL_0071:
	{
		__this->___userSpecifiedWidth_20 = 1;
		goto IL_0094;
	}

IL_007d:
	{
		GUILayoutOption_t882 * L_9 = V_0;
		NullCheck(L_9);
		Object_t * L_10 = (L_9->___value_1);
		__this->___spacing_13 = (((float)((*(int32_t*)((int32_t*)UnBox (L_10, Int32_t478_il2cpp_TypeInfo_var))))));
		goto IL_0094;
	}

IL_0094:
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0098:
	{
		int32_t L_12 = V_2;
		GUILayoutOptionU5BU5D_t544* L_13 = V_1;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)(((Array_t *)L_13)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ApplyStyleSettings(UnityEngine.GUIStyle)
extern "C" void GUILayoutGroup_ApplyStyleSettings_m5192 (GUILayoutGroup_t873 * __this, GUIStyle_t273 * ___style, const MethodInfo* method)
{
	RectOffset_t741 * V_0 = {0};
	{
		GUIStyle_t273 * L_0 = ___style;
		GUILayoutEntry_ApplyStyleSettings_m5186(__this, L_0, /*hidden argument*/NULL);
		GUIStyle_t273 * L_1 = ___style;
		NullCheck(L_1);
		RectOffset_t741 * L_2 = GUIStyle_get_margin_m5314(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		RectOffset_t741 * L_3 = (__this->___m_Margin_26);
		RectOffset_t741 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_left_m4939(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_set_left_m5298(L_3, L_5, /*hidden argument*/NULL);
		RectOffset_t741 * L_6 = (__this->___m_Margin_26);
		RectOffset_t741 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = RectOffset_get_right_m5299(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectOffset_set_right_m5300(L_6, L_8, /*hidden argument*/NULL);
		RectOffset_t741 * L_9 = (__this->___m_Margin_26);
		RectOffset_t741 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m4940(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		RectOffset_set_top_m5301(L_9, L_11, /*hidden argument*/NULL);
		RectOffset_t741 * L_12 = (__this->___m_Margin_26);
		RectOffset_t741 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = RectOffset_get_bottom_m5302(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		RectOffset_set_bottom_m5303(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::ResetCursor()
extern "C" void GUILayoutGroup_ResetCursor_m5193 (GUILayoutGroup_t873 * __this, const MethodInfo* method)
{
	{
		__this->___cursor_17 = 0;
		return;
	}
}
// UnityEngine.GUILayoutEntry UnityEngine.GUILayoutGroup::GetNext()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t893_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral892;
extern Il2CppCodeGenString* _stringLiteral893;
extern Il2CppCodeGenString* _stringLiteral894;
extern Il2CppCodeGenString* _stringLiteral895;
extern "C" GUILayoutEntry_t877 * GUILayoutGroup_GetNext_m5194 (GUILayoutGroup_t873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		EventType_t893_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		_stringLiteral892 = il2cpp_codegen_string_literal_from_index(892);
		_stringLiteral893 = il2cpp_codegen_string_literal_from_index(893);
		_stringLiteral894 = il2cpp_codegen_string_literal_from_index(894);
		_stringLiteral895 = il2cpp_codegen_string_literal_from_index(895);
		s_Il2CppMethodIntialized = true;
	}
	GUILayoutEntry_t877 * V_0 = {0};
	{
		int32_t L_0 = (__this->___cursor_17);
		List_1_t878 * L_1 = (__this->___entries_10);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_1);
		if ((((int32_t)L_0) >= ((int32_t)L_2)))
		{
			goto IL_0038;
		}
	}
	{
		List_1_t878 * L_3 = (__this->___entries_10);
		int32_t L_4 = (__this->___cursor_17);
		NullCheck(L_3);
		GUILayoutEntry_t877 * L_5 = (GUILayoutEntry_t877 *)VirtFuncInvoker1< GUILayoutEntry_t877 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_3, L_4);
		V_0 = L_5;
		int32_t L_6 = (__this->___cursor_17);
		__this->___cursor_17 = ((int32_t)((int32_t)L_6+(int32_t)1));
		GUILayoutEntry_t877 * L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		ObjectU5BU5D_t470* L_8 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 7));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, _stringLiteral892);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0)) = (Object_t *)_stringLiteral892;
		ObjectU5BU5D_t470* L_9 = L_8;
		int32_t L_10 = (__this->___cursor_17);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 1)) = (Object_t *)L_12;
		ObjectU5BU5D_t470* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, _stringLiteral893);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 2)) = (Object_t *)_stringLiteral893;
		ObjectU5BU5D_t470* L_14 = L_13;
		List_1_t878 * L_15 = (__this->___entries_10);
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_15);
		int32_t L_17 = L_16;
		Object_t * L_18 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 3)) = (Object_t *)L_18;
		ObjectU5BU5D_t470* L_19 = L_14;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, _stringLiteral894);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 4)) = (Object_t *)_stringLiteral894;
		ObjectU5BU5D_t470* L_20 = L_19;
		Event_t481 * L_21 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = Event_get_rawType_m4803(L_21, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(EventType_t893_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5)) = (Object_t *)L_24;
		ObjectU5BU5D_t470* L_25 = L_20;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 6);
		ArrayElementTypeCheck (L_25, _stringLiteral895);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 6)) = (Object_t *)_stringLiteral895;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m3056(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		ArgumentException_t556 * L_27 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_27, L_26, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_27);
	}
}
// System.Void UnityEngine.GUILayoutGroup::Add(UnityEngine.GUILayoutEntry)
extern "C" void GUILayoutGroup_Add_m5195 (GUILayoutGroup_t873 * __this, GUILayoutEntry_t877 * ___e, const MethodInfo* method)
{
	{
		List_1_t878 * L_0 = (__this->___entries_10);
		GUILayoutEntry_t877 * L_1 = ___e;
		NullCheck(L_0);
		VirtActionInvoker1< GUILayoutEntry_t877 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcWidth()
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1112_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m6363_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6364_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6365_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcWidth_m5196 (GUILayoutGroup_t873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Enumerator_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		List_1_GetEnumerator_m6363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		Enumerator_get_Current_m6364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		Enumerator_MoveNext_m6365_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	GUILayoutEntry_t877 * V_3 = {0};
	Enumerator_t1112  V_4 = {0};
	RectOffset_t741 * V_5 = {0};
	int32_t V_6 = 0;
	GUILayoutEntry_t877 * V_7 = {0};
	Enumerator_t1112  V_8 = {0};
	RectOffset_t741 * V_9 = {0};
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	int32_t G_B39_0 = 0;
	int32_t G_B39_1 = 0;
	GUILayoutGroup_t873 * G_B39_2 = {0};
	int32_t G_B38_0 = 0;
	int32_t G_B38_1 = 0;
	GUILayoutGroup_t873 * G_B38_2 = {0};
	int32_t G_B40_0 = 0;
	int32_t G_B40_1 = 0;
	int32_t G_B40_2 = 0;
	GUILayoutGroup_t873 * G_B40_3 = {0};
	{
		List_1_t878 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t273 * L_2 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t741 * L_3 = GUIStyle_get_padding_m5315(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_horizontal_m4934(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)L_4));
		V_13 = L_5;
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = L_5;
		float L_6 = V_13;
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = L_6;
		return;
	}

IL_0033:
	{
		__this->___childMinWidth_22 = (0.0f);
		__this->___childMaxWidth_23 = (0.0f);
		V_0 = 0;
		V_1 = 0;
		__this->___stretchableCountX_18 = 0;
		V_2 = 1;
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_016a;
		}
	}
	{
		List_1_t878 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1112  L_9 = List_1_GetEnumerator_m6363(L_8, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_4 = L_9;
	}

IL_006e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0125;
		}

IL_0073:
		{
			GUILayoutEntry_t877 * L_10 = Enumerator_get_Current_m6364((&V_4), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_3 = L_10;
			GUILayoutEntry_t877 * L_11 = V_3;
			NullCheck(L_11);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_11);
			GUILayoutEntry_t877 * L_12 = V_3;
			NullCheck(L_12);
			RectOffset_t741 * L_13 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_12);
			V_5 = L_13;
			GUILayoutEntry_t877 * L_14 = V_3;
			NullCheck(L_14);
			GUIStyle_t273 * L_15 = GUILayoutEntry_get_style_m5179(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUIStyle_t273 * L_16 = GUILayoutUtility_get_spaceStyle_m5175(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t273 *)L_15) == ((Object_t*)(GUIStyle_t273 *)L_16)))
			{
				goto IL_0112;
			}
		}

IL_0099:
		{
			bool L_17 = V_2;
			if (L_17)
			{
				goto IL_00c0;
			}
		}

IL_009f:
		{
			RectOffset_t741 * L_18 = V_5;
			NullCheck(L_18);
			int32_t L_19 = RectOffset_get_left_m4939(L_18, /*hidden argument*/NULL);
			int32_t L_20 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			int32_t L_21 = Mathf_Min_m3050(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
			V_0 = L_21;
			RectOffset_t741 * L_22 = V_5;
			NullCheck(L_22);
			int32_t L_23 = RectOffset_get_right_m5299(L_22, /*hidden argument*/NULL);
			int32_t L_24 = V_1;
			int32_t L_25 = Mathf_Min_m3050(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
			V_1 = L_25;
			goto IL_00d2;
		}

IL_00c0:
		{
			RectOffset_t741 * L_26 = V_5;
			NullCheck(L_26);
			int32_t L_27 = RectOffset_get_left_m4939(L_26, /*hidden argument*/NULL);
			V_0 = L_27;
			RectOffset_t741 * L_28 = V_5;
			NullCheck(L_28);
			int32_t L_29 = RectOffset_get_right_m5299(L_28, /*hidden argument*/NULL);
			V_1 = L_29;
			V_2 = 0;
		}

IL_00d2:
		{
			GUILayoutEntry_t877 * L_30 = V_3;
			NullCheck(L_30);
			float L_31 = (L_30->___minWidth_0);
			RectOffset_t741 * L_32 = V_5;
			NullCheck(L_32);
			int32_t L_33 = RectOffset_get_horizontal_m4934(L_32, /*hidden argument*/NULL);
			float L_34 = (__this->___childMinWidth_22);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_35 = Mathf_Max_m2937(NULL /*static, unused*/, ((float)((float)L_31+(float)(((float)L_33)))), L_34, /*hidden argument*/NULL);
			__this->___childMinWidth_22 = L_35;
			GUILayoutEntry_t877 * L_36 = V_3;
			NullCheck(L_36);
			float L_37 = (L_36->___maxWidth_1);
			RectOffset_t741 * L_38 = V_5;
			NullCheck(L_38);
			int32_t L_39 = RectOffset_get_horizontal_m4934(L_38, /*hidden argument*/NULL);
			float L_40 = (__this->___childMaxWidth_23);
			float L_41 = Mathf_Max_m2937(NULL /*static, unused*/, ((float)((float)L_37+(float)(((float)L_39)))), L_40, /*hidden argument*/NULL);
			__this->___childMaxWidth_23 = L_41;
		}

IL_0112:
		{
			int32_t L_42 = (__this->___stretchableCountX_18);
			GUILayoutEntry_t877 * L_43 = V_3;
			NullCheck(L_43);
			int32_t L_44 = (L_43->___stretchWidth_5);
			__this->___stretchableCountX_18 = ((int32_t)((int32_t)L_42+(int32_t)L_44));
		}

IL_0125:
		{
			bool L_45 = Enumerator_MoveNext_m6365((&V_4), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_45)
			{
				goto IL_0073;
			}
		}

IL_0131:
		{
			IL2CPP_LEAVE(0x143, FINALLY_0136);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0136;
	}

FINALLY_0136:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_46 = V_4;
		Enumerator_t1112  L_47 = L_46;
		Object_t * L_48 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_48);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_48);
		IL2CPP_END_FINALLY(310)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(310)
	{
		IL2CPP_JUMP_TBL(0x143, IL_0143)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0143:
	{
		float L_49 = (__this->___childMinWidth_22);
		int32_t L_50 = V_0;
		int32_t L_51 = V_1;
		__this->___childMinWidth_22 = ((float)((float)L_49-(float)(((float)((int32_t)((int32_t)L_50+(int32_t)L_51))))));
		float L_52 = (__this->___childMaxWidth_23);
		int32_t L_53 = V_0;
		int32_t L_54 = V_1;
		__this->___childMaxWidth_23 = ((float)((float)L_52-(float)(((float)((int32_t)((int32_t)L_53+(int32_t)L_54))))));
		goto IL_02ea;
	}

IL_016a:
	{
		V_6 = 0;
		List_1_t878 * L_55 = (__this->___entries_10);
		NullCheck(L_55);
		Enumerator_t1112  L_56 = List_1_GetEnumerator_m6363(L_55, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_8 = L_56;
	}

IL_017a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0273;
		}

IL_017f:
		{
			GUILayoutEntry_t877 * L_57 = Enumerator_get_Current_m6364((&V_8), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_7 = L_57;
			GUILayoutEntry_t877 * L_58 = V_7;
			NullCheck(L_58);
			VirtActionInvoker0::Invoke(5 /* System.Void UnityEngine.GUILayoutEntry::CalcWidth() */, L_58);
			GUILayoutEntry_t877 * L_59 = V_7;
			NullCheck(L_59);
			RectOffset_t741 * L_60 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_59);
			V_9 = L_60;
			GUILayoutEntry_t877 * L_61 = V_7;
			NullCheck(L_61);
			GUIStyle_t273 * L_62 = GUILayoutEntry_get_style_m5179(L_61, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUIStyle_t273 * L_63 = GUILayoutUtility_get_spaceStyle_m5175(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t273 *)L_62) == ((Object_t*)(GUIStyle_t273 *)L_63)))
			{
				goto IL_0237;
			}
		}

IL_01a9:
		{
			bool L_64 = V_2;
			if (L_64)
			{
				goto IL_01d2;
			}
		}

IL_01af:
		{
			int32_t L_65 = V_6;
			RectOffset_t741 * L_66 = V_9;
			NullCheck(L_66);
			int32_t L_67 = RectOffset_get_left_m4939(L_66, /*hidden argument*/NULL);
			if ((((int32_t)L_65) <= ((int32_t)L_67)))
			{
				goto IL_01c4;
			}
		}

IL_01bd:
		{
			int32_t L_68 = V_6;
			G_B22_0 = L_68;
			goto IL_01cb;
		}

IL_01c4:
		{
			RectOffset_t741 * L_69 = V_9;
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_left_m4939(L_69, /*hidden argument*/NULL);
			G_B22_0 = L_70;
		}

IL_01cb:
		{
			V_10 = G_B22_0;
			goto IL_01d7;
		}

IL_01d2:
		{
			V_10 = 0;
			V_2 = 0;
		}

IL_01d7:
		{
			float L_71 = (__this->___childMinWidth_22);
			GUILayoutEntry_t877 * L_72 = V_7;
			NullCheck(L_72);
			float L_73 = (L_72->___minWidth_0);
			float L_74 = (__this->___spacing_13);
			int32_t L_75 = V_10;
			__this->___childMinWidth_22 = ((float)((float)L_71+(float)((float)((float)((float)((float)L_73+(float)L_74))+(float)(((float)L_75))))));
			float L_76 = (__this->___childMaxWidth_23);
			GUILayoutEntry_t877 * L_77 = V_7;
			NullCheck(L_77);
			float L_78 = (L_77->___maxWidth_1);
			float L_79 = (__this->___spacing_13);
			int32_t L_80 = V_10;
			__this->___childMaxWidth_23 = ((float)((float)L_76+(float)((float)((float)((float)((float)L_78+(float)L_79))+(float)(((float)L_80))))));
			RectOffset_t741 * L_81 = V_9;
			NullCheck(L_81);
			int32_t L_82 = RectOffset_get_right_m5299(L_81, /*hidden argument*/NULL);
			V_6 = L_82;
			int32_t L_83 = (__this->___stretchableCountX_18);
			GUILayoutEntry_t877 * L_84 = V_7;
			NullCheck(L_84);
			int32_t L_85 = (L_84->___stretchWidth_5);
			__this->___stretchableCountX_18 = ((int32_t)((int32_t)L_83+(int32_t)L_85));
			goto IL_0273;
		}

IL_0237:
		{
			float L_86 = (__this->___childMinWidth_22);
			GUILayoutEntry_t877 * L_87 = V_7;
			NullCheck(L_87);
			float L_88 = (L_87->___minWidth_0);
			__this->___childMinWidth_22 = ((float)((float)L_86+(float)L_88));
			float L_89 = (__this->___childMaxWidth_23);
			GUILayoutEntry_t877 * L_90 = V_7;
			NullCheck(L_90);
			float L_91 = (L_90->___maxWidth_1);
			__this->___childMaxWidth_23 = ((float)((float)L_89+(float)L_91));
			int32_t L_92 = (__this->___stretchableCountX_18);
			GUILayoutEntry_t877 * L_93 = V_7;
			NullCheck(L_93);
			int32_t L_94 = (L_93->___stretchWidth_5);
			__this->___stretchableCountX_18 = ((int32_t)((int32_t)L_92+(int32_t)L_94));
		}

IL_0273:
		{
			bool L_95 = Enumerator_MoveNext_m6365((&V_8), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_95)
			{
				goto IL_017f;
			}
		}

IL_027f:
		{
			IL2CPP_LEAVE(0x291, FINALLY_0284);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0284;
	}

FINALLY_0284:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_96 = V_8;
		Enumerator_t1112  L_97 = L_96;
		Object_t * L_98 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_97);
		NullCheck(L_98);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_98);
		IL2CPP_END_FINALLY(644)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(644)
	{
		IL2CPP_JUMP_TBL(0x291, IL_0291)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0291:
	{
		float L_99 = (__this->___childMinWidth_22);
		float L_100 = (__this->___spacing_13);
		__this->___childMinWidth_22 = ((float)((float)L_99-(float)L_100));
		float L_101 = (__this->___childMaxWidth_23);
		float L_102 = (__this->___spacing_13);
		__this->___childMaxWidth_23 = ((float)((float)L_101-(float)L_102));
		List_1_t878 * L_103 = (__this->___entries_10);
		NullCheck(L_103);
		int32_t L_104 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_103);
		if (!L_104)
		{
			goto IL_02e6;
		}
	}
	{
		List_1_t878 * L_105 = (__this->___entries_10);
		NullCheck(L_105);
		GUILayoutEntry_t877 * L_106 = (GUILayoutEntry_t877 *)VirtFuncInvoker1< GUILayoutEntry_t877 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_105, 0);
		NullCheck(L_106);
		RectOffset_t741 * L_107 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_106);
		NullCheck(L_107);
		int32_t L_108 = RectOffset_get_left_m4939(L_107, /*hidden argument*/NULL);
		V_0 = L_108;
		int32_t L_109 = V_6;
		V_1 = L_109;
		goto IL_02ea;
	}

IL_02e6:
	{
		int32_t L_110 = 0;
		V_1 = L_110;
		V_0 = L_110;
	}

IL_02ea:
	{
		V_11 = (0.0f);
		V_12 = (0.0f);
		GUIStyle_t273 * L_111 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_112 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t273 *)L_111) == ((Object_t*)(GUIStyle_t273 *)L_112))))
		{
			goto IL_0313;
		}
	}
	{
		bool L_113 = (__this->___userSpecifiedWidth_20);
		if (!L_113)
		{
			goto IL_034a;
		}
	}

IL_0313:
	{
		GUIStyle_t273 * L_114 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_114);
		RectOffset_t741 * L_115 = GUIStyle_get_padding_m5315(L_114, /*hidden argument*/NULL);
		NullCheck(L_115);
		int32_t L_116 = RectOffset_get_left_m4939(L_115, /*hidden argument*/NULL);
		int32_t L_117 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		int32_t L_118 = Mathf_Max_m3049(NULL /*static, unused*/, L_116, L_117, /*hidden argument*/NULL);
		V_11 = (((float)L_118));
		GUIStyle_t273 * L_119 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_119);
		RectOffset_t741 * L_120 = GUIStyle_get_padding_m5315(L_119, /*hidden argument*/NULL);
		NullCheck(L_120);
		int32_t L_121 = RectOffset_get_right_m5299(L_120, /*hidden argument*/NULL);
		int32_t L_122 = V_1;
		int32_t L_123 = Mathf_Max_m3049(NULL /*static, unused*/, L_121, L_122, /*hidden argument*/NULL);
		V_12 = (((float)L_123));
		goto IL_036c;
	}

IL_034a:
	{
		RectOffset_t741 * L_124 = (__this->___m_Margin_26);
		int32_t L_125 = V_0;
		NullCheck(L_124);
		RectOffset_set_left_m5298(L_124, L_125, /*hidden argument*/NULL);
		RectOffset_t741 * L_126 = (__this->___m_Margin_26);
		int32_t L_127 = V_1;
		NullCheck(L_126);
		RectOffset_set_right_m5300(L_126, L_127, /*hidden argument*/NULL);
		float L_128 = (0.0f);
		V_12 = L_128;
		V_11 = L_128;
	}

IL_036c:
	{
		float L_129 = (((GUILayoutEntry_t877 *)__this)->___minWidth_0);
		float L_130 = (__this->___childMinWidth_22);
		float L_131 = V_11;
		float L_132 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_133 = Mathf_Max_m2937(NULL /*static, unused*/, L_129, ((float)((float)((float)((float)L_130+(float)L_131))+(float)L_132)), /*hidden argument*/NULL);
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = L_133;
		float L_134 = (((GUILayoutEntry_t877 *)__this)->___maxWidth_1);
		if ((!(((float)L_134) == ((float)(0.0f)))))
		{
			goto IL_03db;
		}
	}
	{
		int32_t L_135 = (((GUILayoutEntry_t877 *)__this)->___stretchWidth_5);
		int32_t L_136 = (__this->___stretchableCountX_18);
		GUIStyle_t273 * L_137 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_137);
		bool L_138 = GUIStyle_get_stretchWidth_m5321(L_137, /*hidden argument*/NULL);
		G_B38_0 = L_136;
		G_B38_1 = L_135;
		G_B38_2 = __this;
		if (!L_138)
		{
			G_B39_0 = L_136;
			G_B39_1 = L_135;
			G_B39_2 = __this;
			goto IL_03bc;
		}
	}
	{
		G_B40_0 = 1;
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		G_B40_3 = G_B38_2;
		goto IL_03bd;
	}

IL_03bc:
	{
		G_B40_0 = 0;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
		G_B40_3 = G_B39_2;
	}

IL_03bd:
	{
		NullCheck(G_B40_3);
		((GUILayoutEntry_t877 *)G_B40_3)->___stretchWidth_5 = ((int32_t)((int32_t)G_B40_2+(int32_t)((int32_t)((int32_t)G_B40_1+(int32_t)G_B40_0))));
		float L_139 = (__this->___childMaxWidth_23);
		float L_140 = V_11;
		float L_141 = V_12;
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = ((float)((float)((float)((float)L_139+(float)L_140))+(float)L_141));
		goto IL_03e2;
	}

IL_03db:
	{
		((GUILayoutEntry_t877 *)__this)->___stretchWidth_5 = 0;
	}

IL_03e2:
	{
		float L_142 = (((GUILayoutEntry_t877 *)__this)->___maxWidth_1);
		float L_143 = (((GUILayoutEntry_t877 *)__this)->___minWidth_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_144 = Mathf_Max_m2937(NULL /*static, unused*/, L_142, L_143, /*hidden argument*/NULL);
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = L_144;
		GUIStyle_t273 * L_145 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_145);
		float L_146 = GUIStyle_get_fixedWidth_m5319(L_145, /*hidden argument*/NULL);
		if ((((float)L_146) == ((float)(0.0f))))
		{
			goto IL_0431;
		}
	}
	{
		GUIStyle_t273 * L_147 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_147);
		float L_148 = GUIStyle_get_fixedWidth_m5319(L_147, /*hidden argument*/NULL);
		float L_149 = L_148;
		V_13 = L_149;
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = L_149;
		float L_150 = V_13;
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = L_150;
		((GUILayoutEntry_t877 *)__this)->___stretchWidth_5 = 0;
	}

IL_0431:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1112_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m6363_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6364_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6365_MethodInfo_var;
extern "C" void GUILayoutGroup_SetHorizontal_m5197 (GUILayoutGroup_t873 * __this, float ___x, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Enumerator_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		List_1_GetEnumerator_m6363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		Enumerator_get_Current_m6364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		Enumerator_MoveNext_m6365_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t741 * V_0 = {0};
	GUILayoutEntry_t877 * V_1 = {0};
	Enumerator_t1112  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	GUILayoutEntry_t877 * V_8 = {0};
	Enumerator_t1112  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t V_15 = 0;
	bool V_16 = false;
	GUILayoutEntry_t877 * V_17 = {0};
	Enumerator_t1112  V_18 = {0};
	float V_19 = 0.0f;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B39_0 = 0;
	{
		float L_0 = ___x;
		float L_1 = ___width;
		GUILayoutEntry_SetHorizontal_m5184(__this, L_0, L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___resetCoords_12);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		___x = (0.0f);
	}

IL_001a:
	{
		GUIStyle_t273 * L_3 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_t741 * L_4 = GUIStyle_get_padding_m5315(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = (__this->___isVertical_11);
		if (!L_5)
		{
			goto IL_01bb;
		}
	}
	{
		GUIStyle_t273 * L_6 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_7 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t273 *)L_6) == ((Object_t*)(GUIStyle_t273 *)L_7)))
		{
			goto IL_00eb;
		}
	}
	{
		List_1_t878 * L_8 = (__this->___entries_10);
		NullCheck(L_8);
		Enumerator_t1112  L_9 = List_1_GetEnumerator_m6363(L_8, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_2 = L_9;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c9;
		}

IL_0052:
		{
			GUILayoutEntry_t877 * L_10 = Enumerator_get_Current_m6364((&V_2), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_1 = L_10;
			GUILayoutEntry_t877 * L_11 = V_1;
			NullCheck(L_11);
			RectOffset_t741 * L_12 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_11);
			NullCheck(L_12);
			int32_t L_13 = RectOffset_get_left_m4939(L_12, /*hidden argument*/NULL);
			RectOffset_t741 * L_14 = V_0;
			NullCheck(L_14);
			int32_t L_15 = RectOffset_get_left_m4939(L_14, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			int32_t L_16 = Mathf_Max_m3049(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
			V_3 = (((float)L_16));
			float L_17 = ___x;
			float L_18 = V_3;
			V_4 = ((float)((float)L_17+(float)L_18));
			float L_19 = ___width;
			GUILayoutEntry_t877 * L_20 = V_1;
			NullCheck(L_20);
			RectOffset_t741 * L_21 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_20);
			NullCheck(L_21);
			int32_t L_22 = RectOffset_get_right_m5299(L_21, /*hidden argument*/NULL);
			RectOffset_t741 * L_23 = V_0;
			NullCheck(L_23);
			int32_t L_24 = RectOffset_get_right_m5299(L_23, /*hidden argument*/NULL);
			int32_t L_25 = Mathf_Max_m3049(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
			float L_26 = V_3;
			V_5 = ((float)((float)((float)((float)L_19-(float)(((float)L_25))))-(float)L_26));
			GUILayoutEntry_t877 * L_27 = V_1;
			NullCheck(L_27);
			int32_t L_28 = (L_27->___stretchWidth_5);
			if (!L_28)
			{
				goto IL_00ae;
			}
		}

IL_009f:
		{
			GUILayoutEntry_t877 * L_29 = V_1;
			float L_30 = V_4;
			float L_31 = V_5;
			NullCheck(L_29);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_29, L_30, L_31);
			goto IL_00c9;
		}

IL_00ae:
		{
			GUILayoutEntry_t877 * L_32 = V_1;
			float L_33 = V_4;
			float L_34 = V_5;
			GUILayoutEntry_t877 * L_35 = V_1;
			NullCheck(L_35);
			float L_36 = (L_35->___minWidth_0);
			GUILayoutEntry_t877 * L_37 = V_1;
			NullCheck(L_37);
			float L_38 = (L_37->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_39 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_34, L_36, L_38, /*hidden argument*/NULL);
			NullCheck(L_32);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_32, L_33, L_39);
		}

IL_00c9:
		{
			bool L_40 = Enumerator_MoveNext_m6365((&V_2), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_40)
			{
				goto IL_0052;
			}
		}

IL_00d5:
		{
			IL2CPP_LEAVE(0xE6, FINALLY_00da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_00da;
	}

FINALLY_00da:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_41 = V_2;
		Enumerator_t1112  L_42 = L_41;
		Object_t * L_43 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_43);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_43);
		IL2CPP_END_FINALLY(218)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(218)
	{
		IL2CPP_JUMP_TBL(0xE6, IL_00e6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_00e6:
	{
		goto IL_01b6;
	}

IL_00eb:
	{
		float L_44 = ___x;
		RectOffset_t741 * L_45 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_45);
		int32_t L_46 = RectOffset_get_left_m4939(L_45, /*hidden argument*/NULL);
		V_6 = ((float)((float)L_44-(float)(((float)L_46))));
		float L_47 = ___width;
		RectOffset_t741 * L_48 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_48);
		int32_t L_49 = RectOffset_get_horizontal_m4934(L_48, /*hidden argument*/NULL);
		V_7 = ((float)((float)L_47+(float)(((float)L_49))));
		List_1_t878 * L_50 = (__this->___entries_10);
		NullCheck(L_50);
		Enumerator_t1112  L_51 = List_1_GetEnumerator_m6363(L_50, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_9 = L_51;
	}

IL_0118:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0198;
		}

IL_011d:
		{
			GUILayoutEntry_t877 * L_52 = Enumerator_get_Current_m6364((&V_9), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_8 = L_52;
			GUILayoutEntry_t877 * L_53 = V_8;
			NullCheck(L_53);
			int32_t L_54 = (L_53->___stretchWidth_5);
			if (!L_54)
			{
				goto IL_015e;
			}
		}

IL_0132:
		{
			GUILayoutEntry_t877 * L_55 = V_8;
			float L_56 = V_6;
			GUILayoutEntry_t877 * L_57 = V_8;
			NullCheck(L_57);
			RectOffset_t741 * L_58 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_57);
			NullCheck(L_58);
			int32_t L_59 = RectOffset_get_left_m4939(L_58, /*hidden argument*/NULL);
			float L_60 = V_7;
			GUILayoutEntry_t877 * L_61 = V_8;
			NullCheck(L_61);
			RectOffset_t741 * L_62 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_61);
			NullCheck(L_62);
			int32_t L_63 = RectOffset_get_horizontal_m4934(L_62, /*hidden argument*/NULL);
			NullCheck(L_55);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_55, ((float)((float)L_56+(float)(((float)L_59)))), ((float)((float)L_60-(float)(((float)L_63)))));
			goto IL_0198;
		}

IL_015e:
		{
			GUILayoutEntry_t877 * L_64 = V_8;
			float L_65 = V_6;
			GUILayoutEntry_t877 * L_66 = V_8;
			NullCheck(L_66);
			RectOffset_t741 * L_67 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_66);
			NullCheck(L_67);
			int32_t L_68 = RectOffset_get_left_m4939(L_67, /*hidden argument*/NULL);
			float L_69 = V_7;
			GUILayoutEntry_t877 * L_70 = V_8;
			NullCheck(L_70);
			RectOffset_t741 * L_71 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_70);
			NullCheck(L_71);
			int32_t L_72 = RectOffset_get_horizontal_m4934(L_71, /*hidden argument*/NULL);
			GUILayoutEntry_t877 * L_73 = V_8;
			NullCheck(L_73);
			float L_74 = (L_73->___minWidth_0);
			GUILayoutEntry_t877 * L_75 = V_8;
			NullCheck(L_75);
			float L_76 = (L_75->___maxWidth_1);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_77 = Mathf_Clamp_m2768(NULL /*static, unused*/, ((float)((float)L_69-(float)(((float)L_72)))), L_74, L_76, /*hidden argument*/NULL);
			NullCheck(L_64);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_64, ((float)((float)L_65+(float)(((float)L_68)))), L_77);
		}

IL_0198:
		{
			bool L_78 = Enumerator_MoveNext_m6365((&V_9), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_78)
			{
				goto IL_011d;
			}
		}

IL_01a4:
		{
			IL2CPP_LEAVE(0x1B6, FINALLY_01a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_01a9;
	}

FINALLY_01a9:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_79 = V_9;
		Enumerator_t1112  L_80 = L_79;
		Object_t * L_81 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_80);
		NullCheck(L_81);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_81);
		IL2CPP_END_FINALLY(425)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(425)
	{
		IL2CPP_JUMP_TBL(0x1B6, IL_01b6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_01b6:
	{
		goto IL_03b0;
	}

IL_01bb:
	{
		GUIStyle_t273 * L_82 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_83 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t273 *)L_82) == ((Object_t*)(GUIStyle_t273 *)L_83)))
		{
			goto IL_0248;
		}
	}
	{
		RectOffset_t741 * L_84 = V_0;
		NullCheck(L_84);
		int32_t L_85 = RectOffset_get_left_m4939(L_84, /*hidden argument*/NULL);
		V_10 = (((float)L_85));
		RectOffset_t741 * L_86 = V_0;
		NullCheck(L_86);
		int32_t L_87 = RectOffset_get_right_m5299(L_86, /*hidden argument*/NULL);
		V_11 = (((float)L_87));
		List_1_t878 * L_88 = (__this->___entries_10);
		NullCheck(L_88);
		int32_t L_89 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_88);
		if (!L_89)
		{
			goto IL_0239;
		}
	}
	{
		float L_90 = V_10;
		List_1_t878 * L_91 = (__this->___entries_10);
		NullCheck(L_91);
		GUILayoutEntry_t877 * L_92 = (GUILayoutEntry_t877 *)VirtFuncInvoker1< GUILayoutEntry_t877 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_91, 0);
		NullCheck(L_92);
		RectOffset_t741 * L_93 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_92);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_left_m4939(L_93, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_95 = Mathf_Max_m2937(NULL /*static, unused*/, L_90, (((float)L_94)), /*hidden argument*/NULL);
		V_10 = L_95;
		float L_96 = V_11;
		List_1_t878 * L_97 = (__this->___entries_10);
		List_1_t878 * L_98 = (__this->___entries_10);
		NullCheck(L_98);
		int32_t L_99 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_98);
		NullCheck(L_97);
		GUILayoutEntry_t877 * L_100 = (GUILayoutEntry_t877 *)VirtFuncInvoker1< GUILayoutEntry_t877 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_97, ((int32_t)((int32_t)L_99-(int32_t)1)));
		NullCheck(L_100);
		RectOffset_t741 * L_101 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_100);
		NullCheck(L_101);
		int32_t L_102 = RectOffset_get_right_m5299(L_101, /*hidden argument*/NULL);
		float L_103 = Mathf_Max_m2937(NULL /*static, unused*/, L_96, (((float)L_102)), /*hidden argument*/NULL);
		V_11 = L_103;
	}

IL_0239:
	{
		float L_104 = ___x;
		float L_105 = V_10;
		___x = ((float)((float)L_104+(float)L_105));
		float L_106 = ___width;
		float L_107 = V_11;
		float L_108 = V_10;
		___width = ((float)((float)L_106-(float)((float)((float)L_107+(float)L_108))));
	}

IL_0248:
	{
		float L_109 = ___width;
		float L_110 = (__this->___spacing_13);
		List_1_t878 * L_111 = (__this->___entries_10);
		NullCheck(L_111);
		int32_t L_112 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_111);
		V_12 = ((float)((float)L_109-(float)((float)((float)L_110*(float)(((float)((int32_t)((int32_t)L_112-(int32_t)1))))))));
		V_13 = (0.0f);
		float L_113 = (__this->___childMinWidth_22);
		float L_114 = (__this->___childMaxWidth_23);
		if ((((float)L_113) == ((float)L_114)))
		{
			goto IL_02a1;
		}
	}
	{
		float L_115 = V_12;
		float L_116 = (__this->___childMinWidth_22);
		float L_117 = (__this->___childMaxWidth_23);
		float L_118 = (__this->___childMinWidth_22);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_119 = Mathf_Clamp_m2768(NULL /*static, unused*/, ((float)((float)((float)((float)L_115-(float)L_116))/(float)((float)((float)L_117-(float)L_118)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_13 = L_119;
	}

IL_02a1:
	{
		V_14 = (0.0f);
		float L_120 = V_12;
		float L_121 = (__this->___childMaxWidth_23);
		if ((!(((float)L_120) > ((float)L_121))))
		{
			goto IL_02d4;
		}
	}
	{
		int32_t L_122 = (__this->___stretchableCountX_18);
		if ((((int32_t)L_122) <= ((int32_t)0)))
		{
			goto IL_02d4;
		}
	}
	{
		float L_123 = V_12;
		float L_124 = (__this->___childMaxWidth_23);
		int32_t L_125 = (__this->___stretchableCountX_18);
		V_14 = ((float)((float)((float)((float)L_123-(float)L_124))/(float)(((float)L_125))));
	}

IL_02d4:
	{
		V_15 = 0;
		V_16 = 1;
		List_1_t878 * L_126 = (__this->___entries_10);
		NullCheck(L_126);
		Enumerator_t1112  L_127 = List_1_GetEnumerator_m6363(L_126, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_18 = L_127;
	}

IL_02e7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0392;
		}

IL_02ec:
		{
			GUILayoutEntry_t877 * L_128 = Enumerator_get_Current_m6364((&V_18), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_17 = L_128;
			GUILayoutEntry_t877 * L_129 = V_17;
			NullCheck(L_129);
			float L_130 = (L_129->___minWidth_0);
			GUILayoutEntry_t877 * L_131 = V_17;
			NullCheck(L_131);
			float L_132 = (L_131->___maxWidth_1);
			float L_133 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_134 = Mathf_Lerp_m2756(NULL /*static, unused*/, L_130, L_132, L_133, /*hidden argument*/NULL);
			V_19 = L_134;
			float L_135 = V_19;
			float L_136 = V_14;
			GUILayoutEntry_t877 * L_137 = V_17;
			NullCheck(L_137);
			int32_t L_138 = (L_137->___stretchWidth_5);
			V_19 = ((float)((float)L_135+(float)((float)((float)L_136*(float)(((float)L_138))))));
			GUILayoutEntry_t877 * L_139 = V_17;
			NullCheck(L_139);
			GUIStyle_t273 * L_140 = GUILayoutEntry_get_style_m5179(L_139, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUIStyle_t273 * L_141 = GUILayoutUtility_get_spaceStyle_m5175(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t273 *)L_140) == ((Object_t*)(GUIStyle_t273 *)L_141)))
			{
				goto IL_0371;
			}
		}

IL_032d:
		{
			GUILayoutEntry_t877 * L_142 = V_17;
			NullCheck(L_142);
			RectOffset_t741 * L_143 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_142);
			NullCheck(L_143);
			int32_t L_144 = RectOffset_get_left_m4939(L_143, /*hidden argument*/NULL);
			V_20 = L_144;
			bool L_145 = V_16;
			if (!L_145)
			{
				goto IL_0348;
			}
		}

IL_0342:
		{
			V_20 = 0;
			V_16 = 0;
		}

IL_0348:
		{
			int32_t L_146 = V_15;
			int32_t L_147 = V_20;
			if ((((int32_t)L_146) <= ((int32_t)L_147)))
			{
				goto IL_0358;
			}
		}

IL_0351:
		{
			int32_t L_148 = V_15;
			G_B39_0 = L_148;
			goto IL_035a;
		}

IL_0358:
		{
			int32_t L_149 = V_20;
			G_B39_0 = L_149;
		}

IL_035a:
		{
			V_21 = G_B39_0;
			float L_150 = ___x;
			int32_t L_151 = V_21;
			___x = ((float)((float)L_150+(float)(((float)L_151))));
			GUILayoutEntry_t877 * L_152 = V_17;
			NullCheck(L_152);
			RectOffset_t741 * L_153 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_152);
			NullCheck(L_153);
			int32_t L_154 = RectOffset_get_right_m5299(L_153, /*hidden argument*/NULL);
			V_15 = L_154;
		}

IL_0371:
		{
			GUILayoutEntry_t877 * L_155 = V_17;
			float L_156 = ___x;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_157 = roundf(L_156);
			float L_158 = V_19;
			float L_159 = roundf(L_158);
			NullCheck(L_155);
			VirtActionInvoker2< float, float >::Invoke(7 /* System.Void UnityEngine.GUILayoutEntry::SetHorizontal(System.Single,System.Single) */, L_155, L_157, L_159);
			float L_160 = ___x;
			float L_161 = V_19;
			float L_162 = (__this->___spacing_13);
			___x = ((float)((float)L_160+(float)((float)((float)L_161+(float)L_162))));
		}

IL_0392:
		{
			bool L_163 = Enumerator_MoveNext_m6365((&V_18), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_163)
			{
				goto IL_02ec;
			}
		}

IL_039e:
		{
			IL2CPP_LEAVE(0x3B0, FINALLY_03a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_03a3;
	}

FINALLY_03a3:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_164 = V_18;
		Enumerator_t1112  L_165 = L_164;
		Object_t * L_166 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_165);
		NullCheck(L_166);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_166);
		IL2CPP_END_FINALLY(931)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(931)
	{
		IL2CPP_JUMP_TBL(0x3B0, IL_03b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_03b0:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::CalcHeight()
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1112_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m6363_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6364_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6365_MethodInfo_var;
extern "C" void GUILayoutGroup_CalcHeight_m5198 (GUILayoutGroup_t873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Enumerator_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		List_1_GetEnumerator_m6363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		Enumerator_get_Current_m6364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		Enumerator_MoveNext_m6365_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	GUILayoutEntry_t877 * V_4 = {0};
	Enumerator_t1112  V_5 = {0};
	RectOffset_t741 * V_6 = {0};
	int32_t V_7 = 0;
	bool V_8 = false;
	GUILayoutEntry_t877 * V_9 = {0};
	Enumerator_t1112  V_10 = {0};
	RectOffset_t741 * V_11 = {0};
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B36_0 = 0;
	int32_t G_B36_1 = 0;
	GUILayoutGroup_t873 * G_B36_2 = {0};
	int32_t G_B35_0 = 0;
	int32_t G_B35_1 = 0;
	GUILayoutGroup_t873 * G_B35_2 = {0};
	int32_t G_B37_0 = 0;
	int32_t G_B37_1 = 0;
	int32_t G_B37_2 = 0;
	GUILayoutGroup_t873 * G_B37_3 = {0};
	{
		List_1_t878 * L_0 = (__this->___entries_10);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0033;
		}
	}
	{
		GUIStyle_t273 * L_2 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_t741 * L_3 = GUIStyle_get_padding_m5315(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = RectOffset_get_vertical_m4935(L_3, /*hidden argument*/NULL);
		float L_5 = (((float)L_4));
		V_14 = L_5;
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_5;
		float L_6 = V_14;
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_6;
		return;
	}

IL_0033:
	{
		float L_7 = (0.0f);
		V_14 = L_7;
		__this->___childMaxHeight_25 = L_7;
		float L_8 = V_14;
		__this->___childMinHeight_24 = L_8;
		V_0 = 0;
		V_1 = 0;
		__this->___stretchableCountY_19 = 0;
		bool L_9 = (__this->___isVertical_11);
		if (!L_9)
		{
			goto IL_01d4;
		}
	}
	{
		V_2 = 0;
		V_3 = 1;
		List_1_t878 * L_10 = (__this->___entries_10);
		NullCheck(L_10);
		Enumerator_t1112  L_11 = List_1_GetEnumerator_m6363(L_10, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_5 = L_11;
	}

IL_0070:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0159;
		}

IL_0075:
		{
			GUILayoutEntry_t877 * L_12 = Enumerator_get_Current_m6364((&V_5), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_4 = L_12;
			GUILayoutEntry_t877 * L_13 = V_4;
			NullCheck(L_13);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_13);
			GUILayoutEntry_t877 * L_14 = V_4;
			NullCheck(L_14);
			RectOffset_t741 * L_15 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_14);
			V_6 = L_15;
			GUILayoutEntry_t877 * L_16 = V_4;
			NullCheck(L_16);
			GUIStyle_t273 * L_17 = GUILayoutEntry_get_style_m5179(L_16, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUIStyle_t273 * L_18 = GUILayoutUtility_get_spaceStyle_m5175(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t273 *)L_17) == ((Object_t*)(GUIStyle_t273 *)L_18)))
			{
				goto IL_011d;
			}
		}

IL_009f:
		{
			bool L_19 = V_3;
			if (L_19)
			{
				goto IL_00b9;
			}
		}

IL_00a5:
		{
			int32_t L_20 = V_2;
			RectOffset_t741 * L_21 = V_6;
			NullCheck(L_21);
			int32_t L_22 = RectOffset_get_top_m4940(L_21, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			int32_t L_23 = Mathf_Max_m3049(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
			V_7 = L_23;
			goto IL_00be;
		}

IL_00b9:
		{
			V_7 = 0;
			V_3 = 0;
		}

IL_00be:
		{
			float L_24 = (__this->___childMinHeight_24);
			GUILayoutEntry_t877 * L_25 = V_4;
			NullCheck(L_25);
			float L_26 = (L_25->___minHeight_2);
			float L_27 = (__this->___spacing_13);
			int32_t L_28 = V_7;
			__this->___childMinHeight_24 = ((float)((float)L_24+(float)((float)((float)((float)((float)L_26+(float)L_27))+(float)(((float)L_28))))));
			float L_29 = (__this->___childMaxHeight_25);
			GUILayoutEntry_t877 * L_30 = V_4;
			NullCheck(L_30);
			float L_31 = (L_30->___maxHeight_3);
			float L_32 = (__this->___spacing_13);
			int32_t L_33 = V_7;
			__this->___childMaxHeight_25 = ((float)((float)L_29+(float)((float)((float)((float)((float)L_31+(float)L_32))+(float)(((float)L_33))))));
			RectOffset_t741 * L_34 = V_6;
			NullCheck(L_34);
			int32_t L_35 = RectOffset_get_bottom_m5302(L_34, /*hidden argument*/NULL);
			V_2 = L_35;
			int32_t L_36 = (__this->___stretchableCountY_19);
			GUILayoutEntry_t877 * L_37 = V_4;
			NullCheck(L_37);
			int32_t L_38 = (L_37->___stretchHeight_6);
			__this->___stretchableCountY_19 = ((int32_t)((int32_t)L_36+(int32_t)L_38));
			goto IL_0159;
		}

IL_011d:
		{
			float L_39 = (__this->___childMinHeight_24);
			GUILayoutEntry_t877 * L_40 = V_4;
			NullCheck(L_40);
			float L_41 = (L_40->___minHeight_2);
			__this->___childMinHeight_24 = ((float)((float)L_39+(float)L_41));
			float L_42 = (__this->___childMaxHeight_25);
			GUILayoutEntry_t877 * L_43 = V_4;
			NullCheck(L_43);
			float L_44 = (L_43->___maxHeight_3);
			__this->___childMaxHeight_25 = ((float)((float)L_42+(float)L_44));
			int32_t L_45 = (__this->___stretchableCountY_19);
			GUILayoutEntry_t877 * L_46 = V_4;
			NullCheck(L_46);
			int32_t L_47 = (L_46->___stretchHeight_6);
			__this->___stretchableCountY_19 = ((int32_t)((int32_t)L_45+(int32_t)L_47));
		}

IL_0159:
		{
			bool L_48 = Enumerator_MoveNext_m6365((&V_5), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_48)
			{
				goto IL_0075;
			}
		}

IL_0165:
		{
			IL2CPP_LEAVE(0x177, FINALLY_016a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_016a;
	}

FINALLY_016a:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_49 = V_5;
		Enumerator_t1112  L_50 = L_49;
		Object_t * L_51 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_51);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_51);
		IL2CPP_END_FINALLY(362)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(362)
	{
		IL2CPP_JUMP_TBL(0x177, IL_0177)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0177:
	{
		float L_52 = (__this->___childMinHeight_24);
		float L_53 = (__this->___spacing_13);
		__this->___childMinHeight_24 = ((float)((float)L_52-(float)L_53));
		float L_54 = (__this->___childMaxHeight_25);
		float L_55 = (__this->___spacing_13);
		__this->___childMaxHeight_25 = ((float)((float)L_54-(float)L_55));
		List_1_t878 * L_56 = (__this->___entries_10);
		NullCheck(L_56);
		int32_t L_57 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_56);
		if (!L_57)
		{
			goto IL_01cb;
		}
	}
	{
		List_1_t878 * L_58 = (__this->___entries_10);
		NullCheck(L_58);
		GUILayoutEntry_t877 * L_59 = (GUILayoutEntry_t877 *)VirtFuncInvoker1< GUILayoutEntry_t877 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_58, 0);
		NullCheck(L_59);
		RectOffset_t741 * L_60 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_59);
		NullCheck(L_60);
		int32_t L_61 = RectOffset_get_top_m4940(L_60, /*hidden argument*/NULL);
		V_0 = L_61;
		int32_t L_62 = V_2;
		V_1 = L_62;
		goto IL_01cf;
	}

IL_01cb:
	{
		int32_t L_63 = 0;
		V_0 = L_63;
		V_1 = L_63;
	}

IL_01cf:
	{
		goto IL_02b0;
	}

IL_01d4:
	{
		V_8 = 1;
		List_1_t878 * L_64 = (__this->___entries_10);
		NullCheck(L_64);
		Enumerator_t1112  L_65 = List_1_GetEnumerator_m6363(L_64, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_10 = L_65;
	}

IL_01e4:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0292;
		}

IL_01e9:
		{
			GUILayoutEntry_t877 * L_66 = Enumerator_get_Current_m6364((&V_10), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_9 = L_66;
			GUILayoutEntry_t877 * L_67 = V_9;
			NullCheck(L_67);
			VirtActionInvoker0::Invoke(6 /* System.Void UnityEngine.GUILayoutEntry::CalcHeight() */, L_67);
			GUILayoutEntry_t877 * L_68 = V_9;
			NullCheck(L_68);
			RectOffset_t741 * L_69 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_68);
			V_11 = L_69;
			GUILayoutEntry_t877 * L_70 = V_9;
			NullCheck(L_70);
			GUIStyle_t273 * L_71 = GUILayoutEntry_get_style_m5179(L_70, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUIStyle_t273 * L_72 = GUILayoutUtility_get_spaceStyle_m5175(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t273 *)L_71) == ((Object_t*)(GUIStyle_t273 *)L_72)))
			{
				goto IL_027e;
			}
		}

IL_0213:
		{
			bool L_73 = V_8;
			if (L_73)
			{
				goto IL_023b;
			}
		}

IL_021a:
		{
			RectOffset_t741 * L_74 = V_11;
			NullCheck(L_74);
			int32_t L_75 = RectOffset_get_top_m4940(L_74, /*hidden argument*/NULL);
			int32_t L_76 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			int32_t L_77 = Mathf_Min_m3050(NULL /*static, unused*/, L_75, L_76, /*hidden argument*/NULL);
			V_0 = L_77;
			RectOffset_t741 * L_78 = V_11;
			NullCheck(L_78);
			int32_t L_79 = RectOffset_get_bottom_m5302(L_78, /*hidden argument*/NULL);
			int32_t L_80 = V_1;
			int32_t L_81 = Mathf_Min_m3050(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
			V_1 = L_81;
			goto IL_024e;
		}

IL_023b:
		{
			RectOffset_t741 * L_82 = V_11;
			NullCheck(L_82);
			int32_t L_83 = RectOffset_get_top_m4940(L_82, /*hidden argument*/NULL);
			V_0 = L_83;
			RectOffset_t741 * L_84 = V_11;
			NullCheck(L_84);
			int32_t L_85 = RectOffset_get_bottom_m5302(L_84, /*hidden argument*/NULL);
			V_1 = L_85;
			V_8 = 0;
		}

IL_024e:
		{
			GUILayoutEntry_t877 * L_86 = V_9;
			NullCheck(L_86);
			float L_87 = (L_86->___minHeight_2);
			float L_88 = (__this->___childMinHeight_24);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_89 = Mathf_Max_m2937(NULL /*static, unused*/, L_87, L_88, /*hidden argument*/NULL);
			__this->___childMinHeight_24 = L_89;
			GUILayoutEntry_t877 * L_90 = V_9;
			NullCheck(L_90);
			float L_91 = (L_90->___maxHeight_3);
			float L_92 = (__this->___childMaxHeight_25);
			float L_93 = Mathf_Max_m2937(NULL /*static, unused*/, L_91, L_92, /*hidden argument*/NULL);
			__this->___childMaxHeight_25 = L_93;
		}

IL_027e:
		{
			int32_t L_94 = (__this->___stretchableCountY_19);
			GUILayoutEntry_t877 * L_95 = V_9;
			NullCheck(L_95);
			int32_t L_96 = (L_95->___stretchHeight_6);
			__this->___stretchableCountY_19 = ((int32_t)((int32_t)L_94+(int32_t)L_96));
		}

IL_0292:
		{
			bool L_97 = Enumerator_MoveNext_m6365((&V_10), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_97)
			{
				goto IL_01e9;
			}
		}

IL_029e:
		{
			IL2CPP_LEAVE(0x2B0, FINALLY_02a3);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_02a3;
	}

FINALLY_02a3:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_98 = V_10;
		Enumerator_t1112  L_99 = L_98;
		Object_t * L_100 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_99);
		NullCheck(L_100);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_100);
		IL2CPP_END_FINALLY(675)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(675)
	{
		IL2CPP_JUMP_TBL(0x2B0, IL_02b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_02b0:
	{
		V_12 = (0.0f);
		V_13 = (0.0f);
		GUIStyle_t273 * L_101 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_102 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((Object_t*)(GUIStyle_t273 *)L_101) == ((Object_t*)(GUIStyle_t273 *)L_102))))
		{
			goto IL_02d9;
		}
	}
	{
		bool L_103 = (__this->___userSpecifiedHeight_21);
		if (!L_103)
		{
			goto IL_0310;
		}
	}

IL_02d9:
	{
		GUIStyle_t273 * L_104 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_104);
		RectOffset_t741 * L_105 = GUIStyle_get_padding_m5315(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		int32_t L_106 = RectOffset_get_top_m4940(L_105, /*hidden argument*/NULL);
		int32_t L_107 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		int32_t L_108 = Mathf_Max_m3049(NULL /*static, unused*/, L_106, L_107, /*hidden argument*/NULL);
		V_12 = (((float)L_108));
		GUIStyle_t273 * L_109 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_109);
		RectOffset_t741 * L_110 = GUIStyle_get_padding_m5315(L_109, /*hidden argument*/NULL);
		NullCheck(L_110);
		int32_t L_111 = RectOffset_get_bottom_m5302(L_110, /*hidden argument*/NULL);
		int32_t L_112 = V_1;
		int32_t L_113 = Mathf_Max_m3049(NULL /*static, unused*/, L_111, L_112, /*hidden argument*/NULL);
		V_13 = (((float)L_113));
		goto IL_0332;
	}

IL_0310:
	{
		RectOffset_t741 * L_114 = (__this->___m_Margin_26);
		int32_t L_115 = V_0;
		NullCheck(L_114);
		RectOffset_set_top_m5301(L_114, L_115, /*hidden argument*/NULL);
		RectOffset_t741 * L_116 = (__this->___m_Margin_26);
		int32_t L_117 = V_1;
		NullCheck(L_116);
		RectOffset_set_bottom_m5303(L_116, L_117, /*hidden argument*/NULL);
		float L_118 = (0.0f);
		V_13 = L_118;
		V_12 = L_118;
	}

IL_0332:
	{
		float L_119 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		float L_120 = (__this->___childMinHeight_24);
		float L_121 = V_12;
		float L_122 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_123 = Mathf_Max_m2937(NULL /*static, unused*/, L_119, ((float)((float)((float)((float)L_120+(float)L_121))+(float)L_122)), /*hidden argument*/NULL);
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_123;
		float L_124 = (((GUILayoutEntry_t877 *)__this)->___maxHeight_3);
		if ((!(((float)L_124) == ((float)(0.0f)))))
		{
			goto IL_03a1;
		}
	}
	{
		int32_t L_125 = (((GUILayoutEntry_t877 *)__this)->___stretchHeight_6);
		int32_t L_126 = (__this->___stretchableCountY_19);
		GUIStyle_t273 * L_127 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_127);
		bool L_128 = GUIStyle_get_stretchHeight_m5323(L_127, /*hidden argument*/NULL);
		G_B35_0 = L_126;
		G_B35_1 = L_125;
		G_B35_2 = __this;
		if (!L_128)
		{
			G_B36_0 = L_126;
			G_B36_1 = L_125;
			G_B36_2 = __this;
			goto IL_0382;
		}
	}
	{
		G_B37_0 = 1;
		G_B37_1 = G_B35_0;
		G_B37_2 = G_B35_1;
		G_B37_3 = G_B35_2;
		goto IL_0383;
	}

IL_0382:
	{
		G_B37_0 = 0;
		G_B37_1 = G_B36_0;
		G_B37_2 = G_B36_1;
		G_B37_3 = G_B36_2;
	}

IL_0383:
	{
		NullCheck(G_B37_3);
		((GUILayoutEntry_t877 *)G_B37_3)->___stretchHeight_6 = ((int32_t)((int32_t)G_B37_2+(int32_t)((int32_t)((int32_t)G_B37_1+(int32_t)G_B37_0))));
		float L_129 = (__this->___childMaxHeight_25);
		float L_130 = V_12;
		float L_131 = V_13;
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = ((float)((float)((float)((float)L_129+(float)L_130))+(float)L_131));
		goto IL_03a8;
	}

IL_03a1:
	{
		((GUILayoutEntry_t877 *)__this)->___stretchHeight_6 = 0;
	}

IL_03a8:
	{
		float L_132 = (((GUILayoutEntry_t877 *)__this)->___maxHeight_3);
		float L_133 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_134 = Mathf_Max_m2937(NULL /*static, unused*/, L_132, L_133, /*hidden argument*/NULL);
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_134;
		GUIStyle_t273 * L_135 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_135);
		float L_136 = GUIStyle_get_fixedHeight_m5320(L_135, /*hidden argument*/NULL);
		if ((((float)L_136) == ((float)(0.0f))))
		{
			goto IL_03f7;
		}
	}
	{
		GUIStyle_t273 * L_137 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_137);
		float L_138 = GUIStyle_get_fixedHeight_m5320(L_137, /*hidden argument*/NULL);
		float L_139 = L_138;
		V_14 = L_139;
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_139;
		float L_140 = V_14;
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_140;
		((GUILayoutEntry_t877 *)__this)->___stretchHeight_6 = 0;
	}

IL_03f7:
	{
		return;
	}
}
// System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1112_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m6363_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6364_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6365_MethodInfo_var;
extern "C" void GUILayoutGroup_SetVertical_m5199 (GUILayoutGroup_t873 * __this, float ___y, float ___height, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		Enumerator_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		List_1_GetEnumerator_m6363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		Enumerator_get_Current_m6364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		Enumerator_MoveNext_m6365_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		s_Il2CppMethodIntialized = true;
	}
	RectOffset_t741 * V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	bool V_7 = false;
	GUILayoutEntry_t877 * V_8 = {0};
	Enumerator_t1112  V_9 = {0};
	float V_10 = 0.0f;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	GUILayoutEntry_t877 * V_13 = {0};
	Enumerator_t1112  V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	GUILayoutEntry_t877 * V_20 = {0};
	Enumerator_t1112  V_21 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B22_0 = 0;
	{
		float L_0 = ___y;
		float L_1 = ___height;
		GUILayoutEntry_SetVertical_m5185(__this, L_0, L_1, /*hidden argument*/NULL);
		List_1_t878 * L_2 = (__this->___entries_10);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		return;
	}

IL_0019:
	{
		GUIStyle_t273 * L_4 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		RectOffset_t741 * L_5 = GUIStyle_get_padding_m5315(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		bool L_6 = (__this->___resetCoords_12);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		___y = (0.0f);
	}

IL_0037:
	{
		bool L_7 = (__this->___isVertical_11);
		if (!L_7)
		{
			goto IL_022f;
		}
	}
	{
		GUIStyle_t273 * L_8 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_9 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t273 *)L_8) == ((Object_t*)(GUIStyle_t273 *)L_9)))
		{
			goto IL_00c6;
		}
	}
	{
		RectOffset_t741 * L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_top_m4940(L_10, /*hidden argument*/NULL);
		V_1 = (((float)L_11));
		RectOffset_t741 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_bottom_m5302(L_12, /*hidden argument*/NULL);
		V_2 = (((float)L_13));
		List_1_t878 * L_14 = (__this->___entries_10);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_14);
		if (!L_15)
		{
			goto IL_00ba;
		}
	}
	{
		float L_16 = V_1;
		List_1_t878 * L_17 = (__this->___entries_10);
		NullCheck(L_17);
		GUILayoutEntry_t877 * L_18 = (GUILayoutEntry_t877 *)VirtFuncInvoker1< GUILayoutEntry_t877 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_17, 0);
		NullCheck(L_18);
		RectOffset_t741 * L_19 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_18);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m4940(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Max_m2937(NULL /*static, unused*/, L_16, (((float)L_20)), /*hidden argument*/NULL);
		V_1 = L_21;
		float L_22 = V_2;
		List_1_t878 * L_23 = (__this->___entries_10);
		List_1_t878 * L_24 = (__this->___entries_10);
		NullCheck(L_24);
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_24);
		NullCheck(L_23);
		GUILayoutEntry_t877 * L_26 = (GUILayoutEntry_t877 *)VirtFuncInvoker1< GUILayoutEntry_t877 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Item(System.Int32) */, L_23, ((int32_t)((int32_t)L_25-(int32_t)1)));
		NullCheck(L_26);
		RectOffset_t741 * L_27 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_26);
		NullCheck(L_27);
		int32_t L_28 = RectOffset_get_bottom_m5302(L_27, /*hidden argument*/NULL);
		float L_29 = Mathf_Max_m2937(NULL /*static, unused*/, L_22, (((float)L_28)), /*hidden argument*/NULL);
		V_2 = L_29;
	}

IL_00ba:
	{
		float L_30 = ___y;
		float L_31 = V_1;
		___y = ((float)((float)L_30+(float)L_31));
		float L_32 = ___height;
		float L_33 = V_2;
		float L_34 = V_1;
		___height = ((float)((float)L_32-(float)((float)((float)L_33+(float)L_34))));
	}

IL_00c6:
	{
		float L_35 = ___height;
		float L_36 = (__this->___spacing_13);
		List_1_t878 * L_37 = (__this->___entries_10);
		NullCheck(L_37);
		int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>::get_Count() */, L_37);
		V_3 = ((float)((float)L_35-(float)((float)((float)L_36*(float)(((float)((int32_t)((int32_t)L_38-(int32_t)1))))))));
		V_4 = (0.0f);
		float L_39 = (__this->___childMinHeight_24);
		float L_40 = (__this->___childMaxHeight_25);
		if ((((float)L_39) == ((float)L_40)))
		{
			goto IL_011d;
		}
	}
	{
		float L_41 = V_3;
		float L_42 = (__this->___childMinHeight_24);
		float L_43 = (__this->___childMaxHeight_25);
		float L_44 = (__this->___childMinHeight_24);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_45 = Mathf_Clamp_m2768(NULL /*static, unused*/, ((float)((float)((float)((float)L_41-(float)L_42))/(float)((float)((float)L_43-(float)L_44)))), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_4 = L_45;
	}

IL_011d:
	{
		V_5 = (0.0f);
		float L_46 = V_3;
		float L_47 = (__this->___childMaxHeight_25);
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_014e;
		}
	}
	{
		int32_t L_48 = (__this->___stretchableCountY_19);
		if ((((int32_t)L_48) <= ((int32_t)0)))
		{
			goto IL_014e;
		}
	}
	{
		float L_49 = V_3;
		float L_50 = (__this->___childMaxHeight_25);
		int32_t L_51 = (__this->___stretchableCountY_19);
		V_5 = ((float)((float)((float)((float)L_49-(float)L_50))/(float)(((float)L_51))));
	}

IL_014e:
	{
		V_6 = 0;
		V_7 = 1;
		List_1_t878 * L_52 = (__this->___entries_10);
		NullCheck(L_52);
		Enumerator_t1112  L_53 = List_1_GetEnumerator_m6363(L_52, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_9 = L_53;
	}

IL_0161:
	try
	{ // begin try (depth: 1)
		{
			goto IL_020c;
		}

IL_0166:
		{
			GUILayoutEntry_t877 * L_54 = Enumerator_get_Current_m6364((&V_9), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_8 = L_54;
			GUILayoutEntry_t877 * L_55 = V_8;
			NullCheck(L_55);
			float L_56 = (L_55->___minHeight_2);
			GUILayoutEntry_t877 * L_57 = V_8;
			NullCheck(L_57);
			float L_58 = (L_57->___maxHeight_3);
			float L_59 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_60 = Mathf_Lerp_m2756(NULL /*static, unused*/, L_56, L_58, L_59, /*hidden argument*/NULL);
			V_10 = L_60;
			float L_61 = V_10;
			float L_62 = V_5;
			GUILayoutEntry_t877 * L_63 = V_8;
			NullCheck(L_63);
			int32_t L_64 = (L_63->___stretchHeight_6);
			V_10 = ((float)((float)L_61+(float)((float)((float)L_62*(float)(((float)L_64))))));
			GUILayoutEntry_t877 * L_65 = V_8;
			NullCheck(L_65);
			GUIStyle_t273 * L_66 = GUILayoutEntry_get_style_m5179(L_65, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUIStyle_t273 * L_67 = GUILayoutUtility_get_spaceStyle_m5175(NULL /*static, unused*/, /*hidden argument*/NULL);
			if ((((Object_t*)(GUIStyle_t273 *)L_66) == ((Object_t*)(GUIStyle_t273 *)L_67)))
			{
				goto IL_01eb;
			}
		}

IL_01a7:
		{
			GUILayoutEntry_t877 * L_68 = V_8;
			NullCheck(L_68);
			RectOffset_t741 * L_69 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_68);
			NullCheck(L_69);
			int32_t L_70 = RectOffset_get_top_m4940(L_69, /*hidden argument*/NULL);
			V_11 = L_70;
			bool L_71 = V_7;
			if (!L_71)
			{
				goto IL_01c2;
			}
		}

IL_01bc:
		{
			V_11 = 0;
			V_7 = 0;
		}

IL_01c2:
		{
			int32_t L_72 = V_6;
			int32_t L_73 = V_11;
			if ((((int32_t)L_72) <= ((int32_t)L_73)))
			{
				goto IL_01d2;
			}
		}

IL_01cb:
		{
			int32_t L_74 = V_6;
			G_B22_0 = L_74;
			goto IL_01d4;
		}

IL_01d2:
		{
			int32_t L_75 = V_11;
			G_B22_0 = L_75;
		}

IL_01d4:
		{
			V_12 = G_B22_0;
			float L_76 = ___y;
			int32_t L_77 = V_12;
			___y = ((float)((float)L_76+(float)(((float)L_77))));
			GUILayoutEntry_t877 * L_78 = V_8;
			NullCheck(L_78);
			RectOffset_t741 * L_79 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_78);
			NullCheck(L_79);
			int32_t L_80 = RectOffset_get_bottom_m5302(L_79, /*hidden argument*/NULL);
			V_6 = L_80;
		}

IL_01eb:
		{
			GUILayoutEntry_t877 * L_81 = V_8;
			float L_82 = ___y;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_83 = roundf(L_82);
			float L_84 = V_10;
			float L_85 = roundf(L_84);
			NullCheck(L_81);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_81, L_83, L_85);
			float L_86 = ___y;
			float L_87 = V_10;
			float L_88 = (__this->___spacing_13);
			___y = ((float)((float)L_86+(float)((float)((float)L_87+(float)L_88))));
		}

IL_020c:
		{
			bool L_89 = Enumerator_MoveNext_m6365((&V_9), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_89)
			{
				goto IL_0166;
			}
		}

IL_0218:
		{
			IL2CPP_LEAVE(0x22A, FINALLY_021d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_021d;
	}

FINALLY_021d:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_90 = V_9;
		Enumerator_t1112  L_91 = L_90;
		Object_t * L_92 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_91);
		NullCheck(L_92);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_92);
		IL2CPP_END_FINALLY(541)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(541)
	{
		IL2CPP_JUMP_TBL(0x22A, IL_022a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_022a:
	{
		goto IL_03c1;
	}

IL_022f:
	{
		GUIStyle_t273 * L_93 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_94 = GUIStyle_get_none_m5328(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((Object_t*)(GUIStyle_t273 *)L_93) == ((Object_t*)(GUIStyle_t273 *)L_94)))
		{
			goto IL_02f6;
		}
	}
	{
		List_1_t878 * L_95 = (__this->___entries_10);
		NullCheck(L_95);
		Enumerator_t1112  L_96 = List_1_GetEnumerator_m6363(L_95, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_14 = L_96;
	}

IL_024c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_02d3;
		}

IL_0251:
		{
			GUILayoutEntry_t877 * L_97 = Enumerator_get_Current_m6364((&V_14), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_13 = L_97;
			GUILayoutEntry_t877 * L_98 = V_13;
			NullCheck(L_98);
			RectOffset_t741 * L_99 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_98);
			NullCheck(L_99);
			int32_t L_100 = RectOffset_get_top_m4940(L_99, /*hidden argument*/NULL);
			RectOffset_t741 * L_101 = V_0;
			NullCheck(L_101);
			int32_t L_102 = RectOffset_get_top_m4940(L_101, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			int32_t L_103 = Mathf_Max_m3049(NULL /*static, unused*/, L_100, L_102, /*hidden argument*/NULL);
			V_15 = (((float)L_103));
			float L_104 = ___y;
			float L_105 = V_15;
			V_16 = ((float)((float)L_104+(float)L_105));
			float L_106 = ___height;
			GUILayoutEntry_t877 * L_107 = V_13;
			NullCheck(L_107);
			RectOffset_t741 * L_108 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_107);
			NullCheck(L_108);
			int32_t L_109 = RectOffset_get_bottom_m5302(L_108, /*hidden argument*/NULL);
			RectOffset_t741 * L_110 = V_0;
			NullCheck(L_110);
			int32_t L_111 = RectOffset_get_bottom_m5302(L_110, /*hidden argument*/NULL);
			int32_t L_112 = Mathf_Max_m3049(NULL /*static, unused*/, L_109, L_111, /*hidden argument*/NULL);
			float L_113 = V_15;
			V_17 = ((float)((float)((float)((float)L_106-(float)(((float)L_112))))-(float)L_113));
			GUILayoutEntry_t877 * L_114 = V_13;
			NullCheck(L_114);
			int32_t L_115 = (L_114->___stretchHeight_6);
			if (!L_115)
			{
				goto IL_02b5;
			}
		}

IL_02a5:
		{
			GUILayoutEntry_t877 * L_116 = V_13;
			float L_117 = V_16;
			float L_118 = V_17;
			NullCheck(L_116);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_116, L_117, L_118);
			goto IL_02d3;
		}

IL_02b5:
		{
			GUILayoutEntry_t877 * L_119 = V_13;
			float L_120 = V_16;
			float L_121 = V_17;
			GUILayoutEntry_t877 * L_122 = V_13;
			NullCheck(L_122);
			float L_123 = (L_122->___minHeight_2);
			GUILayoutEntry_t877 * L_124 = V_13;
			NullCheck(L_124);
			float L_125 = (L_124->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_126 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_121, L_123, L_125, /*hidden argument*/NULL);
			NullCheck(L_119);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_119, L_120, L_126);
		}

IL_02d3:
		{
			bool L_127 = Enumerator_MoveNext_m6365((&V_14), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_127)
			{
				goto IL_0251;
			}
		}

IL_02df:
		{
			IL2CPP_LEAVE(0x2F1, FINALLY_02e4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_02e4;
	}

FINALLY_02e4:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_128 = V_14;
		Enumerator_t1112  L_129 = L_128;
		Object_t * L_130 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_129);
		NullCheck(L_130);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_130);
		IL2CPP_END_FINALLY(740)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(740)
	{
		IL2CPP_JUMP_TBL(0x2F1, IL_02f1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_02f1:
	{
		goto IL_03c1;
	}

IL_02f6:
	{
		float L_131 = ___y;
		RectOffset_t741 * L_132 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_132);
		int32_t L_133 = RectOffset_get_top_m4940(L_132, /*hidden argument*/NULL);
		V_18 = ((float)((float)L_131-(float)(((float)L_133))));
		float L_134 = ___height;
		RectOffset_t741 * L_135 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin() */, __this);
		NullCheck(L_135);
		int32_t L_136 = RectOffset_get_vertical_m4935(L_135, /*hidden argument*/NULL);
		V_19 = ((float)((float)L_134+(float)(((float)L_136))));
		List_1_t878 * L_137 = (__this->___entries_10);
		NullCheck(L_137);
		Enumerator_t1112  L_138 = List_1_GetEnumerator_m6363(L_137, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_21 = L_138;
	}

IL_0323:
	try
	{ // begin try (depth: 1)
		{
			goto IL_03a3;
		}

IL_0328:
		{
			GUILayoutEntry_t877 * L_139 = Enumerator_get_Current_m6364((&V_21), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_20 = L_139;
			GUILayoutEntry_t877 * L_140 = V_20;
			NullCheck(L_140);
			int32_t L_141 = (L_140->___stretchHeight_6);
			if (!L_141)
			{
				goto IL_0369;
			}
		}

IL_033d:
		{
			GUILayoutEntry_t877 * L_142 = V_20;
			float L_143 = V_18;
			GUILayoutEntry_t877 * L_144 = V_20;
			NullCheck(L_144);
			RectOffset_t741 * L_145 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_144);
			NullCheck(L_145);
			int32_t L_146 = RectOffset_get_top_m4940(L_145, /*hidden argument*/NULL);
			float L_147 = V_19;
			GUILayoutEntry_t877 * L_148 = V_20;
			NullCheck(L_148);
			RectOffset_t741 * L_149 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_148);
			NullCheck(L_149);
			int32_t L_150 = RectOffset_get_vertical_m4935(L_149, /*hidden argument*/NULL);
			NullCheck(L_142);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_142, ((float)((float)L_143+(float)(((float)L_146)))), ((float)((float)L_147-(float)(((float)L_150)))));
			goto IL_03a3;
		}

IL_0369:
		{
			GUILayoutEntry_t877 * L_151 = V_20;
			float L_152 = V_18;
			GUILayoutEntry_t877 * L_153 = V_20;
			NullCheck(L_153);
			RectOffset_t741 * L_154 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_153);
			NullCheck(L_154);
			int32_t L_155 = RectOffset_get_top_m4940(L_154, /*hidden argument*/NULL);
			float L_156 = V_19;
			GUILayoutEntry_t877 * L_157 = V_20;
			NullCheck(L_157);
			RectOffset_t741 * L_158 = (RectOffset_t741 *)VirtFuncInvoker0< RectOffset_t741 * >::Invoke(4 /* UnityEngine.RectOffset UnityEngine.GUILayoutEntry::get_margin() */, L_157);
			NullCheck(L_158);
			int32_t L_159 = RectOffset_get_vertical_m4935(L_158, /*hidden argument*/NULL);
			GUILayoutEntry_t877 * L_160 = V_20;
			NullCheck(L_160);
			float L_161 = (L_160->___minHeight_2);
			GUILayoutEntry_t877 * L_162 = V_20;
			NullCheck(L_162);
			float L_163 = (L_162->___maxHeight_3);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
			float L_164 = Mathf_Clamp_m2768(NULL /*static, unused*/, ((float)((float)L_156-(float)(((float)L_159)))), L_161, L_163, /*hidden argument*/NULL);
			NullCheck(L_151);
			VirtActionInvoker2< float, float >::Invoke(8 /* System.Void UnityEngine.GUILayoutEntry::SetVertical(System.Single,System.Single) */, L_151, ((float)((float)L_152+(float)(((float)L_155)))), L_164);
		}

IL_03a3:
		{
			bool L_165 = Enumerator_MoveNext_m6365((&V_21), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_165)
			{
				goto IL_0328;
			}
		}

IL_03af:
		{
			IL2CPP_LEAVE(0x3C1, FINALLY_03b4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_03b4;
	}

FINALLY_03b4:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_166 = V_21;
		Enumerator_t1112  L_167 = L_166;
		Object_t * L_168 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_167);
		NullCheck(L_168);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_168);
		IL2CPP_END_FINALLY(948)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(948)
	{
		IL2CPP_JUMP_TBL(0x3C1, IL_03c1)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_03c1:
	{
		return;
	}
}
// System.String UnityEngine.GUILayoutGroup::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutEntry_t877_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1112_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m6363_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6364_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6365_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral885;
extern Il2CppCodeGenString* _stringLiteral896;
extern Il2CppCodeGenString* _stringLiteral897;
extern Il2CppCodeGenString* _stringLiteral898;
extern Il2CppCodeGenString* _stringLiteral899;
extern "C" String_t* GUILayoutGroup_ToString_m5200 (GUILayoutGroup_t873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		GUILayoutEntry_t877_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Enumerator_t1112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(641);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		List_1_GetEnumerator_m6363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484132);
		Enumerator_get_Current_m6364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484133);
		Enumerator_MoveNext_m6365_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484134);
		_stringLiteral885 = il2cpp_codegen_string_literal_from_index(885);
		_stringLiteral896 = il2cpp_codegen_string_literal_from_index(896);
		_stringLiteral897 = il2cpp_codegen_string_literal_from_index(897);
		_stringLiteral898 = il2cpp_codegen_string_literal_from_index(898);
		_stringLiteral899 = il2cpp_codegen_string_literal_from_index(899);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	GUILayoutEntry_t877 * V_3 = {0};
	Enumerator_t1112  V_4 = {0};
	String_t* V_5 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0023;
	}

IL_0013:
	{
		String_t* L_2 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3031(NULL /*static, unused*/, L_2, _stringLiteral885, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t877_il2cpp_TypeInfo_var);
		int32_t L_6 = ((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_7 = V_0;
		V_5 = L_7;
		ObjectU5BU5D_t470* L_8 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 5));
		String_t* L_9 = V_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0)) = (Object_t *)L_9;
		ObjectU5BU5D_t470* L_10 = L_8;
		String_t* L_11 = GUILayoutEntry_ToString_m5188(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 1);
		ArrayElementTypeCheck (L_10, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 1)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 2);
		ArrayElementTypeCheck (L_12, _stringLiteral896);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 2)) = (Object_t *)_stringLiteral896;
		ObjectU5BU5D_t470* L_13 = L_12;
		float L_14 = (__this->___childMinHeight_24);
		float L_15 = L_14;
		Object_t * L_16 = Box(Single_t531_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 3);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 3)) = (Object_t *)L_16;
		ObjectU5BU5D_t470* L_17 = L_13;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 4);
		ArrayElementTypeCheck (L_17, _stringLiteral897);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 4)) = (Object_t *)_stringLiteral897;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m3056(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t877_il2cpp_TypeInfo_var);
		int32_t L_19 = ((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_19+(int32_t)4));
		List_1_t878 * L_20 = (__this->___entries_10);
		NullCheck(L_20);
		Enumerator_t1112  L_21 = List_1_GetEnumerator_m6363(L_20, /*hidden argument*/List_1_GetEnumerator_m6363_MethodInfo_var);
		V_4 = L_21;
	}

IL_0082:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a1;
		}

IL_0087:
		{
			GUILayoutEntry_t877 * L_22 = Enumerator_get_Current_m6364((&V_4), /*hidden argument*/Enumerator_get_Current_m6364_MethodInfo_var);
			V_3 = L_22;
			String_t* L_23 = V_0;
			GUILayoutEntry_t877 * L_24 = V_3;
			NullCheck(L_24);
			String_t* L_25 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String UnityEngine.GUILayoutEntry::ToString() */, L_24);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_26 = String_Concat_m2834(NULL /*static, unused*/, L_23, L_25, _stringLiteral898, /*hidden argument*/NULL);
			V_0 = L_26;
		}

IL_00a1:
		{
			bool L_27 = Enumerator_MoveNext_m6365((&V_4), /*hidden argument*/Enumerator_MoveNext_m6365_MethodInfo_var);
			if (L_27)
			{
				goto IL_0087;
			}
		}

IL_00ad:
		{
			IL2CPP_LEAVE(0xBF, FINALLY_00b2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_00b2;
	}

FINALLY_00b2:
	{ // begin finally (depth: 1)
		Enumerator_t1112  L_28 = V_4;
		Enumerator_t1112  L_29 = L_28;
		Object_t * L_30 = Box(Enumerator_t1112_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_30);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_30);
		IL2CPP_END_FINALLY(178)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(178)
	{
		IL2CPP_JUMP_TBL(0xBF, IL_00bf)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_00bf:
	{
		String_t* L_31 = V_0;
		String_t* L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m2834(NULL /*static, unused*/, L_31, L_32, _stringLiteral899, /*hidden argument*/NULL);
		V_0 = L_33;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t877_il2cpp_TypeInfo_var);
		int32_t L_34 = ((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___indent_9;
		((GUILayoutEntry_t877_StaticFields*)GUILayoutEntry_t877_il2cpp_TypeInfo_var->static_fields)->___indent_9 = ((int32_t)((int32_t)L_34-(int32_t)4));
		String_t* L_35 = V_0;
		return L_35;
	}
}
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroup.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUIScrollGroup
#include "UnityEngine_UnityEngine_GUIScrollGroupMethodDeclarations.h"



// System.Void UnityEngine.GUIScrollGroup::.ctor()
extern "C" void GUIScrollGroup__ctor_m5201 (GUIScrollGroup_t879 * __this, const MethodInfo* method)
{
	{
		__this->___allowHorizontalScroll_33 = 1;
		__this->___allowVerticalScroll_34 = 1;
		GUILayoutGroup__ctor_m5189(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcWidth()
extern "C" void GUIScrollGroup_CalcWidth_m5202 (GUIScrollGroup_t879 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t877 *)__this)->___minWidth_0);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t877 *)__this)->___maxWidth_1);
		V_1 = L_1;
		bool L_2 = (__this->___allowHorizontalScroll_33);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = (0.0f);
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcWidth_m5196(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t877 *)__this)->___minWidth_0);
		__this->___calcMinWidth_27 = L_3;
		float L_4 = (((GUILayoutEntry_t877 *)__this)->___maxWidth_1);
		__this->___calcMaxWidth_28 = L_4;
		bool L_5 = (__this->___allowHorizontalScroll_33);
		if (!L_5)
		{
			goto IL_009e;
		}
	}
	{
		float L_6 = (((GUILayoutEntry_t877 *)__this)->___minWidth_0);
		if ((!(((float)L_6) > ((float)(32.0f)))))
		{
			goto IL_0073;
		}
	}
	{
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = (32.0f);
	}

IL_0073:
	{
		float L_7 = V_0;
		if ((((float)L_7) == ((float)(0.0f))))
		{
			goto IL_0085;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = L_8;
	}

IL_0085:
	{
		float L_9 = V_1;
		if ((((float)L_9) == ((float)(0.0f))))
		{
			goto IL_009e;
		}
	}
	{
		float L_10 = V_1;
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = L_10;
		((GUILayoutEntry_t877 *)__this)->___stretchWidth_5 = 0;
	}

IL_009e:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetHorizontal(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetHorizontal_m5203 (GUIScrollGroup_t879 * __this, float ___x, float ___width, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (__this->___needsVerticalScrollbar_36);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		float L_1 = ___width;
		GUIStyle_t273 * L_2 = (__this->___verticalScrollbar_38);
		NullCheck(L_2);
		float L_3 = GUIStyle_get_fixedWidth_m5319(L_2, /*hidden argument*/NULL);
		GUIStyle_t273 * L_4 = (__this->___verticalScrollbar_38);
		NullCheck(L_4);
		RectOffset_t741 * L_5 = GUIStyle_get_margin_m5314(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = RectOffset_get_left_m4939(L_5, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)((float)((float)L_1-(float)L_3))-(float)(((float)L_6))));
		goto IL_0030;
	}

IL_002f:
	{
		float L_7 = ___width;
		G_B3_0 = L_7;
	}

IL_0030:
	{
		V_0 = G_B3_0;
		bool L_8 = (__this->___allowHorizontalScroll_33);
		if (!L_8)
		{
			goto IL_0091;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinWidth_27);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0091;
		}
	}
	{
		__this->___needsHorizontalScrollbar_35 = 1;
		float L_11 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = L_11;
		float L_12 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = L_12;
		float L_13 = ___x;
		float L_14 = (__this->___calcMinWidth_27);
		GUILayoutGroup_SetHorizontal_m5197(__this, L_13, L_14, /*hidden argument*/NULL);
		Rect_t225 * L_15 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_16 = ___width;
		Rect_set_width_m2863(L_15, L_16, /*hidden argument*/NULL);
		float L_17 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_17;
		goto IL_00d6;
	}

IL_0091:
	{
		__this->___needsHorizontalScrollbar_35 = 0;
		bool L_18 = (__this->___allowHorizontalScroll_33);
		if (!L_18)
		{
			goto IL_00bb;
		}
	}
	{
		float L_19 = (__this->___calcMinWidth_27);
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = L_19;
		float L_20 = (__this->___calcMaxWidth_28);
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = L_20;
	}

IL_00bb:
	{
		float L_21 = ___x;
		float L_22 = V_0;
		GUILayoutGroup_SetHorizontal_m5197(__this, L_21, L_22, /*hidden argument*/NULL);
		Rect_t225 * L_23 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_24 = ___width;
		Rect_set_width_m2863(L_23, L_24, /*hidden argument*/NULL);
		float L_25 = V_0;
		__this->___clientWidth_31 = L_25;
	}

IL_00d6:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::CalcHeight()
extern "C" void GUIScrollGroup_CalcHeight_m5204 (GUIScrollGroup_t879 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		V_0 = L_0;
		float L_1 = (((GUILayoutEntry_t877 *)__this)->___maxHeight_3);
		V_1 = L_1;
		bool L_2 = (__this->___allowVerticalScroll_34);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = (0.0f);
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = (0.0f);
	}

IL_002f:
	{
		GUILayoutGroup_CalcHeight_m5198(__this, /*hidden argument*/NULL);
		float L_3 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		__this->___calcMinHeight_29 = L_3;
		float L_4 = (((GUILayoutEntry_t877 *)__this)->___maxHeight_3);
		__this->___calcMaxHeight_30 = L_4;
		bool L_5 = (__this->___needsHorizontalScrollbar_35);
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		GUIStyle_t273 * L_6 = (__this->___horizontalScrollbar_37);
		NullCheck(L_6);
		float L_7 = GUIStyle_get_fixedHeight_m5320(L_6, /*hidden argument*/NULL);
		GUIStyle_t273 * L_8 = (__this->___horizontalScrollbar_37);
		NullCheck(L_8);
		RectOffset_t741 * L_9 = GUIStyle_get_margin_m5314(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_top_m4940(L_9, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_7+(float)(((float)L_10))));
		float L_11 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		float L_12 = V_2;
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = ((float)((float)L_11+(float)L_12));
		float L_13 = (((GUILayoutEntry_t877 *)__this)->___maxHeight_3);
		float L_14 = V_2;
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = ((float)((float)L_13+(float)L_14));
	}

IL_0092:
	{
		bool L_15 = (__this->___allowVerticalScroll_34);
		if (!L_15)
		{
			goto IL_00e3;
		}
	}
	{
		float L_16 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		if ((!(((float)L_16) > ((float)(32.0f)))))
		{
			goto IL_00b8;
		}
	}
	{
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = (32.0f);
	}

IL_00b8:
	{
		float L_17 = V_0;
		if ((((float)L_17) == ((float)(0.0f))))
		{
			goto IL_00ca;
		}
	}
	{
		float L_18 = V_0;
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_18;
	}

IL_00ca:
	{
		float L_19 = V_1;
		if ((((float)L_19) == ((float)(0.0f))))
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_1;
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_20;
		((GUILayoutEntry_t877 *)__this)->___stretchHeight_6 = 0;
	}

IL_00e3:
	{
		return;
	}
}
// System.Void UnityEngine.GUIScrollGroup::SetVertical(System.Single,System.Single)
extern "C" void GUIScrollGroup_SetVertical_m5205 (GUIScrollGroup_t879 * __this, float ___y, float ___height, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = ___height;
		V_0 = L_0;
		bool L_1 = (__this->___needsHorizontalScrollbar_35);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		float L_2 = V_0;
		GUIStyle_t273 * L_3 = (__this->___horizontalScrollbar_37);
		NullCheck(L_3);
		float L_4 = GUIStyle_get_fixedHeight_m5320(L_3, /*hidden argument*/NULL);
		GUIStyle_t273 * L_5 = (__this->___horizontalScrollbar_37);
		NullCheck(L_5);
		RectOffset_t741 * L_6 = GUIStyle_get_margin_m5314(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_top_m4940(L_6, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_2-(float)((float)((float)L_4+(float)(((float)L_7))))));
	}

IL_002d:
	{
		bool L_8 = (__this->___allowVerticalScroll_34);
		if (!L_8)
		{
			goto IL_0139;
		}
	}
	{
		float L_9 = V_0;
		float L_10 = (__this->___calcMinHeight_29);
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0139;
		}
	}
	{
		bool L_11 = (__this->___needsHorizontalScrollbar_35);
		if (L_11)
		{
			goto IL_00db;
		}
	}
	{
		bool L_12 = (__this->___needsVerticalScrollbar_36);
		if (L_12)
		{
			goto IL_00db;
		}
	}
	{
		Rect_t225 * L_13 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_14 = Rect_get_width_m2798(L_13, /*hidden argument*/NULL);
		GUIStyle_t273 * L_15 = (__this->___verticalScrollbar_38);
		NullCheck(L_15);
		float L_16 = GUIStyle_get_fixedWidth_m5319(L_15, /*hidden argument*/NULL);
		GUIStyle_t273 * L_17 = (__this->___verticalScrollbar_38);
		NullCheck(L_17);
		RectOffset_t741 * L_18 = GUIStyle_get_margin_m5314(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = RectOffset_get_left_m4939(L_18, /*hidden argument*/NULL);
		__this->___clientWidth_31 = ((float)((float)((float)((float)L_14-(float)L_16))-(float)(((float)L_19))));
		float L_20 = (__this->___clientWidth_31);
		float L_21 = (__this->___calcMinWidth_27);
		if ((!(((float)L_20) < ((float)L_21))))
		{
			goto IL_00a6;
		}
	}
	{
		float L_22 = (__this->___calcMinWidth_27);
		__this->___clientWidth_31 = L_22;
	}

IL_00a6:
	{
		Rect_t225 * L_23 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_24 = Rect_get_width_m2798(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		Rect_t225 * L_25 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_26 = Rect_get_x_m2861(L_25, /*hidden argument*/NULL);
		float L_27 = (__this->___clientWidth_31);
		GUIScrollGroup_SetHorizontal_m5203(__this, L_26, L_27, /*hidden argument*/NULL);
		GUIScrollGroup_CalcHeight_m5204(__this, /*hidden argument*/NULL);
		Rect_t225 * L_28 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_29 = V_1;
		Rect_set_width_m2863(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00db:
	{
		float L_30 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		V_2 = L_30;
		float L_31 = (((GUILayoutEntry_t877 *)__this)->___maxHeight_3);
		V_3 = L_31;
		float L_32 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_32;
		float L_33 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_33;
		float L_34 = ___y;
		float L_35 = (__this->___calcMinHeight_29);
		GUILayoutGroup_SetVertical_m5199(__this, L_34, L_35, /*hidden argument*/NULL);
		float L_36 = V_2;
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_36;
		float L_37 = V_3;
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_37;
		Rect_t225 * L_38 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_39 = ___height;
		Rect_set_height_m2864(L_38, L_39, /*hidden argument*/NULL);
		float L_40 = (__this->___calcMinHeight_29);
		__this->___clientHeight_32 = L_40;
		goto IL_0177;
	}

IL_0139:
	{
		bool L_41 = (__this->___allowVerticalScroll_34);
		if (!L_41)
		{
			goto IL_015c;
		}
	}
	{
		float L_42 = (__this->___calcMinHeight_29);
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_42;
		float L_43 = (__this->___calcMaxHeight_30);
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_43;
	}

IL_015c:
	{
		float L_44 = ___y;
		float L_45 = V_0;
		GUILayoutGroup_SetVertical_m5199(__this, L_44, L_45, /*hidden argument*/NULL);
		Rect_t225 * L_46 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_47 = ___height;
		Rect_set_height_m2864(L_46, L_47, /*hidden argument*/NULL);
		float L_48 = V_0;
		__this->___clientHeight_32 = L_48;
	}

IL_0177:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.GUIWordWrapSizer::.ctor(UnityEngine.GUIStyle,UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern TypeInfo* GUILayoutEntry_t877_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern "C" void GUIWordWrapSizer__ctor_m5206 (GUIWordWrapSizer_t880 * __this, GUIStyle_t273 * ____style, GUIContent_t807 * ____content, GUILayoutOptionU5BU5D_t544* ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutEntry_t877_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(640);
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t273 * L_0 = ____style;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutEntry_t877_il2cpp_TypeInfo_var);
		GUILayoutEntry__ctor_m5176(__this, (0.0f), (0.0f), (0.0f), (0.0f), L_0, /*hidden argument*/NULL);
		GUIContent_t807 * L_1 = ____content;
		GUIContent_t807 * L_2 = (GUIContent_t807 *)il2cpp_codegen_object_new (GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent__ctor_m5281(L_2, L_1, /*hidden argument*/NULL);
		__this->___content_10 = L_2;
		GUILayoutOptionU5BU5D_t544* L_3 = ___options;
		GUILayoutEntry_ApplyOptions_m5187(__this, L_3, /*hidden argument*/NULL);
		float L_4 = (((GUILayoutEntry_t877 *)__this)->___minHeight_2);
		__this->___forcedMinHeight_11 = L_4;
		float L_5 = (((GUILayoutEntry_t877 *)__this)->___maxHeight_3);
		__this->___forcedMaxHeight_12 = L_5;
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcWidth()
extern "C" void GUIWordWrapSizer_CalcWidth_m5207 (GUIWordWrapSizer_t880 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = (((GUILayoutEntry_t877 *)__this)->___minWidth_0);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (((GUILayoutEntry_t877 *)__this)->___maxWidth_1);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}

IL_0020:
	{
		GUIStyle_t273 * L_2 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		GUIContent_t807 * L_3 = (__this->___content_10);
		NullCheck(L_2);
		GUIStyle_CalcMinMaxWidth_m5337(L_2, L_3, (&V_0), (&V_1), /*hidden argument*/NULL);
		float L_4 = (((GUILayoutEntry_t877 *)__this)->___minWidth_0);
		if ((!(((float)L_4) == ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		float L_5 = V_0;
		((GUILayoutEntry_t877 *)__this)->___minWidth_0 = L_5;
	}

IL_004c:
	{
		float L_6 = (((GUILayoutEntry_t877 *)__this)->___maxWidth_1);
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0063;
		}
	}
	{
		float L_7 = V_1;
		((GUILayoutEntry_t877 *)__this)->___maxWidth_1 = L_7;
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityEngine.GUIWordWrapSizer::CalcHeight()
extern "C" void GUIWordWrapSizer_CalcHeight_m5208 (GUIWordWrapSizer_t880 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (__this->___forcedMinHeight_11);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = (__this->___forcedMaxHeight_12);
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}

IL_0020:
	{
		GUIStyle_t273 * L_2 = GUILayoutEntry_get_style_m5179(__this, /*hidden argument*/NULL);
		GUIContent_t807 * L_3 = (__this->___content_10);
		Rect_t225 * L_4 = &(((GUILayoutEntry_t877 *)__this)->___rect_4);
		float L_5 = Rect_get_width_m2798(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_6 = GUIStyle_CalcHeight_m5334(L_2, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = (__this->___forcedMinHeight_11);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_8 = V_0;
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_8;
		goto IL_0065;
	}

IL_0059:
	{
		float L_9 = (__this->___forcedMinHeight_11);
		((GUILayoutEntry_t877 *)__this)->___minHeight_2 = L_9;
	}

IL_0065:
	{
		float L_10 = (__this->___forcedMaxHeight_12);
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = V_0;
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_11;
		goto IL_008d;
	}

IL_0081:
	{
		float L_12 = (__this->___forcedMaxHeight_12);
		((GUILayoutEntry_t877 *)__this)->___maxHeight_3 = L_12;
	}

IL_008d:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_TypeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.GUILayoutOption::.ctor(UnityEngine.GUILayoutOption/Type,System.Object)
extern "C" void GUILayoutOption__ctor_m5209 (GUILayoutOption_t882 * __this, int32_t ___type, Object_t * ___value, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___type;
		__this->___type_0 = L_0;
		Object_t * L_1 = ___value;
		__this->___value_1 = L_1;
		return;
	}
}
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIException.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ExitGUIException
#include "UnityEngine_UnityEngine_ExitGUIExceptionMethodDeclarations.h"



// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtility.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"


// System.Void UnityEngine.GUIUtility::.cctor()
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern "C" void GUIUtility__cctor_m5210 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t7  L_0 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		((GUIUtility_t884_StaticFields*)GUIUtility_t884_il2cpp_TypeInfo_var->static_fields)->___s_EditorScreenPointOffset_2 = L_0;
		((GUIUtility_t884_StaticFields*)GUIUtility_t884_il2cpp_TypeInfo_var->static_fields)->___s_HasKeyboardFocus_3 = 0;
		return;
	}
}
// System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
extern "C" String_t* GUIUtility_get_systemCopyBuffer_m5211 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*GUIUtility_get_systemCopyBuffer_m5211_ftn) ();
	static GUIUtility_get_systemCopyBuffer_m5211_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_get_systemCopyBuffer_m5211_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::get_systemCopyBuffer()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
extern "C" void GUIUtility_set_systemCopyBuffer_m5212 (Object_t * __this /* static, unused */, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIUtility_set_systemCopyBuffer_m5212_ftn) (String_t*);
	static GUIUtility_set_systemCopyBuffer_m5212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_set_systemCopyBuffer_m5212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::GetDefaultSkin()
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern "C" GUISkin_t869 * GUIUtility_GetDefaultSkin_m5213 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		int32_t L_0 = ((GUIUtility_t884_StaticFields*)GUIUtility_t884_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0;
		GUISkin_t869 * L_1 = GUIUtility_Internal_GetDefaultSkin_m5214(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
extern "C" GUISkin_t869 * GUIUtility_Internal_GetDefaultSkin_m5214 (Object_t * __this /* static, unused */, int32_t ___skinMode, const MethodInfo* method)
{
	typedef GUISkin_t869 * (*GUIUtility_Internal_GetDefaultSkin_m5214_ftn) (int32_t);
	static GUIUtility_Internal_GetDefaultSkin_m5214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetDefaultSkin_m5214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)");
	return _il2cpp_icall_func(___skinMode);
}
// System.Void UnityEngine.GUIUtility::BeginGUI(System.Int32,System.Int32,System.Int32)
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_BeginGUI_m5215 (Object_t * __this /* static, unused */, int32_t ___skinMode, int32_t ___instanceID, int32_t ___useGUILayout, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___skinMode;
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		((GUIUtility_t884_StaticFields*)GUIUtility_t884_il2cpp_TypeInfo_var->static_fields)->___s_SkinMode_0 = L_0;
		int32_t L_1 = ___instanceID;
		((GUIUtility_t884_StaticFields*)GUIUtility_t884_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_set_skin_m5143(NULL /*static, unused*/, (GUISkin_t869 *)NULL, /*hidden argument*/NULL);
		int32_t L_2 = ___useGUILayout;
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_3 = ___instanceID;
		IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
		GUILayoutUtility_SelectIDList_m5163(NULL /*static, unused*/, L_3, 0, /*hidden argument*/NULL);
		int32_t L_4 = ___instanceID;
		GUILayoutUtility_Begin_m5164(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_set_changed_m5147(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
extern "C" void GUIUtility_Internal_ExitGUI_m5216 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*GUIUtility_Internal_ExitGUI_m5216_ftn) ();
	static GUIUtility_Internal_ExitGUI_m5216_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_ExitGUI_m5216_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_ExitGUI()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.GUIUtility::EndGUI(System.Int32)
extern TypeInfo* GUILayoutUtility_t876_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern "C" void GUIUtility_EndGUI_m5217 (Object_t * __this /* static, unused */, int32_t ___layoutType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUILayoutUtility_t876_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(636);
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Event_t481 * L_0 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_0);
			int32_t L_1 = Event_get_type_m2786(L_0, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_1) == ((uint32_t)8))))
			{
				goto IL_0042;
			}
		}

IL_0010:
		{
			int32_t L_2 = ___layoutType;
			V_0 = L_2;
			int32_t L_3 = V_0;
			if (L_3 == 0)
			{
				goto IL_0029;
			}
			if (L_3 == 1)
			{
				goto IL_002e;
			}
			if (L_3 == 2)
			{
				goto IL_0038;
			}
		}

IL_0024:
		{
			goto IL_0042;
		}

IL_0029:
		{
			goto IL_0042;
		}

IL_002e:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUILayoutUtility_Layout_m5166(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0038:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUILayoutUtility_LayoutFromEditorWindow_m5167(NULL /*static, unused*/, /*hidden argument*/NULL);
			goto IL_0042;
		}

IL_0042:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
			int32_t L_4 = ((GUIUtility_t884_StaticFields*)GUIUtility_t884_il2cpp_TypeInfo_var->static_fields)->___s_OriginalID_1;
			IL2CPP_RUNTIME_CLASS_INIT(GUILayoutUtility_t876_il2cpp_TypeInfo_var);
			GUILayoutUtility_SelectIDList_m5163(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t807_il2cpp_TypeInfo_var);
			GUIContent_ClearStaticCache_m5285(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x5E, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m5216(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_005e:
	{
		return;
	}
}
// System.Boolean UnityEngine.GUIUtility::EndGUIFromException(System.Exception)
extern TypeInfo* ExitGUIException_t883_il2cpp_TypeInfo_var;
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern "C" bool GUIUtility_EndGUIFromException_m5218 (Object_t * __this /* static, unused */, Exception_t520 * ___exception, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExitGUIException_t883_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(645);
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t520 * L_0 = ___exception;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Exception_t520 * L_1 = ___exception;
		if (((ExitGUIException_t883 *)IsInst(L_1, ExitGUIException_t883_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		Exception_t520 * L_2 = ___exception;
		NullCheck(L_2);
		Exception_t520 * L_3 = (Exception_t520 *)VirtFuncInvoker0< Exception_t520 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_2);
		if (((ExitGUIException_t883 *)IsInst(L_3, ExitGUIException_t883_il2cpp_TypeInfo_var)))
		{
			goto IL_0025;
		}
	}
	{
		return 0;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		GUIUtility_Internal_ExitGUI_m5216(NULL /*static, unused*/, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Void UnityEngine.GUIUtility::CheckOnGUI()
extern TypeInfo* GUIUtility_t884_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral900;
extern "C" void GUIUtility_CheckOnGUI_m5219 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIUtility_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(635);
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		_stringLiteral900 = il2cpp_codegen_string_literal_from_index(900);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIUtility_t884_il2cpp_TypeInfo_var);
		int32_t L_0 = GUIUtility_Internal_GetGUIDepth_m5220(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		ArgumentException_t556 * L_1 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_1, _stringLiteral900, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
extern "C" int32_t GUIUtility_Internal_GetGUIDepth_m5220 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*GUIUtility_Internal_GetGUIDepth_m5220_ftn) ();
	static GUIUtility_Internal_GetGUIDepth_m5220_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIUtility_Internal_GetGUIDepth_m5220_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIUtility::Internal_GetGUIDepth()");
	return _il2cpp_icall_func();
}
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettings.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUISettings
#include "UnityEngine_UnityEngine_GUISettingsMethodDeclarations.h"

// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"


// System.Void UnityEngine.GUISettings::.ctor()
extern "C" void GUISettings__ctor_m5221 (GUISettings_t885 * __this, const MethodInfo* method)
{
	{
		__this->___m_DoubleClickSelectsWord_0 = 1;
		__this->___m_TripleClickSelectsLine_1 = 1;
		Color_t6  L_0 = Color_get_white_m2920(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_CursorColor_2 = L_0;
		__this->___m_CursorFlashSpeed_3 = (-1.0f);
		Color_t6  L_1 = {0};
		Color__ctor_m2747(&L_1, (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		__this->___m_SelectionColor_4 = L_1;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegate.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUISkin/SkinChangedDelegate
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegateMethodDeclarations.h"



// System.Void UnityEngine.GUISkin/SkinChangedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void SkinChangedDelegate__ctor_m5222 (SkinChangedDelegate_t886 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::Invoke()
extern "C" void SkinChangedDelegate_Invoke_m5223 (SkinChangedDelegate_t886 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SkinChangedDelegate_Invoke_m5223((SkinChangedDelegate_t886 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t886(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.GUISkin/SkinChangedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * SkinChangedDelegate_BeginInvoke_m5224 (SkinChangedDelegate_t886 * __this, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.GUISkin/SkinChangedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void SkinChangedDelegate_EndInvoke_m5225 (SkinChangedDelegate_t886 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleState.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2.h"
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10MethodDeclarations.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1MethodDeclarations.h"


// System.Void UnityEngine.GUISkin::.ctor()
extern TypeInfo* GUISettings_t885_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyleU5BU5D_t887_il2cpp_TypeInfo_var;
extern "C" void GUISkin__ctor_m5226 (GUISkin_t869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISettings_t885_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(646);
		GUIStyleU5BU5D_t887_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(647);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUISettings_t885 * L_0 = (GUISettings_t885 *)il2cpp_codegen_object_new (GUISettings_t885_il2cpp_TypeInfo_var);
		GUISettings__ctor_m5221(L_0, /*hidden argument*/NULL);
		__this->___m_Settings_24 = L_0;
		ScriptableObject__ctor_m4987(__this, /*hidden argument*/NULL);
		__this->___m_CustomStyles_23 = ((GUIStyleU5BU5D_t887*)SZArrayNew(GUIStyleU5BU5D_t887_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Void UnityEngine.GUISkin::OnEnable()
extern "C" void GUISkin_OnEnable_m5227 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Font UnityEngine.GUISkin::get_font()
extern "C" Font_t643 * GUISkin_get_font_m5228 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		Font_t643 * L_0 = (__this->___m_Font_2);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_font(UnityEngine.Font)
extern TypeInfo* GUISkin_t869_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void GUISkin_set_font_m5229 (GUISkin_t869 * __this, Font_t643 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t869_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t643 * L_0 = ___value;
		__this->___m_Font_2 = L_0;
		GUISkin_t869 * L_1 = ((GUISkin_t869_StaticFields*)GUISkin_t869_il2cpp_TypeInfo_var->static_fields)->___current_28;
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Font_t643 * L_3 = (__this->___m_Font_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m5327(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_box()
extern "C" GUIStyle_t273 * GUISkin_get_box_m5230 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_box_3);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_box(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_box_m5231 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_box_3 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C" GUIStyle_t273 * GUISkin_get_label_m5232 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_label_6);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_label(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_label_m5233 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_label_6 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textField()
extern "C" GUIStyle_t273 * GUISkin_get_textField_m5234 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_textField_7);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textField(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textField_m5235 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_textField_7 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textArea()
extern "C" GUIStyle_t273 * GUISkin_get_textArea_m5236 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_textArea_8);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_textArea(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_textArea_m5237 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_textArea_8 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_button()
extern "C" GUIStyle_t273 * GUISkin_get_button_m5238 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_button_4);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_button(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_button_m5239 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_button_4 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_toggle()
extern "C" GUIStyle_t273 * GUISkin_get_toggle_m5240 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_toggle_5);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_toggle(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_toggle_m5241 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_toggle_5 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_window()
extern "C" GUIStyle_t273 * GUISkin_get_window_m5242 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_window_9);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_window(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_window_m5243 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_window_9 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSlider()
extern "C" GUIStyle_t273 * GUISkin_get_horizontalSlider_m5244 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_horizontalSlider_10);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSlider_m5245 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_horizontalSlider_10 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalSliderThumb()
extern "C" GUIStyle_t273 * GUISkin_get_horizontalSliderThumb_m5246 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_horizontalSliderThumb_11);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalSliderThumb_m5247 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_horizontalSliderThumb_11 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSlider()
extern "C" GUIStyle_t273 * GUISkin_get_verticalSlider_m5248 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_verticalSlider_12);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSlider(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSlider_m5249 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_verticalSlider_12 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalSliderThumb()
extern "C" GUIStyle_t273 * GUISkin_get_verticalSliderThumb_m5250 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_verticalSliderThumb_13);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalSliderThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalSliderThumb_m5251 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_verticalSliderThumb_13 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbar()
extern "C" GUIStyle_t273 * GUISkin_get_horizontalScrollbar_m5252 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_horizontalScrollbar_14);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbar_m5253 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_horizontalScrollbar_14 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarThumb()
extern "C" GUIStyle_t273 * GUISkin_get_horizontalScrollbarThumb_m5254 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_horizontalScrollbarThumb_15);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarThumb_m5255 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_horizontalScrollbarThumb_15 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarLeftButton()
extern "C" GUIStyle_t273 * GUISkin_get_horizontalScrollbarLeftButton_m5256 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_horizontalScrollbarLeftButton_16);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarLeftButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m5257 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_horizontalScrollbarLeftButton_16 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_horizontalScrollbarRightButton()
extern "C" GUIStyle_t273 * GUISkin_get_horizontalScrollbarRightButton_m5258 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_horizontalScrollbarRightButton_17);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_horizontalScrollbarRightButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m5259 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_horizontalScrollbarRightButton_17 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbar()
extern "C" GUIStyle_t273 * GUISkin_get_verticalScrollbar_m5260 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_verticalScrollbar_18);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbar(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbar_m5261 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_verticalScrollbar_18 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarThumb()
extern "C" GUIStyle_t273 * GUISkin_get_verticalScrollbarThumb_m5262 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_verticalScrollbarThumb_19);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarThumb(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarThumb_m5263 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_verticalScrollbarThumb_19 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarUpButton()
extern "C" GUIStyle_t273 * GUISkin_get_verticalScrollbarUpButton_m5264 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_verticalScrollbarUpButton_20);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarUpButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarUpButton_m5265 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_verticalScrollbarUpButton_20 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_verticalScrollbarDownButton()
extern "C" GUIStyle_t273 * GUISkin_get_verticalScrollbarDownButton_m5266 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_verticalScrollbarDownButton_21);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_verticalScrollbarDownButton(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_verticalScrollbarDownButton_m5267 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_verticalScrollbarDownButton_21 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_scrollView()
extern "C" GUIStyle_t273 * GUISkin_get_scrollView_m5268 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = (__this->___m_ScrollView_22);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_scrollView(UnityEngine.GUIStyle)
extern "C" void GUISkin_set_scrollView_m5269 (GUISkin_t869 * __this, GUIStyle_t273 * ___value, const MethodInfo* method)
{
	{
		GUIStyle_t273 * L_0 = ___value;
		__this->___m_ScrollView_22 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle[] UnityEngine.GUISkin::get_customStyles()
extern "C" GUIStyleU5BU5D_t887* GUISkin_get_customStyles_m5270 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t887* L_0 = (__this->___m_CustomStyles_23);
		return L_0;
	}
}
// System.Void UnityEngine.GUISkin::set_customStyles(UnityEngine.GUIStyle[])
extern "C" void GUISkin_set_customStyles_m5271 (GUISkin_t869 * __this, GUIStyleU5BU5D_t887* ___value, const MethodInfo* method)
{
	{
		GUIStyleU5BU5D_t887* L_0 = ___value;
		__this->___m_CustomStyles_23 = L_0;
		GUISkin_Apply_m5274(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUISettings UnityEngine.GUISkin::get_settings()
extern "C" GUISettings_t885 * GUISkin_get_settings_m5272 (GUISkin_t869 * __this, const MethodInfo* method)
{
	{
		GUISettings_t885 * L_0 = (__this->___m_Settings_24);
		return L_0;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_error()
extern TypeInfo* GUISkin_t869_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t273 * GUISkin_get_error_m5273 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t869_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t273 * L_0 = ((GUISkin_t869_StaticFields*)GUISkin_t869_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t273 * L_1 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_1, /*hidden argument*/NULL);
		((GUISkin_t869_StaticFields*)GUISkin_t869_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25 = L_1;
	}

IL_0014:
	{
		GUIStyle_t273 * L_2 = ((GUISkin_t869_StaticFields*)GUISkin_t869_il2cpp_TypeInfo_var->static_fields)->___ms_Error_25;
		return L_2;
	}
}
// System.Void UnityEngine.GUISkin::Apply()
extern Il2CppCodeGenString* _stringLiteral901;
extern "C" void GUISkin_Apply_m5274 (GUISkin_t869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral901 = il2cpp_codegen_string_literal_from_index(901);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleU5BU5D_t887* L_0 = (__this->___m_CustomStyles_23);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral901, /*hidden argument*/NULL);
	}

IL_0015:
	{
		GUISkin_BuildStyleCache_m5275(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUISkin::BuildStyleCache()
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern TypeInfo* StringComparer_t1113_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t888_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m6368_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral902;
extern Il2CppCodeGenString* _stringLiteral903;
extern Il2CppCodeGenString* _stringLiteral904;
extern Il2CppCodeGenString* _stringLiteral905;
extern Il2CppCodeGenString* _stringLiteral906;
extern Il2CppCodeGenString* _stringLiteral907;
extern Il2CppCodeGenString* _stringLiteral908;
extern Il2CppCodeGenString* _stringLiteral909;
extern Il2CppCodeGenString* _stringLiteral910;
extern Il2CppCodeGenString* _stringLiteral911;
extern Il2CppCodeGenString* _stringLiteral912;
extern Il2CppCodeGenString* _stringLiteral913;
extern Il2CppCodeGenString* _stringLiteral914;
extern Il2CppCodeGenString* _stringLiteral915;
extern Il2CppCodeGenString* _stringLiteral916;
extern Il2CppCodeGenString* _stringLiteral917;
extern Il2CppCodeGenString* _stringLiteral918;
extern Il2CppCodeGenString* _stringLiteral919;
extern Il2CppCodeGenString* _stringLiteral920;
extern Il2CppCodeGenString* _stringLiteral921;
extern "C" void GUISkin_BuildStyleCache_m5275 (GUISkin_t869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		StringComparer_t1113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(649);
		Dictionary_2_t888_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(650);
		Dictionary_2__ctor_m6368_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484136);
		_stringLiteral902 = il2cpp_codegen_string_literal_from_index(902);
		_stringLiteral903 = il2cpp_codegen_string_literal_from_index(903);
		_stringLiteral904 = il2cpp_codegen_string_literal_from_index(904);
		_stringLiteral905 = il2cpp_codegen_string_literal_from_index(905);
		_stringLiteral906 = il2cpp_codegen_string_literal_from_index(906);
		_stringLiteral907 = il2cpp_codegen_string_literal_from_index(907);
		_stringLiteral908 = il2cpp_codegen_string_literal_from_index(908);
		_stringLiteral909 = il2cpp_codegen_string_literal_from_index(909);
		_stringLiteral910 = il2cpp_codegen_string_literal_from_index(910);
		_stringLiteral911 = il2cpp_codegen_string_literal_from_index(911);
		_stringLiteral912 = il2cpp_codegen_string_literal_from_index(912);
		_stringLiteral913 = il2cpp_codegen_string_literal_from_index(913);
		_stringLiteral914 = il2cpp_codegen_string_literal_from_index(914);
		_stringLiteral915 = il2cpp_codegen_string_literal_from_index(915);
		_stringLiteral916 = il2cpp_codegen_string_literal_from_index(916);
		_stringLiteral917 = il2cpp_codegen_string_literal_from_index(917);
		_stringLiteral918 = il2cpp_codegen_string_literal_from_index(918);
		_stringLiteral919 = il2cpp_codegen_string_literal_from_index(919);
		_stringLiteral920 = il2cpp_codegen_string_literal_from_index(920);
		_stringLiteral921 = il2cpp_codegen_string_literal_from_index(921);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GUIStyle_t273 * L_0 = (__this->___m_box_3);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		GUIStyle_t273 * L_1 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_1, /*hidden argument*/NULL);
		__this->___m_box_3 = L_1;
	}

IL_0016:
	{
		GUIStyle_t273 * L_2 = (__this->___m_button_4);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		GUIStyle_t273 * L_3 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_3, /*hidden argument*/NULL);
		__this->___m_button_4 = L_3;
	}

IL_002c:
	{
		GUIStyle_t273 * L_4 = (__this->___m_toggle_5);
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		GUIStyle_t273 * L_5 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_5, /*hidden argument*/NULL);
		__this->___m_toggle_5 = L_5;
	}

IL_0042:
	{
		GUIStyle_t273 * L_6 = (__this->___m_label_6);
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		GUIStyle_t273 * L_7 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_7, /*hidden argument*/NULL);
		__this->___m_label_6 = L_7;
	}

IL_0058:
	{
		GUIStyle_t273 * L_8 = (__this->___m_window_9);
		if (L_8)
		{
			goto IL_006e;
		}
	}
	{
		GUIStyle_t273 * L_9 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_9, /*hidden argument*/NULL);
		__this->___m_window_9 = L_9;
	}

IL_006e:
	{
		GUIStyle_t273 * L_10 = (__this->___m_textField_7);
		if (L_10)
		{
			goto IL_0084;
		}
	}
	{
		GUIStyle_t273 * L_11 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_11, /*hidden argument*/NULL);
		__this->___m_textField_7 = L_11;
	}

IL_0084:
	{
		GUIStyle_t273 * L_12 = (__this->___m_textArea_8);
		if (L_12)
		{
			goto IL_009a;
		}
	}
	{
		GUIStyle_t273 * L_13 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_13, /*hidden argument*/NULL);
		__this->___m_textArea_8 = L_13;
	}

IL_009a:
	{
		GUIStyle_t273 * L_14 = (__this->___m_horizontalSlider_10);
		if (L_14)
		{
			goto IL_00b0;
		}
	}
	{
		GUIStyle_t273 * L_15 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_15, /*hidden argument*/NULL);
		__this->___m_horizontalSlider_10 = L_15;
	}

IL_00b0:
	{
		GUIStyle_t273 * L_16 = (__this->___m_horizontalSliderThumb_11);
		if (L_16)
		{
			goto IL_00c6;
		}
	}
	{
		GUIStyle_t273 * L_17 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_17, /*hidden argument*/NULL);
		__this->___m_horizontalSliderThumb_11 = L_17;
	}

IL_00c6:
	{
		GUIStyle_t273 * L_18 = (__this->___m_verticalSlider_12);
		if (L_18)
		{
			goto IL_00dc;
		}
	}
	{
		GUIStyle_t273 * L_19 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_19, /*hidden argument*/NULL);
		__this->___m_verticalSlider_12 = L_19;
	}

IL_00dc:
	{
		GUIStyle_t273 * L_20 = (__this->___m_verticalSliderThumb_13);
		if (L_20)
		{
			goto IL_00f2;
		}
	}
	{
		GUIStyle_t273 * L_21 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_21, /*hidden argument*/NULL);
		__this->___m_verticalSliderThumb_13 = L_21;
	}

IL_00f2:
	{
		GUIStyle_t273 * L_22 = (__this->___m_horizontalScrollbar_14);
		if (L_22)
		{
			goto IL_0108;
		}
	}
	{
		GUIStyle_t273 * L_23 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_23, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbar_14 = L_23;
	}

IL_0108:
	{
		GUIStyle_t273 * L_24 = (__this->___m_horizontalScrollbarThumb_15);
		if (L_24)
		{
			goto IL_011e;
		}
	}
	{
		GUIStyle_t273 * L_25 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_25, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarThumb_15 = L_25;
	}

IL_011e:
	{
		GUIStyle_t273 * L_26 = (__this->___m_horizontalScrollbarLeftButton_16);
		if (L_26)
		{
			goto IL_0134;
		}
	}
	{
		GUIStyle_t273 * L_27 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_27, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarLeftButton_16 = L_27;
	}

IL_0134:
	{
		GUIStyle_t273 * L_28 = (__this->___m_horizontalScrollbarRightButton_17);
		if (L_28)
		{
			goto IL_014a;
		}
	}
	{
		GUIStyle_t273 * L_29 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_29, /*hidden argument*/NULL);
		__this->___m_horizontalScrollbarRightButton_17 = L_29;
	}

IL_014a:
	{
		GUIStyle_t273 * L_30 = (__this->___m_verticalScrollbar_18);
		if (L_30)
		{
			goto IL_0160;
		}
	}
	{
		GUIStyle_t273 * L_31 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_31, /*hidden argument*/NULL);
		__this->___m_verticalScrollbar_18 = L_31;
	}

IL_0160:
	{
		GUIStyle_t273 * L_32 = (__this->___m_verticalScrollbarThumb_19);
		if (L_32)
		{
			goto IL_0176;
		}
	}
	{
		GUIStyle_t273 * L_33 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_33, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarThumb_19 = L_33;
	}

IL_0176:
	{
		GUIStyle_t273 * L_34 = (__this->___m_verticalScrollbarUpButton_20);
		if (L_34)
		{
			goto IL_018c;
		}
	}
	{
		GUIStyle_t273 * L_35 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_35, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarUpButton_20 = L_35;
	}

IL_018c:
	{
		GUIStyle_t273 * L_36 = (__this->___m_verticalScrollbarDownButton_21);
		if (L_36)
		{
			goto IL_01a2;
		}
	}
	{
		GUIStyle_t273 * L_37 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_37, /*hidden argument*/NULL);
		__this->___m_verticalScrollbarDownButton_21 = L_37;
	}

IL_01a2:
	{
		GUIStyle_t273 * L_38 = (__this->___m_ScrollView_22);
		if (L_38)
		{
			goto IL_01b8;
		}
	}
	{
		GUIStyle_t273 * L_39 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_39, /*hidden argument*/NULL);
		__this->___m_ScrollView_22 = L_39;
	}

IL_01b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t1113_il2cpp_TypeInfo_var);
		StringComparer_t1113 * L_40 = StringComparer_get_OrdinalIgnoreCase_m6367(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_41 = (Dictionary_2_t888 *)il2cpp_codegen_object_new (Dictionary_2_t888_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m6368(L_41, L_40, /*hidden argument*/Dictionary_2__ctor_m6368_MethodInfo_var);
		__this->___styles_26 = L_41;
		Dictionary_2_t888 * L_42 = (__this->___styles_26);
		GUIStyle_t273 * L_43 = (__this->___m_box_3);
		NullCheck(L_42);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_42, _stringLiteral902, L_43);
		GUIStyle_t273 * L_44 = (__this->___m_box_3);
		NullCheck(L_44);
		GUIStyle_set_name_m5312(L_44, _stringLiteral902, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_45 = (__this->___styles_26);
		GUIStyle_t273 * L_46 = (__this->___m_button_4);
		NullCheck(L_45);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_45, _stringLiteral903, L_46);
		GUIStyle_t273 * L_47 = (__this->___m_button_4);
		NullCheck(L_47);
		GUIStyle_set_name_m5312(L_47, _stringLiteral903, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_48 = (__this->___styles_26);
		GUIStyle_t273 * L_49 = (__this->___m_toggle_5);
		NullCheck(L_48);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_48, _stringLiteral904, L_49);
		GUIStyle_t273 * L_50 = (__this->___m_toggle_5);
		NullCheck(L_50);
		GUIStyle_set_name_m5312(L_50, _stringLiteral904, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_51 = (__this->___styles_26);
		GUIStyle_t273 * L_52 = (__this->___m_label_6);
		NullCheck(L_51);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_51, _stringLiteral905, L_52);
		GUIStyle_t273 * L_53 = (__this->___m_label_6);
		NullCheck(L_53);
		GUIStyle_set_name_m5312(L_53, _stringLiteral905, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_54 = (__this->___styles_26);
		GUIStyle_t273 * L_55 = (__this->___m_window_9);
		NullCheck(L_54);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_54, _stringLiteral906, L_55);
		GUIStyle_t273 * L_56 = (__this->___m_window_9);
		NullCheck(L_56);
		GUIStyle_set_name_m5312(L_56, _stringLiteral906, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_57 = (__this->___styles_26);
		GUIStyle_t273 * L_58 = (__this->___m_textField_7);
		NullCheck(L_57);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_57, _stringLiteral907, L_58);
		GUIStyle_t273 * L_59 = (__this->___m_textField_7);
		NullCheck(L_59);
		GUIStyle_set_name_m5312(L_59, _stringLiteral907, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_60 = (__this->___styles_26);
		GUIStyle_t273 * L_61 = (__this->___m_textArea_8);
		NullCheck(L_60);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_60, _stringLiteral908, L_61);
		GUIStyle_t273 * L_62 = (__this->___m_textArea_8);
		NullCheck(L_62);
		GUIStyle_set_name_m5312(L_62, _stringLiteral908, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_63 = (__this->___styles_26);
		GUIStyle_t273 * L_64 = (__this->___m_horizontalSlider_10);
		NullCheck(L_63);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_63, _stringLiteral909, L_64);
		GUIStyle_t273 * L_65 = (__this->___m_horizontalSlider_10);
		NullCheck(L_65);
		GUIStyle_set_name_m5312(L_65, _stringLiteral909, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_66 = (__this->___styles_26);
		GUIStyle_t273 * L_67 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_66);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_66, _stringLiteral910, L_67);
		GUIStyle_t273 * L_68 = (__this->___m_horizontalSliderThumb_11);
		NullCheck(L_68);
		GUIStyle_set_name_m5312(L_68, _stringLiteral910, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_69 = (__this->___styles_26);
		GUIStyle_t273 * L_70 = (__this->___m_verticalSlider_12);
		NullCheck(L_69);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_69, _stringLiteral911, L_70);
		GUIStyle_t273 * L_71 = (__this->___m_verticalSlider_12);
		NullCheck(L_71);
		GUIStyle_set_name_m5312(L_71, _stringLiteral911, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_72 = (__this->___styles_26);
		GUIStyle_t273 * L_73 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_72);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_72, _stringLiteral912, L_73);
		GUIStyle_t273 * L_74 = (__this->___m_verticalSliderThumb_13);
		NullCheck(L_74);
		GUIStyle_set_name_m5312(L_74, _stringLiteral912, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_75 = (__this->___styles_26);
		GUIStyle_t273 * L_76 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_75);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_75, _stringLiteral913, L_76);
		GUIStyle_t273 * L_77 = (__this->___m_horizontalScrollbar_14);
		NullCheck(L_77);
		GUIStyle_set_name_m5312(L_77, _stringLiteral913, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_78 = (__this->___styles_26);
		GUIStyle_t273 * L_79 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_78);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_78, _stringLiteral914, L_79);
		GUIStyle_t273 * L_80 = (__this->___m_horizontalScrollbarThumb_15);
		NullCheck(L_80);
		GUIStyle_set_name_m5312(L_80, _stringLiteral914, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_81 = (__this->___styles_26);
		GUIStyle_t273 * L_82 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_81);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_81, _stringLiteral915, L_82);
		GUIStyle_t273 * L_83 = (__this->___m_horizontalScrollbarLeftButton_16);
		NullCheck(L_83);
		GUIStyle_set_name_m5312(L_83, _stringLiteral915, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_84 = (__this->___styles_26);
		GUIStyle_t273 * L_85 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_84);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_84, _stringLiteral916, L_85);
		GUIStyle_t273 * L_86 = (__this->___m_horizontalScrollbarRightButton_17);
		NullCheck(L_86);
		GUIStyle_set_name_m5312(L_86, _stringLiteral916, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_87 = (__this->___styles_26);
		GUIStyle_t273 * L_88 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_87);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_87, _stringLiteral917, L_88);
		GUIStyle_t273 * L_89 = (__this->___m_verticalScrollbar_18);
		NullCheck(L_89);
		GUIStyle_set_name_m5312(L_89, _stringLiteral917, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_90 = (__this->___styles_26);
		GUIStyle_t273 * L_91 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_90);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_90, _stringLiteral918, L_91);
		GUIStyle_t273 * L_92 = (__this->___m_verticalScrollbarThumb_19);
		NullCheck(L_92);
		GUIStyle_set_name_m5312(L_92, _stringLiteral918, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_93 = (__this->___styles_26);
		GUIStyle_t273 * L_94 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_93);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_93, _stringLiteral919, L_94);
		GUIStyle_t273 * L_95 = (__this->___m_verticalScrollbarUpButton_20);
		NullCheck(L_95);
		GUIStyle_set_name_m5312(L_95, _stringLiteral919, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_96 = (__this->___styles_26);
		GUIStyle_t273 * L_97 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_96);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_96, _stringLiteral920, L_97);
		GUIStyle_t273 * L_98 = (__this->___m_verticalScrollbarDownButton_21);
		NullCheck(L_98);
		GUIStyle_set_name_m5312(L_98, _stringLiteral920, /*hidden argument*/NULL);
		Dictionary_2_t888 * L_99 = (__this->___styles_26);
		GUIStyle_t273 * L_100 = (__this->___m_ScrollView_22);
		NullCheck(L_99);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_99, _stringLiteral921, L_100);
		GUIStyle_t273 * L_101 = (__this->___m_ScrollView_22);
		NullCheck(L_101);
		GUIStyle_set_name_m5312(L_101, _stringLiteral921, /*hidden argument*/NULL);
		GUIStyleU5BU5D_t887* L_102 = (__this->___m_CustomStyles_23);
		if (!L_102)
		{
			goto IL_0516;
		}
	}
	{
		V_0 = 0;
		goto IL_0508;
	}

IL_04d2:
	{
		GUIStyleU5BU5D_t887* L_103 = (__this->___m_CustomStyles_23);
		int32_t L_104 = V_0;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, L_104);
		int32_t L_105 = L_104;
		if ((*(GUIStyle_t273 **)(GUIStyle_t273 **)SZArrayLdElema(L_103, L_105)))
		{
			goto IL_04e4;
		}
	}
	{
		goto IL_0504;
	}

IL_04e4:
	{
		Dictionary_2_t888 * L_106 = (__this->___styles_26);
		GUIStyleU5BU5D_t887* L_107 = (__this->___m_CustomStyles_23);
		int32_t L_108 = V_0;
		NullCheck(L_107);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_107, L_108);
		int32_t L_109 = L_108;
		NullCheck((*(GUIStyle_t273 **)(GUIStyle_t273 **)SZArrayLdElema(L_107, L_109)));
		String_t* L_110 = GUIStyle_get_name_m5311((*(GUIStyle_t273 **)(GUIStyle_t273 **)SZArrayLdElema(L_107, L_109)), /*hidden argument*/NULL);
		GUIStyleU5BU5D_t887* L_111 = (__this->___m_CustomStyles_23);
		int32_t L_112 = V_0;
		NullCheck(L_111);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_111, L_112);
		int32_t L_113 = L_112;
		NullCheck(L_106);
		VirtActionInvoker2< String_t*, GUIStyle_t273 * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::set_Item(!0,!1) */, L_106, L_110, (*(GUIStyle_t273 **)(GUIStyle_t273 **)SZArrayLdElema(L_111, L_113)));
	}

IL_0504:
	{
		int32_t L_114 = V_0;
		V_0 = ((int32_t)((int32_t)L_114+(int32_t)1));
	}

IL_0508:
	{
		int32_t L_115 = V_0;
		GUIStyleU5BU5D_t887* L_116 = (__this->___m_CustomStyles_23);
		NullCheck(L_116);
		if ((((int32_t)L_115) < ((int32_t)(((int32_t)(((Array_t *)L_116)->max_length))))))
		{
			goto IL_04d2;
		}
	}

IL_0516:
	{
		GUIStyle_t273 * L_117 = GUISkin_get_error_m5273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_117);
		GUIStyle_set_stretchHeight_m5324(L_117, 1, /*hidden argument*/NULL);
		GUIStyle_t273 * L_118 = GUISkin_get_error_m5273(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_118);
		GUIStyleState_t523 * L_119 = GUIStyle_get_normal_m3063(L_118, /*hidden argument*/NULL);
		Color_t6  L_120 = Color_get_red_m2764(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_119);
		GUIStyleState_set_textColor_m3064(L_119, L_120, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::GetStyle(System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t893_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral922;
extern Il2CppCodeGenString* _stringLiteral923;
extern Il2CppCodeGenString* _stringLiteral924;
extern "C" GUIStyle_t273 * GUISkin_GetStyle_m5276 (GUISkin_t869 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		EventType_t893_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral922 = il2cpp_codegen_string_literal_from_index(922);
		_stringLiteral923 = il2cpp_codegen_string_literal_from_index(923);
		_stringLiteral924 = il2cpp_codegen_string_literal_from_index(924);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t273 * V_0 = {0};
	{
		String_t* L_0 = ___styleName;
		GUIStyle_t273 * L_1 = GUISkin_FindStyle_m5277(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GUIStyle_t273 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		GUIStyle_t273 * L_3 = V_0;
		return L_3;
	}

IL_0010:
	{
		ObjectU5BU5D_t470* L_4 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 6));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral922);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0)) = (Object_t *)_stringLiteral922;
		ObjectU5BU5D_t470* L_5 = L_4;
		String_t* L_6 = ___styleName;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1)) = (Object_t *)L_6;
		ObjectU5BU5D_t470* L_7 = L_5;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral923);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2)) = (Object_t *)_stringLiteral923;
		ObjectU5BU5D_t470* L_8 = L_7;
		String_t* L_9 = Object_get_name_m2979(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_9;
		ObjectU5BU5D_t470* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral924);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 4)) = (Object_t *)_stringLiteral924;
		ObjectU5BU5D_t470* L_11 = L_10;
		Event_t481 * L_12 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = Event_get_type_m2786(L_12, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(EventType_t893_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 5)) = (Object_t *)L_15;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3056(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Debug_LogWarning_m2781(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GUIStyle_t273 * L_17 = GUISkin_get_error_m5273(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
// UnityEngine.GUIStyle UnityEngine.GUISkin::FindStyle(System.String)
extern Il2CppCodeGenString* _stringLiteral925;
extern "C" GUIStyle_t273 * GUISkin_FindStyle_m5277 (GUISkin_t869 * __this, String_t* ___styleName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral925 = il2cpp_codegen_string_literal_from_index(925);
		s_Il2CppMethodIntialized = true;
	}
	GUIStyle_t273 * V_0 = {0};
	{
		bool L_0 = Object_op_Equality_m2716(NULL /*static, unused*/, __this, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral925, /*hidden argument*/NULL);
		return (GUIStyle_t273 *)NULL;
	}

IL_0018:
	{
		Dictionary_2_t888 * L_1 = (__this->___styles_26);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		GUISkin_BuildStyleCache_m5275(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		Dictionary_2_t888 * L_2 = (__this->___styles_26);
		String_t* L_3 = ___styleName;
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, GUIStyle_t273 ** >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>::TryGetValue(!0,!1&) */, L_2, L_3, (&V_0));
		if (!L_4)
		{
			goto IL_003e;
		}
	}
	{
		GUIStyle_t273 * L_5 = V_0;
		return L_5;
	}

IL_003e:
	{
		return (GUIStyle_t273 *)NULL;
	}
}
// System.Void UnityEngine.GUISkin::MakeCurrent()
extern TypeInfo* GUISkin_t869_il2cpp_TypeInfo_var;
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void GUISkin_MakeCurrent_m5278 (GUISkin_t869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUISkin_t869_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(648);
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUISkin_t869_StaticFields*)GUISkin_t869_il2cpp_TypeInfo_var->static_fields)->___current_28 = __this;
		Font_t643 * L_0 = GUISkin_get_font_m5228(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_SetDefaultFont_m5327(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SkinChangedDelegate_t886 * L_1 = ((GUISkin_t869_StaticFields*)GUISkin_t869_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		SkinChangedDelegate_t886 * L_2 = ((GUISkin_t869_StaticFields*)GUISkin_t869_il2cpp_TypeInfo_var->static_fields)->___m_SkinChanged_27;
		NullCheck(L_2);
		SkinChangedDelegate_Invoke_m5223(L_2, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.GUISkin::GetEnumerator()
extern TypeInfo* Enumerator_t1115_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m6369_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m6370_MethodInfo_var;
extern "C" Object_t * GUISkin_GetEnumerator_m5279 (GUISkin_t869 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t1115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(651);
		Dictionary_2_get_Values_m6369_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484137);
		ValueCollection_GetEnumerator_m6370_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484138);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t888 * L_0 = (__this->___styles_26);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		GUISkin_BuildStyleCache_m5275(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Dictionary_2_t888 * L_1 = (__this->___styles_26);
		NullCheck(L_1);
		ValueCollection_t1114 * L_2 = Dictionary_2_get_Values_m6369(L_1, /*hidden argument*/Dictionary_2_get_Values_m6369_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t1115  L_3 = ValueCollection_GetEnumerator_m6370(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m6370_MethodInfo_var);
		Enumerator_t1115  L_4 = L_3;
		Object_t * L_5 = Box(Enumerator_t1115_il2cpp_TypeInfo_var, &L_4);
		return (Object_t *)L_5;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.GUIContent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m5280 (GUIContent_t807 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m4784 (GUIContent_t807 * __this, String_t* ___text, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_2 = ___text;
		__this->___m_Text_0 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.ctor(UnityEngine.GUIContent)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent__ctor_m5281 (GUIContent_t807 * __this, GUIContent_t807 * ___src, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Text_0 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_Tooltip_2 = L_1;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		GUIContent_t807 * L_2 = ___src;
		NullCheck(L_2);
		String_t* L_3 = (L_2->___m_Text_0);
		__this->___m_Text_0 = L_3;
		GUIContent_t807 * L_4 = ___src;
		NullCheck(L_4);
		Texture_t221 * L_5 = (L_4->___m_Image_1);
		__this->___m_Image_1 = L_5;
		GUIContent_t807 * L_6 = ___src;
		NullCheck(L_6);
		String_t* L_7 = (L_6->___m_Tooltip_2);
		__this->___m_Tooltip_2 = L_7;
		return;
	}
}
// System.Void UnityEngine.GUIContent::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern "C" void GUIContent__cctor_m5282 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		GUIContent_t807 * L_1 = (GUIContent_t807 *)il2cpp_codegen_object_new (GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent__ctor_m4784(L_1, L_0, /*hidden argument*/NULL);
		((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___none_3 = L_1;
		GUIContent_t807 * L_2 = (GUIContent_t807 *)il2cpp_codegen_object_new (GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent__ctor_m5280(L_2, /*hidden argument*/NULL);
		((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Text_4 = L_2;
		GUIContent_t807 * L_3 = (GUIContent_t807 *)il2cpp_codegen_object_new (GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent__ctor_m5280(L_3, /*hidden argument*/NULL);
		((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Image_5 = L_3;
		GUIContent_t807 * L_4 = (GUIContent_t807 *)il2cpp_codegen_object_new (GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent__ctor_m5280(L_4, /*hidden argument*/NULL);
		((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_6 = L_4;
		return;
	}
}
// System.String UnityEngine.GUIContent::get_text()
extern "C" String_t* GUIContent_get_text_m4783 (GUIContent_t807 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Text_0);
		return L_0;
	}
}
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C" void GUIContent_set_text_m5283 (GUIContent_t807 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_Text_0 = L_0;
		return;
	}
}
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" GUIContent_t807 * GUIContent_Temp_m5284 (Object_t * __this /* static, unused */, String_t* ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent_t807 * L_0 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		String_t* L_1 = ___t;
		NullCheck(L_0);
		L_0->___m_Text_0 = L_1;
		GUIContent_t807 * L_2 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_2);
		L_2->___m_Tooltip_2 = L_3;
		GUIContent_t807 * L_4 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		return L_4;
	}
}
// System.Void UnityEngine.GUIContent::ClearStaticCache()
extern TypeInfo* GUIContent_t807_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GUIContent_ClearStaticCache_m5285 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIContent_t807_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(541);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t807_il2cpp_TypeInfo_var);
		GUIContent_t807 * L_0 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		NullCheck(L_0);
		L_0->___m_Text_0 = (String_t*)NULL;
		GUIContent_t807 * L_1 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Text_4;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_1);
		L_1->___m_Tooltip_2 = L_2;
		GUIContent_t807 * L_3 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Image_5;
		NullCheck(L_3);
		L_3->___m_Image_1 = (Texture_t221 *)NULL;
		GUIContent_t807 * L_4 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_Image_5;
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_4);
		L_4->___m_Tooltip_2 = L_5;
		GUIContent_t807 * L_6 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_6;
		NullCheck(L_6);
		L_6->___m_Text_0 = (String_t*)NULL;
		GUIContent_t807 * L_7 = ((GUIContent_t807_StaticFields*)GUIContent_t807_il2cpp_TypeInfo_var->static_fields)->___s_TextImage_6;
		NullCheck(L_7);
		L_7->___m_Image_1 = (Texture_t221 *)NULL;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.GUIStyleState::.ctor()
extern "C" void GUIStyleState__ctor_m5286 (GUIStyleState_t523 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		GUIStyleState_Init_m5289(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void GUIStyleState__ctor_m5287 (GUIStyleState_t523 * __this, GUIStyle_t273 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		GUIStyle_t273 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		Texture2D_t9 * L_2 = GUIStyleState_GetBackgroundInternal_m5291(__this, /*hidden argument*/NULL);
		__this->___m_Background_2 = L_2;
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Finalize()
extern "C" void GUIStyleState_Finalize_m5288 (GUIStyleState_t523 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t273 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			GUIStyleState_Cleanup_m5290(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::Init()
extern "C" void GUIStyleState_Init_m5289 (GUIStyleState_t523 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Init_m5289_ftn) (GUIStyleState_t523 *);
	static GUIStyleState_Init_m5289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Init_m5289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::Cleanup()
extern "C" void GUIStyleState_Cleanup_m5290 (GUIStyleState_t523 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyleState_Cleanup_m5290_ftn) (GUIStyleState_t523 *);
	static GUIStyleState_Cleanup_m5290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_Cleanup_m5290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
extern "C" Texture2D_t9 * GUIStyleState_GetBackgroundInternal_m5291 (GUIStyleState_t523 * __this, const MethodInfo* method)
{
	typedef Texture2D_t9 * (*GUIStyleState_GetBackgroundInternal_m5291_ftn) (GUIStyleState_t523 *);
	static GUIStyleState_GetBackgroundInternal_m5291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_GetBackgroundInternal_m5291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::GetBackgroundInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C" void GUIStyleState_set_textColor_m3064 (GUIStyleState_t523 * __this, Color_t6  ___value, const MethodInfo* method)
{
	{
		GUIStyleState_INTERNAL_set_textColor_m5292(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
extern "C" void GUIStyleState_INTERNAL_set_textColor_m5292 (GUIStyleState_t523 * __this, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*GUIStyleState_INTERNAL_set_textColor_m5292_ftn) (GUIStyleState_t523 *, Color_t6 *);
	static GUIStyleState_INTERNAL_set_textColor_m5292_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyleState_INTERNAL_set_textColor_m5292_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.RectOffset::.ctor()
extern "C" void RectOffset__ctor_m4944 (RectOffset_t741 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		RectOffset_Init_m5296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
extern "C" void RectOffset__ctor_m5293 (RectOffset_t741 * __this, GUIStyle_t273 * ___sourceStyle, IntPtr_t ___source, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		GUIStyle_t273 * L_0 = ___sourceStyle;
		__this->___m_SourceStyle_1 = L_0;
		IntPtr_t L_1 = ___source;
		__this->___m_Ptr_0 = L_1;
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void RectOffset__ctor_m5294 (RectOffset_t741 * __this, int32_t ___left, int32_t ___right, int32_t ___top, int32_t ___bottom, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		RectOffset_Init_m5296(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___left;
		RectOffset_set_left_m5298(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___right;
		RectOffset_set_right_m5300(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___top;
		RectOffset_set_top_m5301(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___bottom;
		RectOffset_set_bottom_m5303(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C" void RectOffset_Finalize_m5295 (RectOffset_t741 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			GUIStyle_t273 * L_0 = (__this->___m_SourceStyle_1);
			if (L_0)
			{
				goto IL_0011;
			}
		}

IL_000b:
		{
			RectOffset_Cleanup_m5297(__this, /*hidden argument*/NULL);
		}

IL_0011:
		{
			IL2CPP_LEAVE(0x1D, FINALLY_0016);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0016;
	}

FINALLY_0016:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(22)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(22)
	{
		IL2CPP_JUMP_TBL(0x1D, IL_001d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_001d:
	{
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C" void RectOffset_Init_m5296 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m5296_ftn) (RectOffset_t741 *);
	static RectOffset_Init_m5296_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m5296_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C" void RectOffset_Cleanup_m5297 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m5297_ftn) (RectOffset_t741 *);
	static RectOffset_Cleanup_m5297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m5297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C" int32_t RectOffset_get_left_m4939 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m4939_ftn) (RectOffset_t741 *);
	static RectOffset_get_left_m4939_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m4939_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C" void RectOffset_set_left_m5298 (RectOffset_t741 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m5298_ftn) (RectOffset_t741 *, int32_t);
	static RectOffset_set_left_m5298_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m5298_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C" int32_t RectOffset_get_right_m5299 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m5299_ftn) (RectOffset_t741 *);
	static RectOffset_get_right_m5299_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m5299_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C" void RectOffset_set_right_m5300 (RectOffset_t741 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m5300_ftn) (RectOffset_t741 *, int32_t);
	static RectOffset_set_right_m5300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m5300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C" int32_t RectOffset_get_top_m4940 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m4940_ftn) (RectOffset_t741 *);
	static RectOffset_get_top_m4940_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m4940_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C" void RectOffset_set_top_m5301 (RectOffset_t741 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m5301_ftn) (RectOffset_t741 *, int32_t);
	static RectOffset_set_top_m5301_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m5301_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C" int32_t RectOffset_get_bottom_m5302 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m5302_ftn) (RectOffset_t741 *);
	static RectOffset_get_bottom_m5302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m5302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C" void RectOffset_set_bottom_m5303 (RectOffset_t741 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m5303_ftn) (RectOffset_t741 *, int32_t);
	static RectOffset_set_bottom_m5303_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m5303_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C" int32_t RectOffset_get_horizontal_m4934 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m4934_ftn) (RectOffset_t741 *);
	static RectOffset_get_horizontal_m4934_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m4934_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C" int32_t RectOffset_get_vertical_m4935 (RectOffset_t741 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m4935_ftn) (RectOffset_t741 *);
	static RectOffset_get_vertical_m4935_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m4935_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
extern "C" Rect_t225  RectOffset_Remove_m5304 (RectOffset_t741 * __this, Rect_t225  ___rect, const MethodInfo* method)
{
	{
		Rect_t225  L_0 = RectOffset_INTERNAL_CALL_Remove_m5305(NULL /*static, unused*/, __this, (&___rect), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)
extern "C" Rect_t225  RectOffset_INTERNAL_CALL_Remove_m5305 (Object_t * __this /* static, unused */, RectOffset_t741 * ___self, Rect_t225 * ___rect, const MethodInfo* method)
{
	typedef Rect_t225  (*RectOffset_INTERNAL_CALL_Remove_m5305_ftn) (RectOffset_t741 *, Rect_t225 *);
	static RectOffset_INTERNAL_CALL_Remove_m5305_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_INTERNAL_CALL_Remove_m5305_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)");
	return _il2cpp_icall_func(___self, ___rect);
}
// System.String UnityEngine.RectOffset::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral926;
extern "C" String_t* RectOffset_ToString_m5306 (RectOffset_t741 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		_stringLiteral926 = il2cpp_codegen_string_literal_from_index(926);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		int32_t L_1 = RectOffset_get_left_m4939(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m5299(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m4940(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m5302(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral926, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyleMethodDeclarations.h"



// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePositionMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"


// System.Void UnityEngine.GUIStyle::.ctor()
extern "C" void GUIStyle__ctor_m3062 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		GUIStyle_Init_m5309(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::.cctor()
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void GUIStyle__cctor_m5307 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GUIStyle_t273_StaticFields*)GUIStyle_t273_il2cpp_TypeInfo_var->static_fields)->___showKeyboardFocus_14 = 1;
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Finalize()
extern "C" void GUIStyle_Finalize_m5308 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		GUIStyle_Cleanup_m5310(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Init()
extern "C" void GUIStyle_Init_m5309 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Init_m5309_ftn) (GUIStyle_t273 *);
	static GUIStyle_Init_m5309_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Init_m5309_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::Cleanup()
extern "C" void GUIStyle_Cleanup_m5310 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef void (*GUIStyle_Cleanup_m5310_ftn) (GUIStyle_t273 *);
	static GUIStyle_Cleanup_m5310_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Cleanup_m5310_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.String UnityEngine.GUIStyle::get_name()
extern "C" String_t* GUIStyle_get_name_m5311 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef String_t* (*GUIStyle_get_name_m5311_ftn) (GUIStyle_t273 *);
	static GUIStyle_get_name_m5311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_name_m5311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_name(System.String)
extern "C" void GUIStyle_set_name_m5312 (GUIStyle_t273 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_name_m5312_ftn) (GUIStyle_t273 *, String_t*);
	static GUIStyle_set_name_m5312_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_name_m5312_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern TypeInfo* GUIStyleState_t523_il2cpp_TypeInfo_var;
extern "C" GUIStyleState_t523 * GUIStyle_get_normal_m3063 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyleState_t523_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(652);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyleState_t523 * L_0 = (__this->___m_Normal_1);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetStyleStatePtr_m5313(__this, 0, /*hidden argument*/NULL);
		GUIStyleState_t523 * L_2 = (GUIStyleState_t523 *)il2cpp_codegen_object_new (GUIStyleState_t523_il2cpp_TypeInfo_var);
		GUIStyleState__ctor_m5287(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Normal_1 = L_2;
	}

IL_001e:
	{
		GUIStyleState_t523 * L_3 = (__this->___m_Normal_1);
		return L_3;
	}
}
// System.IntPtr UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetStyleStatePtr_m5313 (GUIStyle_t273 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetStyleStatePtr_m5313_ftn) (GUIStyle_t273 *, int32_t);
	static GUIStyle_GetStyleStatePtr_m5313_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetStyleStatePtr_m5313_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetStyleStatePtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_margin()
extern TypeInfo* RectOffset_t741_il2cpp_TypeInfo_var;
extern "C" RectOffset_t741 * GUIStyle_get_margin_m5314 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t741_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(582);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t741 * L_0 = (__this->___m_Margin_11);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m5316(__this, 1, /*hidden argument*/NULL);
		RectOffset_t741 * L_2 = (RectOffset_t741 *)il2cpp_codegen_object_new (RectOffset_t741_il2cpp_TypeInfo_var);
		RectOffset__ctor_m5293(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Margin_11 = L_2;
	}

IL_001e:
	{
		RectOffset_t741 * L_3 = (__this->___m_Margin_11);
		return L_3;
	}
}
// UnityEngine.RectOffset UnityEngine.GUIStyle::get_padding()
extern TypeInfo* RectOffset_t741_il2cpp_TypeInfo_var;
extern "C" RectOffset_t741 * GUIStyle_get_padding_m5315 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t741_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(582);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t741 * L_0 = (__this->___m_Padding_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_1 = GUIStyle_GetRectOffsetPtr_m5316(__this, 2, /*hidden argument*/NULL);
		RectOffset_t741 * L_2 = (RectOffset_t741 *)il2cpp_codegen_object_new (RectOffset_t741_il2cpp_TypeInfo_var);
		RectOffset__ctor_m5293(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->___m_Padding_10 = L_2;
	}

IL_001e:
	{
		RectOffset_t741 * L_3 = (__this->___m_Padding_10);
		return L_3;
	}
}
// System.IntPtr UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)
extern "C" IntPtr_t GUIStyle_GetRectOffsetPtr_m5316 (GUIStyle_t273 * __this, int32_t ___idx, const MethodInfo* method)
{
	typedef IntPtr_t (*GUIStyle_GetRectOffsetPtr_m5316_ftn) (GUIStyle_t273 *, int32_t);
	static GUIStyle_GetRectOffsetPtr_m5316_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_GetRectOffsetPtr_m5316_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::GetRectOffsetPtr(System.Int32)");
	return _il2cpp_icall_func(__this, ___idx);
}
// UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
extern "C" int32_t GUIStyle_get_imagePosition_m5317 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef int32_t (*GUIStyle_get_imagePosition_m5317_ftn) (GUIStyle_t273 *);
	static GUIStyle_get_imagePosition_m5317_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_imagePosition_m5317_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_imagePosition()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C" void GUIStyle_set_alignment_m3067 (GUIStyle_t273 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_alignment_m3067_ftn) (GUIStyle_t273 *, int32_t);
	static GUIStyle_set_alignment_m3067_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_alignment_m3067_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_wordWrap()
extern "C" bool GUIStyle_get_wordWrap_m5318 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_wordWrap_m5318_ftn) (GUIStyle_t273 *);
	static GUIStyle_get_wordWrap_m5318_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_wordWrap_m5318_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_wordWrap()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
extern "C" void GUIStyle_set_wordWrap_m3068 (GUIStyle_t273 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_wordWrap_m3068_ftn) (GUIStyle_t273 *, bool);
	static GUIStyle_set_wordWrap_m3068_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_wordWrap_m3068_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_wordWrap(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::get_fixedWidth()
extern "C" float GUIStyle_get_fixedWidth_m5319 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedWidth_m5319_ftn) (GUIStyle_t273 *);
	static GUIStyle_get_fixedWidth_m5319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedWidth_m5319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.GUIStyle::get_fixedHeight()
extern "C" float GUIStyle_get_fixedHeight_m5320 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef float (*GUIStyle_get_fixedHeight_m5320_ftn) (GUIStyle_t273 *);
	static GUIStyle_get_fixedHeight_m5320_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_fixedHeight_m5320_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_fixedHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
extern "C" bool GUIStyle_get_stretchWidth_m5321 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchWidth_m5321_ftn) (GUIStyle_t273 *);
	static GUIStyle_get_stretchWidth_m5321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchWidth_m5321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
extern "C" void GUIStyle_set_stretchWidth_m5322 (GUIStyle_t273 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchWidth_m5322_ftn) (GUIStyle_t273 *, bool);
	static GUIStyle_set_stretchWidth_m5322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchWidth_m5322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
extern "C" bool GUIStyle_get_stretchHeight_m5323 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	typedef bool (*GUIStyle_get_stretchHeight_m5323_ftn) (GUIStyle_t273 *);
	static GUIStyle_get_stretchHeight_m5323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_get_stretchHeight_m5323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::get_stretchHeight()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
extern "C" void GUIStyle_set_stretchHeight_m5324 (GUIStyle_t273 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_stretchHeight_m5324_ftn) (GUIStyle_t273 *, bool);
	static GUIStyle_set_stretchHeight_m5324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_stretchHeight_m5324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
extern "C" float GUIStyle_Internal_GetLineHeight_m5325 (Object_t * __this /* static, unused */, IntPtr_t ___target, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_GetLineHeight_m5325_ftn) (IntPtr_t);
	static GUIStyle_Internal_GetLineHeight_m5325_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_GetLineHeight_m5325_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)");
	return _il2cpp_icall_func(___target);
}
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C" void GUIStyle_set_fontSize_m3065 (GUIStyle_t273 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_fontSize_m3065_ftn) (GUIStyle_t273 *, int32_t);
	static GUIStyle_set_fontSize_m3065_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_fontSize_m3065_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_fontSize(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)
extern "C" void GUIStyle_set_fontStyle_m3066 (GUIStyle_t273 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GUIStyle_set_fontStyle_m3066_ftn) (GUIStyle_t273 *, int32_t);
	static GUIStyle_set_fontStyle_m3066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_set_fontStyle_m3066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.GUIStyle::get_lineHeight()
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_get_lineHeight_m5326 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		float L_1 = GUIStyle_Internal_GetLineHeight_m5325(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = roundf(L_1);
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
extern "C" void GUIStyle_SetDefaultFont_m5327 (Object_t * __this /* static, unused */, Font_t643 * ___font, const MethodInfo* method)
{
	typedef void (*GUIStyle_SetDefaultFont_m5327_ftn) (Font_t643 *);
	static GUIStyle_SetDefaultFont_m5327_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_SetDefaultFont_m5327_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)");
	_il2cpp_icall_func(___font);
}
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" GUIStyle_t273 * GUIStyle_get_none_m5328 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_0 = ((GUIStyle_t273_StaticFields*)GUIStyle_t273_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		GUIStyle_t273 * L_1 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		((GUIStyle_t273_StaticFields*)GUIStyle_t273_il2cpp_TypeInfo_var->static_fields)->___s_None_15 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_t273 * L_2 = ((GUIStyle_t273_StaticFields*)GUIStyle_t273_il2cpp_TypeInfo_var->static_fields)->___s_None_15;
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::GetCursorPixelPosition(UnityEngine.Rect,UnityEngine.GUIContent,System.Int32)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" Vector2_t7  GUIStyle_GetCursorPixelPosition_m5329 (GUIStyle_t273 * __this, Rect_t225  ___position, GUIContent_t807 * ___content, int32_t ___cursorStringIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		Rect_t225  L_1 = ___position;
		GUIContent_t807 * L_2 = ___content;
		int32_t L_3 = ___cursorStringIndex;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_Internal_GetCursorPixelPosition_m5330(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_Internal_GetCursorPixelPosition_m5330 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t225  ___position, GUIContent_t807 * ___content, int32_t ___cursorStringIndex, Vector2_t7 * ___ret, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ___target;
		GUIContent_t807 * L_1 = ___content;
		int32_t L_2 = ___cursorStringIndex;
		Vector2_t7 * L_3 = ___ret;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5331(NULL /*static, unused*/, L_0, (&___position), L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
extern "C" void GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5331 (Object_t * __this /* static, unused */, IntPtr_t ___target, Rect_t225 * ___position, GUIContent_t807 * ___content, int32_t ___cursorStringIndex, Vector2_t7 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5331_ftn) (IntPtr_t, Rect_t225 *, GUIContent_t807 *, int32_t, Vector2_t7 *);
	static GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5331_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5331_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___position, ___content, ___cursorStringIndex, ___ret);
}
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" Vector2_t7  GUIStyle_CalcSize_m5332 (GUIStyle_t273 * __this, GUIContent_t807 * ___content, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t807 * L_1 = ___content;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcSize_m5333(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
extern "C" void GUIStyle_Internal_CalcSize_m5333 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t807 * ___content, Vector2_t7 * ___ret, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcSize_m5333_ftn) (IntPtr_t, GUIContent_t807 *, Vector2_t7 *);
	static GUIStyle_Internal_CalcSize_m5333_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcSize_m5333_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___target, ___content, ___ret);
}
// System.Single UnityEngine.GUIStyle::CalcHeight(UnityEngine.GUIContent,System.Single)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" float GUIStyle_CalcHeight_m5334 (GUIStyle_t273 * __this, GUIContent_t807 * ___content, float ___width, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t807 * L_1 = ___content;
		float L_2 = ___width;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		float L_3 = GUIStyle_Internal_CalcHeight_m5335(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
extern "C" float GUIStyle_Internal_CalcHeight_m5335 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t807 * ___content, float ___width, const MethodInfo* method)
{
	typedef float (*GUIStyle_Internal_CalcHeight_m5335_ftn) (IntPtr_t, GUIContent_t807 *, float);
	static GUIStyle_Internal_CalcHeight_m5335_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcHeight_m5335_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)");
	return _il2cpp_icall_func(___target, ___content, ___width);
}
// System.Boolean UnityEngine.GUIStyle::get_isHeightDependantOnWidth()
extern "C" bool GUIStyle_get_isHeightDependantOnWidth_m5336 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		float L_0 = GUIStyle_get_fixedHeight_m5320(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		bool L_1 = GUIStyle_get_wordWrap_m5318(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = GUIStyle_get_imagePosition_m5317(__this, /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002a;
	}

IL_0029:
	{
		G_B4_0 = 0;
	}

IL_002a:
	{
		G_B6_0 = G_B4_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B6_0 = 0;
	}

IL_002d:
	{
		return G_B6_0;
	}
}
// System.Void UnityEngine.GUIStyle::CalcMinMaxWidth(UnityEngine.GUIContent,System.Single&,System.Single&)
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void GUIStyle_CalcMinMaxWidth_m5337 (GUIStyle_t273 * __this, GUIContent_t807 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___m_Ptr_0);
		GUIContent_t807 * L_1 = ___content;
		float* L_2 = ___minWidth;
		float* L_3 = ___maxWidth;
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle_Internal_CalcMinMaxWidth_m5338(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
extern "C" void GUIStyle_Internal_CalcMinMaxWidth_m5338 (Object_t * __this /* static, unused */, IntPtr_t ___target, GUIContent_t807 * ___content, float* ___minWidth, float* ___maxWidth, const MethodInfo* method)
{
	typedef void (*GUIStyle_Internal_CalcMinMaxWidth_m5338_ftn) (IntPtr_t, GUIContent_t807 *, float*, float*);
	static GUIStyle_Internal_CalcMinMaxWidth_m5338_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIStyle_Internal_CalcMinMaxWidth_m5338_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)");
	_il2cpp_icall_func(___target, ___content, ___minWidth, ___maxWidth);
}
// System.String UnityEngine.GUIStyle::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral927;
extern "C" String_t* GUIStyle_ToString_m5339 (GUIStyle_t273 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral927 = il2cpp_codegen_string_literal_from_index(927);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = GUIStyle_get_name_m5311(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		String_t* L_2 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral927, L_0, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstrucMethodDeclarations.h"



// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardTypeMethodDeclarations.h"



// UnityEngine.TouchScreenKeyboard
#include "UnityEngine_UnityEngine_TouchScreenKeyboard.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TouchScreenKeyboard
#include "UnityEngine_UnityEngine_TouchScreenKeyboardMethodDeclarations.h"

// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"


// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_InternalConstructorHelperArguments_t891_il2cpp_TypeInfo_var;
extern TypeInfo* TouchScreenKeyboardType_t805_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern "C" void TouchScreenKeyboard__ctor_m5340 (TouchScreenKeyboard_t683 * __this, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_InternalConstructorHelperArguments_t891_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(653);
		TouchScreenKeyboardType_t805_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(537);
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		s_Il2CppMethodIntialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t891  V_0 = {0};
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t891_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType;
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(TouchScreenKeyboardType_t805_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m6371(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->___keyboardType_0 = L_3;
		bool L_4 = ___autocorrection;
		uint32_t L_5 = Convert_ToUInt32_m6372(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->___autocorrection_1 = L_5;
		bool L_6 = ___multiline;
		uint32_t L_7 = Convert_ToUInt32_m6372(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->___multiline_2 = L_7;
		bool L_8 = ___secure;
		uint32_t L_9 = Convert_ToUInt32_m6372(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->___secure_3 = L_9;
		bool L_10 = ___alert;
		uint32_t L_11 = Convert_ToUInt32_m6372(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->___alert_4 = L_11;
		String_t* L_12 = ___text;
		String_t* L_13 = ___textPlaceholder;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5343(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C" void TouchScreenKeyboard_Destroy_m5341 (TouchScreenKeyboard_t683 * __this, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m5341_ftn) (TouchScreenKeyboard_t683 *);
	static TouchScreenKeyboard_Destroy_m5341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m5341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C" void TouchScreenKeyboard_Finalize_m5342 (TouchScreenKeyboard_t683 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m5341(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5343 (TouchScreenKeyboard_t683 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t891 * ___arguments, String_t* ___text, String_t* ___textPlaceholder, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5343_ftn) (TouchScreenKeyboard_t683 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t891 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5343_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5343_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments, ___text, ___textPlaceholder);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C" bool TouchScreenKeyboard_get_isSupported_m4787 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = Application_get_platform_m4774(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 0)
		{
			goto IL_0054;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 1)
		{
			goto IL_0054;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 2)
		{
			goto IL_0054;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 3)
		{
			goto IL_0052;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 4)
		{
			goto IL_0052;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 5)
		{
			goto IL_0052;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 6)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 7)
		{
			goto IL_0035;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)((int32_t)18))) == 8)
		{
			goto IL_0052;
		}
	}

IL_0035:
	{
		int32_t L_3 = V_1;
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 0)
		{
			goto IL_0052;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 1)
		{
			goto IL_0056;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 2)
		{
			goto IL_0056;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)8)) == 3)
		{
			goto IL_0052;
		}
	}
	{
		goto IL_0056;
	}

IL_0052:
	{
		return 1;
	}

IL_0054:
	{
		return 0;
	}

IL_0056:
	{
		return 0;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t683 * TouchScreenKeyboard_Open_m4842 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		String_t* L_1 = ___text;
		int32_t L_2 = ___keyboardType;
		bool L_3 = ___autocorrection;
		bool L_4 = ___multiline;
		bool L_5 = ___secure;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t683 * L_8 = TouchScreenKeyboard_Open_m5344(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t683 * TouchScreenKeyboard_Open_m4843 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	bool V_2 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		String_t* L_1 = ___text;
		int32_t L_2 = ___keyboardType;
		bool L_3 = ___autocorrection;
		bool L_4 = ___multiline;
		bool L_5 = V_2;
		bool L_6 = V_1;
		String_t* L_7 = V_0;
		TouchScreenKeyboard_t683 * L_8 = TouchScreenKeyboard_Open_m5344(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern TypeInfo* TouchScreenKeyboard_t683_il2cpp_TypeInfo_var;
extern "C" TouchScreenKeyboard_t683 * TouchScreenKeyboard_Open_m5344 (Object_t * __this /* static, unused */, String_t* ___text, int32_t ___keyboardType, bool ___autocorrection, bool ___multiline, bool ___secure, bool ___alert, String_t* ___textPlaceholder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TouchScreenKeyboard_t683_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(654);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___text;
		int32_t L_1 = ___keyboardType;
		bool L_2 = ___autocorrection;
		bool L_3 = ___multiline;
		bool L_4 = ___secure;
		bool L_5 = ___alert;
		String_t* L_6 = ___textPlaceholder;
		TouchScreenKeyboard_t683 * L_7 = (TouchScreenKeyboard_t683 *)il2cpp_codegen_object_new (TouchScreenKeyboard_t683_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m5340(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C" String_t* TouchScreenKeyboard_get_text_m4776 (TouchScreenKeyboard_t683 * __this, const MethodInfo* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m4776_ftn) (TouchScreenKeyboard_t683 *);
	static TouchScreenKeyboard_get_text_m4776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m4776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C" void TouchScreenKeyboard_set_text_m4777 (TouchScreenKeyboard_t683 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m4777_ftn) (TouchScreenKeyboard_t683 *, String_t*);
	static TouchScreenKeyboard_set_text_m4777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m4777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C" void TouchScreenKeyboard_set_hideInput_m4841 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m4841_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m4841_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m4841_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C" bool TouchScreenKeyboard_get_active_m4775 (TouchScreenKeyboard_t683 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m4775_ftn) (TouchScreenKeyboard_t683 *);
	static TouchScreenKeyboard_get_active_m4775_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m4775_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C" void TouchScreenKeyboard_set_active_m4840 (TouchScreenKeyboard_t683 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m4840_ftn) (TouchScreenKeyboard_t683 *, bool);
	static TouchScreenKeyboard_set_active_m4840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m4840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C" bool TouchScreenKeyboard_get_done_m4789 (TouchScreenKeyboard_t683 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m4789_ftn) (TouchScreenKeyboard_t683 *);
	static TouchScreenKeyboard_get_done_m4789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m4789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C" bool TouchScreenKeyboard_get_wasCanceled_m4788 (TouchScreenKeyboard_t683 * __this, const MethodInfo* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m4788_ftn) (TouchScreenKeyboard_t683 *);
	static TouchScreenKeyboard_get_wasCanceled_m4788_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m4788_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	return _il2cpp_icall_func(__this);
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
// System.Char
#include "mscorlib_System_Char.h"
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"


// System.Void UnityEngine.Event::.ctor()
extern "C" void Event__ctor_m4772 (Event_t481 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Event_Init_m5345(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Init()
extern "C" void Event_Init_m5345 (Event_t481 * __this, const MethodInfo* method)
{
	typedef void (*Event_Init_m5345_ftn) (Event_t481 *);
	static Event_Init_m5345_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m5345_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::Finalize()
extern "C" void Event_Finalize_m5346 (Event_t481 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m5347(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.Event::Cleanup()
extern "C" void Event_Cleanup_m5347 (Event_t481 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m5347_ftn) (Event_t481 *);
	static Event_Cleanup_m5347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m5347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C" int32_t Event_get_rawType_m4803 (Event_t481 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m4803_ftn) (Event_t481 *);
	static Event_get_rawType_m4803_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m4803_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C" int32_t Event_get_type_m2786 (Event_t481 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m2786_ftn) (Event_t481 *);
	static Event_get_type_m2786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m2786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C" Vector2_t7  Event_get_mousePosition_m5348 (Event_t481 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		Event_Internal_GetMousePosition_m5349(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C" void Event_Internal_GetMousePosition_m5349 (Event_t481 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m5349_ftn) (Event_t481 *, Vector2_t7 *);
	static Event_Internal_GetMousePosition_m5349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m5349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C" int32_t Event_get_modifiers_m4799 (Event_t481 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m4799_ftn) (Event_t481 *);
	static Event_get_modifiers_m4799_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m4799_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Char UnityEngine.Event::get_character()
extern "C" uint16_t Event_get_character_m4801 (Event_t481 * __this, const MethodInfo* method)
{
	typedef uint16_t (*Event_get_character_m4801_ftn) (Event_t481 *);
	static Event_get_character_m4801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m4801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Event::get_commandName()
extern "C" String_t* Event_get_commandName_m5350 (Event_t481 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m5350_ftn) (Event_t481 *);
	static Event_get_commandName_m5350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m5350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C" int32_t Event_get_keyCode_m4800 (Event_t481 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m4800_ftn) (Event_t481 *);
	static Event_get_keyCode_m4800_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m4800_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern TypeInfo* Event_t481_il2cpp_TypeInfo_var;
extern "C" Event_t481 * Event_get_current_m2785 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t481_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(529);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t481 * L_0 = ((Event_t481_StaticFields*)Event_t481_il2cpp_TypeInfo_var->static_fields)->___s_Current_1;
		return L_0;
	}
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C" void Event_Internal_SetNativeEvent_m5351 (Object_t * __this /* static, unused */, IntPtr_t ___ptr, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m5351_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m5351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m5351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr);
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent()
extern TypeInfo* Event_t481_il2cpp_TypeInfo_var;
extern "C" void Event_Internal_MakeMasterEventCurrent_m5352 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t481_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(529);
		s_Il2CppMethodIntialized = true;
	}
	{
		Event_t481 * L_0 = ((Event_t481_StaticFields*)Event_t481_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		Event_t481 * L_1 = (Event_t481 *)il2cpp_codegen_object_new (Event_t481_il2cpp_TypeInfo_var);
		Event__ctor_m4772(L_1, /*hidden argument*/NULL);
		((Event_t481_StaticFields*)Event_t481_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2 = L_1;
	}

IL_0014:
	{
		Event_t481 * L_2 = ((Event_t481_StaticFields*)Event_t481_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		((Event_t481_StaticFields*)Event_t481_il2cpp_TypeInfo_var->static_fields)->___s_Current_1 = L_2;
		Event_t481 * L_3 = ((Event_t481_StaticFields*)Event_t481_il2cpp_TypeInfo_var->static_fields)->___s_MasterEvent_2;
		NullCheck(L_3);
		IntPtr_t L_4 = (L_3->___m_Ptr_0);
		Event_Internal_SetNativeEvent_m5351(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C" bool Event_PopEvent_m4804 (Object_t * __this /* static, unused */, Event_t481 * ___outEvent, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m4804_ftn) (Event_t481 *);
	static Event_PopEvent_m4804_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m4804_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent);
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C" bool Event_get_isKey_m5353 (Event_t481 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 1;
	}

IL_0015:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C" bool Event_get_isMouse_m5354 (Event_t481 * __this, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_4 = V_0;
		G_B5_0 = ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B5_0 = 1;
	}

IL_0022:
	{
		return G_B5_0;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
extern "C" int32_t Event_GetHashCode_m5355 (Event_t481 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t7  V_1 = {0};
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m5353(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m4800(__this, /*hidden argument*/NULL);
		V_0 = (((uint16_t)L_1));
	}

IL_0015:
	{
		bool L_2 = Event_get_isMouse_m5354(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector2_t7  L_3 = Event_get_mousePosition_m5348(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m5362((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_002f:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m4799(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
extern TypeInfo* Event_t481_il2cpp_TypeInfo_var;
extern "C" bool Event_Equals_m5356 (Event_t481 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Event_t481_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(529);
		s_Il2CppMethodIntialized = true;
	}
	Event_t481 * V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return 0;
	}

IL_0008:
	{
		Object_t * L_1 = ___obj;
		bool L_2 = Object_ReferenceEquals_m6373(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		Object_t * L_3 = ___obj;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m3299(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m3299(__this, /*hidden argument*/NULL);
		if ((((Object_t*)(Type_t *)L_4) == ((Object_t*)(Type_t *)L_5)))
		{
			goto IL_0029;
		}
	}
	{
		return 0;
	}

IL_0029:
	{
		Object_t * L_6 = ___obj;
		V_0 = ((Event_t481 *)Castclass(L_6, Event_t481_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		Event_t481 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m2786(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0058;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m4799(__this, /*hidden argument*/NULL);
		Event_t481 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m4799(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)-33)))) == ((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)-33))))))
		{
			goto IL_005a;
		}
	}

IL_0058:
	{
		return 0;
	}

IL_005a:
	{
		bool L_13 = Event_get_isKey_m5353(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m4800(__this, /*hidden argument*/NULL);
		Event_t481 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m4800(L_15, /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0);
	}

IL_0074:
	{
		bool L_17 = Event_get_isMouse_m5354(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0091;
		}
	}
	{
		Vector2_t7  L_18 = Event_get_mousePosition_m5348(__this, /*hidden argument*/NULL);
		Event_t481 * L_19 = V_0;
		NullCheck(L_19);
		Vector2_t7  L_20 = Event_get_mousePosition_m5348(L_19, /*hidden argument*/NULL);
		bool L_21 = Vector2_op_Equality_m4968(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		return L_21;
	}

IL_0091:
	{
		return 0;
	}
}
// System.String UnityEngine.Event::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* EventType_t893_il2cpp_TypeInfo_var;
extern TypeInfo* EventModifiers_t894_il2cpp_TypeInfo_var;
extern TypeInfo* KeyCode_t892_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2_t7_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral928;
extern Il2CppCodeGenString* _stringLiteral929;
extern Il2CppCodeGenString* _stringLiteral930;
extern Il2CppCodeGenString* _stringLiteral931;
extern Il2CppCodeGenString* _stringLiteral932;
extern Il2CppCodeGenString* _stringLiteral933;
extern Il2CppCodeGenString* _stringLiteral934;
extern "C" String_t* Event_ToString_m5357 (Event_t481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		EventType_t893_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(644);
		EventModifiers_t894_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(655);
		KeyCode_t892_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(656);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		Vector2_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		_stringLiteral928 = il2cpp_codegen_string_literal_from_index(928);
		_stringLiteral929 = il2cpp_codegen_string_literal_from_index(929);
		_stringLiteral930 = il2cpp_codegen_string_literal_from_index(930);
		_stringLiteral931 = il2cpp_codegen_string_literal_from_index(931);
		_stringLiteral932 = il2cpp_codegen_string_literal_from_index(932);
		_stringLiteral933 = il2cpp_codegen_string_literal_from_index(933);
		_stringLiteral934 = il2cpp_codegen_string_literal_from_index(934);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Event_get_isKey_m5353(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00c0;
		}
	}
	{
		uint16_t L_1 = Event_get_character_m4801(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0051;
		}
	}
	{
		ObjectU5BU5D_t470* L_2 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 3));
		int32_t L_3 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(EventType_t893_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0)) = (Object_t *)L_5;
		ObjectU5BU5D_t470* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m4799(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(EventModifiers_t894_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 1)) = (Object_t *)L_9;
		ObjectU5BU5D_t470* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m4800(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(KeyCode_t892_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 2)) = (Object_t *)L_13;
		String_t* L_14 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral928, L_10, /*hidden argument*/NULL);
		return L_14;
	}

IL_0051:
	{
		ObjectU5BU5D_t470* L_15 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 8));
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		ArrayElementTypeCheck (L_15, _stringLiteral929);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 0)) = (Object_t *)_stringLiteral929;
		ObjectU5BU5D_t470* L_16 = L_15;
		int32_t L_17 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(EventType_t893_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 1)) = (Object_t *)L_19;
		ObjectU5BU5D_t470* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		ArrayElementTypeCheck (L_20, _stringLiteral930);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 2)) = (Object_t *)_stringLiteral930;
		ObjectU5BU5D_t470* L_21 = L_20;
		uint16_t L_22 = Event_get_character_m4801(__this, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 3)) = (Object_t *)L_24;
		ObjectU5BU5D_t470* L_25 = L_21;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 4);
		ArrayElementTypeCheck (L_25, _stringLiteral931);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, 4)) = (Object_t *)_stringLiteral931;
		ObjectU5BU5D_t470* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m4799(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Object_t * L_29 = Box(EventModifiers_t894_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 5);
		ArrayElementTypeCheck (L_26, L_29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, 5)) = (Object_t *)L_29;
		ObjectU5BU5D_t470* L_30 = L_26;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 6);
		ArrayElementTypeCheck (L_30, _stringLiteral932);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_30, 6)) = (Object_t *)_stringLiteral932;
		ObjectU5BU5D_t470* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m4800(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Object_t * L_34 = Box(KeyCode_t892_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 7);
		ArrayElementTypeCheck (L_31, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 7)) = (Object_t *)L_34;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m3056(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		String_t* L_36 = UnityString_Format_m5568(NULL /*static, unused*/, L_35, ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return L_36;
	}

IL_00c0:
	{
		bool L_37 = Event_get_isMouse_m5354(__this, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0106;
		}
	}
	{
		ObjectU5BU5D_t470* L_38 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 3));
		int32_t L_39 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		int32_t L_40 = L_39;
		Object_t * L_41 = Box(EventType_t893_il2cpp_TypeInfo_var, &L_40);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 0);
		ArrayElementTypeCheck (L_38, L_41);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_38, 0)) = (Object_t *)L_41;
		ObjectU5BU5D_t470* L_42 = L_38;
		Vector2_t7  L_43 = Event_get_mousePosition_m5348(__this, /*hidden argument*/NULL);
		Vector2_t7  L_44 = L_43;
		Object_t * L_45 = Box(Vector2_t7_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 1);
		ArrayElementTypeCheck (L_42, L_45);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 1)) = (Object_t *)L_45;
		ObjectU5BU5D_t470* L_46 = L_42;
		int32_t L_47 = Event_get_modifiers_m4799(__this, /*hidden argument*/NULL);
		int32_t L_48 = L_47;
		Object_t * L_49 = Box(EventModifiers_t894_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 2);
		ArrayElementTypeCheck (L_46, L_49);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_46, 2)) = (Object_t *)L_49;
		String_t* L_50 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral933, L_46, /*hidden argument*/NULL);
		return L_50;
	}

IL_0106:
	{
		int32_t L_51 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_51) == ((int32_t)((int32_t)14))))
		{
			goto IL_0120;
		}
	}
	{
		int32_t L_52 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_52) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0148;
		}
	}

IL_0120:
	{
		ObjectU5BU5D_t470* L_53 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 2));
		int32_t L_54 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		int32_t L_55 = L_54;
		Object_t * L_56 = Box(EventType_t893_il2cpp_TypeInfo_var, &L_55);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
		ArrayElementTypeCheck (L_53, L_56);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_53, 0)) = (Object_t *)L_56;
		ObjectU5BU5D_t470* L_57 = L_53;
		String_t* L_58 = Event_get_commandName_m5350(__this, /*hidden argument*/NULL);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 1);
		ArrayElementTypeCheck (L_57, L_58);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_57, 1)) = (Object_t *)L_58;
		String_t* L_59 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral934, L_57, /*hidden argument*/NULL);
		return L_59;
	}

IL_0148:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_60 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		int32_t L_61 = Event_get_type_m2786(__this, /*hidden argument*/NULL);
		int32_t L_62 = L_61;
		Object_t * L_63 = Box(EventType_t893_il2cpp_TypeInfo_var, &L_62);
		String_t* L_64 = String_Concat_m2758(NULL /*static, unused*/, L_60, L_63, /*hidden argument*/NULL);
		return L_64;
	}
}
// Conversion methods for marshalling of: UnityEngine.Event
void Event_t481_marshal(const Event_t481& unmarshaled, Event_t481_marshaled& marshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception(___s_Current_1Exception);
}
void Event_t481_marshal_back(const Event_t481_marshaled& marshaled, Event_t481& unmarshaled)
{
	Il2CppCodeGenException* ___s_Current_1Exception = il2cpp_codegen_get_not_supported_exception("Cannot marshal field 's_Current' of type 'Event': Reference type field marshaling is not supported.");
	il2cpp_codegen_raise_exception(___s_Current_1Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
void Event_t481_marshal_cleanup(Event_t481_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCodeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventTypeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiersMethodDeclarations.h"



// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_Gizmos.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"



// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawLine_m3346 (Object_t * __this /* static, unused */, Vector3_t215  ___from, Vector3_t215  ___to, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_CALL_DrawLine_m5358(NULL /*static, unused*/, (&___from), (&___to), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawLine_m5358 (Object_t * __this /* static, unused */, Vector3_t215 * ___from, Vector3_t215 * ___to, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawLine_m5358_ftn) (Vector3_t215 *, Vector3_t215 *);
	static Gizmos_INTERNAL_CALL_DrawLine_m5358_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawLine_m5358_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___from, ___to);
}
// System.Void UnityEngine.Gizmos::DrawIcon(UnityEngine.Vector3,System.String)
extern "C" void Gizmos_DrawIcon_m3388 (Object_t * __this /* static, unused */, Vector3_t215  ___center, String_t* ___name, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 1;
		String_t* L_0 = ___name;
		bool L_1 = V_0;
		Gizmos_INTERNAL_CALL_DrawIcon_m5359(NULL /*static, unused*/, (&___center), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawIcon(UnityEngine.Vector3&,System.String,System.Boolean)
extern "C" void Gizmos_INTERNAL_CALL_DrawIcon_m5359 (Object_t * __this /* static, unused */, Vector3_t215 * ___center, String_t* ___name, bool ___allowScaling, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_CALL_DrawIcon_m5359_ftn) (Vector3_t215 *, String_t*, bool);
	static Gizmos_INTERNAL_CALL_DrawIcon_m5359_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_CALL_DrawIcon_m5359_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_CALL_DrawIcon(UnityEngine.Vector3&,System.String,System.Boolean)");
	_il2cpp_icall_func(___center, ___name, ___allowScaling);
}
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" void Gizmos_set_color_m3345 (Object_t * __this /* static, unused */, Color_t6  ___value, const MethodInfo* method)
{
	{
		Gizmos_INTERNAL_set_color_m5360(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Gizmos_INTERNAL_set_color_m5360 (Object_t * __this /* static, unused */, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Gizmos_INTERNAL_set_color_m5360_ftn) (Color_t6 *);
	static Gizmos_INTERNAL_set_color_m5360_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Gizmos_INTERNAL_set_color_m5360_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(___value);
}
#ifndef _MSC_VER
#else
#endif

// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeException.h"
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"


// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" void Vector2__ctor_m2714 (Vector2_t7 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		return;
	}
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral935;
extern "C" float Vector2_get_Item_m4746 (Vector2_t7 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral935 = il2cpp_codegen_string_literal_from_index(935);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		float L_3 = (__this->___x_1);
		return L_3;
	}

IL_001b:
	{
		float L_4 = (__this->___y_2);
		return L_4;
	}

IL_0022:
	{
		IndexOutOfRangeException_t1116 * L_5 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_5, _stringLiteral935, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral935;
extern "C" void Vector2_set_Item_m4754 (Vector2_t7 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral935 = il2cpp_codegen_string_literal_from_index(935);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		goto IL_002c;
	}

IL_0014:
	{
		float L_3 = ___value;
		__this->___x_1 = L_3;
		goto IL_0037;
	}

IL_0020:
	{
		float L_4 = ___value;
		__this->___y_2 = L_4;
		goto IL_0037;
	}

IL_002c:
	{
		IndexOutOfRangeException_t1116 * L_5 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_5, _stringLiteral935, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0037:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t7  Vector2_Scale_m4830 (Object_t * __this /* static, unused */, Vector2_t7  ___a, Vector2_t7  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t7  L_4 = {0};
		Vector2__ctor_m2714(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Vector2::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral936;
extern "C" String_t* Vector2_ToString_m5361 (Vector2_t7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral936 = il2cpp_codegen_string_literal_from_index(936);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 2));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral936, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C" int32_t Vector2_GetHashCode_m5362 (Vector2_t7 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m6357(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m6357(L_2, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern TypeInfo* Vector2_t7_il2cpp_TypeInfo_var;
extern "C" bool Vector2_Equals_m5363 (Vector2_t7 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Vector2_t7_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector2_t7 *)((Vector2_t7 *)UnBox (L_1, Vector2_t7_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m6375(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m6375(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		return G_B5_0;
	}
}
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" float Vector2_Dot_m4633 (Object_t * __this /* static, unused */, Vector2_t7  ___lhs, Vector2_t7  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___y_2);
		float L_3 = ((&___rhs)->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C" float Vector2_get_sqrMagnitude_m2801 (Vector2_t7 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Single UnityEngine.Vector2::SqrMagnitude(UnityEngine.Vector2)
extern "C" float Vector2_SqrMagnitude_m5364 (Object_t * __this /* static, unused */, Vector2_t7  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		return ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" Vector2_t7  Vector2_get_zero_m2793 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = {0};
		Vector2__ctor_m2714(&L_0, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C" Vector2_t7  Vector2_get_one_m4744 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = {0};
		Vector2__ctor_m2714(&L_0, (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C" Vector2_t7  Vector2_get_up_m4936 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = {0};
		Vector2__ctor_m2714(&L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t7  Vector2_op_Addition_m3335 (Object_t * __this /* static, unused */, Vector2_t7  ___a, Vector2_t7  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t7  L_4 = {0};
		Vector2__ctor_m2714(&L_4, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" Vector2_t7  Vector2_op_Subtraction_m2973 (Object_t * __this /* static, unused */, Vector2_t7  ___a, Vector2_t7  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		Vector2_t7  L_4 = {0};
		Vector2__ctor_m2714(&L_4, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t7  Vector2_op_Multiply_m3334 (Object_t * __this /* static, unused */, Vector2_t7  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		Vector2_t7  L_4 = {0};
		Vector2__ctor_m2714(&L_4, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C" Vector2_t7  Vector2_op_Division_m4798 (Object_t * __this /* static, unused */, Vector2_t7  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		Vector2_t7  L_4 = {0};
		Vector2__ctor_m2714(&L_4, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool Vector2_op_Equality_m4968 (Object_t * __this /* static, unused */, Vector2_t7  ___lhs, Vector2_t7  ___rhs, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = ___lhs;
		Vector2_t7  L_1 = ___rhs;
		Vector2_t7  L_2 = Vector2_op_Subtraction_m2973(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m5364(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" bool Vector2_op_Inequality_m4823 (Object_t * __this /* static, unused */, Vector2_t7  ___lhs, Vector2_t7  ___rhs, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = ___lhs;
		Vector2_t7  L_1 = ___rhs;
		Vector2_t7  L_2 = Vector2_op_Subtraction_m2973(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector2_SqrMagnitude_m5364(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" Vector2_t7  Vector2_op_Implicit_m4613 (Object_t * __this /* static, unused */, Vector3_t215  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector2_t7  L_2 = {0};
		Vector2__ctor_m2714(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" Vector3_t215  Vector2_op_Implicit_m4639 (Object_t * __this /* static, unused */, Vector2_t7  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector3_t215  L_2 = {0};
		Vector3__ctor_m2812(&L_2, L_0, L_1, (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" void Vector3__ctor_m2812 (Vector3_t215 * __this, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		return;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C" void Vector3__ctor_m4695 (Vector3_t215 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		__this->___z_3 = (0.0f);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" Vector3_t215  Vector3_Lerp_m2873 (Object_t * __this /* static, unused */, Vector3_t215  ___from, Vector3_t215  ___to, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ((&___from)->___x_1);
		float L_3 = ((&___to)->___x_1);
		float L_4 = ((&___from)->___x_1);
		float L_5 = ___t;
		float L_6 = ((&___from)->___y_2);
		float L_7 = ((&___to)->___y_2);
		float L_8 = ((&___from)->___y_2);
		float L_9 = ___t;
		float L_10 = ((&___from)->___z_3);
		float L_11 = ((&___to)->___z_3);
		float L_12 = ((&___from)->___z_3);
		float L_13 = ___t;
		Vector3_t215  L_14 = {0};
		Vector3__ctor_m2812(&L_14, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		return L_14;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" Vector3_t215  Vector3_MoveTowards_m3261 (Object_t * __this /* static, unused */, Vector3_t215  ___current, Vector3_t215  ___target, float ___maxDistanceDelta, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	float V_1 = 0.0f;
	{
		Vector3_t215  L_0 = ___target;
		Vector3_t215  L_1 = ___current;
		Vector3_t215  L_2 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_m3033((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = V_1;
		float L_5 = ___maxDistanceDelta;
		if ((((float)L_4) <= ((float)L_5)))
		{
			goto IL_0022;
		}
	}
	{
		float L_6 = V_1;
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_0024;
		}
	}

IL_0022:
	{
		Vector3_t215  L_7 = ___target;
		return L_7;
	}

IL_0024:
	{
		Vector3_t215  L_8 = ___current;
		Vector3_t215  L_9 = V_0;
		float L_10 = V_1;
		Vector3_t215  L_11 = Vector3_op_Division_m3285(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = ___maxDistanceDelta;
		Vector3_t215  L_13 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t215  L_14 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral937;
extern "C" float Vector3_get_Item_m4865 (Vector3_t215 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral937 = il2cpp_codegen_string_literal_from_index(937);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0020;
		}
		if (L_1 == 2)
		{
			goto IL_0027;
		}
	}
	{
		goto IL_002e;
	}

IL_0019:
	{
		float L_2 = (__this->___x_1);
		return L_2;
	}

IL_0020:
	{
		float L_3 = (__this->___y_2);
		return L_3;
	}

IL_0027:
	{
		float L_4 = (__this->___z_3);
		return L_4;
	}

IL_002e:
	{
		IndexOutOfRangeException_t1116 * L_5 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_5, _stringLiteral937, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}
}
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral937;
extern "C" void Vector3_set_Item_m4866 (Vector3_t215 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral937 = il2cpp_codegen_string_literal_from_index(937);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0019;
		}
		if (L_1 == 1)
		{
			goto IL_0025;
		}
		if (L_1 == 2)
		{
			goto IL_0031;
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		float L_2 = ___value;
		__this->___x_1 = L_2;
		goto IL_0048;
	}

IL_0025:
	{
		float L_3 = ___value;
		__this->___y_2 = L_3;
		goto IL_0048;
	}

IL_0031:
	{
		float L_4 = ___value;
		__this->___z_3 = L_4;
		goto IL_0048;
	}

IL_003d:
	{
		IndexOutOfRangeException_t1116 * L_5 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_5, _stringLiteral937, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0048:
	{
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t215  Vector3_Scale_m3327 (Object_t * __this /* static, unused */, Vector3_t215  ___a, Vector3_t215  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t215  L_6 = {0};
		Vector3__ctor_m2812(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C" int32_t Vector3_GetHashCode_m5365 (Vector3_t215 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m6357(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m6357(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m6357(L_4, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern TypeInfo* Vector3_t215_il2cpp_TypeInfo_var;
extern "C" bool Vector3_Equals_m5366 (Vector3_t215 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t215  V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Vector3_t215_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector3_t215 *)((Vector3_t215 *)UnBox (L_1, Vector3_t215_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m6375(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m6375(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m6375(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_0057;
	}

IL_0056:
	{
		G_B6_0 = 0;
	}

IL_0057:
	{
		return G_B6_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C" Vector3_t215  Vector3_Normalize_m5367 (Object_t * __this /* static, unused */, Vector3_t215  ___value, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t215  L_0 = ___value;
		float L_1 = Vector3_Magnitude_m5370(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_001a;
		}
	}
	{
		Vector3_t215  L_3 = ___value;
		float L_4 = V_0;
		Vector3_t215  L_5 = Vector3_op_Division_m3285(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		Vector3_t215  L_6 = Vector3_get_zero_m2826(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void UnityEngine.Vector3::Normalize()
extern "C" void Vector3_Normalize_m3009 (Vector3_t215 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Vector3_Magnitude_m5370(NULL /*static, unused*/, (*(Vector3_t215 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_002e;
		}
	}
	{
		float L_2 = V_0;
		Vector3_t215  L_3 = Vector3_op_Division_m3285(NULL /*static, unused*/, (*(Vector3_t215 *)__this), L_2, /*hidden argument*/NULL);
		*__this = L_3;
		goto IL_0039;
	}

IL_002e:
	{
		Vector3_t215  L_4 = Vector3_get_zero_m2826(NULL /*static, unused*/, /*hidden argument*/NULL);
		*__this = L_4;
	}

IL_0039:
	{
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C" Vector3_t215  Vector3_get_normalized_m3222 (Vector3_t215 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Vector3_Normalize_m5367(NULL /*static, unused*/, (*(Vector3_t215 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Vector3::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral938;
extern "C" String_t* Vector3_ToString_m5368 (Vector3_t215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral938 = il2cpp_codegen_string_literal_from_index(938);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 3));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t531_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral938, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.String UnityEngine.Vector3::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral939;
extern "C" String_t* Vector3_ToString_m5369 (Vector3_t215 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral939 = il2cpp_codegen_string_literal_from_index(939);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 3));
		float* L_1 = &(__this->___x_1);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m3117(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float* L_5 = &(__this->___y_2);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m3117(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float* L_9 = &(__this->___z_3);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m3117(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral939, L_8, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" float Vector3_Dot_m3153 (Object_t * __this /* static, unused */, Vector3_t215  ___lhs, Vector3_t215  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___y_2);
		float L_3 = ((&___rhs)->___y_2);
		float L_4 = ((&___lhs)->___z_3);
		float L_5 = ((&___rhs)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Vector3_Distance_m3142 (Object_t * __this /* static, unused */, Vector3_t215  ___a, Vector3_t215  ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t215  V_0 = {0};
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3__ctor_m2812((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = ((&V_0)->___x_1);
		float L_7 = ((&V_0)->___x_1);
		float L_8 = ((&V_0)->___y_2);
		float L_9 = ((&V_0)->___y_2);
		float L_10 = ((&V_0)->___z_3);
		float L_11 = ((&V_0)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		return L_12;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Vector3_Magnitude_m5370 (Object_t * __this /* static, unused */, Vector3_t215  ___a, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Vector3_get_magnitude_m3033 (Vector3_t215 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		float L_4 = (__this->___z_3);
		float L_5 = (__this->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C" float Vector3_SqrMagnitude_m5371 (Object_t * __this /* static, unused */, Vector3_t215  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___a)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___a)->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C" float Vector3_get_sqrMagnitude_m4892 (Vector3_t215 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___x_1);
		float L_1 = (__this->___x_1);
		float L_2 = (__this->___y_2);
		float L_3 = (__this->___y_2);
		float L_4 = (__this->___z_3);
		float L_5 = (__this->___z_3);
		return ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" Vector3_t215  Vector3_Min_m4875 (Object_t * __this /* static, unused */, Vector3_t215  ___lhs, Vector3_t215  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m3328(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Min_m3328(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Min_m3328(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t215  L_9 = {0};
		Vector3__ctor_m2812(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" Vector3_t215  Vector3_Max_m4876 (Object_t * __this /* static, unused */, Vector3_t215  ___lhs, Vector3_t215  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___lhs)->___x_1);
		float L_1 = ((&___rhs)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m2937(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = ((&___lhs)->___y_2);
		float L_4 = ((&___rhs)->___y_2);
		float L_5 = Mathf_Max_m2937(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___z_3);
		float L_8 = Mathf_Max_m2937(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t215  L_9 = {0};
		Vector3__ctor_m2812(&L_9, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C" Vector3_t215  Vector3_get_zero_m2826 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C" Vector3_t215  Vector3_get_one_m2890 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C" Vector3_t215  Vector3_get_forward_m2966 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C" Vector3_t215  Vector3_get_back_m3195 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C" Vector3_t215  Vector3_get_up_m3249 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C" Vector3_t215  Vector3_get_down_m3217 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C" Vector3_t215  Vector3_get_left_m4893 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C" Vector3_t215  Vector3_get_right_m2880 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t215  Vector3_op_Addition_m2901 (Object_t * __this /* static, unused */, Vector3_t215  ___a, Vector3_t215  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t215  L_6 = {0};
		Vector3__ctor_m2812(&L_6, ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Vector3_t215  Vector3_op_Subtraction_m3032 (Object_t * __this /* static, unused */, Vector3_t215  ___a, Vector3_t215  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		Vector3_t215  L_6 = {0};
		Vector3__ctor_m2812(&L_6, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern "C" Vector3_t215  Vector3_op_UnaryNegation_m3244 (Object_t * __this /* static, unused */, Vector3_t215  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___a)->___y_2);
		float L_2 = ((&___a)->___z_3);
		Vector3_t215  L_3 = {0};
		Vector3__ctor_m2812(&L_3, ((-L_0)), ((-L_1)), ((-L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t215  Vector3_op_Multiply_m2770 (Object_t * __this /* static, unused */, Vector3_t215  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t215  L_6 = {0};
		Vector3__ctor_m2812(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C" Vector3_t215  Vector3_op_Multiply_m2881 (Object_t * __this /* static, unused */, float ___d, Vector3_t215  ___a, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t215  L_6 = {0};
		Vector3__ctor_m2812(&L_6, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C" Vector3_t215  Vector3_op_Division_m3285 (Object_t * __this /* static, unused */, Vector3_t215  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		Vector3_t215  L_6 = {0};
		Vector3__ctor_m2812(&L_6, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Equality_m3326 (Object_t * __this /* static, unused */, Vector3_t215  ___lhs, Vector3_t215  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___lhs;
		Vector3_t215  L_1 = ___rhs;
		Vector3_t215  L_2 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m5371(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool Vector3_op_Inequality_m3262 (Object_t * __this /* static, unused */, Vector3_t215  ___lhs, Vector3_t215  ___rhs, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___lhs;
		Vector3_t215  L_1 = ___rhs;
		Vector3_t215  L_2 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m5371(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_3) >= ((float)(9.99999944E-11f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m2713 (Color_t6 * __this, float ___r, float ___g, float ___b, float ___a, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		float L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C" void Color__ctor_m2747 (Color_t6 * __this, float ___r, float ___g, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ___r;
		__this->___r_0 = L_0;
		float L_1 = ___g;
		__this->___g_1 = L_1;
		float L_2 = ___b;
		__this->___b_2 = L_2;
		__this->___a_3 = (1.0f);
		return;
	}
}
// System.String UnityEngine.Color::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral940;
extern "C" String_t* Color_ToString_m5372 (Color_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral940 = il2cpp_codegen_string_literal_from_index(940);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___r_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float L_5 = (__this->___g_1);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float L_9 = (__this->___b_2);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t531_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		float L_13 = (__this->___a_3);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral940, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C" int32_t Color_GetHashCode_m5373 (Color_t6 * __this, const MethodInfo* method)
{
	Vector4_t5  V_0 = {0};
	{
		Vector4_t5  L_0 = Color_op_Implicit_m5374(NULL /*static, unused*/, (*(Color_t6 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m5448((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern TypeInfo* Color_t6_il2cpp_TypeInfo_var;
extern "C" bool Color_Equals_m4701 (Color_t6 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(195);
		s_Il2CppMethodIntialized = true;
	}
	Color_t6  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Color_t6_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Color_t6 *)((Color_t6 *)UnBox (L_1, Color_t6_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___r_0);
		float L_3 = ((&V_0)->___r_0);
		bool L_4 = Single_Equals_m6375(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___g_1);
		float L_6 = ((&V_0)->___g_1);
		bool L_7 = Single_Equals_m6375(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___b_2);
		float L_9 = ((&V_0)->___b_2);
		bool L_10 = Single_Equals_m6375(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___a_3);
		float L_12 = ((&V_0)->___a_3);
		bool L_13 = Single_Equals_m6375(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" Color_t6  Color_Lerp_m4652 (Object_t * __this /* static, unused */, Color_t6  ___a, Color_t6  ___b, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ((&___a)->___r_0);
		float L_3 = ((&___b)->___r_0);
		float L_4 = ((&___a)->___r_0);
		float L_5 = ___t;
		float L_6 = ((&___a)->___g_1);
		float L_7 = ((&___b)->___g_1);
		float L_8 = ((&___a)->___g_1);
		float L_9 = ___t;
		float L_10 = ((&___a)->___b_2);
		float L_11 = ((&___b)->___b_2);
		float L_12 = ((&___a)->___b_2);
		float L_13 = ___t;
		float L_14 = ((&___a)->___a_3);
		float L_15 = ((&___b)->___a_3);
		float L_16 = ((&___a)->___a_3);
		float L_17 = ___t;
		Color_t6  L_18 = {0};
		Color__ctor_m2713(&L_18, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		return L_18;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C" Color_t6  Color_get_red_m2764 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6  L_0 = {0};
		Color__ctor_m2713(&L_0, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C" Color_t6  Color_get_green_m2763 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6  L_0 = {0};
		Color__ctor_m2713(&L_0, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C" Color_t6  Color_get_white_m2920 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6  L_0 = {0};
		Color__ctor_m2713(&L_0, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C" Color_t6  Color_get_black_m2921 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6  L_0 = {0};
		Color__ctor_m2713(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_cyan()
extern "C" Color_t6  Color_get_cyan_m3416 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6  L_0 = {0};
		Color__ctor_m2713(&L_0, (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_magenta()
extern "C" Color_t6  Color_get_magenta_m2904 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6  L_0 = {0};
		Color__ctor_m2713(&L_0, (1.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C" Color_t6  Color_get_clear_m2774 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Color_t6  L_0 = {0};
		Color__ctor_m2713(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C" Color_t6  Color_op_Multiply_m4889 (Object_t * __this /* static, unused */, Color_t6  ___a, float ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___r_0);
		float L_1 = ___b;
		float L_2 = ((&___a)->___g_1);
		float L_3 = ___b;
		float L_4 = ((&___a)->___b_2);
		float L_5 = ___b;
		float L_6 = ((&___a)->___a_3);
		float L_7 = ___b;
		Color_t6  L_8 = {0};
		Color__ctor_m2713(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C" Vector4_t5  Color_op_Implicit_m5374 (Object_t * __this /* static, unused */, Color_t6  ___c, const MethodInfo* method)
{
	{
		float L_0 = ((&___c)->___r_0);
		float L_1 = ((&___c)->___g_1);
		float L_2 = ((&___c)->___b_2);
		float L_3 = ((&___c)->___a_3);
		Vector4_t5  L_4 = {0};
		Vector4__ctor_m2728(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Implicit(UnityEngine.Vector4)
extern "C" Color_t6  Color_op_Implicit_m5375 (Object_t * __this /* static, unused */, Vector4_t5  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		float L_2 = ((&___v)->___z_3);
		float L_3 = ((&___v)->___w_4);
		Color_t6  L_4 = {0};
		Color__ctor_m2713(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C" void Color32__ctor_m4667 (Color32_t778 * __this, uint8_t ___r, uint8_t ___g, uint8_t ___b, uint8_t ___a, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___r;
		__this->___r_0 = L_0;
		uint8_t L_1 = ___g;
		__this->___g_1 = L_1;
		uint8_t L_2 = ___b;
		__this->___b_2 = L_2;
		uint8_t L_3 = ___a;
		__this->___a_3 = L_3;
		return;
	}
}
// System.String UnityEngine.Color32::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t1117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral941;
extern "C" String_t* Color32_ToString_m5376 (Color32_t778 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Byte_t1117_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(658);
		_stringLiteral941 = il2cpp_codegen_string_literal_from_index(941);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		uint8_t L_1 = (__this->___r_0);
		uint8_t L_2 = L_1;
		Object_t * L_3 = Box(Byte_t1117_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		uint8_t L_5 = (__this->___g_1);
		uint8_t L_6 = L_5;
		Object_t * L_7 = Box(Byte_t1117_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		uint8_t L_9 = (__this->___b_2);
		uint8_t L_10 = L_9;
		Object_t * L_11 = Box(Byte_t1117_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		uint8_t L_13 = (__this->___a_3);
		uint8_t L_14 = L_13;
		Object_t * L_15 = Box(Byte_t1117_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral941, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" Color32_t778  Color32_op_Implicit_m4694 (Object_t * __this /* static, unused */, Color_t6  ___c, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___c)->___r_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ((&___c)->___g_1);
		float L_3 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = ((&___c)->___b_2);
		float L_5 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = ((&___c)->___a_3);
		float L_7 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t778  L_8 = {0};
		Color32__ctor_m4667(&L_8, (((uint8_t)((float)((float)L_1*(float)(255.0f))))), (((uint8_t)((float)((float)L_3*(float)(255.0f))))), (((uint8_t)((float)((float)L_5*(float)(255.0f))))), (((uint8_t)((float)((float)L_7*(float)(255.0f))))), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C" Color_t6  Color32_op_Implicit_m4668 (Object_t * __this /* static, unused */, Color32_t778  ___c, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((&___c)->___r_0);
		uint8_t L_1 = ((&___c)->___g_1);
		uint8_t L_2 = ((&___c)->___b_2);
		uint8_t L_3 = ((&___c)->___a_3);
		Color_t6  L_4 = {0};
		Color__ctor_m2713(&L_4, ((float)((float)(((float)L_0))/(float)(255.0f))), ((float)((float)(((float)L_1))/(float)(255.0f))), ((float)((float)(((float)L_2))/(float)(255.0f))), ((float)((float)(((float)L_3))/(float)(255.0f))), /*hidden argument*/NULL);
		return L_4;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"



// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Quaternion__ctor_m5377 (Quaternion_t261 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		float L_3 = ___w;
		__this->___w_4 = L_3;
		return;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C" Quaternion_t261  Quaternion_get_identity_m2888 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = {0};
		Quaternion__ctor_m5377(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" float Quaternion_Dot_m5378 (Object_t * __this /* static, unused */, Quaternion_t261  ___a, Quaternion_t261  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Quaternion_t261  Quaternion_FromToRotation_m3223 (Object_t * __this /* static, unused */, Vector3_t215  ___fromDirection, Vector3_t215  ___toDirection, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = Quaternion_INTERNAL_CALL_FromToRotation_m5379(NULL /*static, unused*/, (&___fromDirection), (&___toDirection), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Quaternion_t261  Quaternion_INTERNAL_CALL_FromToRotation_m5379 (Object_t * __this /* static, unused */, Vector3_t215 * ___fromDirection, Vector3_t215 * ___toDirection, const MethodInfo* method)
{
	typedef Quaternion_t261  (*Quaternion_INTERNAL_CALL_FromToRotation_m5379_ftn) (Vector3_t215 *, Vector3_t215 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m5379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m5379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___fromDirection, ___toDirection);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" Quaternion_t261  Quaternion_LookRotation_m3014 (Object_t * __this /* static, unused */, Vector3_t215  ___forward, Vector3_t215  ___upwards, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = Quaternion_INTERNAL_CALL_LookRotation_m5380(NULL /*static, unused*/, (&___forward), (&___upwards), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Quaternion_t261  Quaternion_INTERNAL_CALL_LookRotation_m5380 (Object_t * __this /* static, unused */, Vector3_t215 * ___forward, Vector3_t215 * ___upwards, const MethodInfo* method)
{
	typedef Quaternion_t261  (*Quaternion_INTERNAL_CALL_LookRotation_m5380_ftn) (Vector3_t215 *, Vector3_t215 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m5380_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m5380_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___forward, ___upwards);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C" Quaternion_t261  Quaternion_Slerp_m3224 (Object_t * __this /* static, unused */, Quaternion_t261  ___from, Quaternion_t261  ___to, float ___t, const MethodInfo* method)
{
	{
		float L_0 = ___t;
		Quaternion_t261  L_1 = Quaternion_INTERNAL_CALL_Slerp_m5381(NULL /*static, unused*/, (&___from), (&___to), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
extern "C" Quaternion_t261  Quaternion_INTERNAL_CALL_Slerp_m5381 (Object_t * __this /* static, unused */, Quaternion_t261 * ___from, Quaternion_t261 * ___to, float ___t, const MethodInfo* method)
{
	typedef Quaternion_t261  (*Quaternion_INTERNAL_CALL_Slerp_m5381_ftn) (Quaternion_t261 *, Quaternion_t261 *, float);
	static Quaternion_INTERNAL_CALL_Slerp_m5381_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m5381_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)");
	return _il2cpp_icall_func(___from, ___to, ___t);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C" Quaternion_t261  Quaternion_Inverse_m4890 (Object_t * __this /* static, unused */, Quaternion_t261  ___rotation, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = Quaternion_INTERNAL_CALL_Inverse_m5382(NULL /*static, unused*/, (&___rotation), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
extern "C" Quaternion_t261  Quaternion_INTERNAL_CALL_Inverse_m5382 (Object_t * __this /* static, unused */, Quaternion_t261 * ___rotation, const MethodInfo* method)
{
	typedef Quaternion_t261  (*Quaternion_INTERNAL_CALL_Inverse_m5382_ftn) (Quaternion_t261 *);
	static Quaternion_INTERNAL_CALL_Inverse_m5382_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m5382_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___rotation);
}
// System.String UnityEngine.Quaternion::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral942;
extern "C" String_t* Quaternion_ToString_m5383 (Quaternion_t261 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral942 = il2cpp_codegen_string_literal_from_index(942);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t531_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		float L_13 = (__this->___w_4);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral942, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C" Vector3_t215  Quaternion_get_eulerAngles_m5384 (Quaternion_t261 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Quaternion_Internal_ToEulerRad_m5386(NULL /*static, unused*/, (*(Quaternion_t261 *)__this), /*hidden argument*/NULL);
		Vector3_t215  L_1 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" Quaternion_t261  Quaternion_Euler_m5385 (Object_t * __this /* static, unused */, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		Vector3_t215  L_3 = {0};
		Vector3__ctor_m2812(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t215  L_4 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t261  L_5 = Quaternion_Internal_FromEulerRad_m5388(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C" Quaternion_t261  Quaternion_Euler_m3330 (Object_t * __this /* static, unused */, Vector3_t215  ___euler, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___euler;
		Vector3_t215  L_1 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t261  L_2 = Quaternion_Internal_FromEulerRad_m5388(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C" Vector3_t215  Quaternion_Internal_ToEulerRad_m5386 (Object_t * __this /* static, unused */, Quaternion_t261  ___rotation, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m5387(NULL /*static, unused*/, (&___rotation), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)
extern "C" Vector3_t215  Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m5387 (Object_t * __this /* static, unused */, Quaternion_t261 * ___rotation, const MethodInfo* method)
{
	typedef Vector3_t215  (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m5387_ftn) (Quaternion_t261 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m5387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m5387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___rotation);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C" Quaternion_t261  Quaternion_Internal_FromEulerRad_m5388 (Object_t * __this /* static, unused */, Vector3_t215  ___euler, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m5389(NULL /*static, unused*/, (&___euler), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)
extern "C" Quaternion_t261  Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m5389 (Object_t * __this /* static, unused */, Vector3_t215 * ___euler, const MethodInfo* method)
{
	typedef Quaternion_t261  (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m5389_ftn) (Vector3_t215 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m5389_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m5389_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___euler);
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C" int32_t Quaternion_GetHashCode_m5390 (Quaternion_t261 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m6357(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m6357(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m6357(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_4);
		int32_t L_7 = Single_GetHashCode_m6357(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern TypeInfo* Quaternion_t261_il2cpp_TypeInfo_var;
extern "C" bool Quaternion_Equals_m5391 (Quaternion_t261 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Quaternion_t261_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(659);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t261  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Quaternion_t261_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Quaternion_t261 *)((Quaternion_t261 *)UnBox (L_1, Quaternion_t261_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m6375(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m6375(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m6375(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_4);
		float L_12 = ((&V_0)->___w_4);
		bool L_13 = Single_Equals_m6375(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" Quaternion_t261  Quaternion_op_Multiply_m2898 (Object_t * __this /* static, unused */, Quaternion_t261  ___lhs, Quaternion_t261  ___rhs, const MethodInfo* method)
{
	{
		float L_0 = ((&___lhs)->___w_4);
		float L_1 = ((&___rhs)->___x_1);
		float L_2 = ((&___lhs)->___x_1);
		float L_3 = ((&___rhs)->___w_4);
		float L_4 = ((&___lhs)->___y_2);
		float L_5 = ((&___rhs)->___z_3);
		float L_6 = ((&___lhs)->___z_3);
		float L_7 = ((&___rhs)->___y_2);
		float L_8 = ((&___lhs)->___w_4);
		float L_9 = ((&___rhs)->___y_2);
		float L_10 = ((&___lhs)->___y_2);
		float L_11 = ((&___rhs)->___w_4);
		float L_12 = ((&___lhs)->___z_3);
		float L_13 = ((&___rhs)->___x_1);
		float L_14 = ((&___lhs)->___x_1);
		float L_15 = ((&___rhs)->___z_3);
		float L_16 = ((&___lhs)->___w_4);
		float L_17 = ((&___rhs)->___z_3);
		float L_18 = ((&___lhs)->___z_3);
		float L_19 = ((&___rhs)->___w_4);
		float L_20 = ((&___lhs)->___x_1);
		float L_21 = ((&___rhs)->___y_2);
		float L_22 = ((&___lhs)->___y_2);
		float L_23 = ((&___rhs)->___x_1);
		float L_24 = ((&___lhs)->___w_4);
		float L_25 = ((&___rhs)->___w_4);
		float L_26 = ((&___lhs)->___x_1);
		float L_27 = ((&___rhs)->___x_1);
		float L_28 = ((&___lhs)->___y_2);
		float L_29 = ((&___rhs)->___y_2);
		float L_30 = ((&___lhs)->___z_3);
		float L_31 = ((&___rhs)->___z_3);
		Quaternion_t261  L_32 = {0};
		Quaternion__ctor_m5377(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		return L_32;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Vector3_t215  Quaternion_op_Multiply_m2900 (Object_t * __this /* static, unused */, Quaternion_t261  ___rotation, Vector3_t215  ___point, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t215  V_12 = {0};
	{
		float L_0 = ((&___rotation)->___x_1);
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = ((&___rotation)->___y_2);
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = ((&___rotation)->___z_3);
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = ((&___rotation)->___x_1);
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = ((&___rotation)->___y_2);
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = ((&___rotation)->___z_3);
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = ((&___rotation)->___x_1);
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = ((&___rotation)->___x_1);
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = ((&___rotation)->___y_2);
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = ((&___rotation)->___w_4);
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = ((&___rotation)->___w_4);
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = ((&___rotation)->___w_4);
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = ((&___point)->___x_1);
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = ((&___point)->___y_2);
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = ((&___point)->___z_3);
		(&V_12)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = ((&___point)->___x_1);
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = ((&___point)->___y_2);
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = ((&___point)->___z_3);
		(&V_12)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = ((&___point)->___x_1);
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = ((&___point)->___y_2);
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = ((&___point)->___z_3);
		(&V_12)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47))));
		Vector3_t215  L_48 = V_12;
		return L_48;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C" bool Quaternion_op_Inequality_m4822 (Object_t * __this /* static, unused */, Quaternion_t261  ___lhs, Quaternion_t261  ___rhs, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = ___lhs;
		Quaternion_t261  L_1 = ___rhs;
		float L_2 = Quaternion_Dot_m5378(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)((!(((float)L_2) <= ((float)(0.999999f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Rect__ctor_m2802 (Rect_t225 * __this, float ___left, float ___top, float ___width, float ___height, const MethodInfo* method)
{
	{
		float L_0 = ___left;
		__this->___m_XMin_0 = L_0;
		float L_1 = ___top;
		__this->___m_YMin_1 = L_1;
		float L_2 = ___width;
		__this->___m_Width_2 = L_2;
		float L_3 = ___height;
		__this->___m_Height_3 = L_3;
		return;
	}
}
// System.Void UnityEngine.Rect::Set(System.Single,System.Single,System.Single,System.Single)
extern "C" void Rect_Set_m3043 (Rect_t225 * __this, float ___left, float ___top, float ___width, float ___height, const MethodInfo* method)
{
	{
		float L_0 = ___left;
		__this->___m_XMin_0 = L_0;
		float L_1 = ___top;
		__this->___m_YMin_1 = L_1;
		float L_2 = ___width;
		__this->___m_Width_2 = L_2;
		float L_3 = ___height;
		__this->___m_Height_3 = L_3;
		return;
	}
}
// System.Single UnityEngine.Rect::get_x()
extern "C" float Rect_get_x_m2861 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C" void Rect_set_x_m2862 (Rect_t225 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_XMin_0 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_y()
extern "C" float Rect_get_y_m2865 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C" void Rect_set_y_m2866 (Rect_t225 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_YMin_1 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C" Vector2_t7  Rect_get_position_m2806 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		float L_1 = (__this->___m_YMin_1);
		Vector2_t7  L_2 = {0};
		Vector2__ctor_m2714(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C" Vector2_t7  Rect_get_center_m2891 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = Rect_get_x_m2861(__this, /*hidden argument*/NULL);
		float L_1 = (__this->___m_Width_2);
		float L_2 = Rect_get_y_m2865(__this, /*hidden argument*/NULL);
		float L_3 = (__this->___m_Height_3);
		Vector2_t7  L_4 = {0};
		Vector2__ctor_m2714(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Rect::set_center(UnityEngine.Vector2)
extern "C" void Rect_set_center_m2892 (Rect_t225 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	{
		float L_0 = ((&___value)->___x_1);
		float L_1 = (__this->___m_Width_2);
		__this->___m_XMin_0 = ((float)((float)L_0-(float)((float)((float)L_1/(float)(2.0f)))));
		float L_2 = ((&___value)->___y_2);
		float L_3 = (__this->___m_Height_3);
		__this->___m_YMin_1 = ((float)((float)L_2-(float)((float)((float)L_3/(float)(2.0f)))));
		return;
	}
}
// System.Single UnityEngine.Rect::get_width()
extern "C" float Rect_get_width_m2798 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C" void Rect_set_width_m2863 (Rect_t225 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Width_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Rect::get_height()
extern "C" float Rect_get_height_m2800 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		return L_0;
	}
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C" void Rect_set_height_m2864 (Rect_t225 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Height_3 = L_0;
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C" Vector2_t7  Rect_get_size_m2808 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		float L_1 = (__this->___m_Height_3);
		Vector2_t7  L_2 = {0};
		Vector2__ctor_m2714(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C" float Rect_get_xMin_m2797 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_XMin_0);
		return L_0;
	}
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C" float Rect_get_yMin_m2799 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_YMin_1);
		return L_0;
	}
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C" float Rect_get_xMax_m3060 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Width_2);
		float L_1 = (__this->___m_XMin_0);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C" float Rect_get_yMax_m4751 (Rect_t225 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Height_3);
		float L_1 = (__this->___m_YMin_1);
		return ((float)((float)L_0+(float)L_1));
	}
}
// System.String UnityEngine.Rect::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral943;
extern "C" String_t* Rect_ToString_m5392 (Rect_t225 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral943 = il2cpp_codegen_string_literal_from_index(943);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		float L_1 = Rect_get_x_m2861(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float L_5 = Rect_get_y_m2865(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float L_9 = Rect_get_width_m2798(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t531_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		float L_13 = Rect_get_height_m2800(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral943, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C" bool Rect_Contains_m5393 (Rect_t225 * __this, Vector3_t215  ___point, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = ((&___point)->___x_1);
		float L_1 = Rect_get_xMin_m2797(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ((&___point)->___x_1);
		float L_3 = Rect_get_xMax_m3060(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&___point)->___y_2);
		float L_5 = Rect_get_yMin_m2799(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ((&___point)->___y_2);
		float L_7 = Rect_get_yMax_m4751(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0048;
	}

IL_0047:
	{
		G_B5_0 = 0;
	}

IL_0048:
	{
		return G_B5_0;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C" int32_t Rect_GetHashCode_m5394 (Rect_t225 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		float L_0 = Rect_get_x_m2861(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m6357((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m2798(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m6357((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m2865(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m6357((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m2800(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m6357((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern TypeInfo* Rect_t225_il2cpp_TypeInfo_var;
extern "C" bool Rect_Equals_m5395 (Rect_t225 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t225_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(233);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t225  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Rect_t225_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Rect_t225 *)((Rect_t225 *)UnBox (L_1, Rect_t225_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m2861(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = Rect_get_x_m2861((&V_0), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m6375((&V_1), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007a;
		}
	}
	{
		float L_5 = Rect_get_y_m2865(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		float L_6 = Rect_get_y_m2865((&V_0), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m6375((&V_2), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = Rect_get_width_m2798(__this, /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = Rect_get_width_m2798((&V_0), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m6375((&V_3), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007a;
		}
	}
	{
		float L_11 = Rect_get_height_m2800(__this, /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = Rect_get_height_m2800((&V_0), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m6375((&V_4), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_007b;
	}

IL_007a:
	{
		G_B7_0 = 0;
	}

IL_007b:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C" bool Rect_op_Equality_m4845 (Object_t * __this /* static, unused */, Rect_t225  ___lhs, Rect_t225  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m2861((&___lhs), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m2861((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004b;
		}
	}
	{
		float L_2 = Rect_get_y_m2865((&___lhs), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m2865((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		float L_4 = Rect_get_width_m2798((&___lhs), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m2798((&___rhs), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004b;
		}
	}
	{
		float L_6 = Rect_get_height_m2800((&___lhs), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m2800((&___rhs), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 0;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"



// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern "C" float Matrix4x4_get_Item_m2856 (Matrix4x4_t242 * __this, int32_t ___row, int32_t ___column, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = Matrix4x4_get_Item_m5396(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern "C" void Matrix4x4_set_Item_m2858 (Matrix4x4_t242 * __this, int32_t ___row, int32_t ___column, float ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___row;
		int32_t L_1 = ___column;
		float L_2 = ___value;
		Matrix4x4_set_Item_m5397(__this, ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_1*(int32_t)4)))), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral944;
extern "C" float Matrix4x4_get_Item_m5396 (Matrix4x4_t242 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral944 = il2cpp_codegen_string_literal_from_index(944);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0054;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0062;
		}
		if (L_1 == 4)
		{
			goto IL_0069;
		}
		if (L_1 == 5)
		{
			goto IL_0070;
		}
		if (L_1 == 6)
		{
			goto IL_0077;
		}
		if (L_1 == 7)
		{
			goto IL_007e;
		}
		if (L_1 == 8)
		{
			goto IL_0085;
		}
		if (L_1 == 9)
		{
			goto IL_008c;
		}
		if (L_1 == 10)
		{
			goto IL_0093;
		}
		if (L_1 == 11)
		{
			goto IL_009a;
		}
		if (L_1 == 12)
		{
			goto IL_00a1;
		}
		if (L_1 == 13)
		{
			goto IL_00a8;
		}
		if (L_1 == 14)
		{
			goto IL_00af;
		}
		if (L_1 == 15)
		{
			goto IL_00b6;
		}
	}
	{
		goto IL_00bd;
	}

IL_004d:
	{
		float L_2 = (__this->___m00_0);
		return L_2;
	}

IL_0054:
	{
		float L_3 = (__this->___m10_1);
		return L_3;
	}

IL_005b:
	{
		float L_4 = (__this->___m20_2);
		return L_4;
	}

IL_0062:
	{
		float L_5 = (__this->___m30_3);
		return L_5;
	}

IL_0069:
	{
		float L_6 = (__this->___m01_4);
		return L_6;
	}

IL_0070:
	{
		float L_7 = (__this->___m11_5);
		return L_7;
	}

IL_0077:
	{
		float L_8 = (__this->___m21_6);
		return L_8;
	}

IL_007e:
	{
		float L_9 = (__this->___m31_7);
		return L_9;
	}

IL_0085:
	{
		float L_10 = (__this->___m02_8);
		return L_10;
	}

IL_008c:
	{
		float L_11 = (__this->___m12_9);
		return L_11;
	}

IL_0093:
	{
		float L_12 = (__this->___m22_10);
		return L_12;
	}

IL_009a:
	{
		float L_13 = (__this->___m32_11);
		return L_13;
	}

IL_00a1:
	{
		float L_14 = (__this->___m03_12);
		return L_14;
	}

IL_00a8:
	{
		float L_15 = (__this->___m13_13);
		return L_15;
	}

IL_00af:
	{
		float L_16 = (__this->___m23_14);
		return L_16;
	}

IL_00b6:
	{
		float L_17 = (__this->___m33_15);
		return L_17;
	}

IL_00bd:
	{
		IndexOutOfRangeException_t1116 * L_18 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_18, _stringLiteral944, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral944;
extern "C" void Matrix4x4_set_Item_m5397 (Matrix4x4_t242 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral944 = il2cpp_codegen_string_literal_from_index(944);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_004d;
		}
		if (L_1 == 1)
		{
			goto IL_0059;
		}
		if (L_1 == 2)
		{
			goto IL_0065;
		}
		if (L_1 == 3)
		{
			goto IL_0071;
		}
		if (L_1 == 4)
		{
			goto IL_007d;
		}
		if (L_1 == 5)
		{
			goto IL_0089;
		}
		if (L_1 == 6)
		{
			goto IL_0095;
		}
		if (L_1 == 7)
		{
			goto IL_00a1;
		}
		if (L_1 == 8)
		{
			goto IL_00ad;
		}
		if (L_1 == 9)
		{
			goto IL_00b9;
		}
		if (L_1 == 10)
		{
			goto IL_00c5;
		}
		if (L_1 == 11)
		{
			goto IL_00d1;
		}
		if (L_1 == 12)
		{
			goto IL_00dd;
		}
		if (L_1 == 13)
		{
			goto IL_00e9;
		}
		if (L_1 == 14)
		{
			goto IL_00f5;
		}
		if (L_1 == 15)
		{
			goto IL_0101;
		}
	}
	{
		goto IL_010d;
	}

IL_004d:
	{
		float L_2 = ___value;
		__this->___m00_0 = L_2;
		goto IL_0118;
	}

IL_0059:
	{
		float L_3 = ___value;
		__this->___m10_1 = L_3;
		goto IL_0118;
	}

IL_0065:
	{
		float L_4 = ___value;
		__this->___m20_2 = L_4;
		goto IL_0118;
	}

IL_0071:
	{
		float L_5 = ___value;
		__this->___m30_3 = L_5;
		goto IL_0118;
	}

IL_007d:
	{
		float L_6 = ___value;
		__this->___m01_4 = L_6;
		goto IL_0118;
	}

IL_0089:
	{
		float L_7 = ___value;
		__this->___m11_5 = L_7;
		goto IL_0118;
	}

IL_0095:
	{
		float L_8 = ___value;
		__this->___m21_6 = L_8;
		goto IL_0118;
	}

IL_00a1:
	{
		float L_9 = ___value;
		__this->___m31_7 = L_9;
		goto IL_0118;
	}

IL_00ad:
	{
		float L_10 = ___value;
		__this->___m02_8 = L_10;
		goto IL_0118;
	}

IL_00b9:
	{
		float L_11 = ___value;
		__this->___m12_9 = L_11;
		goto IL_0118;
	}

IL_00c5:
	{
		float L_12 = ___value;
		__this->___m22_10 = L_12;
		goto IL_0118;
	}

IL_00d1:
	{
		float L_13 = ___value;
		__this->___m32_11 = L_13;
		goto IL_0118;
	}

IL_00dd:
	{
		float L_14 = ___value;
		__this->___m03_12 = L_14;
		goto IL_0118;
	}

IL_00e9:
	{
		float L_15 = ___value;
		__this->___m13_13 = L_15;
		goto IL_0118;
	}

IL_00f5:
	{
		float L_16 = ___value;
		__this->___m23_14 = L_16;
		goto IL_0118;
	}

IL_0101:
	{
		float L_17 = ___value;
		__this->___m33_15 = L_17;
		goto IL_0118;
	}

IL_010d:
	{
		IndexOutOfRangeException_t1116 * L_18 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_18, _stringLiteral944, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_18);
	}

IL_0118:
	{
		return;
	}
}
// System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern "C" int32_t Matrix4x4_GetHashCode_m5398 (Matrix4x4_t242 * __this, const MethodInfo* method)
{
	Vector4_t5  V_0 = {0};
	Vector4_t5  V_1 = {0};
	Vector4_t5  V_2 = {0};
	Vector4_t5  V_3 = {0};
	{
		Vector4_t5  L_0 = Matrix4x4_GetColumn_m3012(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m5448((&V_0), /*hidden argument*/NULL);
		Vector4_t5  L_2 = Matrix4x4_GetColumn_m3012(__this, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector4_GetHashCode_m5448((&V_1), /*hidden argument*/NULL);
		Vector4_t5  L_4 = Matrix4x4_GetColumn_m3012(__this, 2, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Vector4_GetHashCode_m5448((&V_2), /*hidden argument*/NULL);
		Vector4_t5  L_6 = Matrix4x4_GetColumn_m3012(__this, 3, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Vector4_GetHashCode_m5448((&V_3), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern TypeInfo* Matrix4x4_t242_il2cpp_TypeInfo_var;
extern TypeInfo* Vector4_t5_il2cpp_TypeInfo_var;
extern "C" bool Matrix4x4_Equals_m5399 (Matrix4x4_t242 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(223);
		Vector4_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	Vector4_t5  V_1 = {0};
	Vector4_t5  V_2 = {0};
	Vector4_t5  V_3 = {0};
	Vector4_t5  V_4 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Matrix4x4_t242_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Matrix4x4_t242 *)((Matrix4x4_t242 *)UnBox (L_1, Matrix4x4_t242_il2cpp_TypeInfo_var))));
		Vector4_t5  L_2 = Matrix4x4_GetColumn_m3012(__this, 0, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector4_t5  L_3 = Matrix4x4_GetColumn_m3012((&V_0), 0, /*hidden argument*/NULL);
		Vector4_t5  L_4 = L_3;
		Object_t * L_5 = Box(Vector4_t5_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector4_Equals_m5449((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t5  L_7 = Matrix4x4_GetColumn_m3012(__this, 1, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector4_t5  L_8 = Matrix4x4_GetColumn_m3012((&V_0), 1, /*hidden argument*/NULL);
		Vector4_t5  L_9 = L_8;
		Object_t * L_10 = Box(Vector4_t5_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector4_Equals_m5449((&V_2), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t5  L_12 = Matrix4x4_GetColumn_m3012(__this, 2, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector4_t5  L_13 = Matrix4x4_GetColumn_m3012((&V_0), 2, /*hidden argument*/NULL);
		Vector4_t5  L_14 = L_13;
		Object_t * L_15 = Box(Vector4_t5_il2cpp_TypeInfo_var, &L_14);
		bool L_16 = Vector4_Equals_m5449((&V_3), L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0096;
		}
	}
	{
		Vector4_t5  L_17 = Matrix4x4_GetColumn_m3012(__this, 3, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector4_t5  L_18 = Matrix4x4_GetColumn_m3012((&V_0), 3, /*hidden argument*/NULL);
		Vector4_t5  L_19 = L_18;
		Object_t * L_20 = Box(Vector4_t5_il2cpp_TypeInfo_var, &L_19);
		bool L_21 = Vector4_Equals_m5449((&V_4), L_20, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_21));
		goto IL_0097;
	}

IL_0096:
	{
		G_B7_0 = 0;
	}

IL_0097:
	{
		return G_B7_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Inverse(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t242  Matrix4x4_Inverse_m2875 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = Matrix4x4_INTERNAL_CALL_Inverse_m5400(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t242  Matrix4x4_INTERNAL_CALL_Inverse_m5400 (Object_t * __this /* static, unused */, Matrix4x4_t242 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t242  (*Matrix4x4_INTERNAL_CALL_Inverse_m5400_ftn) (Matrix4x4_t242 *);
	static Matrix4x4_INTERNAL_CALL_Inverse_m5400_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Inverse_m5400_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Transpose(UnityEngine.Matrix4x4)
extern "C" Matrix4x4_t242  Matrix4x4_Transpose_m5401 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___m, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = Matrix4x4_INTERNAL_CALL_Transpose_m5402(NULL /*static, unused*/, (&___m), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)
extern "C" Matrix4x4_t242  Matrix4x4_INTERNAL_CALL_Transpose_m5402 (Object_t * __this /* static, unused */, Matrix4x4_t242 * ___m, const MethodInfo* method)
{
	typedef Matrix4x4_t242  (*Matrix4x4_INTERNAL_CALL_Transpose_m5402_ftn) (Matrix4x4_t242 *);
	static Matrix4x4_INTERNAL_CALL_Transpose_m5402_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Transpose_m5402_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___m);
}
// System.Boolean UnityEngine.Matrix4x4::Invert(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_Invert_m5403 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___inMatrix, Matrix4x4_t242 * ___dest, const MethodInfo* method)
{
	{
		Matrix4x4_t242 * L_0 = ___dest;
		bool L_1 = Matrix4x4_INTERNAL_CALL_Invert_m5404(NULL /*static, unused*/, (&___inMatrix), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
extern "C" bool Matrix4x4_INTERNAL_CALL_Invert_m5404 (Object_t * __this /* static, unused */, Matrix4x4_t242 * ___inMatrix, Matrix4x4_t242 * ___dest, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_INTERNAL_CALL_Invert_m5404_ftn) (Matrix4x4_t242 *, Matrix4x4_t242 *);
	static Matrix4x4_INTERNAL_CALL_Invert_m5404_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_Invert_m5404_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)");
	return _il2cpp_icall_func(___inMatrix, ___dest);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern "C" Matrix4x4_t242  Matrix4x4_get_inverse_m3041 (Matrix4x4_t242 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = Matrix4x4_Inverse_m2875(NULL /*static, unused*/, (*(Matrix4x4_t242 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_transpose()
extern "C" Matrix4x4_t242  Matrix4x4_get_transpose_m5405 (Matrix4x4_t242 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = Matrix4x4_Transpose_m5401(NULL /*static, unused*/, (*(Matrix4x4_t242 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::get_isIdentity()
extern "C" bool Matrix4x4_get_isIdentity_m5406 (Matrix4x4_t242 * __this, const MethodInfo* method)
{
	typedef bool (*Matrix4x4_get_isIdentity_m5406_ftn) (Matrix4x4_t242 *);
	static Matrix4x4_get_isIdentity_m5406_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_get_isIdentity_m5406_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::get_isIdentity()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern "C" Vector4_t5  Matrix4x4_GetColumn_m3012 (Matrix4x4_t242 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m2856(__this, 0, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m2856(__this, 1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m2856(__this, 2, L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m2856(__this, 3, L_6, /*hidden argument*/NULL);
		Vector4_t5  L_8 = {0};
		Vector4__ctor_m2728(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetRow(System.Int32)
extern "C" Vector4_t5  Matrix4x4_GetRow_m5407 (Matrix4x4_t242 * __this, int32_t ___i, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = Matrix4x4_get_Item_m2856(__this, L_0, 0, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = Matrix4x4_get_Item_m2856(__this, L_2, 1, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = Matrix4x4_get_Item_m2856(__this, L_4, 2, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = Matrix4x4_get_Item_m2856(__this, L_6, 3, /*hidden argument*/NULL);
		Vector4_t5  L_8 = {0};
		Vector4__ctor_m2728(&L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Matrix4x4::SetColumn(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetColumn_m5408 (Matrix4x4_t242 * __this, int32_t ___i, Vector4_t5  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_1);
		Matrix4x4_set_Item_m2858(__this, 0, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_2);
		Matrix4x4_set_Item_m2858(__this, 1, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_3);
		Matrix4x4_set_Item_m2858(__this, 2, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_4);
		Matrix4x4_set_Item_m2858(__this, 3, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Matrix4x4::SetRow(System.Int32,UnityEngine.Vector4)
extern "C" void Matrix4x4_SetRow_m5409 (Matrix4x4_t242 * __this, int32_t ___i, Vector4_t5  ___v, const MethodInfo* method)
{
	{
		int32_t L_0 = ___i;
		float L_1 = ((&___v)->___x_1);
		Matrix4x4_set_Item_m2858(__this, L_0, 0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___i;
		float L_3 = ((&___v)->___y_2);
		Matrix4x4_set_Item_m2858(__this, L_2, 1, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___i;
		float L_5 = ((&___v)->___z_3);
		Matrix4x4_set_Item_m2858(__this, L_4, 2, L_5, /*hidden argument*/NULL);
		int32_t L_6 = ___i;
		float L_7 = ((&___v)->___w_4);
		Matrix4x4_set_Item_m2858(__this, L_6, 3, L_7, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern "C" Vector3_t215  Matrix4x4_MultiplyPoint_m5410 (Matrix4x4_t242 * __this, Vector3_t215  ___v, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	float V_1 = 0.0f;
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		float L_21 = (__this->___m30_3);
		float L_22 = ((&___v)->___x_1);
		float L_23 = (__this->___m31_7);
		float L_24 = ((&___v)->___y_2);
		float L_25 = (__this->___m32_11);
		float L_26 = ((&___v)->___z_3);
		float L_27 = (__this->___m33_15);
		V_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_21*(float)L_22))+(float)((float)((float)L_23*(float)L_24))))+(float)((float)((float)L_25*(float)L_26))))+(float)L_27));
		float L_28 = V_1;
		V_1 = ((float)((float)(1.0f)/(float)L_28));
		Vector3_t215 * L_29 = (&V_0);
		float L_30 = (L_29->___x_1);
		float L_31 = V_1;
		L_29->___x_1 = ((float)((float)L_30*(float)L_31));
		Vector3_t215 * L_32 = (&V_0);
		float L_33 = (L_32->___y_2);
		float L_34 = V_1;
		L_32->___y_2 = ((float)((float)L_33*(float)L_34));
		Vector3_t215 * L_35 = (&V_0);
		float L_36 = (L_35->___z_3);
		float L_37 = V_1;
		L_35->___z_3 = ((float)((float)L_36*(float)L_37));
		Vector3_t215  L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern "C" Vector3_t215  Matrix4x4_MultiplyPoint3x4_m4874 (Matrix4x4_t242 * __this, Vector3_t215  ___v, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = (__this->___m03_12);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)L_6));
		float L_7 = (__this->___m10_1);
		float L_8 = ((&___v)->___x_1);
		float L_9 = (__this->___m11_5);
		float L_10 = ((&___v)->___y_2);
		float L_11 = (__this->___m12_9);
		float L_12 = ((&___v)->___z_3);
		float L_13 = (__this->___m13_13);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)L_10))))+(float)((float)((float)L_11*(float)L_12))))+(float)L_13));
		float L_14 = (__this->___m20_2);
		float L_15 = ((&___v)->___x_1);
		float L_16 = (__this->___m21_6);
		float L_17 = ((&___v)->___y_2);
		float L_18 = (__this->___m22_10);
		float L_19 = ((&___v)->___z_3);
		float L_20 = (__this->___m23_14);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17))))+(float)((float)((float)L_18*(float)L_19))))+(float)L_20));
		Vector3_t215  L_21 = V_0;
		return L_21;
	}
}
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern "C" Vector3_t215  Matrix4x4_MultiplyVector_m5411 (Matrix4x4_t242 * __this, Vector3_t215  ___v, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		float L_0 = (__this->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = (__this->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = (__this->___m02_8);
		float L_5 = ((&___v)->___z_3);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		float L_6 = (__this->___m10_1);
		float L_7 = ((&___v)->___x_1);
		float L_8 = (__this->___m11_5);
		float L_9 = ((&___v)->___y_2);
		float L_10 = (__this->___m12_9);
		float L_11 = ((&___v)->___z_3);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11))));
		float L_12 = (__this->___m20_2);
		float L_13 = ((&___v)->___x_1);
		float L_14 = (__this->___m21_6);
		float L_15 = ((&___v)->___y_2);
		float L_16 = (__this->___m22_10);
		float L_17 = ((&___v)->___z_3);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)L_12*(float)L_13))+(float)((float)((float)L_14*(float)L_15))))+(float)((float)((float)L_16*(float)L_17))));
		Vector3_t215  L_18 = V_0;
		return L_18;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Scale(UnityEngine.Vector3)
extern TypeInfo* Matrix4x4_t242_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t242  Matrix4x4_Scale_m3011 (Object_t * __this /* static, unused */, Vector3_t215  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(223);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	{
		Initobj (Matrix4x4_t242_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___v)->___x_1);
		(&V_0)->___m00_0 = L_0;
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		float L_1 = ((&___v)->___y_2);
		(&V_0)->___m11_5 = L_1;
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		float L_2 = ((&___v)->___z_3);
		(&V_0)->___m22_10 = L_2;
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t242  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern TypeInfo* Matrix4x4_t242_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t242  Matrix4x4_get_zero_m3061 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(223);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	{
		Initobj (Matrix4x4_t242_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (0.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (0.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (0.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (0.0f);
		Matrix4x4_t242  L_0 = V_0;
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern TypeInfo* Matrix4x4_t242_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t242  Matrix4x4_get_identity_m3010 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(223);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	{
		Initobj (Matrix4x4_t242_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___m00_0 = (1.0f);
		(&V_0)->___m01_4 = (0.0f);
		(&V_0)->___m02_8 = (0.0f);
		(&V_0)->___m03_12 = (0.0f);
		(&V_0)->___m10_1 = (0.0f);
		(&V_0)->___m11_5 = (1.0f);
		(&V_0)->___m12_9 = (0.0f);
		(&V_0)->___m13_13 = (0.0f);
		(&V_0)->___m20_2 = (0.0f);
		(&V_0)->___m21_6 = (0.0f);
		(&V_0)->___m22_10 = (1.0f);
		(&V_0)->___m23_14 = (0.0f);
		(&V_0)->___m30_3 = (0.0f);
		(&V_0)->___m31_7 = (0.0f);
		(&V_0)->___m32_11 = (0.0f);
		(&V_0)->___m33_15 = (1.0f);
		Matrix4x4_t242  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Matrix4x4::SetTRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" void Matrix4x4_SetTRS_m5412 (Matrix4x4_t242 * __this, Vector3_t215  ___pos, Quaternion_t261  ___q, Vector3_t215  ___s, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___pos;
		Quaternion_t261  L_1 = ___q;
		Vector3_t215  L_2 = ___s;
		Matrix4x4_t242  L_3 = Matrix4x4_TRS_m2923(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		*__this = L_3;
		return;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" Matrix4x4_t242  Matrix4x4_TRS_m2923 (Object_t * __this /* static, unused */, Vector3_t215  ___pos, Quaternion_t261  ___q, Vector3_t215  ___s, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = Matrix4x4_INTERNAL_CALL_TRS_m5413(NULL /*static, unused*/, (&___pos), (&___q), (&___s), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C" Matrix4x4_t242  Matrix4x4_INTERNAL_CALL_TRS_m5413 (Object_t * __this /* static, unused */, Vector3_t215 * ___pos, Quaternion_t261 * ___q, Vector3_t215 * ___s, const MethodInfo* method)
{
	typedef Matrix4x4_t242  (*Matrix4x4_INTERNAL_CALL_TRS_m5413_ftn) (Vector3_t215 *, Quaternion_t261 *, Vector3_t215 *);
	static Matrix4x4_INTERNAL_CALL_TRS_m5413_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_INTERNAL_CALL_TRS_m5413_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___pos, ___q, ___s);
}
// System.String UnityEngine.Matrix4x4::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral945;
extern "C" String_t* Matrix4x4_ToString_m5414 (Matrix4x4_t242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral945 = il2cpp_codegen_string_literal_from_index(945);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, ((int32_t)16)));
		float L_1 = (__this->___m00_0);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float L_5 = (__this->___m01_4);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float L_9 = (__this->___m02_8);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t531_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		float L_13 = (__this->___m03_12);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		ObjectU5BU5D_t470* L_16 = L_12;
		float L_17 = (__this->___m10_1);
		float L_18 = L_17;
		Object_t * L_19 = Box(Single_t531_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4)) = (Object_t *)L_19;
		ObjectU5BU5D_t470* L_20 = L_16;
		float L_21 = (__this->___m11_5);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t531_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5)) = (Object_t *)L_23;
		ObjectU5BU5D_t470* L_24 = L_20;
		float L_25 = (__this->___m12_9);
		float L_26 = L_25;
		Object_t * L_27 = Box(Single_t531_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6)) = (Object_t *)L_27;
		ObjectU5BU5D_t470* L_28 = L_24;
		float L_29 = (__this->___m13_13);
		float L_30 = L_29;
		Object_t * L_31 = Box(Single_t531_il2cpp_TypeInfo_var, &L_30);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7)) = (Object_t *)L_31;
		ObjectU5BU5D_t470* L_32 = L_28;
		float L_33 = (__this->___m20_2);
		float L_34 = L_33;
		Object_t * L_35 = Box(Single_t531_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8)) = (Object_t *)L_35;
		ObjectU5BU5D_t470* L_36 = L_32;
		float L_37 = (__this->___m21_6);
		float L_38 = L_37;
		Object_t * L_39 = Box(Single_t531_il2cpp_TypeInfo_var, &L_38);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9))) = (Object_t *)L_39;
		ObjectU5BU5D_t470* L_40 = L_36;
		float L_41 = (__this->___m22_10);
		float L_42 = L_41;
		Object_t * L_43 = Box(Single_t531_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10))) = (Object_t *)L_43;
		ObjectU5BU5D_t470* L_44 = L_40;
		float L_45 = (__this->___m23_14);
		float L_46 = L_45;
		Object_t * L_47 = Box(Single_t531_il2cpp_TypeInfo_var, &L_46);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11))) = (Object_t *)L_47;
		ObjectU5BU5D_t470* L_48 = L_44;
		float L_49 = (__this->___m30_3);
		float L_50 = L_49;
		Object_t * L_51 = Box(Single_t531_il2cpp_TypeInfo_var, &L_50);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12))) = (Object_t *)L_51;
		ObjectU5BU5D_t470* L_52 = L_48;
		float L_53 = (__this->___m31_7);
		float L_54 = L_53;
		Object_t * L_55 = Box(Single_t531_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13))) = (Object_t *)L_55;
		ObjectU5BU5D_t470* L_56 = L_52;
		float L_57 = (__this->___m32_11);
		float L_58 = L_57;
		Object_t * L_59 = Box(Single_t531_il2cpp_TypeInfo_var, &L_58);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14))) = (Object_t *)L_59;
		ObjectU5BU5D_t470* L_60 = L_56;
		float L_61 = (__this->___m33_15);
		float L_62 = L_61;
		Object_t * L_63 = Box(Single_t531_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral945, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// System.String UnityEngine.Matrix4x4::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral946;
extern "C" String_t* Matrix4x4_ToString_m5415 (Matrix4x4_t242 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral946 = il2cpp_codegen_string_literal_from_index(946);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, ((int32_t)16)));
		float* L_1 = &(__this->___m00_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Single_ToString_m3117(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float* L_5 = &(__this->___m01_4);
		String_t* L_6 = ___format;
		String_t* L_7 = Single_ToString_m3117(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float* L_9 = &(__this->___m02_8);
		String_t* L_10 = ___format;
		String_t* L_11 = Single_ToString_m3117(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		float* L_13 = &(__this->___m03_12);
		String_t* L_14 = ___format;
		String_t* L_15 = Single_ToString_m3117(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		ObjectU5BU5D_t470* L_16 = L_12;
		float* L_17 = &(__this->___m10_1);
		String_t* L_18 = ___format;
		String_t* L_19 = Single_ToString_m3117(L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 4);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 4)) = (Object_t *)L_19;
		ObjectU5BU5D_t470* L_20 = L_16;
		float* L_21 = &(__this->___m11_5);
		String_t* L_22 = ___format;
		String_t* L_23 = Single_ToString_m3117(L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5)) = (Object_t *)L_23;
		ObjectU5BU5D_t470* L_24 = L_20;
		float* L_25 = &(__this->___m12_9);
		String_t* L_26 = ___format;
		String_t* L_27 = Single_ToString_m3117(L_25, L_26, /*hidden argument*/NULL);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 6);
		ArrayElementTypeCheck (L_24, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 6)) = (Object_t *)L_27;
		ObjectU5BU5D_t470* L_28 = L_24;
		float* L_29 = &(__this->___m13_13);
		String_t* L_30 = ___format;
		String_t* L_31 = Single_ToString_m3117(L_29, L_30, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 7);
		ArrayElementTypeCheck (L_28, L_31);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_28, 7)) = (Object_t *)L_31;
		ObjectU5BU5D_t470* L_32 = L_28;
		float* L_33 = &(__this->___m20_2);
		String_t* L_34 = ___format;
		String_t* L_35 = Single_ToString_m3117(L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		ArrayElementTypeCheck (L_32, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_32, 8)) = (Object_t *)L_35;
		ObjectU5BU5D_t470* L_36 = L_32;
		float* L_37 = &(__this->___m21_6);
		String_t* L_38 = ___format;
		String_t* L_39 = Single_ToString_m3117(L_37, L_38, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)9));
		ArrayElementTypeCheck (L_36, L_39);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_36, ((int32_t)9))) = (Object_t *)L_39;
		ObjectU5BU5D_t470* L_40 = L_36;
		float* L_41 = &(__this->___m22_10);
		String_t* L_42 = ___format;
		String_t* L_43 = Single_ToString_m3117(L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)10));
		ArrayElementTypeCheck (L_40, L_43);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_40, ((int32_t)10))) = (Object_t *)L_43;
		ObjectU5BU5D_t470* L_44 = L_40;
		float* L_45 = &(__this->___m23_14);
		String_t* L_46 = ___format;
		String_t* L_47 = Single_ToString_m3117(L_45, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)11));
		ArrayElementTypeCheck (L_44, L_47);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_44, ((int32_t)11))) = (Object_t *)L_47;
		ObjectU5BU5D_t470* L_48 = L_44;
		float* L_49 = &(__this->___m30_3);
		String_t* L_50 = ___format;
		String_t* L_51 = Single_ToString_m3117(L_49, L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)12));
		ArrayElementTypeCheck (L_48, L_51);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_48, ((int32_t)12))) = (Object_t *)L_51;
		ObjectU5BU5D_t470* L_52 = L_48;
		float* L_53 = &(__this->___m31_7);
		String_t* L_54 = ___format;
		String_t* L_55 = Single_ToString_m3117(L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)13));
		ArrayElementTypeCheck (L_52, L_55);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_52, ((int32_t)13))) = (Object_t *)L_55;
		ObjectU5BU5D_t470* L_56 = L_52;
		float* L_57 = &(__this->___m32_11);
		String_t* L_58 = ___format;
		String_t* L_59 = Single_ToString_m3117(L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)14));
		ArrayElementTypeCheck (L_56, L_59);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_56, ((int32_t)14))) = (Object_t *)L_59;
		ObjectU5BU5D_t470* L_60 = L_56;
		float* L_61 = &(__this->___m33_15);
		String_t* L_62 = ___format;
		String_t* L_63 = Single_ToString_m3117(L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)15));
		ArrayElementTypeCheck (L_60, L_63);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_60, ((int32_t)15))) = (Object_t *)L_63;
		String_t* L_64 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral946, L_60, /*hidden argument*/NULL);
		return L_64;
	}
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t242  Matrix4x4_Ortho_m5416 (Object_t * __this /* static, unused */, float ___left, float ___right, float ___bottom, float ___top, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t242  (*Matrix4x4_Ortho_m5416_ftn) (float, float, float, float, float, float);
	static Matrix4x4_Ortho_m5416_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Ortho_m5416_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___left, ___right, ___bottom, ___top, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t242  Matrix4x4_Perspective_m5417 (Object_t * __this /* static, unused */, float ___fov, float ___aspect, float ___zNear, float ___zFar, const MethodInfo* method)
{
	typedef Matrix4x4_t242  (*Matrix4x4_Perspective_m5417_ftn) (float, float, float, float);
	static Matrix4x4_Perspective_m5417_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Matrix4x4_Perspective_m5417_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)");
	return _il2cpp_icall_func(___fov, ___aspect, ___zNear, ___zFar);
}
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern TypeInfo* Matrix4x4_t242_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t242  Matrix4x4_op_Multiply_m2876 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___lhs, Matrix4x4_t242  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Matrix4x4_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(223);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	{
		Initobj (Matrix4x4_t242_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___rhs)->___m00_0);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___rhs)->___m10_1);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___rhs)->___m20_2);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___rhs)->___m30_3);
		(&V_0)->___m00_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m00_0);
		float L_9 = ((&___rhs)->___m01_4);
		float L_10 = ((&___lhs)->___m01_4);
		float L_11 = ((&___rhs)->___m11_5);
		float L_12 = ((&___lhs)->___m02_8);
		float L_13 = ((&___rhs)->___m21_6);
		float L_14 = ((&___lhs)->___m03_12);
		float L_15 = ((&___rhs)->___m31_7);
		(&V_0)->___m01_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m00_0);
		float L_17 = ((&___rhs)->___m02_8);
		float L_18 = ((&___lhs)->___m01_4);
		float L_19 = ((&___rhs)->___m12_9);
		float L_20 = ((&___lhs)->___m02_8);
		float L_21 = ((&___rhs)->___m22_10);
		float L_22 = ((&___lhs)->___m03_12);
		float L_23 = ((&___rhs)->___m32_11);
		(&V_0)->___m02_8 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m00_0);
		float L_25 = ((&___rhs)->___m03_12);
		float L_26 = ((&___lhs)->___m01_4);
		float L_27 = ((&___rhs)->___m13_13);
		float L_28 = ((&___lhs)->___m02_8);
		float L_29 = ((&___rhs)->___m23_14);
		float L_30 = ((&___lhs)->___m03_12);
		float L_31 = ((&___rhs)->___m33_15);
		(&V_0)->___m03_12 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		float L_32 = ((&___lhs)->___m10_1);
		float L_33 = ((&___rhs)->___m00_0);
		float L_34 = ((&___lhs)->___m11_5);
		float L_35 = ((&___rhs)->___m10_1);
		float L_36 = ((&___lhs)->___m12_9);
		float L_37 = ((&___rhs)->___m20_2);
		float L_38 = ((&___lhs)->___m13_13);
		float L_39 = ((&___rhs)->___m30_3);
		(&V_0)->___m10_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_32*(float)L_33))+(float)((float)((float)L_34*(float)L_35))))+(float)((float)((float)L_36*(float)L_37))))+(float)((float)((float)L_38*(float)L_39))));
		float L_40 = ((&___lhs)->___m10_1);
		float L_41 = ((&___rhs)->___m01_4);
		float L_42 = ((&___lhs)->___m11_5);
		float L_43 = ((&___rhs)->___m11_5);
		float L_44 = ((&___lhs)->___m12_9);
		float L_45 = ((&___rhs)->___m21_6);
		float L_46 = ((&___lhs)->___m13_13);
		float L_47 = ((&___rhs)->___m31_7);
		(&V_0)->___m11_5 = ((float)((float)((float)((float)((float)((float)((float)((float)L_40*(float)L_41))+(float)((float)((float)L_42*(float)L_43))))+(float)((float)((float)L_44*(float)L_45))))+(float)((float)((float)L_46*(float)L_47))));
		float L_48 = ((&___lhs)->___m10_1);
		float L_49 = ((&___rhs)->___m02_8);
		float L_50 = ((&___lhs)->___m11_5);
		float L_51 = ((&___rhs)->___m12_9);
		float L_52 = ((&___lhs)->___m12_9);
		float L_53 = ((&___rhs)->___m22_10);
		float L_54 = ((&___lhs)->___m13_13);
		float L_55 = ((&___rhs)->___m32_11);
		(&V_0)->___m12_9 = ((float)((float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)((float)((float)L_52*(float)L_53))))+(float)((float)((float)L_54*(float)L_55))));
		float L_56 = ((&___lhs)->___m10_1);
		float L_57 = ((&___rhs)->___m03_12);
		float L_58 = ((&___lhs)->___m11_5);
		float L_59 = ((&___rhs)->___m13_13);
		float L_60 = ((&___lhs)->___m12_9);
		float L_61 = ((&___rhs)->___m23_14);
		float L_62 = ((&___lhs)->___m13_13);
		float L_63 = ((&___rhs)->___m33_15);
		(&V_0)->___m13_13 = ((float)((float)((float)((float)((float)((float)((float)((float)L_56*(float)L_57))+(float)((float)((float)L_58*(float)L_59))))+(float)((float)((float)L_60*(float)L_61))))+(float)((float)((float)L_62*(float)L_63))));
		float L_64 = ((&___lhs)->___m20_2);
		float L_65 = ((&___rhs)->___m00_0);
		float L_66 = ((&___lhs)->___m21_6);
		float L_67 = ((&___rhs)->___m10_1);
		float L_68 = ((&___lhs)->___m22_10);
		float L_69 = ((&___rhs)->___m20_2);
		float L_70 = ((&___lhs)->___m23_14);
		float L_71 = ((&___rhs)->___m30_3);
		(&V_0)->___m20_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_64*(float)L_65))+(float)((float)((float)L_66*(float)L_67))))+(float)((float)((float)L_68*(float)L_69))))+(float)((float)((float)L_70*(float)L_71))));
		float L_72 = ((&___lhs)->___m20_2);
		float L_73 = ((&___rhs)->___m01_4);
		float L_74 = ((&___lhs)->___m21_6);
		float L_75 = ((&___rhs)->___m11_5);
		float L_76 = ((&___lhs)->___m22_10);
		float L_77 = ((&___rhs)->___m21_6);
		float L_78 = ((&___lhs)->___m23_14);
		float L_79 = ((&___rhs)->___m31_7);
		(&V_0)->___m21_6 = ((float)((float)((float)((float)((float)((float)((float)((float)L_72*(float)L_73))+(float)((float)((float)L_74*(float)L_75))))+(float)((float)((float)L_76*(float)L_77))))+(float)((float)((float)L_78*(float)L_79))));
		float L_80 = ((&___lhs)->___m20_2);
		float L_81 = ((&___rhs)->___m02_8);
		float L_82 = ((&___lhs)->___m21_6);
		float L_83 = ((&___rhs)->___m12_9);
		float L_84 = ((&___lhs)->___m22_10);
		float L_85 = ((&___rhs)->___m22_10);
		float L_86 = ((&___lhs)->___m23_14);
		float L_87 = ((&___rhs)->___m32_11);
		(&V_0)->___m22_10 = ((float)((float)((float)((float)((float)((float)((float)((float)L_80*(float)L_81))+(float)((float)((float)L_82*(float)L_83))))+(float)((float)((float)L_84*(float)L_85))))+(float)((float)((float)L_86*(float)L_87))));
		float L_88 = ((&___lhs)->___m20_2);
		float L_89 = ((&___rhs)->___m03_12);
		float L_90 = ((&___lhs)->___m21_6);
		float L_91 = ((&___rhs)->___m13_13);
		float L_92 = ((&___lhs)->___m22_10);
		float L_93 = ((&___rhs)->___m23_14);
		float L_94 = ((&___lhs)->___m23_14);
		float L_95 = ((&___rhs)->___m33_15);
		(&V_0)->___m23_14 = ((float)((float)((float)((float)((float)((float)((float)((float)L_88*(float)L_89))+(float)((float)((float)L_90*(float)L_91))))+(float)((float)((float)L_92*(float)L_93))))+(float)((float)((float)L_94*(float)L_95))));
		float L_96 = ((&___lhs)->___m30_3);
		float L_97 = ((&___rhs)->___m00_0);
		float L_98 = ((&___lhs)->___m31_7);
		float L_99 = ((&___rhs)->___m10_1);
		float L_100 = ((&___lhs)->___m32_11);
		float L_101 = ((&___rhs)->___m20_2);
		float L_102 = ((&___lhs)->___m33_15);
		float L_103 = ((&___rhs)->___m30_3);
		(&V_0)->___m30_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))+(float)((float)((float)L_98*(float)L_99))))+(float)((float)((float)L_100*(float)L_101))))+(float)((float)((float)L_102*(float)L_103))));
		float L_104 = ((&___lhs)->___m30_3);
		float L_105 = ((&___rhs)->___m01_4);
		float L_106 = ((&___lhs)->___m31_7);
		float L_107 = ((&___rhs)->___m11_5);
		float L_108 = ((&___lhs)->___m32_11);
		float L_109 = ((&___rhs)->___m21_6);
		float L_110 = ((&___lhs)->___m33_15);
		float L_111 = ((&___rhs)->___m31_7);
		(&V_0)->___m31_7 = ((float)((float)((float)((float)((float)((float)((float)((float)L_104*(float)L_105))+(float)((float)((float)L_106*(float)L_107))))+(float)((float)((float)L_108*(float)L_109))))+(float)((float)((float)L_110*(float)L_111))));
		float L_112 = ((&___lhs)->___m30_3);
		float L_113 = ((&___rhs)->___m02_8);
		float L_114 = ((&___lhs)->___m31_7);
		float L_115 = ((&___rhs)->___m12_9);
		float L_116 = ((&___lhs)->___m32_11);
		float L_117 = ((&___rhs)->___m22_10);
		float L_118 = ((&___lhs)->___m33_15);
		float L_119 = ((&___rhs)->___m32_11);
		(&V_0)->___m32_11 = ((float)((float)((float)((float)((float)((float)((float)((float)L_112*(float)L_113))+(float)((float)((float)L_114*(float)L_115))))+(float)((float)((float)L_116*(float)L_117))))+(float)((float)((float)L_118*(float)L_119))));
		float L_120 = ((&___lhs)->___m30_3);
		float L_121 = ((&___rhs)->___m03_12);
		float L_122 = ((&___lhs)->___m31_7);
		float L_123 = ((&___rhs)->___m13_13);
		float L_124 = ((&___lhs)->___m32_11);
		float L_125 = ((&___rhs)->___m23_14);
		float L_126 = ((&___lhs)->___m33_15);
		float L_127 = ((&___rhs)->___m33_15);
		(&V_0)->___m33_15 = ((float)((float)((float)((float)((float)((float)((float)((float)L_120*(float)L_121))+(float)((float)((float)L_122*(float)L_123))))+(float)((float)((float)L_124*(float)L_125))))+(float)((float)((float)L_126*(float)L_127))));
		Matrix4x4_t242  L_128 = V_0;
		return L_128;
	}
}
// UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern "C" Vector4_t5  Matrix4x4_op_Multiply_m5418 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___lhs, Vector4_t5  ___v, const MethodInfo* method)
{
	Vector4_t5  V_0 = {0};
	{
		float L_0 = ((&___lhs)->___m00_0);
		float L_1 = ((&___v)->___x_1);
		float L_2 = ((&___lhs)->___m01_4);
		float L_3 = ((&___v)->___y_2);
		float L_4 = ((&___lhs)->___m02_8);
		float L_5 = ((&___v)->___z_3);
		float L_6 = ((&___lhs)->___m03_12);
		float L_7 = ((&___v)->___w_4);
		(&V_0)->___x_1 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		float L_8 = ((&___lhs)->___m10_1);
		float L_9 = ((&___v)->___x_1);
		float L_10 = ((&___lhs)->___m11_5);
		float L_11 = ((&___v)->___y_2);
		float L_12 = ((&___lhs)->___m12_9);
		float L_13 = ((&___v)->___z_3);
		float L_14 = ((&___lhs)->___m13_13);
		float L_15 = ((&___v)->___w_4);
		(&V_0)->___y_2 = ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))+(float)((float)((float)L_14*(float)L_15))));
		float L_16 = ((&___lhs)->___m20_2);
		float L_17 = ((&___v)->___x_1);
		float L_18 = ((&___lhs)->___m21_6);
		float L_19 = ((&___v)->___y_2);
		float L_20 = ((&___lhs)->___m22_10);
		float L_21 = ((&___v)->___z_3);
		float L_22 = ((&___lhs)->___m23_14);
		float L_23 = ((&___v)->___w_4);
		(&V_0)->___z_3 = ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = ((&___lhs)->___m30_3);
		float L_25 = ((&___v)->___x_1);
		float L_26 = ((&___lhs)->___m31_7);
		float L_27 = ((&___v)->___y_2);
		float L_28 = ((&___lhs)->___m32_11);
		float L_29 = ((&___v)->___z_3);
		float L_30 = ((&___lhs)->___m33_15);
		float L_31 = ((&___v)->___w_4);
		(&V_0)->___w_4 = ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))+(float)((float)((float)L_26*(float)L_27))))+(float)((float)((float)L_28*(float)L_29))))+(float)((float)((float)L_30*(float)L_31))));
		Vector4_t5  L_32 = V_0;
		return L_32;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Equality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Equality_m5419 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___lhs, Matrix4x4_t242  ___rhs, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		Vector4_t5  L_0 = Matrix4x4_GetColumn_m3012((&___lhs), 0, /*hidden argument*/NULL);
		Vector4_t5  L_1 = Matrix4x4_GetColumn_m3012((&___rhs), 0, /*hidden argument*/NULL);
		bool L_2 = Vector4_op_Equality_m5454(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t5  L_3 = Matrix4x4_GetColumn_m3012((&___lhs), 1, /*hidden argument*/NULL);
		Vector4_t5  L_4 = Matrix4x4_GetColumn_m3012((&___rhs), 1, /*hidden argument*/NULL);
		bool L_5 = Vector4_op_Equality_m5454(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t5  L_6 = Matrix4x4_GetColumn_m3012((&___lhs), 2, /*hidden argument*/NULL);
		Vector4_t5  L_7 = Matrix4x4_GetColumn_m3012((&___rhs), 2, /*hidden argument*/NULL);
		bool L_8 = Vector4_op_Equality_m5454(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0065;
		}
	}
	{
		Vector4_t5  L_9 = Matrix4x4_GetColumn_m3012((&___lhs), 3, /*hidden argument*/NULL);
		Vector4_t5  L_10 = Matrix4x4_GetColumn_m3012((&___rhs), 3, /*hidden argument*/NULL);
		bool L_11 = Vector4_op_Equality_m5454(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0066;
	}

IL_0065:
	{
		G_B5_0 = 0;
	}

IL_0066:
	{
		return G_B5_0;
	}
}
// System.Boolean UnityEngine.Matrix4x4::op_Inequality(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern "C" bool Matrix4x4_op_Inequality_m5420 (Object_t * __this /* static, unused */, Matrix4x4_t242  ___lhs, Matrix4x4_t242  ___rhs, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = ___lhs;
		Matrix4x4_t242  L_1 = ___rhs;
		bool L_2 = Matrix4x4_op_Equality_m5419(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"

// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"


// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds__ctor_m4868 (Bounds_t703 * __this, Vector3_t215  ___center, Vector3_t215  ___size, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___center;
		__this->___m_Center_0 = L_0;
		Vector3_t215  L_1 = ___size;
		Vector3_t215  L_2 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_2;
		return;
	}
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C" int32_t Bounds_GetHashCode_m5421 (Bounds_t703 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	Vector3_t215  V_1 = {0};
	{
		Vector3_t215  L_0 = Bounds_get_center_m4869(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m5365((&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_2 = Bounds_get_extents_m5423(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m5365((&V_1), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
	}
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern TypeInfo* Bounds_t703_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t215_il2cpp_TypeInfo_var;
extern "C" bool Bounds_Equals_m5422 (Bounds_t703 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Bounds_t703_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		Vector3_t215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t703  V_0 = {0};
	Vector3_t215  V_1 = {0};
	Vector3_t215  V_2 = {0};
	int32_t G_B5_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Bounds_t703_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Bounds_t703 *)((Bounds_t703 *)UnBox (L_1, Bounds_t703_il2cpp_TypeInfo_var))));
		Vector3_t215  L_2 = Bounds_get_center_m4869(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t215  L_3 = Bounds_get_center_m4869((&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_4 = L_3;
		Object_t * L_5 = Box(Vector3_t215_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m5366((&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004f;
		}
	}
	{
		Vector3_t215  L_7 = Bounds_get_extents_m5423(__this, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t215  L_8 = Bounds_get_extents_m5423((&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_9 = L_8;
		Object_t * L_10 = Box(Vector3_t215_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m5366((&V_2), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0050;
	}

IL_004f:
	{
		G_B5_0 = 0;
	}

IL_0050:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C" Vector3_t215  Bounds_get_center_m4869 (Bounds_t703 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Center_0);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C" void Bounds_set_center_m4871 (Bounds_t703 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		__this->___m_Center_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C" Vector3_t215  Bounds_get_size_m4860 (Bounds_t703 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Extents_1);
		Vector3_t215  L_1 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C" void Bounds_set_size_m4870 (Bounds_t703 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		Vector3_t215  L_1 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->___m_Extents_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C" Vector3_t215  Bounds_get_extents_m5423 (Bounds_t703 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Extents_1);
		return L_0;
	}
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C" void Bounds_set_extents_m5424 (Bounds_t703 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		__this->___m_Extents_1 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C" Vector3_t215  Bounds_get_min_m4864 (Bounds_t703 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Bounds_get_center_m4869(__this, /*hidden argument*/NULL);
		Vector3_t215  L_1 = Bounds_get_extents_m5423(__this, /*hidden argument*/NULL);
		Vector3_t215  L_2 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_min(UnityEngine.Vector3)
extern "C" void Bounds_set_min_m5425 (Bounds_t703 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		Vector3_t215  L_1 = Bounds_get_max_m4878(__this, /*hidden argument*/NULL);
		Bounds_SetMinMax_m5427(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C" Vector3_t215  Bounds_get_max_m4878 (Bounds_t703 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Bounds_get_center_m4869(__this, /*hidden argument*/NULL);
		Vector3_t215  L_1 = Bounds_get_extents_m5423(__this, /*hidden argument*/NULL);
		Vector3_t215  L_2 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Bounds::set_max(UnityEngine.Vector3)
extern "C" void Bounds_set_max_m5426 (Bounds_t703 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Bounds_get_min_m4864(__this, /*hidden argument*/NULL);
		Vector3_t215  L_1 = ___value;
		Bounds_SetMinMax_m5427(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Bounds_SetMinMax_m5427 (Bounds_t703 * __this, Vector3_t215  ___min, Vector3_t215  ___max, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___max;
		Vector3_t215  L_1 = ___min;
		Vector3_t215  L_2 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t215  L_3 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m5424(__this, L_3, /*hidden argument*/NULL);
		Vector3_t215  L_4 = ___min;
		Vector3_t215  L_5 = Bounds_get_extents_m5423(__this, /*hidden argument*/NULL);
		Vector3_t215  L_6 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m4871(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C" void Bounds_Encapsulate_m4877 (Bounds_t703 * __this, Vector3_t215  ___point, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Bounds_get_min_m4864(__this, /*hidden argument*/NULL);
		Vector3_t215  L_1 = ___point;
		Vector3_t215  L_2 = Vector3_Min_m4875(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t215  L_3 = Bounds_get_max_m4878(__this, /*hidden argument*/NULL);
		Vector3_t215  L_4 = ___point;
		Vector3_t215  L_5 = Vector3_Max_m4876(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m5427(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C" void Bounds_Encapsulate_m5428 (Bounds_t703 * __this, Bounds_t703  ___bounds, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Bounds_get_center_m4869((&___bounds), /*hidden argument*/NULL);
		Vector3_t215  L_1 = Bounds_get_extents_m5423((&___bounds), /*hidden argument*/NULL);
		Vector3_t215  L_2 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Bounds_Encapsulate_m4877(__this, L_2, /*hidden argument*/NULL);
		Vector3_t215  L_3 = Bounds_get_center_m4869((&___bounds), /*hidden argument*/NULL);
		Vector3_t215  L_4 = Bounds_get_extents_m5423((&___bounds), /*hidden argument*/NULL);
		Vector3_t215  L_5 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_Encapsulate_m4877(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(System.Single)
extern "C" void Bounds_Expand_m5429 (Bounds_t703 * __this, float ___amount, const MethodInfo* method)
{
	{
		float L_0 = ___amount;
		___amount = ((float)((float)L_0*(float)(0.5f)));
		Vector3_t215  L_1 = Bounds_get_extents_m5423(__this, /*hidden argument*/NULL);
		float L_2 = ___amount;
		float L_3 = ___amount;
		float L_4 = ___amount;
		Vector3_t215  L_5 = {0};
		Vector3__ctor_m2812(&L_5, L_2, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t215  L_6 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		Bounds_set_extents_m5424(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Bounds::Expand(UnityEngine.Vector3)
extern "C" void Bounds_Expand_m5430 (Bounds_t703 * __this, Vector3_t215  ___amount, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Bounds_get_extents_m5423(__this, /*hidden argument*/NULL);
		Vector3_t215  L_1 = ___amount;
		Vector3_t215  L_2 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		Vector3_t215  L_3 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		Bounds_set_extents_m5424(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Bounds::Intersects(UnityEngine.Bounds)
extern "C" bool Bounds_Intersects_m5431 (Bounds_t703 * __this, Bounds_t703  ___bounds, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	Vector3_t215  V_1 = {0};
	Vector3_t215  V_2 = {0};
	Vector3_t215  V_3 = {0};
	Vector3_t215  V_4 = {0};
	Vector3_t215  V_5 = {0};
	Vector3_t215  V_6 = {0};
	Vector3_t215  V_7 = {0};
	Vector3_t215  V_8 = {0};
	Vector3_t215  V_9 = {0};
	Vector3_t215  V_10 = {0};
	Vector3_t215  V_11 = {0};
	int32_t G_B7_0 = 0;
	{
		Vector3_t215  L_0 = Bounds_get_min_m4864(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		Vector3_t215  L_2 = Bounds_get_max_m4878((&___bounds), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___x_1);
		if ((!(((float)L_1) <= ((float)L_3))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t215  L_4 = Bounds_get_max_m4878(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___x_1);
		Vector3_t215  L_6 = Bounds_get_min_m4864((&___bounds), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___x_1);
		if ((!(((float)L_5) >= ((float)L_7))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t215  L_8 = Bounds_get_min_m4864(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___y_2);
		Vector3_t215  L_10 = Bounds_get_max_m4878((&___bounds), /*hidden argument*/NULL);
		V_5 = L_10;
		float L_11 = ((&V_5)->___y_2);
		if ((!(((float)L_9) <= ((float)L_11))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t215  L_12 = Bounds_get_max_m4878(__this, /*hidden argument*/NULL);
		V_6 = L_12;
		float L_13 = ((&V_6)->___y_2);
		Vector3_t215  L_14 = Bounds_get_min_m4864((&___bounds), /*hidden argument*/NULL);
		V_7 = L_14;
		float L_15 = ((&V_7)->___y_2);
		if ((!(((float)L_13) >= ((float)L_15))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t215  L_16 = Bounds_get_min_m4864(__this, /*hidden argument*/NULL);
		V_8 = L_16;
		float L_17 = ((&V_8)->___z_3);
		Vector3_t215  L_18 = Bounds_get_max_m4878((&___bounds), /*hidden argument*/NULL);
		V_9 = L_18;
		float L_19 = ((&V_9)->___z_3);
		if ((!(((float)L_17) <= ((float)L_19))))
		{
			goto IL_00d6;
		}
	}
	{
		Vector3_t215  L_20 = Bounds_get_max_m4878(__this, /*hidden argument*/NULL);
		V_10 = L_20;
		float L_21 = ((&V_10)->___z_3);
		Vector3_t215  L_22 = Bounds_get_min_m4864((&___bounds), /*hidden argument*/NULL);
		V_11 = L_22;
		float L_23 = ((&V_11)->___z_3);
		G_B7_0 = ((((int32_t)((!(((float)L_21) >= ((float)L_23)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_00d7;
	}

IL_00d6:
	{
		G_B7_0 = 0;
	}

IL_00d7:
	{
		return G_B7_0;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_Contains(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" bool Bounds_Internal_Contains_m5432 (Object_t * __this /* static, unused */, Bounds_t703  ___m, Vector3_t215  ___point, const MethodInfo* method)
{
	{
		bool L_0 = Bounds_INTERNAL_CALL_Internal_Contains_m5433(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_Contains_m5433 (Object_t * __this /* static, unused */, Bounds_t703 * ___m, Vector3_t215 * ___point, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_Contains_m5433_ftn) (Bounds_t703 *, Vector3_t215 *);
	static Bounds_INTERNAL_CALL_Internal_Contains_m5433_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_Contains_m5433_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
extern "C" bool Bounds_Contains_m5434 (Bounds_t703 * __this, Vector3_t215  ___point, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___point;
		bool L_1 = Bounds_Internal_Contains_m5432(NULL /*static, unused*/, (*(Bounds_t703 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityEngine.Bounds::Internal_SqrDistance(UnityEngine.Bounds,UnityEngine.Vector3)
extern "C" float Bounds_Internal_SqrDistance_m5435 (Object_t * __this /* static, unused */, Bounds_t703  ___m, Vector3_t215  ___point, const MethodInfo* method)
{
	{
		float L_0 = Bounds_INTERNAL_CALL_Internal_SqrDistance_m5436(NULL /*static, unused*/, (&___m), (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" float Bounds_INTERNAL_CALL_Internal_SqrDistance_m5436 (Object_t * __this /* static, unused */, Bounds_t703 * ___m, Vector3_t215 * ___point, const MethodInfo* method)
{
	typedef float (*Bounds_INTERNAL_CALL_Internal_SqrDistance_m5436_ftn) (Bounds_t703 *, Vector3_t215 *);
	static Bounds_INTERNAL_CALL_Internal_SqrDistance_m5436_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_SqrDistance_m5436_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___m, ___point);
}
// System.Single UnityEngine.Bounds::SqrDistance(UnityEngine.Vector3)
extern "C" float Bounds_SqrDistance_m5437 (Bounds_t703 * __this, Vector3_t215  ___point, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___point;
		float L_1 = Bounds_Internal_SqrDistance_m5435(NULL /*static, unused*/, (*(Bounds_t703 *)__this), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Bounds::Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_Internal_IntersectRay_m5438 (Object_t * __this /* static, unused */, Ray_t465 * ___ray, Bounds_t703 * ___bounds, float* ___distance, const MethodInfo* method)
{
	{
		Ray_t465 * L_0 = ___ray;
		Bounds_t703 * L_1 = ___bounds;
		float* L_2 = ___distance;
		bool L_3 = Bounds_INTERNAL_CALL_Internal_IntersectRay_m5439(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
extern "C" bool Bounds_INTERNAL_CALL_Internal_IntersectRay_m5439 (Object_t * __this /* static, unused */, Ray_t465 * ___ray, Bounds_t703 * ___bounds, float* ___distance, const MethodInfo* method)
{
	typedef bool (*Bounds_INTERNAL_CALL_Internal_IntersectRay_m5439_ftn) (Ray_t465 *, Bounds_t703 *, float*);
	static Bounds_INTERNAL_CALL_Internal_IntersectRay_m5439_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_IntersectRay_m5439_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)");
	return _il2cpp_icall_func(___ray, ___bounds, ___distance);
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray)
extern "C" bool Bounds_IntersectRay_m5440 (Bounds_t703 * __this, Ray_t465  ___ray, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		bool L_0 = Bounds_Internal_IntersectRay_m5438(NULL /*static, unused*/, (&___ray), __this, (&V_0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Bounds::IntersectRay(UnityEngine.Ray,System.Single&)
extern "C" bool Bounds_IntersectRay_m5441 (Bounds_t703 * __this, Ray_t465  ___ray, float* ___distance, const MethodInfo* method)
{
	{
		float* L_0 = ___distance;
		bool L_1 = Bounds_Internal_IntersectRay_m5438(NULL /*static, unused*/, (&___ray), __this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" Vector3_t215  Bounds_Internal_GetClosestPoint_m5442 (Object_t * __this /* static, unused */, Bounds_t703 * ___bounds, Vector3_t215 * ___point, const MethodInfo* method)
{
	{
		Bounds_t703 * L_0 = ___bounds;
		Vector3_t215 * L_1 = ___point;
		Vector3_t215  L_2 = Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m5443(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern "C" Vector3_t215  Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m5443 (Object_t * __this /* static, unused */, Bounds_t703 * ___bounds, Vector3_t215 * ___point, const MethodInfo* method)
{
	typedef Vector3_t215  (*Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m5443_ftn) (Bounds_t703 *, Vector3_t215 *);
	static Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m5443_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m5443_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___bounds, ___point);
}
// UnityEngine.Vector3 UnityEngine.Bounds::ClosestPoint(UnityEngine.Vector3)
extern "C" Vector3_t215  Bounds_ClosestPoint_m5444 (Bounds_t703 * __this, Vector3_t215  ___point, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Bounds_Internal_GetClosestPoint_m5442(NULL /*static, unused*/, __this, (&___point), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.Bounds::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t215_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral947;
extern "C" String_t* Bounds_ToString_m5445 (Bounds_t703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Vector3_t215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		_stringLiteral947 = il2cpp_codegen_string_literal_from_index(947);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 2));
		Vector3_t215  L_1 = (__this->___m_Center_0);
		Vector3_t215  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t215_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		Vector3_t215  L_5 = (__this->___m_Extents_1);
		Vector3_t215  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t215_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral947, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.String UnityEngine.Bounds::ToString(System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral947;
extern "C" String_t* Bounds_ToString_m5446 (Bounds_t703 * __this, String_t* ___format, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral947 = il2cpp_codegen_string_literal_from_index(947);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 2));
		Vector3_t215 * L_1 = &(__this->___m_Center_0);
		String_t* L_2 = ___format;
		String_t* L_3 = Vector3_ToString_m5369(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		Vector3_t215 * L_5 = &(__this->___m_Extents_1);
		String_t* L_6 = ___format;
		String_t* L_7 = Vector3_ToString_m5369(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral947, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Equality_m5447 (Object_t * __this /* static, unused */, Bounds_t703  ___lhs, Bounds_t703  ___rhs, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Vector3_t215  L_0 = Bounds_get_center_m4869((&___lhs), /*hidden argument*/NULL);
		Vector3_t215  L_1 = Bounds_get_center_m4869((&___rhs), /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m3326(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		Vector3_t215  L_3 = Bounds_get_extents_m5423((&___lhs), /*hidden argument*/NULL);
		Vector3_t215  L_4 = Bounds_get_extents_m5423((&___rhs), /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m3326(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C" bool Bounds_op_Inequality_m4862 (Object_t * __this /* static, unused */, Bounds_t703  ___lhs, Bounds_t703  ___rhs, const MethodInfo* method)
{
	{
		Bounds_t703  L_0 = ___lhs;
		Bounds_t703  L_1 = ___rhs;
		bool L_2 = Bounds_op_Equality_m5447(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" void Vector4__ctor_m2728 (Vector4_t5 * __this, float ___x, float ___y, float ___z, float ___w, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		float L_2 = ___z;
		__this->___z_3 = L_2;
		float L_3 = ___w;
		__this->___w_4 = L_3;
		return;
	}
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single)
extern "C" void Vector4__ctor_m2934 (Vector4_t5 * __this, float ___x, float ___y, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		__this->___x_1 = L_0;
		float L_1 = ___y;
		__this->___y_2 = L_1;
		__this->___z_3 = (0.0f);
		__this->___w_4 = (0.0f);
		return;
	}
}
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral948;
extern "C" float Vector4_get_Item_m4745 (Vector4_t5 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral948 = il2cpp_codegen_string_literal_from_index(948);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0024;
		}
		if (L_1 == 2)
		{
			goto IL_002b;
		}
		if (L_1 == 3)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_0039;
	}

IL_001d:
	{
		float L_2 = (__this->___x_1);
		return L_2;
	}

IL_0024:
	{
		float L_3 = (__this->___y_2);
		return L_3;
	}

IL_002b:
	{
		float L_4 = (__this->___z_3);
		return L_4;
	}

IL_0032:
	{
		float L_5 = (__this->___w_4);
		return L_5;
	}

IL_0039:
	{
		IndexOutOfRangeException_t1116 * L_6 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_6, _stringLiteral948, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}
}
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral948;
extern "C" void Vector4_set_Item_m4747 (Vector4_t5 * __this, int32_t ___index, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral948 = il2cpp_codegen_string_literal_from_index(948);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_0029;
		}
		if (L_1 == 2)
		{
			goto IL_0035;
		}
		if (L_1 == 3)
		{
			goto IL_0041;
		}
	}
	{
		goto IL_004d;
	}

IL_001d:
	{
		float L_2 = ___value;
		__this->___x_1 = L_2;
		goto IL_0058;
	}

IL_0029:
	{
		float L_3 = ___value;
		__this->___y_2 = L_3;
		goto IL_0058;
	}

IL_0035:
	{
		float L_4 = ___value;
		__this->___z_3 = L_4;
		goto IL_0058;
	}

IL_0041:
	{
		float L_5 = ___value;
		__this->___w_4 = L_5;
		goto IL_0058;
	}

IL_004d:
	{
		IndexOutOfRangeException_t1116 * L_6 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_6, _stringLiteral948, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_0058:
	{
		return;
	}
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C" int32_t Vector4_GetHashCode_m5448 (Vector4_t5 * __this, const MethodInfo* method)
{
	{
		float* L_0 = &(__this->___x_1);
		int32_t L_1 = Single_GetHashCode_m6357(L_0, /*hidden argument*/NULL);
		float* L_2 = &(__this->___y_2);
		int32_t L_3 = Single_GetHashCode_m6357(L_2, /*hidden argument*/NULL);
		float* L_4 = &(__this->___z_3);
		int32_t L_5 = Single_GetHashCode_m6357(L_4, /*hidden argument*/NULL);
		float* L_6 = &(__this->___w_4);
		int32_t L_7 = Single_GetHashCode_m6357(L_6, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
	}
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern TypeInfo* Vector4_t5_il2cpp_TypeInfo_var;
extern "C" bool Vector4_Equals_m5449 (Vector4_t5 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector4_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(629);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t5  V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, Vector4_t5_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(Vector4_t5 *)((Vector4_t5 *)UnBox (L_1, Vector4_t5_il2cpp_TypeInfo_var))));
		float* L_2 = &(__this->___x_1);
		float L_3 = ((&V_0)->___x_1);
		bool L_4 = Single_Equals_m6375(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_006d;
		}
	}
	{
		float* L_5 = &(__this->___y_2);
		float L_6 = ((&V_0)->___y_2);
		bool L_7 = Single_Equals_m6375(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		float* L_8 = &(__this->___z_3);
		float L_9 = ((&V_0)->___z_3);
		bool L_10 = Single_Equals_m6375(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006d;
		}
	}
	{
		float* L_11 = &(__this->___w_4);
		float L_12 = ((&V_0)->___w_4);
		bool L_13 = Single_Equals_m6375(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_006e;
	}

IL_006d:
	{
		G_B7_0 = 0;
	}

IL_006e:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.Vector4::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral942;
extern "C" String_t* Vector4_ToString_m5450 (Vector4_t5 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral942 = il2cpp_codegen_string_literal_from_index(942);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		float L_1 = (__this->___x_1);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		float L_5 = (__this->___y_2);
		float L_6 = L_5;
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		float L_9 = (__this->___z_3);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t531_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		float L_13 = (__this->___w_4);
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral942, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" float Vector4_Dot_m5451 (Object_t * __this /* static, unused */, Vector4_t5  ___a, Vector4_t5  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
	}
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C" float Vector4_SqrMagnitude_m5452 (Object_t * __this /* static, unused */, Vector4_t5  ___a, const MethodInfo* method)
{
	{
		Vector4_t5  L_0 = ___a;
		Vector4_t5  L_1 = ___a;
		float L_2 = Vector4_Dot_m5451(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C" float Vector4_get_sqrMagnitude_m4732 (Vector4_t5 * __this, const MethodInfo* method)
{
	{
		float L_0 = Vector4_Dot_m5451(NULL /*static, unused*/, (*(Vector4_t5 *)__this), (*(Vector4_t5 *)__this), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C" Vector4_t5  Vector4_get_zero_m4735 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector4_t5  L_0 = {0};
		Vector4__ctor_m2728(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" Vector4_t5  Vector4_op_Subtraction_m5453 (Object_t * __this /* static, unused */, Vector4_t5  ___a, Vector4_t5  ___b, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ((&___b)->___x_1);
		float L_2 = ((&___a)->___y_2);
		float L_3 = ((&___b)->___y_2);
		float L_4 = ((&___a)->___z_3);
		float L_5 = ((&___b)->___z_3);
		float L_6 = ((&___a)->___w_4);
		float L_7 = ((&___b)->___w_4);
		Vector4_t5  L_8 = {0};
		Vector4__ctor_m2728(&L_8, ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C" Vector4_t5  Vector4_op_Division_m2867 (Object_t * __this /* static, unused */, Vector4_t5  ___a, float ___d, const MethodInfo* method)
{
	{
		float L_0 = ((&___a)->___x_1);
		float L_1 = ___d;
		float L_2 = ((&___a)->___y_2);
		float L_3 = ___d;
		float L_4 = ((&___a)->___z_3);
		float L_5 = ___d;
		float L_6 = ((&___a)->___w_4);
		float L_7 = ___d;
		Vector4_t5  L_8 = {0};
		Vector4__ctor_m2728(&L_8, ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C" bool Vector4_op_Equality_m5454 (Object_t * __this /* static, unused */, Vector4_t5  ___lhs, Vector4_t5  ___rhs, const MethodInfo* method)
{
	{
		Vector4_t5  L_0 = ___lhs;
		Vector4_t5  L_1 = ___rhs;
		Vector4_t5  L_2 = Vector4_op_Subtraction_m5453(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m5452(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return ((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
	}
}
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern "C" Vector3_t215  Vector4_op_Implicit_m3013 (Object_t * __this /* static, unused */, Vector4_t5  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		float L_2 = ((&___v)->___z_3);
		Vector3_t215  L_3 = {0};
		Vector3__ctor_m2812(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
extern "C" Vector4_t5  Vector4_op_Implicit_m2739 (Object_t * __this /* static, unused */, Vector2_t7  ___v, const MethodInfo* method)
{
	{
		float L_0 = ((&___v)->___x_1);
		float L_1 = ((&___v)->___y_2);
		Vector4_t5  L_2 = {0};
		Vector4__ctor_m2728(&L_2, L_0, L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"



// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Ray__ctor_m2896 (Ray_t465 * __this, Vector3_t215  ___origin, Vector3_t215  ___direction, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___origin;
		__this->___m_Origin_0 = L_0;
		Vector3_t215  L_1 = Vector3_get_normalized_m3222((&___direction), /*hidden argument*/NULL);
		__this->___m_Direction_1 = L_1;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C" Vector3_t215  Ray_get_origin_m4640 (Ray_t465 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Origin_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C" Vector3_t215  Ray_get_direction_m3242 (Ray_t465 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Direction_1);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C" Vector3_t215  Ray_GetPoint_m4793 (Ray_t465 * __this, float ___distance, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Origin_0);
		Vector3_t215  L_1 = (__this->___m_Direction_1);
		float L_2 = ___distance;
		Vector3_t215  L_3 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t215  L_4 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.Ray::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t215_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral949;
extern "C" String_t* Ray_ToString_m5455 (Ray_t465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Vector3_t215_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(191);
		_stringLiteral949 = il2cpp_codegen_string_literal_from_index(949);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 2));
		Vector3_t215  L_1 = (__this->___m_Origin_0);
		Vector3_t215  L_2 = L_1;
		Object_t * L_3 = Box(Vector3_t215_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_0;
		Vector3_t215  L_5 = (__this->___m_Direction_1);
		Vector3_t215  L_6 = L_5;
		Object_t * L_7 = Box(Vector3_t215_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 1)) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral949, L_4, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"



// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Plane__ctor_m4791 (Plane_t808 * __this, Vector3_t215  ___inNormal, Vector3_t215  ___inPoint, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___inNormal;
		Vector3_t215  L_1 = Vector3_Normalize_m5367(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___m_Normal_0 = L_1;
		Vector3_t215  L_2 = ___inNormal;
		Vector3_t215  L_3 = ___inPoint;
		float L_4 = Vector3_Dot_m3153(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->___m_Distance_1 = ((-L_4));
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C" Vector3_t215  Plane_get_normal_m5456 (Plane_t808 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Normal_0);
		return L_0;
	}
}
// System.Single UnityEngine.Plane::get_distance()
extern "C" float Plane_get_distance_m5457 (Plane_t808 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_1);
		return L_0;
	}
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" bool Plane_Raycast_m4792 (Plane_t808 * __this, Ray_t465  ___ray, float* ___enter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_t215  L_0 = Ray_get_direction_m3242((&___ray), /*hidden argument*/NULL);
		Vector3_t215  L_1 = Plane_get_normal_m5456(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m3153(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t215  L_3 = Ray_get_origin_m4640((&___ray), /*hidden argument*/NULL);
		Vector3_t215  L_4 = Plane_get_normal_m5456(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m3153(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m5457(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m2829(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0047;
		}
	}
	{
		float* L_9 = ___enter;
		*((float*)(L_9)) = (float)(0.0f);
		return 0;
	}

IL_0047:
	{
		float* L_10 = ___enter;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter;
		return ((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
	}
}
// UnityEngineInternal.MathfInternal
#include "UnityEngine_UnityEngineInternal_MathfInternal.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngineInternal.MathfInternal
#include "UnityEngine_UnityEngineInternal_MathfInternalMethodDeclarations.h"



// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern TypeInfo* MathfInternal_t896_il2cpp_TypeInfo_var;
extern "C" void MathfInternal__cctor_m5458 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MathfInternal_t896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(660);
		s_Il2CppMethodIntialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t896_StaticFields*)MathfInternal_t896_il2cpp_TypeInfo_var->static_fields)->___FloatMinNormal_0 = (1.17549435E-38f);
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t896_StaticFields*)MathfInternal_t896_il2cpp_TypeInfo_var->static_fields)->___FloatMinDenormal_1 = (1.401298E-45f);
		float L_0 = ((MathfInternal_t896_StaticFields*)MathfInternal_t896_il2cpp_TypeInfo_var->static_fields)->___FloatMinDenormal_1;
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t896_StaticFields*)MathfInternal_t896_il2cpp_TypeInfo_var->static_fields)->___IsFlushToZeroEnabled_2 = ((((float)L_0) == ((float)(0.0f)))? 1 : 0);
		return;
	}
}
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_Mathf.h"
#ifndef _MSC_VER
#else
#endif

// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"


// System.Void UnityEngine.Mathf::.cctor()
extern TypeInfo* MathfInternal_t896_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void Mathf__cctor_m5459 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MathfInternal_t896_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(660);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t896_il2cpp_TypeInfo_var);
		bool L_0 = ((MathfInternal_t896_StaticFields*)MathfInternal_t896_il2cpp_TypeInfo_var->static_fields)->___IsFlushToZeroEnabled_2;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t896_il2cpp_TypeInfo_var);
		float L_1 = ((MathfInternal_t896_StaticFields*)MathfInternal_t896_il2cpp_TypeInfo_var->static_fields)->___FloatMinNormal_0;
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MathfInternal_t896_il2cpp_TypeInfo_var);
		float L_2 = ((MathfInternal_t896_StaticFields*)MathfInternal_t896_il2cpp_TypeInfo_var->static_fields)->___FloatMinDenormal_1;
		il2cpp_codegen_memory_barrier();
		G_B3_0 = L_2;
	}

IL_001d:
	{
		((Mathf_t474_StaticFields*)Mathf_t474_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0 = G_B3_0;
		return;
	}
}
// System.Single UnityEngine.Mathf::Sin(System.Single)
extern "C" float Mathf_Sin_m5460 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = sin((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Cos(System.Single)
extern "C" float Mathf_Cos_m5461 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = cos((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Tan(System.Single)
extern "C" float Mathf_Tan_m5462 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = tan((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Asin(System.Single)
extern "C" float Mathf_Asin_m5463 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = asin((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Atan(System.Single)
extern "C" float Mathf_Atan_m5464 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = atan((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Atan2(System.Single,System.Single)
extern "C" float Mathf_Atan2_m5465 (Object_t * __this /* static, unused */, float ___y, float ___x, const MethodInfo* method)
{
	{
		float L_0 = ___y;
		float L_1 = ___x;
		double L_2 = atan2((((double)L_0)), (((double)L_1)));
		return (((float)L_2));
	}
}
// System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern "C" float Mathf_Sqrt_m5466 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = sqrt((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Abs(System.Single)
extern "C" float Mathf_Abs_m5467 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = fabsf(L_0);
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C" float Mathf_Min_m3328 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C" int32_t Mathf_Min_m3050 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C" float Mathf_Max_m2937 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___a;
		float L_1 = ___b;
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		float L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		float L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C" int32_t Mathf_Max_m3049 (Object_t * __this /* static, unused */, int32_t ___a, int32_t ___b, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___a;
		int32_t L_1 = ___b;
		if ((((int32_t)L_0) <= ((int32_t)L_1)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_2 = ___a;
		G_B3_0 = L_2;
		goto IL_000e;
	}

IL_000d:
	{
		int32_t L_3 = ___b;
		G_B3_0 = L_3;
	}

IL_000e:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern "C" float Mathf_Pow_m5468 (Object_t * __this /* static, unused */, float ___f, float ___p, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = ___p;
		double L_2 = pow((((double)L_0)), (((double)L_1)));
		return (((float)L_2));
	}
}
// System.Single UnityEngine.Mathf::Exp(System.Single)
extern "C" float Mathf_Exp_m5469 (Object_t * __this /* static, unused */, float ___power, const MethodInfo* method)
{
	{
		float L_0 = ___power;
		double L_1 = exp((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C" float Mathf_Log_m4924 (Object_t * __this /* static, unused */, float ___f, float ___p, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		float L_1 = ___p;
		double L_2 = Math_Log_m6376(NULL /*static, unused*/, (((double)L_0)), (((double)L_1)), /*hidden argument*/NULL);
		return (((float)L_2));
	}
}
// System.Single UnityEngine.Mathf::Floor(System.Single)
extern "C" float Mathf_Floor_m5470 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Single UnityEngine.Mathf::Round(System.Single)
extern "C" float Mathf_Round_m5471 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = round((((double)L_0)));
		return (((float)L_1));
	}
}
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C" int32_t Mathf_CeilToInt_m4933 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = ceil((((double)L_0)));
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C" int32_t Mathf_FloorToInt_m3290 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = floor((((double)L_0)));
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C" int32_t Mathf_RoundToInt_m2757 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	{
		float L_0 = ___f;
		double L_1 = round((((double)L_0)));
		return (((int32_t)L_1));
	}
}
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C" float Mathf_Sign_m4867 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___f;
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_001a;
	}

IL_0015:
	{
		G_B3_0 = (-1.0f);
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C" float Mathf_Clamp_m2768 (Object_t * __this /* static, unused */, float ___value, float ___min, float ___max, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = ___min;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		float L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		float L_3 = ___value;
		float L_4 = ___max;
		if ((!(((float)L_3) > ((float)L_4))))
		{
			goto IL_0019;
		}
	}
	{
		float L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		float L_6 = ___value;
		return L_6;
	}
}
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C" int32_t Mathf_Clamp_m4669 (Object_t * __this /* static, unused */, int32_t ___value, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		int32_t L_1 = ___min;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = ___min;
		___value = L_2;
		goto IL_0019;
	}

IL_000f:
	{
		int32_t L_3 = ___value;
		int32_t L_4 = ___max;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_5 = ___max;
		___value = L_5;
	}

IL_0019:
	{
		int32_t L_6 = ___value;
		return L_6;
	}
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C" float Mathf_Clamp01_m2828 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		return (0.0f);
	}

IL_0011:
	{
		float L_1 = ___value;
		if ((!(((float)L_1) > ((float)(1.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		return (1.0f);
	}

IL_0022:
	{
		float L_2 = ___value;
		return L_2;
	}
}
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_Lerp_m2756 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___from;
		float L_1 = ___to;
		float L_2 = ___from;
		float L_3 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::MoveTowards(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_MoveTowards_m3258 (Object_t * __this /* static, unused */, float ___current, float ___target, float ___maxDelta, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___target;
		float L_1 = ___current;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___maxDelta;
		if ((!(((float)L_2) <= ((float)L_3))))
		{
			goto IL_0010;
		}
	}
	{
		float L_4 = ___target;
		return L_4;
	}

IL_0010:
	{
		float L_5 = ___current;
		float L_6 = ___target;
		float L_7 = ___current;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_8 = Mathf_Sign_m4867(NULL /*static, unused*/, ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		float L_9 = ___maxDelta;
		return ((float)((float)L_5+(float)((float)((float)L_8*(float)L_9))));
	}
}
// System.Single UnityEngine.Mathf::SmoothStep(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothStep_m2893 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t = L_1;
		float L_2 = ___t;
		float L_3 = ___t;
		float L_4 = ___t;
		float L_5 = ___t;
		float L_6 = ___t;
		___t = ((float)((float)((float)((float)((float)((float)((float)((float)(-2.0f)*(float)L_2))*(float)L_3))*(float)L_4))+(float)((float)((float)((float)((float)(3.0f)*(float)L_5))*(float)L_6))));
		float L_7 = ___to;
		float L_8 = ___t;
		float L_9 = ___from;
		float L_10 = ___t;
		return ((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)L_9*(float)((float)((float)(1.0f)-(float)L_10))))));
	}
}
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" bool Mathf_Approximately_m2829 (Object_t * __this /* static, unused */, float ___a, float ___b, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___b;
		float L_1 = ___a;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0-(float)L_1)));
		float L_3 = ___a;
		float L_4 = fabsf(L_3);
		float L_5 = ___b;
		float L_6 = fabsf(L_5);
		float L_7 = Mathf_Max_m2937(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ((Mathf_t474_StaticFields*)Mathf_t474_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		float L_9 = Mathf_Max_m2937(NULL /*static, unused*/, ((float)((float)(1.0E-06f)*(float)L_7)), ((float)((float)L_8*(float)(8.0f))), /*hidden argument*/NULL);
		return ((((float)L_2) < ((float)L_9))? 1 : 0);
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDamp_m3336 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current;
		float L_2 = ___target;
		float* L_3 = ___currentVelocity;
		float L_4 = ___smoothTime;
		float L_5 = V_1;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDamp_m4861(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDamp_m4861 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	{
		float L_0 = ___smoothTime;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Max_m2937(NULL /*static, unused*/, (0.0001f), L_0, /*hidden argument*/NULL);
		___smoothTime = L_1;
		float L_2 = ___smoothTime;
		V_0 = ((float)((float)(2.0f)/(float)L_2));
		float L_3 = V_0;
		float L_4 = ___deltaTime;
		V_1 = ((float)((float)L_3*(float)L_4));
		float L_5 = V_1;
		float L_6 = V_1;
		float L_7 = V_1;
		float L_8 = V_1;
		float L_9 = V_1;
		float L_10 = V_1;
		V_2 = ((float)((float)(1.0f)/(float)((float)((float)((float)((float)((float)((float)(1.0f)+(float)L_5))+(float)((float)((float)((float)((float)(0.48f)*(float)L_6))*(float)L_7))))+(float)((float)((float)((float)((float)((float)((float)(0.235f)*(float)L_8))*(float)L_9))*(float)L_10))))));
		float L_11 = ___current;
		float L_12 = ___target;
		V_3 = ((float)((float)L_11-(float)L_12));
		float L_13 = ___target;
		V_4 = L_13;
		float L_14 = ___maxSpeed;
		float L_15 = ___smoothTime;
		V_5 = ((float)((float)L_14*(float)L_15));
		float L_16 = V_3;
		float L_17 = V_5;
		float L_18 = V_5;
		float L_19 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_16, ((-L_17)), L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		float L_20 = ___current;
		float L_21 = V_3;
		___target = ((float)((float)L_20-(float)L_21));
		float* L_22 = ___currentVelocity;
		float L_23 = V_0;
		float L_24 = V_3;
		float L_25 = ___deltaTime;
		V_6 = ((float)((float)((float)((float)(*((float*)L_22))+(float)((float)((float)L_23*(float)L_24))))*(float)L_25));
		float* L_26 = ___currentVelocity;
		float* L_27 = ___currentVelocity;
		float L_28 = V_0;
		float L_29 = V_6;
		float L_30 = V_2;
		*((float*)(L_26)) = (float)((float)((float)((float)((float)(*((float*)L_27))-(float)((float)((float)L_28*(float)L_29))))*(float)L_30));
		float L_31 = ___target;
		float L_32 = V_3;
		float L_33 = V_6;
		float L_34 = V_2;
		V_7 = ((float)((float)L_31+(float)((float)((float)((float)((float)L_32+(float)L_33))*(float)L_34))));
		float L_35 = V_4;
		float L_36 = ___current;
		float L_37 = V_7;
		float L_38 = V_4;
		if ((!(((uint32_t)((((float)((float)((float)L_35-(float)L_36))) > ((float)(0.0f)))? 1 : 0)) == ((uint32_t)((((float)L_37) > ((float)L_38))? 1 : 0)))))
		{
			goto IL_00a0;
		}
	}
	{
		float L_39 = V_4;
		V_7 = L_39;
		float* L_40 = ___currentVelocity;
		float L_41 = V_7;
		float L_42 = V_4;
		float L_43 = ___deltaTime;
		*((float*)(L_40)) = (float)((float)((float)((float)((float)L_41-(float)L_42))/(float)L_43));
	}

IL_00a0:
	{
		float L_44 = V_7;
		return L_44;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDampAngle_m3337 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = (std::numeric_limits<float>::infinity());
		float L_1 = ___current;
		float L_2 = ___target;
		float* L_3 = ___currentVelocity;
		float L_4 = ___smoothTime;
		float L_5 = V_1;
		float L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDampAngle_m5472(NULL /*static, unused*/, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_SmoothDampAngle_m5472 (Object_t * __this /* static, unused */, float ___current, float ___target, float* ___currentVelocity, float ___smoothTime, float ___maxSpeed, float ___deltaTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___current;
		float L_1 = ___current;
		float L_2 = ___target;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_3 = Mathf_DeltaAngle_m5473(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		___target = ((float)((float)L_0+(float)L_3));
		float L_4 = ___current;
		float L_5 = ___target;
		float* L_6 = ___currentVelocity;
		float L_7 = ___smoothTime;
		float L_8 = ___maxSpeed;
		float L_9 = ___deltaTime;
		float L_10 = Mathf_SmoothDamp_m4861(NULL /*static, unused*/, L_4, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_Repeat_m3277 (Object_t * __this /* static, unused */, float ___t, float ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		float L_1 = ___t;
		float L_2 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_3 = floorf(((float)((float)L_1/(float)L_2)));
		float L_4 = ___length;
		return ((float)((float)L_0-(float)((float)((float)L_3*(float)L_4))));
	}
}
// System.Single UnityEngine.Mathf::PingPong(System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_PingPong_m2928 (Object_t * __this /* static, unused */, float ___t, float ___length, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___t;
		float L_1 = ___length;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Repeat_m3277(NULL /*static, unused*/, L_0, ((float)((float)L_1*(float)(2.0f))), /*hidden argument*/NULL);
		___t = L_2;
		float L_3 = ___length;
		float L_4 = ___t;
		float L_5 = ___length;
		float L_6 = fabsf(((float)((float)L_4-(float)L_5)));
		return ((float)((float)L_3-(float)L_6));
	}
}
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C" float Mathf_InverseLerp_m4755 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___from;
		float L_1 = ___to;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_002f;
		}
	}
	{
		float L_2 = ___value;
		float L_3 = ___from;
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0014;
		}
	}
	{
		return (0.0f);
	}

IL_0014:
	{
		float L_4 = ___value;
		float L_5 = ___to;
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0021;
		}
	}
	{
		return (1.0f);
	}

IL_0021:
	{
		float L_6 = ___value;
		float L_7 = ___from;
		___value = ((float)((float)L_6-(float)L_7));
		float L_8 = ___value;
		float L_9 = ___to;
		float L_10 = ___from;
		___value = ((float)((float)L_8/(float)((float)((float)L_9-(float)L_10))));
		float L_11 = ___value;
		return L_11;
	}

IL_002f:
	{
		float L_12 = ___from;
		float L_13 = ___to;
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_005e;
		}
	}
	{
		float L_14 = ___value;
		float L_15 = ___to;
		if ((!(((float)L_14) < ((float)L_15))))
		{
			goto IL_0043;
		}
	}
	{
		return (1.0f);
	}

IL_0043:
	{
		float L_16 = ___value;
		float L_17 = ___from;
		if ((!(((float)L_16) > ((float)L_17))))
		{
			goto IL_0050;
		}
	}
	{
		return (0.0f);
	}

IL_0050:
	{
		float L_18 = ___value;
		float L_19 = ___to;
		float L_20 = ___from;
		float L_21 = ___to;
		return ((float)((float)(1.0f)-(float)((float)((float)((float)((float)L_18-(float)L_19))/(float)((float)((float)L_20-(float)L_21))))));
	}

IL_005e:
	{
		return (0.0f);
	}
}
// System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Mathf_DeltaAngle_m5473 (Object_t * __this /* static, unused */, float ___current, float ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = ___target;
		float L_1 = ___current;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Repeat_m3277(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), (360.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		if ((!(((float)L_3) > ((float)(180.0f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((float)((float)L_4-(float)(360.0f)));
	}

IL_0021:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformPropertiesMethodDeclarations.h"



// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.DrivenRectTransformTracker
#include "UnityEngine_UnityEngine_DrivenRectTransformTrackerMethodDeclarations.h"

// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"


// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m4852 (DrivenRectTransformTracker_t698 * __this, Object_t473 * ___driver, RectTransform_t648 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m4850 (DrivenRectTransformTracker_t698 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_EdgeMethodDeclarations.h"



// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_AxisMethodDeclarations.h"



// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertie.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RectTransform/ReapplyDrivenProperties
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrivenPropertieMethodDeclarations.h"



// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C" void ReapplyDrivenProperties__ctor_m4949 (ReapplyDrivenProperties_t822 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C" void ReapplyDrivenProperties_Invoke_m5474 (ReapplyDrivenProperties_t822 * __this, RectTransform_t648 * ___driven, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ReapplyDrivenProperties_Invoke_m5474((ReapplyDrivenProperties_t822 *)__this->___prev_9,___driven, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, RectTransform_t648 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, RectTransform_t648 * ___driven, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___driven,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t822(Il2CppObject* delegate, RectTransform_t648 * ___driven)
{
	// Marshaling of parameter '___driven' to native representation
	RectTransform_t648 * ____driven_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.RectTransform'."));
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C" Object_t * ReapplyDrivenProperties_BeginInvoke_m5475 (ReapplyDrivenProperties_t822 * __this, RectTransform_t648 * ___driven, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C" void ReapplyDrivenProperties_EndInvoke_m5476 (ReapplyDrivenProperties_t822 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"

// System.Delegate
#include "mscorlib_System_Delegate.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"


// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern TypeInfo* RectTransform_t648_il2cpp_TypeInfo_var;
extern TypeInfo* ReapplyDrivenProperties_t822_il2cpp_TypeInfo_var;
extern "C" void RectTransform_add_reapplyDrivenProperties_m4950 (Object_t * __this /* static, unused */, ReapplyDrivenProperties_t822 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ReapplyDrivenProperties_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t822 * L_0 = ((RectTransform_t648_StaticFields*)RectTransform_t648_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		ReapplyDrivenProperties_t822 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t648_StaticFields*)RectTransform_t648_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2 = ((ReapplyDrivenProperties_t822 *)Castclass(L_2, ReapplyDrivenProperties_t822_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern TypeInfo* RectTransform_t648_il2cpp_TypeInfo_var;
extern TypeInfo* ReapplyDrivenProperties_t822_il2cpp_TypeInfo_var;
extern "C" void RectTransform_remove_reapplyDrivenProperties_m5477 (Object_t * __this /* static, unused */, ReapplyDrivenProperties_t822 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ReapplyDrivenProperties_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(587);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t822 * L_0 = ((RectTransform_t648_StaticFields*)RectTransform_t648_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		ReapplyDrivenProperties_t822 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((RectTransform_t648_StaticFields*)RectTransform_t648_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2 = ((ReapplyDrivenProperties_t822 *)Castclass(L_2, ReapplyDrivenProperties_t822_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C" Rect_t225  RectTransform_get_rect_m4690 (RectTransform_t648 * __this, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		RectTransform_INTERNAL_get_rect_m5478(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t225  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void RectTransform_INTERNAL_get_rect_m5478 (RectTransform_t648 * __this, Rect_t225 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m5478_ftn) (RectTransform_t648 *, Rect_t225 *);
	static RectTransform_INTERNAL_get_rect_m5478_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m5478_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C" Vector2_t7  RectTransform_get_anchorMin_m4739 (RectTransform_t648 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchorMin_m5479(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchorMin_m4827 (RectTransform_t648 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m5480(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchorMin_m5479 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m5479_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_get_anchorMin_m5479_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m5479_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchorMin_m5480 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m5480_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_set_anchorMin_m5480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m5480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C" Vector2_t7  RectTransform_get_anchorMax_m4824 (RectTransform_t648 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchorMax_m5481(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchorMax_m4740 (RectTransform_t648 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m5482(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchorMax_m5481 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m5481_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_get_anchorMax_m5481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m5481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchorMax_m5482 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m5482_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_set_anchorMax_m5482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m5482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C" Vector2_t7  RectTransform_get_anchoredPosition_m4825 (RectTransform_t648 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		RectTransform_INTERNAL_get_anchoredPosition_m5483(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C" void RectTransform_set_anchoredPosition_m4828 (RectTransform_t648 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m5484(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_anchoredPosition_m5483 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m5483_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m5483_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m5483_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_anchoredPosition_m5484 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m5484_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m5484_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m5484_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C" Vector2_t7  RectTransform_get_sizeDelta_m4826 (RectTransform_t648 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		RectTransform_INTERNAL_get_sizeDelta_m5485(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C" void RectTransform_set_sizeDelta_m4741 (RectTransform_t648 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m5486(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_sizeDelta_m5485 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m5485_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_get_sizeDelta_m5485_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m5485_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_sizeDelta_m5486 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m5486_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_set_sizeDelta_m5486_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m5486_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C" Vector2_t7  RectTransform_get_pivot_m4738 (RectTransform_t648 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		RectTransform_INTERNAL_get_pivot_m5487(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C" void RectTransform_set_pivot_m4829 (RectTransform_t648 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m5488(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_get_pivot_m5487 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m5487_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_get_pivot_m5487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m5487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C" void RectTransform_INTERNAL_set_pivot_m5488 (RectTransform_t648 * __this, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m5488_ftn) (RectTransform_t648 *, Vector2_t7 *);
	static RectTransform_INTERNAL_set_pivot_m5488_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m5488_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern TypeInfo* RectTransform_t648_il2cpp_TypeInfo_var;
extern "C" void RectTransform_SendReapplyDrivenProperties_m5489 (Object_t * __this /* static, unused */, RectTransform_t648 * ___driven, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReapplyDrivenProperties_t822 * L_0 = ((RectTransform_t648_StaticFields*)RectTransform_t648_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		ReapplyDrivenProperties_t822 * L_1 = ((RectTransform_t648_StaticFields*)RectTransform_t648_il2cpp_TypeInfo_var->static_fields)->___reapplyDrivenProperties_2;
		RectTransform_t648 * L_2 = ___driven;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m5474(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern Il2CppCodeGenString* _stringLiteral950;
extern "C" void RectTransform_GetLocalCorners_m5490 (RectTransform_t648 * __this, Vector3U5BU5D_t317* ___fourCornersArray, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral950 = il2cpp_codegen_string_literal_from_index(950);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t225  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t317* L_0 = ___fourCornersArray;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t317* L_1 = ___fourCornersArray;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral950, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Rect_t225  L_2 = RectTransform_get_rect_m4690(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m2861((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m2865((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m3060((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m4751((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t317* L_7 = ___fourCornersArray;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t215  L_10 = {0};
		Vector3__ctor_m2812(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_7, 0)) = L_10;
		Vector3U5BU5D_t317* L_11 = ___fourCornersArray;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t215  L_14 = {0};
		Vector3__ctor_m2812(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_11, 1)) = L_14;
		Vector3U5BU5D_t317* L_15 = ___fourCornersArray;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t215  L_18 = {0};
		Vector3__ctor_m2812(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_15, 2)) = L_18;
		Vector3U5BU5D_t317* L_19 = ___fourCornersArray;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t215  L_22 = {0};
		Vector3__ctor_m2812(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_19, 3)) = L_22;
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern Il2CppCodeGenString* _stringLiteral951;
extern "C" void RectTransform_GetWorldCorners_m4873 (RectTransform_t648 * __this, Vector3U5BU5D_t317* ___fourCornersArray, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral951 = il2cpp_codegen_string_literal_from_index(951);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t243 * V_0 = {0};
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t317* L_0 = ___fourCornersArray;
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		Vector3U5BU5D_t317* L_1 = ___fourCornersArray;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) >= ((int32_t)4)))
		{
			goto IL_001a;
		}
	}

IL_000f:
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral951, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		Vector3U5BU5D_t317* L_2 = ___fourCornersArray;
		RectTransform_GetLocalCorners_m5490(__this, L_2, /*hidden argument*/NULL);
		Transform_t243 * L_3 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0051;
	}

IL_002f:
	{
		Vector3U5BU5D_t317* L_4 = ___fourCornersArray;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Transform_t243 * L_6 = V_0;
		Vector3U5BU5D_t317* L_7 = ___fourCornersArray;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		NullCheck(L_6);
		Vector3_t215  L_9 = Transform_TransformPoint_m4891(L_6, (*(Vector3_t215 *)((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_7, L_8))), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_4, L_5)) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_002f;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C" void RectTransform_SetInsetAndSizeFromParentEdge_m4948 (RectTransform_t648 * __this, int32_t ___edge, float ___inset, float ___size, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t7  V_3 = {0};
	Vector2_t7  V_4 = {0};
	Vector2_t7  V_5 = {0};
	Vector2_t7  V_6 = {0};
	Vector2_t7  V_7 = {0};
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t7 * G_B12_1 = {0};
	int32_t G_B11_0 = 0;
	Vector2_t7 * G_B11_1 = {0};
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t7 * G_B13_2 = {0};
	{
		int32_t L_0 = ___edge;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___edge;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0014;
		}
	}

IL_000e:
	{
		G_B4_0 = 1;
		goto IL_0015;
	}

IL_0014:
	{
		G_B4_0 = 0;
	}

IL_0015:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_3 = ___edge;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B7_0 = 1;
	}

IL_0024:
	{
		V_1 = G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0031;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B10_0 = 0;
	}

IL_0032:
	{
		V_2 = (((float)G_B10_0));
		Vector2_t7  L_5 = RectTransform_get_anchorMin_m4739(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m4754((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t7  L_8 = V_3;
		RectTransform_set_anchorMin_m4827(__this, L_8, /*hidden argument*/NULL);
		Vector2_t7  L_9 = RectTransform_get_anchorMax_m4824(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m4754((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t7  L_12 = V_3;
		RectTransform_set_anchorMax_m4740(__this, L_12, /*hidden argument*/NULL);
		Vector2_t7  L_13 = RectTransform_get_sizeDelta_m4826(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size;
		Vector2_set_Item_m4754((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t7  L_16 = V_4;
		RectTransform_set_sizeDelta_m4741(__this, L_16, /*hidden argument*/NULL);
		Vector2_t7  L_17 = RectTransform_get_anchoredPosition_m4825(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ac;
		}
	}
	{
		float L_20 = ___inset;
		float L_21 = ___size;
		Vector2_t7  L_22 = RectTransform_get_pivot_m4738(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m4746((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c0;
	}

IL_00ac:
	{
		float L_25 = ___inset;
		float L_26 = ___size;
		Vector2_t7  L_27 = RectTransform_get_pivot_m4738(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m4746((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c0:
	{
		Vector2_set_Item_m4754(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t7  L_30 = V_5;
		RectTransform_set_anchoredPosition_m4828(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C" void RectTransform_SetSizeWithCurrentAnchors_m4922 (RectTransform_t648 * __this, int32_t ___axis, float ___size, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t7  V_1 = {0};
	Vector2_t7  V_2 = {0};
	Vector2_t7  V_3 = {0};
	Vector2_t7  V_4 = {0};
	{
		int32_t L_0 = ___axis;
		V_0 = L_0;
		Vector2_t7  L_1 = RectTransform_get_sizeDelta_m4826(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size;
		Vector2_t7  L_4 = RectTransform_GetParentSize_m5491(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m4746((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t7  L_7 = RectTransform_get_anchorMax_m4824(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m4746((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t7  L_10 = RectTransform_get_anchorMin_m4739(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m4746((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m4754((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t7  L_13 = V_1;
		RectTransform_set_sizeDelta_m4741(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern TypeInfo* RectTransform_t648_il2cpp_TypeInfo_var;
extern "C" Vector2_t7  RectTransform_GetParentSize_m5491 (RectTransform_t648 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	RectTransform_t648 * V_0 = {0};
	Rect_t225  V_1 = {0};
	{
		Transform_t243 * L_0 = Transform_get_parent_m2854(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t648 *)IsInst(L_0, RectTransform_t648_il2cpp_TypeInfo_var));
		RectTransform_t648 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t7  L_3 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		RectTransform_t648 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t225  L_5 = RectTransform_get_rect_m4690(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t7  L_6 = Rect_get_size_m2808((&V_1), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.ResourceRequest
#include "UnityEngine_UnityEngine_ResourceRequest.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ResourceRequest
#include "UnityEngine_UnityEngine_ResourceRequestMethodDeclarations.h"

// UnityEngine.Resources
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"


// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m5492 (ResourceRequest_t900 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m5569(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t473 * ResourceRequest_get_asset_m5493 (ResourceRequest_t900 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Path_1);
		Type_t * L_1 = (__this->___m_Type_2);
		Object_t473 * L_2 = Resources_Load_m3161(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_Resources.h"
#ifndef _MSC_VER
#else
#endif

// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"


// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern const Il2CppType* Object_t473_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" Object_t473 * Resources_Load_m2735 (Object_t * __this /* static, unused */, String_t* ___path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t473_0_0_0_var = il2cpp_codegen_type_from_index(481);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m2838(NULL /*static, unused*/, LoadTypeToken(Object_t473_0_0_0_var), /*hidden argument*/NULL);
		Object_t473 * L_2 = Resources_Load_m3161(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C" Object_t473 * Resources_Load_m3161 (Object_t * __this /* static, unused */, String_t* ___path, Type_t * ___systemTypeInstance, const MethodInfo* method)
{
	typedef Object_t473 * (*Resources_Load_m3161_ftn) (String_t*, Type_t *);
	static Resources_Load_m3161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m3161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path, ___systemTypeInstance);
}
// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariables.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SerializePrivateVariables
#include "UnityEngine_UnityEngine_SerializePrivateVariablesMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"


// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C" void SerializePrivateVariables__ctor_m5494 (SerializePrivateVariables_t902 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6377(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"



// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m5495 (SerializeField_t904 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m6377(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"



// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern "C" Shader_t1 * Shader_Find_m2719 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef Shader_t1 * (*Shader_Find_m2719_ftn) (String_t*);
	static Shader_Find_m2719_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_Find_m2719_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Shader::SetGlobalColor(System.String,UnityEngine.Color)
extern "C" void Shader_SetGlobalColor_m5496 (Object_t * __this /* static, unused */, String_t* ___propertyName, Color_t6  ___color, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t6  L_2 = ___color;
		Shader_SetGlobalColor_m5497(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalColor(System.Int32,UnityEngine.Color)
extern "C" void Shader_SetGlobalColor_m5497 (Object_t * __this /* static, unused */, int32_t ___nameID, Color_t6  ___color, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID;
		Shader_INTERNAL_CALL_SetGlobalColor_m5498(NULL /*static, unused*/, L_0, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalColor(System.Int32,UnityEngine.Color&)
extern "C" void Shader_INTERNAL_CALL_SetGlobalColor_m5498 (Object_t * __this /* static, unused */, int32_t ___nameID, Color_t6 * ___color, const MethodInfo* method)
{
	typedef void (*Shader_INTERNAL_CALL_SetGlobalColor_m5498_ftn) (int32_t, Color_t6 *);
	static Shader_INTERNAL_CALL_SetGlobalColor_m5498_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_INTERNAL_CALL_SetGlobalColor_m5498_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::INTERNAL_CALL_SetGlobalColor(System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___nameID, ___color);
}
// System.Void UnityEngine.Shader::SetGlobalVector(System.String,UnityEngine.Vector4)
extern "C" void Shader_SetGlobalVector_m2878 (Object_t * __this /* static, unused */, String_t* ___propertyName, Vector4_t5  ___vec, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		Vector4_t5  L_1 = ___vec;
		Color_t6  L_2 = Color_op_Implicit_m5375(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Shader_SetGlobalColor_m5496(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
extern "C" void Shader_SetGlobalFloat_m2879 (Object_t * __this /* static, unused */, String_t* ___propertyName, float ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value;
		Shader_SetGlobalFloat_m5499(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)
extern "C" void Shader_SetGlobalFloat_m5499 (Object_t * __this /* static, unused */, int32_t ___nameID, float ___value, const MethodInfo* method)
{
	typedef void (*Shader_SetGlobalFloat_m5499_ftn) (int32_t, float);
	static Shader_SetGlobalFloat_m5499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_SetGlobalFloat_m5499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::SetGlobalFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(___nameID, ___value);
}
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.String,UnityEngine.Matrix4x4)
extern "C" void Shader_SetGlobalMatrix_m2877 (Object_t * __this /* static, unused */, String_t* ___propertyName, Matrix4x4_t242  ___mat, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Matrix4x4_t242  L_2 = ___mat;
		Shader_SetGlobalMatrix_m5500(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4)
extern "C" void Shader_SetGlobalMatrix_m5500 (Object_t * __this /* static, unused */, int32_t ___nameID, Matrix4x4_t242  ___mat, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID;
		Shader_INTERNAL_CALL_SetGlobalMatrix_m5501(NULL /*static, unused*/, L_0, (&___mat), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)
extern "C" void Shader_INTERNAL_CALL_SetGlobalMatrix_m5501 (Object_t * __this /* static, unused */, int32_t ___nameID, Matrix4x4_t242 * ___mat, const MethodInfo* method)
{
	typedef void (*Shader_INTERNAL_CALL_SetGlobalMatrix_m5501_ftn) (int32_t, Matrix4x4_t242 *);
	static Shader_INTERNAL_CALL_SetGlobalMatrix_m5501_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_INTERNAL_CALL_SetGlobalMatrix_m5501_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::INTERNAL_CALL_SetGlobalMatrix(System.Int32,UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(___nameID, ___mat);
}
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern "C" int32_t Shader_PropertyToID_m5502 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Shader_PropertyToID_m5502_ftn) (String_t*);
	static Shader_PropertyToID_m5502_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Shader_PropertyToID_m5502_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Shader::PropertyToID(System.String)");
	return _il2cpp_icall_func(___name);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"



// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern "C" void Material__ctor_m2717 (Material_t2 * __this, Shader_t1 * ___shader, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		Shader_t1 * L_0 = ___shader;
		Material_Internal_CreateWithShader_m5515(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern "C" void Material__ctor_m4901 (Material_t2 * __this, Material_t2 * ___source, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		Material_t2 * L_0 = ___source;
		Material_Internal_CreateWithMaterial_m5516(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color UnityEngine.Material::get_color()
extern Il2CppCodeGenString* _stringLiteral952;
extern "C" Color_t6  Material_get_color_m3303 (Material_t2 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral952 = il2cpp_codegen_string_literal_from_index(952);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t6  L_0 = Material_GetColor_m3320(__this, _stringLiteral952, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern Il2CppCodeGenString* _stringLiteral952;
extern "C" void Material_set_color_m2765 (Material_t2 * __this, Color_t6  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral952 = il2cpp_codegen_string_literal_from_index(952);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t6  L_0 = ___value;
		Material_SetColor_m2725(__this, _stringLiteral952, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern Il2CppCodeGenString* _stringLiteral953;
extern "C" Texture_t221 * Material_get_mainTexture_m4905 (Material_t2 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral953 = il2cpp_codegen_string_literal_from_index(953);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t221 * L_0 = Material_GetTexture_m5507(__this, _stringLiteral953, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern Il2CppCodeGenString* _stringLiteral953;
extern "C" void Material_set_mainTexture_m2805 (Material_t2 * __this, Texture_t221 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral953 = il2cpp_codegen_string_literal_from_index(953);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture_t221 * L_0 = ___value;
		Material_SetTexture_m2736(__this, _stringLiteral953, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::set_mainTextureOffset(UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral953;
extern "C" void Material_set_mainTextureOffset_m2807 (Material_t2 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral953 = il2cpp_codegen_string_literal_from_index(953);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t7  L_0 = ___value;
		Material_SetTextureOffset_m5509(__this, _stringLiteral953, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::set_mainTextureScale(UnityEngine.Vector2)
extern Il2CppCodeGenString* _stringLiteral953;
extern "C" void Material_set_mainTextureScale_m2809 (Material_t2 * __this, Vector2_t7  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral953 = il2cpp_codegen_string_literal_from_index(953);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t7  L_0 = ___value;
		Material_SetTextureScale_m5511(__this, _stringLiteral953, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern "C" void Material_SetColor_m2725 (Material_t2 * __this, String_t* ___propertyName, Color_t6  ___color, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t6  L_2 = ___color;
		Material_SetColor_m5503(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern "C" void Material_SetColor_m5503 (Material_t2 * __this, int32_t ___nameID, Color_t6  ___color, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nameID;
		Material_INTERNAL_CALL_SetColor_m5504(NULL /*static, unused*/, __this, L_0, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
extern "C" void Material_INTERNAL_CALL_SetColor_m5504 (Object_t * __this /* static, unused */, Material_t2 * ___self, int32_t ___nameID, Color_t6 * ___color, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetColor_m5504_ftn) (Material_t2 *, int32_t, Color_t6 *);
	static Material_INTERNAL_CALL_SetColor_m5504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetColor_m5504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___nameID, ___color);
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.String)
extern "C" Color_t6  Material_GetColor_m3320 (Material_t2 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t6  L_2 = Material_GetColor_m5505(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Color UnityEngine.Material::GetColor(System.Int32)
extern "C" Color_t6  Material_GetColor_m5505 (Material_t2 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef Color_t6  (*Material_GetColor_m5505_ftn) (Material_t2 *, int32_t);
	static Material_GetColor_m5505_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetColor_m5505_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetColor(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern "C" void Material_SetVector_m2729 (Material_t2 * __this, String_t* ___propertyName, Vector4_t5  ___vector, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		float L_1 = ((&___vector)->___x_1);
		float L_2 = ((&___vector)->___y_2);
		float L_3 = ((&___vector)->___z_3);
		float L_4 = ((&___vector)->___w_4);
		Color_t6  L_5 = {0};
		Color__ctor_m2713(&L_5, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		Material_SetColor_m2725(__this, L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern "C" void Material_SetTexture_m2736 (Material_t2 * __this, String_t* ___propertyName, Texture_t221 * ___texture, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t221 * L_2 = ___texture;
		Material_SetTexture_m5506(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern "C" void Material_SetTexture_m5506 (Material_t2 * __this, int32_t ___nameID, Texture_t221 * ___texture, const MethodInfo* method)
{
	typedef void (*Material_SetTexture_m5506_ftn) (Material_t2 *, int32_t, Texture_t221 *);
	static Material_SetTexture_m5506_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetTexture_m5506_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___nameID, ___texture);
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern "C" Texture_t221 * Material_GetTexture_m5507 (Material_t2 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Texture_t221 * L_2 = Material_GetTexture_m5508(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern "C" Texture_t221 * Material_GetTexture_m5508 (Material_t2 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef Texture_t221 * (*Material_GetTexture_m5508_ftn) (Material_t2 *, int32_t);
	static Material_GetTexture_m5508_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_GetTexture_m5508_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::GetTexture(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Void UnityEngine.Material::SetTextureOffset(System.String,UnityEngine.Vector2)
extern "C" void Material_SetTextureOffset_m5509 (Material_t2 * __this, String_t* ___propertyName, Vector2_t7  ___offset, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		Material_INTERNAL_CALL_SetTextureOffset_m5510(NULL /*static, unused*/, __this, L_0, (&___offset), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)
extern "C" void Material_INTERNAL_CALL_SetTextureOffset_m5510 (Object_t * __this /* static, unused */, Material_t2 * ___self, String_t* ___propertyName, Vector2_t7 * ___offset, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetTextureOffset_m5510_ftn) (Material_t2 *, String_t*, Vector2_t7 *);
	static Material_INTERNAL_CALL_SetTextureOffset_m5510_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetTextureOffset_m5510_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___propertyName, ___offset);
}
// System.Void UnityEngine.Material::SetTextureScale(System.String,UnityEngine.Vector2)
extern "C" void Material_SetTextureScale_m5511 (Material_t2 * __this, String_t* ___propertyName, Vector2_t7  ___scale, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		Material_INTERNAL_CALL_SetTextureScale_m5512(NULL /*static, unused*/, __this, L_0, (&___scale), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureScale(UnityEngine.Material,System.String,UnityEngine.Vector2&)
extern "C" void Material_INTERNAL_CALL_SetTextureScale_m5512 (Object_t * __this /* static, unused */, Material_t2 * ___self, String_t* ___propertyName, Vector2_t7 * ___scale, const MethodInfo* method)
{
	typedef void (*Material_INTERNAL_CALL_SetTextureScale_m5512_ftn) (Material_t2 *, String_t*, Vector2_t7 *);
	static Material_INTERNAL_CALL_SetTextureScale_m5512_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_INTERNAL_CALL_SetTextureScale_m5512_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::INTERNAL_CALL_SetTextureScale(UnityEngine.Material,System.String,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___propertyName, ___scale);
}
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C" void Material_SetFloat_m2724 (Material_t2 * __this, String_t* ___propertyName, float ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ___value;
		Material_SetFloat_m5513(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern "C" void Material_SetFloat_m5513 (Material_t2 * __this, int32_t ___nameID, float ___value, const MethodInfo* method)
{
	typedef void (*Material_SetFloat_m5513_ftn) (Material_t2 *, int32_t, float);
	static Material_SetFloat_m5513_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetFloat_m5513_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___nameID, ___value);
}
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern "C" void Material_SetInt_m2743 (Material_t2 * __this, String_t* ___propertyName, int32_t ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = ___value;
		Material_SetFloat_m2724(__this, L_0, (((float)L_1)), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.String)
extern "C" bool Material_HasProperty_m4899 (Material_t2 * __this, String_t* ___propertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___propertyName;
		int32_t L_1 = Shader_PropertyToID_m5502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_2 = Material_HasProperty_m5514(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern "C" bool Material_HasProperty_m5514 (Material_t2 * __this, int32_t ___nameID, const MethodInfo* method)
{
	typedef bool (*Material_HasProperty_m5514_ftn) (Material_t2 *, int32_t);
	static Material_HasProperty_m5514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_HasProperty_m5514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::HasProperty(System.Int32)");
	return _il2cpp_icall_func(__this, ___nameID);
}
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern "C" bool Material_SetPass_m2910 (Material_t2 * __this, int32_t ___pass, const MethodInfo* method)
{
	typedef bool (*Material_SetPass_m2910_ftn) (Material_t2 *, int32_t);
	static Material_SetPass_m2910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_SetPass_m2910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::SetPass(System.Int32)");
	return _il2cpp_icall_func(__this, ___pass);
}
// System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
extern "C" void Material_Internal_CreateWithShader_m5515 (Object_t * __this /* static, unused */, Material_t2 * ___mono, Shader_t1 * ___shader, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithShader_m5515_ftn) (Material_t2 *, Shader_t1 *);
	static Material_Internal_CreateWithShader_m5515_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithShader_m5515_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)");
	_il2cpp_icall_func(___mono, ___shader);
}
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern "C" void Material_Internal_CreateWithMaterial_m5516 (Object_t * __this /* static, unused */, Material_t2 * ___mono, Material_t2 * ___source, const MethodInfo* method)
{
	typedef void (*Material_Internal_CreateWithMaterial_m5516_ftn) (Material_t2 *, Material_t2 *);
	static Material_Internal_CreateWithMaterial_m5516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Material_Internal_CreateWithMaterial_m5516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)");
	_il2cpp_icall_func(___mono, ___source);
}
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2MethodDeclarations.h"



// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::Clear()
extern "C" void SphericalHarmonicsL2_Clear_m5517 (SphericalHarmonicsL2_t905 * __this, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_ClearInternal_m5518(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_ClearInternal_m5518 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t905 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t905 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m5519(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m5519 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t905 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m5519_ftn) (SphericalHarmonicsL2_t905 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m5519_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m5519_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___sh);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddAmbientLight(UnityEngine.Color)
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m5520 (SphericalHarmonicsL2_t905 * __this, Color_t6  ___color, const MethodInfo* method)
{
	{
		Color_t6  L_0 = ___color;
		SphericalHarmonicsL2_AddAmbientLightInternal_m5521(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddAmbientLightInternal(UnityEngine.Color,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m5521 (Object_t * __this /* static, unused */, Color_t6  ___color, SphericalHarmonicsL2_t905 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t905 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m5522(NULL /*static, unused*/, (&___color), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m5522 (Object_t * __this /* static, unused */, Color_t6 * ___color, SphericalHarmonicsL2_t905 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m5522_ftn) (Color_t6 *, SphericalHarmonicsL2_t905 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m5522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m5522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___color, ___sh);
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddDirectionalLight(UnityEngine.Vector3,UnityEngine.Color,System.Single)
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m5523 (SphericalHarmonicsL2_t905 * __this, Vector3_t215  ___direction, Color_t6  ___color, float ___intensity, const MethodInfo* method)
{
	Color_t6  V_0 = {0};
	{
		Color_t6  L_0 = ___color;
		float L_1 = ___intensity;
		Color_t6  L_2 = Color_op_Multiply_m4889(NULL /*static, unused*/, L_0, ((float)((float)(2.0f)*(float)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t215  L_3 = ___direction;
		Color_t6  L_4 = V_0;
		SphericalHarmonicsL2_AddDirectionalLightInternal_m5524(NULL /*static, unused*/, L_3, L_4, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::AddDirectionalLightInternal(UnityEngine.Vector3,UnityEngine.Color,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m5524 (Object_t * __this /* static, unused */, Vector3_t215  ___direction, Color_t6  ___color, SphericalHarmonicsL2_t905 * ___sh, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t905 * L_0 = ___sh;
		SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m5525(NULL /*static, unused*/, (&___direction), (&___color), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m5525 (Object_t * __this /* static, unused */, Vector3_t215 * ___direction, Color_t6 * ___color, SphericalHarmonicsL2_t905 * ___sh, const MethodInfo* method)
{
	typedef void (*SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m5525_ftn) (Vector3_t215 *, Color_t6 *, SphericalHarmonicsL2_t905 *);
	static SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m5525_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m5525_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)");
	_il2cpp_icall_func(___direction, ___color, ___sh);
}
// System.Single UnityEngine.Rendering.SphericalHarmonicsL2::get_Item(System.Int32,System.Int32)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral954;
extern "C" float SphericalHarmonicsL2_get_Item_m5526 (SphericalHarmonicsL2_t905 * __this, int32_t ___rgb, int32_t ___coefficient, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral954 = il2cpp_codegen_string_literal_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___rgb;
		int32_t L_1 = ___coefficient;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0080;
		}
		if (L_3 == 1)
		{
			goto IL_0087;
		}
		if (L_3 == 2)
		{
			goto IL_008e;
		}
		if (L_3 == 3)
		{
			goto IL_0095;
		}
		if (L_3 == 4)
		{
			goto IL_009c;
		}
		if (L_3 == 5)
		{
			goto IL_00a3;
		}
		if (L_3 == 6)
		{
			goto IL_00aa;
		}
		if (L_3 == 7)
		{
			goto IL_00b1;
		}
		if (L_3 == 8)
		{
			goto IL_00b8;
		}
		if (L_3 == 9)
		{
			goto IL_00bf;
		}
		if (L_3 == 10)
		{
			goto IL_00c6;
		}
		if (L_3 == 11)
		{
			goto IL_00cd;
		}
		if (L_3 == 12)
		{
			goto IL_00d4;
		}
		if (L_3 == 13)
		{
			goto IL_00db;
		}
		if (L_3 == 14)
		{
			goto IL_00e2;
		}
		if (L_3 == 15)
		{
			goto IL_00e9;
		}
		if (L_3 == 16)
		{
			goto IL_00f0;
		}
		if (L_3 == 17)
		{
			goto IL_00f7;
		}
		if (L_3 == 18)
		{
			goto IL_00fe;
		}
		if (L_3 == 19)
		{
			goto IL_0105;
		}
		if (L_3 == 20)
		{
			goto IL_010c;
		}
		if (L_3 == 21)
		{
			goto IL_0113;
		}
		if (L_3 == 22)
		{
			goto IL_011a;
		}
		if (L_3 == 23)
		{
			goto IL_0121;
		}
		if (L_3 == 24)
		{
			goto IL_0128;
		}
		if (L_3 == 25)
		{
			goto IL_012f;
		}
		if (L_3 == 26)
		{
			goto IL_0136;
		}
	}
	{
		goto IL_013d;
	}

IL_0080:
	{
		float L_4 = (__this->___shr0_0);
		return L_4;
	}

IL_0087:
	{
		float L_5 = (__this->___shr1_1);
		return L_5;
	}

IL_008e:
	{
		float L_6 = (__this->___shr2_2);
		return L_6;
	}

IL_0095:
	{
		float L_7 = (__this->___shr3_3);
		return L_7;
	}

IL_009c:
	{
		float L_8 = (__this->___shr4_4);
		return L_8;
	}

IL_00a3:
	{
		float L_9 = (__this->___shr5_5);
		return L_9;
	}

IL_00aa:
	{
		float L_10 = (__this->___shr6_6);
		return L_10;
	}

IL_00b1:
	{
		float L_11 = (__this->___shr7_7);
		return L_11;
	}

IL_00b8:
	{
		float L_12 = (__this->___shr8_8);
		return L_12;
	}

IL_00bf:
	{
		float L_13 = (__this->___shg0_9);
		return L_13;
	}

IL_00c6:
	{
		float L_14 = (__this->___shg1_10);
		return L_14;
	}

IL_00cd:
	{
		float L_15 = (__this->___shg2_11);
		return L_15;
	}

IL_00d4:
	{
		float L_16 = (__this->___shg3_12);
		return L_16;
	}

IL_00db:
	{
		float L_17 = (__this->___shg4_13);
		return L_17;
	}

IL_00e2:
	{
		float L_18 = (__this->___shg5_14);
		return L_18;
	}

IL_00e9:
	{
		float L_19 = (__this->___shg6_15);
		return L_19;
	}

IL_00f0:
	{
		float L_20 = (__this->___shg7_16);
		return L_20;
	}

IL_00f7:
	{
		float L_21 = (__this->___shg8_17);
		return L_21;
	}

IL_00fe:
	{
		float L_22 = (__this->___shb0_18);
		return L_22;
	}

IL_0105:
	{
		float L_23 = (__this->___shb1_19);
		return L_23;
	}

IL_010c:
	{
		float L_24 = (__this->___shb2_20);
		return L_24;
	}

IL_0113:
	{
		float L_25 = (__this->___shb3_21);
		return L_25;
	}

IL_011a:
	{
		float L_26 = (__this->___shb4_22);
		return L_26;
	}

IL_0121:
	{
		float L_27 = (__this->___shb5_23);
		return L_27;
	}

IL_0128:
	{
		float L_28 = (__this->___shb6_24);
		return L_28;
	}

IL_012f:
	{
		float L_29 = (__this->___shb7_25);
		return L_29;
	}

IL_0136:
	{
		float L_30 = (__this->___shb8_26);
		return L_30;
	}

IL_013d:
	{
		IndexOutOfRangeException_t1116 * L_31 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_31, _stringLiteral954, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_31);
	}
}
// System.Void UnityEngine.Rendering.SphericalHarmonicsL2::set_Item(System.Int32,System.Int32,System.Single)
extern TypeInfo* IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral954;
extern "C" void SphericalHarmonicsL2_set_Item_m5527 (SphericalHarmonicsL2_t905 * __this, int32_t ___rgb, int32_t ___coefficient, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(657);
		_stringLiteral954 = il2cpp_codegen_string_literal_from_index(954);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___rgb;
		int32_t L_1 = ___coefficient;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)9)))+(int32_t)L_1));
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0080;
		}
		if (L_3 == 1)
		{
			goto IL_008c;
		}
		if (L_3 == 2)
		{
			goto IL_0098;
		}
		if (L_3 == 3)
		{
			goto IL_00a4;
		}
		if (L_3 == 4)
		{
			goto IL_00b0;
		}
		if (L_3 == 5)
		{
			goto IL_00bc;
		}
		if (L_3 == 6)
		{
			goto IL_00c8;
		}
		if (L_3 == 7)
		{
			goto IL_00d4;
		}
		if (L_3 == 8)
		{
			goto IL_00e0;
		}
		if (L_3 == 9)
		{
			goto IL_00ec;
		}
		if (L_3 == 10)
		{
			goto IL_00f8;
		}
		if (L_3 == 11)
		{
			goto IL_0104;
		}
		if (L_3 == 12)
		{
			goto IL_0110;
		}
		if (L_3 == 13)
		{
			goto IL_011c;
		}
		if (L_3 == 14)
		{
			goto IL_0128;
		}
		if (L_3 == 15)
		{
			goto IL_0134;
		}
		if (L_3 == 16)
		{
			goto IL_0140;
		}
		if (L_3 == 17)
		{
			goto IL_014c;
		}
		if (L_3 == 18)
		{
			goto IL_0158;
		}
		if (L_3 == 19)
		{
			goto IL_0164;
		}
		if (L_3 == 20)
		{
			goto IL_0170;
		}
		if (L_3 == 21)
		{
			goto IL_017c;
		}
		if (L_3 == 22)
		{
			goto IL_0188;
		}
		if (L_3 == 23)
		{
			goto IL_0194;
		}
		if (L_3 == 24)
		{
			goto IL_01a0;
		}
		if (L_3 == 25)
		{
			goto IL_01ac;
		}
		if (L_3 == 26)
		{
			goto IL_01b8;
		}
	}
	{
		goto IL_01c4;
	}

IL_0080:
	{
		float L_4 = ___value;
		__this->___shr0_0 = L_4;
		goto IL_01cf;
	}

IL_008c:
	{
		float L_5 = ___value;
		__this->___shr1_1 = L_5;
		goto IL_01cf;
	}

IL_0098:
	{
		float L_6 = ___value;
		__this->___shr2_2 = L_6;
		goto IL_01cf;
	}

IL_00a4:
	{
		float L_7 = ___value;
		__this->___shr3_3 = L_7;
		goto IL_01cf;
	}

IL_00b0:
	{
		float L_8 = ___value;
		__this->___shr4_4 = L_8;
		goto IL_01cf;
	}

IL_00bc:
	{
		float L_9 = ___value;
		__this->___shr5_5 = L_9;
		goto IL_01cf;
	}

IL_00c8:
	{
		float L_10 = ___value;
		__this->___shr6_6 = L_10;
		goto IL_01cf;
	}

IL_00d4:
	{
		float L_11 = ___value;
		__this->___shr7_7 = L_11;
		goto IL_01cf;
	}

IL_00e0:
	{
		float L_12 = ___value;
		__this->___shr8_8 = L_12;
		goto IL_01cf;
	}

IL_00ec:
	{
		float L_13 = ___value;
		__this->___shg0_9 = L_13;
		goto IL_01cf;
	}

IL_00f8:
	{
		float L_14 = ___value;
		__this->___shg1_10 = L_14;
		goto IL_01cf;
	}

IL_0104:
	{
		float L_15 = ___value;
		__this->___shg2_11 = L_15;
		goto IL_01cf;
	}

IL_0110:
	{
		float L_16 = ___value;
		__this->___shg3_12 = L_16;
		goto IL_01cf;
	}

IL_011c:
	{
		float L_17 = ___value;
		__this->___shg4_13 = L_17;
		goto IL_01cf;
	}

IL_0128:
	{
		float L_18 = ___value;
		__this->___shg5_14 = L_18;
		goto IL_01cf;
	}

IL_0134:
	{
		float L_19 = ___value;
		__this->___shg6_15 = L_19;
		goto IL_01cf;
	}

IL_0140:
	{
		float L_20 = ___value;
		__this->___shg7_16 = L_20;
		goto IL_01cf;
	}

IL_014c:
	{
		float L_21 = ___value;
		__this->___shg8_17 = L_21;
		goto IL_01cf;
	}

IL_0158:
	{
		float L_22 = ___value;
		__this->___shb0_18 = L_22;
		goto IL_01cf;
	}

IL_0164:
	{
		float L_23 = ___value;
		__this->___shb1_19 = L_23;
		goto IL_01cf;
	}

IL_0170:
	{
		float L_24 = ___value;
		__this->___shb2_20 = L_24;
		goto IL_01cf;
	}

IL_017c:
	{
		float L_25 = ___value;
		__this->___shb3_21 = L_25;
		goto IL_01cf;
	}

IL_0188:
	{
		float L_26 = ___value;
		__this->___shb4_22 = L_26;
		goto IL_01cf;
	}

IL_0194:
	{
		float L_27 = ___value;
		__this->___shb5_23 = L_27;
		goto IL_01cf;
	}

IL_01a0:
	{
		float L_28 = ___value;
		__this->___shb6_24 = L_28;
		goto IL_01cf;
	}

IL_01ac:
	{
		float L_29 = ___value;
		__this->___shb7_25 = L_29;
		goto IL_01cf;
	}

IL_01b8:
	{
		float L_30 = ___value;
		__this->___shb8_26 = L_30;
		goto IL_01cf;
	}

IL_01c4:
	{
		IndexOutOfRangeException_t1116 * L_31 = (IndexOutOfRangeException_t1116 *)il2cpp_codegen_object_new (IndexOutOfRangeException_t1116_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6374(L_31, _stringLiteral954, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_31);
	}

IL_01cf:
	{
		return;
	}
}
// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C" int32_t SphericalHarmonicsL2_GetHashCode_m5528 (SphericalHarmonicsL2_t905 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = ((int32_t)17);
		int32_t L_0 = V_0;
		float* L_1 = &(__this->___shr0_0);
		int32_t L_2 = Single_GetHashCode_m6357(L_1, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0*(int32_t)((int32_t)23)))+(int32_t)L_2));
		int32_t L_3 = V_0;
		float* L_4 = &(__this->___shr1_1);
		int32_t L_5 = Single_GetHashCode_m6357(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_3*(int32_t)((int32_t)23)))+(int32_t)L_5));
		int32_t L_6 = V_0;
		float* L_7 = &(__this->___shr2_2);
		int32_t L_8 = Single_GetHashCode_m6357(L_7, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)23)))+(int32_t)L_8));
		int32_t L_9 = V_0;
		float* L_10 = &(__this->___shr3_3);
		int32_t L_11 = Single_GetHashCode_m6357(L_10, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)((int32_t)23)))+(int32_t)L_11));
		int32_t L_12 = V_0;
		float* L_13 = &(__this->___shr4_4);
		int32_t L_14 = Single_GetHashCode_m6357(L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)((int32_t)23)))+(int32_t)L_14));
		int32_t L_15 = V_0;
		float* L_16 = &(__this->___shr5_5);
		int32_t L_17 = Single_GetHashCode_m6357(L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)((int32_t)23)))+(int32_t)L_17));
		int32_t L_18 = V_0;
		float* L_19 = &(__this->___shr6_6);
		int32_t L_20 = Single_GetHashCode_m6357(L_19, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)((int32_t)23)))+(int32_t)L_20));
		int32_t L_21 = V_0;
		float* L_22 = &(__this->___shr7_7);
		int32_t L_23 = Single_GetHashCode_m6357(L_22, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_21*(int32_t)((int32_t)23)))+(int32_t)L_23));
		int32_t L_24 = V_0;
		float* L_25 = &(__this->___shr8_8);
		int32_t L_26 = Single_GetHashCode_m6357(L_25, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_24*(int32_t)((int32_t)23)))+(int32_t)L_26));
		int32_t L_27 = V_0;
		float* L_28 = &(__this->___shg0_9);
		int32_t L_29 = Single_GetHashCode_m6357(L_28, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_27*(int32_t)((int32_t)23)))+(int32_t)L_29));
		int32_t L_30 = V_0;
		float* L_31 = &(__this->___shg1_10);
		int32_t L_32 = Single_GetHashCode_m6357(L_31, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)23)))+(int32_t)L_32));
		int32_t L_33 = V_0;
		float* L_34 = &(__this->___shg2_11);
		int32_t L_35 = Single_GetHashCode_m6357(L_34, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_33*(int32_t)((int32_t)23)))+(int32_t)L_35));
		int32_t L_36 = V_0;
		float* L_37 = &(__this->___shg3_12);
		int32_t L_38 = Single_GetHashCode_m6357(L_37, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_36*(int32_t)((int32_t)23)))+(int32_t)L_38));
		int32_t L_39 = V_0;
		float* L_40 = &(__this->___shg4_13);
		int32_t L_41 = Single_GetHashCode_m6357(L_40, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_39*(int32_t)((int32_t)23)))+(int32_t)L_41));
		int32_t L_42 = V_0;
		float* L_43 = &(__this->___shg5_14);
		int32_t L_44 = Single_GetHashCode_m6357(L_43, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_42*(int32_t)((int32_t)23)))+(int32_t)L_44));
		int32_t L_45 = V_0;
		float* L_46 = &(__this->___shg6_15);
		int32_t L_47 = Single_GetHashCode_m6357(L_46, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)((int32_t)23)))+(int32_t)L_47));
		int32_t L_48 = V_0;
		float* L_49 = &(__this->___shg7_16);
		int32_t L_50 = Single_GetHashCode_m6357(L_49, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_48*(int32_t)((int32_t)23)))+(int32_t)L_50));
		int32_t L_51 = V_0;
		float* L_52 = &(__this->___shg8_17);
		int32_t L_53 = Single_GetHashCode_m6357(L_52, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_51*(int32_t)((int32_t)23)))+(int32_t)L_53));
		int32_t L_54 = V_0;
		float* L_55 = &(__this->___shb0_18);
		int32_t L_56 = Single_GetHashCode_m6357(L_55, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_54*(int32_t)((int32_t)23)))+(int32_t)L_56));
		int32_t L_57 = V_0;
		float* L_58 = &(__this->___shb1_19);
		int32_t L_59 = Single_GetHashCode_m6357(L_58, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_57*(int32_t)((int32_t)23)))+(int32_t)L_59));
		int32_t L_60 = V_0;
		float* L_61 = &(__this->___shb2_20);
		int32_t L_62 = Single_GetHashCode_m6357(L_61, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60*(int32_t)((int32_t)23)))+(int32_t)L_62));
		int32_t L_63 = V_0;
		float* L_64 = &(__this->___shb3_21);
		int32_t L_65 = Single_GetHashCode_m6357(L_64, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_63*(int32_t)((int32_t)23)))+(int32_t)L_65));
		int32_t L_66 = V_0;
		float* L_67 = &(__this->___shb4_22);
		int32_t L_68 = Single_GetHashCode_m6357(L_67, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)((int32_t)23)))+(int32_t)L_68));
		int32_t L_69 = V_0;
		float* L_70 = &(__this->___shb5_23);
		int32_t L_71 = Single_GetHashCode_m6357(L_70, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)((int32_t)23)))+(int32_t)L_71));
		int32_t L_72 = V_0;
		float* L_73 = &(__this->___shb6_24);
		int32_t L_74 = Single_GetHashCode_m6357(L_73, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_72*(int32_t)((int32_t)23)))+(int32_t)L_74));
		int32_t L_75 = V_0;
		float* L_76 = &(__this->___shb7_25);
		int32_t L_77 = Single_GetHashCode_m6357(L_76, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_75*(int32_t)((int32_t)23)))+(int32_t)L_77));
		int32_t L_78 = V_0;
		float* L_79 = &(__this->___shb8_26);
		int32_t L_80 = Single_GetHashCode_m6357(L_79, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_78*(int32_t)((int32_t)23)))+(int32_t)L_80));
		int32_t L_81 = V_0;
		return L_81;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern TypeInfo* SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var;
extern "C" bool SphericalHarmonicsL2_Equals_m5529 (SphericalHarmonicsL2_t905 * __this, Object_t * ___other, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t905  V_0 = {0};
	{
		Object_t * L_0 = ___other;
		if (((Object_t *)IsInst(L_0, SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Object_t * L_1 = ___other;
		V_0 = ((*(SphericalHarmonicsL2_t905 *)((SphericalHarmonicsL2_t905 *)UnBox (L_1, SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var))));
		SphericalHarmonicsL2_t905  L_2 = V_0;
		bool L_3 = SphericalHarmonicsL2_op_Equality_m5533(NULL /*static, unused*/, (*(SphericalHarmonicsL2_t905 *)__this), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Multiply(UnityEngine.Rendering.SphericalHarmonicsL2,System.Single)
extern TypeInfo* SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t905  SphericalHarmonicsL2_op_Multiply_m5530 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t905  ___lhs, float ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t905  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ___rhs;
		(&V_0)->___shr0_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ___rhs;
		(&V_0)->___shr1_1 = ((float)((float)L_2*(float)L_3));
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ___rhs;
		(&V_0)->___shr2_2 = ((float)((float)L_4*(float)L_5));
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ___rhs;
		(&V_0)->___shr3_3 = ((float)((float)L_6*(float)L_7));
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ___rhs;
		(&V_0)->___shr4_4 = ((float)((float)L_8*(float)L_9));
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ___rhs;
		(&V_0)->___shr5_5 = ((float)((float)L_10*(float)L_11));
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ___rhs;
		(&V_0)->___shr6_6 = ((float)((float)L_12*(float)L_13));
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ___rhs;
		(&V_0)->___shr7_7 = ((float)((float)L_14*(float)L_15));
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ___rhs;
		(&V_0)->___shr8_8 = ((float)((float)L_16*(float)L_17));
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ___rhs;
		(&V_0)->___shg0_9 = ((float)((float)L_18*(float)L_19));
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ___rhs;
		(&V_0)->___shg1_10 = ((float)((float)L_20*(float)L_21));
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ___rhs;
		(&V_0)->___shg2_11 = ((float)((float)L_22*(float)L_23));
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ___rhs;
		(&V_0)->___shg3_12 = ((float)((float)L_24*(float)L_25));
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ___rhs;
		(&V_0)->___shg4_13 = ((float)((float)L_26*(float)L_27));
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ___rhs;
		(&V_0)->___shg5_14 = ((float)((float)L_28*(float)L_29));
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ___rhs;
		(&V_0)->___shg6_15 = ((float)((float)L_30*(float)L_31));
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ___rhs;
		(&V_0)->___shg7_16 = ((float)((float)L_32*(float)L_33));
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ___rhs;
		(&V_0)->___shg8_17 = ((float)((float)L_34*(float)L_35));
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ___rhs;
		(&V_0)->___shb0_18 = ((float)((float)L_36*(float)L_37));
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ___rhs;
		(&V_0)->___shb1_19 = ((float)((float)L_38*(float)L_39));
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ___rhs;
		(&V_0)->___shb2_20 = ((float)((float)L_40*(float)L_41));
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ___rhs;
		(&V_0)->___shb3_21 = ((float)((float)L_42*(float)L_43));
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ___rhs;
		(&V_0)->___shb4_22 = ((float)((float)L_44*(float)L_45));
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ___rhs;
		(&V_0)->___shb5_23 = ((float)((float)L_46*(float)L_47));
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ___rhs;
		(&V_0)->___shb6_24 = ((float)((float)L_48*(float)L_49));
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ___rhs;
		(&V_0)->___shb7_25 = ((float)((float)L_50*(float)L_51));
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ___rhs;
		(&V_0)->___shb8_26 = ((float)((float)L_52*(float)L_53));
		SphericalHarmonicsL2_t905  L_54 = V_0;
		return L_54;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Multiply(System.Single,UnityEngine.Rendering.SphericalHarmonicsL2)
extern TypeInfo* SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t905  SphericalHarmonicsL2_op_Multiply_m5531 (Object_t * __this /* static, unused */, float ___lhs, SphericalHarmonicsL2_t905  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t905  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___rhs)->___shr0_0);
		float L_1 = ___lhs;
		(&V_0)->___shr0_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = ((&___rhs)->___shr1_1);
		float L_3 = ___lhs;
		(&V_0)->___shr1_1 = ((float)((float)L_2*(float)L_3));
		float L_4 = ((&___rhs)->___shr2_2);
		float L_5 = ___lhs;
		(&V_0)->___shr2_2 = ((float)((float)L_4*(float)L_5));
		float L_6 = ((&___rhs)->___shr3_3);
		float L_7 = ___lhs;
		(&V_0)->___shr3_3 = ((float)((float)L_6*(float)L_7));
		float L_8 = ((&___rhs)->___shr4_4);
		float L_9 = ___lhs;
		(&V_0)->___shr4_4 = ((float)((float)L_8*(float)L_9));
		float L_10 = ((&___rhs)->___shr5_5);
		float L_11 = ___lhs;
		(&V_0)->___shr5_5 = ((float)((float)L_10*(float)L_11));
		float L_12 = ((&___rhs)->___shr6_6);
		float L_13 = ___lhs;
		(&V_0)->___shr6_6 = ((float)((float)L_12*(float)L_13));
		float L_14 = ((&___rhs)->___shr7_7);
		float L_15 = ___lhs;
		(&V_0)->___shr7_7 = ((float)((float)L_14*(float)L_15));
		float L_16 = ((&___rhs)->___shr8_8);
		float L_17 = ___lhs;
		(&V_0)->___shr8_8 = ((float)((float)L_16*(float)L_17));
		float L_18 = ((&___rhs)->___shg0_9);
		float L_19 = ___lhs;
		(&V_0)->___shg0_9 = ((float)((float)L_18*(float)L_19));
		float L_20 = ((&___rhs)->___shg1_10);
		float L_21 = ___lhs;
		(&V_0)->___shg1_10 = ((float)((float)L_20*(float)L_21));
		float L_22 = ((&___rhs)->___shg2_11);
		float L_23 = ___lhs;
		(&V_0)->___shg2_11 = ((float)((float)L_22*(float)L_23));
		float L_24 = ((&___rhs)->___shg3_12);
		float L_25 = ___lhs;
		(&V_0)->___shg3_12 = ((float)((float)L_24*(float)L_25));
		float L_26 = ((&___rhs)->___shg4_13);
		float L_27 = ___lhs;
		(&V_0)->___shg4_13 = ((float)((float)L_26*(float)L_27));
		float L_28 = ((&___rhs)->___shg5_14);
		float L_29 = ___lhs;
		(&V_0)->___shg5_14 = ((float)((float)L_28*(float)L_29));
		float L_30 = ((&___rhs)->___shg6_15);
		float L_31 = ___lhs;
		(&V_0)->___shg6_15 = ((float)((float)L_30*(float)L_31));
		float L_32 = ((&___rhs)->___shg7_16);
		float L_33 = ___lhs;
		(&V_0)->___shg7_16 = ((float)((float)L_32*(float)L_33));
		float L_34 = ((&___rhs)->___shg8_17);
		float L_35 = ___lhs;
		(&V_0)->___shg8_17 = ((float)((float)L_34*(float)L_35));
		float L_36 = ((&___rhs)->___shb0_18);
		float L_37 = ___lhs;
		(&V_0)->___shb0_18 = ((float)((float)L_36*(float)L_37));
		float L_38 = ((&___rhs)->___shb1_19);
		float L_39 = ___lhs;
		(&V_0)->___shb1_19 = ((float)((float)L_38*(float)L_39));
		float L_40 = ((&___rhs)->___shb2_20);
		float L_41 = ___lhs;
		(&V_0)->___shb2_20 = ((float)((float)L_40*(float)L_41));
		float L_42 = ((&___rhs)->___shb3_21);
		float L_43 = ___lhs;
		(&V_0)->___shb3_21 = ((float)((float)L_42*(float)L_43));
		float L_44 = ((&___rhs)->___shb4_22);
		float L_45 = ___lhs;
		(&V_0)->___shb4_22 = ((float)((float)L_44*(float)L_45));
		float L_46 = ((&___rhs)->___shb5_23);
		float L_47 = ___lhs;
		(&V_0)->___shb5_23 = ((float)((float)L_46*(float)L_47));
		float L_48 = ((&___rhs)->___shb6_24);
		float L_49 = ___lhs;
		(&V_0)->___shb6_24 = ((float)((float)L_48*(float)L_49));
		float L_50 = ((&___rhs)->___shb7_25);
		float L_51 = ___lhs;
		(&V_0)->___shb7_25 = ((float)((float)L_50*(float)L_51));
		float L_52 = ((&___rhs)->___shb8_26);
		float L_53 = ___lhs;
		(&V_0)->___shb8_26 = ((float)((float)L_52*(float)L_53));
		SphericalHarmonicsL2_t905  L_54 = V_0;
		return L_54;
	}
}
// UnityEngine.Rendering.SphericalHarmonicsL2 UnityEngine.Rendering.SphericalHarmonicsL2::op_Addition(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern TypeInfo* SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var;
extern "C" SphericalHarmonicsL2_t905  SphericalHarmonicsL2_op_Addition_m5532 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t905  ___lhs, SphericalHarmonicsL2_t905  ___rhs, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(661);
		s_Il2CppMethodIntialized = true;
	}
	SphericalHarmonicsL2_t905  V_0 = {0};
	{
		Initobj (SphericalHarmonicsL2_t905_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ((&___rhs)->___shr0_0);
		(&V_0)->___shr0_0 = ((float)((float)L_0+(float)L_1));
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ((&___rhs)->___shr1_1);
		(&V_0)->___shr1_1 = ((float)((float)L_2+(float)L_3));
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ((&___rhs)->___shr2_2);
		(&V_0)->___shr2_2 = ((float)((float)L_4+(float)L_5));
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ((&___rhs)->___shr3_3);
		(&V_0)->___shr3_3 = ((float)((float)L_6+(float)L_7));
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ((&___rhs)->___shr4_4);
		(&V_0)->___shr4_4 = ((float)((float)L_8+(float)L_9));
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ((&___rhs)->___shr5_5);
		(&V_0)->___shr5_5 = ((float)((float)L_10+(float)L_11));
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ((&___rhs)->___shr6_6);
		(&V_0)->___shr6_6 = ((float)((float)L_12+(float)L_13));
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ((&___rhs)->___shr7_7);
		(&V_0)->___shr7_7 = ((float)((float)L_14+(float)L_15));
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ((&___rhs)->___shr8_8);
		(&V_0)->___shr8_8 = ((float)((float)L_16+(float)L_17));
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ((&___rhs)->___shg0_9);
		(&V_0)->___shg0_9 = ((float)((float)L_18+(float)L_19));
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ((&___rhs)->___shg1_10);
		(&V_0)->___shg1_10 = ((float)((float)L_20+(float)L_21));
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ((&___rhs)->___shg2_11);
		(&V_0)->___shg2_11 = ((float)((float)L_22+(float)L_23));
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ((&___rhs)->___shg3_12);
		(&V_0)->___shg3_12 = ((float)((float)L_24+(float)L_25));
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ((&___rhs)->___shg4_13);
		(&V_0)->___shg4_13 = ((float)((float)L_26+(float)L_27));
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ((&___rhs)->___shg5_14);
		(&V_0)->___shg5_14 = ((float)((float)L_28+(float)L_29));
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ((&___rhs)->___shg6_15);
		(&V_0)->___shg6_15 = ((float)((float)L_30+(float)L_31));
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ((&___rhs)->___shg7_16);
		(&V_0)->___shg7_16 = ((float)((float)L_32+(float)L_33));
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ((&___rhs)->___shg8_17);
		(&V_0)->___shg8_17 = ((float)((float)L_34+(float)L_35));
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ((&___rhs)->___shb0_18);
		(&V_0)->___shb0_18 = ((float)((float)L_36+(float)L_37));
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ((&___rhs)->___shb1_19);
		(&V_0)->___shb1_19 = ((float)((float)L_38+(float)L_39));
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ((&___rhs)->___shb2_20);
		(&V_0)->___shb2_20 = ((float)((float)L_40+(float)L_41));
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ((&___rhs)->___shb3_21);
		(&V_0)->___shb3_21 = ((float)((float)L_42+(float)L_43));
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ((&___rhs)->___shb4_22);
		(&V_0)->___shb4_22 = ((float)((float)L_44+(float)L_45));
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ((&___rhs)->___shb5_23);
		(&V_0)->___shb5_23 = ((float)((float)L_46+(float)L_47));
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ((&___rhs)->___shb6_24);
		(&V_0)->___shb6_24 = ((float)((float)L_48+(float)L_49));
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ((&___rhs)->___shb7_25);
		(&V_0)->___shb7_25 = ((float)((float)L_50+(float)L_51));
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ((&___rhs)->___shb8_26);
		(&V_0)->___shb8_26 = ((float)((float)L_52+(float)L_53));
		SphericalHarmonicsL2_t905  L_54 = V_0;
		return L_54;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C" bool SphericalHarmonicsL2_op_Equality_m5533 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t905  ___lhs, SphericalHarmonicsL2_t905  ___rhs, const MethodInfo* method)
{
	int32_t G_B28_0 = 0;
	{
		float L_0 = ((&___lhs)->___shr0_0);
		float L_1 = ((&___rhs)->___shr0_0);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_0200;
		}
	}
	{
		float L_2 = ((&___lhs)->___shr1_1);
		float L_3 = ((&___rhs)->___shr1_1);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0200;
		}
	}
	{
		float L_4 = ((&___lhs)->___shr2_2);
		float L_5 = ((&___rhs)->___shr2_2);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_0200;
		}
	}
	{
		float L_6 = ((&___lhs)->___shr3_3);
		float L_7 = ((&___rhs)->___shr3_3);
		if ((!(((float)L_6) == ((float)L_7))))
		{
			goto IL_0200;
		}
	}
	{
		float L_8 = ((&___lhs)->___shr4_4);
		float L_9 = ((&___rhs)->___shr4_4);
		if ((!(((float)L_8) == ((float)L_9))))
		{
			goto IL_0200;
		}
	}
	{
		float L_10 = ((&___lhs)->___shr5_5);
		float L_11 = ((&___rhs)->___shr5_5);
		if ((!(((float)L_10) == ((float)L_11))))
		{
			goto IL_0200;
		}
	}
	{
		float L_12 = ((&___lhs)->___shr6_6);
		float L_13 = ((&___rhs)->___shr6_6);
		if ((!(((float)L_12) == ((float)L_13))))
		{
			goto IL_0200;
		}
	}
	{
		float L_14 = ((&___lhs)->___shr7_7);
		float L_15 = ((&___rhs)->___shr7_7);
		if ((!(((float)L_14) == ((float)L_15))))
		{
			goto IL_0200;
		}
	}
	{
		float L_16 = ((&___lhs)->___shr8_8);
		float L_17 = ((&___rhs)->___shr8_8);
		if ((!(((float)L_16) == ((float)L_17))))
		{
			goto IL_0200;
		}
	}
	{
		float L_18 = ((&___lhs)->___shg0_9);
		float L_19 = ((&___rhs)->___shg0_9);
		if ((!(((float)L_18) == ((float)L_19))))
		{
			goto IL_0200;
		}
	}
	{
		float L_20 = ((&___lhs)->___shg1_10);
		float L_21 = ((&___rhs)->___shg1_10);
		if ((!(((float)L_20) == ((float)L_21))))
		{
			goto IL_0200;
		}
	}
	{
		float L_22 = ((&___lhs)->___shg2_11);
		float L_23 = ((&___rhs)->___shg2_11);
		if ((!(((float)L_22) == ((float)L_23))))
		{
			goto IL_0200;
		}
	}
	{
		float L_24 = ((&___lhs)->___shg3_12);
		float L_25 = ((&___rhs)->___shg3_12);
		if ((!(((float)L_24) == ((float)L_25))))
		{
			goto IL_0200;
		}
	}
	{
		float L_26 = ((&___lhs)->___shg4_13);
		float L_27 = ((&___rhs)->___shg4_13);
		if ((!(((float)L_26) == ((float)L_27))))
		{
			goto IL_0200;
		}
	}
	{
		float L_28 = ((&___lhs)->___shg5_14);
		float L_29 = ((&___rhs)->___shg5_14);
		if ((!(((float)L_28) == ((float)L_29))))
		{
			goto IL_0200;
		}
	}
	{
		float L_30 = ((&___lhs)->___shg6_15);
		float L_31 = ((&___rhs)->___shg6_15);
		if ((!(((float)L_30) == ((float)L_31))))
		{
			goto IL_0200;
		}
	}
	{
		float L_32 = ((&___lhs)->___shg7_16);
		float L_33 = ((&___rhs)->___shg7_16);
		if ((!(((float)L_32) == ((float)L_33))))
		{
			goto IL_0200;
		}
	}
	{
		float L_34 = ((&___lhs)->___shg8_17);
		float L_35 = ((&___rhs)->___shg8_17);
		if ((!(((float)L_34) == ((float)L_35))))
		{
			goto IL_0200;
		}
	}
	{
		float L_36 = ((&___lhs)->___shb0_18);
		float L_37 = ((&___rhs)->___shb0_18);
		if ((!(((float)L_36) == ((float)L_37))))
		{
			goto IL_0200;
		}
	}
	{
		float L_38 = ((&___lhs)->___shb1_19);
		float L_39 = ((&___rhs)->___shb1_19);
		if ((!(((float)L_38) == ((float)L_39))))
		{
			goto IL_0200;
		}
	}
	{
		float L_40 = ((&___lhs)->___shb2_20);
		float L_41 = ((&___rhs)->___shb2_20);
		if ((!(((float)L_40) == ((float)L_41))))
		{
			goto IL_0200;
		}
	}
	{
		float L_42 = ((&___lhs)->___shb3_21);
		float L_43 = ((&___rhs)->___shb3_21);
		if ((!(((float)L_42) == ((float)L_43))))
		{
			goto IL_0200;
		}
	}
	{
		float L_44 = ((&___lhs)->___shb4_22);
		float L_45 = ((&___rhs)->___shb4_22);
		if ((!(((float)L_44) == ((float)L_45))))
		{
			goto IL_0200;
		}
	}
	{
		float L_46 = ((&___lhs)->___shb5_23);
		float L_47 = ((&___rhs)->___shb5_23);
		if ((!(((float)L_46) == ((float)L_47))))
		{
			goto IL_0200;
		}
	}
	{
		float L_48 = ((&___lhs)->___shb6_24);
		float L_49 = ((&___rhs)->___shb6_24);
		if ((!(((float)L_48) == ((float)L_49))))
		{
			goto IL_0200;
		}
	}
	{
		float L_50 = ((&___lhs)->___shb7_25);
		float L_51 = ((&___rhs)->___shb7_25);
		if ((!(((float)L_50) == ((float)L_51))))
		{
			goto IL_0200;
		}
	}
	{
		float L_52 = ((&___lhs)->___shb8_26);
		float L_53 = ((&___rhs)->___shb8_26);
		G_B28_0 = ((((float)L_52) == ((float)L_53))? 1 : 0);
		goto IL_0201;
	}

IL_0200:
	{
		G_B28_0 = 0;
	}

IL_0201:
	{
		return G_B28_0;
	}
}
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Inequality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C" bool SphericalHarmonicsL2_op_Inequality_m5534 (Object_t * __this /* static, unused */, SphericalHarmonicsL2_t905  ___lhs, SphericalHarmonicsL2_t905  ___rhs, const MethodInfo* method)
{
	{
		SphericalHarmonicsL2_t905  L_0 = ___lhs;
		SphericalHarmonicsL2_t905  L_1 = ___rhs;
		bool L_2 = SphericalHarmonicsL2_op_Equality_m5533(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_SpriteMethodDeclarations.h"



// UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern "C" Rect_t225  Sprite_get_rect_m4737 (Sprite_t668 * __this, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		Sprite_INTERNAL_get_rect_m5535(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t225  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void Sprite_INTERNAL_get_rect_m5535 (Sprite_t668 * __this, Rect_t225 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_rect_m5535_ftn) (Sprite_t668 *, Rect_t225 *);
	static Sprite_INTERNAL_get_rect_m5535_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_rect_m5535_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern "C" float Sprite_get_pixelsPerUnit_m4733 (Sprite_t668 * __this, const MethodInfo* method)
{
	typedef float (*Sprite_get_pixelsPerUnit_m4733_ftn) (Sprite_t668 *);
	static Sprite_get_pixelsPerUnit_m4733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_pixelsPerUnit_m4733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_pixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern "C" Texture2D_t9 * Sprite_get_texture_m4730 (Sprite_t668 * __this, const MethodInfo* method)
{
	typedef Texture2D_t9 * (*Sprite_get_texture_m4730_ftn) (Sprite_t668 *);
	static Sprite_get_texture_m4730_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_get_texture_m4730_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::get_texture()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern "C" Rect_t225  Sprite_get_textureRect_m4750 (Sprite_t668 * __this, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		Sprite_INTERNAL_get_textureRect_m5536(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t225  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
extern "C" void Sprite_INTERNAL_get_textureRect_m5536 (Sprite_t668 * __this, Rect_t225 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_textureRect_m5536_ftn) (Sprite_t668 *, Rect_t225 *);
	static Sprite_INTERNAL_get_textureRect_m5536_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_textureRect_m5536_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern "C" Vector4_t5  Sprite_get_border_m4731 (Sprite_t668 * __this, const MethodInfo* method)
{
	Vector4_t5  V_0 = {0};
	{
		Sprite_INTERNAL_get_border_m5537(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t5  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
extern "C" void Sprite_INTERNAL_get_border_m5537 (Sprite_t668 * __this, Vector4_t5 * ___value, const MethodInfo* method)
{
	typedef void (*Sprite_INTERNAL_get_border_m5537_ftn) (Sprite_t668 *, Vector4_t5 *);
	static Sprite_INTERNAL_get_border_m5537_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Sprite_INTERNAL_get_border_m5537_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRendererMethodDeclarations.h"



#ifdef __clang__
#pragma clang diagnostic pop
#endif
