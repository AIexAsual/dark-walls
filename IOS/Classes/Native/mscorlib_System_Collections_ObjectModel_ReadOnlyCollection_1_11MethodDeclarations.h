﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>
struct ReadOnlyCollection_1_t2432;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.Color>
struct IList_1_t567;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Color[]
struct ColorU5BU5D_t449;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color>
struct IEnumerator_1_t3034;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m16128_gshared (ReadOnlyCollection_1_t2432 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m16128(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2432 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m16128_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16129_gshared (ReadOnlyCollection_1_t2432 * __this, Color_t6  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16129(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2432 *, Color_t6 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m16129_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16130_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16130(__this, method) (( void (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m16130_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16131_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, Color_t6  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16131(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2432 *, int32_t, Color_t6 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m16131_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16132_gshared (ReadOnlyCollection_1_t2432 * __this, Color_t6  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16132(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2432 *, Color_t6 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m16132_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16133_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16133(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2432 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m16133_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" Color_t6  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16134_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16134(__this, ___index, method) (( Color_t6  (*) (ReadOnlyCollection_1_t2432 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m16134_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16135_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, Color_t6  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16135(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2432 *, int32_t, Color_t6 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m16135_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16136_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16136(__this, method) (( bool (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16136_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16137_gshared (ReadOnlyCollection_1_t2432 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16137(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2432 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m16137_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16138_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16138(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m16138_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m16139_gshared (ReadOnlyCollection_1_t2432 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m16139(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2432 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m16139_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m16140_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m16140(__this, method) (( void (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m16140_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m16141_gshared (ReadOnlyCollection_1_t2432 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m16141(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2432 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m16141_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16142_gshared (ReadOnlyCollection_1_t2432 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16142(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2432 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m16142_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m16143_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m16143(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2432 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m16143_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m16144_gshared (ReadOnlyCollection_1_t2432 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m16144(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2432 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m16144_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16145_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16145(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2432 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m16145_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16146_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16146(__this, method) (( bool (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m16146_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16147_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16147(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m16147_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16148_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16148(__this, method) (( bool (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m16148_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16149_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16149(__this, method) (( bool (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m16149_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m16150_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m16150(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2432 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m16150_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m16151_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m16151(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2432 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m16151_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m16152_gshared (ReadOnlyCollection_1_t2432 * __this, Color_t6  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m16152(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2432 *, Color_t6 , const MethodInfo*))ReadOnlyCollection_1_Contains_m16152_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m16153_gshared (ReadOnlyCollection_1_t2432 * __this, ColorU5BU5D_t449* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m16153(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2432 *, ColorU5BU5D_t449*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m16153_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m16154_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m16154(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m16154_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m16155_gshared (ReadOnlyCollection_1_t2432 * __this, Color_t6  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m16155(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2432 *, Color_t6 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m16155_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m16156_gshared (ReadOnlyCollection_1_t2432 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m16156(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2432 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m16156_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Color>::get_Item(System.Int32)
extern "C" Color_t6  ReadOnlyCollection_1_get_Item_m16157_gshared (ReadOnlyCollection_1_t2432 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m16157(__this, ___index, method) (( Color_t6  (*) (ReadOnlyCollection_1_t2432 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m16157_gshared)(__this, ___index, method)
