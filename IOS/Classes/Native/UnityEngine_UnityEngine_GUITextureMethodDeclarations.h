﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUITexture
struct GUITexture_t548;
// UnityEngine.Texture
struct Texture_t221;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// UnityEngine.Color UnityEngine.GUITexture::get_color()
extern "C" Color_t6  GUITexture_get_color_m3301 (GUITexture_t548 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_color(UnityEngine.Color)
extern "C" void GUITexture_set_color_m3305 (GUITexture_t548 * __this, Color_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_get_color_m5126 (GUITexture_t548 * __this, Color_t6 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void GUITexture_INTERNAL_set_color_m5127 (GUITexture_t548 * __this, Color_t6 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
extern "C" void GUITexture_set_texture_m3339 (GUITexture_t548 * __this, Texture_t221 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
