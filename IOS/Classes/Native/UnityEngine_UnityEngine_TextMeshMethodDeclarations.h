﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TextMesh
struct TextMesh_t397;
// System.String
struct String_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C" void TextMesh_set_text_m3201 (TextMesh_t397 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_color(UnityEngine.Color)
extern "C" void TextMesh_set_color_m3236 (TextMesh_t397 * __this, Color_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void TextMesh_INTERNAL_set_color_m5855 (TextMesh_t397 * __this, Color_t6 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
