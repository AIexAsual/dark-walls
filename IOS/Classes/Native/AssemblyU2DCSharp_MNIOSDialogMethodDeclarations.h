﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNIOSDialog
struct MNIOSDialog_t288;
// System.String
struct String_t;

// System.Void MNIOSDialog::.ctor()
extern "C" void MNIOSDialog__ctor_m1779 (MNIOSDialog_t288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSDialog MNIOSDialog::Create(System.String,System.String)
extern "C" MNIOSDialog_t288 * MNIOSDialog_Create_m1780 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSDialog MNIOSDialog::Create(System.String,System.String,System.String,System.String)
extern "C" MNIOSDialog_t288 * MNIOSDialog_Create_m1781 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSDialog::init()
extern "C" void MNIOSDialog_init_m1782 (MNIOSDialog_t288 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSDialog::onPopUpCallBack(System.String)
extern "C" void MNIOSDialog_onPopUpCallBack_m1783 (MNIOSDialog_t288 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
