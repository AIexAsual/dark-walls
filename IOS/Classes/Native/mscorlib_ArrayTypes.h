﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Single[]
// System.Single[]
struct  SingleU5BU5D_t72  : public Array_t
{
};
// System.Object[]
// System.Object[]
struct  ObjectU5BU5D_t470  : public Array_t
{
};
// System.IFormattable[]
// System.IFormattable[]
struct  IFormattableU5BU5D_t3276  : public Array_t
{
};
// System.IConvertible[]
// System.IConvertible[]
struct  IConvertibleU5BU5D_t3277  : public Array_t
{
};
// System.IComparable[]
// System.IComparable[]
struct  IComparableU5BU5D_t3278  : public Array_t
{
};
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct  IComparable_1U5BU5D_t3279  : public Array_t
{
};
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct  IEquatable_1U5BU5D_t3280  : public Array_t
{
};
// System.ValueType[]
// System.ValueType[]
struct  ValueTypeU5BU5D_t3281  : public Array_t
{
};
// System.Type[]
// System.Type[]
struct  TypeU5BU5D_t485  : public Array_t
{
};
struct TypeU5BU5D_t485_StaticFields{
};
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct  IReflectU5BU5D_t3282  : public Array_t
{
};
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct  _TypeU5BU5D_t3283  : public Array_t
{
};
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct  MemberInfoU5BU5D_t1935  : public Array_t
{
};
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct  ICustomAttributeProviderU5BU5D_t3284  : public Array_t
{
};
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct  _MemberInfoU5BU5D_t3285  : public Array_t
{
};
// System.Int32[]
// System.Int32[]
struct  Int32U5BU5D_t269  : public Array_t
{
};
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct  IComparable_1U5BU5D_t3286  : public Array_t
{
};
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct  IEquatable_1U5BU5D_t3287  : public Array_t
{
};
// System.Double[]
// System.Double[]
struct  DoubleU5BU5D_t466  : public Array_t
{
};
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct  IComparable_1U5BU5D_t3288  : public Array_t
{
};
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct  IEquatable_1U5BU5D_t3289  : public Array_t
{
};
// System.Char[]
// System.Char[]
struct  CharU5BU5D_t530  : public Array_t
{
};
struct CharU5BU5D_t530_StaticFields{
};
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct  IComparable_1U5BU5D_t3290  : public Array_t
{
};
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct  IEquatable_1U5BU5D_t3291  : public Array_t
{
};
// System.String[]
// System.String[]
struct  StringU5BU5D_t398  : public Array_t
{
};
struct StringU5BU5D_t398_StaticFields{
};
// System.Double[,]
// System.Double[,]
struct  DoubleU5BU2CU5D_t467  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct  List_1U5BU5D_t2270  : public Array_t
{
};
struct List_1U5BU5D_t2270_StaticFields{
};
// System.Byte[]
// System.Byte[]
struct  ByteU5BU5D_t469  : public Array_t
{
};
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct  IComparable_1U5BU5D_t3292  : public Array_t
{
};
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct  IEquatable_1U5BU5D_t3293  : public Array_t
{
};
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct  LinkU5BU5D_t2315  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Type>[]
struct  KeyValuePair_2U5BU5D_t3005  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3000  : public Array_t
{
};
// System.Enum[]
// System.Enum[]
struct  EnumU5BU5D_t3294  : public Array_t
{
};
struct EnumU5BU5D_t3294_StaticFields{
};
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct  DictionaryEntryU5BU5D_t3295  : public Array_t
{
};
// System.Collections.Generic.Dictionary`2<System.String,System.Type>[]
// System.Collections.Generic.Dictionary`2<System.String,System.Type>[]
struct  Dictionary_2U5BU5D_t2317  : public Array_t
{
};
struct Dictionary_2U5BU5D_t2317_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>[]
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>[]
struct  KeyValuePair_2U5BU5D_t2994  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct  KeyValuePair_2U5BU5D_t2786  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3017  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3014  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct  KeyValuePair_2U5BU5D_t1095  : public Array_t
{
};
// System.Boolean[]
// System.Boolean[]
struct  BooleanU5BU5D_t448  : public Array_t
{
};
struct BooleanU5BU5D_t448_StaticFields{
};
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct  IComparable_1U5BU5D_t3296  : public Array_t
{
};
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct  IEquatable_1U5BU5D_t3297  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>[]
// System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>[]
struct  KeyValuePair_2U5BU5D_t3059  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3078  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct  KeyValuePair_2U5BU5D_t3075  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct  List_1U5BU5D_t2540  : public Array_t
{
};
struct List_1U5BU5D_t2540_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct  KeyValuePair_2U5BU5D_t3096  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct  List_1U5BU5D_t2558  : public Array_t
{
};
struct List_1U5BU5D_t2558_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct  KeyValuePair_2U5BU5D_t3116  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3119  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3091  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct  List_1U5BU5D_t2632  : public Array_t
{
};
struct List_1U5BU5D_t2632_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct  List_1U5BU5D_t2634  : public Array_t
{
};
struct List_1U5BU5D_t2634_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct  KeyValuePair_2U5BU5D_t3153  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct  KeyValuePair_2U5BU5D_t3163  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct  KeyValuePair_2U5BU5D_t3167  : public Array_t
{
};
// System.Byte[][]
// System.Byte[][]
struct  ByteU5BU5DU5BU5D_t1353  : public Array_t
{
};
// System.IntPtr[]
// System.IntPtr[]
struct  IntPtrU5BU5D_t1084  : public Array_t
{
};
struct IntPtrU5BU5D_t1084_StaticFields{
};
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct  ISerializableU5BU5D_t3298  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct  KeyValuePair_2U5BU5D_t3187  : public Array_t
{
};
// System.Int64[]
// System.Int64[]
struct  Int64U5BU5D_t2176  : public Array_t
{
};
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct  IComparable_1U5BU5D_t3299  : public Array_t
{
};
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct  IEquatable_1U5BU5D_t3300  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>[]
struct  KeyValuePair_2U5BU5D_t3184  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3204  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>[]
struct  KeyValuePair_2U5BU5D_t3201  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct  KeyValuePair_2U5BU5D_t3238  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct  KeyValuePair_2U5BU5D_t3211  : public Array_t
{
};
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct  IDictionary_2U5BU5D_t2772  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>[]
struct  KeyValuePair_2U5BU5D_t3217  : public Array_t
{
};
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
struct  IDictionary_2U5BU5D_t2776  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>[]
struct  KeyValuePair_2U5BU5D_t3223  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct  KeyValuePair_2U5BU5D_t3229  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>[]
struct  KeyValuePair_2U5BU5D_t2784  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
struct  KeyValuePair_2U5BU5D_t3234  : public Array_t
{
};
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct  ConstructorInfoU5BU5D_t1152  : public Array_t
{
};
struct ConstructorInfoU5BU5D_t1152_StaticFields{
};
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct  _ConstructorInfoU5BU5D_t3301  : public Array_t
{
};
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct  MethodBaseU5BU5D_t2179  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct  _MethodBaseU5BU5D_t3302  : public Array_t
{
};
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct  ParameterInfoU5BU5D_t1153  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct  _ParameterInfoU5BU5D_t3303  : public Array_t
{
};
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct  PropertyInfoU5BU5D_t1156  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct  _PropertyInfoU5BU5D_t3304  : public Array_t
{
};
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct  FieldInfoU5BU5D_t1157  : public Array_t
{
};
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct  _FieldInfoU5BU5D_t3305  : public Array_t
{
};
// System.Attribute[]
// System.Attribute[]
struct  AttributeU5BU5D_t3306  : public Array_t
{
};
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct  _AttributeU5BU5D_t3307  : public Array_t
{
};
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct  ParameterModifierU5BU5D_t1161  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>[]
struct  KeyValuePair_2U5BU5D_t3250  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct  KeyValuePair_2U5BU5D_t3247  : public Array_t
{
};
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct  KeySizesU5BU5D_t1194  : public Array_t
{
};
// System.UInt32[]
// System.UInt32[]
struct  UInt32U5BU5D_t1182  : public Array_t
{
};
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct  IComparable_1U5BU5D_t3308  : public Array_t
{
};
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct  IEquatable_1U5BU5D_t3309  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct  X509CertificateU5BU5D_t1366  : public Array_t
{
};
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct  IDeserializationCallbackU5BU5D_t3310  : public Array_t
{
};
// System.UInt16[]
// System.UInt16[]
struct  UInt16U5BU5D_t1407  : public Array_t
{
};
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct  IComparable_1U5BU5D_t3311  : public Array_t
{
};
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct  IEquatable_1U5BU5D_t3312  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t3266  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t3263  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t3271  : public Array_t
{
};
// System.Single[,]
// System.Single[,]
struct  SingleU5BU2CU5D_t3313  : public Array_t
{
};
// System.Delegate[]
// System.Delegate[]
struct  DelegateU5BU5D_t2175  : public Array_t
{
};
// System.ICloneable[]
// System.ICloneable[]
struct  ICloneableU5BU5D_t3314  : public Array_t
{
};
// System.UInt64[]
// System.UInt64[]
struct  UInt64U5BU5D_t1992  : public Array_t
{
};
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct  IComparable_1U5BU5D_t3315  : public Array_t
{
};
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct  IEquatable_1U5BU5D_t3316  : public Array_t
{
};
// System.Int16[]
// System.Int16[]
struct  Int16U5BU5D_t2191  : public Array_t
{
};
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct  IComparable_1U5BU5D_t3317  : public Array_t
{
};
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct  IEquatable_1U5BU5D_t3318  : public Array_t
{
};
// System.SByte[]
// System.SByte[]
struct  SByteU5BU5D_t2048  : public Array_t
{
};
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct  IComparable_1U5BU5D_t3319  : public Array_t
{
};
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct  IEquatable_1U5BU5D_t3320  : public Array_t
{
};
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct  MethodInfoU5BU5D_t1796  : public Array_t
{
};
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct  _MethodInfoU5BU5D_t3321  : public Array_t
{
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct  TableRangeU5BU5D_t1581  : public Array_t
{
};
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct  TailoringInfoU5BU5D_t1588  : public Array_t
{
};
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct  ContractionU5BU5D_t1597  : public Array_t
{
};
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct  Level2MapU5BU5D_t1598  : public Array_t
{
};
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct  BigIntegerU5BU5D_t2177  : public Array_t
{
};
struct BigIntegerU5BU5D_t2177_StaticFields{
};
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct  SlotU5BU5D_t1663  : public Array_t
{
};
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct  SlotU5BU5D_t1667  : public Array_t
{
};
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct  StackFrameU5BU5D_t1677  : public Array_t
{
};
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct  CalendarU5BU5D_t1685  : public Array_t
{
};
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct  ModuleBuilderU5BU5D_t1729  : public Array_t
{
};
struct ModuleBuilderU5BU5D_t1729_StaticFields{
};
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct  _ModuleBuilderU5BU5D_t3322  : public Array_t
{
};
// System.Reflection.Module[]
// System.Reflection.Module[]
struct  ModuleU5BU5D_t1730  : public Array_t
{
};
struct ModuleU5BU5D_t1730_StaticFields{
};
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct  _ModuleU5BU5D_t3323  : public Array_t
{
};
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct  ParameterBuilderU5BU5D_t1734  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct  _ParameterBuilderU5BU5D_t3324  : public Array_t
{
};
// System.Type[][]
// System.Type[][]
struct  TypeU5BU5DU5BU5D_t1735  : public Array_t
{
};
struct TypeU5BU5DU5BU5D_t1735_StaticFields{
};
// System.Array[]
// System.Array[]
struct  ArrayU5BU5D_t3325  : public Array_t
{
};
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct  IEnumerableU5BU5D_t3326  : public Array_t
{
};
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct  ICollectionU5BU5D_t3327  : public Array_t
{
};
// System.Collections.IList[]
// System.Collections.IList[]
struct  IListU5BU5D_t3328  : public Array_t
{
};
// System.Reflection.Emit.ILTokenInfo[]
// System.Reflection.Emit.ILTokenInfo[]
struct  ILTokenInfoU5BU5D_t1745  : public Array_t
{
};
// System.Reflection.Emit.ILGenerator/LabelData[]
// System.Reflection.Emit.ILGenerator/LabelData[]
struct  LabelDataU5BU5D_t1746  : public Array_t
{
};
// System.Reflection.Emit.ILGenerator/LabelFixup[]
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct  LabelFixupU5BU5D_t1747  : public Array_t
{
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct  GenericTypeParameterBuilderU5BU5D_t1750  : public Array_t
{
};
// System.Reflection.Emit.TypeBuilder[]
// System.Reflection.Emit.TypeBuilder[]
struct  TypeBuilderU5BU5D_t1752  : public Array_t
{
};
// System.Runtime.InteropServices._TypeBuilder[]
// System.Runtime.InteropServices._TypeBuilder[]
struct  _TypeBuilderU5BU5D_t3329  : public Array_t
{
};
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct  MethodBuilderU5BU5D_t1761  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct  _MethodBuilderU5BU5D_t3330  : public Array_t
{
};
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct  ConstructorBuilderU5BU5D_t1762  : public Array_t
{
};
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct  _ConstructorBuilderU5BU5D_t3331  : public Array_t
{
};
// System.Reflection.Emit.PropertyBuilder[]
// System.Reflection.Emit.PropertyBuilder[]
struct  PropertyBuilderU5BU5D_t1763  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyBuilder[]
// System.Runtime.InteropServices._PropertyBuilder[]
struct  _PropertyBuilderU5BU5D_t3332  : public Array_t
{
};
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct  FieldBuilderU5BU5D_t1764  : public Array_t
{
};
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct  _FieldBuilderU5BU5D_t3333  : public Array_t
{
};
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct  HeaderU5BU5D_t2147  : public Array_t
{
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct  ITrackingHandlerU5BU5D_t2204  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct  IContextAttributeU5BU5D_t2186  : public Array_t
{
};
// System.DateTime[]
// System.DateTime[]
struct  DateTimeU5BU5D_t2206  : public Array_t
{
};
struct DateTimeU5BU5D_t2206_StaticFields{
};
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct  IComparable_1U5BU5D_t3334  : public Array_t
{
};
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct  IEquatable_1U5BU5D_t3335  : public Array_t
{
};
// System.Decimal[]
// System.Decimal[]
struct  DecimalU5BU5D_t2207  : public Array_t
{
};
struct DecimalU5BU5D_t2207_StaticFields{
};
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct  IComparable_1U5BU5D_t3336  : public Array_t
{
};
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct  IEquatable_1U5BU5D_t3337  : public Array_t
{
};
// System.TimeSpan[]
// System.TimeSpan[]
struct  TimeSpanU5BU5D_t2208  : public Array_t
{
};
struct TimeSpanU5BU5D_t2208_StaticFields{
};
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct  IComparable_1U5BU5D_t3338  : public Array_t
{
};
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct  IEquatable_1U5BU5D_t3339  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct  TypeTagU5BU5D_t2209  : public Array_t
{
};
// System.MonoType[]
// System.MonoType[]
struct  MonoTypeU5BU5D_t2212  : public Array_t
{
};
// System.Byte[,]
// System.Byte[,]
struct  ByteU5BU2CU5D_t1967  : public Array_t
{
};
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct  StrongNameU5BU5D_t2951  : public Array_t
{
};
