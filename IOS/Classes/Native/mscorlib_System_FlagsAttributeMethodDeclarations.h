﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.FlagsAttribute
struct FlagsAttribute_t2109;

// System.Void System.FlagsAttribute::.ctor()
extern "C" void FlagsAttribute__ctor_m13026 (FlagsAttribute_t2109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
