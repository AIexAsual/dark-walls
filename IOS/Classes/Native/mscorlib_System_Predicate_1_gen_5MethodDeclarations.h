﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2255;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m13892_gshared (Predicate_1_t2255 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m13892(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2255 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m13892_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m13893_gshared (Predicate_1_t2255 * __this, RaycastResult_t511  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m13893(__this, ___obj, method) (( bool (*) (Predicate_1_t2255 *, RaycastResult_t511 , const MethodInfo*))Predicate_1_Invoke_m13893_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m13894_gshared (Predicate_1_t2255 * __this, RaycastResult_t511  ___obj, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m13894(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2255 *, RaycastResult_t511 , AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m13894_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m13895_gshared (Predicate_1_t2255 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m13895(__this, ___result, method) (( bool (*) (Predicate_1_t2255 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m13895_gshared)(__this, ___result, method)
