﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_t1782;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
extern "C" void AssemblyTitleAttribute__ctor_m10999 (AssemblyTitleAttribute_t1782 * __this, String_t* ___title, const MethodInfo* method) IL2CPP_METHOD_ATTR;
