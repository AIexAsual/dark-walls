﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1542;
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"

// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
extern "C" void AttributeUsageAttribute__ctor_m8575 (AttributeUsageAttribute_t1542 * __this, int32_t ___validOn, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_AllowMultiple()
extern "C" bool AttributeUsageAttribute_get_AllowMultiple_m8576 (AttributeUsageAttribute_t1542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m8577 (AttributeUsageAttribute_t1542 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_Inherited()
extern "C" bool AttributeUsageAttribute_get_Inherited_m8578 (AttributeUsageAttribute_t1542 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
extern "C" void AttributeUsageAttribute_set_Inherited_m8579 (AttributeUsageAttribute_t1542 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
