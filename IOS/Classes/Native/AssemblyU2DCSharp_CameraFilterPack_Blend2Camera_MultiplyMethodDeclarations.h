﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Multiply
struct CameraFilterPack_Blend2Camera_Multiply_t36;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Multiply::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Multiply__ctor_m201 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Multiply::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Multiply_get_material_m202 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::Start()
extern "C" void CameraFilterPack_Blend2Camera_Multiply_Start_m203 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Multiply_OnRenderImage_m204 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Multiply_OnValidate_m205 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::Update()
extern "C" void CameraFilterPack_Blend2Camera_Multiply_Update_m206 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Multiply_OnEnable_m207 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Multiply::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Multiply_OnDisable_m208 (CameraFilterPack_Blend2Camera_Multiply_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
