﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RagdollScript
struct RagdollScript_t405;

// System.Void RagdollScript::.ctor()
extern "C" void RagdollScript__ctor_m2310 (RagdollScript_t405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollScript::SetKinematic(System.Boolean)
extern "C" void RagdollScript_SetKinematic_m2311 (RagdollScript_t405 * __this, bool ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollScript::Start()
extern "C" void RagdollScript_Start_m2312 (RagdollScript_t405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollScript::Update()
extern "C" void RagdollScript_Update_m2313 (RagdollScript_t405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollScript::ragdollMode()
extern "C" void RagdollScript_ragdollMode_m2314 (RagdollScript_t405 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
