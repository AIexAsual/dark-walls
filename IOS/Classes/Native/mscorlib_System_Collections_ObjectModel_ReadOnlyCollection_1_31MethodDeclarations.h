﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>
struct ReadOnlyCollection_1_t2626;
// UnityEngine.RectTransform
struct RectTransform_t648;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.RectTransform>
struct IList_1_t2625;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t2624;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform>
struct IEnumerator_1_t3140;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m19198(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2626 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m13592_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19199(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2626 *, RectTransform_t648 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m13593_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19200(__this, method) (( void (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m13594_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19201(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2626 *, int32_t, RectTransform_t648 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m13595_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19202(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2626 *, RectTransform_t648 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m13596_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19203(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2626 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m13597_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19204(__this, ___index, method) (( RectTransform_t648 * (*) (ReadOnlyCollection_1_t2626 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m13598_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19205(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2626 *, int32_t, RectTransform_t648 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m13599_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19206(__this, method) (( bool (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13600_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19207(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2626 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m13601_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19208(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m13602_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m19209(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2626 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m13603_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m19210(__this, method) (( void (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m13604_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m19211(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2626 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m13605_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19212(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2626 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m13606_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m19213(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2626 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m13607_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m19214(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2626 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m13608_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19215(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2626 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m13609_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19216(__this, method) (( bool (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m13610_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19217(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m13611_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19218(__this, method) (( bool (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m13612_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19219(__this, method) (( bool (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m13613_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m19220(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2626 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m13614_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m19221(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2626 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m13615_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::Contains(T)
#define ReadOnlyCollection_1_Contains_m19222(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2626 *, RectTransform_t648 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m13616_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m19223(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2626 *, RectTransformU5BU5D_t2624*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m13617_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m19224(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m13618_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m19225(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2626 *, RectTransform_t648 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m13619_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::get_Count()
#define ReadOnlyCollection_1_get_Count_m19226(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2626 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m13620_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m19227(__this, ___index, method) (( RectTransform_t648 * (*) (ReadOnlyCollection_1_t2626 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m13621_gshared)(__this, ___index, method)
