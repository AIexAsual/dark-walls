﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Saturation
struct CameraFilterPack_Blend2Camera_Saturation_t41;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Saturation::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Saturation__ctor_m234 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Saturation::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Saturation_get_material_m235 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::Start()
extern "C" void CameraFilterPack_Blend2Camera_Saturation_Start_m236 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Saturation_OnRenderImage_m237 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Saturation_OnValidate_m238 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::Update()
extern "C" void CameraFilterPack_Blend2Camera_Saturation_Update_m239 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Saturation_OnEnable_m240 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Saturation::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Saturation_OnDisable_m241 (CameraFilterPack_Blend2Camera_Saturation_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
