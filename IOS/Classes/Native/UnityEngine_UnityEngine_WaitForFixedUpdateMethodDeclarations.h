﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t837;

// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m4983 (WaitForFixedUpdate_t837 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
