﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct ObjectPool_1_t753;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t754;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.UI.ComponentListPool
struct  ComponentListPool_t755  : public Object_t
{
};
struct ComponentListPool_t755_StaticFields{
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>> UnityEngine.UI.ComponentListPool::s_ComponentListPool
	ObjectPool_1_t753 * ___s_ComponentListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>> UnityEngine.UI.ComponentListPool::<>f__am$cache1
	UnityAction_1_t754 * ___U3CU3Ef__amU24cache1_1;
};
