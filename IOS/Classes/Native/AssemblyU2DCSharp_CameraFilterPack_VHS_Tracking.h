﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_VHS_Tracking
struct  CameraFilterPack_VHS_Tracking_t204  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_VHS_Tracking::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_VHS_Tracking::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_VHS_Tracking::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_VHS_Tracking::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_VHS_Tracking::Tracking
	float ___Tracking_6;
};
struct CameraFilterPack_VHS_Tracking_t204_StaticFields{
	// System.Single CameraFilterPack_VHS_Tracking::ChangeTracking
	float ___ChangeTracking_7;
};
