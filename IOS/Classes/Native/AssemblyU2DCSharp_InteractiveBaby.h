﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t307;
// UnityEngine.Animator
struct Animator_t321;
// AICharacterControl
struct AICharacterControl_t308;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t323;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InteractiveBaby
struct  InteractiveBaby_t324  : public MonoBehaviour_t4
{
	// UnityEngine.Transform InteractiveBaby::targetPos
	Transform_t243 * ___targetPos_2;
	// UnityEngine.Transform InteractiveBaby::origPos
	Transform_t243 * ___origPos_3;
	// UnityEngine.NavMeshAgent InteractiveBaby::navMeshAgent
	NavMeshAgent_t307 * ___navMeshAgent_4;
	// UnityEngine.Animator InteractiveBaby::anim
	Animator_t321 * ___anim_5;
	// System.Boolean InteractiveBaby::goToPos
	bool ___goToPos_6;
	// AICharacterControl InteractiveBaby::aiControl
	AICharacterControl_t308 * ___aiControl_7;
	// UnityEngine.GameObject[] InteractiveBaby::baby_skin
	GameObjectU5BU5D_t323* ___baby_skin_8;
};
