﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Metadata Definition System.Collections.IDictionary
extern TypeInfo IDictionary_t1462_il2cpp_TypeInfo;
extern const Il2CppType IEnumerable_t1097_0_0_0;
extern const Il2CppType ICollection_t1528_0_0_0;
static const Il2CppType* IDictionary_t1462_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDictionary_t1462_0_0_0;
extern const Il2CppType IDictionary_t1462_1_0_0;
struct IDictionary_t1462;
const Il2CppTypeDefinitionMetadata IDictionary_t1462_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDictionary_t1462_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10295/* methodStart */
	, -1/* eventStart */
	, 1958/* propertyStart */

};
TypeInfo IDictionary_t1462_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDictionary"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IDictionary_t1462_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2711/* custom_attributes_cache */
	, &IDictionary_t1462_0_0_0/* byval_arg */
	, &IDictionary_t1462_1_0_0/* this_arg */
	, &IDictionary_t1462_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IDictionaryEnumerator
extern TypeInfo IDictionaryEnumerator_t1527_il2cpp_TypeInfo;
extern const Il2CppType IEnumerator_t464_0_0_0;
static const Il2CppType* IDictionaryEnumerator_t1527_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IDictionaryEnumerator_t1527_0_0_0;
extern const Il2CppType IDictionaryEnumerator_t1527_1_0_0;
struct IDictionaryEnumerator_t1527;
const Il2CppTypeDefinitionMetadata IDictionaryEnumerator_t1527_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IDictionaryEnumerator_t1527_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10301/* methodStart */
	, -1/* eventStart */
	, 1959/* propertyStart */

};
TypeInfo IDictionaryEnumerator_t1527_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDictionaryEnumerator"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IDictionaryEnumerator_t1527_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2712/* custom_attributes_cache */
	, &IDictionaryEnumerator_t1527_0_0_0/* byval_arg */
	, &IDictionaryEnumerator_t1527_1_0_0/* this_arg */
	, &IDictionaryEnumerator_t1527_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IEqualityComparer
extern TypeInfo IEqualityComparer_t1387_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IEqualityComparer_t1387_0_0_0;
extern const Il2CppType IEqualityComparer_t1387_1_0_0;
struct IEqualityComparer_t1387;
const Il2CppTypeDefinitionMetadata IEqualityComparer_t1387_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10304/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IEqualityComparer_t1387_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEqualityComparer"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IEqualityComparer_t1387_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2713/* custom_attributes_cache */
	, &IEqualityComparer_t1387_0_0_0/* byval_arg */
	, &IEqualityComparer_t1387_1_0_0/* this_arg */
	, &IEqualityComparer_t1387_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.IHashCodeProvider
extern TypeInfo IHashCodeProvider_t1386_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IHashCodeProvider_t1386_0_0_0;
extern const Il2CppType IHashCodeProvider_t1386_1_0_0;
struct IHashCodeProvider_t1386;
const Il2CppTypeDefinitionMetadata IHashCodeProvider_t1386_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10306/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IHashCodeProvider_t1386_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IHashCodeProvider"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &IHashCodeProvider_t1386_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2714/* custom_attributes_cache */
	, &IHashCodeProvider_t1386_0_0_0/* byval_arg */
	, &IHashCodeProvider_t1386_1_0_0/* this_arg */
	, &IHashCodeProvider_t1386_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.SortedList
#include "mscorlib_System_Collections_SortedList.h"
// Metadata Definition System.Collections.SortedList
extern TypeInfo SortedList_t1534_il2cpp_TypeInfo;
// System.Collections.SortedList
#include "mscorlib_System_Collections_SortedListMethodDeclarations.h"
extern const Il2CppType Slot_t1664_0_0_0;
extern const Il2CppType EnumeratorMode_t1665_0_0_0;
extern const Il2CppType Enumerator_t1666_0_0_0;
static const Il2CppType* SortedList_t1534_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Slot_t1664_0_0_0,
	&EnumeratorMode_t1665_0_0_0,
	&Enumerator_t1666_0_0_0,
};
static const EncodedMethodIndex SortedList_t1534_VTable[33] = 
{
	626,
	601,
	627,
	628,
	3121,
	3122,
	3123,
	3124,
	3125,
	3126,
	3127,
	3128,
	3129,
	3130,
	3131,
	3122,
	3123,
	3124,
	3132,
	3133,
	3126,
	3127,
	3134,
	3135,
	3128,
	3129,
	3130,
	3131,
	3125,
	3136,
	3137,
	3138,
	3139,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
static const Il2CppType* SortedList_t1534_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICloneable_t3433_0_0_0,
	&ICollection_t1528_0_0_0,
	&IDictionary_t1462_0_0_0,
};
static Il2CppInterfaceOffsetPair SortedList_t1534_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IDictionary_t1462_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SortedList_t1534_0_0_0;
extern const Il2CppType SortedList_t1534_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SortedList_t1534;
const Il2CppTypeDefinitionMetadata SortedList_t1534_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SortedList_t1534_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, SortedList_t1534_InterfacesTypeInfos/* implementedInterfaces */
	, SortedList_t1534_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SortedList_t1534_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6942/* fieldStart */
	, 10307/* methodStart */
	, -1/* eventStart */
	, 1962/* propertyStart */

};
TypeInfo SortedList_t1534_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SortedList"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &SortedList_t1534_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2715/* custom_attributes_cache */
	, &SortedList_t1534_0_0_0/* byval_arg */
	, &SortedList_t1534_1_0_0/* this_arg */
	, &SortedList_t1534_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SortedList_t1534)/* instance_size */
	, sizeof (SortedList_t1534)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SortedList_t1534_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 7/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 33/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
// Metadata Definition System.Collections.SortedList/Slot
extern TypeInfo Slot_t1664_il2cpp_TypeInfo;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_SlotMethodDeclarations.h"
static const EncodedMethodIndex Slot_t1664_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Slot_t1664_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata Slot_t1664_DefinitionMetadata = 
{
	&SortedList_t1534_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Slot_t1664_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6948/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Slot_t1664_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slot"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Slot_t1664_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Slot_t1664_0_0_0/* byval_arg */
	, &Slot_t1664_1_0_0/* this_arg */
	, &Slot_t1664_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slot_t1664)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Slot_t1664)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057037/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorMode.h"
// Metadata Definition System.Collections.SortedList/EnumeratorMode
extern TypeInfo EnumeratorMode_t1665_il2cpp_TypeInfo;
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorModeMethodDeclarations.h"
static const EncodedMethodIndex EnumeratorMode_t1665_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair EnumeratorMode_t1665_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnumeratorMode_t1665_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata EnumeratorMode_t1665_DefinitionMetadata = 
{
	&SortedList_t1534_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EnumeratorMode_t1665_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EnumeratorMode_t1665_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6950/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EnumeratorMode_t1665_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnumeratorMode"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EnumeratorMode_t1665_0_0_0/* byval_arg */
	, &EnumeratorMode_t1665_1_0_0/* this_arg */
	, &EnumeratorMode_t1665_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnumeratorMode_t1665)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EnumeratorMode_t1665)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.SortedList/Enumerator
#include "mscorlib_System_Collections_SortedList_Enumerator.h"
// Metadata Definition System.Collections.SortedList/Enumerator
extern TypeInfo Enumerator_t1666_il2cpp_TypeInfo;
// System.Collections.SortedList/Enumerator
#include "mscorlib_System_Collections_SortedList_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t1666_VTable[10] = 
{
	626,
	601,
	627,
	628,
	3140,
	3141,
	3142,
	3143,
	3144,
	3145,
};
static const Il2CppType* Enumerator_t1666_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&ICloneable_t3433_0_0_0,
	&IDictionaryEnumerator_t1527_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1666_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 6},
	{ &IDictionaryEnumerator_t1527_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t1666_1_0_0;
struct Enumerator_t1666;
const Il2CppTypeDefinitionMetadata Enumerator_t1666_DefinitionMetadata = 
{
	&SortedList_t1534_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1666_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1666_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t1666_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6954/* fieldStart */
	, 10335/* methodStart */
	, -1/* eventStart */
	, 1969/* propertyStart */

};
TypeInfo Enumerator_t1666_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t1666_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1666_0_0_0/* byval_arg */
	, &Enumerator_t1666_1_0_0/* this_arg */
	, &Enumerator_t1666_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t1666)/* instance_size */
	, sizeof (Enumerator_t1666)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Enumerator_t1666_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Stack
#include "mscorlib_System_Collections_Stack.h"
// Metadata Definition System.Collections.Stack
extern TypeInfo Stack_t1076_il2cpp_TypeInfo;
// System.Collections.Stack
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
extern const Il2CppType Enumerator_t1668_0_0_0;
static const Il2CppType* Stack_t1076_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Enumerator_t1668_0_0_0,
};
static const EncodedMethodIndex Stack_t1076_VTable[18] = 
{
	626,
	601,
	627,
	628,
	1576,
	1577,
	1578,
	1579,
	1580,
	1577,
	1578,
	1579,
	1581,
	1580,
	1576,
	1582,
	1583,
	1584,
};
static const Il2CppType* Stack_t1076_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICloneable_t3433_0_0_0,
	&ICollection_t1528_0_0_0,
};
static Il2CppInterfaceOffsetPair Stack_t1076_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 5},
	{ &ICollection_t1528_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Stack_t1076_0_0_0;
extern const Il2CppType Stack_t1076_1_0_0;
struct Stack_t1076;
const Il2CppTypeDefinitionMetadata Stack_t1076_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Stack_t1076_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Stack_t1076_InterfacesTypeInfos/* implementedInterfaces */
	, Stack_t1076_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Stack_t1076_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6963/* fieldStart */
	, 10343/* methodStart */
	, -1/* eventStart */
	, 1973/* propertyStart */

};
TypeInfo Stack_t1076_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stack"/* name */
	, "System.Collections"/* namespaze */
	, NULL/* methods */
	, &Stack_t1076_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2716/* custom_attributes_cache */
	, &Stack_t1076_0_0_0/* byval_arg */
	, &Stack_t1076_1_0_0/* this_arg */
	, &Stack_t1076_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Stack_t1076)/* instance_size */
	, sizeof (Stack_t1076)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 18/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Collections.Stack/Enumerator
#include "mscorlib_System_Collections_Stack_Enumerator.h"
// Metadata Definition System.Collections.Stack/Enumerator
extern TypeInfo Enumerator_t1668_il2cpp_TypeInfo;
// System.Collections.Stack/Enumerator
#include "mscorlib_System_Collections_Stack_EnumeratorMethodDeclarations.h"
static const EncodedMethodIndex Enumerator_t1668_VTable[8] = 
{
	626,
	601,
	627,
	628,
	3146,
	3147,
	3146,
	3147,
};
static const Il2CppType* Enumerator_t1668_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1668_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &ICloneable_t3433_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Enumerator_t1668_1_0_0;
struct Enumerator_t1668;
const Il2CppTypeDefinitionMetadata Enumerator_t1668_DefinitionMetadata = 
{
	&Stack_t1076_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1668_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1668_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerator_t1668_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6968/* fieldStart */
	, 10354/* methodStart */
	, -1/* eventStart */
	, 1976/* propertyStart */

};
TypeInfo Enumerator_t1668_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Enumerator_t1668_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1668_0_0_0/* byval_arg */
	, &Enumerator_t1668_1_0_0/* this_arg */
	, &Enumerator_t1668_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t1668)/* instance_size */
	, sizeof (Enumerator_t1668)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Configuration.Assemblies.AssemblyHashAlgorithm
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgorit.h"
// Metadata Definition System.Configuration.Assemblies.AssemblyHashAlgorithm
extern TypeInfo AssemblyHashAlgorithm_t1669_il2cpp_TypeInfo;
// System.Configuration.Assemblies.AssemblyHashAlgorithm
#include "mscorlib_System_Configuration_Assemblies_AssemblyHashAlgoritMethodDeclarations.h"
static const EncodedMethodIndex AssemblyHashAlgorithm_t1669_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AssemblyHashAlgorithm_t1669_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyHashAlgorithm_t1669_0_0_0;
extern const Il2CppType AssemblyHashAlgorithm_t1669_1_0_0;
const Il2CppTypeDefinitionMetadata AssemblyHashAlgorithm_t1669_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyHashAlgorithm_t1669_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AssemblyHashAlgorithm_t1669_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6971/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyHashAlgorithm_t1669_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyHashAlgorithm"/* name */
	, "System.Configuration.Assemblies"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2717/* custom_attributes_cache */
	, &AssemblyHashAlgorithm_t1669_0_0_0/* byval_arg */
	, &AssemblyHashAlgorithm_t1669_1_0_0/* this_arg */
	, &AssemblyHashAlgorithm_t1669_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyHashAlgorithm_t1669)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AssemblyHashAlgorithm_t1669)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Configuration.Assemblies.AssemblyVersionCompatibility
#include "mscorlib_System_Configuration_Assemblies_AssemblyVersionComp.h"
// Metadata Definition System.Configuration.Assemblies.AssemblyVersionCompatibility
extern TypeInfo AssemblyVersionCompatibility_t1670_il2cpp_TypeInfo;
// System.Configuration.Assemblies.AssemblyVersionCompatibility
#include "mscorlib_System_Configuration_Assemblies_AssemblyVersionCompMethodDeclarations.h"
static const EncodedMethodIndex AssemblyVersionCompatibility_t1670_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AssemblyVersionCompatibility_t1670_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyVersionCompatibility_t1670_0_0_0;
extern const Il2CppType AssemblyVersionCompatibility_t1670_1_0_0;
const Il2CppTypeDefinitionMetadata AssemblyVersionCompatibility_t1670_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyVersionCompatibility_t1670_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AssemblyVersionCompatibility_t1670_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6975/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyVersionCompatibility_t1670_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyVersionCompatibility"/* name */
	, "System.Configuration.Assemblies"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2718/* custom_attributes_cache */
	, &AssemblyVersionCompatibility_t1670_0_0_0/* byval_arg */
	, &AssemblyVersionCompatibility_t1670_1_0_0/* this_arg */
	, &AssemblyVersionCompatibility_t1670_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyVersionCompatibility_t1670)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AssemblyVersionCompatibility_t1670)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttr.h"
// Metadata Definition System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
extern TypeInfo SuppressMessageAttribute_t1671_il2cpp_TypeInfo;
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttrMethodDeclarations.h"
static const EncodedMethodIndex SuppressMessageAttribute_t1671_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair SuppressMessageAttribute_t1671_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SuppressMessageAttribute_t1671_0_0_0;
extern const Il2CppType SuppressMessageAttribute_t1671_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct SuppressMessageAttribute_t1671;
const Il2CppTypeDefinitionMetadata SuppressMessageAttribute_t1671_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SuppressMessageAttribute_t1671_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SuppressMessageAttribute_t1671_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6979/* fieldStart */
	, 10357/* methodStart */
	, -1/* eventStart */
	, 1977/* propertyStart */

};
TypeInfo SuppressMessageAttribute_t1671_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SuppressMessageAttribute"/* name */
	, "System.Diagnostics.CodeAnalysis"/* namespaze */
	, NULL/* methods */
	, &SuppressMessageAttribute_t1671_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2719/* custom_attributes_cache */
	, &SuppressMessageAttribute_t1671_0_0_0/* byval_arg */
	, &SuppressMessageAttribute_t1671_1_0_0/* this_arg */
	, &SuppressMessageAttribute_t1671_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SuppressMessageAttribute_t1671)/* instance_size */
	, sizeof (SuppressMessageAttribute_t1671)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// Metadata Definition System.Diagnostics.DebuggableAttribute
extern TypeInfo DebuggableAttribute_t1673_il2cpp_TypeInfo;
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
extern const Il2CppType DebuggingModes_t1672_0_0_0;
static const Il2CppType* DebuggableAttribute_t1673_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DebuggingModes_t1672_0_0_0,
};
static const EncodedMethodIndex DebuggableAttribute_t1673_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DebuggableAttribute_t1673_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggableAttribute_t1673_0_0_0;
extern const Il2CppType DebuggableAttribute_t1673_1_0_0;
struct DebuggableAttribute_t1673;
const Il2CppTypeDefinitionMetadata DebuggableAttribute_t1673_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DebuggableAttribute_t1673_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggableAttribute_t1673_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DebuggableAttribute_t1673_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6982/* fieldStart */
	, 10359/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggableAttribute_t1673_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggableAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggableAttribute_t1673_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2720/* custom_attributes_cache */
	, &DebuggableAttribute_t1673_0_0_0/* byval_arg */
	, &DebuggableAttribute_t1673_1_0_0/* this_arg */
	, &DebuggableAttribute_t1673_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggableAttribute_t1673)/* instance_size */
	, sizeof (DebuggableAttribute_t1673)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggableAttribute/DebuggingModes
#include "mscorlib_System_Diagnostics_DebuggableAttribute_DebuggingMod.h"
// Metadata Definition System.Diagnostics.DebuggableAttribute/DebuggingModes
extern TypeInfo DebuggingModes_t1672_il2cpp_TypeInfo;
// System.Diagnostics.DebuggableAttribute/DebuggingModes
#include "mscorlib_System_Diagnostics_DebuggableAttribute_DebuggingModMethodDeclarations.h"
static const EncodedMethodIndex DebuggingModes_t1672_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DebuggingModes_t1672_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggingModes_t1672_1_0_0;
const Il2CppTypeDefinitionMetadata DebuggingModes_t1672_DefinitionMetadata = 
{
	&DebuggableAttribute_t1673_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggingModes_t1672_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DebuggingModes_t1672_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6985/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggingModes_t1672_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggingModes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2721/* custom_attributes_cache */
	, &DebuggingModes_t1672_0_0_0/* byval_arg */
	, &DebuggingModes_t1672_1_0_0/* this_arg */
	, &DebuggingModes_t1672_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggingModes_t1672)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DebuggingModes_t1672)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerDisplayAttribute
extern TypeInfo DebuggerDisplayAttribute_t1674_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerDisplayAttribute_t1674_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DebuggerDisplayAttribute_t1674_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerDisplayAttribute_t1674_0_0_0;
extern const Il2CppType DebuggerDisplayAttribute_t1674_1_0_0;
struct DebuggerDisplayAttribute_t1674;
const Il2CppTypeDefinitionMetadata DebuggerDisplayAttribute_t1674_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerDisplayAttribute_t1674_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DebuggerDisplayAttribute_t1674_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6991/* fieldStart */
	, 10360/* methodStart */
	, -1/* eventStart */
	, 1978/* propertyStart */

};
TypeInfo DebuggerDisplayAttribute_t1674_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerDisplayAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerDisplayAttribute_t1674_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2722/* custom_attributes_cache */
	, &DebuggerDisplayAttribute_t1674_0_0_0/* byval_arg */
	, &DebuggerDisplayAttribute_t1674_1_0_0/* this_arg */
	, &DebuggerDisplayAttribute_t1674_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerDisplayAttribute_t1674)/* instance_size */
	, sizeof (DebuggerDisplayAttribute_t1674)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerStepThroughAttribute
extern TypeInfo DebuggerStepThroughAttribute_t1675_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerStepThroughAttribute_t1675_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DebuggerStepThroughAttribute_t1675_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerStepThroughAttribute_t1675_0_0_0;
extern const Il2CppType DebuggerStepThroughAttribute_t1675_1_0_0;
struct DebuggerStepThroughAttribute_t1675;
const Il2CppTypeDefinitionMetadata DebuggerStepThroughAttribute_t1675_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerStepThroughAttribute_t1675_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DebuggerStepThroughAttribute_t1675_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10362/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggerStepThroughAttribute_t1675_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerStepThroughAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerStepThroughAttribute_t1675_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2723/* custom_attributes_cache */
	, &DebuggerStepThroughAttribute_t1675_0_0_0/* byval_arg */
	, &DebuggerStepThroughAttribute_t1675_1_0_0/* this_arg */
	, &DebuggerStepThroughAttribute_t1675_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerStepThroughAttribute_t1675)/* instance_size */
	, sizeof (DebuggerStepThroughAttribute_t1675)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"
// Metadata Definition System.Diagnostics.DebuggerTypeProxyAttribute
extern TypeInfo DebuggerTypeProxyAttribute_t1676_il2cpp_TypeInfo;
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttributeMethodDeclarations.h"
static const EncodedMethodIndex DebuggerTypeProxyAttribute_t1676_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DebuggerTypeProxyAttribute_t1676_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DebuggerTypeProxyAttribute_t1676_0_0_0;
extern const Il2CppType DebuggerTypeProxyAttribute_t1676_1_0_0;
struct DebuggerTypeProxyAttribute_t1676;
const Il2CppTypeDefinitionMetadata DebuggerTypeProxyAttribute_t1676_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DebuggerTypeProxyAttribute_t1676_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DebuggerTypeProxyAttribute_t1676_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6994/* fieldStart */
	, 10363/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DebuggerTypeProxyAttribute_t1676_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DebuggerTypeProxyAttribute"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &DebuggerTypeProxyAttribute_t1676_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2724/* custom_attributes_cache */
	, &DebuggerTypeProxyAttribute_t1676_0_0_0/* byval_arg */
	, &DebuggerTypeProxyAttribute_t1676_1_0_0/* this_arg */
	, &DebuggerTypeProxyAttribute_t1676_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DebuggerTypeProxyAttribute_t1676)/* instance_size */
	, sizeof (DebuggerTypeProxyAttribute_t1676)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"
// Metadata Definition System.Diagnostics.StackFrame
extern TypeInfo StackFrame_t1162_il2cpp_TypeInfo;
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
static const EncodedMethodIndex StackFrame_t1162_VTable[9] = 
{
	626,
	601,
	627,
	3148,
	3149,
	3150,
	3151,
	3152,
	3153,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StackFrame_t1162_0_0_0;
extern const Il2CppType StackFrame_t1162_1_0_0;
struct StackFrame_t1162;
const Il2CppTypeDefinitionMetadata StackFrame_t1162_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackFrame_t1162_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 6995/* fieldStart */
	, 10364/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StackFrame_t1162_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackFrame"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &StackFrame_t1162_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2725/* custom_attributes_cache */
	, &StackFrame_t1162_0_0_0/* byval_arg */
	, &StackFrame_t1162_1_0_0/* this_arg */
	, &StackFrame_t1162_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackFrame_t1162)/* instance_size */
	, sizeof (StackFrame_t1162)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTrace.h"
// Metadata Definition System.Diagnostics.StackTrace
extern TypeInfo StackTrace_t1103_il2cpp_TypeInfo;
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
static const EncodedMethodIndex StackTrace_t1103_VTable[6] = 
{
	626,
	601,
	627,
	3154,
	3155,
	3156,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StackTrace_t1103_0_0_0;
extern const Il2CppType StackTrace_t1103_1_0_0;
struct StackTrace_t1103;
const Il2CppTypeDefinitionMetadata StackTrace_t1103_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTrace_t1103_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7003/* fieldStart */
	, 10375/* methodStart */
	, -1/* eventStart */
	, 1979/* propertyStart */

};
TypeInfo StackTrace_t1103_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTrace"/* name */
	, "System.Diagnostics"/* namespaze */
	, NULL/* methods */
	, &StackTrace_t1103_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2726/* custom_attributes_cache */
	, &StackTrace_t1103_0_0_0/* byval_arg */
	, &StackTrace_t1103_1_0_0/* this_arg */
	, &StackTrace_t1103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTrace_t1103)/* instance_size */
	, sizeof (StackTrace_t1103)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.Calendar
#include "mscorlib_System_Globalization_Calendar.h"
// Metadata Definition System.Globalization.Calendar
extern TypeInfo Calendar_t1678_il2cpp_TypeInfo;
// System.Globalization.Calendar
#include "mscorlib_System_Globalization_CalendarMethodDeclarations.h"
static const EncodedMethodIndex Calendar_t1678_VTable[10] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
	0,
	0,
	0,
};
static const Il2CppType* Calendar_t1678_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair Calendar_t1678_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Calendar_t1678_0_0_0;
extern const Il2CppType Calendar_t1678_1_0_0;
struct Calendar_t1678;
const Il2CppTypeDefinitionMetadata Calendar_t1678_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Calendar_t1678_InterfacesTypeInfos/* implementedInterfaces */
	, Calendar_t1678_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Calendar_t1678_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7006/* fieldStart */
	, 10385/* methodStart */
	, -1/* eventStart */
	, 1980/* propertyStart */

};
TypeInfo Calendar_t1678_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Calendar"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Calendar_t1678_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2727/* custom_attributes_cache */
	, &Calendar_t1678_0_0_0/* byval_arg */
	, &Calendar_t1678_1_0_0/* this_arg */
	, &Calendar_t1678_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Calendar_t1678)/* instance_size */
	, sizeof (Calendar_t1678)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Globalization.CCMath
#include "mscorlib_System_Globalization_CCMath.h"
// Metadata Definition System.Globalization.CCMath
extern TypeInfo CCMath_t1679_il2cpp_TypeInfo;
// System.Globalization.CCMath
#include "mscorlib_System_Globalization_CCMathMethodDeclarations.h"
static const EncodedMethodIndex CCMath_t1679_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CCMath_t1679_0_0_0;
extern const Il2CppType CCMath_t1679_1_0_0;
struct CCMath_t1679;
const Il2CppTypeDefinitionMetadata CCMath_t1679_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CCMath_t1679_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10394/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CCMath_t1679_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CCMath"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CCMath_t1679_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CCMath_t1679_0_0_0/* byval_arg */
	, &CCMath_t1679_1_0_0/* this_arg */
	, &CCMath_t1679_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CCMath_t1679)/* instance_size */
	, sizeof (CCMath_t1679)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.CCFixed
#include "mscorlib_System_Globalization_CCFixed.h"
// Metadata Definition System.Globalization.CCFixed
extern TypeInfo CCFixed_t1680_il2cpp_TypeInfo;
// System.Globalization.CCFixed
#include "mscorlib_System_Globalization_CCFixedMethodDeclarations.h"
static const EncodedMethodIndex CCFixed_t1680_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CCFixed_t1680_0_0_0;
extern const Il2CppType CCFixed_t1680_1_0_0;
struct CCFixed_t1680;
const Il2CppTypeDefinitionMetadata CCFixed_t1680_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CCFixed_t1680_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10397/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CCFixed_t1680_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CCFixed"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CCFixed_t1680_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CCFixed_t1680_0_0_0/* byval_arg */
	, &CCFixed_t1680_1_0_0/* this_arg */
	, &CCFixed_t1680_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CCFixed_t1680)/* instance_size */
	, sizeof (CCFixed_t1680)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.CCGregorianCalendar
#include "mscorlib_System_Globalization_CCGregorianCalendar.h"
// Metadata Definition System.Globalization.CCGregorianCalendar
extern TypeInfo CCGregorianCalendar_t1681_il2cpp_TypeInfo;
// System.Globalization.CCGregorianCalendar
#include "mscorlib_System_Globalization_CCGregorianCalendarMethodDeclarations.h"
static const EncodedMethodIndex CCGregorianCalendar_t1681_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CCGregorianCalendar_t1681_0_0_0;
extern const Il2CppType CCGregorianCalendar_t1681_1_0_0;
struct CCGregorianCalendar_t1681;
const Il2CppTypeDefinitionMetadata CCGregorianCalendar_t1681_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CCGregorianCalendar_t1681_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10399/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CCGregorianCalendar_t1681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CCGregorianCalendar"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CCGregorianCalendar_t1681_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CCGregorianCalendar_t1681_0_0_0/* byval_arg */
	, &CCGregorianCalendar_t1681_1_0_0/* this_arg */
	, &CCGregorianCalendar_t1681_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CCGregorianCalendar_t1681)/* instance_size */
	, sizeof (CCGregorianCalendar_t1681)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.CompareInfo
#include "mscorlib_System_Globalization_CompareInfo.h"
// Metadata Definition System.Globalization.CompareInfo
extern TypeInfo CompareInfo_t1358_il2cpp_TypeInfo;
// System.Globalization.CompareInfo
#include "mscorlib_System_Globalization_CompareInfoMethodDeclarations.h"
static const EncodedMethodIndex CompareInfo_t1358_VTable[15] = 
{
	3157,
	3158,
	3159,
	3160,
	3161,
	3162,
	3163,
	3164,
	3165,
	3166,
	3167,
	3168,
	3169,
	3170,
	3171,
};
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
static const Il2CppType* CompareInfo_t1358_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair CompareInfo_t1358_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t2213_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompareInfo_t1358_0_0_0;
extern const Il2CppType CompareInfo_t1358_1_0_0;
struct CompareInfo_t1358;
const Il2CppTypeDefinitionMetadata CompareInfo_t1358_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CompareInfo_t1358_InterfacesTypeInfos/* implementedInterfaces */
	, CompareInfo_t1358_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CompareInfo_t1358_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7010/* fieldStart */
	, 10409/* methodStart */
	, -1/* eventStart */
	, 1982/* propertyStart */

};
TypeInfo CompareInfo_t1358_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompareInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CompareInfo_t1358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2728/* custom_attributes_cache */
	, &CompareInfo_t1358_0_0_0/* byval_arg */
	, &CompareInfo_t1358_1_0_0/* this_arg */
	, &CompareInfo_t1358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompareInfo_t1358)/* instance_size */
	, sizeof (CompareInfo_t1358)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CompareInfo_t1358_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 29/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Globalization.CompareOptions
#include "mscorlib_System_Globalization_CompareOptions.h"
// Metadata Definition System.Globalization.CompareOptions
extern TypeInfo CompareOptions_t1682_il2cpp_TypeInfo;
// System.Globalization.CompareOptions
#include "mscorlib_System_Globalization_CompareOptionsMethodDeclarations.h"
static const EncodedMethodIndex CompareOptions_t1682_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CompareOptions_t1682_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompareOptions_t1682_0_0_0;
extern const Il2CppType CompareOptions_t1682_1_0_0;
const Il2CppTypeDefinitionMetadata CompareOptions_t1682_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompareOptions_t1682_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CompareOptions_t1682_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7016/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompareOptions_t1682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompareOptions"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2729/* custom_attributes_cache */
	, &CompareOptions_t1682_0_0_0/* byval_arg */
	, &CompareOptions_t1682_1_0_0/* this_arg */
	, &CompareOptions_t1682_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompareOptions_t1682)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CompareOptions_t1682)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.CultureInfo
#include "mscorlib_System_Globalization_CultureInfo.h"
// Metadata Definition System.Globalization.CultureInfo
extern TypeInfo CultureInfo_t1129_il2cpp_TypeInfo;
// System.Globalization.CultureInfo
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
static const EncodedMethodIndex CultureInfo_t1129_VTable[14] = 
{
	3172,
	601,
	3173,
	3174,
	3175,
	3176,
	3177,
	3178,
	3179,
	3180,
	3181,
	3182,
	3183,
	3175,
};
extern const Il2CppType IFormatProvider_t2173_0_0_0;
static const Il2CppType* CultureInfo_t1129_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&IFormatProvider_t2173_0_0_0,
};
static Il2CppInterfaceOffsetPair CultureInfo_t1129_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &IFormatProvider_t2173_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureInfo_t1129_0_0_0;
extern const Il2CppType CultureInfo_t1129_1_0_0;
struct CultureInfo_t1129;
const Il2CppTypeDefinitionMetadata CultureInfo_t1129_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CultureInfo_t1129_InterfacesTypeInfos/* implementedInterfaces */
	, CultureInfo_t1129_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CultureInfo_t1129_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7026/* fieldStart */
	, 10438/* methodStart */
	, -1/* eventStart */
	, 1984/* propertyStart */

};
TypeInfo CultureInfo_t1129_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &CultureInfo_t1129_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2730/* custom_attributes_cache */
	, &CultureInfo_t1129_0_0_0/* byval_arg */
	, &CultureInfo_t1129_1_0_0/* this_arg */
	, &CultureInfo_t1129_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureInfo_t1129)/* instance_size */
	, sizeof (CultureInfo_t1129)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CultureInfo_t1129_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 38/* method_count */
	, 13/* property_count */
	, 40/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlags.h"
// Metadata Definition System.Globalization.DateTimeFormatFlags
extern TypeInfo DateTimeFormatFlags_t1686_il2cpp_TypeInfo;
// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlagsMethodDeclarations.h"
static const EncodedMethodIndex DateTimeFormatFlags_t1686_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DateTimeFormatFlags_t1686_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeFormatFlags_t1686_0_0_0;
extern const Il2CppType DateTimeFormatFlags_t1686_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeFormatFlags_t1686_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DateTimeFormatFlags_t1686_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DateTimeFormatFlags_t1686_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7066/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeFormatFlags_t1686_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeFormatFlags"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2733/* custom_attributes_cache */
	, &DateTimeFormatFlags_t1686_0_0_0/* byval_arg */
	, &DateTimeFormatFlags_t1686_1_0_0/* this_arg */
	, &DateTimeFormatFlags_t1686_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeFormatFlags_t1686)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeFormatFlags_t1686)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.DateTimeFormatInfo
#include "mscorlib_System_Globalization_DateTimeFormatInfo.h"
// Metadata Definition System.Globalization.DateTimeFormatInfo
extern TypeInfo DateTimeFormatInfo_t1684_il2cpp_TypeInfo;
// System.Globalization.DateTimeFormatInfo
#include "mscorlib_System_Globalization_DateTimeFormatInfoMethodDeclarations.h"
static const EncodedMethodIndex DateTimeFormatInfo_t1684_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3184,
	3185,
};
static const Il2CppType* DateTimeFormatInfo_t1684_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&IFormatProvider_t2173_0_0_0,
};
static Il2CppInterfaceOffsetPair DateTimeFormatInfo_t1684_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &IFormatProvider_t2173_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeFormatInfo_t1684_0_0_0;
extern const Il2CppType DateTimeFormatInfo_t1684_1_0_0;
struct DateTimeFormatInfo_t1684;
const Il2CppTypeDefinitionMetadata DateTimeFormatInfo_t1684_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DateTimeFormatInfo_t1684_InterfacesTypeInfos/* implementedInterfaces */
	, DateTimeFormatInfo_t1684_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DateTimeFormatInfo_t1684_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7072/* fieldStart */
	, 10476/* methodStart */
	, -1/* eventStart */
	, 1997/* propertyStart */

};
TypeInfo DateTimeFormatInfo_t1684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeFormatInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &DateTimeFormatInfo_t1684_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2734/* custom_attributes_cache */
	, &DateTimeFormatInfo_t1684_0_0_0/* byval_arg */
	, &DateTimeFormatInfo_t1684_1_0_0/* this_arg */
	, &DateTimeFormatInfo_t1684_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeFormatInfo_t1684)/* instance_size */
	, sizeof (DateTimeFormatInfo_t1684)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DateTimeFormatInfo_t1684_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 41/* method_count */
	, 23/* property_count */
	, 58/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.DateTimeStyles
#include "mscorlib_System_Globalization_DateTimeStyles.h"
// Metadata Definition System.Globalization.DateTimeStyles
extern TypeInfo DateTimeStyles_t1687_il2cpp_TypeInfo;
// System.Globalization.DateTimeStyles
#include "mscorlib_System_Globalization_DateTimeStylesMethodDeclarations.h"
static const EncodedMethodIndex DateTimeStyles_t1687_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DateTimeStyles_t1687_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeStyles_t1687_0_0_0;
extern const Il2CppType DateTimeStyles_t1687_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeStyles_t1687_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DateTimeStyles_t1687_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DateTimeStyles_t1687_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7130/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeStyles_t1687_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeStyles"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2735/* custom_attributes_cache */
	, &DateTimeStyles_t1687_0_0_0/* byval_arg */
	, &DateTimeStyles_t1687_1_0_0/* this_arg */
	, &DateTimeStyles_t1687_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeStyles_t1687)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeStyles_t1687)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.DaylightTime
#include "mscorlib_System_Globalization_DaylightTime.h"
// Metadata Definition System.Globalization.DaylightTime
extern TypeInfo DaylightTime_t1688_il2cpp_TypeInfo;
// System.Globalization.DaylightTime
#include "mscorlib_System_Globalization_DaylightTimeMethodDeclarations.h"
static const EncodedMethodIndex DaylightTime_t1688_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DaylightTime_t1688_0_0_0;
extern const Il2CppType DaylightTime_t1688_1_0_0;
struct DaylightTime_t1688;
const Il2CppTypeDefinitionMetadata DaylightTime_t1688_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DaylightTime_t1688_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7141/* fieldStart */
	, 10517/* methodStart */
	, -1/* eventStart */
	, 2020/* propertyStart */

};
TypeInfo DaylightTime_t1688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DaylightTime"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &DaylightTime_t1688_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2736/* custom_attributes_cache */
	, &DaylightTime_t1688_0_0_0/* byval_arg */
	, &DaylightTime_t1688_1_0_0/* this_arg */
	, &DaylightTime_t1688_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DaylightTime_t1688)/* instance_size */
	, sizeof (DaylightTime_t1688)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.GregorianCalendar
#include "mscorlib_System_Globalization_GregorianCalendar.h"
// Metadata Definition System.Globalization.GregorianCalendar
extern TypeInfo GregorianCalendar_t1689_il2cpp_TypeInfo;
// System.Globalization.GregorianCalendar
#include "mscorlib_System_Globalization_GregorianCalendarMethodDeclarations.h"
static const EncodedMethodIndex GregorianCalendar_t1689_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3186,
	3187,
	3188,
	3189,
	3190,
	3191,
	3192,
};
static Il2CppInterfaceOffsetPair GregorianCalendar_t1689_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GregorianCalendar_t1689_0_0_0;
extern const Il2CppType GregorianCalendar_t1689_1_0_0;
struct GregorianCalendar_t1689;
const Il2CppTypeDefinitionMetadata GregorianCalendar_t1689_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GregorianCalendar_t1689_InterfacesOffsets/* interfaceOffsets */
	, &Calendar_t1678_0_0_0/* parent */
	, GregorianCalendar_t1689_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7144/* fieldStart */
	, 10521/* methodStart */
	, -1/* eventStart */
	, 2023/* propertyStart */

};
TypeInfo GregorianCalendar_t1689_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GregorianCalendar"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &GregorianCalendar_t1689_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2737/* custom_attributes_cache */
	, &GregorianCalendar_t1689_0_0_0/* byval_arg */
	, &GregorianCalendar_t1689_1_0_0/* this_arg */
	, &GregorianCalendar_t1689_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GregorianCalendar_t1689)/* instance_size */
	, sizeof (GregorianCalendar_t1689)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"
// Metadata Definition System.Globalization.GregorianCalendarTypes
extern TypeInfo GregorianCalendarTypes_t1690_il2cpp_TypeInfo;
// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypesMethodDeclarations.h"
static const EncodedMethodIndex GregorianCalendarTypes_t1690_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair GregorianCalendarTypes_t1690_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GregorianCalendarTypes_t1690_0_0_0;
extern const Il2CppType GregorianCalendarTypes_t1690_1_0_0;
const Il2CppTypeDefinitionMetadata GregorianCalendarTypes_t1690_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GregorianCalendarTypes_t1690_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, GregorianCalendarTypes_t1690_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7145/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GregorianCalendarTypes_t1690_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GregorianCalendarTypes"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2738/* custom_attributes_cache */
	, &GregorianCalendarTypes_t1690_0_0_0/* byval_arg */
	, &GregorianCalendarTypes_t1690_1_0_0/* this_arg */
	, &GregorianCalendarTypes_t1690_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GregorianCalendarTypes_t1690)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GregorianCalendarTypes_t1690)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.NumberFormatInfo
#include "mscorlib_System_Globalization_NumberFormatInfo.h"
// Metadata Definition System.Globalization.NumberFormatInfo
extern TypeInfo NumberFormatInfo_t1683_il2cpp_TypeInfo;
// System.Globalization.NumberFormatInfo
#include "mscorlib_System_Globalization_NumberFormatInfoMethodDeclarations.h"
static const EncodedMethodIndex NumberFormatInfo_t1683_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3193,
	3194,
};
static const Il2CppType* NumberFormatInfo_t1683_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&IFormatProvider_t2173_0_0_0,
};
static Il2CppInterfaceOffsetPair NumberFormatInfo_t1683_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &IFormatProvider_t2173_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatInfo_t1683_0_0_0;
extern const Il2CppType NumberFormatInfo_t1683_1_0_0;
struct NumberFormatInfo_t1683;
const Il2CppTypeDefinitionMetadata NumberFormatInfo_t1683_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NumberFormatInfo_t1683_InterfacesTypeInfos/* implementedInterfaces */
	, NumberFormatInfo_t1683_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatInfo_t1683_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7152/* fieldStart */
	, 10530/* methodStart */
	, -1/* eventStart */
	, 2025/* propertyStart */

};
TypeInfo NumberFormatInfo_t1683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &NumberFormatInfo_t1683_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2739/* custom_attributes_cache */
	, &NumberFormatInfo_t1683_0_0_0/* byval_arg */
	, &NumberFormatInfo_t1683_1_0_0/* this_arg */
	, &NumberFormatInfo_t1683_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatInfo_t1683)/* instance_size */
	, sizeof (NumberFormatInfo_t1683)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatInfo_t1683_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 35/* method_count */
	, 27/* property_count */
	, 39/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStyles.h"
// Metadata Definition System.Globalization.NumberStyles
extern TypeInfo NumberStyles_t1691_il2cpp_TypeInfo;
// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStylesMethodDeclarations.h"
static const EncodedMethodIndex NumberStyles_t1691_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair NumberStyles_t1691_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberStyles_t1691_0_0_0;
extern const Il2CppType NumberStyles_t1691_1_0_0;
const Il2CppTypeDefinitionMetadata NumberStyles_t1691_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NumberStyles_t1691_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, NumberStyles_t1691_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7191/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NumberStyles_t1691_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberStyles"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2740/* custom_attributes_cache */
	, &NumberStyles_t1691_0_0_0/* byval_arg */
	, &NumberStyles_t1691_1_0_0/* this_arg */
	, &NumberStyles_t1691_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberStyles_t1691)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NumberStyles_t1691)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 18/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Globalization.TextInfo
#include "mscorlib_System_Globalization_TextInfo.h"
// Metadata Definition System.Globalization.TextInfo
extern TypeInfo TextInfo_t1596_il2cpp_TypeInfo;
// System.Globalization.TextInfo
#include "mscorlib_System_Globalization_TextInfoMethodDeclarations.h"
extern const Il2CppType Data_t1692_0_0_0;
static const Il2CppType* TextInfo_t1596_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Data_t1692_0_0_0,
};
static const EncodedMethodIndex TextInfo_t1596_VTable[9] = 
{
	3195,
	601,
	3196,
	3197,
	3198,
	3199,
	3200,
	3201,
	3202,
};
static const Il2CppType* TextInfo_t1596_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair TextInfo_t1596_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &IDeserializationCallback_t2213_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TextInfo_t1596_0_0_0;
extern const Il2CppType TextInfo_t1596_1_0_0;
struct TextInfo_t1596;
const Il2CppTypeDefinitionMetadata TextInfo_t1596_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextInfo_t1596_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, TextInfo_t1596_InterfacesTypeInfos/* implementedInterfaces */
	, TextInfo_t1596_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextInfo_t1596_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7209/* fieldStart */
	, 10565/* methodStart */
	, -1/* eventStart */
	, 2052/* propertyStart */

};
TypeInfo TextInfo_t1596_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextInfo"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &TextInfo_t1596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2741/* custom_attributes_cache */
	, &TextInfo_t1596_0_0_0/* byval_arg */
	, &TextInfo_t1596_1_0_0/* this_arg */
	, &TextInfo_t1596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextInfo_t1596)/* instance_size */
	, sizeof (TextInfo_t1596)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 1/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Globalization.TextInfo/Data
#include "mscorlib_System_Globalization_TextInfo_Data.h"
// Metadata Definition System.Globalization.TextInfo/Data
extern TypeInfo Data_t1692_il2cpp_TypeInfo;
// System.Globalization.TextInfo/Data
#include "mscorlib_System_Globalization_TextInfo_DataMethodDeclarations.h"
static const EncodedMethodIndex Data_t1692_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Data_t1692_1_0_0;
const Il2CppTypeDefinitionMetadata Data_t1692_DefinitionMetadata = 
{
	&TextInfo_t1596_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Data_t1692_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7215/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Data_t1692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Data"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Data_t1692_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Data_t1692_0_0_0/* byval_arg */
	, &Data_t1692_1_0_0/* this_arg */
	, &Data_t1692_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Data_t1692)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Data_t1692)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Data_t1692 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
// Metadata Definition System.Globalization.UnicodeCategory
extern TypeInfo UnicodeCategory_t1693_il2cpp_TypeInfo;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategoryMethodDeclarations.h"
static const EncodedMethodIndex UnicodeCategory_t1693_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair UnicodeCategory_t1693_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnicodeCategory_t1693_0_0_0;
extern const Il2CppType UnicodeCategory_t1693_1_0_0;
const Il2CppTypeDefinitionMetadata UnicodeCategory_t1693_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnicodeCategory_t1693_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, UnicodeCategory_t1693_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7220/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnicodeCategory_t1693_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnicodeCategory"/* name */
	, "System.Globalization"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2744/* custom_attributes_cache */
	, &UnicodeCategory_t1693_0_0_0/* byval_arg */
	, &UnicodeCategory_t1693_1_0_0/* this_arg */
	, &UnicodeCategory_t1693_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnicodeCategory_t1693)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnicodeCategory_t1693)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 31/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.IsolatedStorage.IsolatedStorageException
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageException.h"
// Metadata Definition System.IO.IsolatedStorage.IsolatedStorageException
extern TypeInfo IsolatedStorageException_t1694_il2cpp_TypeInfo;
// System.IO.IsolatedStorage.IsolatedStorageException
#include "mscorlib_System_IO_IsolatedStorage_IsolatedStorageExceptionMethodDeclarations.h"
static const EncodedMethodIndex IsolatedStorageException_t1694_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair IsolatedStorageException_t1694_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IsolatedStorageException_t1694_0_0_0;
extern const Il2CppType IsolatedStorageException_t1694_1_0_0;
extern const Il2CppType Exception_t520_0_0_0;
struct IsolatedStorageException_t1694;
const Il2CppTypeDefinitionMetadata IsolatedStorageException_t1694_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IsolatedStorageException_t1694_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, IsolatedStorageException_t1694_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10575/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IsolatedStorageException_t1694_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IsolatedStorageException"/* name */
	, "System.IO.IsolatedStorage"/* namespaze */
	, NULL/* methods */
	, &IsolatedStorageException_t1694_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2745/* custom_attributes_cache */
	, &IsolatedStorageException_t1694_0_0_0/* byval_arg */
	, &IsolatedStorageException_t1694_1_0_0/* this_arg */
	, &IsolatedStorageException_t1694_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IsolatedStorageException_t1694)/* instance_size */
	, sizeof (IsolatedStorageException_t1694)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.BinaryReader
#include "mscorlib_System_IO_BinaryReader.h"
// Metadata Definition System.IO.BinaryReader
extern TypeInfo BinaryReader_t1696_il2cpp_TypeInfo;
// System.IO.BinaryReader
#include "mscorlib_System_IO_BinaryReaderMethodDeclarations.h"
static const EncodedMethodIndex BinaryReader_t1696_VTable[24] = 
{
	626,
	601,
	627,
	628,
	3203,
	3204,
	3205,
	3206,
	3207,
	3208,
	3209,
	3210,
	3211,
	3212,
	3213,
	3214,
	3215,
	3216,
	3217,
	3218,
	3219,
	3220,
	3221,
	3222,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* BinaryReader_t1696_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair BinaryReader_t1696_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BinaryReader_t1696_0_0_0;
extern const Il2CppType BinaryReader_t1696_1_0_0;
struct BinaryReader_t1696;
const Il2CppTypeDefinitionMetadata BinaryReader_t1696_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BinaryReader_t1696_InterfacesTypeInfos/* implementedInterfaces */
	, BinaryReader_t1696_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BinaryReader_t1696_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7251/* fieldStart */
	, 10578/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BinaryReader_t1696_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BinaryReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &BinaryReader_t1696_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2746/* custom_attributes_cache */
	, &BinaryReader_t1696_0_0_0/* byval_arg */
	, &BinaryReader_t1696_1_0_0/* this_arg */
	, &BinaryReader_t1696_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BinaryReader_t1696)/* instance_size */
	, sizeof (BinaryReader_t1696)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 25/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.Directory
#include "mscorlib_System_IO_Directory.h"
// Metadata Definition System.IO.Directory
extern TypeInfo Directory_t1697_il2cpp_TypeInfo;
// System.IO.Directory
#include "mscorlib_System_IO_DirectoryMethodDeclarations.h"
static const EncodedMethodIndex Directory_t1697_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Directory_t1697_0_0_0;
extern const Il2CppType Directory_t1697_1_0_0;
struct Directory_t1697;
const Il2CppTypeDefinitionMetadata Directory_t1697_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Directory_t1697_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10603/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Directory_t1697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Directory"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Directory_t1697_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2751/* custom_attributes_cache */
	, &Directory_t1697_0_0_0/* byval_arg */
	, &Directory_t1697_1_0_0/* this_arg */
	, &Directory_t1697_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Directory_t1697)/* instance_size */
	, sizeof (Directory_t1697)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.DirectoryInfo
#include "mscorlib_System_IO_DirectoryInfo.h"
// Metadata Definition System.IO.DirectoryInfo
extern TypeInfo DirectoryInfo_t1698_il2cpp_TypeInfo;
// System.IO.DirectoryInfo
#include "mscorlib_System_IO_DirectoryInfoMethodDeclarations.h"
static const EncodedMethodIndex DirectoryInfo_t1698_VTable[9] = 
{
	626,
	601,
	627,
	3223,
	3224,
	3224,
	3225,
	3226,
	3227,
};
static Il2CppInterfaceOffsetPair DirectoryInfo_t1698_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DirectoryInfo_t1698_0_0_0;
extern const Il2CppType DirectoryInfo_t1698_1_0_0;
extern const Il2CppType FileSystemInfo_t1699_0_0_0;
struct DirectoryInfo_t1698;
const Il2CppTypeDefinitionMetadata DirectoryInfo_t1698_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DirectoryInfo_t1698_InterfacesOffsets/* interfaceOffsets */
	, &FileSystemInfo_t1699_0_0_0/* parent */
	, DirectoryInfo_t1698_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7257/* fieldStart */
	, 10609/* methodStart */
	, -1/* eventStart */
	, 2053/* propertyStart */

};
TypeInfo DirectoryInfo_t1698_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DirectoryInfo"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &DirectoryInfo_t1698_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2752/* custom_attributes_cache */
	, &DirectoryInfo_t1698_0_0_0/* byval_arg */
	, &DirectoryInfo_t1698_1_0_0/* this_arg */
	, &DirectoryInfo_t1698_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DirectoryInfo_t1698)/* instance_size */
	, sizeof (DirectoryInfo_t1698)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.DirectoryNotFoundException
#include "mscorlib_System_IO_DirectoryNotFoundException.h"
// Metadata Definition System.IO.DirectoryNotFoundException
extern TypeInfo DirectoryNotFoundException_t1700_il2cpp_TypeInfo;
// System.IO.DirectoryNotFoundException
#include "mscorlib_System_IO_DirectoryNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex DirectoryNotFoundException_t1700_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair DirectoryNotFoundException_t1700_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DirectoryNotFoundException_t1700_0_0_0;
extern const Il2CppType DirectoryNotFoundException_t1700_1_0_0;
extern const Il2CppType IOException_t1367_0_0_0;
struct DirectoryNotFoundException_t1700;
const Il2CppTypeDefinitionMetadata DirectoryNotFoundException_t1700_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DirectoryNotFoundException_t1700_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t1367_0_0_0/* parent */
	, DirectoryNotFoundException_t1700_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10617/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DirectoryNotFoundException_t1700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DirectoryNotFoundException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &DirectoryNotFoundException_t1700_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2753/* custom_attributes_cache */
	, &DirectoryNotFoundException_t1700_0_0_0/* byval_arg */
	, &DirectoryNotFoundException_t1700_1_0_0/* this_arg */
	, &DirectoryNotFoundException_t1700_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DirectoryNotFoundException_t1700)/* instance_size */
	, sizeof (DirectoryNotFoundException_t1700)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.EndOfStreamException
#include "mscorlib_System_IO_EndOfStreamException.h"
// Metadata Definition System.IO.EndOfStreamException
extern TypeInfo EndOfStreamException_t1701_il2cpp_TypeInfo;
// System.IO.EndOfStreamException
#include "mscorlib_System_IO_EndOfStreamExceptionMethodDeclarations.h"
static const EncodedMethodIndex EndOfStreamException_t1701_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair EndOfStreamException_t1701_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EndOfStreamException_t1701_0_0_0;
extern const Il2CppType EndOfStreamException_t1701_1_0_0;
struct EndOfStreamException_t1701;
const Il2CppTypeDefinitionMetadata EndOfStreamException_t1701_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EndOfStreamException_t1701_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t1367_0_0_0/* parent */
	, EndOfStreamException_t1701_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10620/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EndOfStreamException_t1701_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EndOfStreamException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &EndOfStreamException_t1701_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2754/* custom_attributes_cache */
	, &EndOfStreamException_t1701_0_0_0/* byval_arg */
	, &EndOfStreamException_t1701_1_0_0/* this_arg */
	, &EndOfStreamException_t1701_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EndOfStreamException_t1701)/* instance_size */
	, sizeof (EndOfStreamException_t1701)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.File
#include "mscorlib_System_IO_File.h"
// Metadata Definition System.IO.File
extern TypeInfo File_t1702_il2cpp_TypeInfo;
// System.IO.File
#include "mscorlib_System_IO_FileMethodDeclarations.h"
static const EncodedMethodIndex File_t1702_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType File_t1702_0_0_0;
extern const Il2CppType File_t1702_1_0_0;
struct File_t1702;
const Il2CppTypeDefinitionMetadata File_t1702_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, File_t1702_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10622/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo File_t1702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "File"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &File_t1702_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2755/* custom_attributes_cache */
	, &File_t1702_0_0_0/* byval_arg */
	, &File_t1702_1_0_0/* this_arg */
	, &File_t1702_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (File_t1702)/* instance_size */
	, sizeof (File_t1702)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.FileAccess
#include "mscorlib_System_IO_FileAccess.h"
// Metadata Definition System.IO.FileAccess
extern TypeInfo FileAccess_t1533_il2cpp_TypeInfo;
// System.IO.FileAccess
#include "mscorlib_System_IO_FileAccessMethodDeclarations.h"
static const EncodedMethodIndex FileAccess_t1533_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FileAccess_t1533_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileAccess_t1533_0_0_0;
extern const Il2CppType FileAccess_t1533_1_0_0;
const Il2CppTypeDefinitionMetadata FileAccess_t1533_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileAccess_t1533_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FileAccess_t1533_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7259/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileAccess_t1533_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileAccess"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2756/* custom_attributes_cache */
	, &FileAccess_t1533_0_0_0/* byval_arg */
	, &FileAccess_t1533_1_0_0/* this_arg */
	, &FileAccess_t1533_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileAccess_t1533)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileAccess_t1533)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
// Metadata Definition System.IO.FileAttributes
extern TypeInfo FileAttributes_t1703_il2cpp_TypeInfo;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributesMethodDeclarations.h"
static const EncodedMethodIndex FileAttributes_t1703_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FileAttributes_t1703_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileAttributes_t1703_0_0_0;
extern const Il2CppType FileAttributes_t1703_1_0_0;
const Il2CppTypeDefinitionMetadata FileAttributes_t1703_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileAttributes_t1703_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FileAttributes_t1703_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7263/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileAttributes_t1703_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileAttributes"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2757/* custom_attributes_cache */
	, &FileAttributes_t1703_0_0_0/* byval_arg */
	, &FileAttributes_t1703_1_0_0/* this_arg */
	, &FileAttributes_t1703_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileAttributes_t1703)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileAttributes_t1703)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileMode
#include "mscorlib_System_IO_FileMode.h"
// Metadata Definition System.IO.FileMode
extern TypeInfo FileMode_t1704_il2cpp_TypeInfo;
// System.IO.FileMode
#include "mscorlib_System_IO_FileModeMethodDeclarations.h"
static const EncodedMethodIndex FileMode_t1704_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FileMode_t1704_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileMode_t1704_0_0_0;
extern const Il2CppType FileMode_t1704_1_0_0;
const Il2CppTypeDefinitionMetadata FileMode_t1704_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileMode_t1704_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FileMode_t1704_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7278/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileMode_t1704_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileMode"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2758/* custom_attributes_cache */
	, &FileMode_t1704_0_0_0/* byval_arg */
	, &FileMode_t1704_1_0_0/* this_arg */
	, &FileMode_t1704_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileMode_t1704)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileMode_t1704)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileNotFoundException
#include "mscorlib_System_IO_FileNotFoundException.h"
// Metadata Definition System.IO.FileNotFoundException
extern TypeInfo FileNotFoundException_t1705_il2cpp_TypeInfo;
// System.IO.FileNotFoundException
#include "mscorlib_System_IO_FileNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex FileNotFoundException_t1705_VTable[11] = 
{
	626,
	601,
	627,
	3228,
	3229,
	1318,
	3230,
	1320,
	1321,
	3229,
	1322,
};
static Il2CppInterfaceOffsetPair FileNotFoundException_t1705_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileNotFoundException_t1705_0_0_0;
extern const Il2CppType FileNotFoundException_t1705_1_0_0;
struct FileNotFoundException_t1705;
const Il2CppTypeDefinitionMetadata FileNotFoundException_t1705_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileNotFoundException_t1705_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t1367_0_0_0/* parent */
	, FileNotFoundException_t1705_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7285/* fieldStart */
	, 10627/* methodStart */
	, -1/* eventStart */
	, 2055/* propertyStart */

};
TypeInfo FileNotFoundException_t1705_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileNotFoundException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileNotFoundException_t1705_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2759/* custom_attributes_cache */
	, &FileNotFoundException_t1705_0_0_0/* byval_arg */
	, &FileNotFoundException_t1705_1_0_0/* this_arg */
	, &FileNotFoundException_t1705_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileNotFoundException_t1705)/* instance_size */
	, sizeof (FileNotFoundException_t1705)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptions.h"
// Metadata Definition System.IO.FileOptions
extern TypeInfo FileOptions_t1706_il2cpp_TypeInfo;
// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptionsMethodDeclarations.h"
static const EncodedMethodIndex FileOptions_t1706_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FileOptions_t1706_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileOptions_t1706_0_0_0;
extern const Il2CppType FileOptions_t1706_1_0_0;
const Il2CppTypeDefinitionMetadata FileOptions_t1706_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileOptions_t1706_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FileOptions_t1706_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7287/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileOptions_t1706_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileOptions"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2760/* custom_attributes_cache */
	, &FileOptions_t1706_0_0_0/* byval_arg */
	, &FileOptions_t1706_1_0_0/* this_arg */
	, &FileOptions_t1706_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileOptions_t1706)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileOptions_t1706)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileShare
#include "mscorlib_System_IO_FileShare.h"
// Metadata Definition System.IO.FileShare
extern TypeInfo FileShare_t1707_il2cpp_TypeInfo;
// System.IO.FileShare
#include "mscorlib_System_IO_FileShareMethodDeclarations.h"
static const EncodedMethodIndex FileShare_t1707_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FileShare_t1707_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileShare_t1707_0_0_0;
extern const Il2CppType FileShare_t1707_1_0_0;
const Il2CppTypeDefinitionMetadata FileShare_t1707_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileShare_t1707_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FileShare_t1707_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7295/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FileShare_t1707_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileShare"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2761/* custom_attributes_cache */
	, &FileShare_t1707_0_0_0/* byval_arg */
	, &FileShare_t1707_1_0_0/* this_arg */
	, &FileShare_t1707_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileShare_t1707)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FileShare_t1707)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.FileStream
#include "mscorlib_System_IO_FileStream.h"
// Metadata Definition System.IO.FileStream
extern TypeInfo FileStream_t1350_il2cpp_TypeInfo;
// System.IO.FileStream
#include "mscorlib_System_IO_FileStreamMethodDeclarations.h"
extern const Il2CppType ReadDelegate_t1708_0_0_0;
extern const Il2CppType WriteDelegate_t1709_0_0_0;
static const Il2CppType* FileStream_t1350_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ReadDelegate_t1708_0_0_0,
	&WriteDelegate_t1709_0_0_0,
};
static const EncodedMethodIndex FileStream_t1350_VTable[24] = 
{
	626,
	3231,
	627,
	628,
	1820,
	3232,
	3233,
	3234,
	3235,
	3236,
	3237,
	3238,
	1886,
	3239,
	3240,
	3241,
	3242,
	3243,
	3244,
	3245,
	3246,
	3247,
	3248,
	3249,
};
static Il2CppInterfaceOffsetPair FileStream_t1350_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileStream_t1350_0_0_0;
extern const Il2CppType FileStream_t1350_1_0_0;
extern const Il2CppType Stream_t1284_0_0_0;
struct FileStream_t1350;
const Il2CppTypeDefinitionMetadata FileStream_t1350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, FileStream_t1350_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FileStream_t1350_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t1284_0_0_0/* parent */
	, FileStream_t1350_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7302/* fieldStart */
	, 10633/* methodStart */
	, -1/* eventStart */
	, 2056/* propertyStart */

};
TypeInfo FileStream_t1350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileStream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileStream_t1350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2762/* custom_attributes_cache */
	, &FileStream_t1350_0_0_0/* byval_arg */
	, &FileStream_t1350_1_0_0/* this_arg */
	, &FileStream_t1350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileStream_t1350)/* instance_size */
	, sizeof (FileStream_t1350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 36/* method_count */
	, 5/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 24/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.FileStream/ReadDelegate
#include "mscorlib_System_IO_FileStream_ReadDelegate.h"
// Metadata Definition System.IO.FileStream/ReadDelegate
extern TypeInfo ReadDelegate_t1708_il2cpp_TypeInfo;
// System.IO.FileStream/ReadDelegate
#include "mscorlib_System_IO_FileStream_ReadDelegateMethodDeclarations.h"
static const EncodedMethodIndex ReadDelegate_t1708_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	3250,
	3251,
	3252,
};
static Il2CppInterfaceOffsetPair ReadDelegate_t1708_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReadDelegate_t1708_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct ReadDelegate_t1708;
const Il2CppTypeDefinitionMetadata ReadDelegate_t1708_DefinitionMetadata = 
{
	&FileStream_t1350_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReadDelegate_t1708_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, ReadDelegate_t1708_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10669/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReadDelegate_t1708_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReadDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ReadDelegate_t1708_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReadDelegate_t1708_0_0_0/* byval_arg */
	, &ReadDelegate_t1708_1_0_0/* this_arg */
	, &ReadDelegate_t1708_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ReadDelegate_t1708/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReadDelegate_t1708)/* instance_size */
	, sizeof (ReadDelegate_t1708)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.FileStream/WriteDelegate
#include "mscorlib_System_IO_FileStream_WriteDelegate.h"
// Metadata Definition System.IO.FileStream/WriteDelegate
extern TypeInfo WriteDelegate_t1709_il2cpp_TypeInfo;
// System.IO.FileStream/WriteDelegate
#include "mscorlib_System_IO_FileStream_WriteDelegateMethodDeclarations.h"
static const EncodedMethodIndex WriteDelegate_t1709_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	3253,
	3254,
	3255,
};
static Il2CppInterfaceOffsetPair WriteDelegate_t1709_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WriteDelegate_t1709_1_0_0;
struct WriteDelegate_t1709;
const Il2CppTypeDefinitionMetadata WriteDelegate_t1709_DefinitionMetadata = 
{
	&FileStream_t1350_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WriteDelegate_t1709_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, WriteDelegate_t1709_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10673/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WriteDelegate_t1709_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WriteDelegate"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &WriteDelegate_t1709_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WriteDelegate_t1709_0_0_0/* byval_arg */
	, &WriteDelegate_t1709_1_0_0/* this_arg */
	, &WriteDelegate_t1709_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_WriteDelegate_t1709/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WriteDelegate_t1709)/* instance_size */
	, sizeof (WriteDelegate_t1709)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.FileStreamAsyncResult
#include "mscorlib_System_IO_FileStreamAsyncResult.h"
// Metadata Definition System.IO.FileStreamAsyncResult
extern TypeInfo FileStreamAsyncResult_t1710_il2cpp_TypeInfo;
// System.IO.FileStreamAsyncResult
#include "mscorlib_System_IO_FileStreamAsyncResultMethodDeclarations.h"
static const EncodedMethodIndex FileStreamAsyncResult_t1710_VTable[7] = 
{
	626,
	601,
	627,
	628,
	3256,
	3257,
	3258,
};
extern const Il2CppType IAsyncResult_t216_0_0_0;
static const Il2CppType* FileStreamAsyncResult_t1710_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t216_0_0_0,
};
static Il2CppInterfaceOffsetPair FileStreamAsyncResult_t1710_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t216_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileStreamAsyncResult_t1710_0_0_0;
extern const Il2CppType FileStreamAsyncResult_t1710_1_0_0;
struct FileStreamAsyncResult_t1710;
const Il2CppTypeDefinitionMetadata FileStreamAsyncResult_t1710_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileStreamAsyncResult_t1710_InterfacesTypeInfos/* implementedInterfaces */
	, FileStreamAsyncResult_t1710_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, FileStreamAsyncResult_t1710_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7316/* fieldStart */
	, 10677/* methodStart */
	, -1/* eventStart */
	, 2061/* propertyStart */

};
TypeInfo FileStreamAsyncResult_t1710_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileStreamAsyncResult"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileStreamAsyncResult_t1710_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FileStreamAsyncResult_t1710_0_0_0/* byval_arg */
	, &FileStreamAsyncResult_t1710_1_0_0/* this_arg */
	, &FileStreamAsyncResult_t1710_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileStreamAsyncResult_t1710)/* instance_size */
	, sizeof (FileStreamAsyncResult_t1710)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.FileSystemInfo
#include "mscorlib_System_IO_FileSystemInfo.h"
// Metadata Definition System.IO.FileSystemInfo
extern TypeInfo FileSystemInfo_t1699_il2cpp_TypeInfo;
// System.IO.FileSystemInfo
#include "mscorlib_System_IO_FileSystemInfoMethodDeclarations.h"
static const EncodedMethodIndex FileSystemInfo_t1699_VTable[9] = 
{
	626,
	601,
	627,
	628,
	3224,
	3224,
	0,
	3226,
	3227,
};
static const Il2CppType* FileSystemInfo_t1699_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair FileSystemInfo_t1699_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FileSystemInfo_t1699_1_0_0;
extern const Il2CppType MarshalByRefObject_t1415_0_0_0;
struct FileSystemInfo_t1699;
const Il2CppTypeDefinitionMetadata FileSystemInfo_t1699_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FileSystemInfo_t1699_InterfacesTypeInfos/* implementedInterfaces */
	, FileSystemInfo_t1699_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, FileSystemInfo_t1699_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7324/* fieldStart */
	, 10682/* methodStart */
	, -1/* eventStart */
	, 2064/* propertyStart */

};
TypeInfo FileSystemInfo_t1699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FileSystemInfo"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &FileSystemInfo_t1699_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2763/* custom_attributes_cache */
	, &FileSystemInfo_t1699_0_0_0/* byval_arg */
	, &FileSystemInfo_t1699_1_0_0/* this_arg */
	, &FileSystemInfo_t1699_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FileSystemInfo_t1699)/* instance_size */
	, sizeof (FileSystemInfo_t1699)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.IOException
#include "mscorlib_System_IO_IOException.h"
// Metadata Definition System.IO.IOException
extern TypeInfo IOException_t1367_il2cpp_TypeInfo;
// System.IO.IOException
#include "mscorlib_System_IO_IOExceptionMethodDeclarations.h"
static const EncodedMethodIndex IOException_t1367_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair IOException_t1367_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IOException_t1367_1_0_0;
extern const Il2CppType SystemException_t1536_0_0_0;
struct IOException_t1367;
const Il2CppTypeDefinitionMetadata IOException_t1367_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IOException_t1367_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, IOException_t1367_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10690/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IOException_t1367_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IOException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &IOException_t1367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2765/* custom_attributes_cache */
	, &IOException_t1367_0_0_0/* byval_arg */
	, &IOException_t1367_1_0_0/* this_arg */
	, &IOException_t1367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IOException_t1367)/* instance_size */
	, sizeof (IOException_t1367)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStream.h"
// Metadata Definition System.IO.MemoryStream
extern TypeInfo MemoryStream_t1121_il2cpp_TypeInfo;
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
static const EncodedMethodIndex MemoryStream_t1121_VTable[26] = 
{
	626,
	601,
	627,
	628,
	1820,
	3259,
	3260,
	3261,
	3262,
	3263,
	3264,
	3265,
	1886,
	3266,
	3267,
	3268,
	3269,
	3270,
	3271,
	3272,
	1892,
	1893,
	1894,
	1895,
	3273,
	3274,
};
static Il2CppInterfaceOffsetPair MemoryStream_t1121_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemoryStream_t1121_0_0_0;
extern const Il2CppType MemoryStream_t1121_1_0_0;
struct MemoryStream_t1121;
const Il2CppTypeDefinitionMetadata MemoryStream_t1121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemoryStream_t1121_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t1284_0_0_0/* parent */
	, MemoryStream_t1121_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7328/* fieldStart */
	, 10695/* methodStart */
	, -1/* eventStart */
	, 2066/* propertyStart */

};
TypeInfo MemoryStream_t1121_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemoryStream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &MemoryStream_t1121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2766/* custom_attributes_cache */
	, &MemoryStream_t1121_0_0_0/* byval_arg */
	, &MemoryStream_t1121_1_0_0/* this_arg */
	, &MemoryStream_t1121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemoryStream_t1121)/* instance_size */
	, sizeof (MemoryStream_t1121)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 6/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
// Metadata Definition System.IO.MonoFileType
extern TypeInfo MonoFileType_t1712_il2cpp_TypeInfo;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileTypeMethodDeclarations.h"
static const EncodedMethodIndex MonoFileType_t1712_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MonoFileType_t1712_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoFileType_t1712_0_0_0;
extern const Il2CppType MonoFileType_t1712_1_0_0;
const Il2CppTypeDefinitionMetadata MonoFileType_t1712_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoFileType_t1712_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MonoFileType_t1712_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7338/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoFileType_t1712_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoFileType"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoFileType_t1712_0_0_0/* byval_arg */
	, &MonoFileType_t1712_1_0_0/* this_arg */
	, &MonoFileType_t1712_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoFileType_t1712)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoFileType_t1712)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.MonoIO
#include "mscorlib_System_IO_MonoIO.h"
// Metadata Definition System.IO.MonoIO
extern TypeInfo MonoIO_t1713_il2cpp_TypeInfo;
// System.IO.MonoIO
#include "mscorlib_System_IO_MonoIOMethodDeclarations.h"
static const EncodedMethodIndex MonoIO_t1713_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoIO_t1713_0_0_0;
extern const Il2CppType MonoIO_t1713_1_0_0;
struct MonoIO_t1713;
const Il2CppTypeDefinitionMetadata MonoIO_t1713_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoIO_t1713_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7344/* fieldStart */
	, 10718/* methodStart */
	, -1/* eventStart */
	, 2072/* propertyStart */

};
TypeInfo MonoIO_t1713_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoIO"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &MonoIO_t1713_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoIO_t1713_0_0_0/* byval_arg */
	, &MonoIO_t1713_1_0_0/* this_arg */
	, &MonoIO_t1713_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoIO_t1713)/* instance_size */
	, sizeof (MonoIO_t1713)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoIO_t1713_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 7/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
// Metadata Definition System.IO.MonoIOError
extern TypeInfo MonoIOError_t1714_il2cpp_TypeInfo;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOErrorMethodDeclarations.h"
static const EncodedMethodIndex MonoIOError_t1714_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MonoIOError_t1714_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoIOError_t1714_0_0_0;
extern const Il2CppType MonoIOError_t1714_1_0_0;
const Il2CppTypeDefinitionMetadata MonoIOError_t1714_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoIOError_t1714_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MonoIOError_t1714_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7346/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoIOError_t1714_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoIOError"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoIOError_t1714_0_0_0/* byval_arg */
	, &MonoIOError_t1714_1_0_0/* this_arg */
	, &MonoIOError_t1714_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoIOError_t1714)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoIOError_t1714)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 25/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
// Metadata Definition System.IO.MonoIOStat
extern TypeInfo MonoIOStat_t1711_il2cpp_TypeInfo;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStatMethodDeclarations.h"
static const EncodedMethodIndex MonoIOStat_t1711_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoIOStat_t1711_0_0_0;
extern const Il2CppType MonoIOStat_t1711_1_0_0;
const Il2CppTypeDefinitionMetadata MonoIOStat_t1711_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MonoIOStat_t1711_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7371/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoIOStat_t1711_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoIOStat"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &MonoIOStat_t1711_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoIOStat_t1711_0_0_0/* byval_arg */
	, &MonoIOStat_t1711_1_0_0/* this_arg */
	, &MonoIOStat_t1711_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)MonoIOStat_t1711_marshal/* marshal_to_native_func */
	, (methodPointerType)MonoIOStat_t1711_marshal_back/* marshal_from_native_func */
	, (methodPointerType)MonoIOStat_t1711_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (MonoIOStat_t1711)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoIOStat_t1711)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(MonoIOStat_t1711_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.Path
#include "mscorlib_System_IO_Path.h"
// Metadata Definition System.IO.Path
extern TypeInfo Path_t1351_il2cpp_TypeInfo;
// System.IO.Path
#include "mscorlib_System_IO_PathMethodDeclarations.h"
static const EncodedMethodIndex Path_t1351_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Path_t1351_0_0_0;
extern const Il2CppType Path_t1351_1_0_0;
struct Path_t1351;
const Il2CppTypeDefinitionMetadata Path_t1351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Path_t1351_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7377/* fieldStart */
	, 10744/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Path_t1351_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Path"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Path_t1351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2767/* custom_attributes_cache */
	, &Path_t1351_0_0_0/* byval_arg */
	, &Path_t1351_1_0_0/* this_arg */
	, &Path_t1351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Path_t1351)/* instance_size */
	, sizeof (Path_t1351)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Path_t1351_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 385/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.PathTooLongException
#include "mscorlib_System_IO_PathTooLongException.h"
// Metadata Definition System.IO.PathTooLongException
extern TypeInfo PathTooLongException_t1715_il2cpp_TypeInfo;
// System.IO.PathTooLongException
#include "mscorlib_System_IO_PathTooLongExceptionMethodDeclarations.h"
static const EncodedMethodIndex PathTooLongException_t1715_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair PathTooLongException_t1715_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PathTooLongException_t1715_0_0_0;
extern const Il2CppType PathTooLongException_t1715_1_0_0;
struct PathTooLongException_t1715;
const Il2CppTypeDefinitionMetadata PathTooLongException_t1715_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PathTooLongException_t1715_InterfacesOffsets/* interfaceOffsets */
	, &IOException_t1367_0_0_0/* parent */
	, PathTooLongException_t1715_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10759/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PathTooLongException_t1715_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PathTooLongException"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &PathTooLongException_t1715_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2769/* custom_attributes_cache */
	, &PathTooLongException_t1715_0_0_0/* byval_arg */
	, &PathTooLongException_t1715_1_0_0/* this_arg */
	, &PathTooLongException_t1715_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PathTooLongException_t1715)/* instance_size */
	, sizeof (PathTooLongException_t1715)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.IO.SearchPattern
#include "mscorlib_System_IO_SearchPattern.h"
// Metadata Definition System.IO.SearchPattern
extern TypeInfo SearchPattern_t1716_il2cpp_TypeInfo;
// System.IO.SearchPattern
#include "mscorlib_System_IO_SearchPatternMethodDeclarations.h"
static const EncodedMethodIndex SearchPattern_t1716_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SearchPattern_t1716_0_0_0;
extern const Il2CppType SearchPattern_t1716_1_0_0;
struct SearchPattern_t1716;
const Il2CppTypeDefinitionMetadata SearchPattern_t1716_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SearchPattern_t1716_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7385/* fieldStart */
	, 10762/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SearchPattern_t1716_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SearchPattern"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &SearchPattern_t1716_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SearchPattern_t1716_0_0_0/* byval_arg */
	, &SearchPattern_t1716_1_0_0/* this_arg */
	, &SearchPattern_t1716_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SearchPattern_t1716)/* instance_size */
	, sizeof (SearchPattern_t1716)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SearchPattern_t1716_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOrigin.h"
// Metadata Definition System.IO.SeekOrigin
extern TypeInfo SeekOrigin_t1717_il2cpp_TypeInfo;
// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOriginMethodDeclarations.h"
static const EncodedMethodIndex SeekOrigin_t1717_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SeekOrigin_t1717_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SeekOrigin_t1717_0_0_0;
extern const Il2CppType SeekOrigin_t1717_1_0_0;
const Il2CppTypeDefinitionMetadata SeekOrigin_t1717_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SeekOrigin_t1717_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SeekOrigin_t1717_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7387/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SeekOrigin_t1717_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SeekOrigin"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2770/* custom_attributes_cache */
	, &SeekOrigin_t1717_0_0_0/* byval_arg */
	, &SeekOrigin_t1717_1_0_0/* this_arg */
	, &SeekOrigin_t1717_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SeekOrigin_t1717)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SeekOrigin_t1717)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.IO.Stream
#include "mscorlib_System_IO_Stream.h"
// Metadata Definition System.IO.Stream
extern TypeInfo Stream_t1284_il2cpp_TypeInfo;
// System.IO.Stream
#include "mscorlib_System_IO_StreamMethodDeclarations.h"
static const EncodedMethodIndex Stream_t1284_VTable[24] = 
{
	626,
	601,
	627,
	628,
	1820,
	0,
	0,
	0,
	0,
	0,
	0,
	1885,
	1886,
	0,
	0,
	1831,
	0,
	0,
	0,
	1835,
	1892,
	1893,
	1894,
	1895,
};
static const Il2CppType* Stream_t1284_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair Stream_t1284_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Stream_t1284_1_0_0;
struct Stream_t1284;
const Il2CppTypeDefinitionMetadata Stream_t1284_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Stream_t1284_InterfacesTypeInfos/* implementedInterfaces */
	, Stream_t1284_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Stream_t1284_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7391/* fieldStart */
	, 10763/* methodStart */
	, -1/* eventStart */
	, 2079/* propertyStart */

};
TypeInfo Stream_t1284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &Stream_t1284_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2771/* custom_attributes_cache */
	, &Stream_t1284_0_0_0/* byval_arg */
	, &Stream_t1284_1_0_0/* this_arg */
	, &Stream_t1284_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Stream_t1284)/* instance_size */
	, sizeof (Stream_t1284)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Stream_t1284_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 22/* method_count */
	, 5/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.NullStream
#include "mscorlib_System_IO_NullStream.h"
// Metadata Definition System.IO.NullStream
extern TypeInfo NullStream_t1718_il2cpp_TypeInfo;
// System.IO.NullStream
#include "mscorlib_System_IO_NullStreamMethodDeclarations.h"
static const EncodedMethodIndex NullStream_t1718_VTable[24] = 
{
	626,
	601,
	627,
	628,
	1820,
	3275,
	3276,
	3277,
	3278,
	3279,
	3280,
	1885,
	1886,
	3281,
	3282,
	3283,
	3284,
	3285,
	3286,
	3287,
	1892,
	1893,
	1894,
	1895,
};
static Il2CppInterfaceOffsetPair NullStream_t1718_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullStream_t1718_0_0_0;
extern const Il2CppType NullStream_t1718_1_0_0;
struct NullStream_t1718;
const Il2CppTypeDefinitionMetadata NullStream_t1718_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullStream_t1718_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t1284_0_0_0/* parent */
	, NullStream_t1718_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10785/* methodStart */
	, -1/* eventStart */
	, 2084/* propertyStart */

};
TypeInfo NullStream_t1718_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullStream"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &NullStream_t1718_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullStream_t1718_0_0_0/* byval_arg */
	, &NullStream_t1718_1_0_0/* this_arg */
	, &NullStream_t1718_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullStream_t1718)/* instance_size */
	, sizeof (NullStream_t1718)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamAsyncResult
#include "mscorlib_System_IO_StreamAsyncResult.h"
// Metadata Definition System.IO.StreamAsyncResult
extern TypeInfo StreamAsyncResult_t1719_il2cpp_TypeInfo;
// System.IO.StreamAsyncResult
#include "mscorlib_System_IO_StreamAsyncResultMethodDeclarations.h"
static const EncodedMethodIndex StreamAsyncResult_t1719_VTable[7] = 
{
	626,
	601,
	627,
	628,
	3288,
	3289,
	3290,
};
static const Il2CppType* StreamAsyncResult_t1719_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t216_0_0_0,
};
static Il2CppInterfaceOffsetPair StreamAsyncResult_t1719_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t216_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamAsyncResult_t1719_0_0_0;
extern const Il2CppType StreamAsyncResult_t1719_1_0_0;
struct StreamAsyncResult_t1719;
const Il2CppTypeDefinitionMetadata StreamAsyncResult_t1719_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StreamAsyncResult_t1719_InterfacesTypeInfos/* implementedInterfaces */
	, StreamAsyncResult_t1719_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StreamAsyncResult_t1719_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7392/* fieldStart */
	, 10799/* methodStart */
	, -1/* eventStart */
	, 2089/* propertyStart */

};
TypeInfo StreamAsyncResult_t1719_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamAsyncResult"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StreamAsyncResult_t1719_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StreamAsyncResult_t1719_0_0_0/* byval_arg */
	, &StreamAsyncResult_t1719_1_0_0/* this_arg */
	, &StreamAsyncResult_t1719_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamAsyncResult_t1719)/* instance_size */
	, sizeof (StreamAsyncResult_t1719)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReader.h"
// Metadata Definition System.IO.StreamReader
extern TypeInfo StreamReader_t1721_il2cpp_TypeInfo;
// System.IO.StreamReader
#include "mscorlib_System_IO_StreamReaderMethodDeclarations.h"
extern const Il2CppType NullStreamReader_t1720_0_0_0;
static const Il2CppType* StreamReader_t1721_il2cpp_TypeInfo__nestedTypes[1] =
{
	&NullStreamReader_t1720_0_0_0,
};
static const EncodedMethodIndex StreamReader_t1721_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3291,
	3292,
	3293,
	3294,
	3295,
	3296,
	3297,
};
static Il2CppInterfaceOffsetPair StreamReader_t1721_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamReader_t1721_0_0_0;
extern const Il2CppType StreamReader_t1721_1_0_0;
extern const Il2CppType TextReader_t1644_0_0_0;
struct StreamReader_t1721;
const Il2CppTypeDefinitionMetadata StreamReader_t1721_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StreamReader_t1721_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StreamReader_t1721_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t1644_0_0_0/* parent */
	, StreamReader_t1721_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7398/* fieldStart */
	, 10809/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StreamReader_t1721_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StreamReader_t1721_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2772/* custom_attributes_cache */
	, &StreamReader_t1721_0_0_0/* byval_arg */
	, &StreamReader_t1721_1_0_0/* this_arg */
	, &StreamReader_t1721_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamReader_t1721)/* instance_size */
	, sizeof (StreamReader_t1721)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StreamReader_t1721_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamReader/NullStreamReader
#include "mscorlib_System_IO_StreamReader_NullStreamReader.h"
// Metadata Definition System.IO.StreamReader/NullStreamReader
extern TypeInfo NullStreamReader_t1720_il2cpp_TypeInfo;
// System.IO.StreamReader/NullStreamReader
#include "mscorlib_System_IO_StreamReader_NullStreamReaderMethodDeclarations.h"
static const EncodedMethodIndex NullStreamReader_t1720_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3291,
	3292,
	3298,
	3299,
	3300,
	3301,
	3302,
};
static Il2CppInterfaceOffsetPair NullStreamReader_t1720_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullStreamReader_t1720_1_0_0;
struct NullStreamReader_t1720;
const Il2CppTypeDefinitionMetadata NullStreamReader_t1720_DefinitionMetadata = 
{
	&StreamReader_t1721_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullStreamReader_t1720_InterfacesOffsets/* interfaceOffsets */
	, &StreamReader_t1721_0_0_0/* parent */
	, NullStreamReader_t1720_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10825/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullStreamReader_t1720_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullStreamReader"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &NullStreamReader_t1720_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullStreamReader_t1720_0_0_0/* byval_arg */
	, &NullStreamReader_t1720_1_0_0/* this_arg */
	, &NullStreamReader_t1720_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullStreamReader_t1720)/* instance_size */
	, sizeof (NullStreamReader_t1720)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StreamWriter
#include "mscorlib_System_IO_StreamWriter.h"
// Metadata Definition System.IO.StreamWriter
extern TypeInfo StreamWriter_t1722_il2cpp_TypeInfo;
// System.IO.StreamWriter
#include "mscorlib_System_IO_StreamWriterMethodDeclarations.h"
static const EncodedMethodIndex StreamWriter_t1722_VTable[15] = 
{
	626,
	3303,
	627,
	628,
	3304,
	3305,
	3306,
	3307,
	3308,
	3309,
	3310,
	3311,
	3312,
	3313,
	3314,
};
static Il2CppInterfaceOffsetPair StreamWriter_t1722_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StreamWriter_t1722_0_0_0;
extern const Il2CppType StreamWriter_t1722_1_0_0;
extern const Il2CppType TextWriter_t1538_0_0_0;
struct StreamWriter_t1722;
const Il2CppTypeDefinitionMetadata StreamWriter_t1722_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StreamWriter_t1722_InterfacesOffsets/* interfaceOffsets */
	, &TextWriter_t1538_0_0_0/* parent */
	, StreamWriter_t1722_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7411/* fieldStart */
	, 10831/* methodStart */
	, -1/* eventStart */
	, 2095/* propertyStart */

};
TypeInfo StreamWriter_t1722_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StreamWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StreamWriter_t1722_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2773/* custom_attributes_cache */
	, &StreamWriter_t1722_0_0_0/* byval_arg */
	, &StreamWriter_t1722_1_0_0/* this_arg */
	, &StreamWriter_t1722_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StreamWriter_t1722)/* instance_size */
	, sizeof (StreamWriter_t1722)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StreamWriter_t1722_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 1/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.StringReader
#include "mscorlib_System_IO_StringReader.h"
// Metadata Definition System.IO.StringReader
extern TypeInfo StringReader_t1120_il2cpp_TypeInfo;
// System.IO.StringReader
#include "mscorlib_System_IO_StringReaderMethodDeclarations.h"
static const EncodedMethodIndex StringReader_t1120_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3291,
	3315,
	3316,
	3317,
	3318,
	3319,
	3320,
};
static Il2CppInterfaceOffsetPair StringReader_t1120_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringReader_t1120_0_0_0;
extern const Il2CppType StringReader_t1120_1_0_0;
struct StringReader_t1120;
const Il2CppTypeDefinitionMetadata StringReader_t1120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringReader_t1120_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t1644_0_0_0/* parent */
	, StringReader_t1120_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7421/* fieldStart */
	, 10848/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StringReader_t1120_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &StringReader_t1120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2774/* custom_attributes_cache */
	, &StringReader_t1120_0_0_0/* byval_arg */
	, &StringReader_t1120_1_0_0/* this_arg */
	, &StringReader_t1120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringReader_t1120)/* instance_size */
	, sizeof (StringReader_t1120)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextReader
#include "mscorlib_System_IO_TextReader.h"
// Metadata Definition System.IO.TextReader
extern TypeInfo TextReader_t1644_il2cpp_TypeInfo;
// System.IO.TextReader
#include "mscorlib_System_IO_TextReaderMethodDeclarations.h"
extern const Il2CppType NullTextReader_t1723_0_0_0;
static const Il2CppType* TextReader_t1644_il2cpp_TypeInfo__nestedTypes[1] =
{
	&NullTextReader_t1723_0_0_0,
};
static const EncodedMethodIndex TextReader_t1644_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3291,
	3321,
	3322,
	3323,
	3324,
	3325,
	3326,
};
static const Il2CppType* TextReader_t1644_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair TextReader_t1644_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TextReader_t1644_1_0_0;
struct TextReader_t1644;
const Il2CppTypeDefinitionMetadata TextReader_t1644_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextReader_t1644_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, TextReader_t1644_InterfacesTypeInfos/* implementedInterfaces */
	, TextReader_t1644_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextReader_t1644_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7424/* fieldStart */
	, 10856/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextReader_t1644_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &TextReader_t1644_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2775/* custom_attributes_cache */
	, &TextReader_t1644_0_0_0/* byval_arg */
	, &TextReader_t1644_1_0_0/* this_arg */
	, &TextReader_t1644_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextReader_t1644)/* instance_size */
	, sizeof (TextReader_t1644)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextReader_t1644_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8321/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextReader/NullTextReader
#include "mscorlib_System_IO_TextReader_NullTextReader.h"
// Metadata Definition System.IO.TextReader/NullTextReader
extern TypeInfo NullTextReader_t1723_il2cpp_TypeInfo;
// System.IO.TextReader/NullTextReader
#include "mscorlib_System_IO_TextReader_NullTextReaderMethodDeclarations.h"
static const EncodedMethodIndex NullTextReader_t1723_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3291,
	3321,
	3322,
	3323,
	3324,
	3327,
	3326,
};
static Il2CppInterfaceOffsetPair NullTextReader_t1723_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullTextReader_t1723_1_0_0;
struct NullTextReader_t1723;
const Il2CppTypeDefinitionMetadata NullTextReader_t1723_DefinitionMetadata = 
{
	&TextReader_t1644_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullTextReader_t1723_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t1644_0_0_0/* parent */
	, NullTextReader_t1723_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10866/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullTextReader_t1723_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullTextReader"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &NullTextReader_t1723_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullTextReader_t1723_0_0_0/* byval_arg */
	, &NullTextReader_t1723_1_0_0/* this_arg */
	, &NullTextReader_t1723_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullTextReader_t1723)/* instance_size */
	, sizeof (NullTextReader_t1723)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.SynchronizedReader
#include "mscorlib_System_IO_SynchronizedReader.h"
// Metadata Definition System.IO.SynchronizedReader
extern TypeInfo SynchronizedReader_t1724_il2cpp_TypeInfo;
// System.IO.SynchronizedReader
#include "mscorlib_System_IO_SynchronizedReaderMethodDeclarations.h"
static const EncodedMethodIndex SynchronizedReader_t1724_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3291,
	3321,
	3328,
	3329,
	3330,
	3331,
	3332,
};
static Il2CppInterfaceOffsetPair SynchronizedReader_t1724_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizedReader_t1724_0_0_0;
extern const Il2CppType SynchronizedReader_t1724_1_0_0;
struct SynchronizedReader_t1724;
const Il2CppTypeDefinitionMetadata SynchronizedReader_t1724_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizedReader_t1724_InterfacesOffsets/* interfaceOffsets */
	, &TextReader_t1644_0_0_0/* parent */
	, SynchronizedReader_t1724_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7425/* fieldStart */
	, 10868/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SynchronizedReader_t1724_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizedReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &SynchronizedReader_t1724_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SynchronizedReader_t1724_0_0_0/* byval_arg */
	, &SynchronizedReader_t1724_1_0_0/* this_arg */
	, &SynchronizedReader_t1724_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizedReader_t1724)/* instance_size */
	, sizeof (SynchronizedReader_t1724)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextWriter
#include "mscorlib_System_IO_TextWriter.h"
// Metadata Definition System.IO.TextWriter
extern TypeInfo TextWriter_t1538_il2cpp_TypeInfo;
// System.IO.TextWriter
#include "mscorlib_System_IO_TextWriterMethodDeclarations.h"
extern const Il2CppType NullTextWriter_t1725_0_0_0;
static const Il2CppType* TextWriter_t1538_il2cpp_TypeInfo__nestedTypes[1] =
{
	&NullTextWriter_t1725_0_0_0,
};
static const EncodedMethodIndex TextWriter_t1538_VTable[14] = 
{
	626,
	601,
	627,
	628,
	3304,
	3333,
	3334,
	3335,
	3336,
	3337,
	3338,
	3339,
	3312,
	3313,
};
static const Il2CppType* TextWriter_t1538_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair TextWriter_t1538_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TextWriter_t1538_1_0_0;
struct TextWriter_t1538;
const Il2CppTypeDefinitionMetadata TextWriter_t1538_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextWriter_t1538_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, TextWriter_t1538_InterfacesTypeInfos/* implementedInterfaces */
	, TextWriter_t1538_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextWriter_t1538_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7426/* fieldStart */
	, 10874/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TextWriter_t1538_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &TextWriter_t1538_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2776/* custom_attributes_cache */
	, &TextWriter_t1538_0_0_0/* byval_arg */
	, &TextWriter_t1538_1_0_0/* this_arg */
	, &TextWriter_t1538_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextWriter_t1538)/* instance_size */
	, sizeof (TextWriter_t1538)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextWriter_t1538_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.TextWriter/NullTextWriter
#include "mscorlib_System_IO_TextWriter_NullTextWriter.h"
// Metadata Definition System.IO.TextWriter/NullTextWriter
extern TypeInfo NullTextWriter_t1725_il2cpp_TypeInfo;
// System.IO.TextWriter/NullTextWriter
#include "mscorlib_System_IO_TextWriter_NullTextWriterMethodDeclarations.h"
static const EncodedMethodIndex NullTextWriter_t1725_VTable[14] = 
{
	626,
	601,
	627,
	628,
	3304,
	3333,
	3334,
	3335,
	3340,
	3337,
	3341,
	3342,
	3312,
	3313,
};
static Il2CppInterfaceOffsetPair NullTextWriter_t1725_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullTextWriter_t1725_1_0_0;
struct NullTextWriter_t1725;
const Il2CppTypeDefinitionMetadata NullTextWriter_t1725_DefinitionMetadata = 
{
	&TextWriter_t1538_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullTextWriter_t1725_InterfacesOffsets/* interfaceOffsets */
	, &TextWriter_t1538_0_0_0/* parent */
	, NullTextWriter_t1725_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10887/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullTextWriter_t1725_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullTextWriter"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &NullTextWriter_t1725_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NullTextWriter_t1725_0_0_0/* byval_arg */
	, &NullTextWriter_t1725_1_0_0/* this_arg */
	, &NullTextWriter_t1725_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullTextWriter_t1725)/* instance_size */
	, sizeof (NullTextWriter_t1725)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.SynchronizedWriter
#include "mscorlib_System_IO_SynchronizedWriter.h"
// Metadata Definition System.IO.SynchronizedWriter
extern TypeInfo SynchronizedWriter_t1726_il2cpp_TypeInfo;
// System.IO.SynchronizedWriter
#include "mscorlib_System_IO_SynchronizedWriterMethodDeclarations.h"
static const EncodedMethodIndex SynchronizedWriter_t1726_VTable[14] = 
{
	626,
	601,
	627,
	628,
	3304,
	3343,
	3334,
	3344,
	3345,
	3346,
	3347,
	3348,
	3349,
	3350,
};
static Il2CppInterfaceOffsetPair SynchronizedWriter_t1726_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizedWriter_t1726_0_0_0;
extern const Il2CppType SynchronizedWriter_t1726_1_0_0;
struct SynchronizedWriter_t1726;
const Il2CppTypeDefinitionMetadata SynchronizedWriter_t1726_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizedWriter_t1726_InterfacesOffsets/* interfaceOffsets */
	, &TextWriter_t1538_0_0_0/* parent */
	, SynchronizedWriter_t1726_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7428/* fieldStart */
	, 10891/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SynchronizedWriter_t1726_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizedWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &SynchronizedWriter_t1726_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SynchronizedWriter_t1726_0_0_0/* byval_arg */
	, &SynchronizedWriter_t1726_1_0_0/* this_arg */
	, &SynchronizedWriter_t1726_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizedWriter_t1726)/* instance_size */
	, sizeof (SynchronizedWriter_t1726)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.UnexceptionalStreamReader
#include "mscorlib_System_IO_UnexceptionalStreamReader.h"
// Metadata Definition System.IO.UnexceptionalStreamReader
extern TypeInfo UnexceptionalStreamReader_t1727_il2cpp_TypeInfo;
// System.IO.UnexceptionalStreamReader
#include "mscorlib_System_IO_UnexceptionalStreamReaderMethodDeclarations.h"
static const EncodedMethodIndex UnexceptionalStreamReader_t1727_VTable[11] = 
{
	626,
	601,
	627,
	628,
	3291,
	3292,
	3351,
	3352,
	3353,
	3354,
	3355,
};
static Il2CppInterfaceOffsetPair UnexceptionalStreamReader_t1727_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnexceptionalStreamReader_t1727_0_0_0;
extern const Il2CppType UnexceptionalStreamReader_t1727_1_0_0;
struct UnexceptionalStreamReader_t1727;
const Il2CppTypeDefinitionMetadata UnexceptionalStreamReader_t1727_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnexceptionalStreamReader_t1727_InterfacesOffsets/* interfaceOffsets */
	, &StreamReader_t1721_0_0_0/* parent */
	, UnexceptionalStreamReader_t1727_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7430/* fieldStart */
	, 10900/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnexceptionalStreamReader_t1727_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnexceptionalStreamReader"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &UnexceptionalStreamReader_t1727_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnexceptionalStreamReader_t1727_0_0_0/* byval_arg */
	, &UnexceptionalStreamReader_t1727_1_0_0/* this_arg */
	, &UnexceptionalStreamReader_t1727_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnexceptionalStreamReader_t1727)/* instance_size */
	, sizeof (UnexceptionalStreamReader_t1727)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UnexceptionalStreamReader_t1727_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.IO.UnexceptionalStreamWriter
#include "mscorlib_System_IO_UnexceptionalStreamWriter.h"
// Metadata Definition System.IO.UnexceptionalStreamWriter
extern TypeInfo UnexceptionalStreamWriter_t1728_il2cpp_TypeInfo;
// System.IO.UnexceptionalStreamWriter
#include "mscorlib_System_IO_UnexceptionalStreamWriterMethodDeclarations.h"
static const EncodedMethodIndex UnexceptionalStreamWriter_t1728_VTable[15] = 
{
	626,
	3303,
	627,
	628,
	3304,
	3305,
	3306,
	3356,
	3357,
	3358,
	3359,
	3360,
	3312,
	3313,
	3314,
};
static Il2CppInterfaceOffsetPair UnexceptionalStreamWriter_t1728_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnexceptionalStreamWriter_t1728_0_0_0;
extern const Il2CppType UnexceptionalStreamWriter_t1728_1_0_0;
struct UnexceptionalStreamWriter_t1728;
const Il2CppTypeDefinitionMetadata UnexceptionalStreamWriter_t1728_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnexceptionalStreamWriter_t1728_InterfacesOffsets/* interfaceOffsets */
	, &StreamWriter_t1722_0_0_0/* parent */
	, UnexceptionalStreamWriter_t1728_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 10908/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnexceptionalStreamWriter_t1728_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnexceptionalStreamWriter"/* name */
	, "System.IO"/* namespaze */
	, NULL/* methods */
	, &UnexceptionalStreamWriter_t1728_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnexceptionalStreamWriter_t1728_0_0_0/* byval_arg */
	, &UnexceptionalStreamWriter_t1728_1_0_0/* this_arg */
	, &UnexceptionalStreamWriter_t1728_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnexceptionalStreamWriter_t1728)/* instance_size */
	, sizeof (UnexceptionalStreamWriter_t1728)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
// Metadata Definition System.Reflection.Emit.AssemblyBuilder
extern TypeInfo AssemblyBuilder_t1731_il2cpp_TypeInfo;
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilderMethodDeclarations.h"
static const EncodedMethodIndex AssemblyBuilder_t1731_VTable[21] = 
{
	626,
	601,
	627,
	3361,
	3362,
	3363,
	3364,
	3365,
	3363,
	3362,
	3366,
	3367,
	3368,
	3369,
	3370,
	3371,
	3372,
	3373,
	3374,
	3375,
	3376,
};
extern const Il2CppType _AssemblyBuilder_t3466_0_0_0;
static const Il2CppType* AssemblyBuilder_t1731_InterfacesTypeInfos[] = 
{
	&_AssemblyBuilder_t3466_0_0_0,
};
extern const Il2CppType ICustomAttributeProvider_t2188_0_0_0;
extern const Il2CppType _Assembly_t3476_0_0_0;
static Il2CppInterfaceOffsetPair AssemblyBuilder_t1731_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_Assembly_t3476_0_0_0, 6},
	{ &_AssemblyBuilder_t3466_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyBuilder_t1731_0_0_0;
extern const Il2CppType AssemblyBuilder_t1731_1_0_0;
extern const Il2CppType Assembly_t1535_0_0_0;
struct AssemblyBuilder_t1731;
const Il2CppTypeDefinitionMetadata AssemblyBuilder_t1731_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AssemblyBuilder_t1731_InterfacesTypeInfos/* implementedInterfaces */
	, AssemblyBuilder_t1731_InterfacesOffsets/* interfaceOffsets */
	, &Assembly_t1535_0_0_0/* parent */
	, AssemblyBuilder_t1731_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7432/* fieldStart */
	, 10914/* methodStart */
	, -1/* eventStart */
	, 2096/* propertyStart */

};
TypeInfo AssemblyBuilder_t1731_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &AssemblyBuilder_t1731_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2777/* custom_attributes_cache */
	, &AssemblyBuilder_t1731_0_0_0/* byval_arg */
	, &AssemblyBuilder_t1731_1_0_0/* this_arg */
	, &AssemblyBuilder_t1731_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyBuilder_t1731)/* instance_size */
	, sizeof (AssemblyBuilder_t1731)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 21/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"
// Metadata Definition System.Reflection.Emit.ConstructorBuilder
extern TypeInfo ConstructorBuilder_t1736_il2cpp_TypeInfo;
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilderMethodDeclarations.h"
static const EncodedMethodIndex ConstructorBuilder_t1736_VTable[31] = 
{
	626,
	601,
	627,
	3377,
	3378,
	3379,
	3380,
	3381,
	3382,
	3383,
	3384,
	3379,
	3385,
	3378,
	3386,
	3387,
	3388,
	3389,
	3390,
	3391,
	3392,
	3393,
	3394,
	3395,
	3396,
	3397,
	3398,
	3399,
	3400,
	3401,
	3402,
};
extern const Il2CppType _ConstructorBuilder_t3467_0_0_0;
static const Il2CppType* ConstructorBuilder_t1736_InterfacesTypeInfos[] = 
{
	&_ConstructorBuilder_t3467_0_0_0,
};
extern const Il2CppType _ConstructorInfo_t3478_0_0_0;
extern const Il2CppType _MethodBase_t3481_0_0_0;
extern const Il2CppType _MemberInfo_t3441_0_0_0;
static Il2CppInterfaceOffsetPair ConstructorBuilder_t1736_InterfacesOffsets[] = 
{
	{ &_ConstructorInfo_t3478_0_0_0, 30},
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_ConstructorBuilder_t3467_0_0_0, 31},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructorBuilder_t1736_0_0_0;
extern const Il2CppType ConstructorBuilder_t1736_1_0_0;
extern const Il2CppType ConstructorInfo_t996_0_0_0;
struct ConstructorBuilder_t1736;
const Il2CppTypeDefinitionMetadata ConstructorBuilder_t1736_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructorBuilder_t1736_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructorBuilder_t1736_InterfacesOffsets/* interfaceOffsets */
	, &ConstructorInfo_t996_0_0_0/* parent */
	, ConstructorBuilder_t1736_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7439/* fieldStart */
	, 10920/* methodStart */
	, -1/* eventStart */
	, 2098/* propertyStart */

};
TypeInfo ConstructorBuilder_t1736_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ConstructorBuilder_t1736_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2778/* custom_attributes_cache */
	, &ConstructorBuilder_t1736_0_0_0/* byval_arg */
	, &ConstructorBuilder_t1736_1_0_0/* this_arg */
	, &ConstructorBuilder_t1736_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorBuilder_t1736)/* instance_size */
	, sizeof (ConstructorBuilder_t1736)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 9/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilder.h"
// Metadata Definition System.Reflection.Emit.EnumBuilder
extern TypeInfo EnumBuilder_t1737_il2cpp_TypeInfo;
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilderMethodDeclarations.h"
static const EncodedMethodIndex EnumBuilder_t1737_VTable[81] = 
{
	2626,
	601,
	2627,
	2628,
	3403,
	3404,
	3405,
	2632,
	3406,
	3407,
	3408,
	3404,
	3409,
	3403,
	3410,
	3411,
	2634,
	3412,
	3413,
	2635,
	2636,
	2637,
	2638,
	2639,
	2640,
	2641,
	2642,
	2643,
	2644,
	2645,
	2646,
	2647,
	2648,
	2649,
	3414,
	3415,
	3416,
	2651,
	2652,
	3417,
	2653,
	2654,
	3418,
	3419,
	3420,
	3421,
	2655,
	2656,
	2657,
	2658,
	3422,
	3423,
	3424,
	2659,
	2660,
	2661,
	2662,
	3425,
	3426,
	3427,
	3428,
	3429,
	3430,
	3431,
	3432,
	3433,
	2664,
	2665,
	2666,
	2667,
	2668,
	2669,
	3434,
	3435,
	2670,
	2671,
	2672,
	2673,
	2674,
	2675,
	2676,
};
extern const Il2CppType _EnumBuilder_t3468_0_0_0;
static const Il2CppType* EnumBuilder_t1737_InterfacesTypeInfos[] = 
{
	&_EnumBuilder_t3468_0_0_0,
};
extern const Il2CppType IReflect_t3442_0_0_0;
extern const Il2CppType _Type_t3440_0_0_0;
static Il2CppInterfaceOffsetPair EnumBuilder_t1737_InterfacesOffsets[] = 
{
	{ &IReflect_t3442_0_0_0, 14},
	{ &_Type_t3440_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_EnumBuilder_t3468_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EnumBuilder_t1737_0_0_0;
extern const Il2CppType EnumBuilder_t1737_1_0_0;
extern const Il2CppType Type_t_0_0_0;
struct EnumBuilder_t1737;
const Il2CppTypeDefinitionMetadata EnumBuilder_t1737_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, EnumBuilder_t1737_InterfacesTypeInfos/* implementedInterfaces */
	, EnumBuilder_t1737_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, EnumBuilder_t1737_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7450/* fieldStart */
	, 10946/* methodStart */
	, -1/* eventStart */
	, 2107/* propertyStart */

};
TypeInfo EnumBuilder_t1737_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EnumBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &EnumBuilder_t1737_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2780/* custom_attributes_cache */
	, &EnumBuilder_t1737_0_0_0/* byval_arg */
	, &EnumBuilder_t1737_1_0_0/* this_arg */
	, &EnumBuilder_t1737_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EnumBuilder_t1737)/* instance_size */
	, sizeof (EnumBuilder_t1737)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 34/* method_count */
	, 11/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 81/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"
// Metadata Definition System.Reflection.Emit.FieldBuilder
extern TypeInfo FieldBuilder_t1739_il2cpp_TypeInfo;
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilderMethodDeclarations.h"
static const EncodedMethodIndex FieldBuilder_t1739_VTable[27] = 
{
	626,
	601,
	627,
	628,
	3436,
	3437,
	3438,
	3439,
	3440,
	3441,
	3442,
	3437,
	3443,
	3436,
	3444,
	3445,
	3446,
	3447,
	3448,
	3449,
	3450,
	3451,
	3452,
	3453,
	3454,
	3455,
	3456,
};
extern const Il2CppType _FieldBuilder_t3469_0_0_0;
static const Il2CppType* FieldBuilder_t1739_InterfacesTypeInfos[] = 
{
	&_FieldBuilder_t3469_0_0_0,
};
extern const Il2CppType _FieldInfo_t3480_0_0_0;
static Il2CppInterfaceOffsetPair FieldBuilder_t1739_InterfacesOffsets[] = 
{
	{ &_FieldInfo_t3480_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_FieldBuilder_t3469_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldBuilder_t1739_0_0_0;
extern const Il2CppType FieldBuilder_t1739_1_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
struct FieldBuilder_t1739;
const Il2CppTypeDefinitionMetadata FieldBuilder_t1739_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FieldBuilder_t1739_InterfacesTypeInfos/* implementedInterfaces */
	, FieldBuilder_t1739_InterfacesOffsets/* interfaceOffsets */
	, &FieldInfo_t_0_0_0/* parent */
	, FieldBuilder_t1739_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7452/* fieldStart */
	, 10980/* methodStart */
	, -1/* eventStart */
	, 2118/* propertyStart */

};
TypeInfo FieldBuilder_t1739_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &FieldBuilder_t1739_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2782/* custom_attributes_cache */
	, &FieldBuilder_t1739_0_0_0/* byval_arg */
	, &FieldBuilder_t1739_1_0_0/* this_arg */
	, &FieldBuilder_t1739_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldBuilder_t1739)/* instance_size */
	, sizeof (FieldBuilder_t1739)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 8/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Emit.GenericTypeParameterBuilder
#include "mscorlib_System_Reflection_Emit_GenericTypeParameterBuilder.h"
// Metadata Definition System.Reflection.Emit.GenericTypeParameterBuilder
extern TypeInfo GenericTypeParameterBuilder_t1741_il2cpp_TypeInfo;
// System.Reflection.Emit.GenericTypeParameterBuilder
#include "mscorlib_System_Reflection_Emit_GenericTypeParameterBuilderMethodDeclarations.h"
static const EncodedMethodIndex GenericTypeParameterBuilder_t1741_VTable[81] = 
{
	3457,
	601,
	3458,
	3459,
	3460,
	3461,
	3462,
	2632,
	3463,
	3464,
	3465,
	3461,
	3466,
	3460,
	3467,
	3468,
	2634,
	3469,
	3470,
	2635,
	2636,
	2637,
	2638,
	2639,
	2640,
	2641,
	2642,
	2643,
	2644,
	2645,
	2646,
	2647,
	2648,
	2649,
	3471,
	3472,
	3473,
	2651,
	3474,
	3475,
	3476,
	3477,
	3478,
	3479,
	3480,
	3481,
	2655,
	2656,
	2657,
	2658,
	3482,
	3483,
	3484,
	2659,
	2660,
	2661,
	2662,
	3485,
	3486,
	3487,
	3488,
	3489,
	3490,
	3491,
	3492,
	3493,
	2664,
	2665,
	2666,
	2667,
	2668,
	2669,
	3494,
	3495,
	3496,
	3497,
	3498,
	3499,
	3500,
	3501,
	3502,
};
static Il2CppInterfaceOffsetPair GenericTypeParameterBuilder_t1741_InterfacesOffsets[] = 
{
	{ &IReflect_t3442_0_0_0, 14},
	{ &_Type_t3440_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GenericTypeParameterBuilder_t1741_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t1741_1_0_0;
struct GenericTypeParameterBuilder_t1741;
const Il2CppTypeDefinitionMetadata GenericTypeParameterBuilder_t1741_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericTypeParameterBuilder_t1741_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, GenericTypeParameterBuilder_t1741_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7457/* fieldStart */
	, 10995/* methodStart */
	, -1/* eventStart */
	, 2126/* propertyStart */

};
TypeInfo GenericTypeParameterBuilder_t1741_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericTypeParameterBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &GenericTypeParameterBuilder_t1741_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2783/* custom_attributes_cache */
	, &GenericTypeParameterBuilder_t1741_0_0_0/* byval_arg */
	, &GenericTypeParameterBuilder_t1741_1_0_0/* this_arg */
	, &GenericTypeParameterBuilder_t1741_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericTypeParameterBuilder_t1741)/* instance_size */
	, sizeof (GenericTypeParameterBuilder_t1741)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 15/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 81/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
// Metadata Definition System.Reflection.Emit.ILTokenInfo
extern TypeInfo ILTokenInfo_t1742_il2cpp_TypeInfo;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfoMethodDeclarations.h"
static const EncodedMethodIndex ILTokenInfo_t1742_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ILTokenInfo_t1742_0_0_0;
extern const Il2CppType ILTokenInfo_t1742_1_0_0;
const Il2CppTypeDefinitionMetadata ILTokenInfo_t1742_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, ILTokenInfo_t1742_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7461/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ILTokenInfo_t1742_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILTokenInfo"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ILTokenInfo_t1742_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILTokenInfo_t1742_0_0_0/* byval_arg */
	, &ILTokenInfo_t1742_1_0_0/* this_arg */
	, &ILTokenInfo_t1742_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ILTokenInfo_t1742)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ILTokenInfo_t1742)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Reflection.Emit.TokenGenerator
extern TypeInfo TokenGenerator_t1749_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TokenGenerator_t1749_0_0_0;
extern const Il2CppType TokenGenerator_t1749_1_0_0;
struct TokenGenerator_t1749;
const Il2CppTypeDefinitionMetadata TokenGenerator_t1749_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11042/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TokenGenerator_t1749_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TokenGenerator"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &TokenGenerator_t1749_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TokenGenerator_t1749_0_0_0/* byval_arg */
	, &TokenGenerator_t1749_1_0_0/* this_arg */
	, &TokenGenerator_t1749_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ILGenerator
#include "mscorlib_System_Reflection_Emit_ILGenerator.h"
// Metadata Definition System.Reflection.Emit.ILGenerator
extern TypeInfo ILGenerator_t1732_il2cpp_TypeInfo;
// System.Reflection.Emit.ILGenerator
#include "mscorlib_System_Reflection_Emit_ILGeneratorMethodDeclarations.h"
extern const Il2CppType LabelFixup_t1743_0_0_0;
extern const Il2CppType LabelData_t1744_0_0_0;
static const Il2CppType* ILGenerator_t1732_il2cpp_TypeInfo__nestedTypes[2] =
{
	&LabelFixup_t1743_0_0_0,
	&LabelData_t1744_0_0_0,
};
static const EncodedMethodIndex ILGenerator_t1732_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3503,
	3504,
};
extern const Il2CppType _ILGenerator_t3470_0_0_0;
static const Il2CppType* ILGenerator_t1732_InterfacesTypeInfos[] = 
{
	&_ILGenerator_t3470_0_0_0,
};
static Il2CppInterfaceOffsetPair ILGenerator_t1732_InterfacesOffsets[] = 
{
	{ &_ILGenerator_t3470_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ILGenerator_t1732_0_0_0;
extern const Il2CppType ILGenerator_t1732_1_0_0;
struct ILGenerator_t1732;
const Il2CppTypeDefinitionMetadata ILGenerator_t1732_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ILGenerator_t1732_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ILGenerator_t1732_InterfacesTypeInfos/* implementedInterfaces */
	, ILGenerator_t1732_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ILGenerator_t1732_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7463/* fieldStart */
	, 11043/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ILGenerator_t1732_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILGenerator"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ILGenerator_t1732_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2790/* custom_attributes_cache */
	, &ILGenerator_t1732_0_0_0/* byval_arg */
	, &ILGenerator_t1732_1_0_0/* this_arg */
	, &ILGenerator_t1732_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ILGenerator_t1732)/* instance_size */
	, sizeof (ILGenerator_t1732)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ILGenerator_t1732_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
// Metadata Definition System.Reflection.Emit.ILGenerator/LabelFixup
extern TypeInfo LabelFixup_t1743_il2cpp_TypeInfo;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixupMethodDeclarations.h"
static const EncodedMethodIndex LabelFixup_t1743_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LabelFixup_t1743_1_0_0;
const Il2CppTypeDefinitionMetadata LabelFixup_t1743_DefinitionMetadata = 
{
	&ILGenerator_t1732_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, LabelFixup_t1743_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7475/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LabelFixup_t1743_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LabelFixup"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LabelFixup_t1743_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LabelFixup_t1743_0_0_0/* byval_arg */
	, &LabelFixup_t1743_1_0_0/* this_arg */
	, &LabelFixup_t1743_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LabelFixup_t1743)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LabelFixup_t1743)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(LabelFixup_t1743 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
// Metadata Definition System.Reflection.Emit.ILGenerator/LabelData
extern TypeInfo LabelData_t1744_il2cpp_TypeInfo;
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelDataMethodDeclarations.h"
static const EncodedMethodIndex LabelData_t1744_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LabelData_t1744_1_0_0;
const Il2CppTypeDefinitionMetadata LabelData_t1744_DefinitionMetadata = 
{
	&ILGenerator_t1732_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, LabelData_t1744_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7478/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LabelData_t1744_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LabelData"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LabelData_t1744_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LabelData_t1744_0_0_0/* byval_arg */
	, &LabelData_t1744_1_0_0/* this_arg */
	, &LabelData_t1744_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LabelData_t1744)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LabelData_t1744)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(LabelData_t1744 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"
// Metadata Definition System.Reflection.Emit.MethodBuilder
extern TypeInfo MethodBuilder_t1740_il2cpp_TypeInfo;
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilderMethodDeclarations.h"
static const EncodedMethodIndex MethodBuilder_t1740_VTable[33] = 
{
	3505,
	601,
	3506,
	3507,
	3508,
	3509,
	3510,
	3511,
	3512,
	3513,
	3514,
	3509,
	3515,
	3508,
	3516,
	3517,
	3388,
	3518,
	3519,
	3520,
	3521,
	3393,
	3394,
	3395,
	3396,
	3522,
	3523,
	3524,
	3525,
	3526,
	3527,
	3528,
	3529,
};
extern const Il2CppType _MethodBuilder_t3471_0_0_0;
static const Il2CppType* MethodBuilder_t1740_InterfacesTypeInfos[] = 
{
	&_MethodBuilder_t3471_0_0_0,
};
extern const Il2CppType _MethodInfo_t3482_0_0_0;
static Il2CppInterfaceOffsetPair MethodBuilder_t1740_InterfacesOffsets[] = 
{
	{ &_MethodInfo_t3482_0_0_0, 30},
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_MethodBuilder_t3471_0_0_0, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodBuilder_t1740_0_0_0;
extern const Il2CppType MethodBuilder_t1740_1_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
struct MethodBuilder_t1740;
const Il2CppTypeDefinitionMetadata MethodBuilder_t1740_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodBuilder_t1740_InterfacesTypeInfos/* implementedInterfaces */
	, MethodBuilder_t1740_InterfacesOffsets/* interfaceOffsets */
	, &MethodInfo_t_0_0_0/* parent */
	, MethodBuilder_t1740_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7480/* fieldStart */
	, 11053/* methodStart */
	, -1/* eventStart */
	, 2141/* propertyStart */

};
TypeInfo MethodBuilder_t1740_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &MethodBuilder_t1740_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2793/* custom_attributes_cache */
	, &MethodBuilder_t1740_0_0_0/* byval_arg */
	, &MethodBuilder_t1740_1_0_0/* this_arg */
	, &MethodBuilder_t1740_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodBuilder_t1740)/* instance_size */
	, sizeof (MethodBuilder_t1740)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 11/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
// Metadata Definition System.Reflection.Emit.MethodToken
extern TypeInfo MethodToken_t1751_il2cpp_TypeInfo;
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodTokenMethodDeclarations.h"
static const EncodedMethodIndex MethodToken_t1751_VTable[4] = 
{
	3530,
	601,
	3531,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodToken_t1751_0_0_0;
extern const Il2CppType MethodToken_t1751_1_0_0;
const Il2CppTypeDefinitionMetadata MethodToken_t1751_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MethodToken_t1751_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7492/* fieldStart */
	, 11080/* methodStart */
	, -1/* eventStart */
	, 2152/* propertyStart */

};
TypeInfo MethodToken_t1751_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodToken"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &MethodToken_t1751_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2796/* custom_attributes_cache */
	, &MethodToken_t1751_0_0_0/* byval_arg */
	, &MethodToken_t1751_1_0_0/* this_arg */
	, &MethodToken_t1751_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodToken_t1751)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodToken_t1751)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(MethodToken_t1751 )/* native_size */
	, sizeof(MethodToken_t1751_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"
// Metadata Definition System.Reflection.Emit.ModuleBuilder
extern TypeInfo ModuleBuilder_t1754_il2cpp_TypeInfo;
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilderMethodDeclarations.h"
static const EncodedMethodIndex ModuleBuilder_t1754_VTable[11] = 
{
	626,
	601,
	627,
	3532,
	3533,
	3534,
	3535,
	3534,
	3533,
	3536,
	3535,
};
extern const Il2CppType _ModuleBuilder_t3472_0_0_0;
static const Il2CppType* ModuleBuilder_t1754_InterfacesTypeInfos[] = 
{
	&_ModuleBuilder_t3472_0_0_0,
};
extern const Il2CppType _Module_t3483_0_0_0;
static Il2CppInterfaceOffsetPair ModuleBuilder_t1754_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &ICustomAttributeProvider_t2188_0_0_0, 5},
	{ &_Module_t3483_0_0_0, 7},
	{ &_ModuleBuilder_t3472_0_0_0, 11},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ModuleBuilder_t1754_0_0_0;
extern const Il2CppType ModuleBuilder_t1754_1_0_0;
extern const Il2CppType Module_t1748_0_0_0;
struct ModuleBuilder_t1754;
const Il2CppTypeDefinitionMetadata ModuleBuilder_t1754_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ModuleBuilder_t1754_InterfacesTypeInfos/* implementedInterfaces */
	, ModuleBuilder_t1754_InterfacesOffsets/* interfaceOffsets */
	, &Module_t1748_0_0_0/* parent */
	, ModuleBuilder_t1754_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7494/* fieldStart */
	, 11085/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ModuleBuilder_t1754_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ModuleBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ModuleBuilder_t1754_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2797/* custom_attributes_cache */
	, &ModuleBuilder_t1754_0_0_0/* byval_arg */
	, &ModuleBuilder_t1754_1_0_0/* this_arg */
	, &ModuleBuilder_t1754_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ModuleBuilder_t1754)/* instance_size */
	, sizeof (ModuleBuilder_t1754)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ModuleBuilder_t1754_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Emit.ModuleBuilderTokenGenerator
#include "mscorlib_System_Reflection_Emit_ModuleBuilderTokenGenerator.h"
// Metadata Definition System.Reflection.Emit.ModuleBuilderTokenGenerator
extern TypeInfo ModuleBuilderTokenGenerator_t1753_il2cpp_TypeInfo;
// System.Reflection.Emit.ModuleBuilderTokenGenerator
#include "mscorlib_System_Reflection_Emit_ModuleBuilderTokenGeneratorMethodDeclarations.h"
static const EncodedMethodIndex ModuleBuilderTokenGenerator_t1753_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3537,
};
static const Il2CppType* ModuleBuilderTokenGenerator_t1753_InterfacesTypeInfos[] = 
{
	&TokenGenerator_t1749_0_0_0,
};
static Il2CppInterfaceOffsetPair ModuleBuilderTokenGenerator_t1753_InterfacesOffsets[] = 
{
	{ &TokenGenerator_t1749_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ModuleBuilderTokenGenerator_t1753_0_0_0;
extern const Il2CppType ModuleBuilderTokenGenerator_t1753_1_0_0;
struct ModuleBuilderTokenGenerator_t1753;
const Il2CppTypeDefinitionMetadata ModuleBuilderTokenGenerator_t1753_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ModuleBuilderTokenGenerator_t1753_InterfacesTypeInfos/* implementedInterfaces */
	, ModuleBuilderTokenGenerator_t1753_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ModuleBuilderTokenGenerator_t1753_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7500/* fieldStart */
	, 11092/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ModuleBuilderTokenGenerator_t1753_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ModuleBuilderTokenGenerator"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ModuleBuilderTokenGenerator_t1753_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ModuleBuilderTokenGenerator_t1753_0_0_0/* byval_arg */
	, &ModuleBuilderTokenGenerator_t1753_1_0_0/* this_arg */
	, &ModuleBuilderTokenGenerator_t1753_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ModuleBuilderTokenGenerator_t1753)/* instance_size */
	, sizeof (ModuleBuilderTokenGenerator_t1753)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCode.h"
// Metadata Definition System.Reflection.Emit.OpCode
extern TypeInfo OpCode_t1755_il2cpp_TypeInfo;
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCodeMethodDeclarations.h"
static const EncodedMethodIndex OpCode_t1755_VTable[4] = 
{
	3538,
	601,
	3539,
	3540,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OpCode_t1755_0_0_0;
extern const Il2CppType OpCode_t1755_1_0_0;
const Il2CppTypeDefinitionMetadata OpCode_t1755_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, OpCode_t1755_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7501/* fieldStart */
	, 11094/* methodStart */
	, -1/* eventStart */
	, 2153/* propertyStart */

};
TypeInfo OpCode_t1755_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCode"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &OpCode_t1755_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2798/* custom_attributes_cache */
	, &OpCode_t1755_0_0_0/* byval_arg */
	, &OpCode_t1755_1_0_0/* this_arg */
	, &OpCode_t1755_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCode_t1755)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (OpCode_t1755)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(OpCode_t1755 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.OpCodeNames
#include "mscorlib_System_Reflection_Emit_OpCodeNames.h"
// Metadata Definition System.Reflection.Emit.OpCodeNames
extern TypeInfo OpCodeNames_t1756_il2cpp_TypeInfo;
// System.Reflection.Emit.OpCodeNames
#include "mscorlib_System_Reflection_Emit_OpCodeNamesMethodDeclarations.h"
static const EncodedMethodIndex OpCodeNames_t1756_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OpCodeNames_t1756_0_0_0;
extern const Il2CppType OpCodeNames_t1756_1_0_0;
struct OpCodeNames_t1756;
const Il2CppTypeDefinitionMetadata OpCodeNames_t1756_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OpCodeNames_t1756_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7509/* fieldStart */
	, 11102/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpCodeNames_t1756_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCodeNames"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &OpCodeNames_t1756_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OpCodeNames_t1756_0_0_0/* byval_arg */
	, &OpCodeNames_t1756_1_0_0/* this_arg */
	, &OpCodeNames_t1756_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCodeNames_t1756)/* instance_size */
	, sizeof (OpCodeNames_t1756)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(OpCodeNames_t1756_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.OpCodes
#include "mscorlib_System_Reflection_Emit_OpCodes.h"
// Metadata Definition System.Reflection.Emit.OpCodes
extern TypeInfo OpCodes_t1757_il2cpp_TypeInfo;
// System.Reflection.Emit.OpCodes
#include "mscorlib_System_Reflection_Emit_OpCodesMethodDeclarations.h"
static const EncodedMethodIndex OpCodes_t1757_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OpCodes_t1757_0_0_0;
extern const Il2CppType OpCodes_t1757_1_0_0;
struct OpCodes_t1757;
const Il2CppTypeDefinitionMetadata OpCodes_t1757_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OpCodes_t1757_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7510/* fieldStart */
	, 11103/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OpCodes_t1757_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OpCodes"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &OpCodes_t1757_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2799/* custom_attributes_cache */
	, &OpCodes_t1757_0_0_0/* byval_arg */
	, &OpCodes_t1757_1_0_0/* this_arg */
	, &OpCodes_t1757_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OpCodes_t1757)/* instance_size */
	, sizeof (OpCodes_t1757)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(OpCodes_t1757_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 226/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"
// Metadata Definition System.Reflection.Emit.ParameterBuilder
extern TypeInfo ParameterBuilder_t1758_il2cpp_TypeInfo;
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilderMethodDeclarations.h"
static const EncodedMethodIndex ParameterBuilder_t1758_VTable[7] = 
{
	626,
	601,
	627,
	628,
	3541,
	3542,
	3543,
};
extern const Il2CppType _ParameterBuilder_t3473_0_0_0;
static const Il2CppType* ParameterBuilder_t1758_InterfacesTypeInfos[] = 
{
	&_ParameterBuilder_t3473_0_0_0,
};
static Il2CppInterfaceOffsetPair ParameterBuilder_t1758_InterfacesOffsets[] = 
{
	{ &_ParameterBuilder_t3473_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterBuilder_t1758_0_0_0;
extern const Il2CppType ParameterBuilder_t1758_1_0_0;
struct ParameterBuilder_t1758;
const Il2CppTypeDefinitionMetadata ParameterBuilder_t1758_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ParameterBuilder_t1758_InterfacesTypeInfos/* implementedInterfaces */
	, ParameterBuilder_t1758_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ParameterBuilder_t1758_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7736/* fieldStart */
	, 11104/* methodStart */
	, -1/* eventStart */
	, 2157/* propertyStart */

};
TypeInfo ParameterBuilder_t1758_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &ParameterBuilder_t1758_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2801/* custom_attributes_cache */
	, &ParameterBuilder_t1758_0_0_0/* byval_arg */
	, &ParameterBuilder_t1758_1_0_0/* this_arg */
	, &ParameterBuilder_t1758_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterBuilder_t1758)/* instance_size */
	, sizeof (ParameterBuilder_t1758)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Emit.PropertyBuilder
#include "mscorlib_System_Reflection_Emit_PropertyBuilder.h"
// Metadata Definition System.Reflection.Emit.PropertyBuilder
extern TypeInfo PropertyBuilder_t1759_il2cpp_TypeInfo;
// System.Reflection.Emit.PropertyBuilder
#include "mscorlib_System_Reflection_Emit_PropertyBuilderMethodDeclarations.h"
static const EncodedMethodIndex PropertyBuilder_t1759_VTable[28] = 
{
	626,
	601,
	627,
	628,
	3544,
	3545,
	3546,
	3547,
	3548,
	3549,
	3550,
	3545,
	3551,
	3544,
	3552,
	3553,
	3554,
	3555,
	3556,
	3557,
	3558,
	3559,
	3560,
	3561,
	3562,
	3563,
	3564,
	3565,
};
extern const Il2CppType _PropertyBuilder_t3474_0_0_0;
static const Il2CppType* PropertyBuilder_t1759_InterfacesTypeInfos[] = 
{
	&_PropertyBuilder_t3474_0_0_0,
};
extern const Il2CppType _PropertyInfo_t3485_0_0_0;
static Il2CppInterfaceOffsetPair PropertyBuilder_t1759_InterfacesOffsets[] = 
{
	{ &_PropertyInfo_t3485_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_PropertyBuilder_t3474_0_0_0, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyBuilder_t1759_0_0_0;
extern const Il2CppType PropertyBuilder_t1759_1_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
struct PropertyBuilder_t1759;
const Il2CppTypeDefinitionMetadata PropertyBuilder_t1759_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PropertyBuilder_t1759_InterfacesTypeInfos/* implementedInterfaces */
	, PropertyBuilder_t1759_InterfacesOffsets/* interfaceOffsets */
	, &PropertyInfo_t_0_0_0/* parent */
	, PropertyBuilder_t1759_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7739/* fieldStart */
	, 11107/* methodStart */
	, -1/* eventStart */
	, 2160/* propertyStart */

};
TypeInfo PropertyBuilder_t1759_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &PropertyBuilder_t1759_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2802/* custom_attributes_cache */
	, &PropertyBuilder_t1759_0_0_0/* byval_arg */
	, &PropertyBuilder_t1759_1_0_0/* this_arg */
	, &PropertyBuilder_t1759_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyBuilder_t1759)/* instance_size */
	, sizeof (PropertyBuilder_t1759)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 8/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
