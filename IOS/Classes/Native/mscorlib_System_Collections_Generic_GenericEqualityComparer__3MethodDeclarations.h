﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Char>
struct GenericEqualityComparer_1_t2294;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Char>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14463_gshared (GenericEqualityComparer_1_t2294 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14463(__this, method) (( void (*) (GenericEqualityComparer_1_t2294 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m14463_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Char>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m14464_gshared (GenericEqualityComparer_1_t2294 * __this, uint16_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m14464(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2294 *, uint16_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m14464_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Char>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m14465_gshared (GenericEqualityComparer_1_t2294 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m14465(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2294 *, uint16_t, uint16_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m14465_gshared)(__this, ___x, ___y, method)
