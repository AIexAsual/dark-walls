﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// iTween
struct iTween_t432;
// System.Object
#include "mscorlib_System_Object.h"
// iTween/<Start>c__Iterator1C
struct  U3CStartU3Ec__Iterator1C_t435  : public Object_t
{
	// System.Int32 iTween/<Start>c__Iterator1C::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<Start>c__Iterator1C::$current
	Object_t * ___U24current_1;
	// iTween iTween/<Start>c__Iterator1C::<>f__this
	iTween_t432 * ___U3CU3Ef__this_2;
};
