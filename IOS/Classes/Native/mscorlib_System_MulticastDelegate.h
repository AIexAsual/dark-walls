﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t219;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct  MulticastDelegate_t219  : public Delegate_t480
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t219 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t219 * ___kpm_next_10;
};
