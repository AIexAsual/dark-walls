﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t2695;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t810;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1087;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3181;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m20240_gshared (ReadOnlyCollection_1_t2695 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m20240(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2695 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m20240_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241_gshared (ReadOnlyCollection_1_t2695 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2695 *, UILineInfo_t809 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m20241_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20242_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20242(__this, method) (( void (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m20242_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20243_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, UILineInfo_t809  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20243(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2695 *, int32_t, UILineInfo_t809 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m20243_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20244_gshared (ReadOnlyCollection_1_t2695 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20244(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2695 *, UILineInfo_t809 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m20244_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20245_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20245(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2695 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m20245_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UILineInfo_t809  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20246_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20246(__this, ___index, method) (( UILineInfo_t809  (*) (ReadOnlyCollection_1_t2695 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m20246_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20247_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, UILineInfo_t809  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20247(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2695 *, int32_t, UILineInfo_t809 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m20247_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20248_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20248(__this, method) (( bool (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20248_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20249_gshared (ReadOnlyCollection_1_t2695 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20249(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2695 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m20249_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20250_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20250(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m20250_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m20251_gshared (ReadOnlyCollection_1_t2695 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m20251(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2695 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m20251_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m20252_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m20252(__this, method) (( void (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m20252_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m20253_gshared (ReadOnlyCollection_1_t2695 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m20253(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2695 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m20253_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20254_gshared (ReadOnlyCollection_1_t2695 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20254(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2695 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m20254_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m20255_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m20255(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2695 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m20255_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m20256_gshared (ReadOnlyCollection_1_t2695 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m20256(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2695 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m20256_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20257_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20257(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2695 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m20257_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20258_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20258(__this, method) (( bool (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m20258_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20259_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20259(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m20259_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20260_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20260(__this, method) (( bool (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m20260_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20261_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20261(__this, method) (( bool (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m20261_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m20262_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m20262(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2695 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m20262_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m20263_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m20263(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2695 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m20263_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m20264_gshared (ReadOnlyCollection_1_t2695 * __this, UILineInfo_t809  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m20264(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2695 *, UILineInfo_t809 , const MethodInfo*))ReadOnlyCollection_1_Contains_m20264_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m20265_gshared (ReadOnlyCollection_1_t2695 * __this, UILineInfoU5BU5D_t1087* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m20265(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2695 *, UILineInfoU5BU5D_t1087*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m20265_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m20266_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m20266(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m20266_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m20267_gshared (ReadOnlyCollection_1_t2695 * __this, UILineInfo_t809  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m20267(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2695 *, UILineInfo_t809 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m20267_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m20268_gshared (ReadOnlyCollection_1_t2695 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m20268(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2695 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m20268_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t809  ReadOnlyCollection_1_get_Item_m20269_gshared (ReadOnlyCollection_1_t2695 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m20269(__this, ___index, method) (( UILineInfo_t809  (*) (ReadOnlyCollection_1_t2695 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m20269_gshared)(__this, ___index, method)
