﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$72
struct U24ArrayTypeU2472_t2163;
struct U24ArrayTypeU2472_t2163_marshaled;

void U24ArrayTypeU2472_t2163_marshal(const U24ArrayTypeU2472_t2163& unmarshaled, U24ArrayTypeU2472_t2163_marshaled& marshaled);
void U24ArrayTypeU2472_t2163_marshal_back(const U24ArrayTypeU2472_t2163_marshaled& marshaled, U24ArrayTypeU2472_t2163& unmarshaled);
void U24ArrayTypeU2472_t2163_marshal_cleanup(U24ArrayTypeU2472_t2163_marshaled& marshaled);
