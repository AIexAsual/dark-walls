﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct UnityEvent_1_t629;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t769;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1061;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::.ctor()
extern "C" void UnityEvent_1__ctor_m4651_gshared (UnityEvent_1_t629 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m4651(__this, method) (( void (*) (UnityEvent_1_t629 *, const MethodInfo*))UnityEvent_1__ctor_m4651_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m4654_gshared (UnityEvent_1_t629 * __this, UnityAction_1_t769 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m4654(__this, ___call, method) (( void (*) (UnityEvent_1_t629 *, UnityAction_1_t769 *, const MethodInfo*))UnityEvent_1_AddListener_m4654_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m17760_gshared (UnityEvent_1_t629 * __this, UnityAction_1_t769 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m17760(__this, ___call, method) (( void (*) (UnityEvent_1_t629 *, UnityAction_1_t769 *, const MethodInfo*))UnityEvent_1_RemoveListener_m17760_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m17761_gshared (UnityEvent_1_t629 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m17761(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t629 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m17761_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t1061 * UnityEvent_1_GetDelegate_m17762_gshared (UnityEvent_1_t629 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m17762(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t1061 * (*) (UnityEvent_1_t629 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m17762_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t1061 * UnityEvent_1_GetDelegate_m17763_gshared (Object_t * __this /* static, unused */, UnityAction_1_t769 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m17763(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t1061 * (*) (Object_t * /* static, unused */, UnityAction_1_t769 *, const MethodInfo*))UnityEvent_1_GetDelegate_m17763_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.Color>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m4653_gshared (UnityEvent_1_t629 * __this, Color_t6  ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m4653(__this, ___arg0, method) (( void (*) (UnityEvent_1_t629 *, Color_t6 , const MethodInfo*))UnityEvent_1_Invoke_m4653_gshared)(__this, ___arg0, method)
