﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>
struct Collection_1_t2252;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2247;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t2974;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t2250;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void Collection_1__ctor_m13848_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13848(__this, method) (( void (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1__ctor_m13848_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13849_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13849(__this, method) (( bool (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13849_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13850_gshared (Collection_1_t2252 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13850(__this, ___array, ___index, method) (( void (*) (Collection_1_t2252 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13850_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13851_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13851(__this, method) (( Object_t * (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13851_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13852_gshared (Collection_1_t2252 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13852(__this, ___value, method) (( int32_t (*) (Collection_1_t2252 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13852_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13853_gshared (Collection_1_t2252 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13853(__this, ___value, method) (( bool (*) (Collection_1_t2252 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13853_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13854_gshared (Collection_1_t2252 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13854(__this, ___value, method) (( int32_t (*) (Collection_1_t2252 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13854_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13855_gshared (Collection_1_t2252 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13855(__this, ___index, ___value, method) (( void (*) (Collection_1_t2252 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13855_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13856_gshared (Collection_1_t2252 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13856(__this, ___value, method) (( void (*) (Collection_1_t2252 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13856_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13857_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13857(__this, method) (( bool (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13858_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13858(__this, method) (( Object_t * (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13858_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13859_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13859(__this, method) (( bool (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13859_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13860_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13860(__this, method) (( bool (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13860_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13861_gshared (Collection_1_t2252 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13861(__this, ___index, method) (( Object_t * (*) (Collection_1_t2252 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13861_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13862_gshared (Collection_1_t2252 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13862(__this, ___index, ___value, method) (( void (*) (Collection_1_t2252 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13862_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void Collection_1_Add_m13863_gshared (Collection_1_t2252 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define Collection_1_Add_m13863(__this, ___item, method) (( void (*) (Collection_1_t2252 *, RaycastResult_t511 , const MethodInfo*))Collection_1_Add_m13863_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void Collection_1_Clear_m13864_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13864(__this, method) (( void (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_Clear_m13864_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m13865_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13865(__this, method) (( void (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_ClearItems_m13865_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool Collection_1_Contains_m13866_gshared (Collection_1_t2252 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define Collection_1_Contains_m13866(__this, ___item, method) (( bool (*) (Collection_1_t2252 *, RaycastResult_t511 , const MethodInfo*))Collection_1_Contains_m13866_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13867_gshared (Collection_1_t2252 * __this, RaycastResultU5BU5D_t2247* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13867(__this, ___array, ___index, method) (( void (*) (Collection_1_t2252 *, RaycastResultU5BU5D_t2247*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13867_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13868_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13868(__this, method) (( Object_t* (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_GetEnumerator_m13868_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13869_gshared (Collection_1_t2252 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13869(__this, ___item, method) (( int32_t (*) (Collection_1_t2252 *, RaycastResult_t511 , const MethodInfo*))Collection_1_IndexOf_m13869_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13870_gshared (Collection_1_t2252 * __this, int32_t ___index, RaycastResult_t511  ___item, const MethodInfo* method);
#define Collection_1_Insert_m13870(__this, ___index, ___item, method) (( void (*) (Collection_1_t2252 *, int32_t, RaycastResult_t511 , const MethodInfo*))Collection_1_Insert_m13870_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13871_gshared (Collection_1_t2252 * __this, int32_t ___index, RaycastResult_t511  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13871(__this, ___index, ___item, method) (( void (*) (Collection_1_t2252 *, int32_t, RaycastResult_t511 , const MethodInfo*))Collection_1_InsertItem_m13871_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool Collection_1_Remove_m13872_gshared (Collection_1_t2252 * __this, RaycastResult_t511  ___item, const MethodInfo* method);
#define Collection_1_Remove_m13872(__this, ___item, method) (( bool (*) (Collection_1_t2252 *, RaycastResult_t511 , const MethodInfo*))Collection_1_Remove_m13872_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13873_gshared (Collection_1_t2252 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13873(__this, ___index, method) (( void (*) (Collection_1_t2252 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13873_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13874_gshared (Collection_1_t2252 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13874(__this, ___index, method) (( void (*) (Collection_1_t2252 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13874_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13875_gshared (Collection_1_t2252 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13875(__this, method) (( int32_t (*) (Collection_1_t2252 *, const MethodInfo*))Collection_1_get_Count_m13875_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t511  Collection_1_get_Item_m13876_gshared (Collection_1_t2252 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13876(__this, ___index, method) (( RaycastResult_t511  (*) (Collection_1_t2252 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13876_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13877_gshared (Collection_1_t2252 * __this, int32_t ___index, RaycastResult_t511  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13877(__this, ___index, ___value, method) (( void (*) (Collection_1_t2252 *, int32_t, RaycastResult_t511 , const MethodInfo*))Collection_1_set_Item_m13877_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13878_gshared (Collection_1_t2252 * __this, int32_t ___index, RaycastResult_t511  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13878(__this, ___index, ___item, method) (( void (*) (Collection_1_t2252 *, int32_t, RaycastResult_t511 , const MethodInfo*))Collection_1_SetItem_m13878_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13879_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13879(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13879_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ConvertItem(System.Object)
extern "C" RaycastResult_t511  Collection_1_ConvertItem_m13880_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13880(__this /* static, unused */, ___item, method) (( RaycastResult_t511  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13880_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13881_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13881(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13881_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13882_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13882(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13882_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13883_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13883(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13883_gshared)(__this /* static, unused */, ___list, method)
