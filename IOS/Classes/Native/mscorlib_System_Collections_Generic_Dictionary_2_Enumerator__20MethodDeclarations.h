﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Enumerator_t2783;
// System.Object
struct Object_t;
// System.String
struct String_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t993;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Dictionary_2_t1143;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m21386(__this, ___dictionary, method) (( void (*) (Enumerator_t2783 *, Dictionary_2_t1143 *, const MethodInfo*))Enumerator__ctor_m14807_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21387(__this, method) (( Object_t * (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m21388(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14811_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m21389(__this, method) (( Object_t * (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m21390(__this, method) (( Object_t * (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14815_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::MoveNext()
#define Enumerator_MoveNext_m21391(__this, method) (( bool (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_MoveNext_m14817_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Current()
#define Enumerator_get_Current_m21392(__this, method) (( KeyValuePair_2_t1149  (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_get_Current_m14819_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m21393(__this, method) (( String_t* (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_get_CurrentKey_m14821_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m21394(__this, method) (( GetDelegate_t993 * (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_get_CurrentValue_m14823_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::VerifyState()
#define Enumerator_VerifyState_m21395(__this, method) (( void (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_VerifyState_m14825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m21396(__this, method) (( void (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_VerifyCurrent_m14827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Dispose()
#define Enumerator_Dispose_m21397(__this, method) (( void (*) (Enumerator_t2783 *, const MethodInfo*))Enumerator_Dispose_m14829_gshared)(__this, method)
