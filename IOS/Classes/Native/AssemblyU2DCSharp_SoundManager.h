﻿#pragma once
#include <stdint.h>
// SoundManager
struct SoundManager_t384;
// SFX
struct SFX_t385;
// System.Object
#include "mscorlib_System_Object.h"
// SoundManager
struct  SoundManager_t384  : public Object_t
{
	// SFX SoundManager::sfx
	SFX_t385 * ___sfx_1;
};
struct SoundManager_t384_StaticFields{
	// SoundManager SoundManager::_instance
	SoundManager_t384 * ____instance_0;
};
