﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Vision_Warp2
struct CameraFilterPack_Vision_Warp2_t211;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Vision_Warp2::.ctor()
extern "C" void CameraFilterPack_Vision_Warp2__ctor_m1373 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Warp2::get_material()
extern "C" Material_t2 * CameraFilterPack_Vision_Warp2_get_material_m1374 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::Start()
extern "C" void CameraFilterPack_Vision_Warp2_Start_m1375 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Vision_Warp2_OnRenderImage_m1376 (CameraFilterPack_Vision_Warp2_t211 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::OnValidate()
extern "C" void CameraFilterPack_Vision_Warp2_OnValidate_m1377 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::Update()
extern "C" void CameraFilterPack_Vision_Warp2_Update_m1378 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp2::OnDisable()
extern "C" void CameraFilterPack_Vision_Warp2_OnDisable_m1379 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
