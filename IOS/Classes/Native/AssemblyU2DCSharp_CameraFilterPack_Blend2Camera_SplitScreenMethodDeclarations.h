﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_SplitScreen
struct CameraFilterPack_Blend2Camera_SplitScreen_t44;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_SplitScreen::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_SplitScreen__ctor_m258 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_SplitScreen::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_SplitScreen_get_material_m259 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::Start()
extern "C" void CameraFilterPack_Blend2Camera_SplitScreen_Start_m260 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_SplitScreen_OnRenderImage_m261 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_SplitScreen_OnValidate_m262 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::Update()
extern "C" void CameraFilterPack_Blend2Camera_SplitScreen_Update_m263 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_SplitScreen_OnEnable_m264 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SplitScreen::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_SplitScreen_OnDisable_m265 (CameraFilterPack_Blend2Camera_SplitScreen_t44 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
