﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
struct InternalEnumerator_1_t2866;
// System.Object
struct Object_t;
// System.Security.Cryptography.KeySizes
struct KeySizes_t1195;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22251(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2866 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13479_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22252(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2866 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13480_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::Dispose()
#define InternalEnumerator_1_Dispose_m22253(__this, method) (( void (*) (InternalEnumerator_1_t2866 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13481_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22254(__this, method) (( bool (*) (InternalEnumerator_1_t2866 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13482_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
#define InternalEnumerator_1_get_Current_m22255(__this, method) (( KeySizes_t1195 * (*) (InternalEnumerator_1_t2866 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13483_gshared)(__this, method)
