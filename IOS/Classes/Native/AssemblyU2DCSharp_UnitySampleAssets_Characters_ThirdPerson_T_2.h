﻿#pragma once
#include <stdint.h>
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings
struct AdvancedSettings_t411;
// UnityEngine.Animator
struct Animator_t321;
// UnityEngine.CapsuleCollider
struct CapsuleCollider_t415;
// System.Collections.IComparer
struct IComparer_t416;
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct  ThirdPersonCharacter_t413  : public MonoBehaviour_t4
{
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::jumpPower
	float ___jumpPower_3;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::airSpeed
	float ___airSpeed_4;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::airControl
	float ___airControl_5;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::gravityMultiplier
	float ___gravityMultiplier_6;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::moveSpeedMultiplier
	float ___moveSpeedMultiplier_7;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::animSpeedMultiplier
	float ___animSpeedMultiplier_8;
	// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/AdvancedSettings UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::advancedSettings
	AdvancedSettings_t411 * ___advancedSettings_9;
	// UnityEngine.LayerMask UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::groundCheckMask
	LayerMask_t241  ___groundCheckMask_10;
	// UnityEngine.LayerMask UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::crouchCheckMask
	LayerMask_t241  ___crouchCheckMask_11;
	// System.Boolean UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::onGround
	bool ___onGround_12;
	// UnityEngine.Vector3 UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::currentLookPos
	Vector3_t215  ___currentLookPos_13;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::originalHeight
	float ___originalHeight_14;
	// UnityEngine.Animator UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::animator
	Animator_t321 * ___animator_15;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::lastAirTime
	float ___lastAirTime_16;
	// UnityEngine.CapsuleCollider UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::capsule
	CapsuleCollider_t415 * ___capsule_17;
	// UnityEngine.Vector3 UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::moveInput
	Vector3_t215  ___moveInput_18;
	// System.Boolean UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::crouchInput
	bool ___crouchInput_19;
	// System.Boolean UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::jumpInput
	bool ___jumpInput_20;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::turnAmount
	float ___turnAmount_21;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::forwardAmount
	float ___forwardAmount_22;
	// UnityEngine.Vector3 UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::velocity
	Vector3_t215  ___velocity_23;
	// System.Collections.IComparer UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::rayHitComparer
	Object_t * ___rayHitComparer_24;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::lookBlendTime
	float ___lookBlendTime_25;
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::lookWeight
	float ___lookWeight_26;
	// UnityEngine.Transform UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter::<lookTarget>k__BackingField
	Transform_t243 * ___U3ClookTargetU3Ek__BackingField_27;
};
