﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t2687;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1086;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t3178;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t812;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m20127_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1__ctor_m20127(__this, method) (( void (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1__ctor_m20127_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20128_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20128(__this, method) (( bool (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20128_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m20129_gshared (Collection_1_t2687 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m20129(__this, ___array, ___index, method) (( void (*) (Collection_1_t2687 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m20129_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m20130_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m20130(__this, method) (( Object_t * (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m20130_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m20131_gshared (Collection_1_t2687 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m20131(__this, ___value, method) (( int32_t (*) (Collection_1_t2687 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m20131_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m20132_gshared (Collection_1_t2687 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m20132(__this, ___value, method) (( bool (*) (Collection_1_t2687 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m20132_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m20133_gshared (Collection_1_t2687 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m20133(__this, ___value, method) (( int32_t (*) (Collection_1_t2687 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m20133_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m20134_gshared (Collection_1_t2687 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m20134(__this, ___index, ___value, method) (( void (*) (Collection_1_t2687 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m20134_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m20135_gshared (Collection_1_t2687 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m20135(__this, ___value, method) (( void (*) (Collection_1_t2687 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m20135_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m20136_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m20136(__this, method) (( bool (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m20136_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m20137_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m20137(__this, method) (( Object_t * (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m20137_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m20138_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m20138(__this, method) (( bool (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m20138_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m20139_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m20139(__this, method) (( bool (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m20139_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m20140_gshared (Collection_1_t2687 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m20140(__this, ___index, method) (( Object_t * (*) (Collection_1_t2687 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m20140_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m20141_gshared (Collection_1_t2687 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m20141(__this, ___index, ___value, method) (( void (*) (Collection_1_t2687 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m20141_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m20142_gshared (Collection_1_t2687 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define Collection_1_Add_m20142(__this, ___item, method) (( void (*) (Collection_1_t2687 *, UICharInfo_t811 , const MethodInfo*))Collection_1_Add_m20142_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m20143_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_Clear_m20143(__this, method) (( void (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_Clear_m20143_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m20144_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m20144(__this, method) (( void (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_ClearItems_m20144_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m20145_gshared (Collection_1_t2687 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define Collection_1_Contains_m20145(__this, ___item, method) (( bool (*) (Collection_1_t2687 *, UICharInfo_t811 , const MethodInfo*))Collection_1_Contains_m20145_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m20146_gshared (Collection_1_t2687 * __this, UICharInfoU5BU5D_t1086* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m20146(__this, ___array, ___index, method) (( void (*) (Collection_1_t2687 *, UICharInfoU5BU5D_t1086*, int32_t, const MethodInfo*))Collection_1_CopyTo_m20146_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m20147_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m20147(__this, method) (( Object_t* (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_GetEnumerator_m20147_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m20148_gshared (Collection_1_t2687 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m20148(__this, ___item, method) (( int32_t (*) (Collection_1_t2687 *, UICharInfo_t811 , const MethodInfo*))Collection_1_IndexOf_m20148_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m20149_gshared (Collection_1_t2687 * __this, int32_t ___index, UICharInfo_t811  ___item, const MethodInfo* method);
#define Collection_1_Insert_m20149(__this, ___index, ___item, method) (( void (*) (Collection_1_t2687 *, int32_t, UICharInfo_t811 , const MethodInfo*))Collection_1_Insert_m20149_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m20150_gshared (Collection_1_t2687 * __this, int32_t ___index, UICharInfo_t811  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m20150(__this, ___index, ___item, method) (( void (*) (Collection_1_t2687 *, int32_t, UICharInfo_t811 , const MethodInfo*))Collection_1_InsertItem_m20150_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m20151_gshared (Collection_1_t2687 * __this, UICharInfo_t811  ___item, const MethodInfo* method);
#define Collection_1_Remove_m20151(__this, ___item, method) (( bool (*) (Collection_1_t2687 *, UICharInfo_t811 , const MethodInfo*))Collection_1_Remove_m20151_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m20152_gshared (Collection_1_t2687 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m20152(__this, ___index, method) (( void (*) (Collection_1_t2687 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m20152_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m20153_gshared (Collection_1_t2687 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m20153(__this, ___index, method) (( void (*) (Collection_1_t2687 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m20153_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m20154_gshared (Collection_1_t2687 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m20154(__this, method) (( int32_t (*) (Collection_1_t2687 *, const MethodInfo*))Collection_1_get_Count_m20154_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t811  Collection_1_get_Item_m20155_gshared (Collection_1_t2687 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m20155(__this, ___index, method) (( UICharInfo_t811  (*) (Collection_1_t2687 *, int32_t, const MethodInfo*))Collection_1_get_Item_m20155_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m20156_gshared (Collection_1_t2687 * __this, int32_t ___index, UICharInfo_t811  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m20156(__this, ___index, ___value, method) (( void (*) (Collection_1_t2687 *, int32_t, UICharInfo_t811 , const MethodInfo*))Collection_1_set_Item_m20156_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m20157_gshared (Collection_1_t2687 * __this, int32_t ___index, UICharInfo_t811  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m20157(__this, ___index, ___item, method) (( void (*) (Collection_1_t2687 *, int32_t, UICharInfo_t811 , const MethodInfo*))Collection_1_SetItem_m20157_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m20158_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m20158(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m20158_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t811  Collection_1_ConvertItem_m20159_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m20159(__this /* static, unused */, ___item, method) (( UICharInfo_t811  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m20159_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m20160_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m20160(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m20160_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m20161_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m20161(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m20161_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m20162_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m20162(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m20162_gshared)(__this /* static, unused */, ___list, method)
