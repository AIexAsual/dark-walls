﻿#pragma once
#include <stdint.h>
// System.Action
struct Action_t238;
// System.Object
#include "mscorlib_System_Object.h"
// MobileNativeMessage
struct  MobileNativeMessage_t280  : public Object_t
{
	// System.Action MobileNativeMessage::OnComplete
	Action_t238 * ___OnComplete_0;
};
struct MobileNativeMessage_t280_StaticFields{
	// System.Action MobileNativeMessage::<>f__am$cache1
	Action_t238 * ___U3CU3Ef__amU24cache1_1;
};
