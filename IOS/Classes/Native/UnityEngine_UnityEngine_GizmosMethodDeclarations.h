﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gizmos
struct Gizmos_t895;
// System.String
struct String_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Gizmos_DrawLine_m3346 (Object_t * __this /* static, unused */, Vector3_t215  ___from, Vector3_t215  ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Gizmos_INTERNAL_CALL_DrawLine_m5358 (Object_t * __this /* static, unused */, Vector3_t215 * ___from, Vector3_t215 * ___to, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawIcon(UnityEngine.Vector3,System.String)
extern "C" void Gizmos_DrawIcon_m3388 (Object_t * __this /* static, unused */, Vector3_t215  ___center, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawIcon(UnityEngine.Vector3&,System.String,System.Boolean)
extern "C" void Gizmos_INTERNAL_CALL_DrawIcon_m5359 (Object_t * __this /* static, unused */, Vector3_t215 * ___center, String_t* ___name, bool ___allowScaling, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C" void Gizmos_set_color_m3345 (Object_t * __this /* static, unused */, Color_t6  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Gizmos_INTERNAL_set_color_m5360 (Object_t * __this /* static, unused */, Color_t6 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
