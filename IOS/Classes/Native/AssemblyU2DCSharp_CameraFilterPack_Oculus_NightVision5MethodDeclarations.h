﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Oculus_NightVision5
struct CameraFilterPack_Oculus_NightVision5_t165;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Oculus_NightVision5::.ctor()
extern "C" void CameraFilterPack_Oculus_NightVision5__ctor_m1064 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision5::get_material()
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision5_get_material_m1065 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::ChangeFilters()
extern "C" void CameraFilterPack_Oculus_NightVision5_ChangeFilters_m1066 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::Start()
extern "C" void CameraFilterPack_Oculus_NightVision5_Start_m1067 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Oculus_NightVision5_OnRenderImage_m1068 (CameraFilterPack_Oculus_NightVision5_t165 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::OnValidate()
extern "C" void CameraFilterPack_Oculus_NightVision5_OnValidate_m1069 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::Update()
extern "C" void CameraFilterPack_Oculus_NightVision5_Update_m1070 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision5::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision5_OnDisable_m1071 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
