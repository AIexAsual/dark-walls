﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>

extern TypeInfo SA_Singleton_1_t3392_il2cpp_TypeInfo;
extern TypeInfo EventFunction_1_t3393_il2cpp_TypeInfo;
extern TypeInfo TweenRunner_1_t3395_il2cpp_TypeInfo;
extern TypeInfo U3CStartU3Ec__Iterator0_t3396_il2cpp_TypeInfo;
extern TypeInfo IndexedSet_1_t3397_il2cpp_TypeInfo;
extern TypeInfo ObjectPool_1_t3398_il2cpp_TypeInfo;
extern TypeInfo ResponseDelegate_1_t3401_il2cpp_TypeInfo;
extern TypeInfo U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_il2cpp_TypeInfo;
extern TypeInfo ThreadSafeDictionary_2_t3403_il2cpp_TypeInfo;
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t3404_il2cpp_TypeInfo;
extern TypeInfo CastHelper_1_t3405_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3407_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_2_t3408_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_3_t3409_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_4_t3410_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1168_il2cpp_TypeInfo;
extern TypeInfo UnityEvent_1_t3411_il2cpp_TypeInfo;
extern TypeInfo UnityEvent_2_t3412_il2cpp_TypeInfo;
extern TypeInfo UnityEvent_3_t3413_il2cpp_TypeInfo;
extern TypeInfo UnityEvent_4_t3414_il2cpp_TypeInfo;
extern TypeInfo UnityAction_1_t3415_il2cpp_TypeInfo;
extern TypeInfo UnityAction_2_t3416_il2cpp_TypeInfo;
extern TypeInfo UnityAction_3_t3417_il2cpp_TypeInfo;
extern TypeInfo UnityAction_4_t3418_il2cpp_TypeInfo;
extern TypeInfo PredicateOf_1_t3419_il2cpp_TypeInfo;
extern TypeInfo U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_il2cpp_TypeInfo;
extern TypeInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_il2cpp_TypeInfo;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_il2cpp_TypeInfo;
extern TypeInfo Func_2_t3423_il2cpp_TypeInfo;
extern TypeInfo Queue_1_t3424_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3425_il2cpp_TypeInfo;
extern TypeInfo Stack_1_t3426_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3427_il2cpp_TypeInfo;
extern TypeInfo IComparable_1_t3430_il2cpp_TypeInfo;
extern TypeInfo IEquatable_1_t3431_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t3432_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t3434_il2cpp_TypeInfo;
extern TypeInfo InternalEnumerator_1_t3435_il2cpp_TypeInfo;
extern TypeInfo ArrayReadOnlyList_1_t3436_il2cpp_TypeInfo;
extern TypeInfo U3CGetEnumeratorU3Ec__Iterator0_t3437_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3438_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t3439_il2cpp_TypeInfo;
extern TypeInfo Nullable_1_t2194_il2cpp_TypeInfo;
extern TypeInfo CollectionDebuggerView_1_t3444_il2cpp_TypeInfo;
extern TypeInfo CollectionDebuggerView_2_t3445_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t3446_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3447_il2cpp_TypeInfo;
extern TypeInfo GenericComparer_1_t3391_il2cpp_TypeInfo;
extern TypeInfo Dictionary_2_t3448_il2cpp_TypeInfo;
extern TypeInfo ShimEnumerator_t3449_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3450_il2cpp_TypeInfo;
extern TypeInfo KeyCollection_t3451_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3452_il2cpp_TypeInfo;
extern TypeInfo ValueCollection_t3453_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3454_il2cpp_TypeInfo;
extern TypeInfo Transform_1_t3455_il2cpp_TypeInfo;
extern TypeInfo EqualityComparer_1_t3456_il2cpp_TypeInfo;
extern TypeInfo DefaultComparer_t3457_il2cpp_TypeInfo;
extern TypeInfo GenericEqualityComparer_1_t3390_il2cpp_TypeInfo;
extern TypeInfo IComparer_1_t3458_il2cpp_TypeInfo;
extern TypeInfo IDictionary_2_t3459_il2cpp_TypeInfo;
extern TypeInfo IEqualityComparer_1_t3460_il2cpp_TypeInfo;
extern TypeInfo KeyValuePair_2_t3461_il2cpp_TypeInfo;
extern TypeInfo List_1_t3462_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3463_il2cpp_TypeInfo;
extern TypeInfo Collection_1_t3464_il2cpp_TypeInfo;
extern TypeInfo ReadOnlyCollection_1_t3465_il2cpp_TypeInfo;
extern TypeInfo Getter_2_t2196_il2cpp_TypeInfo;
extern TypeInfo StaticGetter_1_t2195_il2cpp_TypeInfo;
extern TypeInfo Action_1_t3501_il2cpp_TypeInfo;
extern TypeInfo Comparison_1_t3502_il2cpp_TypeInfo;
extern TypeInfo Converter_2_t3503_il2cpp_TypeInfo;
extern TypeInfo Predicate_1_t3504_il2cpp_TypeInfo;
extern Il2CppGenericContainer s_GenericContainerTable[177] = 
{
	{ -1, &SA_Singleton_1_t3392_il2cpp_TypeInfo, 1, 0, 0 },
	{ -1, NULL, 1, 1, 1 },
	{ -1, NULL, 1, 1, 2 },
	{ -1, NULL, 1, 1, 3 },
	{ -1, NULL, 1, 1, 4 },
	{ -1, NULL, 1, 1, 5 },
	{ -1, NULL, 1, 1, 6 },
	{ -1, NULL, 1, 1, 7 },
	{ -1, NULL, 1, 1, 8 },
	{ -1, NULL, 1, 1, 9 },
	{ -1, NULL, 1, 1, 10 },
	{ -1, NULL, 1, 1, 11 },
	{ -1, &EventFunction_1_t3393_il2cpp_TypeInfo, 1, 0, 12 },
	{ -1, &TweenRunner_1_t3395_il2cpp_TypeInfo, 1, 0, 13 },
	{ -1, &U3CStartU3Ec__Iterator0_t3396_il2cpp_TypeInfo, 1, 0, 14 },
	{ -1, NULL, 1, 1, 15 },
	{ -1, NULL, 1, 1, 16 },
	{ -1, NULL, 1, 1, 17 },
	{ -1, &IndexedSet_1_t3397_il2cpp_TypeInfo, 1, 0, 18 },
	{ -1, &ObjectPool_1_t3398_il2cpp_TypeInfo, 1, 0, 19 },
	{ -1, NULL, 1, 1, 20 },
	{ -1, NULL, 1, 1, 21 },
	{ -1, NULL, 1, 1, 22 },
	{ -1, NULL, 1, 1, 23 },
	{ -1, NULL, 1, 1, 24 },
	{ -1, NULL, 1, 1, 25 },
	{ -1, NULL, 1, 1, 26 },
	{ -1, NULL, 1, 1, 27 },
	{ -1, NULL, 1, 1, 28 },
	{ -1, NULL, 1, 1, 29 },
	{ -1, NULL, 1, 1, 30 },
	{ -1, NULL, 1, 1, 31 },
	{ -1, NULL, 1, 1, 32 },
	{ -1, NULL, 1, 1, 33 },
	{ -1, NULL, 1, 1, 34 },
	{ -1, NULL, 1, 1, 35 },
	{ -1, NULL, 1, 1, 36 },
	{ -1, NULL, 1, 1, 37 },
	{ -1, NULL, 1, 1, 38 },
	{ -1, NULL, 1, 1, 39 },
	{ -1, &ResponseDelegate_1_t3401_il2cpp_TypeInfo, 1, 0, 40 },
	{ -1, &U3CProcessMatchResponseU3Ec__Iterator0_1_t3402_il2cpp_TypeInfo, 1, 0, 41 },
	{ -1, &ThreadSafeDictionary_2_t3403_il2cpp_TypeInfo, 2, 0, 42 },
	{ -1, &ThreadSafeDictionaryValueFactory_2_t3404_il2cpp_TypeInfo, 2, 0, 44 },
	{ -1, &CastHelper_1_t3405_il2cpp_TypeInfo, 1, 0, 46 },
	{ -1, NULL, 1, 1, 47 },
	{ -1, &InvokableCall_1_t3407_il2cpp_TypeInfo, 1, 0, 48 },
	{ -1, &InvokableCall_2_t3408_il2cpp_TypeInfo, 2, 0, 49 },
	{ -1, &InvokableCall_3_t3409_il2cpp_TypeInfo, 3, 0, 51 },
	{ -1, &InvokableCall_4_t3410_il2cpp_TypeInfo, 4, 0, 54 },
	{ -1, &CachedInvokableCall_1_t1168_il2cpp_TypeInfo, 1, 0, 58 },
	{ -1, &UnityEvent_1_t3411_il2cpp_TypeInfo, 1, 0, 59 },
	{ -1, &UnityEvent_2_t3412_il2cpp_TypeInfo, 2, 0, 60 },
	{ -1, &UnityEvent_3_t3413_il2cpp_TypeInfo, 3, 0, 62 },
	{ -1, &UnityEvent_4_t3414_il2cpp_TypeInfo, 4, 0, 65 },
	{ -1, &UnityAction_1_t3415_il2cpp_TypeInfo, 1, 0, 69 },
	{ -1, &UnityAction_2_t3416_il2cpp_TypeInfo, 2, 0, 70 },
	{ -1, &UnityAction_3_t3417_il2cpp_TypeInfo, 3, 0, 72 },
	{ -1, &UnityAction_4_t3418_il2cpp_TypeInfo, 4, 0, 75 },
	{ -1, NULL, 1, 1, 79 },
	{ -1, NULL, 1, 1, 80 },
	{ -1, NULL, 1, 1, 81 },
	{ -1, NULL, 1, 1, 82 },
	{ -1, NULL, 1, 1, 83 },
	{ -1, NULL, 1, 1, 84 },
	{ -1, NULL, 2, 1, 85 },
	{ -1, NULL, 2, 1, 87 },
	{ -1, NULL, 1, 1, 89 },
	{ -1, NULL, 1, 1, 90 },
	{ -1, NULL, 1, 1, 91 },
	{ -1, &PredicateOf_1_t3419_il2cpp_TypeInfo, 1, 0, 92 },
	{ -1, &U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_il2cpp_TypeInfo, 1, 0, 93 },
	{ -1, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_il2cpp_TypeInfo, 2, 0, 94 },
	{ -1, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_il2cpp_TypeInfo, 1, 0, 96 },
	{ -1, &Func_2_t3423_il2cpp_TypeInfo, 2, 0, 97 },
	{ -1, &Queue_1_t3424_il2cpp_TypeInfo, 1, 0, 99 },
	{ -1, &Enumerator_t3425_il2cpp_TypeInfo, 1, 0, 100 },
	{ -1, &Stack_1_t3426_il2cpp_TypeInfo, 1, 0, 101 },
	{ -1, &Enumerator_t3427_il2cpp_TypeInfo, 1, 0, 102 },
	{ -1, &IComparable_1_t3430_il2cpp_TypeInfo, 1, 0, 103 },
	{ -1, &IEquatable_1_t3431_il2cpp_TypeInfo, 1, 0, 104 },
	{ -1, &IEnumerator_1_t3432_il2cpp_TypeInfo, 1, 0, 105 },
	{ -1, &IEnumerable_1_t3434_il2cpp_TypeInfo, 1, 0, 106 },
	{ -1, NULL, 1, 1, 107 },
	{ -1, NULL, 1, 1, 108 },
	{ -1, NULL, 1, 1, 109 },
	{ -1, NULL, 1, 1, 110 },
	{ -1, NULL, 1, 1, 111 },
	{ -1, NULL, 1, 1, 112 },
	{ -1, NULL, 1, 1, 113 },
	{ -1, NULL, 1, 1, 114 },
	{ -1, NULL, 1, 1, 115 },
	{ -1, NULL, 1, 1, 116 },
	{ -1, NULL, 1, 1, 117 },
	{ -1, NULL, 1, 1, 118 },
	{ -1, NULL, 1, 1, 119 },
	{ -1, NULL, 2, 1, 120 },
	{ -1, NULL, 1, 1, 122 },
	{ -1, NULL, 2, 1, 123 },
	{ -1, NULL, 1, 1, 125 },
	{ -1, NULL, 2, 1, 126 },
	{ -1, NULL, 1, 1, 128 },
	{ -1, NULL, 2, 1, 129 },
	{ -1, NULL, 1, 1, 131 },
	{ -1, NULL, 1, 1, 132 },
	{ -1, NULL, 2, 1, 133 },
	{ -1, NULL, 1, 1, 135 },
	{ -1, NULL, 1, 1, 136 },
	{ -1, NULL, 2, 1, 137 },
	{ -1, NULL, 1, 1, 139 },
	{ -1, NULL, 1, 1, 140 },
	{ -1, NULL, 1, 1, 141 },
	{ -1, NULL, 1, 1, 142 },
	{ -1, NULL, 1, 1, 143 },
	{ -1, NULL, 2, 1, 144 },
	{ -1, NULL, 1, 1, 146 },
	{ -1, NULL, 1, 1, 147 },
	{ -1, NULL, 1, 1, 148 },
	{ -1, NULL, 1, 1, 149 },
	{ -1, NULL, 1, 1, 150 },
	{ -1, NULL, 1, 1, 151 },
	{ -1, NULL, 1, 1, 152 },
	{ -1, NULL, 1, 1, 153 },
	{ -1, NULL, 1, 1, 154 },
	{ -1, NULL, 1, 1, 155 },
	{ -1, NULL, 1, 1, 156 },
	{ -1, NULL, 1, 1, 157 },
	{ -1, NULL, 1, 1, 158 },
	{ -1, NULL, 1, 1, 159 },
	{ -1, NULL, 1, 1, 160 },
	{ -1, NULL, 1, 1, 161 },
	{ -1, NULL, 1, 1, 162 },
	{ -1, NULL, 1, 1, 163 },
	{ -1, NULL, 1, 1, 164 },
	{ -1, NULL, 1, 1, 165 },
	{ -1, NULL, 1, 1, 166 },
	{ -1, &InternalEnumerator_1_t3435_il2cpp_TypeInfo, 1, 0, 167 },
	{ -1, &ArrayReadOnlyList_1_t3436_il2cpp_TypeInfo, 1, 0, 168 },
	{ -1, &U3CGetEnumeratorU3Ec__Iterator0_t3437_il2cpp_TypeInfo, 1, 0, 169 },
	{ -1, &IList_1_t3438_il2cpp_TypeInfo, 1, 0, 170 },
	{ -1, &ICollection_1_t3439_il2cpp_TypeInfo, 1, 0, 171 },
	{ -1, &Nullable_1_t2194_il2cpp_TypeInfo, 1, 0, 172 },
	{ -1, &CollectionDebuggerView_1_t3444_il2cpp_TypeInfo, 1, 0, 173 },
	{ -1, &CollectionDebuggerView_2_t3445_il2cpp_TypeInfo, 2, 0, 174 },
	{ -1, &Comparer_1_t3446_il2cpp_TypeInfo, 1, 0, 176 },
	{ -1, &DefaultComparer_t3447_il2cpp_TypeInfo, 1, 0, 177 },
	{ -1, &GenericComparer_1_t3391_il2cpp_TypeInfo, 1, 0, 178 },
	{ -1, NULL, 2, 1, 179 },
	{ -1, NULL, 1, 1, 181 },
	{ -1, &Dictionary_2_t3448_il2cpp_TypeInfo, 2, 0, 182 },
	{ -1, &ShimEnumerator_t3449_il2cpp_TypeInfo, 2, 0, 184 },
	{ -1, &Enumerator_t3450_il2cpp_TypeInfo, 2, 0, 186 },
	{ -1, &KeyCollection_t3451_il2cpp_TypeInfo, 2, 0, 188 },
	{ -1, &Enumerator_t3452_il2cpp_TypeInfo, 2, 0, 190 },
	{ -1, &ValueCollection_t3453_il2cpp_TypeInfo, 2, 0, 192 },
	{ -1, &Enumerator_t3454_il2cpp_TypeInfo, 2, 0, 194 },
	{ -1, &Transform_1_t3455_il2cpp_TypeInfo, 3, 0, 196 },
	{ -1, &EqualityComparer_1_t3456_il2cpp_TypeInfo, 1, 0, 199 },
	{ -1, &DefaultComparer_t3457_il2cpp_TypeInfo, 1, 0, 200 },
	{ -1, &GenericEqualityComparer_1_t3390_il2cpp_TypeInfo, 1, 0, 201 },
	{ -1, &IComparer_1_t3458_il2cpp_TypeInfo, 1, 0, 202 },
	{ -1, &IDictionary_2_t3459_il2cpp_TypeInfo, 2, 0, 203 },
	{ -1, &IEqualityComparer_1_t3460_il2cpp_TypeInfo, 1, 0, 205 },
	{ -1, &KeyValuePair_2_t3461_il2cpp_TypeInfo, 2, 0, 206 },
	{ -1, &List_1_t3462_il2cpp_TypeInfo, 1, 0, 208 },
	{ -1, &Enumerator_t3463_il2cpp_TypeInfo, 1, 0, 209 },
	{ -1, &Collection_1_t3464_il2cpp_TypeInfo, 1, 0, 210 },
	{ -1, &ReadOnlyCollection_1_t3465_il2cpp_TypeInfo, 1, 0, 211 },
	{ -1, NULL, 2, 1, 212 },
	{ -1, NULL, 1, 1, 214 },
	{ -1, &Getter_2_t2196_il2cpp_TypeInfo, 2, 0, 215 },
	{ -1, &StaticGetter_1_t2195_il2cpp_TypeInfo, 1, 0, 217 },
	{ -1, NULL, 1, 1, 218 },
	{ -1, &Action_1_t3501_il2cpp_TypeInfo, 1, 0, 219 },
	{ -1, &Comparison_1_t3502_il2cpp_TypeInfo, 1, 0, 220 },
	{ -1, &Converter_2_t3503_il2cpp_TypeInfo, 2, 0, 221 },
	{ -1, &Predicate_1_t3504_il2cpp_TypeInfo, 1, 0, 223 },
};
