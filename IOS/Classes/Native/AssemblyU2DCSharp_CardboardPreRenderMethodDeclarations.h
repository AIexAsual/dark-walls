﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CardboardPreRender
struct CardboardPreRender_t247;
// UnityEngine.Camera
struct Camera_t14;

// System.Void CardboardPreRender::.ctor()
extern "C" void CardboardPreRender__ctor_m1548 (CardboardPreRender_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera CardboardPreRender::get_camera()
extern "C" Camera_t14 * CardboardPreRender_get_camera_m1549 (CardboardPreRender_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardPreRender::set_camera(UnityEngine.Camera)
extern "C" void CardboardPreRender_set_camera_m1550 (CardboardPreRender_t247 * __this, Camera_t14 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardPreRender::Awake()
extern "C" void CardboardPreRender_Awake_m1551 (CardboardPreRender_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardPreRender::Reset()
extern "C" void CardboardPreRender_Reset_m1552 (CardboardPreRender_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardPreRender::OnPreCull()
extern "C" void CardboardPreRender_OnPreCull_m1553 (CardboardPreRender_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardPreRender::SetShaderGlobals()
extern "C" void CardboardPreRender_SetShaderGlobals_m1554 (CardboardPreRender_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
