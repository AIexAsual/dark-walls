﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyKeyFileAttribute
struct  AssemblyKeyFileAttribute_t1777  : public Attribute_t903
{
	// System.String System.Reflection.AssemblyKeyFileAttribute::name
	String_t* ___name_0;
};
