﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNAndroidNative
struct MNAndroidNative_t283;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t470;

// System.Void MNAndroidNative::.ctor()
extern "C" void MNAndroidNative__ctor_m1752 (MNAndroidNative_t283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::CallActivityFunction(System.String,System.Object[])
extern "C" void MNAndroidNative_CallActivityFunction_m1753 (Object_t * __this /* static, unused */, String_t* ___methodName, ObjectU5BU5D_t470* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showDialog(System.String,System.String)
extern "C" void MNAndroidNative_showDialog_m1754 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showDialog(System.String,System.String,System.String,System.String)
extern "C" void MNAndroidNative_showDialog_m1755 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showMessage(System.String,System.String)
extern "C" void MNAndroidNative_showMessage_m1756 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showMessage(System.String,System.String,System.String)
extern "C" void MNAndroidNative_showMessage_m1757 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::showRateDialog(System.String,System.String,System.String,System.String,System.String)
extern "C" void MNAndroidNative_showRateDialog_m1758 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___laiter, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::ShowPreloader(System.String,System.String)
extern "C" void MNAndroidNative_ShowPreloader_m1759 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::HidePreloader()
extern "C" void MNAndroidNative_HidePreloader_m1760 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidNative::RedirectStoreRatingPage(System.String)
extern "C" void MNAndroidNative_RedirectStoreRatingPage_m1761 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
