﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Colors_Brightness
struct CameraFilterPack_Colors_Brightness_t74;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Colors_Brightness::.ctor()
extern "C" void CameraFilterPack_Colors_Brightness__ctor_m454 (CameraFilterPack_Colors_Brightness_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Colors_Brightness::get_material()
extern "C" Material_t2 * CameraFilterPack_Colors_Brightness_get_material_m455 (CameraFilterPack_Colors_Brightness_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::Start()
extern "C" void CameraFilterPack_Colors_Brightness_Start_m456 (CameraFilterPack_Colors_Brightness_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Colors_Brightness_OnRenderImage_m457 (CameraFilterPack_Colors_Brightness_t74 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::OnValidate()
extern "C" void CameraFilterPack_Colors_Brightness_OnValidate_m458 (CameraFilterPack_Colors_Brightness_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::Update()
extern "C" void CameraFilterPack_Colors_Brightness_Update_m459 (CameraFilterPack_Colors_Brightness_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Colors_Brightness::OnDisable()
extern "C" void CameraFilterPack_Colors_Brightness_OnDisable_m460 (CameraFilterPack_Colors_Brightness_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
