﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RangeAttribute
struct RangeAttribute_t1049;

// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C" void RangeAttribute__ctor_m6245 (RangeAttribute_t1049 * __this, float ___min, float ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
