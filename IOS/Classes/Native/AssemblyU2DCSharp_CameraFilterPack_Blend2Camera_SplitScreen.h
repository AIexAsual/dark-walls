﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// CameraFilterPack_Blend2Camera_SplitScreen
struct  CameraFilterPack_Blend2Camera_SplitScreen_t44  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_Blend2Camera_SplitScreen::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_SplitScreen::SCShader
	Shader_t1 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_SplitScreen::Camera2
	Camera_t14 * ___Camera2_4;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::TimeX
	float ___TimeX_5;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_SplitScreen::SCMaterial
	Material_t2 * ___SCMaterial_6;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::SwitchCameraToCamera2
	float ___SwitchCameraToCamera2_7;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::BlendFX
	float ___BlendFX_8;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::SplitX
	float ___SplitX_9;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::SplitY
	float ___SplitY_10;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::Smooth
	float ___Smooth_11;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::Rotation
	float ___Rotation_12;
	// System.Boolean CameraFilterPack_Blend2Camera_SplitScreen::ForceYSwap
	bool ___ForceYSwap_13;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_SplitScreen::Camera2tex
	RenderTexture_t15 * ___Camera2tex_21;
	// UnityEngine.Vector2 CameraFilterPack_Blend2Camera_SplitScreen::ScreenSize
	Vector2_t7  ___ScreenSize_22;
};
struct CameraFilterPack_Blend2Camera_SplitScreen_t44_StaticFields{
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue
	float ___ChangeValue_14;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue2
	float ___ChangeValue2_15;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue3
	float ___ChangeValue3_16;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue4
	float ___ChangeValue4_17;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue5
	float ___ChangeValue5_18;
	// System.Single CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue6
	float ___ChangeValue6_19;
	// System.Boolean CameraFilterPack_Blend2Camera_SplitScreen::ChangeValue7
	bool ___ChangeValue7_20;
};
