﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CameraFilterPack_Vision_Warp
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// CameraFilterPack_Vision_Warp
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_WarpMethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_GraphicsMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void CameraFilterPack_Vision_Warp::.ctor()
extern "C" void CameraFilterPack_Vision_Warp__ctor_m1366 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (0.6f);
		__this->___Value2_7 = (0.6f);
		__this->___Value3_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Vision_Warp::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Vision_Warp_get_material_m1367 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Vision_Warp::Start()
extern TypeInfo* CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral296;
extern "C" void CameraFilterPack_Vision_Warp_Start_m1368 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(157);
		_stringLiteral296 = il2cpp_codegen_string_literal_from_index(296);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral296, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Vision_Warp_OnRenderImage_m1369 (CameraFilterPack_Vision_Warp_t210 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Vision_Warp_get_material_m1367(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Vision_Warp_get_material_m1367(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Vision_Warp_get_material_m1367(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Value2_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Vision_Warp_get_material_m1367(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Value3_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Vision_Warp_get_material_m1367(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Vision_Warp_get_material_m1367(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Vision_Warp_get_material_m1367(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp::OnValidate()
extern TypeInfo* CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Warp_OnValidate_m1370 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(157);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp::Update()
extern TypeInfo* CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Warp_Update_m1371 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(157);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Value_6 = L_1;
		float L_2 = ((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Value2_7 = L_2;
		float L_3 = ((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Value3_8 = L_3;
		float L_4 = ((CameraFilterPack_Vision_Warp_t210_StaticFields*)CameraFilterPack_Vision_Warp_t210_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp::OnDisable()
extern "C" void CameraFilterPack_Vision_Warp_OnDisable_m1372 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Vision_Warp2
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Vision_Warp2
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Warp2MethodDeclarations.h"



// System.Void CameraFilterPack_Vision_Warp2::.ctor()
extern "C" void CameraFilterPack_Vision_Warp2__ctor_m1373 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (0.5f);
		__this->___Value2_7 = (0.2f);
		__this->___Intensity_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Vision_Warp2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Vision_Warp2_get_material_m1374 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Vision_Warp2::Start()
extern TypeInfo* CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral297;
extern "C" void CameraFilterPack_Vision_Warp2_Start_m1375 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		_stringLiteral297 = il2cpp_codegen_string_literal_from_index(297);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Intensity_8);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral297, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Vision_Warp2_OnRenderImage_m1376 (CameraFilterPack_Vision_Warp2_t211 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Vision_Warp2_get_material_m1374(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Vision_Warp2_get_material_m1374(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Vision_Warp2_get_material_m1374(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Value2_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Vision_Warp2_get_material_m1374(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Intensity_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Vision_Warp2_get_material_m1374(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Vision_Warp2_get_material_m1374(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Vision_Warp2_get_material_m1374(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp2::OnValidate()
extern TypeInfo* CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Warp2_OnValidate_m1377 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Intensity_8);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp2::Update()
extern TypeInfo* CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Warp2_Update_m1378 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(158);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Value_6 = L_1;
		float L_2 = ((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Value2_7 = L_2;
		float L_3 = ((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Intensity_8 = L_3;
		float L_4 = ((CameraFilterPack_Vision_Warp2_t211_StaticFields*)CameraFilterPack_Vision_Warp2_t211_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Warp2::OnDisable()
extern "C" void CameraFilterPack_Vision_Warp2_OnDisable_m1379 (CameraFilterPack_Vision_Warp2_t211 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// FPS
#include "AssemblyU2DCSharp_FPS.h"
#ifndef _MSC_VER
#else
#endif
// FPS
#include "AssemblyU2DCSharp_FPSMethodDeclarations.h"

// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
struct Component_t477;
struct Text_t212;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t477;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m2755_gshared (Component_t477 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m2755(__this, method) (( Object_t * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t212_m2754(__this, method) (( Text_t212 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void FPS::.ctor()
extern "C" void FPS__ctor_m1380 (FPS_t213 * __this, const MethodInfo* method)
{
	{
		__this->___fps_3 = (60.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPS::Awake()
extern const MethodInfo* Component_GetComponent_TisText_t212_m2754_MethodInfo_var;
extern "C" void FPS_Awake_m1381 (FPS_t213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisText_t212_m2754_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t212 * L_0 = Component_GetComponent_TisText_t212_m2754(__this, /*hidden argument*/Component_GetComponent_TisText_t212_m2754_MethodInfo_var);
		__this->___text_2 = L_0;
		return;
	}
}
// System.Void FPS::LateUpdate()
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral298;
extern "C" void FPS_LateUpdate_m1382 (FPS_t213 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral298 = il2cpp_codegen_string_literal_from_index(298);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0/(float)((float)((float)(0.5f)+(float)L_1))));
		float L_2 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(1.0f)/(float)L_2));
		float L_3 = (__this->___fps_3);
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Lerp_m2756(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		__this->___fps_3 = L_6;
		Text_t212 * L_7 = (__this->___text_2);
		float L_8 = (__this->___fps_3);
		int32_t L_9 = Mathf_RoundToInt_m2757(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2758(NULL /*static, unused*/, L_11, _stringLiteral298, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_12);
		return;
	}
}
// Teleport
#include "AssemblyU2DCSharp_Teleport.h"
#ifndef _MSC_VER
#else
#endif
// Teleport
#include "AssemblyU2DCSharp_TeleportMethodDeclarations.h"

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Cardboard
#include "AssemblyU2DCSharp_Cardboard.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// Cardboard
#include "AssemblyU2DCSharp_CardboardMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
struct Component_t477;
struct Renderer_t312;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t312_m2759(__this, method) (( Renderer_t312 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void Teleport::.ctor()
extern "C" void Teleport__ctor_m1383 (Teleport_t214 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Teleport::Start()
extern "C" void Teleport_Start_m1384 (Teleport_t214 * __this, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t215  L_1 = Transform_get_localPosition_m2761(L_0, /*hidden argument*/NULL);
		__this->___startingPosition_2 = L_1;
		Teleport_SetGazedAt_m1385(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Teleport::SetGazedAt(System.Boolean)
extern const MethodInfo* Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var;
extern "C" void Teleport_SetGazedAt_m1385 (Teleport_t214 * __this, bool ___gazedAt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	Material_t2 * G_B2_0 = {0};
	Material_t2 * G_B1_0 = {0};
	Color_t6  G_B3_0 = {0};
	Material_t2 * G_B3_1 = {0};
	{
		Renderer_t312 * L_0 = Component_GetComponent_TisRenderer_t312_m2759(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var);
		NullCheck(L_0);
		Material_t2 * L_1 = Renderer_get_material_m2762(L_0, /*hidden argument*/NULL);
		bool L_2 = ___gazedAt;
		G_B1_0 = L_1;
		if (!L_2)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		Color_t6  L_3 = Color_get_green_m2763(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_0020;
	}

IL_001b:
	{
		Color_t6  L_4 = Color_get_red_m2764(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_0020:
	{
		NullCheck(G_B3_1);
		Material_set_color_m2765(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Teleport::Reset()
extern "C" void Teleport_Reset_m1386 (Teleport_t214 * __this, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Vector3_t215  L_1 = (__this->___startingPosition_2);
		NullCheck(L_0);
		Transform_set_localPosition_m2766(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Teleport::ToggleVRMode()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Teleport_ToggleVRMode_m1387 (Teleport_t214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		Cardboard_t233 * L_1 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Cardboard_get_VRModeEnabled_m1442(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Cardboard_set_VRModeEnabled_m1443(L_0, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Teleport::TeleportRandomly()
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void Teleport_TeleportRandomly_m1388 (Teleport_t214 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t215  V_0 = {0};
	float V_1 = 0.0f;
	{
		Vector3_t215  L_0 = Random_get_onUnitSphere_m2767(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_1, (0.5f), (1.0f), /*hidden argument*/NULL);
		(&V_0)->___y_2 = L_2;
		float L_3 = Random_get_value_m2769(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)(2.0f)*(float)L_3))+(float)(1.5f)));
		Transform_t243 * L_4 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Vector3_t215  L_5 = V_0;
		float L_6 = V_1;
		Vector3_t215  L_7 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localPosition_m2766(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// CardboardOnGUI/OnGUICallback
#include "AssemblyU2DCSharp_CardboardOnGUI_OnGUICallback.h"
#ifndef _MSC_VER
#else
#endif
// CardboardOnGUI/OnGUICallback
#include "AssemblyU2DCSharp_CardboardOnGUI_OnGUICallbackMethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void CardboardOnGUI/OnGUICallback::.ctor(System.Object,System.IntPtr)
extern "C" void OnGUICallback__ctor_m1389 (OnGUICallback_t218 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void CardboardOnGUI/OnGUICallback::Invoke()
extern "C" void OnGUICallback_Invoke_m1390 (OnGUICallback_t218 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnGUICallback_Invoke_m1390((OnGUICallback_t218 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnGUICallback_t218(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult CardboardOnGUI/OnGUICallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * OnGUICallback_BeginInvoke_m1391 (OnGUICallback_t218 * __this, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void CardboardOnGUI/OnGUICallback::EndInvoke(System.IAsyncResult)
extern "C" void OnGUICallback_EndInvoke_m1392 (OnGUICallback_t218 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// CardboardOnGUI
#include "AssemblyU2DCSharp_CardboardOnGUI.h"
#ifndef _MSC_VER
#else
#endif
// CardboardOnGUI
#include "AssemblyU2DCSharp_CardboardOnGUIMethodDeclarations.h"

// System.Delegate
#include "mscorlib_System_Delegate.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// CardboardOnGUIWindow
#include "AssemblyU2DCSharp_CardboardOnGUIWindow.h"
#include "Assembly-CSharp_ArrayTypes.h"
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// CardboardOnGUIMouse
#include "AssemblyU2DCSharp_CardboardOnGUIMouse.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_Event.h"
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// CardboardOnGUIWindow
#include "AssemblyU2DCSharp_CardboardOnGUIWindowMethodDeclarations.h"
// UnityEngine.Event
#include "UnityEngine_UnityEngine_EventMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// CardboardOnGUIMouse
#include "AssemblyU2DCSharp_CardboardOnGUIMouseMethodDeclarations.h"
struct Component_t477;
struct CardboardOnGUIWindowU5BU5D_t479;
struct Component_t477;
struct ObjectU5BU5D_t470;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C" ObjectU5BU5D_t470* Component_GetComponentsInChildren_TisObject_t_m2772_gshared (Component_t477 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m2772(__this, p0, method) (( ObjectU5BU5D_t470* (*) (Component_t477 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2772_gshared)(__this, p0, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<CardboardOnGUIWindow>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<CardboardOnGUIWindow>(System.Boolean)
#define Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2771(__this, p0, method) (( CardboardOnGUIWindowU5BU5D_t479* (*) (Component_t477 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2772_gshared)(__this, p0, method)
struct Component_t477;
struct CardboardOnGUIMouse_t222;
// Declaration !!0 UnityEngine.Component::GetComponent<CardboardOnGUIMouse>()
// !!0 UnityEngine.Component::GetComponent<CardboardOnGUIMouse>()
#define Component_GetComponent_TisCardboardOnGUIMouse_t222_m2773(__this, method) (( CardboardOnGUIMouse_t222 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void CardboardOnGUI::.ctor()
extern "C" void CardboardOnGUI__ctor_m1393 (CardboardOnGUI_t220 * __this, const MethodInfo* method)
{
	{
		Color_t6  L_0 = Color_get_clear_m2774(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___background_4 = L_0;
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardOnGUI::add_onGUICallback(CardboardOnGUI/OnGUICallback)
extern TypeInfo* CardboardOnGUI_t220_il2cpp_TypeInfo_var;
extern TypeInfo* OnGUICallback_t218_il2cpp_TypeInfo_var;
extern "C" void CardboardOnGUI_add_onGUICallback_m1394 (Object_t * __this /* static, unused */, OnGUICallback_t218 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardOnGUI_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		OnGUICallback_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGUICallback_t218 * L_0 = ((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___onGUICallback_6;
		OnGUICallback_t218 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___onGUICallback_6 = ((OnGUICallback_t218 *)Castclass(L_2, OnGUICallback_t218_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void CardboardOnGUI::remove_onGUICallback(CardboardOnGUI/OnGUICallback)
extern TypeInfo* CardboardOnGUI_t220_il2cpp_TypeInfo_var;
extern TypeInfo* OnGUICallback_t218_il2cpp_TypeInfo_var;
extern "C" void CardboardOnGUI_remove_onGUICallback_m1395 (Object_t * __this /* static, unused */, OnGUICallback_t218 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardOnGUI_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		OnGUICallback_t218_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(165);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnGUICallback_t218 * L_0 = ((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___onGUICallback_6;
		OnGUICallback_t218 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___onGUICallback_6 = ((OnGUICallback_t218 *)Castclass(L_2, OnGUICallback_t218_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Boolean CardboardOnGUI::OKToDraw(UnityEngine.MonoBehaviour)
extern TypeInfo* CardboardOnGUI_t220_il2cpp_TypeInfo_var;
extern "C" bool CardboardOnGUI_OKToDraw_m1396 (Object_t * __this /* static, unused */, MonoBehaviour_t4 * ___mb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardOnGUI_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = ((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___okToDraw_2;
		if (!L_0)
		{
			goto IL_0034;
		}
	}
	{
		MonoBehaviour_t4 * L_1 = ___mb;
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		MonoBehaviour_t4 * L_3 = ___mb;
		NullCheck(L_3);
		bool L_4 = Behaviour_get_enabled_m2777(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		MonoBehaviour_t4 * L_5 = ___mb;
		NullCheck(L_5);
		GameObject_t256 * L_6 = Component_get_gameObject_m2778(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m2779(L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_002f;
	}

IL_002e:
	{
		G_B5_0 = 0;
	}

IL_002f:
	{
		G_B7_0 = G_B5_0;
		goto IL_0032;
	}

IL_0031:
	{
		G_B7_0 = 1;
	}

IL_0032:
	{
		G_B9_0 = G_B7_0;
		goto IL_0035;
	}

IL_0034:
	{
		G_B9_0 = 0;
	}

IL_0035:
	{
		return G_B9_0;
	}
}
// System.Boolean CardboardOnGUI::get_IsGUIVisible()
extern TypeInfo* CardboardOnGUI_t220_il2cpp_TypeInfo_var;
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" bool CardboardOnGUI_get_IsGUIVisible_m1397 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardOnGUI_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		bool L_0 = ((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___isGUIVisible_3;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_1 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Cardboard_get_VRModeEnabled_m1442(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		bool L_3 = SystemInfo_get_supportsRenderTextures_m2780(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_3));
		goto IL_0021;
	}

IL_0020:
	{
		G_B4_0 = 0;
	}

IL_0021:
	{
		return G_B4_0;
	}
}
// System.Void CardboardOnGUI::set_IsGUIVisible(System.Boolean)
extern TypeInfo* CardboardOnGUI_t220_il2cpp_TypeInfo_var;
extern "C" void CardboardOnGUI_set_IsGUIVisible_m1398 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardOnGUI_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___isGUIVisible_3 = L_0;
		return;
	}
}
// System.Boolean CardboardOnGUI::get_Triggered()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" bool CardboardOnGUI_get_Triggered_m1399 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		bool L_0 = CardboardOnGUI_get_IsGUIVisible_m1397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_1 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Cardboard_get_Triggered_m1479(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Void CardboardOnGUI::Awake()
extern Il2CppCodeGenString* _stringLiteral299;
extern "C" void CardboardOnGUI_Awake_m1400 (CardboardOnGUI_t220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral299 = il2cpp_codegen_string_literal_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = SystemInfo_get_supportsRenderTextures_m2780(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		Debug_LogWarning_m2781(NULL /*static, unused*/, _stringLiteral299, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void CardboardOnGUI::Start()
extern "C" void CardboardOnGUI_Start_m1401 (CardboardOnGUI_t220 * __this, const MethodInfo* method)
{
	{
		CardboardOnGUI_Create_m1402(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardOnGUI::Create()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* RenderTexture_t15_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2771_MethodInfo_var;
extern "C" void CardboardOnGUI_Create_m1402 (CardboardOnGUI_t220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		RenderTexture_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2771_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t15 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	CardboardOnGUIWindow_t224 * V_3 = {0};
	CardboardOnGUIWindowU5BU5D_t479* V_4 = {0};
	int32_t V_5 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B8_0 = 0;
	{
		RenderTexture_t15 * L_0 = (__this->___guiScreen_5);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RenderTexture_t15 * L_2 = (__this->___guiScreen_5);
		NullCheck(L_2);
		RenderTexture_Release_m2782(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_3 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		RenderTexture_t15 * L_4 = Cardboard_get_StereoScreen_m1468(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		RenderTexture_t15 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		RenderTexture_t15 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = RenderTexture_get_width_m2726(L_7, /*hidden argument*/NULL);
		G_B5_0 = L_8;
		goto IL_0042;
	}

IL_003d:
	{
		int32_t L_9 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = L_9;
	}

IL_0042:
	{
		V_1 = G_B5_0;
		RenderTexture_t15 * L_10 = V_0;
		bool L_11 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0059;
		}
	}
	{
		RenderTexture_t15 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		G_B8_0 = L_13;
		goto IL_005e;
	}

IL_0059:
	{
		int32_t L_14 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B8_0 = L_14;
	}

IL_005e:
	{
		V_2 = G_B8_0;
		int32_t L_15 = V_1;
		int32_t L_16 = V_2;
		RenderTexture_t15 * L_17 = (RenderTexture_t15 *)il2cpp_codegen_object_new (RenderTexture_t15_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m2783(L_17, L_15, L_16, 0, 0, /*hidden argument*/NULL);
		__this->___guiScreen_5 = L_17;
		RenderTexture_t15 * L_18 = (__this->___guiScreen_5);
		NullCheck(L_18);
		RenderTexture_Create_m2784(L_18, /*hidden argument*/NULL);
		CardboardOnGUIWindowU5BU5D_t479* L_19 = Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2771(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2771_MethodInfo_var);
		V_4 = L_19;
		V_5 = 0;
		goto IL_00a3;
	}

IL_008b:
	{
		CardboardOnGUIWindowU5BU5D_t479* L_20 = V_4;
		int32_t L_21 = V_5;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_3 = (*(CardboardOnGUIWindow_t224 **)(CardboardOnGUIWindow_t224 **)SZArrayLdElema(L_20, L_22));
		CardboardOnGUIWindow_t224 * L_23 = V_3;
		RenderTexture_t15 * L_24 = (__this->___guiScreen_5);
		NullCheck(L_23);
		CardboardOnGUIWindow_Create_m1411(L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_5;
		V_5 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_26 = V_5;
		CardboardOnGUIWindowU5BU5D_t479* L_27 = V_4;
		NullCheck(L_27);
		if ((((int32_t)L_26) < ((int32_t)(((int32_t)(((Array_t *)L_27)->max_length))))))
		{
			goto IL_008b;
		}
	}
	{
		return;
	}
}
// System.Void CardboardOnGUI::LateUpdate()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardOnGUI_t220_il2cpp_TypeInfo_var;
extern "C" void CardboardOnGUI_LateUpdate_m1403 (CardboardOnGUI_t220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		CardboardOnGUI_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Cardboard_get_VRModeEnabled_m1442(L_0, /*hidden argument*/NULL);
		((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___okToDraw_2 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		return;
	}
}
// System.Void CardboardOnGUI::OnGUI()
extern TypeInfo* CardboardOnGUI_t220_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCardboardOnGUIMouse_t222_m2773_MethodInfo_var;
extern "C" void CardboardOnGUI_OnGUI_m1404 (CardboardOnGUI_t220 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardOnGUI_t220_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(164);
		Component_GetComponent_TisCardboardOnGUIMouse_t222_m2773_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t15 * V_0 = {0};
	CardboardOnGUIMouse_t222 * V_1 = {0};
	{
		bool L_0 = CardboardOnGUI_get_IsGUIVisible_m1397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		V_0 = (RenderTexture_t15 *)NULL;
		Event_t481 * L_1 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Event_get_type_m2786(L_1, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)7))))
		{
			goto IL_003b;
		}
	}
	{
		RenderTexture_t15 * L_3 = RenderTexture_get_active_m2787(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		RenderTexture_t15 * L_4 = (__this->___guiScreen_5);
		RenderTexture_set_active_m2788(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Color_t6  L_5 = (__this->___background_4);
		GL_Clear_m2789(NULL /*static, unused*/, 0, 1, L_5, /*hidden argument*/NULL);
	}

IL_003b:
	{
		OnGUICallback_t218 * L_6 = ((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___onGUICallback_6;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___okToDraw_2 = 1;
		OnGUICallback_t218 * L_7 = ((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___onGUICallback_6;
		NullCheck(L_7);
		OnGUICallback_Invoke_m1390(L_7, /*hidden argument*/NULL);
		((CardboardOnGUI_t220_StaticFields*)CardboardOnGUI_t220_il2cpp_TypeInfo_var->static_fields)->___okToDraw_2 = 0;
	}

IL_005b:
	{
		Event_t481 * L_8 = Event_get_current_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m2786(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)7))))
		{
			goto IL_008a;
		}
	}
	{
		CardboardOnGUIMouse_t222 * L_10 = Component_GetComponent_TisCardboardOnGUIMouse_t222_m2773(__this, /*hidden argument*/Component_GetComponent_TisCardboardOnGUIMouse_t222_m2773_MethodInfo_var);
		V_1 = L_10;
		CardboardOnGUIMouse_t222 * L_11 = V_1;
		bool L_12 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_11, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		CardboardOnGUIMouse_t222 * L_13 = V_1;
		NullCheck(L_13);
		CardboardOnGUIMouse_DrawPointerImage_m1407(L_13, /*hidden argument*/NULL);
	}

IL_0084:
	{
		RenderTexture_t15 * L_14 = V_0;
		RenderTexture_set_active_m2788(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// StereoController
#include "AssemblyU2DCSharp_StereoController.h"
// CardboardHead
#include "AssemblyU2DCSharp_CardboardHead.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.ScaleMode
#include "UnityEngine_UnityEngine_ScaleMode.h"
// StereoController
#include "AssemblyU2DCSharp_StereoControllerMethodDeclarations.h"
// CardboardHead
#include "AssemblyU2DCSharp_CardboardHeadMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
struct Component_t477;
struct CardboardOnGUIWindowU5BU5D_t479;
struct Component_t477;
struct ObjectU5BU5D_t470;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t470* Component_GetComponentsInChildren_TisObject_t_m2791_gshared (Component_t477 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m2791(__this, method) (( ObjectU5BU5D_t470* (*) (Component_t477 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2791_gshared)(__this, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<CardboardOnGUIWindow>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<CardboardOnGUIWindow>()
#define Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2790(__this, method) (( CardboardOnGUIWindowU5BU5D_t479* (*) (Component_t477 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2791_gshared)(__this, method)
struct Component_t477;
struct Collider_t319;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t319_m2792(__this, method) (( Collider_t319 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void CardboardOnGUIMouse::.ctor()
extern "C" void CardboardOnGUIMouse__ctor_m1405 (CardboardOnGUIMouse_t222 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardOnGUIMouse::LateUpdate()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2790_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var;
extern "C" void CardboardOnGUIMouse_LateUpdate_m1406 (CardboardOnGUIMouse_t222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2790_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		s_Il2CppMethodIntialized = true;
	}
	StereoController_t235 * V_0 = {0};
	CardboardHead_t244 * V_1 = {0};
	Ray_t465  V_2 = {0};
	CardboardOnGUIWindow_t224 * V_3 = {0};
	float V_4 = 0.0f;
	Vector2_t7  V_5 = {0};
	CardboardOnGUIWindow_t224 * V_6 = {0};
	CardboardOnGUIWindowU5BU5D_t479* V_7 = {0};
	int32_t V_8 = 0;
	RaycastHit_t482  V_9 = {0};
	CardboardHead_t244 * G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		StereoController_t235 * L_0 = Cardboard_get_Controller_m1441(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		StereoController_t235 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		StereoController_t235 * L_3 = V_0;
		NullCheck(L_3);
		CardboardHead_t244 * L_4 = StereoController_get_Head_m1610(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = ((CardboardHead_t244 *)(NULL));
	}

IL_001d:
	{
		V_1 = G_B3_0;
		CardboardHead_t244 * L_5 = V_1;
		bool L_6 = Object_op_Equality_m2716(NULL /*static, unused*/, L_5, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0032;
		}
	}
	{
		__this->___pointerVisible_5 = 1;
		return;
	}

IL_0032:
	{
		bool L_7 = CardboardOnGUI_get_IsGUIVisible_m1397(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0044;
		}
	}
	{
		__this->___pointerVisible_5 = 0;
		return;
	}

IL_0044:
	{
		CardboardHead_t244 * L_8 = V_1;
		NullCheck(L_8);
		Ray_t465  L_9 = CardboardHead_get_Gaze_m1528(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		V_3 = (CardboardOnGUIWindow_t224 *)NULL;
		V_4 = (std::numeric_limits<float>::infinity());
		Vector2_t7  L_10 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_10;
		CardboardOnGUIWindowU5BU5D_t479* L_11 = Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2790(__this, /*hidden argument*/Component_GetComponentsInChildren_TisCardboardOnGUIWindow_t224_m2790_MethodInfo_var);
		V_7 = L_11;
		V_8 = 0;
		goto IL_00b4;
	}

IL_006b:
	{
		CardboardOnGUIWindowU5BU5D_t479* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(CardboardOnGUIWindow_t224 **)(CardboardOnGUIWindow_t224 **)SZArrayLdElema(L_12, L_14));
		CardboardOnGUIWindow_t224 * L_15 = V_6;
		NullCheck(L_15);
		Collider_t319 * L_16 = Component_GetComponent_TisCollider_t319_m2792(L_15, /*hidden argument*/Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var);
		Ray_t465  L_17 = V_2;
		NullCheck(L_16);
		bool L_18 = Collider_Raycast_m2794(L_16, L_17, (&V_9), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ae;
		}
	}
	{
		float L_19 = RaycastHit_get_distance_m2795((&V_9), /*hidden argument*/NULL);
		float L_20 = V_4;
		if ((!(((float)L_19) < ((float)L_20))))
		{
			goto IL_00ae;
		}
	}
	{
		float L_21 = RaycastHit_get_distance_m2795((&V_9), /*hidden argument*/NULL);
		V_4 = L_21;
		CardboardOnGUIWindow_t224 * L_22 = V_6;
		V_3 = L_22;
		Vector2_t7  L_23 = RaycastHit_get_textureCoord_m2796((&V_9), /*hidden argument*/NULL);
		V_5 = L_23;
	}

IL_00ae:
	{
		int32_t L_24 = V_8;
		V_8 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00b4:
	{
		int32_t L_25 = V_8;
		CardboardOnGUIWindowU5BU5D_t479* L_26 = V_7;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)(((Array_t *)L_26)->max_length))))))
		{
			goto IL_006b;
		}
	}
	{
		CardboardOnGUIWindow_t224 * L_27 = V_3;
		bool L_28 = Object_op_Equality_m2716(NULL /*static, unused*/, L_27, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00d3;
		}
	}
	{
		__this->___pointerVisible_5 = 0;
		return;
	}

IL_00d3:
	{
		CardboardOnGUIWindow_t224 * L_29 = V_3;
		NullCheck(L_29);
		Rect_t225 * L_30 = &(L_29->___rect_3);
		float L_31 = Rect_get_xMin_m2797(L_30, /*hidden argument*/NULL);
		CardboardOnGUIWindow_t224 * L_32 = V_3;
		NullCheck(L_32);
		Rect_t225 * L_33 = &(L_32->___rect_3);
		float L_34 = Rect_get_width_m2798(L_33, /*hidden argument*/NULL);
		float L_35 = ((&V_5)->___x_1);
		(&V_5)->___x_1 = ((float)((float)L_31+(float)((float)((float)L_34*(float)L_35))));
		CardboardOnGUIWindow_t224 * L_36 = V_3;
		NullCheck(L_36);
		Rect_t225 * L_37 = &(L_36->___rect_3);
		float L_38 = Rect_get_yMin_m2799(L_37, /*hidden argument*/NULL);
		CardboardOnGUIWindow_t224 * L_39 = V_3;
		NullCheck(L_39);
		Rect_t225 * L_40 = &(L_39->___rect_3);
		float L_41 = Rect_get_height_m2800(L_40, /*hidden argument*/NULL);
		float L_42 = ((&V_5)->___y_2);
		(&V_5)->___y_2 = ((float)((float)L_38+(float)((float)((float)L_41*(float)L_42))));
		float L_43 = ((&V_5)->___x_1);
		int32_t L_44 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___pointerX_6 = (((int32_t)((float)((float)L_43*(float)(((float)L_44))))));
		float L_45 = ((&V_5)->___y_2);
		int32_t L_46 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___pointerY_7 = (((int32_t)((float)((float)L_45*(float)(((float)L_46))))));
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_47 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_48 = (__this->___pointerX_6);
		int32_t L_49 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_50 = (__this->___pointerY_7);
		NullCheck(L_47);
		Cardboard_SetTouchCoordinates_m1492(L_47, L_48, ((int32_t)((int32_t)L_49-(int32_t)L_50)), /*hidden argument*/NULL);
		__this->___pointerVisible_5 = 1;
		return;
	}
}
// System.Void CardboardOnGUIMouse::DrawPointerImage()
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern "C" void CardboardOnGUIMouse_DrawPointerImage_m1407 (CardboardOnGUIMouse_t222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	Vector2_t7  V_1 = {0};
	Vector2_t7  V_2 = {0};
	{
		Texture_t221 * L_0 = (__this->___pointerImage_2);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		bool L_2 = (__this->___pointerVisible_5);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m2777(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0028;
		}
	}

IL_0027:
	{
		return;
	}

IL_0028:
	{
		int32_t L_4 = (__this->___pointerX_6);
		int32_t L_5 = (__this->___pointerY_7);
		Vector2__ctor_m2714((&V_0), (((float)L_4)), (((float)L_5)), /*hidden argument*/NULL);
		Vector2_t7  L_6 = (__this->___pointerSpot_4);
		V_1 = L_6;
		Vector2_t7  L_7 = (__this->___pointerSize_3);
		V_2 = L_7;
		float L_8 = Vector2_get_sqrMagnitude_m2801((&V_2), /*hidden argument*/NULL);
		if ((!(((float)L_8) < ((float)(1.0f)))))
		{
			goto IL_0082;
		}
	}
	{
		Texture_t221 * L_9 = (__this->___pointerImage_2);
		NullCheck(L_9);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_9);
		(&V_2)->___x_1 = (((float)L_10));
		Texture_t221 * L_11 = (__this->___pointerImage_2);
		NullCheck(L_11);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_11);
		(&V_2)->___y_2 = (((float)L_12));
	}

IL_0082:
	{
		float L_13 = ((&V_0)->___x_1);
		float L_14 = ((&V_1)->___x_1);
		int32_t L_15 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = ((&V_0)->___y_2);
		float L_17 = ((&V_1)->___y_2);
		float L_18 = ((&V_2)->___x_1);
		float L_19 = ((&V_2)->___y_2);
		Rect_t225  L_20 = {0};
		Rect__ctor_m2802(&L_20, ((float)((float)L_13-(float)L_14)), ((float)((float)((float)((float)(((float)L_15))-(float)L_16))-(float)L_17)), L_18, L_19, /*hidden argument*/NULL);
		Texture_t221 * L_21 = (__this->___pointerImage_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m2803(NULL /*static, unused*/, L_20, L_21, 0, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
struct Component_t477;
struct MeshRenderer_t223;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t223_m2804(__this, method) (( MeshRenderer_t223 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void CardboardOnGUIWindow::.ctor()
extern "C" void CardboardOnGUIWindow__ctor_m1408 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardOnGUIWindow::Awake()
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t223_m2804_MethodInfo_var;
extern "C" void CardboardOnGUIWindow_Awake_m1409 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisMeshRenderer_t223_m2804_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		s_Il2CppMethodIntialized = true;
	}
	{
		MeshRenderer_t223 * L_0 = Component_GetComponent_TisMeshRenderer_t223_m2804(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t223_m2804_MethodInfo_var);
		__this->___meshRenderer_2 = L_0;
		bool L_1 = SystemInfo_get_supportsRenderTextures_m2780(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void CardboardOnGUIWindow::Reset()
extern "C" void CardboardOnGUIWindow_Reset_m1410 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method)
{
	{
		Rect_t225  L_0 = {0};
		Rect__ctor_m2802(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->___rect_3 = L_0;
		return;
	}
}
// System.Void CardboardOnGUIWindow::Create(UnityEngine.RenderTexture)
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral300;
extern "C" void CardboardOnGUIWindow_Create_m1411 (CardboardOnGUIWindow_t224 * __this, RenderTexture_t15 * ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		_stringLiteral300 = il2cpp_codegen_string_literal_from_index(300);
		s_Il2CppMethodIntialized = true;
	}
	Material_t2 * V_0 = {0};
	{
		MeshRenderer_t223 * L_0 = (__this->___meshRenderer_2);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral300, /*hidden argument*/NULL);
		Material_t2 * L_2 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Material_t2 * L_3 = V_0;
		RenderTexture_t15 * L_4 = ___target;
		NullCheck(L_3);
		Material_set_mainTexture_m2805(L_3, L_4, /*hidden argument*/NULL);
		Material_t2 * L_5 = V_0;
		Rect_t225 * L_6 = &(__this->___rect_3);
		Vector2_t7  L_7 = Rect_get_position_m2806(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_mainTextureOffset_m2807(L_5, L_7, /*hidden argument*/NULL);
		Material_t2 * L_8 = V_0;
		Rect_t225 * L_9 = &(__this->___rect_3);
		Vector2_t7  L_10 = Rect_get_size_m2808(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Material_set_mainTextureScale_m2809(L_8, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = V_0;
		NullCheck(L_0);
		Renderer_set_material_m2810(L_0, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardOnGUIWindow::OnDisable()
extern "C" void CardboardOnGUIWindow_OnDisable_m1412 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method)
{
	{
		MeshRenderer_t223 * L_0 = (__this->___meshRenderer_2);
		NullCheck(L_0);
		Renderer_set_enabled_m2811(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardOnGUIWindow::LateUpdate()
extern "C" void CardboardOnGUIWindow_LateUpdate_m1413 (CardboardOnGUIWindow_t224 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		MeshRenderer_t223 * L_0 = (__this->___meshRenderer_2);
		bool L_1 = CardboardOnGUI_get_IsGUIVisible_m1397(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_set_enabled_m2811(L_0, L_1, /*hidden argument*/NULL);
		MeshRenderer_t223 * L_2 = (__this->___meshRenderer_2);
		NullCheck(L_2);
		Material_t2 * L_3 = Renderer_get_material_m2762(L_2, /*hidden argument*/NULL);
		Rect_t225 * L_4 = &(__this->___rect_3);
		Vector2_t7  L_5 = Rect_get_position_m2806(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Material_set_mainTextureOffset_m2807(L_3, L_5, /*hidden argument*/NULL);
		MeshRenderer_t223 * L_6 = (__this->___meshRenderer_2);
		NullCheck(L_6);
		Material_t2 * L_7 = Renderer_get_material_m2762(L_6, /*hidden argument*/NULL);
		Rect_t225 * L_8 = &(__this->___rect_3);
		Vector2_t7  L_9 = Rect_get_size_m2808(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_set_mainTextureScale_m2809(L_7, L_9, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t225 * L_11 = &(__this->___rect_3);
		float L_12 = Rect_get_width_m2798(L_11, /*hidden argument*/NULL);
		int32_t L_13 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t225 * L_14 = &(__this->___rect_3);
		float L_15 = Rect_get_height_m2800(L_14, /*hidden argument*/NULL);
		V_0 = ((float)((float)((float)((float)(((float)L_10))*(float)L_12))/(float)((float)((float)(((float)L_13))*(float)L_15))));
		Transform_t243 * L_16 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		float L_17 = V_0;
		Vector3_t215  L_18 = {0};
		Vector3__ctor_m2812(&L_18, L_17, (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localScale_m2813(L_16, L_18, /*hidden argument*/NULL);
		return;
	}
}
// SkyboxMesh
#include "AssemblyU2DCSharp_SkyboxMesh.h"
#ifndef _MSC_VER
#else
#endif
// SkyboxMesh
#include "AssemblyU2DCSharp_SkyboxMeshMethodDeclarations.h"



// System.Void SkyboxMesh::.ctor()
extern "C" void SkyboxMesh__ctor_m1414 (SkyboxMesh_t226 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SkyboxMesh::Awake()
extern Il2CppCodeGenString* _stringLiteral301;
extern "C" void SkyboxMesh_Awake_m1415 (SkyboxMesh_t226 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral301 = il2cpp_codegen_string_literal_from_index(301);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral301, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// StereoLensFlare
#include "AssemblyU2DCSharp_StereoLensFlare.h"
#ifndef _MSC_VER
#else
#endif
// StereoLensFlare
#include "AssemblyU2DCSharp_StereoLensFlareMethodDeclarations.h"



// System.Void StereoLensFlare::.ctor()
extern "C" void StereoLensFlare__ctor_m1416 (StereoLensFlare_t227 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StereoLensFlare::Awake()
extern Il2CppCodeGenString* _stringLiteral302;
extern "C" void StereoLensFlare_Awake_m1417 (StereoLensFlare_t227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral302 = il2cpp_codegen_string_literal_from_index(302);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral302, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// Cardboard/DistortionCorrectionMethod
#include "AssemblyU2DCSharp_Cardboard_DistortionCorrectionMethod.h"
#ifndef _MSC_VER
#else
#endif
// Cardboard/DistortionCorrectionMethod
#include "AssemblyU2DCSharp_Cardboard_DistortionCorrectionMethodMethodDeclarations.h"



// Cardboard/BackButtonModes
#include "AssemblyU2DCSharp_Cardboard_BackButtonModes.h"
#ifndef _MSC_VER
#else
#endif
// Cardboard/BackButtonModes
#include "AssemblyU2DCSharp_Cardboard_BackButtonModesMethodDeclarations.h"



// Cardboard/Eye
#include "AssemblyU2DCSharp_Cardboard_Eye.h"
#ifndef _MSC_VER
#else
#endif
// Cardboard/Eye
#include "AssemblyU2DCSharp_Cardboard_EyeMethodDeclarations.h"



// Cardboard/Distortion
#include "AssemblyU2DCSharp_Cardboard_Distortion.h"
#ifndef _MSC_VER
#else
#endif
// Cardboard/Distortion
#include "AssemblyU2DCSharp_Cardboard_DistortionMethodDeclarations.h"



// Cardboard/StereoScreenChangeDelegate
#include "AssemblyU2DCSharp_Cardboard_StereoScreenChangeDelegate.h"
#ifndef _MSC_VER
#else
#endif
// Cardboard/StereoScreenChangeDelegate
#include "AssemblyU2DCSharp_Cardboard_StereoScreenChangeDelegateMethodDeclarations.h"



// System.Void Cardboard/StereoScreenChangeDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void StereoScreenChangeDelegate__ctor_m1418 (StereoScreenChangeDelegate_t232 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void Cardboard/StereoScreenChangeDelegate::Invoke(UnityEngine.RenderTexture)
extern "C" void StereoScreenChangeDelegate_Invoke_m1419 (StereoScreenChangeDelegate_t232 * __this, RenderTexture_t15 * ___newStereoScreen, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		StereoScreenChangeDelegate_Invoke_m1419((StereoScreenChangeDelegate_t232 *)__this->___prev_9,___newStereoScreen, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, RenderTexture_t15 * ___newStereoScreen, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___newStereoScreen,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, RenderTexture_t15 * ___newStereoScreen, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___newStereoScreen,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___newStereoScreen,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_StereoScreenChangeDelegate_t232(Il2CppObject* delegate, RenderTexture_t15 * ___newStereoScreen)
{
	// Marshaling of parameter '___newStereoScreen' to native representation
	RenderTexture_t15 * ____newStereoScreen_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.RenderTexture'."));
}
// System.IAsyncResult Cardboard/StereoScreenChangeDelegate::BeginInvoke(UnityEngine.RenderTexture,System.AsyncCallback,System.Object)
extern "C" Object_t * StereoScreenChangeDelegate_BeginInvoke_m1420 (StereoScreenChangeDelegate_t232 * __this, RenderTexture_t15 * ___newStereoScreen, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___newStereoScreen;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void Cardboard/StereoScreenChangeDelegate::EndInvoke(System.IAsyncResult)
extern "C" void StereoScreenChangeDelegate_EndInvoke_m1421 (StereoScreenChangeDelegate_t232 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Cardboard/<EndOfFrame>c__Iterator1
#include "AssemblyU2DCSharp_Cardboard_U3CEndOfFrameU3Ec__Iterator1.h"
#ifndef _MSC_VER
#else
#endif
// Cardboard/<EndOfFrame>c__Iterator1
#include "AssemblyU2DCSharp_Cardboard_U3CEndOfFrameU3Ec__Iterator1MethodDeclarations.h"

// System.UInt32
#include "mscorlib_System_UInt32.h"
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrame.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.WaitForEndOfFrame
#include "UnityEngine_UnityEngine_WaitForEndOfFrameMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"


// System.Void Cardboard/<EndOfFrame>c__Iterator1::.ctor()
extern "C" void U3CEndOfFrameU3Ec__Iterator1__ctor_m1422 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Cardboard/<EndOfFrame>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1423 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object Cardboard/<EndOfFrame>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1424 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean Cardboard/<EndOfFrame>c__Iterator1::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t484_il2cpp_TypeInfo_var;
extern "C" bool U3CEndOfFrameU3Ec__Iterator1_MoveNext_m1425 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_005b;
	}

IL_0021:
	{
		WaitForEndOfFrame_t484 * L_2 = (WaitForEndOfFrame_t484 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t484_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2816(L_2, /*hidden argument*/NULL);
		__this->___U24current_1 = L_2;
		__this->___U24PC_0 = 1;
		goto IL_005d;
	}

IL_0038:
	{
		Cardboard_t233 * L_3 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_3);
		Cardboard_UpdateState_m1487(L_3, /*hidden argument*/NULL);
		Cardboard_t233 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		L_4->___updated_20 = 0;
		goto IL_0021;
	}
	// Dead block : IL_0054: ldarg.0

IL_005b:
	{
		return 0;
	}

IL_005d:
	{
		return 1;
	}
}
// System.Void Cardboard/<EndOfFrame>c__Iterator1::Dispose()
extern "C" void U3CEndOfFrameU3Ec__Iterator1_Dispose_m1426 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void Cardboard/<EndOfFrame>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CEndOfFrameU3Ec__Iterator1_Reset_m1427 (U3CEndOfFrameU3Ec__Iterator1_t234 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// BaseVRDevice
#include "AssemblyU2DCSharp_BaseVRDevice.h"
// CardboardProfile
#include "AssemblyU2DCSharp_CardboardProfile.h"
// Pose3D
#include "AssemblyU2DCSharp_Pose3D.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "mscorlib_ArrayTypes.h"
// System.Uri
#include "System_System_Uri.h"
// CardboardPreRender
#include "AssemblyU2DCSharp_CardboardPreRender.h"
// CardboardPostRender
#include "AssemblyU2DCSharp_CardboardPostRender.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// BaseVRDevice
#include "AssemblyU2DCSharp_BaseVRDeviceMethodDeclarations.h"
// Pose3D
#include "AssemblyU2DCSharp_Pose3DMethodDeclarations.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.Uri
#include "System_System_UriMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
struct Object_t473;
struct Cardboard_t233;
struct Object_t473;
struct Object_t;
// Declaration !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" Object_t * Object_FindObjectOfType_TisObject_t_m2818_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisObject_t_m2818(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m2818_gshared)(__this /* static, unused */, method)
// Declaration !!0 UnityEngine.Object::FindObjectOfType<Cardboard>()
// !!0 UnityEngine.Object::FindObjectOfType<Cardboard>()
#define Object_FindObjectOfType_TisCardboard_t233_m2817(__this /* static, unused */, method) (( Cardboard_t233 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m2818_gshared)(__this /* static, unused */, method)
struct GameObject_t256;
struct Cardboard_t233;
struct GameObject_t256;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m2820_gshared (GameObject_t256 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m2820(__this, method) (( Object_t * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<Cardboard>()
// !!0 UnityEngine.GameObject::AddComponent<Cardboard>()
#define GameObject_AddComponent_TisCardboard_t233_m2819(__this, method) (( Cardboard_t233 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)
struct Component_t477;
struct StereoController_t235;
// Declaration !!0 UnityEngine.Component::GetComponent<StereoController>()
// !!0 UnityEngine.Component::GetComponent<StereoController>()
#define Component_GetComponent_TisStereoController_t235_m2821(__this, method) (( StereoController_t235 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)
struct Object_t473;
struct CardboardPreRender_t247;
// Declaration !!0 UnityEngine.Object::FindObjectOfType<CardboardPreRender>()
// !!0 UnityEngine.Object::FindObjectOfType<CardboardPreRender>()
#define Object_FindObjectOfType_TisCardboardPreRender_t247_m2822(__this /* static, unused */, method) (( CardboardPreRender_t247 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m2818_gshared)(__this /* static, unused */, method)
struct Object_t473;
struct CardboardPostRender_t246;
// Declaration !!0 UnityEngine.Object::FindObjectOfType<CardboardPostRender>()
// !!0 UnityEngine.Object::FindObjectOfType<CardboardPostRender>()
#define Object_FindObjectOfType_TisCardboardPostRender_t246_m2823(__this /* static, unused */, method) (( CardboardPostRender_t246 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m2818_gshared)(__this /* static, unused */, method)


// System.Void Cardboard::.ctor()
extern "C" void Cardboard__ctor_m1428 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		__this->___vrModeEnabled_5 = 1;
		__this->___distortionCorrection_6 = 2;
		__this->___enableAlignmentMarker_7 = 1;
		__this->___enableSettingsButton_8 = 1;
		__this->___backButtonMode_9 = 1;
		__this->___tapIsTrigger_10 = 1;
		__this->___autoDriftCorrection_12 = 1;
		__this->___stereoScreenScale_16 = (1.0f);
		Vector2_t7  L_0 = {0};
		Vector2__ctor_m2714(&L_0, (0.4f), (100000.0f), /*hidden argument*/NULL);
		__this->___defaultComfortableViewingRange_18 = L_0;
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cardboard::.cctor()
extern "C" void Cardboard__cctor_m1429 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Cardboard::add_OnStereoScreenChanged(Cardboard/StereoScreenChangeDelegate)
extern TypeInfo* StereoScreenChangeDelegate_t232_il2cpp_TypeInfo_var;
extern "C" void Cardboard_add_OnStereoScreenChanged_m1430 (Cardboard_t233 * __this, StereoScreenChangeDelegate_t232 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StereoScreenChangeDelegate_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	{
		StereoScreenChangeDelegate_t232 * L_0 = (__this->___OnStereoScreenChanged_21);
		StereoScreenChangeDelegate_t232 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnStereoScreenChanged_21 = ((StereoScreenChangeDelegate_t232 *)Castclass(L_2, StereoScreenChangeDelegate_t232_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::remove_OnStereoScreenChanged(Cardboard/StereoScreenChangeDelegate)
extern TypeInfo* StereoScreenChangeDelegate_t232_il2cpp_TypeInfo_var;
extern "C" void Cardboard_remove_OnStereoScreenChanged_m1431 (Cardboard_t233 * __this, StereoScreenChangeDelegate_t232 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StereoScreenChangeDelegate_t232_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	{
		StereoScreenChangeDelegate_t232 * L_0 = (__this->___OnStereoScreenChanged_21);
		StereoScreenChangeDelegate_t232 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnStereoScreenChanged_21 = ((StereoScreenChangeDelegate_t232 *)Castclass(L_2, StereoScreenChangeDelegate_t232_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::add_OnTrigger(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_add_OnTrigger_m1432 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnTrigger_22);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnTrigger_22 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::remove_OnTrigger(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_remove_OnTrigger_m1433 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnTrigger_22);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnTrigger_22 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::add_OnTilt(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_add_OnTilt_m1434 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnTilt_23);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnTilt_23 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::remove_OnTilt(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_remove_OnTilt_m1435 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnTilt_23);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnTilt_23 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::add_OnProfileChange(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_add_OnProfileChange_m1436 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnProfileChange_24);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnProfileChange_24 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::remove_OnProfileChange(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_remove_OnProfileChange_m1437 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnProfileChange_24);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnProfileChange_24 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::add_OnBackButton(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_add_OnBackButton_m1438 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnBackButton_25);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnBackButton_25 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Cardboard::remove_OnBackButton(System.Action)
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern "C" void Cardboard_remove_OnBackButton_m1439 (Cardboard_t233 * __this, Action_t238 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t238 * L_0 = (__this->___OnBackButton_25);
		Action_t238 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnBackButton_25 = ((Action_t238 *)Castclass(L_2, Action_t238_il2cpp_TypeInfo_var));
		return;
	}
}
// Cardboard Cardboard::get_SDK()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisCardboard_t233_m2817_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCardboard_t233_m2819_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral303;
extern Il2CppCodeGenString* _stringLiteral304;
extern "C" Cardboard_t233 * Cardboard_get_SDK_m1440 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		Object_FindObjectOfType_TisCardboard_t233_m2817_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		GameObject_AddComponent_TisCardboard_t233_m2819_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483657);
		_stringLiteral303 = il2cpp_codegen_string_literal_from_index(303);
		_stringLiteral304 = il2cpp_codegen_string_literal_from_index(304);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t256 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2;
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Cardboard_t233 * L_2 = Object_FindObjectOfType_TisCardboard_t233_m2817(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisCardboard_t233_m2817_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2 = L_2;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2;
		bool L_4 = Object_op_Equality_m2716(NULL /*static, unused*/, L_3, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral303, /*hidden argument*/NULL);
		GameObject_t256 * L_5 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_5, _stringLiteral304, /*hidden argument*/NULL);
		V_0 = L_5;
		GameObject_t256 * L_6 = V_0;
		NullCheck(L_6);
		Cardboard_t233 * L_7 = GameObject_AddComponent_TisCardboard_t233_m2819(L_6, /*hidden argument*/GameObject_AddComponent_TisCardboard_t233_m2819_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2 = L_7;
		GameObject_t256 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t243 * L_9 = GameObject_get_transform_m2825(L_8, /*hidden argument*/NULL);
		Vector3_t215  L_10 = Vector3_get_zero_m2826(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localPosition_m2766(L_9, L_10, /*hidden argument*/NULL);
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_11 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2;
		return L_11;
	}
}
// StereoController Cardboard::get_Controller()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisStereoController_t235_m2821_MethodInfo_var;
extern "C" StereoController_t235 * Cardboard_get_Controller_m1441 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Component_GetComponent_TisStereoController_t235_m2821_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t14 * V_0 = {0};
	{
		Camera_t14 * L_0 = Camera_get_main_m2827(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t14 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Camera_t14 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___currentMainCamera_3;
		bool L_3 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		StereoController_t235 * L_4 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___currentController_4;
		bool L_5 = Object_op_Equality_m2716(NULL /*static, unused*/, L_4, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}

IL_0026:
	{
		Camera_t14 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___currentMainCamera_3 = L_6;
		Camera_t14 * L_7 = V_0;
		NullCheck(L_7);
		StereoController_t235 * L_8 = Component_GetComponent_TisStereoController_t235_m2821(L_7, /*hidden argument*/Component_GetComponent_TisStereoController_t235_m2821_MethodInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___currentController_4 = L_8;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		StereoController_t235 * L_9 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___currentController_4;
		return L_9;
	}
}
// System.Boolean Cardboard::get_VRModeEnabled()
extern "C" bool Cardboard_get_VRModeEnabled_m1442 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___vrModeEnabled_5);
		return L_0;
	}
}
// System.Void Cardboard::set_VRModeEnabled(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_VRModeEnabled_m1443 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		bool L_1 = (__this->___vrModeEnabled_5);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_4 = ___value;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void BaseVRDevice::SetVRModeEnabled(System.Boolean) */, L_3, L_4);
	}

IL_0021:
	{
		bool L_5 = ___value;
		__this->___vrModeEnabled_5 = L_5;
		return;
	}
}
// Cardboard/DistortionCorrectionMethod Cardboard::get_DistortionCorrection()
extern "C" int32_t Cardboard_get_DistortionCorrection_m1444 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___distortionCorrection_6);
		return L_0;
	}
}
// System.Void Cardboard::set_DistortionCorrection(Cardboard/DistortionCorrectionMethod)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_DistortionCorrection_m1445 (Cardboard_t233 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	BaseVRDevice_t236 * G_B4_0 = {0};
	BaseVRDevice_t236 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	BaseVRDevice_t236 * G_B5_1 = {0};
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (__this->___distortionCorrection_6);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_4 = ___value;
		G_B3_0 = L_3;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			G_B4_0 = L_3;
			goto IL_002a;
		}
	}
	{
		bool L_5 = Cardboard_get_NativeDistortionCorrectionSupported_m1462(__this, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_5));
		G_B5_1 = G_B3_0;
		goto IL_002b;
	}

IL_002a:
	{
		G_B5_0 = 0;
		G_B5_1 = G_B4_0;
	}

IL_002b:
	{
		NullCheck(G_B5_1);
		VirtActionInvoker1< bool >::Invoke(6 /* System.Void BaseVRDevice::SetDistortionCorrectionEnabled(System.Boolean) */, G_B5_1, G_B5_0);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_6 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void BaseVRDevice::UpdateScreenData() */, L_6);
	}

IL_003a:
	{
		int32_t L_7 = ___value;
		__this->___distortionCorrection_6 = L_7;
		return;
	}
}
// System.Boolean Cardboard::get_EnableAlignmentMarker()
extern "C" bool Cardboard_get_EnableAlignmentMarker_m1446 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___enableAlignmentMarker_7);
		return L_0;
	}
}
// System.Void Cardboard::set_EnableAlignmentMarker(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_EnableAlignmentMarker_m1447 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		bool L_1 = (__this->___enableAlignmentMarker_7);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_4 = ___value;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(9 /* System.Void BaseVRDevice::SetAlignmentMarkerEnabled(System.Boolean) */, L_3, L_4);
	}

IL_0021:
	{
		bool L_5 = ___value;
		__this->___enableAlignmentMarker_7 = L_5;
		return;
	}
}
// System.Boolean Cardboard::get_EnableSettingsButton()
extern "C" bool Cardboard_get_EnableSettingsButton_m1448 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___enableSettingsButton_8);
		return L_0;
	}
}
// System.Void Cardboard::set_EnableSettingsButton(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_EnableSettingsButton_m1449 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		bool L_1 = (__this->___enableSettingsButton_8);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_4 = ___value;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void BaseVRDevice::SetSettingsButtonEnabled(System.Boolean) */, L_3, L_4);
	}

IL_0021:
	{
		bool L_5 = ___value;
		__this->___enableSettingsButton_8 = L_5;
		return;
	}
}
// Cardboard/BackButtonModes Cardboard::get_BackButtonMode()
extern "C" int32_t Cardboard_get_BackButtonMode_m1450 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___backButtonMode_9);
		return L_0;
	}
}
// System.Void Cardboard::set_BackButtonMode(Cardboard/BackButtonModes)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_BackButtonMode_m1451 (Cardboard_t233 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (__this->___backButtonMode_9);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_4 = ___value;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void BaseVRDevice::SetVRBackButtonEnabled(System.Boolean) */, L_3, ((((int32_t)((((int32_t)L_4) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		BaseVRDevice_t236 * L_5 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_6 = ___value;
		NullCheck(L_5);
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void BaseVRDevice::SetShowVrBackButtonOnlyInVR(System.Boolean) */, L_5, ((((int32_t)L_6) == ((int32_t)1))? 1 : 0));
	}

IL_0035:
	{
		int32_t L_7 = ___value;
		__this->___backButtonMode_9 = L_7;
		return;
	}
}
// System.Boolean Cardboard::get_TapIsTrigger()
extern "C" bool Cardboard_get_TapIsTrigger_m1452 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___tapIsTrigger_10);
		return L_0;
	}
}
// System.Void Cardboard::set_TapIsTrigger(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_TapIsTrigger_m1453 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		bool L_1 = (__this->___tapIsTrigger_10);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_4 = ___value;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void BaseVRDevice::SetTapIsTrigger(System.Boolean) */, L_3, L_4);
	}

IL_0021:
	{
		bool L_5 = ___value;
		__this->___tapIsTrigger_10 = L_5;
		return;
	}
}
// System.Single Cardboard::get_NeckModelScale()
extern "C" float Cardboard_get_NeckModelScale_m1454 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___neckModelScale_11);
		return L_0;
	}
}
// System.Void Cardboard::set_NeckModelScale(System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_NeckModelScale_m1455 (Cardboard_t233 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___value = L_1;
		float L_2 = ___value;
		float L_3 = (__this->___neckModelScale_11);
		bool L_4 = Mathf_Approximately_m2829(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_5 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_6 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		float L_7 = ___value;
		NullCheck(L_6);
		VirtActionInvoker1< float >::Invoke(13 /* System.Void BaseVRDevice::SetNeckModelScale(System.Single) */, L_6, L_7);
	}

IL_002e:
	{
		float L_8 = ___value;
		__this->___neckModelScale_11 = L_8;
		return;
	}
}
// System.Boolean Cardboard::get_AutoDriftCorrection()
extern "C" bool Cardboard_get_AutoDriftCorrection_m1456 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___autoDriftCorrection_12);
		return L_0;
	}
}
// System.Void Cardboard::set_AutoDriftCorrection(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_AutoDriftCorrection_m1457 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		bool L_1 = (__this->___autoDriftCorrection_12);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_4 = ___value;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(14 /* System.Void BaseVRDevice::SetAutoDriftCorrectionEnabled(System.Boolean) */, L_3, L_4);
	}

IL_0021:
	{
		bool L_5 = ___value;
		__this->___autoDriftCorrection_12 = L_5;
		return;
	}
}
// System.Boolean Cardboard::get_ElectronicDisplayStabilization()
extern "C" bool Cardboard_get_ElectronicDisplayStabilization_m1458 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___electronicDisplayStabilization_13);
		return L_0;
	}
}
// System.Void Cardboard::set_ElectronicDisplayStabilization(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_ElectronicDisplayStabilization_m1459 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		bool L_1 = (__this->___electronicDisplayStabilization_13);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_4 = ___value;
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(15 /* System.Void BaseVRDevice::SetElectronicDisplayStabilizationEnabled(System.Boolean) */, L_3, L_4);
	}

IL_0021:
	{
		bool L_5 = ___value;
		__this->___electronicDisplayStabilization_13 = L_5;
		return;
	}
}
// System.Boolean Cardboard::get_SyncWithCardboardApp()
extern "C" bool Cardboard_get_SyncWithCardboardApp_m1460 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___syncWithCardboardApp_14);
		return L_0;
	}
}
// System.Void Cardboard::set_SyncWithCardboardApp(System.Boolean)
extern Il2CppCodeGenString* _stringLiteral305;
extern "C" void Cardboard_set_SyncWithCardboardApp_m1461 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral305 = il2cpp_codegen_string_literal_from_index(305);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___value;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		bool L_1 = ___value;
		bool L_2 = (__this->___syncWithCardboardApp_14);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		Debug_LogWarning_m2781(NULL /*static, unused*/, _stringLiteral305, /*hidden argument*/NULL);
	}

IL_001c:
	{
		bool L_3 = ___value;
		__this->___syncWithCardboardApp_14 = L_3;
		return;
	}
}
// System.Boolean Cardboard::get_NativeDistortionCorrectionSupported()
extern "C" bool Cardboard_get_NativeDistortionCorrectionSupported_m1462 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CNativeDistortionCorrectionSupportedU3Ek__BackingField_26);
		return L_0;
	}
}
// System.Void Cardboard::set_NativeDistortionCorrectionSupported(System.Boolean)
extern "C" void Cardboard_set_NativeDistortionCorrectionSupported_m1463 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CNativeDistortionCorrectionSupportedU3Ek__BackingField_26 = L_0;
		return;
	}
}
// System.Boolean Cardboard::get_NativeUILayerSupported()
extern "C" bool Cardboard_get_NativeUILayerSupported_m1464 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CNativeUILayerSupportedU3Ek__BackingField_27);
		return L_0;
	}
}
// System.Void Cardboard::set_NativeUILayerSupported(System.Boolean)
extern "C" void Cardboard_set_NativeUILayerSupported_m1465 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CNativeUILayerSupportedU3Ek__BackingField_27 = L_0;
		return;
	}
}
// System.Single Cardboard::get_StereoScreenScale()
extern "C" float Cardboard_get_StereoScreenScale_m1466 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___stereoScreenScale_16);
		return L_0;
	}
}
// System.Void Cardboard::set_StereoScreenScale(System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void Cardboard_set_StereoScreenScale_m1467 (Cardboard_t233 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_0, (0.1f), (10.0f), /*hidden argument*/NULL);
		___value = L_1;
		float L_2 = (__this->___stereoScreenScale_16);
		float L_3 = ___value;
		if ((((float)L_2) == ((float)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		float L_4 = ___value;
		__this->___stereoScreenScale_16 = L_4;
		Cardboard_set_StereoScreen_m1469(__this, (RenderTexture_t15 *)NULL, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// UnityEngine.RenderTexture Cardboard::get_StereoScreen()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" RenderTexture_t15 * Cardboard_get_StereoScreen_m1468 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___distortionCorrection_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (__this->___vrModeEnabled_5);
		if (L_1)
		{
			goto IL_0018;
		}
	}

IL_0016:
	{
		return (RenderTexture_t15 *)NULL;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		RenderTexture_t15 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17;
		bool L_3 = Object_op_Equality_m2716(NULL /*static, unused*/, L_2, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_4 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_4);
		RenderTexture_t15 * L_5 = (RenderTexture_t15 *)VirtFuncInvoker0< RenderTexture_t15 * >::Invoke(19 /* UnityEngine.RenderTexture BaseVRDevice::CreateStereoScreen() */, L_4);
		Cardboard_set_StereoScreen_m1469(__this, L_5, /*hidden argument*/NULL);
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		RenderTexture_t15 * L_6 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17;
		return L_6;
	}
}
// System.Void Cardboard::set_StereoScreen(UnityEngine.RenderTexture)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral306;
extern "C" void Cardboard_set_StereoScreen_m1469 (Cardboard_t233 * __this, RenderTexture_t15 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral306 = il2cpp_codegen_string_literal_from_index(306);
		s_Il2CppMethodIntialized = true;
	}
	{
		RenderTexture_t15 * L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		RenderTexture_t15 * L_1 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17;
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		bool L_3 = SystemInfo_get_supportsRenderTextures_m2780(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		RenderTexture_t15 * L_4 = ___value;
		bool L_5 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_4, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0032;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral306, /*hidden argument*/NULL);
		return;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		RenderTexture_t15 * L_6 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17;
		bool L_7 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_6, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		RenderTexture_t15 * L_8 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17;
		NullCheck(L_8);
		RenderTexture_Release_m2782(L_8, /*hidden argument*/NULL);
	}

IL_004c:
	{
		RenderTexture_t15 * L_9 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17 = L_9;
		BaseVRDevice_t236 * L_10 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_11 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		RenderTexture_t15 * L_12 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17;
		NullCheck(L_11);
		VirtActionInvoker1< RenderTexture_t15 * >::Invoke(7 /* System.Void BaseVRDevice::SetStereoScreen(UnityEngine.RenderTexture) */, L_11, L_12);
	}

IL_006b:
	{
		StereoScreenChangeDelegate_t232 * L_13 = (__this->___OnStereoScreenChanged_21);
		if (!L_13)
		{
			goto IL_0086;
		}
	}
	{
		StereoScreenChangeDelegate_t232 * L_14 = (__this->___OnStereoScreenChanged_21);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		RenderTexture_t15 * L_15 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___stereoScreen_17;
		NullCheck(L_14);
		StereoScreenChangeDelegate_Invoke_m1419(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0086:
	{
		return;
	}
}
// CardboardProfile Cardboard::get_Profile()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" CardboardProfile_t255 * Cardboard_get_Profile_m1470 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		CardboardProfile_t255 * L_1 = BaseVRDevice_get_Profile_m1672(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Pose3D Cardboard::get_HeadPose()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" Pose3D_t260 * Cardboard_get_HeadPose_m1471 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		Pose3D_t260 * L_1 = BaseVRDevice_GetHeadPose_m1681(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Pose3D Cardboard::EyePose(Cardboard/Eye)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" Pose3D_t260 * Cardboard_EyePose_m1472 (Cardboard_t233 * __this, int32_t ___eye, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_1 = ___eye;
		NullCheck(L_0);
		Pose3D_t260 * L_2 = BaseVRDevice_GetEyePose_m1682(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Matrix4x4 Cardboard::Projection(Cardboard/Eye,Cardboard/Distortion)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t242  Cardboard_Projection_m1473 (Cardboard_t233 * __this, int32_t ___eye, int32_t ___distortion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_1 = ___eye;
		int32_t L_2 = ___distortion;
		NullCheck(L_0);
		Matrix4x4_t242  L_3 = BaseVRDevice_GetProjection_m1683(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Rect Cardboard::Viewport(Cardboard/Eye,Cardboard/Distortion)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" Rect_t225  Cardboard_Viewport_m1474 (Cardboard_t233 * __this, int32_t ___eye, int32_t ___distortion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_1 = ___eye;
		int32_t L_2 = ___distortion;
		NullCheck(L_0);
		Rect_t225  L_3 = BaseVRDevice_GetViewport_m1684(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector2 Cardboard::get_ComfortableViewingRange()
extern "C" Vector2_t7  Cardboard_get_ComfortableViewingRange_m1475 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = (__this->___defaultComfortableViewingRange_18);
		return L_0;
	}
}
// System.Void Cardboard::InitDevice()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* BaseVRDevice_t236_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t468_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t237_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2831_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2832_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral307;
extern Il2CppCodeGenString* _stringLiteral308;
extern Il2CppCodeGenString* _stringLiteral309;
extern Il2CppCodeGenString* _stringLiteral310;
extern "C" void Cardboard_InitDevice_m1476 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		BaseVRDevice_t236_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(176);
		List_1_t468_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		Uri_t237_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(178);
		List_1__ctor_m2831_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		List_1_ToArray_m2832_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		_stringLiteral307 = il2cpp_codegen_string_literal_from_index(307);
		_stringLiteral308 = il2cpp_codegen_string_literal_from_index(308);
		_stringLiteral309 = il2cpp_codegen_string_literal_from_index(309);
		_stringLiteral310 = il2cpp_codegen_string_literal_from_index(310);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t468 * V_0 = {0};
	BaseVRDevice_t236 * G_B10_0 = {0};
	BaseVRDevice_t236 * G_B9_0 = {0};
	int32_t G_B11_0 = 0;
	BaseVRDevice_t236 * G_B11_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_1 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(31 /* System.Void BaseVRDevice::Destroy() */, L_1);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = BaseVRDevice_GetDevice_m1693(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15 = L_2;
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(4 /* System.Void BaseVRDevice::Init() */, L_3);
		List_1_t468 * L_4 = (List_1_t468 *)il2cpp_codegen_object_new (List_1_t468_il2cpp_TypeInfo_var);
		List_1__ctor_m2831(L_4, /*hidden argument*/List_1__ctor_m2831_MethodInfo_var);
		V_0 = L_4;
		BaseVRDevice_t236 * L_5 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		List_1_t468 * L_6 = V_0;
		NullCheck(L_5);
		bool L_7 = (bool)VirtFuncInvoker1< bool, List_1_t468 * >::Invoke(17 /* System.Boolean BaseVRDevice::SupportsNativeDistortionCorrection(System.Collections.Generic.List`1<System.String>) */, L_5, L_6);
		Cardboard_set_NativeDistortionCorrectionSupported_m1463(__this, L_7, /*hidden argument*/NULL);
		List_1_t468 * L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_8);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_006f;
		}
	}
	{
		List_1_t468 * L_10 = V_0;
		NullCheck(L_10);
		StringU5BU5D_t398* L_11 = List_1_ToArray_m2832(L_10, /*hidden argument*/List_1_ToArray_m2832_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Join_m2833(NULL /*static, unused*/, _stringLiteral308, L_11, /*hidden argument*/NULL);
		String_t* L_13 = String_Concat_m2834(NULL /*static, unused*/, _stringLiteral307, L_12, _stringLiteral309, /*hidden argument*/NULL);
		Debug_LogWarning_m2781(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_006f:
	{
		List_1_t468 * L_14 = V_0;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_14);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_15 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		List_1_t468 * L_16 = V_0;
		NullCheck(L_15);
		bool L_17 = (bool)VirtFuncInvoker1< bool, List_1_t468 * >::Invoke(18 /* System.Boolean BaseVRDevice::SupportsNativeUILayer(System.Collections.Generic.List`1<System.String>) */, L_15, L_16);
		Cardboard_set_NativeUILayerSupported_m1465(__this, L_17, /*hidden argument*/NULL);
		List_1_t468 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_18);
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_00b6;
		}
	}
	{
		List_1_t468 * L_20 = V_0;
		NullCheck(L_20);
		StringU5BU5D_t398* L_21 = List_1_ToArray_m2832(L_20, /*hidden argument*/List_1_ToArray_m2832_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Join_m2833(NULL /*static, unused*/, _stringLiteral308, L_21, /*hidden argument*/NULL);
		String_t* L_23 = String_Concat_m2834(NULL /*static, unused*/, _stringLiteral310, L_22, _stringLiteral309, /*hidden argument*/NULL);
		Debug_LogWarning_m2781(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
	}

IL_00b6:
	{
		Uri_t237 * L_24 = (__this->___DefaultDeviceProfile_19);
		IL2CPP_RUNTIME_CLASS_INIT(Uri_t237_il2cpp_TypeInfo_var);
		bool L_25 = Uri_op_Inequality_m2835(NULL /*static, unused*/, L_24, (Uri_t237 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_26 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		Uri_t237 * L_27 = (__this->___DefaultDeviceProfile_19);
		NullCheck(L_26);
		VirtFuncInvoker1< bool, Uri_t237 * >::Invoke(20 /* System.Boolean BaseVRDevice::SetDefaultDeviceProfile(System.Uri) */, L_26, L_27);
	}

IL_00d8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_28 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_29 = (__this->___enableAlignmentMarker_7);
		NullCheck(L_28);
		VirtActionInvoker1< bool >::Invoke(9 /* System.Void BaseVRDevice::SetAlignmentMarkerEnabled(System.Boolean) */, L_28, L_29);
		BaseVRDevice_t236 * L_30 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_31 = (__this->___enableSettingsButton_8);
		NullCheck(L_30);
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void BaseVRDevice::SetSettingsButtonEnabled(System.Boolean) */, L_30, L_31);
		BaseVRDevice_t236 * L_32 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_33 = (__this->___backButtonMode_9);
		NullCheck(L_32);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void BaseVRDevice::SetVRBackButtonEnabled(System.Boolean) */, L_32, ((((int32_t)((((int32_t)L_33) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		BaseVRDevice_t236 * L_34 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_35 = (__this->___backButtonMode_9);
		NullCheck(L_34);
		VirtActionInvoker1< bool >::Invoke(11 /* System.Void BaseVRDevice::SetShowVrBackButtonOnlyInVR(System.Boolean) */, L_34, ((((int32_t)L_35) == ((int32_t)1))? 1 : 0));
		BaseVRDevice_t236 * L_36 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_37 = (__this->___distortionCorrection_6);
		G_B9_0 = L_36;
		if ((!(((uint32_t)L_37) == ((uint32_t)1))))
		{
			G_B10_0 = L_36;
			goto IL_013a;
		}
	}
	{
		bool L_38 = Cardboard_get_NativeDistortionCorrectionSupported_m1462(__this, /*hidden argument*/NULL);
		G_B11_0 = ((int32_t)(L_38));
		G_B11_1 = G_B9_0;
		goto IL_013b;
	}

IL_013a:
	{
		G_B11_0 = 0;
		G_B11_1 = G_B10_0;
	}

IL_013b:
	{
		NullCheck(G_B11_1);
		VirtActionInvoker1< bool >::Invoke(6 /* System.Void BaseVRDevice::SetDistortionCorrectionEnabled(System.Boolean) */, G_B11_1, G_B11_0);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_39 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_40 = (__this->___tapIsTrigger_10);
		NullCheck(L_39);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void BaseVRDevice::SetTapIsTrigger(System.Boolean) */, L_39, L_40);
		BaseVRDevice_t236 * L_41 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		float L_42 = (__this->___neckModelScale_11);
		NullCheck(L_41);
		VirtActionInvoker1< float >::Invoke(13 /* System.Void BaseVRDevice::SetNeckModelScale(System.Single) */, L_41, L_42);
		BaseVRDevice_t236 * L_43 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_44 = (__this->___autoDriftCorrection_12);
		NullCheck(L_43);
		VirtActionInvoker1< bool >::Invoke(14 /* System.Void BaseVRDevice::SetAutoDriftCorrectionEnabled(System.Boolean) */, L_43, L_44);
		BaseVRDevice_t236 * L_45 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_46 = (__this->___electronicDisplayStabilization_13);
		NullCheck(L_45);
		VirtActionInvoker1< bool >::Invoke(15 /* System.Void BaseVRDevice::SetElectronicDisplayStabilizationEnabled(System.Boolean) */, L_45, L_46);
		BaseVRDevice_t236 * L_47 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_48 = (__this->___vrModeEnabled_5);
		NullCheck(L_47);
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void BaseVRDevice::SetVRModeEnabled(System.Boolean) */, L_47, L_48);
		BaseVRDevice_t236 * L_49 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_49);
		VirtActionInvoker0::Invoke(23 /* System.Void BaseVRDevice::UpdateScreenData() */, L_49);
		return;
	}
}
// System.Void Cardboard::Awake()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral311;
extern "C" void Cardboard_Awake_m1477 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral311 = il2cpp_codegen_string_literal_from_index(311);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2;
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2 = __this;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2;
		bool L_3 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		Debug_LogWarning_m2781(NULL /*static, unused*/, _stringLiteral311, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		Application_set_targetFrameRate_m2836(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		Screen_set_sleepTimeout_m2837(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		Cardboard_InitDevice_m1476(__this, /*hidden argument*/NULL);
		Cardboard_set_StereoScreen_m1469(__this, (RenderTexture_t15 *)NULL, /*hidden argument*/NULL);
		Cardboard_AddCardboardCamera_m1478(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cardboard::AddCardboardCamera()
extern const Il2CppType* CardboardPreRender_t247_0_0_0_var;
extern const Il2CppType* CardboardPostRender_t246_0_0_0_var;
extern TypeInfo* TypeU5BU5D_t485_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisCardboardPreRender_t247_m2822_MethodInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisCardboardPostRender_t246_m2823_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral312;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral314;
extern "C" void Cardboard_AddCardboardCamera_m1478 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardPreRender_t247_0_0_0_var = il2cpp_codegen_type_from_index(179);
		CardboardPostRender_t246_0_0_0_var = il2cpp_codegen_type_from_index(180);
		TypeU5BU5D_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		Object_FindObjectOfType_TisCardboardPreRender_t247_m2822_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483661);
		Object_FindObjectOfType_TisCardboardPostRender_t246_m2823_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		_stringLiteral312 = il2cpp_codegen_string_literal_from_index(312);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral314 = il2cpp_codegen_string_literal_from_index(314);
		s_Il2CppMethodIntialized = true;
	}
	CardboardPreRender_t247 * V_0 = {0};
	GameObject_t256 * V_1 = {0};
	CardboardPostRender_t246 * V_2 = {0};
	GameObject_t256 * V_3 = {0};
	{
		CardboardPreRender_t247 * L_0 = Object_FindObjectOfType_TisCardboardPreRender_t247_m2822(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisCardboardPreRender_t247_m2822_MethodInfo_var);
		V_0 = L_0;
		CardboardPreRender_t247 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		TypeU5BU5D_t485* L_3 = ((TypeU5BU5D_t485*)SZArrayNew(TypeU5BU5D_t485_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m2838(NULL /*static, unused*/, LoadTypeToken(CardboardPreRender_t247_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_3, 0)) = (Type_t *)L_4;
		GameObject_t256 * L_5 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2839(L_5, _stringLiteral312, L_3, /*hidden argument*/NULL);
		V_1 = L_5;
		GameObject_t256 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_SendMessage_m2840(L_6, _stringLiteral313, /*hidden argument*/NULL);
		GameObject_t256 * L_7 = V_1;
		NullCheck(L_7);
		Transform_t243 * L_8 = GameObject_get_transform_m2825(L_7, /*hidden argument*/NULL);
		Transform_t243 * L_9 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_parent_m2841(L_8, L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		CardboardPostRender_t246 * L_10 = Object_FindObjectOfType_TisCardboardPostRender_t246_m2823(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisCardboardPostRender_t246_m2823_MethodInfo_var);
		V_2 = L_10;
		CardboardPostRender_t246 * L_11 = V_2;
		bool L_12 = Object_op_Equality_m2716(NULL /*static, unused*/, L_11, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0098;
		}
	}
	{
		TypeU5BU5D_t485* L_13 = ((TypeU5BU5D_t485*)SZArrayNew(TypeU5BU5D_t485_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_14 = Type_GetTypeFromHandle_m2838(NULL /*static, unused*/, LoadTypeToken(CardboardPostRender_t246_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		ArrayElementTypeCheck (L_13, L_14);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_13, 0)) = (Type_t *)L_14;
		GameObject_t256 * L_15 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2839(L_15, _stringLiteral314, L_13, /*hidden argument*/NULL);
		V_3 = L_15;
		GameObject_t256 * L_16 = V_3;
		NullCheck(L_16);
		GameObject_SendMessage_m2840(L_16, _stringLiteral313, /*hidden argument*/NULL);
		GameObject_t256 * L_17 = V_3;
		NullCheck(L_17);
		Transform_t243 * L_18 = GameObject_get_transform_m2825(L_17, /*hidden argument*/NULL);
		Transform_t243 * L_19 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_set_parent_m2841(L_18, L_19, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Boolean Cardboard::get_Triggered()
extern "C" bool Cardboard_get_Triggered_m1479 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CTriggeredU3Ek__BackingField_28);
		return L_0;
	}
}
// System.Void Cardboard::set_Triggered(System.Boolean)
extern "C" void Cardboard_set_Triggered_m1480 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CTriggeredU3Ek__BackingField_28 = L_0;
		return;
	}
}
// System.Boolean Cardboard::get_Tilted()
extern "C" bool Cardboard_get_Tilted_m1481 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CTiltedU3Ek__BackingField_29);
		return L_0;
	}
}
// System.Void Cardboard::set_Tilted(System.Boolean)
extern "C" void Cardboard_set_Tilted_m1482 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CTiltedU3Ek__BackingField_29 = L_0;
		return;
	}
}
// System.Boolean Cardboard::get_ProfileChanged()
extern "C" bool Cardboard_get_ProfileChanged_m1483 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CProfileChangedU3Ek__BackingField_30);
		return L_0;
	}
}
// System.Void Cardboard::set_ProfileChanged(System.Boolean)
extern "C" void Cardboard_set_ProfileChanged_m1484 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CProfileChangedU3Ek__BackingField_30 = L_0;
		return;
	}
}
// System.Boolean Cardboard::get_BackButtonPressed()
extern "C" bool Cardboard_get_BackButtonPressed_m1485 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CBackButtonPressedU3Ek__BackingField_31);
		return L_0;
	}
}
// System.Void Cardboard::set_BackButtonPressed(System.Boolean)
extern "C" void Cardboard_set_BackButtonPressed_m1486 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CBackButtonPressedU3Ek__BackingField_31 = L_0;
		return;
	}
}
// System.Void Cardboard::UpdateState()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_UpdateState_m1487 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___updated_20);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		__this->___updated_20 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_1 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(22 /* System.Void BaseVRDevice::UpdateState() */, L_1);
		Cardboard_DispatchEvents_m1488(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void Cardboard::DispatchEvents()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" void Cardboard_DispatchEvents_m1488 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	Cardboard_t233 * G_B2_0 = {0};
	Cardboard_t233 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Cardboard_t233 * G_B3_1 = {0};
	Cardboard_t233 * G_B5_0 = {0};
	Cardboard_t233 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Cardboard_t233 * G_B6_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		bool L_1 = (L_0->___triggered_13);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButtonDown_m2842(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		NullCheck(G_B3_1);
		Cardboard_set_Triggered_m1480(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_3 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_3);
		bool L_4 = (L_3->___tilted_14);
		Cardboard_set_Tilted_m1482(__this, L_4, /*hidden argument*/NULL);
		BaseVRDevice_t236 * L_5 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_5);
		bool L_6 = (L_5->___profileChanged_15);
		Cardboard_set_ProfileChanged_m1484(__this, L_6, /*hidden argument*/NULL);
		BaseVRDevice_t236 * L_7 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_7);
		bool L_8 = (L_7->___backButtonPressed_16);
		G_B4_0 = __this;
		if (L_8)
		{
			G_B5_0 = __this;
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetKeyDown_m2843(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_9));
		G_B6_1 = G_B4_0;
		goto IL_0058;
	}

IL_0057:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0058:
	{
		NullCheck(G_B6_1);
		Cardboard_set_BackButtonPressed_m1486(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_10 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_10);
		L_10->___triggered_13 = 0;
		BaseVRDevice_t236 * L_11 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_11);
		L_11->___tilted_14 = 0;
		BaseVRDevice_t236 * L_12 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_12);
		L_12->___profileChanged_15 = 0;
		BaseVRDevice_t236 * L_13 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_13);
		L_13->___backButtonPressed_16 = 0;
		bool L_14 = Cardboard_get_Tilted_m1481(__this, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00aa;
		}
	}
	{
		Action_t238 * L_15 = (__this->___OnTilt_23);
		if (!L_15)
		{
			goto IL_00aa;
		}
	}
	{
		Action_t238 * L_16 = (__this->___OnTilt_23);
		NullCheck(L_16);
		Action_Invoke_m2844(L_16, /*hidden argument*/NULL);
	}

IL_00aa:
	{
		bool L_17 = Cardboard_get_Triggered_m1479(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00cb;
		}
	}
	{
		Action_t238 * L_18 = (__this->___OnTrigger_22);
		if (!L_18)
		{
			goto IL_00cb;
		}
	}
	{
		Action_t238 * L_19 = (__this->___OnTrigger_22);
		NullCheck(L_19);
		Action_Invoke_m2844(L_19, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		bool L_20 = Cardboard_get_ProfileChanged_m1483(__this, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00ec;
		}
	}
	{
		Action_t238 * L_21 = (__this->___OnProfileChange_24);
		if (!L_21)
		{
			goto IL_00ec;
		}
	}
	{
		Action_t238 * L_22 = (__this->___OnProfileChange_24);
		NullCheck(L_22);
		Action_Invoke_m2844(L_22, /*hidden argument*/NULL);
	}

IL_00ec:
	{
		bool L_23 = Cardboard_get_BackButtonPressed_m1485(__this, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_010d;
		}
	}
	{
		Action_t238 * L_24 = (__this->___OnBackButton_25);
		if (!L_24)
		{
			goto IL_010d;
		}
	}
	{
		Action_t238 * L_25 = (__this->___OnBackButton_25);
		NullCheck(L_25);
		Action_Invoke_m2844(L_25, /*hidden argument*/NULL);
	}

IL_010d:
	{
		return;
	}
}
// System.Collections.IEnumerator Cardboard::EndOfFrame()
extern TypeInfo* U3CEndOfFrameU3Ec__Iterator1_t234_il2cpp_TypeInfo_var;
extern "C" Object_t * Cardboard_EndOfFrame_m1489 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CEndOfFrameU3Ec__Iterator1_t234_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(184);
		s_Il2CppMethodIntialized = true;
	}
	U3CEndOfFrameU3Ec__Iterator1_t234 * V_0 = {0};
	{
		U3CEndOfFrameU3Ec__Iterator1_t234 * L_0 = (U3CEndOfFrameU3Ec__Iterator1_t234 *)il2cpp_codegen_object_new (U3CEndOfFrameU3Ec__Iterator1_t234_il2cpp_TypeInfo_var);
		U3CEndOfFrameU3Ec__Iterator1__ctor_m1422(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEndOfFrameU3Ec__Iterator1_t234 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CEndOfFrameU3Ec__Iterator1_t234 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Cardboard::PostRender()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_PostRender_m1490 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Cardboard_get_NativeDistortionCorrectionSupported_m1462(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_1 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(25 /* System.Void BaseVRDevice::PostRender() */, L_1);
	}

IL_0015:
	{
		return;
	}
}
// System.Void Cardboard::Recenter()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_Recenter_m1491 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(24 /* System.Void BaseVRDevice::Recenter() */, L_0);
		return;
	}
}
// System.Void Cardboard::SetTouchCoordinates(System.Int32,System.Int32)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_SetTouchCoordinates_m1492 (Cardboard_t233 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_1 = ___x;
		int32_t L_2 = ___y;
		NullCheck(L_0);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(26 /* System.Void BaseVRDevice::SetTouchCoordinates(System.Int32,System.Int32) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void Cardboard::ShowSettingsDialog()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_ShowSettingsDialog_m1493 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(21 /* System.Void BaseVRDevice::ShowSettingsDialog() */, L_0);
		return;
	}
}
// System.Void Cardboard::OnEnable()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral315;
extern "C" void Cardboard_OnEnable_m1494 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(27 /* System.Void BaseVRDevice::OnPause(System.Boolean) */, L_0, 0);
		MonoBehaviour_StartCoroutine_m2845(__this, _stringLiteral315, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Cardboard::OnDisable()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral315;
extern "C" void Cardboard_OnDisable_m1495 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StopCoroutine_m2846(__this, _stringLiteral315, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(27 /* System.Void BaseVRDevice::OnPause(System.Boolean) */, L_0, 1);
		return;
	}
}
// System.Void Cardboard::OnApplicationPause(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_OnApplicationPause_m1496 (Cardboard_t233 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_1 = ___pause;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(27 /* System.Void BaseVRDevice::OnPause(System.Boolean) */, L_0, L_1);
		return;
	}
}
// System.Void Cardboard::OnApplicationFocus(System.Boolean)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_OnApplicationFocus_m1497 (Cardboard_t233 * __this, bool ___focus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		bool L_1 = ___focus;
		NullCheck(L_0);
		VirtActionInvoker1< bool >::Invoke(28 /* System.Void BaseVRDevice::OnFocus(System.Boolean) */, L_0, L_1);
		return;
	}
}
// System.Void Cardboard::OnLevelWasLoaded(System.Int32)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_OnLevelWasLoaded_m1498 (Cardboard_t233 * __this, int32_t ___level, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		int32_t L_1 = ___level;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(29 /* System.Void BaseVRDevice::OnLevelLoaded(System.Int32) */, L_0, L_1);
		return;
	}
}
// System.Void Cardboard::OnApplicationQuit()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_OnApplicationQuit_m1499 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(30 /* System.Void BaseVRDevice::OnApplicationQuit() */, L_0);
		return;
	}
}
// System.Void Cardboard::OnDestroy()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void Cardboard_OnDestroy_m1500 (Cardboard_t233 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		Cardboard_set_VRModeEnabled_m1443(__this, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_1 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___device_15;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(31 /* System.Void BaseVRDevice::Destroy() */, L_1);
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_2 = ((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2;
		bool L_3 = Object_op_Equality_m2716(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		((Cardboard_t233_StaticFields*)Cardboard_t233_il2cpp_TypeInfo_var->static_fields)->___sdk_2 = (Cardboard_t233 *)NULL;
	}

IL_0031:
	{
		return;
	}
}
// System.Boolean Cardboard::get_nativeDistortionCorrection()
extern "C" bool Cardboard_get_nativeDistortionCorrection_m1501 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Cardboard_get_DistortionCorrection_m1444(__this, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Cardboard::set_nativeDistortionCorrection(System.Boolean)
extern "C" void Cardboard_set_nativeDistortionCorrection_m1502 (Cardboard_t233 * __this, bool ___value, const MethodInfo* method)
{
	Cardboard_t233 * G_B2_0 = {0};
	Cardboard_t233 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Cardboard_t233 * G_B3_1 = {0};
	{
		bool L_0 = ___value;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		NullCheck(G_B3_1);
		Cardboard_set_DistortionCorrection_m1445(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Cardboard::get_InCardboard()
extern "C" bool Cardboard_get_InCardboard_m1503 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean Cardboard::get_CardboardTriggered()
extern "C" bool Cardboard_get_CardboardTriggered_m1504 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Cardboard_get_Triggered_m1479(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Matrix4x4 Cardboard::get_HeadView()
extern "C" Matrix4x4_t242  Cardboard_get_HeadView_m1505 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		Pose3D_t260 * L_0 = Cardboard_get_HeadPose_m1471(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Matrix4x4_t242  L_1 = Pose3D_get_Matrix_m1590(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion Cardboard::get_HeadRotation()
extern "C" Quaternion_t261  Cardboard_get_HeadRotation_m1506 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		Pose3D_t260 * L_0 = Cardboard_get_HeadPose_m1471(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t261  L_1 = Pose3D_get_Orientation_m1588(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 Cardboard::get_HeadPosition()
extern "C" Vector3_t215  Cardboard_get_HeadPosition_m1507 (Cardboard_t233 * __this, const MethodInfo* method)
{
	{
		Pose3D_t260 * L_0 = Cardboard_get_HeadPose_m1471(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t215  L_1 = Pose3D_get_Position_m1586(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Matrix4x4 Cardboard::EyeView(Cardboard/Eye)
extern "C" Matrix4x4_t242  Cardboard_EyeView_m1508 (Cardboard_t233 * __this, int32_t ___eye, const MethodInfo* method)
{
	{
		int32_t L_0 = ___eye;
		Pose3D_t260 * L_1 = Cardboard_EyePose_m1472(__this, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Matrix4x4_t242  L_2 = Pose3D_get_Matrix_m1590(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector3 Cardboard::EyeOffset(Cardboard/Eye)
extern "C" Vector3_t215  Cardboard_EyeOffset_m1509 (Cardboard_t233 * __this, int32_t ___eye, const MethodInfo* method)
{
	{
		int32_t L_0 = ___eye;
		Pose3D_t260 * L_1 = Cardboard_EyePose_m1472(__this, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t215  L_2 = Pose3D_get_Position_m1586(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Matrix4x4 Cardboard::UndistortedProjection(Cardboard/Eye)
extern "C" Matrix4x4_t242  Cardboard_UndistortedProjection_m1510 (Cardboard_t233 * __this, int32_t ___eye, const MethodInfo* method)
{
	{
		int32_t L_0 = ___eye;
		Matrix4x4_t242  L_1 = Cardboard_Projection_m1473(__this, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Rect Cardboard::EyeRect(Cardboard/Eye)
extern "C" Rect_t225  Cardboard_EyeRect_m1511 (Cardboard_t233 * __this, int32_t ___eye, const MethodInfo* method)
{
	{
		int32_t L_0 = ___eye;
		Rect_t225  L_1 = Cardboard_Viewport_m1474(__this, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single Cardboard::get_MinimumComfortDistance()
extern "C" float Cardboard_get_MinimumComfortDistance_m1512 (Cardboard_t233 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		Vector2_t7  L_0 = Cardboard_get_ComfortableViewingRange_m1475(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		return L_1;
	}
}
// System.Single Cardboard::get_MaximumComfortDistance()
extern "C" float Cardboard_get_MaximumComfortDistance_m1513 (Cardboard_t233 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		Vector2_t7  L_0 = Cardboard_get_ComfortableViewingRange_m1475(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___y_2);
		return L_1;
	}
}
// CardboardEye
#include "AssemblyU2DCSharp_CardboardEye.h"
#ifndef _MSC_VER
#else
#endif
// CardboardEye
#include "AssemblyU2DCSharp_CardboardEyeMethodDeclarations.h"

// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// CardboardProfile/Screen
#include "AssemblyU2DCSharp_CardboardProfile_Screen.h"
// StereoRenderEffect
#include "AssemblyU2DCSharp_StereoRenderEffect.h"
// CardboardProfile/Device
#include "AssemblyU2DCSharp_CardboardProfile_Device.h"
// CardboardProfile/Lenses
#include "AssemblyU2DCSharp_CardboardProfile_Lenses.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// CardboardProfile
#include "AssemblyU2DCSharp_CardboardProfileMethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
struct Component_t477;
struct StereoController_t235;
struct Component_t477;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
// !!0 UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C" Object_t * Component_GetComponentInParent_TisObject_t_m2848_gshared (Component_t477 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisObject_t_m2848(__this, method) (( Object_t * (*) (Component_t477 *, const MethodInfo*))Component_GetComponentInParent_TisObject_t_m2848_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInParent<StereoController>()
// !!0 UnityEngine.Component::GetComponentInParent<StereoController>()
#define Component_GetComponentInParent_TisStereoController_t235_m2847(__this, method) (( StereoController_t235 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponentInParent_TisObject_t_m2848_gshared)(__this, method)
struct Component_t477;
struct CardboardHead_t244;
// Declaration !!0 UnityEngine.Component::GetComponentInParent<CardboardHead>()
// !!0 UnityEngine.Component::GetComponentInParent<CardboardHead>()
#define Component_GetComponentInParent_TisCardboardHead_t244_m2849(__this, method) (( CardboardHead_t244 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponentInParent_TisObject_t_m2848_gshared)(__this, method)
struct Component_t477;
struct Camera_t14;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t14_m2850(__this, method) (( Camera_t14 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)
struct Component_t477;
struct StereoRenderEffect_t239;
// Declaration !!0 UnityEngine.Component::GetComponent<StereoRenderEffect>()
// !!0 UnityEngine.Component::GetComponent<StereoRenderEffect>()
#define Component_GetComponent_TisStereoRenderEffect_t239_m2851(__this, method) (( StereoRenderEffect_t239 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)
struct GameObject_t256;
struct StereoRenderEffect_t239;
// Declaration !!0 UnityEngine.GameObject::AddComponent<StereoRenderEffect>()
// !!0 UnityEngine.GameObject::AddComponent<StereoRenderEffect>()
#define GameObject_AddComponent_TisStereoRenderEffect_t239_m2852(__this, method) (( StereoRenderEffect_t239 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void CardboardEye::.ctor()
extern "C" void CardboardEye__ctor_m1514 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	{
		LayerMask_t241  L_0 = LayerMask_op_Implicit_m2853(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		__this->___toggleCullingMask_3 = L_0;
		__this->___interpPosition_10 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// StereoController CardboardEye::get_Controller()
extern const MethodInfo* Component_GetComponentInParent_TisStereoController_t235_m2847_MethodInfo_var;
extern "C" StereoController_t235 * CardboardEye_get_Controller_m1515 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInParent_TisStereoController_t235_m2847_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t243 * L_0 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t243 * L_1 = Transform_get_parent_m2854(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		return (StereoController_t235 *)NULL;
	}

IL_0018:
	{
		bool L_3 = Application_get_isEditor_m2855(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		bool L_4 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}

IL_002c:
	{
		StereoController_t235 * L_5 = (__this->___controller_4);
		bool L_6 = Object_op_Equality_m2716(NULL /*static, unused*/, L_5, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004e;
		}
	}

IL_003d:
	{
		Transform_t243 * L_7 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t243 * L_8 = Transform_get_parent_m2854(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		StereoController_t235 * L_9 = Component_GetComponentInParent_TisStereoController_t235_m2847(L_8, /*hidden argument*/Component_GetComponentInParent_TisStereoController_t235_m2847_MethodInfo_var);
		return L_9;
	}

IL_004e:
	{
		StereoController_t235 * L_10 = (__this->___controller_4);
		return L_10;
	}
}
// CardboardHead CardboardEye::get_Head()
extern const MethodInfo* Component_GetComponentInParent_TisCardboardHead_t244_m2849_MethodInfo_var;
extern "C" CardboardHead_t244 * CardboardEye_get_Head_m1516 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInParent_TisCardboardHead_t244_m2849_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardboardHead_t244 * L_0 = Component_GetComponentInParent_TisCardboardHead_t244_m2849(__this, /*hidden argument*/Component_GetComponentInParent_TisCardboardHead_t244_m2849_MethodInfo_var);
		return L_0;
	}
}
// UnityEngine.Camera CardboardEye::get_camera()
extern "C" Camera_t14 * CardboardEye_get_camera_m1517 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = (__this->___U3CcameraU3Ek__BackingField_11);
		return L_0;
	}
}
// System.Void CardboardEye::set_camera(UnityEngine.Camera)
extern "C" void CardboardEye_set_camera_m1518 (CardboardEye_t240 * __this, Camera_t14 * ___value, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = ___value;
		__this->___U3CcameraU3Ek__BackingField_11 = L_0;
		return;
	}
}
// System.Void CardboardEye::Awake()
extern const MethodInfo* Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var;
extern "C" void CardboardEye_Awake_m1519 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t14 * L_0 = Component_GetComponent_TisCamera_t14_m2850(__this, /*hidden argument*/Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var);
		CardboardEye_set_camera_m1518(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardEye::Start()
extern const MethodInfo* Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral316;
extern "C" void CardboardEye_Start_m1520 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		_stringLiteral316 = il2cpp_codegen_string_literal_from_index(316);
		s_Il2CppMethodIntialized = true;
	}
	StereoController_t235 * V_0 = {0};
	{
		StereoController_t235 * L_0 = CardboardEye_get_Controller_m1515(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		StereoController_t235 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral316, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		StereoController_t235 * L_3 = V_0;
		__this->___controller_4 = L_3;
		StereoController_t235 * L_4 = (__this->___controller_4);
		NullCheck(L_4);
		Camera_t14 * L_5 = Component_GetComponent_TisCamera_t14_m2850(L_4, /*hidden argument*/Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var);
		__this->___monoCamera_6 = L_5;
		CardboardEye_UpdateStereoValues_m1523(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardEye::FixProjection(UnityEngine.Matrix4x4&)
extern "C" void CardboardEye_FixProjection_m1521 (CardboardEye_t240 * __this, Matrix4x4_t242 * ___proj, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Matrix4x4_t242 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	Rect_t225  V_6 = {0};
	Rect_t225  V_7 = {0};
	{
		Matrix4x4_t242 * L_0 = ___proj;
		Matrix4x4_t242 * L_1 = L_0;
		V_2 = (Matrix4x4_t242 *)L_1;
		int32_t L_2 = 0;
		V_3 = L_2;
		int32_t L_3 = 0;
		V_4 = L_3;
		Matrix4x4_t242 * L_4 = V_2;
		int32_t L_5 = V_3;
		int32_t L_6 = V_4;
		float L_7 = Matrix4x4_get_Item_m2856(L_4, L_5, L_6, /*hidden argument*/NULL);
		V_5 = L_7;
		float L_8 = V_5;
		Camera_t14 * L_9 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rect_t225  L_10 = Camera_get_rect_m2857(L_9, /*hidden argument*/NULL);
		V_6 = L_10;
		float L_11 = Rect_get_height_m2800((&V_6), /*hidden argument*/NULL);
		Camera_t14 * L_12 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Rect_t225  L_13 = Camera_get_rect_m2857(L_12, /*hidden argument*/NULL);
		V_7 = L_13;
		float L_14 = Rect_get_width_m2798((&V_7), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m2858(L_1, L_2, L_3, ((float)((float)L_8*(float)((float)((float)((float)((float)L_11/(float)L_14))/(float)(2.0f))))), /*hidden argument*/NULL);
		Camera_t14 * L_15 = (__this->___monoCamera_6);
		NullCheck(L_15);
		float L_16 = Camera_get_nearClipPlane_m2859(L_15, /*hidden argument*/NULL);
		V_0 = L_16;
		Camera_t14 * L_17 = (__this->___monoCamera_6);
		NullCheck(L_17);
		float L_18 = Camera_get_farClipPlane_m2860(L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		Matrix4x4_t242 * L_19 = ___proj;
		float L_20 = V_0;
		float L_21 = V_1;
		float L_22 = V_0;
		float L_23 = V_1;
		Matrix4x4_set_Item_m2858(L_19, 2, 2, ((float)((float)((float)((float)L_20+(float)L_21))/(float)((float)((float)L_22-(float)L_23)))), /*hidden argument*/NULL);
		Matrix4x4_t242 * L_24 = ___proj;
		float L_25 = V_0;
		float L_26 = V_1;
		float L_27 = V_0;
		float L_28 = V_1;
		Matrix4x4_set_Item_m2858(L_24, 2, 3, ((float)((float)((float)((float)((float)((float)(2.0f)*(float)L_25))*(float)L_26))/(float)((float)((float)L_27-(float)L_28)))), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect CardboardEye::FixViewport(UnityEngine.Rect)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" Rect_t225  CardboardEye_FixViewport_m1522 (CardboardEye_t240 * __this, Rect_t225  ___rect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t225  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___eye_2);
		NullCheck(L_0);
		Rect_t225  L_2 = Cardboard_Viewport_m1474(L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = (__this->___eye_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0031;
		}
	}
	{
		Rect_t225 * L_4 = (&___rect);
		float L_5 = Rect_get_x_m2861(L_4, /*hidden argument*/NULL);
		Rect_set_x_m2862(L_4, ((float)((float)L_5-(float)(0.5f))), /*hidden argument*/NULL);
	}

IL_0031:
	{
		Rect_t225 * L_6 = (&___rect);
		float L_7 = Rect_get_width_m2798(L_6, /*hidden argument*/NULL);
		float L_8 = Rect_get_width_m2798((&V_0), /*hidden argument*/NULL);
		Rect_set_width_m2863(L_6, ((float)((float)L_7*(float)((float)((float)(2.0f)*(float)L_8)))), /*hidden argument*/NULL);
		float L_9 = Rect_get_x_m2861((&V_0), /*hidden argument*/NULL);
		float L_10 = Rect_get_x_m2861((&___rect), /*hidden argument*/NULL);
		float L_11 = Rect_get_width_m2798((&V_0), /*hidden argument*/NULL);
		Rect_set_x_m2862((&___rect), ((float)((float)L_9+(float)((float)((float)((float)((float)(2.0f)*(float)L_10))*(float)L_11)))), /*hidden argument*/NULL);
		Rect_t225 * L_12 = (&___rect);
		float L_13 = Rect_get_height_m2800(L_12, /*hidden argument*/NULL);
		float L_14 = Rect_get_height_m2800((&V_0), /*hidden argument*/NULL);
		Rect_set_height_m2864(L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		float L_15 = Rect_get_y_m2865((&V_0), /*hidden argument*/NULL);
		float L_16 = Rect_get_y_m2865((&___rect), /*hidden argument*/NULL);
		float L_17 = Rect_get_height_m2800((&V_0), /*hidden argument*/NULL);
		Rect_set_y_m2866((&___rect), ((float)((float)L_15+(float)((float)((float)L_16*(float)L_17)))), /*hidden argument*/NULL);
		bool L_18 = Application_get_isEditor_m2855(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_19 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((float)((float)(((float)L_19))/(float)(((float)L_20))));
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_21 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_21);
		CardboardProfile_t255 * L_22 = Cardboard_get_Profile_m1470(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Screen_t248 * L_23 = &(L_22->___screen_0);
		float L_24 = (L_23->___width_0);
		Cardboard_t233 * L_25 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_25);
		CardboardProfile_t255 * L_26 = Cardboard_get_Profile_m1470(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Screen_t248 * L_27 = &(L_26->___screen_0);
		float L_28 = (L_27->___height_1);
		V_2 = ((float)((float)L_24/(float)L_28));
		float L_29 = V_2;
		float L_30 = V_1;
		V_3 = ((float)((float)L_29/(float)L_30));
		float L_31 = V_3;
		if ((!(((float)L_31) < ((float)(1.0f)))))
		{
			goto IL_0132;
		}
	}
	{
		Rect_t225 * L_32 = (&___rect);
		float L_33 = Rect_get_width_m2798(L_32, /*hidden argument*/NULL);
		float L_34 = V_3;
		Rect_set_width_m2863(L_32, ((float)((float)L_33*(float)L_34)), /*hidden argument*/NULL);
		Rect_t225 * L_35 = (&___rect);
		float L_36 = Rect_get_x_m2861(L_35, /*hidden argument*/NULL);
		float L_37 = V_3;
		Rect_set_x_m2862(L_35, ((float)((float)L_36*(float)L_37)), /*hidden argument*/NULL);
		Rect_t225 * L_38 = (&___rect);
		float L_39 = Rect_get_x_m2861(L_38, /*hidden argument*/NULL);
		float L_40 = V_3;
		Rect_set_x_m2862(L_38, ((float)((float)L_39+(float)((float)((float)((float)((float)(1.0f)-(float)L_40))/(float)(2.0f))))), /*hidden argument*/NULL);
		goto IL_0150;
	}

IL_0132:
	{
		Rect_t225 * L_41 = (&___rect);
		float L_42 = Rect_get_height_m2800(L_41, /*hidden argument*/NULL);
		float L_43 = V_3;
		Rect_set_height_m2864(L_41, ((float)((float)L_42/(float)L_43)), /*hidden argument*/NULL);
		Rect_t225 * L_44 = (&___rect);
		float L_45 = Rect_get_y_m2865(L_44, /*hidden argument*/NULL);
		float L_46 = V_3;
		Rect_set_y_m2866(L_44, ((float)((float)L_45/(float)L_46)), /*hidden argument*/NULL);
	}

IL_0150:
	{
		Rect_t225  L_47 = ___rect;
		return L_47;
	}
}
// System.Void CardboardEye::UpdateStereoValues()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void CardboardEye_UpdateStereoValues_m1523 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Matrix4x4_t242  V_4 = {0};
	Matrix4x4_t242 * V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	Matrix4x4_t242 * V_9 = {0};
	RenderTexture_t15 * G_B4_0 = {0};
	Camera_t14 * G_B4_1 = {0};
	RenderTexture_t15 * G_B3_0 = {0};
	Camera_t14 * G_B3_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___eye_2);
		NullCheck(L_0);
		Matrix4x4_t242  L_2 = Cardboard_Projection_m1473(L_0, L_1, 0, /*hidden argument*/NULL);
		V_0 = L_2;
		Cardboard_t233 * L_3 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = (__this->___eye_2);
		NullCheck(L_3);
		Matrix4x4_t242  L_5 = Cardboard_Projection_m1473(L_3, L_4, 1, /*hidden argument*/NULL);
		__this->___realProj_7 = L_5;
		StereoController_t235 * L_6 = (__this->___controller_4);
		float L_7 = Matrix4x4_get_Item_m2856((&V_0), 0, 2, /*hidden argument*/NULL);
		float L_8 = Matrix4x4_get_Item_m2856((&V_0), 1, 2, /*hidden argument*/NULL);
		CardboardEye_CopyCameraAndMakeSideBySide_m1526(__this, L_6, L_7, L_8, /*hidden argument*/NULL);
		CardboardEye_FixProjection_m1521(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t242 * L_9 = &(__this->___realProj_7);
		CardboardEye_FixProjection_m1521(__this, L_9, /*hidden argument*/NULL);
		float L_10 = Matrix4x4_get_Item_m2856((&V_0), 0, 0, /*hidden argument*/NULL);
		float L_11 = Matrix4x4_get_Item_m2856((&V_0), 1, 1, /*hidden argument*/NULL);
		float L_12 = Matrix4x4_get_Item_m2856((&V_0), 0, 2, /*hidden argument*/NULL);
		float L_13 = Matrix4x4_get_Item_m2856((&V_0), 1, 2, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, L_10, L_11, ((float)((float)L_12-(float)(1.0f))), ((float)((float)L_13-(float)(1.0f))), /*hidden argument*/NULL);
		Vector4_t5  L_15 = Vector4_op_Division_m2867(NULL /*static, unused*/, L_14, (2.0f), /*hidden argument*/NULL);
		__this->___projvec_8 = L_15;
		Matrix4x4_t242 * L_16 = &(__this->___realProj_7);
		float L_17 = Matrix4x4_get_Item_m2856(L_16, 0, 0, /*hidden argument*/NULL);
		Matrix4x4_t242 * L_18 = &(__this->___realProj_7);
		float L_19 = Matrix4x4_get_Item_m2856(L_18, 1, 1, /*hidden argument*/NULL);
		Matrix4x4_t242 * L_20 = &(__this->___realProj_7);
		float L_21 = Matrix4x4_get_Item_m2856(L_20, 0, 2, /*hidden argument*/NULL);
		Matrix4x4_t242 * L_22 = &(__this->___realProj_7);
		float L_23 = Matrix4x4_get_Item_m2856(L_22, 1, 2, /*hidden argument*/NULL);
		Vector4_t5  L_24 = {0};
		Vector4__ctor_m2728(&L_24, L_17, L_19, ((float)((float)L_21-(float)(1.0f))), ((float)((float)L_23-(float)(1.0f))), /*hidden argument*/NULL);
		Vector4_t5  L_25 = Vector4_op_Division_m2867(NULL /*static, unused*/, L_24, (2.0f), /*hidden argument*/NULL);
		__this->___unprojvec_9 = L_25;
		StereoController_t235 * L_26 = (__this->___controller_4);
		NullCheck(L_26);
		float L_27 = (L_26->___matchByZoom_6);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_28 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		StereoController_t235 * L_29 = (__this->___controller_4);
		NullCheck(L_29);
		float L_30 = (L_29->___matchMonoFOV_5);
		float L_31 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_28*(float)L_31));
		Camera_t14 * L_32 = (__this->___monoCamera_6);
		NullCheck(L_32);
		Matrix4x4_t242  L_33 = Camera_get_projectionMatrix_m2868(L_32, /*hidden argument*/NULL);
		V_4 = L_33;
		float L_34 = Matrix4x4_get_Item_m2856((&V_4), 1, 1, /*hidden argument*/NULL);
		V_2 = L_34;
		float L_35 = Matrix4x4_get_Item_m2856((&V_0), 1, 1, /*hidden argument*/NULL);
		float L_36 = V_2;
		float L_37 = V_1;
		float L_38 = Mathf_Lerp_m2756(NULL /*static, unused*/, ((float)((float)(1.0f)/(float)L_35)), ((float)((float)(1.0f)/(float)L_36)), L_37, /*hidden argument*/NULL);
		float L_39 = Matrix4x4_get_Item_m2856((&V_0), 1, 1, /*hidden argument*/NULL);
		V_3 = ((float)((float)((float)((float)(1.0f)/(float)L_38))/(float)L_39));
		Matrix4x4_t242 * L_40 = (&V_0);
		V_5 = (Matrix4x4_t242 *)L_40;
		int32_t L_41 = 0;
		V_6 = L_41;
		int32_t L_42 = 0;
		V_7 = L_42;
		Matrix4x4_t242 * L_43 = V_5;
		int32_t L_44 = V_6;
		int32_t L_45 = V_7;
		float L_46 = Matrix4x4_get_Item_m2856(L_43, L_44, L_45, /*hidden argument*/NULL);
		V_8 = L_46;
		float L_47 = V_8;
		float L_48 = V_3;
		Matrix4x4_set_Item_m2858(L_40, L_41, L_42, ((float)((float)L_47*(float)L_48)), /*hidden argument*/NULL);
		Matrix4x4_t242 * L_49 = (&V_0);
		V_9 = (Matrix4x4_t242 *)L_49;
		int32_t L_50 = 1;
		V_7 = L_50;
		int32_t L_51 = 1;
		V_6 = L_51;
		Matrix4x4_t242 * L_52 = V_9;
		int32_t L_53 = V_7;
		int32_t L_54 = V_6;
		float L_55 = Matrix4x4_get_Item_m2856(L_52, L_53, L_54, /*hidden argument*/NULL);
		V_8 = L_55;
		float L_56 = V_8;
		float L_57 = V_3;
		Matrix4x4_set_Item_m2858(L_49, L_50, L_51, ((float)((float)L_56*(float)L_57)), /*hidden argument*/NULL);
		Camera_t14 * L_58 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		Matrix4x4_t242  L_59 = V_0;
		NullCheck(L_58);
		Camera_set_projectionMatrix_m2869(L_58, L_59, /*hidden argument*/NULL);
		bool L_60 = Application_get_isEditor_m2855(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_01e2;
		}
	}
	{
		Camera_t14 * L_61 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		float L_62 = Matrix4x4_get_Item_m2856((&V_0), 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_63 = atanf(((float)((float)(1.0f)/(float)L_62)));
		NullCheck(L_61);
		Camera_set_fieldOfView_m2870(L_61, ((float)((float)((float)((float)(2.0f)*(float)L_63))*(float)(57.29578f))), /*hidden argument*/NULL);
	}

IL_01e2:
	{
		Camera_t14 * L_64 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		Camera_t14 * L_65 = (__this->___monoCamera_6);
		NullCheck(L_65);
		RenderTexture_t15 * L_66 = Camera_get_targetTexture_m2740(L_65, /*hidden argument*/NULL);
		RenderTexture_t15 * L_67 = L_66;
		G_B3_0 = L_67;
		G_B3_1 = L_64;
		if (L_67)
		{
			G_B4_0 = L_67;
			G_B4_1 = L_64;
			goto IL_0204;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_68 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_68);
		RenderTexture_t15 * L_69 = Cardboard_get_StereoScreen_m1468(L_68, /*hidden argument*/NULL);
		G_B4_0 = L_69;
		G_B4_1 = G_B3_1;
	}

IL_0204:
	{
		NullCheck(G_B4_1);
		Camera_set_targetTexture_m2742(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		Camera_t14 * L_70 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_70);
		RenderTexture_t15 * L_71 = Camera_get_targetTexture_m2740(L_70, /*hidden argument*/NULL);
		bool L_72 = Object_op_Equality_m2716(NULL /*static, unused*/, L_71, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_023b;
		}
	}
	{
		Camera_t14 * L_73 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		Camera_t14 * L_74 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_74);
		Rect_t225  L_75 = Camera_get_rect_m2857(L_74, /*hidden argument*/NULL);
		Rect_t225  L_76 = CardboardEye_FixViewport_m1522(__this, L_75, /*hidden argument*/NULL);
		NullCheck(L_73);
		Camera_set_rect_m2871(L_73, L_76, /*hidden argument*/NULL);
	}

IL_023b:
	{
		return;
	}
}
// System.Void CardboardEye::SetupStereo()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral317;
extern Il2CppCodeGenString* _stringLiteral318;
extern Il2CppCodeGenString* _stringLiteral319;
extern Il2CppCodeGenString* _stringLiteral320;
extern Il2CppCodeGenString* _stringLiteral321;
extern "C" void CardboardEye_SetupStereo_m1524 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral317 = il2cpp_codegen_string_literal_from_index(317);
		_stringLiteral318 = il2cpp_codegen_string_literal_from_index(318);
		_stringLiteral319 = il2cpp_codegen_string_literal_from_index(319);
		_stringLiteral320 = il2cpp_codegen_string_literal_from_index(320);
		_stringLiteral321 = il2cpp_codegen_string_literal_from_index(321);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector3_t215  V_4 = {0};
	Matrix4x4_t242  V_5 = {0};
	Matrix4x4_t242  V_6 = {0};
	Vector3_t215  V_7 = {0};
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	CardboardEye_t240 * G_B13_0 = {0};
	CardboardEye_t240 * G_B12_0 = {0};
	CardboardEye_t240 * G_B14_0 = {0};
	float G_B15_0 = 0.0f;
	CardboardEye_t240 * G_B15_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Cardboard_UpdateState_m1487(L_0, /*hidden argument*/NULL);
		StereoController_t235 * L_1 = (__this->___controller_4);
		NullCheck(L_1);
		Transform_t243 * L_2 = (L_1->___centerOfInterest_7);
		bool L_3 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_2, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		StereoController_t235 * L_4 = (__this->___controller_4);
		NullCheck(L_4);
		Transform_t243 * L_5 = (L_4->___centerOfInterest_7);
		NullCheck(L_5);
		GameObject_t256 * L_6 = Component_get_gameObject_m2778(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = GameObject_get_activeInHierarchy_m2779(L_6, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_7));
		goto IL_0038;
	}

IL_0037:
	{
		G_B3_0 = 0;
	}

IL_0038:
	{
		V_0 = G_B3_0;
		bool L_8 = V_0;
		if (L_8)
		{
			goto IL_004e;
		}
	}
	{
		float L_9 = (__this->___interpPosition_10);
		G_B6_0 = ((((float)L_9) < ((float)(1.0f)))? 1 : 0);
		goto IL_004f;
	}

IL_004e:
	{
		G_B6_0 = 1;
	}

IL_004f:
	{
		V_1 = G_B6_0;
		StereoController_t235 * L_10 = (__this->___controller_4);
		NullCheck(L_10);
		bool L_11 = (L_10->___keepStereoUpdated_3);
		if (L_11)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_12 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = Cardboard_get_ProfileChanged_m1483(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0079;
		}
	}
	{
		bool L_14 = Application_get_isEditor_m2855(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0081;
		}
	}

IL_0079:
	{
		CardboardEye_UpdateStereoValues_m1523(__this, /*hidden argument*/NULL);
		V_1 = 1;
	}

IL_0081:
	{
		bool L_15 = V_1;
		if (!L_15)
		{
			goto IL_0128;
		}
	}
	{
		Camera_t14 * L_16 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Matrix4x4_t242  L_17 = Camera_get_projectionMatrix_m2868(L_16, /*hidden argument*/NULL);
		V_6 = L_17;
		float L_18 = Matrix4x4_get_Item_m2856((&V_6), 1, 1, /*hidden argument*/NULL);
		V_2 = L_18;
		Transform_t243 * L_19 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t215  L_20 = Transform_get_lossyScale_m2872(L_19, /*hidden argument*/NULL);
		V_7 = L_20;
		float L_21 = ((&V_7)->___z_3);
		V_3 = L_21;
		StereoController_t235 * L_22 = (__this->___controller_4);
		int32_t L_23 = (__this->___eye_2);
		float L_24 = V_2;
		float L_25 = V_3;
		NullCheck(L_22);
		Vector3_t215  L_26 = StereoController_ComputeStereoEyePosition_m1618(L_22, L_23, L_24, L_25, /*hidden argument*/NULL);
		V_4 = L_26;
		StereoController_t235 * L_27 = (__this->___controller_4);
		NullCheck(L_27);
		bool L_28 = (L_27->___keepStereoUpdated_3);
		G_B12_0 = __this;
		if (L_28)
		{
			G_B13_0 = __this;
			goto IL_00df;
		}
	}
	{
		bool L_29 = V_0;
		G_B13_0 = G_B12_0;
		if (!L_29)
		{
			G_B14_0 = G_B12_0;
			goto IL_00fb;
		}
	}

IL_00df:
	{
		float L_30 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		StereoController_t235 * L_31 = (__this->___controller_4);
		NullCheck(L_31);
		float L_32 = (L_31->___stereoAdjustSmoothing_10);
		float L_33 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B15_0 = ((float)((float)L_30/(float)((float)((float)L_32+(float)L_33))));
		G_B15_1 = G_B13_0;
		goto IL_0100;
	}

IL_00fb:
	{
		G_B15_0 = (1.0f);
		G_B15_1 = G_B14_0;
	}

IL_0100:
	{
		NullCheck(G_B15_1);
		G_B15_1->___interpPosition_10 = G_B15_0;
		Transform_t243 * L_34 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Transform_t243 * L_35 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t215  L_36 = Transform_get_localPosition_m2761(L_35, /*hidden argument*/NULL);
		Vector3_t215  L_37 = V_4;
		float L_38 = (__this->___interpPosition_10);
		Vector3_t215  L_39 = Vector3_Lerp_m2873(NULL /*static, unused*/, L_36, L_37, L_38, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_localPosition_m2766(L_34, L_39, /*hidden argument*/NULL);
	}

IL_0128:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_40 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_40);
		int32_t L_41 = Cardboard_get_DistortionCorrection_m1444(L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_0185;
		}
	}
	{
		Camera_t14 * L_42 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		Matrix4x4_t242  L_43 = Camera_get_cameraToWorldMatrix_m2874(L_42, /*hidden argument*/NULL);
		Camera_t14 * L_44 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Matrix4x4_t242  L_45 = Camera_get_projectionMatrix_m2868(L_44, /*hidden argument*/NULL);
		Matrix4x4_t242  L_46 = Matrix4x4_Inverse_m2875(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Matrix4x4_t242  L_47 = Matrix4x4_op_Multiply_m2876(NULL /*static, unused*/, L_43, L_46, /*hidden argument*/NULL);
		Matrix4x4_t242  L_48 = (__this->___realProj_7);
		Matrix4x4_t242  L_49 = Matrix4x4_op_Multiply_m2876(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		V_5 = L_49;
		Matrix4x4_t242  L_50 = (__this->___realProj_7);
		Shader_SetGlobalMatrix_m2877(NULL /*static, unused*/, _stringLiteral317, L_50, /*hidden argument*/NULL);
		Matrix4x4_t242  L_51 = V_5;
		Shader_SetGlobalMatrix_m2877(NULL /*static, unused*/, _stringLiteral318, L_51, /*hidden argument*/NULL);
		goto IL_01af;
	}

IL_0185:
	{
		Camera_t14 * L_52 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		Matrix4x4_t242  L_53 = Camera_get_projectionMatrix_m2868(L_52, /*hidden argument*/NULL);
		Shader_SetGlobalMatrix_m2877(NULL /*static, unused*/, _stringLiteral317, L_53, /*hidden argument*/NULL);
		Camera_t14 * L_54 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		Matrix4x4_t242  L_55 = Camera_get_cameraToWorldMatrix_m2874(L_54, /*hidden argument*/NULL);
		Shader_SetGlobalMatrix_m2877(NULL /*static, unused*/, _stringLiteral318, L_55, /*hidden argument*/NULL);
	}

IL_01af:
	{
		Vector4_t5  L_56 = (__this->___projvec_8);
		Shader_SetGlobalVector_m2878(NULL /*static, unused*/, _stringLiteral319, L_56, /*hidden argument*/NULL);
		Vector4_t5  L_57 = (__this->___unprojvec_9);
		Shader_SetGlobalVector_m2878(NULL /*static, unused*/, _stringLiteral320, L_57, /*hidden argument*/NULL);
		Camera_t14 * L_58 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_58);
		float L_59 = Camera_get_nearClipPlane_m2859(L_58, /*hidden argument*/NULL);
		Shader_SetGlobalFloat_m2879(NULL /*static, unused*/, _stringLiteral321, L_59, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardEye::OnPreCull()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisStereoRenderEffect_t239_m2851_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisStereoRenderEffect_t239_m2852_MethodInfo_var;
extern "C" void CardboardEye_OnPreCull_m1525 (CardboardEye_t240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Component_GetComponent_TisStereoRenderEffect_t239_m2851_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483666);
		GameObject_AddComponent_TisStereoRenderEffect_t239_m2852_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483667);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Cardboard_get_VRModeEnabled_m1442(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Camera_t14 * L_2 = (__this->___monoCamera_6);
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m2777(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002c;
		}
	}

IL_001f:
	{
		Camera_t14 * L_4 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Behaviour_set_enabled_m2721(L_4, 0, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		CardboardEye_SetupStereo_m1524(__this, /*hidden argument*/NULL);
		StereoController_t235 * L_5 = (__this->___controller_4);
		NullCheck(L_5);
		bool L_6 = (L_5->___directRender_2);
		if (L_6)
		{
			goto IL_0096;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_7 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		RenderTexture_t15 * L_8 = Cardboard_get_StereoScreen_m1468(L_7, /*hidden argument*/NULL);
		bool L_9 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_8, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0096;
		}
	}
	{
		StereoRenderEffect_t239 * L_10 = Component_GetComponent_TisStereoRenderEffect_t239_m2851(__this, /*hidden argument*/Component_GetComponent_TisStereoRenderEffect_t239_m2851_MethodInfo_var);
		__this->___stereoEffect_5 = L_10;
		StereoRenderEffect_t239 * L_11 = (__this->___stereoEffect_5);
		bool L_12 = Object_op_Equality_m2716(NULL /*static, unused*/, L_11, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t256 * L_13 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		StereoRenderEffect_t239 * L_14 = GameObject_AddComponent_TisStereoRenderEffect_t239_m2852(L_13, /*hidden argument*/GameObject_AddComponent_TisStereoRenderEffect_t239_m2852_MethodInfo_var);
		__this->___stereoEffect_5 = L_14;
	}

IL_0085:
	{
		StereoRenderEffect_t239 * L_15 = (__this->___stereoEffect_5);
		NullCheck(L_15);
		Behaviour_set_enabled_m2721(L_15, 1, /*hidden argument*/NULL);
		goto IL_00b3;
	}

IL_0096:
	{
		StereoRenderEffect_t239 * L_16 = (__this->___stereoEffect_5);
		bool L_17 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_16, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00b3;
		}
	}
	{
		StereoRenderEffect_t239 * L_18 = (__this->___stereoEffect_5);
		NullCheck(L_18);
		Behaviour_set_enabled_m2721(L_18, 0, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		return;
	}
}
// System.Void CardboardEye::CopyCameraAndMakeSideBySide(StereoController,System.Single,System.Single)
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void CardboardEye_CopyCameraAndMakeSideBySide_m1526 (CardboardEye_t240 * __this, StereoController_t235 * ___controller, float ___parx, float ___pary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t215  V_1 = {0};
	Rect_t225  V_2 = {0};
	Vector2_t7  V_3 = {0};
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Rect_t225  V_6 = {0};
	Vector3_t215  G_B6_0 = {0};
	float G_B5_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		CardboardProfile_t255 * L_0 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Default_14;
		NullCheck(L_0);
		Device_t252 * L_1 = &(L_0->___device_1);
		Lenses_t249 * L_2 = &(L_1->___lenses_0);
		float L_3 = (L_2->___separation_3);
		StereoController_t235 * L_4 = ___controller;
		NullCheck(L_4);
		float L_5 = (L_4->___stereoMultiplier_4);
		V_0 = ((float)((float)L_3*(float)L_5));
		bool L_6 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0036;
		}
	}
	{
		Transform_t243 * L_7 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t215  L_8 = Transform_get_localPosition_m2761(L_7, /*hidden argument*/NULL);
		G_B6_0 = L_8;
		goto IL_005f;
	}

IL_0036:
	{
		int32_t L_9 = (__this->___eye_2);
		if (L_9)
		{
			goto IL_004e;
		}
	}
	{
		float L_10 = V_0;
		G_B5_0 = ((float)((float)((-L_10))/(float)(2.0f)));
		goto IL_0055;
	}

IL_004e:
	{
		float L_11 = V_0;
		G_B5_0 = ((float)((float)L_11/(float)(2.0f)));
	}

IL_0055:
	{
		Vector3_t215  L_12 = Vector3_get_right_m2880(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_13 = Vector3_op_Multiply_m2881(NULL /*static, unused*/, G_B5_0, L_12, /*hidden argument*/NULL);
		G_B6_0 = L_13;
	}

IL_005f:
	{
		V_1 = G_B6_0;
		Camera_t14 * L_14 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		Camera_t14 * L_15 = (__this->___monoCamera_6);
		NullCheck(L_14);
		Camera_CopyFrom_m2882(L_14, L_15, /*hidden argument*/NULL);
		Camera_t14 * L_16 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		Camera_t14 * L_17 = L_16;
		NullCheck(L_17);
		int32_t L_18 = Camera_get_cullingMask_m2883(L_17, /*hidden argument*/NULL);
		LayerMask_t241 * L_19 = &(__this->___toggleCullingMask_3);
		int32_t L_20 = LayerMask_get_value_m2884(L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		Camera_set_cullingMask_m2885(L_17, ((int32_t)((int32_t)L_18^(int32_t)L_20)), /*hidden argument*/NULL);
		Camera_t14 * L_21 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		Camera_t14 * L_22 = (__this->___monoCamera_6);
		NullCheck(L_22);
		float L_23 = Camera_get_depth_m2886(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		Camera_set_depth_m2887(L_21, L_23, /*hidden argument*/NULL);
		Transform_t243 * L_24 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Vector3_t215  L_25 = V_1;
		NullCheck(L_24);
		Transform_set_localPosition_m2766(L_24, L_25, /*hidden argument*/NULL);
		Transform_t243 * L_26 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Quaternion_t261  L_27 = Quaternion_get_identity_m2888(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_localRotation_m2889(L_26, L_27, /*hidden argument*/NULL);
		Transform_t243 * L_28 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Vector3_t215  L_29 = Vector3_get_one_m2890(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_localScale_m2813(L_28, L_29, /*hidden argument*/NULL);
		Camera_t14 * L_30 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		Rect_t225  L_31 = Camera_get_rect_m2857(L_30, /*hidden argument*/NULL);
		V_2 = L_31;
		Vector2_t7  L_32 = Rect_get_center_m2891((&V_2), /*hidden argument*/NULL);
		V_3 = L_32;
		float L_33 = ((&V_3)->___x_1);
		StereoController_t235 * L_34 = ___controller;
		NullCheck(L_34);
		float L_35 = (L_34->___stereoPaddingX_12);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_36 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		float L_37 = Mathf_Lerp_m2756(NULL /*static, unused*/, L_33, (0.5f), L_36, /*hidden argument*/NULL);
		(&V_3)->___x_1 = L_37;
		float L_38 = ((&V_3)->___y_2);
		StereoController_t235 * L_39 = ___controller;
		NullCheck(L_39);
		float L_40 = (L_39->___stereoPaddingY_13);
		float L_41 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		float L_42 = Mathf_Lerp_m2756(NULL /*static, unused*/, L_38, (0.5f), L_41, /*hidden argument*/NULL);
		(&V_3)->___y_2 = L_42;
		Vector2_t7  L_43 = V_3;
		Rect_set_center_m2892((&V_2), L_43, /*hidden argument*/NULL);
		float L_44 = Rect_get_width_m2798((&V_2), /*hidden argument*/NULL);
		float L_45 = Mathf_SmoothStep_m2893(NULL /*static, unused*/, (-0.5f), (0.5f), ((float)((float)((float)((float)L_44+(float)(1.0f)))/(float)(2.0f))), /*hidden argument*/NULL);
		V_4 = L_45;
		Rect_t225 * L_46 = (&V_2);
		float L_47 = Rect_get_x_m2861(L_46, /*hidden argument*/NULL);
		float L_48 = Rect_get_width_m2798((&V_2), /*hidden argument*/NULL);
		float L_49 = V_4;
		Rect_set_x_m2862(L_46, ((float)((float)L_47+(float)((float)((float)((float)((float)L_48-(float)L_49))/(float)(2.0f))))), /*hidden argument*/NULL);
		float L_50 = V_4;
		Rect_set_width_m2863((&V_2), L_50, /*hidden argument*/NULL);
		Rect_t225 * L_51 = (&V_2);
		float L_52 = Rect_get_x_m2861(L_51, /*hidden argument*/NULL);
		float L_53 = Rect_get_width_m2798((&V_2), /*hidden argument*/NULL);
		float L_54 = Rect_get_width_m2798((&V_2), /*hidden argument*/NULL);
		Rect_set_x_m2862(L_51, ((float)((float)L_52*(float)((float)((float)((float)((float)(0.5f)-(float)L_53))/(float)((float)((float)(1.0f)-(float)L_54)))))), /*hidden argument*/NULL);
		int32_t L_55 = (__this->___eye_2);
		if ((!(((uint32_t)L_55) == ((uint32_t)1))))
		{
			goto IL_01c5;
		}
	}
	{
		Rect_t225 * L_56 = (&V_2);
		float L_57 = Rect_get_x_m2861(L_56, /*hidden argument*/NULL);
		Rect_set_x_m2862(L_56, ((float)((float)L_57+(float)(0.5f))), /*hidden argument*/NULL);
	}

IL_01c5:
	{
		StereoController_t235 * L_58 = ___controller;
		NullCheck(L_58);
		float L_59 = (L_58->___screenParallax_11);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_60 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		V_5 = L_60;
		Camera_t14 * L_61 = (__this->___monoCamera_6);
		NullCheck(L_61);
		Rect_t225  L_62 = Camera_get_rect_m2857(L_61, /*hidden argument*/NULL);
		V_6 = L_62;
		float L_63 = Rect_get_width_m2798((&V_6), /*hidden argument*/NULL);
		if ((!(((float)L_63) < ((float)(1.0f)))))
		{
			goto IL_022c;
		}
	}
	{
		float L_64 = V_5;
		if ((!(((float)L_64) > ((float)(0.0f)))))
		{
			goto IL_022c;
		}
	}
	{
		Rect_t225 * L_65 = (&V_2);
		float L_66 = Rect_get_x_m2861(L_65, /*hidden argument*/NULL);
		float L_67 = ___parx;
		float L_68 = V_5;
		Rect_set_x_m2862(L_65, ((float)((float)L_66-(float)((float)((float)((float)((float)L_67/(float)(4.0f)))*(float)L_68)))), /*hidden argument*/NULL);
		Rect_t225 * L_69 = (&V_2);
		float L_70 = Rect_get_y_m2865(L_69, /*hidden argument*/NULL);
		float L_71 = ___pary;
		float L_72 = V_5;
		Rect_set_y_m2866(L_69, ((float)((float)L_70-(float)((float)((float)((float)((float)L_71/(float)(2.0f)))*(float)L_72)))), /*hidden argument*/NULL);
	}

IL_022c:
	{
		Camera_t14 * L_73 = CardboardEye_get_camera_m1517(__this, /*hidden argument*/NULL);
		Rect_t225  L_74 = V_2;
		NullCheck(L_73);
		Camera_set_rect_m2871(L_73, L_74, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"


// System.Void CardboardHead::.ctor()
extern "C" void CardboardHead__ctor_m1527 (CardboardHead_t244 * __this, const MethodInfo* method)
{
	{
		__this->___trackRotation_2 = 1;
		__this->___trackPosition_3 = 1;
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Ray CardboardHead::get_Gaze()
extern "C" Ray_t465  CardboardHead_get_Gaze_m1528 (CardboardHead_t244 * __this, const MethodInfo* method)
{
	{
		CardboardHead_UpdateHead_m1531(__this, /*hidden argument*/NULL);
		Transform_t243 * L_0 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t215  L_1 = Transform_get_position_m2894(L_0, /*hidden argument*/NULL);
		Transform_t243 * L_2 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t215  L_3 = Transform_get_forward_m2895(L_2, /*hidden argument*/NULL);
		Ray_t465  L_4 = {0};
		Ray__ctor_m2896(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void CardboardHead::Update()
extern "C" void CardboardHead_Update_m1529 (CardboardHead_t244 * __this, const MethodInfo* method)
{
	{
		__this->___updated_6 = 0;
		bool L_0 = (__this->___updateEarly_5);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		CardboardHead_UpdateHead_m1531(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void CardboardHead::LateUpdate()
extern "C" void CardboardHead_LateUpdate_m1530 (CardboardHead_t244 * __this, const MethodInfo* method)
{
	{
		CardboardHead_UpdateHead_m1531(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardHead::UpdateHead()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void CardboardHead_UpdateHead_m1531 (CardboardHead_t244 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t261  V_0 = {0};
	Vector3_t215  V_1 = {0};
	{
		bool L_0 = (__this->___updated_6);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->___updated_6 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_1 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Cardboard_UpdateState_m1487(L_1, /*hidden argument*/NULL);
		bool L_2 = (__this->___trackRotation_2);
		if (!L_2)
		{
			goto IL_0076;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_3 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Pose3D_t260 * L_4 = Cardboard_get_HeadPose_m1471(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t261  L_5 = Pose3D_get_Orientation_m1588(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t243 * L_6 = (__this->___target_4);
		bool L_7 = Object_op_Equality_m2716(NULL /*static, unused*/, L_6, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005a;
		}
	}
	{
		Transform_t243 * L_8 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Quaternion_t261  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_localRotation_m2889(L_8, L_9, /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_005a:
	{
		Transform_t243 * L_10 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Transform_t243 * L_11 = (__this->___target_4);
		NullCheck(L_11);
		Quaternion_t261  L_12 = Transform_get_rotation_m2897(L_11, /*hidden argument*/NULL);
		Quaternion_t261  L_13 = V_0;
		Quaternion_t261  L_14 = Quaternion_op_Multiply_m2898(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_rotation_m2899(L_10, L_14, /*hidden argument*/NULL);
	}

IL_0076:
	{
		bool L_15 = (__this->___trackPosition_3);
		if (!L_15)
		{
			goto IL_00df;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_16 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		Pose3D_t260 * L_17 = Cardboard_get_HeadPose_m1471(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t215  L_18 = Pose3D_get_Position_m1586(L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		Transform_t243 * L_19 = (__this->___target_4);
		bool L_20 = Object_op_Equality_m2716(NULL /*static, unused*/, L_19, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00b3;
		}
	}
	{
		Transform_t243 * L_21 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Vector3_t215  L_22 = V_1;
		NullCheck(L_21);
		Transform_set_localPosition_m2766(L_21, L_22, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00b3:
	{
		Transform_t243 * L_23 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Transform_t243 * L_24 = (__this->___target_4);
		NullCheck(L_24);
		Vector3_t215  L_25 = Transform_get_position_m2894(L_24, /*hidden argument*/NULL);
		Transform_t243 * L_26 = (__this->___target_4);
		NullCheck(L_26);
		Quaternion_t261  L_27 = Transform_get_rotation_m2897(L_26, /*hidden argument*/NULL);
		Vector3_t215  L_28 = V_1;
		Vector3_t215  L_29 = Quaternion_op_Multiply_m2900(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		Vector3_t215  L_30 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_25, L_29, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m2902(L_23, L_30, /*hidden argument*/NULL);
	}

IL_00df:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// CardboardPostRender
#include "AssemblyU2DCSharp_CardboardPostRenderMethodDeclarations.h"

// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E.h"
// <PrivateImplementationDetails>/$ArrayType$20
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra_0.h"
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
#include "UnityEngine_ArrayTypes.h"
// CardboardProfile/Distortion
#include "AssemblyU2DCSharp_CardboardProfile_Distortion.h"
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// CardboardProfile/Distortion
#include "AssemblyU2DCSharp_CardboardProfile_DistortionMethodDeclarations.h"


// System.Void CardboardPostRender::.ctor()
extern "C" void CardboardPostRender__ctor_m1532 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardPostRender::.cctor()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardPostRender_t246_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D41_41_FieldInfo_var;
extern "C" void CardboardPostRender__cctor_m1533 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		CardboardPostRender_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D41_41_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 41);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleU5BU5D_t72* L_0 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, 5));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D41_41_FieldInfo_var), /*hidden argument*/NULL);
		((CardboardPostRender_t246_StaticFields*)CardboardPostRender_t246_il2cpp_TypeInfo_var->static_fields)->___Angles_22 = L_0;
		return;
	}
}
// UnityEngine.Camera CardboardPostRender::get_camera()
extern "C" Camera_t14 * CardboardPostRender_get_camera_m1534 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = (__this->___U3CcameraU3Ek__BackingField_23);
		return L_0;
	}
}
// System.Void CardboardPostRender::set_camera(UnityEngine.Camera)
extern "C" void CardboardPostRender_set_camera_m1535 (CardboardPostRender_t246 * __this, Camera_t14 * ___value, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = ___value;
		__this->___U3CcameraU3Ek__BackingField_23 = L_0;
		return;
	}
}
// System.Void CardboardPostRender::Reset()
extern "C" void CardboardPostRender_Reset_m1536 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_set_clearFlags_m2903(L_0, 3, /*hidden argument*/NULL);
		Camera_t14 * L_1 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		Color_t6  L_2 = Color_get_magenta_m2904(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_set_backgroundColor_m2905(L_1, L_2, /*hidden argument*/NULL);
		Camera_t14 * L_3 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_set_orthographic_m2906(L_3, 1, /*hidden argument*/NULL);
		Camera_t14 * L_4 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_orthographicSize_m2907(L_4, (0.5f), /*hidden argument*/NULL);
		Camera_t14 * L_5 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_cullingMask_m2885(L_5, 0, /*hidden argument*/NULL);
		Camera_t14 * L_6 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Camera_set_useOcclusionCulling_m2908(L_6, 0, /*hidden argument*/NULL);
		Camera_t14 * L_7 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Camera_set_depth_m2887(L_7, (100.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardPostRender::Awake()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral322;
extern Il2CppCodeGenString* _stringLiteral323;
extern "C" void CardboardPostRender_Awake_m1537 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		_stringLiteral322 = il2cpp_codegen_string_literal_from_index(322);
		_stringLiteral323 = il2cpp_codegen_string_literal_from_index(323);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t14 * L_0 = Component_GetComponent_TisCamera_t14_m2850(__this, /*hidden argument*/Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var);
		CardboardPostRender_set_camera_m1535(__this, L_0, /*hidden argument*/NULL);
		CardboardPostRender_Reset_m1536(__this, /*hidden argument*/NULL);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral322, /*hidden argument*/NULL);
		Material_t2 * L_2 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_2, L_1, /*hidden argument*/NULL);
		__this->___meshMaterial_15 = L_2;
		Shader_t1 * L_3 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral323, /*hidden argument*/NULL);
		Material_t2 * L_4 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_4, L_3, /*hidden argument*/NULL);
		__this->___uiMaterial_16 = L_4;
		Material_t2 * L_5 = (__this->___uiMaterial_16);
		Color_t6  L_6 = {0};
		Color__ctor_m2747(&L_6, (0.8f), (0.8f), (0.8f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_color_m2765(L_5, L_6, /*hidden argument*/NULL);
		bool L_7 = Application_get_isEditor_m2855(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_006b;
		}
	}
	{
		CardboardPostRender_ComputeUIMatrix_m1544(__this, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void CardboardPostRender::OnRenderObject()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void CardboardPostRender_OnRenderObject_m1538 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	RenderTexture_t15 * V_1 = {0};
	{
		Camera_t14 * L_0 = Camera_get_current_m2909(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t14 * L_1 = CardboardPostRender_get_camera_m1534(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		return;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_3 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Cardboard_UpdateState_m1487(L_3, /*hidden argument*/NULL);
		Cardboard_t233 * L_4 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = Cardboard_get_DistortionCorrection_m1444(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Cardboard_t233 * L_6 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		RenderTexture_t15 * L_7 = Cardboard_get_StereoScreen_m1468(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		RenderTexture_t15 * L_8 = V_1;
		bool L_9 = Object_op_Equality_m2716(NULL /*static, unused*/, L_8, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_0049;
		}
	}

IL_0048:
	{
		return;
	}

IL_0049:
	{
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_12 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = Cardboard_get_NativeDistortionCorrectionSupported_m1462(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_14 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		Cardboard_PostRender_m1490(L_14, /*hidden argument*/NULL);
		goto IL_00ce;
	}

IL_006e:
	{
		Mesh_t245 * L_15 = (__this->___distortionMesh_14);
		bool L_16 = Object_op_Equality_m2716(NULL /*static, unused*/, L_15, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_17 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		bool L_18 = Cardboard_get_ProfileChanged_m1483(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0094;
		}
	}

IL_008e:
	{
		CardboardPostRender_RebuildDistortionMesh_m1539(__this, /*hidden argument*/NULL);
	}

IL_0094:
	{
		Material_t2 * L_19 = (__this->___meshMaterial_15);
		RenderTexture_t15 * L_20 = V_1;
		NullCheck(L_19);
		Material_set_mainTexture_m2805(L_19, L_20, /*hidden argument*/NULL);
		Material_t2 * L_21 = (__this->___meshMaterial_15);
		NullCheck(L_21);
		Material_SetPass_m2910(L_21, 0, /*hidden argument*/NULL);
		Mesh_t245 * L_22 = (__this->___distortionMesh_14);
		Transform_t243 * L_23 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t215  L_24 = Transform_get_position_m2894(L_23, /*hidden argument*/NULL);
		Transform_t243 * L_25 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Quaternion_t261  L_26 = Transform_get_rotation_m2897(L_25, /*hidden argument*/NULL);
		Graphics_DrawMeshNow_m2911(NULL /*static, unused*/, L_22, L_24, L_26, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		RenderTexture_t15 * L_27 = V_1;
		NullCheck(L_27);
		RenderTexture_DiscardContents_m2912(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_28 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_28);
		bool L_29 = Cardboard_get_NativeUILayerSupported_m1464(L_28, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00e9;
		}
	}
	{
		CardboardPostRender_DrawUILayer_m1543(__this, /*hidden argument*/NULL);
	}

IL_00e9:
	{
		return;
	}
}
// System.Void CardboardPostRender::RebuildDistortionMesh()
extern TypeInfo* Mesh_t245_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardPostRender_t246_il2cpp_TypeInfo_var;
extern "C" void CardboardPostRender_RebuildDistortionMesh_m1539 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mesh_t245_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(189);
		CardboardPostRender_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		s_Il2CppMethodIntialized = true;
	}
	Vector3U5BU5D_t317* V_0 = {0};
	Vector2U5BU5D_t437* V_1 = {0};
	Int32U5BU5D_t269* V_2 = {0};
	ColorU5BU5D_t449* V_3 = {0};
	{
		Mesh_t245 * L_0 = (Mesh_t245 *)il2cpp_codegen_object_new (Mesh_t245_il2cpp_TypeInfo_var);
		Mesh__ctor_m2913(L_0, /*hidden argument*/NULL);
		__this->___distortionMesh_14 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(CardboardPostRender_t246_il2cpp_TypeInfo_var);
		CardboardPostRender_ComputeMeshPoints_m1540(NULL /*static, unused*/, ((int32_t)40), ((int32_t)40), 1, (&V_0), (&V_1), /*hidden argument*/NULL);
		Int32U5BU5D_t269* L_1 = CardboardPostRender_ComputeMeshIndices_m1542(NULL /*static, unused*/, ((int32_t)40), ((int32_t)40), 1, /*hidden argument*/NULL);
		V_2 = L_1;
		Vector2U5BU5D_t437* L_2 = V_1;
		Int32U5BU5D_t269* L_3 = V_2;
		ColorU5BU5D_t449* L_4 = CardboardPostRender_ComputeMeshColors_m1541(NULL /*static, unused*/, ((int32_t)40), ((int32_t)40), L_2, L_3, 1, /*hidden argument*/NULL);
		V_3 = L_4;
		Mesh_t245 * L_5 = (__this->___distortionMesh_14);
		Vector3U5BU5D_t317* L_6 = V_0;
		NullCheck(L_5);
		Mesh_set_vertices_m2914(L_5, L_6, /*hidden argument*/NULL);
		Mesh_t245 * L_7 = (__this->___distortionMesh_14);
		Vector2U5BU5D_t437* L_8 = V_1;
		NullCheck(L_7);
		Mesh_set_uv_m2915(L_7, L_8, /*hidden argument*/NULL);
		Mesh_t245 * L_9 = (__this->___distortionMesh_14);
		ColorU5BU5D_t449* L_10 = V_3;
		NullCheck(L_9);
		Mesh_set_colors_m2916(L_9, L_10, /*hidden argument*/NULL);
		Mesh_t245 * L_11 = (__this->___distortionMesh_14);
		Int32U5BU5D_t269* L_12 = V_2;
		NullCheck(L_11);
		Mesh_set_triangles_m2917(L_11, L_12, /*hidden argument*/NULL);
		Mesh_t245 * L_13 = (__this->___distortionMesh_14);
		NullCheck(L_13);
		Mesh_Optimize_m2918(L_13, /*hidden argument*/NULL);
		Mesh_t245 * L_14 = (__this->___distortionMesh_14);
		NullCheck(L_14);
		Mesh_UploadMeshData_m2919(L_14, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardPostRender::ComputeMeshPoints(System.Int32,System.Int32,System.Boolean,UnityEngine.Vector3[]&,UnityEngine.Vector2[]&)
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3U5BU5D_t317_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2U5BU5D_t437_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void CardboardPostRender_ComputeMeshPoints_m1540 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, bool ___distortVertices, Vector3U5BU5D_t317** ___vertices, Vector2U5BU5D_t437** ___tex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Vector3U5BU5D_t317_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(190);
		Vector2U5BU5D_t437_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(192);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t72* V_0 = {0};
	SingleU5BU5D_t72* V_1 = {0};
	Rect_t225  V_2 = {0};
	CardboardProfile_t255 * V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	float V_20 = 0.0f;
	float V_21 = 0.0f;
	float V_22 = 0.0f;
	float V_23 = 0.0f;
	float V_24 = 0.0f;
	float V_25 = 0.0f;
	{
		V_0 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, 4));
		V_1 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, 4));
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CardboardProfile_t255 * L_1 = Cardboard_get_Profile_m1470(L_0, /*hidden argument*/NULL);
		V_3 = L_1;
		CardboardProfile_t255 * L_2 = V_3;
		SingleU5BU5D_t72* L_3 = V_0;
		NullCheck(L_2);
		CardboardProfile_GetLeftEyeVisibleTanAngles_m1562(L_2, L_3, /*hidden argument*/NULL);
		CardboardProfile_t255 * L_4 = V_3;
		SingleU5BU5D_t72* L_5 = V_1;
		NullCheck(L_4);
		CardboardProfile_GetLeftEyeNoLensTanAngles_m1563(L_4, L_5, /*hidden argument*/NULL);
		CardboardProfile_t255 * L_6 = V_3;
		SingleU5BU5D_t72* L_7 = V_1;
		NullCheck(L_6);
		Rect_t225  L_8 = CardboardProfile_GetLeftEyeVisibleScreenRect_m1564(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3U5BU5D_t317** L_9 = ___vertices;
		int32_t L_10 = ___width;
		int32_t L_11 = ___height;
		*((Object_t **)(L_9)) = (Object_t *)((Vector3U5BU5D_t317*)SZArrayNew(Vector3U5BU5D_t317_il2cpp_TypeInfo_var, ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_10))*(int32_t)L_11))));
		Vector2U5BU5D_t437** L_12 = ___tex;
		int32_t L_13 = ___width;
		int32_t L_14 = ___height;
		*((Object_t **)(L_12)) = (Object_t *)((Vector2U5BU5D_t437*)SZArrayNew(Vector2U5BU5D_t437_il2cpp_TypeInfo_var, ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_13))*(int32_t)L_14))));
		V_4 = 0;
		V_5 = 0;
		goto IL_0292;
	}

IL_0053:
	{
		V_6 = 0;
		goto IL_0230;
	}

IL_005b:
	{
		V_7 = 0;
		goto IL_0222;
	}

IL_0063:
	{
		int32_t L_15 = V_7;
		int32_t L_16 = ___width;
		V_8 = ((float)((float)(((float)L_15))/(float)(((float)((int32_t)((int32_t)L_16-(int32_t)1))))));
		int32_t L_17 = V_6;
		int32_t L_18 = ___height;
		V_9 = ((float)((float)(((float)L_17))/(float)(((float)((int32_t)((int32_t)L_18-(int32_t)1))))));
		bool L_19 = ___distortVertices;
		if (!L_19)
		{
			goto IL_0102;
		}
	}
	{
		float L_20 = V_8;
		V_10 = L_20;
		float L_21 = V_9;
		V_11 = L_21;
		SingleU5BU5D_t72* L_22 = V_0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 0);
		int32_t L_23 = 0;
		SingleU5BU5D_t72* L_24 = V_0;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		int32_t L_25 = 2;
		float L_26 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_27 = Mathf_Lerp_m2756(NULL /*static, unused*/, (*(float*)(float*)SZArrayLdElema(L_22, L_23)), (*(float*)(float*)SZArrayLdElema(L_24, L_25)), L_26, /*hidden argument*/NULL);
		V_12 = L_27;
		SingleU5BU5D_t72* L_28 = V_0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 3);
		int32_t L_29 = 3;
		SingleU5BU5D_t72* L_30 = V_0;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 1);
		int32_t L_31 = 1;
		float L_32 = V_9;
		float L_33 = Mathf_Lerp_m2756(NULL /*static, unused*/, (*(float*)(float*)SZArrayLdElema(L_28, L_29)), (*(float*)(float*)SZArrayLdElema(L_30, L_31)), L_32, /*hidden argument*/NULL);
		V_13 = L_33;
		float L_34 = V_12;
		float L_35 = V_12;
		float L_36 = V_13;
		float L_37 = V_13;
		float L_38 = sqrtf(((float)((float)((float)((float)L_34*(float)L_35))+(float)((float)((float)L_36*(float)L_37)))));
		V_14 = L_38;
		CardboardProfile_t255 * L_39 = V_3;
		NullCheck(L_39);
		Device_t252 * L_40 = &(L_39->___device_1);
		Distortion_t251 * L_41 = &(L_40->___distortion_2);
		float L_42 = V_14;
		float L_43 = Distortion_distortInv_m1556(L_41, L_42, /*hidden argument*/NULL);
		V_15 = L_43;
		float L_44 = V_12;
		float L_45 = V_15;
		float L_46 = V_14;
		V_16 = ((float)((float)((float)((float)L_44*(float)L_45))/(float)L_46));
		float L_47 = V_13;
		float L_48 = V_15;
		float L_49 = V_14;
		V_17 = ((float)((float)((float)((float)L_47*(float)L_48))/(float)L_49));
		float L_50 = V_16;
		SingleU5BU5D_t72* L_51 = V_1;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 0);
		int32_t L_52 = 0;
		SingleU5BU5D_t72* L_53 = V_1;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 2);
		int32_t L_54 = 2;
		SingleU5BU5D_t72* L_55 = V_1;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 0);
		int32_t L_56 = 0;
		V_8 = ((float)((float)((float)((float)L_50-(float)(*(float*)(float*)SZArrayLdElema(L_51, L_52))))/(float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_53, L_54))-(float)(*(float*)(float*)SZArrayLdElema(L_55, L_56))))));
		float L_57 = V_17;
		SingleU5BU5D_t72* L_58 = V_1;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 3);
		int32_t L_59 = 3;
		SingleU5BU5D_t72* L_60 = V_1;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 1);
		int32_t L_61 = 1;
		SingleU5BU5D_t72* L_62 = V_1;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 3);
		int32_t L_63 = 3;
		V_9 = ((float)((float)((float)((float)L_57-(float)(*(float*)(float*)SZArrayLdElema(L_58, L_59))))/(float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_60, L_61))-(float)(*(float*)(float*)SZArrayLdElema(L_62, L_63))))));
		goto IL_0184;
	}

IL_0102:
	{
		SingleU5BU5D_t72* L_64 = V_1;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 0);
		int32_t L_65 = 0;
		SingleU5BU5D_t72* L_66 = V_1;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 2);
		int32_t L_67 = 2;
		float L_68 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_69 = Mathf_Lerp_m2756(NULL /*static, unused*/, (*(float*)(float*)SZArrayLdElema(L_64, L_65)), (*(float*)(float*)SZArrayLdElema(L_66, L_67)), L_68, /*hidden argument*/NULL);
		V_18 = L_69;
		SingleU5BU5D_t72* L_70 = V_1;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 3);
		int32_t L_71 = 3;
		SingleU5BU5D_t72* L_72 = V_1;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, 1);
		int32_t L_73 = 1;
		float L_74 = V_9;
		float L_75 = Mathf_Lerp_m2756(NULL /*static, unused*/, (*(float*)(float*)SZArrayLdElema(L_70, L_71)), (*(float*)(float*)SZArrayLdElema(L_72, L_73)), L_74, /*hidden argument*/NULL);
		V_19 = L_75;
		float L_76 = V_18;
		float L_77 = V_18;
		float L_78 = V_19;
		float L_79 = V_19;
		float L_80 = sqrtf(((float)((float)((float)((float)L_76*(float)L_77))+(float)((float)((float)L_78*(float)L_79)))));
		V_20 = L_80;
		CardboardProfile_t255 * L_81 = V_3;
		NullCheck(L_81);
		Device_t252 * L_82 = &(L_81->___device_1);
		Distortion_t251 * L_83 = &(L_82->___distortion_2);
		float L_84 = V_20;
		float L_85 = Distortion_distort_m1555(L_83, L_84, /*hidden argument*/NULL);
		V_21 = L_85;
		float L_86 = V_18;
		float L_87 = V_21;
		float L_88 = V_20;
		V_22 = ((float)((float)((float)((float)L_86*(float)L_87))/(float)L_88));
		float L_89 = V_19;
		float L_90 = V_21;
		float L_91 = V_20;
		V_23 = ((float)((float)((float)((float)L_89*(float)L_90))/(float)L_91));
		float L_92 = V_22;
		SingleU5BU5D_t72* L_93 = V_0;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 0);
		int32_t L_94 = 0;
		SingleU5BU5D_t72* L_95 = V_0;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 2);
		int32_t L_96 = 2;
		SingleU5BU5D_t72* L_97 = V_0;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 0);
		int32_t L_98 = 0;
		float L_99 = Mathf_Clamp01_m2828(NULL /*static, unused*/, ((float)((float)((float)((float)L_92-(float)(*(float*)(float*)SZArrayLdElema(L_93, L_94))))/(float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_95, L_96))-(float)(*(float*)(float*)SZArrayLdElema(L_97, L_98)))))), /*hidden argument*/NULL);
		V_10 = L_99;
		float L_100 = V_23;
		SingleU5BU5D_t72* L_101 = V_0;
		NullCheck(L_101);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_101, 3);
		int32_t L_102 = 3;
		SingleU5BU5D_t72* L_103 = V_0;
		NullCheck(L_103);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_103, 1);
		int32_t L_104 = 1;
		SingleU5BU5D_t72* L_105 = V_0;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, 3);
		int32_t L_106 = 3;
		float L_107 = Mathf_Clamp01_m2828(NULL /*static, unused*/, ((float)((float)((float)((float)L_100-(float)(*(float*)(float*)SZArrayLdElema(L_101, L_102))))/(float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_103, L_104))-(float)(*(float*)(float*)SZArrayLdElema(L_105, L_106)))))), /*hidden argument*/NULL);
		V_11 = L_107;
	}

IL_0184:
	{
		CardboardProfile_t255 * L_108 = V_3;
		NullCheck(L_108);
		Screen_t248 * L_109 = &(L_108->___screen_0);
		float L_110 = (L_109->___width_0);
		CardboardProfile_t255 * L_111 = V_3;
		NullCheck(L_111);
		Screen_t248 * L_112 = &(L_111->___screen_0);
		float L_113 = (L_112->___height_1);
		V_24 = ((float)((float)L_110/(float)L_113));
		float L_114 = Rect_get_x_m2861((&V_2), /*hidden argument*/NULL);
		float L_115 = V_8;
		float L_116 = Rect_get_width_m2798((&V_2), /*hidden argument*/NULL);
		float L_117 = V_24;
		V_8 = ((float)((float)((float)((float)((float)((float)L_114+(float)((float)((float)L_115*(float)L_116))))-(float)(0.5f)))*(float)L_117));
		float L_118 = Rect_get_y_m2865((&V_2), /*hidden argument*/NULL);
		float L_119 = V_9;
		float L_120 = Rect_get_height_m2800((&V_2), /*hidden argument*/NULL);
		V_9 = ((float)((float)((float)((float)L_118+(float)((float)((float)L_119*(float)L_120))))-(float)(0.5f)));
		Vector3U5BU5D_t317** L_121 = ___vertices;
		int32_t L_122 = V_5;
		NullCheck((*((Vector3U5BU5D_t317**)L_121)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((Vector3U5BU5D_t317**)L_121)), L_122);
		float L_123 = V_8;
		float L_124 = V_9;
		Vector3_t215  L_125 = {0};
		Vector3__ctor_m2812(&L_125, L_123, L_124, (1.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema((*((Vector3U5BU5D_t317**)L_121)), L_122)) = L_125;
		float L_126 = V_10;
		int32_t L_127 = V_4;
		V_10 = ((float)((float)((float)((float)L_126+(float)(((float)L_127))))/(float)(2.0f)));
		Vector2U5BU5D_t437** L_128 = ___tex;
		int32_t L_129 = V_5;
		NullCheck((*((Vector2U5BU5D_t437**)L_128)));
		IL2CPP_ARRAY_BOUNDS_CHECK((*((Vector2U5BU5D_t437**)L_128)), L_129);
		float L_130 = V_10;
		float L_131 = V_11;
		Vector2_t7  L_132 = {0};
		Vector2__ctor_m2714(&L_132, L_130, L_131, /*hidden argument*/NULL);
		*((Vector2_t7 *)(Vector2_t7 *)SZArrayLdElema((*((Vector2U5BU5D_t437**)L_128)), L_129)) = L_132;
		int32_t L_133 = V_7;
		V_7 = ((int32_t)((int32_t)L_133+(int32_t)1));
		int32_t L_134 = V_5;
		V_5 = ((int32_t)((int32_t)L_134+(int32_t)1));
	}

IL_0222:
	{
		int32_t L_135 = V_7;
		int32_t L_136 = ___width;
		if ((((int32_t)L_135) < ((int32_t)L_136)))
		{
			goto IL_0063;
		}
	}
	{
		int32_t L_137 = V_6;
		V_6 = ((int32_t)((int32_t)L_137+(int32_t)1));
	}

IL_0230:
	{
		int32_t L_138 = V_6;
		int32_t L_139 = ___height;
		if ((((int32_t)L_138) < ((int32_t)L_139)))
		{
			goto IL_005b;
		}
	}
	{
		SingleU5BU5D_t72* L_140 = V_0;
		NullCheck(L_140);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_140, 2);
		int32_t L_141 = 2;
		SingleU5BU5D_t72* L_142 = V_0;
		NullCheck(L_142);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_142, 0);
		int32_t L_143 = 0;
		V_25 = ((float)((float)(*(float*)(float*)SZArrayLdElema(L_140, L_141))-(float)(*(float*)(float*)SZArrayLdElema(L_142, L_143))));
		SingleU5BU5D_t72* L_144 = V_0;
		float L_145 = V_25;
		SingleU5BU5D_t72* L_146 = V_0;
		NullCheck(L_146);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_146, 0);
		int32_t L_147 = 0;
		NullCheck(L_144);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_144, 0);
		*((float*)(float*)SZArrayLdElema(L_144, 0)) = (float)((-((float)((float)L_145+(float)(*(float*)(float*)SZArrayLdElema(L_146, L_147))))));
		SingleU5BU5D_t72* L_148 = V_0;
		float L_149 = V_25;
		SingleU5BU5D_t72* L_150 = V_0;
		NullCheck(L_150);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_150, 2);
		int32_t L_151 = 2;
		NullCheck(L_148);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_148, 2);
		*((float*)(float*)SZArrayLdElema(L_148, 2)) = (float)((float)((float)L_149-(float)(*(float*)(float*)SZArrayLdElema(L_150, L_151))));
		SingleU5BU5D_t72* L_152 = V_1;
		NullCheck(L_152);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_152, 2);
		int32_t L_153 = 2;
		SingleU5BU5D_t72* L_154 = V_1;
		NullCheck(L_154);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_154, 0);
		int32_t L_155 = 0;
		V_25 = ((float)((float)(*(float*)(float*)SZArrayLdElema(L_152, L_153))-(float)(*(float*)(float*)SZArrayLdElema(L_154, L_155))));
		SingleU5BU5D_t72* L_156 = V_1;
		float L_157 = V_25;
		SingleU5BU5D_t72* L_158 = V_1;
		NullCheck(L_158);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_158, 0);
		int32_t L_159 = 0;
		NullCheck(L_156);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_156, 0);
		*((float*)(float*)SZArrayLdElema(L_156, 0)) = (float)((-((float)((float)L_157+(float)(*(float*)(float*)SZArrayLdElema(L_158, L_159))))));
		SingleU5BU5D_t72* L_160 = V_1;
		float L_161 = V_25;
		SingleU5BU5D_t72* L_162 = V_1;
		NullCheck(L_162);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_162, 2);
		int32_t L_163 = 2;
		NullCheck(L_160);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_160, 2);
		*((float*)(float*)SZArrayLdElema(L_160, 2)) = (float)((float)((float)L_161-(float)(*(float*)(float*)SZArrayLdElema(L_162, L_163))));
		float L_164 = Rect_get_x_m2861((&V_2), /*hidden argument*/NULL);
		float L_165 = Rect_get_width_m2798((&V_2), /*hidden argument*/NULL);
		Rect_set_x_m2862((&V_2), ((float)((float)(1.0f)-(float)((float)((float)L_164+(float)L_165)))), /*hidden argument*/NULL);
		int32_t L_166 = V_4;
		V_4 = ((int32_t)((int32_t)L_166+(int32_t)1));
	}

IL_0292:
	{
		int32_t L_167 = V_4;
		if ((((int32_t)L_167) < ((int32_t)2)))
		{
			goto IL_0053;
		}
	}
	{
		return;
	}
}
// UnityEngine.Color[] CardboardPostRender::ComputeMeshColors(System.Int32,System.Int32,UnityEngine.Vector2[],System.Int32[],System.Boolean)
extern TypeInfo* ColorU5BU5D_t449_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" ColorU5BU5D_t449* CardboardPostRender_ComputeMeshColors_m1541 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, Vector2U5BU5D_t437* ___tex, Int32U5BU5D_t269* ___indices, bool ___distortVertices, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ColorU5BU5D_t449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(194);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	ColorU5BU5D_t449* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector2_t7  V_5 = {0};
	{
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		V_0 = ((ColorU5BU5D_t449*)SZArrayNew(ColorU5BU5D_t449_il2cpp_TypeInfo_var, ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_0))*(int32_t)L_1))));
		V_1 = 0;
		V_2 = 0;
		goto IL_0114;
	}

IL_0014:
	{
		V_3 = 0;
		goto IL_0109;
	}

IL_001b:
	{
		V_4 = 0;
		goto IL_00fd;
	}

IL_0023:
	{
		ColorU5BU5D_t449* L_2 = V_0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		Color_t6  L_4 = Color_get_white_m2920(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Color_t6 *)(Color_t6 *)SZArrayLdElema(L_2, L_3)) = L_4;
		bool L_5 = ___distortVertices;
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_6 = V_4;
		if (!L_6)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_7 = V_3;
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_8 = V_4;
		int32_t L_9 = ___width;
		if ((((int32_t)L_8) == ((int32_t)((int32_t)((int32_t)L_9-(int32_t)1)))))
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_10 = V_3;
		int32_t L_11 = ___height;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)((int32_t)L_11-(int32_t)1))))))
		{
			goto IL_006c;
		}
	}

IL_005b:
	{
		ColorU5BU5D_t449* L_12 = V_0;
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		Color_t6  L_14 = Color_get_black_m2921(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Color_t6 *)(Color_t6 *)SZArrayLdElema(L_12, L_13)) = L_14;
	}

IL_006c:
	{
		goto IL_00f3;
	}

IL_0071:
	{
		Vector2U5BU5D_t437* L_15 = ___tex;
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		V_5 = (*(Vector2_t7 *)((Vector2_t7 *)(Vector2_t7 *)SZArrayLdElema(L_15, L_16)));
		float L_17 = ((&V_5)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_18 = fabsf(((float)((float)((float)((float)L_17*(float)(2.0f)))-(float)(1.0f))));
		(&V_5)->___x_1 = L_18;
		float L_19 = ((&V_5)->___x_1);
		if ((((float)L_19) <= ((float)(0.0f))))
		{
			goto IL_00e2;
		}
	}
	{
		float L_20 = ((&V_5)->___y_2);
		if ((((float)L_20) <= ((float)(0.0f))))
		{
			goto IL_00e2;
		}
	}
	{
		float L_21 = ((&V_5)->___x_1);
		if ((((float)L_21) >= ((float)(1.0f))))
		{
			goto IL_00e2;
		}
	}
	{
		float L_22 = ((&V_5)->___y_2);
		if ((!(((float)L_22) >= ((float)(1.0f)))))
		{
			goto IL_00f3;
		}
	}

IL_00e2:
	{
		ColorU5BU5D_t449* L_23 = V_0;
		int32_t L_24 = V_2;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		Color_t6  L_25 = Color_get_black_m2921(NULL /*static, unused*/, /*hidden argument*/NULL);
		*((Color_t6 *)(Color_t6 *)SZArrayLdElema(L_23, L_24)) = L_25;
	}

IL_00f3:
	{
		int32_t L_26 = V_4;
		V_4 = ((int32_t)((int32_t)L_26+(int32_t)1));
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00fd:
	{
		int32_t L_28 = V_4;
		int32_t L_29 = ___width;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_30 = V_3;
		V_3 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_0109:
	{
		int32_t L_31 = V_3;
		int32_t L_32 = ___height;
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_33 = V_1;
		V_1 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_0114:
	{
		int32_t L_34 = V_1;
		if ((((int32_t)L_34) < ((int32_t)2)))
		{
			goto IL_0014;
		}
	}
	{
		ColorU5BU5D_t449* L_35 = V_0;
		return L_35;
	}
}
// System.Int32[] CardboardPostRender::ComputeMeshIndices(System.Int32,System.Int32,System.Boolean)
extern TypeInfo* Int32U5BU5D_t269_il2cpp_TypeInfo_var;
extern "C" Int32U5BU5D_t269* CardboardPostRender_ComputeMeshIndices_m1542 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, bool ___distortVertices, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(196);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t269* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		int32_t L_0 = ___width;
		int32_t L_1 = ___height;
		V_0 = ((Int32U5BU5D_t269*)SZArrayNew(Int32U5BU5D_t269_il2cpp_TypeInfo_var, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)((int32_t)((int32_t)L_0-(int32_t)1))))*(int32_t)((int32_t)((int32_t)L_1-(int32_t)1))))*(int32_t)6))));
		int32_t L_2 = ___width;
		V_1 = ((int32_t)((int32_t)L_2/(int32_t)2));
		int32_t L_3 = ___height;
		V_2 = ((int32_t)((int32_t)L_3/(int32_t)2));
		V_3 = 0;
		V_4 = 0;
		V_5 = 0;
		goto IL_0125;
	}

IL_0026:
	{
		V_6 = 0;
		goto IL_0119;
	}

IL_002e:
	{
		V_7 = 0;
		goto IL_010b;
	}

IL_0036:
	{
		int32_t L_4 = V_7;
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_5 = V_6;
		if (L_5)
		{
			goto IL_0049;
		}
	}

IL_0044:
	{
		goto IL_00ff;
	}

IL_0049:
	{
		int32_t L_6 = V_7;
		int32_t L_7 = V_1;
		int32_t L_8 = V_6;
		int32_t L_9 = V_2;
		if ((!(((uint32_t)((((int32_t)((((int32_t)L_6) > ((int32_t)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0)) == ((uint32_t)((((int32_t)((((int32_t)L_8) > ((int32_t)L_9))? 1 : 0)) == ((int32_t)0))? 1 : 0)))))
		{
			goto IL_00b1;
		}
	}
	{
		Int32U5BU5D_t269* L_10 = V_0;
		int32_t L_11 = V_5;
		int32_t L_12 = L_11;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = V_4;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_12);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_10, L_12)) = (int32_t)L_13;
		Int32U5BU5D_t269* L_14 = V_0;
		int32_t L_15 = V_5;
		int32_t L_16 = L_15;
		V_5 = ((int32_t)((int32_t)L_16+(int32_t)1));
		int32_t L_17 = V_4;
		int32_t L_18 = ___width;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_16);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_14, L_16)) = (int32_t)((int32_t)((int32_t)L_17-(int32_t)L_18));
		Int32U5BU5D_t269* L_19 = V_0;
		int32_t L_20 = V_5;
		int32_t L_21 = L_20;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
		int32_t L_22 = V_4;
		int32_t L_23 = ___width;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_21);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_19, L_21)) = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_22-(int32_t)L_23))-(int32_t)1));
		Int32U5BU5D_t269* L_24 = V_0;
		int32_t L_25 = V_5;
		int32_t L_26 = L_25;
		V_5 = ((int32_t)((int32_t)L_26+(int32_t)1));
		int32_t L_27 = V_4;
		int32_t L_28 = ___width;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_26);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_24, L_26)) = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27-(int32_t)L_28))-(int32_t)1));
		Int32U5BU5D_t269* L_29 = V_0;
		int32_t L_30 = V_5;
		int32_t L_31 = L_30;
		V_5 = ((int32_t)((int32_t)L_31+(int32_t)1));
		int32_t L_32 = V_4;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_31);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, L_31)) = (int32_t)((int32_t)((int32_t)L_32-(int32_t)1));
		Int32U5BU5D_t269* L_33 = V_0;
		int32_t L_34 = V_5;
		int32_t L_35 = L_34;
		V_5 = ((int32_t)((int32_t)L_35+(int32_t)1));
		int32_t L_36 = V_4;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_35);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_33, L_35)) = (int32_t)L_36;
		goto IL_00ff;
	}

IL_00b1:
	{
		Int32U5BU5D_t269* L_37 = V_0;
		int32_t L_38 = V_5;
		int32_t L_39 = L_38;
		V_5 = ((int32_t)((int32_t)L_39+(int32_t)1));
		int32_t L_40 = V_4;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_39);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, L_39)) = (int32_t)((int32_t)((int32_t)L_40-(int32_t)1));
		Int32U5BU5D_t269* L_41 = V_0;
		int32_t L_42 = V_5;
		int32_t L_43 = L_42;
		V_5 = ((int32_t)((int32_t)L_43+(int32_t)1));
		int32_t L_44 = V_4;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_43);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_41, L_43)) = (int32_t)L_44;
		Int32U5BU5D_t269* L_45 = V_0;
		int32_t L_46 = V_5;
		int32_t L_47 = L_46;
		V_5 = ((int32_t)((int32_t)L_47+(int32_t)1));
		int32_t L_48 = V_4;
		int32_t L_49 = ___width;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_47);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_45, L_47)) = (int32_t)((int32_t)((int32_t)L_48-(int32_t)L_49));
		Int32U5BU5D_t269* L_50 = V_0;
		int32_t L_51 = V_5;
		int32_t L_52 = L_51;
		V_5 = ((int32_t)((int32_t)L_52+(int32_t)1));
		int32_t L_53 = V_4;
		int32_t L_54 = ___width;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, L_52);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_50, L_52)) = (int32_t)((int32_t)((int32_t)L_53-(int32_t)L_54));
		Int32U5BU5D_t269* L_55 = V_0;
		int32_t L_56 = V_5;
		int32_t L_57 = L_56;
		V_5 = ((int32_t)((int32_t)L_57+(int32_t)1));
		int32_t L_58 = V_4;
		int32_t L_59 = ___width;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_57);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_55, L_57)) = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_58-(int32_t)L_59))-(int32_t)1));
		Int32U5BU5D_t269* L_60 = V_0;
		int32_t L_61 = V_5;
		int32_t L_62 = L_61;
		V_5 = ((int32_t)((int32_t)L_62+(int32_t)1));
		int32_t L_63 = V_4;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, L_62);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_60, L_62)) = (int32_t)((int32_t)((int32_t)L_63-(int32_t)1));
	}

IL_00ff:
	{
		int32_t L_64 = V_7;
		V_7 = ((int32_t)((int32_t)L_64+(int32_t)1));
		int32_t L_65 = V_4;
		V_4 = ((int32_t)((int32_t)L_65+(int32_t)1));
	}

IL_010b:
	{
		int32_t L_66 = V_7;
		int32_t L_67 = ___width;
		if ((((int32_t)L_66) < ((int32_t)L_67)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_68 = V_6;
		V_6 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_0119:
	{
		int32_t L_69 = V_6;
		int32_t L_70 = ___height;
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_71 = V_3;
		V_3 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_0125:
	{
		int32_t L_72 = V_3;
		if ((((int32_t)L_72) < ((int32_t)2)))
		{
			goto IL_0026;
		}
	}
	{
		Int32U5BU5D_t269* L_73 = V_0;
		return L_73;
	}
}
// System.Void CardboardPostRender::DrawUILayer()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void CardboardPostRender_DrawUILayer_m1543 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Cardboard_get_VRModeEnabled_m1442(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Application_get_isEditor_m2855(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		CardboardPostRender_ComputeUIMatrix_m1544(__this, /*hidden argument*/NULL);
	}

IL_001b:
	{
		Material_t2 * L_3 = (__this->___uiMaterial_16);
		NullCheck(L_3);
		Material_SetPass_m2910(L_3, 0, /*hidden argument*/NULL);
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_5 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Cardboard_get_EnableSettingsButton_m1448(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		CardboardPostRender_DrawSettingsButton_m1545(__this, /*hidden argument*/NULL);
	}

IL_0043:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_8 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = Cardboard_get_EnableAlignmentMarker_m1446(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005e;
		}
	}
	{
		CardboardPostRender_DrawAlignmentMarker_m1546(__this, /*hidden argument*/NULL);
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_10 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = Cardboard_get_BackButtonMode_m1450(L_10, /*hidden argument*/NULL);
		if ((((int32_t)L_11) == ((int32_t)2)))
		{
			goto IL_0084;
		}
	}
	{
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_13 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = Cardboard_get_BackButtonMode_m1450(L_13, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_008a;
		}
	}

IL_0084:
	{
		CardboardPostRender_DrawVRBackButton_m1547(__this, /*hidden argument*/NULL);
	}

IL_008a:
	{
		return;
	}
}
// System.Void CardboardPostRender::ComputeUIMatrix()
extern "C" void CardboardPostRender_ComputeUIMatrix_m1544 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	{
		float L_0 = Screen_get_dpi_m2922(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___centerWidthPx_17 = ((float)((float)((float)((float)(0.025f)*(float)L_0))/(float)(2.0f)));
		float L_1 = Screen_get_dpi_m2922(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___buttonWidthPx_18 = ((float)((float)((float)((float)(0.175f)*(float)L_1))/(float)(2.0f)));
		float L_2 = (__this->___buttonWidthPx_18);
		int32_t L_3 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___xScale_19 = ((float)((float)L_2/(float)(((float)L_3))));
		float L_4 = (__this->___buttonWidthPx_18);
		int32_t L_5 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___yScale_20 = ((float)((float)L_4/(float)(((float)L_5))));
		float L_6 = (__this->___yScale_20);
		Vector3_t215  L_7 = {0};
		Vector3__ctor_m2812(&L_7, (0.5f), L_6, (0.0f), /*hidden argument*/NULL);
		Quaternion_t261  L_8 = Quaternion_get_identity_m2888(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (__this->___xScale_19);
		float L_10 = (__this->___yScale_20);
		Vector3_t215  L_11 = {0};
		Vector3__ctor_m2812(&L_11, L_9, L_10, (1.0f), /*hidden argument*/NULL);
		Matrix4x4_t242  L_12 = Matrix4x4_TRS_m2923(NULL /*static, unused*/, L_7, L_8, L_11, /*hidden argument*/NULL);
		__this->___xfm_21 = L_12;
		return;
	}
}
// System.Void CardboardPostRender::DrawSettingsButton()
extern TypeInfo* CardboardPostRender_t246_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void CardboardPostRender_DrawSettingsButton_m1545 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardPostRender_t246_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(180);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		GL_PushMatrix_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadOrtho_m2925(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t242  L_0 = (__this->___xfm_21);
		GL_MultMatrix_m2926(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		GL_Begin_m2927(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CardboardPostRender_t246_il2cpp_TypeInfo_var);
		SingleU5BU5D_t72* L_1 = ((CardboardPostRender_t246_StaticFields*)CardboardPostRender_t246_il2cpp_TypeInfo_var->static_fields)->___Angles_22;
		NullCheck(L_1);
		V_1 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))*(int32_t)6));
		goto IL_00cd;
	}

IL_002c:
	{
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(CardboardPostRender_t246_il2cpp_TypeInfo_var);
		SingleU5BU5D_t72* L_3 = ((CardboardPostRender_t246_StaticFields*)CardboardPostRender_t246_il2cpp_TypeInfo_var->static_fields)->___Angles_22;
		NullCheck(L_3);
		SingleU5BU5D_t72* L_4 = ((CardboardPostRender_t246_StaticFields*)CardboardPostRender_t246_il2cpp_TypeInfo_var->static_fields)->___Angles_22;
		int32_t L_5 = V_0;
		SingleU5BU5D_t72* L_6 = ((CardboardPostRender_t246_StaticFields*)CardboardPostRender_t246_il2cpp_TypeInfo_var->static_fields)->___Angles_22;
		NullCheck(L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, ((int32_t)((int32_t)L_5%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length))))));
		int32_t L_7 = ((int32_t)((int32_t)L_5%(int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))));
		V_2 = ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_2/(int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))))))*(float)(60.0f)))+(float)(*(float*)(float*)SZArrayLdElema(L_4, L_7))));
		float L_8 = V_2;
		V_3 = ((float)((float)((float)((float)(90.0f)-(float)L_8))*(float)(0.0174532924f)));
		float L_9 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_10 = cosf(L_9);
		V_4 = L_10;
		float L_11 = V_3;
		float L_12 = sinf(L_11);
		V_5 = L_12;
		float L_13 = V_2;
		float L_14 = Mathf_PingPong_m2928(NULL /*static, unused*/, L_13, (30.0f), /*hidden argument*/NULL);
		V_6 = L_14;
		float L_15 = V_6;
		V_7 = ((float)((float)((float)((float)L_15-(float)(12.0f)))/(float)(8.0f)));
		float L_16 = V_7;
		float L_17 = Mathf_Lerp_m2756(NULL /*static, unused*/, (1.0f), (0.75f), L_16, /*hidden argument*/NULL);
		V_8 = L_17;
		float L_18 = V_4;
		float L_19 = V_5;
		GL_Vertex3_m2929(NULL /*static, unused*/, ((float)((float)(0.3125f)*(float)L_18)), ((float)((float)(0.3125f)*(float)L_19)), (0.0f), /*hidden argument*/NULL);
		float L_20 = V_8;
		float L_21 = V_4;
		float L_22 = V_8;
		float L_23 = V_5;
		GL_Vertex3_m2929(NULL /*static, unused*/, ((float)((float)L_20*(float)L_21)), ((float)((float)L_22*(float)L_23)), (0.0f), /*hidden argument*/NULL);
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00cd:
	{
		int32_t L_25 = V_0;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) <= ((int32_t)L_26)))
		{
			goto IL_002c;
		}
	}
	{
		GL_End_m2930(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m2931(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardPostRender::DrawAlignmentMarker()
extern "C" void CardboardPostRender_DrawAlignmentMarker_m1546 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_0/(int32_t)2));
		float L_1 = (__this->___centerWidthPx_17);
		V_1 = (((int32_t)L_1));
		float L_2 = (__this->___buttonWidthPx_18);
		V_2 = (((int32_t)((float)((float)(3.0f)*(float)L_2))));
		GL_PushMatrix_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_LoadPixelMatrix_m2932(NULL /*static, unused*/, (0.0f), (((float)L_3)), (0.0f), (((float)L_4)), /*hidden argument*/NULL);
		GL_Begin_m2927(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		int32_t L_7 = V_2;
		GL_Vertex3_m2929(NULL /*static, unused*/, (((float)((int32_t)((int32_t)L_5-(int32_t)L_6)))), (((float)L_7)), (0.0f), /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		int32_t L_10 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = V_2;
		GL_Vertex3_m2929(NULL /*static, unused*/, (((float)((int32_t)((int32_t)L_8-(int32_t)L_9)))), (((float)((int32_t)((int32_t)L_10-(int32_t)L_11)))), (0.0f), /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		int32_t L_14 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		GL_Vertex3_m2929(NULL /*static, unused*/, (((float)((int32_t)((int32_t)L_12+(int32_t)L_13)))), (((float)((int32_t)((int32_t)L_14-(int32_t)L_15)))), (0.0f), /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		int32_t L_18 = V_2;
		GL_Vertex3_m2929(NULL /*static, unused*/, (((float)((int32_t)((int32_t)L_16+(int32_t)L_17)))), (((float)L_18)), (0.0f), /*hidden argument*/NULL);
		GL_End_m2930(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m2931(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardPostRender::DrawVRBackButton()
extern "C" void CardboardPostRender_DrawVRBackButton_m1547 (CardboardPostRender_t246 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// CardboardPreRender
#include "AssemblyU2DCSharp_CardboardPreRenderMethodDeclarations.h"



// System.Void CardboardPreRender::.ctor()
extern "C" void CardboardPreRender__ctor_m1548 (CardboardPreRender_t247 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Camera CardboardPreRender::get_camera()
extern "C" Camera_t14 * CardboardPreRender_get_camera_m1549 (CardboardPreRender_t247 * __this, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = (__this->___U3CcameraU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void CardboardPreRender::set_camera(UnityEngine.Camera)
extern "C" void CardboardPreRender_set_camera_m1550 (CardboardPreRender_t247 * __this, Camera_t14 * ___value, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = ___value;
		__this->___U3CcameraU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Void CardboardPreRender::Awake()
extern const MethodInfo* Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var;
extern "C" void CardboardPreRender_Awake_m1551 (CardboardPreRender_t247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t14 * L_0 = Component_GetComponent_TisCamera_t14_m2850(__this, /*hidden argument*/Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var);
		CardboardPreRender_set_camera_m1550(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardPreRender::Reset()
extern "C" void CardboardPreRender_Reset_m1552 (CardboardPreRender_t247 * __this, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = CardboardPreRender_get_camera_m1549(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Camera_set_clearFlags_m2903(L_0, 2, /*hidden argument*/NULL);
		Camera_t14 * L_1 = CardboardPreRender_get_camera_m1549(__this, /*hidden argument*/NULL);
		Color_t6  L_2 = Color_get_black_m2921(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_set_backgroundColor_m2905(L_1, L_2, /*hidden argument*/NULL);
		Camera_t14 * L_3 = CardboardPreRender_get_camera_m1549(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_set_cullingMask_m2885(L_3, 0, /*hidden argument*/NULL);
		Camera_t14 * L_4 = CardboardPreRender_get_camera_m1549(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_set_useOcclusionCulling_m2908(L_4, 0, /*hidden argument*/NULL);
		Camera_t14 * L_5 = CardboardPreRender_get_camera_m1549(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Camera_set_depth_m2887(L_5, (-100.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardPreRender::OnPreCull()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void CardboardPreRender_OnPreCull_m1553 (CardboardPreRender_t247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t15 * V_0 = {0};
	Camera_t14 * G_B4_0 = {0};
	Camera_t14 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	Camera_t14 * G_B5_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Cardboard_UpdateState_m1487(L_0, /*hidden argument*/NULL);
		Cardboard_t233 * L_1 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Cardboard_get_ProfileChanged_m1483(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		CardboardPreRender_SetShaderGlobals_m1554(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		Camera_t14 * L_3 = CardboardPreRender_get_camera_m1549(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_4 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = Cardboard_get_VRModeEnabled_m1442(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		if (!L_5)
		{
			G_B4_0 = L_3;
			goto IL_003a;
		}
	}
	{
		G_B5_0 = 2;
		G_B5_1 = G_B3_0;
		goto IL_003b;
	}

IL_003a:
	{
		G_B5_0 = 4;
		G_B5_1 = G_B4_0;
	}

IL_003b:
	{
		NullCheck(G_B5_1);
		Camera_set_clearFlags_m2903(G_B5_1, G_B5_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_6 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		RenderTexture_t15 * L_7 = Cardboard_get_StereoScreen_m1468(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		RenderTexture_t15 * L_8 = V_0;
		bool L_9 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_8, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0069;
		}
	}
	{
		RenderTexture_t15 * L_10 = V_0;
		NullCheck(L_10);
		bool L_11 = RenderTexture_IsCreated_m2933(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0069;
		}
	}
	{
		RenderTexture_t15 * L_12 = V_0;
		NullCheck(L_12);
		RenderTexture_Create_m2784(L_12, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// System.Void CardboardPreRender::SetShaderGlobals()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral324;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral325;
extern "C" void CardboardPreRender_SetShaderGlobals_m1554 (CardboardPreRender_t247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		_stringLiteral324 = il2cpp_codegen_string_literal_from_index(324);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral325 = il2cpp_codegen_string_literal_from_index(325);
		s_Il2CppMethodIntialized = true;
	}
	CardboardProfile_t255 * V_0 = {0};
	SingleU5BU5D_t72* V_1 = {0};
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		CardboardProfile_t255 * L_1 = Cardboard_get_Profile_m1470(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		CardboardProfile_t255 * L_2 = V_0;
		NullCheck(L_2);
		Device_t252 * L_3 = &(L_2->___device_1);
		Distortion_t251 * L_4 = &(L_3->___inverse_3);
		float L_5 = (L_4->___k1_0);
		CardboardProfile_t255 * L_6 = V_0;
		NullCheck(L_6);
		Device_t252 * L_7 = &(L_6->___device_1);
		Distortion_t251 * L_8 = &(L_7->___inverse_3);
		float L_9 = (L_8->___k2_1);
		Vector4_t5  L_10 = {0};
		Vector4__ctor_m2934(&L_10, L_5, L_9, /*hidden argument*/NULL);
		Shader_SetGlobalVector_m2878(NULL /*static, unused*/, _stringLiteral324, L_10, /*hidden argument*/NULL);
		CardboardProfile_t255 * L_11 = V_0;
		NullCheck(L_11);
		Device_t252 * L_12 = &(L_11->___device_1);
		Distortion_t251 * L_13 = &(L_12->___distortion_2);
		float L_14 = (L_13->___k1_0);
		CardboardProfile_t255 * L_15 = V_0;
		NullCheck(L_15);
		Device_t252 * L_16 = &(L_15->___device_1);
		Distortion_t251 * L_17 = &(L_16->___distortion_2);
		float L_18 = (L_17->___k2_1);
		Vector4_t5  L_19 = {0};
		Vector4__ctor_m2934(&L_19, L_14, L_18, /*hidden argument*/NULL);
		Shader_SetGlobalVector_m2878(NULL /*static, unused*/, _stringLiteral16, L_19, /*hidden argument*/NULL);
		V_1 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, 4));
		CardboardProfile_t255 * L_20 = V_0;
		SingleU5BU5D_t72* L_21 = V_1;
		NullCheck(L_20);
		CardboardProfile_GetLeftEyeVisibleTanAngles_m1562(L_20, L_21, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		float L_23 = CardboardProfile_GetMaxRadius_m1565(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		float L_24 = V_2;
		float L_25 = V_2;
		Shader_SetGlobalFloat_m2879(NULL /*static, unused*/, _stringLiteral325, ((float)((float)L_24*(float)L_25)), /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// CardboardProfile/Screen
#include "AssemblyU2DCSharp_CardboardProfile_ScreenMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// CardboardProfile/Lenses
#include "AssemblyU2DCSharp_CardboardProfile_LensesMethodDeclarations.h"



// CardboardProfile/MaxFOV
#include "AssemblyU2DCSharp_CardboardProfile_MaxFOV.h"
#ifndef _MSC_VER
#else
#endif
// CardboardProfile/MaxFOV
#include "AssemblyU2DCSharp_CardboardProfile_MaxFOVMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// System.Single CardboardProfile/Distortion::distort(System.Single)
extern "C" float Distortion_distort_m1555 (Distortion_t251 * __this, float ___r, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___r;
		float L_1 = ___r;
		V_0 = ((float)((float)L_0*(float)L_1));
		float L_2 = (__this->___k2_1);
		float L_3 = V_0;
		float L_4 = (__this->___k1_0);
		float L_5 = V_0;
		float L_6 = ___r;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_2*(float)L_3))+(float)L_4))*(float)L_5))+(float)(1.0f)))*(float)L_6));
	}
}
// System.Single CardboardProfile/Distortion::distortInv(System.Single)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float Distortion_distortInv_m1556 (Distortion_t251 * __this, float ___radius, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		V_0 = (0.0f);
		V_1 = (1.0f);
		float L_0 = ___radius;
		float L_1 = V_0;
		float L_2 = Distortion_distort_m1555(__this, L_1, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_0-(float)L_2));
		goto IL_0039;
	}

IL_001b:
	{
		float L_3 = ___radius;
		float L_4 = V_1;
		float L_5 = Distortion_distort_m1555(__this, L_4, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_3-(float)L_5));
		float L_6 = V_1;
		float L_7 = V_3;
		float L_8 = V_1;
		float L_9 = V_0;
		float L_10 = V_3;
		float L_11 = V_2;
		V_4 = ((float)((float)L_6-(float)((float)((float)L_7*(float)((float)((float)((float)((float)L_8-(float)L_9))/(float)((float)((float)L_10-(float)L_11))))))));
		float L_12 = V_1;
		V_0 = L_12;
		float L_13 = V_4;
		V_1 = L_13;
		float L_14 = V_3;
		V_2 = L_14;
	}

IL_0039:
	{
		float L_15 = V_1;
		float L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_17 = fabsf(((float)((float)L_15-(float)L_16)));
		if ((((float)L_17) > ((float)(0.0001f))))
		{
			goto IL_001b;
		}
	}
	{
		float L_18 = V_1;
		return L_18;
	}
}
#ifndef _MSC_VER
#else
#endif
// CardboardProfile/Device
#include "AssemblyU2DCSharp_CardboardProfile_DeviceMethodDeclarations.h"



// CardboardProfile/ScreenSizes
#include "AssemblyU2DCSharp_CardboardProfile_ScreenSizes.h"
#ifndef _MSC_VER
#else
#endif
// CardboardProfile/ScreenSizes
#include "AssemblyU2DCSharp_CardboardProfile_ScreenSizesMethodDeclarations.h"



// CardboardProfile/DeviceTypes
#include "AssemblyU2DCSharp_CardboardProfile_DeviceTypes.h"
#ifndef _MSC_VER
#else
#endif
// CardboardProfile/DeviceTypes
#include "AssemblyU2DCSharp_CardboardProfile_DeviceTypesMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// System.Double
#include "mscorlib_System_Double.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"


// System.Void CardboardProfile::.ctor()
extern "C" void CardboardProfile__ctor_m1557 (CardboardProfile_t255 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardProfile::.cctor()
extern TypeInfo* Screen_t248_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern TypeInfo* Device_t252_il2cpp_TypeInfo_var;
extern "C" void CardboardProfile__cctor_m1558 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Screen_t248_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		Device_t252_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(198);
		s_Il2CppMethodIntialized = true;
	}
	Screen_t248  V_0 = {0};
	Screen_t248  V_1 = {0};
	Screen_t248  V_2 = {0};
	Screen_t248  V_3 = {0};
	Screen_t248  V_4 = {0};
	Screen_t248  V_5 = {0};
	Screen_t248  V_6 = {0};
	Screen_t248  V_7 = {0};
	Screen_t248  V_8 = {0};
	Device_t252  V_9 = {0};
	Device_t252  V_10 = {0};
	Device_t252  V_11 = {0};
	CardboardProfile_t255 * V_12 = {0};
	{
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->___width_0 = (0.11f);
		(&V_0)->___height_1 = (0.062f);
		(&V_0)->___border_2 = (0.004f);
		Screen_t248  L_0 = V_0;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Nexus5_2 = L_0;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_1));
		(&V_1)->___width_0 = (0.133f);
		(&V_1)->___height_1 = (0.074f);
		(&V_1)->___border_2 = (0.004f);
		Screen_t248  L_1 = V_1;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Nexus6_3 = L_1;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_2));
		(&V_2)->___width_0 = (0.114f);
		(&V_2)->___height_1 = (0.0635f);
		(&V_2)->___border_2 = (0.0035f);
		Screen_t248  L_2 = V_2;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___GalaxyS6_4 = L_2;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_3));
		(&V_3)->___width_0 = (0.125f);
		(&V_3)->___height_1 = (0.0705f);
		(&V_3)->___border_2 = (0.0045f);
		Screen_t248  L_3 = V_3;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___GalaxyNote4_5 = L_3;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_4));
		(&V_4)->___width_0 = (0.121f);
		(&V_4)->___height_1 = (0.068f);
		(&V_4)->___border_2 = (0.003f);
		Screen_t248  L_4 = V_4;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___LGG3_6 = L_4;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_5));
		(&V_5)->___width_0 = (0.075f);
		(&V_5)->___height_1 = (0.05f);
		(&V_5)->___border_2 = (0.0045f);
		Screen_t248  L_5 = V_5;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone4_7 = L_5;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_6));
		(&V_6)->___width_0 = (0.089f);
		(&V_6)->___height_1 = (0.05f);
		(&V_6)->___border_2 = (0.0045f);
		Screen_t248  L_6 = V_6;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone5_8 = L_6;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_7));
		(&V_7)->___width_0 = (0.104f);
		(&V_7)->___height_1 = (0.058f);
		(&V_7)->___border_2 = (0.005f);
		Screen_t248  L_7 = V_7;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone6_9 = L_7;
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_8));
		(&V_8)->___width_0 = (0.112f);
		(&V_8)->___height_1 = (0.068f);
		(&V_8)->___border_2 = (0.005f);
		Screen_t248  L_8 = V_8;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone6p_10 = L_8;
		Initobj (Device_t252_il2cpp_TypeInfo_var, (&V_9));
		Lenses_t249 * L_9 = &((&V_9)->___lenses_0);
		L_9->___separation_3 = (0.06f);
		Lenses_t249 * L_10 = &((&V_9)->___lenses_0);
		L_10->___offset_4 = (0.035f);
		Lenses_t249 * L_11 = &((&V_9)->___lenses_0);
		L_11->___screenDistance_5 = (0.042f);
		Lenses_t249 * L_12 = &((&V_9)->___lenses_0);
		L_12->___alignment_6 = 1;
		MaxFOV_t250 * L_13 = &((&V_9)->___maxFOV_1);
		L_13->___outer_0 = (40.0f);
		MaxFOV_t250 * L_14 = &((&V_9)->___maxFOV_1);
		L_14->___inner_1 = (40.0f);
		MaxFOV_t250 * L_15 = &((&V_9)->___maxFOV_1);
		L_15->___upper_2 = (40.0f);
		MaxFOV_t250 * L_16 = &((&V_9)->___maxFOV_1);
		L_16->___lower_3 = (40.0f);
		Distortion_t251 * L_17 = &((&V_9)->___distortion_2);
		L_17->___k1_0 = (0.441f);
		Distortion_t251 * L_18 = &((&V_9)->___distortion_2);
		L_18->___k2_1 = (0.156f);
		Distortion_t251  L_19 = CardboardProfile_ApproximateInverse_m1567(NULL /*static, unused*/, (0.441f), (0.156f), (1.0f), ((int32_t)10), /*hidden argument*/NULL);
		(&V_9)->___inverse_3 = L_19;
		Device_t252  L_20 = V_9;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___CardboardJun2014_11 = L_20;
		Initobj (Device_t252_il2cpp_TypeInfo_var, (&V_10));
		Lenses_t249 * L_21 = &((&V_10)->___lenses_0);
		L_21->___separation_3 = (0.064f);
		Lenses_t249 * L_22 = &((&V_10)->___lenses_0);
		L_22->___offset_4 = (0.035f);
		Lenses_t249 * L_23 = &((&V_10)->___lenses_0);
		L_23->___screenDistance_5 = (0.039f);
		Lenses_t249 * L_24 = &((&V_10)->___lenses_0);
		L_24->___alignment_6 = 1;
		MaxFOV_t250 * L_25 = &((&V_10)->___maxFOV_1);
		L_25->___outer_0 = (60.0f);
		MaxFOV_t250 * L_26 = &((&V_10)->___maxFOV_1);
		L_26->___inner_1 = (60.0f);
		MaxFOV_t250 * L_27 = &((&V_10)->___maxFOV_1);
		L_27->___upper_2 = (60.0f);
		MaxFOV_t250 * L_28 = &((&V_10)->___maxFOV_1);
		L_28->___lower_3 = (60.0f);
		Distortion_t251 * L_29 = &((&V_10)->___distortion_2);
		L_29->___k1_0 = (0.34f);
		Distortion_t251 * L_30 = &((&V_10)->___distortion_2);
		L_30->___k2_1 = (0.55f);
		Distortion_t251  L_31 = CardboardProfile_ApproximateInverse_m1567(NULL /*static, unused*/, (0.34f), (0.55f), (1.0f), ((int32_t)10), /*hidden argument*/NULL);
		(&V_10)->___inverse_3 = L_31;
		Device_t252  L_32 = V_10;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___CardboardMay2015_12 = L_32;
		Initobj (Device_t252_il2cpp_TypeInfo_var, (&V_11));
		Lenses_t249 * L_33 = &((&V_11)->___lenses_0);
		L_33->___separation_3 = (0.065f);
		Lenses_t249 * L_34 = &((&V_11)->___lenses_0);
		L_34->___offset_4 = (0.036f);
		Lenses_t249 * L_35 = &((&V_11)->___lenses_0);
		L_35->___screenDistance_5 = (0.058f);
		Lenses_t249 * L_36 = &((&V_11)->___lenses_0);
		L_36->___alignment_6 = 1;
		MaxFOV_t250 * L_37 = &((&V_11)->___maxFOV_1);
		L_37->___outer_0 = (50.0f);
		MaxFOV_t250 * L_38 = &((&V_11)->___maxFOV_1);
		L_38->___inner_1 = (50.0f);
		MaxFOV_t250 * L_39 = &((&V_11)->___maxFOV_1);
		L_39->___upper_2 = (50.0f);
		MaxFOV_t250 * L_40 = &((&V_11)->___maxFOV_1);
		L_40->___lower_3 = (50.0f);
		Distortion_t251 * L_41 = &((&V_11)->___distortion_2);
		L_41->___k1_0 = (0.3f);
		Distortion_t251  L_42 = CardboardProfile_ApproximateInverse_m1567(NULL /*static, unused*/, (0.3f), (0.0f), (1.0f), ((int32_t)10), /*hidden argument*/NULL);
		(&V_11)->___inverse_3 = L_42;
		Device_t252  L_43 = V_11;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___GoggleTechC1Glass_13 = L_43;
		CardboardProfile_t255 * L_44 = (CardboardProfile_t255 *)il2cpp_codegen_object_new (CardboardProfile_t255_il2cpp_TypeInfo_var);
		CardboardProfile__ctor_m1557(L_44, /*hidden argument*/NULL);
		V_12 = L_44;
		CardboardProfile_t255 * L_45 = V_12;
		Screen_t248  L_46 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Nexus5_2;
		NullCheck(L_45);
		L_45->___screen_0 = L_46;
		CardboardProfile_t255 * L_47 = V_12;
		Device_t252  L_48 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___CardboardJun2014_11;
		NullCheck(L_47);
		L_47->___device_1 = L_48;
		CardboardProfile_t255 * L_49 = V_12;
		((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Default_14 = L_49;
		return;
	}
}
// CardboardProfile CardboardProfile::Clone()
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern "C" CardboardProfile_t255 * CardboardProfile_Clone_m1559 (CardboardProfile_t255 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	CardboardProfile_t255 * V_0 = {0};
	{
		CardboardProfile_t255 * L_0 = (CardboardProfile_t255 *)il2cpp_codegen_object_new (CardboardProfile_t255_il2cpp_TypeInfo_var);
		CardboardProfile__ctor_m1557(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CardboardProfile_t255 * L_1 = V_0;
		Screen_t248  L_2 = (__this->___screen_0);
		NullCheck(L_1);
		L_1->___screen_0 = L_2;
		CardboardProfile_t255 * L_3 = V_0;
		Device_t252  L_4 = (__this->___device_1);
		NullCheck(L_3);
		L_3->___device_1 = L_4;
		CardboardProfile_t255 * L_5 = V_0;
		return L_5;
	}
}
// System.Single CardboardProfile::get_VerticalLensOffset()
extern "C" float CardboardProfile_get_VerticalLensOffset_m1560 (CardboardProfile_t255 * __this, const MethodInfo* method)
{
	{
		Device_t252 * L_0 = &(__this->___device_1);
		Lenses_t249 * L_1 = &(L_0->___lenses_0);
		float L_2 = (L_1->___offset_4);
		Screen_t248 * L_3 = &(__this->___screen_0);
		float L_4 = (L_3->___border_2);
		Screen_t248 * L_5 = &(__this->___screen_0);
		float L_6 = (L_5->___height_1);
		Device_t252 * L_7 = &(__this->___device_1);
		Lenses_t249 * L_8 = &(L_7->___lenses_0);
		int32_t L_9 = (L_8->___alignment_6);
		return ((float)((float)((float)((float)((float)((float)L_2-(float)L_4))-(float)((float)((float)L_6/(float)(2.0f)))))*(float)(((float)L_9))));
	}
}
// CardboardProfile CardboardProfile::GetKnownProfile(CardboardProfile/ScreenSizes,CardboardProfile/DeviceTypes)
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern "C" CardboardProfile_t255 * CardboardProfile_GetKnownProfile_m1561 (Object_t * __this /* static, unused */, int32_t ___screenSize, int32_t ___deviceType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	Screen_t248  V_0 = {0};
	Device_t252  V_1 = {0};
	int32_t V_2 = {0};
	int32_t V_3 = {0};
	CardboardProfile_t255 * V_4 = {0};
	{
		int32_t L_0 = ___screenSize;
		V_2 = L_0;
		int32_t L_1 = V_2;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_002f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_003a;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_0045;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_0050;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_005b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_0066;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_0071;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0087;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_2 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Nexus6_3;
		V_0 = L_2;
		goto IL_0092;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_3 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___GalaxyS6_4;
		V_0 = L_3;
		goto IL_0092;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_4 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___GalaxyNote4_5;
		V_0 = L_4;
		goto IL_0092;
	}

IL_0050:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_5 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___LGG3_6;
		V_0 = L_5;
		goto IL_0092;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_6 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone4_7;
		V_0 = L_6;
		goto IL_0092;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_7 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone5_8;
		V_0 = L_7;
		goto IL_0092;
	}

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_8 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone6_9;
		V_0 = L_8;
		goto IL_0092;
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_9 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___iPhone6p_10;
		V_0 = L_9;
		goto IL_0092;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Screen_t248  L_10 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Nexus5_2;
		V_0 = L_10;
		goto IL_0092;
	}

IL_0092:
	{
		int32_t L_11 = ___deviceType;
		V_3 = L_11;
		int32_t L_12 = V_3;
		if ((((int32_t)L_12) == ((int32_t)1)))
		{
			goto IL_00a7;
		}
	}
	{
		int32_t L_13 = V_3;
		if ((((int32_t)L_13) == ((int32_t)2)))
		{
			goto IL_00b2;
		}
	}
	{
		goto IL_00bd;
	}

IL_00a7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Device_t252  L_14 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___CardboardMay2015_12;
		V_1 = L_14;
		goto IL_00c8;
	}

IL_00b2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Device_t252  L_15 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___GoggleTechC1Glass_13;
		V_1 = L_15;
		goto IL_00c8;
	}

IL_00bd:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Device_t252  L_16 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___CardboardJun2014_11;
		V_1 = L_16;
		goto IL_00c8;
	}

IL_00c8:
	{
		CardboardProfile_t255 * L_17 = (CardboardProfile_t255 *)il2cpp_codegen_object_new (CardboardProfile_t255_il2cpp_TypeInfo_var);
		CardboardProfile__ctor_m1557(L_17, /*hidden argument*/NULL);
		V_4 = L_17;
		CardboardProfile_t255 * L_18 = V_4;
		Screen_t248  L_19 = V_0;
		NullCheck(L_18);
		L_18->___screen_0 = L_19;
		CardboardProfile_t255 * L_20 = V_4;
		Device_t252  L_21 = V_1;
		NullCheck(L_20);
		L_20->___device_1 = L_21;
		CardboardProfile_t255 * L_22 = V_4;
		return L_22;
	}
}
// System.Void CardboardProfile::GetLeftEyeVisibleTanAngles(System.Single[])
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void CardboardProfile_GetLeftEyeVisibleTanAngles_m1562 (CardboardProfile_t255 * __this, SingleU5BU5D_t72* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	{
		Device_t252 * L_0 = &(__this->___device_1);
		MaxFOV_t250 * L_1 = &(L_0->___maxFOV_1);
		float L_2 = (L_1->___outer_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_3 = tanf(((float)((float)((-L_2))*(float)(0.0174532924f))));
		V_0 = L_3;
		Device_t252 * L_4 = &(__this->___device_1);
		MaxFOV_t250 * L_5 = &(L_4->___maxFOV_1);
		float L_6 = (L_5->___upper_2);
		float L_7 = tanf(((float)((float)L_6*(float)(0.0174532924f))));
		V_1 = L_7;
		Device_t252 * L_8 = &(__this->___device_1);
		MaxFOV_t250 * L_9 = &(L_8->___maxFOV_1);
		float L_10 = (L_9->___inner_1);
		float L_11 = tanf(((float)((float)L_10*(float)(0.0174532924f))));
		V_2 = L_11;
		Device_t252 * L_12 = &(__this->___device_1);
		MaxFOV_t250 * L_13 = &(L_12->___maxFOV_1);
		float L_14 = (L_13->___lower_3);
		float L_15 = tanf(((float)((float)((-L_14))*(float)(0.0174532924f))));
		V_3 = L_15;
		Screen_t248 * L_16 = &(__this->___screen_0);
		float L_17 = (L_16->___width_0);
		V_4 = ((float)((float)L_17/(float)(4.0f)));
		Screen_t248 * L_18 = &(__this->___screen_0);
		float L_19 = (L_18->___height_1);
		V_5 = ((float)((float)L_19/(float)(2.0f)));
		Device_t252 * L_20 = &(__this->___device_1);
		Lenses_t249 * L_21 = &(L_20->___lenses_0);
		float L_22 = (L_21->___separation_3);
		float L_23 = V_4;
		V_6 = ((float)((float)((float)((float)L_22/(float)(2.0f)))-(float)L_23));
		float L_24 = CardboardProfile_get_VerticalLensOffset_m1560(__this, /*hidden argument*/NULL);
		V_7 = ((-L_24));
		Device_t252 * L_25 = &(__this->___device_1);
		Lenses_t249 * L_26 = &(L_25->___lenses_0);
		float L_27 = (L_26->___screenDistance_5);
		V_8 = L_27;
		Device_t252 * L_28 = &(__this->___device_1);
		Distortion_t251 * L_29 = &(L_28->___distortion_2);
		float L_30 = V_6;
		float L_31 = V_4;
		float L_32 = V_8;
		float L_33 = Distortion_distort_m1555(L_29, ((float)((float)((float)((float)L_30-(float)L_31))/(float)L_32)), /*hidden argument*/NULL);
		V_9 = L_33;
		Device_t252 * L_34 = &(__this->___device_1);
		Distortion_t251 * L_35 = &(L_34->___distortion_2);
		float L_36 = V_7;
		float L_37 = V_5;
		float L_38 = V_8;
		float L_39 = Distortion_distort_m1555(L_35, ((float)((float)((float)((float)L_36+(float)L_37))/(float)L_38)), /*hidden argument*/NULL);
		V_10 = L_39;
		Device_t252 * L_40 = &(__this->___device_1);
		Distortion_t251 * L_41 = &(L_40->___distortion_2);
		float L_42 = V_6;
		float L_43 = V_4;
		float L_44 = V_8;
		float L_45 = Distortion_distort_m1555(L_41, ((float)((float)((float)((float)L_42+(float)L_43))/(float)L_44)), /*hidden argument*/NULL);
		V_11 = L_45;
		Device_t252 * L_46 = &(__this->___device_1);
		Distortion_t251 * L_47 = &(L_46->___distortion_2);
		float L_48 = V_7;
		float L_49 = V_5;
		float L_50 = V_8;
		float L_51 = Distortion_distort_m1555(L_47, ((float)((float)((float)((float)L_48-(float)L_49))/(float)L_50)), /*hidden argument*/NULL);
		V_12 = L_51;
		SingleU5BU5D_t72* L_52 = ___result;
		float L_53 = V_0;
		float L_54 = V_9;
		float L_55 = Math_Max_m2935(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 0);
		*((float*)(float*)SZArrayLdElema(L_52, 0)) = (float)L_55;
		SingleU5BU5D_t72* L_56 = ___result;
		float L_57 = V_1;
		float L_58 = V_10;
		float L_59 = Math_Min_m2936(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		*((float*)(float*)SZArrayLdElema(L_56, 1)) = (float)L_59;
		SingleU5BU5D_t72* L_60 = ___result;
		float L_61 = V_2;
		float L_62 = V_11;
		float L_63 = Math_Min_m2936(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 2);
		*((float*)(float*)SZArrayLdElema(L_60, 2)) = (float)L_63;
		SingleU5BU5D_t72* L_64 = ___result;
		float L_65 = V_3;
		float L_66 = V_12;
		float L_67 = Math_Max_m2935(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 3);
		*((float*)(float*)SZArrayLdElema(L_64, 3)) = (float)L_67;
		return;
	}
}
// System.Void CardboardProfile::GetLeftEyeNoLensTanAngles(System.Single[])
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void CardboardProfile_GetLeftEyeNoLensTanAngles_m1563 (CardboardProfile_t255 * __this, SingleU5BU5D_t72* ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	{
		Device_t252 * L_0 = &(__this->___device_1);
		Distortion_t251 * L_1 = &(L_0->___distortion_2);
		Device_t252 * L_2 = &(__this->___device_1);
		MaxFOV_t250 * L_3 = &(L_2->___maxFOV_1);
		float L_4 = (L_3->___outer_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_5 = tanf(((float)((float)((-L_4))*(float)(0.0174532924f))));
		float L_6 = Distortion_distortInv_m1556(L_1, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Device_t252 * L_7 = &(__this->___device_1);
		Distortion_t251 * L_8 = &(L_7->___distortion_2);
		Device_t252 * L_9 = &(__this->___device_1);
		MaxFOV_t250 * L_10 = &(L_9->___maxFOV_1);
		float L_11 = (L_10->___upper_2);
		float L_12 = tanf(((float)((float)L_11*(float)(0.0174532924f))));
		float L_13 = Distortion_distortInv_m1556(L_8, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Device_t252 * L_14 = &(__this->___device_1);
		Distortion_t251 * L_15 = &(L_14->___distortion_2);
		Device_t252 * L_16 = &(__this->___device_1);
		MaxFOV_t250 * L_17 = &(L_16->___maxFOV_1);
		float L_18 = (L_17->___inner_1);
		float L_19 = tanf(((float)((float)L_18*(float)(0.0174532924f))));
		float L_20 = Distortion_distortInv_m1556(L_15, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		Device_t252 * L_21 = &(__this->___device_1);
		Distortion_t251 * L_22 = &(L_21->___distortion_2);
		Device_t252 * L_23 = &(__this->___device_1);
		MaxFOV_t250 * L_24 = &(L_23->___maxFOV_1);
		float L_25 = (L_24->___lower_3);
		float L_26 = tanf(((float)((float)((-L_25))*(float)(0.0174532924f))));
		float L_27 = Distortion_distortInv_m1556(L_22, L_26, /*hidden argument*/NULL);
		V_3 = L_27;
		Screen_t248 * L_28 = &(__this->___screen_0);
		float L_29 = (L_28->___width_0);
		V_4 = ((float)((float)L_29/(float)(4.0f)));
		Screen_t248 * L_30 = &(__this->___screen_0);
		float L_31 = (L_30->___height_1);
		V_5 = ((float)((float)L_31/(float)(2.0f)));
		Device_t252 * L_32 = &(__this->___device_1);
		Lenses_t249 * L_33 = &(L_32->___lenses_0);
		float L_34 = (L_33->___separation_3);
		float L_35 = V_4;
		V_6 = ((float)((float)((float)((float)L_34/(float)(2.0f)))-(float)L_35));
		float L_36 = CardboardProfile_get_VerticalLensOffset_m1560(__this, /*hidden argument*/NULL);
		V_7 = ((-L_36));
		Device_t252 * L_37 = &(__this->___device_1);
		Lenses_t249 * L_38 = &(L_37->___lenses_0);
		float L_39 = (L_38->___screenDistance_5);
		V_8 = L_39;
		float L_40 = V_6;
		float L_41 = V_4;
		float L_42 = V_8;
		V_9 = ((float)((float)((float)((float)L_40-(float)L_41))/(float)L_42));
		float L_43 = V_7;
		float L_44 = V_5;
		float L_45 = V_8;
		V_10 = ((float)((float)((float)((float)L_43+(float)L_44))/(float)L_45));
		float L_46 = V_6;
		float L_47 = V_4;
		float L_48 = V_8;
		V_11 = ((float)((float)((float)((float)L_46+(float)L_47))/(float)L_48));
		float L_49 = V_7;
		float L_50 = V_5;
		float L_51 = V_8;
		V_12 = ((float)((float)((float)((float)L_49-(float)L_50))/(float)L_51));
		SingleU5BU5D_t72* L_52 = ___result;
		float L_53 = V_0;
		float L_54 = V_9;
		float L_55 = Math_Max_m2935(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 0);
		*((float*)(float*)SZArrayLdElema(L_52, 0)) = (float)L_55;
		SingleU5BU5D_t72* L_56 = ___result;
		float L_57 = V_1;
		float L_58 = V_10;
		float L_59 = Math_Min_m2936(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 1);
		*((float*)(float*)SZArrayLdElema(L_56, 1)) = (float)L_59;
		SingleU5BU5D_t72* L_60 = ___result;
		float L_61 = V_2;
		float L_62 = V_11;
		float L_63 = Math_Min_m2936(NULL /*static, unused*/, L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 2);
		*((float*)(float*)SZArrayLdElema(L_60, 2)) = (float)L_63;
		SingleU5BU5D_t72* L_64 = ___result;
		float L_65 = V_3;
		float L_66 = V_12;
		float L_67 = Math_Max_m2935(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 3);
		*((float*)(float*)SZArrayLdElema(L_64, 3)) = (float)L_67;
		return;
	}
}
// UnityEngine.Rect CardboardProfile::GetLeftEyeVisibleScreenRect(System.Single[])
extern "C" Rect_t225  CardboardProfile_GetLeftEyeVisibleScreenRect_m1564 (CardboardProfile_t255 * __this, SingleU5BU5D_t72* ___undistortedFrustum, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	{
		Device_t252 * L_0 = &(__this->___device_1);
		Lenses_t249 * L_1 = &(L_0->___lenses_0);
		float L_2 = (L_1->___screenDistance_5);
		V_0 = L_2;
		Screen_t248 * L_3 = &(__this->___screen_0);
		float L_4 = (L_3->___width_0);
		Device_t252 * L_5 = &(__this->___device_1);
		Lenses_t249 * L_6 = &(L_5->___lenses_0);
		float L_7 = (L_6->___separation_3);
		V_1 = ((float)((float)((float)((float)L_4-(float)L_7))/(float)(2.0f)));
		float L_8 = CardboardProfile_get_VerticalLensOffset_m1560(__this, /*hidden argument*/NULL);
		Screen_t248 * L_9 = &(__this->___screen_0);
		float L_10 = (L_9->___height_1);
		V_2 = ((float)((float)L_8+(float)((float)((float)L_10/(float)(2.0f)))));
		SingleU5BU5D_t72* L_11 = ___undistortedFrustum;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		int32_t L_12 = 0;
		float L_13 = V_0;
		float L_14 = V_1;
		Screen_t248 * L_15 = &(__this->___screen_0);
		float L_16 = (L_15->___width_0);
		V_3 = ((float)((float)((float)((float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_11, L_12))*(float)L_13))+(float)L_14))/(float)L_16));
		SingleU5BU5D_t72* L_17 = ___undistortedFrustum;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		int32_t L_18 = 1;
		float L_19 = V_0;
		float L_20 = V_2;
		Screen_t248 * L_21 = &(__this->___screen_0);
		float L_22 = (L_21->___height_1);
		V_4 = ((float)((float)((float)((float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_17, L_18))*(float)L_19))+(float)L_20))/(float)L_22));
		SingleU5BU5D_t72* L_23 = ___undistortedFrustum;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		int32_t L_24 = 2;
		float L_25 = V_0;
		float L_26 = V_1;
		Screen_t248 * L_27 = &(__this->___screen_0);
		float L_28 = (L_27->___width_0);
		V_5 = ((float)((float)((float)((float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_23, L_24))*(float)L_25))+(float)L_26))/(float)L_28));
		SingleU5BU5D_t72* L_29 = ___undistortedFrustum;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
		int32_t L_30 = 3;
		float L_31 = V_0;
		float L_32 = V_2;
		Screen_t248 * L_33 = &(__this->___screen_0);
		float L_34 = (L_33->___height_1);
		V_6 = ((float)((float)((float)((float)((float)((float)(*(float*)(float*)SZArrayLdElema(L_29, L_30))*(float)L_31))+(float)L_32))/(float)L_34));
		float L_35 = V_3;
		float L_36 = V_6;
		float L_37 = V_5;
		float L_38 = V_3;
		float L_39 = V_4;
		float L_40 = V_6;
		Rect_t225  L_41 = {0};
		Rect__ctor_m2802(&L_41, L_35, L_36, ((float)((float)L_37-(float)L_38)), ((float)((float)L_39-(float)L_40)), /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Single CardboardProfile::GetMaxRadius(System.Single[])
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" float CardboardProfile_GetMaxRadius_m1565 (Object_t * __this /* static, unused */, SingleU5BU5D_t72* ___tanAngleRect, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		SingleU5BU5D_t72* L_0 = ___tanAngleRect;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_2 = fabsf((*(float*)(float*)SZArrayLdElema(L_0, L_1)));
		SingleU5BU5D_t72* L_3 = ___tanAngleRect;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 2);
		int32_t L_4 = 2;
		float L_5 = fabsf((*(float*)(float*)SZArrayLdElema(L_3, L_4)));
		float L_6 = Mathf_Max_m2937(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		SingleU5BU5D_t72* L_7 = ___tanAngleRect;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 1);
		int32_t L_8 = 1;
		float L_9 = fabsf((*(float*)(float*)SZArrayLdElema(L_7, L_8)));
		SingleU5BU5D_t72* L_10 = ___tanAngleRect;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		int32_t L_11 = 3;
		float L_12 = fabsf((*(float*)(float*)SZArrayLdElema(L_10, L_11)));
		float L_13 = Mathf_Max_m2937(NULL /*static, unused*/, L_9, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = V_0;
		float L_15 = V_0;
		float L_16 = V_1;
		float L_17 = V_1;
		float L_18 = sqrtf(((float)((float)((float)((float)L_14*(float)L_15))+(float)((float)((float)L_16*(float)L_17)))));
		return L_18;
	}
}
// System.Double[] CardboardProfile::solveLeastSquares(System.Double[,],System.Double[])
extern TypeInfo* DoubleU5BU2CU5D_t467_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t466_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral326;
extern Il2CppCodeGenString* _stringLiteral327;
extern "C" DoubleU5BU5D_t466* CardboardProfile_solveLeastSquares_m1566 (Object_t * __this /* static, unused */, DoubleU5BU2CU5D_t467* ___matA, DoubleU5BU5D_t466* ___vecY, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DoubleU5BU2CU5D_t467_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		DoubleU5BU5D_t466_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(201);
		_stringLiteral326 = il2cpp_codegen_string_literal_from_index(326);
		_stringLiteral327 = il2cpp_codegen_string_literal_from_index(327);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DoubleU5BU2CU5D_t467* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	double V_5 = 0.0;
	int32_t V_6 = 0;
	DoubleU5BU2CU5D_t467* V_7 = {0};
	double V_8 = 0.0;
	DoubleU5BU5D_t466* V_9 = {0};
	int32_t V_10 = 0;
	double V_11 = 0.0;
	int32_t V_12 = 0;
	DoubleU5BU5D_t466* V_13 = {0};
	int32_t V_14 = 0;
	double V_15 = 0.0;
	int32_t V_16 = 0;
	{
		DoubleU5BU2CU5D_t467* L_0 = ___matA;
		NullCheck(L_0);
		int32_t L_1 = Array_GetLength_m2938(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		DoubleU5BU2CU5D_t467* L_2 = ___matA;
		NullCheck(L_2);
		int32_t L_3 = Array_GetLength_m2938(L_2, 1, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_0;
		DoubleU5BU5D_t466* L_5 = ___vecY;
		NullCheck(L_5);
		if ((((int32_t)L_4) == ((int32_t)(((int32_t)(((Array_t *)L_5)->max_length))))))
		{
			goto IL_0025;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral326, /*hidden argument*/NULL);
		return (DoubleU5BU5D_t466*)NULL;
	}

IL_0025:
	{
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_0038;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral327, /*hidden argument*/NULL);
		return (DoubleU5BU5D_t466*)NULL;
	}

IL_0038:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_1;
		DoubleU5BU2CU5D_t467* L_9 = (DoubleU5BU2CU5D_t467*)GenArrayNew2(DoubleU5BU2CU5D_t467_il2cpp_TypeInfo_var, L_7, L_8);
		V_2 = L_9;
		V_3 = 0;
		goto IL_00a6;
	}

IL_0047:
	{
		V_4 = 0;
		goto IL_009a;
	}

IL_004f:
	{
		V_5 = (0.0);
		V_6 = 0;
		goto IL_0081;
	}

IL_0062:
	{
		double L_10 = V_5;
		DoubleU5BU2CU5D_t467* L_11 = ___matA;
		int32_t L_12 = V_6;
		int32_t L_13 = V_4;
		NullCheck(L_11);
		double L_14 = GenArrayGet2(L_11, L_12, L_13, double);;
		DoubleU5BU2CU5D_t467* L_15 = ___matA;
		int32_t L_16 = V_6;
		int32_t L_17 = V_3;
		NullCheck(L_15);
		double L_18 = GenArrayGet2(L_15, L_16, L_17, double);;
		V_5 = ((double)((double)L_10+(double)((double)((double)L_14*(double)L_18))));
		int32_t L_19 = V_6;
		V_6 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0081:
	{
		int32_t L_20 = V_6;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0062;
		}
	}
	{
		DoubleU5BU2CU5D_t467* L_22 = V_2;
		int32_t L_23 = V_4;
		int32_t L_24 = V_3;
		double L_25 = V_5;
		NullCheck(L_22);
		GenArraySet2(L_22, L_23, L_24, L_25, double);;
		int32_t L_26 = V_4;
		V_4 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_27 = V_4;
		int32_t L_28 = V_1;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00a6:
	{
		int32_t L_30 = V_3;
		int32_t L_31 = V_1;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_32 = V_1;
		int32_t L_33 = V_1;
		DoubleU5BU2CU5D_t467* L_34 = (DoubleU5BU2CU5D_t467*)GenArrayNew2(DoubleU5BU2CU5D_t467_il2cpp_TypeInfo_var, L_32, L_33);
		V_7 = L_34;
		DoubleU5BU2CU5D_t467* L_35 = V_2;
		NullCheck(L_35);
		double L_36 = GenArrayGet2(L_35, 0, 0, double);;
		DoubleU5BU2CU5D_t467* L_37 = V_2;
		NullCheck(L_37);
		double L_38 = GenArrayGet2(L_37, 1, 1, double);;
		DoubleU5BU2CU5D_t467* L_39 = V_2;
		NullCheck(L_39);
		double L_40 = GenArrayGet2(L_39, 0, 1, double);;
		DoubleU5BU2CU5D_t467* L_41 = V_2;
		NullCheck(L_41);
		double L_42 = GenArrayGet2(L_41, 1, 0, double);;
		V_8 = ((double)((double)((double)((double)L_36*(double)L_38))-(double)((double)((double)L_40*(double)L_42))));
		DoubleU5BU2CU5D_t467* L_43 = V_7;
		DoubleU5BU2CU5D_t467* L_44 = V_2;
		NullCheck(L_44);
		double L_45 = GenArrayGet2(L_44, 1, 1, double);;
		double L_46 = V_8;
		NullCheck(L_43);
		GenArraySet2(L_43, 0, 0, ((double)((double)L_45/(double)L_46)), double);;
		DoubleU5BU2CU5D_t467* L_47 = V_7;
		DoubleU5BU2CU5D_t467* L_48 = V_2;
		NullCheck(L_48);
		double L_49 = GenArrayGet2(L_48, 0, 0, double);;
		double L_50 = V_8;
		NullCheck(L_47);
		GenArraySet2(L_47, 1, 1, ((double)((double)L_49/(double)L_50)), double);;
		DoubleU5BU2CU5D_t467* L_51 = V_7;
		DoubleU5BU2CU5D_t467* L_52 = V_2;
		NullCheck(L_52);
		double L_53 = GenArrayGet2(L_52, 1, 0, double);;
		double L_54 = V_8;
		NullCheck(L_51);
		GenArraySet2(L_51, 0, 1, ((double)((double)((-L_53))/(double)L_54)), double);;
		DoubleU5BU2CU5D_t467* L_55 = V_7;
		DoubleU5BU2CU5D_t467* L_56 = V_2;
		NullCheck(L_56);
		double L_57 = GenArrayGet2(L_56, 0, 1, double);;
		double L_58 = V_8;
		NullCheck(L_55);
		GenArraySet2(L_55, 1, 0, ((double)((double)((-L_57))/(double)L_58)), double);;
		int32_t L_59 = V_1;
		V_9 = ((DoubleU5BU5D_t466*)SZArrayNew(DoubleU5BU5D_t466_il2cpp_TypeInfo_var, L_59));
		V_10 = 0;
		goto IL_017f;
	}

IL_013d:
	{
		V_11 = (0.0);
		V_12 = 0;
		goto IL_016a;
	}

IL_0150:
	{
		double L_60 = V_11;
		DoubleU5BU2CU5D_t467* L_61 = ___matA;
		int32_t L_62 = V_12;
		int32_t L_63 = V_10;
		NullCheck(L_61);
		double L_64 = GenArrayGet2(L_61, L_62, L_63, double);;
		DoubleU5BU5D_t466* L_65 = ___vecY;
		int32_t L_66 = V_12;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, L_66);
		int32_t L_67 = L_66;
		V_11 = ((double)((double)L_60+(double)((double)((double)L_64*(double)(*(double*)(double*)SZArrayLdElema(L_65, L_67))))));
		int32_t L_68 = V_12;
		V_12 = ((int32_t)((int32_t)L_68+(int32_t)1));
	}

IL_016a:
	{
		int32_t L_69 = V_12;
		int32_t L_70 = V_0;
		if ((((int32_t)L_69) < ((int32_t)L_70)))
		{
			goto IL_0150;
		}
	}
	{
		DoubleU5BU5D_t466* L_71 = V_9;
		int32_t L_72 = V_10;
		double L_73 = V_11;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
		*((double*)(double*)SZArrayLdElema(L_71, L_72)) = (double)L_73;
		int32_t L_74 = V_10;
		V_10 = ((int32_t)((int32_t)L_74+(int32_t)1));
	}

IL_017f:
	{
		int32_t L_75 = V_10;
		int32_t L_76 = V_1;
		if ((((int32_t)L_75) < ((int32_t)L_76)))
		{
			goto IL_013d;
		}
	}
	{
		int32_t L_77 = V_1;
		V_13 = ((DoubleU5BU5D_t466*)SZArrayNew(DoubleU5BU5D_t466_il2cpp_TypeInfo_var, L_77));
		V_14 = 0;
		goto IL_01db;
	}

IL_0197:
	{
		V_15 = (0.0);
		V_16 = 0;
		goto IL_01c6;
	}

IL_01aa:
	{
		double L_78 = V_15;
		DoubleU5BU2CU5D_t467* L_79 = V_7;
		int32_t L_80 = V_16;
		int32_t L_81 = V_14;
		NullCheck(L_79);
		double L_82 = GenArrayGet2(L_79, L_80, L_81, double);;
		DoubleU5BU5D_t466* L_83 = V_9;
		int32_t L_84 = V_16;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, L_84);
		int32_t L_85 = L_84;
		V_15 = ((double)((double)L_78+(double)((double)((double)L_82*(double)(*(double*)(double*)SZArrayLdElema(L_83, L_85))))));
		int32_t L_86 = V_16;
		V_16 = ((int32_t)((int32_t)L_86+(int32_t)1));
	}

IL_01c6:
	{
		int32_t L_87 = V_16;
		int32_t L_88 = V_1;
		if ((((int32_t)L_87) < ((int32_t)L_88)))
		{
			goto IL_01aa;
		}
	}
	{
		DoubleU5BU5D_t466* L_89 = V_13;
		int32_t L_90 = V_14;
		double L_91 = V_15;
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, L_90);
		*((double*)(double*)SZArrayLdElema(L_89, L_90)) = (double)L_91;
		int32_t L_92 = V_14;
		V_14 = ((int32_t)((int32_t)L_92+(int32_t)1));
	}

IL_01db:
	{
		int32_t L_93 = V_14;
		int32_t L_94 = V_1;
		if ((((int32_t)L_93) < ((int32_t)L_94)))
		{
			goto IL_0197;
		}
	}
	{
		DoubleU5BU5D_t466* L_95 = V_13;
		return L_95;
	}
}
// CardboardProfile/Distortion CardboardProfile::ApproximateInverse(System.Single,System.Single,System.Single,System.Int32)
extern TypeInfo* Distortion_t251_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern "C" Distortion_t251  CardboardProfile_ApproximateInverse_m1567 (Object_t * __this /* static, unused */, float ___k1, float ___k2, float ___maxRadius, int32_t ___numSamples, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Distortion_t251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(202);
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	Distortion_t251  V_0 = {0};
	{
		Initobj (Distortion_t251_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ___k1;
		(&V_0)->___k1_0 = L_0;
		float L_1 = ___k2;
		(&V_0)->___k2_1 = L_1;
		Distortion_t251  L_2 = V_0;
		float L_3 = ___maxRadius;
		int32_t L_4 = ___numSamples;
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Distortion_t251  L_5 = CardboardProfile_ApproximateInverse_m1568(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// CardboardProfile/Distortion CardboardProfile::ApproximateInverse(CardboardProfile/Distortion,System.Single,System.Int32)
extern TypeInfo* DoubleU5BU2CU5D_t467_il2cpp_TypeInfo_var;
extern TypeInfo* DoubleU5BU5D_t466_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern TypeInfo* Distortion_t251_il2cpp_TypeInfo_var;
extern "C" Distortion_t251  CardboardProfile_ApproximateInverse_m1568 (Object_t * __this /* static, unused */, Distortion_t251  ___distort, float ___maxRadius, int32_t ___numSamples, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DoubleU5BU2CU5D_t467_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(199);
		DoubleU5BU5D_t466_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(201);
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		Distortion_t251_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(202);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	DoubleU5BU2CU5D_t467* V_1 = {0};
	DoubleU5BU5D_t466* V_2 = {0};
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	double V_5 = 0.0;
	double V_6 = 0.0;
	int32_t V_7 = 0;
	DoubleU5BU5D_t466* V_8 = {0};
	Distortion_t251  V_9 = {0};
	{
		int32_t L_0 = ___numSamples;
		DoubleU5BU2CU5D_t467* L_1 = (DoubleU5BU2CU5D_t467*)GenArrayNew2(DoubleU5BU2CU5D_t467_il2cpp_TypeInfo_var, L_0, 2);
		V_1 = L_1;
		int32_t L_2 = ___numSamples;
		V_2 = ((DoubleU5BU5D_t466*)SZArrayNew(DoubleU5BU5D_t466_il2cpp_TypeInfo_var, L_2));
		V_3 = 0;
		goto IL_0069;
	}

IL_0016:
	{
		float L_3 = ___maxRadius;
		int32_t L_4 = V_3;
		int32_t L_5 = ___numSamples;
		V_4 = ((float)((float)((float)((float)L_3*(float)(((float)((int32_t)((int32_t)L_4+(int32_t)1))))))/(float)(((float)L_5))));
		float L_6 = V_4;
		float L_7 = Distortion_distort_m1555((&___distort), L_6, /*hidden argument*/NULL);
		V_5 = (((double)L_7));
		double L_8 = V_5;
		V_6 = L_8;
		V_7 = 0;
		goto IL_0054;
	}

IL_0039:
	{
		double L_9 = V_6;
		double L_10 = V_5;
		double L_11 = V_5;
		V_6 = ((double)((double)L_9*(double)((double)((double)L_10*(double)L_11))));
		DoubleU5BU2CU5D_t467* L_12 = V_1;
		int32_t L_13 = V_3;
		int32_t L_14 = V_7;
		double L_15 = V_6;
		NullCheck(L_12);
		GenArraySet2(L_12, L_13, L_14, L_15, double);;
		int32_t L_16 = V_7;
		V_7 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_7;
		if ((((int32_t)L_17) < ((int32_t)2)))
		{
			goto IL_0039;
		}
	}
	{
		DoubleU5BU5D_t466* L_18 = V_2;
		int32_t L_19 = V_3;
		float L_20 = V_4;
		double L_21 = V_5;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		*((double*)(double*)SZArrayLdElema(L_18, L_19)) = (double)((double)((double)(((double)L_20))-(double)L_21));
		int32_t L_22 = V_3;
		V_3 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0069:
	{
		int32_t L_23 = V_3;
		int32_t L_24 = ___numSamples;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0016;
		}
	}
	{
		DoubleU5BU2CU5D_t467* L_25 = V_1;
		DoubleU5BU5D_t466* L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		DoubleU5BU5D_t466* L_27 = CardboardProfile_solveLeastSquares_m1566(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_8 = L_27;
		Initobj (Distortion_t251_il2cpp_TypeInfo_var, (&V_9));
		DoubleU5BU5D_t466* L_28 = V_8;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		int32_t L_29 = 0;
		(&V_9)->___k1_0 = (((float)(*(double*)(double*)SZArrayLdElema(L_28, L_29))));
		DoubleU5BU5D_t466* L_30 = V_8;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 1);
		int32_t L_31 = 1;
		(&V_9)->___k2_1 = (((float)(*(double*)(double*)SZArrayLdElema(L_30, L_31))));
		Distortion_t251  L_32 = V_9;
		return L_32;
	}
}
// GazeInputModule
#include "AssemblyU2DCSharp_GazeInputModule.h"
#ifndef _MSC_VER
#else
#endif
// GazeInputModule
#include "AssemblyU2DCSharp_GazeInputModuleMethodDeclarations.h"

// UnityEngine.EventSystems.BaseInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputModule.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
// UnityEngine.EventSystems.BaseEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventData.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_gen_0.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF.h"
// GazeInputManager
#include "AssemblyU2DCSharp_GazeInputManager.h"
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEvent.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_0.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_1.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_2.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_3.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_4.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_5.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_6.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_7.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_Mathf.h"
// UnityEngine.EventSystems.BaseInputModule
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputModuleMethodDeclarations.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystemMethodDeclarations.h"
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"
// UnityEngine.EventSystems.BaseEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventDataMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResultMethodDeclarations.h"
// UnityEngine.EventSystems.ExecuteEvents
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEventsMethodDeclarations.h"
// GazeInputManager
#include "AssemblyU2DCSharp_GazeInputManagerMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
struct ExecuteEvents_t488;
struct GameObject_t256;
// UnityEngine.EventSystems.ExecuteEvents
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents.h"
struct ExecuteEvents_t488;
struct GameObject_t256;
// Declaration UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C" GameObject_t256 * ExecuteEvents_GetEventHandler_TisObject_t_m2940_gshared (Object_t * __this /* static, unused */, GameObject_t256 * p0, const MethodInfo* method);
#define ExecuteEvents_GetEventHandler_TisObject_t_m2940(__this /* static, unused */, p0, method) (( GameObject_t256 * (*) (Object_t * /* static, unused */, GameObject_t256 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisObject_t_m2940_gshared)(__this /* static, unused */, p0, method)
// Declaration UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.ISelectHandler>(UnityEngine.GameObject)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.ISelectHandler>(UnityEngine.GameObject)
#define ExecuteEvents_GetEventHandler_TisISelectHandler_t487_m2939(__this /* static, unused */, p0, method) (( GameObject_t256 * (*) (Object_t * /* static, unused */, GameObject_t256 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisObject_t_m2940_gshared)(__this /* static, unused */, p0, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t491;
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t492;
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C" bool ExecuteEvents_Execute_TisObject_t_m2942_gshared (Object_t * __this /* static, unused */, GameObject_t256 * p0, BaseEventData_t490 * p1, EventFunction_1_t492 * p2, const MethodInfo* method);
#define ExecuteEvents_Execute_TisObject_t_m2942(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t492 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IUpdateSelectedHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IUpdateSelectedHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIUpdateSelectedHandler_t489_m2941(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t491 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t494;
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IBeginDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IBeginDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIBeginDragHandler_t493_m2943(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t494 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t496;
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IPointerUpHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IPointerUpHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t496 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t498;
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIDragHandler_t497_m2945(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t498 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t500;
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IPointerClickHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IPointerClickHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIPointerClickHandler_t499_m2946(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t500 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t502;
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t492;
// Declaration UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C" GameObject_t256 * ExecuteEvents_ExecuteHierarchy_TisObject_t_m2948_gshared (Object_t * __this /* static, unused */, GameObject_t256 * p0, BaseEventData_t490 * p1, EventFunction_1_t492 * p2, const MethodInfo* method);
#define ExecuteEvents_ExecuteHierarchy_TisObject_t_m2948(__this /* static, unused */, p0, p1, p2, method) (( GameObject_t256 * (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t492 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisObject_t_m2948_gshared)(__this /* static, unused */, p0, p1, p2, method)
// Declaration UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IDropHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IDropHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t501_m2947(__this /* static, unused */, p0, p1, p2, method) (( GameObject_t256 * (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t502 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisObject_t_m2948_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t504;
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IEndDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IEndDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIEndDragHandler_t503_m2949(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t504 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t506;
// Declaration UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerDownHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<UnityEngine.EventSystems.IPointerDownHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t505_m2950(__this /* static, unused */, p0, p1, p2, method) (( GameObject_t256 * (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t506 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisObject_t_m2948_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
// Declaration UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.IPointerClickHandler>(UnityEngine.GameObject)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.IPointerClickHandler>(UnityEngine.GameObject)
#define ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t499_m2951(__this /* static, unused */, p0, method) (( GameObject_t256 * (*) (Object_t * /* static, unused */, GameObject_t256 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisObject_t_m2940_gshared)(__this /* static, unused */, p0, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
// Declaration UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.IDragHandler>(UnityEngine.GameObject)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<UnityEngine.EventSystems.IDragHandler>(UnityEngine.GameObject)
#define ExecuteEvents_GetEventHandler_TisIDragHandler_t497_m2952(__this /* static, unused */, p0, method) (( GameObject_t256 * (*) (Object_t * /* static, unused */, GameObject_t256 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisObject_t_m2940_gshared)(__this /* static, unused */, p0, method)
struct ExecuteEvents_t488;
struct GameObject_t256;
struct BaseEventData_t490;
struct EventFunction_1_t508;
// Declaration System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IInitializePotentialDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<UnityEngine.EventSystems.IInitializePotentialDragHandler>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
#define ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t507_m2953(__this /* static, unused */, p0, p1, p2, method) (( bool (*) (Object_t * /* static, unused */, GameObject_t256 *, BaseEventData_t490 *, EventFunction_1_t508 *, const MethodInfo*))ExecuteEvents_Execute_TisObject_t_m2942_gshared)(__this /* static, unused */, p0, p1, p2, method)


// System.Void GazeInputModule::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void GazeInputModule__ctor_m1569 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___showCursor_8 = 1;
		__this->___scaleCursorSize_9 = 1;
		__this->___clickTime_10 = (0.1f);
		Vector2_t7  L_0 = {0};
		Vector2__ctor_m2714(&L_0, (0.5f), (0.5f), /*hidden argument*/NULL);
		__this->___hotspot_11 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___oldHitObject_17 = L_1;
		BaseInputModule__ctor_m2954(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean GazeInputModule::ShouldActivateModule()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" bool GazeInputModule_ShouldActivateModule_m1570 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		bool L_0 = BaseInputModule_ShouldActivateModule_m2955(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_1 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Cardboard_get_VRModeEnabled_m1442(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		bool L_3 = (__this->___vrModeOnly_6);
		G_B5_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0028;
	}

IL_0027:
	{
		G_B5_0 = 1;
	}

IL_0028:
	{
		return G_B5_0;
	}
}
// System.Void GazeInputModule::DeactivateModule()
extern "C" void GazeInputModule_DeactivateModule_m1571 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	{
		BaseInputModule_DeactivateModule_m2956(__this, /*hidden argument*/NULL);
		PointerEventData_t257 * L_0 = (__this->___pointerData_12);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		GazeInputModule_HandlePendingClick_m1579(__this, /*hidden argument*/NULL);
		PointerEventData_t257 * L_1 = (__this->___pointerData_12);
		BaseInputModule_HandlePointerExitAndEnter_m2957(__this, L_1, (GameObject_t256 *)NULL, /*hidden argument*/NULL);
		__this->___pointerData_12 = (PointerEventData_t257 *)NULL;
	}

IL_002b:
	{
		EventSystem_t509 * L_2 = BaseInputModule_get_eventSystem_m2958(__this, /*hidden argument*/NULL);
		BaseEventData_t490 * L_3 = (BaseEventData_t490 *)VirtFuncInvoker0< BaseEventData_t490 * >::Invoke(18 /* UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::GetBaseEventData() */, __this);
		NullCheck(L_2);
		EventSystem_SetSelectedGameObject_m2959(L_2, (GameObject_t256 *)NULL, L_3, /*hidden argument*/NULL);
		GameObject_t256 * L_4 = (__this->___cursor_7);
		bool L_5 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_4, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005a;
		}
	}
	{
		GameObject_t256 * L_6 = (__this->___cursor_7);
		NullCheck(L_6);
		GameObject_SetActive_m2960(L_6, 0, /*hidden argument*/NULL);
	}

IL_005a:
	{
		return;
	}
}
// System.Boolean GazeInputModule::IsPointerOverGameObject(System.Int32)
extern "C" bool GazeInputModule_IsPointerOverGameObject_m1572 (GazeInputModule_t258 * __this, int32_t ___pointerId, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		PointerEventData_t257 * L_0 = (__this->___pointerData_12);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		PointerEventData_t257 * L_1 = (__this->___pointerData_12);
		NullCheck(L_1);
		GameObject_t256 * L_2 = PointerEventData_get_pointerEnter_m2961(L_1, /*hidden argument*/NULL);
		bool L_3 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_2, (Object_t473 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void GazeInputModule::Process()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" void GazeInputModule_Process_m1573 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	{
		GazeInputModule_CastRayFromGaze_m1574(__this, /*hidden argument*/NULL);
		GazeInputModule_UpdateCurrentObject_m1575(__this, /*hidden argument*/NULL);
		GazeInputModule_PlaceCursor_m1577(__this, /*hidden argument*/NULL);
		GazeInputModule_GetObject_m1576(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Cardboard_get_TapIsTrigger_m1452(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButtonDown_m2842(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetMouseButton_m2962(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		GazeInputModule_HandleDrag_m1578(__this, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_0048:
	{
		float L_4 = Time_get_unscaledTime_m2963(NULL /*static, unused*/, /*hidden argument*/NULL);
		PointerEventData_t257 * L_5 = (__this->___pointerData_12);
		NullCheck(L_5);
		float L_6 = PointerEventData_get_clickTime_m2964(L_5, /*hidden argument*/NULL);
		float L_7 = (__this->___clickTime_10);
		if ((!(((float)((float)((float)L_4-(float)L_6))) < ((float)L_7))))
		{
			goto IL_0069;
		}
	}
	{
		goto IL_00cd;
	}

IL_0069:
	{
		PointerEventData_t257 * L_8 = (__this->___pointerData_12);
		NullCheck(L_8);
		bool L_9 = PointerEventData_get_eligibleForClick_m2965(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_00ad;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_10 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = Cardboard_get_Triggered_m1479(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_12 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_13 = Cardboard_get_TapIsTrigger_m1452(L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_00ad;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_14 = Input_GetMouseButtonDown_m2842(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00ad;
		}
	}

IL_00a2:
	{
		GazeInputModule_HandleTrigger_m1580(__this, /*hidden argument*/NULL);
		goto IL_00cd;
	}

IL_00ad:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_15 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_15);
		bool L_16 = Cardboard_get_Triggered_m1479(L_15, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00cd;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_17 = Input_GetMouseButton_m2962(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00cd;
		}
	}
	{
		GazeInputModule_HandlePendingClick_m1579(__this, /*hidden argument*/NULL);
	}

IL_00cd:
	{
		return;
	}
}
// System.Void GazeInputModule::CastRayFromGaze()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* PointerEventData_t257_il2cpp_TypeInfo_var;
extern "C" void GazeInputModule_CastRayFromGaze_m1574 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		PointerEventData_t257_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(203);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Pose3D_t260 * L_1 = Cardboard_get_HeadPose_m1471(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t261  L_2 = Pose3D_get_Orientation_m1588(L_1, /*hidden argument*/NULL);
		Vector3_t215  L_3 = Vector3_get_forward_m2966(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_4 = Quaternion_op_Multiply_m2900(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t7  L_5 = GazeInputModule_NormalizedCartesianToSpherical_m1581(__this, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		PointerEventData_t257 * L_6 = (__this->___pointerData_12);
		if (L_6)
		{
			goto IL_0043;
		}
	}
	{
		EventSystem_t509 * L_7 = BaseInputModule_get_eventSystem_m2958(__this, /*hidden argument*/NULL);
		PointerEventData_t257 * L_8 = (PointerEventData_t257 *)il2cpp_codegen_object_new (PointerEventData_t257_il2cpp_TypeInfo_var);
		PointerEventData__ctor_m2967(L_8, L_7, /*hidden argument*/NULL);
		__this->___pointerData_12 = L_8;
		Vector2_t7  L_9 = V_0;
		__this->___lastHeadPose_13 = L_9;
	}

IL_0043:
	{
		PointerEventData_t257 * L_10 = (__this->___pointerData_12);
		NullCheck(L_10);
		BaseEventData_Reset_m2968(L_10, /*hidden argument*/NULL);
		PointerEventData_t257 * L_11 = (__this->___pointerData_12);
		Vector2_t7 * L_12 = &(__this->___hotspot_11);
		float L_13 = (L_12->___x_1);
		int32_t L_14 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7 * L_15 = &(__this->___hotspot_11);
		float L_16 = (L_15->___y_2);
		int32_t L_17 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_18 = {0};
		Vector2__ctor_m2714(&L_18, ((float)((float)L_13*(float)(((float)L_14)))), ((float)((float)L_16*(float)(((float)L_17)))), /*hidden argument*/NULL);
		NullCheck(L_11);
		PointerEventData_set_position_m2969(L_11, L_18, /*hidden argument*/NULL);
		EventSystem_t509 * L_19 = BaseInputModule_get_eventSystem_m2958(__this, /*hidden argument*/NULL);
		PointerEventData_t257 * L_20 = (__this->___pointerData_12);
		List_1_t510 * L_21 = (((BaseInputModule_t259 *)__this)->___m_RaycastResultCache_2);
		NullCheck(L_19);
		EventSystem_RaycastAll_m2970(L_19, L_20, L_21, /*hidden argument*/NULL);
		PointerEventData_t257 * L_22 = (__this->___pointerData_12);
		List_1_t510 * L_23 = (((BaseInputModule_t259 *)__this)->___m_RaycastResultCache_2);
		RaycastResult_t511  L_24 = BaseInputModule_FindFirstRaycast_m2971(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		PointerEventData_set_pointerCurrentRaycast_m2972(L_22, L_24, /*hidden argument*/NULL);
		List_1_t510 * L_25 = (((BaseInputModule_t259 *)__this)->___m_RaycastResultCache_2);
		NullCheck(L_25);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear() */, L_25);
		PointerEventData_t257 * L_26 = (__this->___pointerData_12);
		Vector2_t7  L_27 = V_0;
		Vector2_t7  L_28 = (__this->___lastHeadPose_13);
		Vector2_t7  L_29 = Vector2_op_Subtraction_m2973(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		PointerEventData_set_delta_m2974(L_26, L_29, /*hidden argument*/NULL);
		Vector2_t7  L_30 = V_0;
		__this->___lastHeadPose_13 = L_30;
		return;
	}
}
// System.Void GazeInputModule::UpdateCurrentObject()
extern TypeInfo* ExecuteEvents_t488_il2cpp_TypeInfo_var;
extern const MethodInfo* ExecuteEvents_GetEventHandler_TisISelectHandler_t487_m2939_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIUpdateSelectedHandler_t489_m2941_MethodInfo_var;
extern "C" void GazeInputModule_UpdateCurrentObject_m1575 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteEvents_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(206);
		ExecuteEvents_GetEventHandler_TisISelectHandler_t487_m2939_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		ExecuteEvents_Execute_TisIUpdateSelectedHandler_t489_m2941_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t256 * V_0 = {0};
	GameObject_t256 * V_1 = {0};
	RaycastResult_t511  V_2 = {0};
	{
		PointerEventData_t257 * L_0 = (__this->___pointerData_12);
		NullCheck(L_0);
		RaycastResult_t511  L_1 = PointerEventData_get_pointerCurrentRaycast_m2975(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		GameObject_t256 * L_2 = RaycastResult_get_gameObject_m2976((&V_2), /*hidden argument*/NULL);
		V_0 = L_2;
		PointerEventData_t257 * L_3 = (__this->___pointerData_12);
		GameObject_t256 * L_4 = V_0;
		BaseInputModule_HandlePointerExitAndEnter_m2957(__this, L_3, L_4, /*hidden argument*/NULL);
		GameObject_t256 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		GameObject_t256 * L_6 = ExecuteEvents_GetEventHandler_TisISelectHandler_t487_m2939(NULL /*static, unused*/, L_5, /*hidden argument*/ExecuteEvents_GetEventHandler_TisISelectHandler_t487_m2939_MethodInfo_var);
		V_1 = L_6;
		GameObject_t256 * L_7 = V_1;
		EventSystem_t509 * L_8 = BaseInputModule_get_eventSystem_m2958(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_t256 * L_9 = EventSystem_get_currentSelectedGameObject_m2977(L_8, /*hidden argument*/NULL);
		bool L_10 = Object_op_Equality_m2716(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005f;
		}
	}
	{
		EventSystem_t509 * L_11 = BaseInputModule_get_eventSystem_m2958(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_t256 * L_12 = EventSystem_get_currentSelectedGameObject_m2977(L_11, /*hidden argument*/NULL);
		BaseEventData_t490 * L_13 = (BaseEventData_t490 *)VirtFuncInvoker0< BaseEventData_t490 * >::Invoke(18 /* UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::GetBaseEventData() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t491 * L_14 = ExecuteEvents_get_updateSelectedHandler_m2978(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIUpdateSelectedHandler_t489_m2941(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/ExecuteEvents_Execute_TisIUpdateSelectedHandler_t489_m2941_MethodInfo_var);
		goto IL_0071;
	}

IL_005f:
	{
		EventSystem_t509 * L_15 = BaseInputModule_get_eventSystem_m2958(__this, /*hidden argument*/NULL);
		PointerEventData_t257 * L_16 = (__this->___pointerData_12);
		NullCheck(L_15);
		EventSystem_SetSelectedGameObject_m2959(L_15, (GameObject_t256 *)NULL, L_16, /*hidden argument*/NULL);
	}

IL_0071:
	{
		return;
	}
}
// System.Void GazeInputModule::GetObject()
extern TypeInfo* GazeInputModule_t258_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GazeInputManager_t359_il2cpp_TypeInfo_var;
extern "C" void GazeInputModule_GetObject_m1576 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GazeInputModule_t258_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(207);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		GazeInputManager_t359_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(208);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t256 * V_0 = {0};
	RaycastResult_t511  V_1 = {0};
	{
		PointerEventData_t257 * L_0 = (__this->___pointerData_12);
		NullCheck(L_0);
		RaycastResult_t511  L_1 = PointerEventData_get_pointerCurrentRaycast_m2975(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		GameObject_t256 * L_2 = RaycastResult_get_gameObject_m2976((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t256 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_3, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_00df;
		}
	}
	{
		GameObject_t256 * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2979(L_5, /*hidden argument*/NULL);
		((GazeInputModule_t258_StaticFields*)GazeInputModule_t258_il2cpp_TypeInfo_var->static_fields)->___hitObject_14 = L_6;
		String_t* L_7 = (__this->___oldHitObject_17);
		String_t* L_8 = ((GazeInputModule_t258_StaticFields*)GazeInputModule_t258_il2cpp_TypeInfo_var->static_fields)->___hitObject_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m2980(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0073;
		}
	}
	{
		String_t* L_10 = ((GazeInputModule_t258_StaticFields*)GazeInputModule_t258_il2cpp_TypeInfo_var->static_fields)->___hitObject_14;
		__this->___oldHitObject_17 = L_10;
		__this->___isStarring_19 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(GazeInputManager_t359_il2cpp_TypeInfo_var);
		GazeInputManager_t359 * L_11 = GazeInputManager_get_Instance_m2090(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_12 = (__this->___oldHitObject_17);
		NullCheck(L_11);
		GazeInputManager_dispatchEvent_m2091(L_11, 1, L_12, /*hidden argument*/NULL);
		__this->___tStare_18 = (0.0f);
		goto IL_00df;
	}

IL_0073:
	{
		String_t* L_13 = (__this->___oldHitObject_17);
		String_t* L_14 = (__this->___oldHitObject_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_op_Equality_m2981(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00df;
		}
	}
	{
		bool L_16 = (__this->___isStarring_19);
		if (L_16)
		{
			goto IL_00ac;
		}
	}
	{
		float L_17 = (__this->___tStare_18);
		float L_18 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___tStare_18 = ((float)((float)L_17+(float)((float)((float)L_18*(float)(1.0f)))));
	}

IL_00ac:
	{
		float L_19 = (__this->___tStare_18);
		if ((!(((float)L_19) > ((float)(2.0f)))))
		{
			goto IL_00df;
		}
	}
	{
		__this->___isStarring_19 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(GazeInputManager_t359_il2cpp_TypeInfo_var);
		GazeInputManager_t359 * L_20 = GazeInputManager_get_Instance_m2090(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_21 = (__this->___oldHitObject_17);
		NullCheck(L_20);
		GazeInputManager_dispatchEvent_m2091(L_20, 2, L_21, /*hidden argument*/NULL);
		__this->___tStare_18 = (0.0f);
	}

IL_00df:
	{
		return;
	}
}
// System.Void GazeInputModule::PlaceCursor()
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void GazeInputModule_PlaceCursor_m1577 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t256 * V_0 = {0};
	Camera_t14 * V_1 = {0};
	RaycastHit_t482  V_2 = {0};
	float V_3 = 0.0f;
	RaycastResult_t511  V_4 = {0};
	GameObject_t256 * G_B5_0 = {0};
	GameObject_t256 * G_B3_0 = {0};
	GameObject_t256 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	GameObject_t256 * G_B6_1 = {0};
	{
		GameObject_t256 * L_0 = (__this->___cursor_7);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		PointerEventData_t257 * L_2 = (__this->___pointerData_12);
		NullCheck(L_2);
		RaycastResult_t511  L_3 = PointerEventData_get_pointerCurrentRaycast_m2975(L_2, /*hidden argument*/NULL);
		V_4 = L_3;
		GameObject_t256 * L_4 = RaycastResult_get_gameObject_m2976((&V_4), /*hidden argument*/NULL);
		V_0 = L_4;
		PointerEventData_t257 * L_5 = (__this->___pointerData_12);
		NullCheck(L_5);
		Camera_t14 * L_6 = PointerEventData_get_enterEventCamera_m2982(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t256 * L_7 = (__this->___cursor_7);
		GameObject_t256 * L_8 = V_0;
		bool L_9 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_8, (Object_t473 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = L_7;
		if (!L_9)
		{
			G_B5_0 = L_7;
			goto IL_0059;
		}
	}
	{
		Camera_t14 * L_10 = V_1;
		bool L_11 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_10, (Object_t473 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = G_B3_0;
		if (!L_11)
		{
			G_B5_0 = G_B3_0;
			goto IL_0059;
		}
	}
	{
		bool L_12 = (__this->___showCursor_8);
		G_B6_0 = ((int32_t)(L_12));
		G_B6_1 = G_B4_0;
		goto IL_005a;
	}

IL_0059:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_005a:
	{
		NullCheck(G_B6_1);
		GameObject_SetActive_m2960(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		GameObject_t256 * L_13 = (__this->___cursor_7);
		NullCheck(L_13);
		bool L_14 = GameObject_get_activeInHierarchy_m2779(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_018c;
		}
	}
	{
		Camera_t14 * L_15 = (__this->___CameraFacing_15);
		NullCheck(L_15);
		Transform_t243 * L_16 = Component_get_transform_m2760(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t215  L_17 = Transform_get_position_m2894(L_16, /*hidden argument*/NULL);
		Camera_t14 * L_18 = (__this->___CameraFacing_15);
		NullCheck(L_18);
		Transform_t243 * L_19 = Component_get_transform_m2760(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Quaternion_t261  L_20 = Transform_get_rotation_m2897(L_19, /*hidden argument*/NULL);
		Vector3_t215  L_21 = Vector3_get_forward_m2966(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_22 = Quaternion_op_Multiply_m2900(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Ray_t465  L_23 = {0};
		Ray__ctor_m2896(&L_23, L_17, L_22, /*hidden argument*/NULL);
		bool L_24 = Physics_Raycast_m2983(NULL /*static, unused*/, L_23, (&V_2), /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00b7;
		}
	}
	{
		float L_25 = RaycastHit_get_distance_m2795((&V_2), /*hidden argument*/NULL);
		V_3 = L_25;
		goto IL_00c9;
	}

IL_00b7:
	{
		Camera_t14 * L_26 = (__this->___CameraFacing_15);
		NullCheck(L_26);
		float L_27 = Camera_get_farClipPlane_m2860(L_26, /*hidden argument*/NULL);
		V_3 = ((float)((float)L_27*(float)(3.0f)));
	}

IL_00c9:
	{
		GameObject_t256 * L_28 = (__this->___cursor_7);
		NullCheck(L_28);
		Transform_t243 * L_29 = GameObject_get_transform_m2825(L_28, /*hidden argument*/NULL);
		Camera_t14 * L_30 = (__this->___CameraFacing_15);
		NullCheck(L_30);
		Transform_t243 * L_31 = Component_get_transform_m2760(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t215  L_32 = Transform_get_position_m2894(L_31, /*hidden argument*/NULL);
		Camera_t14 * L_33 = (__this->___CameraFacing_15);
		NullCheck(L_33);
		Transform_t243 * L_34 = Component_get_transform_m2760(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Quaternion_t261  L_35 = Transform_get_rotation_m2897(L_34, /*hidden argument*/NULL);
		Vector3_t215  L_36 = Vector3_get_forward_m2966(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_37 = Quaternion_op_Multiply_m2900(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Vector3_t215  L_38 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_32, L_37, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_position_m2902(L_29, L_38, /*hidden argument*/NULL);
		GameObject_t256 * L_39 = (__this->___cursor_7);
		NullCheck(L_39);
		Transform_t243 * L_40 = GameObject_get_transform_m2825(L_39, /*hidden argument*/NULL);
		Camera_t14 * L_41 = (__this->___CameraFacing_15);
		NullCheck(L_41);
		Transform_t243 * L_42 = Component_get_transform_m2760(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Vector3_t215  L_43 = Transform_get_position_m2894(L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_LookAt_m2984(L_40, L_43, /*hidden argument*/NULL);
		GameObject_t256 * L_44 = (__this->___cursor_7);
		NullCheck(L_44);
		Transform_t243 * L_45 = GameObject_get_transform_m2825(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_Rotate_m2985(L_45, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
		float L_46 = V_3;
		if ((!(((float)L_46) < ((float)(10.0f)))))
		{
			goto IL_0168;
		}
	}
	{
		float L_47 = V_3;
		float L_48 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_49 = expf(((-L_48)));
		V_3 = ((float)((float)L_47*(float)((float)((float)(1.0f)+(float)((float)((float)(5.0f)*(float)L_49))))));
	}

IL_0168:
	{
		GameObject_t256 * L_50 = (__this->___cursor_7);
		NullCheck(L_50);
		Transform_t243 * L_51 = GameObject_get_transform_m2825(L_50, /*hidden argument*/NULL);
		Vector3_t215  L_52 = {0};
		Vector3__ctor_m2812(&L_52, (0.2f), (0.2f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_51);
		Transform_set_localScale_m2813(L_51, L_52, /*hidden argument*/NULL);
	}

IL_018c:
	{
		return;
	}
}
// System.Void GazeInputModule::HandleDrag()
extern TypeInfo* ExecuteEvents_t488_il2cpp_TypeInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIBeginDragHandler_t493_m2943_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIDragHandler_t497_m2945_MethodInfo_var;
extern "C" void GazeInputModule_HandleDrag_m1578 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteEvents_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(206);
		ExecuteEvents_Execute_TisIBeginDragHandler_t493_m2943_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		ExecuteEvents_Execute_TisIDragHandler_t497_m2945_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		PointerEventData_t257 * L_0 = (__this->___pointerData_12);
		NullCheck(L_0);
		bool L_1 = PointerEventData_IsPointerMoving_m2986(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0060;
		}
	}
	{
		PointerEventData_t257 * L_3 = (__this->___pointerData_12);
		NullCheck(L_3);
		GameObject_t256 * L_4 = PointerEventData_get_pointerDrag_m2987(L_3, /*hidden argument*/NULL);
		bool L_5 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_4, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0060;
		}
	}
	{
		PointerEventData_t257 * L_6 = (__this->___pointerData_12);
		NullCheck(L_6);
		bool L_7 = PointerEventData_get_dragging_m2988(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0060;
		}
	}
	{
		PointerEventData_t257 * L_8 = (__this->___pointerData_12);
		NullCheck(L_8);
		GameObject_t256 * L_9 = PointerEventData_get_pointerDrag_m2987(L_8, /*hidden argument*/NULL);
		PointerEventData_t257 * L_10 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t494 * L_11 = ExecuteEvents_get_beginDragHandler_m2989(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIBeginDragHandler_t493_m2943(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/ExecuteEvents_Execute_TisIBeginDragHandler_t493_m2943_MethodInfo_var);
		PointerEventData_t257 * L_12 = (__this->___pointerData_12);
		NullCheck(L_12);
		PointerEventData_set_dragging_m2990(L_12, 1, /*hidden argument*/NULL);
	}

IL_0060:
	{
		PointerEventData_t257 * L_13 = (__this->___pointerData_12);
		NullCheck(L_13);
		bool L_14 = PointerEventData_get_dragging_m2988(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0108;
		}
	}
	{
		bool L_15 = V_0;
		if (!L_15)
		{
			goto IL_0108;
		}
	}
	{
		PointerEventData_t257 * L_16 = (__this->___pointerData_12);
		NullCheck(L_16);
		GameObject_t256 * L_17 = PointerEventData_get_pointerDrag_m2987(L_16, /*hidden argument*/NULL);
		bool L_18 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_17, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0108;
		}
	}
	{
		PointerEventData_t257 * L_19 = (__this->___pointerData_12);
		NullCheck(L_19);
		GameObject_t256 * L_20 = PointerEventData_get_pointerPress_m2991(L_19, /*hidden argument*/NULL);
		PointerEventData_t257 * L_21 = (__this->___pointerData_12);
		NullCheck(L_21);
		GameObject_t256 * L_22 = PointerEventData_get_pointerDrag_m2987(L_21, /*hidden argument*/NULL);
		bool L_23 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00ec;
		}
	}
	{
		PointerEventData_t257 * L_24 = (__this->___pointerData_12);
		NullCheck(L_24);
		GameObject_t256 * L_25 = PointerEventData_get_pointerPress_m2991(L_24, /*hidden argument*/NULL);
		PointerEventData_t257 * L_26 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t496 * L_27 = ExecuteEvents_get_pointerUpHandler_m2992(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944_MethodInfo_var);
		PointerEventData_t257 * L_28 = (__this->___pointerData_12);
		NullCheck(L_28);
		PointerEventData_set_eligibleForClick_m2993(L_28, 0, /*hidden argument*/NULL);
		PointerEventData_t257 * L_29 = (__this->___pointerData_12);
		NullCheck(L_29);
		PointerEventData_set_pointerPress_m2994(L_29, (GameObject_t256 *)NULL, /*hidden argument*/NULL);
		PointerEventData_t257 * L_30 = (__this->___pointerData_12);
		NullCheck(L_30);
		PointerEventData_set_rawPointerPress_m2995(L_30, (GameObject_t256 *)NULL, /*hidden argument*/NULL);
	}

IL_00ec:
	{
		PointerEventData_t257 * L_31 = (__this->___pointerData_12);
		NullCheck(L_31);
		GameObject_t256 * L_32 = PointerEventData_get_pointerDrag_m2987(L_31, /*hidden argument*/NULL);
		PointerEventData_t257 * L_33 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t498 * L_34 = ExecuteEvents_get_dragHandler_m2996(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIDragHandler_t497_m2945(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/ExecuteEvents_Execute_TisIDragHandler_t497_m2945_MethodInfo_var);
	}

IL_0108:
	{
		return;
	}
}
// System.Void GazeInputModule::HandlePendingClick()
extern TypeInfo* ExecuteEvents_t488_il2cpp_TypeInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIPointerClickHandler_t499_m2946_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t501_m2947_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIEndDragHandler_t503_m2949_MethodInfo_var;
extern "C" void GazeInputModule_HandlePendingClick_m1579 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteEvents_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(206);
		ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483671);
		ExecuteEvents_Execute_TisIPointerClickHandler_t499_m2946_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483673);
		ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t501_m2947_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483674);
		ExecuteEvents_Execute_TisIEndDragHandler_t503_m2949_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483675);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t256 * V_0 = {0};
	RaycastResult_t511  V_1 = {0};
	{
		PointerEventData_t257 * L_0 = (__this->___pointerData_12);
		NullCheck(L_0);
		bool L_1 = PointerEventData_get_eligibleForClick_m2965(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		PointerEventData_t257 * L_2 = (__this->___pointerData_12);
		NullCheck(L_2);
		RaycastResult_t511  L_3 = PointerEventData_get_pointerCurrentRaycast_m2975(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		GameObject_t256 * L_4 = RaycastResult_get_gameObject_m2976((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
		PointerEventData_t257 * L_5 = (__this->___pointerData_12);
		NullCheck(L_5);
		GameObject_t256 * L_6 = PointerEventData_get_pointerPress_m2991(L_5, /*hidden argument*/NULL);
		PointerEventData_t257 * L_7 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t496 * L_8 = ExecuteEvents_get_pointerUpHandler_m2992(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/ExecuteEvents_Execute_TisIPointerUpHandler_t495_m2944_MethodInfo_var);
		PointerEventData_t257 * L_9 = (__this->___pointerData_12);
		NullCheck(L_9);
		GameObject_t256 * L_10 = PointerEventData_get_pointerPress_m2991(L_9, /*hidden argument*/NULL);
		PointerEventData_t257 * L_11 = (__this->___pointerData_12);
		EventFunction_1_t500 * L_12 = ExecuteEvents_get_pointerClickHandler_m2997(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIPointerClickHandler_t499_m2946(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/ExecuteEvents_Execute_TisIPointerClickHandler_t499_m2946_MethodInfo_var);
		PointerEventData_t257 * L_13 = (__this->___pointerData_12);
		NullCheck(L_13);
		GameObject_t256 * L_14 = PointerEventData_get_pointerDrag_m2987(L_13, /*hidden argument*/NULL);
		bool L_15 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_14, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t256 * L_16 = V_0;
		PointerEventData_t257 * L_17 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t502 * L_18 = ExecuteEvents_get_dropHandler_m2998(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t501_m2947(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIDropHandler_t501_m2947_MethodInfo_var);
	}

IL_0085:
	{
		PointerEventData_t257 * L_19 = (__this->___pointerData_12);
		NullCheck(L_19);
		GameObject_t256 * L_20 = PointerEventData_get_pointerDrag_m2987(L_19, /*hidden argument*/NULL);
		bool L_21 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_20, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00c7;
		}
	}
	{
		PointerEventData_t257 * L_22 = (__this->___pointerData_12);
		NullCheck(L_22);
		bool L_23 = PointerEventData_get_dragging_m2988(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00c7;
		}
	}
	{
		PointerEventData_t257 * L_24 = (__this->___pointerData_12);
		NullCheck(L_24);
		GameObject_t256 * L_25 = PointerEventData_get_pointerDrag_m2987(L_24, /*hidden argument*/NULL);
		PointerEventData_t257 * L_26 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t504 * L_27 = ExecuteEvents_get_endDragHandler_m2999(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIEndDragHandler_t503_m2949(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/ExecuteEvents_Execute_TisIEndDragHandler_t503_m2949_MethodInfo_var);
	}

IL_00c7:
	{
		PointerEventData_t257 * L_28 = (__this->___pointerData_12);
		NullCheck(L_28);
		PointerEventData_set_pointerPress_m2994(L_28, (GameObject_t256 *)NULL, /*hidden argument*/NULL);
		PointerEventData_t257 * L_29 = (__this->___pointerData_12);
		NullCheck(L_29);
		PointerEventData_set_rawPointerPress_m2995(L_29, (GameObject_t256 *)NULL, /*hidden argument*/NULL);
		PointerEventData_t257 * L_30 = (__this->___pointerData_12);
		NullCheck(L_30);
		PointerEventData_set_eligibleForClick_m2993(L_30, 0, /*hidden argument*/NULL);
		PointerEventData_t257 * L_31 = (__this->___pointerData_12);
		NullCheck(L_31);
		PointerEventData_set_clickCount_m3000(L_31, 0, /*hidden argument*/NULL);
		PointerEventData_t257 * L_32 = (__this->___pointerData_12);
		NullCheck(L_32);
		PointerEventData_set_pointerDrag_m3001(L_32, (GameObject_t256 *)NULL, /*hidden argument*/NULL);
		PointerEventData_t257 * L_33 = (__this->___pointerData_12);
		NullCheck(L_33);
		PointerEventData_set_dragging_m2990(L_33, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GazeInputModule::HandleTrigger()
extern TypeInfo* ExecuteEvents_t488_il2cpp_TypeInfo_var;
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern const MethodInfo* ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t505_m2950_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t499_m2951_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_GetEventHandler_TisIDragHandler_t497_m2952_MethodInfo_var;
extern const MethodInfo* ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t507_m2953_MethodInfo_var;
extern "C" void GazeInputModule_HandleTrigger_m1580 (GazeInputModule_t258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteEvents_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(206);
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t505_m2950_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t499_m2951_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483677);
		ExecuteEvents_GetEventHandler_TisIDragHandler_t497_m2952_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483678);
		ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t507_m2953_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t256 * V_0 = {0};
	RaycastResult_t511  V_1 = {0};
	GameObject_t256 * G_B2_0 = {0};
	PointerEventData_t257 * G_B2_1 = {0};
	GameObject_t256 * G_B1_0 = {0};
	PointerEventData_t257 * G_B1_1 = {0};
	{
		PointerEventData_t257 * L_0 = (__this->___pointerData_12);
		NullCheck(L_0);
		RaycastResult_t511  L_1 = PointerEventData_get_pointerCurrentRaycast_m2975(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		GameObject_t256 * L_2 = RaycastResult_get_gameObject_m2976((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		PointerEventData_t257 * L_3 = (__this->___pointerData_12);
		PointerEventData_t257 * L_4 = (__this->___pointerData_12);
		NullCheck(L_4);
		Vector2_t7  L_5 = PointerEventData_get_position_m3002(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		PointerEventData_set_pressPosition_m3003(L_3, L_5, /*hidden argument*/NULL);
		PointerEventData_t257 * L_6 = (__this->___pointerData_12);
		PointerEventData_t257 * L_7 = (__this->___pointerData_12);
		NullCheck(L_7);
		RaycastResult_t511  L_8 = PointerEventData_get_pointerCurrentRaycast_m2975(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		PointerEventData_set_pointerPressRaycast_m3004(L_6, L_8, /*hidden argument*/NULL);
		PointerEventData_t257 * L_9 = (__this->___pointerData_12);
		GameObject_t256 * L_10 = V_0;
		PointerEventData_t257 * L_11 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t506 * L_12 = ExecuteEvents_get_pointerDownHandler_m3005(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t256 * L_13 = ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t505_m2950(NULL /*static, unused*/, L_10, L_11, L_12, /*hidden argument*/ExecuteEvents_ExecuteHierarchy_TisIPointerDownHandler_t505_m2950_MethodInfo_var);
		GameObject_t256 * L_14 = L_13;
		G_B1_0 = L_14;
		G_B1_1 = L_9;
		if (L_14)
		{
			G_B2_0 = L_14;
			G_B2_1 = L_9;
			goto IL_0064;
		}
	}
	{
		GameObject_t256 * L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		GameObject_t256 * L_16 = ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t499_m2951(NULL /*static, unused*/, L_15, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIPointerClickHandler_t499_m2951_MethodInfo_var);
		G_B2_0 = L_16;
		G_B2_1 = G_B1_1;
	}

IL_0064:
	{
		NullCheck(G_B2_1);
		PointerEventData_set_pointerPress_m2994(G_B2_1, G_B2_0, /*hidden argument*/NULL);
		PointerEventData_t257 * L_17 = (__this->___pointerData_12);
		GameObject_t256 * L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		GameObject_t256 * L_19 = ExecuteEvents_GetEventHandler_TisIDragHandler_t497_m2952(NULL /*static, unused*/, L_18, /*hidden argument*/ExecuteEvents_GetEventHandler_TisIDragHandler_t497_m2952_MethodInfo_var);
		NullCheck(L_17);
		PointerEventData_set_pointerDrag_m3001(L_17, L_19, /*hidden argument*/NULL);
		PointerEventData_t257 * L_20 = (__this->___pointerData_12);
		NullCheck(L_20);
		GameObject_t256 * L_21 = PointerEventData_get_pointerDrag_m2987(L_20, /*hidden argument*/NULL);
		bool L_22 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_21, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00bb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_23 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = Cardboard_get_TapIsTrigger_m1452(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00bb;
		}
	}
	{
		PointerEventData_t257 * L_25 = (__this->___pointerData_12);
		NullCheck(L_25);
		GameObject_t256 * L_26 = PointerEventData_get_pointerDrag_m2987(L_25, /*hidden argument*/NULL);
		PointerEventData_t257 * L_27 = (__this->___pointerData_12);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t488_il2cpp_TypeInfo_var);
		EventFunction_1_t508 * L_28 = ExecuteEvents_get_initializePotentialDrag_m3006(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t507_m2953(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/ExecuteEvents_Execute_TisIInitializePotentialDragHandler_t507_m2953_MethodInfo_var);
	}

IL_00bb:
	{
		PointerEventData_t257 * L_29 = (__this->___pointerData_12);
		GameObject_t256 * L_30 = V_0;
		NullCheck(L_29);
		PointerEventData_set_rawPointerPress_m2995(L_29, L_30, /*hidden argument*/NULL);
		PointerEventData_t257 * L_31 = (__this->___pointerData_12);
		NullCheck(L_31);
		PointerEventData_set_eligibleForClick_m2993(L_31, 1, /*hidden argument*/NULL);
		PointerEventData_t257 * L_32 = (__this->___pointerData_12);
		Vector2_t7  L_33 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_32);
		PointerEventData_set_delta_m2974(L_32, L_33, /*hidden argument*/NULL);
		PointerEventData_t257 * L_34 = (__this->___pointerData_12);
		NullCheck(L_34);
		PointerEventData_set_dragging_m2990(L_34, 0, /*hidden argument*/NULL);
		PointerEventData_t257 * L_35 = (__this->___pointerData_12);
		NullCheck(L_35);
		PointerEventData_set_useDragThreshold_m3007(L_35, 1, /*hidden argument*/NULL);
		PointerEventData_t257 * L_36 = (__this->___pointerData_12);
		NullCheck(L_36);
		PointerEventData_set_clickCount_m3000(L_36, 1, /*hidden argument*/NULL);
		PointerEventData_t257 * L_37 = (__this->___pointerData_12);
		float L_38 = Time_get_unscaledTime_m2963(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_37);
		PointerEventData_set_clickTime_m3008(L_37, L_38, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 GazeInputModule::NormalizedCartesianToSpherical(UnityEngine.Vector3)
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" Vector2_t7  GazeInputModule_NormalizedCartesianToSpherical_m1581 (GazeInputModule_t258 * __this, Vector3_t215  ___cartCoords, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Vector3_Normalize_m3009((&___cartCoords), /*hidden argument*/NULL);
		float L_0 = ((&___cartCoords)->___x_1);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_1 = ((Mathf_t474_StaticFields*)Mathf_t474_il2cpp_TypeInfo_var->static_fields)->___Epsilon_0;
		(&___cartCoords)->___x_1 = L_1;
	}

IL_0024:
	{
		float L_2 = ((&___cartCoords)->___z_3);
		float L_3 = ((&___cartCoords)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_4 = atanf(((float)((float)L_2/(float)L_3)));
		V_0 = L_4;
		float L_5 = ((&___cartCoords)->___x_1);
		if ((!(((float)L_5) < ((float)(0.0f)))))
		{
			goto IL_0052;
		}
	}
	{
		float L_6 = V_0;
		V_0 = ((float)((float)L_6+(float)(3.14159274f)));
	}

IL_0052:
	{
		float L_7 = ((&___cartCoords)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_8 = asinf(L_7);
		V_1 = L_8;
		float L_9 = V_0;
		float L_10 = V_1;
		Vector2_t7  L_11 = {0};
		Vector2__ctor_m2714(&L_11, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void Pose3D::.ctor()
extern "C" void Pose3D__ctor_m1582 (Pose3D_t260 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Vector3_t215  L_0 = Vector3_get_zero_m2826(NULL /*static, unused*/, /*hidden argument*/NULL);
		Pose3D_set_Position_m1587(__this, L_0, /*hidden argument*/NULL);
		Quaternion_t261  L_1 = Quaternion_get_identity_m2888(NULL /*static, unused*/, /*hidden argument*/NULL);
		Pose3D_set_Orientation_m1589(__this, L_1, /*hidden argument*/NULL);
		Matrix4x4_t242  L_2 = Matrix4x4_get_identity_m3010(NULL /*static, unused*/, /*hidden argument*/NULL);
		Pose3D_set_Matrix_m1591(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pose3D::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" void Pose3D__ctor_m1583 (Pose3D_t260 * __this, Vector3_t215  ___position, Quaternion_t261  ___orientation, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Vector3_t215  L_0 = ___position;
		Quaternion_t261  L_1 = ___orientation;
		Pose3D_Set_m1593(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pose3D::.ctor(UnityEngine.Matrix4x4)
extern "C" void Pose3D__ctor_m1584 (Pose3D_t260 * __this, Matrix4x4_t242  ___matrix, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Matrix4x4_t242  L_0 = ___matrix;
		Pose3D_Set_m1594(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pose3D::.cctor()
extern TypeInfo* Pose3D_t260_il2cpp_TypeInfo_var;
extern "C" void Pose3D__cctor_m1585 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Pose3D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(217);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t215  L_0 = {0};
		Vector3__ctor_m2812(&L_0, (1.0f), (1.0f), (-1.0f), /*hidden argument*/NULL);
		Matrix4x4_t242  L_1 = Matrix4x4_Scale_m3011(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((Pose3D_t260_StaticFields*)Pose3D_t260_il2cpp_TypeInfo_var->static_fields)->___flipZ_0 = L_1;
		return;
	}
}
// UnityEngine.Vector3 Pose3D::get_Position()
extern "C" Vector3_t215  Pose3D_get_Position_m1586 (Pose3D_t260 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___U3CPositionU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Pose3D::set_Position(UnityEngine.Vector3)
extern "C" void Pose3D_set_Position_m1587 (Pose3D_t260 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		__this->___U3CPositionU3Ek__BackingField_1 = L_0;
		return;
	}
}
// UnityEngine.Quaternion Pose3D::get_Orientation()
extern "C" Quaternion_t261  Pose3D_get_Orientation_m1588 (Pose3D_t260 * __this, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = (__this->___U3COrientationU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void Pose3D::set_Orientation(UnityEngine.Quaternion)
extern "C" void Pose3D_set_Orientation_m1589 (Pose3D_t260 * __this, Quaternion_t261  ___value, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = ___value;
		__this->___U3COrientationU3Ek__BackingField_2 = L_0;
		return;
	}
}
// UnityEngine.Matrix4x4 Pose3D::get_Matrix()
extern "C" Matrix4x4_t242  Pose3D_get_Matrix_m1590 (Pose3D_t260 * __this, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = (__this->___U3CMatrixU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void Pose3D::set_Matrix(UnityEngine.Matrix4x4)
extern "C" void Pose3D_set_Matrix_m1591 (Pose3D_t260 * __this, Matrix4x4_t242  ___value, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = ___value;
		__this->___U3CMatrixU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Matrix4x4 Pose3D::get_RightHandedMatrix()
extern TypeInfo* Pose3D_t260_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t242  Pose3D_get_RightHandedMatrix_m1592 (Pose3D_t260 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Pose3D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(217);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pose3D_t260_il2cpp_TypeInfo_var);
		Matrix4x4_t242  L_0 = ((Pose3D_t260_StaticFields*)Pose3D_t260_il2cpp_TypeInfo_var->static_fields)->___flipZ_0;
		Matrix4x4_t242  L_1 = Pose3D_get_Matrix_m1590(__this, /*hidden argument*/NULL);
		Matrix4x4_t242  L_2 = Matrix4x4_op_Multiply_m2876(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Matrix4x4_t242  L_3 = ((Pose3D_t260_StaticFields*)Pose3D_t260_il2cpp_TypeInfo_var->static_fields)->___flipZ_0;
		Matrix4x4_t242  L_4 = Matrix4x4_op_Multiply_m2876(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Pose3D::Set(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" void Pose3D_Set_m1593 (Pose3D_t260 * __this, Vector3_t215  ___position, Quaternion_t261  ___orientation, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___position;
		Pose3D_set_Position_m1587(__this, L_0, /*hidden argument*/NULL);
		Quaternion_t261  L_1 = ___orientation;
		Pose3D_set_Orientation_m1589(__this, L_1, /*hidden argument*/NULL);
		Vector3_t215  L_2 = ___position;
		Quaternion_t261  L_3 = ___orientation;
		Vector3_t215  L_4 = Vector3_get_one_m2890(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t242  L_5 = Matrix4x4_TRS_m2923(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		Pose3D_set_Matrix_m1591(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pose3D::Set(UnityEngine.Matrix4x4)
extern "C" void Pose3D_Set_m1594 (Pose3D_t260 * __this, Matrix4x4_t242  ___matrix, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = ___matrix;
		Pose3D_set_Matrix_m1591(__this, L_0, /*hidden argument*/NULL);
		Vector4_t5  L_1 = Matrix4x4_GetColumn_m3012((&___matrix), 3, /*hidden argument*/NULL);
		Vector3_t215  L_2 = Vector4_op_Implicit_m3013(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Pose3D_set_Position_m1587(__this, L_2, /*hidden argument*/NULL);
		Vector4_t5  L_3 = Matrix4x4_GetColumn_m3012((&___matrix), 2, /*hidden argument*/NULL);
		Vector3_t215  L_4 = Vector4_op_Implicit_m3013(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector4_t5  L_5 = Matrix4x4_GetColumn_m3012((&___matrix), 1, /*hidden argument*/NULL);
		Vector3_t215  L_6 = Vector4_op_Implicit_m3013(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Quaternion_t261  L_7 = Quaternion_LookRotation_m3014(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Pose3D_set_Orientation_m1589(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// MutablePose3D
#include "AssemblyU2DCSharp_MutablePose3D.h"
#ifndef _MSC_VER
#else
#endif
// MutablePose3D
#include "AssemblyU2DCSharp_MutablePose3DMethodDeclarations.h"



// System.Void MutablePose3D::.ctor()
extern TypeInfo* Pose3D_t260_il2cpp_TypeInfo_var;
extern "C" void MutablePose3D__ctor_m1595 (MutablePose3D_t262 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Pose3D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(217);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pose3D_t260_il2cpp_TypeInfo_var);
		Pose3D__ctor_m1582(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MutablePose3D::Set(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" void MutablePose3D_Set_m1596 (MutablePose3D_t262 * __this, Vector3_t215  ___position, Quaternion_t261  ___orientation, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___position;
		Quaternion_t261  L_1 = ___orientation;
		Pose3D_Set_m1593(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MutablePose3D::Set(UnityEngine.Matrix4x4)
extern "C" void MutablePose3D_Set_m1597 (MutablePose3D_t262 * __this, Matrix4x4_t242  ___matrix, const MethodInfo* method)
{
	{
		Matrix4x4_t242  L_0 = ___matrix;
		Pose3D_Set_m1594(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MutablePose3D::SetRightHanded(UnityEngine.Matrix4x4)
extern TypeInfo* Pose3D_t260_il2cpp_TypeInfo_var;
extern "C" void MutablePose3D_SetRightHanded_m1598 (MutablePose3D_t262 * __this, Matrix4x4_t242  ___matrix, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Pose3D_t260_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(217);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pose3D_t260_il2cpp_TypeInfo_var);
		Matrix4x4_t242  L_0 = ((Pose3D_t260_StaticFields*)Pose3D_t260_il2cpp_TypeInfo_var->static_fields)->___flipZ_0;
		Matrix4x4_t242  L_1 = ___matrix;
		Matrix4x4_t242  L_2 = Matrix4x4_op_Multiply_m2876(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Matrix4x4_t242  L_3 = ((Pose3D_t260_StaticFields*)Pose3D_t260_il2cpp_TypeInfo_var->static_fields)->___flipZ_0;
		Matrix4x4_t242  L_4 = Matrix4x4_op_Multiply_m2876(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		MutablePose3D_Set_m1597(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// RadialUndistortionEffect
#include "AssemblyU2DCSharp_RadialUndistortionEffect.h"
#ifndef _MSC_VER
#else
#endif
// RadialUndistortionEffect
#include "AssemblyU2DCSharp_RadialUndistortionEffectMethodDeclarations.h"



// System.Void RadialUndistortionEffect::.ctor()
extern "C" void RadialUndistortionEffect__ctor_m1599 (RadialUndistortionEffect_t263 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RadialUndistortionEffect::Awake()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral328;
extern Il2CppCodeGenString* _stringLiteral329;
extern "C" void RadialUndistortionEffect_Awake_m1600 (RadialUndistortionEffect_t263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		_stringLiteral328 = il2cpp_codegen_string_literal_from_index(328);
		_stringLiteral329 = il2cpp_codegen_string_literal_from_index(329);
		s_Il2CppMethodIntialized = true;
	}
	Shader_t1 * V_0 = {0};
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral328, /*hidden argument*/NULL);
		V_0 = L_0;
		Shader_t1 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral329, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		Shader_t1 * L_3 = V_0;
		Material_t2 * L_4 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_4, L_3, /*hidden argument*/NULL);
		__this->___material_2 = L_4;
		return;
	}
}
// System.Void RadialUndistortionEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void RadialUndistortionEffect_OnRenderImage_m1601 (RadialUndistortionEffect_t263 * __this, RenderTexture_t15 * ___source, RenderTexture_t15 * ___dest, const MethodInfo* method)
{
	{
		RenderTexture_t15 * L_0 = ___source;
		RenderTexture_t15 * L_1 = ___dest;
		Material_t2 * L_2 = (__this->___material_2);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// StereoController/<EndOfFrame>c__Iterator2
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU3Ec__Iterat.h"
#ifndef _MSC_VER
#else
#endif
// StereoController/<EndOfFrame>c__Iterator2
#include "AssemblyU2DCSharp_StereoController_U3CEndOfFrameU3Ec__IteratMethodDeclarations.h"



// System.Void StereoController/<EndOfFrame>c__Iterator2::.ctor()
extern "C" void U3CEndOfFrameU3Ec__Iterator2__ctor_m1602 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object StereoController/<EndOfFrame>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1603 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object StereoController/<EndOfFrame>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CEndOfFrameU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1604 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean StereoController/<EndOfFrame>c__Iterator2::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t484_il2cpp_TypeInfo_var;
extern "C" bool U3CEndOfFrameU3Ec__Iterator2_MoveNext_m1605 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		StereoController_t235 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___renderedStereo_14);
		if (!L_3)
		{
			goto IL_004e;
		}
	}
	{
		StereoController_t235 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		Camera_t14 * L_5 = StereoController_get_camera_m1613(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Behaviour_set_enabled_m2721(L_5, 1, /*hidden argument*/NULL);
		StereoController_t235 * L_6 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_6);
		L_6->___renderedStereo_14 = 0;
	}

IL_004e:
	{
		WaitForEndOfFrame_t484 * L_7 = (WaitForEndOfFrame_t484 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t484_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2816(L_7, /*hidden argument*/NULL);
		__this->___U24current_1 = L_7;
		__this->___U24PC_0 = 1;
		goto IL_0073;
	}

IL_0065:
	{
		goto IL_0021;
	}
	// Dead block : IL_006a: ldarg.0

IL_0071:
	{
		return 0;
	}

IL_0073:
	{
		return 1;
	}
}
// System.Void StereoController/<EndOfFrame>c__Iterator2::Dispose()
extern "C" void U3CEndOfFrameU3Ec__Iterator2_Dispose_m1606 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void StereoController/<EndOfFrame>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CEndOfFrameU3Ec__Iterator2_Reset_m1607 (U3CEndOfFrameU3Ec__Iterator2_t264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Func`2<CardboardEye,System.Boolean>
#include "System_Core_System_Func_2_gen.h"
// System.Func`2<CardboardEye,CardboardHead>
#include "System_Core_System_Func_2_gen_0.h"
// System.Func`2<CardboardEye,System.Boolean>
#include "System_Core_System_Func_2_genMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Func`2<CardboardEye,CardboardHead>
#include "System_Core_System_Func_2_gen_0MethodDeclarations.h"
struct Component_t477;
struct CardboardEyeU5BU5D_t265;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<CardboardEye>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<CardboardEye>(System.Boolean)
#define Component_GetComponentsInChildren_TisCardboardEye_t240_m3015(__this, p0, method) (( CardboardEyeU5BU5D_t265* (*) (Component_t477 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2772_gshared)(__this, p0, method)
struct Enumerable_t512;
struct IEnumerable_1_t513;
struct Func_2_t514;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t512;
struct IEnumerable_1_t515;
struct Func_2_t516;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
extern "C" Object_t* Enumerable_Where_TisObject_t_m3017_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t516 * p1, const MethodInfo* method);
#define Enumerable_Where_TisObject_t_m3017(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t516 *, const MethodInfo*))Enumerable_Where_TisObject_t_m3017_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<CardboardEye>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<CardboardEye>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
#define Enumerable_Where_TisCardboardEye_t240_m3016(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t514 *, const MethodInfo*))Enumerable_Where_TisObject_t_m3017_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t512;
struct CardboardEyeU5BU5D_t265;
struct IEnumerable_1_t513;
struct Enumerable_t512;
struct ObjectU5BU5D_t470;
struct IEnumerable_1_t515;
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" ObjectU5BU5D_t470* Enumerable_ToArray_TisObject_t_m3019_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisObject_t_m3019(__this /* static, unused */, p0, method) (( ObjectU5BU5D_t470* (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToArray_TisObject_t_m3019_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0[] System.Linq.Enumerable::ToArray<CardboardEye>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<CardboardEye>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisCardboardEye_t240_m3018(__this /* static, unused */, p0, method) (( CardboardEyeU5BU5D_t265* (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToArray_TisObject_t_m3019_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t512;
struct IEnumerable_1_t517;
struct IEnumerable_1_t513;
struct Func_2_t266;
struct Enumerable_t512;
struct IEnumerable_1_t515;
struct Func_2_t518;
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C" Object_t* Enumerable_Select_TisObject_t_TisObject_t_m3021_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t518 * p1, const MethodInfo* method);
#define Enumerable_Select_TisObject_t_TisObject_t_m3021(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t518 *, const MethodInfo*))Enumerable_Select_TisObject_t_TisObject_t_m3021_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<CardboardEye,CardboardHead>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<CardboardEye,CardboardHead>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisCardboardEye_t240_TisCardboardHead_t244_m3020(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t266 *, const MethodInfo*))Enumerable_Select_TisObject_t_TisObject_t_m3021_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t512;
struct CardboardHead_t244;
struct IEnumerable_1_t517;
struct Enumerable_t512;
struct Object_t;
struct IEnumerable_1_t515;
// Declaration !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" Object_t * Enumerable_FirstOrDefault_TisObject_t_m3023_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisObject_t_m3023(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_FirstOrDefault_TisObject_t_m3023_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 System.Linq.Enumerable::FirstOrDefault<CardboardHead>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0 System.Linq.Enumerable::FirstOrDefault<CardboardHead>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_FirstOrDefault_TisCardboardHead_t244_m3022(__this /* static, unused */, p0, method) (( CardboardHead_t244 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_FirstOrDefault_TisObject_t_m3023_gshared)(__this /* static, unused */, p0, method)
struct Enumerable_t512;
struct IEnumerable_1_t513;
struct Enumerable_t512;
struct IEnumerable_1_t515;
// Declaration System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" bool Enumerable_Any_TisObject_t_m3025_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_Any_TisObject_t_m3025(__this /* static, unused */, p0, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Any_TisObject_t_m3025_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Boolean System.Linq.Enumerable::Any<CardboardEye>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Boolean System.Linq.Enumerable::Any<CardboardEye>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisCardboardEye_t240_m3024(__this /* static, unused */, p0, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Any_TisObject_t_m3025_gshared)(__this /* static, unused */, p0, method)
struct GameObject_t256;
struct CardboardHead_t244;
// Declaration !!0 UnityEngine.GameObject::AddComponent<CardboardHead>()
// !!0 UnityEngine.GameObject::AddComponent<CardboardHead>()
#define GameObject_AddComponent_TisCardboardHead_t244_m3026(__this, method) (( CardboardHead_t244 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)
struct GameObject_t256;
struct Camera_t14;
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Camera>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Camera>()
#define GameObject_AddComponent_TisCamera_t14_m3027(__this, method) (( Camera_t14 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)
struct GameObject_t256;
struct CardboardEye_t240;
// Declaration !!0 UnityEngine.GameObject::AddComponent<CardboardEye>()
// !!0 UnityEngine.GameObject::AddComponent<CardboardEye>()
#define GameObject_AddComponent_TisCardboardEye_t240_m3028(__this, method) (( CardboardEye_t240 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void StereoController::.ctor()
extern "C" void StereoController__ctor_m1608 (StereoController_t235 * __this, const MethodInfo* method)
{
	{
		__this->___directRender_2 = 1;
		__this->___stereoMultiplier_4 = (1.0f);
		__this->___checkStereoComfort_9 = 1;
		__this->___stereoAdjustSmoothing_10 = (0.1f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// CardboardEye[] StereoController::get_Eyes()
extern TypeInfo* Func_2_t514_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCardboardEye_t240_m3015_MethodInfo_var;
extern const MethodInfo* StereoController_U3Cget_EyesU3Em__0_m1623_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3029_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisCardboardEye_t240_m3016_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisCardboardEye_t240_m3018_MethodInfo_var;
extern "C" CardboardEyeU5BU5D_t265* StereoController_get_Eyes_m1609 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Func_2_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(220);
		Component_GetComponentsInChildren_TisCardboardEye_t240_m3015_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483680);
		StereoController_U3Cget_EyesU3Em__0_m1623_MethodInfo_var = il2cpp_codegen_method_info_from_index(33);
		Func_2__ctor_m3029_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		Enumerable_Where_TisCardboardEye_t240_m3016_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		Enumerable_ToArray_TisCardboardEye_t240_m3018_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardboardEyeU5BU5D_t265* L_0 = (__this->___eyes_15);
		if (L_0)
		{
			goto IL_002e;
		}
	}
	{
		CardboardEyeU5BU5D_t265* L_1 = Component_GetComponentsInChildren_TisCardboardEye_t240_m3015(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCardboardEye_t240_m3015_MethodInfo_var);
		IntPtr_t L_2 = { (void*)StereoController_U3Cget_EyesU3Em__0_m1623_MethodInfo_var };
		Func_2_t514 * L_3 = (Func_2_t514 *)il2cpp_codegen_object_new (Func_2_t514_il2cpp_TypeInfo_var);
		Func_2__ctor_m3029(L_3, __this, L_2, /*hidden argument*/Func_2__ctor_m3029_MethodInfo_var);
		Object_t* L_4 = Enumerable_Where_TisCardboardEye_t240_m3016(NULL /*static, unused*/, (Object_t*)(Object_t*)L_1, L_3, /*hidden argument*/Enumerable_Where_TisCardboardEye_t240_m3016_MethodInfo_var);
		CardboardEyeU5BU5D_t265* L_5 = Enumerable_ToArray_TisCardboardEye_t240_m3018(NULL /*static, unused*/, L_4, /*hidden argument*/Enumerable_ToArray_TisCardboardEye_t240_m3018_MethodInfo_var);
		__this->___eyes_15 = L_5;
	}

IL_002e:
	{
		CardboardEyeU5BU5D_t265* L_6 = (__this->___eyes_15);
		return L_6;
	}
}
// CardboardHead StereoController::get_Head()
extern TypeInfo* StereoController_t235_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t266_il2cpp_TypeInfo_var;
extern const MethodInfo* StereoController_U3Cget_HeadU3Em__1_m1624_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3030_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisCardboardEye_t240_TisCardboardHead_t244_m3020_MethodInfo_var;
extern const MethodInfo* Enumerable_FirstOrDefault_TisCardboardHead_t244_m3022_MethodInfo_var;
extern "C" CardboardHead_t244 * StereoController_get_Head_m1610 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StereoController_t235_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(175);
		Func_2_t266_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(221);
		StereoController_U3Cget_HeadU3Em__1_m1624_MethodInfo_var = il2cpp_codegen_method_info_from_index(37);
		Func_2__ctor_m3030_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483686);
		Enumerable_Select_TisCardboardEye_t240_TisCardboardHead_t244_m3020_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483687);
		Enumerable_FirstOrDefault_TisCardboardHead_t244_m3022_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483688);
		s_Il2CppMethodIntialized = true;
	}
	CardboardEyeU5BU5D_t265* G_B3_0 = {0};
	StereoController_t235 * G_B3_1 = {0};
	CardboardEyeU5BU5D_t265* G_B2_0 = {0};
	StereoController_t235 * G_B2_1 = {0};
	{
		CardboardHead_t244 * L_0 = (__this->___head_16);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0044;
		}
	}
	{
		CardboardEyeU5BU5D_t265* L_2 = StereoController_get_Eyes_m1609(__this, /*hidden argument*/NULL);
		Func_2_t266 * L_3 = ((StereoController_t235_StaticFields*)StereoController_t235_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache10_18;
		G_B2_0 = L_2;
		G_B2_1 = __this;
		if (L_3)
		{
			G_B3_0 = L_2;
			G_B3_1 = __this;
			goto IL_0030;
		}
	}
	{
		IntPtr_t L_4 = { (void*)StereoController_U3Cget_HeadU3Em__1_m1624_MethodInfo_var };
		Func_2_t266 * L_5 = (Func_2_t266 *)il2cpp_codegen_object_new (Func_2_t266_il2cpp_TypeInfo_var);
		Func_2__ctor_m3030(L_5, NULL, L_4, /*hidden argument*/Func_2__ctor_m3030_MethodInfo_var);
		((StereoController_t235_StaticFields*)StereoController_t235_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache10_18 = L_5;
		G_B3_0 = G_B2_0;
		G_B3_1 = G_B2_1;
	}

IL_0030:
	{
		Func_2_t266 * L_6 = ((StereoController_t235_StaticFields*)StereoController_t235_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache10_18;
		Object_t* L_7 = Enumerable_Select_TisCardboardEye_t240_TisCardboardHead_t244_m3020(NULL /*static, unused*/, (Object_t*)(Object_t*)G_B3_0, L_6, /*hidden argument*/Enumerable_Select_TisCardboardEye_t240_TisCardboardHead_t244_m3020_MethodInfo_var);
		CardboardHead_t244 * L_8 = Enumerable_FirstOrDefault_TisCardboardHead_t244_m3022(NULL /*static, unused*/, L_7, /*hidden argument*/Enumerable_FirstOrDefault_TisCardboardHead_t244_m3022_MethodInfo_var);
		NullCheck(G_B3_1);
		G_B3_1->___head_16 = L_8;
	}

IL_0044:
	{
		CardboardHead_t244 * L_9 = (__this->___head_16);
		return L_9;
	}
}
// System.Void StereoController::InvalidateEyes()
extern "C" void StereoController_InvalidateEyes_m1611 (StereoController_t235 * __this, const MethodInfo* method)
{
	{
		__this->___eyes_15 = (CardboardEyeU5BU5D_t265*)NULL;
		__this->___head_16 = (CardboardHead_t244 *)NULL;
		return;
	}
}
// System.Void StereoController::UpdateStereoValues()
extern "C" void StereoController_UpdateStereoValues_m1612 (StereoController_t235 * __this, const MethodInfo* method)
{
	CardboardEyeU5BU5D_t265* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		CardboardEyeU5BU5D_t265* L_0 = StereoController_get_Eyes_m1609(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		CardboardEyeU5BU5D_t265* L_1 = V_0;
		NullCheck(L_1);
		V_2 = (((int32_t)(((Array_t *)L_1)->max_length)));
		goto IL_001e;
	}

IL_0012:
	{
		CardboardEyeU5BU5D_t265* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((*(CardboardEye_t240 **)(CardboardEye_t240 **)SZArrayLdElema(L_2, L_4)));
		CardboardEye_UpdateStereoValues_m1523((*(CardboardEye_t240 **)(CardboardEye_t240 **)SZArrayLdElema(L_2, L_4)), /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_2;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// UnityEngine.Camera StereoController::get_camera()
extern "C" Camera_t14 * StereoController_get_camera_m1613 (StereoController_t235 * __this, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = (__this->___U3CcameraU3Ek__BackingField_17);
		return L_0;
	}
}
// System.Void StereoController::set_camera(UnityEngine.Camera)
extern "C" void StereoController_set_camera_m1614 (StereoController_t235 * __this, Camera_t14 * ___value, const MethodInfo* method)
{
	{
		Camera_t14 * L_0 = ___value;
		__this->___U3CcameraU3Ek__BackingField_17 = L_0;
		return;
	}
}
// System.Void StereoController::Awake()
extern const MethodInfo* Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var;
extern "C" void StereoController_Awake_m1615 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	{
		StereoController_AddStereoRig_m1616(__this, /*hidden argument*/NULL);
		Camera_t14 * L_0 = Component_GetComponent_TisCamera_t14_m2850(__this, /*hidden argument*/Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var);
		StereoController_set_camera_m1614(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StereoController::AddStereoRig()
extern TypeInfo* Func_2_t514_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCardboardEye_t240_m3015_MethodInfo_var;
extern const MethodInfo* StereoController_U3CAddStereoRigU3Em__2_m1625_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3029_MethodInfo_var;
extern const MethodInfo* Enumerable_Where_TisCardboardEye_t240_m3016_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisCardboardEye_t240_m3024_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCardboardHead_t244_m3026_MethodInfo_var;
extern "C" void StereoController_AddStereoRig_m1616 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Func_2_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(220);
		Component_GetComponentsInChildren_TisCardboardEye_t240_m3015_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483680);
		StereoController_U3CAddStereoRigU3Em__2_m1625_MethodInfo_var = il2cpp_codegen_method_info_from_index(41);
		Func_2__ctor_m3029_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		Enumerable_Where_TisCardboardEye_t240_m3016_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		Enumerable_Any_TisCardboardEye_t240_m3024_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483690);
		GameObject_AddComponent_TisCardboardHead_t244_m3026_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	CardboardHead_t244 * V_1 = {0};
	{
		CardboardEyeU5BU5D_t265* L_0 = Component_GetComponentsInChildren_TisCardboardEye_t240_m3015(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCardboardEye_t240_m3015_MethodInfo_var);
		IntPtr_t L_1 = { (void*)StereoController_U3CAddStereoRigU3Em__2_m1625_MethodInfo_var };
		Func_2_t514 * L_2 = (Func_2_t514 *)il2cpp_codegen_object_new (Func_2_t514_il2cpp_TypeInfo_var);
		Func_2__ctor_m3029(L_2, __this, L_1, /*hidden argument*/Func_2__ctor_m3029_MethodInfo_var);
		Object_t* L_3 = Enumerable_Where_TisCardboardEye_t240_m3016(NULL /*static, unused*/, (Object_t*)(Object_t*)L_0, L_2, /*hidden argument*/Enumerable_Where_TisCardboardEye_t240_m3016_MethodInfo_var);
		V_0 = L_3;
		Object_t* L_4 = V_0;
		bool L_5 = Enumerable_Any_TisCardboardEye_t240_m3024(NULL /*static, unused*/, L_4, /*hidden argument*/Enumerable_Any_TisCardboardEye_t240_m3024_MethodInfo_var);
		if (!L_5)
		{
			goto IL_0025;
		}
	}
	{
		return;
	}

IL_0025:
	{
		StereoController_CreateEye_m1617(__this, 0, /*hidden argument*/NULL);
		StereoController_CreateEye_m1617(__this, 1, /*hidden argument*/NULL);
		CardboardHead_t244 * L_6 = StereoController_get_Head_m1610(__this, /*hidden argument*/NULL);
		bool L_7 = Object_op_Equality_m2716(NULL /*static, unused*/, L_6, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		GameObject_t256 * L_8 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		CardboardHead_t244 * L_9 = GameObject_AddComponent_TisCardboardHead_t244_m3026(L_8, /*hidden argument*/GameObject_AddComponent_TisCardboardHead_t244_m3026_MethodInfo_var);
		V_1 = L_9;
		CardboardHead_t244 * L_10 = V_1;
		NullCheck(L_10);
		L_10->___trackPosition_3 = 0;
	}

IL_0057:
	{
		return;
	}
}
// System.Void StereoController::CreateEye(Cardboard/Eye)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCamera_t14_m3027_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisCardboardEye_t240_m3028_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral330;
extern Il2CppCodeGenString* _stringLiteral331;
extern "C" void StereoController_CreateEye_m1617 (StereoController_t235 * __this, int32_t ___eye, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisCamera_t14_m3027_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		GameObject_AddComponent_TisCardboardEye_t240_m3028_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483693);
		_stringLiteral330 = il2cpp_codegen_string_literal_from_index(330);
		_stringLiteral331 = il2cpp_codegen_string_literal_from_index(331);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	GameObject_t256 * V_1 = {0};
	CardboardEye_t240 * V_2 = {0};
	String_t* G_B2_0 = {0};
	String_t* G_B1_0 = {0};
	String_t* G_B3_0 = {0};
	String_t* G_B3_1 = {0};
	{
		String_t* L_0 = Object_get_name_m2979(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___eye;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_0016;
		}
	}
	{
		G_B3_0 = _stringLiteral330;
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0016:
	{
		G_B3_0 = _stringLiteral331;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m3031(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		GameObject_t256 * L_4 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		GameObject_t256 * L_5 = V_1;
		NullCheck(L_5);
		Transform_t243 * L_6 = GameObject_get_transform_m2825(L_5, /*hidden argument*/NULL);
		Transform_t243 * L_7 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_parent_m2841(L_6, L_7, /*hidden argument*/NULL);
		GameObject_t256 * L_8 = V_1;
		NullCheck(L_8);
		Camera_t14 * L_9 = GameObject_AddComponent_TisCamera_t14_m3027(L_8, /*hidden argument*/GameObject_AddComponent_TisCamera_t14_m3027_MethodInfo_var);
		NullCheck(L_9);
		Behaviour_set_enabled_m2721(L_9, 0, /*hidden argument*/NULL);
		GameObject_t256 * L_10 = V_1;
		NullCheck(L_10);
		CardboardEye_t240 * L_11 = GameObject_AddComponent_TisCardboardEye_t240_m3028(L_10, /*hidden argument*/GameObject_AddComponent_TisCardboardEye_t240_m3028_MethodInfo_var);
		V_2 = L_11;
		CardboardEye_t240 * L_12 = V_2;
		int32_t L_13 = ___eye;
		NullCheck(L_12);
		L_12->___eye_2 = L_13;
		CardboardEye_t240 * L_14 = V_2;
		NullCheck(L_14);
		CardboardEye_CopyCameraAndMakeSideBySide_m1526(L_14, __this, (0.0f), (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 StereoController::ComputeStereoEyePosition(Cardboard/Eye,System.Single,System.Single)
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" Vector3_t215  StereoController_ComputeStereoEyePosition_m1618 (StereoController_t235 * __this, int32_t ___eye, float ___proj11, float ___zScale, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	Vector3_t215  V_9 = {0};
	Matrix4x4_t242  V_10 = {0};
	Vector2_t7  V_11 = {0};
	Vector2_t7  V_12 = {0};
	float G_B6_0 = 0.0f;
	{
		Transform_t243 * L_0 = (__this->___centerOfInterest_7);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		Transform_t243 * L_2 = (__this->___centerOfInterest_7);
		NullCheck(L_2);
		GameObject_t256 * L_3 = Component_get_gameObject_m2778(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeInHierarchy_m2779(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0042;
		}
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_5 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = ___eye;
		NullCheck(L_5);
		Pose3D_t260 * L_7 = Cardboard_EyePose_m1472(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t215  L_8 = Pose3D_get_Position_m1586(L_7, /*hidden argument*/NULL);
		float L_9 = (__this->___stereoMultiplier_4);
		Vector3_t215  L_10 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0042:
	{
		Transform_t243 * L_11 = (__this->___centerOfInterest_7);
		bool L_12 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_11, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_007c;
		}
	}
	{
		Transform_t243 * L_13 = (__this->___centerOfInterest_7);
		NullCheck(L_13);
		Vector3_t215  L_14 = Transform_get_position_m2894(L_13, /*hidden argument*/NULL);
		Transform_t243 * L_15 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t215  L_16 = Transform_get_position_m2894(L_15, /*hidden argument*/NULL);
		Vector3_t215  L_17 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		V_9 = L_17;
		float L_18 = Vector3_get_magnitude_m3033((&V_9), /*hidden argument*/NULL);
		G_B6_0 = L_18;
		goto IL_0081;
	}

IL_007c:
	{
		G_B6_0 = (0.0f);
	}

IL_0081:
	{
		V_0 = G_B6_0;
		float L_19 = (__this->___radiusOfInterest_8);
		float L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_21 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_19, (0.0f), L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		float L_22 = ___proj11;
		Camera_t14 * L_23 = StereoController_get_camera_m1613(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Matrix4x4_t242  L_24 = Camera_get_projectionMatrix_m2868(L_23, /*hidden argument*/NULL);
		V_10 = L_24;
		float L_25 = Matrix4x4_get_Item_m2856((&V_10), 1, 1, /*hidden argument*/NULL);
		V_2 = ((float)((float)L_22/(float)L_25));
		float L_26 = V_1;
		float L_27 = V_1;
		float L_28 = V_0;
		float L_29 = V_0;
		float L_30 = V_1;
		float L_31 = V_1;
		float L_32 = V_2;
		float L_33 = V_2;
		float L_34 = sqrtf(((float)((float)((float)((float)L_26*(float)L_27))+(float)((float)((float)((float)((float)((float)((float)((float)((float)L_28*(float)L_29))-(float)((float)((float)L_30*(float)L_31))))*(float)L_32))*(float)L_33)))));
		V_3 = L_34;
		float L_35 = V_0;
		float L_36 = V_3;
		float L_37 = (__this->___matchMonoFOV_5);
		float L_38 = Mathf_Clamp01_m2828(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		float L_39 = ___zScale;
		V_4 = ((float)((float)((float)((float)((float)((float)L_35-(float)L_36))*(float)L_38))/(float)L_39));
		float L_40 = (__this->___stereoMultiplier_4);
		V_5 = L_40;
		bool L_41 = (__this->___checkStereoComfort_9);
		if (!L_41)
		{
			goto IL_0138;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_42 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_42);
		Vector2_t7  L_43 = Cardboard_get_ComfortableViewingRange_m1475(L_42, /*hidden argument*/NULL);
		V_11 = L_43;
		float L_44 = ((&V_11)->___x_1);
		V_6 = L_44;
		Cardboard_t233 * L_45 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector2_t7  L_46 = Cardboard_get_ComfortableViewingRange_m1475(L_45, /*hidden argument*/NULL);
		V_12 = L_46;
		float L_47 = ((&V_12)->___y_2);
		V_7 = L_47;
		float L_48 = V_6;
		float L_49 = V_7;
		if ((!(((float)L_48) < ((float)L_49))))
		{
			goto IL_0138;
		}
	}
	{
		float L_50 = V_0;
		float L_51 = V_1;
		float L_52 = ___zScale;
		float L_53 = V_4;
		V_8 = ((float)((float)((float)((float)((float)((float)L_50-(float)L_51))/(float)L_52))-(float)L_53));
		float L_54 = V_5;
		float L_55 = V_8;
		float L_56 = V_8;
		float L_57 = V_6;
		float L_58 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_59 = Mathf_Clamp_m2768(NULL /*static, unused*/, L_56, L_57, L_58, /*hidden argument*/NULL);
		V_5 = ((float)((float)L_54*(float)((float)((float)L_55/(float)L_59))));
	}

IL_0138:
	{
		float L_60 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_61 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_62 = ___eye;
		NullCheck(L_61);
		Pose3D_t260 * L_63 = Cardboard_EyePose_m1472(L_61, L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		Vector3_t215  L_64 = Pose3D_get_Position_m1586(L_63, /*hidden argument*/NULL);
		Vector3_t215  L_65 = Vector3_op_Multiply_m2881(NULL /*static, unused*/, L_60, L_64, /*hidden argument*/NULL);
		float L_66 = V_4;
		Vector3_t215  L_67 = Vector3_get_forward_m2966(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_68 = Vector3_op_Multiply_m2881(NULL /*static, unused*/, L_66, L_67, /*hidden argument*/NULL);
		Vector3_t215  L_69 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_65, L_68, /*hidden argument*/NULL);
		return L_69;
	}
}
// System.Void StereoController::OnEnable()
extern Il2CppCodeGenString* _stringLiteral315;
extern "C" void StereoController_OnEnable_m1619 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StartCoroutine_m2845(__this, _stringLiteral315, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StereoController::OnDisable()
extern Il2CppCodeGenString* _stringLiteral315;
extern "C" void StereoController_OnDisable_m1620 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StopCoroutine_m2846(__this, _stringLiteral315, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StereoController::OnPreCull()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral332;
extern Il2CppCodeGenString* _stringLiteral333;
extern Il2CppCodeGenString* _stringLiteral321;
extern "C" void StereoController_OnPreCull_m1621 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		_stringLiteral332 = il2cpp_codegen_string_literal_from_index(332);
		_stringLiteral333 = il2cpp_codegen_string_literal_from_index(333);
		_stringLiteral321 = il2cpp_codegen_string_literal_from_index(321);
		s_Il2CppMethodIntialized = true;
	}
	CardboardEyeU5BU5D_t265* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = Cardboard_get_VRModeEnabled_m1442(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		CardboardEyeU5BU5D_t265* L_2 = StereoController_get_Eyes_m1609(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		CardboardEyeU5BU5D_t265* L_3 = V_0;
		NullCheck(L_3);
		V_2 = (((int32_t)(((Array_t *)L_3)->max_length)));
		goto IL_0033;
	}

IL_0021:
	{
		CardboardEyeU5BU5D_t265* L_4 = V_0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((*(CardboardEye_t240 **)(CardboardEye_t240 **)SZArrayLdElema(L_4, L_6)));
		Camera_t14 * L_7 = CardboardEye_get_camera_m1517((*(CardboardEye_t240 **)(CardboardEye_t240 **)SZArrayLdElema(L_4, L_6)), /*hidden argument*/NULL);
		NullCheck(L_7);
		Behaviour_set_enabled_m2721(L_7, 1, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0021;
		}
	}
	{
		Camera_t14 * L_11 = StereoController_get_camera_m1613(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Behaviour_set_enabled_m2721(L_11, 0, /*hidden argument*/NULL);
		__this->___renderedStereo_14 = 1;
		goto IL_009b;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_12 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Cardboard_UpdateState_m1487(L_12, /*hidden argument*/NULL);
		Camera_t14 * L_13 = StereoController_get_camera_m1613(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Matrix4x4_t242  L_14 = Camera_get_projectionMatrix_m2868(L_13, /*hidden argument*/NULL);
		Shader_SetGlobalMatrix_m2877(NULL /*static, unused*/, _stringLiteral332, L_14, /*hidden argument*/NULL);
		Camera_t14 * L_15 = StereoController_get_camera_m1613(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Matrix4x4_t242  L_16 = Camera_get_cameraToWorldMatrix_m2874(L_15, /*hidden argument*/NULL);
		Shader_SetGlobalMatrix_m2877(NULL /*static, unused*/, _stringLiteral333, L_16, /*hidden argument*/NULL);
		Camera_t14 * L_17 = StereoController_get_camera_m1613(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		float L_18 = Camera_get_nearClipPlane_m2859(L_17, /*hidden argument*/NULL);
		Shader_SetGlobalFloat_m2879(NULL /*static, unused*/, _stringLiteral321, L_18, /*hidden argument*/NULL);
	}

IL_009b:
	{
		return;
	}
}
// System.Collections.IEnumerator StereoController::EndOfFrame()
extern TypeInfo* U3CEndOfFrameU3Ec__Iterator2_t264_il2cpp_TypeInfo_var;
extern "C" Object_t * StereoController_EndOfFrame_m1622 (StereoController_t235 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CEndOfFrameU3Ec__Iterator2_t264_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(222);
		s_Il2CppMethodIntialized = true;
	}
	U3CEndOfFrameU3Ec__Iterator2_t264 * V_0 = {0};
	{
		U3CEndOfFrameU3Ec__Iterator2_t264 * L_0 = (U3CEndOfFrameU3Ec__Iterator2_t264 *)il2cpp_codegen_object_new (U3CEndOfFrameU3Ec__Iterator2_t264_il2cpp_TypeInfo_var);
		U3CEndOfFrameU3Ec__Iterator2__ctor_m1602(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CEndOfFrameU3Ec__Iterator2_t264 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CEndOfFrameU3Ec__Iterator2_t264 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean StereoController::<get_Eyes>m__0(CardboardEye)
extern "C" bool StereoController_U3Cget_EyesU3Em__0_m1623 (StereoController_t235 * __this, CardboardEye_t240 * ___eye, const MethodInfo* method)
{
	{
		CardboardEye_t240 * L_0 = ___eye;
		NullCheck(L_0);
		StereoController_t235 * L_1 = CardboardEye_get_Controller_m1515(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		return L_2;
	}
}
// CardboardHead StereoController::<get_Head>m__1(CardboardEye)
extern "C" CardboardHead_t244 * StereoController_U3Cget_HeadU3Em__1_m1624 (Object_t * __this /* static, unused */, CardboardEye_t240 * ___eye, const MethodInfo* method)
{
	{
		CardboardEye_t240 * L_0 = ___eye;
		NullCheck(L_0);
		CardboardHead_t244 * L_1 = CardboardEye_get_Head_m1516(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean StereoController::<AddStereoRig>m__2(CardboardEye)
extern "C" bool StereoController_U3CAddStereoRigU3Em__2_m1625 (StereoController_t235 * __this, CardboardEye_t240 * ___eye, const MethodInfo* method)
{
	{
		CardboardEye_t240 * L_0 = ___eye;
		NullCheck(L_0);
		StereoController_t235 * L_1 = CardboardEye_get_Controller_m1515(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m2716(NULL /*static, unused*/, L_1, __this, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif
// StereoRenderEffect
#include "AssemblyU2DCSharp_StereoRenderEffectMethodDeclarations.h"



// System.Void StereoRenderEffect::.ctor()
extern "C" void StereoRenderEffect__ctor_m1626 (StereoRenderEffect_t239 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StereoRenderEffect::Awake()
extern const MethodInfo* Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var;
extern "C" void StereoRenderEffect_Awake_m1627 (StereoRenderEffect_t239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t14 * L_0 = Component_GetComponent_TisCamera_t14_m2850(__this, /*hidden argument*/Component_GetComponent_TisCamera_t14_m2850_MethodInfo_var);
		__this->___camera_3 = L_0;
		return;
	}
}
// System.Void StereoRenderEffect::Start()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral334;
extern "C" void StereoRenderEffect_Start_m1628 (StereoRenderEffect_t239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		_stringLiteral334 = il2cpp_codegen_string_literal_from_index(334);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral334, /*hidden argument*/NULL);
		Material_t2 * L_1 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_1, L_0, /*hidden argument*/NULL);
		__this->___material_2 = L_1;
		return;
	}
}
// System.Void StereoRenderEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void StereoRenderEffect_OnRenderImage_m1629 (StereoRenderEffect_t239 * __this, RenderTexture_t15 * ___source, RenderTexture_t15 * ___dest, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Rect_t225  V_2 = {0};
	RenderTexture_t15 * V_3 = {0};
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		GL_PushMatrix_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		RenderTexture_t15 * L_0 = ___dest;
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		RenderTexture_t15 * L_2 = ___dest;
		NullCheck(L_2);
		int32_t L_3 = RenderTexture_get_width_m2726(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0020;
	}

IL_001b:
	{
		int32_t L_4 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0020:
	{
		V_0 = G_B3_0;
		RenderTexture_t15 * L_5 = ___dest;
		bool L_6 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		RenderTexture_t15 * L_7 = ___dest;
		NullCheck(L_7);
		int32_t L_8 = RenderTexture_get_height_m2727(L_7, /*hidden argument*/NULL);
		G_B6_0 = L_8;
		goto IL_003c;
	}

IL_0037:
	{
		int32_t L_9 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_9;
	}

IL_003c:
	{
		V_1 = G_B6_0;
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		GL_LoadPixelMatrix_m2932(NULL /*static, unused*/, (0.0f), (((float)L_10)), (((float)L_11)), (0.0f), /*hidden argument*/NULL);
		Camera_t14 * L_12 = (__this->___camera_3);
		NullCheck(L_12);
		Rect_t225  L_13 = Camera_get_pixelRect_m3034(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		int32_t L_14 = V_1;
		float L_15 = Rect_get_height_m2800((&V_2), /*hidden argument*/NULL);
		float L_16 = Rect_get_y_m2865((&V_2), /*hidden argument*/NULL);
		Rect_set_y_m2866((&V_2), ((float)((float)((float)((float)(((float)L_14))-(float)L_15))-(float)L_16)), /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = RenderTexture_get_active_m2787(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_17;
		RenderTexture_t15 * L_18 = ___dest;
		RenderTexture_set_active_m2788(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Rect_t225  L_19 = V_2;
		RenderTexture_t15 * L_20 = ___source;
		Material_t2 * L_21 = (__this->___material_2);
		Graphics_DrawTexture_m3035(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		RenderTexture_t15 * L_22 = V_3;
		RenderTexture_set_active_m2788(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		GL_PopMatrix_m2931(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// BaseCardboardDevice/VREventCallback
#include "AssemblyU2DCSharp_BaseCardboardDevice_VREventCallback.h"
#ifndef _MSC_VER
#else
#endif
// BaseCardboardDevice/VREventCallback
#include "AssemblyU2DCSharp_BaseCardboardDevice_VREventCallbackMethodDeclarations.h"



// System.Void BaseCardboardDevice/VREventCallback::.ctor(System.Object,System.IntPtr)
extern "C" void VREventCallback__ctor_m1630 (VREventCallback_t267 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void BaseCardboardDevice/VREventCallback::Invoke(System.Int32)
extern "C" void VREventCallback_Invoke_m1631 (VREventCallback_t267 * __this, int32_t ___eventID, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		VREventCallback_Invoke_m1631((VREventCallback_t267 *)__this->___prev_9,___eventID, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___eventID, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___eventID,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___eventID, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___eventID,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_VREventCallback_t267(Il2CppObject* delegate, int32_t ___eventID)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___eventID' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___eventID);

	// Marshaling cleanup of parameter '___eventID' native representation

}
// System.IAsyncResult BaseCardboardDevice/VREventCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern "C" Object_t * VREventCallback_BeginInvoke_m1632 (VREventCallback_t267 * __this, int32_t ___eventID, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t478_il2cpp_TypeInfo_var, &___eventID);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void BaseCardboardDevice/VREventCallback::EndInvoke(System.IAsyncResult)
extern "C" void VREventCallback_EndInvoke_m1633 (VREventCallback_t267 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// BaseCardboardDevice
#include "AssemblyU2DCSharp_BaseCardboardDevice.h"
#ifndef _MSC_VER
#else
#endif
// BaseCardboardDevice
#include "AssemblyU2DCSharp_BaseCardboardDeviceMethodDeclarations.h"

// System.Collections.Generic.Queue`1<System.Int32>
#include "System_System_Collections_Generic_Queue_1_gen.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// BaseVRDevice/DisplayMetrics
#include "AssemblyU2DCSharp_BaseVRDevice_DisplayMetrics.h"
// System.Collections.Generic.Queue`1<System.Int32>
#include "System_System_Collections_Generic_Queue_1_genMethodDeclarations.h"
// System.Text.Encoding
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"


// System.Void BaseCardboardDevice::.ctor()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern TypeInfo* Matrix4x4_t242_il2cpp_TypeInfo_var;
extern TypeInfo* Queue_1_t268_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t269_il2cpp_TypeInfo_var;
extern TypeInfo* BaseVRDevice_t236_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1__ctor_m3036_MethodInfo_var;
extern "C" void BaseCardboardDevice__ctor_m1634 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		Matrix4x4_t242_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(223);
		Queue_1_t268_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(224);
		Int32U5BU5D_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(196);
		BaseVRDevice_t236_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(176);
		Queue_1__ctor_m3036_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	Matrix4x4_t242  V_1 = {0};
	Matrix4x4_t242  V_2 = {0};
	{
		__this->___headData_24 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)16)));
		__this->___viewData_25 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)106)));
		__this->___profileData_26 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)13)));
		Initobj (Matrix4x4_t242_il2cpp_TypeInfo_var, (&V_0));
		Matrix4x4_t242  L_0 = V_0;
		__this->___headView_27 = L_0;
		Initobj (Matrix4x4_t242_il2cpp_TypeInfo_var, (&V_1));
		Matrix4x4_t242  L_1 = V_1;
		__this->___leftEyeView_28 = L_1;
		Initobj (Matrix4x4_t242_il2cpp_TypeInfo_var, (&V_2));
		Matrix4x4_t242  L_2 = V_2;
		__this->___rightEyeView_29 = L_2;
		Queue_1_t268 * L_3 = (Queue_1_t268 *)il2cpp_codegen_object_new (Queue_1_t268_il2cpp_TypeInfo_var);
		Queue_1__ctor_m3036(L_3, /*hidden argument*/Queue_1__ctor_m3036_MethodInfo_var);
		__this->___eventQueue_30 = L_3;
		__this->___events_34 = ((Int32U5BU5D_t269*)SZArrayNew(Int32U5BU5D_t269_il2cpp_TypeInfo_var, 4));
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		BaseVRDevice__ctor_m1670(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean BaseCardboardDevice::SupportsNativeDistortionCorrection(System.Collections.Generic.List`1<System.String>)
extern Il2CppCodeGenString* _stringLiteral335;
extern "C" bool BaseCardboardDevice_SupportsNativeDistortionCorrection_m1635 (BaseCardboardDevice_t270 * __this, List_1_t468 * ___diagnostics, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral335 = il2cpp_codegen_string_literal_from_index(335);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		List_1_t468 * L_0 = ___diagnostics;
		bool L_1 = BaseVRDevice_SupportsNativeDistortionCorrection_m1675(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = (__this->___debugDisableNativeDistortion_32);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = 0;
		List_1_t468 * L_3 = ___diagnostics;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_3, _stringLiteral335);
	}

IL_0020:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void BaseCardboardDevice::SetDistortionCorrectionEnabled(System.Boolean)
extern "C" void BaseCardboardDevice_SetDistortionCorrectionEnabled_m1636 (BaseCardboardDevice_t270 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled;
		BaseCardboardDevice_EnableDistortionCorrection_m1659(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::SetNeckModelScale(System.Single)
extern "C" void BaseCardboardDevice_SetNeckModelScale_m1637 (BaseCardboardDevice_t270 * __this, float ___scale, const MethodInfo* method)
{
	{
		float L_0 = ___scale;
		BaseCardboardDevice_SetNeckModelFactor_m1662(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::SetAutoDriftCorrectionEnabled(System.Boolean)
extern "C" void BaseCardboardDevice_SetAutoDriftCorrectionEnabled_m1638 (BaseCardboardDevice_t270 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled;
		BaseCardboardDevice_EnableAutoDriftCorrection_m1660(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::SetElectronicDisplayStabilizationEnabled(System.Boolean)
extern "C" void BaseCardboardDevice_SetElectronicDisplayStabilizationEnabled_m1639 (BaseCardboardDevice_t270 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled;
		BaseCardboardDevice_EnableElectronicDisplayStabilization_m1661(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean BaseCardboardDevice::SetDefaultDeviceProfile(System.Uri)
extern TypeInfo* Encoding_t519_il2cpp_TypeInfo_var;
extern "C" bool BaseCardboardDevice_SetDefaultDeviceProfile_m1640 (BaseCardboardDevice_t270 * __this, Uri_t237 * ___uri, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(225);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t469* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
		Encoding_t519 * L_0 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
		Uri_t237 * L_1 = ___uri;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_1);
		NullCheck(L_0);
		ByteU5BU5D_t469* L_3 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_2);
		V_0 = L_3;
		ByteU5BU5D_t469* L_4 = V_0;
		ByteU5BU5D_t469* L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = BaseCardboardDevice_SetDefaultProfile_m1657(NULL /*static, unused*/, L_4, (((int32_t)(((Array_t *)L_5)->max_length))), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void BaseCardboardDevice::Init()
extern TypeInfo* Encoding_t519_il2cpp_TypeInfo_var;
extern TypeInfo* VREventCallback_t267_il2cpp_TypeInfo_var;
extern const MethodInfo* BaseCardboardDevice_OnVREvent_m1653_MethodInfo_var;
extern "C" void BaseCardboardDevice_Init_m1641 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(225);
		VREventCallback_t267_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(226);
		BaseCardboardDevice_OnVREvent_m1653_MethodInfo_var = il2cpp_codegen_method_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	DisplayMetrics_t271  V_0 = {0};
	ByteU5BU5D_t469* V_1 = {0};
	{
		DisplayMetrics_t271  L_0 = (DisplayMetrics_t271 )VirtFuncInvoker0< DisplayMetrics_t271  >::Invoke(16 /* BaseVRDevice/DisplayMetrics BaseVRDevice::GetDisplayMetrics() */, __this);
		V_0 = L_0;
		int32_t L_1 = ((&V_0)->___width_0);
		int32_t L_2 = ((&V_0)->___height_1);
		float L_3 = ((&V_0)->___xdpi_2);
		float L_4 = ((&V_0)->___ydpi_3);
		BaseCardboardDevice_Start_m1654(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
		Encoding_t519 * L_5 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = Application_get_unityVersion_m3038(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t469* L_7 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, L_6);
		V_1 = L_7;
		ByteU5BU5D_t469* L_8 = V_1;
		ByteU5BU5D_t469* L_9 = V_1;
		NullCheck(L_9);
		BaseCardboardDevice_SetUnityVersion_m1658(NULL /*static, unused*/, L_8, (((int32_t)(((Array_t *)L_9)->max_length))), /*hidden argument*/NULL);
		IntPtr_t L_10 = { (void*)BaseCardboardDevice_OnVREvent_m1653_MethodInfo_var };
		VREventCallback_t267 * L_11 = (VREventCallback_t267 *)il2cpp_codegen_object_new (VREventCallback_t267_il2cpp_TypeInfo_var);
		VREventCallback__ctor_m1630(L_11, NULL, L_10, /*hidden argument*/NULL);
		BaseCardboardDevice_SetEventCallback_m1655(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::SetStereoScreen(UnityEngine.RenderTexture)
extern "C" void BaseCardboardDevice_SetStereoScreen_m1642 (BaseCardboardDevice_t270 * __this, RenderTexture_t15 * ___stereoScreen, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		RenderTexture_t15 * L_0 = ___stereoScreen;
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		RenderTexture_t15 * L_2 = ___stereoScreen;
		NullCheck(L_2);
		IntPtr_t L_3 = Texture_GetNativeTexturePtr_m3039(L_2, /*hidden argument*/NULL);
		int32_t L_4 = IntPtr_op_Explicit_m3040(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = 0;
	}

IL_001d:
	{
		BaseCardboardDevice_SetTextureId_m1656(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::UpdateState()
extern "C" void BaseCardboardDevice_UpdateState_m1643 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(32 /* System.Void BaseCardboardDevice::ProcessEvents() */, __this);
		SingleU5BU5D_t72* L_0 = (__this->___headData_24);
		BaseCardboardDevice_GetHeadPose_m1665(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Matrix4x4_t242 * L_1 = &(__this->___headView_27);
		SingleU5BU5D_t72* L_2 = (__this->___headData_24);
		BaseCardboardDevice_ExtractMatrix_m1651(NULL /*static, unused*/, L_1, L_2, 0, /*hidden argument*/NULL);
		MutablePose3D_t262 * L_3 = (((BaseVRDevice_t236 *)__this)->___headPose_1);
		Matrix4x4_t242 * L_4 = &(__this->___headView_27);
		Matrix4x4_t242  L_5 = Matrix4x4_get_inverse_m3041(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		MutablePose3D_SetRightHanded_m1598(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::UpdateScreenData()
extern "C" void BaseCardboardDevice_UpdateScreenData_m1644 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	{
		BaseCardboardDevice_UpdateProfile_m1650(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___debugDisableNativeProjections_31);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		BaseVRDevice_ComputeEyesFromProfile_m1691(__this, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_001c:
	{
		BaseCardboardDevice_UpdateView_m1649(__this, /*hidden argument*/NULL);
	}

IL_0022:
	{
		((BaseVRDevice_t236 *)__this)->___profileChanged_15 = 1;
		return;
	}
}
// System.Void BaseCardboardDevice::Recenter()
extern "C" void BaseCardboardDevice_Recenter_m1645 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	{
		BaseCardboardDevice_ResetHeadTracker_m1663(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::PostRender()
extern "C" void BaseCardboardDevice_PostRender_m1646 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	{
		GL_IssuePluginEvent_m3042(NULL /*static, unused*/, ((int32_t)1196770114), /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::OnPause(System.Boolean)
extern "C" void BaseCardboardDevice_OnPause_m1647 (BaseCardboardDevice_t270 * __this, bool ___pause, const MethodInfo* method)
{
	{
		bool L_0 = ___pause;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		BaseCardboardDevice_Pause_m1667(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0015;
	}

IL_0010:
	{
		BaseCardboardDevice_Resume_m1668(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void BaseCardboardDevice::OnApplicationQuit()
extern "C" void BaseCardboardDevice_OnApplicationQuit_m1648 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	{
		BaseCardboardDevice_Stop_m1669(NULL /*static, unused*/, /*hidden argument*/NULL);
		BaseVRDevice_OnApplicationQuit_m1689(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseCardboardDevice::UpdateView()
extern "C" void BaseCardboardDevice_UpdateView_m1649 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t72* L_0 = (__this->___viewData_25);
		BaseCardboardDevice_GetViewParameters_m1666(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = 0;
		Matrix4x4_t242 * L_1 = &(__this->___leftEyeView_28);
		SingleU5BU5D_t72* L_2 = (__this->___viewData_25);
		int32_t L_3 = V_0;
		int32_t L_4 = BaseCardboardDevice_ExtractMatrix_m1651(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Matrix4x4_t242 * L_5 = &(__this->___rightEyeView_29);
		SingleU5BU5D_t72* L_6 = (__this->___viewData_25);
		int32_t L_7 = V_0;
		int32_t L_8 = BaseCardboardDevice_ExtractMatrix_m1651(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		MutablePose3D_t262 * L_9 = (((BaseVRDevice_t236 *)__this)->___leftEyePose_2);
		Matrix4x4_t242 * L_10 = &(__this->___leftEyeView_28);
		Matrix4x4_t242  L_11 = Matrix4x4_get_inverse_m3041(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		MutablePose3D_SetRightHanded_m1598(L_9, L_11, /*hidden argument*/NULL);
		MutablePose3D_t262 * L_12 = (((BaseVRDevice_t236 *)__this)->___rightEyePose_3);
		Matrix4x4_t242 * L_13 = &(__this->___rightEyeView_29);
		Matrix4x4_t242  L_14 = Matrix4x4_get_inverse_m3041(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		MutablePose3D_SetRightHanded_m1598(L_12, L_14, /*hidden argument*/NULL);
		Matrix4x4_t242 * L_15 = &(((BaseVRDevice_t236 *)__this)->___leftEyeDistortedProjection_4);
		SingleU5BU5D_t72* L_16 = (__this->___viewData_25);
		int32_t L_17 = V_0;
		int32_t L_18 = BaseCardboardDevice_ExtractMatrix_m1651(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		Matrix4x4_t242 * L_19 = &(((BaseVRDevice_t236 *)__this)->___rightEyeDistortedProjection_5);
		SingleU5BU5D_t72* L_20 = (__this->___viewData_25);
		int32_t L_21 = V_0;
		int32_t L_22 = BaseCardboardDevice_ExtractMatrix_m1651(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		Matrix4x4_t242 * L_23 = &(((BaseVRDevice_t236 *)__this)->___leftEyeUndistortedProjection_6);
		SingleU5BU5D_t72* L_24 = (__this->___viewData_25);
		int32_t L_25 = V_0;
		int32_t L_26 = BaseCardboardDevice_ExtractMatrix_m1651(NULL /*static, unused*/, L_23, L_24, L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		Matrix4x4_t242 * L_27 = &(((BaseVRDevice_t236 *)__this)->___rightEyeUndistortedProjection_7);
		SingleU5BU5D_t72* L_28 = (__this->___viewData_25);
		int32_t L_29 = V_0;
		int32_t L_30 = BaseCardboardDevice_ExtractMatrix_m1651(NULL /*static, unused*/, L_27, L_28, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		Rect_t225 * L_31 = &(((BaseVRDevice_t236 *)__this)->___leftEyeUndistortedViewport_10);
		SingleU5BU5D_t72* L_32 = (__this->___viewData_25);
		int32_t L_33 = V_0;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		int32_t L_34 = L_33;
		SingleU5BU5D_t72* L_35 = (__this->___viewData_25);
		int32_t L_36 = V_0;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)((int32_t)L_36+(int32_t)1)));
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)1));
		SingleU5BU5D_t72* L_38 = (__this->___viewData_25);
		int32_t L_39 = V_0;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)((int32_t)L_39+(int32_t)2)));
		int32_t L_40 = ((int32_t)((int32_t)L_39+(int32_t)2));
		SingleU5BU5D_t72* L_41 = (__this->___viewData_25);
		int32_t L_42 = V_0;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)((int32_t)L_42+(int32_t)3)));
		int32_t L_43 = ((int32_t)((int32_t)L_42+(int32_t)3));
		Rect_Set_m3043(L_31, (*(float*)(float*)SZArrayLdElema(L_32, L_34)), (*(float*)(float*)SZArrayLdElema(L_35, L_37)), (*(float*)(float*)SZArrayLdElema(L_38, L_40)), (*(float*)(float*)SZArrayLdElema(L_41, L_43)), /*hidden argument*/NULL);
		Rect_t225  L_44 = (((BaseVRDevice_t236 *)__this)->___leftEyeUndistortedViewport_10);
		((BaseVRDevice_t236 *)__this)->___leftEyeDistortedViewport_8 = L_44;
		int32_t L_45 = V_0;
		V_0 = ((int32_t)((int32_t)L_45+(int32_t)4));
		Rect_t225 * L_46 = &(((BaseVRDevice_t236 *)__this)->___rightEyeUndistortedViewport_11);
		SingleU5BU5D_t72* L_47 = (__this->___viewData_25);
		int32_t L_48 = V_0;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		int32_t L_49 = L_48;
		SingleU5BU5D_t72* L_50 = (__this->___viewData_25);
		int32_t L_51 = V_0;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, ((int32_t)((int32_t)L_51+(int32_t)1)));
		int32_t L_52 = ((int32_t)((int32_t)L_51+(int32_t)1));
		SingleU5BU5D_t72* L_53 = (__this->___viewData_25);
		int32_t L_54 = V_0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, ((int32_t)((int32_t)L_54+(int32_t)2)));
		int32_t L_55 = ((int32_t)((int32_t)L_54+(int32_t)2));
		SingleU5BU5D_t72* L_56 = (__this->___viewData_25);
		int32_t L_57 = V_0;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)((int32_t)L_57+(int32_t)3)));
		int32_t L_58 = ((int32_t)((int32_t)L_57+(int32_t)3));
		Rect_Set_m3043(L_46, (*(float*)(float*)SZArrayLdElema(L_47, L_49)), (*(float*)(float*)SZArrayLdElema(L_50, L_52)), (*(float*)(float*)SZArrayLdElema(L_53, L_55)), (*(float*)(float*)SZArrayLdElema(L_56, L_58)), /*hidden argument*/NULL);
		Rect_t225  L_59 = (((BaseVRDevice_t236 *)__this)->___rightEyeUndistortedViewport_11);
		((BaseVRDevice_t236 *)__this)->___rightEyeDistortedViewport_9 = L_59;
		int32_t L_60 = V_0;
		V_0 = ((int32_t)((int32_t)L_60+(int32_t)4));
		SingleU5BU5D_t72* L_61 = (__this->___viewData_25);
		int32_t L_62 = V_0;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		int32_t L_63 = L_62;
		SingleU5BU5D_t72* L_64 = (__this->___viewData_25);
		int32_t L_65 = V_0;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, ((int32_t)((int32_t)L_65+(int32_t)1)));
		int32_t L_66 = ((int32_t)((int32_t)L_65+(int32_t)1));
		Vector2_t7  L_67 = {0};
		Vector2__ctor_m2714(&L_67, (*(float*)(float*)SZArrayLdElema(L_61, L_63)), (*(float*)(float*)SZArrayLdElema(L_64, L_66)), /*hidden argument*/NULL);
		((BaseVRDevice_t236 *)__this)->___recommendedTextureSize_12 = L_67;
		int32_t L_68 = V_0;
		V_0 = ((int32_t)((int32_t)L_68+(int32_t)2));
		return;
	}
}
// System.Void BaseCardboardDevice::UpdateProfile()
extern TypeInfo* Device_t252_il2cpp_TypeInfo_var;
extern TypeInfo* Screen_t248_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern "C" void BaseCardboardDevice_UpdateProfile_m1650 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Device_t252_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(198);
		Screen_t248_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(197);
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	Device_t252  V_0 = {0};
	Screen_t248  V_1 = {0};
	{
		SingleU5BU5D_t72* L_0 = (__this->___profileData_26);
		BaseCardboardDevice_GetProfile_m1664(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Initobj (Device_t252_il2cpp_TypeInfo_var, (&V_0));
		Initobj (Screen_t248_il2cpp_TypeInfo_var, (&V_1));
		MaxFOV_t250 * L_1 = &((&V_0)->___maxFOV_1);
		SingleU5BU5D_t72* L_2 = (__this->___profileData_26);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		L_1->___outer_0 = (*(float*)(float*)SZArrayLdElema(L_2, L_3));
		MaxFOV_t250 * L_4 = &((&V_0)->___maxFOV_1);
		SingleU5BU5D_t72* L_5 = (__this->___profileData_26);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		int32_t L_6 = 1;
		L_4->___upper_2 = (*(float*)(float*)SZArrayLdElema(L_5, L_6));
		MaxFOV_t250 * L_7 = &((&V_0)->___maxFOV_1);
		SingleU5BU5D_t72* L_8 = (__this->___profileData_26);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		L_7->___inner_1 = (*(float*)(float*)SZArrayLdElema(L_8, L_9));
		MaxFOV_t250 * L_10 = &((&V_0)->___maxFOV_1);
		SingleU5BU5D_t72* L_11 = (__this->___profileData_26);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		int32_t L_12 = 3;
		L_10->___lower_3 = (*(float*)(float*)SZArrayLdElema(L_11, L_12));
		SingleU5BU5D_t72* L_13 = (__this->___profileData_26);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		int32_t L_14 = 4;
		(&V_1)->___width_0 = (*(float*)(float*)SZArrayLdElema(L_13, L_14));
		SingleU5BU5D_t72* L_15 = (__this->___profileData_26);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 5);
		int32_t L_16 = 5;
		(&V_1)->___height_1 = (*(float*)(float*)SZArrayLdElema(L_15, L_16));
		SingleU5BU5D_t72* L_17 = (__this->___profileData_26);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 6);
		int32_t L_18 = 6;
		(&V_1)->___border_2 = (*(float*)(float*)SZArrayLdElema(L_17, L_18));
		Lenses_t249 * L_19 = &((&V_0)->___lenses_0);
		SingleU5BU5D_t72* L_20 = (__this->___profileData_26);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 7);
		int32_t L_21 = 7;
		L_19->___separation_3 = (*(float*)(float*)SZArrayLdElema(L_20, L_21));
		Lenses_t249 * L_22 = &((&V_0)->___lenses_0);
		SingleU5BU5D_t72* L_23 = (__this->___profileData_26);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 8);
		int32_t L_24 = 8;
		L_22->___offset_4 = (*(float*)(float*)SZArrayLdElema(L_23, L_24));
		Lenses_t249 * L_25 = &((&V_0)->___lenses_0);
		SingleU5BU5D_t72* L_26 = (__this->___profileData_26);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)9));
		int32_t L_27 = ((int32_t)9);
		L_25->___screenDistance_5 = (*(float*)(float*)SZArrayLdElema(L_26, L_27));
		Lenses_t249 * L_28 = &((&V_0)->___lenses_0);
		SingleU5BU5D_t72* L_29 = (__this->___profileData_26);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)10));
		int32_t L_30 = ((int32_t)10);
		L_28->___alignment_6 = (((int32_t)(*(float*)(float*)SZArrayLdElema(L_29, L_30))));
		Distortion_t251 * L_31 = &((&V_0)->___distortion_2);
		SingleU5BU5D_t72* L_32 = (__this->___profileData_26);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)11));
		int32_t L_33 = ((int32_t)11);
		L_31->___k1_0 = (*(float*)(float*)SZArrayLdElema(L_32, L_33));
		Distortion_t251 * L_34 = &((&V_0)->___distortion_2);
		SingleU5BU5D_t72* L_35 = (__this->___profileData_26);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)12));
		int32_t L_36 = ((int32_t)12);
		L_34->___k2_1 = (*(float*)(float*)SZArrayLdElema(L_35, L_36));
		Distortion_t251  L_37 = ((&V_0)->___distortion_2);
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		Distortion_t251  L_38 = CardboardProfile_ApproximateInverse_m1568(NULL /*static, unused*/, L_37, (1.0f), ((int32_t)10), /*hidden argument*/NULL);
		(&V_0)->___inverse_3 = L_38;
		CardboardProfile_t255 * L_39 = BaseVRDevice_get_Profile_m1672(__this, /*hidden argument*/NULL);
		Screen_t248  L_40 = V_1;
		NullCheck(L_39);
		L_39->___screen_0 = L_40;
		CardboardProfile_t255 * L_41 = BaseVRDevice_get_Profile_m1672(__this, /*hidden argument*/NULL);
		Device_t252  L_42 = V_0;
		NullCheck(L_41);
		L_41->___device_1 = L_42;
		return;
	}
}
// System.Int32 BaseCardboardDevice::ExtractMatrix(UnityEngine.Matrix4x4&,System.Single[],System.Int32)
extern "C" int32_t BaseCardboardDevice_ExtractMatrix_m1651 (Object_t * __this /* static, unused */, Matrix4x4_t242 * ___mat, SingleU5BU5D_t72* ___data, int32_t ___i, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_002d;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0022;
	}

IL_000e:
	{
		Matrix4x4_t242 * L_0 = ___mat;
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		SingleU5BU5D_t72* L_3 = ___data;
		int32_t L_4 = ___i;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Matrix4x4_set_Item_m2858(L_0, L_1, L_2, (*(float*)(float*)SZArrayLdElema(L_3, L_5)), /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = ___i;
		___i = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) < ((int32_t)4)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_002d:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) < ((int32_t)4)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_11 = ___i;
		return L_11;
	}
}
// System.Void BaseCardboardDevice::ProcessEvents()
extern TypeInfo* Int32U5BU5D_t269_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_CopyTo_m3045_MethodInfo_var;
extern const MethodInfo* Queue_1_Clear_m3046_MethodInfo_var;
extern "C" void BaseCardboardDevice_ProcessEvents_m1652 (BaseCardboardDevice_t270 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t269_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(196);
		Queue_1_CopyTo_m3045_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		Queue_1_Clear_m3046_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Queue_1_t268 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		Queue_1_t268 * L_0 = (__this->___eventQueue_30);
		V_1 = L_0;
		Queue_1_t268 * L_1 = V_1;
		Monitor_Enter_m3044(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t268 * L_2 = (__this->___eventQueue_30);
			NullCheck(L_2);
			int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 System.Collections.Generic.Queue`1<System.Int32>::get_Count() */, L_2);
			V_0 = L_3;
			int32_t L_4 = V_0;
			if (L_4)
			{
				goto IL_0026;
			}
		}

IL_0021:
		{
			IL2CPP_LEAVE(0xD0, FINALLY_0062);
		}

IL_0026:
		{
			int32_t L_5 = V_0;
			Int32U5BU5D_t269* L_6 = (__this->___events_34);
			NullCheck(L_6);
			if ((((int32_t)L_5) <= ((int32_t)(((int32_t)(((Array_t *)L_6)->max_length))))))
			{
				goto IL_0040;
			}
		}

IL_0034:
		{
			int32_t L_7 = V_0;
			__this->___events_34 = ((Int32U5BU5D_t269*)SZArrayNew(Int32U5BU5D_t269_il2cpp_TypeInfo_var, L_7));
		}

IL_0040:
		{
			Queue_1_t268 * L_8 = (__this->___eventQueue_30);
			Int32U5BU5D_t269* L_9 = (__this->___events_34);
			NullCheck(L_8);
			Queue_1_CopyTo_m3045(L_8, L_9, 0, /*hidden argument*/Queue_1_CopyTo_m3045_MethodInfo_var);
			Queue_1_t268 * L_10 = (__this->___eventQueue_30);
			NullCheck(L_10);
			Queue_1_Clear_m3046(L_10, /*hidden argument*/Queue_1_Clear_m3046_MethodInfo_var);
			IL2CPP_LEAVE(0x69, FINALLY_0062);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0062;
	}

FINALLY_0062:
	{ // begin finally (depth: 1)
		Queue_1_t268 * L_11 = V_1;
		Monitor_Exit_m3047(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(98)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(98)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0069:
	{
		V_2 = 0;
		goto IL_00c9;
	}

IL_0070:
	{
		Int32U5BU5D_t269* L_12 = (__this->___events_34);
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_3 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_12, L_14));
		int32_t L_15 = V_3;
		if (((int32_t)((int32_t)L_15-(int32_t)1)) == 0)
		{
			goto IL_0096;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)1)) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)1)) == 2)
		{
			goto IL_00ae;
		}
		if (((int32_t)((int32_t)L_15-(int32_t)1)) == 3)
		{
			goto IL_00b9;
		}
	}
	{
		goto IL_00c5;
	}

IL_0096:
	{
		((BaseVRDevice_t236 *)__this)->___triggered_13 = 1;
		goto IL_00c5;
	}

IL_00a2:
	{
		((BaseVRDevice_t236 *)__this)->___tilted_14 = 1;
		goto IL_00c5;
	}

IL_00ae:
	{
		VirtActionInvoker0::Invoke(23 /* System.Void BaseCardboardDevice::UpdateScreenData() */, __this);
		goto IL_00c5;
	}

IL_00b9:
	{
		((BaseVRDevice_t236 *)__this)->___backButtonPressed_16 = 1;
		goto IL_00c5;
	}

IL_00c5:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_00c9:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0070;
		}
	}

IL_00d0:
	{
		return;
	}
}
// System.Void BaseCardboardDevice::OnVREvent(System.Int32)
void STDCALL native_delegate_wrapper_BaseCardboardDevice_OnVREvent_m1653(int32_t ___eventID)
{
	il2cpp_native_wrapper_vm_thread_attacher _vmThreadHelper;
	// Marshaling of parameter '___eventID' to managed representation

	BaseCardboardDevice_OnVREvent_m1653(NULL, ___eventID, NULL);

	// Marshaling of parameter '___eventID' to native representation

}
extern const methodPointerType* native_delegate_wrapper_BaseCardboardDevice_OnVREvent_m1653_indirect = (const methodPointerType*)&native_delegate_wrapper_BaseCardboardDevice_OnVREvent_m1653;
extern TypeInfo* BaseVRDevice_t236_il2cpp_TypeInfo_var;
extern TypeInfo* BaseCardboardDevice_t270_il2cpp_TypeInfo_var;
extern const MethodInfo* Queue_1_Enqueue_m3048_MethodInfo_var;
extern "C" void BaseCardboardDevice_OnVREvent_m1653 (Object_t * __this /* static, unused */, int32_t ___eventID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BaseVRDevice_t236_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(176);
		BaseCardboardDevice_t270_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		Queue_1_Enqueue_m3048_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		s_Il2CppMethodIntialized = true;
	}
	BaseCardboardDevice_t270 * V_0 = {0};
	Queue_1_t268 * V_1 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = BaseVRDevice_GetDevice_m1693(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((BaseCardboardDevice_t270 *)IsInst(L_0, BaseCardboardDevice_t270_il2cpp_TypeInfo_var));
		BaseCardboardDevice_t270 * L_1 = V_0;
		NullCheck(L_1);
		Queue_1_t268 * L_2 = (L_1->___eventQueue_30);
		V_1 = L_2;
		Queue_1_t268 * L_3 = V_1;
		Monitor_Enter_m3044(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		BaseCardboardDevice_t270 * L_4 = V_0;
		NullCheck(L_4);
		Queue_1_t268 * L_5 = (L_4->___eventQueue_30);
		int32_t L_6 = ___eventID;
		NullCheck(L_5);
		Queue_1_Enqueue_m3048(L_5, L_6, /*hidden argument*/Queue_1_Enqueue_m3048_MethodInfo_var);
		IL2CPP_LEAVE(0x30, FINALLY_0029);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0029;
	}

FINALLY_0029:
	{ // begin finally (depth: 1)
		Queue_1_t268 * L_7 = V_1;
		Monitor_Exit_m3047(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(41)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(41)
	{
		IL2CPP_JUMP_TBL(0x30, IL_0030)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0030:
	{
		return;
	}
}
// System.Void BaseCardboardDevice::Start(System.Int32,System.Int32,System.Single,System.Single)
extern "C" {void DEFAULT_CALL Start(int32_t, int32_t, float, float);}
extern "C" void BaseCardboardDevice_Start_m1654 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, float ___xdpi, float ___ydpi, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, float, float);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Start;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Start'"));
		}
	}

	// Marshaling of parameter '___width' to native representation

	// Marshaling of parameter '___height' to native representation

	// Marshaling of parameter '___xdpi' to native representation

	// Marshaling of parameter '___ydpi' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___width, ___height, ___xdpi, ___ydpi);

	// Marshaling cleanup of parameter '___width' native representation

	// Marshaling cleanup of parameter '___height' native representation

	// Marshaling cleanup of parameter '___xdpi' native representation

	// Marshaling cleanup of parameter '___ydpi' native representation

}
// System.Void BaseCardboardDevice::SetEventCallback(BaseCardboardDevice/VREventCallback)
extern "C" {void DEFAULT_CALL SetEventCallback(methodPointerType);}
extern "C" void BaseCardboardDevice_SetEventCallback_m1655 (Object_t * __this /* static, unused */, VREventCallback_t267 * ___callback, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (methodPointerType);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SetEventCallback;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SetEventCallback'"));
		}
	}

	// Marshaling of parameter '___callback' to native representation
	methodPointerType ____callback_marshaled = { 0 };
	____callback_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___callback));

	// Native function invocation
	_il2cpp_pinvoke_func(____callback_marshaled);

	// Marshaling cleanup of parameter '___callback' native representation

}
// System.Void BaseCardboardDevice::SetTextureId(System.Int32)
extern "C" {void DEFAULT_CALL SetTextureId(int32_t);}
extern "C" void BaseCardboardDevice_SetTextureId_m1656 (Object_t * __this /* static, unused */, int32_t ___id, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SetTextureId;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SetTextureId'"));
		}
	}

	// Marshaling of parameter '___id' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___id);

	// Marshaling cleanup of parameter '___id' native representation

}
// System.Boolean BaseCardboardDevice::SetDefaultProfile(System.Byte[],System.Int32)
extern "C" {int32_t DEFAULT_CALL SetDefaultProfile(uint8_t*, int32_t);}
extern "C" bool BaseCardboardDevice_SetDefaultProfile_m1657 (Object_t * __this /* static, unused */, ByteU5BU5D_t469* ___uri, int32_t ___size, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SetDefaultProfile;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SetDefaultProfile'"));
		}
	}

	// Marshaling of parameter '___uri' to native representation
	uint8_t* ____uri_marshaled = { 0 };
	____uri_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___uri);

	// Marshaling of parameter '___size' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____uri_marshaled, ___size);

	// Marshaling cleanup of parameter '___uri' native representation

	// Marshaling cleanup of parameter '___size' native representation

	return _return_value;
}
// System.Void BaseCardboardDevice::SetUnityVersion(System.Byte[],System.Int32)
extern "C" {void DEFAULT_CALL SetUnityVersion(uint8_t*, int32_t);}
extern "C" void BaseCardboardDevice_SetUnityVersion_m1658 (Object_t * __this /* static, unused */, ByteU5BU5D_t469* ___version_str, int32_t ___version_length, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint8_t*, int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SetUnityVersion;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SetUnityVersion'"));
		}
	}

	// Marshaling of parameter '___version_str' to native representation
	uint8_t* ____version_str_marshaled = { 0 };
	____version_str_marshaled = il2cpp_codegen_marshal_array<uint8_t>((Il2CppCodeGenArray*)___version_str);

	// Marshaling of parameter '___version_length' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____version_str_marshaled, ___version_length);

	// Marshaling cleanup of parameter '___version_str' native representation

	// Marshaling cleanup of parameter '___version_length' native representation

}
// System.Void BaseCardboardDevice::EnableDistortionCorrection(System.Boolean)
extern "C" {void DEFAULT_CALL EnableDistortionCorrection(int32_t);}
extern "C" void BaseCardboardDevice_EnableDistortionCorrection_m1659 (Object_t * __this /* static, unused */, bool ___enable, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EnableDistortionCorrection;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EnableDistortionCorrection'"));
		}
	}

	// Marshaling of parameter '___enable' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enable);

	// Marshaling cleanup of parameter '___enable' native representation

}
// System.Void BaseCardboardDevice::EnableAutoDriftCorrection(System.Boolean)
extern "C" {void DEFAULT_CALL EnableAutoDriftCorrection(int32_t);}
extern "C" void BaseCardboardDevice_EnableAutoDriftCorrection_m1660 (Object_t * __this /* static, unused */, bool ___enable, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EnableAutoDriftCorrection;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EnableAutoDriftCorrection'"));
		}
	}

	// Marshaling of parameter '___enable' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enable);

	// Marshaling cleanup of parameter '___enable' native representation

}
// System.Void BaseCardboardDevice::EnableElectronicDisplayStabilization(System.Boolean)
extern "C" {void DEFAULT_CALL EnableElectronicDisplayStabilization(int32_t);}
extern "C" void BaseCardboardDevice_EnableElectronicDisplayStabilization_m1661 (Object_t * __this /* static, unused */, bool ___enable, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)EnableElectronicDisplayStabilization;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'EnableElectronicDisplayStabilization'"));
		}
	}

	// Marshaling of parameter '___enable' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enable);

	// Marshaling cleanup of parameter '___enable' native representation

}
// System.Void BaseCardboardDevice::SetNeckModelFactor(System.Single)
extern "C" {void DEFAULT_CALL SetNeckModelFactor(float);}
extern "C" void BaseCardboardDevice_SetNeckModelFactor_m1662 (Object_t * __this /* static, unused */, float ___factor, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)SetNeckModelFactor;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'SetNeckModelFactor'"));
		}
	}

	// Marshaling of parameter '___factor' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___factor);

	// Marshaling cleanup of parameter '___factor' native representation

}
// System.Void BaseCardboardDevice::ResetHeadTracker()
extern "C" {void DEFAULT_CALL ResetHeadTracker();}
extern "C" void BaseCardboardDevice_ResetHeadTracker_m1663 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)ResetHeadTracker;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'ResetHeadTracker'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void BaseCardboardDevice::GetProfile(System.Single[])
extern "C" {void DEFAULT_CALL GetProfile(float*);}
extern "C" void BaseCardboardDevice_GetProfile_m1664 (Object_t * __this /* static, unused */, SingleU5BU5D_t72* ___profile, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GetProfile;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetProfile'"));
		}
	}

	// Marshaling of parameter '___profile' to native representation
	float* ____profile_marshaled = { 0 };
	____profile_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___profile);

	// Native function invocation
	_il2cpp_pinvoke_func(____profile_marshaled);

	// Marshaling cleanup of parameter '___profile' native representation

}
// System.Void BaseCardboardDevice::GetHeadPose(System.Single[])
extern "C" {void DEFAULT_CALL GetHeadPose(float*);}
extern "C" void BaseCardboardDevice_GetHeadPose_m1665 (Object_t * __this /* static, unused */, SingleU5BU5D_t72* ___pose, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GetHeadPose;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetHeadPose'"));
		}
	}

	// Marshaling of parameter '___pose' to native representation
	float* ____pose_marshaled = { 0 };
	____pose_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___pose);

	// Native function invocation
	_il2cpp_pinvoke_func(____pose_marshaled);

	// Marshaling cleanup of parameter '___pose' native representation

}
// System.Void BaseCardboardDevice::GetViewParameters(System.Single[])
extern "C" {void DEFAULT_CALL GetViewParameters(float*);}
extern "C" void BaseCardboardDevice_GetViewParameters_m1666 (Object_t * __this /* static, unused */, SingleU5BU5D_t72* ___viewParams, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (float*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)GetViewParameters;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'GetViewParameters'"));
		}
	}

	// Marshaling of parameter '___viewParams' to native representation
	float* ____viewParams_marshaled = { 0 };
	____viewParams_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___viewParams);

	// Native function invocation
	_il2cpp_pinvoke_func(____viewParams_marshaled);

	// Marshaling cleanup of parameter '___viewParams' native representation

}
// System.Void BaseCardboardDevice::Pause()
extern "C" {void DEFAULT_CALL Pause();}
extern "C" void BaseCardboardDevice_Pause_m1667 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Pause;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Pause'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void BaseCardboardDevice::Resume()
extern "C" {void DEFAULT_CALL Resume();}
extern "C" void BaseCardboardDevice_Resume_m1668 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Resume;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Resume'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void BaseCardboardDevice::Stop()
extern "C" {void DEFAULT_CALL Stop();}
extern "C" void BaseCardboardDevice_Stop_m1669 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)Stop;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'Stop'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
#ifndef _MSC_VER
#else
#endif
// BaseVRDevice/DisplayMetrics
#include "AssemblyU2DCSharp_BaseVRDevice_DisplayMetricsMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_Regex.h"
// System.Version
#include "mscorlib_System_Version.h"
// CardboardiOSDevice
#include "AssemblyU2DCSharp_CardboardiOSDevice.h"
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
// System.Version
#include "mscorlib_System_VersionMethodDeclarations.h"
// UnityEngine.QualitySettings
#include "UnityEngine_UnityEngine_QualitySettingsMethodDeclarations.h"
// CardboardiOSDevice
#include "AssemblyU2DCSharp_CardboardiOSDeviceMethodDeclarations.h"


// System.Void BaseVRDevice::.ctor()
extern TypeInfo* MutablePose3D_t262_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardProfile_t255_il2cpp_TypeInfo_var;
extern "C" void BaseVRDevice__ctor_m1670 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MutablePose3D_t262_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(228);
		CardboardProfile_t255_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	{
		MutablePose3D_t262 * L_0 = (MutablePose3D_t262 *)il2cpp_codegen_object_new (MutablePose3D_t262_il2cpp_TypeInfo_var);
		MutablePose3D__ctor_m1595(L_0, /*hidden argument*/NULL);
		__this->___headPose_1 = L_0;
		MutablePose3D_t262 * L_1 = (MutablePose3D_t262 *)il2cpp_codegen_object_new (MutablePose3D_t262_il2cpp_TypeInfo_var);
		MutablePose3D__ctor_m1595(L_1, /*hidden argument*/NULL);
		__this->___leftEyePose_2 = L_1;
		MutablePose3D_t262 * L_2 = (MutablePose3D_t262 *)il2cpp_codegen_object_new (MutablePose3D_t262_il2cpp_TypeInfo_var);
		MutablePose3D__ctor_m1595(L_2, /*hidden argument*/NULL);
		__this->___rightEyePose_3 = L_2;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CardboardProfile_t255_il2cpp_TypeInfo_var);
		CardboardProfile_t255 * L_3 = ((CardboardProfile_t255_StaticFields*)CardboardProfile_t255_il2cpp_TypeInfo_var->static_fields)->___Default_14;
		NullCheck(L_3);
		CardboardProfile_t255 * L_4 = CardboardProfile_Clone_m1559(L_3, /*hidden argument*/NULL);
		BaseVRDevice_set_Profile_m1673(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BaseVRDevice::.cctor()
extern "C" void BaseVRDevice__cctor_m1671 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// CardboardProfile BaseVRDevice::get_Profile()
extern "C" CardboardProfile_t255 * BaseVRDevice_get_Profile_m1672 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	{
		CardboardProfile_t255 * L_0 = (__this->___U3CProfileU3Ek__BackingField_17);
		return L_0;
	}
}
// System.Void BaseVRDevice::set_Profile(CardboardProfile)
extern "C" void BaseVRDevice_set_Profile_m1673 (BaseVRDevice_t236 * __this, CardboardProfile_t255 * ___value, const MethodInfo* method)
{
	{
		CardboardProfile_t255 * L_0 = ___value;
		__this->___U3CProfileU3Ek__BackingField_17 = L_0;
		return;
	}
}
// BaseVRDevice/DisplayMetrics BaseVRDevice::GetDisplayMetrics()
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* DisplayMetrics_t271_il2cpp_TypeInfo_var;
extern "C" DisplayMetrics_t271  BaseVRDevice_GetDisplayMetrics_m1674 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		DisplayMetrics_t271_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(229);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	DisplayMetrics_t271  V_2 = {0};
	{
		int32_t L_0 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m3049(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Mathf_Min_m3050(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Initobj (DisplayMetrics_t271_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_6 = V_0;
		(&V_2)->___width_0 = L_6;
		int32_t L_7 = V_1;
		(&V_2)->___height_1 = L_7;
		float L_8 = Screen_get_dpi_m2922(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_2)->___xdpi_2 = L_8;
		float L_9 = Screen_get_dpi_m2922(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_2)->___ydpi_3 = L_9;
		DisplayMetrics_t271  L_10 = V_2;
		return L_10;
	}
}
// System.Boolean BaseVRDevice::SupportsNativeDistortionCorrection(System.Collections.Generic.List`1<System.String>)
extern Il2CppCodeGenString* _stringLiteral336;
extern Il2CppCodeGenString* _stringLiteral337;
extern "C" bool BaseVRDevice_SupportsNativeDistortionCorrection_m1675 (BaseVRDevice_t236 * __this, List_1_t468 * ___diagnostics, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral336 = il2cpp_codegen_string_literal_from_index(336);
		_stringLiteral337 = il2cpp_codegen_string_literal_from_index(337);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		V_0 = 1;
		bool L_0 = SystemInfo_get_supportsRenderTextures_m2780(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		List_1_t468 * L_1 = ___diagnostics;
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_1, _stringLiteral336);
		V_0 = 0;
	}

IL_0019:
	{
		bool L_2 = BaseVRDevice_SupportsUnityRenderEvent_m1677(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		List_1_t468 * L_3 = ___diagnostics;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_3, _stringLiteral337);
		V_0 = 0;
	}

IL_0031:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean BaseVRDevice::SupportsNativeUILayer(System.Collections.Generic.List`1<System.String>)
extern "C" bool BaseVRDevice_SupportsNativeUILayer_m1676 (BaseVRDevice_t236 * __this, List_1_t468 * ___diagnostics, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean BaseVRDevice::SupportsUnityRenderEvent()
extern TypeInfo* Regex_t521_il2cpp_TypeInfo_var;
extern TypeInfo* Version_t522_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral338;
extern Il2CppCodeGenString* _stringLiteral339;
extern Il2CppCodeGenString* _stringLiteral340;
extern Il2CppCodeGenString* _stringLiteral341;
extern "C" bool BaseVRDevice_SupportsUnityRenderEvent_m1677 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Regex_t521_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(230);
		Version_t522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(231);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral338 = il2cpp_codegen_string_literal_from_index(338);
		_stringLiteral339 = il2cpp_codegen_string_literal_from_index(339);
		_stringLiteral340 = il2cpp_codegen_string_literal_from_index(340);
		_stringLiteral341 = il2cpp_codegen_string_literal_from_index(341);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 1;
		bool L_0 = Application_get_isMobilePlatform_m3051(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0061;
		}
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			Regex_t521 * L_1 = (Regex_t521 *)il2cpp_codegen_object_new (Regex_t521_il2cpp_TypeInfo_var);
			Regex__ctor_m3052(L_1, _stringLiteral338, /*hidden argument*/NULL);
			String_t* L_2 = Application_get_unityVersion_m3038(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_1);
			String_t* L_3 = Regex_Replace_m3053(L_1, L_2, _stringLiteral339, /*hidden argument*/NULL);
			V_1 = L_3;
			String_t* L_4 = V_1;
			Version_t522 * L_5 = (Version_t522 *)il2cpp_codegen_object_new (Version_t522_il2cpp_TypeInfo_var);
			Version__ctor_m3054(L_5, L_4, /*hidden argument*/NULL);
			Version_t522 * L_6 = (Version_t522 *)il2cpp_codegen_object_new (Version_t522_il2cpp_TypeInfo_var);
			Version__ctor_m3054(L_6, _stringLiteral340, /*hidden argument*/NULL);
			bool L_7 = Version_op_LessThan_m3055(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0042;
			}
		}

IL_0040:
		{
			V_0 = 0;
		}

IL_0042:
		{
			goto IL_0061;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t520 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0047;
		throw e;
	}

CATCH_0047:
	{ // begin catch(System.Object)
		String_t* L_8 = Application_get_unityVersion_m3038(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3031(NULL /*static, unused*/, _stringLiteral341, L_8, /*hidden argument*/NULL);
		Debug_LogWarning_m2781(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		goto IL_0061;
	} // end catch (depth: 1)

IL_0061:
	{
		bool L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.RenderTexture BaseVRDevice::CreateStereoScreen()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* RenderTexture_t15_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral342;
extern Il2CppCodeGenString* _stringLiteral343;
extern Il2CppCodeGenString* _stringLiteral344;
extern "C" RenderTexture_t15 * BaseVRDevice_CreateStereoScreen_m1678 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		RenderTexture_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		_stringLiteral342 = il2cpp_codegen_string_literal_from_index(342);
		_stringLiteral343 = il2cpp_codegen_string_literal_from_index(343);
		_stringLiteral344 = il2cpp_codegen_string_literal_from_index(344);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	RenderTexture_t15 * V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_0 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = Cardboard_get_StereoScreenScale_m1466(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t7 * L_2 = &(__this->___recommendedTextureSize_12);
		float L_3 = (L_2->___x_1);
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_RoundToInt_m2757(NULL /*static, unused*/, ((float)((float)L_3*(float)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t7 * L_6 = &(__this->___recommendedTextureSize_12);
		float L_7 = (L_6->___y_2);
		float L_8 = V_0;
		int32_t L_9 = Mathf_RoundToInt_m2757(NULL /*static, unused*/, ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		V_2 = L_9;
		ObjectU5BU5D_t470* L_10 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 5));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, _stringLiteral342);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 0)) = (Object_t *)_stringLiteral342;
		ObjectU5BU5D_t470* L_11 = L_10;
		int32_t L_12 = V_1;
		int32_t L_13 = L_12;
		Object_t * L_14 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		ArrayElementTypeCheck (L_11, L_14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 1)) = (Object_t *)L_14;
		ObjectU5BU5D_t470* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 2);
		ArrayElementTypeCheck (L_15, _stringLiteral343);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_15, 2)) = (Object_t *)_stringLiteral343;
		ObjectU5BU5D_t470* L_16 = L_15;
		int32_t L_17 = V_2;
		int32_t L_18 = L_17;
		Object_t * L_19 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 3);
		ArrayElementTypeCheck (L_16, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 3)) = (Object_t *)L_19;
		ObjectU5BU5D_t470* L_20 = L_16;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 4);
		ArrayElementTypeCheck (L_20, _stringLiteral344);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 4)) = (Object_t *)_stringLiteral344;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Concat_m3056(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		Debug_Log_m2814(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_1;
		int32_t L_23 = V_2;
		RenderTexture_t15 * L_24 = (RenderTexture_t15 *)il2cpp_codegen_object_new (RenderTexture_t15_il2cpp_TypeInfo_var);
		RenderTexture__ctor_m2783(L_24, L_22, L_23, ((int32_t)24), 7, /*hidden argument*/NULL);
		V_3 = L_24;
		RenderTexture_t15 * L_25 = V_3;
		NullCheck(L_25);
		Texture_set_anisoLevel_m3057(L_25, 0, /*hidden argument*/NULL);
		RenderTexture_t15 * L_26 = V_3;
		int32_t L_27 = QualitySettings_get_antiAliasing_m3058(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_28 = Mathf_Max_m3049(NULL /*static, unused*/, L_27, 1, /*hidden argument*/NULL);
		NullCheck(L_26);
		RenderTexture_set_antiAliasing_m3059(L_26, L_28, /*hidden argument*/NULL);
		RenderTexture_t15 * L_29 = V_3;
		return L_29;
	}
}
// System.Boolean BaseVRDevice::SetDefaultDeviceProfile(System.Uri)
extern "C" bool BaseVRDevice_SetDefaultDeviceProfile_m1679 (BaseVRDevice_t236 * __this, Uri_t237 * ___uri, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void BaseVRDevice::ShowSettingsDialog()
extern "C" void BaseVRDevice_ShowSettingsDialog_m1680 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// Pose3D BaseVRDevice::GetHeadPose()
extern "C" Pose3D_t260 * BaseVRDevice_GetHeadPose_m1681 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	{
		MutablePose3D_t262 * L_0 = (__this->___headPose_1);
		return L_0;
	}
}
// Pose3D BaseVRDevice::GetEyePose(Cardboard/Eye)
extern "C" Pose3D_t260 * BaseVRDevice_GetEyePose_m1682 (BaseVRDevice_t236 * __this, int32_t ___eye, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___eye;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		MutablePose3D_t262 * L_3 = (__this->___leftEyePose_2);
		return L_3;
	}

IL_001b:
	{
		MutablePose3D_t262 * L_4 = (__this->___rightEyePose_3);
		return L_4;
	}

IL_0022:
	{
		return (Pose3D_t260 *)NULL;
	}
}
// UnityEngine.Matrix4x4 BaseVRDevice::GetProjection(Cardboard/Eye,Cardboard/Distortion)
extern "C" Matrix4x4_t242  BaseVRDevice_GetProjection_m1683 (BaseVRDevice_t236 * __this, int32_t ___eye, int32_t ___distortion, const MethodInfo* method)
{
	int32_t V_0 = {0};
	Matrix4x4_t242  G_B6_0 = {0};
	Matrix4x4_t242  G_B10_0 = {0};
	{
		int32_t L_0 = ___eye;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0044;
	}

IL_0014:
	{
		int32_t L_3 = ___distortion;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		Matrix4x4_t242  L_4 = (__this->___leftEyeDistortedProjection_4);
		G_B6_0 = L_4;
		goto IL_002b;
	}

IL_0025:
	{
		Matrix4x4_t242  L_5 = (__this->___leftEyeUndistortedProjection_6);
		G_B6_0 = L_5;
	}

IL_002b:
	{
		return G_B6_0;
	}

IL_002c:
	{
		int32_t L_6 = ___distortion;
		if (L_6)
		{
			goto IL_003d;
		}
	}
	{
		Matrix4x4_t242  L_7 = (__this->___rightEyeDistortedProjection_5);
		G_B10_0 = L_7;
		goto IL_0043;
	}

IL_003d:
	{
		Matrix4x4_t242  L_8 = (__this->___rightEyeUndistortedProjection_7);
		G_B10_0 = L_8;
	}

IL_0043:
	{
		return G_B10_0;
	}

IL_0044:
	{
		Matrix4x4_t242  L_9 = Matrix4x4_get_identity_m3010(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Rect BaseVRDevice::GetViewport(Cardboard/Eye,Cardboard/Distortion)
extern TypeInfo* Rect_t225_il2cpp_TypeInfo_var;
extern "C" Rect_t225  BaseVRDevice_GetViewport_m1684 (BaseVRDevice_t236 * __this, int32_t ___eye, int32_t ___distortion, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t225_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(233);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Rect_t225  V_1 = {0};
	Rect_t225  G_B6_0 = {0};
	Rect_t225  G_B10_0 = {0};
	{
		int32_t L_0 = ___eye;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0044;
	}

IL_0014:
	{
		int32_t L_3 = ___distortion;
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		Rect_t225  L_4 = (__this->___leftEyeDistortedViewport_8);
		G_B6_0 = L_4;
		goto IL_002b;
	}

IL_0025:
	{
		Rect_t225  L_5 = (__this->___leftEyeUndistortedViewport_10);
		G_B6_0 = L_5;
	}

IL_002b:
	{
		return G_B6_0;
	}

IL_002c:
	{
		int32_t L_6 = ___distortion;
		if (L_6)
		{
			goto IL_003d;
		}
	}
	{
		Rect_t225  L_7 = (__this->___rightEyeDistortedViewport_9);
		G_B10_0 = L_7;
		goto IL_0043;
	}

IL_003d:
	{
		Rect_t225  L_8 = (__this->___rightEyeUndistortedViewport_11);
		G_B10_0 = L_8;
	}

IL_0043:
	{
		return G_B10_0;
	}

IL_0044:
	{
		Initobj (Rect_t225_il2cpp_TypeInfo_var, (&V_1));
		Rect_t225  L_9 = V_1;
		return L_9;
	}
}
// System.Void BaseVRDevice::SetTouchCoordinates(System.Int32,System.Int32)
extern "C" void BaseVRDevice_SetTouchCoordinates_m1685 (BaseVRDevice_t236 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BaseVRDevice::OnPause(System.Boolean)
extern "C" void BaseVRDevice_OnPause_m1686 (BaseVRDevice_t236 * __this, bool ___pause, const MethodInfo* method)
{
	{
		bool L_0 = ___pause;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		VirtActionInvoker0::Invoke(23 /* System.Void BaseVRDevice::UpdateScreenData() */, __this);
	}

IL_000c:
	{
		return;
	}
}
// System.Void BaseVRDevice::OnFocus(System.Boolean)
extern "C" void BaseVRDevice_OnFocus_m1687 (BaseVRDevice_t236 * __this, bool ___focus, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BaseVRDevice::OnLevelLoaded(System.Int32)
extern "C" void BaseVRDevice_OnLevelLoaded_m1688 (BaseVRDevice_t236 * __this, int32_t ___level, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BaseVRDevice::OnApplicationQuit()
extern "C" void BaseVRDevice_OnApplicationQuit_m1689 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BaseVRDevice::Destroy()
extern TypeInfo* BaseVRDevice_t236_il2cpp_TypeInfo_var;
extern "C" void BaseVRDevice_Destroy_m1690 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BaseVRDevice_t236_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(176);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((BaseVRDevice_t236_StaticFields*)BaseVRDevice_t236_il2cpp_TypeInfo_var->static_fields)->___device_0;
		if ((!(((Object_t*)(BaseVRDevice_t236 *)L_0) == ((Object_t*)(BaseVRDevice_t236 *)__this))))
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		((BaseVRDevice_t236_StaticFields*)BaseVRDevice_t236_il2cpp_TypeInfo_var->static_fields)->___device_0 = (BaseVRDevice_t236 *)NULL;
	}

IL_0011:
	{
		return;
	}
}
// System.Void BaseVRDevice::ComputeEyesFromProfile()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern TypeInfo* BaseVRDevice_t236_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" void BaseVRDevice_ComputeEyesFromProfile_m1691 (BaseVRDevice_t236 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		BaseVRDevice_t236_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(176);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	Matrix4x4_t242  V_0 = {0};
	SingleU5BU5D_t72* V_1 = {0};
	Matrix4x4_t242  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Matrix4x4_t242 * V_5 = {0};
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	Matrix4x4_t242 * V_9 = {0};
	Matrix4x4_t242 * V_10 = {0};
	{
		Matrix4x4_t242  L_0 = Matrix4x4_get_identity_m3010(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		CardboardProfile_t255 * L_1 = BaseVRDevice_get_Profile_m1672(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Device_t252 * L_2 = &(L_1->___device_1);
		Lenses_t249 * L_3 = &(L_2->___lenses_0);
		float L_4 = (L_3->___separation_3);
		Matrix4x4_set_Item_m2858((&V_0), 0, 3, ((float)((float)((-L_4))/(float)(2.0f))), /*hidden argument*/NULL);
		MutablePose3D_t262 * L_5 = (__this->___leftEyePose_2);
		Matrix4x4_t242  L_6 = V_0;
		NullCheck(L_5);
		MutablePose3D_Set_m1597(L_5, L_6, /*hidden argument*/NULL);
		V_1 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, 4));
		CardboardProfile_t255 * L_7 = BaseVRDevice_get_Profile_m1672(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_8 = V_1;
		NullCheck(L_7);
		CardboardProfile_GetLeftEyeVisibleTanAngles_m1562(L_7, L_8, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_9 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		SingleU5BU5D_t72* L_11 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		int32_t L_12 = 1;
		SingleU5BU5D_t72* L_13 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		int32_t L_14 = 2;
		SingleU5BU5D_t72* L_15 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 3);
		int32_t L_16 = 3;
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		Matrix4x4_t242  L_17 = BaseVRDevice_MakeProjection_m1692(NULL /*static, unused*/, (*(float*)(float*)SZArrayLdElema(L_9, L_10)), (*(float*)(float*)SZArrayLdElema(L_11, L_12)), (*(float*)(float*)SZArrayLdElema(L_13, L_14)), (*(float*)(float*)SZArrayLdElema(L_15, L_16)), (1.0f), (1000.0f), /*hidden argument*/NULL);
		__this->___leftEyeDistortedProjection_4 = L_17;
		CardboardProfile_t255 * L_18 = BaseVRDevice_get_Profile_m1672(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_19 = V_1;
		NullCheck(L_18);
		CardboardProfile_GetLeftEyeNoLensTanAngles_m1563(L_18, L_19, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_20 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		int32_t L_21 = 0;
		SingleU5BU5D_t72* L_22 = V_1;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 1);
		int32_t L_23 = 1;
		SingleU5BU5D_t72* L_24 = V_1;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		int32_t L_25 = 2;
		SingleU5BU5D_t72* L_26 = V_1;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 3);
		int32_t L_27 = 3;
		Matrix4x4_t242  L_28 = BaseVRDevice_MakeProjection_m1692(NULL /*static, unused*/, (*(float*)(float*)SZArrayLdElema(L_20, L_21)), (*(float*)(float*)SZArrayLdElema(L_22, L_23)), (*(float*)(float*)SZArrayLdElema(L_24, L_25)), (*(float*)(float*)SZArrayLdElema(L_26, L_27)), (1.0f), (1000.0f), /*hidden argument*/NULL);
		__this->___leftEyeUndistortedProjection_6 = L_28;
		CardboardProfile_t255 * L_29 = BaseVRDevice_get_Profile_m1672(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_30 = V_1;
		NullCheck(L_29);
		Rect_t225  L_31 = CardboardProfile_GetLeftEyeVisibleScreenRect_m1564(L_29, L_30, /*hidden argument*/NULL);
		__this->___leftEyeUndistortedViewport_10 = L_31;
		Rect_t225  L_32 = (__this->___leftEyeUndistortedViewport_10);
		__this->___leftEyeDistortedViewport_8 = L_32;
		Matrix4x4_t242  L_33 = V_0;
		V_2 = L_33;
		Matrix4x4_t242 * L_34 = (&V_2);
		V_5 = (Matrix4x4_t242 *)L_34;
		int32_t L_35 = 0;
		V_6 = L_35;
		int32_t L_36 = 3;
		V_7 = L_36;
		Matrix4x4_t242 * L_37 = V_5;
		int32_t L_38 = V_6;
		int32_t L_39 = V_7;
		float L_40 = Matrix4x4_get_Item_m2856(L_37, L_38, L_39, /*hidden argument*/NULL);
		V_8 = L_40;
		float L_41 = V_8;
		Matrix4x4_set_Item_m2858(L_34, L_35, L_36, ((float)((float)L_41*(float)(-1.0f))), /*hidden argument*/NULL);
		MutablePose3D_t262 * L_42 = (__this->___rightEyePose_3);
		Matrix4x4_t242  L_43 = V_2;
		NullCheck(L_42);
		MutablePose3D_Set_m1597(L_42, L_43, /*hidden argument*/NULL);
		Matrix4x4_t242  L_44 = (__this->___leftEyeDistortedProjection_4);
		__this->___rightEyeDistortedProjection_5 = L_44;
		Matrix4x4_t242 * L_45 = &(__this->___rightEyeDistortedProjection_5);
		Matrix4x4_t242 * L_46 = L_45;
		V_9 = (Matrix4x4_t242 *)L_46;
		int32_t L_47 = 0;
		V_7 = L_47;
		int32_t L_48 = 2;
		V_6 = L_48;
		Matrix4x4_t242 * L_49 = V_9;
		int32_t L_50 = V_7;
		int32_t L_51 = V_6;
		float L_52 = Matrix4x4_get_Item_m2856(L_49, L_50, L_51, /*hidden argument*/NULL);
		V_8 = L_52;
		float L_53 = V_8;
		Matrix4x4_set_Item_m2858(L_46, L_47, L_48, ((float)((float)L_53*(float)(-1.0f))), /*hidden argument*/NULL);
		Matrix4x4_t242  L_54 = (__this->___leftEyeUndistortedProjection_6);
		__this->___rightEyeUndistortedProjection_7 = L_54;
		Matrix4x4_t242 * L_55 = &(__this->___rightEyeUndistortedProjection_7);
		Matrix4x4_t242 * L_56 = L_55;
		V_10 = (Matrix4x4_t242 *)L_56;
		int32_t L_57 = 0;
		V_6 = L_57;
		int32_t L_58 = 2;
		V_7 = L_58;
		Matrix4x4_t242 * L_59 = V_10;
		int32_t L_60 = V_6;
		int32_t L_61 = V_7;
		float L_62 = Matrix4x4_get_Item_m2856(L_59, L_60, L_61, /*hidden argument*/NULL);
		V_8 = L_62;
		float L_63 = V_8;
		Matrix4x4_set_Item_m2858(L_56, L_57, L_58, ((float)((float)L_63*(float)(-1.0f))), /*hidden argument*/NULL);
		Rect_t225  L_64 = (__this->___leftEyeUndistortedViewport_10);
		__this->___rightEyeUndistortedViewport_11 = L_64;
		Rect_t225 * L_65 = &(__this->___rightEyeUndistortedViewport_11);
		Rect_t225 * L_66 = &(__this->___rightEyeUndistortedViewport_11);
		float L_67 = Rect_get_xMax_m3060(L_66, /*hidden argument*/NULL);
		Rect_set_x_m2862(L_65, ((float)((float)(1.0f)-(float)L_67)), /*hidden argument*/NULL);
		Rect_t225  L_68 = (__this->___rightEyeUndistortedViewport_11);
		__this->___rightEyeDistortedViewport_9 = L_68;
		int32_t L_69 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t225 * L_70 = &(__this->___leftEyeUndistortedViewport_10);
		float L_71 = Rect_get_width_m2798(L_70, /*hidden argument*/NULL);
		Rect_t225 * L_72 = &(__this->___rightEyeDistortedViewport_9);
		float L_73 = Rect_get_width_m2798(L_72, /*hidden argument*/NULL);
		V_3 = ((float)((float)(((float)L_69))*(float)((float)((float)L_71+(float)L_73))));
		int32_t L_74 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t225 * L_75 = &(__this->___leftEyeUndistortedViewport_10);
		float L_76 = Rect_get_height_m2800(L_75, /*hidden argument*/NULL);
		Rect_t225 * L_77 = &(__this->___rightEyeUndistortedViewport_11);
		float L_78 = Rect_get_height_m2800(L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		float L_79 = Mathf_Max_m2937(NULL /*static, unused*/, L_76, L_78, /*hidden argument*/NULL);
		V_4 = ((float)((float)(((float)L_74))*(float)L_79));
		float L_80 = V_3;
		float L_81 = V_4;
		Vector2_t7  L_82 = {0};
		Vector2__ctor_m2714(&L_82, L_80, L_81, /*hidden argument*/NULL);
		__this->___recommendedTextureSize_12 = L_82;
		return;
	}
}
// UnityEngine.Matrix4x4 BaseVRDevice::MakeProjection(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" Matrix4x4_t242  BaseVRDevice_MakeProjection_m1692 (Object_t * __this /* static, unused */, float ___l, float ___t, float ___r, float ___b, float ___n, float ___f, const MethodInfo* method)
{
	Matrix4x4_t242  V_0 = {0};
	{
		Matrix4x4_t242  L_0 = Matrix4x4_get_zero_m3061(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___n;
		float L_2 = ___r;
		float L_3 = ___l;
		Matrix4x4_set_Item_m2858((&V_0), 0, 0, ((float)((float)((float)((float)(2.0f)*(float)L_1))/(float)((float)((float)L_2-(float)L_3)))), /*hidden argument*/NULL);
		float L_4 = ___n;
		float L_5 = ___t;
		float L_6 = ___b;
		Matrix4x4_set_Item_m2858((&V_0), 1, 1, ((float)((float)((float)((float)(2.0f)*(float)L_4))/(float)((float)((float)L_5-(float)L_6)))), /*hidden argument*/NULL);
		float L_7 = ___r;
		float L_8 = ___l;
		float L_9 = ___r;
		float L_10 = ___l;
		Matrix4x4_set_Item_m2858((&V_0), 0, 2, ((float)((float)((float)((float)L_7+(float)L_8))/(float)((float)((float)L_9-(float)L_10)))), /*hidden argument*/NULL);
		float L_11 = ___t;
		float L_12 = ___b;
		float L_13 = ___t;
		float L_14 = ___b;
		Matrix4x4_set_Item_m2858((&V_0), 1, 2, ((float)((float)((float)((float)L_11+(float)L_12))/(float)((float)((float)L_13-(float)L_14)))), /*hidden argument*/NULL);
		float L_15 = ___n;
		float L_16 = ___f;
		float L_17 = ___n;
		float L_18 = ___f;
		Matrix4x4_set_Item_m2858((&V_0), 2, 2, ((float)((float)((float)((float)L_15+(float)L_16))/(float)((float)((float)L_17-(float)L_18)))), /*hidden argument*/NULL);
		float L_19 = ___n;
		float L_20 = ___f;
		float L_21 = ___n;
		float L_22 = ___f;
		Matrix4x4_set_Item_m2858((&V_0), 2, 3, ((float)((float)((float)((float)((float)((float)(2.0f)*(float)L_19))*(float)L_20))/(float)((float)((float)L_21-(float)L_22)))), /*hidden argument*/NULL);
		Matrix4x4_set_Item_m2858((&V_0), 3, 2, (-1.0f), /*hidden argument*/NULL);
		Matrix4x4_t242  L_23 = V_0;
		return L_23;
	}
}
// BaseVRDevice BaseVRDevice::GetDevice()
extern TypeInfo* BaseVRDevice_t236_il2cpp_TypeInfo_var;
extern TypeInfo* CardboardiOSDevice_t272_il2cpp_TypeInfo_var;
extern "C" BaseVRDevice_t236 * BaseVRDevice_GetDevice_m1693 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BaseVRDevice_t236_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(176);
		CardboardiOSDevice_t272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(234);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_0 = ((BaseVRDevice_t236_StaticFields*)BaseVRDevice_t236_il2cpp_TypeInfo_var->static_fields)->___device_0;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		CardboardiOSDevice_t272 * L_1 = (CardboardiOSDevice_t272 *)il2cpp_codegen_object_new (CardboardiOSDevice_t272_il2cpp_TypeInfo_var);
		CardboardiOSDevice__ctor_m1694(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		((BaseVRDevice_t236_StaticFields*)BaseVRDevice_t236_il2cpp_TypeInfo_var->static_fields)->___device_0 = L_1;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(BaseVRDevice_t236_il2cpp_TypeInfo_var);
		BaseVRDevice_t236 * L_2 = ((BaseVRDevice_t236_StaticFields*)BaseVRDevice_t236_il2cpp_TypeInfo_var->static_fields)->___device_0;
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void CardboardiOSDevice::.ctor()
extern "C" void CardboardiOSDevice__ctor_m1694 (CardboardiOSDevice_t272 * __this, const MethodInfo* method)
{
	{
		BaseCardboardDevice__ctor_m1634(__this, /*hidden argument*/NULL);
		return;
	}
}
// BaseVRDevice/DisplayMetrics CardboardiOSDevice::GetDisplayMetrics()
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern TypeInfo* DisplayMetrics_t271_il2cpp_TypeInfo_var;
extern "C" DisplayMetrics_t271  CardboardiOSDevice_GetDisplayMetrics_m1695 (CardboardiOSDevice_t272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		DisplayMetrics_t271_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(229);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	DisplayMetrics_t271  V_3 = {0};
	{
		int32_t L_0 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m3049(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Mathf_Min_m3050(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = CardboardiOSDevice_getScreenDPI_m1718(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_6;
		Initobj (DisplayMetrics_t271_il2cpp_TypeInfo_var, (&V_3));
		int32_t L_7 = V_0;
		(&V_3)->___width_0 = L_7;
		int32_t L_8 = V_1;
		(&V_3)->___height_1 = L_8;
		float L_9 = V_2;
		(&V_3)->___xdpi_2 = L_9;
		float L_10 = V_2;
		(&V_3)->___ydpi_3 = L_10;
		DisplayMetrics_t271  L_11 = V_3;
		return L_11;
	}
}
// System.Boolean CardboardiOSDevice::SupportsNativeDistortionCorrection(System.Collections.Generic.List`1<System.String>)
extern Il2CppCodeGenString* _stringLiteral345;
extern "C" bool CardboardiOSDevice_SupportsNativeDistortionCorrection_m1696 (CardboardiOSDevice_t272 * __this, List_1_t468 * ___diagnostics, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral345 = il2cpp_codegen_string_literal_from_index(345);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	{
		List_1_t468 * L_0 = ___diagnostics;
		bool L_1 = BaseCardboardDevice_SupportsNativeDistortionCorrection_m1635(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = (__this->___isOpenGL_35);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		List_1_t468 * L_3 = ___diagnostics;
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_3, _stringLiteral345);
		V_0 = 0;
	}

IL_0020:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Void CardboardiOSDevice::SetVRModeEnabled(System.Boolean)
extern "C" void CardboardiOSDevice_SetVRModeEnabled_m1697 (CardboardiOSDevice_t272 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled;
		CardboardiOSDevice_setVRModeEnabled_m1708(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardiOSDevice::SetSettingsButtonEnabled(System.Boolean)
extern "C" void CardboardiOSDevice_SetSettingsButtonEnabled_m1698 (CardboardiOSDevice_t272 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled;
		CardboardiOSDevice_setSettingsButtonEnabled_m1710(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardiOSDevice::SetAlignmentMarkerEnabled(System.Boolean)
extern "C" void CardboardiOSDevice_SetAlignmentMarkerEnabled_m1699 (CardboardiOSDevice_t272 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled;
		CardboardiOSDevice_setAlignmentMarkerEnabled_m1711(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardiOSDevice::SetVRBackButtonEnabled(System.Boolean)
extern "C" void CardboardiOSDevice_SetVRBackButtonEnabled_m1700 (CardboardiOSDevice_t272 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		bool L_0 = ___enabled;
		CardboardiOSDevice_setVRBackButtonEnabled_m1712(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardiOSDevice::SetShowVrBackButtonOnlyInVR(System.Boolean)
extern "C" void CardboardiOSDevice_SetShowVrBackButtonOnlyInVR_m1701 (CardboardiOSDevice_t272 * __this, bool ___only, const MethodInfo* method)
{
	{
		bool L_0 = ___only;
		CardboardiOSDevice_setShowVrBackButtonOnlyInVR_m1709(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardboardiOSDevice::SetTapIsTrigger(System.Boolean)
extern "C" void CardboardiOSDevice_SetTapIsTrigger_m1702 (CardboardiOSDevice_t272 * __this, bool ___enabled, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean CardboardiOSDevice::SetDefaultDeviceProfile(System.Uri)
extern "C" bool CardboardiOSDevice_SetDefaultDeviceProfile_m1703 (CardboardiOSDevice_t272 * __this, Uri_t237 * ___uri, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Uri_t237 * L_0 = ___uri;
		bool L_1 = BaseCardboardDevice_SetDefaultDeviceProfile_m1640(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		CardboardiOSDevice_setOnboardingDone_m1714(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0013:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void CardboardiOSDevice::Init()
extern TypeInfo* Cardboard_t233_il2cpp_TypeInfo_var;
extern "C" void CardboardiOSDevice_Init_m1704 (CardboardiOSDevice_t272 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Cardboard_t233_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(163);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = CardboardiOSDevice_isOpenGLAPI_m1707(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___isOpenGL_35 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Cardboard_t233_il2cpp_TypeInfo_var);
		Cardboard_t233 * L_1 = Cardboard_get_SDK_m1440(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Cardboard_get_SyncWithCardboardApp_m1460(L_1, /*hidden argument*/NULL);
		CardboardiOSDevice_setSyncWithCardboardEnabled_m1713(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		BaseCardboardDevice_Init_m1641(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< bool >::Invoke(14 /* System.Void BaseCardboardDevice::SetAutoDriftCorrectionEnabled(System.Boolean) */, __this, 0);
		return;
	}
}
// System.Void CardboardiOSDevice::OnFocus(System.Boolean)
extern "C" void CardboardiOSDevice_OnFocus_m1705 (CardboardiOSDevice_t272 * __this, bool ___focus, const MethodInfo* method)
{
	{
		bool L_0 = ___focus;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		bool L_1 = (__this->___debugOnboarding_36);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		bool L_2 = CardboardiOSDevice_isOnboardingDone_m1715(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0027;
		}
	}

IL_001b:
	{
		__this->___debugOnboarding_36 = 0;
		CardboardiOSDevice_launchOnboardingDialog_m1717(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void CardboardiOSDevice::ShowSettingsDialog()
extern "C" void CardboardiOSDevice_ShowSettingsDialog_m1706 (CardboardiOSDevice_t272 * __this, const MethodInfo* method)
{
	{
		CardboardiOSDevice_launchSettingsDialog_m1716(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CardboardiOSDevice::isOpenGLAPI()
extern "C" {int32_t DEFAULT_CALL isOpenGLAPI();}
extern "C" bool CardboardiOSDevice_isOpenGLAPI_m1707 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)isOpenGLAPI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'isOpenGLAPI'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void CardboardiOSDevice::setVRModeEnabled(System.Boolean)
extern "C" {void DEFAULT_CALL setVRModeEnabled(int32_t);}
extern "C" void CardboardiOSDevice_setVRModeEnabled_m1708 (Object_t * __this /* static, unused */, bool ___enabled, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setVRModeEnabled;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setVRModeEnabled'"));
		}
	}

	// Marshaling of parameter '___enabled' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enabled);

	// Marshaling cleanup of parameter '___enabled' native representation

}
// System.Void CardboardiOSDevice::setShowVrBackButtonOnlyInVR(System.Boolean)
extern "C" {void DEFAULT_CALL setShowVrBackButtonOnlyInVR(int32_t);}
extern "C" void CardboardiOSDevice_setShowVrBackButtonOnlyInVR_m1709 (Object_t * __this /* static, unused */, bool ___only, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setShowVrBackButtonOnlyInVR;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setShowVrBackButtonOnlyInVR'"));
		}
	}

	// Marshaling of parameter '___only' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___only);

	// Marshaling cleanup of parameter '___only' native representation

}
// System.Void CardboardiOSDevice::setSettingsButtonEnabled(System.Boolean)
extern "C" {void DEFAULT_CALL setSettingsButtonEnabled(int32_t);}
extern "C" void CardboardiOSDevice_setSettingsButtonEnabled_m1710 (Object_t * __this /* static, unused */, bool ___enabled, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setSettingsButtonEnabled;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSettingsButtonEnabled'"));
		}
	}

	// Marshaling of parameter '___enabled' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enabled);

	// Marshaling cleanup of parameter '___enabled' native representation

}
// System.Void CardboardiOSDevice::setAlignmentMarkerEnabled(System.Boolean)
extern "C" {void DEFAULT_CALL setAlignmentMarkerEnabled(int32_t);}
extern "C" void CardboardiOSDevice_setAlignmentMarkerEnabled_m1711 (Object_t * __this /* static, unused */, bool ___enabled, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setAlignmentMarkerEnabled;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setAlignmentMarkerEnabled'"));
		}
	}

	// Marshaling of parameter '___enabled' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enabled);

	// Marshaling cleanup of parameter '___enabled' native representation

}
// System.Void CardboardiOSDevice::setVRBackButtonEnabled(System.Boolean)
extern "C" {void DEFAULT_CALL setVRBackButtonEnabled(int32_t);}
extern "C" void CardboardiOSDevice_setVRBackButtonEnabled_m1712 (Object_t * __this /* static, unused */, bool ___enabled, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setVRBackButtonEnabled;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setVRBackButtonEnabled'"));
		}
	}

	// Marshaling of parameter '___enabled' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enabled);

	// Marshaling cleanup of parameter '___enabled' native representation

}
// System.Void CardboardiOSDevice::setSyncWithCardboardEnabled(System.Boolean)
extern "C" {void DEFAULT_CALL setSyncWithCardboardEnabled(int32_t);}
extern "C" void CardboardiOSDevice_setSyncWithCardboardEnabled_m1713 (Object_t * __this /* static, unused */, bool ___enabled, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setSyncWithCardboardEnabled;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSyncWithCardboardEnabled'"));
		}
	}

	// Marshaling of parameter '___enabled' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___enabled);

	// Marshaling cleanup of parameter '___enabled' native representation

}
// System.Void CardboardiOSDevice::setOnboardingDone()
extern "C" {void DEFAULT_CALL setOnboardingDone();}
extern "C" void CardboardiOSDevice_setOnboardingDone_m1714 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setOnboardingDone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setOnboardingDone'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Boolean CardboardiOSDevice::isOnboardingDone()
extern "C" {int32_t DEFAULT_CALL isOnboardingDone();}
extern "C" bool CardboardiOSDevice_isOnboardingDone_m1715 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)isOnboardingDone;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'isOnboardingDone'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void CardboardiOSDevice::launchSettingsDialog()
extern "C" {void DEFAULT_CALL launchSettingsDialog();}
extern "C" void CardboardiOSDevice_launchSettingsDialog_m1716 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)launchSettingsDialog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'launchSettingsDialog'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void CardboardiOSDevice::launchOnboardingDialog()
extern "C" {void DEFAULT_CALL launchOnboardingDialog();}
extern "C" void CardboardiOSDevice_launchOnboardingDialog_m1717 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)launchOnboardingDialog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'launchOnboardingDialog'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Single CardboardiOSDevice::getScreenDPI()
extern "C" {float DEFAULT_CALL getScreenDPI();}
extern "C" float CardboardiOSDevice_getScreenDPI_m1718 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)getScreenDPI;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'getScreenDPI'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	float _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// MNFeaturePreview
#include "AssemblyU2DCSharp_MNFeaturePreview.h"
#ifndef _MSC_VER
#else
#endif
// MNFeaturePreview
#include "AssemblyU2DCSharp_MNFeaturePreviewMethodDeclarations.h"

// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleState.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.GUIStyleState
#include "UnityEngine_UnityEngine_GUIStyleStateMethodDeclarations.h"


// System.Void MNFeaturePreview::.ctor()
extern "C" void MNFeaturePreview__ctor_m1719 (MNFeaturePreview_t274 * __this, const MethodInfo* method)
{
	{
		__this->___buttonWidth_3 = ((int32_t)200);
		__this->___buttonHeight_4 = ((int32_t)50);
		__this->___StartY_5 = (20.0f);
		__this->___StartX_6 = (10.0f);
		__this->___XStartPos_7 = (10.0f);
		__this->___YStartPos_8 = (10.0f);
		__this->___XButtonStep_9 = (220.0f);
		__this->___YButtonStep_10 = (60.0f);
		__this->___YLableStep_11 = (40.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNFeaturePreview::InitStyles()
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern "C" void MNFeaturePreview_InitStyles_m1720 (MNFeaturePreview_t274 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		s_Il2CppMethodIntialized = true;
	}
	{
		GUIStyle_t273 * L_0 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_0, /*hidden argument*/NULL);
		__this->___style_2 = L_0;
		GUIStyle_t273 * L_1 = (__this->___style_2);
		NullCheck(L_1);
		GUIStyleState_t523 * L_2 = GUIStyle_get_normal_m3063(L_1, /*hidden argument*/NULL);
		Color_t6  L_3 = Color_get_white_m2920(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		GUIStyleState_set_textColor_m3064(L_2, L_3, /*hidden argument*/NULL);
		GUIStyle_t273 * L_4 = (__this->___style_2);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m3065(L_4, ((int32_t)16), /*hidden argument*/NULL);
		GUIStyle_t273 * L_5 = (__this->___style_2);
		NullCheck(L_5);
		GUIStyle_set_fontStyle_m3066(L_5, 3, /*hidden argument*/NULL);
		GUIStyle_t273 * L_6 = (__this->___style_2);
		NullCheck(L_6);
		GUIStyle_set_alignment_m3067(L_6, 0, /*hidden argument*/NULL);
		GUIStyle_t273 * L_7 = (__this->___style_2);
		NullCheck(L_7);
		GUIStyle_set_wordWrap_m3068(L_7, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNFeaturePreview::Start()
extern "C" void MNFeaturePreview_Start_m1721 (MNFeaturePreview_t274 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(4 /* System.Void MNFeaturePreview::InitStyles() */, __this);
		return;
	}
}
// System.Void MNFeaturePreview::UpdateToStartPos()
extern "C" void MNFeaturePreview_UpdateToStartPos_m1722 (MNFeaturePreview_t274 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___YStartPos_8);
		__this->___StartY_5 = L_0;
		float L_1 = (__this->___XStartPos_7);
		__this->___StartX_6 = L_1;
		return;
	}
}
// MNUseExample
#include "AssemblyU2DCSharp_MNUseExample.h"
#ifndef _MSC_VER
#else
#endif
// MNUseExample
#include "AssemblyU2DCSharp_MNUseExampleMethodDeclarations.h"

// MobileNativeDialog
#include "AssemblyU2DCSharp_MobileNativeDialog.h"
// MobileNativeRateUs
#include "AssemblyU2DCSharp_MobileNativeRateUs.h"
// MobileNativeMessage
#include "AssemblyU2DCSharp_MobileNativeMessage.h"
// System.Action`1<MNDialogResult>
#include "mscorlib_System_Action_1_gen.h"
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"
// System.Enum
#include "mscorlib_System_Enum.h"
// MobileNativeDialog
#include "AssemblyU2DCSharp_MobileNativeDialogMethodDeclarations.h"
// MobileNativeRateUs
#include "AssemblyU2DCSharp_MobileNativeRateUsMethodDeclarations.h"
// System.Action`1<MNDialogResult>
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
// MobileNativeMessage
#include "AssemblyU2DCSharp_MobileNativeMessageMethodDeclarations.h"
// MNP
#include "AssemblyU2DCSharp_MNPMethodDeclarations.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"


// System.Void MNUseExample::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral346;
extern "C" void MNUseExample__ctor_m1723 (MNUseExample_t275 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral346 = il2cpp_codegen_string_literal_from_index(346);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___appleId_12 = L_0;
		__this->___apdroidAppUrl_13 = _stringLiteral346;
		MNFeaturePreview__ctor_m1719(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNUseExample::Awake()
extern TypeInfo* MobileNativeDialog_t279_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral347;
extern Il2CppCodeGenString* _stringLiteral348;
extern "C" void MNUseExample_Awake_m1724 (MNUseExample_t275 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeDialog_t279_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(236);
		_stringLiteral347 = il2cpp_codegen_string_literal_from_index(347);
		_stringLiteral348 = il2cpp_codegen_string_literal_from_index(348);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeDialog_t279 * V_0 = {0};
	{
		MobileNativeDialog_t279 * L_0 = (MobileNativeDialog_t279 *)il2cpp_codegen_object_new (MobileNativeDialog_t279_il2cpp_TypeInfo_var);
		MobileNativeDialog__ctor_m1735(L_0, _stringLiteral347, _stringLiteral348, /*hidden argument*/NULL);
		V_0 = L_0;
		MNUseExample_OnMessageClose_m1729(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNUseExample::OnGUI()
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern TypeInfo* MobileNativeRateUs_t281_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern TypeInfo* MobileNativeDialog_t279_il2cpp_TypeInfo_var;
extern TypeInfo* MobileNativeMessage_t280_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern const MethodInfo* MNUseExample_OnRatePopUpClose_m1727_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern const MethodInfo* MNUseExample_OnDialogClose_m1728_MethodInfo_var;
extern const MethodInfo* MNUseExample_OnMessageClose_m1729_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral349;
extern Il2CppCodeGenString* _stringLiteral350;
extern Il2CppCodeGenString* _stringLiteral351;
extern Il2CppCodeGenString* _stringLiteral352;
extern Il2CppCodeGenString* _stringLiteral353;
extern Il2CppCodeGenString* _stringLiteral347;
extern Il2CppCodeGenString* _stringLiteral348;
extern Il2CppCodeGenString* _stringLiteral354;
extern Il2CppCodeGenString* _stringLiteral355;
extern Il2CppCodeGenString* _stringLiteral356;
extern Il2CppCodeGenString* _stringLiteral357;
extern Il2CppCodeGenString* _stringLiteral358;
extern Il2CppCodeGenString* _stringLiteral359;
extern Il2CppCodeGenString* _stringLiteral360;
extern "C" void MNUseExample_OnGUI_m1725 (MNUseExample_t275 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		MobileNativeRateUs_t281_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeDialog_t279_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(236);
		MobileNativeMessage_t280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(240);
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		MNUseExample_OnRatePopUpClose_m1727_MethodInfo_var = il2cpp_codegen_method_info_from_index(51);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		MNUseExample_OnDialogClose_m1728_MethodInfo_var = il2cpp_codegen_method_info_from_index(53);
		MNUseExample_OnMessageClose_m1729_MethodInfo_var = il2cpp_codegen_method_info_from_index(54);
		_stringLiteral349 = il2cpp_codegen_string_literal_from_index(349);
		_stringLiteral350 = il2cpp_codegen_string_literal_from_index(350);
		_stringLiteral351 = il2cpp_codegen_string_literal_from_index(351);
		_stringLiteral352 = il2cpp_codegen_string_literal_from_index(352);
		_stringLiteral353 = il2cpp_codegen_string_literal_from_index(353);
		_stringLiteral347 = il2cpp_codegen_string_literal_from_index(347);
		_stringLiteral348 = il2cpp_codegen_string_literal_from_index(348);
		_stringLiteral354 = il2cpp_codegen_string_literal_from_index(354);
		_stringLiteral355 = il2cpp_codegen_string_literal_from_index(355);
		_stringLiteral356 = il2cpp_codegen_string_literal_from_index(356);
		_stringLiteral357 = il2cpp_codegen_string_literal_from_index(357);
		_stringLiteral358 = il2cpp_codegen_string_literal_from_index(358);
		_stringLiteral359 = il2cpp_codegen_string_literal_from_index(359);
		_stringLiteral360 = il2cpp_codegen_string_literal_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeRateUs_t281 * V_0 = {0};
	MobileNativeDialog_t279 * V_1 = {0};
	MobileNativeMessage_t280 * V_2 = {0};
	{
		MNFeaturePreview_UpdateToStartPos_m1722(__this, /*hidden argument*/NULL);
		float L_0 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_1 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		int32_t L_2 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t225  L_3 = {0};
		Rect__ctor_m2802(&L_3, L_0, L_1, (((float)L_2)), (40.0f), /*hidden argument*/NULL);
		GUIStyle_t273 * L_4 = (((MNFeaturePreview_t274 *)__this)->___style_2);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_Label_m3069(NULL /*static, unused*/, L_3, _stringLiteral349, L_4, /*hidden argument*/NULL);
		float L_5 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		float L_6 = (((MNFeaturePreview_t274 *)__this)->___YLableStep_11);
		((MNFeaturePreview_t274 *)__this)->___StartY_5 = ((float)((float)L_5+(float)L_6));
		float L_7 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_8 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		int32_t L_9 = (((MNFeaturePreview_t274 *)__this)->___buttonWidth_3);
		int32_t L_10 = (((MNFeaturePreview_t274 *)__this)->___buttonHeight_4);
		Rect_t225  L_11 = {0};
		Rect__ctor_m2802(&L_11, L_7, L_8, (((float)L_9)), (((float)L_10)), /*hidden argument*/NULL);
		bool L_12 = GUI_Button_m3070(NULL /*static, unused*/, L_11, _stringLiteral350, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00c3;
		}
	}
	{
		MobileNativeRateUs_t281 * L_13 = (MobileNativeRateUs_t281 *)il2cpp_codegen_object_new (MobileNativeRateUs_t281_il2cpp_TypeInfo_var);
		MobileNativeRateUs__ctor_m1745(L_13, _stringLiteral351, _stringLiteral352, /*hidden argument*/NULL);
		V_0 = L_13;
		MobileNativeRateUs_t281 * L_14 = V_0;
		String_t* L_15 = (__this->___appleId_12);
		NullCheck(L_14);
		MobileNativeRateUs_SetAppleId_m1748(L_14, L_15, /*hidden argument*/NULL);
		MobileNativeRateUs_t281 * L_16 = V_0;
		String_t* L_17 = (__this->___apdroidAppUrl_13);
		NullCheck(L_16);
		MobileNativeRateUs_SetAndroidAppUrl_m1747(L_16, L_17, /*hidden argument*/NULL);
		MobileNativeRateUs_t281 * L_18 = V_0;
		MobileNativeRateUs_t281 * L_19 = L_18;
		NullCheck(L_19);
		Action_1_t277 * L_20 = (L_19->___OnComplete_7);
		IntPtr_t L_21 = { (void*)MNUseExample_OnRatePopUpClose_m1727_MethodInfo_var };
		Action_1_t277 * L_22 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_22, __this, L_21, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		Delegate_t480 * L_23 = Delegate_Combine_m2775(NULL /*static, unused*/, L_20, L_22, /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->___OnComplete_7 = ((Action_1_t277 *)Castclass(L_23, Action_1_t277_il2cpp_TypeInfo_var));
		MobileNativeRateUs_t281 * L_24 = V_0;
		NullCheck(L_24);
		MobileNativeRateUs_Start_m1749(L_24, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		float L_25 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_26 = (((MNFeaturePreview_t274 *)__this)->___XButtonStep_9);
		((MNFeaturePreview_t274 *)__this)->___StartX_6 = ((float)((float)L_25+(float)L_26));
		float L_27 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_28 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		int32_t L_29 = (((MNFeaturePreview_t274 *)__this)->___buttonWidth_3);
		int32_t L_30 = (((MNFeaturePreview_t274 *)__this)->___buttonHeight_4);
		Rect_t225  L_31 = {0};
		Rect__ctor_m2802(&L_31, L_27, L_28, (((float)L_29)), (((float)L_30)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		bool L_32 = GUI_Button_m3070(NULL /*static, unused*/, L_31, _stringLiteral353, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0136;
		}
	}
	{
		MobileNativeDialog_t279 * L_33 = (MobileNativeDialog_t279 *)il2cpp_codegen_object_new (MobileNativeDialog_t279_il2cpp_TypeInfo_var);
		MobileNativeDialog__ctor_m1735(L_33, _stringLiteral347, _stringLiteral348, /*hidden argument*/NULL);
		V_1 = L_33;
		MobileNativeDialog_t279 * L_34 = V_1;
		MobileNativeDialog_t279 * L_35 = L_34;
		NullCheck(L_35);
		Action_1_t277 * L_36 = (L_35->___OnComplete_0);
		IntPtr_t L_37 = { (void*)MNUseExample_OnDialogClose_m1728_MethodInfo_var };
		Action_1_t277 * L_38 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_38, __this, L_37, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		Delegate_t480 * L_39 = Delegate_Combine_m2775(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		NullCheck(L_35);
		L_35->___OnComplete_0 = ((Action_1_t277 *)Castclass(L_39, Action_1_t277_il2cpp_TypeInfo_var));
	}

IL_0136:
	{
		float L_40 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_41 = (((MNFeaturePreview_t274 *)__this)->___XButtonStep_9);
		((MNFeaturePreview_t274 *)__this)->___StartX_6 = ((float)((float)L_40+(float)L_41));
		float L_42 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_43 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		int32_t L_44 = (((MNFeaturePreview_t274 *)__this)->___buttonWidth_3);
		int32_t L_45 = (((MNFeaturePreview_t274 *)__this)->___buttonHeight_4);
		Rect_t225  L_46 = {0};
		Rect__ctor_m2802(&L_46, L_42, L_43, (((float)L_44)), (((float)L_45)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		bool L_47 = GUI_Button_m3070(NULL /*static, unused*/, L_46, _stringLiteral354, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01a9;
		}
	}
	{
		MobileNativeMessage_t280 * L_48 = (MobileNativeMessage_t280 *)il2cpp_codegen_object_new (MobileNativeMessage_t280_il2cpp_TypeInfo_var);
		MobileNativeMessage__ctor_m1740(L_48, _stringLiteral355, _stringLiteral356, /*hidden argument*/NULL);
		V_2 = L_48;
		MobileNativeMessage_t280 * L_49 = V_2;
		MobileNativeMessage_t280 * L_50 = L_49;
		NullCheck(L_50);
		Action_t238 * L_51 = (L_50->___OnComplete_0);
		IntPtr_t L_52 = { (void*)MNUseExample_OnMessageClose_m1729_MethodInfo_var };
		Action_t238 * L_53 = (Action_t238 *)il2cpp_codegen_object_new (Action_t238_il2cpp_TypeInfo_var);
		Action__ctor_m3072(L_53, __this, L_52, /*hidden argument*/NULL);
		Delegate_t480 * L_54 = Delegate_Combine_m2775(NULL /*static, unused*/, L_51, L_53, /*hidden argument*/NULL);
		NullCheck(L_50);
		L_50->___OnComplete_0 = ((Action_t238 *)Castclass(L_54, Action_t238_il2cpp_TypeInfo_var));
	}

IL_01a9:
	{
		float L_55 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		float L_56 = (((MNFeaturePreview_t274 *)__this)->___YButtonStep_10);
		((MNFeaturePreview_t274 *)__this)->___StartY_5 = ((float)((float)L_55+(float)L_56));
		float L_57 = (((MNFeaturePreview_t274 *)__this)->___XStartPos_7);
		((MNFeaturePreview_t274 *)__this)->___StartX_6 = L_57;
		float L_58 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_59 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		int32_t L_60 = (((MNFeaturePreview_t274 *)__this)->___buttonWidth_3);
		int32_t L_61 = (((MNFeaturePreview_t274 *)__this)->___buttonHeight_4);
		Rect_t225  L_62 = {0};
		Rect__ctor_m2802(&L_62, L_58, L_59, (((float)L_60)), (((float)L_61)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		bool L_63 = GUI_Button_m3070(NULL /*static, unused*/, L_62, _stringLiteral357, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0215;
		}
	}
	{
		MNP_ShowPreloader_m1731(NULL /*static, unused*/, _stringLiteral358, _stringLiteral347, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m3073(__this, _stringLiteral359, (3.0f), /*hidden argument*/NULL);
	}

IL_0215:
	{
		float L_64 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_65 = (((MNFeaturePreview_t274 *)__this)->___XButtonStep_9);
		((MNFeaturePreview_t274 *)__this)->___StartX_6 = ((float)((float)L_64+(float)L_65));
		float L_66 = (((MNFeaturePreview_t274 *)__this)->___StartX_6);
		float L_67 = (((MNFeaturePreview_t274 *)__this)->___StartY_5);
		int32_t L_68 = (((MNFeaturePreview_t274 *)__this)->___buttonWidth_3);
		int32_t L_69 = (((MNFeaturePreview_t274 *)__this)->___buttonHeight_4);
		Rect_t225  L_70 = {0};
		Rect__ctor_m2802(&L_70, L_66, L_67, (((float)L_68)), (((float)L_69)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		bool L_71 = GUI_Button_m3070(NULL /*static, unused*/, L_70, _stringLiteral360, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_025b;
		}
	}
	{
		MNP_HidePreloader_m1732(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_025b:
	{
		return;
	}
}
// System.Void MNUseExample::OnPreloaderTimeOut()
extern "C" void MNUseExample_OnPreloaderTimeOut_m1726 (MNUseExample_t275 * __this, const MethodInfo* method)
{
	{
		MNP_HidePreloader_m1732(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNUseExample::OnRatePopUpClose(MNDialogResult)
extern TypeInfo* MNDialogResult_t282_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MobileNativeMessage_t280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral361;
extern Il2CppCodeGenString* _stringLiteral362;
extern Il2CppCodeGenString* _stringLiteral363;
extern Il2CppCodeGenString* _stringLiteral364;
extern Il2CppCodeGenString* _stringLiteral365;
extern "C" void MNUseExample_OnRatePopUpClose_m1727 (MNUseExample_t275 * __this, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MNDialogResult_t282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(237);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		MobileNativeMessage_t280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(240);
		_stringLiteral361 = il2cpp_codegen_string_literal_from_index(361);
		_stringLiteral362 = il2cpp_codegen_string_literal_from_index(362);
		_stringLiteral363 = il2cpp_codegen_string_literal_from_index(363);
		_stringLiteral364 = il2cpp_codegen_string_literal_from_index(364);
		_stringLiteral365 = il2cpp_codegen_string_literal_from_index(365);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___result;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 0)
		{
			goto IL_001b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 1)
		{
			goto IL_002a;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 2)
		{
			goto IL_0039;
		}
	}
	{
		goto IL_0048;
	}

IL_001b:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral361, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_002a:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral362, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0039:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral363, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0048:
	{
		int32_t L_2 = ___result;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(MNDialogResult_t282_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3031(NULL /*static, unused*/, L_5, _stringLiteral365, /*hidden argument*/NULL);
		MobileNativeMessage_t280 * L_7 = (MobileNativeMessage_t280 *)il2cpp_codegen_object_new (MobileNativeMessage_t280_il2cpp_TypeInfo_var);
		MobileNativeMessage__ctor_m1740(L_7, _stringLiteral364, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNUseExample::OnDialogClose(MNDialogResult)
extern TypeInfo* MNDialogResult_t282_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MobileNativeMessage_t280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral366;
extern Il2CppCodeGenString* _stringLiteral367;
extern Il2CppCodeGenString* _stringLiteral364;
extern Il2CppCodeGenString* _stringLiteral365;
extern "C" void MNUseExample_OnDialogClose_m1728 (MNUseExample_t275 * __this, int32_t ___result, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MNDialogResult_t282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(237);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		MobileNativeMessage_t280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(240);
		_stringLiteral366 = il2cpp_codegen_string_literal_from_index(366);
		_stringLiteral367 = il2cpp_codegen_string_literal_from_index(367);
		_stringLiteral364 = il2cpp_codegen_string_literal_from_index(364);
		_stringLiteral365 = il2cpp_codegen_string_literal_from_index(365);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___result;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0023;
		}
	}
	{
		goto IL_0032;
	}

IL_0014:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral366, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0023:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral367, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_0032:
	{
		int32_t L_3 = ___result;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(MNDialogResult_t282_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_5);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m3031(NULL /*static, unused*/, L_6, _stringLiteral365, /*hidden argument*/NULL);
		MobileNativeMessage_t280 * L_8 = (MobileNativeMessage_t280 *)il2cpp_codegen_object_new (MobileNativeMessage_t280_il2cpp_TypeInfo_var);
		MobileNativeMessage__ctor_m1740(L_8, _stringLiteral364, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNUseExample::OnMessageClose()
extern TypeInfo* MobileNativeMessage_t280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral347;
extern Il2CppCodeGenString* _stringLiteral368;
extern "C" void MNUseExample_OnMessageClose_m1729 (MNUseExample_t275 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeMessage_t280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(240);
		_stringLiteral347 = il2cpp_codegen_string_literal_from_index(347);
		_stringLiteral368 = il2cpp_codegen_string_literal_from_index(368);
		s_Il2CppMethodIntialized = true;
	}
	{
		MobileNativeMessage_t280 * L_0 = (MobileNativeMessage_t280 *)il2cpp_codegen_object_new (MobileNativeMessage_t280_il2cpp_TypeInfo_var);
		MobileNativeMessage__ctor_m1740(L_0, _stringLiteral347, _stringLiteral368, /*hidden argument*/NULL);
		return;
	}
}
// MNP
#include "AssemblyU2DCSharp_MNP.h"
#ifndef _MSC_VER
#else
#endif

// MNIOSNative
#include "AssemblyU2DCSharp_MNIOSNativeMethodDeclarations.h"


// System.Void MNP::.ctor()
extern "C" void MNP__ctor_m1730 (MNP_t276 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNP::ShowPreloader(System.String,System.String)
extern "C" void MNP_ShowPreloader_m1731 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	{
		MNIOSNative_ShowPreloader_m1810(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNP::HidePreloader()
extern "C" void MNP_HidePreloader_m1732 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		MNIOSNative_HidePreloader_m1811(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// MNPopup
#include "AssemblyU2DCSharp_MNPopup.h"
#ifndef _MSC_VER
#else
#endif
// MNPopup
#include "AssemblyU2DCSharp_MNPopupMethodDeclarations.h"



// System.Void MNPopup::.ctor()
extern TypeInfo* MNPopup_t278_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MNPopup_U3COnCompleteU3Em__3_m1734_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern "C" void MNPopup__ctor_m1733 (MNPopup_t278 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MNPopup_t278_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(241);
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MNPopup_U3COnCompleteU3Em__3_m1734_MethodInfo_var = il2cpp_codegen_method_info_from_index(55);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	MNPopup_t278 * G_B2_0 = {0};
	MNPopup_t278 * G_B1_0 = {0};
	{
		Action_1_t277 * L_0 = ((MNPopup_t278_StaticFields*)MNPopup_t278_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)MNPopup_U3COnCompleteU3Em__3_m1734_MethodInfo_var };
		Action_1_t277 * L_2 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		((MNPopup_t278_StaticFields*)MNPopup_t278_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t277 * L_3 = ((MNPopup_t278_StaticFields*)MNPopup_t278_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache3_5;
		NullCheck(G_B2_0);
		G_B2_0->___OnComplete_4 = L_3;
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNPopup::<OnComplete>m__3(MNDialogResult)
extern "C" void MNPopup_U3COnCompleteU3Em__3_m1734 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// MNIOSDialog
#include "AssemblyU2DCSharp_MNIOSDialog.h"
// MNIOSDialog
#include "AssemblyU2DCSharp_MNIOSDialogMethodDeclarations.h"


// System.Void MobileNativeDialog::.ctor(System.String,System.String)
extern TypeInfo* MobileNativeDialog_t279_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeDialog_U3COnCompleteU3Em__4_m1739_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral369;
extern Il2CppCodeGenString* _stringLiteral370;
extern "C" void MobileNativeDialog__ctor_m1735 (MobileNativeDialog_t279 * __this, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeDialog_t279_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(236);
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeDialog_U3COnCompleteU3Em__4_m1739_MethodInfo_var = il2cpp_codegen_method_info_from_index(56);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		_stringLiteral369 = il2cpp_codegen_string_literal_from_index(369);
		_stringLiteral370 = il2cpp_codegen_string_literal_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeDialog_t279 * G_B2_0 = {0};
	MobileNativeDialog_t279 * G_B1_0 = {0};
	{
		Action_1_t277 * L_0 = ((MobileNativeDialog_t279_StaticFields*)MobileNativeDialog_t279_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)MobileNativeDialog_U3COnCompleteU3Em__4_m1739_MethodInfo_var };
		Action_1_t277 * L_2 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		((MobileNativeDialog_t279_StaticFields*)MobileNativeDialog_t279_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t277 * L_3 = ((MobileNativeDialog_t279_StaticFields*)MobileNativeDialog_t279_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		NullCheck(G_B2_0);
		G_B2_0->___OnComplete_0 = L_3;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___title;
		String_t* L_5 = ___message;
		MobileNativeDialog_init_m1737(__this, L_4, L_5, _stringLiteral369, _stringLiteral370, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MobileNativeDialog::.ctor(System.String,System.String,System.String,System.String)
extern TypeInfo* MobileNativeDialog_t279_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeDialog_U3COnCompleteU3Em__4_m1739_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern "C" void MobileNativeDialog__ctor_m1736 (MobileNativeDialog_t279 * __this, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeDialog_t279_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(236);
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeDialog_U3COnCompleteU3Em__4_m1739_MethodInfo_var = il2cpp_codegen_method_info_from_index(56);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeDialog_t279 * G_B2_0 = {0};
	MobileNativeDialog_t279 * G_B1_0 = {0};
	{
		Action_1_t277 * L_0 = ((MobileNativeDialog_t279_StaticFields*)MobileNativeDialog_t279_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)MobileNativeDialog_U3COnCompleteU3Em__4_m1739_MethodInfo_var };
		Action_1_t277 * L_2 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		((MobileNativeDialog_t279_StaticFields*)MobileNativeDialog_t279_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t277 * L_3 = ((MobileNativeDialog_t279_StaticFields*)MobileNativeDialog_t279_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		NullCheck(G_B2_0);
		G_B2_0->___OnComplete_0 = L_3;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___title;
		String_t* L_5 = ___message;
		String_t* L_6 = ___yes;
		String_t* L_7 = ___no;
		MobileNativeDialog_init_m1737(__this, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MobileNativeDialog::init(System.String,System.String,System.String,System.String)
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeDialog_OnCompleteListener_m1738_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern "C" void MobileNativeDialog_init_m1737 (MobileNativeDialog_t279 * __this, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeDialog_OnCompleteListener_m1738_MethodInfo_var = il2cpp_codegen_method_info_from_index(57);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	MNIOSDialog_t288 * V_0 = {0};
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		String_t* L_2 = ___yes;
		String_t* L_3 = ___no;
		MNIOSDialog_t288 * L_4 = MNIOSDialog_Create_m1781(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		MNIOSDialog_t288 * L_5 = V_0;
		MNIOSDialog_t288 * L_6 = L_5;
		NullCheck(L_6);
		Action_1_t277 * L_7 = (((MNPopup_t278 *)L_6)->___OnComplete_4);
		IntPtr_t L_8 = { (void*)MobileNativeDialog_OnCompleteListener_m1738_MethodInfo_var };
		Action_1_t277 * L_9 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_9, __this, L_8, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		Delegate_t480 * L_10 = Delegate_Combine_m2775(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		((MNPopup_t278 *)L_6)->___OnComplete_4 = ((Action_1_t277 *)Castclass(L_10, Action_1_t277_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void MobileNativeDialog::OnCompleteListener(MNDialogResult)
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MobileNativeDialog_OnCompleteListener_m1738 (MobileNativeDialog_t279 * __this, int32_t ___res, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (__this->___OnComplete_0);
		int32_t L_1 = ___res;
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		return;
	}
}
// System.Void MobileNativeDialog::<OnComplete>m__4(MNDialogResult)
extern "C" void MobileNativeDialog_U3COnCompleteU3Em__4_m1739 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// MNIOSMessage
#include "AssemblyU2DCSharp_MNIOSMessage.h"
// MNIOSMessage
#include "AssemblyU2DCSharp_MNIOSMessageMethodDeclarations.h"


// System.Void MobileNativeMessage::.ctor(System.String,System.String)
extern TypeInfo* MobileNativeMessage_t280_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeMessage_U3COnCompleteU3Em__5_m1744_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral371;
extern "C" void MobileNativeMessage__ctor_m1740 (MobileNativeMessage_t280 * __this, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeMessage_t280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(240);
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		MobileNativeMessage_U3COnCompleteU3Em__5_m1744_MethodInfo_var = il2cpp_codegen_method_info_from_index(59);
		_stringLiteral371 = il2cpp_codegen_string_literal_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeMessage_t280 * G_B2_0 = {0};
	MobileNativeMessage_t280 * G_B1_0 = {0};
	{
		Action_t238 * L_0 = ((MobileNativeMessage_t280_StaticFields*)MobileNativeMessage_t280_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)MobileNativeMessage_U3COnCompleteU3Em__5_m1744_MethodInfo_var };
		Action_t238 * L_2 = (Action_t238 *)il2cpp_codegen_object_new (Action_t238_il2cpp_TypeInfo_var);
		Action__ctor_m3072(L_2, NULL, L_1, /*hidden argument*/NULL);
		((MobileNativeMessage_t280_StaticFields*)MobileNativeMessage_t280_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_t238 * L_3 = ((MobileNativeMessage_t280_StaticFields*)MobileNativeMessage_t280_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		NullCheck(G_B2_0);
		G_B2_0->___OnComplete_0 = L_3;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___title;
		String_t* L_5 = ___message;
		MobileNativeMessage_init_m1742(__this, L_4, L_5, _stringLiteral371, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MobileNativeMessage::.ctor(System.String,System.String,System.String)
extern TypeInfo* MobileNativeMessage_t280_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t238_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeMessage_U3COnCompleteU3Em__5_m1744_MethodInfo_var;
extern "C" void MobileNativeMessage__ctor_m1741 (MobileNativeMessage_t280 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeMessage_t280_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(240);
		Action_t238_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(173);
		MobileNativeMessage_U3COnCompleteU3Em__5_m1744_MethodInfo_var = il2cpp_codegen_method_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeMessage_t280 * G_B2_0 = {0};
	MobileNativeMessage_t280 * G_B1_0 = {0};
	{
		Action_t238 * L_0 = ((MobileNativeMessage_t280_StaticFields*)MobileNativeMessage_t280_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)MobileNativeMessage_U3COnCompleteU3Em__5_m1744_MethodInfo_var };
		Action_t238 * L_2 = (Action_t238 *)il2cpp_codegen_object_new (Action_t238_il2cpp_TypeInfo_var);
		Action__ctor_m3072(L_2, NULL, L_1, /*hidden argument*/NULL);
		((MobileNativeMessage_t280_StaticFields*)MobileNativeMessage_t280_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_t238 * L_3 = ((MobileNativeMessage_t280_StaticFields*)MobileNativeMessage_t280_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache1_1;
		NullCheck(G_B2_0);
		G_B2_0->___OnComplete_0 = L_3;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___title;
		String_t* L_5 = ___message;
		String_t* L_6 = ___ok;
		MobileNativeMessage_init_m1742(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MobileNativeMessage::init(System.String,System.String,System.String)
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeMessage_OnCompleteListener_m1743_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern "C" void MobileNativeMessage_init_m1742 (MobileNativeMessage_t280 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeMessage_OnCompleteListener_m1743_MethodInfo_var = il2cpp_codegen_method_info_from_index(60);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	MNIOSMessage_t289 * V_0 = {0};
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		String_t* L_2 = ___ok;
		MNIOSMessage_t289 * L_3 = MNIOSMessage_Create_m1786(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		MNIOSMessage_t289 * L_4 = V_0;
		MNIOSMessage_t289 * L_5 = L_4;
		NullCheck(L_5);
		Action_1_t277 * L_6 = (((MNPopup_t278 *)L_5)->___OnComplete_4);
		IntPtr_t L_7 = { (void*)MobileNativeMessage_OnCompleteListener_m1743_MethodInfo_var };
		Action_1_t277 * L_8 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		Delegate_t480 * L_9 = Delegate_Combine_m2775(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		((MNPopup_t278 *)L_5)->___OnComplete_4 = ((Action_1_t277 *)Castclass(L_9, Action_1_t277_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void MobileNativeMessage::OnCompleteListener(MNDialogResult)
extern "C" void MobileNativeMessage_OnCompleteListener_m1743 (MobileNativeMessage_t280 * __this, int32_t ___res, const MethodInfo* method)
{
	{
		Action_t238 * L_0 = (__this->___OnComplete_0);
		NullCheck(L_0);
		Action_Invoke_m2844(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MobileNativeMessage::<OnComplete>m__5()
extern "C" void MobileNativeMessage_U3COnCompleteU3Em__5_m1744 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// MNIOSRateUsPopUp
#include "AssemblyU2DCSharp_MNIOSRateUsPopUp.h"
// MNIOSRateUsPopUp
#include "AssemblyU2DCSharp_MNIOSRateUsPopUpMethodDeclarations.h"


// System.Void MobileNativeRateUs::.ctor(System.String,System.String)
extern TypeInfo* MobileNativeRateUs_t281_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeRateUs_U3COnCompleteU3Em__6_m1751_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372;
extern Il2CppCodeGenString* _stringLiteral373;
extern Il2CppCodeGenString* _stringLiteral374;
extern "C" void MobileNativeRateUs__ctor_m1745 (MobileNativeRateUs_t281 * __this, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeRateUs_t281_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeRateUs_U3COnCompleteU3Em__6_m1751_MethodInfo_var = il2cpp_codegen_method_info_from_index(61);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		_stringLiteral372 = il2cpp_codegen_string_literal_from_index(372);
		_stringLiteral373 = il2cpp_codegen_string_literal_from_index(373);
		_stringLiteral374 = il2cpp_codegen_string_literal_from_index(374);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeRateUs_t281 * G_B2_0 = {0};
	MobileNativeRateUs_t281 * G_B1_0 = {0};
	{
		Action_1_t277 * L_0 = ((MobileNativeRateUs_t281_StaticFields*)MobileNativeRateUs_t281_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)MobileNativeRateUs_U3COnCompleteU3Em__6_m1751_MethodInfo_var };
		Action_1_t277 * L_2 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		((MobileNativeRateUs_t281_StaticFields*)MobileNativeRateUs_t281_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t277 * L_3 = ((MobileNativeRateUs_t281_StaticFields*)MobileNativeRateUs_t281_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
		NullCheck(G_B2_0);
		G_B2_0->___OnComplete_7 = L_3;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___title;
		__this->___title_0 = L_4;
		String_t* L_5 = ___message;
		__this->___message_1 = L_5;
		__this->___yes_2 = _stringLiteral372;
		__this->___later_3 = _stringLiteral373;
		__this->___no_4 = _stringLiteral374;
		return;
	}
}
// System.Void MobileNativeRateUs::.ctor(System.String,System.String,System.String,System.String,System.String)
extern TypeInfo* MobileNativeRateUs_t281_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeRateUs_U3COnCompleteU3Em__6_m1751_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern "C" void MobileNativeRateUs__ctor_m1746 (MobileNativeRateUs_t281 * __this, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___later, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileNativeRateUs_t281_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(238);
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeRateUs_U3COnCompleteU3Em__6_m1751_MethodInfo_var = il2cpp_codegen_method_info_from_index(61);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	MobileNativeRateUs_t281 * G_B2_0 = {0};
	MobileNativeRateUs_t281 * G_B1_0 = {0};
	{
		Action_1_t277 * L_0 = ((MobileNativeRateUs_t281_StaticFields*)MobileNativeRateUs_t281_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)MobileNativeRateUs_U3COnCompleteU3Em__6_m1751_MethodInfo_var };
		Action_1_t277 * L_2 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		((MobileNativeRateUs_t281_StaticFields*)MobileNativeRateUs_t281_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t277 * L_3 = ((MobileNativeRateUs_t281_StaticFields*)MobileNativeRateUs_t281_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache8_8;
		NullCheck(G_B2_0);
		G_B2_0->___OnComplete_7 = L_3;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_4 = ___title;
		__this->___title_0 = L_4;
		String_t* L_5 = ___message;
		__this->___message_1 = L_5;
		String_t* L_6 = ___yes;
		__this->___yes_2 = L_6;
		String_t* L_7 = ___later;
		__this->___later_3 = L_7;
		String_t* L_8 = ___no;
		__this->___no_4 = L_8;
		return;
	}
}
// System.Void MobileNativeRateUs::SetAndroidAppUrl(System.String)
extern "C" void MobileNativeRateUs_SetAndroidAppUrl_m1747 (MobileNativeRateUs_t281 * __this, String_t* ____url, const MethodInfo* method)
{
	{
		String_t* L_0 = ____url;
		__this->___url_5 = L_0;
		return;
	}
}
// System.Void MobileNativeRateUs::SetAppleId(System.String)
extern "C" void MobileNativeRateUs_SetAppleId_m1748 (MobileNativeRateUs_t281 * __this, String_t* ____appleId, const MethodInfo* method)
{
	{
		String_t* L_0 = ____appleId;
		__this->___appleId_6 = L_0;
		return;
	}
}
// System.Void MobileNativeRateUs::Start()
extern TypeInfo* Action_1_t277_il2cpp_TypeInfo_var;
extern const MethodInfo* MobileNativeRateUs_OnCompleteListener_m1750_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3071_MethodInfo_var;
extern "C" void MobileNativeRateUs_Start_m1749 (MobileNativeRateUs_t281 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(239);
		MobileNativeRateUs_OnCompleteListener_m1750_MethodInfo_var = il2cpp_codegen_method_info_from_index(62);
		Action_1__ctor_m3071_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		s_Il2CppMethodIntialized = true;
	}
	MNIOSRateUsPopUp_t290 * V_0 = {0};
	{
		String_t* L_0 = (__this->___title_0);
		String_t* L_1 = (__this->___message_1);
		String_t* L_2 = (__this->___yes_2);
		String_t* L_3 = (__this->___later_3);
		String_t* L_4 = (__this->___no_4);
		MNIOSRateUsPopUp_t290 * L_5 = MNIOSRateUsPopUp_Create_m1792(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		MNIOSRateUsPopUp_t290 * L_6 = V_0;
		String_t* L_7 = (__this->___appleId_6);
		NullCheck(L_6);
		L_6->___appleId_9 = L_7;
		MNIOSRateUsPopUp_t290 * L_8 = V_0;
		MNIOSRateUsPopUp_t290 * L_9 = L_8;
		NullCheck(L_9);
		Action_1_t277 * L_10 = (((MNPopup_t278 *)L_9)->___OnComplete_4);
		IntPtr_t L_11 = { (void*)MobileNativeRateUs_OnCompleteListener_m1750_MethodInfo_var };
		Action_1_t277 * L_12 = (Action_1_t277 *)il2cpp_codegen_object_new (Action_1_t277_il2cpp_TypeInfo_var);
		Action_1__ctor_m3071(L_12, __this, L_11, /*hidden argument*/Action_1__ctor_m3071_MethodInfo_var);
		Delegate_t480 * L_13 = Delegate_Combine_m2775(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		((MNPopup_t278 *)L_9)->___OnComplete_4 = ((Action_1_t277 *)Castclass(L_13, Action_1_t277_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void MobileNativeRateUs::OnCompleteListener(MNDialogResult)
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MobileNativeRateUs_OnCompleteListener_m1750 (MobileNativeRateUs_t281 * __this, int32_t ___res, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (__this->___OnComplete_7);
		int32_t L_1 = ___res;
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, L_1, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		return;
	}
}
// System.Void MobileNativeRateUs::<OnComplete>m__6(MNDialogResult)
extern "C" void MobileNativeRateUs_U3COnCompleteU3Em__6_m1751 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResultMethodDeclarations.h"



// MNAndroidNative
#include "AssemblyU2DCSharp_MNAndroidNative.h"
#ifndef _MSC_VER
#else
#endif
// MNAndroidNative
#include "AssemblyU2DCSharp_MNAndroidNativeMethodDeclarations.h"

// MNProxyPool
#include "AssemblyU2DCSharp_MNProxyPoolMethodDeclarations.h"


// System.Void MNAndroidNative::.ctor()
extern "C" void MNAndroidNative__ctor_m1752 (MNAndroidNative_t283 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::CallActivityFunction(System.String,System.Object[])
extern Il2CppCodeGenString* _stringLiteral375;
extern "C" void MNAndroidNative_CallActivityFunction_m1753 (Object_t * __this /* static, unused */, String_t* ___methodName, ObjectU5BU5D_t470* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral375 = il2cpp_codegen_string_literal_from_index(375);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___methodName;
		ObjectU5BU5D_t470* L_1 = ___args;
		MNProxyPool_CallStatic_m1763(NULL /*static, unused*/, _stringLiteral375, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::showDialog(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral369;
extern Il2CppCodeGenString* _stringLiteral370;
extern "C" void MNAndroidNative_showDialog_m1754 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral369 = il2cpp_codegen_string_literal_from_index(369);
		_stringLiteral370 = il2cpp_codegen_string_literal_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNAndroidNative_showDialog_m1755(NULL /*static, unused*/, L_0, L_1, _stringLiteral369, _stringLiteral370, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::showDialog(System.String,System.String,System.String,System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral376;
extern "C" void MNAndroidNative_showDialog_m1755 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral376 = il2cpp_codegen_string_literal_from_index(376);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = ___title;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		String_t* L_3 = ___message;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_2;
		String_t* L_5 = ___yes;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_5;
		ObjectU5BU5D_t470* L_6 = L_4;
		String_t* L_7 = ___no;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3)) = (Object_t *)L_7;
		MNAndroidNative_CallActivityFunction_m1753(NULL /*static, unused*/, _stringLiteral376, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::showMessage(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral371;
extern "C" void MNAndroidNative_showMessage_m1756 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral371 = il2cpp_codegen_string_literal_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNAndroidNative_showMessage_m1757(NULL /*static, unused*/, L_0, L_1, _stringLiteral371, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::showMessage(System.String,System.String,System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral377;
extern "C" void MNAndroidNative_showMessage_m1757 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral377 = il2cpp_codegen_string_literal_from_index(377);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 3));
		String_t* L_1 = ___title;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		String_t* L_3 = ___message;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_2;
		String_t* L_5 = ___ok;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_5;
		MNAndroidNative_CallActivityFunction_m1753(NULL /*static, unused*/, _stringLiteral377, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::showRateDialog(System.String,System.String,System.String,System.String,System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral378;
extern "C" void MNAndroidNative_showRateDialog_m1758 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___laiter, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral378 = il2cpp_codegen_string_literal_from_index(378);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 5));
		String_t* L_1 = ___title;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		String_t* L_3 = ___message;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_2;
		String_t* L_5 = ___yes;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_5;
		ObjectU5BU5D_t470* L_6 = L_4;
		String_t* L_7 = ___laiter;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_6;
		String_t* L_9 = ___no;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 4)) = (Object_t *)L_9;
		MNAndroidNative_CallActivityFunction_m1753(NULL /*static, unused*/, _stringLiteral378, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::ShowPreloader(System.String,System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral379;
extern "C" void MNAndroidNative_ShowPreloader_m1759 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral379 = il2cpp_codegen_string_literal_from_index(379);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 2));
		String_t* L_1 = ___title;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		String_t* L_3 = ___message;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		MNAndroidNative_CallActivityFunction_m1753(NULL /*static, unused*/, _stringLiteral379, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::HidePreloader()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral380;
extern "C" void MNAndroidNative_HidePreloader_m1760 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral380 = il2cpp_codegen_string_literal_from_index(380);
		s_Il2CppMethodIntialized = true;
	}
	{
		MNAndroidNative_CallActivityFunction_m1753(NULL /*static, unused*/, _stringLiteral380, ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 0)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidNative::RedirectStoreRatingPage(System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral381;
extern "C" void MNAndroidNative_RedirectStoreRatingPage_m1761 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		_stringLiteral381 = il2cpp_codegen_string_literal_from_index(381);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 1));
		String_t* L_1 = ___url;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		MNAndroidNative_CallActivityFunction_m1753(NULL /*static, unused*/, _stringLiteral381, L_0, /*hidden argument*/NULL);
		return;
	}
}
// MNProxyPool
#include "AssemblyU2DCSharp_MNProxyPool.h"
#ifndef _MSC_VER
#else
#endif



// System.Void MNProxyPool::.ctor()
extern "C" void MNProxyPool__ctor_m1762 (MNProxyPool_t284 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNProxyPool::CallStatic(System.String,System.String,System.Object[])
extern "C" void MNProxyPool_CallStatic_m1763 (Object_t * __this /* static, unused */, String_t* ___className, String_t* ___methodName, ObjectU5BU5D_t470* ___args, const MethodInfo* method)
{
	{
		return;
	}
}
// MNAndroidDialog
#include "AssemblyU2DCSharp_MNAndroidDialog.h"
#ifndef _MSC_VER
#else
#endif
// MNAndroidDialog
#include "AssemblyU2DCSharp_MNAndroidDialogMethodDeclarations.h"

// System.Int16
#include "mscorlib_System_Int16.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
struct GameObject_t256;
struct MNAndroidDialog_t285;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNAndroidDialog>()
// !!0 UnityEngine.GameObject::AddComponent<MNAndroidDialog>()
#define GameObject_AddComponent_TisMNAndroidDialog_t285_m3075(__this, method) (( MNAndroidDialog_t285 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNAndroidDialog::.ctor()
extern "C" void MNAndroidDialog__ctor_m1764 (MNAndroidDialog_t285 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNAndroidDialog MNAndroidDialog::Create(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral369;
extern Il2CppCodeGenString* _stringLiteral370;
extern "C" MNAndroidDialog_t285 * MNAndroidDialog_Create_m1765 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral369 = il2cpp_codegen_string_literal_from_index(369);
		_stringLiteral370 = il2cpp_codegen_string_literal_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNAndroidDialog_t285 * L_2 = MNAndroidDialog_Create_m1766(NULL /*static, unused*/, L_0, L_1, _stringLiteral369, _stringLiteral370, /*hidden argument*/NULL);
		return L_2;
	}
}
// MNAndroidDialog MNAndroidDialog::Create(System.String,System.String,System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNAndroidDialog_t285_m3075_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral382;
extern "C" MNAndroidDialog_t285 * MNAndroidDialog_Create_m1766 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNAndroidDialog_t285_m3075_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483711);
		_stringLiteral382 = il2cpp_codegen_string_literal_from_index(382);
		s_Il2CppMethodIntialized = true;
	}
	MNAndroidDialog_t285 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral382, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNAndroidDialog_t285 * L_1 = GameObject_AddComponent_TisMNAndroidDialog_t285_m3075(L_0, /*hidden argument*/GameObject_AddComponent_TisMNAndroidDialog_t285_m3075_MethodInfo_var);
		V_0 = L_1;
		MNAndroidDialog_t285 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNAndroidDialog_t285 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNAndroidDialog_t285 * L_6 = V_0;
		String_t* L_7 = ___yes;
		NullCheck(L_6);
		L_6->___yes_6 = L_7;
		MNAndroidDialog_t285 * L_8 = V_0;
		String_t* L_9 = ___no;
		NullCheck(L_8);
		L_8->___no_7 = L_9;
		MNAndroidDialog_t285 * L_10 = V_0;
		NullCheck(L_10);
		MNAndroidDialog_init_m1767(L_10, /*hidden argument*/NULL);
		MNAndroidDialog_t285 * L_11 = V_0;
		return L_11;
	}
}
// System.Void MNAndroidDialog::init()
extern "C" void MNAndroidDialog_init_m1767 (MNAndroidDialog_t285 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (((MNPopup_t278 *)__this)->___title_2);
		String_t* L_1 = (((MNPopup_t278 *)__this)->___message_3);
		String_t* L_2 = (__this->___yes_6);
		String_t* L_3 = (__this->___no_7);
		MNAndroidNative_showDialog_m1755(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidDialog::onPopUpCallBack(System.String)
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNAndroidDialog_onPopUpCallBack_m1768 (MNAndroidDialog_t285 * __this, String_t* ___buttonIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___buttonIndex;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		int16_t L_1 = Convert_ToInt16_m3076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_002c;
		}
	}
	{
		goto IL_003d;
	}

IL_001b:
	{
		Action_1_t277 * L_5 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_5);
		Action_1_Invoke_m3074(L_5, 0, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_003d;
	}

IL_002c:
	{
		Action_1_t277 * L_6 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_6);
		Action_1_Invoke_m3074(L_6, 1, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_003d;
	}

IL_003d:
	{
		GameObject_t256 * L_7 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// MNAndroidMessage
#include "AssemblyU2DCSharp_MNAndroidMessage.h"
#ifndef _MSC_VER
#else
#endif
// MNAndroidMessage
#include "AssemblyU2DCSharp_MNAndroidMessageMethodDeclarations.h"

struct GameObject_t256;
struct MNAndroidMessage_t286;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNAndroidMessage>()
// !!0 UnityEngine.GameObject::AddComponent<MNAndroidMessage>()
#define GameObject_AddComponent_TisMNAndroidMessage_t286_m3077(__this, method) (( MNAndroidMessage_t286 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNAndroidMessage::.ctor()
extern "C" void MNAndroidMessage__ctor_m1769 (MNAndroidMessage_t286 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNAndroidMessage MNAndroidMessage::Create(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral371;
extern "C" MNAndroidMessage_t286 * MNAndroidMessage_Create_m1770 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral371 = il2cpp_codegen_string_literal_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNAndroidMessage_t286 * L_2 = MNAndroidMessage_Create_m1771(NULL /*static, unused*/, L_0, L_1, _stringLiteral371, /*hidden argument*/NULL);
		return L_2;
	}
}
// MNAndroidMessage MNAndroidMessage::Create(System.String,System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNAndroidMessage_t286_m3077_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral382;
extern "C" MNAndroidMessage_t286 * MNAndroidMessage_Create_m1771 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNAndroidMessage_t286_m3077_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483712);
		_stringLiteral382 = il2cpp_codegen_string_literal_from_index(382);
		s_Il2CppMethodIntialized = true;
	}
	MNAndroidMessage_t286 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral382, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNAndroidMessage_t286 * L_1 = GameObject_AddComponent_TisMNAndroidMessage_t286_m3077(L_0, /*hidden argument*/GameObject_AddComponent_TisMNAndroidMessage_t286_m3077_MethodInfo_var);
		V_0 = L_1;
		MNAndroidMessage_t286 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNAndroidMessage_t286 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNAndroidMessage_t286 * L_6 = V_0;
		String_t* L_7 = ___ok;
		NullCheck(L_6);
		L_6->___ok_6 = L_7;
		MNAndroidMessage_t286 * L_8 = V_0;
		NullCheck(L_8);
		MNAndroidMessage_init_m1772(L_8, /*hidden argument*/NULL);
		MNAndroidMessage_t286 * L_9 = V_0;
		return L_9;
	}
}
// System.Void MNAndroidMessage::init()
extern "C" void MNAndroidMessage_init_m1772 (MNAndroidMessage_t286 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (((MNPopup_t278 *)__this)->___title_2);
		String_t* L_1 = (((MNPopup_t278 *)__this)->___message_3);
		String_t* L_2 = (__this->___ok_6);
		MNAndroidNative_showMessage_m1757(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidMessage::onPopUpCallBack(System.String)
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNAndroidMessage_onPopUpCallBack_m1773 (MNAndroidMessage_t286 * __this, String_t* ___buttonIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, 0, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		GameObject_t256 * L_1 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// MNAndroidRateUsPopUp
#include "AssemblyU2DCSharp_MNAndroidRateUsPopUp.h"
#ifndef _MSC_VER
#else
#endif
// MNAndroidRateUsPopUp
#include "AssemblyU2DCSharp_MNAndroidRateUsPopUpMethodDeclarations.h"

struct GameObject_t256;
struct MNAndroidRateUsPopUp_t287;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNAndroidRateUsPopUp>()
// !!0 UnityEngine.GameObject::AddComponent<MNAndroidRateUsPopUp>()
#define GameObject_AddComponent_TisMNAndroidRateUsPopUp_t287_m3078(__this, method) (( MNAndroidRateUsPopUp_t287 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNAndroidRateUsPopUp::.ctor()
extern "C" void MNAndroidRateUsPopUp__ctor_m1774 (MNAndroidRateUsPopUp_t287 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNAndroidRateUsPopUp MNAndroidRateUsPopUp::Create(System.String,System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral372;
extern Il2CppCodeGenString* _stringLiteral373;
extern Il2CppCodeGenString* _stringLiteral374;
extern "C" MNAndroidRateUsPopUp_t287 * MNAndroidRateUsPopUp_Create_m1775 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___url, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral372 = il2cpp_codegen_string_literal_from_index(372);
		_stringLiteral373 = il2cpp_codegen_string_literal_from_index(373);
		_stringLiteral374 = il2cpp_codegen_string_literal_from_index(374);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		String_t* L_2 = ___url;
		MNAndroidRateUsPopUp_t287 * L_3 = MNAndroidRateUsPopUp_Create_m1776(NULL /*static, unused*/, L_0, L_1, L_2, _stringLiteral372, _stringLiteral373, _stringLiteral374, /*hidden argument*/NULL);
		return L_3;
	}
}
// MNAndroidRateUsPopUp MNAndroidRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNAndroidRateUsPopUp_t287_m3078_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral383;
extern "C" MNAndroidRateUsPopUp_t287 * MNAndroidRateUsPopUp_Create_m1776 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___url, String_t* ___yes, String_t* ___later, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNAndroidRateUsPopUp_t287_m3078_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483713);
		_stringLiteral383 = il2cpp_codegen_string_literal_from_index(383);
		s_Il2CppMethodIntialized = true;
	}
	MNAndroidRateUsPopUp_t287 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral383, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNAndroidRateUsPopUp_t287 * L_1 = GameObject_AddComponent_TisMNAndroidRateUsPopUp_t287_m3078(L_0, /*hidden argument*/GameObject_AddComponent_TisMNAndroidRateUsPopUp_t287_m3078_MethodInfo_var);
		V_0 = L_1;
		MNAndroidRateUsPopUp_t287 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNAndroidRateUsPopUp_t287 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNAndroidRateUsPopUp_t287 * L_6 = V_0;
		String_t* L_7 = ___url;
		NullCheck(L_6);
		L_6->___url_9 = L_7;
		MNAndroidRateUsPopUp_t287 * L_8 = V_0;
		String_t* L_9 = ___yes;
		NullCheck(L_8);
		L_8->___yes_6 = L_9;
		MNAndroidRateUsPopUp_t287 * L_10 = V_0;
		String_t* L_11 = ___later;
		NullCheck(L_10);
		L_10->___later_7 = L_11;
		MNAndroidRateUsPopUp_t287 * L_12 = V_0;
		String_t* L_13 = ___no;
		NullCheck(L_12);
		L_12->___no_8 = L_13;
		MNAndroidRateUsPopUp_t287 * L_14 = V_0;
		NullCheck(L_14);
		MNAndroidRateUsPopUp_init_m1777(L_14, /*hidden argument*/NULL);
		MNAndroidRateUsPopUp_t287 * L_15 = V_0;
		return L_15;
	}
}
// System.Void MNAndroidRateUsPopUp::init()
extern "C" void MNAndroidRateUsPopUp_init_m1777 (MNAndroidRateUsPopUp_t287 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (((MNPopup_t278 *)__this)->___title_2);
		String_t* L_1 = (((MNPopup_t278 *)__this)->___message_3);
		String_t* L_2 = (__this->___yes_6);
		String_t* L_3 = (__this->___later_7);
		String_t* L_4 = (__this->___no_8);
		MNAndroidNative_showRateDialog_m1758(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNAndroidRateUsPopUp::onPopUpCallBack(System.String)
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNAndroidRateUsPopUp_onPopUpCallBack_m1778 (MNAndroidRateUsPopUp_t287 * __this, String_t* ___buttonIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___buttonIndex;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		int16_t L_1 = Convert_ToInt16_m3076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0020;
		}
		if (L_3 == 1)
		{
			goto IL_003c;
		}
		if (L_3 == 2)
		{
			goto IL_004d;
		}
	}
	{
		goto IL_005e;
	}

IL_0020:
	{
		String_t* L_4 = (__this->___url_9);
		MNAndroidNative_RedirectStoreRatingPage_m1761(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Action_1_t277 * L_5 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_5);
		Action_1_Invoke_m3074(L_5, 2, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_005e;
	}

IL_003c:
	{
		Action_1_t277 * L_6 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_6);
		Action_1_Invoke_m3074(L_6, 3, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_005e;
	}

IL_004d:
	{
		Action_1_t277 * L_7 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_7);
		Action_1_Invoke_m3074(L_7, 4, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_005e;
	}

IL_005e:
	{
		GameObject_t256 * L_8 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

struct GameObject_t256;
struct MNIOSDialog_t288;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNIOSDialog>()
// !!0 UnityEngine.GameObject::AddComponent<MNIOSDialog>()
#define GameObject_AddComponent_TisMNIOSDialog_t288_m3079(__this, method) (( MNIOSDialog_t288 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNIOSDialog::.ctor()
extern "C" void MNIOSDialog__ctor_m1779 (MNIOSDialog_t288 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNIOSDialog MNIOSDialog::Create(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral369;
extern Il2CppCodeGenString* _stringLiteral370;
extern "C" MNIOSDialog_t288 * MNIOSDialog_Create_m1780 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral369 = il2cpp_codegen_string_literal_from_index(369);
		_stringLiteral370 = il2cpp_codegen_string_literal_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNIOSDialog_t288 * L_2 = MNIOSDialog_Create_m1781(NULL /*static, unused*/, L_0, L_1, _stringLiteral369, _stringLiteral370, /*hidden argument*/NULL);
		return L_2;
	}
}
// MNIOSDialog MNIOSDialog::Create(System.String,System.String,System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNIOSDialog_t288_m3079_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral384;
extern "C" MNIOSDialog_t288 * MNIOSDialog_Create_m1781 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNIOSDialog_t288_m3079_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483714);
		_stringLiteral384 = il2cpp_codegen_string_literal_from_index(384);
		s_Il2CppMethodIntialized = true;
	}
	MNIOSDialog_t288 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral384, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNIOSDialog_t288 * L_1 = GameObject_AddComponent_TisMNIOSDialog_t288_m3079(L_0, /*hidden argument*/GameObject_AddComponent_TisMNIOSDialog_t288_m3079_MethodInfo_var);
		V_0 = L_1;
		MNIOSDialog_t288 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNIOSDialog_t288 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNIOSDialog_t288 * L_6 = V_0;
		String_t* L_7 = ___yes;
		NullCheck(L_6);
		L_6->___yes_6 = L_7;
		MNIOSDialog_t288 * L_8 = V_0;
		String_t* L_9 = ___no;
		NullCheck(L_8);
		L_8->___no_7 = L_9;
		MNIOSDialog_t288 * L_10 = V_0;
		NullCheck(L_10);
		MNIOSDialog_init_m1782(L_10, /*hidden argument*/NULL);
		MNIOSDialog_t288 * L_11 = V_0;
		return L_11;
	}
}
// System.Void MNIOSDialog::init()
extern "C" void MNIOSDialog_init_m1782 (MNIOSDialog_t288 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (((MNPopup_t278 *)__this)->___title_2);
		String_t* L_1 = (((MNPopup_t278 *)__this)->___message_3);
		String_t* L_2 = (__this->___yes_6);
		String_t* L_3 = (__this->___no_7);
		MNIOSNative_showDialog_m1806(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSDialog::onPopUpCallBack(System.String)
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNIOSDialog_onPopUpCallBack_m1783 (MNIOSDialog_t288 * __this, String_t* ___buttonIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___buttonIndex;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		int16_t L_1 = Convert_ToInt16_m3076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_002c;
		}
	}
	{
		goto IL_003d;
	}

IL_001b:
	{
		Action_1_t277 * L_5 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_5);
		Action_1_Invoke_m3074(L_5, 0, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_003d;
	}

IL_002c:
	{
		Action_1_t277 * L_6 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_6);
		Action_1_Invoke_m3074(L_6, 1, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_003d;
	}

IL_003d:
	{
		GameObject_t256 * L_7 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

struct GameObject_t256;
struct MNIOSMessage_t289;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNIOSMessage>()
// !!0 UnityEngine.GameObject::AddComponent<MNIOSMessage>()
#define GameObject_AddComponent_TisMNIOSMessage_t289_m3080(__this, method) (( MNIOSMessage_t289 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNIOSMessage::.ctor()
extern "C" void MNIOSMessage__ctor_m1784 (MNIOSMessage_t289 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNIOSMessage MNIOSMessage::Create(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral371;
extern "C" MNIOSMessage_t289 * MNIOSMessage_Create_m1785 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral371 = il2cpp_codegen_string_literal_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNIOSMessage_t289 * L_2 = MNIOSMessage_Create_m1786(NULL /*static, unused*/, L_0, L_1, _stringLiteral371, /*hidden argument*/NULL);
		return L_2;
	}
}
// MNIOSMessage MNIOSMessage::Create(System.String,System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNIOSMessage_t289_m3080_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral384;
extern "C" MNIOSMessage_t289 * MNIOSMessage_Create_m1786 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNIOSMessage_t289_m3080_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483715);
		_stringLiteral384 = il2cpp_codegen_string_literal_from_index(384);
		s_Il2CppMethodIntialized = true;
	}
	MNIOSMessage_t289 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral384, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNIOSMessage_t289 * L_1 = GameObject_AddComponent_TisMNIOSMessage_t289_m3080(L_0, /*hidden argument*/GameObject_AddComponent_TisMNIOSMessage_t289_m3080_MethodInfo_var);
		V_0 = L_1;
		MNIOSMessage_t289 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNIOSMessage_t289 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNIOSMessage_t289 * L_6 = V_0;
		String_t* L_7 = ___ok;
		NullCheck(L_6);
		L_6->___ok_6 = L_7;
		MNIOSMessage_t289 * L_8 = V_0;
		NullCheck(L_8);
		MNIOSMessage_init_m1787(L_8, /*hidden argument*/NULL);
		MNIOSMessage_t289 * L_9 = V_0;
		return L_9;
	}
}
// System.Void MNIOSMessage::init()
extern "C" void MNIOSMessage_init_m1787 (MNIOSMessage_t289 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (((MNPopup_t278 *)__this)->___title_2);
		String_t* L_1 = (((MNPopup_t278 *)__this)->___message_3);
		String_t* L_2 = (__this->___ok_6);
		MNIOSNative_showMessage_m1808(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSMessage::onPopUpCallBack(System.String)
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNIOSMessage_onPopUpCallBack_m1788 (MNIOSMessage_t289 * __this, String_t* ___buttonIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, 0, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		GameObject_t256 * L_1 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

struct GameObject_t256;
struct MNIOSRateUsPopUp_t290;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNIOSRateUsPopUp>()
// !!0 UnityEngine.GameObject::AddComponent<MNIOSRateUsPopUp>()
#define GameObject_AddComponent_TisMNIOSRateUsPopUp_t290_m3081(__this, method) (( MNIOSRateUsPopUp_t290 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNIOSRateUsPopUp::.ctor()
extern "C" void MNIOSRateUsPopUp__ctor_m1789 (MNIOSRateUsPopUp_t290 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create()
extern Il2CppCodeGenString* _stringLiteral385;
extern Il2CppCodeGenString* _stringLiteral386;
extern "C" MNIOSRateUsPopUp_t290 * MNIOSRateUsPopUp_Create_m1790 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral385 = il2cpp_codegen_string_literal_from_index(385);
		_stringLiteral386 = il2cpp_codegen_string_literal_from_index(386);
		s_Il2CppMethodIntialized = true;
	}
	{
		MNIOSRateUsPopUp_t290 * L_0 = MNIOSRateUsPopUp_Create_m1791(NULL /*static, unused*/, _stringLiteral385, _stringLiteral386, /*hidden argument*/NULL);
		return L_0;
	}
}
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral387;
extern Il2CppCodeGenString* _stringLiteral388;
extern Il2CppCodeGenString* _stringLiteral374;
extern "C" MNIOSRateUsPopUp_t290 * MNIOSRateUsPopUp_Create_m1791 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral387 = il2cpp_codegen_string_literal_from_index(387);
		_stringLiteral388 = il2cpp_codegen_string_literal_from_index(388);
		_stringLiteral374 = il2cpp_codegen_string_literal_from_index(374);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNIOSRateUsPopUp_t290 * L_2 = MNIOSRateUsPopUp_Create_m1792(NULL /*static, unused*/, L_0, L_1, _stringLiteral387, _stringLiteral388, _stringLiteral374, /*hidden argument*/NULL);
		return L_2;
	}
}
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNIOSRateUsPopUp_t290_m3081_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral389;
extern "C" MNIOSRateUsPopUp_t290 * MNIOSRateUsPopUp_Create_m1792 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNIOSRateUsPopUp_t290_m3081_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483716);
		_stringLiteral389 = il2cpp_codegen_string_literal_from_index(389);
		s_Il2CppMethodIntialized = true;
	}
	MNIOSRateUsPopUp_t290 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral389, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNIOSRateUsPopUp_t290 * L_1 = GameObject_AddComponent_TisMNIOSRateUsPopUp_t290_m3081(L_0, /*hidden argument*/GameObject_AddComponent_TisMNIOSRateUsPopUp_t290_m3081_MethodInfo_var);
		V_0 = L_1;
		MNIOSRateUsPopUp_t290 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNIOSRateUsPopUp_t290 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNIOSRateUsPopUp_t290 * L_6 = V_0;
		String_t* L_7 = ___rate;
		NullCheck(L_6);
		L_6->___rate_6 = L_7;
		MNIOSRateUsPopUp_t290 * L_8 = V_0;
		String_t* L_9 = ___remind;
		NullCheck(L_8);
		L_8->___remind_7 = L_9;
		MNIOSRateUsPopUp_t290 * L_10 = V_0;
		String_t* L_11 = ___declined;
		NullCheck(L_10);
		L_10->___declined_8 = L_11;
		MNIOSRateUsPopUp_t290 * L_12 = V_0;
		NullCheck(L_12);
		MNIOSRateUsPopUp_init_m1793(L_12, /*hidden argument*/NULL);
		MNIOSRateUsPopUp_t290 * L_13 = V_0;
		return L_13;
	}
}
// System.Void MNIOSRateUsPopUp::init()
extern "C" void MNIOSRateUsPopUp_init_m1793 (MNIOSRateUsPopUp_t290 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (((MNPopup_t278 *)__this)->___title_2);
		String_t* L_1 = (((MNPopup_t278 *)__this)->___message_3);
		String_t* L_2 = (__this->___rate_6);
		String_t* L_3 = (__this->___remind_7);
		String_t* L_4 = (__this->___declined_8);
		MNIOSNative_showRateUsPopUP_m1804(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSRateUsPopUp::onPopUpCallBack(System.String)
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNIOSRateUsPopUp_onPopUpCallBack_m1794 (MNIOSRateUsPopUp_t290 * __this, String_t* ___buttonIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___buttonIndex;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		int16_t L_1 = Convert_ToInt16_m3076(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = L_2;
		int32_t L_3 = V_1;
		if (L_3 == 0)
		{
			goto IL_0020;
		}
		if (L_3 == 1)
		{
			goto IL_003c;
		}
		if (L_3 == 2)
		{
			goto IL_004d;
		}
	}
	{
		goto IL_005e;
	}

IL_0020:
	{
		String_t* L_4 = (__this->___appleId_9);
		MNIOSNative_RedirectToAppStoreRatingPage_m1809(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Action_1_t277 * L_5 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_5);
		Action_1_Invoke_m3074(L_5, 2, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_005e;
	}

IL_003c:
	{
		Action_1_t277 * L_6 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_6);
		Action_1_Invoke_m3074(L_6, 3, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_005e;
	}

IL_004d:
	{
		Action_1_t277 * L_7 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_7);
		Action_1_Invoke_m3074(L_7, 4, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		goto IL_005e;
	}

IL_005e:
	{
		GameObject_t256 * L_8 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return;
	}
}
// MNIOSNative
#include "AssemblyU2DCSharp_MNIOSNative.h"
#ifndef _MSC_VER
#else
#endif



// System.Void MNIOSNative::.ctor()
extern "C" void MNIOSNative__ctor_m1795 (MNIOSNative_t291 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::_MNP_ShowRateUsPopUp(System.String,System.String,System.String,System.String,System.String)
extern "C" {void DEFAULT_CALL _MNP_ShowRateUsPopUp(char*, char*, char*, char*, char*);}
extern "C" void MNIOSNative__MNP_ShowRateUsPopUp_m1796 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)_MNP_ShowRateUsPopUp;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_MNP_ShowRateUsPopUp'"));
		}
	}

	// Marshaling of parameter '___title' to native representation
	char* ____title_marshaled = { 0 };
	____title_marshaled = il2cpp_codegen_marshal_string(___title);

	// Marshaling of parameter '___message' to native representation
	char* ____message_marshaled = { 0 };
	____message_marshaled = il2cpp_codegen_marshal_string(___message);

	// Marshaling of parameter '___rate' to native representation
	char* ____rate_marshaled = { 0 };
	____rate_marshaled = il2cpp_codegen_marshal_string(___rate);

	// Marshaling of parameter '___remind' to native representation
	char* ____remind_marshaled = { 0 };
	____remind_marshaled = il2cpp_codegen_marshal_string(___remind);

	// Marshaling of parameter '___declined' to native representation
	char* ____declined_marshaled = { 0 };
	____declined_marshaled = il2cpp_codegen_marshal_string(___declined);

	// Native function invocation
	_il2cpp_pinvoke_func(____title_marshaled, ____message_marshaled, ____rate_marshaled, ____remind_marshaled, ____declined_marshaled);

	// Marshaling cleanup of parameter '___title' native representation
	il2cpp_codegen_marshal_free(____title_marshaled);
	____title_marshaled = NULL;

	// Marshaling cleanup of parameter '___message' native representation
	il2cpp_codegen_marshal_free(____message_marshaled);
	____message_marshaled = NULL;

	// Marshaling cleanup of parameter '___rate' native representation
	il2cpp_codegen_marshal_free(____rate_marshaled);
	____rate_marshaled = NULL;

	// Marshaling cleanup of parameter '___remind' native representation
	il2cpp_codegen_marshal_free(____remind_marshaled);
	____remind_marshaled = NULL;

	// Marshaling cleanup of parameter '___declined' native representation
	il2cpp_codegen_marshal_free(____declined_marshaled);
	____declined_marshaled = NULL;

}
// System.Void MNIOSNative::_MNP_ShowDialog(System.String,System.String,System.String,System.String)
extern "C" {void DEFAULT_CALL _MNP_ShowDialog(char*, char*, char*, char*);}
extern "C" void MNIOSNative__MNP_ShowDialog_m1797 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)_MNP_ShowDialog;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_MNP_ShowDialog'"));
		}
	}

	// Marshaling of parameter '___title' to native representation
	char* ____title_marshaled = { 0 };
	____title_marshaled = il2cpp_codegen_marshal_string(___title);

	// Marshaling of parameter '___message' to native representation
	char* ____message_marshaled = { 0 };
	____message_marshaled = il2cpp_codegen_marshal_string(___message);

	// Marshaling of parameter '___yes' to native representation
	char* ____yes_marshaled = { 0 };
	____yes_marshaled = il2cpp_codegen_marshal_string(___yes);

	// Marshaling of parameter '___no' to native representation
	char* ____no_marshaled = { 0 };
	____no_marshaled = il2cpp_codegen_marshal_string(___no);

	// Native function invocation
	_il2cpp_pinvoke_func(____title_marshaled, ____message_marshaled, ____yes_marshaled, ____no_marshaled);

	// Marshaling cleanup of parameter '___title' native representation
	il2cpp_codegen_marshal_free(____title_marshaled);
	____title_marshaled = NULL;

	// Marshaling cleanup of parameter '___message' native representation
	il2cpp_codegen_marshal_free(____message_marshaled);
	____message_marshaled = NULL;

	// Marshaling cleanup of parameter '___yes' native representation
	il2cpp_codegen_marshal_free(____yes_marshaled);
	____yes_marshaled = NULL;

	// Marshaling cleanup of parameter '___no' native representation
	il2cpp_codegen_marshal_free(____no_marshaled);
	____no_marshaled = NULL;

}
// System.Void MNIOSNative::_MNP_ShowMessage(System.String,System.String,System.String)
extern "C" {void DEFAULT_CALL _MNP_ShowMessage(char*, char*, char*);}
extern "C" void MNIOSNative__MNP_ShowMessage_m1798 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)_MNP_ShowMessage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_MNP_ShowMessage'"));
		}
	}

	// Marshaling of parameter '___title' to native representation
	char* ____title_marshaled = { 0 };
	____title_marshaled = il2cpp_codegen_marshal_string(___title);

	// Marshaling of parameter '___message' to native representation
	char* ____message_marshaled = { 0 };
	____message_marshaled = il2cpp_codegen_marshal_string(___message);

	// Marshaling of parameter '___ok' to native representation
	char* ____ok_marshaled = { 0 };
	____ok_marshaled = il2cpp_codegen_marshal_string(___ok);

	// Native function invocation
	_il2cpp_pinvoke_func(____title_marshaled, ____message_marshaled, ____ok_marshaled);

	// Marshaling cleanup of parameter '___title' native representation
	il2cpp_codegen_marshal_free(____title_marshaled);
	____title_marshaled = NULL;

	// Marshaling cleanup of parameter '___message' native representation
	il2cpp_codegen_marshal_free(____message_marshaled);
	____message_marshaled = NULL;

	// Marshaling cleanup of parameter '___ok' native representation
	il2cpp_codegen_marshal_free(____ok_marshaled);
	____ok_marshaled = NULL;

}
// System.Void MNIOSNative::_MNP_DismissCurrentAlert()
extern "C" {void DEFAULT_CALL _MNP_DismissCurrentAlert();}
extern "C" void MNIOSNative__MNP_DismissCurrentAlert_m1799 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)_MNP_DismissCurrentAlert;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_MNP_DismissCurrentAlert'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void MNIOSNative::_MNP_RedirectToAppStoreRatingPage(System.String)
extern "C" {void DEFAULT_CALL _MNP_RedirectToAppStoreRatingPage(char*);}
extern "C" void MNIOSNative__MNP_RedirectToAppStoreRatingPage_m1800 (Object_t * __this /* static, unused */, String_t* ___appId, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)_MNP_RedirectToAppStoreRatingPage;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_MNP_RedirectToAppStoreRatingPage'"));
		}
	}

	// Marshaling of parameter '___appId' to native representation
	char* ____appId_marshaled = { 0 };
	____appId_marshaled = il2cpp_codegen_marshal_string(___appId);

	// Native function invocation
	_il2cpp_pinvoke_func(____appId_marshaled);

	// Marshaling cleanup of parameter '___appId' native representation
	il2cpp_codegen_marshal_free(____appId_marshaled);
	____appId_marshaled = NULL;

}
// System.Void MNIOSNative::_MNP_ShowPreloader()
extern "C" {void DEFAULT_CALL _MNP_ShowPreloader();}
extern "C" void MNIOSNative__MNP_ShowPreloader_m1801 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)_MNP_ShowPreloader;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_MNP_ShowPreloader'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void MNIOSNative::_MNP_HidePreloader()
extern "C" {void DEFAULT_CALL _MNP_HidePreloader();}
extern "C" void MNIOSNative__MNP_HidePreloader_m1802 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)_MNP_HidePreloader;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: '_MNP_HidePreloader'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Void MNIOSNative::dismissCurrentAlert()
extern "C" void MNIOSNative_dismissCurrentAlert_m1803 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		MNIOSNative__MNP_DismissCurrentAlert_m1799(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::showRateUsPopUP(System.String,System.String,System.String,System.String,System.String)
extern "C" void MNIOSNative_showRateUsPopUP_m1804 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		String_t* L_2 = ___rate;
		String_t* L_3 = ___remind;
		String_t* L_4 = ___declined;
		MNIOSNative__MNP_ShowRateUsPopUp_m1796(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::showDialog(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral369;
extern Il2CppCodeGenString* _stringLiteral370;
extern "C" void MNIOSNative_showDialog_m1805 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral369 = il2cpp_codegen_string_literal_from_index(369);
		_stringLiteral370 = il2cpp_codegen_string_literal_from_index(370);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNIOSNative_showDialog_m1806(NULL /*static, unused*/, L_0, L_1, _stringLiteral369, _stringLiteral370, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::showDialog(System.String,System.String,System.String,System.String)
extern "C" void MNIOSNative_showDialog_m1806 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		String_t* L_2 = ___yes;
		String_t* L_3 = ___no;
		MNIOSNative__MNP_ShowDialog_m1797(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::showMessage(System.String,System.String)
extern Il2CppCodeGenString* _stringLiteral371;
extern "C" void MNIOSNative_showMessage_m1807 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral371 = il2cpp_codegen_string_literal_from_index(371);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		MNIOSNative_showMessage_m1808(NULL /*static, unused*/, L_0, L_1, _stringLiteral371, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::showMessage(System.String,System.String,System.String)
extern "C" void MNIOSNative_showMessage_m1808 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	{
		String_t* L_0 = ___title;
		String_t* L_1 = ___message;
		String_t* L_2 = ___ok;
		MNIOSNative__MNP_ShowMessage_m1798(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::RedirectToAppStoreRatingPage(System.String)
extern "C" void MNIOSNative_RedirectToAppStoreRatingPage_m1809 (Object_t * __this /* static, unused */, String_t* ___appleId, const MethodInfo* method)
{
	{
		String_t* L_0 = ___appleId;
		MNIOSNative__MNP_RedirectToAppStoreRatingPage_m1800(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::ShowPreloader()
extern "C" void MNIOSNative_ShowPreloader_m1810 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		MNIOSNative__MNP_ShowPreloader_m1801(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNIOSNative::HidePreloader()
extern "C" void MNIOSNative_HidePreloader_m1811 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		MNIOSNative__MNP_HidePreloader_m1802(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// MNWP8Dialog
#include "AssemblyU2DCSharp_MNWP8Dialog.h"
#ifndef _MSC_VER
#else
#endif
// MNWP8Dialog
#include "AssemblyU2DCSharp_MNWP8DialogMethodDeclarations.h"

struct GameObject_t256;
struct MNWP8Dialog_t292;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNWP8Dialog>()
// !!0 UnityEngine.GameObject::AddComponent<MNWP8Dialog>()
#define GameObject_AddComponent_TisMNWP8Dialog_t292_m3082(__this, method) (( MNWP8Dialog_t292 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNWP8Dialog::.ctor()
extern "C" void MNWP8Dialog__ctor_m1812 (MNWP8Dialog_t292 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNWP8Dialog MNWP8Dialog::Create(System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNWP8Dialog_t292_m3082_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral390;
extern "C" MNWP8Dialog_t292 * MNWP8Dialog_Create_m1813 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNWP8Dialog_t292_m3082_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483717);
		_stringLiteral390 = il2cpp_codegen_string_literal_from_index(390);
		s_Il2CppMethodIntialized = true;
	}
	MNWP8Dialog_t292 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral390, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNWP8Dialog_t292 * L_1 = GameObject_AddComponent_TisMNWP8Dialog_t292_m3082(L_0, /*hidden argument*/GameObject_AddComponent_TisMNWP8Dialog_t292_m3082_MethodInfo_var);
		V_0 = L_1;
		MNWP8Dialog_t292 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNWP8Dialog_t292 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNWP8Dialog_t292 * L_6 = V_0;
		NullCheck(L_6);
		MNWP8Dialog_init_m1814(L_6, /*hidden argument*/NULL);
		MNWP8Dialog_t292 * L_7 = V_0;
		return L_7;
	}
}
// System.Void MNWP8Dialog::init()
extern "C" void MNWP8Dialog_init_m1814 (MNWP8Dialog_t292 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MNWP8Dialog::OnOkDel()
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNWP8Dialog_OnOkDel_m1815 (MNWP8Dialog_t292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, 0, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		GameObject_t256 * L_1 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNWP8Dialog::OnCancelDel()
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNWP8Dialog_OnCancelDel_m1816 (MNWP8Dialog_t292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, 1, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		GameObject_t256 * L_1 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// MNWP8Message
#include "AssemblyU2DCSharp_MNWP8Message.h"
#ifndef _MSC_VER
#else
#endif
// MNWP8Message
#include "AssemblyU2DCSharp_MNWP8MessageMethodDeclarations.h"

struct GameObject_t256;
struct MNWP8Message_t293;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNWP8Message>()
// !!0 UnityEngine.GameObject::AddComponent<MNWP8Message>()
#define GameObject_AddComponent_TisMNWP8Message_t293_m3083(__this, method) (( MNWP8Message_t293 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNWP8Message::.ctor()
extern "C" void MNWP8Message__ctor_m1817 (MNWP8Message_t293 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNWP8Message MNWP8Message::Create(System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNWP8Message_t293_m3083_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral391;
extern "C" MNWP8Message_t293 * MNWP8Message_Create_m1818 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNWP8Message_t293_m3083_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483718);
		_stringLiteral391 = il2cpp_codegen_string_literal_from_index(391);
		s_Il2CppMethodIntialized = true;
	}
	MNWP8Message_t293 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral391, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNWP8Message_t293 * L_1 = GameObject_AddComponent_TisMNWP8Message_t293_m3083(L_0, /*hidden argument*/GameObject_AddComponent_TisMNWP8Message_t293_m3083_MethodInfo_var);
		V_0 = L_1;
		MNWP8Message_t293 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNWP8Message_t293 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNWP8Message_t293 * L_6 = V_0;
		NullCheck(L_6);
		MNWP8Message_init_m1819(L_6, /*hidden argument*/NULL);
		MNWP8Message_t293 * L_7 = V_0;
		return L_7;
	}
}
// System.Void MNWP8Message::init()
extern "C" void MNWP8Message_init_m1819 (MNWP8Message_t293 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MNWP8Message::onPopUpCallBack()
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNWP8Message_onPopUpCallBack_m1820 (MNWP8Message_t293 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, 0, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		GameObject_t256 * L_1 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// MNWP8RateUsPopUp
#include "AssemblyU2DCSharp_MNWP8RateUsPopUp.h"
#ifndef _MSC_VER
#else
#endif
// MNWP8RateUsPopUp
#include "AssemblyU2DCSharp_MNWP8RateUsPopUpMethodDeclarations.h"

struct GameObject_t256;
struct MNWP8RateUsPopUp_t294;
// Declaration !!0 UnityEngine.GameObject::AddComponent<MNWP8RateUsPopUp>()
// !!0 UnityEngine.GameObject::AddComponent<MNWP8RateUsPopUp>()
#define GameObject_AddComponent_TisMNWP8RateUsPopUp_t294_m3084(__this, method) (( MNWP8RateUsPopUp_t294 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void MNWP8RateUsPopUp::.ctor()
extern "C" void MNWP8RateUsPopUp__ctor_m1821 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method)
{
	{
		MNPopup__ctor_m1733(__this, /*hidden argument*/NULL);
		return;
	}
}
// MNWP8RateUsPopUp MNWP8RateUsPopUp::Create(System.String,System.String)
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMNWP8RateUsPopUp_t294_m3084_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral392;
extern "C" MNWP8RateUsPopUp_t294 * MNWP8RateUsPopUp_Create_m1822 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisMNWP8RateUsPopUp_t294_m3084_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483719);
		_stringLiteral392 = il2cpp_codegen_string_literal_from_index(392);
		s_Il2CppMethodIntialized = true;
	}
	MNWP8RateUsPopUp_t294 * V_0 = {0};
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral392, /*hidden argument*/NULL);
		NullCheck(L_0);
		MNWP8RateUsPopUp_t294 * L_1 = GameObject_AddComponent_TisMNWP8RateUsPopUp_t294_m3084(L_0, /*hidden argument*/GameObject_AddComponent_TisMNWP8RateUsPopUp_t294_m3084_MethodInfo_var);
		V_0 = L_1;
		MNWP8RateUsPopUp_t294 * L_2 = V_0;
		String_t* L_3 = ___title;
		NullCheck(L_2);
		((MNPopup_t278 *)L_2)->___title_2 = L_3;
		MNWP8RateUsPopUp_t294 * L_4 = V_0;
		String_t* L_5 = ___message;
		NullCheck(L_4);
		((MNPopup_t278 *)L_4)->___message_3 = L_5;
		MNWP8RateUsPopUp_t294 * L_6 = V_0;
		NullCheck(L_6);
		MNWP8RateUsPopUp_init_m1823(L_6, /*hidden argument*/NULL);
		MNWP8RateUsPopUp_t294 * L_7 = V_0;
		return L_7;
	}
}
// System.Void MNWP8RateUsPopUp::init()
extern "C" void MNWP8RateUsPopUp_init_m1823 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void MNWP8RateUsPopUp::OnOkDel()
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNWP8RateUsPopUp_OnOkDel_m1824 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, 2, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		GameObject_t256 * L_1 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MNWP8RateUsPopUp::OnCancelDel()
extern const MethodInfo* Action_1_Invoke_m3074_MethodInfo_var;
extern "C" void MNWP8RateUsPopUp_OnCancelDel_m1825 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_Invoke_m3074_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483706);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t277 * L_0 = (((MNPopup_t278 *)__this)->___OnComplete_4);
		NullCheck(L_0);
		Action_1_Invoke_m3074(L_0, 4, /*hidden argument*/Action_1_Invoke_m3074_MethodInfo_var);
		GameObject_t256 * L_1 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Object_Destroy_m2815(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// BaseWP8Popup
#include "AssemblyU2DCSharp_BaseWP8Popup.h"
#ifndef _MSC_VER
#else
#endif
// BaseWP8Popup
#include "AssemblyU2DCSharp_BaseWP8PopupMethodDeclarations.h"



// System.Void BaseWP8Popup::.ctor()
extern "C" void BaseWP8Popup__ctor_m1826 (BaseWP8Popup_t295 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// SA_DataConverter
#include "AssemblyU2DCSharp_SA_DataConverter.h"
#ifndef _MSC_VER
#else
#endif
// SA_DataConverter
#include "AssemblyU2DCSharp_SA_DataConverterMethodDeclarations.h"

// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"


// System.Void SA_DataConverter::.ctor()
extern "C" void SA_DataConverter__ctor_m1827 (SA_DataConverter_t296 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SA_DataConverter::SerializeArray(System.String[],System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* SA_DataConverter_SerializeArray_m1828 (Object_t * __this /* static, unused */, StringU5BU5D_t398* ___array, String_t* ___splitter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		StringU5BU5D_t398* L_0 = ___array;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_1;
	}

IL_000c:
	{
		StringU5BU5D_t398* L_2 = ___array;
		NullCheck(L_2);
		if ((((int32_t)(((Array_t *)L_2)->max_length))))
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_3;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_4;
		StringU5BU5D_t398* L_5 = ___array;
		NullCheck(L_5);
		V_1 = (((int32_t)(((Array_t *)L_5)->max_length)));
		V_2 = 0;
		goto IL_0047;
	}

IL_002b:
	{
		int32_t L_6 = V_2;
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		String_t* L_7 = V_0;
		String_t* L_8 = ___splitter;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m3031(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
	}

IL_0039:
	{
		String_t* L_10 = V_0;
		StringU5BU5D_t398* L_11 = ___array;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3031(NULL /*static, unused*/, L_10, (*(String_t**)(String_t**)SZArrayLdElema(L_11, L_13)), /*hidden argument*/NULL);
		V_0 = L_14;
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_16 = V_2;
		int32_t L_17 = V_1;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_18 = V_0;
		return L_18;
	}
}
// System.String[] SA_DataConverter::ParseArray(System.String,System.String)
extern TypeInfo* List_1_t468_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t398_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2831_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2832_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral393;
extern "C" StringU5BU5D_t398* SA_DataConverter_ParseArray_m1829 (Object_t * __this /* static, unused */, String_t* ___arrayData, String_t* ___splitter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t468_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		StringU5BU5D_t398_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(252);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		List_1__ctor_m2831_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		List_1_ToArray_m2832_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		_stringLiteral393 = il2cpp_codegen_string_literal_from_index(393);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t468 * V_0 = {0};
	StringU5BU5D_t398* V_1 = {0};
	int32_t V_2 = 0;
	{
		List_1_t468 * L_0 = (List_1_t468 *)il2cpp_codegen_object_new (List_1_t468_il2cpp_TypeInfo_var);
		List_1__ctor_m2831(L_0, /*hidden argument*/List_1__ctor_m2831_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___arrayData;
		StringU5BU5D_t398* L_2 = ((StringU5BU5D_t398*)SZArrayNew(StringU5BU5D_t398_il2cpp_TypeInfo_var, 1));
		String_t* L_3 = ___splitter;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 0)) = (String_t*)L_3;
		NullCheck(L_1);
		StringU5BU5D_t398* L_4 = String_Split_m3085(L_1, L_2, 0, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0043;
	}

IL_001f:
	{
		StringU5BU5D_t398* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m2981(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_5, L_7)), _stringLiteral393, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_004c;
	}

IL_0036:
	{
		List_1_t468 * L_9 = V_0;
		StringU5BU5D_t398* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_9, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_12)));
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0043:
	{
		int32_t L_14 = V_2;
		StringU5BU5D_t398* L_15 = V_1;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)(((Array_t *)L_15)->max_length))))))
		{
			goto IL_001f;
		}
	}

IL_004c:
	{
		List_1_t468 * L_16 = V_0;
		NullCheck(L_16);
		StringU5BU5D_t398* L_17 = List_1_ToArray_m2832(L_16, /*hidden argument*/List_1_ToArray_m2832_MethodInfo_var);
		return L_17;
	}
}
// SA_IdFactory/<>c__AnonStorey1E
#include "AssemblyU2DCSharp_SA_IdFactory_U3CU3Ec__AnonStorey1E.h"
#ifndef _MSC_VER
#else
#endif
// SA_IdFactory/<>c__AnonStorey1E
#include "AssemblyU2DCSharp_SA_IdFactory_U3CU3Ec__AnonStorey1EMethodDeclarations.h"

// System.Char
#include "mscorlib_System_Char.h"
// System.Random
#include "mscorlib_System_Random.h"
// System.Random
#include "mscorlib_System_RandomMethodDeclarations.h"


// System.Void SA_IdFactory/<>c__AnonStorey1E::.ctor()
extern "C" void U3CU3Ec__AnonStorey1E__ctor_m1830 (U3CU3Ec__AnonStorey1E_t298 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Char SA_IdFactory/<>c__AnonStorey1E::<>m__7(System.String)
extern "C" uint16_t U3CU3Ec__AnonStorey1E_U3CU3Em__7_m1831 (U3CU3Ec__AnonStorey1E_t298 * __this, String_t* ___s, const MethodInfo* method)
{
	{
		String_t* L_0 = ___s;
		Random_t297 * L_1 = (__this->___random_0);
		String_t* L_2 = ___s;
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3086(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_4 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32) */, L_1, L_3);
		NullCheck(L_0);
		uint16_t L_5 = String_get_Chars_m3087(L_0, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// SA_IdFactory
#include "AssemblyU2DCSharp_SA_IdFactory.h"
#ifndef _MSC_VER
#else
#endif
// SA_IdFactory
#include "AssemblyU2DCSharp_SA_IdFactoryMethodDeclarations.h"

// System.Func`2<System.String,System.Char>
#include "System_Core_System_Func_2_gen_1.h"
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
// System.Func`2<System.String,System.Char>
#include "System_Core_System_Func_2_gen_1MethodDeclarations.h"
struct Enumerable_t512;
struct IEnumerable_1_t525;
struct String_t;
struct Enumerable_t512;
struct IEnumerable_1_t515;
struct Object_t;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Repeat<System.Object>(!!0,System.Int32)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Repeat<System.Object>(!!0,System.Int32)
extern "C" Object_t* Enumerable_Repeat_TisObject_t_m3089_gshared (Object_t * __this /* static, unused */, Object_t * p0, int32_t p1, const MethodInfo* method);
#define Enumerable_Repeat_TisObject_t_m3089(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Enumerable_Repeat_TisObject_t_m3089_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Repeat<System.String>(!!0,System.Int32)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Repeat<System.String>(!!0,System.Int32)
#define Enumerable_Repeat_TisString_t_m3088(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, String_t*, int32_t, const MethodInfo*))Enumerable_Repeat_TisObject_t_m3089_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t512;
struct IEnumerable_1_t527;
struct IEnumerable_1_t525;
struct Func_2_t528;
struct Enumerable_t512;
struct IEnumerable_1_t527;
struct IEnumerable_1_t515;
struct Func_2_t529;
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Char>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Char>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C" Object_t* Enumerable_Select_TisObject_t_TisChar_t526_m3091_gshared (Object_t * __this /* static, unused */, Object_t* p0, Func_2_t529 * p1, const MethodInfo* method);
#define Enumerable_Select_TisObject_t_TisChar_t526_m3091(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t529 *, const MethodInfo*))Enumerable_Select_TisObject_t_TisChar_t526_m3091_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.String,System.Char>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.String,System.Char>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisString_t_TisChar_t526_m3090(__this /* static, unused */, p0, p1, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t*, Func_2_t528 *, const MethodInfo*))Enumerable_Select_TisObject_t_TisChar_t526_m3091_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t512;
struct CharU5BU5D_t530;
struct IEnumerable_1_t527;
// Declaration !!0[] System.Linq.Enumerable::ToArray<System.Char>(System.Collections.Generic.IEnumerable`1<!!0>)
// !!0[] System.Linq.Enumerable::ToArray<System.Char>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" CharU5BU5D_t530* Enumerable_ToArray_TisChar_t526_m3092_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisChar_t526_m3092(__this /* static, unused */, p0, method) (( CharU5BU5D_t530* (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToArray_TisChar_t526_m3092_gshared)(__this /* static, unused */, p0, method)


// System.Void SA_IdFactory::.ctor()
extern "C" void SA_IdFactory__ctor_m1832 (SA_IdFactory_t299 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 SA_IdFactory::get_NextId()
extern Il2CppCodeGenString* _stringLiteral394;
extern "C" int32_t SA_IdFactory_get_NextId_m1833 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral394 = il2cpp_codegen_string_literal_from_index(394);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 1;
		bool L_0 = PlayerPrefs_HasKey_m3093(NULL /*static, unused*/, _stringLiteral394, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = PlayerPrefs_GetInt_m3094(NULL /*static, unused*/, _stringLiteral394, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_3 = V_0;
		PlayerPrefs_SetInt_m3095(NULL /*static, unused*/, _stringLiteral394, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		return L_4;
	}
}
// System.String SA_IdFactory::get_RandomString()
extern TypeInfo* U3CU3Ec__AnonStorey1E_t298_il2cpp_TypeInfo_var;
extern TypeInfo* Random_t297_il2cpp_TypeInfo_var;
extern TypeInfo* Func_2_t528_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_Repeat_TisString_t_m3088_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__AnonStorey1E_U3CU3Em__7_m1831_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3097_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisString_t_TisChar_t526_m3090_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisChar_t526_m3092_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral395;
extern "C" String_t* SA_IdFactory_get_RandomString_m1834 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__AnonStorey1E_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(254);
		Random_t297_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(255);
		Func_2_t528_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(256);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		Enumerable_Repeat_TisString_t_m3088_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483720);
		U3CU3Ec__AnonStorey1E_U3CU3Em__7_m1831_MethodInfo_var = il2cpp_codegen_method_info_from_index(73);
		Func_2__ctor_m3097_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483722);
		Enumerable_Select_TisString_t_TisChar_t526_m3090_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483723);
		Enumerable_ToArray_TisChar_t526_m3092_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483724);
		_stringLiteral395 = il2cpp_codegen_string_literal_from_index(395);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	U3CU3Ec__AnonStorey1E_t298 * V_2 = {0};
	{
		U3CU3Ec__AnonStorey1E_t298 * L_0 = (U3CU3Ec__AnonStorey1E_t298 *)il2cpp_codegen_object_new (U3CU3Ec__AnonStorey1E_t298_il2cpp_TypeInfo_var);
		U3CU3Ec__AnonStorey1E__ctor_m1830(L_0, /*hidden argument*/NULL);
		V_2 = L_0;
		V_0 = _stringLiteral395;
		U3CU3Ec__AnonStorey1E_t298 * L_1 = V_2;
		Random_t297 * L_2 = (Random_t297 *)il2cpp_codegen_object_new (Random_t297_il2cpp_TypeInfo_var);
		Random__ctor_m3096(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___random_0 = L_2;
		String_t* L_3 = V_0;
		Object_t* L_4 = Enumerable_Repeat_TisString_t_m3088(NULL /*static, unused*/, L_3, 8, /*hidden argument*/Enumerable_Repeat_TisString_t_m3088_MethodInfo_var);
		U3CU3Ec__AnonStorey1E_t298 * L_5 = V_2;
		IntPtr_t L_6 = { (void*)U3CU3Ec__AnonStorey1E_U3CU3Em__7_m1831_MethodInfo_var };
		Func_2_t528 * L_7 = (Func_2_t528 *)il2cpp_codegen_object_new (Func_2_t528_il2cpp_TypeInfo_var);
		Func_2__ctor_m3097(L_7, L_5, L_6, /*hidden argument*/Func_2__ctor_m3097_MethodInfo_var);
		Object_t* L_8 = Enumerable_Select_TisString_t_TisChar_t526_m3090(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/Enumerable_Select_TisString_t_TisChar_t526_m3090_MethodInfo_var);
		CharU5BU5D_t530* L_9 = Enumerable_ToArray_TisChar_t526_m3092(NULL /*static, unused*/, L_8, /*hidden argument*/Enumerable_ToArray_TisChar_t526_m3092_MethodInfo_var);
		String_t* L_10 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_10 = String_CreateString_m3098(L_10, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		String_t* L_11 = V_1;
		return L_11;
	}
}
// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3
#include "AssemblyU2DCSharp_SA_ScreenShotMaker_U3CSaveScreenshotU3Ec__.h"
#ifndef _MSC_VER
#else
#endif
// SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3
#include "AssemblyU2DCSharp_SA_ScreenShotMaker_U3CSaveScreenshotU3Ec__MethodDeclarations.h"

// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// SA_ScreenShotMaker
#include "AssemblyU2DCSharp_SA_ScreenShotMaker.h"
// System.Action`1<UnityEngine.Texture2D>
#include "mscorlib_System_Action_1_gen_0.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
// System.Action`1<UnityEngine.Texture2D>
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"


// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::.ctor()
extern "C" void U3CSaveScreenshotU3Ec__Iterator3__ctor_m1835 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1836 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSaveScreenshotU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1837 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::MoveNext()
extern TypeInfo* WaitForEndOfFrame_t484_il2cpp_TypeInfo_var;
extern TypeInfo* Texture2D_t9_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3102_MethodInfo_var;
extern "C" bool U3CSaveScreenshotU3Ec__Iterator3_MoveNext_m1838 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForEndOfFrame_t484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(171);
		Texture2D_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Action_1_Invoke_m3102_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483725);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_00c9;
	}

IL_0021:
	{
		WaitForEndOfFrame_t484 * L_2 = (WaitForEndOfFrame_t484 *)il2cpp_codegen_object_new (WaitForEndOfFrame_t484_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m2816(L_2, /*hidden argument*/NULL);
		__this->___U24current_4 = L_2;
		__this->___U24PC_3 = 1;
		goto IL_00cb;
	}

IL_0038:
	{
		int32_t L_3 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CwidthU3E__0_0 = L_3;
		int32_t L_4 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CheightU3E__1_1 = L_4;
		int32_t L_5 = (__this->___U3CwidthU3E__0_0);
		int32_t L_6 = (__this->___U3CheightU3E__1_1);
		Texture2D_t9 * L_7 = (Texture2D_t9 *)il2cpp_codegen_object_new (Texture2D_t9_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3099(L_7, L_5, L_6, 3, 0, /*hidden argument*/NULL);
		__this->___U3CtexU3E__2_2 = L_7;
		Texture2D_t9 * L_8 = (__this->___U3CtexU3E__2_2);
		int32_t L_9 = (__this->___U3CwidthU3E__0_0);
		int32_t L_10 = (__this->___U3CheightU3E__1_1);
		Rect_t225  L_11 = {0};
		Rect__ctor_m2802(&L_11, (0.0f), (0.0f), (((float)L_9)), (((float)L_10)), /*hidden argument*/NULL);
		NullCheck(L_8);
		Texture2D_ReadPixels_m3100(L_8, L_11, 0, 0, /*hidden argument*/NULL);
		Texture2D_t9 * L_12 = (__this->___U3CtexU3E__2_2);
		NullCheck(L_12);
		Texture2D_Apply_m3101(L_12, /*hidden argument*/NULL);
		SA_ScreenShotMaker_t300 * L_13 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_13);
		Action_1_t302 * L_14 = (L_13->___OnScreenshotReady_4);
		if (!L_14)
		{
			goto IL_00c2;
		}
	}
	{
		SA_ScreenShotMaker_t300 * L_15 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_15);
		Action_1_t302 * L_16 = (L_15->___OnScreenshotReady_4);
		Texture2D_t9 * L_17 = (__this->___U3CtexU3E__2_2);
		NullCheck(L_16);
		Action_1_Invoke_m3102(L_16, L_17, /*hidden argument*/Action_1_Invoke_m3102_MethodInfo_var);
	}

IL_00c2:
	{
		__this->___U24PC_3 = (-1);
	}

IL_00c9:
	{
		return 0;
	}

IL_00cb:
	{
		return 1;
	}
	// Dead block : IL_00cd: ldloc.1
}
// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::Dispose()
extern "C" void U3CSaveScreenshotU3Ec__Iterator3_Dispose_m1839 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void SA_ScreenShotMaker/<SaveScreenshot>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CSaveScreenshotU3Ec__Iterator3_Reset_m1840 (U3CSaveScreenshotU3Ec__Iterator3_t301 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// SA_ScreenShotMaker
#include "AssemblyU2DCSharp_SA_ScreenShotMakerMethodDeclarations.h"

// SA_Singleton`1<SA_ScreenShotMaker>
#include "AssemblyU2DCSharp_SA_Singleton_1_genMethodDeclarations.h"


// System.Void SA_ScreenShotMaker::.ctor()
extern TypeInfo* SA_Singleton_1_t303_il2cpp_TypeInfo_var;
extern const MethodInfo* SA_Singleton_1__ctor_m3103_MethodInfo_var;
extern "C" void SA_ScreenShotMaker__ctor_m1841 (SA_ScreenShotMaker_t300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SA_Singleton_1_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(258);
		SA_Singleton_1__ctor_m3103_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483726);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SA_Singleton_1_t303_il2cpp_TypeInfo_var);
		SA_Singleton_1__ctor_m3103(__this, /*hidden argument*/SA_Singleton_1__ctor_m3103_MethodInfo_var);
		return;
	}
}
// System.Void SA_ScreenShotMaker::GetScreenshot()
extern "C" void SA_ScreenShotMaker_GetScreenshot_m1842 (SA_ScreenShotMaker_t300 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = SA_ScreenShotMaker_SaveScreenshot_m1843(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator SA_ScreenShotMaker::SaveScreenshot()
extern TypeInfo* U3CSaveScreenshotU3Ec__Iterator3_t301_il2cpp_TypeInfo_var;
extern "C" Object_t * SA_ScreenShotMaker_SaveScreenshot_m1843 (SA_ScreenShotMaker_t300 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSaveScreenshotU3Ec__Iterator3_t301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(259);
		s_Il2CppMethodIntialized = true;
	}
	U3CSaveScreenshotU3Ec__Iterator3_t301 * V_0 = {0};
	{
		U3CSaveScreenshotU3Ec__Iterator3_t301 * L_0 = (U3CSaveScreenshotU3Ec__Iterator3_t301 *)il2cpp_codegen_object_new (U3CSaveScreenshotU3Ec__Iterator3_t301_il2cpp_TypeInfo_var);
		U3CSaveScreenshotU3Ec__Iterator3__ctor_m1835(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSaveScreenshotU3Ec__Iterator3_t301 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_5 = __this;
		U3CSaveScreenshotU3Ec__Iterator3_t301 * L_2 = V_0;
		return L_2;
	}
}
// WWWTextureLoader/<LoadCoroutin>c__Iterator4
#include "AssemblyU2DCSharp_WWWTextureLoader_U3CLoadCoroutinU3Ec__Iter.h"
#ifndef _MSC_VER
#else
#endif
// WWWTextureLoader/<LoadCoroutin>c__Iterator4
#include "AssemblyU2DCSharp_WWWTextureLoader_U3CLoadCoroutinU3Ec__IterMethodDeclarations.h"

// WWWTextureLoader
#include "AssemblyU2DCSharp_WWWTextureLoader.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"


// System.Void WWWTextureLoader/<LoadCoroutin>c__Iterator4::.ctor()
extern "C" void U3CLoadCoroutinU3Ec__Iterator4__ctor_m1844 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object WWWTextureLoader/<LoadCoroutin>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CLoadCoroutinU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1845 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object WWWTextureLoader/<LoadCoroutin>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CLoadCoroutinU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1846 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean WWWTextureLoader/<LoadCoroutin>c__Iterator4::MoveNext()
extern TypeInfo* WWW_t304_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3102_MethodInfo_var;
extern "C" bool U3CLoadCoroutinU3Ec__Iterator4_MoveNext_m1847 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWW_t304_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(260);
		Action_1_Invoke_m3102_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483725);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_0097;
	}

IL_0021:
	{
		WWWTextureLoader_t305 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		String_t* L_3 = (L_2->____url_2);
		WWW_t304 * L_4 = (WWW_t304 *)il2cpp_codegen_object_new (WWW_t304_il2cpp_TypeInfo_var);
		WWW__ctor_m3104(L_4, L_3, /*hidden argument*/NULL);
		__this->___U3CwwwU3E__0_0 = L_4;
		WWW_t304 * L_5 = (__this->___U3CwwwU3E__0_0);
		__this->___U24current_2 = L_5;
		__this->___U24PC_1 = 1;
		goto IL_0099;
	}

IL_004f:
	{
		WWW_t304 * L_6 = (__this->___U3CwwwU3E__0_0);
		NullCheck(L_6);
		String_t* L_7 = WWW_get_error_m3105(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_007f;
		}
	}
	{
		WWWTextureLoader_t305 * L_8 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_8);
		Action_1_t302 * L_9 = (L_8->___OnLoad_3);
		WWW_t304 * L_10 = (__this->___U3CwwwU3E__0_0);
		NullCheck(L_10);
		Texture2D_t9 * L_11 = WWW_get_texture_m3106(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		Action_1_Invoke_m3102(L_9, L_11, /*hidden argument*/Action_1_Invoke_m3102_MethodInfo_var);
		goto IL_0090;
	}

IL_007f:
	{
		WWWTextureLoader_t305 * L_12 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_12);
		Action_1_t302 * L_13 = (L_12->___OnLoad_3);
		NullCheck(L_13);
		Action_1_Invoke_m3102(L_13, (Texture2D_t9 *)NULL, /*hidden argument*/Action_1_Invoke_m3102_MethodInfo_var);
	}

IL_0090:
	{
		__this->___U24PC_1 = (-1);
	}

IL_0097:
	{
		return 0;
	}

IL_0099:
	{
		return 1;
	}
	// Dead block : IL_009b: ldloc.1
}
// System.Void WWWTextureLoader/<LoadCoroutin>c__Iterator4::Dispose()
extern "C" void U3CLoadCoroutinU3Ec__Iterator4_Dispose_m1848 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void WWWTextureLoader/<LoadCoroutin>c__Iterator4::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CLoadCoroutinU3Ec__Iterator4_Reset_m1849 (U3CLoadCoroutinU3Ec__Iterator4_t306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// WWWTextureLoader
#include "AssemblyU2DCSharp_WWWTextureLoaderMethodDeclarations.h"

struct GameObject_t256;
struct WWWTextureLoader_t305;
// Declaration !!0 UnityEngine.GameObject::AddComponent<WWWTextureLoader>()
// !!0 UnityEngine.GameObject::AddComponent<WWWTextureLoader>()
#define GameObject_AddComponent_TisWWWTextureLoader_t305_m3107(__this, method) (( WWWTextureLoader_t305 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m2820_gshared)(__this, method)


// System.Void WWWTextureLoader::.ctor()
extern TypeInfo* WWWTextureLoader_t305_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t302_il2cpp_TypeInfo_var;
extern const MethodInfo* WWWTextureLoader_U3COnLoadU3Em__8_m1856_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m3108_MethodInfo_var;
extern "C" void WWWTextureLoader__ctor_m1850 (WWWTextureLoader_t305 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTextureLoader_t305_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(261);
		Action_1_t302_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(262);
		WWWTextureLoader_U3COnLoadU3Em__8_m1856_MethodInfo_var = il2cpp_codegen_method_info_from_index(79);
		Action_1__ctor_m3108_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483728);
		s_Il2CppMethodIntialized = true;
	}
	WWWTextureLoader_t305 * G_B2_0 = {0};
	WWWTextureLoader_t305 * G_B1_0 = {0};
	{
		Action_1_t302 * L_0 = ((WWWTextureLoader_t305_StaticFields*)WWWTextureLoader_t305_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4;
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		IntPtr_t L_1 = { (void*)WWWTextureLoader_U3COnLoadU3Em__8_m1856_MethodInfo_var };
		Action_1_t302 * L_2 = (Action_1_t302 *)il2cpp_codegen_object_new (Action_1_t302_il2cpp_TypeInfo_var);
		Action_1__ctor_m3108(L_2, NULL, L_1, /*hidden argument*/Action_1__ctor_m3108_MethodInfo_var);
		((WWWTextureLoader_t305_StaticFields*)WWWTextureLoader_t305_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		Action_1_t302 * L_3 = ((WWWTextureLoader_t305_StaticFields*)WWWTextureLoader_t305_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__amU24cache2_4;
		NullCheck(G_B2_0);
		G_B2_0->___OnLoad_3 = L_3;
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WWWTextureLoader::add_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern TypeInfo* Action_1_t302_il2cpp_TypeInfo_var;
extern "C" void WWWTextureLoader_add_OnLoad_m1851 (WWWTextureLoader_t305 * __this, Action_1_t302 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t302_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(262);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t302 * L_0 = (__this->___OnLoad_3);
		Action_1_t302 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnLoad_3 = ((Action_1_t302 *)Castclass(L_2, Action_1_t302_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void WWWTextureLoader::remove_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern TypeInfo* Action_1_t302_il2cpp_TypeInfo_var;
extern "C" void WWWTextureLoader_remove_OnLoad_m1852 (WWWTextureLoader_t305 * __this, Action_1_t302 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t302_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(262);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t302 * L_0 = (__this->___OnLoad_3);
		Action_1_t302 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___OnLoad_3 = ((Action_1_t302 *)Castclass(L_2, Action_1_t302_il2cpp_TypeInfo_var));
		return;
	}
}
// WWWTextureLoader WWWTextureLoader::Create()
extern TypeInfo* GameObject_t256_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisWWWTextureLoader_t305_m3107_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral396;
extern "C" WWWTextureLoader_t305 * WWWTextureLoader_Create_m1853 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t256_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(174);
		GameObject_AddComponent_TisWWWTextureLoader_t305_m3107_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483729);
		_stringLiteral396 = il2cpp_codegen_string_literal_from_index(396);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = (GameObject_t256 *)il2cpp_codegen_object_new (GameObject_t256_il2cpp_TypeInfo_var);
		GameObject__ctor_m2824(L_0, _stringLiteral396, /*hidden argument*/NULL);
		NullCheck(L_0);
		WWWTextureLoader_t305 * L_1 = GameObject_AddComponent_TisWWWTextureLoader_t305_m3107(L_0, /*hidden argument*/GameObject_AddComponent_TisWWWTextureLoader_t305_m3107_MethodInfo_var);
		return L_1;
	}
}
// System.Void WWWTextureLoader::LoadTexture(System.String)
extern "C" void WWWTextureLoader_LoadTexture_m1854 (WWWTextureLoader_t305 * __this, String_t* ___url, const MethodInfo* method)
{
	{
		String_t* L_0 = ___url;
		__this->____url_2 = L_0;
		Object_t * L_1 = WWWTextureLoader_LoadCoroutin_m1855(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator WWWTextureLoader::LoadCoroutin()
extern TypeInfo* U3CLoadCoroutinU3Ec__Iterator4_t306_il2cpp_TypeInfo_var;
extern "C" Object_t * WWWTextureLoader_LoadCoroutin_m1855 (WWWTextureLoader_t305 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CLoadCoroutinU3Ec__Iterator4_t306_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(263);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadCoroutinU3Ec__Iterator4_t306 * V_0 = {0};
	{
		U3CLoadCoroutinU3Ec__Iterator4_t306 * L_0 = (U3CLoadCoroutinU3Ec__Iterator4_t306 *)il2cpp_codegen_object_new (U3CLoadCoroutinU3Ec__Iterator4_t306_il2cpp_TypeInfo_var);
		U3CLoadCoroutinU3Ec__Iterator4__ctor_m1844(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadCoroutinU3Ec__Iterator4_t306 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CLoadCoroutinU3Ec__Iterator4_t306 * L_2 = V_0;
		return L_2;
	}
}
// System.Void WWWTextureLoader::<OnLoad>m__8(UnityEngine.Texture2D)
extern "C" void WWWTextureLoader_U3COnLoadU3Em__8_m1856 (Object_t * __this /* static, unused */, Texture2D_t9 * p0, const MethodInfo* method)
{
	{
		return;
	}
}
// AICharacterControl
#include "AssemblyU2DCSharp_AICharacterControl.h"
#ifndef _MSC_VER
#else
#endif
// AICharacterControl
#include "AssemblyU2DCSharp_AICharacterControlMethodDeclarations.h"

// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgent.h"
struct Component_t477;
struct NavMeshAgent_t307;
struct Component_t477;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m3110_gshared (Component_t477 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m3110(__this, method) (( Object_t * (*) (Component_t477 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m3110_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.NavMeshAgent>()
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.NavMeshAgent>()
#define Component_GetComponentInChildren_TisNavMeshAgent_t307_m3109(__this, method) (( NavMeshAgent_t307 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m3110_gshared)(__this, method)


// System.Void AICharacterControl::.ctor()
extern "C" void AICharacterControl__ctor_m1857 (AICharacterControl_t308 * __this, const MethodInfo* method)
{
	{
		__this->___targetChangeTolerance_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.NavMeshAgent AICharacterControl::get_agent()
extern "C" NavMeshAgent_t307 * AICharacterControl_get_agent_m1858 (AICharacterControl_t308 * __this, const MethodInfo* method)
{
	{
		NavMeshAgent_t307 * L_0 = (__this->___U3CagentU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void AICharacterControl::set_agent(UnityEngine.NavMeshAgent)
extern "C" void AICharacterControl_set_agent_m1859 (AICharacterControl_t308 * __this, NavMeshAgent_t307 * ___value, const MethodInfo* method)
{
	{
		NavMeshAgent_t307 * L_0 = ___value;
		__this->___U3CagentU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Void AICharacterControl::Start()
extern const MethodInfo* Component_GetComponentInChildren_TisNavMeshAgent_t307_m3109_MethodInfo_var;
extern "C" void AICharacterControl_Start_m1860 (AICharacterControl_t308 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisNavMeshAgent_t307_m3109_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483730);
		s_Il2CppMethodIntialized = true;
	}
	NavMeshAgent_t307 * V_0 = {0};
	{
		NavMeshAgent_t307 * L_0 = Component_GetComponentInChildren_TisNavMeshAgent_t307_m3109(__this, /*hidden argument*/Component_GetComponentInChildren_TisNavMeshAgent_t307_m3109_MethodInfo_var);
		V_0 = L_0;
		return;
	}
}
// System.Void AICharacterControl::Update()
extern "C" void AICharacterControl_Update_m1861 (AICharacterControl_t308 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Transform_t243 * L_0 = (__this->___target_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t243 * L_2 = (__this->___target_2);
		NullCheck(L_2);
		Vector3_t215  L_3 = Transform_get_position_m2894(L_2, /*hidden argument*/NULL);
		Vector3_t215  L_4 = (__this->___targetPos_4);
		Vector3_t215  L_5 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = Vector3_get_magnitude_m3033((&V_0), /*hidden argument*/NULL);
		float L_7 = (__this->___targetChangeTolerance_3);
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_003a;
		}
	}

IL_003a:
	{
		goto IL_003f;
	}

IL_003f:
	{
		return;
	}
}
// System.Void AICharacterControl::SetTarget(UnityEngine.Transform)
extern "C" void AICharacterControl_SetTarget_m1862 (AICharacterControl_t308 * __this, Transform_t243 * ___target, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = ___target;
		__this->___target_2 = L_0;
		return;
	}
}
// CoroutineScript/<testCor>c__Iterator5
#include "AssemblyU2DCSharp_CoroutineScript_U3CtestCorU3Ec__Iterator5.h"
#ifndef _MSC_VER
#else
#endif
// CoroutineScript/<testCor>c__Iterator5
#include "AssemblyU2DCSharp_CoroutineScript_U3CtestCorU3Ec__Iterator5MethodDeclarations.h"

// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"


// System.Void CoroutineScript/<testCor>c__Iterator5::.ctor()
extern "C" void U3CtestCorU3Ec__Iterator5__ctor_m1863 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CoroutineScript/<testCor>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CtestCorU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1864 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object CoroutineScript/<testCor>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CtestCorU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1865 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean CoroutineScript/<testCor>c__Iterator5::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral397;
extern "C" bool U3CtestCorU3Ec__Iterator5_MoveNext_m1866 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		_stringLiteral397 = il2cpp_codegen_string_literal_from_index(397);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_004f;
	}

IL_0021:
	{
		float L_2 = (__this->___time_0);
		WaitForSeconds_t475 * L_3 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_2 = L_3;
		__this->___U24PC_1 = 1;
		goto IL_0051;
	}

IL_003e:
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral397, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_004f:
	{
		return 0;
	}

IL_0051:
	{
		return 1;
	}
	// Dead block : IL_0053: ldloc.1
}
// System.Void CoroutineScript/<testCor>c__Iterator5::Dispose()
extern "C" void U3CtestCorU3Ec__Iterator5_Dispose_m1867 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void CoroutineScript/<testCor>c__Iterator5::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CtestCorU3Ec__Iterator5_Reset_m1868 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// CoroutineScript
#include "AssemblyU2DCSharp_CoroutineScript.h"
#ifndef _MSC_VER
#else
#endif
// CoroutineScript
#include "AssemblyU2DCSharp_CoroutineScriptMethodDeclarations.h"



// System.Void CoroutineScript::.ctor()
extern "C" void CoroutineScript__ctor_m1869 (CoroutineScript_t310 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single CoroutineScript::get_CoroutineTime()
extern "C" float CoroutineScript_get_CoroutineTime_m1870 (CoroutineScript_t310 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___coroutineTime_2);
		return L_0;
	}
}
// System.Void CoroutineScript::set_CoroutineTime(System.Single)
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral398;
extern "C" void CoroutineScript_set_CoroutineTime_m1871 (CoroutineScript_t310 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		_stringLiteral398 = il2cpp_codegen_string_literal_from_index(398);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value;
		__this->___coroutineTime_2 = L_0;
		MonoBehaviour_StopCoroutine_m2846(__this, _stringLiteral398, /*hidden argument*/NULL);
		float L_1 = (__this->___coroutineTime_2);
		float L_2 = L_1;
		Object_t * L_3 = Box(Single_t531_il2cpp_TypeInfo_var, &L_2);
		MonoBehaviour_StartCoroutine_m3111(__this, _stringLiteral398, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CoroutineScript::testCor(System.Single)
extern TypeInfo* U3CtestCorU3Ec__Iterator5_t309_il2cpp_TypeInfo_var;
extern "C" Object_t * CoroutineScript_testCor_m1872 (CoroutineScript_t310 * __this, float ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CtestCorU3Ec__Iterator5_t309_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(265);
		s_Il2CppMethodIntialized = true;
	}
	U3CtestCorU3Ec__Iterator5_t309 * V_0 = {0};
	{
		U3CtestCorU3Ec__Iterator5_t309 * L_0 = (U3CtestCorU3Ec__Iterator5_t309 *)il2cpp_codegen_object_new (U3CtestCorU3Ec__Iterator5_t309_il2cpp_TypeInfo_var);
		U3CtestCorU3Ec__Iterator5__ctor_m1863(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CtestCorU3Ec__Iterator5_t309 * L_1 = V_0;
		float L_2 = ___time;
		NullCheck(L_1);
		L_1->___time_0 = L_2;
		U3CtestCorU3Ec__Iterator5_t309 * L_3 = V_0;
		float L_4 = ___time;
		NullCheck(L_3);
		L_3->___U3CU24U3Etime_3 = L_4;
		U3CtestCorU3Ec__Iterator5_t309 * L_5 = V_0;
		return L_5;
	}
}
// System.Void CoroutineScript::Start()
extern "C" void CoroutineScript_Start_m1873 (CoroutineScript_t310 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CoroutineScript::Update()
extern "C" void CoroutineScript_Update_m1874 (CoroutineScript_t310 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// DoorController
#include "AssemblyU2DCSharp_DoorController.h"
#ifndef _MSC_VER
#else
#endif
// DoorController
#include "AssemblyU2DCSharp_DoorControllerMethodDeclarations.h"

// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// System.Collections.Hashtable
#include "mscorlib_System_Collections_Hashtable.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
// InteractiveEventManager
#include "AssemblyU2DCSharp_InteractiveEventManager.h"
// InteractiveEvent
#include "AssemblyU2DCSharp_InteractiveEvent.h"
// iTween
#include "AssemblyU2DCSharp_iTweenMethodDeclarations.h"
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
// InteractiveEventManager
#include "AssemblyU2DCSharp_InteractiveEventManagerMethodDeclarations.h"
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
struct Component_t477;
struct AudioSource_t339;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t339_m3112(__this, method) (( AudioSource_t339 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)
struct Component_t477;
struct Rigidbody_t325;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t325_m3113(__this, method) (( Rigidbody_t325 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void DoorController::.ctor()
extern "C" void DoorController__ctor_m1875 (DoorController_t311 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DoorController::Start()
extern "C" void DoorController_Start_m1876 (DoorController_t311 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DoorController::Update()
extern "C" void DoorController_Update_m1877 (DoorController_t311 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DoorController::OpenDoors(System.Single,System.Single,System.Single,System.String)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* EaseType_t425_il2cpp_TypeInfo_var;
extern TypeInfo* iTween_t432_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral399;
extern Il2CppCodeGenString* _stringLiteral400;
extern Il2CppCodeGenString* _stringLiteral401;
extern Il2CppCodeGenString* _stringLiteral402;
extern Il2CppCodeGenString* _stringLiteral403;
extern Il2CppCodeGenString* _stringLiteral404;
extern Il2CppCodeGenString* _stringLiteral405;
extern Il2CppCodeGenString* _stringLiteral406;
extern "C" void DoorController_OpenDoors_m1878 (DoorController_t311 * __this, float ___rot, float ___time, float ___delay, String_t* ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		EaseType_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(266);
		iTween_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(267);
		_stringLiteral399 = il2cpp_codegen_string_literal_from_index(399);
		_stringLiteral400 = il2cpp_codegen_string_literal_from_index(400);
		_stringLiteral401 = il2cpp_codegen_string_literal_from_index(401);
		_stringLiteral402 = il2cpp_codegen_string_literal_from_index(402);
		_stringLiteral403 = il2cpp_codegen_string_literal_from_index(403);
		_stringLiteral404 = il2cpp_codegen_string_literal_from_index(404);
		_stringLiteral405 = il2cpp_codegen_string_literal_from_index(405);
		_stringLiteral406 = il2cpp_codegen_string_literal_from_index(406);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t470* L_1 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, ((int32_t)12)));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral399);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)_stringLiteral399;
		ObjectU5BU5D_t470* L_2 = L_1;
		float L_3 = ___rot;
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t531_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t470* L_6 = L_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral400);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)_stringLiteral400;
		ObjectU5BU5D_t470* L_7 = L_6;
		float L_8 = ___time;
		float L_9 = L_8;
		Object_t * L_10 = Box(Single_t531_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_10;
		ObjectU5BU5D_t470* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 4);
		ArrayElementTypeCheck (L_11, _stringLiteral401);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 4)) = (Object_t *)_stringLiteral401;
		ObjectU5BU5D_t470* L_12 = L_11;
		float L_13 = ___delay;
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 5)) = (Object_t *)L_15;
		ObjectU5BU5D_t470* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 6);
		ArrayElementTypeCheck (L_16, _stringLiteral402);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 6)) = (Object_t *)_stringLiteral402;
		ObjectU5BU5D_t470* L_17 = L_16;
		int32_t L_18 = ((int32_t)21);
		Object_t * L_19 = Box(EaseType_t425_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 7)) = (Object_t *)L_19;
		ObjectU5BU5D_t470* L_20 = L_17;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 8);
		ArrayElementTypeCheck (L_20, _stringLiteral403);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 8)) = (Object_t *)_stringLiteral403;
		ObjectU5BU5D_t470* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)9));
		ArrayElementTypeCheck (L_21, _stringLiteral404);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, ((int32_t)9))) = (Object_t *)_stringLiteral404;
		ObjectU5BU5D_t470* L_22 = L_21;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)10));
		ArrayElementTypeCheck (L_22, _stringLiteral405);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, ((int32_t)10))) = (Object_t *)_stringLiteral405;
		ObjectU5BU5D_t470* L_23 = L_22;
		GameObject_t256 * L_24 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, ((int32_t)11));
		ArrayElementTypeCheck (L_23, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, ((int32_t)11))) = (Object_t *)L_24;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t432_il2cpp_TypeInfo_var);
		Hashtable_t348 * L_25 = iTween_Hash_m2629(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		iTween_RotateTo_m2476(NULL /*static, unused*/, L_0, L_25, /*hidden argument*/NULL);
		String_t* L_26 = ___val;
		__this->___door_2 = L_26;
		float L_27 = ___delay;
		MonoBehaviour_Invoke_m3073(__this, _stringLiteral406, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DoorController::PlayAudio()
extern const MethodInfo* Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var;
extern "C" void DoorController_PlayAudio_m1879 (DoorController_t311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483731);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t339 * L_0 = Component_GetComponent_TisAudioSource_t339_m3112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var);
		NullCheck(L_0);
		bool L_1 = AudioSource_get_isPlaying_m3114(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		AudioSource_t339 * L_2 = Component_GetComponent_TisAudioSource_t339_m3112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var);
		NullCheck(L_2);
		AudioSource_Play_m3115(L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void DoorController::CloseDoors()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InteractiveEventManager_t362_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* EaseType_t425_il2cpp_TypeInfo_var;
extern TypeInfo* iTween_t432_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t325_m3113_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral407;
extern Il2CppCodeGenString* _stringLiteral399;
extern Il2CppCodeGenString* _stringLiteral400;
extern Il2CppCodeGenString* _stringLiteral401;
extern Il2CppCodeGenString* _stringLiteral402;
extern Il2CppCodeGenString* _stringLiteral408;
extern "C" void DoorController_CloseDoors_m1880 (DoorController_t311 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		InteractiveEventManager_t362_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(270);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		EaseType_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(266);
		iTween_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(267);
		Component_GetComponent_TisRigidbody_t325_m3113_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483732);
		_stringLiteral407 = il2cpp_codegen_string_literal_from_index(407);
		_stringLiteral399 = il2cpp_codegen_string_literal_from_index(399);
		_stringLiteral400 = il2cpp_codegen_string_literal_from_index(400);
		_stringLiteral401 = il2cpp_codegen_string_literal_from_index(401);
		_stringLiteral402 = il2cpp_codegen_string_literal_from_index(402);
		_stringLiteral408 = il2cpp_codegen_string_literal_from_index(408);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody_t325 * V_0 = {0};
	{
		String_t* L_0 = (__this->___door_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m2981(NULL /*static, unused*/, L_0, _stringLiteral407, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InteractiveEventManager_t362_il2cpp_TypeInfo_var);
		InteractiveEventManager_t362 * L_2 = InteractiveEventManager_get_Instance_m2102(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		InteractiveEventManager_dispatchEvent_m2103(L_2, ((int32_t)9), /*hidden argument*/NULL);
		GameObject_t256 * L_3 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t470* L_4 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 8));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, _stringLiteral399);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 0)) = (Object_t *)_stringLiteral399;
		ObjectU5BU5D_t470* L_5 = L_4;
		float L_6 = (0.0f);
		Object_t * L_7 = Box(Single_t531_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 1);
		ArrayElementTypeCheck (L_5, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 1)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		ArrayElementTypeCheck (L_8, _stringLiteral400);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 2)) = (Object_t *)_stringLiteral400;
		ObjectU5BU5D_t470* L_9 = L_8;
		float L_10 = (5.0f);
		Object_t * L_11 = Box(Single_t531_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 3)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_9;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral401);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4)) = (Object_t *)_stringLiteral401;
		ObjectU5BU5D_t470* L_13 = L_12;
		float L_14 = (3.0f);
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5)) = (Object_t *)L_15;
		ObjectU5BU5D_t470* L_16 = L_13;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 6);
		ArrayElementTypeCheck (L_16, _stringLiteral402);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 6)) = (Object_t *)_stringLiteral402;
		ObjectU5BU5D_t470* L_17 = L_16;
		int32_t L_18 = ((int32_t)30);
		Object_t * L_19 = Box(EaseType_t425_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 7)) = (Object_t *)L_19;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t432_il2cpp_TypeInfo_var);
		Hashtable_t348 * L_20 = iTween_Hash_m2629(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		iTween_RotateTo_m2476(NULL /*static, unused*/, L_3, L_20, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_008d:
	{
		String_t* L_21 = (__this->___door_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m2981(NULL /*static, unused*/, L_21, _stringLiteral408, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00b0;
		}
	}
	{
		Rigidbody_t325 * L_23 = Component_GetComponent_TisRigidbody_t325_m3113(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t325_m3113_MethodInfo_var);
		V_0 = L_23;
		Rigidbody_t325 * L_24 = V_0;
		NullCheck(L_24);
		Rigidbody_set_isKinematic_m3116(L_24, 0, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// FadeTexture
#include "AssemblyU2DCSharp_FadeTexture.h"
#ifndef _MSC_VER
#else
#endif
// FadeTexture
#include "AssemblyU2DCSharp_FadeTextureMethodDeclarations.h"

// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"


// System.Void FadeTexture::.ctor()
extern "C" void FadeTexture__ctor_m1881 (FadeTexture_t313 * __this, const MethodInfo* method)
{
	{
		__this->___val_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FadeTexture::Start()
extern const MethodInfo* Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral409;
extern "C" void FadeTexture_Start_m1882 (FadeTexture_t313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		_stringLiteral409 = il2cpp_codegen_string_literal_from_index(409);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t312 * L_0 = Component_GetComponent_TisRenderer_t312_m2759(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var);
		__this->___mat_2 = L_0;
		Renderer_t312 * L_1 = (__this->___mat_2);
		NullCheck(L_1);
		Material_t2 * L_2 = Renderer_get_material_m2762(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_SetFloat_m2724(L_2, _stringLiteral409, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void FadeTexture::FadeMat()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern TypeInfo* EaseType_t425_il2cpp_TypeInfo_var;
extern TypeInfo* LoopType_t426_il2cpp_TypeInfo_var;
extern TypeInfo* iTween_t432_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral410;
extern Il2CppCodeGenString* _stringLiteral411;
extern Il2CppCodeGenString* _stringLiteral400;
extern Il2CppCodeGenString* _stringLiteral401;
extern Il2CppCodeGenString* _stringLiteral402;
extern Il2CppCodeGenString* _stringLiteral412;
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral414;
extern "C" void FadeTexture_FadeMat_m1883 (FadeTexture_t313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		EaseType_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(266);
		LoopType_t426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(271);
		iTween_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(267);
		_stringLiteral410 = il2cpp_codegen_string_literal_from_index(410);
		_stringLiteral411 = il2cpp_codegen_string_literal_from_index(411);
		_stringLiteral400 = il2cpp_codegen_string_literal_from_index(400);
		_stringLiteral401 = il2cpp_codegen_string_literal_from_index(401);
		_stringLiteral402 = il2cpp_codegen_string_literal_from_index(402);
		_stringLiteral412 = il2cpp_codegen_string_literal_from_index(412);
		_stringLiteral413 = il2cpp_codegen_string_literal_from_index(413);
		_stringLiteral414 = il2cpp_codegen_string_literal_from_index(414);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t470* L_1 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, ((int32_t)14)));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral410);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)_stringLiteral410;
		ObjectU5BU5D_t470* L_2 = L_1;
		float L_3 = (1.0f);
		Object_t * L_4 = Box(Single_t531_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_4;
		ObjectU5BU5D_t470* L_5 = L_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral411);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 2)) = (Object_t *)_stringLiteral411;
		ObjectU5BU5D_t470* L_6 = L_5;
		int32_t L_7 = 0;
		Object_t * L_8 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 3)) = (Object_t *)L_8;
		ObjectU5BU5D_t470* L_9 = L_6;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, _stringLiteral400);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 4)) = (Object_t *)_stringLiteral400;
		ObjectU5BU5D_t470* L_10 = L_9;
		float L_11 = (0.8f);
		Object_t * L_12 = Box(Single_t531_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5)) = (Object_t *)L_12;
		ObjectU5BU5D_t470* L_13 = L_10;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 6);
		ArrayElementTypeCheck (L_13, _stringLiteral401);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 6)) = (Object_t *)_stringLiteral401;
		ObjectU5BU5D_t470* L_14 = L_13;
		float L_15 = (5.0f);
		Object_t * L_16 = Box(Single_t531_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		ArrayElementTypeCheck (L_14, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 7)) = (Object_t *)L_16;
		ObjectU5BU5D_t470* L_17 = L_14;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 8);
		ArrayElementTypeCheck (L_17, _stringLiteral402);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 8)) = (Object_t *)_stringLiteral402;
		ObjectU5BU5D_t470* L_18 = L_17;
		int32_t L_19 = ((int32_t)21);
		Object_t * L_20 = Box(EaseType_t425_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		ArrayElementTypeCheck (L_18, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, ((int32_t)9))) = (Object_t *)L_20;
		ObjectU5BU5D_t470* L_21 = L_18;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)10));
		ArrayElementTypeCheck (L_21, _stringLiteral412);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, ((int32_t)10))) = (Object_t *)_stringLiteral412;
		ObjectU5BU5D_t470* L_22 = L_21;
		int32_t L_23 = 0;
		Object_t * L_24 = Box(LoopType_t426_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)11));
		ArrayElementTypeCheck (L_22, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, ((int32_t)11))) = (Object_t *)L_24;
		ObjectU5BU5D_t470* L_25 = L_22;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)12));
		ArrayElementTypeCheck (L_25, _stringLiteral413);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_25, ((int32_t)12))) = (Object_t *)_stringLiteral413;
		ObjectU5BU5D_t470* L_26 = L_25;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)13));
		ArrayElementTypeCheck (L_26, _stringLiteral414);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_26, ((int32_t)13))) = (Object_t *)_stringLiteral414;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t432_il2cpp_TypeInfo_var);
		Hashtable_t348 * L_27 = iTween_Hash_m2629(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		iTween_ValueTo_m2440(NULL /*static, unused*/, L_0, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FadeTexture::FadeText(System.Single)
extern "C" void FadeTexture_FadeText_m1884 (FadeTexture_t313 * __this, float ___newValue, const MethodInfo* method)
{
	{
		float L_0 = ___newValue;
		__this->___val_3 = L_0;
		return;
	}
}
// System.Void FadeTexture::Update()
extern Il2CppCodeGenString* _stringLiteral409;
extern "C" void FadeTexture_Update_m1885 (FadeTexture_t313 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral409 = il2cpp_codegen_string_literal_from_index(409);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t312 * L_0 = (__this->___mat_2);
		NullCheck(L_0);
		Material_t2 * L_1 = Renderer_get_material_m2762(L_0, /*hidden argument*/NULL);
		float L_2 = (__this->___val_3);
		NullCheck(L_1);
		Material_SetFloat_m2724(L_1, _stringLiteral409, L_2, /*hidden argument*/NULL);
		return;
	}
}
// GetFPS
#include "AssemblyU2DCSharp_GetFPS.h"
#ifndef _MSC_VER
#else
#endif
// GetFPS
#include "AssemblyU2DCSharp_GetFPSMethodDeclarations.h"

// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"


// System.Void GetFPS::.ctor()
extern "C" void GetFPS__ctor_m1886 (GetFPS_t314 * __this, const MethodInfo* method)
{
	{
		__this->___size_3 = 4;
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GetFPS::Update()
extern "C" void GetFPS_Update_m1887 (GetFPS_t314 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___deltaTime_2);
		float L_1 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = (__this->___deltaTime_2);
		__this->___deltaTime_2 = ((float)((float)L_0+(float)((float)((float)((float)((float)L_1-(float)L_2))*(float)(0.1f)))));
		return;
	}
}
// System.Void GetFPS::OnGUI()
extern TypeInfo* GUIStyle_t273_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t483_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral415;
extern Il2CppCodeGenString* _stringLiteral416;
extern Il2CppCodeGenString* _stringLiteral417;
extern Il2CppCodeGenString* _stringLiteral418;
extern "C" void GetFPS_OnGUI_m1888 (GetFPS_t314 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUIStyle_t273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(235);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		GUI_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(169);
		_stringLiteral415 = il2cpp_codegen_string_literal_from_index(415);
		_stringLiteral416 = il2cpp_codegen_string_literal_from_index(416);
		_stringLiteral417 = il2cpp_codegen_string_literal_from_index(417);
		_stringLiteral418 = il2cpp_codegen_string_literal_from_index(418);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GUIStyle_t273 * V_2 = {0};
	Rect_t225  V_3 = {0};
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	String_t* V_6 = {0};
	{
		int32_t L_0 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		GUIStyle_t273 * L_2 = (GUIStyle_t273 *)il2cpp_codegen_object_new (GUIStyle_t273_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3062(L_2, /*hidden argument*/NULL);
		V_2 = L_2;
		int32_t L_3 = V_0;
		int32_t L_4 = V_1;
		Rect__ctor_m2802((&V_3), (0.0f), (0.0f), (((float)L_3)), (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4*(int32_t)2))/(int32_t)((int32_t)100))))), /*hidden argument*/NULL);
		GUIStyle_t273 * L_5 = V_2;
		NullCheck(L_5);
		GUIStyle_set_alignment_m3067(L_5, 0, /*hidden argument*/NULL);
		GUIStyle_t273 * L_6 = V_2;
		int32_t L_7 = V_1;
		int32_t L_8 = (__this->___size_3);
		NullCheck(L_6);
		GUIStyle_set_fontSize_m3065(L_6, ((int32_t)((int32_t)((int32_t)((int32_t)L_7*(int32_t)L_8))/(int32_t)((int32_t)100))), /*hidden argument*/NULL);
		GUIStyle_t273 * L_9 = V_2;
		NullCheck(L_9);
		GUIStyleState_t523 * L_10 = GUIStyle_get_normal_m3063(L_9, /*hidden argument*/NULL);
		Color_t6  L_11 = {0};
		Color__ctor_m2713(&L_11, (0.13f), (0.81f), (0.78f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyleState_set_textColor_m3064(L_10, L_11, /*hidden argument*/NULL);
		float L_12 = (__this->___deltaTime_2);
		V_4 = ((float)((float)L_12*(float)(1000.0f)));
		float L_13 = (__this->___deltaTime_2);
		V_5 = ((float)((float)(1.0f)/(float)L_13));
		String_t* L_14 = Single_ToString_m3117((&V_4), _stringLiteral415, /*hidden argument*/NULL);
		String_t* L_15 = Single_ToString_m3117((&V_5), _stringLiteral417, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m3118(NULL /*static, unused*/, L_14, _stringLiteral416, L_15, _stringLiteral418, /*hidden argument*/NULL);
		V_6 = L_16;
		Rect_t225  L_17 = V_3;
		String_t* L_18 = V_6;
		GUIStyle_t273 * L_19 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t483_il2cpp_TypeInfo_var);
		GUI_Label_m3069(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// GoTo/<WaitAndEnable>c__Iterator6
#include "AssemblyU2DCSharp_GoTo_U3CWaitAndEnableU3Ec__Iterator6.h"
#ifndef _MSC_VER
#else
#endif
// GoTo/<WaitAndEnable>c__Iterator6
#include "AssemblyU2DCSharp_GoTo_U3CWaitAndEnableU3Ec__Iterator6MethodDeclarations.h"

// GoTo
#include "AssemblyU2DCSharp_GoTo.h"
// UnityEngine.Light
#include "UnityEngine_UnityEngine_Light.h"


// System.Void GoTo/<WaitAndEnable>c__Iterator6::.ctor()
extern "C" void U3CWaitAndEnableU3Ec__Iterator6__ctor_m1889 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GoTo/<WaitAndEnable>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndEnableU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1890 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object GoTo/<WaitAndEnable>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndEnableU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1891 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean GoTo/<WaitAndEnable>c__Iterator6::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern "C" bool U3CWaitAndEnableU3Ec__Iterator6_MoveNext_m1892 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0071;
	}

IL_0021:
	{
		float L_2 = (__this->___time_0);
		WaitForSeconds_t475 * L_3 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_3 = L_3;
		__this->___U24PC_2 = 1;
		goto IL_0073;
	}

IL_003e:
	{
		GoTo_t315 * L_4 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_4);
		Collider_t319 * L_5 = (L_4->___col_6);
		bool L_6 = (__this->___val_1);
		NullCheck(L_5);
		Collider_set_enabled_m3119(L_5, L_6, /*hidden argument*/NULL);
		GoTo_t315 * L_7 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_7);
		Light_t318 * L_8 = (L_7->___light_5);
		bool L_9 = (__this->___val_1);
		NullCheck(L_8);
		Behaviour_set_enabled_m2721(L_8, L_9, /*hidden argument*/NULL);
		__this->___U24PC_2 = (-1);
	}

IL_0071:
	{
		return 0;
	}

IL_0073:
	{
		return 1;
	}
	// Dead block : IL_0075: ldloc.1
}
// System.Void GoTo/<WaitAndEnable>c__Iterator6::Dispose()
extern "C" void U3CWaitAndEnableU3Ec__Iterator6_Dispose_m1893 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void GoTo/<WaitAndEnable>c__Iterator6::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CWaitAndEnableU3Ec__Iterator6_Reset_m1894 (U3CWaitAndEnableU3Ec__Iterator6_t316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// GoTo
#include "AssemblyU2DCSharp_GoToMethodDeclarations.h"

// UnityEngine.Light
#include "UnityEngine_UnityEngine_LightMethodDeclarations.h"
struct GameObject_t256;
struct GoTo_t315;
struct GameObject_t256;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m3121_gshared (GameObject_t256 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m3121(__this, method) (( Object_t * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<GoTo>()
// !!0 UnityEngine.GameObject::GetComponent<GoTo>()
#define GameObject_GetComponent_TisGoTo_t315_m3120(__this, method) (( GoTo_t315 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)
struct Component_t477;
struct Light_t318;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Light>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Light>()
#define Component_GetComponent_TisLight_t318_m3122(__this, method) (( Light_t318 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void GoTo::.ctor()
extern "C" void GoTo__ctor_m1895 (GoTo_t315 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoTo::.cctor()
extern "C" void GoTo__cctor_m1896 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// GoTo GoTo::get_Instance()
extern TypeInfo* GoTo_t315_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisGoTo_t315_m3120_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral419;
extern "C" GoTo_t315 * GoTo_get_Instance_m1897 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GoTo_t315_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(272);
		GameObject_GetComponent_TisGoTo_t315_m3120_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483733);
		_stringLiteral419 = il2cpp_codegen_string_literal_from_index(419);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GoTo_t315_il2cpp_TypeInfo_var);
		GoTo_t315 * L_0 = ((GoTo_t315_StaticFields*)GoTo_t315_il2cpp_TypeInfo_var->static_fields)->____instance_2;
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t256 * L_2 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral419, /*hidden argument*/NULL);
		NullCheck(L_2);
		GoTo_t315 * L_3 = GameObject_GetComponent_TisGoTo_t315_m3120(L_2, /*hidden argument*/GameObject_GetComponent_TisGoTo_t315_m3120_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GoTo_t315_il2cpp_TypeInfo_var);
		((GoTo_t315_StaticFields*)GoTo_t315_il2cpp_TypeInfo_var->static_fields)->____instance_2 = L_3;
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GoTo_t315_il2cpp_TypeInfo_var);
		GoTo_t315 * L_4 = ((GoTo_t315_StaticFields*)GoTo_t315_il2cpp_TypeInfo_var->static_fields)->____instance_2;
		return L_4;
	}
}
// System.Void GoTo::Start()
extern TypeInfo* Vector3U5BU5D_t317_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* EaseType_t425_il2cpp_TypeInfo_var;
extern TypeInfo* LoopType_t426_il2cpp_TypeInfo_var;
extern TypeInfo* iTween_t432_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisLight_t318_m3122_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral410;
extern Il2CppCodeGenString* _stringLiteral411;
extern Il2CppCodeGenString* _stringLiteral400;
extern Il2CppCodeGenString* _stringLiteral401;
extern Il2CppCodeGenString* _stringLiteral402;
extern Il2CppCodeGenString* _stringLiteral412;
extern Il2CppCodeGenString* _stringLiteral413;
extern Il2CppCodeGenString* _stringLiteral420;
extern "C" void GoTo_Start_m1898 (GoTo_t315 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t317_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(190);
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		EaseType_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(266);
		LoopType_t426_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(271);
		iTween_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(267);
		Component_GetComponent_TisLight_t318_m3122_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483734);
		Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		_stringLiteral410 = il2cpp_codegen_string_literal_from_index(410);
		_stringLiteral411 = il2cpp_codegen_string_literal_from_index(411);
		_stringLiteral400 = il2cpp_codegen_string_literal_from_index(400);
		_stringLiteral401 = il2cpp_codegen_string_literal_from_index(401);
		_stringLiteral402 = il2cpp_codegen_string_literal_from_index(402);
		_stringLiteral412 = il2cpp_codegen_string_literal_from_index(412);
		_stringLiteral413 = il2cpp_codegen_string_literal_from_index(413);
		_stringLiteral420 = il2cpp_codegen_string_literal_from_index(420);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___count_7 = 0;
		Vector3U5BU5D_t317* L_0 = ((Vector3U5BU5D_t317*)SZArrayNew(Vector3U5BU5D_t317_il2cpp_TypeInfo_var, ((int32_t)25)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Vector3_t215  L_1 = {0};
		Vector3__ctor_m2812(&L_1, (1.54f), (1.0f), (6.49f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_0, 0)) = L_1;
		Vector3U5BU5D_t317* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Vector3_t215  L_3 = {0};
		Vector3__ctor_m2812(&L_3, (-0.37f), (1.0f), (11.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_2, 1)) = L_3;
		Vector3U5BU5D_t317* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Vector3_t215  L_5 = {0};
		Vector3__ctor_m2812(&L_5, (-1.2f), (1.0f), (28.71f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_4, 2)) = L_5;
		Vector3U5BU5D_t317* L_6 = L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		Vector3_t215  L_7 = {0};
		Vector3__ctor_m2812(&L_7, (-10.11f), (1.0f), (27.74f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_6, 3)) = L_7;
		Vector3U5BU5D_t317* L_8 = L_6;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 4);
		Vector3_t215  L_9 = {0};
		Vector3__ctor_m2812(&L_9, (-7.68f), (1.0f), (25.57f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_8, 4)) = L_9;
		Vector3U5BU5D_t317* L_10 = L_8;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		Vector3_t215  L_11 = {0};
		Vector3__ctor_m2812(&L_11, (-4.07f), (1.0f), (28.81f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_10, 5)) = L_11;
		Vector3U5BU5D_t317* L_12 = L_10;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 6);
		Vector3_t215  L_13 = {0};
		Vector3__ctor_m2812(&L_13, (9.24f), (1.0f), (28.81f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_12, 6)) = L_13;
		Vector3U5BU5D_t317* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 7);
		Vector3_t215  L_15 = {0};
		Vector3__ctor_m2812(&L_15, (14.0f), (1.0f), (28.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_14, 7)) = L_15;
		Vector3U5BU5D_t317* L_16 = L_14;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 8);
		Vector3_t215  L_17 = {0};
		Vector3__ctor_m2812(&L_17, (24.31f), (1.0f), (23.51f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_16, 8)) = L_17;
		Vector3U5BU5D_t317* L_18 = L_16;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)9));
		Vector3_t215  L_19 = {0};
		Vector3__ctor_m2812(&L_19, (29.72f), (1.0f), (15.44f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_18, ((int32_t)9))) = L_19;
		Vector3U5BU5D_t317* L_20 = L_18;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, ((int32_t)10));
		Vector3_t215  L_21 = {0};
		Vector3__ctor_m2812(&L_21, (22.7f), (1.0f), (13.34f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_20, ((int32_t)10))) = L_21;
		Vector3U5BU5D_t317* L_22 = L_20;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)11));
		Vector3_t215  L_23 = {0};
		Vector3__ctor_m2812(&L_23, (25.65f), (1.0f), (7.42f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_22, ((int32_t)11))) = L_23;
		Vector3U5BU5D_t317* L_24 = L_22;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)12));
		Vector3_t215  L_25 = {0};
		Vector3__ctor_m2812(&L_25, (22.03f), (1.0f), (9.68f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_24, ((int32_t)12))) = L_25;
		Vector3U5BU5D_t317* L_26 = L_24;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, ((int32_t)13));
		Vector3_t215  L_27 = {0};
		Vector3__ctor_m2812(&L_27, (26.03f), (1.0f), (16.19f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_26, ((int32_t)13))) = L_27;
		Vector3U5BU5D_t317* L_28 = L_26;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)14));
		Vector3_t215  L_29 = {0};
		Vector3__ctor_m2812(&L_29, (32.04f), (1.0f), (37.37f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_28, ((int32_t)14))) = L_29;
		Vector3U5BU5D_t317* L_30 = L_28;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)15));
		Vector3_t215  L_31 = {0};
		Vector3__ctor_m2812(&L_31, (32.04f), (1.0f), (55.13f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_30, ((int32_t)15))) = L_31;
		Vector3U5BU5D_t317* L_32 = L_30;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, ((int32_t)16));
		Vector3_t215  L_33 = {0};
		Vector3__ctor_m2812(&L_33, (48.08f), (1.0f), (55.13f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_32, ((int32_t)16))) = L_33;
		Vector3U5BU5D_t317* L_34 = L_32;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, ((int32_t)17));
		Vector3_t215  L_35 = {0};
		Vector3__ctor_m2812(&L_35, (53.02f), (1.0f), (47.81f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_34, ((int32_t)17))) = L_35;
		Vector3U5BU5D_t317* L_36 = L_34;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)18));
		Vector3_t215  L_37 = {0};
		Vector3__ctor_m2812(&L_37, (58.0f), (1.0f), (54.66f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_36, ((int32_t)18))) = L_37;
		Vector3U5BU5D_t317* L_38 = L_36;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)19));
		Vector3_t215  L_39 = {0};
		Vector3__ctor_m2812(&L_39, (53.0f), (1.0f), (48.26f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_38, ((int32_t)19))) = L_39;
		Vector3U5BU5D_t317* L_40 = L_38;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)20));
		Vector3_t215  L_41 = {0};
		Vector3__ctor_m2812(&L_41, (53.0f), (1.0f), (48.26f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_40, ((int32_t)20))) = L_41;
		Vector3U5BU5D_t317* L_42 = L_40;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)21));
		Vector3_t215  L_43 = {0};
		Vector3__ctor_m2812(&L_43, (62.0f), (1.0f), (48.28f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_42, ((int32_t)21))) = L_43;
		Vector3U5BU5D_t317* L_44 = L_42;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)22));
		Vector3_t215  L_45 = {0};
		Vector3__ctor_m2812(&L_45, (76.18f), (1.0f), (48.41f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_44, ((int32_t)22))) = L_45;
		Vector3U5BU5D_t317* L_46 = L_44;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, ((int32_t)23));
		Vector3_t215  L_47 = {0};
		Vector3__ctor_m2812(&L_47, (81.79f), (1.0f), (48.69f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_46, ((int32_t)23))) = L_47;
		Vector3U5BU5D_t317* L_48 = L_46;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)24));
		Vector3_t215  L_49 = {0};
		Vector3__ctor_m2812(&L_49, (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		*((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_48, ((int32_t)24))) = L_49;
		__this->___pos_3 = L_48;
		Vector3U5BU5D_t317* L_50 = (__this->___pos_3);
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 0);
		__this->___nextPosition_4 = (*(Vector3_t215 *)((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_50, 0)));
		Light_t318 * L_51 = Component_GetComponent_TisLight_t318_m3122(__this, /*hidden argument*/Component_GetComponent_TisLight_t318_m3122_MethodInfo_var);
		__this->___light_5 = L_51;
		Collider_t319 * L_52 = Component_GetComponent_TisCollider_t319_m2792(__this, /*hidden argument*/Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var);
		__this->___col_6 = L_52;
		GameObject_t256 * L_53 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t470* L_54 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, ((int32_t)14)));
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 0);
		ArrayElementTypeCheck (L_54, _stringLiteral410);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_54, 0)) = (Object_t *)_stringLiteral410;
		ObjectU5BU5D_t470* L_55 = L_54;
		float L_56 = (0.0f);
		Object_t * L_57 = Box(Single_t531_il2cpp_TypeInfo_var, &L_56);
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 1);
		ArrayElementTypeCheck (L_55, L_57);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, 1)) = (Object_t *)L_57;
		ObjectU5BU5D_t470* L_58 = L_55;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 2);
		ArrayElementTypeCheck (L_58, _stringLiteral411);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_58, 2)) = (Object_t *)_stringLiteral411;
		ObjectU5BU5D_t470* L_59 = L_58;
		float L_60 = (8.0f);
		Object_t * L_61 = Box(Single_t531_il2cpp_TypeInfo_var, &L_60);
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, 3);
		ArrayElementTypeCheck (L_59, L_61);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_59, 3)) = (Object_t *)L_61;
		ObjectU5BU5D_t470* L_62 = L_59;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 4);
		ArrayElementTypeCheck (L_62, _stringLiteral400);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_62, 4)) = (Object_t *)_stringLiteral400;
		ObjectU5BU5D_t470* L_63 = L_62;
		float L_64 = (3.0f);
		Object_t * L_65 = Box(Single_t531_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, 5);
		ArrayElementTypeCheck (L_63, L_65);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_63, 5)) = (Object_t *)L_65;
		ObjectU5BU5D_t470* L_66 = L_63;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 6);
		ArrayElementTypeCheck (L_66, _stringLiteral401);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_66, 6)) = (Object_t *)_stringLiteral401;
		ObjectU5BU5D_t470* L_67 = L_66;
		float L_68 = (0.2f);
		Object_t * L_69 = Box(Single_t531_il2cpp_TypeInfo_var, &L_68);
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, 7);
		ArrayElementTypeCheck (L_67, L_69);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_67, 7)) = (Object_t *)L_69;
		ObjectU5BU5D_t470* L_70 = L_67;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 8);
		ArrayElementTypeCheck (L_70, _stringLiteral402);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_70, 8)) = (Object_t *)_stringLiteral402;
		ObjectU5BU5D_t470* L_71 = L_70;
		int32_t L_72 = ((int32_t)21);
		Object_t * L_73 = Box(EaseType_t425_il2cpp_TypeInfo_var, &L_72);
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, ((int32_t)9));
		ArrayElementTypeCheck (L_71, L_73);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_71, ((int32_t)9))) = (Object_t *)L_73;
		ObjectU5BU5D_t470* L_74 = L_71;
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, ((int32_t)10));
		ArrayElementTypeCheck (L_74, _stringLiteral412);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_74, ((int32_t)10))) = (Object_t *)_stringLiteral412;
		ObjectU5BU5D_t470* L_75 = L_74;
		int32_t L_76 = 2;
		Object_t * L_77 = Box(LoopType_t426_il2cpp_TypeInfo_var, &L_76);
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, ((int32_t)11));
		ArrayElementTypeCheck (L_75, L_77);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_75, ((int32_t)11))) = (Object_t *)L_77;
		ObjectU5BU5D_t470* L_78 = L_75;
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, ((int32_t)12));
		ArrayElementTypeCheck (L_78, _stringLiteral413);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_78, ((int32_t)12))) = (Object_t *)_stringLiteral413;
		ObjectU5BU5D_t470* L_79 = L_78;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, ((int32_t)13));
		ArrayElementTypeCheck (L_79, _stringLiteral420);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_79, ((int32_t)13))) = (Object_t *)_stringLiteral420;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t432_il2cpp_TypeInfo_var);
		Hashtable_t348 * L_80 = iTween_Hash_m2629(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		iTween_ValueTo_m2440(NULL /*static, unused*/, L_53, L_80, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoTo::GotoPos()
extern "C" void GoTo_GotoPos_m1899 (GoTo_t315 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___count_7);
		__this->___count_7 = ((int32_t)((int32_t)L_0+(int32_t)1));
		Transform_t243 * L_1 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		Vector3U5BU5D_t317* L_2 = (__this->___pos_3);
		int32_t L_3 = (__this->___count_7);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		NullCheck(L_1);
		Transform_set_position_m2902(L_1, (*(Vector3_t215 *)((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_2, L_3))), /*hidden argument*/NULL);
		GoTo_GoHere_m1900(__this, (0.0f), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoTo::GoHere(System.Single,System.Boolean)
extern "C" void GoTo_GoHere_m1900 (GoTo_t315 * __this, float ___time, bool ___val, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t317* L_0 = (__this->___pos_3);
		int32_t L_1 = (__this->___count_7);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		__this->___nextPosition_4 = (*(Vector3_t215 *)((Vector3_t215 *)(Vector3_t215 *)SZArrayLdElema(L_0, L_1)));
		float L_2 = ___time;
		bool L_3 = ___val;
		Object_t * L_4 = GoTo_WaitAndEnable_m1901(__this, L_2, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GoTo::WaitAndEnable(System.Single,System.Boolean)
extern TypeInfo* U3CWaitAndEnableU3Ec__Iterator6_t316_il2cpp_TypeInfo_var;
extern "C" Object_t * GoTo_WaitAndEnable_m1901 (GoTo_t315 * __this, float ___time, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitAndEnableU3Ec__Iterator6_t316_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(274);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitAndEnableU3Ec__Iterator6_t316 * V_0 = {0};
	{
		U3CWaitAndEnableU3Ec__Iterator6_t316 * L_0 = (U3CWaitAndEnableU3Ec__Iterator6_t316 *)il2cpp_codegen_object_new (U3CWaitAndEnableU3Ec__Iterator6_t316_il2cpp_TypeInfo_var);
		U3CWaitAndEnableU3Ec__Iterator6__ctor_m1889(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitAndEnableU3Ec__Iterator6_t316 * L_1 = V_0;
		float L_2 = ___time;
		NullCheck(L_1);
		L_1->___time_0 = L_2;
		U3CWaitAndEnableU3Ec__Iterator6_t316 * L_3 = V_0;
		bool L_4 = ___val;
		NullCheck(L_3);
		L_3->___val_1 = L_4;
		U3CWaitAndEnableU3Ec__Iterator6_t316 * L_5 = V_0;
		float L_6 = ___time;
		NullCheck(L_5);
		L_5->___U3CU24U3Etime_4 = L_6;
		U3CWaitAndEnableU3Ec__Iterator6_t316 * L_7 = V_0;
		bool L_8 = ___val;
		NullCheck(L_7);
		L_7->___U3CU24U3Eval_5 = L_8;
		U3CWaitAndEnableU3Ec__Iterator6_t316 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CWaitAndEnableU3Ec__Iterator6_t316 * L_10 = V_0;
		return L_10;
	}
}
// System.Void GoTo::myValue(System.Single)
extern "C" void GoTo_myValue_m1902 (GoTo_t315 * __this, float ___v, const MethodInfo* method)
{
	{
		Light_t318 * L_0 = (__this->___light_5);
		float L_1 = ___v;
		NullCheck(L_0);
		Light_set_intensity_m3124(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// HeadRotation
#include "AssemblyU2DCSharp_HeadRotation.h"
#ifndef _MSC_VER
#else
#endif
// HeadRotation
#include "AssemblyU2DCSharp_HeadRotationMethodDeclarations.h"



// System.Void HeadRotation::.ctor()
extern "C" void HeadRotation__ctor_m1903 (HeadRotation_t320 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HeadRotation::Start()
extern "C" void HeadRotation_Start_m1904 (HeadRotation_t320 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HeadRotation::Update()
extern "C" void HeadRotation_Update_m1905 (HeadRotation_t320 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// IKHandler
#include "AssemblyU2DCSharp_IKHandler.h"
#ifndef _MSC_VER
#else
#endif
// IKHandler
#include "AssemblyU2DCSharp_IKHandlerMethodDeclarations.h"

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.AvatarIKGoal
#include "UnityEngine_UnityEngine_AvatarIKGoal.h"
// UnityEngine.AvatarIKHint
#include "UnityEngine_UnityEngine_AvatarIKHint.h"
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
struct Component_t477;
struct Animator_t321;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
#define Component_GetComponent_TisAnimator_t321_m3125(__this, method) (( Animator_t321 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void IKHandler::.ctor()
extern "C" void IKHandler__ctor_m1906 (IKHandler_t322 * __this, const MethodInfo* method)
{
	{
		__this->___IKWeight_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void IKHandler::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t321_m3125_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral421;
extern Il2CppCodeGenString* _stringLiteral422;
extern Il2CppCodeGenString* _stringLiteral423;
extern Il2CppCodeGenString* _stringLiteral424;
extern "C" void IKHandler_Start_m1907 (IKHandler_t322 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAnimator_t321_m3125_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483735);
		_stringLiteral421 = il2cpp_codegen_string_literal_from_index(421);
		_stringLiteral422 = il2cpp_codegen_string_literal_from_index(422);
		_stringLiteral423 = il2cpp_codegen_string_literal_from_index(423);
		_stringLiteral424 = il2cpp_codegen_string_literal_from_index(424);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = Component_GetComponent_TisAnimator_t321_m3125(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t321_m3125_MethodInfo_var);
		__this->___anim_7 = L_0;
		GameObject_t256 * L_1 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral421, /*hidden argument*/NULL);
		__this->___Left_Arm_IK_2 = L_1;
		GameObject_t256 * L_2 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral422, /*hidden argument*/NULL);
		__this->___Right_Arm_IK_3 = L_2;
		GameObject_t256 * L_3 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral423, /*hidden argument*/NULL);
		__this->___Head_IK_4 = L_3;
		GameObject_t256 * L_4 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral424, /*hidden argument*/NULL);
		__this->___player_5 = L_4;
		return;
	}
}
// System.Void IKHandler::Update()
extern "C" void IKHandler_Update_m1908 (IKHandler_t322 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void IKHandler::OnAnimatorIK()
extern "C" void IKHandler_OnAnimatorIK_m1909 (IKHandler_t322 * __this, const MethodInfo* method)
{
	{
		Animator_t321 * L_0 = (__this->___anim_7);
		float L_1 = (__this->___IKWeight_6);
		NullCheck(L_0);
		Animator_SetIKPositionWeight_m3126(L_0, 2, L_1, /*hidden argument*/NULL);
		Animator_t321 * L_2 = (__this->___anim_7);
		float L_3 = (__this->___IKWeight_6);
		NullCheck(L_2);
		Animator_SetIKPositionWeight_m3126(L_2, 3, L_3, /*hidden argument*/NULL);
		Animator_t321 * L_4 = (__this->___anim_7);
		GameObject_t256 * L_5 = (__this->___Left_Arm_IK_2);
		NullCheck(L_5);
		Transform_t243 * L_6 = GameObject_get_transform_m2825(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t215  L_7 = Transform_get_position_m2894(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animator_SetIKPosition_m3127(L_4, 2, L_7, /*hidden argument*/NULL);
		Animator_t321 * L_8 = (__this->___anim_7);
		GameObject_t256 * L_9 = (__this->___Right_Arm_IK_3);
		NullCheck(L_9);
		Transform_t243 * L_10 = GameObject_get_transform_m2825(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t215  L_11 = Transform_get_position_m2894(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		Animator_SetIKPosition_m3127(L_8, 3, L_11, /*hidden argument*/NULL);
		Animator_t321 * L_12 = (__this->___anim_7);
		float L_13 = (__this->___IKWeight_6);
		NullCheck(L_12);
		Animator_SetIKHintPositionWeight_m3128(L_12, 2, L_13, /*hidden argument*/NULL);
		Animator_t321 * L_14 = (__this->___anim_7);
		float L_15 = (__this->___IKWeight_6);
		NullCheck(L_14);
		Animator_SetIKHintPositionWeight_m3128(L_14, 3, L_15, /*hidden argument*/NULL);
		Animator_t321 * L_16 = (__this->___anim_7);
		GameObject_t256 * L_17 = (__this->___player_5);
		NullCheck(L_17);
		Transform_t243 * L_18 = GameObject_get_transform_m2825(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t215  L_19 = Transform_get_position_m2894(L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		Animator_SetLookAtPosition_m3129(L_16, L_19, /*hidden argument*/NULL);
		return;
	}
}
// InteractiveBaby
#include "AssemblyU2DCSharp_InteractiveBaby.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveBaby
#include "AssemblyU2DCSharp_InteractiveBabyMethodDeclarations.h"

// UnityEngine.SkinnedMeshRenderer
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer.h"
// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgentMethodDeclarations.h"
struct GameObject_t256;
struct NavMeshAgent_t307;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.NavMeshAgent>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.NavMeshAgent>()
#define GameObject_GetComponent_TisNavMeshAgent_t307_m3130(__this, method) (( NavMeshAgent_t307 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)
struct GameObject_t256;
struct Animator_t321;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
#define GameObject_GetComponent_TisAnimator_t321_m3131(__this, method) (( Animator_t321 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)
struct GameObject_t256;
struct AICharacterControl_t308;
// Declaration !!0 UnityEngine.GameObject::GetComponent<AICharacterControl>()
// !!0 UnityEngine.GameObject::GetComponent<AICharacterControl>()
#define GameObject_GetComponent_TisAICharacterControl_t308_m3132(__this, method) (( AICharacterControl_t308 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)
struct GameObject_t256;
struct SkinnedMeshRenderer_t532;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SkinnedMeshRenderer>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SkinnedMeshRenderer>()
#define GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133(__this, method) (( SkinnedMeshRenderer_t532 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)
struct GameObject_t256;
struct Collider_t319;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
#define GameObject_GetComponent_TisCollider_t319_m3134(__this, method) (( Collider_t319 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)


// System.Void InteractiveBaby::.ctor()
extern "C" void InteractiveBaby__ctor_m1910 (InteractiveBaby_t324 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBaby::Start()
extern const MethodInfo* GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral425;
extern "C" void InteractiveBaby_Start_m1911 (InteractiveBaby_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483736);
		GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483737);
		GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483738);
		_stringLiteral425 = il2cpp_codegen_string_literal_from_index(425);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		NavMeshAgent_t307 * L_1 = GameObject_GetComponent_TisNavMeshAgent_t307_m3130(L_0, /*hidden argument*/GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var);
		__this->___navMeshAgent_4 = L_1;
		GameObject_t256 * L_2 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Animator_t321 * L_3 = GameObject_GetComponent_TisAnimator_t321_m3131(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var);
		__this->___anim_5 = L_3;
		GameObject_t256 * L_4 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		AICharacterControl_t308 * L_5 = GameObject_GetComponent_TisAICharacterControl_t308_m3132(L_4, /*hidden argument*/GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var);
		__this->___aiControl_7 = L_5;
		GameObjectU5BU5D_t323* L_6 = GameObject_FindGameObjectsWithTag_m3135(NULL /*static, unused*/, _stringLiteral425, /*hidden argument*/NULL);
		__this->___baby_skin_8 = L_6;
		return;
	}
}
// System.Void InteractiveBaby::Update()
extern Il2CppCodeGenString* _stringLiteral426;
extern Il2CppCodeGenString* _stringLiteral427;
extern "C" void InteractiveBaby_Update_m1912 (InteractiveBaby_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral426 = il2cpp_codegen_string_literal_from_index(426);
		_stringLiteral427 = il2cpp_codegen_string_literal_from_index(427);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t215  V_0 = {0};
	Vector3_t215  V_1 = {0};
	{
		GameObject_t256 * L_0 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral426, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t243 * L_1 = GameObject_get_transform_m2825(L_0, /*hidden argument*/NULL);
		__this->___origPos_3 = L_1;
		bool L_2 = (__this->___goToPos_6);
		if (!L_2)
		{
			goto IL_008a;
		}
	}
	{
		NavMeshAgent_t307 * L_3 = (__this->___navMeshAgent_4);
		Transform_t243 * L_4 = (__this->___origPos_3);
		NullCheck(L_4);
		Vector3_t215  L_5 = Transform_get_position_m2894(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		NavMeshAgent_set_destination_m3136(L_3, L_5, /*hidden argument*/NULL);
		GameObject_t256 * L_6 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t243 * L_7 = GameObject_get_transform_m2825(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t215  L_8 = Transform_get_position_m2894(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = ((&V_0)->___x_1);
		Transform_t243 * L_10 = (__this->___origPos_3);
		NullCheck(L_10);
		Vector3_t215  L_11 = Transform_get_position_m2894(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = ((&V_1)->___x_1);
		if ((!(((float)((float)((float)L_9-(float)L_12))) == ((float)(0.0f)))))
		{
			goto IL_008a;
		}
	}
	{
		InteractiveBaby_EnableBaby_m1913(__this, 0, /*hidden argument*/NULL);
		Animator_t321 * L_13 = (__this->___anim_5);
		NullCheck(L_13);
		Animator_SetTrigger_m3137(L_13, _stringLiteral427, /*hidden argument*/NULL);
		__this->___goToPos_6 = 0;
	}

IL_008a:
	{
		return;
	}
}
// System.Void InteractiveBaby::EnableBaby(System.Boolean)
extern "C" void InteractiveBaby_EnableBaby_m1913 (InteractiveBaby_t324 * __this, bool ___val, const MethodInfo* method)
{
	{
		NavMeshAgent_t307 * L_0 = (__this->___navMeshAgent_4);
		bool L_1 = ___val;
		NullCheck(L_0);
		Behaviour_set_enabled_m2721(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBaby::HideBaby(System.Boolean)
extern const MethodInfo* GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var;
extern "C" void InteractiveBaby_HideBaby_m1914 (InteractiveBaby_t324 * __this, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483739);
		GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483740);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Collider_t319 * V_1 = {0};
	{
		V_0 = 0;
		goto IL_001e;
	}

IL_0007:
	{
		GameObjectU5BU5D_t323* L_0 = (__this->___baby_skin_8);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(GameObject_t256 **)(GameObject_t256 **)SZArrayLdElema(L_0, L_2)));
		SkinnedMeshRenderer_t532 * L_3 = GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133((*(GameObject_t256 **)(GameObject_t256 **)SZArrayLdElema(L_0, L_2)), /*hidden argument*/GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133_MethodInfo_var);
		bool L_4 = ___val;
		NullCheck(L_3);
		Renderer_set_enabled_m2811(L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_6 = V_0;
		GameObjectU5BU5D_t323* L_7 = (__this->___baby_skin_8);
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		GameObject_t256 * L_8 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Collider_t319 * L_9 = GameObject_GetComponent_TisCollider_t319_m3134(L_8, /*hidden argument*/GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var);
		V_1 = L_9;
		Collider_t319 * L_10 = V_1;
		bool L_11 = ___val;
		NullCheck(L_10);
		Collider_set_enabled_m3119(L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBaby::BabyWalk()
extern Il2CppCodeGenString* _stringLiteral428;
extern "C" void InteractiveBaby_BabyWalk_m1915 (InteractiveBaby_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral428 = il2cpp_codegen_string_literal_from_index(428);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = (__this->___anim_5);
		NullCheck(L_0);
		Animator_SetTrigger_m3137(L_0, _stringLiteral428, /*hidden argument*/NULL);
		InteractiveBaby_EnableBaby_m1913(__this, 1, /*hidden argument*/NULL);
		__this->___goToPos_6 = 1;
		return;
	}
}
// System.Void InteractiveBaby::BabyIdle()
extern Il2CppCodeGenString* _stringLiteral429;
extern "C" void InteractiveBaby_BabyIdle_m1916 (InteractiveBaby_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral429 = il2cpp_codegen_string_literal_from_index(429);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m2814(NULL /*static, unused*/, _stringLiteral429, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBaby::BabyPos(UnityEngine.Vector3)
extern "C" void InteractiveBaby_BabyPos_m1917 (InteractiveBaby_t324 * __this, Vector3_t215  ___val, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t243 * L_1 = GameObject_get_transform_m2825(L_0, /*hidden argument*/NULL);
		Vector3_t215  L_2 = ___val;
		NullCheck(L_1);
		Transform_set_position_m2902(L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBaby::BabyScream()
extern Il2CppCodeGenString* _stringLiteral430;
extern "C" void InteractiveBaby_BabyScream_m1918 (InteractiveBaby_t324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral430 = il2cpp_codegen_string_literal_from_index(430);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = (__this->___anim_5);
		NullCheck(L_0);
		Animator_SetTrigger_m3137(L_0, _stringLiteral430, /*hidden argument*/NULL);
		return;
	}
}
// InteractiveBurnMama
#include "AssemblyU2DCSharp_InteractiveBurnMama.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveBurnMama
#include "AssemblyU2DCSharp_InteractiveBurnMamaMethodDeclarations.h"

struct Component_t477;
struct RigidbodyU5BU5D_t533;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Rigidbody>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Rigidbody>()
#define Component_GetComponentsInChildren_TisRigidbody_t325_m3138(__this, method) (( RigidbodyU5BU5D_t533* (*) (Component_t477 *, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m2791_gshared)(__this, method)


// System.Void InteractiveBurnMama::.ctor()
extern "C" void InteractiveBurnMama__ctor_m1919 (InteractiveBurnMama_t326 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBurnMama::Start()
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var;
extern "C" void InteractiveBurnMama_Start_m1920 (InteractiveBurnMama_t326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483737);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Animator_t321 * L_1 = GameObject_GetComponent_TisAnimator_t321_m3131(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var);
		__this->___anim_6 = L_1;
		InteractiveBurnMama_SetKinematic_m1925(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBurnMama::Update()
extern "C" void InteractiveBurnMama_Update_m1921 (InteractiveBurnMama_t326 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void InteractiveBurnMama::lookAtMeMom()
extern Il2CppCodeGenString* _stringLiteral431;
extern Il2CppCodeGenString* _stringLiteral432;
extern "C" void InteractiveBurnMama_lookAtMeMom_m1922 (InteractiveBurnMama_t326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral431 = il2cpp_codegen_string_literal_from_index(431);
		_stringLiteral432 = il2cpp_codegen_string_literal_from_index(432);
		s_Il2CppMethodIntialized = true;
	}
	{
		InteractiveBurnMama_SetKinematic_m1925(__this, 1, /*hidden argument*/NULL);
		Animator_t321 * L_0 = (__this->___anim_6);
		NullCheck(L_0);
		Behaviour_set_enabled_m2721(L_0, 1, /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m3073(__this, _stringLiteral431, (4.0f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m3073(__this, _stringLiteral432, (8.5f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBurnMama::waitAndTurn()
extern TypeInfo* InteractiveEventManager_t362_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral433;
extern "C" void InteractiveBurnMama_waitAndTurn_m1923 (InteractiveBurnMama_t326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InteractiveEventManager_t362_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(270);
		_stringLiteral433 = il2cpp_codegen_string_literal_from_index(433);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = (__this->___anim_6);
		NullCheck(L_0);
		Animator_SetTrigger_m3137(L_0, _stringLiteral433, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(InteractiveEventManager_t362_il2cpp_TypeInfo_var);
		InteractiveEventManager_t362 * L_1 = InteractiveEventManager_get_Instance_m2102(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		InteractiveEventManager_dispatchEvent_m2103(L_1, ((int32_t)34), /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBurnMama::waitAndRagdoll()
extern "C" void InteractiveBurnMama_waitAndRagdoll_m1924 (InteractiveBurnMama_t326 * __this, const MethodInfo* method)
{
	{
		Animator_t321 * L_0 = (__this->___anim_6);
		NullCheck(L_0);
		Behaviour_set_enabled_m2721(L_0, 0, /*hidden argument*/NULL);
		InteractiveBurnMama_SetKinematic_m1925(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveBurnMama::SetKinematic(System.Boolean)
extern const MethodInfo* Component_GetComponentsInChildren_TisRigidbody_t325_m3138_MethodInfo_var;
extern "C" void InteractiveBurnMama_SetKinematic_m1925 (InteractiveBurnMama_t326 * __this, bool ___newValue, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisRigidbody_t325_m3138_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483741);
		s_Il2CppMethodIntialized = true;
	}
	RigidbodyU5BU5D_t533* V_0 = {0};
	Rigidbody_t325 * V_1 = {0};
	RigidbodyU5BU5D_t533* V_2 = {0};
	int32_t V_3 = 0;
	{
		RigidbodyU5BU5D_t533* L_0 = Component_GetComponentsInChildren_TisRigidbody_t325_m3138(__this, /*hidden argument*/Component_GetComponentsInChildren_TisRigidbody_t325_m3138_MethodInfo_var);
		V_0 = L_0;
		RigidbodyU5BU5D_t533* L_1 = V_0;
		V_2 = L_1;
		V_3 = 0;
		goto IL_001f;
	}

IL_0010:
	{
		RigidbodyU5BU5D_t533* L_2 = V_2;
		int32_t L_3 = V_3;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_1 = (*(Rigidbody_t325 **)(Rigidbody_t325 **)SZArrayLdElema(L_2, L_4));
		Rigidbody_t325 * L_5 = V_1;
		bool L_6 = ___newValue;
		NullCheck(L_5);
		Rigidbody_set_isKinematic_m3116(L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_3;
		V_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_8 = V_3;
		RigidbodyU5BU5D_t533* L_9 = V_2;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// InteractiveDroppingObject
#include "AssemblyU2DCSharp_InteractiveDroppingObject.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveDroppingObject
#include "AssemblyU2DCSharp_InteractiveDroppingObjectMethodDeclarations.h"

// InteractiveObject
#include "AssemblyU2DCSharp_InteractiveObjectMethodDeclarations.h"
struct GameObject_t256;
struct Rigidbody_t325;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t325_m3139(__this, method) (( Rigidbody_t325 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)


// System.Void InteractiveDroppingObject::.ctor()
extern "C" void InteractiveDroppingObject__ctor_m1926 (InteractiveDroppingObject_t327 * __this, const MethodInfo* method)
{
	{
		InteractiveObject__ctor_m1967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveDroppingObject::Start()
extern const MethodInfo* GameObject_GetComponent_TisRigidbody_t325_m3139_MethodInfo_var;
extern "C" void InteractiveDroppingObject_Start_m1927 (InteractiveDroppingObject_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisRigidbody_t325_m3139_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483742);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody_t325 * L_1 = GameObject_GetComponent_TisRigidbody_t325_m3139(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t325_m3139_MethodInfo_var);
		__this->___rigBod_4 = L_1;
		return;
	}
}
// System.Void InteractiveDroppingObject::fallObject()
extern const MethodInfo* Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var;
extern "C" void InteractiveDroppingObject_fallObject_m1928 (InteractiveDroppingObject_t327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t312 * L_0 = Component_GetComponent_TisRenderer_t312_m2759(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t312_m2759_MethodInfo_var);
		NullCheck(L_0);
		Renderer_set_enabled_m2811(L_0, 1, /*hidden argument*/NULL);
		Rigidbody_t325 * L_1 = (__this->___rigBod_4);
		NullCheck(L_1);
		Rigidbody_set_isKinematic_m3116(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// InteractiveFlashlight
#include "AssemblyU2DCSharp_InteractiveFlashlight.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveFlashlight
#include "AssemblyU2DCSharp_InteractiveFlashlightMethodDeclarations.h"

// InteractiveObject
#include "AssemblyU2DCSharp_InteractiveObject.h"
struct GameObject_t256;
struct Light_t318;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Light>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Light>()
#define GameObject_GetComponent_TisLight_t318_m3140(__this, method) (( Light_t318 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)


// System.Void InteractiveFlashlight::.ctor()
extern "C" void InteractiveFlashlight__ctor_m1929 (InteractiveFlashlight_t329 * __this, const MethodInfo* method)
{
	{
		InteractiveObject__ctor_m1967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveFlashlight::Start()
extern "C" void InteractiveFlashlight_Start_m1930 (InteractiveFlashlight_t329 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(4 /* System.Void InteractiveFlashlight::SetGazedAt(System.Boolean) */, __this, 1);
		return;
	}
}
// System.Void InteractiveFlashlight::SetGazedAt(System.Boolean)
extern "C" void InteractiveFlashlight_SetGazedAt_m1931 (InteractiveFlashlight_t329 * __this, bool ___gazeAt, const MethodInfo* method)
{
	{
		bool L_0 = ___gazeAt;
		InteractiveObject_SetGazedAt_m1969(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveFlashlight::LightsOn()
extern const MethodInfo* GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral434;
extern "C" void InteractiveFlashlight_LightsOn_m1932 (InteractiveFlashlight_t329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483743);
		_stringLiteral434 = il2cpp_codegen_string_literal_from_index(434);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral434, /*hidden argument*/NULL);
		__this->___mySpotlight_4 = L_0;
		GameObject_t256 * L_1 = (__this->___mySpotlight_4);
		NullCheck(L_1);
		Light_t318 * L_2 = GameObject_GetComponent_TisLight_t318_m3140(L_1, /*hidden argument*/GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var);
		__this->___lights_5 = L_2;
		__this->___isLightOn_6 = 1;
		Light_t318 * L_3 = (__this->___lights_5);
		NullCheck(L_3);
		Light_set_range_m3141(L_3, (75.0f), /*hidden argument*/NULL);
		Light_t318 * L_4 = (__this->___lights_5);
		NullCheck(L_4);
		Light_set_intensity_m3124(L_4, (8.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveFlashlight::LightsOff()
extern const MethodInfo* GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral434;
extern "C" void InteractiveFlashlight_LightsOff_m1933 (InteractiveFlashlight_t329 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483743);
		_stringLiteral434 = il2cpp_codegen_string_literal_from_index(434);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral434, /*hidden argument*/NULL);
		__this->___mySpotlight_4 = L_0;
		GameObject_t256 * L_1 = (__this->___mySpotlight_4);
		NullCheck(L_1);
		Light_t318 * L_2 = GameObject_GetComponent_TisLight_t318_m3140(L_1, /*hidden argument*/GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var);
		__this->___lights_5 = L_2;
		Light_t318 * L_3 = (__this->___lights_5);
		NullCheck(L_3);
		Light_set_intensity_m3124(L_3, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveFlashlight::GetFlashlight()
extern "C" void InteractiveFlashlight_GetFlashlight_m1934 (InteractiveFlashlight_t329 * __this, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_SetActive_m2960(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// InteractiveMama/<WaitAndEnableMama>c__Iterator7
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndEnableMamaU3Ec__.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveMama/<WaitAndEnableMama>c__Iterator7
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndEnableMamaU3Ec__MethodDeclarations.h"

// InteractiveMama
#include "AssemblyU2DCSharp_InteractiveMama.h"


// System.Void InteractiveMama/<WaitAndEnableMama>c__Iterator7::.ctor()
extern "C" void U3CWaitAndEnableMamaU3Ec__Iterator7__ctor_m1935 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object InteractiveMama/<WaitAndEnableMama>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndEnableMamaU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1936 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object InteractiveMama/<WaitAndEnableMama>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndEnableMamaU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m1937 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean InteractiveMama/<WaitAndEnableMama>c__Iterator7::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var;
extern "C" bool U3CWaitAndEnableMamaU3Ec__Iterator7_MoveNext_m1938 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483736);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_006c;
	}

IL_0021:
	{
		float L_2 = (__this->___delay_0);
		WaitForSeconds_t475 * L_3 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_4 = L_3;
		__this->___U24PC_3 = 1;
		goto IL_006e;
	}

IL_003e:
	{
		InteractiveMama_t330 * L_4 = (__this->___U3CU3Ef__this_7);
		NullCheck(L_4);
		GameObject_t256 * L_5 = Component_get_gameObject_m2778(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		NavMeshAgent_t307 * L_6 = GameObject_GetComponent_TisNavMeshAgent_t307_m3130(L_5, /*hidden argument*/GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var);
		__this->___U3CnavMeshAgentU3E__0_1 = L_6;
		NavMeshAgent_t307 * L_7 = (__this->___U3CnavMeshAgentU3E__0_1);
		bool L_8 = (__this->___val_2);
		NullCheck(L_7);
		Behaviour_set_enabled_m2721(L_7, L_8, /*hidden argument*/NULL);
		__this->___U24PC_3 = (-1);
	}

IL_006c:
	{
		return 0;
	}

IL_006e:
	{
		return 1;
	}
	// Dead block : IL_0070: ldloc.1
}
// System.Void InteractiveMama/<WaitAndEnableMama>c__Iterator7::Dispose()
extern "C" void U3CWaitAndEnableMamaU3Ec__Iterator7_Dispose_m1939 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void InteractiveMama/<WaitAndEnableMama>c__Iterator7::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CWaitAndEnableMamaU3Ec__Iterator7_Reset_m1940 (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// InteractiveMama/<WaitAndMamaPos>c__Iterator8
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndMamaPosU3Ec__Ite.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveMama/<WaitAndMamaPos>c__Iterator8
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndMamaPosU3Ec__IteMethodDeclarations.h"



// System.Void InteractiveMama/<WaitAndMamaPos>c__Iterator8::.ctor()
extern "C" void U3CWaitAndMamaPosU3Ec__Iterator8__ctor_m1941 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object InteractiveMama/<WaitAndMamaPos>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndMamaPosU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1942 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object InteractiveMama/<WaitAndMamaPos>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndMamaPosU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m1943 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean InteractiveMama/<WaitAndMamaPos>c__Iterator8::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern "C" bool U3CWaitAndMamaPosU3Ec__Iterator8_MoveNext_m1944 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0060;
	}

IL_0021:
	{
		float L_2 = (__this->___delay_0);
		WaitForSeconds_t475 * L_3 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_3 = L_3;
		__this->___U24PC_2 = 1;
		goto IL_0062;
	}

IL_003e:
	{
		InteractiveMama_t330 * L_4 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_4);
		GameObject_t256 * L_5 = Component_get_gameObject_m2778(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t243 * L_6 = GameObject_get_transform_m2825(L_5, /*hidden argument*/NULL);
		Vector3_t215  L_7 = (__this->___val_1);
		NullCheck(L_6);
		Transform_set_position_m2902(L_6, L_7, /*hidden argument*/NULL);
		__this->___U24PC_2 = (-1);
	}

IL_0060:
	{
		return 0;
	}

IL_0062:
	{
		return 1;
	}
	// Dead block : IL_0064: ldloc.1
}
// System.Void InteractiveMama/<WaitAndMamaPos>c__Iterator8::Dispose()
extern "C" void U3CWaitAndMamaPosU3Ec__Iterator8_Dispose_m1945 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void InteractiveMama/<WaitAndMamaPos>c__Iterator8::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CWaitAndMamaPosU3Ec__Iterator8_Reset_m1946 (U3CWaitAndMamaPosU3Ec__Iterator8_t332 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// InteractiveMama/<WaitAndHideMama>c__Iterator9
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndHideMamaU3Ec__It.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveMama/<WaitAndHideMama>c__Iterator9
#include "AssemblyU2DCSharp_InteractiveMama_U3CWaitAndHideMamaU3Ec__ItMethodDeclarations.h"



// System.Void InteractiveMama/<WaitAndHideMama>c__Iterator9::.ctor()
extern "C" void U3CWaitAndHideMamaU3Ec__Iterator9__ctor_m1947 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object InteractiveMama/<WaitAndHideMama>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndHideMamaU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1948 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Object InteractiveMama/<WaitAndHideMama>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndHideMamaU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m1949 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Boolean InteractiveMama/<WaitAndHideMama>c__Iterator9::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var;
extern "C" bool U3CWaitAndHideMamaU3Ec__Iterator9_MoveNext_m1950 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483739);
		GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483740);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_4);
		V_0 = L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_00c0;
	}

IL_0021:
	{
		float L_2 = (__this->___delay_0);
		WaitForSeconds_t475 * L_3 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_5 = L_3;
		__this->___U24PC_4 = 1;
		goto IL_00c2;
	}

IL_003e:
	{
		__this->___U3CiU3E__0_1 = 0;
		goto IL_007a;
	}

IL_004a:
	{
		InteractiveMama_t330 * L_4 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_4);
		GameObjectU5BU5D_t323* L_5 = (L_4->___mama_skin_7);
		int32_t L_6 = (__this->___U3CiU3E__0_1);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(GameObject_t256 **)(GameObject_t256 **)SZArrayLdElema(L_5, L_7)));
		SkinnedMeshRenderer_t532 * L_8 = GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133((*(GameObject_t256 **)(GameObject_t256 **)SZArrayLdElema(L_5, L_7)), /*hidden argument*/GameObject_GetComponent_TisSkinnedMeshRenderer_t532_m3133_MethodInfo_var);
		bool L_9 = (__this->___val_2);
		NullCheck(L_8);
		Renderer_set_enabled_m2811(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_10 = (__this->___U3CiU3E__0_1);
		__this->___U3CiU3E__0_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_11 = (__this->___U3CiU3E__0_1);
		InteractiveMama_t330 * L_12 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_12);
		GameObjectU5BU5D_t323* L_13 = (L_12->___mama_skin_7);
		NullCheck(L_13);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_13)->max_length))))))
		{
			goto IL_004a;
		}
	}
	{
		InteractiveMama_t330 * L_14 = (__this->___U3CU3Ef__this_8);
		NullCheck(L_14);
		GameObject_t256 * L_15 = Component_get_gameObject_m2778(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Collider_t319 * L_16 = GameObject_GetComponent_TisCollider_t319_m3134(L_15, /*hidden argument*/GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var);
		__this->___U3CcolU3E__1_3 = L_16;
		Collider_t319 * L_17 = (__this->___U3CcolU3E__1_3);
		bool L_18 = (__this->___val_2);
		NullCheck(L_17);
		Collider_set_enabled_m3119(L_17, L_18, /*hidden argument*/NULL);
		__this->___U24PC_4 = (-1);
	}

IL_00c0:
	{
		return 0;
	}

IL_00c2:
	{
		return 1;
	}
	// Dead block : IL_00c4: ldloc.1
}
// System.Void InteractiveMama/<WaitAndHideMama>c__Iterator9::Dispose()
extern "C" void U3CWaitAndHideMamaU3Ec__Iterator9_Dispose_m1951 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_4 = (-1);
		return;
	}
}
// System.Void InteractiveMama/<WaitAndHideMama>c__Iterator9::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CWaitAndHideMamaU3Ec__Iterator9_Reset_m1952 (U3CWaitAndHideMamaU3Ec__Iterator9_t333 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// InteractiveMama
#include "AssemblyU2DCSharp_InteractiveMamaMethodDeclarations.h"



// System.Void InteractiveMama::.ctor()
extern "C" void InteractiveMama__ctor_m1953 (InteractiveMama_t330 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveMama::Start()
extern const MethodInfo* GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral435;
extern Il2CppCodeGenString* _stringLiteral436;
extern "C" void InteractiveMama_Start_m1954 (InteractiveMama_t330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483736);
		GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483737);
		GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483738);
		_stringLiteral435 = il2cpp_codegen_string_literal_from_index(435);
		_stringLiteral436 = il2cpp_codegen_string_literal_from_index(436);
		s_Il2CppMethodIntialized = true;
	}
	AICharacterControl_t308 * V_0 = {0};
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t243 * L_1 = GameObject_get_transform_m2825(L_0, /*hidden argument*/NULL);
		__this->___myTransform_10 = L_1;
		GameObject_t256 * L_2 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		NavMeshAgent_t307 * L_3 = GameObject_GetComponent_TisNavMeshAgent_t307_m3130(L_2, /*hidden argument*/GameObject_GetComponent_TisNavMeshAgent_t307_m3130_MethodInfo_var);
		__this->___navMeshAgent_3 = L_3;
		GameObject_t256 * L_4 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Animator_t321 * L_5 = GameObject_GetComponent_TisAnimator_t321_m3131(L_4, /*hidden argument*/GameObject_GetComponent_TisAnimator_t321_m3131_MethodInfo_var);
		__this->___anim_4 = L_5;
		GameObject_t256 * L_6 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		AICharacterControl_t308 * L_7 = GameObject_GetComponent_TisAICharacterControl_t308_m3132(L_6, /*hidden argument*/GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var);
		V_0 = L_7;
		GameObjectU5BU5D_t323* L_8 = GameObject_FindGameObjectsWithTag_m3135(NULL /*static, unused*/, _stringLiteral435, /*hidden argument*/NULL);
		__this->___mama_skin_7 = L_8;
		GameObject_t256 * L_9 = GameObject_Find_m3123(NULL /*static, unused*/, _stringLiteral436, /*hidden argument*/NULL);
		__this->___head_8 = L_9;
		return;
	}
}
// System.Void InteractiveMama::Update()
extern TypeInfo* InteractiveMama_t330_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral427;
extern "C" void InteractiveMama_Update_m1955 (InteractiveMama_t330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InteractiveMama_t330_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(278);
		_stringLiteral427 = il2cpp_codegen_string_literal_from_index(427);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = ((InteractiveMama_t330_StaticFields*)InteractiveMama_t330_il2cpp_TypeInfo_var->static_fields)->___targetPos_2;
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0060;
		}
	}
	{
		bool L_2 = (__this->___goToPos_5);
		if (!L_2)
		{
			goto IL_0060;
		}
	}
	{
		GameObject_t256 * L_3 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t243 * L_4 = GameObject_get_transform_m2825(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t215  L_5 = Transform_get_position_m2894(L_4, /*hidden argument*/NULL);
		GameObject_t256 * L_6 = ((InteractiveMama_t330_StaticFields*)InteractiveMama_t330_il2cpp_TypeInfo_var->static_fields)->___targetPos_2;
		NullCheck(L_6);
		Transform_t243 * L_7 = GameObject_get_transform_m2825(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t215  L_8 = Transform_get_position_m2894(L_7, /*hidden argument*/NULL);
		float L_9 = Vector3_Distance_m3142(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		if ((!(((float)L_9) <= ((float)(1.2f)))))
		{
			goto IL_0060;
		}
	}
	{
		Animator_t321 * L_10 = (__this->___anim_4);
		NullCheck(L_10);
		Animator_SetTrigger_m3137(L_10, _stringLiteral427, /*hidden argument*/NULL);
		__this->___goToPos_5 = 0;
	}

IL_0060:
	{
		return;
	}
}
// System.Void InteractiveMama::MamaHeadTurn(System.Single,System.Single,System.Single)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t531_il2cpp_TypeInfo_var;
extern TypeInfo* EaseType_t425_il2cpp_TypeInfo_var;
extern TypeInfo* iTween_t432_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral343;
extern Il2CppCodeGenString* _stringLiteral400;
extern Il2CppCodeGenString* _stringLiteral401;
extern Il2CppCodeGenString* _stringLiteral437;
extern "C" void InteractiveMama_MamaHeadTurn_m1956 (InteractiveMama_t330 * __this, float ___time, float ___delay, float ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Single_t531_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		EaseType_t425_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(266);
		iTween_t432_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(267);
		_stringLiteral343 = il2cpp_codegen_string_literal_from_index(343);
		_stringLiteral400 = il2cpp_codegen_string_literal_from_index(400);
		_stringLiteral401 = il2cpp_codegen_string_literal_from_index(401);
		_stringLiteral437 = il2cpp_codegen_string_literal_from_index(437);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = (__this->___head_8);
		ObjectU5BU5D_t470* L_1 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 8));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, _stringLiteral343);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)_stringLiteral343;
		ObjectU5BU5D_t470* L_2 = L_1;
		float L_3 = ___val;
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t531_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t470* L_6 = L_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral400);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)_stringLiteral400;
		ObjectU5BU5D_t470* L_7 = L_6;
		float L_8 = ___time;
		float L_9 = L_8;
		Object_t * L_10 = Box(Single_t531_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_10);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_10;
		ObjectU5BU5D_t470* L_11 = L_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 4);
		ArrayElementTypeCheck (L_11, _stringLiteral401);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_11, 4)) = (Object_t *)_stringLiteral401;
		ObjectU5BU5D_t470* L_12 = L_11;
		float L_13 = ___delay;
		float L_14 = L_13;
		Object_t * L_15 = Box(Single_t531_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 5);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 5)) = (Object_t *)L_15;
		ObjectU5BU5D_t470* L_16 = L_12;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 6);
		ArrayElementTypeCheck (L_16, _stringLiteral437);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_16, 6)) = (Object_t *)_stringLiteral437;
		ObjectU5BU5D_t470* L_17 = L_16;
		int32_t L_18 = 1;
		Object_t * L_19 = Box(EaseType_t425_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 7);
		ArrayElementTypeCheck (L_17, L_19);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 7)) = (Object_t *)L_19;
		IL2CPP_RUNTIME_CLASS_INIT(iTween_t432_il2cpp_TypeInfo_var);
		Hashtable_t348 * L_20 = iTween_Hash_m2629(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		iTween_RotateTo_m2476(NULL /*static, unused*/, L_0, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveMama::EnableMama(System.Single,System.Boolean)
extern "C" void InteractiveMama_EnableMama_m1957 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method)
{
	{
		float L_0 = ___delay;
		bool L_1 = ___val;
		Object_t * L_2 = InteractiveMama_WaitAndEnableMama_m1958(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator InteractiveMama::WaitAndEnableMama(System.Single,System.Boolean)
extern TypeInfo* U3CWaitAndEnableMamaU3Ec__Iterator7_t331_il2cpp_TypeInfo_var;
extern "C" Object_t * InteractiveMama_WaitAndEnableMama_m1958 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(279);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * V_0 = {0};
	{
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * L_0 = (U3CWaitAndEnableMamaU3Ec__Iterator7_t331 *)il2cpp_codegen_object_new (U3CWaitAndEnableMamaU3Ec__Iterator7_t331_il2cpp_TypeInfo_var);
		U3CWaitAndEnableMamaU3Ec__Iterator7__ctor_m1935(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * L_1 = V_0;
		float L_2 = ___delay;
		NullCheck(L_1);
		L_1->___delay_0 = L_2;
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * L_3 = V_0;
		bool L_4 = ___val;
		NullCheck(L_3);
		L_3->___val_2 = L_4;
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * L_5 = V_0;
		float L_6 = ___delay;
		NullCheck(L_5);
		L_5->___U3CU24U3Edelay_5 = L_6;
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * L_7 = V_0;
		bool L_8 = ___val;
		NullCheck(L_7);
		L_7->___U3CU24U3Eval_6 = L_8;
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_7 = __this;
		U3CWaitAndEnableMamaU3Ec__Iterator7_t331 * L_10 = V_0;
		return L_10;
	}
}
// System.Void InteractiveMama::MamaAction(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral430;
extern Il2CppCodeGenString* _stringLiteral427;
extern "C" void InteractiveMama_MamaAction_m1959 (InteractiveMama_t330 * __this, String_t* ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral430 = il2cpp_codegen_string_literal_from_index(430);
		_stringLiteral427 = il2cpp_codegen_string_literal_from_index(427);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Inequality_m2980(NULL /*static, unused*/, L_0, _stringLiteral430, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_2 = ___val;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m2980(NULL /*static, unused*/, L_2, _stringLiteral427, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}

IL_0020:
	{
		InteractiveMama_EnableMama_m1957(__this, (0.2f), 1, /*hidden argument*/NULL);
		__this->___goToPos_5 = 1;
	}

IL_0033:
	{
		Animator_t321 * L_4 = (__this->___anim_4);
		String_t* L_5 = ___val;
		NullCheck(L_4);
		Animator_SetTrigger_m3137(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveMama::MamaGoto(System.String)
extern TypeInfo* InteractiveMama_t330_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var;
extern "C" void InteractiveMama_MamaGoto_m1960 (InteractiveMama_t330 * __this, String_t* ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InteractiveMama_t330_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(278);
		GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483738);
		s_Il2CppMethodIntialized = true;
	}
	AICharacterControl_t308 * V_0 = {0};
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AICharacterControl_t308 * L_1 = GameObject_GetComponent_TisAICharacterControl_t308_m3132(L_0, /*hidden argument*/GameObject_GetComponent_TisAICharacterControl_t308_m3132_MethodInfo_var);
		V_0 = L_1;
		String_t* L_2 = ___target;
		GameObject_t256 * L_3 = GameObject_Find_m3123(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		((InteractiveMama_t330_StaticFields*)InteractiveMama_t330_il2cpp_TypeInfo_var->static_fields)->___targetPos_2 = L_3;
		NavMeshAgent_t307 * L_4 = (__this->___navMeshAgent_3);
		GameObject_t256 * L_5 = ((InteractiveMama_t330_StaticFields*)InteractiveMama_t330_il2cpp_TypeInfo_var->static_fields)->___targetPos_2;
		NullCheck(L_5);
		Transform_t243 * L_6 = GameObject_get_transform_m2825(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t215  L_7 = Transform_get_position_m2894(L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		NavMeshAgent_set_destination_m3136(L_4, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveMama::MamaPos(System.Single,UnityEngine.Vector3)
extern "C" void InteractiveMama_MamaPos_m1961 (InteractiveMama_t330 * __this, float ___delay, Vector3_t215  ___val, const MethodInfo* method)
{
	{
		float L_0 = ___delay;
		Vector3_t215  L_1 = ___val;
		Object_t * L_2 = InteractiveMama_WaitAndMamaPos_m1962(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator InteractiveMama::WaitAndMamaPos(System.Single,UnityEngine.Vector3)
extern TypeInfo* U3CWaitAndMamaPosU3Ec__Iterator8_t332_il2cpp_TypeInfo_var;
extern "C" Object_t * InteractiveMama_WaitAndMamaPos_m1962 (InteractiveMama_t330 * __this, float ___delay, Vector3_t215  ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitAndMamaPosU3Ec__Iterator8_t332_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(280);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitAndMamaPosU3Ec__Iterator8_t332 * V_0 = {0};
	{
		U3CWaitAndMamaPosU3Ec__Iterator8_t332 * L_0 = (U3CWaitAndMamaPosU3Ec__Iterator8_t332 *)il2cpp_codegen_object_new (U3CWaitAndMamaPosU3Ec__Iterator8_t332_il2cpp_TypeInfo_var);
		U3CWaitAndMamaPosU3Ec__Iterator8__ctor_m1941(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitAndMamaPosU3Ec__Iterator8_t332 * L_1 = V_0;
		float L_2 = ___delay;
		NullCheck(L_1);
		L_1->___delay_0 = L_2;
		U3CWaitAndMamaPosU3Ec__Iterator8_t332 * L_3 = V_0;
		Vector3_t215  L_4 = ___val;
		NullCheck(L_3);
		L_3->___val_1 = L_4;
		U3CWaitAndMamaPosU3Ec__Iterator8_t332 * L_5 = V_0;
		float L_6 = ___delay;
		NullCheck(L_5);
		L_5->___U3CU24U3Edelay_4 = L_6;
		U3CWaitAndMamaPosU3Ec__Iterator8_t332 * L_7 = V_0;
		Vector3_t215  L_8 = ___val;
		NullCheck(L_7);
		L_7->___U3CU24U3Eval_5 = L_8;
		U3CWaitAndMamaPosU3Ec__Iterator8_t332 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CWaitAndMamaPosU3Ec__Iterator8_t332 * L_10 = V_0;
		return L_10;
	}
}
// System.Void InteractiveMama::MamaScream()
extern Il2CppCodeGenString* _stringLiteral430;
extern "C" void InteractiveMama_MamaScream_m1963 (InteractiveMama_t330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral430 = il2cpp_codegen_string_literal_from_index(430);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = (__this->___anim_4);
		NullCheck(L_0);
		Animator_SetTrigger_m3137(L_0, _stringLiteral430, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveMama::MamaLookAt(UnityEngine.Transform)
extern "C" void InteractiveMama_MamaLookAt_m1964 (InteractiveMama_t330 * __this, Transform_t243 * ___val, const MethodInfo* method)
{
	Transform_t243 * V_0 = {0};
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t243 * L_1 = GameObject_get_transform_m2825(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t243 * L_2 = V_0;
		Transform_t243 * L_3 = ___val;
		NullCheck(L_2);
		Transform_LookAt_m3143(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveMama::HideMama(System.Single,System.Boolean)
extern "C" void InteractiveMama_HideMama_m1965 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method)
{
	{
		float L_0 = ___delay;
		bool L_1 = ___val;
		Object_t * L_2 = InteractiveMama_WaitAndHideMama_m1966(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator InteractiveMama::WaitAndHideMama(System.Single,System.Boolean)
extern TypeInfo* U3CWaitAndHideMamaU3Ec__Iterator9_t333_il2cpp_TypeInfo_var;
extern "C" Object_t * InteractiveMama_WaitAndHideMama_m1966 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitAndHideMamaU3Ec__Iterator9_t333_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(281);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitAndHideMamaU3Ec__Iterator9_t333 * V_0 = {0};
	{
		U3CWaitAndHideMamaU3Ec__Iterator9_t333 * L_0 = (U3CWaitAndHideMamaU3Ec__Iterator9_t333 *)il2cpp_codegen_object_new (U3CWaitAndHideMamaU3Ec__Iterator9_t333_il2cpp_TypeInfo_var);
		U3CWaitAndHideMamaU3Ec__Iterator9__ctor_m1947(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitAndHideMamaU3Ec__Iterator9_t333 * L_1 = V_0;
		float L_2 = ___delay;
		NullCheck(L_1);
		L_1->___delay_0 = L_2;
		U3CWaitAndHideMamaU3Ec__Iterator9_t333 * L_3 = V_0;
		bool L_4 = ___val;
		NullCheck(L_3);
		L_3->___val_2 = L_4;
		U3CWaitAndHideMamaU3Ec__Iterator9_t333 * L_5 = V_0;
		float L_6 = ___delay;
		NullCheck(L_5);
		L_5->___U3CU24U3Edelay_6 = L_6;
		U3CWaitAndHideMamaU3Ec__Iterator9_t333 * L_7 = V_0;
		bool L_8 = ___val;
		NullCheck(L_7);
		L_7->___U3CU24U3Eval_7 = L_8;
		U3CWaitAndHideMamaU3Ec__Iterator9_t333 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_8 = __this;
		U3CWaitAndHideMamaU3Ec__Iterator9_t333 * L_10 = V_0;
		return L_10;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void InteractiveObject::.ctor()
extern "C" void InteractiveObject__ctor_m1967 (InteractiveObject_t328 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveObject::Start()
extern "C" void InteractiveObject_Start_m1968 (InteractiveObject_t328 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker1< bool >::Invoke(4 /* System.Void InteractiveObject::SetGazedAt(System.Boolean) */, __this, 0);
		return;
	}
}
// System.Void InteractiveObject::SetGazedAt(System.Boolean)
extern "C" void InteractiveObject_SetGazedAt_m1969 (InteractiveObject_t328 * __this, bool ___gazedAt, const MethodInfo* method)
{
	{
		bool L_0 = ___gazedAt;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		__this->___lookedAt_3 = 1;
	}

IL_000d:
	{
		return;
	}
}
// System.Void InteractiveObject::Update()
extern "C" void InteractiveObject_Update_m1970 (InteractiveObject_t328 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// InteractiveParticles
#include "AssemblyU2DCSharp_InteractiveParticles.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveParticles
#include "AssemblyU2DCSharp_InteractiveParticlesMethodDeclarations.h"

// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystem.h"
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"
struct Component_t477;
struct ParticleSystem_t534;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
#define Component_GetComponent_TisParticleSystem_t534_m3144(__this, method) (( ParticleSystem_t534 * (*) (Component_t477 *, const MethodInfo*))Component_GetComponent_TisObject_t_m2755_gshared)(__this, method)


// System.Void InteractiveParticles::.ctor()
extern "C" void InteractiveParticles__ctor_m1971 (InteractiveParticles_t334 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveParticles::Start()
extern "C" void InteractiveParticles_Start_m1972 (InteractiveParticles_t334 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void InteractiveParticles::Update()
extern "C" void InteractiveParticles_Update_m1973 (InteractiveParticles_t334 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void InteractiveParticles::SetGazedAt(System.Boolean)
extern "C" void InteractiveParticles_SetGazedAt_m1974 (InteractiveParticles_t334 * __this, bool ___gazeAt, const MethodInfo* method)
{
	{
		bool L_0 = ___gazeAt;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		InteractiveParticles_Emitter_m1975(__this, 0, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Void InteractiveParticles::Emitter(System.Boolean)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InteractiveEventManager_t362_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisParticleSystem_t534_m3144_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral438;
extern "C" void InteractiveParticles_Emitter_m1975 (InteractiveParticles_t334 * __this, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		InteractiveEventManager_t362_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(270);
		Component_GetComponent_TisParticleSystem_t534_m3144_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483744);
		Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		_stringLiteral438 = il2cpp_codegen_string_literal_from_index(438);
		s_Il2CppMethodIntialized = true;
	}
	ParticleSystem_t534 * V_0 = {0};
	{
		ParticleSystem_t534 * L_0 = Component_GetComponent_TisParticleSystem_t534_m3144(__this, /*hidden argument*/Component_GetComponent_TisParticleSystem_t534_m3144_MethodInfo_var);
		V_0 = L_0;
		ParticleSystem_t534 * L_1 = V_0;
		bool L_2 = ___val;
		NullCheck(L_1);
		ParticleSystem_set_enableEmission_m3145(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t256 * L_3 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2979(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m2981(NULL /*static, unused*/, L_4, _stringLiteral438, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InteractiveEventManager_t362_il2cpp_TypeInfo_var);
		InteractiveEventManager_t362 * L_6 = InteractiveEventManager_get_Instance_m2102(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		InteractiveEventManager_dispatchEvent_m2103(L_6, ((int32_t)14), /*hidden argument*/NULL);
		Collider_t319 * L_7 = Component_GetComponent_TisCollider_t319_m2792(__this, /*hidden argument*/Component_GetComponent_TisCollider_t319_m2792_MethodInfo_var);
		NullCheck(L_7);
		Collider_set_enabled_m3119(L_7, 0, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// InteractivePlasticBag/<WaitAndWiggle>c__IteratorA
#include "AssemblyU2DCSharp_InteractivePlasticBag_U3CWaitAndWiggleU3Ec.h"
#ifndef _MSC_VER
#else
#endif
// InteractivePlasticBag/<WaitAndWiggle>c__IteratorA
#include "AssemblyU2DCSharp_InteractivePlasticBag_U3CWaitAndWiggleU3EcMethodDeclarations.h"

// InteractivePlasticBag
#include "AssemblyU2DCSharp_InteractivePlasticBag.h"
// UnityEngine.Cloth
#include "UnityEngine_UnityEngine_Cloth.h"
// UnityEngine.Cloth
#include "UnityEngine_UnityEngine_ClothMethodDeclarations.h"


// System.Void InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::.ctor()
extern "C" void U3CWaitAndWiggleU3Ec__IteratorA__ctor_m1976 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndWiggleU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1977 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndWiggleU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m1978 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern "C" bool U3CWaitAndWiggleU3Ec__IteratorA_MoveNext_m1979 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_00af;
	}

IL_0021:
	{
		float L_2 = (__this->___time_0);
		WaitForSeconds_t475 * L_3 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_2 = L_3;
		__this->___U24PC_1 = 1;
		goto IL_00b1;
	}

IL_003e:
	{
		InteractivePlasticBag_t335 * L_4 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_4);
		Cloth_t337 * L_5 = (L_4->___cloth_2);
		NullCheck(L_5);
		Cloth_set_useGravity_m3146(L_5, 1, /*hidden argument*/NULL);
		InteractivePlasticBag_t335 * L_6 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_6);
		Cloth_t337 * L_7 = (L_6->___cloth_2);
		Vector3_t215  L_8 = {0};
		Vector3__ctor_m2812(&L_8, (0.0f), (10.83f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Cloth_set_externalAcceleration_m3147(L_7, L_8, /*hidden argument*/NULL);
		InteractivePlasticBag_t335 * L_9 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_9);
		Cloth_t337 * L_10 = (L_9->___cloth_2);
		Vector3_t215  L_11 = {0};
		Vector3__ctor_m2812(&L_11, (0.0f), (3.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_10);
		Cloth_set_randomAcceleration_m3148(L_10, L_11, /*hidden argument*/NULL);
		InteractivePlasticBag_t335 * L_12 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_12);
		Collider_t319 * L_13 = (L_12->___col_3);
		NullCheck(L_13);
		Collider_set_enabled_m3119(L_13, 1, /*hidden argument*/NULL);
		__this->___U24PC_1 = (-1);
	}

IL_00af:
	{
		return 0;
	}

IL_00b1:
	{
		return 1;
	}
	// Dead block : IL_00b3: ldloc.1
}
// System.Void InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::Dispose()
extern "C" void U3CWaitAndWiggleU3Ec__IteratorA_Dispose_m1980 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void InteractivePlasticBag/<WaitAndWiggle>c__IteratorA::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CWaitAndWiggleU3Ec__IteratorA_Reset_m1981 (U3CWaitAndWiggleU3Ec__IteratorA_t336 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif
// InteractivePlasticBag
#include "AssemblyU2DCSharp_InteractivePlasticBagMethodDeclarations.h"

struct GameObject_t256;
struct Cloth_t337;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Cloth>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Cloth>()
#define GameObject_GetComponent_TisCloth_t337_m3149(__this, method) (( Cloth_t337 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)


// System.Void InteractivePlasticBag::.ctor()
extern "C" void InteractivePlasticBag__ctor_m1982 (InteractivePlasticBag_t335 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractivePlasticBag::Start()
extern const MethodInfo* GameObject_GetComponent_TisCloth_t337_m3149_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var;
extern "C" void InteractivePlasticBag_Start_m1983 (InteractivePlasticBag_t335 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisCloth_t337_m3149_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483745);
		GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483740);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Cloth_t337 * L_1 = GameObject_GetComponent_TisCloth_t337_m3149(L_0, /*hidden argument*/GameObject_GetComponent_TisCloth_t337_m3149_MethodInfo_var);
		__this->___cloth_2 = L_1;
		GameObject_t256 * L_2 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Collider_t319 * L_3 = GameObject_GetComponent_TisCollider_t319_m3134(L_2, /*hidden argument*/GameObject_GetComponent_TisCollider_t319_m3134_MethodInfo_var);
		__this->___col_3 = L_3;
		return;
	}
}
// System.Void InteractivePlasticBag::Update()
extern "C" void InteractivePlasticBag_Update_m1984 (InteractivePlasticBag_t335 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void InteractivePlasticBag::Wiggle()
extern "C" void InteractivePlasticBag_Wiggle_m1985 (InteractivePlasticBag_t335 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = InteractivePlasticBag_WaitAndWiggle_m1986(__this, (2.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator InteractivePlasticBag::WaitAndWiggle(System.Single)
extern TypeInfo* U3CWaitAndWiggleU3Ec__IteratorA_t336_il2cpp_TypeInfo_var;
extern "C" Object_t * InteractivePlasticBag_WaitAndWiggle_m1986 (InteractivePlasticBag_t335 * __this, float ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitAndWiggleU3Ec__IteratorA_t336_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(284);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitAndWiggleU3Ec__IteratorA_t336 * V_0 = {0};
	{
		U3CWaitAndWiggleU3Ec__IteratorA_t336 * L_0 = (U3CWaitAndWiggleU3Ec__IteratorA_t336 *)il2cpp_codegen_object_new (U3CWaitAndWiggleU3Ec__IteratorA_t336_il2cpp_TypeInfo_var);
		U3CWaitAndWiggleU3Ec__IteratorA__ctor_m1976(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitAndWiggleU3Ec__IteratorA_t336 * L_1 = V_0;
		float L_2 = ___time;
		NullCheck(L_1);
		L_1->___time_0 = L_2;
		U3CWaitAndWiggleU3Ec__IteratorA_t336 * L_3 = V_0;
		float L_4 = ___time;
		NullCheck(L_3);
		L_3->___U3CU24U3Etime_3 = L_4;
		U3CWaitAndWiggleU3Ec__IteratorA_t336 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_4 = __this;
		U3CWaitAndWiggleU3Ec__IteratorA_t336 * L_6 = V_0;
		return L_6;
	}
}
// System.Void InteractivePlasticBag::StopWiggle()
extern "C" void InteractivePlasticBag_StopWiggle_m1987 (InteractivePlasticBag_t335 * __this, const MethodInfo* method)
{
	{
		Cloth_t337 * L_0 = (__this->___cloth_2);
		NullCheck(L_0);
		Cloth_set_useGravity_m3146(L_0, 0, /*hidden argument*/NULL);
		Cloth_t337 * L_1 = (__this->___cloth_2);
		Vector3_t215  L_2 = {0};
		Vector3__ctor_m2812(&L_2, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Cloth_set_externalAcceleration_m3147(L_1, L_2, /*hidden argument*/NULL);
		Cloth_t337 * L_3 = (__this->___cloth_2);
		Vector3_t215  L_4 = {0};
		Vector3__ctor_m2812(&L_4, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Cloth_set_randomAcceleration_m3148(L_3, L_4, /*hidden argument*/NULL);
		Collider_t319 * L_5 = (__this->___col_3);
		NullCheck(L_5);
		Collider_set_enabled_m3119(L_5, 0, /*hidden argument*/NULL);
		return;
	}
}
// InteractiveRadio
#include "AssemblyU2DCSharp_InteractiveRadio.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveRadio
#include "AssemblyU2DCSharp_InteractiveRadioMethodDeclarations.h"

// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
struct GameObject_t256;
struct AudioSource_t339;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t339_m3150(__this, method) (( AudioSource_t339 * (*) (GameObject_t256 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m3121_gshared)(__this, method)


// System.Void InteractiveRadio::.ctor()
extern "C" void InteractiveRadio__ctor_m1988 (InteractiveRadio_t340 * __this, const MethodInfo* method)
{
	{
		InteractiveObject__ctor_m1967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveRadio::Awake()
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t339_m3150_MethodInfo_var;
extern "C" void InteractiveRadio_Awake_m1989 (InteractiveRadio_t340 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisAudioSource_t339_m3150_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483746);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t339 * L_1 = GameObject_GetComponent_TisAudioSource_t339_m3150(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t339_m3150_MethodInfo_var);
		__this->___radioSound_5 = L_1;
		return;
	}
}
// System.Void InteractiveRadio::Update()
extern "C" void InteractiveRadio_Update_m1990 (InteractiveRadio_t340 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void InteractiveRadio::SetGazedAt(System.Boolean)
extern const MethodInfo* Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var;
extern "C" void InteractiveRadio_SetGazedAt_m1991 (InteractiveRadio_t340 * __this, bool ___gazeAt, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483731);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t339 * G_B2_0 = {0};
	AudioSource_t339 * G_B1_0 = {0};
	float G_B3_0 = 0.0f;
	AudioSource_t339 * G_B3_1 = {0};
	{
		AudioSource_t339 * L_0 = Component_GetComponent_TisAudioSource_t339_m3112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var);
		bool L_1 = ___gazeAt;
		G_B1_0 = L_0;
		if (!L_1)
		{
			G_B2_0 = L_0;
			goto IL_0016;
		}
	}
	{
		G_B3_0 = (0.1f);
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0016:
	{
		G_B3_0 = (0.05f);
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		NullCheck(G_B3_1);
		AudioSource_set_volume_m3151(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		bool L_2 = ___gazeAt;
		InteractiveObject_SetGazedAt_m1969(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveRadio::changeClip(System.Int32)
extern const MethodInfo* Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var;
extern "C" void InteractiveRadio_changeClip_m1992 (InteractiveRadio_t340 * __this, int32_t ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483731);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t339 * L_0 = (__this->___radioSound_5);
		AudioClipU5BU5D_t338* L_1 = (__this->___radioClips_4);
		int32_t L_2 = ___val;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck(L_0);
		AudioSource_set_clip_m3152(L_0, (*(AudioClip_t472 **)(AudioClip_t472 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		AudioSource_t339 * L_4 = Component_GetComponent_TisAudioSource_t339_m3112(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t339_m3112_MethodInfo_var);
		NullCheck(L_4);
		AudioSource_set_volume_m3151(L_4, (0.8f), /*hidden argument*/NULL);
		AudioSource_t339 * L_5 = (__this->___radioSound_5);
		NullCheck(L_5);
		bool L_6 = AudioSource_get_isPlaying_m3114(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_003e;
		}
	}
	{
		AudioSource_t339 * L_7 = (__this->___radioSound_5);
		NullCheck(L_7);
		AudioSource_Play_m3115(L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// InteractiveShadowMan
#include "AssemblyU2DCSharp_InteractiveShadowMan.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveShadowMan
#include "AssemblyU2DCSharp_InteractiveShadowManMethodDeclarations.h"



// System.Void InteractiveShadowMan::.ctor()
extern "C" void InteractiveShadowMan__ctor_m1993 (InteractiveShadowMan_t341 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveShadowMan::Awake()
extern "C" void InteractiveShadowMan_Awake_m1994 (InteractiveShadowMan_t341 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void InteractiveShadowMan::SetGazedAt(System.Boolean)
extern "C" void InteractiveShadowMan_SetGazedAt_m1995 (InteractiveShadowMan_t341 * __this, bool ___gazeAt, const MethodInfo* method)
{
	{
		bool L_0 = ___gazeAt;
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = (__this->___lookCount_2);
		__this->___lookCount_2 = ((int32_t)((int32_t)L_1+(int32_t)1));
		int32_t L_2 = (__this->___lookCount_2);
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_0020;
		}
	}

IL_0020:
	{
		return;
	}
}
// System.Void InteractiveShadowMan::Update()
extern "C" void InteractiveShadowMan_Update_m1996 (InteractiveShadowMan_t341 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// InteractiveWheelChair
#include "AssemblyU2DCSharp_InteractiveWheelChair.h"
#ifndef _MSC_VER
#else
#endif
// InteractiveWheelChair
#include "AssemblyU2DCSharp_InteractiveWheelChairMethodDeclarations.h"

// UnityEngine.Collision
#include "UnityEngine_UnityEngine_Collision.h"
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"


// System.Void InteractiveWheelChair::.ctor()
extern "C" void InteractiveWheelChair__ctor_m1997 (InteractiveWheelChair_t342 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InteractiveWheelChair::Start()
extern "C" void InteractiveWheelChair_Start_m1998 (InteractiveWheelChair_t342 * __this, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t243 * L_1 = GameObject_get_transform_m2825(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t215  L_2 = Transform_get_position_m2894(L_1, /*hidden argument*/NULL);
		__this->___oldPos_4 = L_2;
		return;
	}
}
// System.Void InteractiveWheelChair::Update()
extern "C" void InteractiveWheelChair_Update_m1999 (InteractiveWheelChair_t342 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t243 * L_1 = GameObject_get_transform_m2825(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t215  L_2 = Transform_get_position_m2894(L_1, /*hidden argument*/NULL);
		__this->___curPos_5 = L_2;
		Vector3_t215  L_3 = (__this->___curPos_5);
		Vector3_t215  L_4 = (__this->___oldPos_4);
		Vector3_t215  L_5 = Vector3_op_Subtraction_m3032(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->___movementPos_6 = L_5;
		V_0 = 0;
		goto IL_0079;
	}

IL_0034:
	{
		GameObjectU5BU5D_t323* L_6 = (__this->___wheels_2);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((*(GameObject_t256 **)(GameObject_t256 **)SZArrayLdElema(L_6, L_8)));
		Transform_t243 * L_9 = GameObject_get_transform_m2825((*(GameObject_t256 **)(GameObject_t256 **)SZArrayLdElema(L_6, L_8)), /*hidden argument*/NULL);
		Vector3_t215  L_10 = Vector3_get_forward_m2966(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_12 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Vector3_t215  L_13 = (__this->___fwd_7);
		Vector3_t215  L_14 = (__this->___movementPos_6);
		float L_15 = Vector3_Dot_m3153(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t215  L_16 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		Vector3_t215  L_17 = Vector3_op_Multiply_m2770(NULL /*static, unused*/, L_16, (80000.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_Rotate_m3154(L_9, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) < ((int32_t)2)))
		{
			goto IL_0034;
		}
	}
	{
		return;
	}
}
// System.Void InteractiveWheelChair::LateUpdate()
extern "C" void InteractiveWheelChair_LateUpdate_m2000 (InteractiveWheelChair_t342 * __this, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t215  L_1 = Transform_get_position_m2894(L_0, /*hidden argument*/NULL);
		__this->___oldPos_4 = L_1;
		Transform_t243 * L_2 = Component_get_transform_m2760(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t215  L_3 = Transform_get_forward_m2895(L_2, /*hidden argument*/NULL);
		__this->___fwd_7 = L_3;
		return;
	}
}
// System.Void InteractiveWheelChair::OnCollisionEnter(UnityEngine.Collision)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InteractiveEventManager_t362_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral439;
extern "C" void InteractiveWheelChair_OnCollisionEnter_m2001 (InteractiveWheelChair_t342 * __this, Collision_t471 * ___collision, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		InteractiveEventManager_t362_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(270);
		_stringLiteral439 = il2cpp_codegen_string_literal_from_index(439);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collision_t471 * L_0 = ___collision;
		NullCheck(L_0);
		Collider_t319 * L_1 = Collision_get_collider_m3155(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2979(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m2981(NULL /*static, unused*/, L_2, _stringLiteral439, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InteractiveEventManager_t362_il2cpp_TypeInfo_var);
		InteractiveEventManager_t362 * L_4 = InteractiveEventManager_get_Instance_m2102(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		InteractiveEventManager_dispatchEvent_m2103(L_4, ((int32_t)19), /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// LightController/<FlickerMe>c__IteratorB
#include "AssemblyU2DCSharp_LightController_U3CFlickerMeU3Ec__Iterator.h"
#ifndef _MSC_VER
#else
#endif
// LightController/<FlickerMe>c__IteratorB
#include "AssemblyU2DCSharp_LightController_U3CFlickerMeU3Ec__IteratorMethodDeclarations.h"

// LightController
#include "AssemblyU2DCSharp_LightController.h"
// UnityEngine.RenderSettings
#include "UnityEngine_UnityEngine_RenderSettingsMethodDeclarations.h"
// LightController
#include "AssemblyU2DCSharp_LightControllerMethodDeclarations.h"


// System.Void LightController/<FlickerMe>c__IteratorB::.ctor()
extern "C" void U3CFlickerMeU3Ec__IteratorB__ctor_m2002 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LightController/<FlickerMe>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFlickerMeU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2003 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object LightController/<FlickerMe>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFlickerMeU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m2004 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean LightController/<FlickerMe>c__IteratorB::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern "C" bool U3CFlickerMeU3Ec__IteratorB_MoveNext_m2005 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005c;
		}
	}
	{
		goto IL_00af;
	}

IL_0021:
	{
		float L_2 = Random_Range_m3156(NULL /*static, unused*/, (10.0f), (100.0f), /*hidden argument*/NULL);
		RenderSettings_set_fogEndDistance_m3157(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_3 = (__this->___time_0);
		float L_4 = Random_Range_m3156(NULL /*static, unused*/, (0.0f), L_3, /*hidden argument*/NULL);
		WaitForSeconds_t475 * L_5 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_5, L_4, /*hidden argument*/NULL);
		__this->___U24current_3 = L_5;
		__this->___U24PC_2 = 1;
		goto IL_00b1;
	}

IL_005c:
	{
		LightController_t343 * L_6 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_6);
		bool L_7 = (L_6->___isFlicker_10);
		if (!L_7)
		{
			goto IL_009e;
		}
	}
	{
		LightController_t343 * L_8 = (__this->___U3CU3Ef__this_6);
		LightController_t343 * L_9 = (__this->___U3CU3Ef__this_6);
		bool L_10 = (__this->___val_1);
		float L_11 = (__this->___time_0);
		float L_12 = Random_Range_m3156(NULL /*static, unused*/, (0.0f), L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		Object_t * L_13 = LightController_FlickerMe_m2018(L_9, L_10, L_12, /*hidden argument*/NULL);
		NullCheck(L_8);
		MonoBehaviour_StartCoroutine_m2752(L_8, L_13, /*hidden argument*/NULL);
		goto IL_00a8;
	}

IL_009e:
	{
		RenderSettings_set_fogEndDistance_m3157(NULL /*static, unused*/, (20.0f), /*hidden argument*/NULL);
	}

IL_00a8:
	{
		__this->___U24PC_2 = (-1);
	}

IL_00af:
	{
		return 0;
	}

IL_00b1:
	{
		return 1;
	}
	// Dead block : IL_00b3: ldloc.1
}
// System.Void LightController/<FlickerMe>c__IteratorB::Dispose()
extern "C" void U3CFlickerMeU3Ec__IteratorB_Dispose_m2006 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void LightController/<FlickerMe>c__IteratorB::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CFlickerMeU3Ec__IteratorB_Reset_m2007 (U3CFlickerMeU3Ec__IteratorB_t344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// LightController/<WaitAndSetFlicker>c__IteratorC
#include "AssemblyU2DCSharp_LightController_U3CWaitAndSetFlickerU3Ec__.h"
#ifndef _MSC_VER
#else
#endif
// LightController/<WaitAndSetFlicker>c__IteratorC
#include "AssemblyU2DCSharp_LightController_U3CWaitAndSetFlickerU3Ec__MethodDeclarations.h"



// System.Void LightController/<WaitAndSetFlicker>c__IteratorC::.ctor()
extern "C" void U3CWaitAndSetFlickerU3Ec__IteratorC__ctor_m2008 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LightController/<WaitAndSetFlicker>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndSetFlickerU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2009 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object LightController/<WaitAndSetFlicker>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndSetFlickerU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m2010 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean LightController/<WaitAndSetFlicker>c__IteratorC::MoveNext()
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern "C" bool U3CWaitAndSetFlickerU3Ec__IteratorC_MoveNext_m2011 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0067;
	}

IL_0021:
	{
		float L_2 = (__this->___time_0);
		WaitForSeconds_t475 * L_3 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_3 = L_3;
		__this->___U24PC_2 = 1;
		goto IL_0069;
	}

IL_003e:
	{
		LightController_t343 * L_4 = (__this->___U3CU3Ef__this_6);
		float L_5 = (__this->___time_0);
		NullCheck(L_4);
		L_4->___t3_6 = L_5;
		LightController_t343 * L_6 = (__this->___U3CU3Ef__this_6);
		bool L_7 = (__this->___val_1);
		NullCheck(L_6);
		L_6->___isFlicker_10 = L_7;
		__this->___U24PC_2 = (-1);
	}

IL_0067:
	{
		return 0;
	}

IL_0069:
	{
		return 1;
	}
	// Dead block : IL_006b: ldloc.1
}
// System.Void LightController/<WaitAndSetFlicker>c__IteratorC::Dispose()
extern "C" void U3CWaitAndSetFlickerU3Ec__IteratorC_Dispose_m2012 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void LightController/<WaitAndSetFlicker>c__IteratorC::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CWaitAndSetFlickerU3Ec__IteratorC_Reset_m2013 (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void LightController::.ctor()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern "C" void LightController__ctor_m2014 (LightController_t343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___t2_5 = (100.0f);
		__this->___fadeTime_9 = (50.0f);
		__this->___smoothing_15 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)20)));
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LightController::Start()
extern const MethodInfo* GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var;
extern "C" void LightController_Start_m2015 (LightController_t343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483743);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Light_t318 * L_1 = GameObject_GetComponent_TisLight_t318_m3140(L_0, /*hidden argument*/GameObject_GetComponent_TisLight_t318_m3140_MethodInfo_var);
		__this->___lights_2 = L_1;
		Light_t318 * L_2 = (__this->___lights_2);
		NullCheck(L_2);
		float L_3 = Light_get_intensity_m3158(L_2, /*hidden argument*/NULL);
		__this->___myLightsIntensity_3 = L_3;
		__this->___isFlicker_10 = 0;
		return;
	}
}
// System.Void LightController::Update()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* InteractiveEventManager_t362_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral440;
extern Il2CppCodeGenString* _stringLiteral441;
extern Il2CppCodeGenString* _stringLiteral442;
extern Il2CppCodeGenString* _stringLiteral443;
extern "C" void LightController_Update_m2016 (LightController_t343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		InteractiveEventManager_t362_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(270);
		_stringLiteral440 = il2cpp_codegen_string_literal_from_index(440);
		_stringLiteral441 = il2cpp_codegen_string_literal_from_index(441);
		_stringLiteral442 = il2cpp_codegen_string_literal_from_index(442);
		_stringLiteral443 = il2cpp_codegen_string_literal_from_index(443);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___isFlicker_10);
		if (!L_0)
		{
			goto IL_0096;
		}
	}
	{
		float L_1 = (__this->___t_4);
		float L_2 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___t_4 = ((float)((float)L_1+(float)((float)((float)L_2*(float)(1.0f)))));
		float L_3 = (__this->___t_4);
		float L_4 = (__this->___t3_6);
		if ((!(((float)L_3) >= ((float)L_4))))
		{
			goto IL_0096;
		}
	}
	{
		__this->___t_4 = (0.0f);
		__this->___isFlicker_10 = 0;
		GameObject_t256 * L_5 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2979(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m2981(NULL /*static, unused*/, L_6, _stringLiteral440, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InteractiveEventManager_t362_il2cpp_TypeInfo_var);
		InteractiveEventManager_t362 * L_8 = InteractiveEventManager_get_Instance_m2102(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		InteractiveEventManager_dispatchEvent_m2103(L_8, 4, /*hidden argument*/NULL);
		goto IL_0096;
	}

IL_0070:
	{
		GameObject_t256 * L_9 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_m2979(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m2981(NULL /*static, unused*/, L_10, _stringLiteral441, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0096;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InteractiveEventManager_t362_il2cpp_TypeInfo_var);
		InteractiveEventManager_t362 * L_12 = InteractiveEventManager_get_Instance_m2102(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		InteractiveEventManager_dispatchEvent_m2103(L_12, ((int32_t)31), /*hidden argument*/NULL);
	}

IL_0096:
	{
		bool L_13 = (__this->___ExitFade_12);
		if (!L_13)
		{
			goto IL_00e0;
		}
	}
	{
		float L_14 = (__this->___t2_5);
		float L_15 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___t2_5 = ((float)((float)L_14-(float)((float)((float)L_15*(float)(20.0f)))));
		float L_16 = (__this->___t2_5);
		if ((!(((float)L_16) < ((float)(-1.0f)))))
		{
			goto IL_00d5;
		}
	}
	{
		__this->___ExitFade_12 = 0;
		goto IL_00e0;
	}

IL_00d5:
	{
		float L_17 = (__this->___t2_5);
		RenderSettings_set_fogEndDistance_m3157(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
	}

IL_00e0:
	{
		bool L_18 = (__this->___isFade_11);
		if (!L_18)
		{
			goto IL_0185;
		}
	}
	{
		String_t* L_19 = (__this->___fadeState_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m2981(NULL /*static, unused*/, L_19, _stringLiteral442, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0135;
		}
	}
	{
		float L_21 = (__this->___fadeTime_9);
		float L_22 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = (__this->___timeSpeed_7);
		__this->___fadeTime_9 = ((float)((float)L_21-(float)((float)((float)L_22*(float)L_23))));
		float L_24 = (__this->___fadeTime_9);
		if ((!(((float)L_24) < ((float)(0.0f)))))
		{
			goto IL_0130;
		}
	}
	{
		__this->___isFade_11 = 0;
	}

IL_0130:
	{
		goto IL_017a;
	}

IL_0135:
	{
		String_t* L_25 = (__this->___fadeState_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_26 = String_op_Equality_m2981(NULL /*static, unused*/, L_25, _stringLiteral443, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_017a;
		}
	}
	{
		float L_27 = (__this->___fadeTime_9);
		float L_28 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_29 = (__this->___timeSpeed_7);
		__this->___fadeTime_9 = ((float)((float)L_27+(float)((float)((float)L_28*(float)L_29))));
		float L_30 = (__this->___fadeTime_9);
		if ((!(((float)L_30) > ((float)(100.0f)))))
		{
			goto IL_017a;
		}
	}
	{
		__this->___isFade_11 = 0;
	}

IL_017a:
	{
		float L_31 = (__this->___fadeTime_9);
		RenderSettings_set_fogEndDistance_m3157(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
	}

IL_0185:
	{
		return;
	}
}
// System.Void LightController::OffGameLight()
extern "C" void LightController_OffGameLight_m2017 (LightController_t343 * __this, const MethodInfo* method)
{
	{
		__this->___ExitFade_12 = 1;
		return;
	}
}
// System.Collections.IEnumerator LightController::FlickerMe(System.Boolean,System.Single)
extern TypeInfo* U3CFlickerMeU3Ec__IteratorB_t344_il2cpp_TypeInfo_var;
extern "C" Object_t * LightController_FlickerMe_m2018 (LightController_t343 * __this, bool ___val, float ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFlickerMeU3Ec__IteratorB_t344_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(285);
		s_Il2CppMethodIntialized = true;
	}
	U3CFlickerMeU3Ec__IteratorB_t344 * V_0 = {0};
	{
		U3CFlickerMeU3Ec__IteratorB_t344 * L_0 = (U3CFlickerMeU3Ec__IteratorB_t344 *)il2cpp_codegen_object_new (U3CFlickerMeU3Ec__IteratorB_t344_il2cpp_TypeInfo_var);
		U3CFlickerMeU3Ec__IteratorB__ctor_m2002(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFlickerMeU3Ec__IteratorB_t344 * L_1 = V_0;
		float L_2 = ___time;
		NullCheck(L_1);
		L_1->___time_0 = L_2;
		U3CFlickerMeU3Ec__IteratorB_t344 * L_3 = V_0;
		bool L_4 = ___val;
		NullCheck(L_3);
		L_3->___val_1 = L_4;
		U3CFlickerMeU3Ec__IteratorB_t344 * L_5 = V_0;
		float L_6 = ___time;
		NullCheck(L_5);
		L_5->___U3CU24U3Etime_4 = L_6;
		U3CFlickerMeU3Ec__IteratorB_t344 * L_7 = V_0;
		bool L_8 = ___val;
		NullCheck(L_7);
		L_7->___U3CU24U3Eval_5 = L_8;
		U3CFlickerMeU3Ec__IteratorB_t344 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CFlickerMeU3Ec__IteratorB_t344 * L_10 = V_0;
		return L_10;
	}
}
// System.Collections.IEnumerator LightController::WaitAndSetFlicker(System.Single,System.Boolean)
extern TypeInfo* U3CWaitAndSetFlickerU3Ec__IteratorC_t345_il2cpp_TypeInfo_var;
extern "C" Object_t * LightController_WaitAndSetFlicker_m2019 (LightController_t343 * __this, float ___time, bool ___val, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(286);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * V_0 = {0};
	{
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * L_0 = (U3CWaitAndSetFlickerU3Ec__IteratorC_t345 *)il2cpp_codegen_object_new (U3CWaitAndSetFlickerU3Ec__IteratorC_t345_il2cpp_TypeInfo_var);
		U3CWaitAndSetFlickerU3Ec__IteratorC__ctor_m2008(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * L_1 = V_0;
		float L_2 = ___time;
		NullCheck(L_1);
		L_1->___time_0 = L_2;
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * L_3 = V_0;
		bool L_4 = ___val;
		NullCheck(L_3);
		L_3->___val_1 = L_4;
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * L_5 = V_0;
		float L_6 = ___time;
		NullCheck(L_5);
		L_5->___U3CU24U3Etime_4 = L_6;
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * L_7 = V_0;
		bool L_8 = ___val;
		NullCheck(L_7);
		L_7->___U3CU24U3Eval_5 = L_8;
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * L_9 = V_0;
		NullCheck(L_9);
		L_9->___U3CU3Ef__this_6 = __this;
		U3CWaitAndSetFlickerU3Ec__IteratorC_t345 * L_10 = V_0;
		return L_10;
	}
}
// System.Void LightController::FlickerLights(System.Boolean,System.Single,System.Single)
extern "C" void LightController_FlickerLights_m2020 (LightController_t343 * __this, bool ___val, float ___time, float ___delay, const MethodInfo* method)
{
	{
		bool L_0 = ___val;
		float L_1 = ___delay;
		Object_t * L_2 = LightController_FlickerMe_m2018(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_2, /*hidden argument*/NULL);
		float L_3 = ___time;
		__this->___t3_6 = L_3;
		bool L_4 = ___val;
		__this->___isFlicker_10 = L_4;
		return;
	}
}
// System.Void LightController::OnFlicker(System.Boolean)
extern "C" void LightController_OnFlicker_m2021 (LightController_t343 * __this, bool ___val, const MethodInfo* method)
{
	{
		bool L_0 = ___val;
		__this->___isFlicker_10 = L_0;
		return;
	}
}
// System.Void LightController::OnLights()
extern Il2CppCodeGenString* _stringLiteral444;
extern "C" void LightController_OnLights_m2022 (LightController_t343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral444 = il2cpp_codegen_string_literal_from_index(444);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2979(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m3159(L_1, _stringLiteral444, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Light_t318 * L_3 = (__this->___lights_2);
		NullCheck(L_3);
		Light_set_intensity_m3124(L_3, (3.0f), /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_002f:
	{
		RenderSettings_set_fogEndDistance_m3157(NULL /*static, unused*/, (100.0f), /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void LightController::OffLights()
extern Il2CppCodeGenString* _stringLiteral444;
extern "C" void LightController_OffLights_m2023 (LightController_t343 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral444 = il2cpp_codegen_string_literal_from_index(444);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m2979(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = String_StartsWith_m3159(L_1, _stringLiteral444, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Light_t318 * L_3 = (__this->___lights_2);
		NullCheck(L_3);
		Light_set_intensity_m3124(L_3, (0.0f), /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_002f:
	{
		RenderSettings_set_fogEndDistance_m3157(NULL /*static, unused*/, (20.0f), /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void LightController::FogState(System.Single)
extern "C" void LightController_FogState_m2024 (LightController_t343 * __this, float ___val, const MethodInfo* method)
{
	{
		float L_0 = ___val;
		RenderSettings_set_fogEndDistance_m3157(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LightController::OnFade(System.Boolean,System.Single,System.Single,System.String)
extern "C" void LightController_OnFade_m2025 (LightController_t343 * __this, bool ___val, float ___delay, float ___speed, String_t* ___fadestate, const MethodInfo* method)
{
	{
		bool L_0 = ___val;
		__this->___isFade_11 = L_0;
		float L_1 = ___speed;
		__this->___timeSpeed_7 = L_1;
		float L_2 = ___delay;
		__this->___timeDelay_8 = L_2;
		String_t* L_3 = ___fadestate;
		__this->___fadeState_13 = L_3;
		return;
	}
}
// Loader
#include "AssemblyU2DCSharp_Loader.h"
#ifndef _MSC_VER
#else
#endif
// Loader
#include "AssemblyU2DCSharp_LoaderMethodDeclarations.h"



// System.Void Loader::.ctor()
extern "C" void Loader__ctor_m2026 (Loader_t346 * __this, const MethodInfo* method)
{
	{
		__this->___val_4 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Loader::Start()
extern const MethodInfo* Component_GetComponent_TisAnimator_t321_m3125_MethodInfo_var;
extern "C" void Loader_Start_m2027 (Loader_t346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisAnimator_t321_m3125_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483735);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = Component_GetComponent_TisAnimator_t321_m3125(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t321_m3125_MethodInfo_var);
		__this->___anim_2 = L_0;
		return;
	}
}
// System.Void Loader::Update()
extern "C" void Loader_Update_m2028 (Loader_t346 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Loader::Unload()
extern Il2CppCodeGenString* _stringLiteral445;
extern "C" void Loader_Unload_m2029 (Loader_t346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral445 = il2cpp_codegen_string_literal_from_index(445);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = (__this->___anim_2);
		NullCheck(L_0);
		Animator_SetBool_m3160(L_0, _stringLiteral445, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Loader::Load()
extern Il2CppCodeGenString* _stringLiteral445;
extern "C" void Loader_Load_m2030 (Loader_t346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral445 = il2cpp_codegen_string_literal_from_index(445);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animator_t321 * L_0 = (__this->___anim_2);
		NullCheck(L_0);
		Animator_SetBool_m3160(L_0, _stringLiteral445, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Loader::changeVal(System.Single)
extern "C" void Loader_changeVal_m2031 (Loader_t346 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___val_4 = L_0;
		return;
	}
}
// System.Void Loader::doneLoading()
extern "C" void Loader_doneLoading_m2032 (Loader_t346 * __this, const MethodInfo* method)
{
	{
		__this->___isDoneload_3 = 1;
		return;
	}
}
// AppEvent
#include "AssemblyU2DCSharp_AppEvent.h"
#ifndef _MSC_VER
#else
#endif
// AppEvent
#include "AssemblyU2DCSharp_AppEventMethodDeclarations.h"



// AppManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_AppManager_OnStateChangeHandler.h"
#ifndef _MSC_VER
#else
#endif
// AppManager/OnStateChangeHandler
#include "AssemblyU2DCSharp_AppManager_OnStateChangeHandlerMethodDeclarations.h"



// System.Void AppManager/OnStateChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void OnStateChangeHandler__ctor_m2033 (OnStateChangeHandler_t349 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void AppManager/OnStateChangeHandler::Invoke(AppEvent,System.Collections.Hashtable)
extern "C" void OnStateChangeHandler_Invoke_m2034 (OnStateChangeHandler_t349 * __this, int32_t ___ev, Hashtable_t348 * ___i, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		OnStateChangeHandler_Invoke_m2034((OnStateChangeHandler_t349 *)__this->___prev_9,___ev, ___i, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___ev, Hashtable_t348 * ___i, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___ev, ___i,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___ev, Hashtable_t348 * ___i, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___ev, ___i,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnStateChangeHandler_t349(Il2CppObject* delegate, int32_t ___ev, Hashtable_t348 * ___i)
{
	// Marshaling of parameter '___i' to native representation
	Hashtable_t348 * ____i_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Collections.Hashtable'."));
}
// System.IAsyncResult AppManager/OnStateChangeHandler::BeginInvoke(AppEvent,System.Collections.Hashtable,System.AsyncCallback,System.Object)
extern TypeInfo* AppEvent_t347_il2cpp_TypeInfo_var;
extern "C" Object_t * OnStateChangeHandler_BeginInvoke_m2035 (OnStateChangeHandler_t349 * __this, int32_t ___ev, Hashtable_t348 * ___i, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AppEvent_t347_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(287);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(AppEvent_t347_il2cpp_TypeInfo_var, &___ev);
	__d_args[1] = ___i;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void AppManager/OnStateChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void OnStateChangeHandler_EndInvoke_m2036 (OnStateChangeHandler_t349 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
