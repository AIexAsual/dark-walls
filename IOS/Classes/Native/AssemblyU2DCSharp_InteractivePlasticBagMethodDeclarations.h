﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractivePlasticBag
struct InteractivePlasticBag_t335;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void InteractivePlasticBag::.ctor()
extern "C" void InteractivePlasticBag__ctor_m1982 (InteractivePlasticBag_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractivePlasticBag::Start()
extern "C" void InteractivePlasticBag_Start_m1983 (InteractivePlasticBag_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractivePlasticBag::Update()
extern "C" void InteractivePlasticBag_Update_m1984 (InteractivePlasticBag_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractivePlasticBag::Wiggle()
extern "C" void InteractivePlasticBag_Wiggle_m1985 (InteractivePlasticBag_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InteractivePlasticBag::WaitAndWiggle(System.Single)
extern "C" Object_t * InteractivePlasticBag_WaitAndWiggle_m1986 (InteractivePlasticBag_t335 * __this, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractivePlasticBag::StopWiggle()
extern "C" void InteractivePlasticBag_StopWiggle_m1987 (InteractivePlasticBag_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
