﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t2375;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct  EqualityComparer_1_t2375  : public Object_t
{
};
struct EqualityComparer_1_t2375_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::_default
	EqualityComparer_1_t2375 * ____default_0;
};
