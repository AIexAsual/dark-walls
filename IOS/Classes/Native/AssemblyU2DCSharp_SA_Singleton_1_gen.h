﻿#pragma once
#include <stdint.h>
// SA_ScreenShotMaker
struct SA_ScreenShotMaker_t300;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SA_Singleton`1<SA_ScreenShotMaker>
struct  SA_Singleton_1_t303  : public MonoBehaviour_t4
{
};
struct SA_Singleton_1_t303_StaticFields{
	// T SA_Singleton`1<SA_ScreenShotMaker>::_instance
	SA_ScreenShotMaker_t300 * ____instance_2;
	// System.Boolean SA_Singleton`1<SA_ScreenShotMaker>::applicationIsQuitting
	bool ___applicationIsQuitting_3;
};
