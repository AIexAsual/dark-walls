﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Stages
struct Stages_t351;

// System.Void Stages::.ctor()
extern "C" void Stages__ctor_m2045 (Stages_t351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stages::.cctor()
extern "C" void Stages__cctor_m2046 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
