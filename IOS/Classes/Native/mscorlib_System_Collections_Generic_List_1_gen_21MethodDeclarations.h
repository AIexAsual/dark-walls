﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t775;
// System.Object
struct Object_t;
// UnityEngine.Component
struct Component_t477;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Component>
struct IEnumerable_1_t2981;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Component>
struct IEnumerator_1_t2982;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.Component>
struct ICollection_1_t2983;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Component>
struct ReadOnlyCollection_1_t2272;
// UnityEngine.Component[]
struct ComponentU5BU5D_t539;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t744;
// System.Comparison`1<UnityEngine.Component>
struct Comparison_1_t2274;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m14046(__this, method) (( void (*) (List_1_t775 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m14047(__this, ___collection, method) (( void (*) (List_1_t775 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.ctor(System.Int32)
#define List_1__ctor_m14048(__this, ___capacity, method) (( void (*) (List_1_t775 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.cctor()
#define List_1__cctor_m14049(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14050(__this, method) (( Object_t* (*) (List_1_t775 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m14051(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t775 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14052(__this, method) (( Object_t * (*) (List_1_t775 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m14053(__this, ___item, method) (( int32_t (*) (List_1_t775 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m14054(__this, ___item, method) (( bool (*) (List_1_t775 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m14055(__this, ___item, method) (( int32_t (*) (List_1_t775 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m14056(__this, ___index, ___item, method) (( void (*) (List_1_t775 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m14057(__this, ___item, method) (( void (*) (List_1_t775 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14058(__this, method) (( bool (*) (List_1_t775 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14059(__this, method) (( bool (*) (List_1_t775 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m14060(__this, method) (( Object_t * (*) (List_1_t775 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m14061(__this, method) (( bool (*) (List_1_t775 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m14062(__this, method) (( bool (*) (List_1_t775 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m14063(__this, ___index, method) (( Object_t * (*) (List_1_t775 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m14064(__this, ___index, ___value, method) (( void (*) (List_1_t775 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Add(T)
#define List_1_Add_m14065(__this, ___item, method) (( void (*) (List_1_t775 *, Component_t477 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m14066(__this, ___newCount, method) (( void (*) (List_1_t775 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m14067(__this, ___collection, method) (( void (*) (List_1_t775 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m14068(__this, ___enumerable, method) (( void (*) (List_1_t775 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m14069(__this, ___collection, method) (( void (*) (List_1_t775 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Component>::AsReadOnly()
#define List_1_AsReadOnly_m14070(__this, method) (( ReadOnlyCollection_1_t2272 * (*) (List_1_t775 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Clear()
#define List_1_Clear_m14071(__this, method) (( void (*) (List_1_t775 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::Contains(T)
#define List_1_Contains_m14072(__this, ___item, method) (( bool (*) (List_1_t775 *, Component_t477 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m14073(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t775 *, ComponentU5BU5D_t539*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Component>::Find(System.Predicate`1<T>)
#define List_1_Find_m14074(__this, ___match, method) (( Component_t477 * (*) (List_1_t775 *, Predicate_1_t744 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m14075(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t744 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m14076(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t775 *, int32_t, int32_t, Predicate_1_t744 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Component>::GetEnumerator()
#define List_1_GetEnumerator_m14077(__this, method) (( Enumerator_t2273  (*) (List_1_t775 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::IndexOf(T)
#define List_1_IndexOf_m14078(__this, ___item, method) (( int32_t (*) (List_1_t775 *, Component_t477 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m14079(__this, ___start, ___delta, method) (( void (*) (List_1_t775 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m14080(__this, ___index, method) (( void (*) (List_1_t775 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Insert(System.Int32,T)
#define List_1_Insert_m14081(__this, ___index, ___item, method) (( void (*) (List_1_t775 *, int32_t, Component_t477 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m14082(__this, ___collection, method) (( void (*) (List_1_t775 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::Remove(T)
#define List_1_Remove_m14083(__this, ___item, method) (( bool (*) (List_1_t775 *, Component_t477 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m4953(__this, ___match, method) (( int32_t (*) (List_1_t775 *, Predicate_1_t744 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m14084(__this, ___index, method) (( void (*) (List_1_t775 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Reverse()
#define List_1_Reverse_m14085(__this, method) (( void (*) (List_1_t775 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Sort()
#define List_1_Sort_m14086(__this, method) (( void (*) (List_1_t775 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m14087(__this, ___comparison, method) (( void (*) (List_1_t775 *, Comparison_1_t2274 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Component>::ToArray()
#define List_1_ToArray_m14088(__this, method) (( ComponentU5BU5D_t539* (*) (List_1_t775 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::TrimExcess()
#define List_1_TrimExcess_m14089(__this, method) (( void (*) (List_1_t775 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Capacity()
#define List_1_get_Capacity_m14090(__this, method) (( int32_t (*) (List_1_t775 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m14091(__this, ___value, method) (( void (*) (List_1_t775 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count()
#define List_1_get_Count_m14092(__this, method) (( int32_t (*) (List_1_t775 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32)
#define List_1_get_Item_m14093(__this, ___index, method) (( Component_t477 * (*) (List_1_t775 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::set_Item(System.Int32,T)
#define List_1_set_Item_m14094(__this, ___index, ___value, method) (( void (*) (List_1_t775 *, int32_t, Component_t477 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
