﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>
struct ShimEnumerator_t2717;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2705;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m20510_gshared (ShimEnumerator_t2717 * __this, Dictionary_2_t2705 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m20510(__this, ___host, method) (( void (*) (ShimEnumerator_t2717 *, Dictionary_2_t2705 *, const MethodInfo*))ShimEnumerator__ctor_m20510_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m20511_gshared (ShimEnumerator_t2717 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m20511(__this, method) (( bool (*) (ShimEnumerator_t2717 *, const MethodInfo*))ShimEnumerator_MoveNext_m20511_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m20512_gshared (ShimEnumerator_t2717 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m20512(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2717 *, const MethodInfo*))ShimEnumerator_get_Entry_m20512_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m20513_gshared (ShimEnumerator_t2717 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m20513(__this, method) (( Object_t * (*) (ShimEnumerator_t2717 *, const MethodInfo*))ShimEnumerator_get_Key_m20513_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m20514_gshared (ShimEnumerator_t2717 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m20514(__this, method) (( Object_t * (*) (ShimEnumerator_t2717 *, const MethodInfo*))ShimEnumerator_get_Value_m20514_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m20515_gshared (ShimEnumerator_t2717 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m20515(__this, method) (( Object_t * (*) (ShimEnumerator_t2717 *, const MethodInfo*))ShimEnumerator_get_Current_m20515_gshared)(__this, method)
