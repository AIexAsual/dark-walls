﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Exclusion
struct CameraFilterPack_Blend2Camera_Exclusion_t25;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Exclusion::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Exclusion__ctor_m114 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Exclusion::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Exclusion_get_material_m115 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::Start()
extern "C" void CameraFilterPack_Blend2Camera_Exclusion_Start_m116 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Exclusion_OnRenderImage_m117 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Exclusion_OnValidate_m118 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::Update()
extern "C" void CameraFilterPack_Blend2Camera_Exclusion_Update_m119 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Exclusion_OnEnable_m120 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Exclusion::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Exclusion_OnDisable_m121 (CameraFilterPack_Blend2Camera_Exclusion_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
