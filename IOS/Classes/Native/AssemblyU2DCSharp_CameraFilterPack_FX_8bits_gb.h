﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_FX_8bits_gb
struct  CameraFilterPack_FX_8bits_gb_t121  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_8bits_gb::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_8bits_gb::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_FX_8bits_gb::SCMaterial
	Material_t2 * ___SCMaterial_4;
	// System.Single CameraFilterPack_FX_8bits_gb::Brightness
	float ___Brightness_5;
};
struct CameraFilterPack_FX_8bits_gb_t121_StaticFields{
	// System.Single CameraFilterPack_FX_8bits_gb::ChangeBrightness
	float ___ChangeBrightness_6;
};
