﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// iTween
struct iTween_t432;
// System.Object
#include "mscorlib_System_Object.h"
// iTween/<TweenDelay>c__Iterator1A
struct  U3CTweenDelayU3Ec__Iterator1A_t433  : public Object_t
{
	// System.Int32 iTween/<TweenDelay>c__Iterator1A::$PC
	int32_t ___U24PC_0;
	// System.Object iTween/<TweenDelay>c__Iterator1A::$current
	Object_t * ___U24current_1;
	// iTween iTween/<TweenDelay>c__Iterator1A::<>f__this
	iTween_t432 * ___U3CU3Ef__this_2;
};
