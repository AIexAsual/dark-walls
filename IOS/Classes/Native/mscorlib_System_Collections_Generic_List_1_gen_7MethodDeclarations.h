﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Space>
struct List_1_t579;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Space>
struct IEnumerable_1_t3036;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Space>
struct IEnumerator_1_t3037;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.Space>
struct ICollection_1_t3038;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Space>
struct ReadOnlyCollection_1_t2441;
// UnityEngine.Space[]
struct SpaceU5BU5D_t450;
// System.Predicate`1<UnityEngine.Space>
struct Predicate_1_t2445;
// System.Comparison`1<UnityEngine.Space>
struct Comparison_1_t2448;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::.ctor()
extern "C" void List_1__ctor_m3394_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1__ctor_m3394(__this, method) (( void (*) (List_1_t579 *, const MethodInfo*))List_1__ctor_m3394_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m16216_gshared (List_1_t579 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m16216(__this, ___collection, method) (( void (*) (List_1_t579 *, Object_t*, const MethodInfo*))List_1__ctor_m16216_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::.ctor(System.Int32)
extern "C" void List_1__ctor_m16217_gshared (List_1_t579 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m16217(__this, ___capacity, method) (( void (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1__ctor_m16217_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::.cctor()
extern "C" void List_1__cctor_m16218_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m16218(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m16218_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16219_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16219(__this, method) (( Object_t* (*) (List_1_t579 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16219_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m16220_gshared (List_1_t579 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m16220(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t579 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m16220_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m16221_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16221(__this, method) (( Object_t * (*) (List_1_t579 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m16221_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m16222_gshared (List_1_t579 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m16222(__this, ___item, method) (( int32_t (*) (List_1_t579 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m16222_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m16223_gshared (List_1_t579 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m16223(__this, ___item, method) (( bool (*) (List_1_t579 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m16223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m16224_gshared (List_1_t579 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m16224(__this, ___item, method) (( int32_t (*) (List_1_t579 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m16224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m16225_gshared (List_1_t579 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m16225(__this, ___index, ___item, method) (( void (*) (List_1_t579 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m16225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m16226_gshared (List_1_t579 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m16226(__this, ___item, method) (( void (*) (List_1_t579 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m16226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16227_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16227(__this, method) (( bool (*) (List_1_t579 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16227_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m16228_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16228(__this, method) (( bool (*) (List_1_t579 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m16228_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m16229_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m16229(__this, method) (( Object_t * (*) (List_1_t579 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m16229_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m16230_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m16230(__this, method) (( bool (*) (List_1_t579 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m16230_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m16231_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m16231(__this, method) (( bool (*) (List_1_t579 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m16231_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m16232_gshared (List_1_t579 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m16232(__this, ___index, method) (( Object_t * (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m16232_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m16233_gshared (List_1_t579 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m16233(__this, ___index, ___value, method) (( void (*) (List_1_t579 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m16233_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::Add(T)
extern "C" void List_1_Add_m16234_gshared (List_1_t579 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m16234(__this, ___item, method) (( void (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_Add_m16234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m16235_gshared (List_1_t579 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m16235(__this, ___newCount, method) (( void (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m16235_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m16236_gshared (List_1_t579 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m16236(__this, ___collection, method) (( void (*) (List_1_t579 *, Object_t*, const MethodInfo*))List_1_AddCollection_m16236_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m16237_gshared (List_1_t579 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m16237(__this, ___enumerable, method) (( void (*) (List_1_t579 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m16237_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m16238_gshared (List_1_t579 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m16238(__this, ___collection, method) (( void (*) (List_1_t579 *, Object_t*, const MethodInfo*))List_1_AddRange_m16238_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Space>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2441 * List_1_AsReadOnly_m16239_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m16239(__this, method) (( ReadOnlyCollection_1_t2441 * (*) (List_1_t579 *, const MethodInfo*))List_1_AsReadOnly_m16239_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::Clear()
extern "C" void List_1_Clear_m16240_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_Clear_m16240(__this, method) (( void (*) (List_1_t579 *, const MethodInfo*))List_1_Clear_m16240_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Space>::Contains(T)
extern "C" bool List_1_Contains_m16241_gshared (List_1_t579 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m16241(__this, ___item, method) (( bool (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_Contains_m16241_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m16242_gshared (List_1_t579 * __this, SpaceU5BU5D_t450* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m16242(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t579 *, SpaceU5BU5D_t450*, int32_t, const MethodInfo*))List_1_CopyTo_m16242_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Space>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m16243_gshared (List_1_t579 * __this, Predicate_1_t2445 * ___match, const MethodInfo* method);
#define List_1_Find_m16243(__this, ___match, method) (( int32_t (*) (List_1_t579 *, Predicate_1_t2445 *, const MethodInfo*))List_1_Find_m16243_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m16244_gshared (Object_t * __this /* static, unused */, Predicate_1_t2445 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m16244(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2445 *, const MethodInfo*))List_1_CheckMatch_m16244_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m16245_gshared (List_1_t579 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2445 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m16245(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t579 *, int32_t, int32_t, Predicate_1_t2445 *, const MethodInfo*))List_1_GetIndex_m16245_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Space>::GetEnumerator()
extern "C" Enumerator_t2440  List_1_GetEnumerator_m16246_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m16246(__this, method) (( Enumerator_t2440  (*) (List_1_t579 *, const MethodInfo*))List_1_GetEnumerator_m16246_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m16247_gshared (List_1_t579 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m16247(__this, ___item, method) (( int32_t (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_IndexOf_m16247_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m16248_gshared (List_1_t579 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m16248(__this, ___start, ___delta, method) (( void (*) (List_1_t579 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m16248_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m16249_gshared (List_1_t579 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m16249(__this, ___index, method) (( void (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_CheckIndex_m16249_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m16250_gshared (List_1_t579 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m16250(__this, ___index, ___item, method) (( void (*) (List_1_t579 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m16250_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m16251_gshared (List_1_t579 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m16251(__this, ___collection, method) (( void (*) (List_1_t579 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m16251_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Space>::Remove(T)
extern "C" bool List_1_Remove_m16252_gshared (List_1_t579 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m16252(__this, ___item, method) (( bool (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_Remove_m16252_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m16253_gshared (List_1_t579 * __this, Predicate_1_t2445 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m16253(__this, ___match, method) (( int32_t (*) (List_1_t579 *, Predicate_1_t2445 *, const MethodInfo*))List_1_RemoveAll_m16253_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m16254_gshared (List_1_t579 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m16254(__this, ___index, method) (( void (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_RemoveAt_m16254_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::Reverse()
extern "C" void List_1_Reverse_m16255_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_Reverse_m16255(__this, method) (( void (*) (List_1_t579 *, const MethodInfo*))List_1_Reverse_m16255_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::Sort()
extern "C" void List_1_Sort_m16256_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_Sort_m16256(__this, method) (( void (*) (List_1_t579 *, const MethodInfo*))List_1_Sort_m16256_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m16257_gshared (List_1_t579 * __this, Comparison_1_t2448 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m16257(__this, ___comparison, method) (( void (*) (List_1_t579 *, Comparison_1_t2448 *, const MethodInfo*))List_1_Sort_m16257_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Space>::ToArray()
extern "C" SpaceU5BU5D_t450* List_1_ToArray_m3407_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_ToArray_m3407(__this, method) (( SpaceU5BU5D_t450* (*) (List_1_t579 *, const MethodInfo*))List_1_ToArray_m3407_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::TrimExcess()
extern "C" void List_1_TrimExcess_m16258_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m16258(__this, method) (( void (*) (List_1_t579 *, const MethodInfo*))List_1_TrimExcess_m16258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m16259_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m16259(__this, method) (( int32_t (*) (List_1_t579 *, const MethodInfo*))List_1_get_Capacity_m16259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m16260_gshared (List_1_t579 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m16260(__this, ___value, method) (( void (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_set_Capacity_m16260_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Space>::get_Count()
extern "C" int32_t List_1_get_Count_m16261_gshared (List_1_t579 * __this, const MethodInfo* method);
#define List_1_get_Count_m16261(__this, method) (( int32_t (*) (List_1_t579 *, const MethodInfo*))List_1_get_Count_m16261_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Space>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m16262_gshared (List_1_t579 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m16262(__this, ___index, method) (( int32_t (*) (List_1_t579 *, int32_t, const MethodInfo*))List_1_get_Item_m16262_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Space>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m16263_gshared (List_1_t579 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m16263(__this, ___index, ___value, method) (( void (*) (List_1_t579 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m16263_gshared)(__this, ___index, ___value, method)
