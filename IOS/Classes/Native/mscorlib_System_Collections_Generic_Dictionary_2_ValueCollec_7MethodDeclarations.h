﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct ValueCollection_t2345;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Dictionary_2_t420;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct IEnumerator_1_t3008;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>[]
struct Dictionary_2U5BU5D_t2317;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_41.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5MethodDeclarations.h"
#define ValueCollection__ctor_m15054(__this, ___dictionary, method) (( void (*) (ValueCollection_t2345 *, Dictionary_2_t420 *, const MethodInfo*))ValueCollection__ctor_m14881_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m15055(__this, ___item, method) (( void (*) (ValueCollection_t2345 *, Dictionary_2_t545 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m14882_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m15056(__this, method) (( void (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m14883_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m15057(__this, ___item, method) (( bool (*) (ValueCollection_t2345 *, Dictionary_2_t545 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m14884_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m15058(__this, ___item, method) (( bool (*) (ValueCollection_t2345 *, Dictionary_2_t545 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m14885_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m15059(__this, method) (( Object_t* (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m14886_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m15060(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2345 *, Array_t *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m14887_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m15061(__this, method) (( Object_t * (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m14888_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m15062(__this, method) (( bool (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m14889_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m15063(__this, method) (( bool (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m14890_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m15064(__this, method) (( Object_t * (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m14891_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m15065(__this, ___array, ___index, method) (( void (*) (ValueCollection_t2345 *, Dictionary_2U5BU5D_t2317*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m14892_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m15066(__this, method) (( Enumerator_t3009  (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_GetEnumerator_m14893_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Count()
#define ValueCollection_get_Count_m15067(__this, method) (( int32_t (*) (ValueCollection_t2345 *, const MethodInfo*))ValueCollection_get_Count_m14894_gshared)(__this, method)
