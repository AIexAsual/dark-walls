﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// CameraFilterPack_Blur_Dithering2x2
struct  CameraFilterPack_Blur_Dithering2x2_t51  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Blur_Dithering2x2::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Dithering2x2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Dithering2x2::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Dithering2x2::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Int32 CameraFilterPack_Blur_Dithering2x2::Level
	int32_t ___Level_6;
	// UnityEngine.Vector2 CameraFilterPack_Blur_Dithering2x2::Distance
	Vector2_t7  ___Distance_7;
};
struct CameraFilterPack_Blur_Dithering2x2_t51_StaticFields{
	// System.Int32 CameraFilterPack_Blur_Dithering2x2::ChangeLevel
	int32_t ___ChangeLevel_8;
	// UnityEngine.Vector2 CameraFilterPack_Blur_Dithering2x2::ChangeDistance
	Vector2_t7  ___ChangeDistance_9;
};
