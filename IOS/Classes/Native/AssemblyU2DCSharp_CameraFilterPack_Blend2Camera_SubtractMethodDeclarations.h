﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Subtract
struct CameraFilterPack_Blend2Camera_Subtract_t45;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Subtract::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Subtract__ctor_m266 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Subtract::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Subtract_get_material_m267 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::Start()
extern "C" void CameraFilterPack_Blend2Camera_Subtract_Start_m268 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Subtract_OnRenderImage_m269 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Subtract_OnValidate_m270 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::Update()
extern "C" void CameraFilterPack_Blend2Camera_Subtract_Update_m271 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Subtract_OnEnable_m272 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Subtract::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Subtract_OnDisable_m273 (CameraFilterPack_Blend2Camera_Subtract_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
