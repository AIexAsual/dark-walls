﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Enumerator_t2748;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2742;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20899_gshared (Enumerator_t2748 * __this, Dictionary_2_t2742 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m20899(__this, ___dictionary, method) (( void (*) (Enumerator_t2748 *, Dictionary_2_t2742 *, const MethodInfo*))Enumerator__ctor_m20899_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20900_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20900(__this, method) (( Object_t * (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20900_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20901_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20901(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20901_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20902_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20902(__this, method) (( Object_t * (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20902_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20903_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20903(__this, method) (( Object_t * (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20903_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20904_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20904(__this, method) (( bool (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_MoveNext_m20904_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2743  Enumerator_get_Current_m20905_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20905(__this, method) (( KeyValuePair_2_t2743  (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_get_Current_m20905_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_CurrentKey()
extern "C" uint64_t Enumerator_get_CurrentKey_m20906_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m20906(__this, method) (( uint64_t (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_get_CurrentKey_m20906_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m20907_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m20907(__this, method) (( Object_t * (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_get_CurrentValue_m20907_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m20908_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20908(__this, method) (( void (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_VerifyState_m20908_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m20909_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m20909(__this, method) (( void (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_VerifyCurrent_m20909_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m20910_gshared (Enumerator_t2748 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20910(__this, method) (( void (*) (Enumerator_t2748 *, const MethodInfo*))Enumerator_Dispose_m20910_gshared)(__this, method)
