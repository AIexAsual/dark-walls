﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_VHS_Tracking
struct CameraFilterPack_VHS_Tracking_t204;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_VHS_Tracking::.ctor()
extern "C" void CameraFilterPack_VHS_Tracking__ctor_m1324 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_VHS_Tracking::get_material()
extern "C" Material_t2 * CameraFilterPack_VHS_Tracking_get_material_m1325 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::Start()
extern "C" void CameraFilterPack_VHS_Tracking_Start_m1326 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_VHS_Tracking_OnRenderImage_m1327 (CameraFilterPack_VHS_Tracking_t204 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::OnValidate()
extern "C" void CameraFilterPack_VHS_Tracking_OnValidate_m1328 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::Update()
extern "C" void CameraFilterPack_VHS_Tracking_Update_m1329 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_VHS_Tracking::OnDisable()
extern "C" void CameraFilterPack_VHS_Tracking_OnDisable_m1330 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
