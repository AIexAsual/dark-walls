﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ScoreTextScript
struct  ScoreTextScript_t409  : public MonoBehaviour_t4
{
	// System.Single ScoreTextScript::fadeTime
	float ___fadeTime_2;
	// System.Single ScoreTextScript::startTime
	float ___startTime_3;
};
