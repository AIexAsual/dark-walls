﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LightController
struct LightController_t343;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.String
struct String_t;

// System.Void LightController::.ctor()
extern "C" void LightController__ctor_m2014 (LightController_t343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::Start()
extern "C" void LightController_Start_m2015 (LightController_t343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::Update()
extern "C" void LightController_Update_m2016 (LightController_t343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::OffGameLight()
extern "C" void LightController_OffGameLight_m2017 (LightController_t343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LightController::FlickerMe(System.Boolean,System.Single)
extern "C" Object_t * LightController_FlickerMe_m2018 (LightController_t343 * __this, bool ___val, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LightController::WaitAndSetFlicker(System.Single,System.Boolean)
extern "C" Object_t * LightController_WaitAndSetFlicker_m2019 (LightController_t343 * __this, float ___time, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::FlickerLights(System.Boolean,System.Single,System.Single)
extern "C" void LightController_FlickerLights_m2020 (LightController_t343 * __this, bool ___val, float ___time, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::OnFlicker(System.Boolean)
extern "C" void LightController_OnFlicker_m2021 (LightController_t343 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::OnLights()
extern "C" void LightController_OnLights_m2022 (LightController_t343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::OffLights()
extern "C" void LightController_OffLights_m2023 (LightController_t343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::FogState(System.Single)
extern "C" void LightController_FogState_m2024 (LightController_t343 * __this, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightController::OnFade(System.Boolean,System.Single,System.Single,System.String)
extern "C" void LightController_OnFade_m2025 (LightController_t343 * __this, bool ___val, float ___delay, float ___speed, String_t* ___fadestate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
