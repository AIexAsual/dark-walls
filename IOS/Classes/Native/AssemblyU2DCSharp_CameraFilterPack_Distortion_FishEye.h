﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_FishEye
struct  CameraFilterPack_Distortion_FishEye_t85  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_FishEye::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_FishEye::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_FishEye::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_FishEye::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_FishEye::Distortion
	float ___Distortion_6;
};
struct CameraFilterPack_Distortion_FishEye_t85_StaticFields{
	// System.Single CameraFilterPack_Distortion_FishEye::ChangeDistortion
	float ___ChangeDistortion_7;
};
