﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNWP8RateUsPopUp
struct MNWP8RateUsPopUp_t294;
// System.String
struct String_t;

// System.Void MNWP8RateUsPopUp::.ctor()
extern "C" void MNWP8RateUsPopUp__ctor_m1821 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNWP8RateUsPopUp MNWP8RateUsPopUp::Create(System.String,System.String)
extern "C" MNWP8RateUsPopUp_t294 * MNWP8RateUsPopUp_Create_m1822 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8RateUsPopUp::init()
extern "C" void MNWP8RateUsPopUp_init_m1823 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8RateUsPopUp::OnOkDel()
extern "C" void MNWP8RateUsPopUp_OnOkDel_m1824 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNWP8RateUsPopUp::OnCancelDel()
extern "C" void MNWP8RateUsPopUp_OnCancelDel_m1825 (MNWP8RateUsPopUp_t294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
