﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIContent
struct GUIContent_t807;
// UnityEngine.GUILayoutEntry
#include "UnityEngine_UnityEngine_GUILayoutEntry.h"
// UnityEngine.GUIWordWrapSizer
struct  GUIWordWrapSizer_t880  : public GUILayoutEntry_t877
{
	// UnityEngine.GUIContent UnityEngine.GUIWordWrapSizer::content
	GUIContent_t807 * ___content_10;
	// System.Single UnityEngine.GUIWordWrapSizer::forcedMinHeight
	float ___forcedMinHeight_11;
	// System.Single UnityEngine.GUIWordWrapSizer::forcedMaxHeight
	float ___forcedMaxHeight_12;
};
