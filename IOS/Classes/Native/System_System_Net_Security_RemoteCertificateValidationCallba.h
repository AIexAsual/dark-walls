﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1304;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1363;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Net.Security.RemoteCertificateValidationCallback
struct  RemoteCertificateValidationCallback_t1361  : public MulticastDelegate_t219
{
};
