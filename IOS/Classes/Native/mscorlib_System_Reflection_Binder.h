﻿#pragma once
#include <stdint.h>
// System.Reflection.Binder
struct Binder_t1160;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Binder
struct  Binder_t1160  : public Object_t
{
};
struct Binder_t1160_StaticFields{
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t1160 * ___default_binder_0;
};
