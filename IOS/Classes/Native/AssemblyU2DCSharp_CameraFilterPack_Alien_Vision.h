﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Alien_Vision
struct  CameraFilterPack_Alien_Vision_t11  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Alien_Vision::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Alien_Vision::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Alien_Vision::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Alien_Vision::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Alien_Vision::Therma_Variation
	float ___Therma_Variation_6;
	// System.Single CameraFilterPack_Alien_Vision::Speed
	float ___Speed_7;
	// System.Single CameraFilterPack_Alien_Vision::Burn
	float ___Burn_8;
	// System.Single CameraFilterPack_Alien_Vision::SceneCut
	float ___SceneCut_9;
};
struct CameraFilterPack_Alien_Vision_t11_StaticFields{
	// System.Single CameraFilterPack_Alien_Vision::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Alien_Vision::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Alien_Vision::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Alien_Vision::ChangeValue4
	float ___ChangeValue4_13;
};
