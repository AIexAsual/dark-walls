﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Dictionary_2_t420;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct  Enumerator_t2346 
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::dictionary
	Dictionary_2_t420 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::current
	KeyValuePair_2_t2343  ___current_3;
};
