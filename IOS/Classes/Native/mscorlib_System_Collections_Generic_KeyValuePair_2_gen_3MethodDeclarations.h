﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyValuePair_2_t1149;
// System.String
struct String_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t993;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21354(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1149 *, String_t*, GetDelegate_t993 *, const MethodInfo*))KeyValuePair_2__ctor_m14739_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m6462(__this, method) (( String_t* (*) (KeyValuePair_2_t1149 *, const MethodInfo*))KeyValuePair_2_get_Key_m14741_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21355(__this, ___value, method) (( void (*) (KeyValuePair_2_t1149 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m14743_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m6461(__this, method) (( GetDelegate_t993 * (*) (KeyValuePair_2_t1149 *, const MethodInfo*))KeyValuePair_2_get_Value_m14745_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21356(__this, ___value, method) (( void (*) (KeyValuePair_2_t1149 *, GetDelegate_t993 *, const MethodInfo*))KeyValuePair_2_set_Value_m14747_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToString()
#define KeyValuePair_2_ToString_m21357(__this, method) (( String_t* (*) (KeyValuePair_2_t1149 *, const MethodInfo*))KeyValuePair_2_ToString_m14749_gshared)(__this, method)
