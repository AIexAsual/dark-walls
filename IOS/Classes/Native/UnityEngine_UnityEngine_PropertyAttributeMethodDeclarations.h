﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.PropertyAttribute
struct PropertyAttribute_t1046;

// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C" void PropertyAttribute__ctor_m6242 (PropertyAttribute_t1046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
