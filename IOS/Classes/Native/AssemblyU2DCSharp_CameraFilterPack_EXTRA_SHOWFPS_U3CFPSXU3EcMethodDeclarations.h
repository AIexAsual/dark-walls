﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0
struct U3CFPSXU3Ec__Iterator0_t111;
// System.Object
struct Object_t;

// System.Void CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::.ctor()
extern "C" void U3CFPSXU3Ec__Iterator0__ctor_m705 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFPSXU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m706 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFPSXU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m707 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::MoveNext()
extern "C" bool U3CFPSXU3Ec__Iterator0_MoveNext_m708 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::Dispose()
extern "C" void U3CFPSXU3Ec__Iterator0_Dispose_m709 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::Reset()
extern "C" void U3CFPSXU3Ec__Iterator0_Reset_m710 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
