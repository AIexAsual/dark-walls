﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t578;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>
struct  Enumerator_t2431 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::l
	List_1_t578 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::current
	Color_t6  ___current_3;
};
