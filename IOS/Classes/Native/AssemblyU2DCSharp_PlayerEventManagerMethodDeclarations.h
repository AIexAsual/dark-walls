﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerEventManager
struct PlayerEventManager_t378;
// PlayerEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t377;
// System.Collections.Hashtable
struct Hashtable_t348;
// PlayerEvent
#include "AssemblyU2DCSharp_PlayerEvent.h"

// System.Void PlayerEventManager::.ctor()
extern "C" void PlayerEventManager__ctor_m2176 (PlayerEventManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager::.cctor()
extern "C" void PlayerEventManager__cctor_m2177 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager::add_OnStateChangeEvent(PlayerEventManager/OnStateChangeHandler)
extern "C" void PlayerEventManager_add_OnStateChangeEvent_m2178 (PlayerEventManager_t378 * __this, OnStateChangeHandler_t377 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager::remove_OnStateChangeEvent(PlayerEventManager/OnStateChangeHandler)
extern "C" void PlayerEventManager_remove_OnStateChangeEvent_m2179 (PlayerEventManager_t378 * __this, OnStateChangeHandler_t377 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PlayerEventManager PlayerEventManager::get_Instance()
extern "C" PlayerEventManager_t378 * PlayerEventManager_get_Instance_m2180 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager::dispatchEvent(PlayerEvent,System.Collections.Hashtable)
extern "C" void PlayerEventManager_dispatchEvent_m2181 (PlayerEventManager_t378 * __this, int32_t ___stateEvent, Hashtable_t348 * ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager::Start()
extern "C" void PlayerEventManager_Start_m2182 (PlayerEventManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerEventManager::Update()
extern "C" void PlayerEventManager_Update_m2183 (PlayerEventManager_t378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
