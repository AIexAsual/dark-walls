﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// HeadRotation
struct HeadRotation_t320;

// System.Void HeadRotation::.ctor()
extern "C" void HeadRotation__ctor_m1903 (HeadRotation_t320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadRotation::Start()
extern "C" void HeadRotation_Start_m1904 (HeadRotation_t320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeadRotation::Update()
extern "C" void HeadRotation_Update_m1905 (HeadRotation_t320 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
