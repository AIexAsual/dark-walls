﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Sprites.DataUtility
#include "UnityEngine_UnityEngine_Sprites_DataUtility.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// UnityEngine.Sprites.DataUtility
#include "UnityEngine_UnityEngine_Sprites_DataUtilityMethodDeclarations.h"

// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.Void
#include "mscorlib_System_Void.h"

// System.Array
#include "mscorlib_System_Array.h"

// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern "C" Vector4_t5  DataUtility_GetInnerUV_m4743 (Object_t * __this /* static, unused */, Sprite_t668 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t5  (*DataUtility_GetInnerUV_m4743_ftn) (Sprite_t668 *);
	static DataUtility_GetInnerUV_m4743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetInnerUV_m4743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern "C" Vector4_t5  DataUtility_GetOuterUV_m4742 (Object_t * __this /* static, unused */, Sprite_t668 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t5  (*DataUtility_GetOuterUV_m4742_ftn) (Sprite_t668 *);
	static DataUtility_GetOuterUV_m4742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetOuterUV_m4742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern "C" Vector4_t5  DataUtility_GetPadding_m4736 (Object_t * __this /* static, unused */, Sprite_t668 * ___sprite, const MethodInfo* method)
{
	typedef Vector4_t5  (*DataUtility_GetPadding_m4736_ftn) (Sprite_t668 *);
	static DataUtility_GetPadding_m4736_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_GetPadding_m4736_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)");
	return _il2cpp_icall_func(___sprite);
}
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern "C" Vector2_t7  DataUtility_GetMinSize_m4748 (Object_t * __this /* static, unused */, Sprite_t668 * ___sprite, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		Sprite_t668 * L_0 = ___sprite;
		DataUtility_Internal_GetMinSize_m5538(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
extern "C" void DataUtility_Internal_GetMinSize_m5538 (Object_t * __this /* static, unused */, Sprite_t668 * ___sprite, Vector2_t7 * ___output, const MethodInfo* method)
{
	typedef void (*DataUtility_Internal_GetMinSize_m5538_ftn) (Sprite_t668 *, Vector2_t7 *);
	static DataUtility_Internal_GetMinSize_m5538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DataUtility_Internal_GetMinSize_m5538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___sprite, ___output);
}
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWW.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WWW
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"

// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
#include "mscorlib_ArrayTypes.h"
// System.Byte
#include "mscorlib_System_Byte.h"
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.WWWForm
#include "UnityEngine_UnityEngine_WWWForm.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
// System.IO.StringReader
#include "mscorlib_System_IO_StringReader.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
// System.Text.Encoding
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.WWWForm
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11MethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1MethodDeclarations.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1MethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.IO.StringReader
#include "mscorlib_System_IO_StringReaderMethodDeclarations.h"


// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C" void WWW__ctor_m3104 (WWW_t304 * __this, String_t* ___url, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url;
		WWW_InitWWW_m5543(__this, L_0, (ByteU5BU5D_t469*)(ByteU5BU5D_t469*)NULL, (StringU5BU5D_t398*)(StringU5BU5D_t398*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,UnityEngine.WWWForm)
extern "C" void WWW__ctor_m5539 (WWW_t304 * __this, String_t* ___url, WWWForm_t908 * ___form, const MethodInfo* method)
{
	StringU5BU5D_t398* V_0 = {0};
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		WWWForm_t908 * L_0 = ___form;
		NullCheck(L_0);
		Dictionary_2_t1083 * L_1 = WWWForm_get_headers_m5558(L_0, /*hidden argument*/NULL);
		StringU5BU5D_t398* L_2 = WWW_FlattenedHeadersFrom_m5552(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___url;
		WWWForm_t908 * L_4 = ___form;
		NullCheck(L_4);
		ByteU5BU5D_t469* L_5 = WWWForm_get_data_m5559(L_4, /*hidden argument*/NULL);
		StringU5BU5D_t398* L_6 = V_0;
		WWW_InitWWW_m5543(__this, L_3, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Dispose()
extern "C" void WWW_Dispose_m5540 (WWW_t304 * __this, const MethodInfo* method)
{
	{
		WWW_DestroyWWW_m5542(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::Finalize()
extern "C" void WWW_Finalize_m5541 (WWW_t304 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		WWW_DestroyWWW_m5542(__this, 0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
extern "C" void WWW_DestroyWWW_m5542 (WWW_t304 * __this, bool ___cancel, const MethodInfo* method)
{
	typedef void (*WWW_DestroyWWW_m5542_ftn) (WWW_t304 *, bool);
	static WWW_DestroyWWW_m5542_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_DestroyWWW_m5542_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::DestroyWWW(System.Boolean)");
	_il2cpp_icall_func(__this, ___cancel);
}
// System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
extern "C" void WWW_InitWWW_m5543 (WWW_t304 * __this, String_t* ___url, ByteU5BU5D_t469* ___postData, StringU5BU5D_t398* ___iHeaders, const MethodInfo* method)
{
	typedef void (*WWW_InitWWW_m5543_ftn) (WWW_t304 *, String_t*, ByteU5BU5D_t469*, StringU5BU5D_t398*);
	static WWW_InitWWW_m5543_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_InitWWW_m5543_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])");
	_il2cpp_icall_func(__this, ___url, ___postData, ___iHeaders);
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::get_responseHeaders()
extern TypeInfo* UnityException_t804_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral955;
extern "C" Dictionary_2_t1083 * WWW_get_responseHeaders_m5544 (WWW_t304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityException_t804_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(526);
		_stringLiteral955 = il2cpp_codegen_string_literal_from_index(955);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = WWW_get_isDone_m5551(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t804 * L_1 = (UnityException_t804 *)il2cpp_codegen_object_new (UnityException_t804_il2cpp_TypeInfo_var);
		UnityException__ctor_m6259(L_1, _stringLiteral955, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		String_t* L_2 = WWW_get_responseHeadersString_m5545(__this, /*hidden argument*/NULL);
		Dictionary_2_t1083 * L_3 = WWW_ParseHTTPHeaderString_m5553(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String UnityEngine.WWW::get_responseHeadersString()
extern "C" String_t* WWW_get_responseHeadersString_m5545 (WWW_t304 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_responseHeadersString_m5545_ftn) (WWW_t304 *);
	static WWW_get_responseHeadersString_m5545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_responseHeadersString_m5545_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_responseHeadersString()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_text()
extern TypeInfo* UnityException_t804_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral956;
extern "C" String_t* WWW_get_text_m5546 (WWW_t304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityException_t804_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(526);
		_stringLiteral956 = il2cpp_codegen_string_literal_from_index(956);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t469* V_0 = {0};
	{
		bool L_0 = WWW_get_isDone_m5551(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		UnityException_t804 * L_1 = (UnityException_t804 *)il2cpp_codegen_object_new (UnityException_t804_il2cpp_TypeInfo_var);
		UnityException__ctor_m6259(L_1, _stringLiteral956, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		ByteU5BU5D_t469* L_2 = WWW_get_bytes_m5549(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		Encoding_t519 * L_3 = WWW_GetTextEncoder_m5548(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t469* L_4 = V_0;
		ByteU5BU5D_t469* L_5 = V_0;
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(19 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)(((Array_t *)L_5)->max_length))));
		return L_6;
	}
}
// System.Text.Encoding UnityEngine.WWW::get_DefaultEncoding()
extern TypeInfo* Encoding_t519_il2cpp_TypeInfo_var;
extern "C" Encoding_t519 * WWW_get_DefaultEncoding_m5547 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(225);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
		Encoding_t519 * L_0 = Encoding_get_ASCII_m6378(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Text.Encoding UnityEngine.WWW::GetTextEncoder()
extern TypeInfo* CharU5BU5D_t530_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t519_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t520_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral957;
extern Il2CppCodeGenString* _stringLiteral958;
extern Il2CppCodeGenString* _stringLiteral959;
extern Il2CppCodeGenString* _stringLiteral806;
extern "C" Encoding_t519 * WWW_GetTextEncoder_m5548 (WWW_t304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t530_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		Encoding_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(225);
		Exception_t520_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral957 = il2cpp_codegen_string_literal_from_index(957);
		_stringLiteral958 = il2cpp_codegen_string_literal_from_index(958);
		_stringLiteral959 = il2cpp_codegen_string_literal_from_index(959);
		_stringLiteral806 = il2cpp_codegen_string_literal_from_index(806);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	Encoding_t519 * V_5 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = (String_t*)NULL;
		Dictionary_2_t1083 * L_0 = WWW_get_responseHeaders_m5544(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_0, _stringLiteral957, (&V_0));
		if (!L_1)
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_2 = V_0;
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m6379(L_2, _stringLiteral958, 5, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = String_IndexOf_m6380(L_5, ((int32_t)61), L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) <= ((int32_t)(-1))))
		{
			goto IL_00b0;
		}
	}
	{
		String_t* L_9 = V_0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		String_t* L_11 = String_Substring_m4812(L_9, ((int32_t)((int32_t)L_10+(int32_t)1)), /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = String_Trim_m6381(L_11, /*hidden argument*/NULL);
		CharU5BU5D_t530* L_13 = ((CharU5BU5D_t530*)SZArrayNew(CharU5BU5D_t530_il2cpp_TypeInfo_var, 2));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_13, 0)) = (uint16_t)((int32_t)39);
		CharU5BU5D_t530* L_14 = L_13;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_14, 1)) = (uint16_t)((int32_t)34);
		NullCheck(L_12);
		String_t* L_15 = String_Trim_m6382(L_12, L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = String_Trim_m6381(L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		String_t* L_17 = V_3;
		NullCheck(L_17);
		int32_t L_18 = String_IndexOf_m4839(L_17, ((int32_t)59), /*hidden argument*/NULL);
		V_4 = L_18;
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) <= ((int32_t)(-1))))
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_20 = V_3;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		String_t* L_22 = String_Substring_m3341(L_20, 0, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_23 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_24 = Encoding_GetEncoding_m6383(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
			V_5 = L_24;
			goto IL_00b6;
		}

IL_0090:
		{
			; // IL_0090: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t520 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t520_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0095;
		throw e;
	}

CATCH_0095:
	{ // begin catch(System.Exception)
		String_t* L_25 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m2834(NULL /*static, unused*/, _stringLiteral959, L_25, _stringLiteral806, /*hidden argument*/NULL);
		Debug_Log_m2814(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		goto IL_00b0;
	} // end catch (depth: 1)

IL_00b0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
		Encoding_t519 * L_27 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_27;
	}

IL_00b6:
	{
		Encoding_t519 * L_28 = V_5;
		return L_28;
	}
}
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C" ByteU5BU5D_t469* WWW_get_bytes_m5549 (WWW_t304 * __this, const MethodInfo* method)
{
	typedef ByteU5BU5D_t469* (*WWW_get_bytes_m5549_ftn) (WWW_t304 *);
	static WWW_get_bytes_m5549_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_bytes_m5549_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_bytes()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.WWW::get_error()
extern "C" String_t* WWW_get_error_m3105 (WWW_t304 * __this, const MethodInfo* method)
{
	typedef String_t* (*WWW_get_error_m3105_ftn) (WWW_t304 *);
	static WWW_get_error_m3105_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_error_m3105_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_error()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Texture2D UnityEngine.WWW::GetTexture(System.Boolean)
extern "C" Texture2D_t9 * WWW_GetTexture_m5550 (WWW_t304 * __this, bool ___markNonReadable, const MethodInfo* method)
{
	typedef Texture2D_t9 * (*WWW_GetTexture_m5550_ftn) (WWW_t304 *, bool);
	static WWW_GetTexture_m5550_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_GetTexture_m5550_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::GetTexture(System.Boolean)");
	return _il2cpp_icall_func(__this, ___markNonReadable);
}
// UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern "C" Texture2D_t9 * WWW_get_texture_m3106 (WWW_t304 * __this, const MethodInfo* method)
{
	{
		Texture2D_t9 * L_0 = WWW_GetTexture_m5550(__this, 0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C" bool WWW_get_isDone_m5551 (WWW_t304 * __this, const MethodInfo* method)
{
	typedef bool (*WWW_get_isDone_m5551_ftn) (WWW_t304 *);
	static WWW_get_isDone_m5551_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WWW_get_isDone_m5551_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WWW::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.String[] UnityEngine.WWW::FlattenedHeadersFrom(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern TypeInfo* StringU5BU5D_t398_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t1119_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m6384_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m6385_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m6386_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m6387_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6388_MethodInfo_var;
extern "C" StringU5BU5D_t398* WWW_FlattenedHeadersFrom_m5552 (Object_t * __this /* static, unused */, Dictionary_2_t1083 * ___headers, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringU5BU5D_t398_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(252);
		Enumerator_t1119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(662);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		Dictionary_2_GetEnumerator_m6384_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484139);
		Enumerator_get_Current_m6385_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484140);
		KeyValuePair_2_get_Key_m6386_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484141);
		KeyValuePair_2_get_Value_m6387_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484142);
		Enumerator_MoveNext_m6388_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484143);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t398* V_0 = {0};
	int32_t V_1 = 0;
	KeyValuePair_2_t1118  V_2 = {0};
	Enumerator_t1119  V_3 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1083 * L_0 = ___headers;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (StringU5BU5D_t398*)NULL;
	}

IL_0008:
	{
		Dictionary_2_t1083 * L_1 = ___headers;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_1);
		V_0 = ((StringU5BU5D_t398*)SZArrayNew(StringU5BU5D_t398_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_2*(int32_t)2))));
		V_1 = 0;
		Dictionary_2_t1083 * L_3 = ___headers;
		NullCheck(L_3);
		Enumerator_t1119  L_4 = Dictionary_2_GetEnumerator_m6384(L_3, /*hidden argument*/Dictionary_2_GetEnumerator_m6384_MethodInfo_var);
		V_3 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0052;
		}

IL_0024:
		{
			KeyValuePair_2_t1118  L_5 = Enumerator_get_Current_m6385((&V_3), /*hidden argument*/Enumerator_get_Current_m6385_MethodInfo_var);
			V_2 = L_5;
			StringU5BU5D_t398* L_6 = V_0;
			int32_t L_7 = V_1;
			int32_t L_8 = L_7;
			V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
			String_t* L_9 = KeyValuePair_2_get_Key_m6386((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m6386_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = String_ToString_m3177(L_9, /*hidden argument*/NULL);
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
			ArrayElementTypeCheck (L_6, L_10);
			*((String_t**)(String_t**)SZArrayLdElema(L_6, L_8)) = (String_t*)L_10;
			StringU5BU5D_t398* L_11 = V_0;
			int32_t L_12 = V_1;
			int32_t L_13 = L_12;
			V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
			String_t* L_14 = KeyValuePair_2_get_Value_m6387((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m6387_MethodInfo_var);
			NullCheck(L_14);
			String_t* L_15 = String_ToString_m3177(L_14, /*hidden argument*/NULL);
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_13);
			ArrayElementTypeCheck (L_11, L_15);
			*((String_t**)(String_t**)SZArrayLdElema(L_11, L_13)) = (String_t*)L_15;
		}

IL_0052:
		{
			bool L_16 = Enumerator_MoveNext_m6388((&V_3), /*hidden argument*/Enumerator_MoveNext_m6388_MethodInfo_var);
			if (L_16)
			{
				goto IL_0024;
			}
		}

IL_005e:
		{
			IL2CPP_LEAVE(0x6F, FINALLY_0063);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0063;
	}

FINALLY_0063:
	{ // begin finally (depth: 1)
		Enumerator_t1119  L_17 = V_3;
		Enumerator_t1119  L_18 = L_17;
		Object_t * L_19 = Box(Enumerator_t1119_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_19);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_19);
		IL2CPP_END_FINALLY(99)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(99)
	{
		IL2CPP_JUMP_TBL(0x6F, IL_006f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_006f:
	{
		StringU5BU5D_t398* L_20 = V_0;
		return L_20;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::ParseHTTPHeaderString(System.String)
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* StringReader_t1120_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m6389_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral960;
extern Il2CppCodeGenString* _stringLiteral961;
extern Il2CppCodeGenString* _stringLiteral962;
extern Il2CppCodeGenString* _stringLiteral963;
extern "C" Dictionary_2_t1083 * WWW_ParseHTTPHeaderString_m5553 (Object_t * __this /* static, unused */, String_t* ___input, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		Dictionary_2_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(663);
		StringReader_t1120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(664);
		Dictionary_2__ctor_m6389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484144);
		_stringLiteral960 = il2cpp_codegen_string_literal_from_index(960);
		_stringLiteral961 = il2cpp_codegen_string_literal_from_index(961);
		_stringLiteral962 = il2cpp_codegen_string_literal_from_index(962);
		_stringLiteral963 = il2cpp_codegen_string_literal_from_index(963);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1083 * V_0 = {0};
	StringReader_t1120 * V_1 = {0};
	int32_t V_2 = 0;
	String_t* V_3 = {0};
	int32_t V_4 = 0;
	String_t* V_5 = {0};
	String_t* V_6 = {0};
	{
		String_t* L_0 = ___input;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t556 * L_1 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_1, _stringLiteral960, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		Dictionary_2_t1083 * L_2 = (Dictionary_2_t1083 *)il2cpp_codegen_object_new (Dictionary_2_t1083_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m6389(L_2, /*hidden argument*/Dictionary_2__ctor_m6389_MethodInfo_var);
		V_0 = L_2;
		String_t* L_3 = ___input;
		StringReader_t1120 * L_4 = (StringReader_t1120 *)il2cpp_codegen_object_new (StringReader_t1120_il2cpp_TypeInfo_var);
		StringReader__ctor_m6390(L_4, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
	}

IL_0020:
	{
		StringReader_t1120 * L_5 = V_1;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(9 /* System.String System.IO.StringReader::ReadLine() */, L_5);
		V_3 = L_6;
		String_t* L_7 = V_3;
		if (L_7)
		{
			goto IL_0032;
		}
	}
	{
		goto IL_00a2;
	}

IL_0032:
	{
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		if (L_9)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_10 = V_3;
		NullCheck(L_10);
		bool L_11 = String_StartsWith_m3159(L_10, _stringLiteral961, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005d;
		}
	}
	{
		Dictionary_2_t1083 * L_12 = V_0;
		String_t* L_13 = V_3;
		NullCheck(L_12);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_12, _stringLiteral962, L_13);
		goto IL_0020;
	}

IL_005d:
	{
		String_t* L_14 = V_3;
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m6391(L_14, _stringLiteral963, /*hidden argument*/NULL);
		V_4 = L_15;
		int32_t L_16 = V_4;
		if ((!(((uint32_t)L_16) == ((uint32_t)(-1)))))
		{
			goto IL_0077;
		}
	}
	{
		goto IL_0020;
	}

IL_0077:
	{
		String_t* L_17 = V_3;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		String_t* L_19 = String_Substring_m3341(L_17, 0, L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		String_t* L_20 = String_ToUpper_m6392(L_19, /*hidden argument*/NULL);
		V_5 = L_20;
		String_t* L_21 = V_3;
		int32_t L_22 = V_4;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m4812(L_21, ((int32_t)((int32_t)L_22+(int32_t)2)), /*hidden argument*/NULL);
		V_6 = L_23;
		Dictionary_2_t1083 * L_24 = V_0;
		String_t* L_25 = V_5;
		String_t* L_26 = V_6;
		NullCheck(L_24);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_24, L_25, L_26);
		goto IL_0020;
	}

IL_00a2:
	{
		Dictionary_2_t1083 * L_27 = V_0;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStream.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.Collections.Generic.List`1<System.Byte[]>
#include "mscorlib_System_Collections_Generic_List_1_gen_33.h"
// System.IO.MemoryStream
#include "mscorlib_System_IO_MemoryStreamMethodDeclarations.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// UnityEngine.WWWTranscoder
#include "UnityEngine_UnityEngine_WWWTranscoderMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Byte[]>
#include "mscorlib_System_Collections_Generic_List_1_gen_33MethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"


// System.Void UnityEngine.WWWForm::.ctor()
extern TypeInfo* List_1_t907_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t468_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t469_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m6393_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2831_MethodInfo_var;
extern "C" void WWWForm__ctor_m5554 (WWWForm_t908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t907_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(666);
		List_1_t468_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(177);
		ByteU5BU5D_t469_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(665);
		List_1__ctor_m6393_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484145);
		List_1__ctor_m2831_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		List_1_t907 * L_0 = (List_1_t907 *)il2cpp_codegen_object_new (List_1_t907_il2cpp_TypeInfo_var);
		List_1__ctor_m6393(L_0, /*hidden argument*/List_1__ctor_m6393_MethodInfo_var);
		__this->___formData_0 = L_0;
		List_1_t468 * L_1 = (List_1_t468 *)il2cpp_codegen_object_new (List_1_t468_il2cpp_TypeInfo_var);
		List_1__ctor_m2831(L_1, /*hidden argument*/List_1__ctor_m2831_MethodInfo_var);
		__this->___fieldNames_1 = L_1;
		List_1_t468 * L_2 = (List_1_t468 *)il2cpp_codegen_object_new (List_1_t468_il2cpp_TypeInfo_var);
		List_1__ctor_m2831(L_2, /*hidden argument*/List_1__ctor_m2831_MethodInfo_var);
		__this->___fileNames_2 = L_2;
		List_1_t468 * L_3 = (List_1_t468 *)il2cpp_codegen_object_new (List_1_t468_il2cpp_TypeInfo_var);
		List_1__ctor_m2831(L_3, /*hidden argument*/List_1__ctor_m2831_MethodInfo_var);
		__this->___types_3 = L_3;
		__this->___boundary_4 = ((ByteU5BU5D_t469*)SZArrayNew(ByteU5BU5D_t469_il2cpp_TypeInfo_var, ((int32_t)40)));
		V_0 = 0;
		goto IL_0076;
	}

IL_0046:
	{
		int32_t L_4 = Random_Range_m3349(NULL /*static, unused*/, ((int32_t)48), ((int32_t)110), /*hidden argument*/NULL);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)57))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)7));
	}

IL_005c:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)90))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)6));
	}

IL_0068:
	{
		ByteU5BU5D_t469* L_9 = (__this->___boundary_4);
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_9, L_10)) = (uint8_t)(((uint8_t)L_11));
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0076:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)((int32_t)40))))
		{
			goto IL_0046;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern TypeInfo* Encoding_t519_il2cpp_TypeInfo_var;
extern "C" void WWWForm_AddField_m5555 (WWWForm_t908 * __this, String_t* ___fieldName, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Encoding_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(225);
		s_Il2CppMethodIntialized = true;
	}
	Encoding_t519 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
		Encoding_t519 * L_0 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = ___fieldName;
		String_t* L_2 = ___value;
		Encoding_t519 * L_3 = V_0;
		WWWForm_AddField_m5556(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String,System.Text.Encoding)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral964;
extern Il2CppCodeGenString* _stringLiteral965;
extern "C" void WWWForm_AddField_m5556 (WWWForm_t908 * __this, String_t* ___fieldName, String_t* ___value, Encoding_t519 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral964 = il2cpp_codegen_string_literal_from_index(964);
		_stringLiteral965 = il2cpp_codegen_string_literal_from_index(965);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t468 * L_0 = (__this->___fieldNames_1);
		String_t* L_1 = ___fieldName;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_0, L_1);
		List_1_t468 * L_2 = (__this->___fileNames_2);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_2, (String_t*)NULL);
		List_1_t907 * L_3 = (__this->___formData_0);
		Encoding_t519 * L_4 = ___e;
		String_t* L_5 = ___value;
		NullCheck(L_4);
		ByteU5BU5D_t469* L_6 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, L_5);
		NullCheck(L_3);
		VirtActionInvoker1< ByteU5BU5D_t469* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Byte[]>::Add(!0) */, L_3, L_6);
		List_1_t468 * L_7 = (__this->___types_3);
		Encoding_t519 * L_8 = ___e;
		NullCheck(L_8);
		String_t* L_9 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(22 /* System.String System.Text.Encoding::get_WebName() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2834(NULL /*static, unused*/, _stringLiteral964, L_9, _stringLiteral965, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_7, L_10);
		return;
	}
}
// System.Void UnityEngine.WWWForm::AddField(System.String,System.Int32)
extern "C" void WWWForm_AddField_m5557 (WWWForm_t908 * __this, String_t* ___fieldName, int32_t ___i, const MethodInfo* method)
{
	{
		String_t* L_0 = ___fieldName;
		String_t* L_1 = Int32_ToString_m3230((&___i), /*hidden argument*/NULL);
		WWWForm_AddField_m5555(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWWForm::get_headers()
extern TypeInfo* Dictionary_2_t1083_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t519_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m6389_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral966;
extern Il2CppCodeGenString* _stringLiteral967;
extern Il2CppCodeGenString* _stringLiteral965;
extern Il2CppCodeGenString* _stringLiteral968;
extern "C" Dictionary_2_t1083 * WWWForm_get_headers_m5558 (WWWForm_t908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t1083_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(663);
		Encoding_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(225);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		Dictionary_2__ctor_m6389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484144);
		_stringLiteral966 = il2cpp_codegen_string_literal_from_index(966);
		_stringLiteral967 = il2cpp_codegen_string_literal_from_index(967);
		_stringLiteral965 = il2cpp_codegen_string_literal_from_index(965);
		_stringLiteral968 = il2cpp_codegen_string_literal_from_index(968);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t1083 * V_0 = {0};
	{
		Dictionary_2_t1083 * L_0 = (Dictionary_2_t1083 *)il2cpp_codegen_object_new (Dictionary_2_t1083_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m6389(L_0, /*hidden argument*/Dictionary_2__ctor_m6389_MethodInfo_var);
		V_0 = L_0;
		bool L_1 = (__this->___containsFiles_5);
		if (!L_1)
		{
			goto IL_0049;
		}
	}
	{
		Dictionary_2_t1083 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
		Encoding_t519 * L_3 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t469* L_4 = (__this->___boundary_4);
		ByteU5BU5D_t469* L_5 = (__this->___boundary_4);
		NullCheck(L_5);
		NullCheck(L_3);
		String_t* L_6 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(19 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_3, L_4, 0, (((int32_t)(((Array_t *)L_5)->max_length))));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2834(NULL /*static, unused*/, _stringLiteral967, L_6, _stringLiteral965, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_2, _stringLiteral966, L_7);
		goto IL_0059;
	}

IL_0049:
	{
		Dictionary_2_t1083 * L_8 = V_0;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_8, _stringLiteral966, _stringLiteral968);
	}

IL_0059:
	{
		Dictionary_2_t1083 * L_9 = V_0;
		return L_9;
	}
}
// System.Byte[] UnityEngine.WWWForm::get_data()
extern TypeInfo* MemoryStream_t1121_il2cpp_TypeInfo_var;
extern TypeInfo* Encoding_t519_il2cpp_TypeInfo_var;
extern TypeInfo* WWWTranscoder_t909_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t398_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral969;
extern Il2CppCodeGenString* _stringLiteral970;
extern Il2CppCodeGenString* _stringLiteral971;
extern Il2CppCodeGenString* _stringLiteral972;
extern Il2CppCodeGenString* _stringLiteral965;
extern Il2CppCodeGenString* _stringLiteral973;
extern Il2CppCodeGenString* _stringLiteral974;
extern Il2CppCodeGenString* _stringLiteral975;
extern Il2CppCodeGenString* _stringLiteral976;
extern Il2CppCodeGenString* _stringLiteral977;
extern Il2CppCodeGenString* _stringLiteral978;
extern "C" ByteU5BU5D_t469* WWWForm_get_data_m5559 (WWWForm_t908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t1121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		Encoding_t519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(225);
		WWWTranscoder_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		StringU5BU5D_t398_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(252);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		_stringLiteral969 = il2cpp_codegen_string_literal_from_index(969);
		_stringLiteral970 = il2cpp_codegen_string_literal_from_index(970);
		_stringLiteral971 = il2cpp_codegen_string_literal_from_index(971);
		_stringLiteral972 = il2cpp_codegen_string_literal_from_index(972);
		_stringLiteral965 = il2cpp_codegen_string_literal_from_index(965);
		_stringLiteral973 = il2cpp_codegen_string_literal_from_index(973);
		_stringLiteral974 = il2cpp_codegen_string_literal_from_index(974);
		_stringLiteral975 = il2cpp_codegen_string_literal_from_index(975);
		_stringLiteral976 = il2cpp_codegen_string_literal_from_index(976);
		_stringLiteral977 = il2cpp_codegen_string_literal_from_index(977);
		_stringLiteral978 = il2cpp_codegen_string_literal_from_index(978);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t469* V_0 = {0};
	ByteU5BU5D_t469* V_1 = {0};
	ByteU5BU5D_t469* V_2 = {0};
	ByteU5BU5D_t469* V_3 = {0};
	ByteU5BU5D_t469* V_4 = {0};
	ByteU5BU5D_t469* V_5 = {0};
	MemoryStream_t1121 * V_6 = {0};
	int32_t V_7 = 0;
	ByteU5BU5D_t469* V_8 = {0};
	String_t* V_9 = {0};
	String_t* V_10 = {0};
	ByteU5BU5D_t469* V_11 = {0};
	String_t* V_12 = {0};
	ByteU5BU5D_t469* V_13 = {0};
	ByteU5BU5D_t469* V_14 = {0};
	ByteU5BU5D_t469* V_15 = {0};
	ByteU5BU5D_t469* V_16 = {0};
	MemoryStream_t1121 * V_17 = {0};
	int32_t V_18 = 0;
	ByteU5BU5D_t469* V_19 = {0};
	ByteU5BU5D_t469* V_20 = {0};
	ByteU5BU5D_t469* V_21 = {0};
	ByteU5BU5D_t469* V_22 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___containsFiles_5);
		if (!L_0)
		{
			goto IL_0311;
		}
	}
	{
		Encoding_t519 * L_1 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_t469* L_2 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_1, _stringLiteral969);
		V_0 = L_2;
		Encoding_t519 * L_3 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		ByteU5BU5D_t469* L_4 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_3, _stringLiteral970);
		V_1 = L_4;
		Encoding_t519 * L_5 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		ByteU5BU5D_t469* L_6 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_5, _stringLiteral971);
		V_2 = L_6;
		Encoding_t519 * L_7 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		ByteU5BU5D_t469* L_8 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_7, _stringLiteral972);
		V_3 = L_8;
		Encoding_t519 * L_9 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		ByteU5BU5D_t469* L_10 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_9, _stringLiteral965);
		V_4 = L_10;
		Encoding_t519 * L_11 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		ByteU5BU5D_t469* L_12 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_11, _stringLiteral973);
		V_5 = L_12;
		MemoryStream_t1121 * L_13 = (MemoryStream_t1121 *)il2cpp_codegen_object_new (MemoryStream_t1121_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m6394(L_13, ((int32_t)1024), /*hidden argument*/NULL);
		V_6 = L_13;
	}

IL_0079:
	try
	{ // begin try (depth: 1)
		{
			V_7 = 0;
			goto IL_0297;
		}

IL_0081:
		{
			MemoryStream_t1121 * L_14 = V_6;
			ByteU5BU5D_t469* L_15 = V_1;
			ByteU5BU5D_t469* L_16 = V_1;
			NullCheck(L_16);
			NullCheck(L_14);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_14, L_15, 0, (((int32_t)(((Array_t *)L_16)->max_length))));
			MemoryStream_t1121 * L_17 = V_6;
			ByteU5BU5D_t469* L_18 = V_0;
			ByteU5BU5D_t469* L_19 = V_0;
			NullCheck(L_19);
			NullCheck(L_17);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_17, L_18, 0, (((int32_t)(((Array_t *)L_19)->max_length))));
			MemoryStream_t1121 * L_20 = V_6;
			ByteU5BU5D_t469* L_21 = (__this->___boundary_4);
			ByteU5BU5D_t469* L_22 = (__this->___boundary_4);
			NullCheck(L_22);
			NullCheck(L_20);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_20, L_21, 0, (((int32_t)(((Array_t *)L_22)->max_length))));
			MemoryStream_t1121 * L_23 = V_6;
			ByteU5BU5D_t469* L_24 = V_1;
			ByteU5BU5D_t469* L_25 = V_1;
			NullCheck(L_25);
			NullCheck(L_23);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_23, L_24, 0, (((int32_t)(((Array_t *)L_25)->max_length))));
			MemoryStream_t1121 * L_26 = V_6;
			ByteU5BU5D_t469* L_27 = V_2;
			ByteU5BU5D_t469* L_28 = V_2;
			NullCheck(L_28);
			NullCheck(L_26);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_26, L_27, 0, (((int32_t)(((Array_t *)L_28)->max_length))));
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_29 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t468 * L_30 = (__this->___types_3);
			int32_t L_31 = V_7;
			NullCheck(L_30);
			String_t* L_32 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_30, L_31);
			NullCheck(L_29);
			ByteU5BU5D_t469* L_33 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_29, L_32);
			V_8 = L_33;
			MemoryStream_t1121 * L_34 = V_6;
			ByteU5BU5D_t469* L_35 = V_8;
			ByteU5BU5D_t469* L_36 = V_8;
			NullCheck(L_36);
			NullCheck(L_34);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_34, L_35, 0, (((int32_t)(((Array_t *)L_36)->max_length))));
			MemoryStream_t1121 * L_37 = V_6;
			ByteU5BU5D_t469* L_38 = V_1;
			ByteU5BU5D_t469* L_39 = V_1;
			NullCheck(L_39);
			NullCheck(L_37);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_37, L_38, 0, (((int32_t)(((Array_t *)L_39)->max_length))));
			MemoryStream_t1121 * L_40 = V_6;
			ByteU5BU5D_t469* L_41 = V_3;
			ByteU5BU5D_t469* L_42 = V_3;
			NullCheck(L_42);
			NullCheck(L_40);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_40, L_41, 0, (((int32_t)(((Array_t *)L_42)->max_length))));
			Encoding_t519 * L_43 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			NullCheck(L_43);
			String_t* L_44 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(21 /* System.String System.Text.Encoding::get_HeaderName() */, L_43);
			V_9 = L_44;
			List_1_t468 * L_45 = (__this->___fieldNames_1);
			int32_t L_46 = V_7;
			NullCheck(L_45);
			String_t* L_47 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_45, L_46);
			V_10 = L_47;
			String_t* L_48 = V_10;
			Encoding_t519 * L_49 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			bool L_50 = WWWTranscoder_SevenBitClean_m5566(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
			if (!L_50)
			{
				goto IL_0144;
			}
		}

IL_0132:
		{
			String_t* L_51 = V_10;
			NullCheck(L_51);
			int32_t L_52 = String_IndexOf_m6391(L_51, _stringLiteral974, /*hidden argument*/NULL);
			if ((((int32_t)L_52) <= ((int32_t)(-1))))
			{
				goto IL_017d;
			}
		}

IL_0144:
		{
			StringU5BU5D_t398* L_53 = ((StringU5BU5D_t398*)SZArrayNew(StringU5BU5D_t398_il2cpp_TypeInfo_var, 5));
			NullCheck(L_53);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
			ArrayElementTypeCheck (L_53, _stringLiteral974);
			*((String_t**)(String_t**)SZArrayLdElema(L_53, 0)) = (String_t*)_stringLiteral974;
			StringU5BU5D_t398* L_54 = L_53;
			String_t* L_55 = V_9;
			NullCheck(L_54);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 1);
			ArrayElementTypeCheck (L_54, L_55);
			*((String_t**)(String_t**)SZArrayLdElema(L_54, 1)) = (String_t*)L_55;
			StringU5BU5D_t398* L_56 = L_54;
			NullCheck(L_56);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 2);
			ArrayElementTypeCheck (L_56, _stringLiteral975);
			*((String_t**)(String_t**)SZArrayLdElema(L_56, 2)) = (String_t*)_stringLiteral975;
			StringU5BU5D_t398* L_57 = L_56;
			String_t* L_58 = V_10;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_59 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			String_t* L_60 = WWWTranscoder_QPEncode_m5563(NULL /*static, unused*/, L_58, L_59, /*hidden argument*/NULL);
			NullCheck(L_57);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 3);
			ArrayElementTypeCheck (L_57, L_60);
			*((String_t**)(String_t**)SZArrayLdElema(L_57, 3)) = (String_t*)L_60;
			StringU5BU5D_t398* L_61 = L_57;
			NullCheck(L_61);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 4);
			ArrayElementTypeCheck (L_61, _stringLiteral976);
			*((String_t**)(String_t**)SZArrayLdElema(L_61, 4)) = (String_t*)_stringLiteral976;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_62 = String_Concat_m3387(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
			V_10 = L_62;
		}

IL_017d:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_63 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_64 = V_10;
			NullCheck(L_63);
			ByteU5BU5D_t469* L_65 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_63, L_64);
			V_11 = L_65;
			MemoryStream_t1121 * L_66 = V_6;
			ByteU5BU5D_t469* L_67 = V_11;
			ByteU5BU5D_t469* L_68 = V_11;
			NullCheck(L_68);
			NullCheck(L_66);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_66, L_67, 0, (((int32_t)(((Array_t *)L_68)->max_length))));
			MemoryStream_t1121 * L_69 = V_6;
			ByteU5BU5D_t469* L_70 = V_4;
			ByteU5BU5D_t469* L_71 = V_4;
			NullCheck(L_71);
			NullCheck(L_69);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_69, L_70, 0, (((int32_t)(((Array_t *)L_71)->max_length))));
			List_1_t468 * L_72 = (__this->___fileNames_2);
			int32_t L_73 = V_7;
			NullCheck(L_72);
			String_t* L_74 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_72, L_73);
			if (!L_74)
			{
				goto IL_025c;
			}
		}

IL_01b9:
		{
			List_1_t468 * L_75 = (__this->___fileNames_2);
			int32_t L_76 = V_7;
			NullCheck(L_75);
			String_t* L_77 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_75, L_76);
			V_12 = L_77;
			String_t* L_78 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_79 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			bool L_80 = WWWTranscoder_SevenBitClean_m5566(NULL /*static, unused*/, L_78, L_79, /*hidden argument*/NULL);
			if (!L_80)
			{
				goto IL_01eb;
			}
		}

IL_01d9:
		{
			String_t* L_81 = V_12;
			NullCheck(L_81);
			int32_t L_82 = String_IndexOf_m6391(L_81, _stringLiteral974, /*hidden argument*/NULL);
			if ((((int32_t)L_82) <= ((int32_t)(-1))))
			{
				goto IL_0224;
			}
		}

IL_01eb:
		{
			StringU5BU5D_t398* L_83 = ((StringU5BU5D_t398*)SZArrayNew(StringU5BU5D_t398_il2cpp_TypeInfo_var, 5));
			NullCheck(L_83);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 0);
			ArrayElementTypeCheck (L_83, _stringLiteral974);
			*((String_t**)(String_t**)SZArrayLdElema(L_83, 0)) = (String_t*)_stringLiteral974;
			StringU5BU5D_t398* L_84 = L_83;
			String_t* L_85 = V_9;
			NullCheck(L_84);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_84, 1);
			ArrayElementTypeCheck (L_84, L_85);
			*((String_t**)(String_t**)SZArrayLdElema(L_84, 1)) = (String_t*)L_85;
			StringU5BU5D_t398* L_86 = L_84;
			NullCheck(L_86);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_86, 2);
			ArrayElementTypeCheck (L_86, _stringLiteral975);
			*((String_t**)(String_t**)SZArrayLdElema(L_86, 2)) = (String_t*)_stringLiteral975;
			StringU5BU5D_t398* L_87 = L_86;
			String_t* L_88 = V_12;
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_89 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			String_t* L_90 = WWWTranscoder_QPEncode_m5563(NULL /*static, unused*/, L_88, L_89, /*hidden argument*/NULL);
			NullCheck(L_87);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 3);
			ArrayElementTypeCheck (L_87, L_90);
			*((String_t**)(String_t**)SZArrayLdElema(L_87, 3)) = (String_t*)L_90;
			StringU5BU5D_t398* L_91 = L_87;
			NullCheck(L_91);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 4);
			ArrayElementTypeCheck (L_91, _stringLiteral976);
			*((String_t**)(String_t**)SZArrayLdElema(L_91, 4)) = (String_t*)_stringLiteral976;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_92 = String_Concat_m3387(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
			V_12 = L_92;
		}

IL_0224:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_93 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			String_t* L_94 = V_12;
			NullCheck(L_93);
			ByteU5BU5D_t469* L_95 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_93, L_94);
			V_13 = L_95;
			MemoryStream_t1121 * L_96 = V_6;
			ByteU5BU5D_t469* L_97 = V_5;
			ByteU5BU5D_t469* L_98 = V_5;
			NullCheck(L_98);
			NullCheck(L_96);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_96, L_97, 0, (((int32_t)(((Array_t *)L_98)->max_length))));
			MemoryStream_t1121 * L_99 = V_6;
			ByteU5BU5D_t469* L_100 = V_13;
			ByteU5BU5D_t469* L_101 = V_13;
			NullCheck(L_101);
			NullCheck(L_99);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_99, L_100, 0, (((int32_t)(((Array_t *)L_101)->max_length))));
			MemoryStream_t1121 * L_102 = V_6;
			ByteU5BU5D_t469* L_103 = V_4;
			ByteU5BU5D_t469* L_104 = V_4;
			NullCheck(L_104);
			NullCheck(L_102);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_102, L_103, 0, (((int32_t)(((Array_t *)L_104)->max_length))));
		}

IL_025c:
		{
			MemoryStream_t1121 * L_105 = V_6;
			ByteU5BU5D_t469* L_106 = V_1;
			ByteU5BU5D_t469* L_107 = V_1;
			NullCheck(L_107);
			NullCheck(L_105);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_105, L_106, 0, (((int32_t)(((Array_t *)L_107)->max_length))));
			MemoryStream_t1121 * L_108 = V_6;
			ByteU5BU5D_t469* L_109 = V_1;
			ByteU5BU5D_t469* L_110 = V_1;
			NullCheck(L_110);
			NullCheck(L_108);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_108, L_109, 0, (((int32_t)(((Array_t *)L_110)->max_length))));
			List_1_t907 * L_111 = (__this->___formData_0);
			int32_t L_112 = V_7;
			NullCheck(L_111);
			ByteU5BU5D_t469* L_113 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32) */, L_111, L_112);
			V_14 = L_113;
			MemoryStream_t1121 * L_114 = V_6;
			ByteU5BU5D_t469* L_115 = V_14;
			ByteU5BU5D_t469* L_116 = V_14;
			NullCheck(L_116);
			NullCheck(L_114);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_114, L_115, 0, (((int32_t)(((Array_t *)L_116)->max_length))));
			int32_t L_117 = V_7;
			V_7 = ((int32_t)((int32_t)L_117+(int32_t)1));
		}

IL_0297:
		{
			int32_t L_118 = V_7;
			List_1_t907 * L_119 = (__this->___formData_0);
			NullCheck(L_119);
			int32_t L_120 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count() */, L_119);
			if ((((int32_t)L_118) < ((int32_t)L_120)))
			{
				goto IL_0081;
			}
		}

IL_02a9:
		{
			MemoryStream_t1121 * L_121 = V_6;
			ByteU5BU5D_t469* L_122 = V_1;
			ByteU5BU5D_t469* L_123 = V_1;
			NullCheck(L_123);
			NullCheck(L_121);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_121, L_122, 0, (((int32_t)(((Array_t *)L_123)->max_length))));
			MemoryStream_t1121 * L_124 = V_6;
			ByteU5BU5D_t469* L_125 = V_0;
			ByteU5BU5D_t469* L_126 = V_0;
			NullCheck(L_126);
			NullCheck(L_124);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_124, L_125, 0, (((int32_t)(((Array_t *)L_126)->max_length))));
			MemoryStream_t1121 * L_127 = V_6;
			ByteU5BU5D_t469* L_128 = (__this->___boundary_4);
			ByteU5BU5D_t469* L_129 = (__this->___boundary_4);
			NullCheck(L_129);
			NullCheck(L_127);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_127, L_128, 0, (((int32_t)(((Array_t *)L_129)->max_length))));
			MemoryStream_t1121 * L_130 = V_6;
			ByteU5BU5D_t469* L_131 = V_0;
			ByteU5BU5D_t469* L_132 = V_0;
			NullCheck(L_132);
			NullCheck(L_130);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_130, L_131, 0, (((int32_t)(((Array_t *)L_132)->max_length))));
			MemoryStream_t1121 * L_133 = V_6;
			ByteU5BU5D_t469* L_134 = V_1;
			ByteU5BU5D_t469* L_135 = V_1;
			NullCheck(L_135);
			NullCheck(L_133);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_133, L_134, 0, (((int32_t)(((Array_t *)L_135)->max_length))));
			MemoryStream_t1121 * L_136 = V_6;
			NullCheck(L_136);
			ByteU5BU5D_t469* L_137 = (ByteU5BU5D_t469*)VirtFuncInvoker0< ByteU5BU5D_t469* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_136);
			V_22 = L_137;
			IL2CPP_LEAVE(0x3F7, FINALLY_0302);
		}

IL_02fd:
		{
			; // IL_02fd: leave IL_0311
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0302;
	}

FINALLY_0302:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t1121 * L_138 = V_6;
			if (!L_138)
			{
				goto IL_0310;
			}
		}

IL_0309:
		{
			MemoryStream_t1121 * L_139 = V_6;
			NullCheck(L_139);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_139);
		}

IL_0310:
		{
			IL2CPP_END_FINALLY(770)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(770)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0311:
	{
		Encoding_t519 * L_140 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_140);
		ByteU5BU5D_t469* L_141 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_140, _stringLiteral977);
		V_15 = L_141;
		Encoding_t519 * L_142 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_142);
		ByteU5BU5D_t469* L_143 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_142, _stringLiteral978);
		V_16 = L_143;
		MemoryStream_t1121 * L_144 = (MemoryStream_t1121 *)il2cpp_codegen_object_new (MemoryStream_t1121_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m6394(L_144, ((int32_t)1024), /*hidden argument*/NULL);
		V_17 = L_144;
	}

IL_033f:
	try
	{ // begin try (depth: 1)
		{
			V_18 = 0;
			goto IL_03c3;
		}

IL_0347:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Encoding_t519_il2cpp_TypeInfo_var);
			Encoding_t519 * L_145 = Encoding_get_UTF8_m3037(NULL /*static, unused*/, /*hidden argument*/NULL);
			List_1_t468 * L_146 = (__this->___fieldNames_1);
			int32_t L_147 = V_18;
			NullCheck(L_146);
			String_t* L_148 = (String_t*)VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_146, L_147);
			NullCheck(L_145);
			ByteU5BU5D_t469* L_149 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_145, L_148);
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			ByteU5BU5D_t469* L_150 = WWWTranscoder_URLEncode_m5562(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
			V_19 = L_150;
			List_1_t907 * L_151 = (__this->___formData_0);
			int32_t L_152 = V_18;
			NullCheck(L_151);
			ByteU5BU5D_t469* L_153 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32) */, L_151, L_152);
			V_20 = L_153;
			ByteU5BU5D_t469* L_154 = V_20;
			ByteU5BU5D_t469* L_155 = WWWTranscoder_URLEncode_m5562(NULL /*static, unused*/, L_154, /*hidden argument*/NULL);
			V_21 = L_155;
			int32_t L_156 = V_18;
			if ((((int32_t)L_156) <= ((int32_t)0)))
			{
				goto IL_0393;
			}
		}

IL_0385:
		{
			MemoryStream_t1121 * L_157 = V_17;
			ByteU5BU5D_t469* L_158 = V_15;
			ByteU5BU5D_t469* L_159 = V_15;
			NullCheck(L_159);
			NullCheck(L_157);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_157, L_158, 0, (((int32_t)(((Array_t *)L_159)->max_length))));
		}

IL_0393:
		{
			MemoryStream_t1121 * L_160 = V_17;
			ByteU5BU5D_t469* L_161 = V_19;
			ByteU5BU5D_t469* L_162 = V_19;
			NullCheck(L_162);
			NullCheck(L_160);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_160, L_161, 0, (((int32_t)(((Array_t *)L_162)->max_length))));
			MemoryStream_t1121 * L_163 = V_17;
			ByteU5BU5D_t469* L_164 = V_16;
			ByteU5BU5D_t469* L_165 = V_16;
			NullCheck(L_165);
			NullCheck(L_163);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_163, L_164, 0, (((int32_t)(((Array_t *)L_165)->max_length))));
			MemoryStream_t1121 * L_166 = V_17;
			ByteU5BU5D_t469* L_167 = V_21;
			ByteU5BU5D_t469* L_168 = V_21;
			NullCheck(L_168);
			NullCheck(L_166);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, L_166, L_167, 0, (((int32_t)(((Array_t *)L_168)->max_length))));
			int32_t L_169 = V_18;
			V_18 = ((int32_t)((int32_t)L_169+(int32_t)1));
		}

IL_03c3:
		{
			int32_t L_170 = V_18;
			List_1_t907 * L_171 = (__this->___formData_0);
			NullCheck(L_171);
			int32_t L_172 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count() */, L_171);
			if ((((int32_t)L_170) < ((int32_t)L_172)))
			{
				goto IL_0347;
			}
		}

IL_03d5:
		{
			MemoryStream_t1121 * L_173 = V_17;
			NullCheck(L_173);
			ByteU5BU5D_t469* L_174 = (ByteU5BU5D_t469*)VirtFuncInvoker0< ByteU5BU5D_t469* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_173);
			V_22 = L_174;
			IL2CPP_LEAVE(0x3F7, FINALLY_03e8);
		}

IL_03e3:
		{
			; // IL_03e3: leave IL_03f7
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_03e8;
	}

FINALLY_03e8:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t1121 * L_175 = V_17;
			if (!L_175)
			{
				goto IL_03f6;
			}
		}

IL_03ef:
		{
			MemoryStream_t1121 * L_176 = V_17;
			NullCheck(L_176);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_176);
		}

IL_03f6:
		{
			IL2CPP_END_FINALLY(1000)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1000)
	{
		IL2CPP_JUMP_TBL(0x3F7, IL_03f7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_03f7:
	{
		ByteU5BU5D_t469* L_177 = V_22;
		return L_177;
	}
}
// UnityEngine.WWWTranscoder
#include "UnityEngine_UnityEngine_WWWTranscoder.h"
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.WWWTranscoder::.cctor()
extern TypeInfo* WWWTranscoder_t909_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral979;
extern Il2CppCodeGenString* _stringLiteral980;
extern Il2CppCodeGenString* _stringLiteral981;
extern Il2CppCodeGenString* _stringLiteral982;
extern "C" void WWWTranscoder__cctor_m5560 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		_stringLiteral979 = il2cpp_codegen_string_literal_from_index(979);
		_stringLiteral980 = il2cpp_codegen_string_literal_from_index(980);
		_stringLiteral981 = il2cpp_codegen_string_literal_from_index(981);
		_stringLiteral982 = il2cpp_codegen_string_literal_from_index(982);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t519 * L_0 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ByteU5BU5D_t469* L_1 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, _stringLiteral979);
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___ucHexChars_0 = L_1;
		Encoding_t519 * L_2 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		ByteU5BU5D_t469* L_3 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_2, _stringLiteral980);
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___lcHexChars_1 = L_3;
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___urlEscapeChar_2 = ((int32_t)37);
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___urlSpace_3 = ((int32_t)43);
		Encoding_t519 * L_4 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		ByteU5BU5D_t469* L_5 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_4, _stringLiteral981);
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___urlForbidden_4 = L_5;
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___qpEscapeChar_5 = ((int32_t)61);
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___qpSpace_6 = ((int32_t)95);
		Encoding_t519 * L_6 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		ByteU5BU5D_t469* L_7 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, _stringLiteral982);
		((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___qpForbidden_7 = L_7;
		return;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Byte2Hex(System.Byte,System.Byte[])
extern TypeInfo* ByteU5BU5D_t469_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t469* WWWTranscoder_Byte2Hex_m5561 (Object_t * __this /* static, unused */, uint8_t ___b, ByteU5BU5D_t469* ___hexChars, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t469_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(665);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t469* V_0 = {0};
	{
		V_0 = ((ByteU5BU5D_t469*)SZArrayNew(ByteU5BU5D_t469_il2cpp_TypeInfo_var, 2));
		ByteU5BU5D_t469* L_0 = V_0;
		ByteU5BU5D_t469* L_1 = ___hexChars;
		uint8_t L_2 = ___b;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2>>(int32_t)4)));
		int32_t L_3 = ((int32_t)((int32_t)L_2>>(int32_t)4));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_0, 0)) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_3));
		ByteU5BU5D_t469* L_4 = V_0;
		ByteU5BU5D_t469* L_5 = ___hexChars;
		uint8_t L_6 = ___b;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)L_6&(int32_t)((int32_t)15))));
		int32_t L_7 = ((int32_t)((int32_t)L_6&(int32_t)((int32_t)15)));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		*((uint8_t*)(uint8_t*)SZArrayLdElema(L_4, 1)) = (uint8_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_5, L_7));
		ByteU5BU5D_t469* L_8 = V_0;
		return L_8;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::URLEncode(System.Byte[])
extern TypeInfo* WWWTranscoder_t909_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t469* WWWTranscoder_URLEncode_m5562 (Object_t * __this /* static, unused */, ByteU5BU5D_t469* ___toEncode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t469* L_0 = ___toEncode;
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
		uint8_t L_1 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___urlEscapeChar_2;
		uint8_t L_2 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___urlSpace_3;
		ByteU5BU5D_t469* L_3 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___urlForbidden_4;
		ByteU5BU5D_t469* L_4 = WWWTranscoder_Encode_m5564(NULL /*static, unused*/, L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String UnityEngine.WWWTranscoder::QPEncode(System.String,System.Text.Encoding)
extern TypeInfo* WWWTranscoder_t909_il2cpp_TypeInfo_var;
extern "C" String_t* WWWTranscoder_QPEncode_m5563 (Object_t * __this /* static, unused */, String_t* ___toEncode, Encoding_t519 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t469* V_0 = {0};
	{
		Encoding_t519 * L_0 = ___e;
		String_t* L_1 = ___toEncode;
		NullCheck(L_0);
		ByteU5BU5D_t469* L_2 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
		uint8_t L_3 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___qpEscapeChar_5;
		uint8_t L_4 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___qpSpace_6;
		ByteU5BU5D_t469* L_5 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___qpForbidden_7;
		ByteU5BU5D_t469* L_6 = WWWTranscoder_Encode_m5564(NULL /*static, unused*/, L_2, L_3, L_4, L_5, 1, /*hidden argument*/NULL);
		V_0 = L_6;
		Encoding_t519 * L_7 = WWW_get_DefaultEncoding_m5547(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t469* L_8 = V_0;
		ByteU5BU5D_t469* L_9 = V_0;
		NullCheck(L_9);
		NullCheck(L_7);
		String_t* L_10 = (String_t*)VirtFuncInvoker3< String_t*, ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(19 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)(((Array_t *)L_9)->max_length))));
		return L_10;
	}
}
// System.Byte[] UnityEngine.WWWTranscoder::Encode(System.Byte[],System.Byte,System.Byte,System.Byte[],System.Boolean)
extern TypeInfo* MemoryStream_t1121_il2cpp_TypeInfo_var;
extern TypeInfo* WWWTranscoder_t909_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern "C" ByteU5BU5D_t469* WWWTranscoder_Encode_m5564 (Object_t * __this /* static, unused */, ByteU5BU5D_t469* ___input, uint8_t ___escapeChar, uint8_t ___space, ByteU5BU5D_t469* ___forbidden, bool ___uppercase, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemoryStream_t1121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(667);
		WWWTranscoder_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		s_Il2CppMethodIntialized = true;
	}
	MemoryStream_t1121 * V_0 = {0};
	int32_t V_1 = 0;
	ByteU5BU5D_t469* V_2 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B9_0 = 0;
	MemoryStream_t1121 * G_B9_1 = {0};
	int32_t G_B8_0 = 0;
	MemoryStream_t1121 * G_B8_1 = {0};
	ByteU5BU5D_t469* G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	MemoryStream_t1121 * G_B10_2 = {0};
	{
		ByteU5BU5D_t469* L_0 = ___input;
		NullCheck(L_0);
		MemoryStream_t1121 * L_1 = (MemoryStream_t1121 *)il2cpp_codegen_object_new (MemoryStream_t1121_il2cpp_TypeInfo_var);
		MemoryStream__ctor_m6394(L_1, ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))*(int32_t)2)), /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			V_1 = 0;
			goto IL_0089;
		}

IL_0012:
		{
			ByteU5BU5D_t469* L_2 = ___input;
			int32_t L_3 = V_1;
			NullCheck(L_2);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
			int32_t L_4 = L_3;
			if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_2, L_4))) == ((uint32_t)((int32_t)32)))))
			{
				goto IL_0028;
			}
		}

IL_001c:
		{
			MemoryStream_t1121 * L_5 = V_0;
			uint8_t L_6 = ___space;
			NullCheck(L_5);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_5, L_6);
			goto IL_0085;
		}

IL_0028:
		{
			ByteU5BU5D_t469* L_7 = ___input;
			int32_t L_8 = V_1;
			NullCheck(L_7);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
			int32_t L_9 = L_8;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_7, L_9))) < ((int32_t)((int32_t)32))))
			{
				goto IL_004a;
			}
		}

IL_0032:
		{
			ByteU5BU5D_t469* L_10 = ___input;
			int32_t L_11 = V_1;
			NullCheck(L_10);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
			int32_t L_12 = L_11;
			if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_10, L_12))) > ((int32_t)((int32_t)126))))
			{
				goto IL_004a;
			}
		}

IL_003c:
		{
			ByteU5BU5D_t469* L_13 = ___forbidden;
			ByteU5BU5D_t469* L_14 = ___input;
			int32_t L_15 = V_1;
			NullCheck(L_14);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
			int32_t L_16 = L_15;
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			bool L_17 = WWWTranscoder_ByteArrayContains_m5565(NULL /*static, unused*/, L_13, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_14, L_16)), /*hidden argument*/NULL);
			if (!L_17)
			{
				goto IL_007c;
			}
		}

IL_004a:
		{
			MemoryStream_t1121 * L_18 = V_0;
			uint8_t L_19 = ___escapeChar;
			NullCheck(L_18);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_18, L_19);
			MemoryStream_t1121 * L_20 = V_0;
			ByteU5BU5D_t469* L_21 = ___input;
			int32_t L_22 = V_1;
			NullCheck(L_21);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
			int32_t L_23 = L_22;
			bool L_24 = ___uppercase;
			G_B8_0 = ((int32_t)((*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23))));
			G_B8_1 = L_20;
			if (!L_24)
			{
				G_B9_0 = ((int32_t)((*(uint8_t*)(uint8_t*)SZArrayLdElema(L_21, L_23))));
				G_B9_1 = L_20;
				goto IL_0066;
			}
		}

IL_005c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			ByteU5BU5D_t469* L_25 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___ucHexChars_0;
			G_B10_0 = L_25;
			G_B10_1 = G_B8_0;
			G_B10_2 = G_B8_1;
			goto IL_006b;
		}

IL_0066:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			ByteU5BU5D_t469* L_26 = ((WWWTranscoder_t909_StaticFields*)WWWTranscoder_t909_il2cpp_TypeInfo_var->static_fields)->___lcHexChars_1;
			G_B10_0 = L_26;
			G_B10_1 = G_B9_0;
			G_B10_2 = G_B9_1;
		}

IL_006b:
		{
			IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
			ByteU5BU5D_t469* L_27 = WWWTranscoder_Byte2Hex_m5561(NULL /*static, unused*/, G_B10_1, G_B10_0, /*hidden argument*/NULL);
			NullCheck(G_B10_2);
			VirtActionInvoker3< ByteU5BU5D_t469*, int32_t, int32_t >::Invoke(18 /* System.Void System.IO.MemoryStream::Write(System.Byte[],System.Int32,System.Int32) */, G_B10_2, L_27, 0, 2);
			goto IL_0085;
		}

IL_007c:
		{
			MemoryStream_t1121 * L_28 = V_0;
			ByteU5BU5D_t469* L_29 = ___input;
			int32_t L_30 = V_1;
			NullCheck(L_29);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
			int32_t L_31 = L_30;
			NullCheck(L_28);
			VirtActionInvoker1< uint8_t >::Invoke(19 /* System.Void System.IO.MemoryStream::WriteByte(System.Byte) */, L_28, (*(uint8_t*)(uint8_t*)SZArrayLdElema(L_29, L_31)));
		}

IL_0085:
		{
			int32_t L_32 = V_1;
			V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
		}

IL_0089:
		{
			int32_t L_33 = V_1;
			ByteU5BU5D_t469* L_34 = ___input;
			NullCheck(L_34);
			if ((((int32_t)L_33) < ((int32_t)(((int32_t)(((Array_t *)L_34)->max_length))))))
			{
				goto IL_0012;
			}
		}

IL_0092:
		{
			MemoryStream_t1121 * L_35 = V_0;
			NullCheck(L_35);
			ByteU5BU5D_t469* L_36 = (ByteU5BU5D_t469*)VirtFuncInvoker0< ByteU5BU5D_t469* >::Invoke(25 /* System.Byte[] System.IO.MemoryStream::ToArray() */, L_35);
			V_2 = L_36;
			IL2CPP_LEAVE(0xB0, FINALLY_00a3);
		}

IL_009e:
		{
			; // IL_009e: leave IL_00b0
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_00a3;
	}

FINALLY_00a3:
	{ // begin finally (depth: 1)
		{
			MemoryStream_t1121 * L_37 = V_0;
			if (!L_37)
			{
				goto IL_00af;
			}
		}

IL_00a9:
		{
			MemoryStream_t1121 * L_38 = V_0;
			NullCheck(L_38);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_38);
		}

IL_00af:
		{
			IL2CPP_END_FINALLY(163)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(163)
	{
		IL2CPP_JUMP_TBL(0xB0, IL_00b0)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_00b0:
	{
		ByteU5BU5D_t469* L_39 = V_2;
		return L_39;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::ByteArrayContains(System.Byte[],System.Byte)
extern "C" bool WWWTranscoder_ByteArrayContains_m5565 (Object_t * __this /* static, unused */, ByteU5BU5D_t469* ___array, uint8_t ___b, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t469* L_0 = ___array;
		NullCheck(L_0);
		V_0 = (((int32_t)(((Array_t *)L_0)->max_length)));
		V_1 = 0;
		goto IL_001a;
	}

IL_000b:
	{
		ByteU5BU5D_t469* L_1 = ___array;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		uint8_t L_4 = ___b;
		if ((!(((uint32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_1, L_3))) == ((uint32_t)L_4))))
		{
			goto IL_0016;
		}
	}
	{
		return 1;
	}

IL_0016:
	{
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001a:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.String,System.Text.Encoding)
extern TypeInfo* WWWTranscoder_t909_il2cpp_TypeInfo_var;
extern "C" bool WWWTranscoder_SevenBitClean_m5566 (Object_t * __this /* static, unused */, String_t* ___s, Encoding_t519 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWTranscoder_t909_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(668);
		s_Il2CppMethodIntialized = true;
	}
	{
		Encoding_t519 * L_0 = ___e;
		String_t* L_1 = ___s;
		NullCheck(L_0);
		ByteU5BU5D_t469* L_2 = (ByteU5BU5D_t469*)VirtFuncInvoker1< ByteU5BU5D_t469*, String_t* >::Invoke(9 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(WWWTranscoder_t909_il2cpp_TypeInfo_var);
		bool L_3 = WWWTranscoder_SevenBitClean_m5567(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.WWWTranscoder::SevenBitClean(System.Byte[])
extern "C" bool WWWTranscoder_SevenBitClean_m5567 (Object_t * __this /* static, unused */, ByteU5BU5D_t469* ___input, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0021;
	}

IL_0007:
	{
		ByteU5BU5D_t469* L_0 = ___input;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_0, L_2))) < ((int32_t)((int32_t)32))))
		{
			goto IL_001b;
		}
	}
	{
		ByteU5BU5D_t469* L_3 = ___input;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		if ((((int32_t)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_3, L_5))) <= ((int32_t)((int32_t)126))))
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return 0;
	}

IL_001d:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_7 = V_0;
		ByteU5BU5D_t469* L_8 = ___input;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return 1;
	}
}
// UnityEngine.CacheIndex
#include "UnityEngine_UnityEngine_CacheIndex.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CacheIndex
#include "UnityEngine_UnityEngine_CacheIndexMethodDeclarations.h"



// Conversion methods for marshalling of: UnityEngine.CacheIndex
void CacheIndex_t910_marshal(const CacheIndex_t910& unmarshaled, CacheIndex_t910_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___bytesUsed_1 = unmarshaled.___bytesUsed_1;
	marshaled.___expires_2 = unmarshaled.___expires_2;
}
void CacheIndex_t910_marshal_back(const CacheIndex_t910_marshaled& marshaled, CacheIndex_t910& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___bytesUsed_1 = marshaled.___bytesUsed_1;
	unmarshaled.___expires_2 = marshaled.___expires_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.CacheIndex
void CacheIndex_t910_marshal_cleanup(CacheIndex_t910_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityString.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UnityString
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"



// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* UnityString_Format_m5568 (Object_t * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t470* ___args, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___fmt;
		ObjectU5BU5D_t470* L_1 = ___args;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m4917(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperation.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AsyncOperation
#include "UnityEngine_UnityEngine_AsyncOperationMethodDeclarations.h"

// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"


// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C" void AsyncOperation__ctor_m5569 (AsyncOperation_t829 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m5711(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C" void AsyncOperation_InternalDestroy_m5570 (AsyncOperation_t829 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m5570_ftn) (AsyncOperation_t829 *);
	static AsyncOperation_InternalDestroy_m5570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m5570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C" void AsyncOperation_Finalize_m5571 (AsyncOperation_t829 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m5570(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
void AsyncOperation_t829_marshal(const AsyncOperation_t829& unmarshaled, AsyncOperation_t829_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
void AsyncOperation_t829_marshal_back(const AsyncOperation_t829_marshaled& marshaled, AsyncOperation_t829& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
void AsyncOperation_t829_marshal_cleanup(AsyncOperation_t829_marshaled& marshaled)
{
}
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Application/LogCallback
#include "UnityEngine_UnityEngine_Application_LogCallbackMethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C" void LogCallback__ctor_m5572 (LogCallback_t912 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C" void LogCallback_Invoke_m5573 (LogCallback_t912 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		LogCallback_Invoke_m5573((LogCallback_t912 *)__this->___prev_9,___condition, ___stackTrace, ___type, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, String_t* ___stackTrace, int32_t ___type, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___condition, ___stackTrace, ___type,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_LogCallback_t912(Il2CppObject* delegate, String_t* ___condition, String_t* ___stackTrace, int32_t ___type)
{
	typedef void (STDCALL *native_function_ptr_type)(char*, char*, int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___condition' to native representation
	char* ____condition_marshaled = { 0 };
	____condition_marshaled = il2cpp_codegen_marshal_string(___condition);

	// Marshaling of parameter '___stackTrace' to native representation
	char* ____stackTrace_marshaled = { 0 };
	____stackTrace_marshaled = il2cpp_codegen_marshal_string(___stackTrace);

	// Marshaling of parameter '___type' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(____condition_marshaled, ____stackTrace_marshaled, ___type);

	// Marshaling cleanup of parameter '___condition' native representation
	il2cpp_codegen_marshal_free(____condition_marshaled);
	____condition_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace' native representation
	il2cpp_codegen_marshal_free(____stackTrace_marshaled);
	____stackTrace_marshaled = NULL;

	// Marshaling cleanup of parameter '___type' native representation

}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern TypeInfo* LogType_t834_il2cpp_TypeInfo_var;
extern "C" Object_t * LogCallback_BeginInvoke_m5574 (LogCallback_t912 * __this, String_t* ___condition, String_t* ___stackTrace, int32_t ___type, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LogType_t834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(669);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition;
	__d_args[1] = ___stackTrace;
	__d_args[2] = Box(LogType_t834_il2cpp_TypeInfo_var, &___type);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C" void LogCallback_EndInvoke_m5575 (LogCallback_t912 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Application
#include "UnityEngine_UnityEngine_Application.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"

// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"


// System.Void UnityEngine.Application::Quit()
extern "C" void Application_Quit_m3171 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Application_Quit_m3171_ftn) ();
	static Application_Quit_m3171_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m3171_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.Void UnityEngine.Application::LoadLevel(System.String)
extern "C" void Application_LoadLevel_m3234 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Application_LoadLevelAsync_m5576(NULL /*static, unused*/, L_0, (-1), 0, 1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.Int32)
extern "C" AsyncOperation_t829 * Application_LoadLevelAsync_m3169 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		AsyncOperation_t829 * L_1 = Application_LoadLevelAsync_m5576(NULL /*static, unused*/, (String_t*)NULL, L_0, 0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.AsyncOperation UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C" AsyncOperation_t829 * Application_LoadLevelAsync_m5576 (Object_t * __this /* static, unused */, String_t* ___monoLevelName, int32_t ___index, bool ___additive, bool ___mustCompleteNextFrame, const MethodInfo* method)
{
	typedef AsyncOperation_t829 * (*Application_LoadLevelAsync_m5576_ftn) (String_t*, int32_t, bool, bool);
	static Application_LoadLevelAsync_m5576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_LoadLevelAsync_m5576_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::LoadLevelAsync(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___monoLevelName, ___index, ___additive, ___mustCompleteNextFrame);
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C" bool Application_get_isPlaying_m2732 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m2732_ftn) ();
	static Application_get_isPlaying_m2732_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m2732_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C" bool Application_get_isEditor_m2855 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isEditor_m2855_ftn) ();
	static Application_get_isEditor_m2855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m2855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" int32_t Application_get_platform_m4774 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m4774_ftn) ();
	static Application_get_platform_m4774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m4774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isMobilePlatform()
extern "C" bool Application_get_isMobilePlatform_m3051 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = Application_get_platform_m4774(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)8)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)11))))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)21))))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)22))))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_5 = V_0;
		G_B6_0 = ((((int32_t)L_5) == ((int32_t)((int32_t)23)))? 1 : 0);
		goto IL_002d;
	}

IL_002c:
	{
		G_B6_0 = 1;
	}

IL_002d:
	{
		return G_B6_0;
	}
}
// System.String UnityEngine.Application::get_unityVersion()
extern "C" String_t* Application_get_unityVersion_m3038 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_unityVersion_m3038_ftn) ();
	static Application_get_unityVersion_m3038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_unityVersion_m3038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_unityVersion()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_cloudProjectId()
extern "C" String_t* Application_get_cloudProjectId_m5577 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_cloudProjectId_m5577_ftn) ();
	static Application_get_cloudProjectId_m5577_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_cloudProjectId_m5577_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_cloudProjectId()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C" void Application_OpenURL_m3174 (Object_t * __this /* static, unused */, String_t* ___url, const MethodInfo* method)
{
	typedef void (*Application_OpenURL_m3174_ftn) (String_t*);
	static Application_OpenURL_m3174_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_OpenURL_m3174_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::OpenURL(System.String)");
	_il2cpp_icall_func(___url);
}
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C" void Application_set_targetFrameRate_m2836 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Application_set_targetFrameRate_m2836_ftn) (int32_t);
	static Application_set_targetFrameRate_m2836_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_targetFrameRate_m2836_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_targetFrameRate(System.Int32)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern TypeInfo* Application_t913_il2cpp_TypeInfo_var;
extern "C" void Application_CallLogCallback_m5578 (Object_t * __this /* static, unused */, String_t* ___logString, String_t* ___stackTrace, int32_t ___type, bool ___invokedOnMainThread, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Application_t913_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(670);
		s_Il2CppMethodIntialized = true;
	}
	LogCallback_t912 * V_0 = {0};
	LogCallback_t912 * V_1 = {0};
	{
		bool L_0 = ___invokedOnMainThread;
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t912 * L_1 = ((Application_t913_StaticFields*)Application_t913_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandler_0;
		V_0 = L_1;
		LogCallback_t912 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		LogCallback_t912 * L_3 = V_0;
		String_t* L_4 = ___logString;
		String_t* L_5 = ___stackTrace;
		int32_t L_6 = ___type;
		NullCheck(L_3);
		LogCallback_Invoke_m5573(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001b:
	{
		LogCallback_t912 * L_7 = ((Application_t913_StaticFields*)Application_t913_il2cpp_TypeInfo_var->static_fields)->___s_LogCallbackHandlerThreaded_1;
		V_1 = L_7;
		LogCallback_t912 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0030;
		}
	}
	{
		LogCallback_t912 * L_9 = V_1;
		String_t* L_10 = ___logString;
		String_t* L_11 = ___stackTrace;
		int32_t L_12 = ___type;
		NullCheck(L_9);
		LogCallback_Invoke_m5573(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"

// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"


// System.Void UnityEngine.Behaviour::.ctor()
extern "C" void Behaviour__ctor_m5579 (Behaviour_t825 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m5664(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C" bool Behaviour_get_enabled_m2777 (Behaviour_t825 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m2777_ftn) (Behaviour_t825 *);
	static Behaviour_get_enabled_m2777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m2777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C" void Behaviour_set_enabled_m2721 (Behaviour_t825 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m2721_ftn) (Behaviour_t825 *, bool);
	static Behaviour_set_enabled_m2721_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m2721_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C" bool Behaviour_get_isActiveAndEnabled_m4602 (Behaviour_t825 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m4602_ftn) (Behaviour_t825 *);
	static Behaviour_get_isActiveAndEnabled_m4602_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m4602_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Camera/CameraCallback
#include "UnityEngine_UnityEngine_Camera_CameraCallbackMethodDeclarations.h"

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"


// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C" void CameraCallback__ctor_m5580 (CameraCallback_t914 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C" void CameraCallback_Invoke_m5581 (CameraCallback_t914 * __this, Camera_t14 * ___cam, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CameraCallback_Invoke_m5581((CameraCallback_t914 *)__this->___prev_9,___cam, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Camera_t14 * ___cam, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Camera_t14 * ___cam, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___cam,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t914(Il2CppObject* delegate, Camera_t14 * ___cam)
{
	// Marshaling of parameter '___cam' to native representation
	Camera_t14 * ____cam_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.Camera'."));
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C" Object_t * CameraCallback_BeginInvoke_m5582 (CameraCallback_t914 * __this, Camera_t14 * ___cam, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C" void CameraCallback_EndInvoke_m5583 (CameraCallback_t914 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"


// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C" void Camera_set_fieldOfView_m2870 (Camera_t14 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_fieldOfView_m2870_ftn) (Camera_t14 *, float);
	static Camera_set_fieldOfView_m2870_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_fieldOfView_m2870_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_fieldOfView(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C" float Camera_get_nearClipPlane_m2859 (Camera_t14 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m2859_ftn) (Camera_t14 *);
	static Camera_get_nearClipPlane_m2859_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m2859_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C" float Camera_get_farClipPlane_m2860 (Camera_t14 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m2860_ftn) (Camera_t14 *);
	static Camera_get_farClipPlane_m2860_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m2860_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C" void Camera_set_orthographicSize_m2907 (Camera_t14 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographicSize_m2907_ftn) (Camera_t14 *, float);
	static Camera_set_orthographicSize_m2907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographicSize_m2907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C" void Camera_set_orthographic_m2906 (Camera_t14 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographic_m2906_ftn) (Camera_t14 *, bool);
	static Camera_set_orthographic_m2906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographic_m2906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C" float Camera_get_depth_m2886 (Camera_t14 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_depth_m2886_ftn) (Camera_t14 *);
	static Camera_get_depth_m2886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m2886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_depth(System.Single)
extern "C" void Camera_set_depth_m2887 (Camera_t14 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_depth_m2887_ftn) (Camera_t14 *, float);
	static Camera_set_depth_m2887_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_depth_m2887_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_depth(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C" int32_t Camera_get_cullingMask_m2883 (Camera_t14 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m2883_ftn) (Camera_t14 *);
	static Camera_get_cullingMask_m2883_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m2883_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C" void Camera_set_cullingMask_m2885 (Camera_t14 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_cullingMask_m2885_ftn) (Camera_t14 *, int32_t);
	static Camera_set_cullingMask_m2885_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_cullingMask_m2885_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_cullingMask(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C" int32_t Camera_get_eventMask_m5584 (Camera_t14 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m5584_ftn) (Camera_t14 *);
	static Camera_get_eventMask_m5584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m5584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern "C" void Camera_set_backgroundColor_m2905 (Camera_t14 * __this, Color_t6  ___value, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_backgroundColor_m5585(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
extern "C" void Camera_INTERNAL_set_backgroundColor_m5585 (Camera_t14 * __this, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_backgroundColor_m5585_ftn) (Camera_t14 *, Color_t6 *);
	static Camera_INTERNAL_set_backgroundColor_m5585_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_backgroundColor_m5585_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Rect UnityEngine.Camera::get_rect()
extern "C" Rect_t225  Camera_get_rect_m2857 (Camera_t14 * __this, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		Camera_INTERNAL_get_rect_m5586(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t225  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_rect(UnityEngine.Rect)
extern "C" void Camera_set_rect_m2871 (Camera_t14 * __this, Rect_t225  ___value, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_rect_m5587(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_get_rect_m5586 (Camera_t14 * __this, Rect_t225 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_rect_m5586_ftn) (Camera_t14 *, Rect_t225 *);
	static Camera_INTERNAL_get_rect_m5586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_rect_m5586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_set_rect_m5587 (Camera_t14 * __this, Rect_t225 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_rect_m5587_ftn) (Camera_t14 *, Rect_t225 *);
	static Camera_INTERNAL_set_rect_m5587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_rect_m5587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C" Rect_t225  Camera_get_pixelRect_m3034 (Camera_t14 * __this, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		Camera_INTERNAL_get_pixelRect_m5588(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t225  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C" void Camera_INTERNAL_get_pixelRect_m5588 (Camera_t14 * __this, Rect_t225 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m5588_ftn) (Camera_t14 *, Rect_t225 *);
	static Camera_INTERNAL_get_pixelRect_m5588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m5588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C" RenderTexture_t15 * Camera_get_targetTexture_m2740 (Camera_t14 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t15 * (*Camera_get_targetTexture_m2740_ftn) (Camera_t14 *);
	static Camera_get_targetTexture_m2740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m2740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern "C" void Camera_set_targetTexture_m2742 (Camera_t14 * __this, RenderTexture_t15 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_targetTexture_m2742_ftn) (Camera_t14 *, RenderTexture_t15 *);
	static Camera_set_targetTexture_m2742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_targetTexture_m2742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_cameraToWorldMatrix()
extern "C" Matrix4x4_t242  Camera_get_cameraToWorldMatrix_m2874 (Camera_t14 * __this, const MethodInfo* method)
{
	Matrix4x4_t242  V_0 = {0};
	{
		Camera_INTERNAL_get_cameraToWorldMatrix_m5589(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t242  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_cameraToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C" void Camera_INTERNAL_get_cameraToWorldMatrix_m5589 (Camera_t14 * __this, Matrix4x4_t242 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_cameraToWorldMatrix_m5589_ftn) (Camera_t14 *, Matrix4x4_t242 *);
	static Camera_INTERNAL_get_cameraToWorldMatrix_m5589_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_cameraToWorldMatrix_m5589_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_cameraToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C" Matrix4x4_t242  Camera_get_projectionMatrix_m2868 (Camera_t14 * __this, const MethodInfo* method)
{
	Matrix4x4_t242  V_0 = {0};
	{
		Camera_INTERNAL_get_projectionMatrix_m5590(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t242  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C" void Camera_set_projectionMatrix_m2869 (Camera_t14 * __this, Matrix4x4_t242  ___value, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_projectionMatrix_m5591(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C" void Camera_INTERNAL_get_projectionMatrix_m5590 (Camera_t14 * __this, Matrix4x4_t242 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_projectionMatrix_m5590_ftn) (Camera_t14 *, Matrix4x4_t242 *);
	static Camera_INTERNAL_get_projectionMatrix_m5590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_projectionMatrix_m5590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C" void Camera_INTERNAL_set_projectionMatrix_m5591 (Camera_t14 * __this, Matrix4x4_t242 * ___value, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_projectionMatrix_m5591_ftn) (Camera_t14 *, Matrix4x4_t242 *);
	static Camera_INTERNAL_set_projectionMatrix_m5591_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_projectionMatrix_m5591_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C" int32_t Camera_get_clearFlags_m5592 (Camera_t14 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m5592_ftn) (Camera_t14 *);
	static Camera_get_clearFlags_m5592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m5592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern "C" void Camera_set_clearFlags_m2903 (Camera_t14 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_clearFlags_m2903_ftn) (Camera_t14 *, int32_t);
	static Camera_set_clearFlags_m2903_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_clearFlags_m2903_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C" Vector3_t215  Camera_ScreenToViewportPoint_m4712 (Camera_t14 * __this, Vector3_t215  ___position, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Camera_INTERNAL_CALL_ScreenToViewportPoint_m5593(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Vector3_t215  Camera_INTERNAL_CALL_ScreenToViewportPoint_m5593 (Object_t * __this /* static, unused */, Camera_t14 * ___self, Vector3_t215 * ___position, const MethodInfo* method)
{
	typedef Vector3_t215  (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m5593_ftn) (Camera_t14 *, Vector3_t215 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m5593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m5593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C" Ray_t465  Camera_ScreenPointToRay_m3240 (Camera_t14 * __this, Vector3_t215  ___position, const MethodInfo* method)
{
	{
		Ray_t465  L_0 = Camera_INTERNAL_CALL_ScreenPointToRay_m5594(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
extern "C" Ray_t465  Camera_INTERNAL_CALL_ScreenPointToRay_m5594 (Object_t * __this /* static, unused */, Camera_t14 * ___self, Vector3_t215 * ___position, const MethodInfo* method)
{
	typedef Ray_t465  (*Camera_INTERNAL_CALL_ScreenPointToRay_m5594_ftn) (Camera_t14 *, Vector3_t215 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m5594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m5594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C" Camera_t14 * Camera_get_main_m2827 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t14 * (*Camera_get_main_m2827_ftn) ();
	static Camera_get_main_m2827_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m2827_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C" Camera_t14 * Camera_get_current_m2909 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t14 * (*Camera_get_current_m2909_ftn) ();
	static Camera_get_current_m2909_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_current_m2909_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_current()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C" int32_t Camera_get_allCamerasCount_m5595 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m5595_ftn) ();
	static Camera_get_allCamerasCount_m5595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m5595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C" int32_t Camera_GetAllCameras_m5596 (Object_t * __this /* static, unused */, CameraU5BU5D_t1041* ___cameras, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m5596_ftn) (CameraU5BU5D_t1041*);
	static Camera_GetAllCameras_m5596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m5596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern TypeInfo* Camera_t14_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreCull_m5597 (Object_t * __this /* static, unused */, Camera_t14 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t914 * L_0 = ((Camera_t14_StaticFields*)Camera_t14_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t914 * L_1 = ((Camera_t14_StaticFields*)Camera_t14_il2cpp_TypeInfo_var->static_fields)->___onPreCull_2;
		Camera_t14 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m5581(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern TypeInfo* Camera_t14_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPreRender_m5598 (Object_t * __this /* static, unused */, Camera_t14 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t914 * L_0 = ((Camera_t14_StaticFields*)Camera_t14_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t914 * L_1 = ((Camera_t14_StaticFields*)Camera_t14_il2cpp_TypeInfo_var->static_fields)->___onPreRender_3;
		Camera_t14 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m5581(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern TypeInfo* Camera_t14_il2cpp_TypeInfo_var;
extern "C" void Camera_FireOnPostRender_m5599 (Object_t * __this /* static, unused */, Camera_t14 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		CameraCallback_t914 * L_0 = ((Camera_t14_StaticFields*)Camera_t14_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		CameraCallback_t914 * L_1 = ((Camera_t14_StaticFields*)Camera_t14_il2cpp_TypeInfo_var->static_fields)->___onPostRender_4;
		Camera_t14 * L_2 = ___cam;
		NullCheck(L_1);
		CameraCallback_Invoke_m5581(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::set_useOcclusionCulling(System.Boolean)
extern "C" void Camera_set_useOcclusionCulling_m2908 (Camera_t14 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Camera_set_useOcclusionCulling_m2908_ftn) (Camera_t14 *, bool);
	static Camera_set_useOcclusionCulling_m2908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_useOcclusionCulling_m2908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_useOcclusionCulling(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Camera::CopyFrom(UnityEngine.Camera)
extern "C" void Camera_CopyFrom_m2882 (Camera_t14 * __this, Camera_t14 * ___other, const MethodInfo* method)
{
	typedef void (*Camera_CopyFrom_m2882_ftn) (Camera_t14 *, Camera_t14 *);
	static Camera_CopyFrom_m2882_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_CopyFrom_m2882_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::CopyFrom(UnityEngine.Camera)");
	_il2cpp_icall_func(__this, ___other);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t256 * Camera_RaycastTry_m5600 (Camera_t14 * __this, Ray_t465  ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		GameObject_t256 * L_2 = Camera_INTERNAL_CALL_RaycastTry_m5601(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C" GameObject_t256 * Camera_INTERNAL_CALL_RaycastTry_m5601 (Object_t * __this /* static, unused */, Camera_t14 * ___self, Ray_t465 * ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	typedef GameObject_t256 * (*Camera_INTERNAL_CALL_RaycastTry_m5601_ftn) (Camera_t14 *, Ray_t465 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m5601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m5601_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C" GameObject_t256 * Camera_RaycastTry2D_m5602 (Camera_t14 * __this, Ray_t465  ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		GameObject_t256 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m5603(NULL /*static, unused*/, __this, (&___ray), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C" GameObject_t256 * Camera_INTERNAL_CALL_RaycastTry2D_m5603 (Object_t * __this /* static, unused */, Camera_t14 * ___self, Ray_t465 * ___ray, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	typedef GameObject_t256 * (*Camera_INTERNAL_CALL_RaycastTry2D_m5603_ftn) (Camera_t14 *, Ray_t465 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m5603_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m5603_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self, ___ray, ___distance, ___layerMask);
}
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_Debug.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Exception
#include "mscorlib_System_Exception.h"


// System.Void UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)
extern "C" void Debug_Internal_Log_m5604 (Object_t * __this /* static, unused */, int32_t ___level, String_t* ___msg, Object_t473 * ___obj, const MethodInfo* method)
{
	typedef void (*Debug_Internal_Log_m5604_ftn) (int32_t, String_t*, Object_t473 *);
	static Debug_Internal_Log_m5604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_Log_m5604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level, ___msg, ___obj);
}
// System.Void UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_Internal_LogException_m5605 (Object_t * __this /* static, unused */, Exception_t520 * ___exception, Object_t473 * ___obj, const MethodInfo* method)
{
	typedef void (*Debug_Internal_LogException_m5605_ftn) (Exception_t520 *, Object_t473 *);
	static Debug_Internal_LogException_m5605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Debug_Internal_LogException_m5605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Debug::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception, ___obj);
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern Il2CppCodeGenString* _stringLiteral983;
extern "C" void Debug_Log_m2814 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral983 = il2cpp_codegen_string_literal_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 0;
		if (!L_0)
		{
			G_B2_0 = 0;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral983;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m5604(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern Il2CppCodeGenString* _stringLiteral983;
extern "C" void Debug_LogError_m2830 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral983 = il2cpp_codegen_string_literal_from_index(983);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B1_0 = 0;
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	{
		Object_t * L_0 = ___message;
		G_B1_0 = 2;
		if (!L_0)
		{
			G_B2_0 = 2;
			goto IL_0012;
		}
	}
	{
		Object_t * L_1 = ___message;
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_1);
		G_B3_0 = L_2;
		G_B3_1 = G_B1_0;
		goto IL_0017;
	}

IL_0012:
	{
		G_B3_0 = _stringLiteral983;
		G_B3_1 = G_B2_0;
	}

IL_0017:
	{
		Debug_Internal_Log_m5604(NULL /*static, unused*/, G_B3_1, G_B3_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C" void Debug_LogError_m4753 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t473 * ___context, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t473 * L_2 = ___context;
		Debug_Internal_Log_m5604(NULL /*static, unused*/, 2, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C" void Debug_LogException_m5606 (Object_t * __this /* static, unused */, Exception_t520 * ___exception, const MethodInfo* method)
{
	{
		Exception_t520 * L_0 = ___exception;
		Debug_Internal_LogException_m5605(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C" void Debug_LogException_m4665 (Object_t * __this /* static, unused */, Exception_t520 * ___exception, Object_t473 * ___context, const MethodInfo* method)
{
	{
		Exception_t520 * L_0 = ___exception;
		Object_t473 * L_1 = ___context;
		Debug_Internal_LogException_m5605(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C" void Debug_LogWarning_m2781 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Debug_Internal_Log_m5604(NULL /*static, unused*/, 1, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C" void Debug_LogWarning_m4900 (Object_t * __this /* static, unused */, Object_t * ___message, Object_t473 * ___context, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Object_t473 * L_2 = ___context;
		Debug_Internal_Log_m5604(NULL /*static, unused*/, 1, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegate.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Display/DisplaysUpdatedDelegate
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDelegateMethodDeclarations.h"



// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void DisplaysUpdatedDelegate__ctor_m5607 (DisplaysUpdatedDelegate_t916 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C" void DisplaysUpdatedDelegate_Invoke_m5608 (DisplaysUpdatedDelegate_t916 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m5608((DisplaysUpdatedDelegate_t916 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t916(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DisplaysUpdatedDelegate_BeginInvoke_m5609 (DisplaysUpdatedDelegate_t916 * __this, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m5610 (DisplaysUpdatedDelegate_t916 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Display
#include "UnityEngine_UnityEngine_Display.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Display
#include "UnityEngine_UnityEngine_DisplayMethodDeclarations.h"

// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"


// System.Void UnityEngine.Display::.ctor()
extern "C" void Display__ctor_m5611 (Display_t918 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = {0};
		IntPtr__ctor_m6395(&L_0, 0, /*hidden argument*/NULL);
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C" void Display__ctor_m5612 (Display_t918 * __this, IntPtr_t ___nativeDisplay, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay;
		__this->___nativeDisplay_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Display::.cctor()
extern TypeInfo* DisplayU5BU5D_t917_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" void Display__cctor_m5613 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t917_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(671);
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		DisplayU5BU5D_t917* L_0 = ((DisplayU5BU5D_t917*)SZArrayNew(DisplayU5BU5D_t917_il2cpp_TypeInfo_var, 1));
		Display_t918 * L_1 = (Display_t918 *)il2cpp_codegen_object_new (Display_t918_il2cpp_TypeInfo_var);
		Display__ctor_m5611(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Display_t918 **)(Display_t918 **)SZArrayLdElema(L_0, 0)) = (Display_t918 *)L_1;
		((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___displays_1 = L_0;
		DisplayU5BU5D_t917* L_2 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t918 **)(Display_t918 **)SZArrayLdElema(L_2, L_3));
		((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = (DisplaysUpdatedDelegate_t916 *)NULL;
		return;
	}
}
// System.Void UnityEngine.Display::add_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo_var;
extern "C" void Display_add_onDisplaysUpdated_m5614 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t916 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(673);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t916 * L_0 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t916 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t916 *)Castclass(L_2, DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Display::remove_onDisplaysUpdated(UnityEngine.Display/DisplaysUpdatedDelegate)
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern TypeInfo* DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo_var;
extern "C" void Display_remove_onDisplaysUpdated_m5615 (Object_t * __this /* static, unused */, DisplaysUpdatedDelegate_t916 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(673);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t916 * L_0 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		DisplaysUpdatedDelegate_t916 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3 = ((DisplaysUpdatedDelegate_t916 *)Castclass(L_2, DisplaysUpdatedDelegate_t916_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingWidth_m5616 (Display_t918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m5632(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_renderingHeight_m5617 (Display_t918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m5632(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemWidth_m5618 (Display_t918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m5631(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" int32_t Display_get_systemHeight_m5619 (Display_t918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m5631(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_colorBuffer()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t1023  Display_get_colorBuffer_m5620 (Display_t918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t1023  V_0 = {0};
	RenderBuffer_t1023  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m5633(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t1023  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.RenderBuffer UnityEngine.Display::get_depthBuffer()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" RenderBuffer_t1023  Display_get_depthBuffer_m5621 (Display_t918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	RenderBuffer_t1023  V_0 = {0};
	RenderBuffer_t1023  V_1 = {0};
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_GetRenderingBuffersImpl_m5633(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		RenderBuffer_t1023  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Display::Activate()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m5622 (Display_t918 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m5635(NULL /*static, unused*/, L_0, 0, 0, ((int32_t)60), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::Activate(System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" void Display_Activate_m5623 (Display_t918 * __this, int32_t ___width, int32_t ___height, int32_t ___refreshRate, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___refreshRate;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_ActivateDisplayImpl_m5635(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetParams(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" void Display_SetParams_m5624 (Display_t918 * __this, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___width;
		int32_t L_2 = ___height;
		int32_t L_3 = ___x;
		int32_t L_4 = ___y;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_SetParamsImpl_m5636(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::SetRenderingResolution(System.Int32,System.Int32)
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" void Display_SetRenderingResolution_m5625 (Display_t918 * __this, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = (__this->___nativeDisplay_0);
		int32_t L_1 = ___w;
		int32_t L_2 = ___h;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_SetRenderingResolutionImpl_m5634(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Display::MultiDisplayLicense()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" bool Display_MultiDisplayLicense_m5626 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		bool L_0 = Display_MultiDisplayLicenseImpl_m5637(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" Vector3_t215  Display_RelativeMouseAt_m5627 (Object_t * __this /* static, unused */, Vector3_t215  ___inputMouseCoordinates, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t215  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = ((&___inputMouseCoordinates)->___x_1);
		V_3 = (((int32_t)L_0));
		float L_1 = ((&___inputMouseCoordinates)->___y_2);
		V_4 = (((int32_t)L_1));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m5638(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->___z_3 = (((float)L_4));
		int32_t L_5 = V_1;
		(&V_0)->___x_1 = (((float)L_5));
		int32_t L_6 = V_2;
		(&V_0)->___y_2 = (((float)L_6));
		Vector3_t215  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Display UnityEngine.Display::get_main()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" Display_t918 * Display_get_main_m5628 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		Display_t918 * L_0 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2;
		return L_0;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern TypeInfo* DisplayU5BU5D_t917_il2cpp_TypeInfo_var;
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" void Display_RecreateDisplayList_m5629 (Object_t * __this /* static, unused */, IntPtrU5BU5D_t1084* ___nativeDisplay, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisplayU5BU5D_t917_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(671);
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t1084* L_0 = ___nativeDisplay;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___displays_1 = ((DisplayU5BU5D_t917*)SZArrayNew(DisplayU5BU5D_t917_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_0)->max_length)))));
		V_0 = 0;
		goto IL_0027;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t917* L_1 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t1084* L_3 = ___nativeDisplay;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Display_t918 * L_6 = (Display_t918 *)il2cpp_codegen_object_new (Display_t918_il2cpp_TypeInfo_var);
		Display__ctor_m5612(L_6, (*(IntPtr_t*)(IntPtr_t*)SZArrayLdElema(L_3, L_5)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		ArrayElementTypeCheck (L_1, L_6);
		*((Display_t918 **)(Display_t918 **)SZArrayLdElema(L_1, L_2)) = (Display_t918 *)L_6;
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_8 = V_0;
		IntPtrU5BU5D_t1084* L_9 = ___nativeDisplay;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t917* L_10 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___displays_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->____mainDisplay_2 = (*(Display_t918 **)(Display_t918 **)SZArrayLdElema(L_10, L_11));
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern TypeInfo* Display_t918_il2cpp_TypeInfo_var;
extern "C" void Display_FireDisplaysUpdated_m5630 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Display_t918_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(672);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t916 * L_0 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t918_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t916 * L_1 = ((Display_t918_StaticFields*)Display_t918_il2cpp_TypeInfo_var->static_fields)->___onDisplaysUpdated_3;
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m5608(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetSystemExtImpl_m5631 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetSystemExtImpl_m5631_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m5631_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m5631_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C" void Display_GetRenderingExtImpl_m5632 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t* ___w, int32_t* ___h, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingExtImpl_m5632_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m5632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m5632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
extern "C" void Display_GetRenderingBuffersImpl_m5633 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, RenderBuffer_t1023 * ___color, RenderBuffer_t1023 * ___depth, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingBuffersImpl_m5633_ftn) (IntPtr_t, RenderBuffer_t1023 *, RenderBuffer_t1023 *);
	static Display_GetRenderingBuffersImpl_m5633_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingBuffersImpl_m5633_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)");
	_il2cpp_icall_func(___nativeDisplay, ___color, ___depth);
}
// System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
extern "C" void Display_SetRenderingResolutionImpl_m5634 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___w, int32_t ___h, const MethodInfo* method)
{
	typedef void (*Display_SetRenderingResolutionImpl_m5634_ftn) (IntPtr_t, int32_t, int32_t);
	static Display_SetRenderingResolutionImpl_m5634_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetRenderingResolutionImpl_m5634_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___w, ___h);
}
// System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern "C" void Display_ActivateDisplayImpl_m5635 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___refreshRate, const MethodInfo* method)
{
	typedef void (*Display_ActivateDisplayImpl_m5635_ftn) (IntPtr_t, int32_t, int32_t, int32_t);
	static Display_ActivateDisplayImpl_m5635_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_ActivateDisplayImpl_m5635_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___refreshRate);
}
// System.Void UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Display_SetParamsImpl_m5636 (Object_t * __this /* static, unused */, IntPtr_t ___nativeDisplay, int32_t ___width, int32_t ___height, int32_t ___x, int32_t ___y, const MethodInfo* method)
{
	typedef void (*Display_SetParamsImpl_m5636_ftn) (IntPtr_t, int32_t, int32_t, int32_t, int32_t);
	static Display_SetParamsImpl_m5636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_SetParamsImpl_m5636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)");
	_il2cpp_icall_func(___nativeDisplay, ___width, ___height, ___x, ___y);
}
// System.Boolean UnityEngine.Display::MultiDisplayLicenseImpl()
extern "C" bool Display_MultiDisplayLicenseImpl_m5637 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Display_MultiDisplayLicenseImpl_m5637_ftn) ();
	static Display_MultiDisplayLicenseImpl_m5637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_MultiDisplayLicenseImpl_m5637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::MultiDisplayLicenseImpl()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C" int32_t Display_RelativeMouseAtImpl_m5638 (Object_t * __this /* static, unused */, int32_t ___x, int32_t ___y, int32_t* ___rx, int32_t* ___ry, const MethodInfo* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m5638_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m5638_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m5638_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	return _il2cpp_icall_func(___x, ___y, ___rx, ___ry);
}
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"

// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"


// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" void MonoBehaviour__ctor_m2715 (MonoBehaviour_t4 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m5579(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C" void MonoBehaviour_Invoke_m3073 (MonoBehaviour_t4 * __this, String_t* ___methodName, float ___time, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_Invoke_m3073_ftn) (MonoBehaviour_t4 *, String_t*, float);
	static MonoBehaviour_Invoke_m3073_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_Invoke_m3073_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___methodName, ___time);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" Coroutine_t547 * MonoBehaviour_StartCoroutine_m2752 (MonoBehaviour_t4 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		Coroutine_t547 * L_1 = MonoBehaviour_StartCoroutine_Auto_m5639(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C" Coroutine_t547 * MonoBehaviour_StartCoroutine_Auto_m5639 (MonoBehaviour_t4 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef Coroutine_t547 * (*MonoBehaviour_StartCoroutine_Auto_m5639_ftn) (MonoBehaviour_t4 *, Object_t *);
	static MonoBehaviour_StartCoroutine_Auto_m5639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m5639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C" Coroutine_t547 * MonoBehaviour_StartCoroutine_m3111 (MonoBehaviour_t4 * __this, String_t* ___methodName, Object_t * ___value, const MethodInfo* method)
{
	typedef Coroutine_t547 * (*MonoBehaviour_StartCoroutine_m3111_ftn) (MonoBehaviour_t4 *, String_t*, Object_t *);
	static MonoBehaviour_StartCoroutine_m3111_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m3111_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName, ___value);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C" Coroutine_t547 * MonoBehaviour_StartCoroutine_m2845 (MonoBehaviour_t4 * __this, String_t* ___methodName, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_0;
		Coroutine_t547 * L_2 = MonoBehaviour_StartCoroutine_m3111(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C" void MonoBehaviour_StopCoroutine_m2846 (MonoBehaviour_t4 * __this, String_t* ___methodName, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_m2846_ftn) (MonoBehaviour_t4 *, String_t*);
	static MonoBehaviour_StopCoroutine_m2846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_m2846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine(System.String)");
	_il2cpp_icall_func(__this, ___methodName);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutine_m5640 (MonoBehaviour_t4 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m5641(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_m4847 (MonoBehaviour_t4 * __this, Coroutine_t547 * ___routine, const MethodInfo* method)
{
	{
		Coroutine_t547 * L_0 = ___routine;
		MonoBehaviour_StopCoroutine_Auto_m5642(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m5641 (MonoBehaviour_t4 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m5641_ftn) (MonoBehaviour_t4 *, Object_t *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m5641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m5641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_Auto_m5642 (MonoBehaviour_t4 * __this, Coroutine_t547 * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m5642_ftn) (MonoBehaviour_t4 *, Coroutine_t547 *);
	static MonoBehaviour_StopCoroutine_Auto_m5642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m5642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C" void MonoBehaviour_print_m3229 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___message;
		Debug_Log_m2814(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhaseMethodDeclarations.h"



// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionModeMethodDeclarations.h"



// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"



// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m4609 (Touch_t768 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FingerId_0);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t7  Touch_get_position_m4611 (Touch_t768 * __this, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = (__this->___m_Position_1);
		return L_0;
	}
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m4610 (Touch_t768 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Phase_6);
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Touch
void Touch_t768_marshal(const Touch_t768& unmarshaled, Touch_t768_marshaled& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.___m_FingerId_0;
	marshaled.___m_Position_1 = unmarshaled.___m_Position_1;
	marshaled.___m_RawPosition_2 = unmarshaled.___m_RawPosition_2;
	marshaled.___m_PositionDelta_3 = unmarshaled.___m_PositionDelta_3;
	marshaled.___m_TimeDelta_4 = unmarshaled.___m_TimeDelta_4;
	marshaled.___m_TapCount_5 = unmarshaled.___m_TapCount_5;
	marshaled.___m_Phase_6 = unmarshaled.___m_Phase_6;
}
void Touch_t768_marshal_back(const Touch_t768_marshaled& marshaled, Touch_t768& unmarshaled)
{
	unmarshaled.___m_FingerId_0 = marshaled.___m_FingerId_0;
	unmarshaled.___m_Position_1 = marshaled.___m_Position_1;
	unmarshaled.___m_RawPosition_2 = marshaled.___m_RawPosition_2;
	unmarshaled.___m_PositionDelta_3 = marshaled.___m_PositionDelta_3;
	unmarshaled.___m_TimeDelta_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.___m_TapCount_5 = marshaled.___m_TapCount_5;
	unmarshaled.___m_Phase_6 = marshaled.___m_Phase_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
void Touch_t768_marshal_cleanup(Touch_t768_marshaled& marshaled)
{
}
// UnityEngine.Input
#include "UnityEngine_UnityEngine_Input.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"

// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"


// System.Void UnityEngine.Input::.cctor()
extern "C" void Input__cctor_m5643 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
extern "C" bool Input_GetKeyInt_m5644 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	typedef bool (*Input_GetKeyInt_m5644_ftn) (int32_t);
	static Input_GetKeyInt_m5644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyInt_m5644_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyInt(System.Int32)");
	return _il2cpp_icall_func(___key);
}
// System.Boolean UnityEngine.Input::GetKeyUpInt(System.Int32)
extern "C" bool Input_GetKeyUpInt_m5645 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	typedef bool (*Input_GetKeyUpInt_m5645_ftn) (int32_t);
	static Input_GetKeyUpInt_m5645_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyUpInt_m5645_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyUpInt(System.Int32)");
	return _il2cpp_icall_func(___key);
}
// System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
extern "C" bool Input_GetKeyDownInt_m5646 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	typedef bool (*Input_GetKeyDownInt_m5646_ftn) (int32_t);
	static Input_GetKeyDownInt_m5646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetKeyDownInt_m5646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetKeyDownInt(System.Int32)");
	return _il2cpp_icall_func(___key);
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C" float Input_GetAxisRaw_m4632 (Object_t * __this /* static, unused */, String_t* ___axisName, const MethodInfo* method)
{
	typedef float (*Input_GetAxisRaw_m4632_ftn) (String_t*);
	static Input_GetAxisRaw_m4632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m4632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	return _il2cpp_icall_func(___axisName);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C" bool Input_GetButtonDown_m4631 (Object_t * __this /* static, unused */, String_t* ___buttonName, const MethodInfo* method)
{
	typedef bool (*Input_GetButtonDown_m4631_ftn) (String_t*);
	static Input_GetButtonDown_m4631_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m4631_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	return _il2cpp_icall_func(___buttonName);
}
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKey_m3173 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyInt_m5644(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKeyDown_m2843 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDownInt_m5646(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" bool Input_GetKeyUp_m3168 (Object_t * __this /* static, unused */, int32_t ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___key;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyUpInt_m5645(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C" bool Input_GetMouseButton_m2962 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButton_m2962_ftn) (int32_t);
	static Input_GetMouseButton_m2962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m2962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" bool Input_GetMouseButtonDown_m2842 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonDown_m2842_ftn) (int32_t);
	static Input_GetMouseButtonDown_m2842_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m2842_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C" bool Input_GetMouseButtonUp_m4612 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonUp_m4612_ftn) (int32_t);
	static Input_GetMouseButtonUp_m4612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m4612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" Vector3_t215  Input_get_mousePosition_m3239 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t215  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m5647(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C" void Input_INTERNAL_get_mousePosition_m5647 (Object_t * __this /* static, unused */, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m5647_ftn) (Vector3_t215 *);
	static Input_INTERNAL_get_mousePosition_m5647_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m5647_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" Vector2_t7  Input_get_mouseScrollDelta_m4614 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mouseScrollDelta_m5648(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_get_mouseScrollDelta_m5648 (Object_t * __this /* static, unused */, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mouseScrollDelta_m5648_ftn) (Vector2_t7 *);
	static Input_INTERNAL_get_mouseScrollDelta_m5648_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mouseScrollDelta_m5648_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern "C" bool Input_get_mousePresent_m4630 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Input_get_mousePresent_m4630_ftn) ();
	static Input_get_mousePresent_m4630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mousePresent_m4630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mousePresent()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Input::get_anyKey()
extern "C" bool Input_get_anyKey_m3172 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Input_get_anyKey_m3172_ftn) ();
	static Input_get_anyKey_m3172_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_anyKey_m3172_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_anyKey()");
	return _il2cpp_icall_func();
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" Touch_t768  Input_GetTouch_m4636 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	typedef Touch_t768  (*Input_GetTouch_m4636_ftn) (int32_t);
	static Input_GetTouch_m4636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetTouch_m4636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetTouch(System.Int32)");
	return _il2cpp_icall_func(___index);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" int32_t Input_get_touchCount_m3170 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Input_get_touchCount_m3170_ftn) ();
	static Input_get_touchCount_m3170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m3170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C" bool Input_get_touchSupported_m4635 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C" void Input_set_imeCompositionMode_m4844 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Input_set_imeCompositionMode_m4844_ftn) (int32_t);
	static Input_set_imeCompositionMode_m4844_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m4844_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C" String_t* Input_get_compositionString_m4778 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Input_get_compositionString_m4778_ftn) ();
	static Input_get_compositionString_m4778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m4778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern TypeInfo* Input_t486_il2cpp_TypeInfo_var;
extern "C" void Input_set_compositionCursorPos_m4832 (Object_t * __this /* static, unused */, Vector2_t7  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t486_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m5649(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_set_compositionCursorPos_m5649 (Object_t * __this /* static, unused */, Vector2_t7 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m5649_ftn) (Vector2_t7 *);
	static Input_INTERNAL_set_compositionCursorPos_m5649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m5649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlagsMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"

// System.Type
#include "mscorlib_System_Type.h"


// System.Void UnityEngine.Object::.ctor()
extern "C" void Object__ctor_m5650 (Object_t473 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C" Object_t473 * Object_Internal_CloneSingle_m5651 (Object_t * __this /* static, unused */, Object_t473 * ___data, const MethodInfo* method)
{
	typedef Object_t473 * (*Object_Internal_CloneSingle_m5651_ftn) (Object_t473 *);
	static Object_Internal_CloneSingle_m5651_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m5651_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" void Object_Destroy_m5652 (Object_t * __this /* static, unused */, Object_t473 * ___obj, float ___t, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m5652_ftn) (Object_t473 *, float);
	static Object_Destroy_m5652_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m5652_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj, ___t);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" void Object_Destroy_m2815 (Object_t * __this /* static, unused */, Object_t473 * ___obj, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t473 * L_0 = ___obj;
		float L_1 = V_0;
		Object_Destroy_m5652(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C" void Object_DestroyImmediate_m5653 (Object_t * __this /* static, unused */, Object_t473 * ___obj, bool ___allowDestroyingAssets, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m5653_ftn) (Object_t473 *, bool);
	static Object_DestroyImmediate_m5653_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m5653_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj, ___allowDestroyingAssets);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" void Object_DestroyImmediate_m2734 (Object_t * __this /* static, unused */, Object_t473 * ___obj, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Object_t473 * L_0 = ___obj;
		bool L_1 = V_0;
		Object_DestroyImmediate_m5653(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C" ObjectU5BU5D_t1077* Object_FindObjectsOfType_m5654 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1077* (*Object_FindObjectsOfType_m5654_ftn) (Type_t *);
	static Object_FindObjectsOfType_m5654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m5654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type);
}
// System.String UnityEngine.Object::get_name()
extern "C" String_t* Object_get_name_m2979 (Object_t473 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m2979_ftn) (Object_t473 *);
	static Object_get_name_m2979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m2979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" void Object_set_name_m3163 (Object_t473 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Object_set_name_m3163_ftn) (Object_t473 *, String_t*);
	static Object_set_name_m3163_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m3163_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" void Object_set_hideFlags_m2718 (Object_t473 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m2718_ftn) (Object_t473 *, int32_t);
	static Object_set_hideFlags_m2718_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m2718_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Object::ToString()
extern "C" String_t* Object_ToString_m5655 (Object_t473 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m5655_ftn) (Object_t473 *);
	static Object_ToString_m5655_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m5655_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern TypeInfo* Object_t473_il2cpp_TypeInfo_var;
extern "C" bool Object_Equals_m5656 (Object_t473 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t473_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(481);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = Object_CompareBaseObjects_m5658(NULL /*static, unused*/, __this, ((Object_t473 *)IsInst(L_0, Object_t473_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C" int32_t Object_GetHashCode_m5657 (Object_t473 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m5660(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_CompareBaseObjects_m5658 (Object_t * __this /* static, unused */, Object_t473 * ___lhs, Object_t473 * ___rhs, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t473 * L_0 = ___lhs;
		V_0 = ((((Object_t*)(Object_t473 *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		Object_t473 * L_1 = ___rhs;
		V_1 = ((((Object_t*)(Object_t473 *)L_1) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t473 * L_5 = ___lhs;
		bool L_6 = Object_IsNativeObjectAlive_m5659(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t473 * L_8 = ___rhs;
		bool L_9 = Object_IsNativeObjectAlive_m5659(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t473 * L_10 = ___lhs;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___m_InstanceID_0);
		Object_t473 * L_12 = ___rhs;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___m_InstanceID_0);
		return ((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool Object_IsNativeObjectAlive_m5659 (Object_t * __this /* static, unused */, Object_t473 * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(632);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t473 * L_0 = ___o;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m5661(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_3 = IntPtr_op_Inequality_m6396(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C" int32_t Object_GetInstanceID_m5660 (Object_t473 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_InstanceID_0);
		return L_0;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C" IntPtr_t Object_GetCachedPtr_m5661 (Object_t473 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_CachedPtr_1);
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern Il2CppCodeGenString* _stringLiteral984;
extern "C" Object_t473 * Object_Instantiate_m3162 (Object_t * __this /* static, unused */, Object_t473 * ___original, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral984 = il2cpp_codegen_string_literal_from_index(984);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t473 * L_0 = ___original;
		Object_CheckNullArgument_m5662(NULL /*static, unused*/, L_0, _stringLiteral984, /*hidden argument*/NULL);
		Object_t473 * L_1 = ___original;
		Object_t473 * L_2 = Object_Internal_CloneSingle_m5651(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern TypeInfo* ArgumentException_t556_il2cpp_TypeInfo_var;
extern "C" void Object_CheckNullArgument_m5662 (Object_t * __this /* static, unused */, Object_t * ___arg, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___arg;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message;
		ArgumentException_t556 * L_2 = (ArgumentException_t556 *)il2cpp_codegen_object_new (ArgumentException_t556_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3362(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_000d:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C" Object_t473 * Object_FindObjectOfType_m5663 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	ObjectU5BU5D_t1077* V_0 = {0};
	{
		Type_t * L_0 = ___type;
		ObjectU5BU5D_t1077* L_1 = Object_FindObjectsOfType_m5654(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t1077* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		ObjectU5BU5D_t1077* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		return (*(Object_t473 **)(Object_t473 **)SZArrayLdElema(L_3, L_4));
	}

IL_0014:
	{
		return (Object_t473 *)NULL;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" bool Object_op_Implicit_m2733 (Object_t * __this /* static, unused */, Object_t473 * ___exists, const MethodInfo* method)
{
	{
		Object_t473 * L_0 = ___exists;
		bool L_1 = Object_CompareBaseObjects_m5658(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Equality_m2716 (Object_t * __this /* static, unused */, Object_t473 * ___x, Object_t473 * ___y, const MethodInfo* method)
{
	{
		Object_t473 * L_0 = ___x;
		Object_t473 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m5658(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Inequality_m2722 (Object_t * __this /* static, unused */, Object_t473 * ___x, Object_t473 * ___y, const MethodInfo* method)
{
	{
		Object_t473 * L_0 = ___x;
		Object_t473 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m5658(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
void Object_t473_marshal(const Object_t473& unmarshaled, Object_t473_marshaled& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.___m_InstanceID_0;
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.___m_CachedPtr_1).___m_value_0);
}
void Object_t473_marshal_back(const Object_t473_marshaled& marshaled, Object_t473& unmarshaled)
{
	unmarshaled.___m_InstanceID_0 = marshaled.___m_InstanceID_0;
	(unmarshaled.___m_CachedPtr_1).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_CachedPtr_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
void Object_t473_marshal_cleanup(Object_t473_marshaled& marshaled)
{
}
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_21.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"


// System.Void UnityEngine.Component::.ctor()
extern "C" void Component__ctor_m5664 (Component_t477 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t243 * Component_get_transform_m2760 (Component_t477 * __this, const MethodInfo* method)
{
	typedef Transform_t243 * (*Component_get_transform_m2760_ftn) (Component_t477 *);
	static Component_get_transform_m2760_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m2760_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t256 * Component_get_gameObject_m2778 (Component_t477 * __this, const MethodInfo* method)
{
	typedef GameObject_t256 * (*Component_get_gameObject_m2778_ftn) (Component_t477 *);
	static Component_get_gameObject_m2778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m2778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C" Component_t477 * Component_GetComponent_m3318 (Component_t477 * __this, Type_t * ___type, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		Component_t477 * L_2 = GameObject_GetComponent_m3300(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void Component_GetComponentFastPath_m5665 (Component_t477 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m5665_ftn) (Component_t477 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m5665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m5665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C" Component_t477 * Component_GetComponentInChildren_m5666 (Component_t477 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t477 * L_2 = GameObject_GetComponentInChildren_m5674(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type)
extern "C" ComponentU5BU5D_t539* Component_GetComponentsInChildren_m3216 (Component_t477 * __this, Type_t * ___t, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Type_t * L_0 = ___t;
		bool L_1 = V_0;
		ComponentU5BU5D_t539* L_2 = Component_GetComponentsInChildren_m5667(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type,System.Boolean)
extern "C" ComponentU5BU5D_t539* Component_GetComponentsInChildren_m5667 (Component_t477 * __this, Type_t * ___t, bool ___includeInactive, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		bool L_2 = ___includeInactive;
		NullCheck(L_0);
		ComponentU5BU5D_t539* L_3 = GameObject_GetComponentsInChildren_m5676(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C" Component_t477 * Component_GetComponentInParent_m5668 (Component_t477 * __this, Type_t * ___t, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t;
		NullCheck(L_0);
		Component_t477 * L_2 = GameObject_GetComponentInParent_m5675(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponents(System.Type)
extern "C" ComponentU5BU5D_t539* Component_GetComponents_m3352 (Component_t477 * __this, Type_t * ___type, const MethodInfo* method)
{
	{
		GameObject_t256 * L_0 = Component_get_gameObject_m2778(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		ComponentU5BU5D_t539* L_2 = GameObject_GetComponents_m3340(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C" void Component_GetComponentsForListInternal_m5669 (Component_t477 * __this, Type_t * ___searchType, Object_t * ___resultList, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m5669_ftn) (Component_t477 *, Type_t *, Object_t *);
	static Component_GetComponentsForListInternal_m5669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m5669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType, ___resultList);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C" void Component_GetComponents_m4686 (Component_t477 * __this, Type_t * ___type, List_1_t775 * ___results, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		List_1_t775 * L_1 = ___results;
		Component_GetComponentsForListInternal_m5669(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Light
#include "UnityEngine_UnityEngine_Light.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Light
#include "UnityEngine_UnityEngine_LightMethodDeclarations.h"

// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"


// UnityEngine.Color UnityEngine.Light::get_color()
extern "C" Color_t6  Light_get_color_m3304 (Light_t318 * __this, const MethodInfo* method)
{
	Color_t6  V_0 = {0};
	{
		Light_INTERNAL_get_color_m5670(__this, (&V_0), /*hidden argument*/NULL);
		Color_t6  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern "C" void Light_set_color_m3306 (Light_t318 * __this, Color_t6  ___value, const MethodInfo* method)
{
	{
		Light_INTERNAL_set_color_m5671(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_get_color_m5670 (Light_t318 * __this, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Light_INTERNAL_get_color_m5670_ftn) (Light_t318 *, Color_t6 *);
	static Light_INTERNAL_get_color_m5670_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_INTERNAL_get_color_m5670_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void Light_INTERNAL_set_color_m5671 (Light_t318 * __this, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*Light_INTERNAL_set_color_m5671_ftn) (Light_t318 *, Color_t6 *);
	static Light_INTERNAL_set_color_m5671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_INTERNAL_set_color_m5671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Light::get_intensity()
extern "C" float Light_get_intensity_m3158 (Light_t318 * __this, const MethodInfo* method)
{
	typedef float (*Light_get_intensity_m3158_ftn) (Light_t318 *);
	static Light_get_intensity_m3158_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_get_intensity_m3158_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::get_intensity()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Light::set_intensity(System.Single)
extern "C" void Light_set_intensity_m3124 (Light_t318 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Light_set_intensity_m3124_ftn) (Light_t318 *, float);
	static Light_set_intensity_m3124_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_intensity_m3124_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_intensity(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Light::set_range(System.Single)
extern "C" void Light_set_range_m3141 (Light_t318 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Light_set_range_m3141_ftn) (Light_t318 *, float);
	static Light_set_range_m3141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_range_m3141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_range(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Light::set_spotAngle(System.Single)
extern "C" void Light_set_spotAngle_m3199 (Light_t318 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Light_set_spotAngle_m3199_ftn) (Light_t318 *, float);
	static Light_set_spotAngle_m3199_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_spotAngle_m3199_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_spotAngle(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Light::set_cookie(UnityEngine.Texture)
extern "C" void Light_set_cookie_m3198 (Light_t318 * __this, Texture_t221 * ___value, const MethodInfo* method)
{
	typedef void (*Light_set_cookie_m3198_ftn) (Light_t318 *, Texture_t221 *);
	static Light_set_cookie_m3198_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Light_set_cookie_m3198_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Light::set_cookie(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___value);
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"


// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" void GameObject__ctor_m2824 (GameObject_t256 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m5679(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor()
extern "C" void GameObject__ctor_m5672 (GameObject_t256 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		GameObject_Internal_CreateGameObject_m5679(NULL /*static, unused*/, __this, (String_t*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern "C" void GameObject__ctor_m2839 (GameObject_t256 * __this, String_t* ___name, TypeU5BU5D_t485* ___components, const MethodInfo* method)
{
	Type_t * V_0 = {0};
	TypeU5BU5D_t485* V_1 = {0};
	int32_t V_2 = 0;
	{
		Object__ctor_m5650(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m5679(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		TypeU5BU5D_t485* L_1 = ___components;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0026;
	}

IL_0016:
	{
		TypeU5BU5D_t485* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = (*(Type_t **)(Type_t **)SZArrayLdElema(L_2, L_4));
		Type_t * L_5 = V_0;
		GameObject_AddComponent_m3321(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_7 = V_2;
		TypeU5BU5D_t485* L_8 = V_1;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))))
		{
			goto IL_0016;
		}
	}
	{
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" Component_t477 * GameObject_GetComponent_m3300 (GameObject_t256 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef Component_t477 * (*GameObject_GetComponent_m3300_ftn) (GameObject_t256 *, Type_t *);
	static GameObject_GetComponent_m3300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m3300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void GameObject_GetComponentFastPath_m5673 (GameObject_t256 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*GameObject_GetComponentFastPath_m5673_ftn) (GameObject_t256 *, Type_t *, IntPtr_t);
	static GameObject_GetComponentFastPath_m5673_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m5673_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type)
extern TypeInfo* IEnumerator_t464_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t243_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern "C" Component_t477 * GameObject_GetComponentInChildren_m5674 (GameObject_t256 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t464_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		Transform_t243_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		s_Il2CppMethodIntialized = true;
	}
	Component_t477 * V_0 = {0};
	Transform_t243 * V_1 = {0};
	Transform_t243 * V_2 = {0};
	Object_t * V_3 = {0};
	Component_t477 * V_4 = {0};
	Component_t477 * V_5 = {0};
	Object_t * V_6 = {0};
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = GameObject_get_activeInHierarchy_m2779(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t477 * L_2 = GameObject_GetComponent_m3300(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t477 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_3, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t477 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t243 * L_6 = GameObject_get_transform_m2825(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		Transform_t243 * L_7 = V_1;
		bool L_8 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_7, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0095;
		}
	}
	{
		Transform_t243 * L_9 = V_1;
		NullCheck(L_9);
		Object_t * L_10 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_9);
		V_3 = L_10;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0070;
		}

IL_0040:
		{
			Object_t * L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t464_il2cpp_TypeInfo_var, L_11);
			V_2 = ((Transform_t243 *)Castclass(L_12, Transform_t243_il2cpp_TypeInfo_var));
			Transform_t243 * L_13 = V_2;
			NullCheck(L_13);
			GameObject_t256 * L_14 = Component_get_gameObject_m2778(L_13, /*hidden argument*/NULL);
			Type_t * L_15 = ___type;
			NullCheck(L_14);
			Component_t477 * L_16 = GameObject_GetComponentInChildren_m5674(L_14, L_15, /*hidden argument*/NULL);
			V_4 = L_16;
			Component_t477 * L_17 = V_4;
			bool L_18 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_17, (Object_t473 *)NULL, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0070;
			}
		}

IL_0067:
		{
			Component_t477 * L_19 = V_4;
			V_5 = L_19;
			IL2CPP_LEAVE(0x97, FINALLY_0080);
		}

IL_0070:
		{
			Object_t * L_20 = V_3;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t464_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0040;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x95, FINALLY_0080);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_0080;
	}

FINALLY_0080:
	{ // begin finally (depth: 1)
		{
			Object_t * L_22 = V_3;
			V_6 = ((Object_t *)IsInst(L_22, IDisposable_t538_il2cpp_TypeInfo_var));
			Object_t * L_23 = V_6;
			if (L_23)
			{
				goto IL_008d;
			}
		}

IL_008c:
		{
			IL2CPP_END_FINALLY(128)
		}

IL_008d:
		{
			Object_t * L_24 = V_6;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, L_24);
			IL2CPP_END_FINALLY(128)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(128)
	{
		IL2CPP_JUMP_TBL(0x97, IL_0097)
		IL2CPP_JUMP_TBL(0x95, IL_0095)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0095:
	{
		return (Component_t477 *)NULL;
	}

IL_0097:
	{
		Component_t477 * L_25 = V_5;
		return L_25;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C" Component_t477 * GameObject_GetComponentInParent_m5675 (GameObject_t256 * __this, Type_t * ___type, const MethodInfo* method)
{
	Component_t477 * V_0 = {0};
	Transform_t243 * V_1 = {0};
	Component_t477 * V_2 = {0};
	{
		bool L_0 = GameObject_get_activeInHierarchy_m2779(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Type_t * L_1 = ___type;
		Component_t477 * L_2 = GameObject_GetComponent_m3300(__this, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Component_t477 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_3, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		Component_t477 * L_5 = V_0;
		return L_5;
	}

IL_0021:
	{
		Transform_t243 * L_6 = GameObject_get_transform_m2825(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t243 * L_7 = Transform_get_parent_m2854(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Transform_t243 * L_8 = V_1;
		bool L_9 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_8, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_007c;
		}
	}
	{
		goto IL_0070;
	}

IL_003e:
	{
		Transform_t243 * L_10 = V_1;
		NullCheck(L_10);
		GameObject_t256 * L_11 = Component_get_gameObject_m2778(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		bool L_12 = GameObject_get_activeInHierarchy_m2779(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0069;
		}
	}
	{
		Transform_t243 * L_13 = V_1;
		NullCheck(L_13);
		GameObject_t256 * L_14 = Component_get_gameObject_m2778(L_13, /*hidden argument*/NULL);
		Type_t * L_15 = ___type;
		NullCheck(L_14);
		Component_t477 * L_16 = GameObject_GetComponent_m3300(L_14, L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		Component_t477 * L_17 = V_2;
		bool L_18 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_17, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0069;
		}
	}
	{
		Component_t477 * L_19 = V_2;
		return L_19;
	}

IL_0069:
	{
		Transform_t243 * L_20 = V_1;
		NullCheck(L_20);
		Transform_t243 * L_21 = Transform_get_parent_m2854(L_20, /*hidden argument*/NULL);
		V_1 = L_21;
	}

IL_0070:
	{
		Transform_t243 * L_22 = V_1;
		bool L_23 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_22, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_003e;
		}
	}

IL_007c:
	{
		return (Component_t477 *)NULL;
	}
}
// UnityEngine.Component[] UnityEngine.GameObject::GetComponents(System.Type)
extern TypeInfo* ComponentU5BU5D_t539_il2cpp_TypeInfo_var;
extern "C" ComponentU5BU5D_t539* GameObject_GetComponents_m3340 (GameObject_t256 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentU5BU5D_t539_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		Array_t * L_1 = GameObject_GetComponentsInternal_m5677(__this, L_0, 0, 0, 1, 0, NULL, /*hidden argument*/NULL);
		return ((ComponentU5BU5D_t539*)Castclass(L_1, ComponentU5BU5D_t539_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type,System.Boolean)
extern TypeInfo* ComponentU5BU5D_t539_il2cpp_TypeInfo_var;
extern "C" ComponentU5BU5D_t539* GameObject_GetComponentsInChildren_m5676 (GameObject_t256 * __this, Type_t * ___type, bool ___includeInactive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComponentU5BU5D_t539_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		bool L_1 = ___includeInactive;
		Array_t * L_2 = GameObject_GetComponentsInternal_m5677(__this, L_0, 0, 1, L_1, 0, NULL, /*hidden argument*/NULL);
		return ((ComponentU5BU5D_t539*)Castclass(L_2, ComponentU5BU5D_t539_il2cpp_TypeInfo_var));
	}
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C" Array_t * GameObject_GetComponentsInternal_m5677 (GameObject_t256 * __this, Type_t * ___type, bool ___useSearchTypeAsArrayReturnType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, const MethodInfo* method)
{
	typedef Array_t * (*GameObject_GetComponentsInternal_m5677_ftn) (GameObject_t256 *, Type_t *, bool, bool, bool, bool, Object_t *);
	static GameObject_GetComponentsInternal_m5677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m5677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, ___type, ___useSearchTypeAsArrayReturnType, ___recursive, ___includeInactive, ___reverse, ___resultList);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t243 * GameObject_get_transform_m2825 (GameObject_t256 * __this, const MethodInfo* method)
{
	typedef Transform_t243 * (*GameObject_get_transform_m2825_ftn) (GameObject_t256 *);
	static GameObject_get_transform_m2825_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m2825_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" int32_t GameObject_get_layer_m4818 (GameObject_t256 * __this, const MethodInfo* method)
{
	typedef int32_t (*GameObject_get_layer_m4818_ftn) (GameObject_t256 *);
	static GameObject_get_layer_m4818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m4818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C" void GameObject_set_layer_m4819 (GameObject_t256 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GameObject_set_layer_m4819_ftn) (GameObject_t256 *, int32_t);
	static GameObject_set_layer_m4819_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m4819_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m2960 (GameObject_t256 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GameObject_SetActive_m2960_ftn) (GameObject_t256 *, bool);
	static GameObject_SetActive_m2960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m2960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m2779 (GameObject_t256 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m2779_ftn) (GameObject_t256 *);
	static GameObject_get_activeInHierarchy_m2779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m2779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.GameObject::get_tag()
extern "C" String_t* GameObject_get_tag_m3175 (GameObject_t256 * __this, const MethodInfo* method)
{
	typedef String_t* (*GameObject_get_tag_m3175_ftn) (GameObject_t256 *);
	static GameObject_get_tag_m3175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_tag_m3175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_tag()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C" GameObjectU5BU5D_t323* GameObject_FindGameObjectsWithTag_m3135 (Object_t * __this /* static, unused */, String_t* ___tag, const MethodInfo* method)
{
	typedef GameObjectU5BU5D_t323* (*GameObject_FindGameObjectsWithTag_m3135_ftn) (String_t*);
	static GameObject_FindGameObjectsWithTag_m3135_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_FindGameObjectsWithTag_m3135_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::FindGameObjectsWithTag(System.String)");
	return _il2cpp_icall_func(___tag);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m3351 (GameObject_t256 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m3351_ftn) (GameObject_t256 *, String_t*, Object_t *, int32_t);
	static GameObject_SendMessage_m3351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m3351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String)
extern "C" void GameObject_SendMessage_m2840 (GameObject_t256 * __this, String_t* ___methodName, const MethodInfo* method)
{
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_1;
		int32_t L_2 = V_0;
		GameObject_SendMessage_m3351(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t477 * GameObject_Internal_AddComponentWithType_m5678 (GameObject_t256 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	typedef Component_t477 * (*GameObject_Internal_AddComponentWithType_m5678_ftn) (GameObject_t256 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m5678_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m5678_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, ___componentType);
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t477 * GameObject_AddComponent_m3321 (GameObject_t256 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___componentType;
		Component_t477 * L_1 = GameObject_Internal_AddComponentWithType_m5678(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C" void GameObject_Internal_CreateGameObject_m5679 (Object_t * __this /* static, unused */, GameObject_t256 * ___mono, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m5679_ftn) (GameObject_t256 *, String_t*);
	static GameObject_Internal_CreateGameObject_m5679_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m5679_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono, ___name);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" GameObject_t256 * GameObject_Find_m3123 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef GameObject_t256 * (*GameObject_Find_m3123_ftn) (String_t*);
	static GameObject_Find_m3123_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Find_m3123_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Transform/Enumerator
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"



// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C" void Enumerator__ctor_m5680 (Enumerator_t922 * __this, Transform_t243 * ___outer, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		Transform_t243 * L_0 = ___outer;
		__this->___outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m5681 (Enumerator_t922 * __this, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = (__this->___outer_0);
		int32_t L_1 = (__this->___currentIndex_1);
		NullCheck(L_0);
		Transform_t243 * L_2 = Transform_GetChild_m4946(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m5682 (Enumerator_t922 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t243 * L_0 = (__this->___outer_0);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m4947(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___currentIndex_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->___currentIndex_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return ((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"


// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" Vector3_t215  Transform_get_position_m2894 (Transform_t243 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Transform_INTERNAL_get_position_m5683(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" void Transform_set_position_m2902 (Transform_t243 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m5684(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_position_m5683 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m5683_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_get_position_m5683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m5683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_position_m5684 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m5684_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_set_position_m5684_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m5684_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" Vector3_t215  Transform_get_localPosition_m2761 (Transform_t243 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Transform_INTERNAL_get_localPosition_m5685(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" void Transform_set_localPosition_m2766 (Transform_t243 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m5686(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localPosition_m5685 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m5685_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_get_localPosition_m5685_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m5685_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localPosition_m5686 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m5686_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_set_localPosition_m5686_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m5686_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern "C" Vector3_t215  Transform_get_eulerAngles_m3209 (Transform_t243 * __this, const MethodInfo* method)
{
	Quaternion_t261  V_0 = {0};
	{
		Quaternion_t261  L_0 = Transform_get_rotation_m2897(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Vector3_t215  L_1 = Quaternion_get_eulerAngles_m5384((&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern "C" void Transform_set_eulerAngles_m3315 (Transform_t243 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		Quaternion_t261  L_1 = Quaternion_Euler_m3330(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Transform_set_rotation_m2899(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern "C" Vector3_t215  Transform_get_localEulerAngles_m3316 (Transform_t243 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Transform_INTERNAL_get_localEulerAngles_m5687(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern "C" void Transform_set_localEulerAngles_m3317 (Transform_t243 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localEulerAngles_m5688(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localEulerAngles_m5687 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localEulerAngles_m5687_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_get_localEulerAngles_m5687_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localEulerAngles_m5687_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localEulerAngles_m5688 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localEulerAngles_m5688_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_set_localEulerAngles_m5688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localEulerAngles_m5688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localEulerAngles(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C" Vector3_t215  Transform_get_forward_m2895 (Transform_t243 * __this, const MethodInfo* method)
{
	{
		Quaternion_t261  L_0 = Transform_get_rotation_m2897(__this, /*hidden argument*/NULL);
		Vector3_t215  L_1 = Vector3_get_forward_m2966(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_2 = Quaternion_op_Multiply_m2900(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" Quaternion_t261  Transform_get_rotation_m2897 (Transform_t243 * __this, const MethodInfo* method)
{
	Quaternion_t261  V_0 = {0};
	{
		Transform_INTERNAL_get_rotation_m5689(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t261  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" void Transform_set_rotation_m2899 (Transform_t243 * __this, Quaternion_t261  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_rotation_m5690(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_rotation_m5689 (Transform_t243 * __this, Quaternion_t261 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m5689_ftn) (Transform_t243 *, Quaternion_t261 *);
	static Transform_INTERNAL_get_rotation_m5689_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m5689_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_rotation_m5690 (Transform_t243 * __this, Quaternion_t261 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m5690_ftn) (Transform_t243 *, Quaternion_t261 *);
	static Transform_INTERNAL_set_rotation_m5690_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m5690_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" Quaternion_t261  Transform_get_localRotation_m4821 (Transform_t243 * __this, const MethodInfo* method)
{
	Quaternion_t261  V_0 = {0};
	{
		Transform_INTERNAL_get_localRotation_m5691(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t261  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" void Transform_set_localRotation_m2889 (Transform_t243 * __this, Quaternion_t261  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m5692(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_localRotation_m5691 (Transform_t243 * __this, Quaternion_t261 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m5691_ftn) (Transform_t243 *, Quaternion_t261 *);
	static Transform_INTERNAL_get_localRotation_m5691_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m5691_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_localRotation_m5692 (Transform_t243 * __this, Quaternion_t261 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m5692_ftn) (Transform_t243 *, Quaternion_t261 *);
	static Transform_INTERNAL_set_localRotation_m5692_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m5692_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" Vector3_t215  Transform_get_localScale_m3176 (Transform_t243 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Transform_INTERNAL_get_localScale_m5693(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" void Transform_set_localScale_m2813 (Transform_t243 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m5694(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localScale_m5693 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m5693_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_get_localScale_m5693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m5693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localScale_m5694 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m5694_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_set_localScale_m5694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m5694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" Transform_t243 * Transform_get_parent_m2854 (Transform_t243 * __this, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = Transform_get_parentInternal_m5695(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern TypeInfo* RectTransform_t648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral985;
extern "C" void Transform_set_parent_m2841 (Transform_t243 * __this, Transform_t243 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		_stringLiteral985 = il2cpp_codegen_string_literal_from_index(985);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t648 *)IsInst(__this, RectTransform_t648_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogWarning_m4900(NULL /*static, unused*/, _stringLiteral985, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t243 * L_0 = ___value;
		Transform_set_parentInternal_m5696(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C" Transform_t243 * Transform_get_parentInternal_m5695 (Transform_t243 * __this, const MethodInfo* method)
{
	typedef Transform_t243 * (*Transform_get_parentInternal_m5695_ftn) (Transform_t243 *);
	static Transform_get_parentInternal_m5695_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m5695_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C" void Transform_set_parentInternal_m5696 (Transform_t243 * __this, Transform_t243 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m5696_ftn) (Transform_t243 *, Transform_t243 *);
	static Transform_set_parentInternal_m5696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m5696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" void Transform_SetParent_m4816 (Transform_t243 * __this, Transform_t243 * ___parent, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = ___parent;
		Transform_SetParent_m5697(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C" void Transform_SetParent_m5697 (Transform_t243 * __this, Transform_t243 * ___parent, bool ___worldPositionStays, const MethodInfo* method)
{
	typedef void (*Transform_SetParent_m5697_ftn) (Transform_t243 *, Transform_t243 *, bool);
	static Transform_SetParent_m5697_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m5697_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent, ___worldPositionStays);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C" Matrix4x4_t242  Transform_get_worldToLocalMatrix_m4872 (Transform_t243 * __this, const MethodInfo* method)
{
	Matrix4x4_t242  V_0 = {0};
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m5698(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t242  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m5698 (Transform_t243 * __this, Matrix4x4_t242 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m5698_ftn) (Transform_t243 *, Matrix4x4_t242 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m5698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m5698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Translate_m3196 (Transform_t243 * __this, Vector3_t215  ___translation, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		int32_t L_0 = ___relativeTo;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t215  L_1 = Transform_get_position_m2894(__this, /*hidden argument*/NULL);
		Vector3_t215  L_2 = ___translation;
		Vector3_t215  L_3 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m2902(__this, L_3, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_001d:
	{
		Vector3_t215  L_4 = Transform_get_position_m2894(__this, /*hidden argument*/NULL);
		Vector3_t215  L_5 = ___translation;
		Vector3_t215  L_6 = Transform_TransformDirection_m5702(__this, L_5, /*hidden argument*/NULL);
		Vector3_t215  L_7 = Vector3_op_Addition_m2901(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m2902(__this, L_7, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern "C" void Transform_Translate_m3231 (Transform_t243 * __this, float ___x, float ___y, float ___z, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		int32_t L_3 = V_0;
		Transform_Translate_m5699(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C" void Transform_Translate_m5699 (Transform_t243 * __this, float ___x, float ___y, float ___z, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		float L_0 = ___x;
		float L_1 = ___y;
		float L_2 = ___z;
		Vector3_t215  L_3 = {0};
		Vector3__ctor_m2812(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo;
		Transform_Translate_m3196(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C" void Transform_Rotate_m3154 (Transform_t243 * __this, Vector3_t215  ___eulerAngles, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		Vector3_t215  L_0 = ___eulerAngles;
		int32_t L_1 = V_0;
		Transform_Rotate_m3332(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C" void Transform_Rotate_m3332 (Transform_t243 * __this, Vector3_t215  ___eulerAngles, int32_t ___relativeTo, const MethodInfo* method)
{
	Quaternion_t261  V_0 = {0};
	{
		float L_0 = ((&___eulerAngles)->___x_1);
		float L_1 = ((&___eulerAngles)->___y_2);
		float L_2 = ((&___eulerAngles)->___z_3);
		Quaternion_t261  L_3 = Quaternion_Euler_m5385(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0039;
		}
	}
	{
		Quaternion_t261  L_5 = Transform_get_localRotation_m4821(__this, /*hidden argument*/NULL);
		Quaternion_t261  L_6 = V_0;
		Quaternion_t261  L_7 = Quaternion_op_Multiply_m2898(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m2889(__this, L_7, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_0039:
	{
		Quaternion_t261  L_8 = Transform_get_rotation_m2897(__this, /*hidden argument*/NULL);
		Quaternion_t261  L_9 = Transform_get_rotation_m2897(__this, /*hidden argument*/NULL);
		Quaternion_t261  L_10 = Quaternion_Inverse_m4890(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t261  L_11 = V_0;
		Quaternion_t261  L_12 = Quaternion_op_Multiply_m2898(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t261  L_13 = Transform_get_rotation_m2897(__this, /*hidden argument*/NULL);
		Quaternion_t261  L_14 = Quaternion_op_Multiply_m2898(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t261  L_15 = Quaternion_op_Multiply_m2898(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m2899(__this, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C" void Transform_Rotate_m2985 (Transform_t243 * __this, float ___xAngle, float ___yAngle, float ___zAngle, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 1;
		float L_0 = ___xAngle;
		float L_1 = ___yAngle;
		float L_2 = ___zAngle;
		int32_t L_3 = V_0;
		Transform_Rotate_m5700(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C" void Transform_Rotate_m5700 (Transform_t243 * __this, float ___xAngle, float ___yAngle, float ___zAngle, int32_t ___relativeTo, const MethodInfo* method)
{
	{
		float L_0 = ___xAngle;
		float L_1 = ___yAngle;
		float L_2 = ___zAngle;
		Vector3_t215  L_3 = {0};
		Vector3__ctor_m2812(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo;
		Transform_Rotate_m3332(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C" void Transform_LookAt_m3143 (Transform_t243 * __this, Transform_t243 * ___target, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Vector3_t215  L_0 = Vector3_get_up_m3249(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_t243 * L_1 = ___target;
		Vector3_t215  L_2 = V_0;
		Transform_LookAt_m3312(__this, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern "C" void Transform_LookAt_m3312 (Transform_t243 * __this, Transform_t243 * ___target, Vector3_t215  ___worldUp, const MethodInfo* method)
{
	{
		Transform_t243 * L_0 = ___target;
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Transform_t243 * L_2 = ___target;
		NullCheck(L_2);
		Vector3_t215  L_3 = Transform_get_position_m2894(L_2, /*hidden argument*/NULL);
		Vector3_t215  L_4 = ___worldUp;
		Transform_LookAt_m3313(__this, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void Transform_LookAt_m3313 (Transform_t243 * __this, Vector3_t215  ___worldPosition, Vector3_t215  ___worldUp, const MethodInfo* method)
{
	{
		Transform_INTERNAL_CALL_LookAt_m5701(NULL /*static, unused*/, __this, (&___worldPosition), (&___worldUp), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C" void Transform_LookAt_m2984 (Transform_t243 * __this, Vector3_t215  ___worldPosition, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Vector3_t215  L_0 = Vector3_get_up_m3249(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Transform_INTERNAL_CALL_LookAt_m5701(NULL /*static, unused*/, __this, (&___worldPosition), (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_CALL_LookAt_m5701 (Object_t * __this /* static, unused */, Transform_t243 * ___self, Vector3_t215 * ___worldPosition, Vector3_t215 * ___worldUp, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_CALL_LookAt_m5701_ftn) (Transform_t243 *, Vector3_t215 *, Vector3_t215 *);
	static Transform_INTERNAL_CALL_LookAt_m5701_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_LookAt_m5701_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___worldPosition, ___worldUp);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C" Vector3_t215  Transform_TransformDirection_m5702 (Transform_t243 * __this, Vector3_t215  ___direction, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Transform_INTERNAL_CALL_TransformDirection_m5703(NULL /*static, unused*/, __this, (&___direction), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t215  Transform_INTERNAL_CALL_TransformDirection_m5703 (Object_t * __this /* static, unused */, Transform_t243 * ___self, Vector3_t215 * ___direction, const MethodInfo* method)
{
	typedef Vector3_t215  (*Transform_INTERNAL_CALL_TransformDirection_m5703_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_CALL_TransformDirection_m5703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m5703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___direction);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C" Vector3_t215  Transform_InverseTransformDirection_m3253 (Transform_t243 * __this, Vector3_t215  ___direction, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Transform_INTERNAL_CALL_InverseTransformDirection_m5704(NULL /*static, unused*/, __this, (&___direction), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t215  Transform_INTERNAL_CALL_InverseTransformDirection_m5704 (Object_t * __this /* static, unused */, Transform_t243 * ___self, Vector3_t215 * ___direction, const MethodInfo* method)
{
	typedef Vector3_t215  (*Transform_INTERNAL_CALL_InverseTransformDirection_m5704_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_CALL_InverseTransformDirection_m5704_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformDirection_m5704_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___direction);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t215  Transform_TransformPoint_m4891 (Transform_t243 * __this, Vector3_t215  ___position, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Transform_INTERNAL_CALL_TransformPoint_m5705(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t215  Transform_INTERNAL_CALL_TransformPoint_m5705 (Object_t * __this /* static, unused */, Transform_t243 * ___self, Vector3_t215 * ___position, const MethodInfo* method)
{
	typedef Vector3_t215  (*Transform_INTERNAL_CALL_TransformPoint_m5705_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_CALL_TransformPoint_m5705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m5705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t215  Transform_InverseTransformPoint_m4790 (Transform_t243 * __this, Vector3_t215  ___position, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Transform_INTERNAL_CALL_InverseTransformPoint_m5706(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t215  Transform_INTERNAL_CALL_InverseTransformPoint_m5706 (Object_t * __this /* static, unused */, Transform_t243 * ___self, Vector3_t215 * ___position, const MethodInfo* method)
{
	typedef Vector3_t215  (*Transform_INTERNAL_CALL_InverseTransformPoint_m5706_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m5706_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m5706_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C" Transform_t243 * Transform_get_root_m3225 (Transform_t243 * __this, const MethodInfo* method)
{
	typedef Transform_t243 * (*Transform_get_root_m3225_ftn) (Transform_t243 *);
	static Transform_get_root_m3225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m3225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" int32_t Transform_get_childCount_m4947 (Transform_t243 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m4947_ftn) (Transform_t243 *);
	static Transform_get_childCount_m4947_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m4947_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C" void Transform_SetAsFirstSibling_m4817 (Transform_t243 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m4817_ftn) (Transform_t243 *);
	static Transform_SetAsFirstSibling_m4817_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m4817_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern "C" Vector3_t215  Transform_get_lossyScale_m2872 (Transform_t243 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Transform_INTERNAL_get_lossyScale_m5707(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_lossyScale_m5707 (Transform_t243 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_lossyScale_m5707_ftn) (Transform_t243 *, Vector3_t215 *);
	static Transform_INTERNAL_get_lossyScale_m5707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_lossyScale_m5707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C" bool Transform_IsChildOf_m3220 (Transform_t243 * __this, Transform_t243 * ___parent, const MethodInfo* method)
{
	typedef bool (*Transform_IsChildOf_m3220_ftn) (Transform_t243 *, Transform_t243 *);
	static Transform_IsChildOf_m3220_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m3220_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	return _il2cpp_icall_func(__this, ___parent);
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern TypeInfo* Enumerator_t922_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_GetEnumerator_m5708 (Transform_t243 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t922_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(675);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t922 * L_0 = (Enumerator_t922 *)il2cpp_codegen_object_new (Enumerator_t922_il2cpp_TypeInfo_var);
		Enumerator__ctor_m5680(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" Transform_t243 * Transform_GetChild_m4946 (Transform_t243 * __this, int32_t ___index, const MethodInfo* method)
{
	typedef Transform_t243 * (*Transform_GetChild_m4946_ftn) (Transform_t243 *, int32_t);
	static Transform_GetChild_m4946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m4946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// UnityEngine.Time
#include "UnityEngine_UnityEngine_Time.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"



// System.Single UnityEngine.Time::get_time()
extern "C" float Time_get_time_m3211 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_time_m3211_ftn) ();
	static Time_get_time_m3211_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m3211_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" float Time_get_deltaTime_m2723 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m2723_ftn) ();
	static Time_get_deltaTime_m2723_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m2723_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C" float Time_get_unscaledTime_m2963 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m2963_ftn) ();
	static Time_get_unscaledTime_m2963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m2963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C" float Time_get_unscaledDeltaTime_m4656 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m4656_ftn) ();
	static Time_get_unscaledDeltaTime_m4656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m4656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_timeScale()
extern "C" float Time_get_timeScale_m2753 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_timeScale_m2753_ftn) ();
	static Time_get_timeScale_m2753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_timeScale_m2753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_timeScale()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C" float Time_get_realtimeSinceStartup_m3344 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m3344_ftn) ();
	static Time_get_realtimeSinceStartup_m3344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m3344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	return _il2cpp_icall_func();
}
// UnityEngine.Random
#include "UnityEngine_UnityEngine_Random.h"
#ifndef _MSC_VER
#else
#endif



// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" float Random_Range_m3156 (Object_t * __this /* static, unused */, float ___min, float ___max, const MethodInfo* method)
{
	typedef float (*Random_Range_m3156_ftn) (float, float);
	static Random_Range_m3156_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m3156_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" int32_t Random_Range_m3349 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min;
		int32_t L_1 = ___max;
		int32_t L_2 = Random_RandomRangeInt_m5709(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C" int32_t Random_RandomRangeInt_m5709 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m5709_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m5709_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m5709_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Single UnityEngine.Random::get_value()
extern "C" float Random_get_value_m2769 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Random_get_value_m2769_ftn) ();
	static Random_get_value_m2769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_value_m2769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_value()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
extern "C" Vector3_t215  Random_get_onUnitSphere_m2767 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Random_INTERNAL_get_onUnitSphere_m5710(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)
extern "C" void Random_INTERNAL_get_onUnitSphere_m5710 (Object_t * __this /* static, unused */, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_onUnitSphere_m5710_ftn) (Vector3_t215 *);
	static Random_INTERNAL_get_onUnitSphere_m5710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_onUnitSphere_m5710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_onUnitSphere(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m5711 (YieldInstruction_t836 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
void YieldInstruction_t836_marshal(const YieldInstruction_t836& unmarshaled, YieldInstruction_t836_marshaled& marshaled)
{
}
void YieldInstruction_t836_marshal_back(const YieldInstruction_t836_marshaled& marshaled, YieldInstruction_t836& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
void YieldInstruction_t836_marshal_cleanup(YieldInstruction_t836_marshaled& marshaled)
{
}
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.PlayerPrefsException
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"

// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"


// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C" void PlayerPrefsException__ctor_m5712 (PlayerPrefsException_t925 * __this, String_t* ___error, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error;
		Exception__ctor_m6359(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.PlayerPrefs
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"



// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C" bool PlayerPrefs_TrySetInt_m5713 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m5713_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m5713_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m5713_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern TypeInfo* PlayerPrefsException_t925_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral986;
extern "C" void PlayerPrefs_SetInt_m3095 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t925_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(676);
		_stringLiteral986 = il2cpp_codegen_string_literal_from_index(986);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		int32_t L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetInt_m5713(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t925 * L_3 = (PlayerPrefsException_t925 *)il2cpp_codegen_object_new (PlayerPrefsException_t925_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m5712(L_3, _stringLiteral986, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" int32_t PlayerPrefs_GetInt_m5714 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___defaultValue, const MethodInfo* method)
{
	typedef int32_t (*PlayerPrefs_GetInt_m5714_ftn) (String_t*, int32_t);
	static PlayerPrefs_GetInt_m5714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetInt_m5714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C" int32_t PlayerPrefs_GetInt_m3094 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___key;
		int32_t L_1 = V_0;
		int32_t L_2 = PlayerPrefs_GetInt_m5714(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C" String_t* PlayerPrefs_GetString_m5715 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___defaultValue, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m5715_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m5715_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m5715_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayerPrefs_GetString_m5716 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ___key;
		String_t* L_2 = V_0;
		String_t* L_3 = PlayerPrefs_GetString_m5715(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C" bool PlayerPrefs_HasKey_m3093 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m3093_ftn) (String_t*);
	static PlayerPrefs_HasKey_m3093_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m3093_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key);
}
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystem.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ParticleSystem
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"



// System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
extern "C" void ParticleSystem_set_enableEmission_m3145 (ParticleSystem_t534 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_enableEmission_m3145_ftn) (ParticleSystem_t534 *, bool);
	static ParticleSystem_set_enableEmission_m3145_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_enableEmission_m3145_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_Particle.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Particle
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"



// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t215  Particle_get_position_m5717 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Position_0);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C" void Particle_set_position_m5718 (Particle_t927 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		__this->___m_Position_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t215  Particle_get_velocity_m5719 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Velocity_1);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m5720 (Particle_t927 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___value;
		__this->___m_Velocity_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m5721 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Energy_5);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C" void Particle_set_energy_m5722 (Particle_t927 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Energy_5 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m5723 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_StartEnergy_6);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m5724 (Particle_t927 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_StartEnergy_6 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m5725 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Size_2);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m5726 (Particle_t927 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Size_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m5727 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Rotation_3);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m5728 (Particle_t927 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Rotation_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m5729 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AngularVelocity_4);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m5730 (Particle_t927 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_AngularVelocity_4 = L_0;
		return;
	}
}
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t6  Particle_get_color_m5731 (Particle_t927 * __this, const MethodInfo* method)
{
	{
		Color_t6  L_0 = (__this->___m_Color_7);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C" void Particle_set_color_m5732 (Particle_t927 * __this, Color_t6  ___value, const MethodInfo* method)
{
	{
		Color_t6  L_0 = ___value;
		__this->___m_Color_7 = L_0;
		return;
	}
}
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ForceMode
#include "UnityEngine_UnityEngine_ForceModeMethodDeclarations.h"



// UnityEngine.Physics
#include "UnityEngine_UnityEngine_Physics.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"

// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"


// UnityEngine.Vector3 UnityEngine.Physics::get_gravity()
extern "C" Vector3_t215  Physics_get_gravity_m3272 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Physics_INTERNAL_get_gravity_m5733(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)
extern "C" void Physics_INTERNAL_get_gravity_m5733 (Object_t * __this /* static, unused */, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Physics_INTERNAL_get_gravity_m5733_ftn) (Vector3_t215 *);
	static Physics_INTERNAL_get_gravity_m5733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_get_gravity_m5733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_get_gravity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Internal_Raycast_m5734 (Object_t * __this /* static, unused */, Vector3_t215  ___origin, Vector3_t215  ___direction, RaycastHit_t482 * ___hitInfo, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	{
		RaycastHit_t482 * L_0 = ___hitInfo;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layermask;
		bool L_3 = Physics_INTERNAL_CALL_Internal_Raycast_m5735(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_INTERNAL_CALL_Internal_Raycast_m5735 (Object_t * __this /* static, unused */, Vector3_t215 * ___origin, Vector3_t215 * ___direction, RaycastHit_t482 * ___hitInfo, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m5735_ftn) (Vector3_t215 *, Vector3_t215 *, RaycastHit_t482 *, float, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m5735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m5735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___hitInfo, ___maxDistance, ___layermask);
}
// System.Boolean UnityEngine.Physics::Internal_CapsuleCast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Internal_CapsuleCast_m5736 (Object_t * __this /* static, unused */, Vector3_t215  ___point1, Vector3_t215  ___point2, float ___radius, Vector3_t215  ___direction, RaycastHit_t482 * ___hitInfo, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	{
		float L_0 = ___radius;
		RaycastHit_t482 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layermask;
		bool L_4 = Physics_INTERNAL_CALL_Internal_CapsuleCast_m5737(NULL /*static, unused*/, (&___point1), (&___point2), L_0, (&___direction), L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_INTERNAL_CALL_Internal_CapsuleCast_m5737 (Object_t * __this /* static, unused */, Vector3_t215 * ___point1, Vector3_t215 * ___point2, float ___radius, Vector3_t215 * ___direction, RaycastHit_t482 * ___hitInfo, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_CapsuleCast_m5737_ftn) (Vector3_t215 *, Vector3_t215 *, float, Vector3_t215 *, RaycastHit_t482 *, float, int32_t);
	static Physics_INTERNAL_CALL_Internal_CapsuleCast_m5737_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_CapsuleCast_m5737_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_CapsuleCast(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___point1, ___point2, ___radius, ___direction, ___hitInfo, ___maxDistance, ___layermask);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m5738 (Object_t * __this /* static, unused */, Vector3_t215  ___origin, Vector3_t215  ___direction, RaycastHit_t482 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = ___origin;
		Vector3_t215  L_1 = ___direction;
		RaycastHit_t482 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Internal_Raycast_m5734(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C" bool Physics_Raycast_m2983 (Object_t * __this /* static, unused */, Ray_t465  ___ray, RaycastHit_t482 * ___hitInfo, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = ((int32_t)-5);
		V_1 = (std::numeric_limits<float>::infinity());
		Ray_t465  L_0 = ___ray;
		RaycastHit_t482 * L_1 = ___hitInfo;
		float L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m4713(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m4713 (Object_t * __this /* static, unused */, Ray_t465  ___ray, RaycastHit_t482 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Ray_get_origin_m4640((&___ray), /*hidden argument*/NULL);
		Vector3_t215  L_1 = Ray_get_direction_m3242((&___ray), /*hidden argument*/NULL);
		RaycastHit_t482 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Raycast_m5738(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern "C" RaycastHitU5BU5D_t540* Physics_RaycastAll_m3218 (Object_t * __this /* static, unused */, Ray_t465  ___ray, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = ((int32_t)-5);
		V_1 = (std::numeric_limits<float>::infinity());
		Ray_t465  L_0 = ___ray;
		float L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t540* L_3 = Physics_RaycastAll_m3263(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t540* Physics_RaycastAll_m3263 (Object_t * __this /* static, unused */, Ray_t465  ___ray, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = Ray_get_origin_m4640((&___ray), /*hidden argument*/NULL);
		Vector3_t215  L_1 = Ray_get_direction_m3242((&___ray), /*hidden argument*/NULL);
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layerMask;
		RaycastHitU5BU5D_t540* L_4 = Physics_RaycastAll_m5739(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t540* Physics_RaycastAll_m5739 (Object_t * __this /* static, unused */, Vector3_t215  ___origin, Vector3_t215  ___direction, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance;
		int32_t L_1 = ___layermask;
		RaycastHitU5BU5D_t540* L_2 = Physics_INTERNAL_CALL_RaycastAll_m5740(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t540* Physics_INTERNAL_CALL_RaycastAll_m5740 (Object_t * __this /* static, unused */, Vector3_t215 * ___origin, Vector3_t215 * ___direction, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t540* (*Physics_INTERNAL_CALL_RaycastAll_m5740_ftn) (Vector3_t215 *, Vector3_t215 *, float, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m5740_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m5740_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___maxDistance, ___layermask);
}
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Ray,System.Single,System.Single,System.Int32)
extern "C" bool Physics_SphereCast_m3257 (Object_t * __this /* static, unused */, Ray_t465  ___ray, float ___radius, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	RaycastHit_t482  V_0 = {0};
	{
		Vector3_t215  L_0 = Ray_get_origin_m4640((&___ray), /*hidden argument*/NULL);
		Vector3_t215  L_1 = Ray_get_origin_m4640((&___ray), /*hidden argument*/NULL);
		float L_2 = ___radius;
		Vector3_t215  L_3 = Ray_get_direction_m3242((&___ray), /*hidden argument*/NULL);
		float L_4 = ___maxDistance;
		int32_t L_5 = ___layerMask;
		bool L_6 = Physics_Internal_CapsuleCast_m5736(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (&V_0), L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"



// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
extern "C" Vector3_t215  Rigidbody_get_velocity_m3251 (Rigidbody_t325 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Rigidbody_INTERNAL_get_velocity_m5741(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C" void Rigidbody_set_velocity_m3252 (Rigidbody_t325 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m5742(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_get_velocity_m5741 (Rigidbody_t325 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_velocity_m5741_ftn) (Rigidbody_t325 *, Vector3_t215 *);
	static Rigidbody_INTERNAL_get_velocity_m5741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_velocity_m5741_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_set_velocity_m5742 (Rigidbody_t325 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m5742_ftn) (Rigidbody_t325 *, Vector3_t215 *);
	static Rigidbody_INTERNAL_set_velocity_m5742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m5742_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
extern "C" void Rigidbody_set_useGravity_m3265 (Rigidbody_t325 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_useGravity_m3265_ftn) (Rigidbody_t325 *, bool);
	static Rigidbody_set_useGravity_m3265_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_useGravity_m3265_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
extern "C" void Rigidbody_set_isKinematic_m3116 (Rigidbody_t325 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_isKinematic_m3116_ftn) (Rigidbody_t325 *, bool);
	static Rigidbody_set_isKinematic_m3116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_isKinematic_m3116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_isKinematic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C" void Rigidbody_AddForce_m3243 (Rigidbody_t325 * __this, Vector3_t215  ___force, int32_t ___mode, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode;
		Rigidbody_INTERNAL_CALL_AddForce_m5743(NULL /*static, unused*/, __this, (&___force), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C" void Rigidbody_AddForce_m3197 (Rigidbody_t325 * __this, Vector3_t215  ___force, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m5743(NULL /*static, unused*/, __this, (&___force), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C" void Rigidbody_INTERNAL_CALL_AddForce_m5743 (Object_t * __this /* static, unused */, Rigidbody_t325 * ___self, Vector3_t215 * ___force, int32_t ___mode, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m5743_ftn) (Rigidbody_t325 *, Vector3_t215 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m5743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m5743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self, ___force, ___mode);
}
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_position()
extern "C" Vector3_t215  Rigidbody_get_position_m3254 (Rigidbody_t325 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Rigidbody_INTERNAL_get_position_m5744(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody::set_position(UnityEngine.Vector3)
extern "C" void Rigidbody_set_position_m3268 (Rigidbody_t325 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_position_m5745(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_get_position_m5744 (Rigidbody_t325 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_get_position_m5744_ftn) (Rigidbody_t325 *, Vector3_t215 *);
	static Rigidbody_INTERNAL_get_position_m5744_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_get_position_m5744_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_set_position_m5745 (Rigidbody_t325 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_position_m5745_ftn) (Rigidbody_t325 *, Vector3_t215 *);
	static Rigidbody_INTERNAL_set_position_m5745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_position_m5745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::set_rotation(UnityEngine.Quaternion)
extern "C" void Rigidbody_set_rotation_m3283 (Rigidbody_t325 * __this, Quaternion_t261  ___value, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_rotation_m5746(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C" void Rigidbody_INTERNAL_set_rotation_m5746 (Rigidbody_t325 * __this, Quaternion_t261 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_rotation_m5746_ftn) (Rigidbody_t325 *, Quaternion_t261 *);
	static Rigidbody_INTERNAL_set_rotation_m5746_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_rotation_m5746_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C" void Rigidbody_MovePosition_m3329 (Rigidbody_t325 * __this, Vector3_t215  ___position, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m5747(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C" void Rigidbody_INTERNAL_CALL_MovePosition_m5747 (Object_t * __this /* static, unused */, Rigidbody_t325 * ___self, Vector3_t215 * ___position, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m5747_ftn) (Rigidbody_t325 *, Vector3_t215 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m5747_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m5747_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___position);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C" void Rigidbody_MoveRotation_m3331 (Rigidbody_t325 * __this, Quaternion_t261  ___rot, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m5748(NULL /*static, unused*/, __this, (&___rot), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C" void Rigidbody_INTERNAL_CALL_MoveRotation_m5748 (Object_t * __this /* static, unused */, Rigidbody_t325 * ___self, Quaternion_t261 * ___rot, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m5748_ftn) (Rigidbody_t325 *, Quaternion_t261 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m5748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m5748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self, ___rot);
}
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C" void Rigidbody_Sleep_m3194 (Rigidbody_t325 * __this, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_Sleep_m5749(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
extern "C" void Rigidbody_INTERNAL_CALL_Sleep_m5749 (Object_t * __this /* static, unused */, Rigidbody_t325 * ___self, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_Sleep_m5749_ftn) (Rigidbody_t325 *);
	static Rigidbody_INTERNAL_CALL_Sleep_m5749_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_Sleep_m5749_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)");
	_il2cpp_icall_func(___self);
}
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"

// UnityEngine.PhysicMaterial
#include "UnityEngine_UnityEngine_PhysicMaterial.h"


// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
extern "C" void Collider_set_enabled_m3119 (Collider_t319 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Collider_set_enabled_m3119_ftn) (Collider_t319 *, bool);
	static Collider_set_enabled_m3119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_enabled_m3119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C" Rigidbody_t325 * Collider_get_attachedRigidbody_m5750 (Collider_t319 * __this, const MethodInfo* method)
{
	typedef Rigidbody_t325 * (*Collider_get_attachedRigidbody_m5750_ftn) (Collider_t319 *);
	static Collider_get_attachedRigidbody_m5750_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_attachedRigidbody_m5750_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Collider::get_isTrigger()
extern "C" bool Collider_get_isTrigger_m3267 (Collider_t319 * __this, const MethodInfo* method)
{
	typedef bool (*Collider_get_isTrigger_m3267_ftn) (Collider_t319 *);
	static Collider_get_isTrigger_m3267_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_isTrigger_m3267_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_isTrigger()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)
extern "C" void Collider_set_material_m3269 (Collider_t319 * __this, PhysicMaterial_t410 * ___value, const MethodInfo* method)
{
	typedef void (*Collider_set_material_m3269_ftn) (Collider_t319 *, PhysicMaterial_t410 *);
	static Collider_set_material_m3269_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_set_material_m3269_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::set_material(UnityEngine.PhysicMaterial)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Collider::Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_Internal_Raycast_m5751 (Object_t * __this /* static, unused */, Collider_t319 * ___col, Ray_t465  ___ray, RaycastHit_t482 * ___hitInfo, float ___maxDistance, const MethodInfo* method)
{
	{
		Collider_t319 * L_0 = ___col;
		RaycastHit_t482 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		bool L_3 = Collider_INTERNAL_CALL_Internal_Raycast_m5752(NULL /*static, unused*/, L_0, (&___ray), L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_INTERNAL_CALL_Internal_Raycast_m5752 (Object_t * __this /* static, unused */, Collider_t319 * ___col, Ray_t465 * ___ray, RaycastHit_t482 * ___hitInfo, float ___maxDistance, const MethodInfo* method)
{
	typedef bool (*Collider_INTERNAL_CALL_Internal_Raycast_m5752_ftn) (Collider_t319 *, Ray_t465 *, RaycastHit_t482 *, float);
	static Collider_INTERNAL_CALL_Internal_Raycast_m5752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_INTERNAL_CALL_Internal_Raycast_m5752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::INTERNAL_CALL_Internal_Raycast(UnityEngine.Collider,UnityEngine.Ray&,UnityEngine.RaycastHit&,System.Single)");
	return _il2cpp_icall_func(___col, ___ray, ___hitInfo, ___maxDistance);
}
// System.Boolean UnityEngine.Collider::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C" bool Collider_Raycast_m2794 (Collider_t319 * __this, Ray_t465  ___ray, RaycastHit_t482 * ___hitInfo, float ___maxDistance, const MethodInfo* method)
{
	{
		Ray_t465  L_0 = ___ray;
		RaycastHit_t482 * L_1 = ___hitInfo;
		float L_2 = ___maxDistance;
		bool L_3 = Collider_Internal_Raycast_m5751(NULL /*static, unused*/, __this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxCollider.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxColliderMethodDeclarations.h"



// UnityEngine.CapsuleCollider
#include "UnityEngine_UnityEngine_CapsuleCollider.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CapsuleCollider
#include "UnityEngine_UnityEngine_CapsuleColliderMethodDeclarations.h"



// UnityEngine.Vector3 UnityEngine.CapsuleCollider::get_center()
extern "C" Vector3_t215  CapsuleCollider_get_center_m3260 (CapsuleCollider_t415 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		CapsuleCollider_INTERNAL_get_center_m5753(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.CapsuleCollider::set_center(UnityEngine.Vector3)
extern "C" void CapsuleCollider_set_center_m3250 (CapsuleCollider_t415 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		CapsuleCollider_INTERNAL_set_center_m5754(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CapsuleCollider::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C" void CapsuleCollider_INTERNAL_get_center_m5753 (CapsuleCollider_t415 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_INTERNAL_get_center_m5753_ftn) (CapsuleCollider_t415 *, Vector3_t215 *);
	static CapsuleCollider_INTERNAL_get_center_m5753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_INTERNAL_get_center_m5753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::INTERNAL_get_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CapsuleCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C" void CapsuleCollider_INTERNAL_set_center_m5754 (CapsuleCollider_t415 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_INTERNAL_set_center_m5754_ftn) (CapsuleCollider_t415 *, Vector3_t215 *);
	static CapsuleCollider_INTERNAL_set_center_m5754_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_INTERNAL_set_center_m5754_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::INTERNAL_set_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.CapsuleCollider::get_radius()
extern "C" float CapsuleCollider_get_radius_m3255 (CapsuleCollider_t415 * __this, const MethodInfo* method)
{
	typedef float (*CapsuleCollider_get_radius_m3255_ftn) (CapsuleCollider_t415 *);
	static CapsuleCollider_get_radius_m3255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_radius_m3255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_radius()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.CapsuleCollider::get_height()
extern "C" float CapsuleCollider_get_height_m3248 (CapsuleCollider_t415 * __this, const MethodInfo* method)
{
	typedef float (*CapsuleCollider_get_height_m3248_ftn) (CapsuleCollider_t415 *);
	static CapsuleCollider_get_height_m3248_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_get_height_m3248_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::get_height()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CapsuleCollider::set_height(System.Single)
extern "C" void CapsuleCollider_set_height_m3259 (CapsuleCollider_t415 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*CapsuleCollider_set_height_m3259_ftn) (CapsuleCollider_t415 *, float);
	static CapsuleCollider_set_height_m3259_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CapsuleCollider_set_height_m3259_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CapsuleCollider::set_height(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"



// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t215  RaycastHit_get_point_m3221 (RaycastHit_t482 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Point_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t215  RaycastHit_get_normal_m4650 (RaycastHit_t482 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_Normal_1);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m2795 (RaycastHit_t482 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_3);
		return L_0;
	}
}
// System.Void UnityEngine.RaycastHit::CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2,UnityEngine.Vector3,System.Int32,System.Int32)
extern "C" void RaycastHit_CalculateRaycastTexCoord_m5755 (Object_t * __this /* static, unused */, Vector2_t7 * ___output, Collider_t319 * ___col, Vector2_t7  ___uv, Vector3_t215  ___point, int32_t ___face, int32_t ___index, const MethodInfo* method)
{
	{
		Vector2_t7 * L_0 = ___output;
		Collider_t319 * L_1 = ___col;
		int32_t L_2 = ___face;
		int32_t L_3 = ___index;
		RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m5756(NULL /*static, unused*/, L_0, L_1, (&___uv), (&___point), L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RaycastHit::INTERNAL_CALL_CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2&,UnityEngine.Vector3&,System.Int32,System.Int32)
extern "C" void RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m5756 (Object_t * __this /* static, unused */, Vector2_t7 * ___output, Collider_t319 * ___col, Vector2_t7 * ___uv, Vector3_t215 * ___point, int32_t ___face, int32_t ___index, const MethodInfo* method)
{
	typedef void (*RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m5756_ftn) (Vector2_t7 *, Collider_t319 *, Vector2_t7 *, Vector3_t215 *, int32_t, int32_t);
	static RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m5756_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RaycastHit_INTERNAL_CALL_CalculateRaycastTexCoord_m5756_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RaycastHit::INTERNAL_CALL_CalculateRaycastTexCoord(UnityEngine.Vector2&,UnityEngine.Collider,UnityEngine.Vector2&,UnityEngine.Vector3&,System.Int32,System.Int32)");
	_il2cpp_icall_func(___output, ___col, ___uv, ___point, ___face, ___index);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit::get_textureCoord()
extern "C" Vector2_t7  RaycastHit_get_textureCoord_m2796 (RaycastHit_t482 * __this, const MethodInfo* method)
{
	Vector2_t7  V_0 = {0};
	{
		Collider_t319 * L_0 = RaycastHit_get_collider_m3266(__this, /*hidden argument*/NULL);
		Vector2_t7  L_1 = (__this->___m_UV_4);
		Vector3_t215  L_2 = (__this->___m_Point_0);
		int32_t L_3 = (__this->___m_FaceID_2);
		RaycastHit_CalculateRaycastTexCoord_m5755(NULL /*static, unused*/, (&V_0), L_0, L_1, L_2, L_3, 0, /*hidden argument*/NULL);
		Vector2_t7  L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t319 * RaycastHit_get_collider_m3266 (RaycastHit_t482 * __this, const MethodInfo* method)
{
	{
		Collider_t319 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C" Rigidbody_t325 * RaycastHit_get_rigidbody_m3241 (RaycastHit_t482 * __this, const MethodInfo* method)
{
	Rigidbody_t325 * G_B3_0 = {0};
	{
		Collider_t319 * L_0 = RaycastHit_get_collider_m3266(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider_t319 * L_2 = RaycastHit_get_collider_m3266(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t325 * L_3 = Collider_get_attachedRigidbody_m5750(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody_t325 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C" Transform_t243 * RaycastHit_get_transform_m3219 (RaycastHit_t482 * __this, const MethodInfo* method)
{
	Rigidbody_t325 * V_0 = {0};
	{
		Rigidbody_t325 * L_0 = RaycastHit_get_rigidbody_m3241(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t325 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody_t325 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t243 * L_4 = Component_get_transform_m2760(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider_t319 * L_5 = RaycastHit_get_collider_m3266(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_5, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider_t319 * L_7 = RaycastHit_get_collider_m3266(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t243 * L_8 = Component_get_transform_m2760(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t243 *)NULL;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.PhysicMaterial
#include "UnityEngine_UnityEngine_PhysicMaterialMethodDeclarations.h"



// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPointMethodDeclarations.h"



// UnityEngine.Collision
#include "UnityEngine_UnityEngine_Collision.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Collision
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"



// UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern "C" Vector3_t215  Collision_get_relativeVelocity_m3227 (Collision_t471 * __this, const MethodInfo* method)
{
	{
		Vector3_t215  L_0 = (__this->___m_RelativeVelocity_0);
		return L_0;
	}
}
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C" Rigidbody_t325 * Collision_get_rigidbody_m5757 (Collision_t471 * __this, const MethodInfo* method)
{
	{
		Rigidbody_t325 * L_0 = (__this->___m_Rigidbody_1);
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.Collision::get_collider()
extern "C" Collider_t319 * Collision_get_collider_m3155 (Collision_t471 * __this, const MethodInfo* method)
{
	{
		Collider_t319 * L_0 = (__this->___m_Collider_2);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Collision::get_transform()
extern "C" Transform_t243 * Collision_get_transform_m3226 (Collision_t471 * __this, const MethodInfo* method)
{
	Transform_t243 * G_B3_0 = {0};
	{
		Rigidbody_t325 * L_0 = Collision_get_rigidbody_m5757(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody_t325 * L_2 = Collision_get_rigidbody_m5757(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t243 * L_3 = Component_get_transform_m2760(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider_t319 * L_4 = Collision_get_collider_m3155(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t243 * L_5 = Component_get_transform_m2760(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern "C" GameObject_t256 * Collision_get_gameObject_m3228 (Collision_t471 * __this, const MethodInfo* method)
{
	GameObject_t256 * G_B3_0 = {0};
	{
		Rigidbody_t325 * L_0 = (__this->___m_Rigidbody_1);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Rigidbody_t325 * L_2 = (__this->___m_Rigidbody_1);
		NullCheck(L_2);
		GameObject_t256 * L_3 = Component_get_gameObject_m2778(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002c;
	}

IL_0021:
	{
		Collider_t319 * L_4 = (__this->___m_Collider_2);
		NullCheck(L_4);
		GameObject_t256 * L_5 = Component_get_gameObject_m2778(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Cloth
#include "UnityEngine_UnityEngine_Cloth.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Cloth
#include "UnityEngine_UnityEngine_ClothMethodDeclarations.h"



// System.Void UnityEngine.Cloth::set_externalAcceleration(UnityEngine.Vector3)
extern "C" void Cloth_set_externalAcceleration_m3147 (Cloth_t337 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Cloth_INTERNAL_set_externalAcceleration_m5758(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Cloth::INTERNAL_set_externalAcceleration(UnityEngine.Vector3&)
extern "C" void Cloth_INTERNAL_set_externalAcceleration_m5758 (Cloth_t337 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Cloth_INTERNAL_set_externalAcceleration_m5758_ftn) (Cloth_t337 *, Vector3_t215 *);
	static Cloth_INTERNAL_set_externalAcceleration_m5758_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cloth_INTERNAL_set_externalAcceleration_m5758_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cloth::INTERNAL_set_externalAcceleration(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Cloth::set_randomAcceleration(UnityEngine.Vector3)
extern "C" void Cloth_set_randomAcceleration_m3148 (Cloth_t337 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		Cloth_INTERNAL_set_randomAcceleration_m5759(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Cloth::INTERNAL_set_randomAcceleration(UnityEngine.Vector3&)
extern "C" void Cloth_INTERNAL_set_randomAcceleration_m5759 (Cloth_t337 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Cloth_INTERNAL_set_randomAcceleration_m5759_ftn) (Cloth_t337 *, Vector3_t215 *);
	static Cloth_INTERNAL_set_randomAcceleration_m5759_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cloth_INTERNAL_set_randomAcceleration_m5759_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cloth::INTERNAL_set_randomAcceleration(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Cloth::set_useGravity(System.Boolean)
extern "C" void Cloth_set_useGravity_m3146 (Cloth_t337 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Cloth_set_useGravity_m3146_ftn) (Cloth_t337 *, bool);
	static Cloth_set_useGravity_m3146_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cloth_set_useGravity_m3146_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cloth::set_useGravity(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2D.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Physics2D
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_34.h"
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_34MethodDeclarations.h"


// System.Void UnityEngine.Physics2D::.cctor()
extern TypeInfo* List_1_t932_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t788_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m6397_MethodInfo_var;
extern "C" void Physics2D__cctor_m5760 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t932_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(678);
		Physics2D_t788_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		List_1__ctor_m6397_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484146);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t932 * L_0 = (List_1_t932 *)il2cpp_codegen_object_new (List_1_t932_il2cpp_TypeInfo_var);
		List_1__ctor_m6397(L_0, /*hidden argument*/List_1__ctor_m6397_MethodInfo_var);
		((Physics2D_t788_StaticFields*)Physics2D_t788_il2cpp_TypeInfo_var->static_fields)->___m_LastDisabledRigidbody2D_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t788_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Raycast_m5761 (Object_t * __this /* static, unused */, Vector2_t7  ___origin, Vector2_t7  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t789 * ___raycastHit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t788_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = ___minDepth;
		float L_3 = ___maxDepth;
		RaycastHit2D_t789 * L_4 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t788_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m5762(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m5762 (Object_t * __this /* static, unused */, Vector2_t7 * ___origin, Vector2_t7 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t789 * ___raycastHit, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m5762_ftn) (Vector2_t7 *, Vector2_t7 *, float, int32_t, float, float, RaycastHit2D_t789 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m5762_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m5762_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t788_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t789  Physics2D_Raycast_m4714 (Object_t * __this /* static, unused */, Vector2_t7  ___origin, Vector2_t7  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t788_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t7  L_0 = ___origin;
		Vector2_t7  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t788_il2cpp_TypeInfo_var);
		RaycastHit2D_t789  L_6 = Physics2D_Raycast_m5763(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t788_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t789  Physics2D_Raycast_m5763 (Object_t * __this /* static, unused */, Vector2_t7  ___origin, Vector2_t7  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t788_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t789  V_0 = {0};
	{
		Vector2_t7  L_0 = ___origin;
		Vector2_t7  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = ___minDepth;
		float L_5 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t788_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m5761(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t789  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t788_il2cpp_TypeInfo_var;
extern "C" RaycastHit2DU5BU5D_t787* Physics2D_RaycastAll_m4641 (Object_t * __this /* static, unused */, Vector2_t7  ___origin, Vector2_t7  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t788_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t788_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t787* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m5764(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C" RaycastHit2DU5BU5D_t787* Physics2D_INTERNAL_CALL_RaycastAll_m5764 (Object_t * __this /* static, unused */, Vector2_t7 * ___origin, Vector2_t7 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t787* (*Physics2D_INTERNAL_CALL_RaycastAll_m5764_ftn) (Vector2_t7 *, Vector2_t7 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m5764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m5764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth);
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"

// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2D.h"
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
// UnityEngine.Collider2D
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"


// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t7  RaycastHit2D_get_point_m4644 (RaycastHit2D_t789 * __this, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = (__this->___m_Point_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t7  RaycastHit2D_get_normal_m4645 (RaycastHit2D_t789 * __this, const MethodInfo* method)
{
	{
		Vector2_t7  L_0 = (__this->___m_Normal_2);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m4715 (RaycastHit2D_t789 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Fraction_4);
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t790 * RaycastHit2D_get_collider_m4642 (RaycastHit2D_t789 * __this, const MethodInfo* method)
{
	{
		Collider2D_t790 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" Rigidbody2D_t933 * RaycastHit2D_get_rigidbody_m5765 (RaycastHit2D_t789 * __this, const MethodInfo* method)
{
	Rigidbody2D_t933 * G_B3_0 = {0};
	{
		Collider2D_t790 * L_0 = RaycastHit2D_get_collider_m4642(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t790 * L_2 = RaycastHit2D_get_collider_m4642(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t933 * L_3 = Collider2D_get_attachedRigidbody_m5766(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t933 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" Transform_t243 * RaycastHit2D_get_transform_m4643 (RaycastHit2D_t789 * __this, const MethodInfo* method)
{
	Rigidbody2D_t933 * V_0 = {0};
	{
		Rigidbody2D_t933 * L_0 = RaycastHit2D_get_rigidbody_m5765(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t933 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_1, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t933 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t243 * L_4 = Component_get_transform_m2760(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t790 * L_5 = RaycastHit2D_get_collider_m4642(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_5, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t790 * L_7 = RaycastHit2D_get_collider_m4642(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t243 * L_8 = Component_get_transform_m2760(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t243 *)NULL;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t933 * Collider2D_get_attachedRigidbody_m5766 (Collider2D_t790 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t933 * (*Collider2D_get_attachedRigidbody_m5766_ftn) (Collider2D_t790 *);
	static Collider2D_get_attachedRigidbody_m5766_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m5766_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgent.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgentMethodDeclarations.h"



// System.Boolean UnityEngine.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern "C" bool NavMeshAgent_SetDestination_m3200 (NavMeshAgent_t307 * __this, Vector3_t215  ___target, const MethodInfo* method)
{
	{
		bool L_0 = NavMeshAgent_INTERNAL_CALL_SetDestination_m5767(NULL /*static, unused*/, __this, (&___target), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.NavMeshAgent::INTERNAL_CALL_SetDestination(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)
extern "C" bool NavMeshAgent_INTERNAL_CALL_SetDestination_m5767 (Object_t * __this /* static, unused */, NavMeshAgent_t307 * ___self, Vector3_t215 * ___target, const MethodInfo* method)
{
	typedef bool (*NavMeshAgent_INTERNAL_CALL_SetDestination_m5767_ftn) (NavMeshAgent_t307 *, Vector3_t215 *);
	static NavMeshAgent_INTERNAL_CALL_SetDestination_m5767_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NavMeshAgent_INTERNAL_CALL_SetDestination_m5767_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NavMeshAgent::INTERNAL_CALL_SetDestination(UnityEngine.NavMeshAgent,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___target);
}
// System.Void UnityEngine.NavMeshAgent::set_destination(UnityEngine.Vector3)
extern "C" void NavMeshAgent_set_destination_m3136 (NavMeshAgent_t307 * __this, Vector3_t215  ___value, const MethodInfo* method)
{
	{
		NavMeshAgent_INTERNAL_set_destination_m5768(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.NavMeshAgent::INTERNAL_set_destination(UnityEngine.Vector3&)
extern "C" void NavMeshAgent_INTERNAL_set_destination_m5768 (NavMeshAgent_t307 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*NavMeshAgent_INTERNAL_set_destination_m5768_ftn) (NavMeshAgent_t307 *, Vector3_t215 *);
	static NavMeshAgent_INTERNAL_set_destination_m5768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NavMeshAgent_INTERNAL_set_destination_m5768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NavMeshAgent::INTERNAL_set_destination(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.NavMeshAgent::set_updateRotation(System.Boolean)
extern "C" void NavMeshAgent_set_updateRotation_m3208 (NavMeshAgent_t307 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*NavMeshAgent_set_updateRotation_m3208_ftn) (NavMeshAgent_t307 *, bool);
	static NavMeshAgent_set_updateRotation_m3208_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (NavMeshAgent_set_updateRotation_m3208_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.NavMeshAgent::set_updateRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"



// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void AudioConfigurationChangeHandler__ctor_m5769 (AudioConfigurationChangeHandler_t934 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C" void AudioConfigurationChangeHandler_Invoke_m5770 (AudioConfigurationChangeHandler_t934 * __this, bool ___deviceWasChanged, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m5770((AudioConfigurationChangeHandler_t934 *)__this->___prev_9,___deviceWasChanged, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t934(Il2CppObject* delegate, bool ___deviceWasChanged)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___deviceWasChanged' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___deviceWasChanged);

	// Marshaling cleanup of parameter '___deviceWasChanged' native representation

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t536_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioConfigurationChangeHandler_BeginInvoke_m5771 (AudioConfigurationChangeHandler_t934 * __this, bool ___deviceWasChanged, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t536_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(219);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t536_il2cpp_TypeInfo_var, &___deviceWasChanged);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m5772 (AudioConfigurationChangeHandler_t934 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettings.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioSettings
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"



// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern TypeInfo* AudioSettings_t935_il2cpp_TypeInfo_var;
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m5773 (AudioSettings_t935 * __this, bool ___deviceWasChanged, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioSettings_t935_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(679);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t934 * L_0 = ((AudioSettings_t935_StaticFields*)AudioSettings_t935_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t934 * L_1 = ((AudioSettings_t935_StaticFields*)AudioSettings_t935_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		bool L_2 = ___deviceWasChanged;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m5770(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioClip/PCMReaderCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"



// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMReaderCallback__ctor_m5774 (PCMReaderCallback_t936 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C" void PCMReaderCallback_Invoke_m5775 (PCMReaderCallback_t936 * __this, SingleU5BU5D_t72* ___data, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMReaderCallback_Invoke_m5775((PCMReaderCallback_t936 *)__this->___prev_9,___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SingleU5BU5D_t72* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SingleU5BU5D_t72* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t936(Il2CppObject* delegate, SingleU5BU5D_t72* ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___data' to native representation
	float* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data);

	// Native function invocation
	_il2cpp_pinvoke_func(____data_marshaled);

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C" Object_t * PCMReaderCallback_BeginInvoke_m5776 (PCMReaderCallback_t936 * __this, SingleU5BU5D_t72* ___data, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMReaderCallback_EndInvoke_m5777 (PCMReaderCallback_t936 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioClip/PCMSetPositionCallback
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"



// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMSetPositionCallback__ctor_m5778 (PCMSetPositionCallback_t937 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C" void PCMSetPositionCallback_Invoke_m5779 (PCMSetPositionCallback_t937 * __this, int32_t ___position, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMSetPositionCallback_Invoke_m5779((PCMSetPositionCallback_t937 *)__this->___prev_9,___position, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t937(Il2CppObject* delegate, int32_t ___position)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___position' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___position);

	// Marshaling cleanup of parameter '___position' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern "C" Object_t * PCMSetPositionCallback_BeginInvoke_m5780 (PCMSetPositionCallback_t937 * __this, int32_t ___position, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t478_il2cpp_TypeInfo_var, &___position);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMSetPositionCallback_EndInvoke_m5781 (PCMSetPositionCallback_t937 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClip.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioClip
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"



// System.Single UnityEngine.AudioClip::get_length()
extern "C" float AudioClip_get_length_m3324 (AudioClip_t472 * __this, const MethodInfo* method)
{
	typedef float (*AudioClip_get_length_m3324_ftn) (AudioClip_t472 *);
	static AudioClip_get_length_m3324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_length_m3324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m5782 (AudioClip_t472 * __this, SingleU5BU5D_t72* ___data, const MethodInfo* method)
{
	{
		PCMReaderCallback_t936 * L_0 = (__this->___m_PCMReaderCallback_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t936 * L_1 = (__this->___m_PCMReaderCallback_2);
		SingleU5BU5D_t72* L_2 = ___data;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m5775(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m5783 (AudioClip_t472 * __this, int32_t ___position, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t937 * L_0 = (__this->___m_PCMSetPositionCallback_3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t937 * L_1 = (__this->___m_PCMSetPositionCallback_3);
		int32_t L_2 = ___position;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m5779(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSource.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AudioSource
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"

// System.UInt64
#include "mscorlib_System_UInt64.h"


// System.Single UnityEngine.AudioSource::get_volume()
extern "C" float AudioSource_get_volume_m3307 (AudioSource_t339 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_volume_m3307_ftn) (AudioSource_t339 *);
	static AudioSource_get_volume_m3307_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m3307_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C" void AudioSource_set_volume_m3151 (AudioSource_t339 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_volume_m3151_ftn) (AudioSource_t339 *, float);
	static AudioSource_set_volume_m3151_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m3151_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C" float AudioSource_get_pitch_m3308 (AudioSource_t339 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_pitch_m3308_ftn) (AudioSource_t339 *);
	static AudioSource_get_pitch_m3308_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_pitch_m3308_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_pitch()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C" void AudioSource_set_pitch_m3309 (AudioSource_t339 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_pitch_m3309_ftn) (AudioSource_t339 *, float);
	static AudioSource_set_pitch_m3309_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m3309_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C" AudioClip_t472 * AudioSource_get_clip_m3323 (AudioSource_t339 * __this, const MethodInfo* method)
{
	typedef AudioClip_t472 * (*AudioSource_get_clip_m3323_ftn) (AudioSource_t339 *);
	static AudioSource_get_clip_m3323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m3323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C" void AudioSource_set_clip_m3152 (AudioSource_t339 * __this, AudioClip_t472 * ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m3152_ftn) (AudioSource_t339 *, AudioClip_t472 *);
	static AudioSource_set_clip_m3152_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m3152_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C" void AudioSource_Play_m5784 (AudioSource_t339 * __this, uint64_t ___delay, const MethodInfo* method)
{
	typedef void (*AudioSource_Play_m5784_ftn) (AudioSource_t339 *, uint64_t);
	static AudioSource_Play_m5784_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m5784_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C" void AudioSource_Play_m3115 (AudioSource_t339 * __this, const MethodInfo* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)0));
		uint64_t L_0 = V_0;
		AudioSource_Play_m5784(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::Stop()
extern "C" void AudioSource_Stop_m3233 (AudioSource_t339 * __this, const MethodInfo* method)
{
	typedef void (*AudioSource_Stop_m3233_ftn) (AudioSource_t339 *);
	static AudioSource_Stop_m3233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Stop_m3233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Stop()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C" bool AudioSource_get_isPlaying_m3114 (AudioSource_t339 * __this, const MethodInfo* method)
{
	typedef bool (*AudioSource_get_isPlaying_m3114_ftn) (AudioSource_t339 *);
	static AudioSource_get_isPlaying_m3114_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_isPlaying_m3114_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C" void AudioSource_PlayOneShot_m5785 (AudioSource_t339 * __this, AudioClip_t472 * ___clip, float ___volumeScale, const MethodInfo* method)
{
	typedef void (*AudioSource_PlayOneShot_m5785_ftn) (AudioSource_t339 *, AudioClip_t472 *, float);
	static AudioSource_PlayOneShot_m5785_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShot_m5785_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip, ___volumeScale);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C" void AudioSource_PlayOneShot_m3333 (AudioSource_t339 * __this, AudioClip_t472 * ___clip, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t472 * L_0 = ___clip;
		float L_1 = V_0;
		AudioSource_PlayOneShot_m5785(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern "C" void AudioSource_set_playOnAwake_m3322 (AudioSource_t339 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*AudioSource_set_playOnAwake_m3322_ftn) (AudioSource_t339 *, bool);
	static AudioSource_set_playOnAwake_m3322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_playOnAwake_m3322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_playOnAwake(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"



// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m5786 (WebCamDevice_t938 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m5787 (WebCamDevice_t938 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Flags_1);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
void WebCamDevice_t938_marshal(const WebCamDevice_t938& unmarshaled, WebCamDevice_t938_marshaled& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Name_0);
	marshaled.___m_Flags_1 = unmarshaled.___m_Flags_1;
}
void WebCamDevice_t938_marshal_back(const WebCamDevice_t938_marshaled& marshaled, WebCamDevice_t938& unmarshaled)
{
	unmarshaled.___m_Name_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0);
	unmarshaled.___m_Flags_1 = marshaled.___m_Flags_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
void WebCamDevice_t938_marshal_cleanup(WebCamDevice_t938_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationEventSource
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"



// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEvent.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationEvent
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"

// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationState.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"


// System.Void UnityEngine.AnimationEvent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AnimationEvent__ctor_m5788 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		__this->___m_Time_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_FunctionName_1 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_StringParameter_2 = L_1;
		__this->___m_ObjectReferenceParameter_3 = (Object_t473 *)NULL;
		__this->___m_FloatParameter_4 = (0.0f);
		__this->___m_IntParameter_5 = 0;
		__this->___m_MessageOptions_6 = 0;
		__this->___m_Source_7 = 0;
		__this->___m_StateSender_8 = (AnimationState_t940 *)NULL;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C" String_t* AnimationEvent_get_data_m5789 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C" void AnimationEvent_set_data_m5790 (AnimationEvent_t941 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C" String_t* AnimationEvent_get_stringParameter_m5791 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C" void AnimationEvent_set_stringParameter_m5792 (AnimationEvent_t941 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C" float AnimationEvent_get_floatParameter_m5793 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatParameter_4);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C" void AnimationEvent_set_floatParameter_m5794 (AnimationEvent_t941 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FloatParameter_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C" int32_t AnimationEvent_get_intParameter_m5795 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntParameter_5);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C" void AnimationEvent_set_intParameter_m5796 (AnimationEvent_t941 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_IntParameter_5 = L_0;
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C" Object_t473 * AnimationEvent_get_objectReferenceParameter_m5797 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		Object_t473 * L_0 = (__this->___m_ObjectReferenceParameter_3);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C" void AnimationEvent_set_objectReferenceParameter_m5798 (AnimationEvent_t941 * __this, Object_t473 * ___value, const MethodInfo* method)
{
	{
		Object_t473 * L_0 = ___value;
		__this->___m_ObjectReferenceParameter_3 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C" String_t* AnimationEvent_get_functionName_m5799 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_FunctionName_1);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C" void AnimationEvent_set_functionName_m5800 (AnimationEvent_t941 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_FunctionName_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C" float AnimationEvent_get_time_m5801 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Time_0);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C" void AnimationEvent_set_time_m5802 (AnimationEvent_t941 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Time_0 = L_0;
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C" int32_t AnimationEvent_get_messageOptions_m5803 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_MessageOptions_6);
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C" void AnimationEvent_set_messageOptions_m5804 (AnimationEvent_t941 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_MessageOptions_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C" bool AnimationEvent_get_isFiredByLegacy_m5805 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C" bool AnimationEvent_get_isFiredByAnimator_m5806 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern Il2CppCodeGenString* _stringLiteral987;
extern "C" AnimationState_t940 * AnimationEvent_get_animationState_m5807 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral987 = il2cpp_codegen_string_literal_from_index(987);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m5805(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral987, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t940 * L_1 = (__this->___m_StateSender_8);
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppCodeGenString* _stringLiteral988;
extern "C" AnimatorStateInfo_t542  AnimationEvent_get_animatorStateInfo_m5808 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral988 = il2cpp_codegen_string_literal_from_index(988);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m5806(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral988, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t542  L_1 = (__this->___m_AnimatorStateInfo_9);
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppCodeGenString* _stringLiteral989;
extern "C" AnimatorClipInfo_t942  AnimationEvent_get_animatorClipInfo_m5809 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral989 = il2cpp_codegen_string_literal_from_index(989);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m5806(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m2830(NULL /*static, unused*/, _stringLiteral989, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t942  L_1 = (__this->___m_AnimatorClipInfo_10);
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C" int32_t AnimationEvent_GetHash_m5810 (AnimationEvent_t941 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m5799(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m6360(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m5801(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m6357((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"



// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurve.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationCurve
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"



// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m5811 (AnimationCurve_t944 * __this, KeyframeU5BU5D_t1085* ___keys, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t1085* L_0 = ___keys;
		AnimationCurve_Init_m5815(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m5812 (AnimationCurve_t944 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m5815(__this, (KeyframeU5BU5D_t1085*)(KeyframeU5BU5D_t1085*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m5813 (AnimationCurve_t944 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m5813_ftn) (AnimationCurve_t944 *);
	static AnimationCurve_Cleanup_m5813_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m5813_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m5814 (AnimationCurve_t944 * __this, const MethodInfo* method)
{
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m5813(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m5815 (AnimationCurve_t944 * __this, KeyframeU5BU5D_t1085* ___keys, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m5815_ftn) (AnimationCurve_t944 *, KeyframeU5BU5D_t1085*);
	static AnimationCurve_Init_m5815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m5815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
void AnimationCurve_t944_marshal(const AnimationCurve_t944& unmarshaled, AnimationCurve_t944_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
void AnimationCurve_t944_marshal_back(const AnimationCurve_t944_marshaled& marshaled, AnimationCurve_t944& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
void AnimationCurve_t944_marshal_cleanup(AnimationCurve_t944_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimationState
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"



// UnityEngine.AvatarIKGoal
#include "UnityEngine_UnityEngine_AvatarIKGoal.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AvatarIKGoal
#include "UnityEngine_UnityEngine_AvatarIKGoalMethodDeclarations.h"



// UnityEngine.AvatarIKHint
#include "UnityEngine_UnityEngine_AvatarIKHint.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AvatarIKHint
#include "UnityEngine_UnityEngine_AvatarIKHintMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"


// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" bool AnimatorStateInfo_IsName_m3271 (AnimatorStateInfo_t542 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m5842(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = (__this->___m_FullPath_2);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (__this->___m_Name_0);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (__this->___m_Path_1);
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return G_B4_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" int32_t AnimatorStateInfo_get_fullPathHash_m5816 (AnimatorStateInfo_t542 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C" int32_t AnimatorStateInfo_get_nameHash_m5817 (AnimatorStateInfo_t542 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Path_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" int32_t AnimatorStateInfo_get_shortNameHash_m5818 (AnimatorStateInfo_t542 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" float AnimatorStateInfo_get_normalizedTime_m3276 (AnimatorStateInfo_t542 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" float AnimatorStateInfo_get_length_m5819 (AnimatorStateInfo_t542 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Length_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" int32_t AnimatorStateInfo_get_tagHash_m5820 (AnimatorStateInfo_t542 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Tag_5);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" bool AnimatorStateInfo_IsTag_m5821 (AnimatorStateInfo_t542 * __this, String_t* ___tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag;
		int32_t L_1 = Animator_StringToHash_m5842(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Tag_5);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" bool AnimatorStateInfo_get_loop_m5822 (AnimatorStateInfo_t542 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Loop_6);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.AnimatorTransitionInfo
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"



// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" bool AnimatorTransitionInfo_IsName_m5823 (AnimatorTransitionInfo_t948 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m5842(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Name_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name;
		int32_t L_4 = Animator_StringToHash_m5842(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = (__this->___m_FullPath_0);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" bool AnimatorTransitionInfo_IsUserName_m5824 (AnimatorTransitionInfo_t948 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m5842(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_UserName_1);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C" int32_t AnimatorTransitionInfo_get_fullPathHash_m5825 (AnimatorTransitionInfo_t948 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_0);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" int32_t AnimatorTransitionInfo_get_nameHash_m5826 (AnimatorTransitionInfo_t948 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" int32_t AnimatorTransitionInfo_get_userNameHash_m5827 (AnimatorTransitionInfo_t948 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UserName_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" float AnimatorTransitionInfo_get_normalizedTime_m5828 (AnimatorTransitionInfo_t948 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C" bool AnimatorTransitionInfo_get_anyState_m5829 (AnimatorTransitionInfo_t948 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AnyState_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C" bool AnimatorTransitionInfo_get_entry_m5830 (AnimatorTransitionInfo_t948 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C" bool AnimatorTransitionInfo_get_exit_m5831 (AnimatorTransitionInfo_t948 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
void AnimatorTransitionInfo_t948_marshal(const AnimatorTransitionInfo_t948& unmarshaled, AnimatorTransitionInfo_t948_marshaled& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.___m_FullPath_0;
	marshaled.___m_UserName_1 = unmarshaled.___m_UserName_1;
	marshaled.___m_Name_2 = unmarshaled.___m_Name_2;
	marshaled.___m_NormalizedTime_3 = unmarshaled.___m_NormalizedTime_3;
	marshaled.___m_AnyState_4 = unmarshaled.___m_AnyState_4;
	marshaled.___m_TransitionType_5 = unmarshaled.___m_TransitionType_5;
}
void AnimatorTransitionInfo_t948_marshal_back(const AnimatorTransitionInfo_t948_marshaled& marshaled, AnimatorTransitionInfo_t948& unmarshaled)
{
	unmarshaled.___m_FullPath_0 = marshaled.___m_FullPath_0;
	unmarshaled.___m_UserName_1 = marshaled.___m_UserName_1;
	unmarshaled.___m_Name_2 = marshaled.___m_Name_2;
	unmarshaled.___m_NormalizedTime_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.___m_AnyState_4 = marshaled.___m_AnyState_4;
	unmarshaled.___m_TransitionType_5 = marshaled.___m_TransitionType_5;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
void AnimatorTransitionInfo_t948_marshal_cleanup(AnimatorTransitionInfo_t948_marshaled& marshaled)
{
}
// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"
// UnityEngine.Avatar
#include "UnityEngine_UnityEngine_Avatar.h"
// UnityEngine.HumanBodyBones
#include "UnityEngine_UnityEngine_HumanBodyBones.h"


// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
extern "C" void Animator_SetFloat_m3275 (Animator_t321 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		float L_1 = ___value;
		Animator_SetFloatString_m5845(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single,System.Single,System.Single)
extern "C" void Animator_SetFloat_m3274 (Animator_t321 * __this, String_t* ___name, float ___value, float ___dampTime, float ___deltaTime, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		float L_1 = ___value;
		float L_2 = ___dampTime;
		float L_3 = ___deltaTime;
		Animator_SetFloatStringDamp_m5849(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C" void Animator_SetBool_m3160 (Animator_t321 * __this, String_t* ___name, bool ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		bool L_1 = ___value;
		Animator_SetBoolString_m5846(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" void Animator_SetTrigger_m3137 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_SetTriggerString_m5847(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" void Animator_ResetTrigger_m4895 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_ResetTriggerString_m5848(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
extern "C" Vector3_t215  Animator_get_deltaPosition_m3284 (Animator_t321 * __this, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Animator_INTERNAL_get_deltaPosition_m5832(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t215  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
extern "C" void Animator_INTERNAL_get_deltaPosition_m5832 (Animator_t321 * __this, Vector3_t215 * ___value, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_deltaPosition_m5832_ftn) (Animator_t321 *, Vector3_t215 *);
	static Animator_INTERNAL_get_deltaPosition_m5832_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_deltaPosition_m5832_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Animator::get_rootRotation()
extern "C" Quaternion_t261  Animator_get_rootRotation_m3282 (Animator_t321 * __this, const MethodInfo* method)
{
	Quaternion_t261  V_0 = {0};
	{
		Animator_INTERNAL_get_rootRotation_m5833(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t261  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
extern "C" void Animator_INTERNAL_get_rootRotation_m5833 (Animator_t321 * __this, Quaternion_t261 * ___value, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_get_rootRotation_m5833_ftn) (Animator_t321 *, Quaternion_t261 *);
	static Animator_INTERNAL_get_rootRotation_m5833_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_get_rootRotation_m5833_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C" void Animator_set_applyRootMotion_m3273 (Animator_t321 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*Animator_set_applyRootMotion_m3273_ftn) (Animator_t321 *, bool);
	static Animator_set_applyRootMotion_m3273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_applyRootMotion_m3273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_applyRootMotion(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Animator::SetIKPosition(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C" void Animator_SetIKPosition_m3127 (Animator_t321 * __this, int32_t ___goal, Vector3_t215  ___goalPosition, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m5843(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal;
		Vector3_t215  L_1 = ___goalPosition;
		Animator_SetIKPositionInternal_m5834(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKPositionInternal(UnityEngine.AvatarIKGoal,UnityEngine.Vector3)
extern "C" void Animator_SetIKPositionInternal_m5834 (Animator_t321 * __this, int32_t ___goal, Vector3_t215  ___goalPosition, const MethodInfo* method)
{
	{
		int32_t L_0 = ___goal;
		Animator_INTERNAL_CALL_SetIKPositionInternal_m5835(NULL /*static, unused*/, __this, L_0, (&___goalPosition), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
extern "C" void Animator_INTERNAL_CALL_SetIKPositionInternal_m5835 (Object_t * __this /* static, unused */, Animator_t321 * ___self, int32_t ___goal, Vector3_t215 * ___goalPosition, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetIKPositionInternal_m5835_ftn) (Animator_t321 *, int32_t, Vector3_t215 *);
	static Animator_INTERNAL_CALL_SetIKPositionInternal_m5835_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetIKPositionInternal_m5835_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___goal, ___goalPosition);
}
// System.Void UnityEngine.Animator::SetIKPositionWeight(UnityEngine.AvatarIKGoal,System.Single)
extern "C" void Animator_SetIKPositionWeight_m3126 (Animator_t321 * __this, int32_t ___goal, float ___value, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m5843(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___goal;
		float L_1 = ___value;
		Animator_SetIKPositionWeightInternal_m5836(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
extern "C" void Animator_SetIKPositionWeightInternal_m5836 (Animator_t321 * __this, int32_t ___goal, float ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetIKPositionWeightInternal_m5836_ftn) (Animator_t321 *, int32_t, float);
	static Animator_SetIKPositionWeightInternal_m5836_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIKPositionWeightInternal_m5836_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)");
	_il2cpp_icall_func(__this, ___goal, ___value);
}
// System.Void UnityEngine.Animator::SetIKHintPositionWeight(UnityEngine.AvatarIKHint,System.Single)
extern "C" void Animator_SetIKHintPositionWeight_m3128 (Animator_t321 * __this, int32_t ___hint, float ___value, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m5843(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___hint;
		float L_1 = ___value;
		Animator_SetIKHintPositionWeightInternal_m5837(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetIKHintPositionWeightInternal(UnityEngine.AvatarIKHint,System.Single)
extern "C" void Animator_SetIKHintPositionWeightInternal_m5837 (Animator_t321 * __this, int32_t ___hint, float ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetIKHintPositionWeightInternal_m5837_ftn) (Animator_t321 *, int32_t, float);
	static Animator_SetIKHintPositionWeightInternal_m5837_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetIKHintPositionWeightInternal_m5837_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetIKHintPositionWeightInternal(UnityEngine.AvatarIKHint,System.Single)");
	_il2cpp_icall_func(__this, ___hint, ___value);
}
// System.Void UnityEngine.Animator::SetLookAtPosition(UnityEngine.Vector3)
extern "C" void Animator_SetLookAtPosition_m3129 (Animator_t321 * __this, Vector3_t215  ___lookAtPosition, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m5843(__this, /*hidden argument*/NULL);
		Vector3_t215  L_0 = ___lookAtPosition;
		Animator_SetLookAtPositionInternal_m5838(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtPositionInternal(UnityEngine.Vector3)
extern "C" void Animator_SetLookAtPositionInternal_m5838 (Animator_t321 * __this, Vector3_t215  ___lookAtPosition, const MethodInfo* method)
{
	{
		Animator_INTERNAL_CALL_SetLookAtPositionInternal_m5839(NULL /*static, unused*/, __this, (&___lookAtPosition), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
extern "C" void Animator_INTERNAL_CALL_SetLookAtPositionInternal_m5839 (Object_t * __this /* static, unused */, Animator_t321 * ___self, Vector3_t215 * ___lookAtPosition, const MethodInfo* method)
{
	typedef void (*Animator_INTERNAL_CALL_SetLookAtPositionInternal_m5839_ftn) (Animator_t321 *, Vector3_t215 *);
	static Animator_INTERNAL_CALL_SetLookAtPositionInternal_m5839_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_INTERNAL_CALL_SetLookAtPositionInternal_m5839_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self, ___lookAtPosition);
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single)
extern "C" void Animator_SetLookAtWeight_m3279 (Animator_t321 * __this, float ___weight, float ___bodyWeight, float ___headWeight, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (0.5f);
		V_1 = (0.0f);
		float L_0 = ___weight;
		float L_1 = ___bodyWeight;
		float L_2 = ___headWeight;
		float L_3 = V_1;
		float L_4 = V_0;
		Animator_SetLookAtWeight_m5840(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeight(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" void Animator_SetLookAtWeight_m5840 (Animator_t321 * __this, float ___weight, float ___bodyWeight, float ___headWeight, float ___eyesWeight, float ___clampWeight, const MethodInfo* method)
{
	{
		Animator_CheckIfInIKPass_m5843(__this, /*hidden argument*/NULL);
		float L_0 = ___weight;
		float L_1 = ___bodyWeight;
		float L_2 = ___headWeight;
		float L_3 = ___eyesWeight;
		float L_4 = ___clampWeight;
		Animator_SetLookAtWeightInternal_m5841(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C" void Animator_SetLookAtWeightInternal_m5841 (Animator_t321 * __this, float ___weight, float ___bodyWeight, float ___headWeight, float ___eyesWeight, float ___clampWeight, const MethodInfo* method)
{
	typedef void (*Animator_SetLookAtWeightInternal_m5841_ftn) (Animator_t321 *, float, float, float, float, float);
	static Animator_SetLookAtWeightInternal_m5841_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetLookAtWeightInternal_m5841_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___weight, ___bodyWeight, ___headWeight, ___eyesWeight, ___clampWeight);
}
// UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
extern "C" AnimatorStateInfo_t542  Animator_GetCurrentAnimatorStateInfo_m3270 (Animator_t321 * __this, int32_t ___layerIndex, const MethodInfo* method)
{
	typedef AnimatorStateInfo_t542  (*Animator_GetCurrentAnimatorStateInfo_m3270_ftn) (Animator_t321 *, int32_t);
	static Animator_GetCurrentAnimatorStateInfo_m3270_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetCurrentAnimatorStateInfo_m3270_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)");
	return _il2cpp_icall_func(__this, ___layerIndex);
}
// System.Void UnityEngine.Animator::set_speed(System.Single)
extern "C" void Animator_set_speed_m3278 (Animator_t321 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Animator_set_speed_m3278_ftn) (Animator_t321 *, float);
	static Animator_set_speed_m3278_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_speed_m3278_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_speed(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)
extern "C" Transform_t243 * Animator_GetBoneTransform_m3215 (Animator_t321 * __this, int32_t ___humanBoneId, const MethodInfo* method)
{
	typedef Transform_t243 * (*Animator_GetBoneTransform_m3215_ftn) (Animator_t321 *, int32_t);
	static Animator_GetBoneTransform_m3215_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoneTransform_m3215_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoneTransform(UnityEngine.HumanBodyBones)");
	return _il2cpp_icall_func(__this, ___humanBoneId);
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C" RuntimeAnimatorController_t816 * Animator_get_runtimeAnimatorController_m4894 (Animator_t321 * __this, const MethodInfo* method)
{
	typedef RuntimeAnimatorController_t816 * (*Animator_get_runtimeAnimatorController_m4894_ftn) (Animator_t321 *);
	static Animator_get_runtimeAnimatorController_m4894_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m4894_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m5842 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m5842_ftn) (String_t*);
	static Animator_StringToHash_m5842_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m5842_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name);
}
// UnityEngine.Avatar UnityEngine.Animator::get_avatar()
extern "C" Avatar_t543 * Animator_get_avatar_m3280 (Animator_t321 * __this, const MethodInfo* method)
{
	typedef Avatar_t543 * (*Animator_get_avatar_m3280_ftn) (Animator_t321 *);
	static Animator_get_avatar_m3280_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_avatar_m3280_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_avatar()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::set_avatar(UnityEngine.Avatar)
extern "C" void Animator_set_avatar_m3281 (Animator_t321 * __this, Avatar_t543 * ___value, const MethodInfo* method)
{
	typedef void (*Animator_set_avatar_m3281_ftn) (Animator_t321 *, Avatar_t543 *);
	static Animator_set_avatar_m3281_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_avatar_m3281_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_avatar(UnityEngine.Avatar)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Animator::CheckIfInIKPass()
extern Il2CppCodeGenString* _stringLiteral990;
extern "C" void Animator_CheckIfInIKPass_m5843 (Animator_t321 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral990 = il2cpp_codegen_string_literal_from_index(990);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Animator_get_logWarnings_m5850(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		bool L_1 = Animator_CheckIfInIKPassInternal_m5844(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Debug_LogWarning_m2781(NULL /*static, unused*/, _stringLiteral990, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
extern "C" bool Animator_CheckIfInIKPassInternal_m5844 (Animator_t321 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_CheckIfInIKPassInternal_m5844_ftn) (Animator_t321 *);
	static Animator_CheckIfInIKPassInternal_m5844_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_CheckIfInIKPassInternal_m5844_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::CheckIfInIKPassInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
extern "C" void Animator_SetFloatString_m5845 (Animator_t321 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatString_m5845_ftn) (Animator_t321 *, String_t*, float);
	static Animator_SetFloatString_m5845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatString_m5845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatString(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___name, ___value);
}
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C" void Animator_SetBoolString_m5846 (Animator_t321 * __this, String_t* ___name, bool ___value, const MethodInfo* method)
{
	typedef void (*Animator_SetBoolString_m5846_ftn) (Animator_t321 *, String_t*, bool);
	static Animator_SetBoolString_m5846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolString_m5846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolString(System.String,System.Boolean)");
	_il2cpp_icall_func(__this, ___name, ___value);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m5847 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m5847_ftn) (Animator_t321 *, String_t*);
	static Animator_SetTriggerString_m5847_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m5847_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" void Animator_ResetTriggerString_m5848 (Animator_t321 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m5848_ftn) (Animator_t321 *, String_t*);
	static Animator_ResetTriggerString_m5848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m5848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)
extern "C" void Animator_SetFloatStringDamp_m5849 (Animator_t321 * __this, String_t* ___name, float ___value, float ___dampTime, float ___deltaTime, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatStringDamp_m5849_ftn) (Animator_t321 *, String_t*, float, float, float);
	static Animator_SetFloatStringDamp_m5849_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatStringDamp_m5849_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatStringDamp(System.String,System.Single,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___name, ___value, ___dampTime, ___deltaTime);
}
// System.Boolean UnityEngine.Animator::get_logWarnings()
extern "C" bool Animator_get_logWarnings_m5850 (Animator_t321 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_logWarnings_m5850_ftn) (Animator_t321 *);
	static Animator_get_logWarnings_m5850_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_logWarnings_m5850_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_logWarnings()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBone.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.SkeletonBone
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"



// Conversion methods for marshalling of: UnityEngine.SkeletonBone
void SkeletonBone_t949_marshal(const SkeletonBone_t949& unmarshaled, SkeletonBone_t949_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___position_1 = unmarshaled.___position_1;
	marshaled.___rotation_2 = unmarshaled.___rotation_2;
	marshaled.___scale_3 = unmarshaled.___scale_3;
	marshaled.___transformModified_4 = unmarshaled.___transformModified_4;
}
void SkeletonBone_t949_marshal_back(const SkeletonBone_t949_marshaled& marshaled, SkeletonBone_t949& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___position_1 = marshaled.___position_1;
	unmarshaled.___rotation_2 = marshaled.___rotation_2;
	unmarshaled.___scale_3 = marshaled.___scale_3;
	unmarshaled.___transformModified_4 = marshaled.___transformModified_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
void SkeletonBone_t949_marshal_cleanup(SkeletonBone_t949_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimit.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HumanLimit
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"



// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBone.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HumanBone
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"



// System.String UnityEngine.HumanBone::get_boneName()
extern "C" String_t* HumanBone_get_boneName_m5851 (HumanBone_t951 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_BoneName_0);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
extern "C" void HumanBone_set_boneName_m5852 (HumanBone_t951 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_BoneName_0 = L_0;
		return;
	}
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C" String_t* HumanBone_get_humanName_m5853 (HumanBone_t951 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_HumanName_1);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C" void HumanBone_set_humanName_m5854 (HumanBone_t951 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_HumanName_1 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
void HumanBone_t951_marshal(const HumanBone_t951& unmarshaled, HumanBone_t951_marshaled& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_BoneName_0);
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.___m_HumanName_1);
	marshaled.___limit_2 = unmarshaled.___limit_2;
}
void HumanBone_t951_marshal_back(const HumanBone_t951_marshaled& marshaled, HumanBone_t951& unmarshaled)
{
	unmarshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0);
	unmarshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1);
	unmarshaled.___limit_2 = marshaled.___limit_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
void HumanBone_t951_marshal_cleanup(HumanBone_t951_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RuntimeAnimatorController
#include "UnityEngine_UnityEngine_RuntimeAnimatorControllerMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.HumanBodyBones
#include "UnityEngine_UnityEngine_HumanBodyBonesMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// UnityEngine.Avatar
#include "UnityEngine_UnityEngine_AvatarMethodDeclarations.h"



// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"



// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"



// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"



// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUIText.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.GUIText
#include "UnityEngine_UnityEngine_GUITextMethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"


// UnityEngine.Material UnityEngine.GUIText::get_material()
extern "C" Material_t2 * GUIText_get_material_m3302 (GUIText_t549 * __this, const MethodInfo* method)
{
	typedef Material_t2 * (*GUIText_get_material_m3302_ftn) (GUIText_t549 *);
	static GUIText_get_material_m3302_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GUIText_get_material_m3302_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GUIText::get_material()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMesh.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TextMesh
#include "UnityEngine_UnityEngine_TextMeshMethodDeclarations.h"



// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C" void TextMesh_set_text_m3201 (TextMesh_t397 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*TextMesh_set_text_m3201_ftn) (TextMesh_t397 *, String_t*);
	static TextMesh_set_text_m3201_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_set_text_m3201_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.TextMesh::set_color(UnityEngine.Color)
extern "C" void TextMesh_set_color_m3236 (TextMesh_t397 * __this, Color_t6  ___value, const MethodInfo* method)
{
	{
		TextMesh_INTERNAL_set_color_m5855(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextMesh::INTERNAL_set_color(UnityEngine.Color&)
extern "C" void TextMesh_INTERNAL_set_color_m5855 (TextMesh_t397 * __this, Color_t6 * ___value, const MethodInfo* method)
{
	typedef void (*TextMesh_INTERNAL_set_color_m5855_ftn) (TextMesh_t397 *, Color_t6 *);
	static TextMesh_INTERNAL_set_color_m5855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextMesh_INTERNAL_set_color_m5855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextMesh::INTERNAL_set_color(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CharacterInfo
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"

// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"


// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m5856 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___width_3);
		return (((int32_t)L_0));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m5857 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_width_m2798(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m5858 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m2800(L_0, /*hidden argument*/NULL);
		return (((int32_t)((-L_1))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m5859 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m2861(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m5860 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t225 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m2865(L_1, /*hidden argument*/NULL);
		Rect_t225 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_height_m2800(L_3, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((float)((float)L_2+(float)L_4))))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m5861 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t225 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m2865(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)L_2))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m5862 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m2861(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m5863 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m2861(L_0, /*hidden argument*/NULL);
		Rect_t225 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_width_m2798(L_2, /*hidden argument*/NULL);
		return (((int32_t)((float)((float)L_1+(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t7  CharacterInfo_get_uvBottomLeftUnFlipped_m5864 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m2861(L_0, /*hidden argument*/NULL);
		Rect_t225 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m2865(L_2, /*hidden argument*/NULL);
		Vector2_t7  L_4 = {0};
		Vector2__ctor_m2714(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t7  CharacterInfo_get_uvBottomRightUnFlipped_m5865 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m2861(L_0, /*hidden argument*/NULL);
		Rect_t225 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m2798(L_2, /*hidden argument*/NULL);
		Rect_t225 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m2865(L_4, /*hidden argument*/NULL);
		Vector2_t7  L_6 = {0};
		Vector2__ctor_m2714(&L_6, ((float)((float)L_1+(float)L_3)), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t7  CharacterInfo_get_uvTopRightUnFlipped_m5866 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m2861(L_0, /*hidden argument*/NULL);
		Rect_t225 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m2798(L_2, /*hidden argument*/NULL);
		Rect_t225 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m2865(L_4, /*hidden argument*/NULL);
		Rect_t225 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_height_m2800(L_6, /*hidden argument*/NULL);
		Vector2_t7  L_8 = {0};
		Vector2__ctor_m2714(&L_8, ((float)((float)L_1+(float)L_3)), ((float)((float)L_5+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t7  CharacterInfo_get_uvTopLeftUnFlipped_m5867 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	{
		Rect_t225 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m2861(L_0, /*hidden argument*/NULL);
		Rect_t225 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m2865(L_2, /*hidden argument*/NULL);
		Rect_t225 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_height_m2800(L_4, /*hidden argument*/NULL);
		Vector2_t7  L_6 = {0};
		Vector2__ctor_m2714(&L_6, L_1, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t7  CharacterInfo_get_uvBottomLeft_m5868 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	Vector2_t7  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t7  L_1 = CharacterInfo_get_uvBottomLeftUnFlipped_m5864(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t7  L_2 = CharacterInfo_get_uvBottomLeftUnFlipped_m5864(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t7  CharacterInfo_get_uvBottomRight_m5869 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	Vector2_t7  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t7  L_1 = CharacterInfo_get_uvTopLeftUnFlipped_m5867(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t7  L_2 = CharacterInfo_get_uvBottomRightUnFlipped_m5865(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t7  CharacterInfo_get_uvTopRight_m5870 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	Vector2_t7  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t7  L_1 = CharacterInfo_get_uvTopRightUnFlipped_m5866(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t7  L_2 = CharacterInfo_get_uvTopRightUnFlipped_m5866(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t7  CharacterInfo_get_uvTopLeft_m5871 (CharacterInfo_t955 * __this, const MethodInfo* method)
{
	Vector2_t7  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t7  L_1 = CharacterInfo_get_uvBottomRightUnFlipped_m5865(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t7  L_2 = CharacterInfo_get_uvTopLeftUnFlipped_m5867(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
void CharacterInfo_t955_marshal(const CharacterInfo_t955& unmarshaled, CharacterInfo_t955_marshaled& marshaled)
{
	marshaled.___index_0 = unmarshaled.___index_0;
	marshaled.___uv_1 = unmarshaled.___uv_1;
	marshaled.___vert_2 = unmarshaled.___vert_2;
	marshaled.___width_3 = unmarshaled.___width_3;
	marshaled.___size_4 = unmarshaled.___size_4;
	marshaled.___style_5 = unmarshaled.___style_5;
	marshaled.___flipped_6 = unmarshaled.___flipped_6;
	marshaled.___ascent_7 = unmarshaled.___ascent_7;
}
void CharacterInfo_t955_marshal_back(const CharacterInfo_t955_marshaled& marshaled, CharacterInfo_t955& unmarshaled)
{
	unmarshaled.___index_0 = marshaled.___index_0;
	unmarshaled.___uv_1 = marshaled.___uv_1;
	unmarshaled.___vert_2 = marshaled.___vert_2;
	unmarshaled.___width_3 = marshaled.___width_3;
	unmarshaled.___size_4 = marshaled.___size_4;
	unmarshaled.___style_5 = marshaled.___style_5;
	unmarshaled.___flipped_6 = marshaled.___flipped_6;
	unmarshaled.___ascent_7 = marshaled.___ascent_7;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
void CharacterInfo_t955_marshal_cleanup(CharacterInfo_t955_marshaled& marshaled)
{
}
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Font/FontTextureRebuildCallback
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"



// System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FontTextureRebuildCallback__ctor_m5872 (FontTextureRebuildCallback_t956 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern "C" void FontTextureRebuildCallback_Invoke_m5873 (FontTextureRebuildCallback_t956 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FontTextureRebuildCallback_Invoke_m5873((FontTextureRebuildCallback_t956 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t956(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * FontTextureRebuildCallback_BeginInvoke_m5874 (FontTextureRebuildCallback_t956 * __this, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern "C" void FontTextureRebuildCallback_EndInvoke_m5875 (FontTextureRebuildCallback_t956 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Font
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"

// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_1.h"
// System.Action`1<UnityEngine.Font>
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"


// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t643_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t793_il2cpp_TypeInfo_var;
extern "C" void Font_add_textureRebuilt_m4673 (Object_t * __this /* static, unused */, Action_1_t793 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(486);
		Action_1_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t793 * L_0 = ((Font_t643_StaticFields*)Font_t643_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t793 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t643_StaticFields*)Font_t643_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t793 *)Castclass(L_2, Action_1_t793_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t643_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t793_il2cpp_TypeInfo_var;
extern "C" void Font_remove_textureRebuilt_m5876 (Object_t * __this /* static, unused */, Action_1_t793 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(486);
		Action_1_t793_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t793 * L_0 = ((Font_t643_StaticFields*)Font_t643_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t793 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t643_StaticFields*)Font_t643_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t793 *)Castclass(L_2, Action_1_t793_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C" Material_t2 * Font_get_material_m4904 (Font_t643 * __this, const MethodInfo* method)
{
	typedef Material_t2 * (*Font_get_material_m4904_ftn) (Font_t643 *);
	static Font_get_material_m4904_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_material_m4904_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern "C" bool Font_HasCharacter_m4802 (Font_t643 * __this, uint16_t ___c, const MethodInfo* method)
{
	typedef bool (*Font_HasCharacter_m4802_ftn) (Font_t643 *, uint16_t);
	static Font_HasCharacter_m4802_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_HasCharacter_m4802_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, ___c);
}
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern TypeInfo* Font_t643_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m6398_MethodInfo_var;
extern "C" void Font_InvokeTextureRebuilt_Internal_m5877 (Object_t * __this /* static, unused */, Font_t643 * ___font, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(486);
		Action_1_Invoke_m6398_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484147);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t793 * V_0 = {0};
	{
		Action_1_t793 * L_0 = ((Font_t643_StaticFields*)Font_t643_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		V_0 = L_0;
		Action_1_t793 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t793 * L_2 = V_0;
		Font_t643 * L_3 = ___font;
		NullCheck(L_2);
		Action_1_Invoke_m6398(L_2, L_3, /*hidden argument*/Action_1_Invoke_m6398_MethodInfo_var);
	}

IL_0013:
	{
		Font_t643 * L_4 = ___font;
		NullCheck(L_4);
		FontTextureRebuildCallback_t956 * L_5 = (L_4->___m_FontTextureRebuildCallback_3);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Font_t643 * L_6 = ___font;
		NullCheck(L_6);
		FontTextureRebuildCallback_t956 * L_7 = (L_6->___m_FontTextureRebuildCallback_3);
		NullCheck(L_7);
		FontTextureRebuildCallback_Invoke_m5873(L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C" bool Font_get_dynamic_m4907 (Font_t643 * __this, const MethodInfo* method)
{
	typedef bool (*Font_get_dynamic_m4907_ftn) (Font_t643 *);
	static Font_get_dynamic_m4907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_dynamic_m4907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C" int32_t Font_get_fontSize_m4909 (Font_t643 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_fontSize_m4909_ftn) (Font_t643 *);
	static Font_get_fontSize_m4909_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontSize_m4909_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"



// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"



// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGenerator.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TextGenerator
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_22.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_35.h"
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_36.h"
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_gen_22MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_35MethodDeclarations.h"
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen_36MethodDeclarations.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"


// System.Void UnityEngine.TextGenerator::.ctor()
extern "C" void TextGenerator__ctor_m4773 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m4902(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern TypeInfo* List_1_t687_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t957_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t958_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m6399_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m6400_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m6401_MethodInfo_var;
extern "C" void TextGenerator__ctor_m4902 (TextGenerator_t686 * __this, int32_t ___initialCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(493);
		List_1_t957_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(680);
		List_1_t958_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(681);
		List_1__ctor_m6399_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484148);
		List_1__ctor_m6400_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484149);
		List_1__ctor_m6401_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484150);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity;
		List_1_t687 * L_1 = (List_1_t687 *)il2cpp_codegen_object_new (List_1_t687_il2cpp_TypeInfo_var);
		List_1__ctor_m6399(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m6399_MethodInfo_var);
		__this->___m_Verts_5 = L_1;
		int32_t L_2 = ___initialCapacity;
		List_1_t957 * L_3 = (List_1_t957 *)il2cpp_codegen_object_new (List_1_t957_il2cpp_TypeInfo_var);
		List_1__ctor_m6400(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m6400_MethodInfo_var);
		__this->___m_Characters_6 = L_3;
		List_1_t958 * L_4 = (List_1_t958 *)il2cpp_codegen_object_new (List_1_t958_il2cpp_TypeInfo_var);
		List_1__ctor_m6401(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m6401_MethodInfo_var);
		__this->___m_Lines_7 = L_4;
		TextGenerator_Init_m5879(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C" void TextGenerator_System_IDisposable_Dispose_m5878 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m5880(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C" void TextGenerator_Init_m5879 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m5879_ftn) (TextGenerator_t686 *);
	static TextGenerator_Init_m5879_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m5879_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C" void TextGenerator_Dispose_cpp_m5880 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m5880_ftn) (TextGenerator_t686 *);
	static TextGenerator_Dispose_cpp_m5880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m5880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_m5881 (TextGenerator_t686 * __this, String_t* ___str, Font_t643 * ___font, Color_t6  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, Vector2_t7  ___extents, Vector2_t7  ___pivot, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t643 * L_1 = ___font;
		Color_t6  L_2 = ___color;
		int32_t L_3 = ___fontSize;
		float L_4 = ___scaleFactor;
		float L_5 = ___lineSpacing;
		int32_t L_6 = ___style;
		bool L_7 = ___richText;
		bool L_8 = ___resizeTextForBestFit;
		int32_t L_9 = ___resizeTextMinSize;
		int32_t L_10 = ___resizeTextMaxSize;
		int32_t L_11 = ___verticalOverFlow;
		int32_t L_12 = ___horizontalOverflow;
		bool L_13 = ___updateBounds;
		int32_t L_14 = ___anchor;
		float L_15 = ((&___extents)->___x_1);
		float L_16 = ((&___extents)->___y_2);
		float L_17 = ((&___pivot)->___x_1);
		float L_18 = ((&___pivot)->___y_2);
		bool L_19 = ___generateOutOfBounds;
		bool L_20 = TextGenerator_Populate_Internal_cpp_m5882(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_cpp_m5882 (TextGenerator_t686 * __this, String_t* ___str, Font_t643 * ___font, Color_t6  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t643 * L_1 = ___font;
		int32_t L_2 = ___fontSize;
		float L_3 = ___scaleFactor;
		float L_4 = ___lineSpacing;
		int32_t L_5 = ___style;
		bool L_6 = ___richText;
		bool L_7 = ___resizeTextForBestFit;
		int32_t L_8 = ___resizeTextMinSize;
		int32_t L_9 = ___resizeTextMaxSize;
		int32_t L_10 = ___verticalOverFlow;
		int32_t L_11 = ___horizontalOverflow;
		bool L_12 = ___updateBounds;
		int32_t L_13 = ___anchor;
		float L_14 = ___extentsX;
		float L_15 = ___extentsY;
		float L_16 = ___pivotX;
		float L_17 = ___pivotY;
		bool L_18 = ___generateOutOfBounds;
		bool L_19 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m5883(NULL /*static, unused*/, __this, L_0, L_1, (&___color), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m5883 (Object_t * __this /* static, unused */, TextGenerator_t686 * ___self, String_t* ___str, Font_t643 * ___font, Color_t6 * ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m5883_ftn) (TextGenerator_t686 *, String_t*, Font_t643 *, Color_t6 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m5883_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m5883_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(___self, ___str, ___font, ___color, ___fontSize, ___scaleFactor, ___lineSpacing, ___style, ___richText, ___resizeTextForBestFit, ___resizeTextMinSize, ___resizeTextMaxSize, ___verticalOverFlow, ___horizontalOverflow, ___updateBounds, ___anchor, ___extentsX, ___extentsY, ___pivotX, ___pivotY, ___generateOutOfBounds);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C" Rect_t225  TextGenerator_get_rectExtents_m4815 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		TextGenerator_INTERNAL_get_rectExtents_m5884(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t225  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m5884 (TextGenerator_t686 * __this, Rect_t225 * ___value, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m5884_ftn) (TextGenerator_t686 *, Rect_t225 *);
	static TextGenerator_INTERNAL_get_rectExtents_m5884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m5884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C" int32_t TextGenerator_get_vertexCount_m5885 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m5885_ftn) (TextGenerator_t686 *);
	static TextGenerator_get_vertexCount_m5885_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m5885_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C" void TextGenerator_GetVerticesInternal_m5886 (TextGenerator_t686 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m5886_ftn) (TextGenerator_t686 *, Object_t *);
	static TextGenerator_GetVerticesInternal_m5886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m5886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
extern "C" UIVertexU5BU5D_t684* TextGenerator_GetVerticesArray_m5887 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef UIVertexU5BU5D_t684* (*TextGenerator_GetVerticesArray_m5887_ftn) (TextGenerator_t686 *);
	static TextGenerator_GetVerticesArray_m5887_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesArray_m5887_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C" int32_t TextGenerator_get_characterCount_m5888 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m5888_ftn) (TextGenerator_t686 *);
	static TextGenerator_get_characterCount_m5888_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m5888_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t474_il2cpp_TypeInfo_var;
extern "C" int32_t TextGenerator_get_characterCountVisible_m4796 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		Mathf_t474_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m3358(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = (__this->___m_LastString_1);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m3086(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m5885(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t474_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m3049(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m3050(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C" void TextGenerator_GetCharactersInternal_m5889 (TextGenerator_t686 * __this, Object_t * ___characters, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m5889_ftn) (TextGenerator_t686 *, Object_t *);
	static TextGenerator_GetCharactersInternal_m5889_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m5889_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters);
}
// UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
extern "C" UICharInfoU5BU5D_t1086* TextGenerator_GetCharactersArray_m5890 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef UICharInfoU5BU5D_t1086* (*TextGenerator_GetCharactersArray_m5890_ftn) (TextGenerator_t686 *);
	static TextGenerator_GetCharactersArray_m5890_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersArray_m5890_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C" int32_t TextGenerator_get_lineCount_m4795 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m4795_ftn) (TextGenerator_t686 *);
	static TextGenerator_get_lineCount_m4795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m4795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C" void TextGenerator_GetLinesInternal_m5891 (TextGenerator_t686 * __this, Object_t * ___lines, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m5891_ftn) (TextGenerator_t686 *, Object_t *);
	static TextGenerator_GetLinesInternal_m5891_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m5891_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines);
}
// UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
extern "C" UILineInfoU5BU5D_t1087* TextGenerator_GetLinesArray_m5892 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef UILineInfoU5BU5D_t1087* (*TextGenerator_GetLinesArray_m5892_ftn) (TextGenerator_t686 *);
	static TextGenerator_GetLinesArray_m5892_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesArray_m5892_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
extern "C" int32_t TextGenerator_get_fontSizeUsedForBestFit_m4831 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_fontSizeUsedForBestFit_m4831_ftn) (TextGenerator_t686 *);
	static TextGenerator_get_fontSizeUsedForBestFit_m4831_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_fontSizeUsedForBestFit_m4831_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern TypeInfo* IDisposable_t538_il2cpp_TypeInfo_var;
extern "C" void TextGenerator_Finalize_m5893 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t538_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t520 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t520 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t538_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t520 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m6346(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t520 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern Il2CppCodeGenString* _stringLiteral991;
extern Il2CppCodeGenString* _stringLiteral992;
extern "C" TextGenerationSettings_t773  TextGenerator_ValidatedSettings_m5894 (TextGenerator_t686 * __this, TextGenerationSettings_t773  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral991 = il2cpp_codegen_string_literal_from_index(991);
		_stringLiteral992 = il2cpp_codegen_string_literal_from_index(992);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t643 * L_0 = ((&___settings)->___font_0);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t643 * L_2 = ((&___settings)->___font_0);
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m4907(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t773  L_4 = ___settings;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = ((&___settings)->___fontSize_2);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = ((&___settings)->___fontStyle_6);
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		Debug_LogWarning_m2781(NULL /*static, unused*/, _stringLiteral991, /*hidden argument*/NULL);
		(&___settings)->___fontSize_2 = 0;
		(&___settings)->___fontStyle_6 = 0;
	}

IL_0057:
	{
		bool L_7 = ((&___settings)->___resizeTextForBestFit_8);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		Debug_LogWarning_m2781(NULL /*static, unused*/, _stringLiteral992, /*hidden argument*/NULL);
		(&___settings)->___resizeTextForBestFit_8 = 0;
	}

IL_0075:
	{
		TextGenerationSettings_t773  L_8 = ___settings;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C" void TextGenerator_Invalidate_m4906 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasGenerated_3 = 0;
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C" void TextGenerator_GetCharacters_m5895 (TextGenerator_t686 * __this, List_1_t957 * ___characters, const MethodInfo* method)
{
	{
		List_1_t957 * L_0 = ___characters;
		TextGenerator_GetCharactersInternal_m5889(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C" void TextGenerator_GetLines_m5896 (TextGenerator_t686 * __this, List_1_t958 * ___lines, const MethodInfo* method)
{
	{
		List_1_t958 * L_0 = ___lines;
		TextGenerator_GetLinesInternal_m5891(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void TextGenerator_GetVertices_m5897 (TextGenerator_t686 * __this, List_1_t687 * ___vertices, const MethodInfo* method)
{
	{
		List_1_t687 * L_0 = ___vertices;
		TextGenerator_GetVerticesInternal_m5886(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredWidth_m4911 (TextGenerator_t686 * __this, String_t* ___str, TextGenerationSettings_t773  ___settings, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		(&___settings)->___horizontalOverflow_13 = 1;
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t773  L_1 = ___settings;
		TextGenerator_Populate_m4814(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t225  L_2 = TextGenerator_get_rectExtents_m4815(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m2798((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredHeight_m4912 (TextGenerator_t686 * __this, String_t* ___str, TextGenerationSettings_t773  ___settings, const MethodInfo* method)
{
	Rect_t225  V_0 = {0};
	{
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t773  L_1 = ___settings;
		TextGenerator_Populate_m4814(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t225  L_2 = TextGenerator_get_rectExtents_m4815(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m2800((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextGenerator_Populate_m4814 (TextGenerator_t686 * __this, String_t* ___str, TextGenerationSettings_t773  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasGenerated_3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str;
		String_t* L_2 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m2981(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t773  L_4 = (__this->___m_LastSettings_2);
		bool L_5 = TextGenerationSettings_Equals_m6279((&___settings), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = (__this->___m_LastValid_4);
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str;
		TextGenerationSettings_t773  L_8 = ___settings;
		bool L_9 = TextGenerator_PopulateAlways_m5898(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerator_PopulateAlways_m5898 (TextGenerator_t686 * __this, String_t* ___str, TextGenerationSettings_t773  ___settings, const MethodInfo* method)
{
	TextGenerationSettings_t773  V_0 = {0};
	{
		String_t* L_0 = ___str;
		__this->___m_LastString_1 = L_0;
		__this->___m_HasGenerated_3 = 1;
		__this->___m_CachedVerts_8 = 0;
		__this->___m_CachedCharacters_9 = 0;
		__this->___m_CachedLines_10 = 0;
		TextGenerationSettings_t773  L_1 = ___settings;
		__this->___m_LastSettings_2 = L_1;
		TextGenerationSettings_t773  L_2 = ___settings;
		TextGenerationSettings_t773  L_3 = TextGenerator_ValidatedSettings_m5894(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str;
		Font_t643 * L_5 = ((&V_0)->___font_0);
		Color_t6  L_6 = ((&V_0)->___color_1);
		int32_t L_7 = ((&V_0)->___fontSize_2);
		float L_8 = ((&V_0)->___scaleFactor_5);
		float L_9 = ((&V_0)->___lineSpacing_3);
		int32_t L_10 = ((&V_0)->___fontStyle_6);
		bool L_11 = ((&V_0)->___richText_4);
		bool L_12 = ((&V_0)->___resizeTextForBestFit_8);
		int32_t L_13 = ((&V_0)->___resizeTextMinSize_9);
		int32_t L_14 = ((&V_0)->___resizeTextMaxSize_10);
		int32_t L_15 = ((&V_0)->___verticalOverflow_12);
		int32_t L_16 = ((&V_0)->___horizontalOverflow_13);
		bool L_17 = ((&V_0)->___updateBounds_11);
		int32_t L_18 = ((&V_0)->___textAnchor_7);
		Vector2_t7  L_19 = ((&V_0)->___generationExtents_14);
		Vector2_t7  L_20 = ((&V_0)->___pivot_15);
		bool L_21 = ((&V_0)->___generateOutOfBounds_16);
		bool L_22 = TextGenerator_Populate_Internal_m5881(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->___m_LastValid_4 = L_22;
		bool L_23 = (__this->___m_LastValid_4);
		return L_23;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C" Object_t* TextGenerator_get_verts_m4910 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedVerts_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t687 * L_1 = (__this->___m_Verts_5);
		TextGenerator_GetVertices_m5897(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedVerts_8 = 1;
	}

IL_001e:
	{
		List_1_t687 * L_2 = (__this->___m_Verts_5);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C" Object_t* TextGenerator_get_characters_m4797 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedCharacters_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t957 * L_1 = (__this->___m_Characters_6);
		TextGenerator_GetCharacters_m5895(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedCharacters_9 = 1;
	}

IL_001e:
	{
		List_1_t957 * L_2 = (__this->___m_Characters_6);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C" Object_t* TextGenerator_get_lines_m4794 (TextGenerator_t686 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedLines_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t958 * L_1 = (__this->___m_Lines_7);
		TextGenerator_GetLines_m5896(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedLines_10 = 1;
	}

IL_001e:
	{
		List_1_t958 * L_2 = (__this->___m_Lines_7);
		return L_2;
	}
}
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"



// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Canvas/WillRenderCanvases
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"



// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C" void WillRenderCanvases__ctor_m4659 (WillRenderCanvases_t791 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C" void WillRenderCanvases_Invoke_m5899 (WillRenderCanvases_t791 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WillRenderCanvases_Invoke_m5899((WillRenderCanvases_t791 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t791(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * WillRenderCanvases_BeginInvoke_m5900 (WillRenderCanvases_t791 * __this, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C" void WillRenderCanvases_EndInvoke_m5901 (WillRenderCanvases_t791 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"



// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t650_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t791_il2cpp_TypeInfo_var;
extern "C" void Canvas_add_willRenderCanvases_m4660 (Object_t * __this /* static, unused */, WillRenderCanvases_t791 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t650_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		WillRenderCanvases_t791_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(478);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t791 * L_0 = ((Canvas_t650_StaticFields*)Canvas_t650_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t791 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Combine_m2775(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t650_StaticFields*)Canvas_t650_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t791 *)Castclass(L_2, WillRenderCanvases_t791_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t650_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t791_il2cpp_TypeInfo_var;
extern "C" void Canvas_remove_willRenderCanvases_m5902 (Object_t * __this /* static, unused */, WillRenderCanvases_t791 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t650_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		WillRenderCanvases_t791_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(478);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t791 * L_0 = ((Canvas_t650_StaticFields*)Canvas_t650_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t791 * L_1 = ___value;
		Delegate_t480 * L_2 = Delegate_Remove_m2776(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t650_StaticFields*)Canvas_t650_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t791 *)Castclass(L_2, WillRenderCanvases_t791_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C" int32_t Canvas_get_renderMode_m4709 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m4709_ftn) (Canvas_t650 *);
	static Canvas_get_renderMode_m4709_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m4709_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C" bool Canvas_get_isRootCanvas_m4923 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m4923_ftn) (Canvas_t650 *);
	static Canvas_get_isRootCanvas_m4923_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m4923_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C" Camera_t14 * Canvas_get_worldCamera_m4717 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef Camera_t14 * (*Canvas_get_worldCamera_m4717_ftn) (Canvas_t650 *);
	static Canvas_get_worldCamera_m4717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m4717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C" float Canvas_get_scaleFactor_m4908 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m4908_ftn) (Canvas_t650 *);
	static Canvas_get_scaleFactor_m4908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m4908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C" void Canvas_set_scaleFactor_m4925 (Canvas_t650 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m4925_ftn) (Canvas_t650 *, float);
	static Canvas_set_scaleFactor_m4925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m4925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C" float Canvas_get_referencePixelsPerUnit_m4734 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m4734_ftn) (Canvas_t650 *);
	static Canvas_get_referencePixelsPerUnit_m4734_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m4734_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C" void Canvas_set_referencePixelsPerUnit_m4926 (Canvas_t650 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m4926_ftn) (Canvas_t650 *, float);
	static Canvas_set_referencePixelsPerUnit_m4926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m4926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C" bool Canvas_get_pixelPerfect_m4697 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m4697_ftn) (Canvas_t650 *);
	static Canvas_get_pixelPerfect_m4697_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m4697_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C" int32_t Canvas_get_renderOrder_m4711 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m4711_ftn) (Canvas_t650 *);
	static Canvas_get_renderOrder_m4711_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m4711_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C" int32_t Canvas_get_sortingOrder_m4710 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m4710_ftn) (Canvas_t650 *);
	static Canvas_get_sortingOrder_m4710_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m4710_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_cachedSortingLayerValue()
extern "C" int32_t Canvas_get_cachedSortingLayerValue_m4716 (Canvas_t650 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_cachedSortingLayerValue_m4716_ftn) (Canvas_t650 *);
	static Canvas_get_cachedSortingLayerValue_m4716_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_cachedSortingLayerValue_m4716_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_cachedSortingLayerValue()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C" Material_t2 * Canvas_GetDefaultCanvasMaterial_m4683 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t2 * (*Canvas_GetDefaultCanvasMaterial_m4683_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m4683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m4683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasTextMaterial()
extern "C" Material_t2 * Canvas_GetDefaultCanvasTextMaterial_m4903 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t2 * (*Canvas_GetDefaultCanvasTextMaterial_m4903_ftn) ();
	static Canvas_GetDefaultCanvasTextMaterial_m4903_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasTextMaterial_m4903_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasTextMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern TypeInfo* Canvas_t650_il2cpp_TypeInfo_var;
extern "C" void Canvas_SendWillRenderCanvases_m5903 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t650_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t791 * L_0 = ((Canvas_t650_StaticFields*)Canvas_t650_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t791 * L_1 = ((Canvas_t650_StaticFields*)Canvas_t650_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m5899(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C" void Canvas_ForceUpdateCanvases_m4859 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m5903(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroup.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CanvasGroup
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"



// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C" bool CanvasGroup_get_interactable_m4888 (CanvasGroup_t797 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m4888_ftn) (CanvasGroup_t797 *);
	static CanvasGroup_get_interactable_m4888_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m4888_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C" bool CanvasGroup_get_blocksRaycasts_m5904 (CanvasGroup_t797 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m5904_ftn) (CanvasGroup_t797 *);
	static CanvasGroup_get_blocksRaycasts_m5904_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m5904_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C" bool CanvasGroup_get_ignoreParentGroups_m4696 (CanvasGroup_t797 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m4696_ftn) (CanvasGroup_t797 *);
	static CanvasGroup_get_ignoreParentGroups_m4696_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m4696_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C" bool CanvasGroup_IsRaycastLocationValid_m5905 (CanvasGroup_t797 * __this, Vector2_t7  ___sp, Camera_t14 * ___eventCamera, const MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m5904(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"

// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"


// System.Void UnityEngine.UIVertex::.cctor()
extern TypeInfo* UIVertex_t685_il2cpp_TypeInfo_var;
extern "C" void UIVertex__cctor_m5906 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t685_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t685  V_0 = {0};
	{
		Color32_t778  L_0 = {0};
		Color32__ctor_m4667(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t685_StaticFields*)UIVertex_t685_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6 = L_0;
		Vector4_t5  L_1 = {0};
		Vector4__ctor_m2728(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t685_StaticFields*)UIVertex_t685_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_1;
		Initobj (UIVertex_t685_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t215  L_2 = Vector3_get_zero_m2826(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_2;
		Vector3_t215  L_3 = Vector3_get_back_m3195(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___normal_1 = L_3;
		Vector4_t5  L_4 = ((UIVertex_t685_StaticFields*)UIVertex_t685_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		(&V_0)->___tangent_5 = L_4;
		Color32_t778  L_5 = ((UIVertex_t685_StaticFields*)UIVertex_t685_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6;
		(&V_0)->___color_2 = L_5;
		Vector2_t7  L_6 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv0_3 = L_6;
		Vector2_t7  L_7 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_7;
		UIVertex_t685  L_8 = V_0;
		((UIVertex_t685_StaticFields*)UIVertex_t685_il2cpp_TypeInfo_var->static_fields)->___simpleVert_8 = L_8;
		return;
	}
}
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"

// System.UInt16
#include "mscorlib_System_UInt16.h"


// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C" void CanvasRenderer_SetColor_m4702 (CanvasRenderer_t649 * __this, Color_t6  ___color, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m5907(NULL /*static, unused*/, __this, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m5907 (Object_t * __this /* static, unused */, CanvasRenderer_t649 * ___self, Color_t6 * ___color, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m5907_ftn) (CanvasRenderer_t649 *, Color_t6 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m5907_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m5907_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___color);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C" Color_t6  CanvasRenderer_GetColor_m4700 (CanvasRenderer_t649 * __this, const MethodInfo* method)
{
	typedef Color_t6  (*CanvasRenderer_GetColor_m4700_ftn) (CanvasRenderer_t649 *);
	static CanvasRenderer_GetColor_m4700_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_GetColor_m4700_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetColor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_isMask(System.Boolean)
extern "C" void CanvasRenderer_set_isMask_m4959 (CanvasRenderer_t649 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_isMask_m4959_ftn) (CanvasRenderer_t649 *, bool);
	static CanvasRenderer_set_isMask_m4959_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_isMask_m4959_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_isMask(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C" void CanvasRenderer_SetMaterial_m4693 (CanvasRenderer_t649 * __this, Material_t2 * ___material, Texture_t221 * ___texture, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m4693_ftn) (CanvasRenderer_t649 *, Material_t2 *, Texture_t221 *);
	static CanvasRenderer_SetMaterial_m4693_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m4693_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___material, ___texture);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1122_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral993;
extern "C" void CanvasRenderer_SetVertices_m4691 (CanvasRenderer_t649 * __this, List_1_t687 * ___vertices, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		UInt16_t1122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(682);
		_stringLiteral993 = il2cpp_codegen_string_literal_from_index(993);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t687 * L_0 = ___vertices;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0039;
		}
	}
	{
		ObjectU5BU5D_t470* L_2 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 1));
		uint16_t L_3 = ((int32_t)65535);
		Object_t * L_4 = Box(UInt16_t1122_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0)) = (Object_t *)L_4;
		String_t* L_5 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral993, L_2, /*hidden argument*/NULL);
		Debug_LogWarning_m4900(NULL /*static, unused*/, L_5, __this, /*hidden argument*/NULL);
		List_1_t687 * L_6 = ___vertices;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear() */, L_6);
	}

IL_0039:
	{
		List_1_t687 * L_7 = ___vertices;
		CanvasRenderer_SetVerticesInternal_m5908(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)
extern "C" void CanvasRenderer_SetVerticesInternal_m5908 (CanvasRenderer_t649 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternal_m5908_ftn) (CanvasRenderer_t649 *, Object_t *);
	static CanvasRenderer_SetVerticesInternal_m5908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternal_m5908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(UnityEngine.UIVertex[],System.Int32)
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t1122_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral993;
extern "C" void CanvasRenderer_SetVertices_m4780 (CanvasRenderer_t649 * __this, UIVertexU5BU5D_t684* ___vertices, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		UInt16_t1122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(682);
		_stringLiteral993 = il2cpp_codegen_string_literal_from_index(993);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t470* L_1 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 1));
		uint16_t L_2 = ((int32_t)65535);
		Object_t * L_3 = Box(UInt16_t1122_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)L_3;
		String_t* L_4 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral993, L_1, /*hidden argument*/NULL);
		Debug_LogWarning_m4900(NULL /*static, unused*/, L_4, __this, /*hidden argument*/NULL);
		___size = 0;
	}

IL_0031:
	{
		UIVertexU5BU5D_t684* L_5 = ___vertices;
		int32_t L_6 = ___size;
		CanvasRenderer_SetVerticesInternalArray_m5909(__this, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)
extern "C" void CanvasRenderer_SetVerticesInternalArray_m5909 (CanvasRenderer_t649 * __this, UIVertexU5BU5D_t684* ___vertices, int32_t ___size, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternalArray_m5909_ftn) (CanvasRenderer_t649 *, UIVertexU5BU5D_t684*, int32_t);
	static CanvasRenderer_SetVerticesInternalArray_m5909_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternalArray_m5909_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)");
	_il2cpp_icall_func(__this, ___vertices, ___size);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C" void CanvasRenderer_Clear_m4688 (CanvasRenderer_t649 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m4688_ftn) (CanvasRenderer_t649 *);
	static CanvasRenderer_Clear_m4688_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m4688_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C" int32_t CanvasRenderer_get_absoluteDepth_m4685 (CanvasRenderer_t649 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m4685_ftn) (CanvasRenderer_t649 *);
	static CanvasRenderer_get_absoluteDepth_m4685_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m4685_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.RectTransformUtility
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"

// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"


// System.Void UnityEngine.RectTransformUtility::.cctor()
extern TypeInfo* Vector3U5BU5D_t317_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility__cctor_m5910 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t317_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(190);
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t799_StaticFields*)RectTransformUtility_t799_il2cpp_TypeInfo_var->static_fields)->___s_Corners_0 = ((Vector3U5BU5D_t317*)SZArrayNew(Vector3U5BU5D_t317_il2cpp_TypeInfo_var, 4));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_RectangleContainsScreenPoint_m4718 (Object_t * __this /* static, unused */, RectTransform_t648 * ___rect, Vector2_t7  ___screenPoint, Camera_t14 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t648 * L_0 = ___rect;
		Camera_t14 * L_1 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m5911(NULL /*static, unused*/, L_0, (&___screenPoint), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C" bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m5911 (Object_t * __this /* static, unused */, RectTransform_t648 * ___rect, Vector2_t7 * ___screenPoint, Camera_t14 * ___cam, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m5911_ftn) (RectTransform_t648 *, Vector2_t7 *, Camera_t14 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m5911_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m5911_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect, ___screenPoint, ___cam);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" Vector2_t7  RectTransformUtility_PixelAdjustPoint_m4698 (Object_t * __this /* static, unused */, Vector2_t7  ___point, Transform_t243 * ___elementTransform, Canvas_t650 * ___canvas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t7  V_0 = {0};
	{
		Vector2_t7  L_0 = ___point;
		Transform_t243 * L_1 = ___elementTransform;
		Canvas_t650 * L_2 = ___canvas;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m5912(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t7  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_PixelAdjustPoint_m5912 (Object_t * __this /* static, unused */, Vector2_t7  ___point, Transform_t243 * ___elementTransform, Canvas_t650 * ___canvas, Vector2_t7 * ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t243 * L_0 = ___elementTransform;
		Canvas_t650 * L_1 = ___canvas;
		Vector2_t7 * L_2 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m5913(NULL /*static, unused*/, (&___point), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m5913 (Object_t * __this /* static, unused */, Vector2_t7 * ___point, Transform_t243 * ___elementTransform, Canvas_t650 * ___canvas, Vector2_t7 * ___output, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m5913_ftn) (Vector2_t7 *, Transform_t243 *, Canvas_t650 *, Vector2_t7 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m5913_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m5913_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point, ___elementTransform, ___canvas, ___output);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t225  RectTransformUtility_PixelAdjustRect_m4699 (Object_t * __this /* static, unused */, RectTransform_t648 * ___rectTransform, Canvas_t650 * ___canvas, const MethodInfo* method)
{
	typedef Rect_t225  (*RectTransformUtility_PixelAdjustRect_m4699_ftn) (RectTransform_t648 *, Canvas_t650 *);
	static RectTransformUtility_PixelAdjustRect_m4699_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_PixelAdjustRect_m4699_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)");
	return _il2cpp_icall_func(___rectTransform, ___canvas);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m5914 (Object_t * __this /* static, unused */, RectTransform_t648 * ___rect, Vector2_t7  ___screenPoint, Camera_t14 * ___cam, Vector3_t215 * ___worldPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t465  V_0 = {0};
	Plane_t808  V_1 = {0};
	float V_2 = 0.0f;
	{
		Vector3_t215 * L_0 = ___worldPoint;
		Vector2_t7  L_1 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_2 = Vector2_op_Implicit_m4639(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		*L_0 = L_2;
		Camera_t14 * L_3 = ___cam;
		Vector2_t7  L_4 = ___screenPoint;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		Ray_t465  L_5 = RectTransformUtility_ScreenPointToRay_m5915(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t648 * L_6 = ___rect;
		NullCheck(L_6);
		Quaternion_t261  L_7 = Transform_get_rotation_m2897(L_6, /*hidden argument*/NULL);
		Vector3_t215  L_8 = Vector3_get_back_m3195(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t215  L_9 = Quaternion_op_Multiply_m2900(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t648 * L_10 = ___rect;
		NullCheck(L_10);
		Vector3_t215  L_11 = Transform_get_position_m2894(L_10, /*hidden argument*/NULL);
		Plane__ctor_m4791((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t465  L_12 = V_0;
		bool L_13 = Plane_Raycast_m4792((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		Vector3_t215 * L_14 = ___worldPoint;
		float L_15 = V_2;
		Vector3_t215  L_16 = Ray_GetPoint_m4793((&V_0), L_15, /*hidden argument*/NULL);
		*L_14 = L_16;
		return 1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m4749 (Object_t * __this /* static, unused */, RectTransform_t648 * ___rect, Vector2_t7  ___screenPoint, Camera_t14 * ___cam, Vector2_t7 * ___localPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t215  V_0 = {0};
	{
		Vector2_t7 * L_0 = ___localPoint;
		Vector2_t7  L_1 = Vector2_get_zero_m2793(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_0 = L_1;
		RectTransform_t648 * L_2 = ___rect;
		Vector2_t7  L_3 = ___screenPoint;
		Camera_t14 * L_4 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m5914(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t7 * L_6 = ___localPoint;
		RectTransform_t648 * L_7 = ___rect;
		Vector3_t215  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t215  L_9 = Transform_InverseTransformPoint_m4790(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t7  L_10 = Vector2_op_Implicit_m4613(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		*L_6 = L_10;
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C" Ray_t465  RectTransformUtility_ScreenPointToRay_m5915 (Object_t * __this /* static, unused */, Camera_t14 * ___cam, Vector2_t7  ___screenPos, const MethodInfo* method)
{
	Vector3_t215  V_0 = {0};
	{
		Camera_t14 * L_0 = ___cam;
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t14 * L_2 = ___cam;
		Vector2_t7  L_3 = ___screenPos;
		Vector3_t215  L_4 = Vector2_op_Implicit_m4639(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t465  L_5 = Camera_ScreenPointToRay_m3240(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t7  L_6 = ___screenPos;
		Vector3_t215  L_7 = Vector2_op_Implicit_m4639(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t215 * L_8 = (&V_0);
		float L_9 = (L_8->___z_3);
		L_8->___z_3 = ((float)((float)L_9-(float)(100.0f)));
		Vector3_t215  L_10 = V_0;
		Vector3_t215  L_11 = Vector3_get_forward_m2966(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t465  L_12 = {0};
		Ray__ctor_m2896(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t648_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m4854 (Object_t * __this /* static, unused */, RectTransform_t648 * ___rect, int32_t ___axis, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t648 * V_1 = {0};
	Vector2_t7  V_2 = {0};
	Vector2_t7  V_3 = {0};
	Vector2_t7  V_4 = {0};
	Vector2_t7  V_5 = {0};
	float V_6 = 0.0f;
	{
		RectTransform_t648 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t648 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t243 * L_5 = Transform_GetChild_m4946(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t648 *)IsInst(L_5, RectTransform_t648_il2cpp_TypeInfo_var));
		RectTransform_t648 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_6, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t648 * L_8 = V_1;
		int32_t L_9 = ___axis;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m4854(NULL /*static, unused*/, L_8, L_9, 0, 1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t648 * L_12 = ___rect;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m4947(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t648 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t7  L_15 = RectTransform_get_pivot_m4738(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis;
		int32_t L_17 = ___axis;
		float L_18 = Vector2_get_Item_m4746((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m4754((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t648 * L_19 = ___rect;
		Vector2_t7  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m4829(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t648 * L_22 = ___rect;
		NullCheck(L_22);
		Vector2_t7  L_23 = RectTransform_get_anchoredPosition_m4825(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis;
		int32_t L_25 = ___axis;
		float L_26 = Vector2_get_Item_m4746((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m4754((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t648 * L_27 = ___rect;
		Vector2_t7  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m4828(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t648 * L_29 = ___rect;
		NullCheck(L_29);
		Vector2_t7  L_30 = RectTransform_get_anchorMin_m4739(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t648 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t7  L_32 = RectTransform_get_anchorMax_m4824(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis;
		float L_34 = Vector2_get_Item_m4746((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis;
		int32_t L_36 = ___axis;
		float L_37 = Vector2_get_Item_m4746((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m4754((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis;
		float L_39 = V_6;
		Vector2_set_Item_m4754((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t648 * L_40 = ___rect;
		Vector2_t7  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m4827(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t648 * L_42 = ___rect;
		Vector2_t7  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m4740(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t648_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t799_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutAxes_m4853 (Object_t * __this /* static, unused */, RectTransform_t648 * ___rect, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		RectTransformUtility_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t648 * V_1 = {0};
	{
		RectTransform_t648 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t648 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t243 * L_5 = Transform_GetChild_m4946(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t648 *)IsInst(L_5, RectTransform_t648_il2cpp_TypeInfo_var));
		RectTransform_t648 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_6, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t648 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m4853(NULL /*static, unused*/, L_8, 0, 1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t648 * L_11 = ___rect;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m4947(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t648 * L_13 = ___rect;
		RectTransform_t648 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t7  L_15 = RectTransform_get_pivot_m4738(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		Vector2_t7  L_16 = RectTransformUtility_GetTransposed_m5916(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m4829(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t648 * L_17 = ___rect;
		RectTransform_t648 * L_18 = ___rect;
		NullCheck(L_18);
		Vector2_t7  L_19 = RectTransform_get_sizeDelta_m4826(L_18, /*hidden argument*/NULL);
		Vector2_t7  L_20 = RectTransformUtility_GetTransposed_m5916(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m4741(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t648 * L_22 = ___rect;
		RectTransform_t648 * L_23 = ___rect;
		NullCheck(L_23);
		Vector2_t7  L_24 = RectTransform_get_anchoredPosition_m4825(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t799_il2cpp_TypeInfo_var);
		Vector2_t7  L_25 = RectTransformUtility_GetTransposed_m5916(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m4828(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t648 * L_26 = ___rect;
		RectTransform_t648 * L_27 = ___rect;
		NullCheck(L_27);
		Vector2_t7  L_28 = RectTransform_get_anchorMin_m4739(L_27, /*hidden argument*/NULL);
		Vector2_t7  L_29 = RectTransformUtility_GetTransposed_m5916(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m4827(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t648 * L_30 = ___rect;
		RectTransform_t648 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t7  L_32 = RectTransform_get_anchorMax_m4824(L_31, /*hidden argument*/NULL);
		Vector2_t7  L_33 = RectTransformUtility_GetTransposed_m5916(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m4740(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C" Vector2_t7  RectTransformUtility_GetTransposed_m5916 (Object_t * __this /* static, unused */, Vector2_t7  ___input, const MethodInfo* method)
{
	{
		float L_0 = ((&___input)->___y_2);
		float L_1 = ((&___input)->___x_1);
		Vector2_t7  L_2 = {0};
		Vector2__ctor_m2714(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_Request.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.Request
#include "UnityEngine_UnityEngine_Networking_Match_RequestMethodDeclarations.h"

// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"


// System.Void UnityEngine.Networking.Match.Request::.ctor()
extern "C" void Request__ctor_m5917 (Request_t960 * __this, const MethodInfo* method)
{
	{
		__this->___version_0 = 2;
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.SourceID UnityEngine.Networking.Match.Request::get_sourceId()
extern "C" uint64_t Request_get_sourceId_m5918 (Request_t960 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CsourceIdU3Ek__BackingField_1);
		return L_0;
	}
}
// UnityEngine.Networking.Types.AppID UnityEngine.Networking.Match.Request::get_appId()
extern "C" uint64_t Request_get_appId_m5919 (Request_t960 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CappIdU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.Networking.Match.Request::get_domain()
extern "C" int32_t Request_get_domain_m5920 (Request_t960 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CdomainU3Ek__BackingField_3);
		return L_0;
	}
}
// System.String UnityEngine.Networking.Match.Request::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t978_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t977_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral994;
extern Il2CppCodeGenString* _stringLiteral995;
extern "C" String_t* Request_ToString_m5921 (Request_t960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		SourceID_t978_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(683);
		AppID_t977_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(684);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		_stringLiteral994 = il2cpp_codegen_string_literal_from_index(994);
		_stringLiteral995 = il2cpp_codegen_string_literal_from_index(995);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = Object_ToString_m6402(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		uint64_t L_3 = Request_get_sourceId_m5918(__this, /*hidden argument*/NULL);
		uint64_t L_4 = L_3;
		Object_t * L_5 = Box(SourceID_t978_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = Enum_ToString_m6403(L_5, _stringLiteral995, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_6;
		ObjectU5BU5D_t470* L_7 = L_2;
		uint64_t L_8 = Request_get_appId_m5919(__this, /*hidden argument*/NULL);
		uint64_t L_9 = L_8;
		Object_t * L_10 = Box(AppID_t977_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = Enum_ToString_m6403(L_10, _stringLiteral995, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_7;
		int32_t L_13 = Request_get_domain_m5920(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3)) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral994, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBase.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.ResponseBase
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBaseMethodDeclarations.h"

// System.FormatException
#include "mscorlib_System_FormatException.h"
// System.FormatException
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"


// System.Void UnityEngine.Networking.Match.ResponseBase::.ctor()
extern "C" void ResponseBase__ctor_m5922 (ResponseBase_t961 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.ResponseBase::ParseJSONString(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral996;
extern "C" String_t* ResponseBase_ParseJSONString_m5923 (ResponseBase_t961 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral996 = il2cpp_codegen_string_literal_from_index(996);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t1088_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		return ((String_t*)IsInst(L_3, String_t_il2cpp_TypeInfo_var));
	}

IL_0015:
	{
		String_t* L_4 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m3031(NULL /*static, unused*/, L_4, _stringLiteral996, /*hidden argument*/NULL);
		FormatException_t1123 * L_6 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_6, L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}
}
// System.Int32 UnityEngine.Networking.Match.ResponseBase::ParseJSONInt32(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral996;
extern "C" int32_t ResponseBase_ParseJSONInt32_m5924 (ResponseBase_t961 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral996 = il2cpp_codegen_string_literal_from_index(996);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t1088_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		int32_t L_4 = Convert_ToInt32_m6405(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3031(NULL /*static, unused*/, L_5, _stringLiteral996, /*hidden argument*/NULL);
		FormatException_t1123 * L_7 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_7);
	}
}
// System.UInt16 UnityEngine.Networking.Match.ResponseBase::ParseJSONUInt16(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral996;
extern "C" uint16_t ResponseBase_ParseJSONUInt16_m5925 (ResponseBase_t961 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral996 = il2cpp_codegen_string_literal_from_index(996);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t1088_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		uint16_t L_4 = Convert_ToUInt16_m6406(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3031(NULL /*static, unused*/, L_5, _stringLiteral996, /*hidden argument*/NULL);
		FormatException_t1123 * L_7 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_7);
	}
}
// System.UInt64 UnityEngine.Networking.Match.ResponseBase::ParseJSONUInt64(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral996;
extern "C" uint64_t ResponseBase_ParseJSONUInt64_m5926 (ResponseBase_t961 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral996 = il2cpp_codegen_string_literal_from_index(996);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t1088_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		uint64_t L_4 = Convert_ToUInt64_m6407(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3031(NULL /*static, unused*/, L_5, _stringLiteral996, /*hidden argument*/NULL);
		FormatException_t1123 * L_7 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_7);
	}
}
// System.Boolean UnityEngine.Networking.Match.ResponseBase::ParseJSONBool(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t524_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral996;
extern "C" bool ResponseBase_ParseJSONBool_m5927 (ResponseBase_t961 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		Convert_t524_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral996 = il2cpp_codegen_string_literal_from_index(996);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t1088_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t524_il2cpp_TypeInfo_var);
		bool L_4 = Convert_ToBoolean_m6408(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m3031(NULL /*static, unused*/, L_5, _stringLiteral996, /*hidden argument*/NULL);
		FormatException_t1123 * L_7 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_7);
	}
}
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_Response.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.Response
#include "UnityEngine_UnityEngine_Networking_Match_ResponseMethodDeclarations.h"



// System.Void UnityEngine.Networking.Match.Response::.ctor()
extern "C" void Response__ctor_m5928 (Response_t962 * __this, const MethodInfo* method)
{
	{
		ResponseBase__ctor_m5922(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.Response::get_success()
extern "C" bool Response_get_success_m5929 (Response_t962 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CsuccessU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.Response::set_success(System.Boolean)
extern "C" void Response_set_success_m5930 (Response_t962 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CsuccessU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.Response::get_extendedInfo()
extern "C" String_t* Response_get_extendedInfo_m5931 (Response_t962 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CextendedInfoU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.Response::set_extendedInfo(System.String)
extern "C" void Response_set_extendedInfo_m5932 (Response_t962 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CextendedInfoU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.Response::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t536_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral997;
extern "C" String_t* Response_ToString_m5933 (Response_t962 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Boolean_t536_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(219);
		_stringLiteral997 = il2cpp_codegen_string_literal_from_index(997);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 3));
		String_t* L_1 = Object_ToString_m6402(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		bool L_3 = Response_get_success_m5929(__this, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Object_t * L_5 = Box(Boolean_t536_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t470* L_6 = L_2;
		String_t* L_7 = Response_get_extendedInfo_m5931(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral997, L_6, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Networking.Match.Response::Parse(System.Object)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral998;
extern Il2CppCodeGenString* _stringLiteral999;
extern Il2CppCodeGenString* _stringLiteral1000;
extern "C" void Response_Parse_m5934 (Response_t962 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral998 = il2cpp_codegen_string_literal_from_index(998);
		_stringLiteral999 = il2cpp_codegen_string_literal_from_index(999);
		_stringLiteral1000 = il2cpp_codegen_string_literal_from_index(1000);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((Object_t*)IsInst(L_0, IDictionary_2_t1088_il2cpp_TypeInfo_var));
		Object_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		Object_t * L_2 = ___obj;
		Object_t* L_3 = V_0;
		bool L_4 = ResponseBase_ParseJSONBool_m5927(__this, _stringLiteral998, L_2, L_3, /*hidden argument*/NULL);
		Response_set_success_m5930(__this, L_4, /*hidden argument*/NULL);
		Object_t * L_5 = ___obj;
		Object_t* L_6 = V_0;
		String_t* L_7 = ResponseBase_ParseJSONString_m5923(__this, _stringLiteral999, L_5, L_6, /*hidden argument*/NULL);
		Response_set_extendedInfo_m5932(__this, L_7, /*hidden argument*/NULL);
		bool L_8 = Response_get_success_m5929(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_9 = Response_get_extendedInfo_m5931(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m3031(NULL /*static, unused*/, _stringLiteral1000, L_9, /*hidden argument*/NULL);
		FormatException_t1123 * L_11 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_11, L_10, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0054:
	{
		return;
	}
}
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponse.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponseMethodDeclarations.h"



// System.Void UnityEngine.Networking.Match.BasicResponse::.ctor()
extern "C" void BasicResponse__ctor_m5935 (BasicResponse_t963 * __this, const MethodInfo* method)
{
	{
		Response__ctor_m5928(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequest.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.CreateMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequestMethodDeclarations.h"

// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12.h"
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_12MethodDeclarations.h"


// System.Void UnityEngine.Networking.Match.CreateMatchRequest::.ctor()
extern "C" void CreateMatchRequest__ctor_m5936 (CreateMatchRequest_t965 * __this, const MethodInfo* method)
{
	{
		Request__ctor_m5917(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_name()
extern "C" String_t* CreateMatchRequest_get_name_m5937 (CreateMatchRequest_t965 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_name(System.String)
extern "C" void CreateMatchRequest_set_name_m5938 (CreateMatchRequest_t965 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.UInt32 UnityEngine.Networking.Match.CreateMatchRequest::get_size()
extern "C" uint32_t CreateMatchRequest_get_size_m5939 (CreateMatchRequest_t965 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___U3CsizeU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_size(System.UInt32)
extern "C" void CreateMatchRequest_set_size_m5940 (CreateMatchRequest_t965 * __this, uint32_t ___value, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value;
		__this->___U3CsizeU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.CreateMatchRequest::get_advertise()
extern "C" bool CreateMatchRequest_get_advertise_m5941 (CreateMatchRequest_t965 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CadvertiseU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_advertise(System.Boolean)
extern "C" void CreateMatchRequest_set_advertise_m5942 (CreateMatchRequest_t965 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CadvertiseU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_password()
extern "C" String_t* CreateMatchRequest_get_password_m5943 (CreateMatchRequest_t965 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CpasswordU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_password(System.String)
extern "C" void CreateMatchRequest_set_password_m5944 (CreateMatchRequest_t965 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CpasswordU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.CreateMatchRequest::get_matchAttributes()
extern "C" Dictionary_2_t964 * CreateMatchRequest_get_matchAttributes_m5945 (CreateMatchRequest_t965 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t964 * L_0 = (__this->___U3CmatchAttributesU3Ek__BackingField_8);
		return L_0;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchRequest::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t1124_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t536_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1001;
extern Il2CppCodeGenString* _stringLiteral1002;
extern Il2CppCodeGenString* _stringLiteral1003;
extern "C" String_t* CreateMatchRequest_ToString_m5946 (CreateMatchRequest_t965 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		UInt32_t1124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(687);
		Boolean_t536_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(219);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		_stringLiteral1001 = il2cpp_codegen_string_literal_from_index(1001);
		_stringLiteral1002 = il2cpp_codegen_string_literal_from_index(1002);
		_stringLiteral1003 = il2cpp_codegen_string_literal_from_index(1003);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t470* G_B2_1 = {0};
	ObjectU5BU5D_t470* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t470* G_B1_1 = {0};
	ObjectU5BU5D_t470* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t470* G_B3_2 = {0};
	ObjectU5BU5D_t470* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t470* G_B5_1 = {0};
	ObjectU5BU5D_t470* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t470* G_B4_1 = {0};
	ObjectU5BU5D_t470* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t470* G_B6_2 = {0};
	ObjectU5BU5D_t470* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = Request_ToString_m5921(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		String_t* L_3 = CreateMatchRequest_get_name_m5937(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_2;
		uint32_t L_5 = CreateMatchRequest_get_size_m5939(__this, /*hidden argument*/NULL);
		uint32_t L_6 = L_5;
		Object_t * L_7 = Box(UInt32_t1124_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		bool L_9 = CreateMatchRequest_get_advertise_m5941(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t536_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_11;
		ObjectU5BU5D_t470* L_12 = L_8;
		String_t* L_13 = CreateMatchRequest_get_password_m5943(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_15 = String_op_Equality_m2981(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		G_B1_0 = 4;
		G_B1_1 = L_12;
		G_B1_2 = L_12;
		G_B1_3 = _stringLiteral1001;
		if (!L_15)
		{
			G_B2_0 = 4;
			G_B2_1 = L_12;
			G_B2_2 = L_12;
			G_B2_3 = _stringLiteral1001;
			goto IL_005a;
		}
	}
	{
		G_B3_0 = _stringLiteral1002;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_005f;
	}

IL_005a:
	{
		G_B3_0 = _stringLiteral1003;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_005f:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1)) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t470* L_16 = G_B3_3;
		Dictionary_2_t964 * L_17 = CreateMatchRequest_get_matchAttributes_m5945(__this, /*hidden argument*/NULL);
		G_B4_0 = 5;
		G_B4_1 = L_16;
		G_B4_2 = L_16;
		G_B4_3 = G_B3_4;
		if (L_17)
		{
			G_B5_0 = 5;
			G_B5_1 = L_16;
			G_B5_2 = L_16;
			G_B5_3 = G_B3_4;
			goto IL_0073;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_007e;
	}

IL_0073:
	{
		Dictionary_2_t964 * L_18 = CreateMatchRequest_get_matchAttributes_m5945(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Count() */, L_18);
		G_B6_0 = L_19;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_007e:
	{
		int32_t L_20 = G_B6_0;
		Object_t * L_21 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_20);
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1)) = (Object_t *)L_21;
		String_t* L_22 = UnityString_Format_m5568(NULL /*static, unused*/, G_B6_4, G_B6_3, /*hidden argument*/NULL);
		return L_22;
	}
}
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponse.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.CreateMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponseMethodDeclarations.h"

// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"


// System.Void UnityEngine.Networking.Match.CreateMatchResponse::.ctor()
extern "C" void CreateMatchResponse__ctor_m5947 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	{
		BasicResponse__ctor_m5935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_address()
extern "C" String_t* CreateMatchResponse_get_address_m5948 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaddressU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_address(System.String)
extern "C" void CreateMatchResponse_set_address_m5949 (CreateMatchResponse_t966 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaddressU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.CreateMatchResponse::get_port()
extern "C" int32_t CreateMatchResponse_get_port_m5950 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CportU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_port(System.Int32)
extern "C" void CreateMatchResponse_set_port_m5951 (CreateMatchResponse_t966 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CportU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.CreateMatchResponse::get_networkId()
extern "C" uint64_t CreateMatchResponse_get_networkId_m5952 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void CreateMatchResponse_set_networkId_m5953 (CreateMatchResponse_t966 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_accessTokenString()
extern "C" String_t* CreateMatchResponse_get_accessTokenString_m5954 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaccessTokenStringU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_accessTokenString(System.String)
extern "C" void CreateMatchResponse_set_accessTokenString_m5955 (CreateMatchResponse_t966 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaccessTokenStringU3Ek__BackingField_5 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.CreateMatchResponse::get_nodeId()
extern "C" uint16_t CreateMatchResponse_get_nodeId_m5956 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___U3CnodeIdU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C" void CreateMatchResponse_set_nodeId_m5957 (CreateMatchResponse_t966 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___U3CnodeIdU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.CreateMatchResponse::get_usingRelay()
extern "C" bool CreateMatchResponse_get_usingRelay_m5958 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CusingRelayU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_usingRelay(System.Boolean)
extern "C" void CreateMatchResponse_set_usingRelay_m5959 (CreateMatchResponse_t966 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CusingRelayU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchResponse::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t979_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t980_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t536_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1004;
extern Il2CppCodeGenString* _stringLiteral995;
extern "C" String_t* CreateMatchResponse_ToString_m5960 (CreateMatchResponse_t966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		NetworkID_t979_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		NodeID_t980_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(689);
		Boolean_t536_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(219);
		_stringLiteral1004 = il2cpp_codegen_string_literal_from_index(1004);
		_stringLiteral995 = il2cpp_codegen_string_literal_from_index(995);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = Response_ToString_m5933(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		String_t* L_3 = CreateMatchResponse_get_address_m5948(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_2;
		int32_t L_5 = CreateMatchResponse_get_port_m5950(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		uint64_t L_9 = CreateMatchResponse_get_networkId_m5952(__this, /*hidden argument*/NULL);
		uint64_t L_10 = L_9;
		Object_t * L_11 = Box(NetworkID_t979_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_11);
		String_t* L_12 = Enum_ToString_m6403(L_11, _stringLiteral995, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_12;
		ObjectU5BU5D_t470* L_13 = L_8;
		uint16_t L_14 = CreateMatchResponse_get_nodeId_m5956(__this, /*hidden argument*/NULL);
		uint16_t L_15 = L_14;
		Object_t * L_16 = Box(NodeID_t980_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		String_t* L_17 = Enum_ToString_m6403(L_16, _stringLiteral995, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4)) = (Object_t *)L_17;
		ObjectU5BU5D_t470* L_18 = L_13;
		bool L_19 = CreateMatchResponse_get_usingRelay_m5958(__this, /*hidden argument*/NULL);
		bool L_20 = L_19;
		Object_t * L_21 = Box(Boolean_t536_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 5);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 5)) = (Object_t *)L_21;
		String_t* L_22 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral1004, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::Parse(System.Object)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1005;
extern Il2CppCodeGenString* _stringLiteral1006;
extern Il2CppCodeGenString* _stringLiteral1007;
extern Il2CppCodeGenString* _stringLiteral1008;
extern Il2CppCodeGenString* _stringLiteral1009;
extern Il2CppCodeGenString* _stringLiteral1010;
extern Il2CppCodeGenString* _stringLiteral1011;
extern "C" void CreateMatchResponse_Parse_m5961 (CreateMatchResponse_t966 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral1005 = il2cpp_codegen_string_literal_from_index(1005);
		_stringLiteral1006 = il2cpp_codegen_string_literal_from_index(1006);
		_stringLiteral1007 = il2cpp_codegen_string_literal_from_index(1007);
		_stringLiteral1008 = il2cpp_codegen_string_literal_from_index(1008);
		_stringLiteral1009 = il2cpp_codegen_string_literal_from_index(1009);
		_stringLiteral1010 = il2cpp_codegen_string_literal_from_index(1010);
		_stringLiteral1011 = il2cpp_codegen_string_literal_from_index(1011);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		Response_Parse_m5934(__this, L_0, /*hidden argument*/NULL);
		Object_t * L_1 = ___obj;
		V_0 = ((Object_t*)IsInst(L_1, IDictionary_2_t1088_il2cpp_TypeInfo_var));
		Object_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_008b;
		}
	}
	{
		Object_t * L_3 = ___obj;
		Object_t* L_4 = V_0;
		String_t* L_5 = ResponseBase_ParseJSONString_m5923(__this, _stringLiteral1005, L_3, L_4, /*hidden argument*/NULL);
		CreateMatchResponse_set_address_m5949(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ___obj;
		Object_t* L_7 = V_0;
		int32_t L_8 = ResponseBase_ParseJSONInt32_m5924(__this, _stringLiteral1006, L_6, L_7, /*hidden argument*/NULL);
		CreateMatchResponse_set_port_m5951(__this, L_8, /*hidden argument*/NULL);
		Object_t * L_9 = ___obj;
		Object_t* L_10 = V_0;
		uint64_t L_11 = ResponseBase_ParseJSONUInt64_m5926(__this, _stringLiteral1007, L_9, L_10, /*hidden argument*/NULL);
		CreateMatchResponse_set_networkId_m5953(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = ___obj;
		Object_t* L_13 = V_0;
		String_t* L_14 = ResponseBase_ParseJSONString_m5923(__this, _stringLiteral1008, L_12, L_13, /*hidden argument*/NULL);
		CreateMatchResponse_set_accessTokenString_m5955(__this, L_14, /*hidden argument*/NULL);
		Object_t * L_15 = ___obj;
		Object_t* L_16 = V_0;
		uint16_t L_17 = ResponseBase_ParseJSONUInt16_m5925(__this, _stringLiteral1009, L_15, L_16, /*hidden argument*/NULL);
		CreateMatchResponse_set_nodeId_m5957(__this, L_17, /*hidden argument*/NULL);
		Object_t * L_18 = ___obj;
		Object_t* L_19 = V_0;
		bool L_20 = ResponseBase_ParseJSONBool_m5927(__this, _stringLiteral1010, L_18, L_19, /*hidden argument*/NULL);
		CreateMatchResponse_set_usingRelay_m5959(__this, L_20, /*hidden argument*/NULL);
		goto IL_00a1;
	}

IL_008b:
	{
		Object_t * L_21 = ___obj;
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m3031(NULL /*static, unused*/, _stringLiteral1011, L_22, /*hidden argument*/NULL);
		FormatException_t1123 * L_24 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_24, L_23, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_24);
	}

IL_00a1:
	{
		return;
	}
}
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequest.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.JoinMatchRequest
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequestMethodDeclarations.h"



// System.Void UnityEngine.Networking.Match.JoinMatchRequest::.ctor()
extern "C" void JoinMatchRequest__ctor_m5962 (JoinMatchRequest_t967 * __this, const MethodInfo* method)
{
	{
		Request__ctor_m5917(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchRequest::get_networkId()
extern "C" uint64_t JoinMatchRequest_get_networkId_m5963 (JoinMatchRequest_t967 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void JoinMatchRequest_set_networkId_m5964 (JoinMatchRequest_t967 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchRequest::get_password()
extern "C" String_t* JoinMatchRequest_get_password_m5965 (JoinMatchRequest_t967 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CpasswordU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_password(System.String)
extern "C" void JoinMatchRequest_set_password_m5966 (JoinMatchRequest_t967 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CpasswordU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchRequest::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t979_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1012;
extern Il2CppCodeGenString* _stringLiteral995;
extern Il2CppCodeGenString* _stringLiteral1002;
extern Il2CppCodeGenString* _stringLiteral1003;
extern "C" String_t* JoinMatchRequest_ToString_m5967 (JoinMatchRequest_t967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		NetworkID_t979_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		_stringLiteral1012 = il2cpp_codegen_string_literal_from_index(1012);
		_stringLiteral995 = il2cpp_codegen_string_literal_from_index(995);
		_stringLiteral1002 = il2cpp_codegen_string_literal_from_index(1002);
		_stringLiteral1003 = il2cpp_codegen_string_literal_from_index(1003);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t470* G_B2_1 = {0};
	ObjectU5BU5D_t470* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t470* G_B1_1 = {0};
	ObjectU5BU5D_t470* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t470* G_B3_2 = {0};
	ObjectU5BU5D_t470* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 3));
		String_t* L_1 = Request_ToString_m5921(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		uint64_t L_3 = JoinMatchRequest_get_networkId_m5963(__this, /*hidden argument*/NULL);
		uint64_t L_4 = L_3;
		Object_t * L_5 = Box(NetworkID_t979_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = Enum_ToString_m6403(L_5, _stringLiteral995, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_6;
		ObjectU5BU5D_t470* L_7 = L_2;
		String_t* L_8 = JoinMatchRequest_get_password_m5965(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_10 = String_op_Equality_m2981(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		G_B1_0 = 2;
		G_B1_1 = L_7;
		G_B1_2 = L_7;
		G_B1_3 = _stringLiteral1012;
		if (!L_10)
		{
			G_B2_0 = 2;
			G_B2_1 = L_7;
			G_B2_2 = L_7;
			G_B2_3 = _stringLiteral1012;
			goto IL_004d;
		}
	}
	{
		G_B3_0 = _stringLiteral1002;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0052;
	}

IL_004d:
	{
		G_B3_0 = _stringLiteral1003;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0052:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1)) = (Object_t *)G_B3_0;
		String_t* L_11 = UnityString_Format_m5568(NULL /*static, unused*/, G_B3_4, G_B3_3, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponse.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.Networking.Match.JoinMatchResponse
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponseMethodDeclarations.h"



// System.Void UnityEngine.Networking.Match.JoinMatchResponse::.ctor()
extern "C" void JoinMatchResponse__ctor_m5968 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	{
		BasicResponse__ctor_m5935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_address()
extern "C" String_t* JoinMatchResponse_get_address_m5969 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaddressU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_address(System.String)
extern "C" void JoinMatchResponse_set_address_m5970 (JoinMatchResponse_t968 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaddressU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.JoinMatchResponse::get_port()
extern "C" int32_t JoinMatchResponse_get_port_m5971 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CportU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_port(System.Int32)
extern "C" void JoinMatchResponse_set_port_m5972 (JoinMatchResponse_t968 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CportU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchResponse::get_networkId()
extern "C" uint64_t JoinMatchResponse_get_networkId_m5973 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void JoinMatchResponse_set_networkId_m5974 (JoinMatchResponse_t968 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_accessTokenString()
extern "C" String_t* JoinMatchResponse_get_accessTokenString_m5975 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaccessTokenStringU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_accessTokenString(System.String)
extern "C" void JoinMatchResponse_set_accessTokenString_m5976 (JoinMatchResponse_t968 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaccessTokenStringU3Ek__BackingField_5 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.JoinMatchResponse::get_nodeId()
extern "C" uint16_t JoinMatchResponse_get_nodeId_m5977 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___U3CnodeIdU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C" void JoinMatchResponse_set_nodeId_m5978 (JoinMatchResponse_t968 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___U3CnodeIdU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.JoinMatchResponse::get_usingRelay()
extern "C" bool JoinMatchResponse_get_usingRelay_m5979 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CusingRelayU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_usingRelay(System.Boolean)
extern "C" void JoinMatchResponse_set_usingRelay_m5980 (JoinMatchResponse_t968 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CusingRelayU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchResponse::ToString()
extern TypeInfo* ObjectU5BU5D_t470_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t478_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t979_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t980_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t536_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1004;
extern Il2CppCodeGenString* _stringLiteral995;
extern "C" String_t* JoinMatchResponse_ToString_m5981 (JoinMatchResponse_t968 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t470_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(232);
		Int32_t478_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(160);
		NetworkID_t979_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(688);
		NodeID_t980_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(689);
		Boolean_t536_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(219);
		_stringLiteral1004 = il2cpp_codegen_string_literal_from_index(1004);
		_stringLiteral995 = il2cpp_codegen_string_literal_from_index(995);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t470* L_0 = ((ObjectU5BU5D_t470*)SZArrayNew(ObjectU5BU5D_t470_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = Response_ToString_m5933(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)L_1;
		ObjectU5BU5D_t470* L_2 = L_0;
		String_t* L_3 = JoinMatchResponse_get_address_m5969(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t470* L_4 = L_2;
		int32_t L_5 = JoinMatchResponse_get_port_m5971(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t478_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)L_7;
		ObjectU5BU5D_t470* L_8 = L_4;
		uint64_t L_9 = JoinMatchResponse_get_networkId_m5973(__this, /*hidden argument*/NULL);
		uint64_t L_10 = L_9;
		Object_t * L_11 = Box(NetworkID_t979_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_11);
		String_t* L_12 = Enum_ToString_m6403(L_11, _stringLiteral995, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_12;
		ObjectU5BU5D_t470* L_13 = L_8;
		uint16_t L_14 = JoinMatchResponse_get_nodeId_m5977(__this, /*hidden argument*/NULL);
		uint16_t L_15 = L_14;
		Object_t * L_16 = Box(NodeID_t980_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		String_t* L_17 = Enum_ToString_m6403(L_16, _stringLiteral995, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4)) = (Object_t *)L_17;
		ObjectU5BU5D_t470* L_18 = L_13;
		bool L_19 = JoinMatchResponse_get_usingRelay_m5979(__this, /*hidden argument*/NULL);
		bool L_20 = L_19;
		Object_t * L_21 = Box(Boolean_t536_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 5);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 5)) = (Object_t *)L_21;
		String_t* L_22 = UnityString_Format_m5568(NULL /*static, unused*/, _stringLiteral1004, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::Parse(System.Object)
extern TypeInfo* IDictionary_2_t1088_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t1123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1005;
extern Il2CppCodeGenString* _stringLiteral1006;
extern Il2CppCodeGenString* _stringLiteral1007;
extern Il2CppCodeGenString* _stringLiteral1008;
extern Il2CppCodeGenString* _stringLiteral1009;
extern Il2CppCodeGenString* _stringLiteral1010;
extern Il2CppCodeGenString* _stringLiteral1011;
extern "C" void JoinMatchResponse_Parse_m5982 (JoinMatchResponse_t968 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t1088_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(685);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(161);
		FormatException_t1123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(686);
		_stringLiteral1005 = il2cpp_codegen_string_literal_from_index(1005);
		_stringLiteral1006 = il2cpp_codegen_string_literal_from_index(1006);
		_stringLiteral1007 = il2cpp_codegen_string_literal_from_index(1007);
		_stringLiteral1008 = il2cpp_codegen_string_literal_from_index(1008);
		_stringLiteral1009 = il2cpp_codegen_string_literal_from_index(1009);
		_stringLiteral1010 = il2cpp_codegen_string_literal_from_index(1010);
		_stringLiteral1011 = il2cpp_codegen_string_literal_from_index(1011);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		Response_Parse_m5934(__this, L_0, /*hidden argument*/NULL);
		Object_t * L_1 = ___obj;
		V_0 = ((Object_t*)IsInst(L_1, IDictionary_2_t1088_il2cpp_TypeInfo_var));
		Object_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_008b;
		}
	}
	{
		Object_t * L_3 = ___obj;
		Object_t* L_4 = V_0;
		String_t* L_5 = ResponseBase_ParseJSONString_m5923(__this, _stringLiteral1005, L_3, L_4, /*hidden argument*/NULL);
		JoinMatchResponse_set_address_m5970(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ___obj;
		Object_t* L_7 = V_0;
		int32_t L_8 = ResponseBase_ParseJSONInt32_m5924(__this, _stringLiteral1006, L_6, L_7, /*hidden argument*/NULL);
		JoinMatchResponse_set_port_m5972(__this, L_8, /*hidden argument*/NULL);
		Object_t * L_9 = ___obj;
		Object_t* L_10 = V_0;
		uint64_t L_11 = ResponseBase_ParseJSONUInt64_m5926(__this, _stringLiteral1007, L_9, L_10, /*hidden argument*/NULL);
		JoinMatchResponse_set_networkId_m5974(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = ___obj;
		Object_t* L_13 = V_0;
		String_t* L_14 = ResponseBase_ParseJSONString_m5923(__this, _stringLiteral1008, L_12, L_13, /*hidden argument*/NULL);
		JoinMatchResponse_set_accessTokenString_m5976(__this, L_14, /*hidden argument*/NULL);
		Object_t * L_15 = ___obj;
		Object_t* L_16 = V_0;
		uint16_t L_17 = ResponseBase_ParseJSONUInt16_m5925(__this, _stringLiteral1009, L_15, L_16, /*hidden argument*/NULL);
		JoinMatchResponse_set_nodeId_m5978(__this, L_17, /*hidden argument*/NULL);
		Object_t * L_18 = ___obj;
		Object_t* L_19 = V_0;
		bool L_20 = ResponseBase_ParseJSONBool_m5927(__this, _stringLiteral1010, L_18, L_19, /*hidden argument*/NULL);
		JoinMatchResponse_set_usingRelay_m5980(__this, L_20, /*hidden argument*/NULL);
		goto IL_00a1;
	}

IL_008b:
	{
		Object_t * L_21 = ___obj;
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m3031(NULL /*static, unused*/, _stringLiteral1011, L_22, /*hidden argument*/NULL);
		FormatException_t1123 * L_24 = (FormatException_t1123 *)il2cpp_codegen_object_new (FormatException_t1123_il2cpp_TypeInfo_var);
		FormatException__ctor_m6404(L_24, L_23, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_24);
	}

IL_00a1:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
