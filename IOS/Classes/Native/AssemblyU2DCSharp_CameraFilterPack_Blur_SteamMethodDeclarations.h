﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Steam
struct CameraFilterPack_Blur_Steam_t59;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Steam::.ctor()
extern "C" void CameraFilterPack_Blur_Steam__ctor_m366 (CameraFilterPack_Blur_Steam_t59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Steam::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Steam_get_material_m367 (CameraFilterPack_Blur_Steam_t59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::Start()
extern "C" void CameraFilterPack_Blur_Steam_Start_m368 (CameraFilterPack_Blur_Steam_t59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Steam_OnRenderImage_m369 (CameraFilterPack_Blur_Steam_t59 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::OnValidate()
extern "C" void CameraFilterPack_Blur_Steam_OnValidate_m370 (CameraFilterPack_Blur_Steam_t59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::Update()
extern "C" void CameraFilterPack_Blur_Steam_Update_m371 (CameraFilterPack_Blur_Steam_t59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Steam::OnDisable()
extern "C" void CameraFilterPack_Blur_Steam_OnDisable_m372 (CameraFilterPack_Blur_Steam_t59 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
