﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.JsonArray
struct JsonArray_t985;
// System.String
struct String_t;

// System.Void SimpleJson.JsonArray::.ctor()
extern "C" void JsonArray__ctor_m6057 (JsonArray_t985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJson.JsonArray::ToString()
extern "C" String_t* JsonArray_ToString_m6058 (JsonArray_t985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
