﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct List_1_t715;
// System.Object
struct Object_t;
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t714;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct IEnumerable_1_t3134;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct IEnumerator_1_t3135;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct ICollection_1_t3136;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct ReadOnlyCollection_1_t2613;
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t2611;
// System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Predicate_1_t2614;
// System.Comparison`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Comparison_1_t2616;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.StencilMaterial/MatEntry>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_32.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m4898(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18949(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Int32)
#define List_1__ctor_m18950(__this, ___capacity, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::.cctor()
#define List_1__cctor_m18951(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18952(__this, method) (( Object_t* (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18953(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t715 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18954(__this, method) (( Object_t * (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18955(__this, ___item, method) (( int32_t (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18956(__this, ___item, method) (( bool (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18957(__this, ___item, method) (( int32_t (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18958(__this, ___index, ___item, method) (( void (*) (List_1_t715 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18959(__this, ___item, method) (( void (*) (List_1_t715 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18960(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18961(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18962(__this, method) (( Object_t * (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18963(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18964(__this, method) (( bool (*) (List_1_t715 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18965(__this, ___index, method) (( Object_t * (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18966(__this, ___index, ___value, method) (( void (*) (List_1_t715 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Add(T)
#define List_1_Add_m18967(__this, ___item, method) (( void (*) (List_1_t715 *, MatEntry_t714 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18968(__this, ___newCount, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18969(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18970(__this, ___enumerable, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18971(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::AsReadOnly()
#define List_1_AsReadOnly_m18972(__this, method) (( ReadOnlyCollection_1_t2613 * (*) (List_1_t715 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Clear()
#define List_1_Clear_m18973(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Contains(T)
#define List_1_Contains_m18974(__this, ___item, method) (( bool (*) (List_1_t715 *, MatEntry_t714 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18975(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t715 *, MatEntryU5BU5D_t2611*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Find(System.Predicate`1<T>)
#define List_1_Find_m18976(__this, ___match, method) (( MatEntry_t714 * (*) (List_1_t715 *, Predicate_1_t2614 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18977(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2614 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18978(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t715 *, int32_t, int32_t, Predicate_1_t2614 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::GetEnumerator()
#define List_1_GetEnumerator_m18979(__this, method) (( Enumerator_t2615  (*) (List_1_t715 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::IndexOf(T)
#define List_1_IndexOf_m18980(__this, ___item, method) (( int32_t (*) (List_1_t715 *, MatEntry_t714 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18981(__this, ___start, ___delta, method) (( void (*) (List_1_t715 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18982(__this, ___index, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Insert(System.Int32,T)
#define List_1_Insert_m18983(__this, ___index, ___item, method) (( void (*) (List_1_t715 *, int32_t, MatEntry_t714 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18984(__this, ___collection, method) (( void (*) (List_1_t715 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Remove(T)
#define List_1_Remove_m18985(__this, ___item, method) (( bool (*) (List_1_t715 *, MatEntry_t714 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18986(__this, ___match, method) (( int32_t (*) (List_1_t715 *, Predicate_1_t2614 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18987(__this, ___index, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Reverse()
#define List_1_Reverse_m18988(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Sort()
#define List_1_Sort_m18989(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18990(__this, ___comparison, method) (( void (*) (List_1_t715 *, Comparison_1_t2616 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::ToArray()
#define List_1_ToArray_m18991(__this, method) (( MatEntryU5BU5D_t2611* (*) (List_1_t715 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::TrimExcess()
#define List_1_TrimExcess_m18992(__this, method) (( void (*) (List_1_t715 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Capacity()
#define List_1_get_Capacity_m18993(__this, method) (( int32_t (*) (List_1_t715 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18994(__this, ___value, method) (( void (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Count()
#define List_1_get_Count_m18995(__this, method) (( int32_t (*) (List_1_t715 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::get_Item(System.Int32)
#define List_1_get_Item_m18996(__this, ___index, method) (( MatEntry_t714 * (*) (List_1_t715 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>::set_Item(System.Int32,T)
#define List_1_set_Item_m18997(__this, ___index, ___value, method) (( void (*) (List_1_t715 *, int32_t, MatEntry_t714 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
