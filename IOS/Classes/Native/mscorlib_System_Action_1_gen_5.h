﻿#pragma once
#include <stdint.h>
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t1037;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct  Action_1_t842  : public MulticastDelegate_t219
{
};
