﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MovingObjectManager
struct MovingObjectManager_t374;
// System.String
struct String_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void MovingObjectManager::.ctor()
extern "C" void MovingObjectManager__ctor_m2162 (MovingObjectManager_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::.cctor()
extern "C" void MovingObjectManager__cctor_m2163 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MovingObjectManager MovingObjectManager::get_Instance()
extern "C" MovingObjectManager_t374 * MovingObjectManager_get_Instance_m2164 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::Start()
extern "C" void MovingObjectManager_Start_m2165 (MovingObjectManager_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::Update()
extern "C" void MovingObjectManager_Update_m2166 (MovingObjectManager_t374 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::ShakeCabinet(System.String)
extern "C" void MovingObjectManager_ShakeCabinet_m2167 (MovingObjectManager_t374 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::MoveTo(System.String,UnityEngine.Vector3,System.Single,System.Single,iTween/EaseType,iTween/LoopType)
extern "C" void MovingObjectManager_MoveTo_m2168 (MovingObjectManager_t374 * __this, String_t* ___name, Vector3_t215  ___val, float ___time, float ___delay, int32_t ___iE, int32_t ___iL, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::MoveWheels(System.Boolean)
extern "C" void MovingObjectManager_MoveWheels_m2169 (MovingObjectManager_t374 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::MoveDoors(System.String,System.String,System.Single,System.Single,System.Single)
extern "C" void MovingObjectManager_MoveDoors_m2170 (MovingObjectManager_t374 * __this, String_t* ___name, String_t* ___val, float ___rot, float ___time, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObjectManager::spawObject(System.String,UnityEngine.Vector3,System.Single)
extern "C" void MovingObjectManager_spawObject_m2171 (MovingObjectManager_t374 * __this, String_t* ___name, Vector3_t215  ___val, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
