﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.Texture2D
struct Texture2D_t9;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_AAA_WaterDrop
struct  CameraFilterPack_AAA_WaterDrop_t10  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_AAA_WaterDrop::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_AAA_WaterDrop::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_AAA_WaterDrop::Distortion
	float ___Distortion_4;
	// System.Single CameraFilterPack_AAA_WaterDrop::SizeX
	float ___SizeX_5;
	// System.Single CameraFilterPack_AAA_WaterDrop::SizeY
	float ___SizeY_6;
	// System.Single CameraFilterPack_AAA_WaterDrop::Speed
	float ___Speed_7;
	// UnityEngine.Material CameraFilterPack_AAA_WaterDrop::SCMaterial
	Material_t2 * ___SCMaterial_8;
	// UnityEngine.Texture2D CameraFilterPack_AAA_WaterDrop::Texture2
	Texture2D_t9 * ___Texture2_9;
};
struct CameraFilterPack_AAA_WaterDrop_t10_StaticFields{
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeDistortion
	float ___ChangeDistortion_10;
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeSizeX
	float ___ChangeSizeX_11;
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeSizeY
	float ___ChangeSizeY_12;
	// System.Single CameraFilterPack_AAA_WaterDrop::ChangeSpeed
	float ___ChangeSpeed_13;
};
