﻿#pragma once
#include <stdint.h>
// iTween/LoopType[]
struct LoopTypeU5BU5D_t452;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<iTween/LoopType>
struct  List_1_t581  : public Object_t
{
	// T[] System.Collections.Generic.List`1<iTween/LoopType>::_items
	LoopTypeU5BU5D_t452* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<iTween/LoopType>::_version
	int32_t ____version_3;
};
struct List_1_t581_StaticFields{
	// T[] System.Collections.Generic.List`1<iTween/LoopType>::EmptyArray
	LoopTypeU5BU5D_t452* ___EmptyArray_4;
};
