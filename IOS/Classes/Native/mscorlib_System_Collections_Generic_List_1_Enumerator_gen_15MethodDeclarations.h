﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>
struct Enumerator_t2440;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Space>
struct List_1_t579;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m16264_gshared (Enumerator_t2440 * __this, List_1_t579 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m16264(__this, ___l, method) (( void (*) (Enumerator_t2440 *, List_1_t579 *, const MethodInfo*))Enumerator__ctor_m16264_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16265_gshared (Enumerator_t2440 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16265(__this, method) (( Object_t * (*) (Enumerator_t2440 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16265_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::Dispose()
extern "C" void Enumerator_Dispose_m16266_gshared (Enumerator_t2440 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16266(__this, method) (( void (*) (Enumerator_t2440 *, const MethodInfo*))Enumerator_Dispose_m16266_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::VerifyState()
extern "C" void Enumerator_VerifyState_m16267_gshared (Enumerator_t2440 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16267(__this, method) (( void (*) (Enumerator_t2440 *, const MethodInfo*))Enumerator_VerifyState_m16267_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16268_gshared (Enumerator_t2440 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16268(__this, method) (( bool (*) (Enumerator_t2440 *, const MethodInfo*))Enumerator_MoveNext_m16268_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::get_Current()
extern "C" int32_t Enumerator_get_Current_m16269_gshared (Enumerator_t2440 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16269(__this, method) (( int32_t (*) (Enumerator_t2440 *, const MethodInfo*))Enumerator_get_Current_m16269_gshared)(__this, method)
