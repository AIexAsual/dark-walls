﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.BaseVertexEffect
struct BaseVertexEffect_t756;
// UnityEngine.UI.Graphic
struct Graphic_t654;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t687;

// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern "C" void BaseVertexEffect__ctor_m4545 (BaseVertexEffect_t756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern "C" Graphic_t654 * BaseVertexEffect_get_graphic_m4546 (BaseVertexEffect_t756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern "C" void BaseVertexEffect_OnEnable_m4547 (BaseVertexEffect_t756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern "C" void BaseVertexEffect_OnDisable_m4548 (BaseVertexEffect_t756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
