﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>
struct Enumerator_t537;
// System.Object
struct Object_t;
// RagdollHelper/BodyPart
struct BodyPart_t400;
// System.Collections.Generic.List`1<RagdollHelper/BodyPart>
struct List_1_t401;

// System.Void System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m14526(__this, ___l, method) (( void (*) (Enumerator_t537 *, List_1_t401 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14527(__this, method) (( Object_t * (*) (Enumerator_t537 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::Dispose()
#define Enumerator_Dispose_m14528(__this, method) (( void (*) (Enumerator_t537 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::VerifyState()
#define Enumerator_VerifyState_m14529(__this, method) (( void (*) (Enumerator_t537 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::MoveNext()
#define Enumerator_MoveNext_m3214(__this, method) (( bool (*) (Enumerator_t537 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<RagdollHelper/BodyPart>::get_Current()
#define Enumerator_get_Current_m3213(__this, method) (( BodyPart_t400 * (*) (Enumerator_t537 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
