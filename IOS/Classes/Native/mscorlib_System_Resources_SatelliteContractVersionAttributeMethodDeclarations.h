﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t1814;
// System.String
struct String_t;

// System.Void System.Resources.SatelliteContractVersionAttribute::.ctor(System.String)
extern "C" void SatelliteContractVersionAttribute__ctor_m11246 (SatelliteContractVersionAttribute_t1814 * __this, String_t* ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
