﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
struct KeyValuePair_2_t2877;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m22402_gshared (KeyValuePair_2_t2877 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m22402(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2877 *, Object_t *, bool, const MethodInfo*))KeyValuePair_2__ctor_m22402_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m22403_gshared (KeyValuePair_2_t2877 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m22403(__this, method) (( Object_t * (*) (KeyValuePair_2_t2877 *, const MethodInfo*))KeyValuePair_2_get_Key_m22403_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m22404_gshared (KeyValuePair_2_t2877 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m22404(__this, ___value, method) (( void (*) (KeyValuePair_2_t2877 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m22404_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C" bool KeyValuePair_2_get_Value_m22405_gshared (KeyValuePair_2_t2877 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m22405(__this, method) (( bool (*) (KeyValuePair_2_t2877 *, const MethodInfo*))KeyValuePair_2_get_Value_m22405_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m22406_gshared (KeyValuePair_2_t2877 * __this, bool ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m22406(__this, ___value, method) (( void (*) (KeyValuePair_2_t2877 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m22406_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m22407_gshared (KeyValuePair_2_t2877 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m22407(__this, method) (( String_t* (*) (KeyValuePair_2_t2877 *, const MethodInfo*))KeyValuePair_2_ToString_m22407_gshared)(__this, method)
