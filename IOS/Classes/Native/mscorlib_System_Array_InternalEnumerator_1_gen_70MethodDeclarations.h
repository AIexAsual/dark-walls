﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>
struct InternalEnumerator_1_t2827;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21888_gshared (InternalEnumerator_1_t2827 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21888(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2827 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21888_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21889_gshared (InternalEnumerator_1_t2827 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21889(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2827 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21889_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21890_gshared (InternalEnumerator_1_t2827 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21890(__this, method) (( void (*) (InternalEnumerator_1_t2827 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21890_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21891_gshared (InternalEnumerator_1_t2827 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21891(__this, method) (( bool (*) (InternalEnumerator_1_t2827 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21891_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m21892_gshared (InternalEnumerator_1_t2827 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21892(__this, method) (( int32_t (*) (InternalEnumerator_1_t2827 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21892_gshared)(__this, method)
