﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>
struct Collection_1_t2424;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t3031;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t566;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor()
extern "C" void Collection_1__ctor_m16016_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16016(__this, method) (( void (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1__ctor_m16016_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16017_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16017(__this, method) (( bool (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16017_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16018_gshared (Collection_1_t2424 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16018(__this, ___array, ___index, method) (( void (*) (Collection_1_t2424 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16018_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16019_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16019(__this, method) (( Object_t * (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16019_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16020_gshared (Collection_1_t2424 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16020(__this, ___value, method) (( int32_t (*) (Collection_1_t2424 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16020_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16021_gshared (Collection_1_t2424 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16021(__this, ___value, method) (( bool (*) (Collection_1_t2424 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16021_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16022_gshared (Collection_1_t2424 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16022(__this, ___value, method) (( int32_t (*) (Collection_1_t2424 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16022_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16023_gshared (Collection_1_t2424 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16023(__this, ___index, ___value, method) (( void (*) (Collection_1_t2424 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16023_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16024_gshared (Collection_1_t2424 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16024(__this, ___value, method) (( void (*) (Collection_1_t2424 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16024_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16025_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16025(__this, method) (( bool (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16025_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16026_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16026(__this, method) (( Object_t * (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16026_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16027_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16027(__this, method) (( bool (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16027_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16028_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16028(__this, method) (( bool (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16028_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16029_gshared (Collection_1_t2424 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16029(__this, ___index, method) (( Object_t * (*) (Collection_1_t2424 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16029_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16030_gshared (Collection_1_t2424 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16030(__this, ___index, ___value, method) (( void (*) (Collection_1_t2424 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16030_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Add(T)
extern "C" void Collection_1_Add_m16031_gshared (Collection_1_t2424 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define Collection_1_Add_m16031(__this, ___item, method) (( void (*) (Collection_1_t2424 *, Vector3_t215 , const MethodInfo*))Collection_1_Add_m16031_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Clear()
extern "C" void Collection_1_Clear_m16032_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16032(__this, method) (( void (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_Clear_m16032_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ClearItems()
extern "C" void Collection_1_ClearItems_m16033_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16033(__this, method) (( void (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_ClearItems_m16033_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool Collection_1_Contains_m16034_gshared (Collection_1_t2424 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define Collection_1_Contains_m16034(__this, ___item, method) (( bool (*) (Collection_1_t2424 *, Vector3_t215 , const MethodInfo*))Collection_1_Contains_m16034_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16035_gshared (Collection_1_t2424 * __this, Vector3U5BU5D_t317* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16035(__this, ___array, ___index, method) (( void (*) (Collection_1_t2424 *, Vector3U5BU5D_t317*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16035_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16036_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16036(__this, method) (( Object_t* (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_GetEnumerator_m16036_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16037_gshared (Collection_1_t2424 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16037(__this, ___item, method) (( int32_t (*) (Collection_1_t2424 *, Vector3_t215 , const MethodInfo*))Collection_1_IndexOf_m16037_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16038_gshared (Collection_1_t2424 * __this, int32_t ___index, Vector3_t215  ___item, const MethodInfo* method);
#define Collection_1_Insert_m16038(__this, ___index, ___item, method) (( void (*) (Collection_1_t2424 *, int32_t, Vector3_t215 , const MethodInfo*))Collection_1_Insert_m16038_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16039_gshared (Collection_1_t2424 * __this, int32_t ___index, Vector3_t215  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16039(__this, ___index, ___item, method) (( void (*) (Collection_1_t2424 *, int32_t, Vector3_t215 , const MethodInfo*))Collection_1_InsertItem_m16039_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool Collection_1_Remove_m16040_gshared (Collection_1_t2424 * __this, Vector3_t215  ___item, const MethodInfo* method);
#define Collection_1_Remove_m16040(__this, ___item, method) (( bool (*) (Collection_1_t2424 *, Vector3_t215 , const MethodInfo*))Collection_1_Remove_m16040_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16041_gshared (Collection_1_t2424 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16041(__this, ___index, method) (( void (*) (Collection_1_t2424 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16041_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16042_gshared (Collection_1_t2424 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16042(__this, ___index, method) (( void (*) (Collection_1_t2424 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16042_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16043_gshared (Collection_1_t2424 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16043(__this, method) (( int32_t (*) (Collection_1_t2424 *, const MethodInfo*))Collection_1_get_Count_m16043_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t215  Collection_1_get_Item_m16044_gshared (Collection_1_t2424 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16044(__this, ___index, method) (( Vector3_t215  (*) (Collection_1_t2424 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16044_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16045_gshared (Collection_1_t2424 * __this, int32_t ___index, Vector3_t215  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16045(__this, ___index, ___value, method) (( void (*) (Collection_1_t2424 *, int32_t, Vector3_t215 , const MethodInfo*))Collection_1_set_Item_m16045_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16046_gshared (Collection_1_t2424 * __this, int32_t ___index, Vector3_t215  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16046(__this, ___index, ___item, method) (( void (*) (Collection_1_t2424 *, int32_t, Vector3_t215 , const MethodInfo*))Collection_1_SetItem_m16046_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16047_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16047(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16047_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ConvertItem(System.Object)
extern "C" Vector3_t215  Collection_1_ConvertItem_m16048_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16048(__this /* static, unused */, ___item, method) (( Vector3_t215  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16048_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16049_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16049(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16049_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16050_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16050(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16050_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16051_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16051(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16051_gshared)(__this /* static, unused */, ___list, method)
