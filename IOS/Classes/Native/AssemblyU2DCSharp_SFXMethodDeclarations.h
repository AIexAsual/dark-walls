﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SFX
struct SFX_t385;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void SFX::.ctor()
extern "C" void SFX__ctor_m2324 (SFX_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFX::Start()
extern "C" void SFX_Start_m2325 (SFX_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFX::Update()
extern "C" void SFX_Update_m2326 (SFX_t385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFX::TriggerSound(System.Boolean,System.Single)
extern "C" void SFX_TriggerSound_m2327 (SFX_t385 * __this, bool ___val, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SFX::WaitAndPlaySounds(System.Single)
extern "C" Object_t * SFX_WaitAndPlaySounds_m2328 (SFX_t385 * __this, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SFX::SoundVol(System.Single)
extern "C" void SFX_SoundVol_m2329 (SFX_t385 * __this, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
