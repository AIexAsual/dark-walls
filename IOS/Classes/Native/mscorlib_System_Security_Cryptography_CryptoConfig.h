﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Collections.Hashtable
struct Hashtable_t348;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.CryptoConfig
struct  CryptoConfig_t1337  : public Object_t
{
};
struct CryptoConfig_t1337_StaticFields{
	// System.Object System.Security.Cryptography.CryptoConfig::lockObject
	Object_t * ___lockObject_0;
	// System.Collections.Hashtable System.Security.Cryptography.CryptoConfig::algorithms
	Hashtable_t348 * ___algorithms_1;
	// System.Collections.Hashtable System.Security.Cryptography.CryptoConfig::oid
	Hashtable_t348 * ___oid_2;
};
