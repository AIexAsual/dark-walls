﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController/<WaitAndMoveTo>c__Iterator11
struct U3CWaitAndMoveToU3Ec__Iterator11_t391;
// System.Object
struct Object_t;

// System.Void PlayerController/<WaitAndMoveTo>c__Iterator11::.ctor()
extern "C" void U3CWaitAndMoveToU3Ec__Iterator11__ctor_m2236 (U3CWaitAndMoveToU3Ec__Iterator11_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndMoveTo>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndMoveToU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2237 (U3CWaitAndMoveToU3Ec__Iterator11_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndMoveTo>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndMoveToU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m2238 (U3CWaitAndMoveToU3Ec__Iterator11_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController/<WaitAndMoveTo>c__Iterator11::MoveNext()
extern "C" bool U3CWaitAndMoveToU3Ec__Iterator11_MoveNext_m2239 (U3CWaitAndMoveToU3Ec__Iterator11_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndMoveTo>c__Iterator11::Dispose()
extern "C" void U3CWaitAndMoveToU3Ec__Iterator11_Dispose_m2240 (U3CWaitAndMoveToU3Ec__Iterator11_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndMoveTo>c__Iterator11::Reset()
extern "C" void U3CWaitAndMoveToU3Ec__Iterator11_Reset_m2241 (U3CWaitAndMoveToU3Ec__Iterator11_t391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
