﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1058;
// System.Collections.Generic.ICollection`1<UnityEngine.Event>
struct ICollection_1_t3244;
// System.Collections.Generic.ICollection`1<UnityEngine.TextEditor/TextEditOp>
struct ICollection_1_t3245;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t481;
struct Event_t481_marshaled;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyCollection_t2840;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct ValueCollection_t2841;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Event>
struct IEqualityComparer_1_t2821;
// System.Collections.Generic.IDictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct IDictionary_2_t3246;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t3247;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>>
struct IEnumerator_1_t3248;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_28.h"
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__24.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_28MethodDeclarations.h"
#define Dictionary_2__ctor_m21777(__this, method) (( void (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2__ctor_m21778_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m21779(__this, ___comparer, method) (( void (*) (Dictionary_2_t1058 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21780_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m21781(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1058 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21782_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Int32)
#define Dictionary_2__ctor_m21783(__this, ___capacity, method) (( void (*) (Dictionary_2_t1058 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21784_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m21785(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1058 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21786_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m21787(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1058 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m21788_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21789(__this, method) (( Object_t* (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21790_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21791(__this, method) (( Object_t* (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21792_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21793(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1058 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21794_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21795(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1058 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m21796_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m21797(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1058 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m21798_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m21799(__this, ___key, method) (( bool (*) (Dictionary_2_t1058 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m21800_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m21801(__this, ___key, method) (( void (*) (Dictionary_2_t1058 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m21802_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21803(__this, method) (( bool (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21804_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21805(__this, method) (( Object_t * (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21806_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21807(__this, method) (( bool (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21808_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21809(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1058 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21810_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21811(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1058 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21812_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21813(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1058 *, KeyValuePair_2U5BU5D_t3247*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21814_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21815(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1058 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21816_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21817(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1058 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m21818_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21819(__this, method) (( Object_t * (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21820_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21821(__this, method) (( Object_t* (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21822_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21823(__this, method) (( Object_t * (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21824_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Count()
#define Dictionary_2_get_Count_m21825(__this, method) (( int32_t (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_get_Count_m21826_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Item(TKey)
#define Dictionary_2_get_Item_m21827(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1058 *, Event_t481 *, const MethodInfo*))Dictionary_2_get_Item_m21828_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m21829(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1058 *, Event_t481 *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m21830_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m21831(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1058 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m21832_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m21833(__this, ___size, method) (( void (*) (Dictionary_2_t1058 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m21834_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m21835(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1058 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m21836_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m21837(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2839  (*) (Object_t * /* static, unused */, Event_t481 *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m21838_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m21839(__this /* static, unused */, ___key, ___value, method) (( Event_t481 * (*) (Object_t * /* static, unused */, Event_t481 *, int32_t, const MethodInfo*))Dictionary_2_pick_key_m21840_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m21841(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Event_t481 *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m21842_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m21843(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1058 *, KeyValuePair_2U5BU5D_t3247*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m21844_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Resize()
#define Dictionary_2_Resize_m21845(__this, method) (( void (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_Resize_m21846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Add(TKey,TValue)
#define Dictionary_2_Add_m21847(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1058 *, Event_t481 *, int32_t, const MethodInfo*))Dictionary_2_Add_m21848_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Clear()
#define Dictionary_2_Clear_m21849(__this, method) (( void (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_Clear_m21850_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m21851(__this, ___key, method) (( bool (*) (Dictionary_2_t1058 *, Event_t481 *, const MethodInfo*))Dictionary_2_ContainsKey_m21852_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m21853(__this, ___value, method) (( bool (*) (Dictionary_2_t1058 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m21854_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m21855(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1058 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m21856_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m21857(__this, ___sender, method) (( void (*) (Dictionary_2_t1058 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m21858_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Remove(TKey)
#define Dictionary_2_Remove_m21859(__this, ___key, method) (( bool (*) (Dictionary_2_t1058 *, Event_t481 *, const MethodInfo*))Dictionary_2_Remove_m21860_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m21861(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1058 *, Event_t481 *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m21862_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Keys()
#define Dictionary_2_get_Keys_m21863(__this, method) (( KeyCollection_t2840 * (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_get_Keys_m21864_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Values()
#define Dictionary_2_get_Values_m21865(__this, method) (( ValueCollection_t2841 * (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_get_Values_m21866_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m21867(__this, ___key, method) (( Event_t481 * (*) (Dictionary_2_t1058 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m21868_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m21869(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1058 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m21870_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m21871(__this, ___pair, method) (( bool (*) (Dictionary_2_t1058 *, KeyValuePair_2_t2839 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m21872_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m21873(__this, method) (( Enumerator_t2842  (*) (Dictionary_2_t1058 *, const MethodInfo*))Dictionary_2_GetEnumerator_m21874_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m21875(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, Event_t481 *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m21876_gshared)(__this /* static, unused */, ___key, ___value, method)
