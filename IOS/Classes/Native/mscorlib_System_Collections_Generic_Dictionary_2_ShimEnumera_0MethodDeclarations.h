﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t2359;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2349;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15103_gshared (ShimEnumerator_t2359 * __this, Dictionary_2_t2349 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15103(__this, ___host, method) (( void (*) (ShimEnumerator_t2359 *, Dictionary_2_t2349 *, const MethodInfo*))ShimEnumerator__ctor_m15103_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15104_gshared (ShimEnumerator_t2359 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15104(__this, method) (( bool (*) (ShimEnumerator_t2359 *, const MethodInfo*))ShimEnumerator_MoveNext_m15104_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m15105_gshared (ShimEnumerator_t2359 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15105(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2359 *, const MethodInfo*))ShimEnumerator_get_Entry_m15105_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15106_gshared (ShimEnumerator_t2359 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15106(__this, method) (( Object_t * (*) (ShimEnumerator_t2359 *, const MethodInfo*))ShimEnumerator_get_Key_m15106_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15107_gshared (ShimEnumerator_t2359 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15107(__this, method) (( Object_t * (*) (ShimEnumerator_t2359 *, const MethodInfo*))ShimEnumerator_get_Value_m15107_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m15108_gshared (ShimEnumerator_t2359 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m15108(__this, method) (( Object_t * (*) (ShimEnumerator_t2359 *, const MethodInfo*))ShimEnumerator_get_Current_m15108_gshared)(__this, method)
