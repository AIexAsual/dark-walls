﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Flag
struct  CameraFilterPack_Distortion_Flag_t86  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Flag::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Flag::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Distortion_Flag::Distortion
	float ___Distortion_4;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Flag::ScreenResolution
	Vector4_t5  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_Distortion_Flag::SCMaterial
	Material_t2 * ___SCMaterial_6;
};
struct CameraFilterPack_Distortion_Flag_t86_StaticFields{
	// System.Single CameraFilterPack_Distortion_Flag::ChangeDistortion
	float ___ChangeDistortion_7;
};
