﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Vision_Psycho
struct  CameraFilterPack_Vision_Psycho_t208  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Vision_Psycho::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Vision_Psycho::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Vision_Psycho::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Vision_Psycho::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Vision_Psycho::HoleSize
	float ___HoleSize_6;
	// System.Single CameraFilterPack_Vision_Psycho::HoleSmooth
	float ___HoleSmooth_7;
	// System.Single CameraFilterPack_Vision_Psycho::Color1
	float ___Color1_8;
	// System.Single CameraFilterPack_Vision_Psycho::Color2
	float ___Color2_9;
};
struct CameraFilterPack_Vision_Psycho_t208_StaticFields{
	// System.Single CameraFilterPack_Vision_Psycho::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Vision_Psycho::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Vision_Psycho::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Vision_Psycho::ChangeValue4
	float ___ChangeValue4_13;
};
