﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Special_Bubble
struct  CameraFilterPack_Special_Bubble_t174  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Special_Bubble::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Special_Bubble::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Special_Bubble::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Special_Bubble::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Special_Bubble::X
	float ___X_6;
	// System.Single CameraFilterPack_Special_Bubble::Y
	float ___Y_7;
	// System.Single CameraFilterPack_Special_Bubble::Rate
	float ___Rate_8;
	// System.Single CameraFilterPack_Special_Bubble::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Special_Bubble_t174_StaticFields{
	// System.Single CameraFilterPack_Special_Bubble::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Special_Bubble::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Special_Bubble::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Special_Bubble::ChangeValue4
	float ___ChangeValue4_13;
};
