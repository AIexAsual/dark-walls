﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Spot
struct CameraFilterPack_FX_Spot_t140;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Spot::.ctor()
extern "C" void CameraFilterPack_FX_Spot__ctor_m907 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Spot::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Spot_get_material_m908 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::Start()
extern "C" void CameraFilterPack_FX_Spot_Start_m909 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Spot_OnRenderImage_m910 (CameraFilterPack_FX_Spot_t140 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::OnValidate()
extern "C" void CameraFilterPack_FX_Spot_OnValidate_m911 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::Update()
extern "C" void CameraFilterPack_FX_Spot_Update_m912 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Spot::OnDisable()
extern "C" void CameraFilterPack_FX_Spot_OnDisable_m913 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
