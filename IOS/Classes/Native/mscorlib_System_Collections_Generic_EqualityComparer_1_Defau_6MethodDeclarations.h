﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>
struct DefaultComparer_t2426;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::.ctor()
extern "C" void DefaultComparer__ctor_m16057_gshared (DefaultComparer_t2426 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16057(__this, method) (( void (*) (DefaultComparer_t2426 *, const MethodInfo*))DefaultComparer__ctor_m16057_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m16058_gshared (DefaultComparer_t2426 * __this, Vector3_t215  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m16058(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2426 *, Vector3_t215 , const MethodInfo*))DefaultComparer_GetHashCode_m16058_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Vector3>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m16059_gshared (DefaultComparer_t2426 * __this, Vector3_t215  ___x, Vector3_t215  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m16059(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2426 *, Vector3_t215 , Vector3_t215 , const MethodInfo*))DefaultComparer_Equals_m16059_gshared)(__this, ___x, ___y, method)
