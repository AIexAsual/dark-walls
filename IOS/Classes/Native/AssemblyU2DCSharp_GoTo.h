﻿#pragma once
#include <stdint.h>
// GoTo
struct GoTo_t315;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// UnityEngine.Light
struct Light_t318;
// UnityEngine.Collider
struct Collider_t319;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// GoTo
struct  GoTo_t315  : public MonoBehaviour_t4
{
	// UnityEngine.Vector3[] GoTo::pos
	Vector3U5BU5D_t317* ___pos_3;
	// UnityEngine.Vector3 GoTo::nextPosition
	Vector3_t215  ___nextPosition_4;
	// UnityEngine.Light GoTo::light
	Light_t318 * ___light_5;
	// UnityEngine.Collider GoTo::col
	Collider_t319 * ___col_6;
	// System.Int32 GoTo::count
	int32_t ___count_7;
};
struct GoTo_t315_StaticFields{
	// GoTo GoTo::_instance
	GoTo_t315 * ____instance_2;
};
