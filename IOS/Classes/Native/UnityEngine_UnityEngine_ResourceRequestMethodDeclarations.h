﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t900;
// UnityEngine.Object
struct Object_t473;
struct Object_t473_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m5492 (ResourceRequest_t900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t473 * ResourceRequest_get_asset_m5493 (ResourceRequest_t900 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
