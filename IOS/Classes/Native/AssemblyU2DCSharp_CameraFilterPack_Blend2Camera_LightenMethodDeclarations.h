﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Lighten
struct CameraFilterPack_Blend2Camera_Lighten_t30;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Lighten::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Lighten__ctor_m153 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Lighten::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Lighten_get_material_m154 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::Start()
extern "C" void CameraFilterPack_Blend2Camera_Lighten_Start_m155 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Lighten_OnRenderImage_m156 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Lighten_OnValidate_m157 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::Update()
extern "C" void CameraFilterPack_Blend2Camera_Lighten_Update_m158 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Lighten_OnEnable_m159 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Lighten::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Lighten_OnDisable_m160 (CameraFilterPack_Blend2Camera_Lighten_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
