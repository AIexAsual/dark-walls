﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt32>
struct InternalEnumerator_1_t2867;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22256_gshared (InternalEnumerator_1_t2867 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22256(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2867 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22256_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22257_gshared (InternalEnumerator_1_t2867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22257(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2867 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22257_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22258_gshared (InternalEnumerator_1_t2867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22258(__this, method) (( void (*) (InternalEnumerator_1_t2867 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22258_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22259_gshared (InternalEnumerator_1_t2867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22259(__this, method) (( bool (*) (InternalEnumerator_1_t2867 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22259_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern "C" uint32_t InternalEnumerator_1_get_Current_m22260_gshared (InternalEnumerator_1_t2867 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22260(__this, method) (( uint32_t (*) (InternalEnumerator_1_t2867 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22260_gshared)(__this, method)
