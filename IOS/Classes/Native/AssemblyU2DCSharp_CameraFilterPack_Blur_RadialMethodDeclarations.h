﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Radial
struct CameraFilterPack_Blur_Radial_t56;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Radial::.ctor()
extern "C" void CameraFilterPack_Blur_Radial__ctor_m345 (CameraFilterPack_Blur_Radial_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Radial::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Radial_get_material_m346 (CameraFilterPack_Blur_Radial_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::Start()
extern "C" void CameraFilterPack_Blur_Radial_Start_m347 (CameraFilterPack_Blur_Radial_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Radial_OnRenderImage_m348 (CameraFilterPack_Blur_Radial_t56 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::OnValidate()
extern "C" void CameraFilterPack_Blur_Radial_OnValidate_m349 (CameraFilterPack_Blur_Radial_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::Update()
extern "C" void CameraFilterPack_Blur_Radial_Update_m350 (CameraFilterPack_Blur_Radial_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial::OnDisable()
extern "C" void CameraFilterPack_Blur_Radial_OnDisable_m351 (CameraFilterPack_Blur_Radial_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
