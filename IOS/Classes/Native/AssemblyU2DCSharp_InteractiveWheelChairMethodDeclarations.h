﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveWheelChair
struct InteractiveWheelChair_t342;
// UnityEngine.Collision
struct Collision_t471;

// System.Void InteractiveWheelChair::.ctor()
extern "C" void InteractiveWheelChair__ctor_m1997 (InteractiveWheelChair_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveWheelChair::Start()
extern "C" void InteractiveWheelChair_Start_m1998 (InteractiveWheelChair_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveWheelChair::Update()
extern "C" void InteractiveWheelChair_Update_m1999 (InteractiveWheelChair_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveWheelChair::LateUpdate()
extern "C" void InteractiveWheelChair_LateUpdate_m2000 (InteractiveWheelChair_t342 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveWheelChair::OnCollisionEnter(UnityEngine.Collision)
extern "C" void InteractiveWheelChair_OnCollisionEnter_m2001 (InteractiveWheelChair_t342 * __this, Collision_t471 * ___collision, const MethodInfo* method) IL2CPP_METHOD_ATTR;
