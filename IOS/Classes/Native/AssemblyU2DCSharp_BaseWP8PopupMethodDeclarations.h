﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// BaseWP8Popup
struct BaseWP8Popup_t295;

// System.Void BaseWP8Popup::.ctor()
extern "C" void BaseWP8Popup__ctor_m1826 (BaseWP8Popup_t295 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
