﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Flag
struct CameraFilterPack_Distortion_Flag_t86;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Flag::.ctor()
extern "C" void CameraFilterPack_Distortion_Flag__ctor_m539 (CameraFilterPack_Distortion_Flag_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Flag::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Flag_get_material_m540 (CameraFilterPack_Distortion_Flag_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::Start()
extern "C" void CameraFilterPack_Distortion_Flag_Start_m541 (CameraFilterPack_Distortion_Flag_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Flag_OnRenderImage_m542 (CameraFilterPack_Distortion_Flag_t86 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::OnValidate()
extern "C" void CameraFilterPack_Distortion_Flag_OnValidate_m543 (CameraFilterPack_Distortion_Flag_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::Update()
extern "C" void CameraFilterPack_Distortion_Flag_Update_m544 (CameraFilterPack_Distortion_Flag_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Flag::OnDisable()
extern "C" void CameraFilterPack_Distortion_Flag_OnDisable_m545 (CameraFilterPack_Distortion_Flag_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
