﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>
struct Enumerator_t2487;
// System.Object
struct Object_t;
// System.String
struct String_t;
// iTweenPath
struct iTweenPath_t459;
// System.Collections.Generic.Dictionary`2<System.String,iTweenPath>
struct Dictionary_2_t458;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_10.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m17097(__this, ___dictionary, method) (( void (*) (Enumerator_t2487 *, Dictionary_2_t458 *, const MethodInfo*))Enumerator__ctor_m14807_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17098(__this, method) (( Object_t * (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17099(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14811_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17100(__this, method) (( Object_t * (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17101(__this, method) (( Object_t * (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14815_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::MoveNext()
#define Enumerator_MoveNext_m17102(__this, method) (( bool (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_MoveNext_m14817_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::get_Current()
#define Enumerator_get_Current_m17103(__this, method) (( KeyValuePair_2_t2484  (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_get_Current_m14819_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17104(__this, method) (( String_t* (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_get_CurrentKey_m14821_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17105(__this, method) (( iTweenPath_t459 * (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_get_CurrentValue_m14823_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::VerifyState()
#define Enumerator_VerifyState_m17106(__this, method) (( void (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_VerifyState_m14825_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17107(__this, method) (( void (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_VerifyCurrent_m14827_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,iTweenPath>::Dispose()
#define Enumerator_Dispose_m17108(__this, method) (( void (*) (Enumerator_t2487 *, const MethodInfo*))Enumerator_Dispose_m14829_gshared)(__this, method)
