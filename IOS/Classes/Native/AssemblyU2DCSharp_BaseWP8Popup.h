﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// BaseWP8Popup
struct  BaseWP8Popup_t295  : public MonoBehaviour_t4
{
	// System.String BaseWP8Popup::title
	String_t* ___title_2;
	// System.String BaseWP8Popup::message
	String_t* ___message_3;
};
