﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.SingleCallIdentity
struct SingleCallIdentity_t1921;
// System.String
struct String_t;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1857;
// System.Type
struct Type_t;

// System.Void System.Runtime.Remoting.SingleCallIdentity::.ctor(System.String,System.Runtime.Remoting.Contexts.Context,System.Type)
extern "C" void SingleCallIdentity__ctor_m11645 (SingleCallIdentity_t1921 * __this, String_t* ___objectUri, Context_t1857 * ___context, Type_t * ___objectType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
