﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNAndroidDialog
struct MNAndroidDialog_t285;
// System.String
struct String_t;

// System.Void MNAndroidDialog::.ctor()
extern "C" void MNAndroidDialog__ctor_m1764 (MNAndroidDialog_t285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidDialog MNAndroidDialog::Create(System.String,System.String)
extern "C" MNAndroidDialog_t285 * MNAndroidDialog_Create_m1765 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidDialog MNAndroidDialog::Create(System.String,System.String,System.String,System.String)
extern "C" MNAndroidDialog_t285 * MNAndroidDialog_Create_m1766 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___yes, String_t* ___no, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidDialog::init()
extern "C" void MNAndroidDialog_init_m1767 (MNAndroidDialog_t285 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidDialog::onPopUpCallBack(System.String)
extern "C" void MNAndroidDialog_onPopUpCallBack_m1768 (MNAndroidDialog_t285 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
