﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPS_U3CFPSXU3Ec.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPS_U3CFPSXU3EcMethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// CameraFilterPack_EXTRA_SHOWFPS
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPS.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// CameraFilterPack_EXTRA_SHOWFPS
#include "AssemblyU2DCSharp_CameraFilterPack_EXTRA_SHOWFPSMethodDeclarations.h"
// UnityEngine.WaitForSeconds
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::.ctor()
extern "C" void U3CFPSXU3Ec__Iterator0__ctor_m705 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2749(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CFPSXU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m706 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CFPSXU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m707 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::MoveNext()
extern TypeInfo* CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var;
extern TypeInfo* WaitForSeconds_t475_il2cpp_TypeInfo_var;
extern "C" bool U3CFPSXU3Ec__Iterator0_MoveNext_m708 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(95);
		WaitForSeconds_t475_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(96);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_009b;
		}
	}
	{
		goto IL_00a7;
	}

IL_0021:
	{
		CameraFilterPack_EXTRA_SHOWFPS_t110 * L_2 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_2);
		float L_3 = (L_2->___accum_15);
		CameraFilterPack_EXTRA_SHOWFPS_t110 * L_4 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_4);
		int32_t L_5 = (L_4->___frames_16);
		__this->___U3CfpsU3E__0_0 = ((float)((float)L_3/(float)(((float)L_5))));
		CameraFilterPack_EXTRA_SHOWFPS_t110 * L_6 = (__this->___U3CU3Ef__this_3);
		float L_7 = (__this->___U3CfpsU3E__0_0);
		NullCheck(L_6);
		L_6->___FPS_7 = (((int32_t)L_7));
		float L_8 = (__this->___U3CfpsU3E__0_0);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_12 = (((int32_t)L_8));
		CameraFilterPack_EXTRA_SHOWFPS_t110 * L_9 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_9);
		L_9->___accum_15 = (0.0f);
		CameraFilterPack_EXTRA_SHOWFPS_t110 * L_10 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_10);
		L_10->___frames_16 = 0;
		CameraFilterPack_EXTRA_SHOWFPS_t110 * L_11 = (__this->___U3CU3Ef__this_3);
		NullCheck(L_11);
		float L_12 = (L_11->___frequency_17);
		WaitForSeconds_t475 * L_13 = (WaitForSeconds_t475 *)il2cpp_codegen_object_new (WaitForSeconds_t475_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2750(L_13, L_12, /*hidden argument*/NULL);
		__this->___U24current_2 = L_13;
		__this->___U24PC_1 = 1;
		goto IL_00a9;
	}

IL_009b:
	{
		goto IL_0021;
	}
	// Dead block : IL_00a0: ldarg.0

IL_00a7:
	{
		return 0;
	}

IL_00a9:
	{
		return 1;
	}
}
// System.Void CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::Dispose()
extern "C" void U3CFPSXU3Ec__Iterator0_Dispose_m709 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void CameraFilterPack_EXTRA_SHOWFPS/<FPSX>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t476_il2cpp_TypeInfo_var;
extern "C" void U3CFPSXU3Ec__Iterator0_Reset_m710 (U3CFPSXU3Ec__Iterator0_t111 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(97);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t476 * L_0 = (NotSupportedException_t476 *)il2cpp_codegen_object_new (NotSupportedException_t476_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2751(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.Coroutine
#include "UnityEngine_UnityEngine_Coroutine.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTexture.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_ShaderMethodDeclarations.h"
// UnityEngine.SystemInfo
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.RenderTexture
#include "UnityEngine_UnityEngine_RenderTextureMethodDeclarations.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// UnityEngine.Graphics
#include "UnityEngine_UnityEngine_GraphicsMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"


// System.Void CameraFilterPack_EXTRA_SHOWFPS::.ctor()
extern "C" void CameraFilterPack_EXTRA_SHOWFPS__ctor_m711 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (12.0f);
		__this->___FPS_7 = 1;
		__this->___Value3_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		__this->___frequency_17 = (0.5f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_EXTRA_SHOWFPS::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_EXTRA_SHOWFPS_get_material_m712 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_EXTRA_SHOWFPS::Start()
extern TypeInfo* CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral170;
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_Start_m713 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(95);
		_stringLiteral170 = il2cpp_codegen_string_literal_from_index(170);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___FPS_7 = 0;
		Object_t * L_0 = CameraFilterPack_EXTRA_SHOWFPS_FPSX_m716(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2752(__this, L_0, /*hidden argument*/NULL);
		float L_1 = (__this->___Size_6);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_11 = L_1;
		int32_t L_2 = (__this->___FPS_7);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_12 = L_2;
		float L_3 = (__this->___Value3_8);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_13 = L_3;
		float L_4 = (__this->___Value4_9);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_14 = L_4;
		Shader_t1 * L_5 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral170, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_5;
		bool L_6 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0062;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0062:
	{
		return;
	}
}
// System.Void CameraFilterPack_EXTRA_SHOWFPS::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_OnRenderImage_m714 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ec;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_EXTRA_SHOWFPS_get_material_m712(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_EXTRA_SHOWFPS_get_material_m712(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_EXTRA_SHOWFPS_get_material_m712(__this, /*hidden argument*/NULL);
		int32_t L_10 = (__this->___FPS_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, (((float)L_10)), /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_EXTRA_SHOWFPS_get_material_m712(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Value3_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_EXTRA_SHOWFPS_get_material_m712(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_EXTRA_SHOWFPS_get_material_m712(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_EXTRA_SHOWFPS_get_material_m712(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f3;
	}

IL_00ec:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Void CameraFilterPack_EXTRA_SHOWFPS::OnValidate()
extern TypeInfo* CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_OnValidate_m715 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(95);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_11 = L_0;
		int32_t L_1 = (__this->___FPS_7);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_12 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_13 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_14 = L_3;
		return;
	}
}
// System.Collections.IEnumerator CameraFilterPack_EXTRA_SHOWFPS::FPSX()
extern TypeInfo* U3CFPSXU3Ec__Iterator0_t111_il2cpp_TypeInfo_var;
extern "C" Object_t * CameraFilterPack_EXTRA_SHOWFPS_FPSX_m716 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CFPSXU3Ec__Iterator0_t111_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(98);
		s_Il2CppMethodIntialized = true;
	}
	U3CFPSXU3Ec__Iterator0_t111 * V_0 = {0};
	{
		U3CFPSXU3Ec__Iterator0_t111 * L_0 = (U3CFPSXU3Ec__Iterator0_t111 *)il2cpp_codegen_object_new (U3CFPSXU3Ec__Iterator0_t111_il2cpp_TypeInfo_var);
		U3CFPSXU3Ec__Iterator0__ctor_m705(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFPSXU3Ec__Iterator0_t111 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_3 = __this;
		U3CFPSXU3Ec__Iterator0_t111 * L_2 = V_0;
		return L_2;
	}
}
// System.Void CameraFilterPack_EXTRA_SHOWFPS::Update()
extern TypeInfo* CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_Update_m717 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(95);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___accum_15);
		float L_1 = Time_get_timeScale_m2753(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___accum_15 = ((float)((float)L_0+(float)((float)((float)L_1/(float)L_2))));
		int32_t L_3 = (__this->___frames_16);
		__this->___frames_16 = ((int32_t)((int32_t)L_3+(int32_t)1));
		bool L_4 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0061;
		}
	}
	{
		float L_5 = ((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_11;
		__this->___Size_6 = L_5;
		int32_t L_6 = ((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_12;
		__this->___FPS_7 = L_6;
		float L_7 = ((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_13;
		__this->___Value3_8 = L_7;
		float L_8 = ((CameraFilterPack_EXTRA_SHOWFPS_t110_StaticFields*)CameraFilterPack_EXTRA_SHOWFPS_t110_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_14;
		__this->___Value4_9 = L_8;
		goto IL_006c;
	}

IL_0061:
	{
		__this->___FPS_7 = ((int32_t)9999);
	}

IL_006c:
	{
		return;
	}
}
// System.Void CameraFilterPack_EXTRA_SHOWFPS::OnDisable()
extern "C" void CameraFilterPack_EXTRA_SHOWFPS_OnDisable_m718 (CameraFilterPack_EXTRA_SHOWFPS_t110 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Edge_BlackLine
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_BlackLine.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Edge_BlackLine
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_BlackLineMethodDeclarations.h"

// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"


// System.Void CameraFilterPack_Edge_BlackLine::.ctor()
extern "C" void CameraFilterPack_Edge_BlackLine__ctor_m719 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Edge_BlackLine::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Edge_BlackLine_get_material_m720 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Edge_BlackLine::Start()
extern Il2CppCodeGenString* _stringLiteral171;
extern "C" void CameraFilterPack_Edge_BlackLine_Start_m721 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral171 = il2cpp_codegen_string_literal_from_index(171);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral171, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_BlackLine::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Edge_BlackLine_OnRenderImage_m722 (CameraFilterPack_Edge_BlackLine_t112 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Edge_BlackLine_get_material_m720(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Edge_BlackLine_get_material_m720(__this, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_10 = {0};
		Vector2__ctor_m2714(&L_10, (((float)L_8)), (((float)L_9)), /*hidden argument*/NULL);
		Vector4_t5  L_11 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_11, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Material_t2 * L_14 = CameraFilterPack_Edge_BlackLine_get_material_m720(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		goto IL_0093;
	}

IL_008c:
	{
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_BlackLine::Update()
extern "C" void CameraFilterPack_Edge_BlackLine_Update_m723 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_BlackLine::OnDisable()
extern "C" void CameraFilterPack_Edge_BlackLine_OnDisable_m724 (CameraFilterPack_Edge_BlackLine_t112 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Edge_Edge_filter
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Edge_filter.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Edge_Edge_filter
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Edge_filterMethodDeclarations.h"



// System.Void CameraFilterPack_Edge_Edge_filter::.ctor()
extern "C" void CameraFilterPack_Edge_Edge_filter__ctor_m725 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___GreenAmplifier_7 = (2.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Edge_Edge_filter::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Edge_Edge_filter_get_material_m726 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Edge_Edge_filter::Start()
extern TypeInfo* CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral172;
extern "C" void CameraFilterPack_Edge_Edge_filter_Start_m727 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(99);
		_stringLiteral172 = il2cpp_codegen_string_literal_from_index(172);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___RedAmplifier_6);
		((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeRedAmplifier_9 = L_0;
		float L_1 = (__this->___GreenAmplifier_7);
		((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeGreenAmplifier_10 = L_1;
		float L_2 = (__this->___BlueAmplifier_8);
		((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeBlueAmplifier_11 = L_2;
		Shader_t1 * L_3 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral172, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_3;
		bool L_4 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Edge_filter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral173;
extern Il2CppCodeGenString* _stringLiteral174;
extern Il2CppCodeGenString* _stringLiteral175;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Edge_Edge_filter_OnRenderImage_m728 (CameraFilterPack_Edge_Edge_filter_t113 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral173 = il2cpp_codegen_string_literal_from_index(173);
		_stringLiteral174 = il2cpp_codegen_string_literal_from_index(174);
		_stringLiteral175 = il2cpp_codegen_string_literal_from_index(175);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ce;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Edge_Edge_filter_get_material_m726(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Edge_Edge_filter_get_material_m726(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___RedAmplifier_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral173, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Edge_Edge_filter_get_material_m726(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___GreenAmplifier_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral174, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Edge_Edge_filter_get_material_m726(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___BlueAmplifier_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral175, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Edge_Edge_filter_get_material_m726(__this, /*hidden argument*/NULL);
		int32_t L_14 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_16 = {0};
		Vector2__ctor_m2714(&L_16, (((float)L_14)), (((float)L_15)), /*hidden argument*/NULL);
		Vector4_t5  L_17 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetVector_m2729(L_13, _stringLiteral11, L_17, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Material_t2 * L_20 = CameraFilterPack_Edge_Edge_filter_get_material_m726(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_00ce:
	{
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Edge_filter::OnValidate()
extern TypeInfo* CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Edge_Edge_filter_OnValidate_m729 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(99);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___RedAmplifier_6);
		((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeRedAmplifier_9 = L_0;
		float L_1 = (__this->___GreenAmplifier_7);
		((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeGreenAmplifier_10 = L_1;
		float L_2 = (__this->___BlueAmplifier_8);
		((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeBlueAmplifier_11 = L_2;
		return;
	}
}
// System.Void CameraFilterPack_Edge_Edge_filter::Update()
extern TypeInfo* CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Edge_Edge_filter_Update_m730 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(99);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeRedAmplifier_9;
		__this->___RedAmplifier_6 = L_1;
		float L_2 = ((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeGreenAmplifier_10;
		__this->___GreenAmplifier_7 = L_2;
		float L_3 = ((CameraFilterPack_Edge_Edge_filter_t113_StaticFields*)CameraFilterPack_Edge_Edge_filter_t113_il2cpp_TypeInfo_var->static_fields)->___ChangeBlueAmplifier_11;
		__this->___BlueAmplifier_8 = L_3;
	}

IL_002b:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Edge_filter::OnDisable()
extern "C" void CameraFilterPack_Edge_Edge_filter_OnDisable_m731 (CameraFilterPack_Edge_Edge_filter_t113 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Edge_Golden
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Golden.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Edge_Golden
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_GoldenMethodDeclarations.h"



// System.Void CameraFilterPack_Edge_Golden::.ctor()
extern "C" void CameraFilterPack_Edge_Golden__ctor_m732 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Edge_Golden::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Edge_Golden_get_material_m733 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Edge_Golden::Start()
extern Il2CppCodeGenString* _stringLiteral176;
extern "C" void CameraFilterPack_Edge_Golden_Start_m734 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral176 = il2cpp_codegen_string_literal_from_index(176);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral176, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Golden::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Edge_Golden_OnRenderImage_m735 (CameraFilterPack_Edge_Golden_t114 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_008c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Edge_Golden_get_material_m733(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Edge_Golden_get_material_m733(__this, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_10 = {0};
		Vector2__ctor_m2714(&L_10, (((float)L_8)), (((float)L_9)), /*hidden argument*/NULL);
		Vector4_t5  L_11 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_11, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Material_t2 * L_14 = CameraFilterPack_Edge_Golden_get_material_m733(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		goto IL_0093;
	}

IL_008c:
	{
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Golden::Update()
extern "C" void CameraFilterPack_Edge_Golden_Update_m736 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Golden::OnDisable()
extern "C" void CameraFilterPack_Edge_Golden_OnDisable_m737 (CameraFilterPack_Edge_Golden_t114 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Edge_Neon
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Neon.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Edge_Neon
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_NeonMethodDeclarations.h"



// System.Void CameraFilterPack_Edge_Neon::.ctor()
extern "C" void CameraFilterPack_Edge_Neon__ctor_m738 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___EdgeWeight_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Edge_Neon::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Edge_Neon_get_material_m739 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Edge_Neon::Start()
extern TypeInfo* CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral177;
extern "C" void CameraFilterPack_Edge_Neon_Start_m740 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(100);
		_stringLiteral177 = il2cpp_codegen_string_literal_from_index(177);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___EdgeWeight_6);
		((CameraFilterPack_Edge_Neon_t115_StaticFields*)CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var->static_fields)->___ChangeEdgeWeight_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral177, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Neon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral178;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Edge_Neon_OnRenderImage_m741 (CameraFilterPack_Edge_Neon_t115 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral178 = il2cpp_codegen_string_literal_from_index(178);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a2;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Edge_Neon_get_material_m739(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Edge_Neon_get_material_m739(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___EdgeWeight_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral178, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Edge_Neon_get_material_m739(__this, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_12 = {0};
		Vector2__ctor_m2714(&L_12, (((float)L_10)), (((float)L_11)), /*hidden argument*/NULL);
		Vector4_t5  L_13 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_13, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		RenderTexture_t15 * L_15 = ___destTexture;
		Material_t2 * L_16 = CameraFilterPack_Edge_Neon_get_material_m739(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		goto IL_00a9;
	}

IL_00a2:
	{
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Neon::OnValidate()
extern TypeInfo* CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Edge_Neon_OnValidate_m742 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(100);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___EdgeWeight_6);
		((CameraFilterPack_Edge_Neon_t115_StaticFields*)CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var->static_fields)->___ChangeEdgeWeight_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Edge_Neon::Update()
extern TypeInfo* CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Edge_Neon_Update_m743 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(100);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Edge_Neon_t115_StaticFields*)CameraFilterPack_Edge_Neon_t115_il2cpp_TypeInfo_var->static_fields)->___ChangeEdgeWeight_7;
		__this->___EdgeWeight_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Neon::OnDisable()
extern "C" void CameraFilterPack_Edge_Neon_OnDisable_m744 (CameraFilterPack_Edge_Neon_t115 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Edge_Sigmoid
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Sigmoid.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Edge_Sigmoid
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_SigmoidMethodDeclarations.h"



// System.Void CameraFilterPack_Edge_Sigmoid::.ctor()
extern "C" void CameraFilterPack_Edge_Sigmoid__ctor_m745 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Gain_6 = (3.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Edge_Sigmoid::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Edge_Sigmoid_get_material_m746 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Edge_Sigmoid::Start()
extern TypeInfo* CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral179;
extern "C" void CameraFilterPack_Edge_Sigmoid_Start_m747 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		_stringLiteral179 = il2cpp_codegen_string_literal_from_index(179);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Gain_6);
		((CameraFilterPack_Edge_Sigmoid_t116_StaticFields*)CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var->static_fields)->___ChangeGain_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral179, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Sigmoid::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral180;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Edge_Sigmoid_OnRenderImage_m748 (CameraFilterPack_Edge_Sigmoid_t116 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral180 = il2cpp_codegen_string_literal_from_index(180);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a2;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Edge_Sigmoid_get_material_m746(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Edge_Sigmoid_get_material_m746(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Gain_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral180, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Edge_Sigmoid_get_material_m746(__this, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_12 = {0};
		Vector2__ctor_m2714(&L_12, (((float)L_10)), (((float)L_11)), /*hidden argument*/NULL);
		Vector4_t5  L_13 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_13, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		RenderTexture_t15 * L_15 = ___destTexture;
		Material_t2 * L_16 = CameraFilterPack_Edge_Sigmoid_get_material_m746(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		goto IL_00a9;
	}

IL_00a2:
	{
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Sigmoid::OnValidate()
extern TypeInfo* CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Edge_Sigmoid_OnValidate_m749 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Gain_6);
		((CameraFilterPack_Edge_Sigmoid_t116_StaticFields*)CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var->static_fields)->___ChangeGain_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Edge_Sigmoid::Update()
extern TypeInfo* CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Edge_Sigmoid_Update_m750 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(101);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Edge_Sigmoid_t116_StaticFields*)CameraFilterPack_Edge_Sigmoid_t116_il2cpp_TypeInfo_var->static_fields)->___ChangeGain_7;
		__this->___Gain_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Sigmoid::OnDisable()
extern "C" void CameraFilterPack_Edge_Sigmoid_OnDisable_m751 (CameraFilterPack_Edge_Sigmoid_t116 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Edge_Sobel
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_Sobel.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Edge_Sobel
#include "AssemblyU2DCSharp_CameraFilterPack_Edge_SobelMethodDeclarations.h"



// System.Void CameraFilterPack_Edge_Sobel::.ctor()
extern "C" void CameraFilterPack_Edge_Sobel__ctor_m752 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Edge_Sobel::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Edge_Sobel_get_material_m753 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Edge_Sobel::Start()
extern Il2CppCodeGenString* _stringLiteral181;
extern "C" void CameraFilterPack_Edge_Sobel_Start_m754 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral181 = il2cpp_codegen_string_literal_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral181, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Sobel::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Edge_Sobel_OnRenderImage_m755 (CameraFilterPack_Edge_Sobel_t117 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Edge_Sobel_get_material_m753(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Edge_Sobel_get_material_m753(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_Edge_Sobel_get_material_m753(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Sobel::Update()
extern "C" void CameraFilterPack_Edge_Sobel_Update_m756 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Edge_Sobel::OnDisable()
extern "C" void CameraFilterPack_Edge_Sobel_OnDisable_m757 (CameraFilterPack_Edge_Sobel_t117 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_EyesVision_1
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_1.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_EyesVision_1
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_1MethodDeclarations.h"

// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"


// System.Void CameraFilterPack_EyesVision_1::.ctor()
extern "C" void CameraFilterPack_EyesVision_1__ctor_m758 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->____EyeWave_4 = (15.0f);
		__this->____EyeSpeed_5 = (1.0f);
		__this->____EyeMove_6 = (2.0f);
		__this->____EyeBlink_7 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_EyesVision_1::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_EyesVision_1_get_material_m759 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_8 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_8);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_8);
		return L_5;
	}
}
// System.Void CameraFilterPack_EyesVision_1::Start()
extern TypeInfo* Texture2D_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral182;
extern Il2CppCodeGenString* _stringLiteral183;
extern "C" void CameraFilterPack_EyesVision_1_Start_m760 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral182 = il2cpp_codegen_string_literal_from_index(182);
		_stringLiteral183 = il2cpp_codegen_string_literal_from_index(183);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t473 * L_0 = Resources_Load_m2735(NULL /*static, unused*/, _stringLiteral182, /*hidden argument*/NULL);
		__this->___Texture2_9 = ((Texture2D_t9 *)IsInst(L_0, Texture2D_t9_il2cpp_TypeInfo_var));
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral183, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		return;
	}
}
// System.Void CameraFilterPack_EyesVision_1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral20;
extern "C" void CameraFilterPack_EyesVision_1_OnRenderImage_m761 (CameraFilterPack_EyesVision_1_t118 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d4;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_EyesVision_1_get_material_m759(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_EyesVision_1_get_material_m759(__this, /*hidden argument*/NULL);
		float L_8 = (__this->____EyeWave_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_EyesVision_1_get_material_m759(__this, /*hidden argument*/NULL);
		float L_10 = (__this->____EyeSpeed_5);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_EyesVision_1_get_material_m759(__this, /*hidden argument*/NULL);
		float L_12 = (__this->____EyeMove_6);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_EyesVision_1_get_material_m759(__this, /*hidden argument*/NULL);
		float L_14 = (__this->____EyeBlink_7);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_EyesVision_1_get_material_m759(__this, /*hidden argument*/NULL);
		Texture2D_t9 * L_16 = (__this->___Texture2_9);
		NullCheck(L_15);
		Material_SetTexture_m2736(L_15, _stringLiteral20, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_EyesVision_1_get_material_m759(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00db;
	}

IL_00d4:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00db:
	{
		return;
	}
}
// System.Void CameraFilterPack_EyesVision_1::Update()
extern "C" void CameraFilterPack_EyesVision_1_Update_m762 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_EyesVision_1::OnDisable()
extern "C" void CameraFilterPack_EyesVision_1_OnDisable_m763 (CameraFilterPack_EyesVision_1_t118 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_8);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_EyesVision_2
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_EyesVision_2
#include "AssemblyU2DCSharp_CameraFilterPack_EyesVision_2MethodDeclarations.h"



// System.Void CameraFilterPack_EyesVision_2::.ctor()
extern "C" void CameraFilterPack_EyesVision_2__ctor_m764 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->____EyeWave_4 = (15.0f);
		__this->____EyeSpeed_5 = (1.0f);
		__this->____EyeMove_6 = (2.0f);
		__this->____EyeBlink_7 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_EyesVision_2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_EyesVision_2_get_material_m765 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_8 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_8);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_8);
		return L_5;
	}
}
// System.Void CameraFilterPack_EyesVision_2::Start()
extern TypeInfo* Texture2D_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral184;
extern Il2CppCodeGenString* _stringLiteral185;
extern "C" void CameraFilterPack_EyesVision_2_Start_m766 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral184 = il2cpp_codegen_string_literal_from_index(184);
		_stringLiteral185 = il2cpp_codegen_string_literal_from_index(185);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t473 * L_0 = Resources_Load_m2735(NULL /*static, unused*/, _stringLiteral184, /*hidden argument*/NULL);
		__this->___Texture2_9 = ((Texture2D_t9 *)IsInst(L_0, Texture2D_t9_il2cpp_TypeInfo_var));
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral185, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		return;
	}
}
// System.Void CameraFilterPack_EyesVision_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral20;
extern "C" void CameraFilterPack_EyesVision_2_OnRenderImage_m767 (CameraFilterPack_EyesVision_2_t119 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d4;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_EyesVision_2_get_material_m765(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_EyesVision_2_get_material_m765(__this, /*hidden argument*/NULL);
		float L_8 = (__this->____EyeWave_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_EyesVision_2_get_material_m765(__this, /*hidden argument*/NULL);
		float L_10 = (__this->____EyeSpeed_5);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_EyesVision_2_get_material_m765(__this, /*hidden argument*/NULL);
		float L_12 = (__this->____EyeMove_6);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_EyesVision_2_get_material_m765(__this, /*hidden argument*/NULL);
		float L_14 = (__this->____EyeBlink_7);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_EyesVision_2_get_material_m765(__this, /*hidden argument*/NULL);
		Texture2D_t9 * L_16 = (__this->___Texture2_9);
		NullCheck(L_15);
		Material_SetTexture_m2736(L_15, _stringLiteral20, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_EyesVision_2_get_material_m765(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00db;
	}

IL_00d4:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00db:
	{
		return;
	}
}
// System.Void CameraFilterPack_EyesVision_2::Update()
extern "C" void CameraFilterPack_EyesVision_2_Update_m768 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_EyesVision_2::OnDisable()
extern "C" void CameraFilterPack_EyesVision_2_OnDisable_m769 (CameraFilterPack_EyesVision_2_t119 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_8);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_8bits
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_8bits
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bitsMethodDeclarations.h"

// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"


// System.Void CameraFilterPack_FX_8bits::.ctor()
extern "C" void CameraFilterPack_FX_8bits__ctor_m770 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___ResolutionX_6 = ((int32_t)160);
		__this->___ResolutionY_7 = ((int32_t)240);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_8bits::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_8bits_get_material_m771 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_4);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_4 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_4);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_4);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_8bits::Start()
extern TypeInfo* CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral186;
extern "C" void CameraFilterPack_FX_8bits_Start_m772 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(102);
		_stringLiteral186 = il2cpp_codegen_string_literal_from_index(186);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Brightness_5);
		((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeBrightness_8 = L_0;
		int32_t L_1 = (__this->___ResolutionX_6);
		((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeResolutionX_9 = L_1;
		int32_t L_2 = (__this->___ResolutionY_7);
		((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeResolutionY_10 = L_2;
		Shader_t1 * L_3 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral186, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_3;
		bool L_4 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_FX_8bits_OnRenderImage_m773 (CameraFilterPack_FX_8bits_t120 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t15 * V_0 = {0};
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00be;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_8bits_get_material_m771(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		float L_7 = (__this->___Brightness_5);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_006f;
		}
	}
	{
		__this->___Brightness_5 = (0.001f);
	}

IL_006f:
	{
		Material_t2 * L_8 = CameraFilterPack_FX_8bits_get_material_m771(__this, /*hidden argument*/NULL);
		float L_9 = (__this->___Brightness_5);
		NullCheck(L_8);
		Material_SetFloat_m2724(L_8, _stringLiteral16, L_9, /*hidden argument*/NULL);
		int32_t L_10 = (__this->___ResolutionX_6);
		int32_t L_11 = (__this->___ResolutionY_7);
		RenderTexture_t15 * L_12 = RenderTexture_GetTemporary_m2744(NULL /*static, unused*/, L_10, L_11, 0, /*hidden argument*/NULL);
		V_0 = L_12;
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = V_0;
		Material_t2 * L_15 = CameraFilterPack_FX_8bits_get_material_m771(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = V_0;
		NullCheck(L_16);
		Texture_set_filterMode_m2745(L_16, 0, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = V_0;
		RenderTexture_t15 * L_18 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		RenderTexture_t15 * L_19 = V_0;
		RenderTexture_ReleaseTemporary_m2746(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		goto IL_00c5;
	}

IL_00be:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits::OnValidate()
extern TypeInfo* CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_8bits_OnValidate_m774 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(102);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Brightness_5);
		((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeBrightness_8 = L_0;
		int32_t L_1 = (__this->___ResolutionX_6);
		((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeResolutionX_9 = L_1;
		int32_t L_2 = (__this->___ResolutionY_7);
		((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeResolutionY_10 = L_2;
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits::Update()
extern TypeInfo* CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_8bits_Update_m775 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(102);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeBrightness_8;
		__this->___Brightness_5 = L_1;
		int32_t L_2 = ((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeResolutionX_9;
		__this->___ResolutionX_6 = L_2;
		int32_t L_3 = ((CameraFilterPack_FX_8bits_t120_StaticFields*)CameraFilterPack_FX_8bits_t120_il2cpp_TypeInfo_var->static_fields)->___ChangeResolutionY_10;
		__this->___ResolutionY_7 = L_3;
	}

IL_002b:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits::OnDisable()
extern "C" void CameraFilterPack_FX_8bits_OnDisable_m776 (CameraFilterPack_FX_8bits_t120 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_4);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_4);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_8bits_gb
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits_gb.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_8bits_gb
#include "AssemblyU2DCSharp_CameraFilterPack_FX_8bits_gbMethodDeclarations.h"



// System.Void CameraFilterPack_FX_8bits_gb::.ctor()
extern "C" void CameraFilterPack_FX_8bits_gb__ctor_m777 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_8bits_gb::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_8bits_gb_get_material_m778 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_4);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_4 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_4);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_4);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_8bits_gb::Start()
extern TypeInfo* CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral187;
extern "C" void CameraFilterPack_FX_8bits_gb_Start_m779 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(103);
		_stringLiteral187 = il2cpp_codegen_string_literal_from_index(187);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Brightness_5);
		((CameraFilterPack_FX_8bits_gb_t121_StaticFields*)CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var->static_fields)->___ChangeBrightness_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral187, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits_gb::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_FX_8bits_gb_OnRenderImage_m780 (CameraFilterPack_FX_8bits_gb_t121 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	RenderTexture_t15 * V_0 = {0};
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bc;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_8bits_gb_get_material_m778(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		float L_7 = (__this->___Brightness_5);
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_006f;
		}
	}
	{
		__this->___Brightness_5 = (0.001f);
	}

IL_006f:
	{
		Material_t2 * L_8 = CameraFilterPack_FX_8bits_gb_get_material_m778(__this, /*hidden argument*/NULL);
		float L_9 = (__this->___Brightness_5);
		NullCheck(L_8);
		Material_SetFloat_m2724(L_8, _stringLiteral16, L_9, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = RenderTexture_GetTemporary_m2744(NULL /*static, unused*/, ((int32_t)160), ((int32_t)144), 0, /*hidden argument*/NULL);
		V_0 = L_10;
		RenderTexture_t15 * L_11 = ___sourceTexture;
		RenderTexture_t15 * L_12 = V_0;
		Material_t2 * L_13 = CameraFilterPack_FX_8bits_gb_get_material_m778(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = V_0;
		NullCheck(L_14);
		Texture_set_filterMode_m2745(L_14, 0, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = V_0;
		RenderTexture_t15 * L_16 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = V_0;
		RenderTexture_ReleaseTemporary_m2746(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		goto IL_00c3;
	}

IL_00bc:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits_gb::OnValidate()
extern TypeInfo* CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_8bits_gb_OnValidate_m781 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(103);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Brightness_5);
		((CameraFilterPack_FX_8bits_gb_t121_StaticFields*)CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var->static_fields)->___ChangeBrightness_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits_gb::Update()
extern TypeInfo* CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_8bits_gb_Update_m782 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(103);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_8bits_gb_t121_StaticFields*)CameraFilterPack_FX_8bits_gb_t121_il2cpp_TypeInfo_var->static_fields)->___ChangeBrightness_6;
		__this->___Brightness_5 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_8bits_gb::OnDisable()
extern "C" void CameraFilterPack_FX_8bits_gb_OnDisable_m783 (CameraFilterPack_FX_8bits_gb_t121 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_4);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_4);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Ascii
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Ascii.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Ascii
#include "AssemblyU2DCSharp_CameraFilterPack_FX_AsciiMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Ascii::.ctor()
extern "C" void CameraFilterPack_FX_Ascii__ctor_m784 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Ascii::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Ascii_get_material_m785 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Ascii::Start()
extern Il2CppCodeGenString* _stringLiteral188;
extern "C" void CameraFilterPack_FX_Ascii_Start_m786 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral188 = il2cpp_codegen_string_literal_from_index(188);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral188, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Ascii::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Ascii_OnRenderImage_m787 (CameraFilterPack_FX_Ascii_t122 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Ascii_get_material_m785(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Ascii_get_material_m785(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_FX_Ascii_get_material_m785(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Ascii::Update()
extern "C" void CameraFilterPack_FX_Ascii_Update_m788 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Ascii::OnDisable()
extern "C" void CameraFilterPack_FX_Ascii_OnDisable_m789 (CameraFilterPack_FX_Ascii_t122 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Dot_Circle
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Dot_Circle.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Dot_Circle
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Dot_CircleMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Dot_Circle::.ctor()
extern "C" void CameraFilterPack_FX_Dot_Circle__ctor_m790 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (7.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Dot_Circle::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Dot_Circle_get_material_m791 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Dot_Circle::Start()
extern TypeInfo* CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral189;
extern "C" void CameraFilterPack_FX_Dot_Circle_Start_m792 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		_stringLiteral189 = il2cpp_codegen_string_literal_from_index(189);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Dot_Circle_t123_StaticFields*)CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral189, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Dot_Circle::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void CameraFilterPack_FX_Dot_Circle_OnRenderImage_m793 (CameraFilterPack_FX_Dot_Circle_t123 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Dot_Circle_get_material_m791(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Dot_Circle_get_material_m791(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_FX_Dot_Circle_get_material_m791(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value_6);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral2, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_Dot_Circle_get_material_m791(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Dot_Circle::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Dot_Circle_OnValidate_m794 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Dot_Circle_t123_StaticFields*)CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Dot_Circle::Update()
extern TypeInfo* CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Dot_Circle_Update_m795 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Dot_Circle_t123_StaticFields*)CameraFilterPack_FX_Dot_Circle_t123_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Dot_Circle::OnDisable()
extern "C" void CameraFilterPack_FX_Dot_Circle_OnDisable_m796 (CameraFilterPack_FX_Dot_Circle_t123 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Drunk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Drunk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_DrunkMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Drunk::.ctor()
extern "C" void CameraFilterPack_FX_Drunk__ctor_m797 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (6.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean CameraFilterPack_FX_Drunk::get_IsDizzy()
extern "C" bool CameraFilterPack_FX_Drunk_get_IsDizzy_m798 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		bool L_0 = CameraFilterPack_FX_Drunk_get_IsDizzy_m798(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void CameraFilterPack_FX_Drunk::set_IsDizzy(System.Boolean)
extern "C" void CameraFilterPack_FX_Drunk_set_IsDizzy_m799 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		CameraFilterPack_FX_Drunk_set_IsDizzy_m799(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Drunk::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Drunk_get_material_m800 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Drunk::Start()
extern TypeInfo* CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral190;
extern "C" void CameraFilterPack_FX_Drunk_Start_m801 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(105);
		_stringLiteral190 = il2cpp_codegen_string_literal_from_index(190);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Drunk_t124_StaticFields*)CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral190, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Drunk_OnRenderImage_m802 (CameraFilterPack_FX_Drunk_t124 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Drunk_get_material_m800(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Drunk_get_material_m800(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Drunk_get_material_m800(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_Drunk_get_material_m800(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Drunk_OnValidate_m803 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(105);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Drunk_t124_StaticFields*)CameraFilterPack_FX_Drunk_t124_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk::Update()
extern "C" void CameraFilterPack_FX_Drunk_Update_m804 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000a;
		}
	}

IL_000a:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk::OnDisable()
extern "C" void CameraFilterPack_FX_Drunk_OnDisable_m805 (CameraFilterPack_FX_Drunk_t124 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Drunk2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Drunk2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Drunk2MethodDeclarations.h"



// System.Void CameraFilterPack_FX_Drunk2::.ctor()
extern "C" void CameraFilterPack_FX_Drunk2__ctor_m806 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (1.0f);
		__this->___Value2_7 = (1.0f);
		__this->___Value3_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Drunk2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Drunk2_get_material_m807 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Drunk2::Start()
extern TypeInfo* CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral191;
extern "C" void CameraFilterPack_FX_Drunk2_Start_m808 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		_stringLiteral191 = il2cpp_codegen_string_literal_from_index(191);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral191, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Drunk2_OnRenderImage_m809 (CameraFilterPack_FX_Drunk2_t125 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Drunk2_get_material_m807(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Drunk2_get_material_m807(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Drunk2_get_material_m807(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Value2_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_FX_Drunk2_get_material_m807(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Value3_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_FX_Drunk2_get_material_m807(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_FX_Drunk2_get_material_m807(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_FX_Drunk2_get_material_m807(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk2::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Drunk2_OnValidate_m810 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk2::Update()
extern TypeInfo* CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Drunk2_Update_m811 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(106);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Value_6 = L_1;
		float L_2 = ((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Value2_7 = L_2;
		float L_3 = ((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Value3_8 = L_3;
		float L_4 = ((CameraFilterPack_FX_Drunk2_t125_StaticFields*)CameraFilterPack_FX_Drunk2_t125_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Drunk2::OnDisable()
extern "C" void CameraFilterPack_FX_Drunk2_OnDisable_m812 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_EarthQuake
#include "AssemblyU2DCSharp_CameraFilterPack_FX_EarthQuake.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_EarthQuake
#include "AssemblyU2DCSharp_CameraFilterPack_FX_EarthQuakeMethodDeclarations.h"



// System.Void CameraFilterPack_FX_EarthQuake::.ctor()
extern "C" void CameraFilterPack_FX_EarthQuake__ctor_m813 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Speed_6 = (15.0f);
		__this->___X_7 = (0.008f);
		__this->___Y_8 = (0.008f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_EarthQuake::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_EarthQuake_get_material_m814 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_EarthQuake::Start()
extern TypeInfo* CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral192;
extern "C" void CameraFilterPack_FX_EarthQuake_Start_m815 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		_stringLiteral192 = il2cpp_codegen_string_literal_from_index(192);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Speed_6);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___X_7);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Y_8);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral192, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_EarthQuake::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_EarthQuake_OnRenderImage_m816 (CameraFilterPack_FX_EarthQuake_t126 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_EarthQuake_get_material_m814(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_EarthQuake_get_material_m814(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Speed_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_EarthQuake_get_material_m814(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___X_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_FX_EarthQuake_get_material_m814(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Y_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_FX_EarthQuake_get_material_m814(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_FX_EarthQuake_get_material_m814(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_FX_EarthQuake_get_material_m814(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_EarthQuake::OnValidate()
extern TypeInfo* CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_EarthQuake_OnValidate_m817 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Speed_6);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___X_7);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Y_8);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_FX_EarthQuake::Update()
extern TypeInfo* CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_EarthQuake_Update_m818 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(107);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Speed_6 = L_1;
		float L_2 = ((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___X_7 = L_2;
		float L_3 = ((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Y_8 = L_3;
		float L_4 = ((CameraFilterPack_FX_EarthQuake_t126_StaticFields*)CameraFilterPack_FX_EarthQuake_t126_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_EarthQuake::OnDisable()
extern "C" void CameraFilterPack_FX_EarthQuake_OnDisable_m819 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Funk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Funk.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Funk
#include "AssemblyU2DCSharp_CameraFilterPack_FX_FunkMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Funk::.ctor()
extern "C" void CameraFilterPack_FX_Funk__ctor_m820 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Funk::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Funk_get_material_m821 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Funk::Start()
extern Il2CppCodeGenString* _stringLiteral193;
extern "C" void CameraFilterPack_FX_Funk_Start_m822 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral193 = il2cpp_codegen_string_literal_from_index(193);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral193, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Funk::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Funk_OnRenderImage_m823 (CameraFilterPack_FX_Funk_t127 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Funk_get_material_m821(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Funk_get_material_m821(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_FX_Funk_get_material_m821(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Funk::Update()
extern "C" void CameraFilterPack_FX_Funk_Update_m824 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Funk::OnDisable()
extern "C" void CameraFilterPack_FX_Funk_OnDisable_m825 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Glitch1
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch1.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Glitch1
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch1MethodDeclarations.h"



// System.Void CameraFilterPack_FX_Glitch1::.ctor()
extern "C" void CameraFilterPack_FX_Glitch1__ctor_m826 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (6.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Glitch1::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Glitch1_get_material_m827 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Glitch1::Start()
extern TypeInfo* CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral194;
extern "C" void CameraFilterPack_FX_Glitch1_Start_m828 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		_stringLiteral194 = il2cpp_codegen_string_literal_from_index(194);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Glitch1_t128_StaticFields*)CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral194, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Glitch1_OnRenderImage_m829 (CameraFilterPack_FX_Glitch1_t128 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Glitch1_get_material_m827(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Glitch1_get_material_m827(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Glitch1_get_material_m827(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_Glitch1_get_material_m827(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch1::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Glitch1_OnValidate_m830 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Glitch1_t128_StaticFields*)CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch1::Update()
extern TypeInfo* CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Glitch1_Update_m831 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(108);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Glitch1_t128_StaticFields*)CameraFilterPack_FX_Glitch1_t128_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch1::OnDisable()
extern "C" void CameraFilterPack_FX_Glitch1_OnDisable_m832 (CameraFilterPack_FX_Glitch1_t128 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Glitch2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Glitch2
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch2MethodDeclarations.h"



// System.Void CameraFilterPack_FX_Glitch2::.ctor()
extern "C" void CameraFilterPack_FX_Glitch2__ctor_m833 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (6.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Glitch2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Glitch2_get_material_m834 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Glitch2::Start()
extern TypeInfo* CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral195;
extern "C" void CameraFilterPack_FX_Glitch2_Start_m835 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		_stringLiteral195 = il2cpp_codegen_string_literal_from_index(195);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Glitch2_t129_StaticFields*)CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral195, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Glitch2_OnRenderImage_m836 (CameraFilterPack_FX_Glitch2_t129 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Glitch2_get_material_m834(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Glitch2_get_material_m834(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Glitch2_get_material_m834(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_Glitch2_get_material_m834(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch2::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Glitch2_OnValidate_m837 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Glitch2_t129_StaticFields*)CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch2::Update()
extern TypeInfo* CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Glitch2_Update_m838 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(109);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Glitch2_t129_StaticFields*)CameraFilterPack_FX_Glitch2_t129_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch2::OnDisable()
extern "C" void CameraFilterPack_FX_Glitch2_OnDisable_m839 (CameraFilterPack_FX_Glitch2_t129 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Glitch3
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch3.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Glitch3
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Glitch3MethodDeclarations.h"



// System.Void CameraFilterPack_FX_Glitch3::.ctor()
extern "C" void CameraFilterPack_FX_Glitch3__ctor_m840 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (6.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Glitch3::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Glitch3_get_material_m841 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Glitch3::Start()
extern TypeInfo* CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral196;
extern "C" void CameraFilterPack_FX_Glitch3_Start_m842 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		_stringLiteral196 = il2cpp_codegen_string_literal_from_index(196);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Glitch3_t130_StaticFields*)CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral196, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch3::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Glitch3_OnRenderImage_m843 (CameraFilterPack_FX_Glitch3_t130 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Glitch3_get_material_m841(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Glitch3_get_material_m841(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Glitch3_get_material_m841(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_Glitch3_get_material_m841(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch3::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Glitch3_OnValidate_m844 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Glitch3_t130_StaticFields*)CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch3::Update()
extern TypeInfo* CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Glitch3_Update_m845 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(110);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Glitch3_t130_StaticFields*)CameraFilterPack_FX_Glitch3_t130_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Glitch3::OnDisable()
extern "C" void CameraFilterPack_FX_Glitch3_OnDisable_m846 (CameraFilterPack_FX_Glitch3_t130 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Grid
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Grid.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Grid
#include "AssemblyU2DCSharp_CameraFilterPack_FX_GridMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Grid::.ctor()
extern "C" void CameraFilterPack_FX_Grid__ctor_m847 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Distortion_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Grid::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Grid_get_material_m848 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_4);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_4 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_4);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_4);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Grid::Start()
extern TypeInfo* CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral197;
extern "C" void CameraFilterPack_FX_Grid_Start_m849 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		_stringLiteral197 = il2cpp_codegen_string_literal_from_index(197);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_5);
		((CameraFilterPack_FX_Grid_t131_StaticFields*)CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral197, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Grid::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_FX_Grid_OnRenderImage_m850 (CameraFilterPack_FX_Grid_t131 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Grid_get_material_m848(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Grid_get_material_m848(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_9 = ___sourceTexture;
		RenderTexture_t15 * L_10 = ___destTexture;
		Material_t2 * L_11 = CameraFilterPack_FX_Grid_get_material_m848(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007c:
	{
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Grid::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Grid_OnValidate_m851 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_5);
		((CameraFilterPack_FX_Grid_t131_StaticFields*)CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Grid::Update()
extern TypeInfo* CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Grid_Update_m852 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Grid_t131_StaticFields*)CameraFilterPack_FX_Grid_t131_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6;
		__this->___Distortion_5 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Grid::OnDisable()
extern "C" void CameraFilterPack_FX_Grid_OnDisable_m853 (CameraFilterPack_FX_Grid_t131 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_4);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_4);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Hexagon
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Hexagon
#include "AssemblyU2DCSharp_CameraFilterPack_FX_HexagonMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Hexagon::.ctor()
extern "C" void CameraFilterPack_FX_Hexagon__ctor_m854 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Hexagon::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Hexagon_get_material_m855 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Hexagon::Start()
extern Il2CppCodeGenString* _stringLiteral198;
extern "C" void CameraFilterPack_FX_Hexagon_Start_m856 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral198 = il2cpp_codegen_string_literal_from_index(198);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral198, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hexagon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Hexagon_OnRenderImage_m857 (CameraFilterPack_FX_Hexagon_t132 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Hexagon_get_material_m855(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Hexagon_get_material_m855(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_FX_Hexagon_get_material_m855(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hexagon::Update()
extern "C" void CameraFilterPack_FX_Hexagon_Update_m858 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hexagon::OnDisable()
extern "C" void CameraFilterPack_FX_Hexagon_OnDisable_m859 (CameraFilterPack_FX_Hexagon_t132 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Hexagon_Black
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon_Black.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Hexagon_Black
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hexagon_BlackMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Hexagon_Black::.ctor()
extern "C" void CameraFilterPack_FX_Hexagon_Black__ctor_m860 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Hexagon_Black::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Hexagon_Black_get_material_m861 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Hexagon_Black::Start()
extern TypeInfo* CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral199;
extern "C" void CameraFilterPack_FX_Hexagon_Black_Start_m862 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		_stringLiteral199 = il2cpp_codegen_string_literal_from_index(199);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Hexagon_Black_t133_StaticFields*)CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral199, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hexagon_Black::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Hexagon_Black_OnRenderImage_m863 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Hexagon_Black_get_material_m861(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Hexagon_Black_get_material_m861(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Hexagon_Black_get_material_m861(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_Hexagon_Black_get_material_m861(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hexagon_Black::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Hexagon_Black_OnValidate_m864 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Hexagon_Black_t133_StaticFields*)CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Hexagon_Black::Update()
extern TypeInfo* CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Hexagon_Black_Update_m865 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(112);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Hexagon_Black_t133_StaticFields*)CameraFilterPack_FX_Hexagon_Black_t133_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hexagon_Black::OnDisable()
extern "C" void CameraFilterPack_FX_Hexagon_Black_OnDisable_m866 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Hypno
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Hypno.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Hypno
#include "AssemblyU2DCSharp_CameraFilterPack_FX_HypnoMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Hypno::.ctor()
extern "C" void CameraFilterPack_FX_Hypno__ctor_m867 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Speed_6 = (1.0f);
		__this->___Green_8 = (1.0f);
		__this->___Blue_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Hypno::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Hypno_get_material_m868 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Hypno::Start()
extern TypeInfo* CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral200;
extern "C" void CameraFilterPack_FX_Hypno_Start_m869 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		_stringLiteral200 = il2cpp_codegen_string_literal_from_index(200);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Speed_6);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Red_7);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Green_8);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Blue_9);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral200, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hypno::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Hypno_OnRenderImage_m870 (CameraFilterPack_FX_Hypno_t134 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Hypno_get_material_m868(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Hypno_get_material_m868(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Speed_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Hypno_get_material_m868(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Red_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_FX_Hypno_get_material_m868(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Green_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_FX_Hypno_get_material_m868(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Blue_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_FX_Hypno_get_material_m868(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_FX_Hypno_get_material_m868(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hypno::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Hypno_OnValidate_m871 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Speed_6);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Red_7);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Green_8);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Blue_9);
		((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_FX_Hypno::Update()
extern TypeInfo* CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Hypno_Update_m872 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Speed_6 = L_1;
		float L_2 = ((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Red_7 = L_2;
		float L_3 = ((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Green_8 = L_3;
		float L_4 = ((CameraFilterPack_FX_Hypno_t134_StaticFields*)CameraFilterPack_FX_Hypno_t134_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Blue_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Hypno::OnDisable()
extern "C" void CameraFilterPack_FX_Hypno_OnDisable_m873 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_InverChromiLum
#include "AssemblyU2DCSharp_CameraFilterPack_FX_InverChromiLum.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_InverChromiLum
#include "AssemblyU2DCSharp_CameraFilterPack_FX_InverChromiLumMethodDeclarations.h"



// System.Void CameraFilterPack_FX_InverChromiLum::.ctor()
extern "C" void CameraFilterPack_FX_InverChromiLum__ctor_m874 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_InverChromiLum::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_InverChromiLum_get_material_m875 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_InverChromiLum::Start()
extern Il2CppCodeGenString* _stringLiteral201;
extern "C" void CameraFilterPack_FX_InverChromiLum_Start_m876 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral201 = il2cpp_codegen_string_literal_from_index(201);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral201, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_InverChromiLum::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_InverChromiLum_OnRenderImage_m877 (CameraFilterPack_FX_InverChromiLum_t135 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_InverChromiLum_get_material_m875(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_InverChromiLum_get_material_m875(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_FX_InverChromiLum_get_material_m875(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_InverChromiLum::Update()
extern "C" void CameraFilterPack_FX_InverChromiLum_Update_m878 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_InverChromiLum::OnDisable()
extern "C" void CameraFilterPack_FX_InverChromiLum_OnDisable_m879 (CameraFilterPack_FX_InverChromiLum_t135 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Mirror
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Mirror.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Mirror
#include "AssemblyU2DCSharp_CameraFilterPack_FX_MirrorMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Mirror::.ctor()
extern "C" void CameraFilterPack_FX_Mirror__ctor_m880 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Mirror::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Mirror_get_material_m881 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Mirror::Start()
extern Il2CppCodeGenString* _stringLiteral202;
extern "C" void CameraFilterPack_FX_Mirror_Start_m882 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral202 = il2cpp_codegen_string_literal_from_index(202);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral202, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Mirror::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Mirror_OnRenderImage_m883 (CameraFilterPack_FX_Mirror_t136 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Mirror_get_material_m881(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Mirror_get_material_m881(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_FX_Mirror_get_material_m881(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Mirror::Update()
extern "C" void CameraFilterPack_FX_Mirror_Update_m884 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Mirror::OnDisable()
extern "C" void CameraFilterPack_FX_Mirror_OnDisable_m885 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Plasma.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_FX_PlasmaMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Plasma::.ctor()
extern "C" void CameraFilterPack_FX_Plasma__ctor_m886 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (6.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Plasma::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Plasma_get_material_m887 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Plasma::Start()
extern TypeInfo* CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral203;
extern "C" void CameraFilterPack_FX_Plasma_Start_m888 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		_stringLiteral203 = il2cpp_codegen_string_literal_from_index(203);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Plasma_t137_StaticFields*)CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral203, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Plasma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Plasma_OnRenderImage_m889 (CameraFilterPack_FX_Plasma_t137 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Plasma_get_material_m887(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Plasma_get_material_m887(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Plasma_get_material_m887(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_Plasma_get_material_m887(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Plasma::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Plasma_OnValidate_m890 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_Plasma_t137_StaticFields*)CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Plasma::Update()
extern TypeInfo* CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Plasma_Update_m891 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(114);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Plasma_t137_StaticFields*)CameraFilterPack_FX_Plasma_t137_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Plasma::OnDisable()
extern "C" void CameraFilterPack_FX_Plasma_OnDisable_m892 (CameraFilterPack_FX_Plasma_t137 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Psycho.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_FX_PsychoMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Psycho::.ctor()
extern "C" void CameraFilterPack_FX_Psycho__ctor_m893 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_4 = (1.0f);
		__this->___Distortion_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Psycho::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Psycho_get_material_m894 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_3);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_3 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_3);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_3);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Psycho::Start()
extern TypeInfo* CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral204;
extern "C" void CameraFilterPack_FX_Psycho_Start_m895 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		_stringLiteral204 = il2cpp_codegen_string_literal_from_index(204);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_5);
		((CameraFilterPack_FX_Psycho_t138_StaticFields*)CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral204, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Psycho::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_FX_Psycho_OnRenderImage_m896 (CameraFilterPack_FX_Psycho_t138 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Psycho_get_material_m894(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Psycho_get_material_m894(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_9 = ___sourceTexture;
		RenderTexture_t15 * L_10 = ___destTexture;
		Material_t2 * L_11 = CameraFilterPack_FX_Psycho_get_material_m894(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007c:
	{
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Psycho::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Psycho_OnValidate_m897 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_5);
		((CameraFilterPack_FX_Psycho_t138_StaticFields*)CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_Psycho::Update()
extern TypeInfo* CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Psycho_Update_m898 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(115);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Psycho_t138_StaticFields*)CameraFilterPack_FX_Psycho_t138_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6;
		__this->___Distortion_5 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Psycho::OnDisable()
extern "C" void CameraFilterPack_FX_Psycho_OnDisable_m899 (CameraFilterPack_FX_Psycho_t138 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_3);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_3);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Screens
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Screens.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Screens
#include "AssemblyU2DCSharp_CameraFilterPack_FX_ScreensMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Screens::.ctor()
extern "C" void CameraFilterPack_FX_Screens__ctor_m900 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Tiles_6 = (8.0f);
		__this->___Speed_7 = (0.25f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Screens::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Screens_get_material_m901 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Screens::Start()
extern TypeInfo* CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral205;
extern "C" void CameraFilterPack_FX_Screens_Start_m902 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		_stringLiteral205 = il2cpp_codegen_string_literal_from_index(205);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Tiles_6);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Speed_7);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___PosX_8);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___PosY_9);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral205, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Screens::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Screens_OnRenderImage_m903 (CameraFilterPack_FX_Screens_t139 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Screens_get_material_m901(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Screens_get_material_m901(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Tiles_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_Screens_get_material_m901(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Speed_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_FX_Screens_get_material_m901(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___PosX_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_FX_Screens_get_material_m901(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___PosY_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_FX_Screens_get_material_m901(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_FX_Screens_get_material_m901(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Screens::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Screens_OnValidate_m904 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Tiles_6);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Speed_7);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___PosX_8);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___PosY_9);
		((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_FX_Screens::Update()
extern TypeInfo* CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Screens_Update_m905 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(116);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Tiles_6 = L_1;
		float L_2 = ((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Speed_7 = L_2;
		float L_3 = ((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___PosX_8 = L_3;
		float L_4 = ((CameraFilterPack_FX_Screens_t139_StaticFields*)CameraFilterPack_FX_Screens_t139_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___PosY_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Screens::OnDisable()
extern "C" void CameraFilterPack_FX_Screens_OnDisable_m906 (CameraFilterPack_FX_Screens_t139 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_Spot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_Spot.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_Spot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_SpotMethodDeclarations.h"



// System.Void CameraFilterPack_FX_Spot::.ctor()
extern "C" void CameraFilterPack_FX_Spot__ctor_m907 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		Vector2_t7  L_0 = {0};
		Vector2__ctor_m2714(&L_0, (0.5f), (0.5f), /*hidden argument*/NULL);
		__this->___center_6 = L_0;
		__this->___Radius_7 = (0.2f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_Spot::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_Spot_get_material_m908 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_Spot::Start()
extern TypeInfo* CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral206;
extern "C" void CameraFilterPack_FX_Spot_Start_m909 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(117);
		_stringLiteral206 = il2cpp_codegen_string_literal_from_index(206);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t7  L_0 = (__this->___center_6);
		((CameraFilterPack_FX_Spot_t140_StaticFields*)CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var->static_fields)->___Changecenter_8 = L_0;
		float L_1 = (__this->___Radius_7);
		((CameraFilterPack_FX_Spot_t140_StaticFields*)CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var->static_fields)->___ChangeRadius_9 = L_1;
		Shader_t1 * L_2 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral206, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_2;
		bool L_3 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Spot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral4;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral6;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_Spot_OnRenderImage_m910 (CameraFilterPack_FX_Spot_t140 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral6 = il2cpp_codegen_string_literal_from_index(6);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00df;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_Spot_get_material_m908(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_Spot_get_material_m908(__this, /*hidden argument*/NULL);
		Vector2_t7 * L_8 = &(__this->___center_6);
		float L_9 = (L_8->___x_1);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral4, L_9, /*hidden argument*/NULL);
		Material_t2 * L_10 = CameraFilterPack_FX_Spot_get_material_m908(__this, /*hidden argument*/NULL);
		Vector2_t7 * L_11 = &(__this->___center_6);
		float L_12 = (L_11->___y_2);
		NullCheck(L_10);
		Material_SetFloat_m2724(L_10, _stringLiteral5, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_FX_Spot_get_material_m908(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Radius_7);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral6, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_FX_Spot_get_material_m908(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_FX_Spot_get_material_m908(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00e6;
	}

IL_00df:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Spot::OnValidate()
extern TypeInfo* CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Spot_OnValidate_m911 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(117);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t7  L_0 = (__this->___center_6);
		((CameraFilterPack_FX_Spot_t140_StaticFields*)CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var->static_fields)->___Changecenter_8 = L_0;
		float L_1 = (__this->___Radius_7);
		((CameraFilterPack_FX_Spot_t140_StaticFields*)CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var->static_fields)->___ChangeRadius_9 = L_1;
		return;
	}
}
// System.Void CameraFilterPack_FX_Spot::Update()
extern TypeInfo* CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_Spot_Update_m912 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(117);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		Vector2_t7  L_1 = ((CameraFilterPack_FX_Spot_t140_StaticFields*)CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var->static_fields)->___Changecenter_8;
		__this->___center_6 = L_1;
		float L_2 = ((CameraFilterPack_FX_Spot_t140_StaticFields*)CameraFilterPack_FX_Spot_t140_il2cpp_TypeInfo_var->static_fields)->___ChangeRadius_9;
		__this->___Radius_7 = L_2;
	}

IL_0020:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_Spot::OnDisable()
extern "C" void CameraFilterPack_FX_Spot_OnDisable_m913 (CameraFilterPack_FX_Spot_t140 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_ZebraColor
#include "AssemblyU2DCSharp_CameraFilterPack_FX_ZebraColor.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_ZebraColor
#include "AssemblyU2DCSharp_CameraFilterPack_FX_ZebraColorMethodDeclarations.h"



// System.Void CameraFilterPack_FX_ZebraColor::.ctor()
extern "C" void CameraFilterPack_FX_ZebraColor__ctor_m914 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (3.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_ZebraColor::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_ZebraColor_get_material_m915 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_ZebraColor::Start()
extern TypeInfo* CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral207;
extern "C" void CameraFilterPack_FX_ZebraColor_Start_m916 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		_stringLiteral207 = il2cpp_codegen_string_literal_from_index(207);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_ZebraColor_t141_StaticFields*)CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral207, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_ZebraColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_ZebraColor_OnRenderImage_m917 (CameraFilterPack_FX_ZebraColor_t141 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_ZebraColor_get_material_m915(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_ZebraColor_get_material_m915(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_FX_ZebraColor_get_material_m915(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_FX_ZebraColor_get_material_m915(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_ZebraColor::OnValidate()
extern TypeInfo* CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_ZebraColor_OnValidate_m918 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_FX_ZebraColor_t141_StaticFields*)CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_FX_ZebraColor::Update()
extern TypeInfo* CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_FX_ZebraColor_Update_m919 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(118);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_FX_ZebraColor_t141_StaticFields*)CameraFilterPack_FX_ZebraColor_t141_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_ZebraColor::OnDisable()
extern "C" void CameraFilterPack_FX_ZebraColor_OnDisable_m920 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_FX_superDot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_superDot.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_FX_superDot
#include "AssemblyU2DCSharp_CameraFilterPack_FX_superDotMethodDeclarations.h"



// System.Void CameraFilterPack_FX_superDot::.ctor()
extern "C" void CameraFilterPack_FX_superDot__ctor_m921 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_FX_superDot::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_FX_superDot_get_material_m922 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_FX_superDot::Start()
extern Il2CppCodeGenString* _stringLiteral208;
extern "C" void CameraFilterPack_FX_superDot_Start_m923 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral208 = il2cpp_codegen_string_literal_from_index(208);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral208, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_superDot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_FX_superDot_OnRenderImage_m924 (CameraFilterPack_FX_superDot_t142 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_FX_superDot_get_material_m922(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_FX_superDot_get_material_m922(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_FX_superDot_get_material_m922(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_superDot::Update()
extern "C" void CameraFilterPack_FX_superDot_Update_m925 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_FX_superDot::OnDisable()
extern "C" void CameraFilterPack_FX_superDot_OnDisable_m926 (CameraFilterPack_FX_superDot_t142 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Film_ColorPerfection
#include "AssemblyU2DCSharp_CameraFilterPack_Film_ColorPerfection.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Film_ColorPerfection
#include "AssemblyU2DCSharp_CameraFilterPack_Film_ColorPerfectionMethodDeclarations.h"



// System.Void CameraFilterPack_Film_ColorPerfection::.ctor()
extern "C" void CameraFilterPack_Film_ColorPerfection__ctor_m927 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Gamma_6 = (0.55f);
		__this->___Value2_7 = (1.0f);
		__this->___Value3_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Film_ColorPerfection::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Film_ColorPerfection_get_material_m928 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Film_ColorPerfection::Start()
extern TypeInfo* CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral209;
extern "C" void CameraFilterPack_Film_ColorPerfection_Start_m929 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		_stringLiteral209 = il2cpp_codegen_string_literal_from_index(209);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Gamma_6);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral209, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Film_ColorPerfection::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Film_ColorPerfection_OnRenderImage_m930 (CameraFilterPack_Film_ColorPerfection_t143 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Film_ColorPerfection_get_material_m928(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Film_ColorPerfection_get_material_m928(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Gamma_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Film_ColorPerfection_get_material_m928(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Value2_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Film_ColorPerfection_get_material_m928(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Value3_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Film_ColorPerfection_get_material_m928(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Film_ColorPerfection_get_material_m928(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Film_ColorPerfection_get_material_m928(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Film_ColorPerfection::OnValidate()
extern TypeInfo* CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Film_ColorPerfection_OnValidate_m931 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Gamma_6);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Film_ColorPerfection::Update()
extern TypeInfo* CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Film_ColorPerfection_Update_m932 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(119);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Gamma_6 = L_1;
		float L_2 = ((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Value2_7 = L_2;
		float L_3 = ((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Value3_8 = L_3;
		float L_4 = ((CameraFilterPack_Film_ColorPerfection_t143_StaticFields*)CameraFilterPack_Film_ColorPerfection_t143_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Film_ColorPerfection::OnDisable()
extern "C" void CameraFilterPack_Film_ColorPerfection_OnDisable_m933 (CameraFilterPack_Film_ColorPerfection_t143 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Film_Grain
#include "AssemblyU2DCSharp_CameraFilterPack_Film_Grain.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Film_Grain
#include "AssemblyU2DCSharp_CameraFilterPack_Film_GrainMethodDeclarations.h"



// System.Void CameraFilterPack_Film_Grain::.ctor()
extern "C" void CameraFilterPack_Film_Grain__ctor_m934 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (32.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Film_Grain::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Film_Grain_get_material_m935 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Film_Grain::Start()
extern TypeInfo* CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral210;
extern "C" void CameraFilterPack_Film_Grain_Start_m936 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(120);
		_stringLiteral210 = il2cpp_codegen_string_literal_from_index(210);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Film_Grain_t144_StaticFields*)CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral210, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Film_Grain::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Film_Grain_OnRenderImage_m937 (CameraFilterPack_Film_Grain_t144 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Film_Grain_get_material_m935(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Film_Grain_get_material_m935(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Film_Grain_get_material_m935(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_Film_Grain_get_material_m935(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_Film_Grain::OnValidate()
extern TypeInfo* CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Film_Grain_OnValidate_m938 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(120);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Film_Grain_t144_StaticFields*)CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Film_Grain::Update()
extern TypeInfo* CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Film_Grain_Update_m939 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(120);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Film_Grain_t144_StaticFields*)CameraFilterPack_Film_Grain_t144_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Film_Grain::OnDisable()
extern "C" void CameraFilterPack_Film_Grain_OnDisable_m940 (CameraFilterPack_Film_Grain_t144 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_Ansi
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Ansi.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_Ansi
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_AnsiMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_Ansi::.ctor()
extern Il2CppCodeGenString* _stringLiteral211;
extern "C" void CameraFilterPack_Gradients_Ansi__ctor_m941 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral211 = il2cpp_codegen_string_literal_from_index(211);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral211;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_Ansi::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_Ansi_get_material_m942 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_Ansi::Start()
extern "C" void CameraFilterPack_Gradients_Ansi_Start_m943 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Ansi::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_Ansi_OnRenderImage_m944 (CameraFilterPack_Gradients_Ansi_t145 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_Ansi_get_material_m942(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_Ansi_get_material_m942(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_Ansi_get_material_m942(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_Ansi_get_material_m942(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_Ansi_get_material_m942(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Ansi::Update()
extern "C" void CameraFilterPack_Gradients_Ansi_Update_m945 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Ansi::OnDisable()
extern "C" void CameraFilterPack_Gradients_Ansi_OnDisable_m946 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_Desert
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Desert.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_Desert
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_DesertMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_Desert::.ctor()
extern Il2CppCodeGenString* _stringLiteral212;
extern "C" void CameraFilterPack_Gradients_Desert__ctor_m947 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral212 = il2cpp_codegen_string_literal_from_index(212);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral212;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_Desert::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_Desert_get_material_m948 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_Desert::Start()
extern "C" void CameraFilterPack_Gradients_Desert_Start_m949 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Desert::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_Desert_OnRenderImage_m950 (CameraFilterPack_Gradients_Desert_t146 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_Desert_get_material_m948(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_Desert_get_material_m948(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_Desert_get_material_m948(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_Desert_get_material_m948(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_Desert_get_material_m948(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Desert::Update()
extern "C" void CameraFilterPack_Gradients_Desert_Update_m951 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Desert::OnDisable()
extern "C" void CameraFilterPack_Gradients_Desert_OnDisable_m952 (CameraFilterPack_Gradients_Desert_t146 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_ElectricGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_ElectricGradien.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_ElectricGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_ElectricGradienMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_ElectricGradient::.ctor()
extern Il2CppCodeGenString* _stringLiteral213;
extern "C" void CameraFilterPack_Gradients_ElectricGradient__ctor_m953 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral213 = il2cpp_codegen_string_literal_from_index(213);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral213;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_ElectricGradient::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_ElectricGradient_get_material_m954 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_ElectricGradient::Start()
extern "C" void CameraFilterPack_Gradients_ElectricGradient_Start_m955 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_ElectricGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_ElectricGradient_OnRenderImage_m956 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_ElectricGradient_get_material_m954(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_ElectricGradient_get_material_m954(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_ElectricGradient_get_material_m954(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_ElectricGradient_get_material_m954(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_ElectricGradient_get_material_m954(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_ElectricGradient::Update()
extern "C" void CameraFilterPack_Gradients_ElectricGradient_Update_m957 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_ElectricGradient::OnDisable()
extern "C" void CameraFilterPack_Gradients_ElectricGradient_OnDisable_m958 (CameraFilterPack_Gradients_ElectricGradient_t147 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_FireGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_FireGradient.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_FireGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_FireGradientMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_FireGradient::.ctor()
extern Il2CppCodeGenString* _stringLiteral214;
extern "C" void CameraFilterPack_Gradients_FireGradient__ctor_m959 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral214 = il2cpp_codegen_string_literal_from_index(214);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral214;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_FireGradient::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_FireGradient_get_material_m960 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_FireGradient::Start()
extern "C" void CameraFilterPack_Gradients_FireGradient_Start_m961 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_FireGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_FireGradient_OnRenderImage_m962 (CameraFilterPack_Gradients_FireGradient_t148 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_FireGradient_get_material_m960(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_FireGradient_get_material_m960(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_FireGradient_get_material_m960(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_FireGradient_get_material_m960(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_FireGradient_get_material_m960(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_FireGradient::Update()
extern "C" void CameraFilterPack_Gradients_FireGradient_Update_m963 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_FireGradient::OnDisable()
extern "C" void CameraFilterPack_Gradients_FireGradient_OnDisable_m964 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_Hue
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Hue.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_Hue
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_HueMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_Hue::.ctor()
extern Il2CppCodeGenString* _stringLiteral215;
extern "C" void CameraFilterPack_Gradients_Hue__ctor_m965 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral215 = il2cpp_codegen_string_literal_from_index(215);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral215;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_Hue::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_Hue_get_material_m966 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_Hue::Start()
extern "C" void CameraFilterPack_Gradients_Hue_Start_m967 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Hue::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_Hue_OnRenderImage_m968 (CameraFilterPack_Gradients_Hue_t149 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_Hue_get_material_m966(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_Hue_get_material_m966(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_Hue_get_material_m966(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_Hue_get_material_m966(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_Hue_get_material_m966(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Hue::Update()
extern "C" void CameraFilterPack_Gradients_Hue_Update_m969 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Hue::OnDisable()
extern "C" void CameraFilterPack_Gradients_Hue_OnDisable_m970 (CameraFilterPack_Gradients_Hue_t149 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_NeonGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_NeonGradient.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_NeonGradient
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_NeonGradientMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_NeonGradient::.ctor()
extern Il2CppCodeGenString* _stringLiteral216;
extern "C" void CameraFilterPack_Gradients_NeonGradient__ctor_m971 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral216 = il2cpp_codegen_string_literal_from_index(216);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral216;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_NeonGradient::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_NeonGradient_get_material_m972 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_NeonGradient::Start()
extern "C" void CameraFilterPack_Gradients_NeonGradient_Start_m973 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_NeonGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_NeonGradient_OnRenderImage_m974 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_NeonGradient_get_material_m972(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_NeonGradient_get_material_m972(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_NeonGradient_get_material_m972(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_NeonGradient_get_material_m972(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_NeonGradient_get_material_m972(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_NeonGradient::Update()
extern "C" void CameraFilterPack_Gradients_NeonGradient_Update_m975 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_NeonGradient::OnDisable()
extern "C" void CameraFilterPack_Gradients_NeonGradient_OnDisable_m976 (CameraFilterPack_Gradients_NeonGradient_t150 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Rainbow.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_RainbowMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_Rainbow::.ctor()
extern Il2CppCodeGenString* _stringLiteral217;
extern "C" void CameraFilterPack_Gradients_Rainbow__ctor_m977 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral217 = il2cpp_codegen_string_literal_from_index(217);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral217;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_Rainbow::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_Rainbow_get_material_m978 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_Rainbow::Start()
extern "C" void CameraFilterPack_Gradients_Rainbow_Start_m979 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Rainbow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_Rainbow_OnRenderImage_m980 (CameraFilterPack_Gradients_Rainbow_t151 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_Rainbow_get_material_m978(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_Rainbow_get_material_m978(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_Rainbow_get_material_m978(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_Rainbow_get_material_m978(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_Rainbow_get_material_m978(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Rainbow::Update()
extern "C" void CameraFilterPack_Gradients_Rainbow_Update_m981 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Rainbow::OnDisable()
extern "C" void CameraFilterPack_Gradients_Rainbow_OnDisable_m982 (CameraFilterPack_Gradients_Rainbow_t151 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_Stripe
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Stripe.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_Stripe
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_StripeMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_Stripe::.ctor()
extern Il2CppCodeGenString* _stringLiteral218;
extern "C" void CameraFilterPack_Gradients_Stripe__ctor_m983 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral218 = il2cpp_codegen_string_literal_from_index(218);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral218;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_Stripe::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_Stripe_get_material_m984 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_Stripe::Start()
extern "C" void CameraFilterPack_Gradients_Stripe_Start_m985 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Stripe::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_Stripe_OnRenderImage_m986 (CameraFilterPack_Gradients_Stripe_t152 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_Stripe_get_material_m984(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_Stripe_get_material_m984(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_Stripe_get_material_m984(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_Stripe_get_material_m984(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_Stripe_get_material_m984(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Stripe::Update()
extern "C" void CameraFilterPack_Gradients_Stripe_Update_m987 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Stripe::OnDisable()
extern "C" void CameraFilterPack_Gradients_Stripe_OnDisable_m988 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_Tech
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Tech.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_Tech
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_TechMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_Tech::.ctor()
extern Il2CppCodeGenString* _stringLiteral219;
extern "C" void CameraFilterPack_Gradients_Tech__ctor_m989 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral219 = il2cpp_codegen_string_literal_from_index(219);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral219;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_Tech::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_Tech_get_material_m990 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_Tech::Start()
extern "C" void CameraFilterPack_Gradients_Tech_Start_m991 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Tech::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_Tech_OnRenderImage_m992 (CameraFilterPack_Gradients_Tech_t153 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_Tech_get_material_m990(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_Tech_get_material_m990(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_Tech_get_material_m990(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_Tech_get_material_m990(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_Tech_get_material_m990(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Tech::Update()
extern "C" void CameraFilterPack_Gradients_Tech_Update_m993 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Tech::OnDisable()
extern "C" void CameraFilterPack_Gradients_Tech_OnDisable_m994 (CameraFilterPack_Gradients_Tech_t153 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Gradients_Therma
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_Therma.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Gradients_Therma
#include "AssemblyU2DCSharp_CameraFilterPack_Gradients_ThermaMethodDeclarations.h"



// System.Void CameraFilterPack_Gradients_Therma::.ctor()
extern Il2CppCodeGenString* _stringLiteral220;
extern "C" void CameraFilterPack_Gradients_Therma__ctor_m995 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral220 = il2cpp_codegen_string_literal_from_index(220);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_3 = _stringLiteral220;
		__this->___TimeX_4 = (1.0f);
		__this->___Switch_7 = (1.0f);
		__this->___Fade_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Gradients_Therma::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Gradients_Therma_get_material_m996 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Gradients_Therma::Start()
extern "C" void CameraFilterPack_Gradients_Therma_Start_m997 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___ShaderName_3);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0023:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Therma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Gradients_Therma_OnRenderImage_m998 (CameraFilterPack_Gradients_Therma_t154 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00bf;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Gradients_Therma_get_material_m996(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Gradients_Therma_get_material_m996(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Switch_7);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Gradients_Therma_get_material_m996(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Fade_8);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Gradients_Therma_get_material_m996(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_Gradients_Therma_get_material_m996(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c6;
	}

IL_00bf:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Therma::Update()
extern "C" void CameraFilterPack_Gradients_Therma_Update_m999 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Gradients_Therma::OnDisable()
extern "C" void CameraFilterPack_Gradients_Therma_OnDisable_m1000 (CameraFilterPack_Gradients_Therma_t154 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Light_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Light_Rainbow
#include "AssemblyU2DCSharp_CameraFilterPack_Light_RainbowMethodDeclarations.h"



// System.Void CameraFilterPack_Light_Rainbow::.ctor()
extern "C" void CameraFilterPack_Light_Rainbow__ctor_m1001 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (1.5f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Light_Rainbow::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Light_Rainbow_get_material_m1002 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Light_Rainbow::Start()
extern TypeInfo* CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral221;
extern "C" void CameraFilterPack_Light_Rainbow_Start_m1003 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(121);
		_stringLiteral221 = il2cpp_codegen_string_literal_from_index(221);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Light_Rainbow_t155_StaticFields*)CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral221, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Light_Rainbow_OnRenderImage_m1004 (CameraFilterPack_Light_Rainbow_t155 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Light_Rainbow_get_material_m1002(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Light_Rainbow_get_material_m1002(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Light_Rainbow_get_material_m1002(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_Light_Rainbow_get_material_m1002(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow::OnValidate()
extern TypeInfo* CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Rainbow_OnValidate_m1005 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(121);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Light_Rainbow_t155_StaticFields*)CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow::Update()
extern TypeInfo* CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Rainbow_Update_m1006 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(121);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Light_Rainbow_t155_StaticFields*)CameraFilterPack_Light_Rainbow_t155_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow::OnDisable()
extern "C" void CameraFilterPack_Light_Rainbow_OnDisable_m1007 (CameraFilterPack_Light_Rainbow_t155 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Light_Rainbow2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Light_Rainbow2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Rainbow2MethodDeclarations.h"



// System.Void CameraFilterPack_Light_Rainbow2::.ctor()
extern "C" void CameraFilterPack_Light_Rainbow2__ctor_m1008 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (1.5f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Light_Rainbow2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Light_Rainbow2_get_material_m1009 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Light_Rainbow2::Start()
extern TypeInfo* CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral222;
extern "C" void CameraFilterPack_Light_Rainbow2_Start_m1010 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		_stringLiteral222 = il2cpp_codegen_string_literal_from_index(222);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Light_Rainbow2_t156_StaticFields*)CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral222, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Light_Rainbow2_OnRenderImage_m1011 (CameraFilterPack_Light_Rainbow2_t156 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Light_Rainbow2_get_material_m1009(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Light_Rainbow2_get_material_m1009(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Light_Rainbow2_get_material_m1009(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_Light_Rainbow2_get_material_m1009(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow2::OnValidate()
extern TypeInfo* CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Rainbow2_OnValidate_m1012 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Light_Rainbow2_t156_StaticFields*)CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow2::Update()
extern TypeInfo* CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Rainbow2_Update_m1013 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(122);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Light_Rainbow2_t156_StaticFields*)CameraFilterPack_Light_Rainbow2_t156_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Rainbow2::OnDisable()
extern "C" void CameraFilterPack_Light_Rainbow2_OnDisable_m1014 (CameraFilterPack_Light_Rainbow2_t156 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Light_Water
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Light_Water
#include "AssemblyU2DCSharp_CameraFilterPack_Light_WaterMethodDeclarations.h"



// System.Void CameraFilterPack_Light_Water::.ctor()
extern "C" void CameraFilterPack_Light_Water__ctor_m1015 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (4.0f);
		__this->___Alpha_7 = (0.07f);
		__this->___Distance_8 = (10.0f);
		__this->___Speed_9 = (0.4f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Light_Water::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Light_Water_get_material_m1016 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Light_Water::Start()
extern TypeInfo* CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral223;
extern "C" void CameraFilterPack_Light_Water_Start_m1017 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		_stringLiteral223 = il2cpp_codegen_string_literal_from_index(223);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Alpha_7);
		((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeAlpha_10 = L_0;
		float L_1 = (__this->___Distance_8);
		((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeDistance_11 = L_1;
		float L_2 = (__this->___Size_6);
		((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeSize_12 = L_2;
		Shader_t1 * L_3 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral223, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_3;
		bool L_4 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Water::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral65;
extern Il2CppCodeGenString* _stringLiteral70;
extern Il2CppCodeGenString* _stringLiteral73;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Light_Water_OnRenderImage_m1018 (CameraFilterPack_Light_Water_t157 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral65 = il2cpp_codegen_string_literal_from_index(65);
		_stringLiteral70 = il2cpp_codegen_string_literal_from_index(70);
		_stringLiteral73 = il2cpp_codegen_string_literal_from_index(73);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00dc;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = (__this->___Speed_9);
		__this->___TimeX_3 = ((float)((float)L_2+(float)((float)((float)L_3*(float)L_4))));
		float L_5 = (__this->___TimeX_3);
		if ((!(((float)L_5) > ((float)(100.0f)))))
		{
			goto IL_0045;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_0045:
	{
		Material_t2 * L_6 = CameraFilterPack_Light_Water_get_material_m1016(__this, /*hidden argument*/NULL);
		float L_7 = (__this->___TimeX_3);
		NullCheck(L_6);
		Material_SetFloat_m2724(L_6, _stringLiteral1, L_7, /*hidden argument*/NULL);
		Material_t2 * L_8 = CameraFilterPack_Light_Water_get_material_m1016(__this, /*hidden argument*/NULL);
		float L_9 = (__this->___Alpha_7);
		NullCheck(L_8);
		Material_SetFloat_m2724(L_8, _stringLiteral65, L_9, /*hidden argument*/NULL);
		Material_t2 * L_10 = CameraFilterPack_Light_Water_get_material_m1016(__this, /*hidden argument*/NULL);
		float L_11 = (__this->___Distance_8);
		NullCheck(L_10);
		Material_SetFloat_m2724(L_10, _stringLiteral70, L_11, /*hidden argument*/NULL);
		Material_t2 * L_12 = CameraFilterPack_Light_Water_get_material_m1016(__this, /*hidden argument*/NULL);
		float L_13 = (__this->___Size_6);
		NullCheck(L_12);
		Material_SetFloat_m2724(L_12, _stringLiteral73, L_13, /*hidden argument*/NULL);
		Material_t2 * L_14 = CameraFilterPack_Light_Water_get_material_m1016(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		NullCheck(L_15);
		int32_t L_16 = RenderTexture_get_width_m2726(L_15, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		NullCheck(L_17);
		int32_t L_18 = RenderTexture_get_height_m2727(L_17, /*hidden argument*/NULL);
		Vector4_t5  L_19 = {0};
		Vector4__ctor_m2728(&L_19, (((float)L_16)), (((float)L_18)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Material_SetVector_m2729(L_14, _stringLiteral11, L_19, /*hidden argument*/NULL);
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Material_t2 * L_22 = CameraFilterPack_Light_Water_get_material_m1016(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		goto IL_00e3;
	}

IL_00dc:
	{
		RenderTexture_t15 * L_23 = ___sourceTexture;
		RenderTexture_t15 * L_24 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Water::OnValidate()
extern TypeInfo* CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Water_OnValidate_m1019 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Alpha_7);
		((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeAlpha_10 = L_0;
		float L_1 = (__this->___Distance_8);
		((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeDistance_11 = L_1;
		float L_2 = (__this->___Size_6);
		((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeSize_12 = L_2;
		return;
	}
}
// System.Void CameraFilterPack_Light_Water::Update()
extern TypeInfo* CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Water_Update_m1020 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(123);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeAlpha_10;
		__this->___Alpha_7 = L_1;
		float L_2 = ((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeDistance_11;
		__this->___Distance_8 = L_2;
		float L_3 = ((CameraFilterPack_Light_Water_t157_StaticFields*)CameraFilterPack_Light_Water_t157_il2cpp_TypeInfo_var->static_fields)->___ChangeSize_12;
		__this->___Size_6 = L_3;
	}

IL_002b:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Water::OnDisable()
extern "C" void CameraFilterPack_Light_Water_OnDisable_m1021 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Light_Water2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Light_Water2
#include "AssemblyU2DCSharp_CameraFilterPack_Light_Water2MethodDeclarations.h"



// System.Void CameraFilterPack_Light_Water2::.ctor()
extern "C" void CameraFilterPack_Light_Water2__ctor_m1022 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Speed_6 = (0.2f);
		__this->___Speed_X_7 = (0.2f);
		__this->___Speed_Y_8 = (0.3f);
		__this->___Intensity_9 = (2.4f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Light_Water2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Light_Water2_get_material_m1023 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Light_Water2::Start()
extern TypeInfo* CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral224;
extern "C" void CameraFilterPack_Light_Water2_Start_m1024 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(124);
		_stringLiteral224 = il2cpp_codegen_string_literal_from_index(224);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Speed_6);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Speed_X_7);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Speed_Y_8);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Intensity_9);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral224, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Water2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Light_Water2_OnRenderImage_m1025 (CameraFilterPack_Light_Water2_t158 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Light_Water2_get_material_m1023(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Light_Water2_get_material_m1023(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Speed_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Light_Water2_get_material_m1023(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Speed_X_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Light_Water2_get_material_m1023(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Speed_Y_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Light_Water2_get_material_m1023(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Intensity_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Light_Water2_get_material_m1023(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Light_Water2_get_material_m1023(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Water2::OnValidate()
extern TypeInfo* CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Water2_OnValidate_m1026 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(124);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Speed_6);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Speed_X_7);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Speed_Y_8);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Intensity_9);
		((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Light_Water2::Update()
extern TypeInfo* CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Light_Water2_Update_m1027 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(124);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Speed_6 = L_1;
		float L_2 = ((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Speed_X_7 = L_2;
		float L_3 = ((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Speed_Y_8 = L_3;
		float L_4 = ((CameraFilterPack_Light_Water2_t158_StaticFields*)CameraFilterPack_Light_Water2_t158_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Intensity_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Light_Water2::OnDisable()
extern "C" void CameraFilterPack_Light_Water2_OnDisable_m1028 (CameraFilterPack_Light_Water2_t158 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_NightVisionFX/preset
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX_preset.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_NightVisionFX/preset
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX_presetMethodDeclarations.h"



// CameraFilterPack_NightVisionFX
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_NightVisionFX
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFXMethodDeclarations.h"

#include "mscorlib_ArrayTypes.h"
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E.h"
// <PrivateImplementationDetails>/$ArrayType$48
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3E_U24Arra.h"
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// <PrivateImplementationDetails>
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"


// System.Void CameraFilterPack_NightVisionFX::.ctor()
extern "C" void CameraFilterPack_NightVisionFX__ctor_m1029 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_5 = (1.0f);
		__this->___Greenness_8 = (1.0f);
		__this->___Vignette_9 = (1.0f);
		__this->___Vignette_Alpha_10 = (1.0f);
		__this->___Distortion_11 = (1.0f);
		__this->___Noise_12 = (1.0f);
		__this->___Intensity_13 = (1.0f);
		__this->___Light_14 = (1.0f);
		__this->___Light2_15 = (1.0f);
		__this->___Line_16 = (1.0f);
		__this->____Binocular_Size_20 = (0.499f);
		__this->____Binocular_Smooth_21 = (0.113f);
		__this->____Binocular_Dist_22 = (0.286f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_NightVisionFX::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_NightVisionFX_get_material_m1030 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_7 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_7);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_7);
		return L_5;
	}
}
// System.Void CameraFilterPack_NightVisionFX::Start()
extern Il2CppCodeGenString* _stringLiteral225;
extern "C" void CameraFilterPack_NightVisionFX_Start_m1031 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral225 = il2cpp_codegen_string_literal_from_index(225);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral225, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_NightVisionFX::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral226;
extern Il2CppCodeGenString* _stringLiteral227;
extern Il2CppCodeGenString* _stringLiteral228;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral229;
extern Il2CppCodeGenString* _stringLiteral230;
extern Il2CppCodeGenString* _stringLiteral231;
extern Il2CppCodeGenString* _stringLiteral232;
extern Il2CppCodeGenString* _stringLiteral233;
extern Il2CppCodeGenString* _stringLiteral234;
extern Il2CppCodeGenString* _stringLiteral235;
extern Il2CppCodeGenString* _stringLiteral73;
extern Il2CppCodeGenString* _stringLiteral236;
extern Il2CppCodeGenString* _stringLiteral237;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_NightVisionFX_OnRenderImage_m1032 (CameraFilterPack_NightVisionFX_t160 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral226 = il2cpp_codegen_string_literal_from_index(226);
		_stringLiteral227 = il2cpp_codegen_string_literal_from_index(227);
		_stringLiteral228 = il2cpp_codegen_string_literal_from_index(228);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral93 = il2cpp_codegen_string_literal_from_index(93);
		_stringLiteral229 = il2cpp_codegen_string_literal_from_index(229);
		_stringLiteral230 = il2cpp_codegen_string_literal_from_index(230);
		_stringLiteral231 = il2cpp_codegen_string_literal_from_index(231);
		_stringLiteral232 = il2cpp_codegen_string_literal_from_index(232);
		_stringLiteral233 = il2cpp_codegen_string_literal_from_index(233);
		_stringLiteral234 = il2cpp_codegen_string_literal_from_index(234);
		_stringLiteral235 = il2cpp_codegen_string_literal_from_index(235);
		_stringLiteral73 = il2cpp_codegen_string_literal_from_index(73);
		_stringLiteral236 = il2cpp_codegen_string_literal_from_index(236);
		_stringLiteral237 = il2cpp_codegen_string_literal_from_index(237);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_01d6;
		}
	}
	{
		float L_2 = (__this->___TimeX_5);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_5 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_5);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_5 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_5);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Greenness_8);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral226, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Vignette_9);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral227, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Vignette_Alpha_10);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral228, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Distortion_11);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral16, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_16 = (__this->___Noise_12);
		NullCheck(L_15);
		Material_SetFloat_m2724(L_15, _stringLiteral93, L_16, /*hidden argument*/NULL);
		Material_t2 * L_17 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_18 = (__this->___Intensity_13);
		NullCheck(L_17);
		Material_SetFloat_m2724(L_17, _stringLiteral229, L_18, /*hidden argument*/NULL);
		Material_t2 * L_19 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_20 = (__this->___Light_14);
		NullCheck(L_19);
		Material_SetFloat_m2724(L_19, _stringLiteral230, L_20, /*hidden argument*/NULL);
		Material_t2 * L_21 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_22 = (__this->___Light2_15);
		NullCheck(L_21);
		Material_SetFloat_m2724(L_21, _stringLiteral231, L_22, /*hidden argument*/NULL);
		Material_t2 * L_23 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_24 = (__this->___Line_16);
		NullCheck(L_23);
		Material_SetFloat_m2724(L_23, _stringLiteral232, L_24, /*hidden argument*/NULL);
		Material_t2 * L_25 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_26 = (__this->___Color_R_17);
		NullCheck(L_25);
		Material_SetFloat_m2724(L_25, _stringLiteral233, L_26, /*hidden argument*/NULL);
		Material_t2 * L_27 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_28 = (__this->___Color_G_18);
		NullCheck(L_27);
		Material_SetFloat_m2724(L_27, _stringLiteral234, L_28, /*hidden argument*/NULL);
		Material_t2 * L_29 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_30 = (__this->___Color_B_19);
		NullCheck(L_29);
		Material_SetFloat_m2724(L_29, _stringLiteral235, L_30, /*hidden argument*/NULL);
		Material_t2 * L_31 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_32 = (__this->____Binocular_Size_20);
		NullCheck(L_31);
		Material_SetFloat_m2724(L_31, _stringLiteral73, L_32, /*hidden argument*/NULL);
		Material_t2 * L_33 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_34 = (__this->____Binocular_Dist_22);
		NullCheck(L_33);
		Material_SetFloat_m2724(L_33, _stringLiteral236, L_34, /*hidden argument*/NULL);
		Material_t2 * L_35 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		float L_36 = (__this->____Binocular_Smooth_21);
		NullCheck(L_35);
		Material_SetFloat_m2724(L_35, _stringLiteral237, L_36, /*hidden argument*/NULL);
		Material_t2 * L_37 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		int32_t L_38 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_39 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_40 = {0};
		Vector2__ctor_m2714(&L_40, (((float)L_38)), (((float)L_39)), /*hidden argument*/NULL);
		Vector4_t5  L_41 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		NullCheck(L_37);
		Material_SetVector_m2729(L_37, _stringLiteral11, L_41, /*hidden argument*/NULL);
		RenderTexture_t15 * L_42 = ___sourceTexture;
		RenderTexture_t15 * L_43 = ___destTexture;
		Material_t2 * L_44 = CameraFilterPack_NightVisionFX_get_material_m1030(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_42, L_43, L_44, /*hidden argument*/NULL);
		goto IL_01dd;
	}

IL_01d6:
	{
		RenderTexture_t15 * L_45 = ___sourceTexture;
		RenderTexture_t15 * L_46 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_45, L_46, /*hidden argument*/NULL);
	}

IL_01dd:
	{
		return;
	}
}
// System.Void CameraFilterPack_NightVisionFX::Update()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D28_28_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D29_29_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D30_30_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D31_31_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D32_32_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D33_33_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D34_34_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D35_35_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D36_36_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D37_37_FieldInfo_var;
extern "C" void CameraFilterPack_NightVisionFX_Update_m1033 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D28_28_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 28);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D29_29_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 29);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D30_30_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 30);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D31_31_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 31);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D32_32_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 32);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D33_33_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 33);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D34_34_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 34);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D35_35_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 35);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D36_36_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 36);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D37_37_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 37);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t72* V_0 = {0};
	SingleU5BU5D_t72* V_1 = {0};
	SingleU5BU5D_t72* V_2 = {0};
	SingleU5BU5D_t72* V_3 = {0};
	SingleU5BU5D_t72* V_4 = {0};
	SingleU5BU5D_t72* V_5 = {0};
	SingleU5BU5D_t72* V_6 = {0};
	SingleU5BU5D_t72* V_7 = {0};
	SingleU5BU5D_t72* V_8 = {0};
	SingleU5BU5D_t72* V_9 = {0};
	SingleU5BU5D_t72* V_10 = {0};
	{
		int32_t L_0 = (__this->___PresetMemo_4);
		int32_t L_1 = (__this->___Preset_3);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_020d;
		}
	}
	{
		int32_t L_2 = (__this->___Preset_3);
		__this->___PresetMemo_4 = L_2;
		SingleU5BU5D_t72* L_3 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D28_28_FieldInfo_var), /*hidden argument*/NULL);
		V_0 = L_3;
		SingleU5BU5D_t72* L_4 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D29_29_FieldInfo_var), /*hidden argument*/NULL);
		V_1 = L_4;
		SingleU5BU5D_t72* L_5 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D30_30_FieldInfo_var), /*hidden argument*/NULL);
		V_2 = L_5;
		SingleU5BU5D_t72* L_6 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_6, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D31_31_FieldInfo_var), /*hidden argument*/NULL);
		V_3 = L_6;
		SingleU5BU5D_t72* L_7 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_7, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D32_32_FieldInfo_var), /*hidden argument*/NULL);
		V_4 = L_7;
		SingleU5BU5D_t72* L_8 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_8, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D33_33_FieldInfo_var), /*hidden argument*/NULL);
		V_5 = L_8;
		SingleU5BU5D_t72* L_9 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_9, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D34_34_FieldInfo_var), /*hidden argument*/NULL);
		V_6 = L_9;
		SingleU5BU5D_t72* L_10 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_10, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D35_35_FieldInfo_var), /*hidden argument*/NULL);
		V_7 = L_10;
		SingleU5BU5D_t72* L_11 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_11, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D36_36_FieldInfo_var), /*hidden argument*/NULL);
		V_8 = L_11;
		SingleU5BU5D_t72* L_12 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_12, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D37_37_FieldInfo_var), /*hidden argument*/NULL);
		V_9 = L_12;
		V_10 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		int32_t L_13 = (__this->___Preset_3);
		if (L_13)
		{
			goto IL_00f8;
		}
	}
	{
		SingleU5BU5D_t72* L_14 = V_0;
		V_10 = L_14;
	}

IL_00f8:
	{
		int32_t L_15 = (__this->___Preset_3);
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_0107;
		}
	}
	{
		SingleU5BU5D_t72* L_16 = V_1;
		V_10 = L_16;
	}

IL_0107:
	{
		int32_t L_17 = (__this->___Preset_3);
		if ((!(((uint32_t)L_17) == ((uint32_t)2))))
		{
			goto IL_0116;
		}
	}
	{
		SingleU5BU5D_t72* L_18 = V_2;
		V_10 = L_18;
	}

IL_0116:
	{
		int32_t L_19 = (__this->___Preset_3);
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_0125;
		}
	}
	{
		SingleU5BU5D_t72* L_20 = V_3;
		V_10 = L_20;
	}

IL_0125:
	{
		int32_t L_21 = (__this->___Preset_3);
		if ((!(((uint32_t)L_21) == ((uint32_t)4))))
		{
			goto IL_0135;
		}
	}
	{
		SingleU5BU5D_t72* L_22 = V_4;
		V_10 = L_22;
	}

IL_0135:
	{
		int32_t L_23 = (__this->___Preset_3);
		if ((!(((uint32_t)L_23) == ((uint32_t)5))))
		{
			goto IL_0145;
		}
	}
	{
		SingleU5BU5D_t72* L_24 = V_5;
		V_10 = L_24;
	}

IL_0145:
	{
		int32_t L_25 = (__this->___Preset_3);
		if ((!(((uint32_t)L_25) == ((uint32_t)6))))
		{
			goto IL_0155;
		}
	}
	{
		SingleU5BU5D_t72* L_26 = V_6;
		V_10 = L_26;
	}

IL_0155:
	{
		int32_t L_27 = (__this->___Preset_3);
		if ((!(((uint32_t)L_27) == ((uint32_t)7))))
		{
			goto IL_0165;
		}
	}
	{
		SingleU5BU5D_t72* L_28 = V_7;
		V_10 = L_28;
	}

IL_0165:
	{
		int32_t L_29 = (__this->___Preset_3);
		if ((!(((uint32_t)L_29) == ((uint32_t)8))))
		{
			goto IL_0175;
		}
	}
	{
		SingleU5BU5D_t72* L_30 = V_8;
		V_10 = L_30;
	}

IL_0175:
	{
		int32_t L_31 = (__this->___Preset_3);
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_0186;
		}
	}
	{
		SingleU5BU5D_t72* L_32 = V_9;
		V_10 = L_32;
	}

IL_0186:
	{
		int32_t L_33 = (__this->___Preset_3);
		if ((((int32_t)L_33) == ((int32_t)(-1))))
		{
			goto IL_020d;
		}
	}
	{
		SingleU5BU5D_t72* L_34 = V_10;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		int32_t L_35 = 0;
		__this->___Greenness_8 = (*(float*)(float*)SZArrayLdElema(L_34, L_35));
		SingleU5BU5D_t72* L_36 = V_10;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 1);
		int32_t L_37 = 1;
		__this->___Vignette_9 = (*(float*)(float*)SZArrayLdElema(L_36, L_37));
		SingleU5BU5D_t72* L_38 = V_10;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 2);
		int32_t L_39 = 2;
		__this->___Vignette_Alpha_10 = (*(float*)(float*)SZArrayLdElema(L_38, L_39));
		SingleU5BU5D_t72* L_40 = V_10;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 3);
		int32_t L_41 = 3;
		__this->___Distortion_11 = (*(float*)(float*)SZArrayLdElema(L_40, L_41));
		SingleU5BU5D_t72* L_42 = V_10;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 4);
		int32_t L_43 = 4;
		__this->___Noise_12 = (*(float*)(float*)SZArrayLdElema(L_42, L_43));
		SingleU5BU5D_t72* L_44 = V_10;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, 5);
		int32_t L_45 = 5;
		__this->___Intensity_13 = (*(float*)(float*)SZArrayLdElema(L_44, L_45));
		SingleU5BU5D_t72* L_46 = V_10;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 6);
		int32_t L_47 = 6;
		__this->___Light_14 = (*(float*)(float*)SZArrayLdElema(L_46, L_47));
		SingleU5BU5D_t72* L_48 = V_10;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 7);
		int32_t L_49 = 7;
		__this->___Light2_15 = (*(float*)(float*)SZArrayLdElema(L_48, L_49));
		SingleU5BU5D_t72* L_50 = V_10;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 8);
		int32_t L_51 = 8;
		__this->___Line_16 = (*(float*)(float*)SZArrayLdElema(L_50, L_51));
		SingleU5BU5D_t72* L_52 = V_10;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)9));
		int32_t L_53 = ((int32_t)9);
		__this->___Color_R_17 = (*(float*)(float*)SZArrayLdElema(L_52, L_53));
		SingleU5BU5D_t72* L_54 = V_10;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)10));
		int32_t L_55 = ((int32_t)10);
		__this->___Color_G_18 = (*(float*)(float*)SZArrayLdElema(L_54, L_55));
		SingleU5BU5D_t72* L_56 = V_10;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, ((int32_t)11));
		int32_t L_57 = ((int32_t)11);
		__this->___Color_B_19 = (*(float*)(float*)SZArrayLdElema(L_56, L_57));
	}

IL_020d:
	{
		return;
	}
}
// System.Void CameraFilterPack_NightVisionFX::OnDisable()
extern "C" void CameraFilterPack_NightVisionFX_OnDisable_m1034 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_7);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_NightVision_4
#include "AssemblyU2DCSharp_CameraFilterPack_NightVision_4.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_NightVision_4
#include "AssemblyU2DCSharp_CameraFilterPack_NightVision_4MethodDeclarations.h"



// System.Void CameraFilterPack_NightVision_4::.ctor()
extern Il2CppCodeGenString* _stringLiteral238;
extern "C" void CameraFilterPack_NightVision_4__ctor_m1035 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral238 = il2cpp_codegen_string_literal_from_index(238);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_2 = _stringLiteral238;
		__this->___FadeFX_4 = (1.0f);
		__this->___TimeX_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_NightVision_4::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_NightVision_4_get_material_m1036 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_3);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_7 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_7);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_7);
		return L_5;
	}
}
// System.Void CameraFilterPack_NightVision_4::ChangeFilters()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D38_38_FieldInfo_var;
extern "C" void CameraFilterPack_NightVision_4_ChangeFilters_m1037 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D38_38_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 38);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleU5BU5D_t72* L_0 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D38_38_FieldInfo_var), /*hidden argument*/NULL);
		__this->___Matrix9_8 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_NightVision_4::Start()
extern "C" void CameraFilterPack_NightVision_4_Start_m1038 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method)
{
	{
		CameraFilterPack_NightVision_4_ChangeFilters_m1037(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___ShaderName_2);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_3 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		return;
	}
}
// System.Void CameraFilterPack_NightVision_4::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral100;
extern Il2CppCodeGenString* _stringLiteral101;
extern Il2CppCodeGenString* _stringLiteral102;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral104;
extern Il2CppCodeGenString* _stringLiteral105;
extern Il2CppCodeGenString* _stringLiteral106;
extern Il2CppCodeGenString* _stringLiteral107;
extern Il2CppCodeGenString* _stringLiteral108;
extern Il2CppCodeGenString* _stringLiteral109;
extern Il2CppCodeGenString* _stringLiteral110;
extern Il2CppCodeGenString* _stringLiteral111;
extern Il2CppCodeGenString* _stringLiteral113;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_NightVision_4_OnRenderImage_m1039 (CameraFilterPack_NightVision_4_t161 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral100 = il2cpp_codegen_string_literal_from_index(100);
		_stringLiteral101 = il2cpp_codegen_string_literal_from_index(101);
		_stringLiteral102 = il2cpp_codegen_string_literal_from_index(102);
		_stringLiteral103 = il2cpp_codegen_string_literal_from_index(103);
		_stringLiteral104 = il2cpp_codegen_string_literal_from_index(104);
		_stringLiteral105 = il2cpp_codegen_string_literal_from_index(105);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		_stringLiteral107 = il2cpp_codegen_string_literal_from_index(107);
		_stringLiteral108 = il2cpp_codegen_string_literal_from_index(108);
		_stringLiteral109 = il2cpp_codegen_string_literal_from_index(109);
		_stringLiteral110 = il2cpp_codegen_string_literal_from_index(110);
		_stringLiteral111 = il2cpp_codegen_string_literal_from_index(111);
		_stringLiteral113 = il2cpp_codegen_string_literal_from_index(113);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_3);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0214;
		}
	}
	{
		float L_2 = (__this->___TimeX_5);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_5 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_5);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_5 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_5);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_8 = (__this->___Matrix9_8);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral100, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_8, L_9))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_10 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_11 = (__this->___Matrix9_8);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		int32_t L_12 = 1;
		NullCheck(L_10);
		Material_SetFloat_m2724(L_10, _stringLiteral101, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_11, L_12))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_14 = (__this->___Matrix9_8);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		int32_t L_15 = 2;
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral102, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_14, L_15))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_16 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_17 = (__this->___Matrix9_8);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		int32_t L_18 = 3;
		NullCheck(L_16);
		Material_SetFloat_m2724(L_16, _stringLiteral103, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_17, L_18))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_19 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_20 = (__this->___Matrix9_8);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 4);
		int32_t L_21 = 4;
		NullCheck(L_19);
		Material_SetFloat_m2724(L_19, _stringLiteral104, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_20, L_21))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_22 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_23 = (__this->___Matrix9_8);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 5);
		int32_t L_24 = 5;
		NullCheck(L_22);
		Material_SetFloat_m2724(L_22, _stringLiteral105, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_23, L_24))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_25 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_26 = (__this->___Matrix9_8);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 6);
		int32_t L_27 = 6;
		NullCheck(L_25);
		Material_SetFloat_m2724(L_25, _stringLiteral106, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_26, L_27))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_28 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_29 = (__this->___Matrix9_8);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 7);
		int32_t L_30 = 7;
		NullCheck(L_28);
		Material_SetFloat_m2724(L_28, _stringLiteral107, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_29, L_30))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_31 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_32 = (__this->___Matrix9_8);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		int32_t L_33 = 8;
		NullCheck(L_31);
		Material_SetFloat_m2724(L_31, _stringLiteral108, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_32, L_33))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_34 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_35 = (__this->___Matrix9_8);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)9));
		int32_t L_36 = ((int32_t)9);
		NullCheck(L_34);
		Material_SetFloat_m2724(L_34, _stringLiteral109, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_35, L_36))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_37 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_38 = (__this->___Matrix9_8);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)10));
		int32_t L_39 = ((int32_t)10);
		NullCheck(L_37);
		Material_SetFloat_m2724(L_37, _stringLiteral110, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_38, L_39))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_40 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_41 = (__this->___Matrix9_8);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)11));
		int32_t L_42 = ((int32_t)11);
		NullCheck(L_40);
		Material_SetFloat_m2724(L_40, _stringLiteral111, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_41, L_42))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_43 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		float L_44 = (__this->___FadeFX_4);
		NullCheck(L_43);
		Material_SetFloat_m2724(L_43, _stringLiteral113, L_44, /*hidden argument*/NULL);
		Material_t2 * L_45 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_46 = ___sourceTexture;
		NullCheck(L_46);
		int32_t L_47 = RenderTexture_get_width_m2726(L_46, /*hidden argument*/NULL);
		RenderTexture_t15 * L_48 = ___sourceTexture;
		NullCheck(L_48);
		int32_t L_49 = RenderTexture_get_height_m2727(L_48, /*hidden argument*/NULL);
		Vector4_t5  L_50 = {0};
		Vector4__ctor_m2728(&L_50, (((float)L_47)), (((float)L_49)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		Material_SetVector_m2729(L_45, _stringLiteral11, L_50, /*hidden argument*/NULL);
		RenderTexture_t15 * L_51 = ___sourceTexture;
		RenderTexture_t15 * L_52 = ___destTexture;
		Material_t2 * L_53 = CameraFilterPack_NightVision_4_get_material_m1036(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_51, L_52, L_53, /*hidden argument*/NULL);
		goto IL_021b;
	}

IL_0214:
	{
		RenderTexture_t15 * L_54 = ___sourceTexture;
		RenderTexture_t15 * L_55 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
	}

IL_021b:
	{
		return;
	}
}
// System.Void CameraFilterPack_NightVision_4::OnValidate()
extern "C" void CameraFilterPack_NightVision_4_OnValidate_m1040 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method)
{
	{
		CameraFilterPack_NightVision_4_ChangeFilters_m1037(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFilterPack_NightVision_4::Update()
extern "C" void CameraFilterPack_NightVision_4_Update_m1041 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_NightVision_4::OnDisable()
extern "C" void CameraFilterPack_NightVision_4_OnDisable_m1042 (CameraFilterPack_NightVision_4_t161 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_7);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Oculus_NightVision1
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision1.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Oculus_NightVision1
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision1MethodDeclarations.h"



// System.Void CameraFilterPack_Oculus_NightVision1::.ctor()
extern "C" void CameraFilterPack_Oculus_NightVision1__ctor_m1043 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Distortion_4 = (1.0f);
		__this->___Vignette_7 = (1.3f);
		__this->___Linecount_8 = (90.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Oculus_NightVision1::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision1_get_material_m1044 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision1::Start()
extern TypeInfo* CameraFilterPack_Oculus_NightVision1_t162_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral239;
extern "C" void CameraFilterPack_Oculus_NightVision1_Start_m1045 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Oculus_NightVision1_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(125);
		_stringLiteral239 = il2cpp_codegen_string_literal_from_index(239);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Vignette_7);
		((CameraFilterPack_Oculus_NightVision1_t162_StaticFields*)CameraFilterPack_Oculus_NightVision1_t162_il2cpp_TypeInfo_var->static_fields)->___ChangeVignette_9 = L_0;
		float L_1 = (__this->___Linecount_8);
		((CameraFilterPack_Oculus_NightVision1_t162_StaticFields*)CameraFilterPack_Oculus_NightVision1_t162_il2cpp_TypeInfo_var->static_fields)->___ChangeLinecount_10 = L_1;
		Shader_t1 * L_2 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral239, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_2;
		bool L_3 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral227;
extern Il2CppCodeGenString* _stringLiteral240;
extern "C" void CameraFilterPack_Oculus_NightVision1_OnRenderImage_m1046 (CameraFilterPack_Oculus_NightVision1_t162 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral227 = il2cpp_codegen_string_literal_from_index(227);
		_stringLiteral240 = il2cpp_codegen_string_literal_from_index(240);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d5;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Oculus_NightVision1_get_material_m1044(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Oculus_NightVision1_get_material_m1044(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Oculus_NightVision1_get_material_m1044(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Oculus_NightVision1_get_material_m1044(__this, /*hidden argument*/NULL);
		float L_16 = (__this->___Vignette_7);
		NullCheck(L_15);
		Material_SetFloat_m2724(L_15, _stringLiteral227, L_16, /*hidden argument*/NULL);
		Material_t2 * L_17 = CameraFilterPack_Oculus_NightVision1_get_material_m1044(__this, /*hidden argument*/NULL);
		float L_18 = (__this->___Linecount_8);
		NullCheck(L_17);
		Material_SetFloat_m2724(L_17, _stringLiteral240, L_18, /*hidden argument*/NULL);
		RenderTexture_t15 * L_19 = ___sourceTexture;
		RenderTexture_t15 * L_20 = ___destTexture;
		Material_t2 * L_21 = CameraFilterPack_Oculus_NightVision1_get_material_m1044(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		goto IL_00dc;
	}

IL_00d5:
	{
		RenderTexture_t15 * L_22 = ___sourceTexture;
		RenderTexture_t15 * L_23 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision1::Update()
extern "C" void CameraFilterPack_Oculus_NightVision1_Update_m1047 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000a;
		}
	}

IL_000a:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision1::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision1_OnDisable_m1048 (CameraFilterPack_Oculus_NightVision1_t162 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Oculus_NightVision2
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Oculus_NightVision2
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision2MethodDeclarations.h"



// System.Void CameraFilterPack_Oculus_NightVision2::.ctor()
extern Il2CppCodeGenString* _stringLiteral241;
extern "C" void CameraFilterPack_Oculus_NightVision2__ctor_m1049 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral241 = il2cpp_codegen_string_literal_from_index(241);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_2 = _stringLiteral241;
		__this->___FadeFX_4 = (1.0f);
		__this->___TimeX_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Oculus_NightVision2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision2_get_material_m1050 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_3);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_7 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_7);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_7);
		return L_5;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision2::ChangeFilters()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D39_39_FieldInfo_var;
extern "C" void CameraFilterPack_Oculus_NightVision2_ChangeFilters_m1051 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D39_39_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 39);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleU5BU5D_t72* L_0 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D39_39_FieldInfo_var), /*hidden argument*/NULL);
		__this->___Matrix9_8 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision2::Start()
extern "C" void CameraFilterPack_Oculus_NightVision2_Start_m1052 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method)
{
	{
		CameraFilterPack_Oculus_NightVision2_ChangeFilters_m1051(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___ShaderName_2);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_3 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral100;
extern Il2CppCodeGenString* _stringLiteral101;
extern Il2CppCodeGenString* _stringLiteral102;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral104;
extern Il2CppCodeGenString* _stringLiteral105;
extern Il2CppCodeGenString* _stringLiteral106;
extern Il2CppCodeGenString* _stringLiteral107;
extern Il2CppCodeGenString* _stringLiteral108;
extern Il2CppCodeGenString* _stringLiteral109;
extern Il2CppCodeGenString* _stringLiteral110;
extern Il2CppCodeGenString* _stringLiteral111;
extern Il2CppCodeGenString* _stringLiteral113;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Oculus_NightVision2_OnRenderImage_m1053 (CameraFilterPack_Oculus_NightVision2_t163 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral100 = il2cpp_codegen_string_literal_from_index(100);
		_stringLiteral101 = il2cpp_codegen_string_literal_from_index(101);
		_stringLiteral102 = il2cpp_codegen_string_literal_from_index(102);
		_stringLiteral103 = il2cpp_codegen_string_literal_from_index(103);
		_stringLiteral104 = il2cpp_codegen_string_literal_from_index(104);
		_stringLiteral105 = il2cpp_codegen_string_literal_from_index(105);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		_stringLiteral107 = il2cpp_codegen_string_literal_from_index(107);
		_stringLiteral108 = il2cpp_codegen_string_literal_from_index(108);
		_stringLiteral109 = il2cpp_codegen_string_literal_from_index(109);
		_stringLiteral110 = il2cpp_codegen_string_literal_from_index(110);
		_stringLiteral111 = il2cpp_codegen_string_literal_from_index(111);
		_stringLiteral113 = il2cpp_codegen_string_literal_from_index(113);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_3);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0214;
		}
	}
	{
		float L_2 = (__this->___TimeX_5);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_5 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_5);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_5 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_5);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_8 = (__this->___Matrix9_8);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral100, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_8, L_9))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_10 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_11 = (__this->___Matrix9_8);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		int32_t L_12 = 1;
		NullCheck(L_10);
		Material_SetFloat_m2724(L_10, _stringLiteral101, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_11, L_12))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_14 = (__this->___Matrix9_8);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		int32_t L_15 = 2;
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral102, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_14, L_15))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_16 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_17 = (__this->___Matrix9_8);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		int32_t L_18 = 3;
		NullCheck(L_16);
		Material_SetFloat_m2724(L_16, _stringLiteral103, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_17, L_18))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_19 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_20 = (__this->___Matrix9_8);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 4);
		int32_t L_21 = 4;
		NullCheck(L_19);
		Material_SetFloat_m2724(L_19, _stringLiteral104, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_20, L_21))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_22 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_23 = (__this->___Matrix9_8);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 5);
		int32_t L_24 = 5;
		NullCheck(L_22);
		Material_SetFloat_m2724(L_22, _stringLiteral105, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_23, L_24))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_25 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_26 = (__this->___Matrix9_8);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 6);
		int32_t L_27 = 6;
		NullCheck(L_25);
		Material_SetFloat_m2724(L_25, _stringLiteral106, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_26, L_27))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_28 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_29 = (__this->___Matrix9_8);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 7);
		int32_t L_30 = 7;
		NullCheck(L_28);
		Material_SetFloat_m2724(L_28, _stringLiteral107, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_29, L_30))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_31 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_32 = (__this->___Matrix9_8);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		int32_t L_33 = 8;
		NullCheck(L_31);
		Material_SetFloat_m2724(L_31, _stringLiteral108, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_32, L_33))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_34 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_35 = (__this->___Matrix9_8);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)9));
		int32_t L_36 = ((int32_t)9);
		NullCheck(L_34);
		Material_SetFloat_m2724(L_34, _stringLiteral109, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_35, L_36))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_37 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_38 = (__this->___Matrix9_8);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)10));
		int32_t L_39 = ((int32_t)10);
		NullCheck(L_37);
		Material_SetFloat_m2724(L_37, _stringLiteral110, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_38, L_39))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_40 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_41 = (__this->___Matrix9_8);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)11));
		int32_t L_42 = ((int32_t)11);
		NullCheck(L_40);
		Material_SetFloat_m2724(L_40, _stringLiteral111, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_41, L_42))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_43 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		float L_44 = (__this->___FadeFX_4);
		NullCheck(L_43);
		Material_SetFloat_m2724(L_43, _stringLiteral113, L_44, /*hidden argument*/NULL);
		Material_t2 * L_45 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_46 = ___sourceTexture;
		NullCheck(L_46);
		int32_t L_47 = RenderTexture_get_width_m2726(L_46, /*hidden argument*/NULL);
		RenderTexture_t15 * L_48 = ___sourceTexture;
		NullCheck(L_48);
		int32_t L_49 = RenderTexture_get_height_m2727(L_48, /*hidden argument*/NULL);
		Vector4_t5  L_50 = {0};
		Vector4__ctor_m2728(&L_50, (((float)L_47)), (((float)L_49)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		Material_SetVector_m2729(L_45, _stringLiteral11, L_50, /*hidden argument*/NULL);
		RenderTexture_t15 * L_51 = ___sourceTexture;
		RenderTexture_t15 * L_52 = ___destTexture;
		Material_t2 * L_53 = CameraFilterPack_Oculus_NightVision2_get_material_m1050(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_51, L_52, L_53, /*hidden argument*/NULL);
		goto IL_021b;
	}

IL_0214:
	{
		RenderTexture_t15 * L_54 = ___sourceTexture;
		RenderTexture_t15 * L_55 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_54, L_55, /*hidden argument*/NULL);
	}

IL_021b:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision2::OnValidate()
extern "C" void CameraFilterPack_Oculus_NightVision2_OnValidate_m1054 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method)
{
	{
		CameraFilterPack_Oculus_NightVision2_ChangeFilters_m1051(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision2::Update()
extern "C" void CameraFilterPack_Oculus_NightVision2_Update_m1055 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision2::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision2_OnDisable_m1056 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_7);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Oculus_NightVision3
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision3.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Oculus_NightVision3
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision3MethodDeclarations.h"



// System.Void CameraFilterPack_Oculus_NightVision3::.ctor()
extern "C" void CameraFilterPack_Oculus_NightVision3__ctor_m1057 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Greenness_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Oculus_NightVision3::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision3_get_material_m1058 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision3::Start()
extern TypeInfo* CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral242;
extern "C" void CameraFilterPack_Oculus_NightVision3_Start_m1059 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(126);
		_stringLiteral242 = il2cpp_codegen_string_literal_from_index(242);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Greenness_6);
		((CameraFilterPack_Oculus_NightVision3_t164_StaticFields*)CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var->static_fields)->___ChangeGreenness_9 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral242, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision3::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral226;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Oculus_NightVision3_OnRenderImage_m1060 (CameraFilterPack_Oculus_NightVision3_t164 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral226 = il2cpp_codegen_string_literal_from_index(226);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a2;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Oculus_NightVision3_get_material_m1058(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Oculus_NightVision3_get_material_m1058(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Greenness_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral226, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Oculus_NightVision3_get_material_m1058(__this, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_12 = {0};
		Vector2__ctor_m2714(&L_12, (((float)L_10)), (((float)L_11)), /*hidden argument*/NULL);
		Vector4_t5  L_13 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_13, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		RenderTexture_t15 * L_15 = ___destTexture;
		Material_t2 * L_16 = CameraFilterPack_Oculus_NightVision3_get_material_m1058(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		goto IL_00a9;
	}

IL_00a2:
	{
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision3::OnValidate()
extern TypeInfo* CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Oculus_NightVision3_OnValidate_m1061 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(126);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Greenness_6);
		((CameraFilterPack_Oculus_NightVision3_t164_StaticFields*)CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var->static_fields)->___ChangeGreenness_9 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision3::Update()
extern TypeInfo* CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Oculus_NightVision3_Update_m1062 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(126);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Oculus_NightVision3_t164_StaticFields*)CameraFilterPack_Oculus_NightVision3_t164_il2cpp_TypeInfo_var->static_fields)->___ChangeGreenness_9;
		__this->___Greenness_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision3::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision3_OnDisable_m1063 (CameraFilterPack_Oculus_NightVision3_t164 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Oculus_NightVision5
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision5.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Oculus_NightVision5
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_NightVision5MethodDeclarations.h"



// System.Void CameraFilterPack_Oculus_NightVision5::.ctor()
extern Il2CppCodeGenString* _stringLiteral243;
extern "C" void CameraFilterPack_Oculus_NightVision5__ctor_m1064 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral243 = il2cpp_codegen_string_literal_from_index(243);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ShaderName_2 = _stringLiteral243;
		__this->___FadeFX_4 = (1.0f);
		__this->____Size_5 = (0.37f);
		__this->____Smooth_6 = (0.15f);
		__this->____Dist_7 = (0.285f);
		__this->___TimeX_8 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Oculus_NightVision5::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision5_get_material_m1065 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_10);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_3);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_10 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_10);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_10);
		return L_5;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision5::ChangeFilters()
extern TypeInfo* SingleU5BU5D_t72_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D40_40_FieldInfo_var;
extern "C" void CameraFilterPack_Oculus_NightVision5_ChangeFilters_m1066 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SingleU5BU5D_t72_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D40_40_FieldInfo_var = il2cpp_codegen_field_info_from_index(60, 40);
		s_Il2CppMethodIntialized = true;
	}
	{
		SingleU5BU5D_t72* L_0 = ((SingleU5BU5D_t72*)SZArrayNew(SingleU5BU5D_t72_il2cpp_TypeInfo_var, ((int32_t)12)));
		RuntimeHelpers_InitializeArray_m2748(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t463____U24U24fieldU2D40_40_FieldInfo_var), /*hidden argument*/NULL);
		__this->___Matrix9_11 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision5::Start()
extern "C" void CameraFilterPack_Oculus_NightVision5_Start_m1067 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method)
{
	{
		CameraFilterPack_Oculus_NightVision5_ChangeFilters_m1066(__this, /*hidden argument*/NULL);
		String_t* L_0 = (__this->___ShaderName_2);
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___SCShader_3 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0029:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision5::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral100;
extern Il2CppCodeGenString* _stringLiteral101;
extern Il2CppCodeGenString* _stringLiteral102;
extern Il2CppCodeGenString* _stringLiteral103;
extern Il2CppCodeGenString* _stringLiteral104;
extern Il2CppCodeGenString* _stringLiteral105;
extern Il2CppCodeGenString* _stringLiteral106;
extern Il2CppCodeGenString* _stringLiteral107;
extern Il2CppCodeGenString* _stringLiteral108;
extern Il2CppCodeGenString* _stringLiteral109;
extern Il2CppCodeGenString* _stringLiteral110;
extern Il2CppCodeGenString* _stringLiteral111;
extern Il2CppCodeGenString* _stringLiteral113;
extern Il2CppCodeGenString* _stringLiteral73;
extern Il2CppCodeGenString* _stringLiteral236;
extern Il2CppCodeGenString* _stringLiteral237;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Oculus_NightVision5_OnRenderImage_m1068 (CameraFilterPack_Oculus_NightVision5_t165 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral100 = il2cpp_codegen_string_literal_from_index(100);
		_stringLiteral101 = il2cpp_codegen_string_literal_from_index(101);
		_stringLiteral102 = il2cpp_codegen_string_literal_from_index(102);
		_stringLiteral103 = il2cpp_codegen_string_literal_from_index(103);
		_stringLiteral104 = il2cpp_codegen_string_literal_from_index(104);
		_stringLiteral105 = il2cpp_codegen_string_literal_from_index(105);
		_stringLiteral106 = il2cpp_codegen_string_literal_from_index(106);
		_stringLiteral107 = il2cpp_codegen_string_literal_from_index(107);
		_stringLiteral108 = il2cpp_codegen_string_literal_from_index(108);
		_stringLiteral109 = il2cpp_codegen_string_literal_from_index(109);
		_stringLiteral110 = il2cpp_codegen_string_literal_from_index(110);
		_stringLiteral111 = il2cpp_codegen_string_literal_from_index(111);
		_stringLiteral113 = il2cpp_codegen_string_literal_from_index(113);
		_stringLiteral73 = il2cpp_codegen_string_literal_from_index(73);
		_stringLiteral236 = il2cpp_codegen_string_literal_from_index(236);
		_stringLiteral237 = il2cpp_codegen_string_literal_from_index(237);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_3);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0256;
		}
	}
	{
		float L_2 = (__this->___TimeX_8);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_8 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_8);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_8 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_8);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_8 = (__this->___Matrix9_11);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral100, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_8, L_9))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_10 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_11 = (__this->___Matrix9_11);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 1);
		int32_t L_12 = 1;
		NullCheck(L_10);
		Material_SetFloat_m2724(L_10, _stringLiteral101, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_11, L_12))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_14 = (__this->___Matrix9_11);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		int32_t L_15 = 2;
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral102, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_14, L_15))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_16 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_17 = (__this->___Matrix9_11);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 3);
		int32_t L_18 = 3;
		NullCheck(L_16);
		Material_SetFloat_m2724(L_16, _stringLiteral103, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_17, L_18))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_19 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_20 = (__this->___Matrix9_11);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 4);
		int32_t L_21 = 4;
		NullCheck(L_19);
		Material_SetFloat_m2724(L_19, _stringLiteral104, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_20, L_21))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_22 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_23 = (__this->___Matrix9_11);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 5);
		int32_t L_24 = 5;
		NullCheck(L_22);
		Material_SetFloat_m2724(L_22, _stringLiteral105, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_23, L_24))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_25 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_26 = (__this->___Matrix9_11);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 6);
		int32_t L_27 = 6;
		NullCheck(L_25);
		Material_SetFloat_m2724(L_25, _stringLiteral106, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_26, L_27))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_28 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_29 = (__this->___Matrix9_11);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 7);
		int32_t L_30 = 7;
		NullCheck(L_28);
		Material_SetFloat_m2724(L_28, _stringLiteral107, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_29, L_30))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_31 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_32 = (__this->___Matrix9_11);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 8);
		int32_t L_33 = 8;
		NullCheck(L_31);
		Material_SetFloat_m2724(L_31, _stringLiteral108, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_32, L_33))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_34 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_35 = (__this->___Matrix9_11);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, ((int32_t)9));
		int32_t L_36 = ((int32_t)9);
		NullCheck(L_34);
		Material_SetFloat_m2724(L_34, _stringLiteral109, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_35, L_36))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_37 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_38 = (__this->___Matrix9_11);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, ((int32_t)10));
		int32_t L_39 = ((int32_t)10);
		NullCheck(L_37);
		Material_SetFloat_m2724(L_37, _stringLiteral110, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_38, L_39))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_40 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t72* L_41 = (__this->___Matrix9_11);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)11));
		int32_t L_42 = ((int32_t)11);
		NullCheck(L_40);
		Material_SetFloat_m2724(L_40, _stringLiteral111, ((float)((float)(*(float*)(float*)SZArrayLdElema(L_41, L_42))/(float)(100.0f))), /*hidden argument*/NULL);
		Material_t2 * L_43 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		float L_44 = (__this->___FadeFX_4);
		NullCheck(L_43);
		Material_SetFloat_m2724(L_43, _stringLiteral113, L_44, /*hidden argument*/NULL);
		Material_t2 * L_45 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		float L_46 = (__this->____Size_5);
		NullCheck(L_45);
		Material_SetFloat_m2724(L_45, _stringLiteral73, L_46, /*hidden argument*/NULL);
		Material_t2 * L_47 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		float L_48 = (__this->____Dist_7);
		NullCheck(L_47);
		Material_SetFloat_m2724(L_47, _stringLiteral236, L_48, /*hidden argument*/NULL);
		Material_t2 * L_49 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		float L_50 = (__this->____Smooth_6);
		NullCheck(L_49);
		Material_SetFloat_m2724(L_49, _stringLiteral237, L_50, /*hidden argument*/NULL);
		Material_t2 * L_51 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_52 = ___sourceTexture;
		NullCheck(L_52);
		int32_t L_53 = RenderTexture_get_width_m2726(L_52, /*hidden argument*/NULL);
		RenderTexture_t15 * L_54 = ___sourceTexture;
		NullCheck(L_54);
		int32_t L_55 = RenderTexture_get_height_m2727(L_54, /*hidden argument*/NULL);
		Vector4_t5  L_56 = {0};
		Vector4__ctor_m2728(&L_56, (((float)L_53)), (((float)L_55)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_51);
		Material_SetVector_m2729(L_51, _stringLiteral11, L_56, /*hidden argument*/NULL);
		RenderTexture_t15 * L_57 = ___sourceTexture;
		RenderTexture_t15 * L_58 = ___destTexture;
		Material_t2 * L_59 = CameraFilterPack_Oculus_NightVision5_get_material_m1065(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_57, L_58, L_59, /*hidden argument*/NULL);
		goto IL_025d;
	}

IL_0256:
	{
		RenderTexture_t15 * L_60 = ___sourceTexture;
		RenderTexture_t15 * L_61 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_60, L_61, /*hidden argument*/NULL);
	}

IL_025d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision5::OnValidate()
extern "C" void CameraFilterPack_Oculus_NightVision5_OnValidate_m1069 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method)
{
	{
		CameraFilterPack_Oculus_NightVision5_ChangeFilters_m1066(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision5::Update()
extern "C" void CameraFilterPack_Oculus_NightVision5_Update_m1070 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_NightVision5::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision5_OnDisable_m1071 (CameraFilterPack_Oculus_NightVision5_t165 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_10);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_10);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Oculus_ThermaVision
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_ThermaVision.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Oculus_ThermaVision
#include "AssemblyU2DCSharp_CameraFilterPack_Oculus_ThermaVisionMethodDeclarations.h"



// System.Void CameraFilterPack_Oculus_ThermaVision::.ctor()
extern "C" void CameraFilterPack_Oculus_ThermaVision__ctor_m1072 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Therma_Variation_6 = (0.5f);
		__this->___Contrast_7 = (3.0f);
		__this->___SceneCut_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Oculus_ThermaVision::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Oculus_ThermaVision_get_material_m1073 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Oculus_ThermaVision::Start()
extern TypeInfo* CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral244;
extern "C" void CameraFilterPack_Oculus_ThermaVision_Start_m1074 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(127);
		_stringLiteral244 = il2cpp_codegen_string_literal_from_index(244);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Therma_Variation_6);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Contrast_7);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Burn_8);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___SceneCut_9);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral244, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_ThermaVision::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Oculus_ThermaVision_OnRenderImage_m1075 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Oculus_ThermaVision_get_material_m1073(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Oculus_ThermaVision_get_material_m1073(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Therma_Variation_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Oculus_ThermaVision_get_material_m1073(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Contrast_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Oculus_ThermaVision_get_material_m1073(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Burn_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Oculus_ThermaVision_get_material_m1073(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___SceneCut_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Oculus_ThermaVision_get_material_m1073(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Oculus_ThermaVision_get_material_m1073(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_ThermaVision::OnValidate()
extern TypeInfo* CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Oculus_ThermaVision_OnValidate_m1076 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(127);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Therma_Variation_6);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Contrast_7);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Burn_8);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___SceneCut_9);
		((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Oculus_ThermaVision::Update()
extern TypeInfo* CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Oculus_ThermaVision_Update_m1077 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(127);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Therma_Variation_6 = L_1;
		float L_2 = ((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Contrast_7 = L_2;
		float L_3 = ((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Burn_8 = L_3;
		float L_4 = ((CameraFilterPack_Oculus_ThermaVision_t166_StaticFields*)CameraFilterPack_Oculus_ThermaVision_t166_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___SceneCut_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Oculus_ThermaVision::OnDisable()
extern "C" void CameraFilterPack_Oculus_ThermaVision_OnDisable_m1078 (CameraFilterPack_Oculus_ThermaVision_t166 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_OldFilm_Cutting1
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting1.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_OldFilm_Cutting1
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting1MethodDeclarations.h"



// System.Void CameraFilterPack_OldFilm_Cutting1::.ctor()
extern "C" void CameraFilterPack_OldFilm_Cutting1__ctor_m1079 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Speed_4 = (1.0f);
		__this->___Luminosity_5 = (1.5f);
		__this->___Vignette_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_OldFilm_Cutting1::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_OldFilm_Cutting1_get_material_m1080 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_8 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_8);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_8);
		return L_5;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting1::Start()
extern TypeInfo* Texture2D_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral245;
extern Il2CppCodeGenString* _stringLiteral246;
extern "C" void CameraFilterPack_OldFilm_Cutting1_Start_m1081 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral245 = il2cpp_codegen_string_literal_from_index(245);
		_stringLiteral246 = il2cpp_codegen_string_literal_from_index(246);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t473 * L_0 = Resources_Load_m2735(NULL /*static, unused*/, _stringLiteral245, /*hidden argument*/NULL);
		__this->___Texture2_9 = ((Texture2D_t9 *)IsInst(L_0, Texture2D_t9_il2cpp_TypeInfo_var));
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral246, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		return;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral19;
extern Il2CppCodeGenString* _stringLiteral20;
extern "C" void CameraFilterPack_OldFilm_Cutting1_OnRenderImage_m1082 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral19 = il2cpp_codegen_string_literal_from_index(19);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00da;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_OldFilm_Cutting1_get_material_m1080(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_OldFilm_Cutting1_get_material_m1080(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Luminosity_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_OldFilm_Cutting1_get_material_m1080(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Vignette_6);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, ((float)((float)(1.0f)-(float)L_10)), /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_OldFilm_Cutting1_get_material_m1080(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Negative_7);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_OldFilm_Cutting1_get_material_m1080(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Speed_4);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral19, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_OldFilm_Cutting1_get_material_m1080(__this, /*hidden argument*/NULL);
		Texture2D_t9 * L_16 = (__this->___Texture2_9);
		NullCheck(L_15);
		Material_SetTexture_m2736(L_15, _stringLiteral20, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_OldFilm_Cutting1_get_material_m1080(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00e1;
	}

IL_00da:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		return;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting1::Update()
extern "C" void CameraFilterPack_OldFilm_Cutting1_Update_m1083 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting1::OnDisable()
extern "C" void CameraFilterPack_OldFilm_Cutting1_OnDisable_m1084 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_8);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_OldFilm_Cutting2
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_OldFilm_Cutting2
#include "AssemblyU2DCSharp_CameraFilterPack_OldFilm_Cutting2MethodDeclarations.h"



// System.Void CameraFilterPack_OldFilm_Cutting2::.ctor()
extern "C" void CameraFilterPack_OldFilm_Cutting2__ctor_m1085 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Speed_4 = (5.0f);
		__this->___Luminosity_5 = (1.0f);
		__this->___Vignette_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_OldFilm_Cutting2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_OldFilm_Cutting2_get_material_m1086 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_8 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_8);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_8);
		return L_5;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting2::Start()
extern TypeInfo* Texture2D_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral247;
extern Il2CppCodeGenString* _stringLiteral248;
extern "C" void CameraFilterPack_OldFilm_Cutting2_Start_m1087 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral247 = il2cpp_codegen_string_literal_from_index(247);
		_stringLiteral248 = il2cpp_codegen_string_literal_from_index(248);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t473 * L_0 = Resources_Load_m2735(NULL /*static, unused*/, _stringLiteral247, /*hidden argument*/NULL);
		__this->___Texture2_9 = ((Texture2D_t9 *)IsInst(L_0, Texture2D_t9_il2cpp_TypeInfo_var));
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral248, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		return;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral19;
extern Il2CppCodeGenString* _stringLiteral20;
extern "C" void CameraFilterPack_OldFilm_Cutting2_OnRenderImage_m1088 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral19 = il2cpp_codegen_string_literal_from_index(19);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00e0;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_OldFilm_Cutting2_get_material_m1086(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_OldFilm_Cutting2_get_material_m1086(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Luminosity_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, ((float)((float)(2.0f)-(float)L_8)), /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_OldFilm_Cutting2_get_material_m1086(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Vignette_6);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, ((float)((float)(1.0f)-(float)L_10)), /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_OldFilm_Cutting2_get_material_m1086(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Negative_7);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_OldFilm_Cutting2_get_material_m1086(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Speed_4);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral19, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_OldFilm_Cutting2_get_material_m1086(__this, /*hidden argument*/NULL);
		Texture2D_t9 * L_16 = (__this->___Texture2_9);
		NullCheck(L_15);
		Material_SetTexture_m2736(L_15, _stringLiteral20, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_OldFilm_Cutting2_get_material_m1086(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00e7;
	}

IL_00e0:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00e7:
	{
		return;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting2::Update()
extern "C" void CameraFilterPack_OldFilm_Cutting2_Update_m1089 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_OldFilm_Cutting2::OnDisable()
extern "C" void CameraFilterPack_OldFilm_Cutting2_OnDisable_m1090 (CameraFilterPack_OldFilm_Cutting2_t168 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_8);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Pixel_Pixelisation
#include "AssemblyU2DCSharp_CameraFilterPack_Pixel_Pixelisation.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Pixel_Pixelisation
#include "AssemblyU2DCSharp_CameraFilterPack_Pixel_PixelisationMethodDeclarations.h"



// System.Void CameraFilterPack_Pixel_Pixelisation::.ctor()
extern "C" void CameraFilterPack_Pixel_Pixelisation__ctor_m1091 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method)
{
	{
		__this->____SizeX_4 = (1.0f);
		__this->____SizeY_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Pixel_Pixelisation::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Pixel_Pixelisation_get_material_m1092 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Pixel_Pixelisation::Start()
extern TypeInfo* CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral249;
extern "C" void CameraFilterPack_Pixel_Pixelisation_Start_m1093 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		_stringLiteral249 = il2cpp_codegen_string_literal_from_index(249);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->____Pixelisation_3);
		((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixel_7 = L_0;
		float L_1 = (__this->____SizeX_4);
		((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixelX_8 = L_1;
		float L_2 = (__this->____SizeY_5);
		((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixelY_9 = L_2;
		Shader_t1 * L_3 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral249, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_3;
		bool L_4 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0043;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0043:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixel_Pixelisation::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral115;
extern Il2CppCodeGenString* _stringLiteral250;
extern Il2CppCodeGenString* _stringLiteral251;
extern "C" void CameraFilterPack_Pixel_Pixelisation_OnRenderImage_m1094 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral115 = il2cpp_codegen_string_literal_from_index(115);
		_stringLiteral250 = il2cpp_codegen_string_literal_from_index(250);
		_stringLiteral251 = il2cpp_codegen_string_literal_from_index(251);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0065;
		}
	}
	{
		Material_t2 * L_2 = CameraFilterPack_Pixel_Pixelisation_get_material_m1092(__this, /*hidden argument*/NULL);
		float L_3 = (__this->____Pixelisation_3);
		NullCheck(L_2);
		Material_SetFloat_m2724(L_2, _stringLiteral115, L_3, /*hidden argument*/NULL);
		Material_t2 * L_4 = CameraFilterPack_Pixel_Pixelisation_get_material_m1092(__this, /*hidden argument*/NULL);
		float L_5 = (__this->____SizeX_4);
		NullCheck(L_4);
		Material_SetFloat_m2724(L_4, _stringLiteral250, L_5, /*hidden argument*/NULL);
		Material_t2 * L_6 = CameraFilterPack_Pixel_Pixelisation_get_material_m1092(__this, /*hidden argument*/NULL);
		float L_7 = (__this->____SizeY_5);
		NullCheck(L_6);
		Material_SetFloat_m2724(L_6, _stringLiteral251, L_7, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		RenderTexture_t15 * L_9 = ___destTexture;
		Material_t2 * L_10 = CameraFilterPack_Pixel_Pixelisation_get_material_m1092(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		goto IL_006c;
	}

IL_0065:
	{
		RenderTexture_t15 * L_11 = ___sourceTexture;
		RenderTexture_t15 * L_12 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixel_Pixelisation::OnValidate()
extern TypeInfo* CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixel_Pixelisation_OnValidate_m1095 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->____Pixelisation_3);
		((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixel_7 = L_0;
		float L_1 = (__this->____SizeX_4);
		((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixelX_8 = L_1;
		float L_2 = (__this->____SizeY_5);
		((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixelY_9 = L_2;
		return;
	}
}
// System.Void CameraFilterPack_Pixel_Pixelisation::Update()
extern TypeInfo* CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixel_Pixelisation_Update_m1096 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixel_7;
		__this->____Pixelisation_3 = L_1;
		float L_2 = ((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixelX_8;
		__this->____SizeX_4 = L_2;
		float L_3 = ((CameraFilterPack_Pixel_Pixelisation_t169_StaticFields*)CameraFilterPack_Pixel_Pixelisation_t169_il2cpp_TypeInfo_var->static_fields)->___ChangePixelY_9;
		__this->____SizeY_5 = L_3;
	}

IL_002b:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixel_Pixelisation::OnDisable()
extern "C" void CameraFilterPack_Pixel_Pixelisation_OnDisable_m1097 (CameraFilterPack_Pixel_Pixelisation_t169 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Pixelisation_Dot
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_Dot.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Pixelisation_Dot
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_DotMethodDeclarations.h"



// System.Void CameraFilterPack_Pixelisation_Dot::.ctor()
extern "C" void CameraFilterPack_Pixelisation_Dot__ctor_m1098 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (0.005f);
		__this->___LightBackGround_7 = (0.3f);
		__this->___Speed_8 = (1.0f);
		__this->___Size2_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Pixelisation_Dot::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Pixelisation_Dot_get_material_m1099 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Pixelisation_Dot::Start()
extern TypeInfo* CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral252;
extern "C" void CameraFilterPack_Pixelisation_Dot_Start_m1100 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		_stringLiteral252 = il2cpp_codegen_string_literal_from_index(252);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___LightBackGround_7);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Speed_8);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Size2_9);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral252, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_Dot::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Pixelisation_Dot_OnRenderImage_m1101 (CameraFilterPack_Pixelisation_Dot_t170 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Pixelisation_Dot_get_material_m1099(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Pixelisation_Dot_get_material_m1099(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Pixelisation_Dot_get_material_m1099(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___LightBackGround_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Pixelisation_Dot_get_material_m1099(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Speed_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Pixelisation_Dot_get_material_m1099(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Size2_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Pixelisation_Dot_get_material_m1099(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Pixelisation_Dot_get_material_m1099(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_Dot::OnValidate()
extern TypeInfo* CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixelisation_Dot_OnValidate_m1102 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___LightBackGround_7);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Speed_8);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Size2_9);
		((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_Dot::Update()
extern TypeInfo* CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixelisation_Dot_Update_m1103 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(129);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Size_6 = L_1;
		float L_2 = ((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___LightBackGround_7 = L_2;
		float L_3 = ((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Speed_8 = L_3;
		float L_4 = ((CameraFilterPack_Pixelisation_Dot_t170_StaticFields*)CameraFilterPack_Pixelisation_Dot_t170_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Size2_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_Dot::OnDisable()
extern "C" void CameraFilterPack_Pixelisation_Dot_OnDisable_m1104 (CameraFilterPack_Pixelisation_Dot_t170 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Pixelisation_OilPaint
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaint.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Pixelisation_OilPaint
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaintMethodDeclarations.h"



// System.Void CameraFilterPack_Pixelisation_OilPaint::.ctor()
extern "C" void CameraFilterPack_Pixelisation_OilPaint__ctor_m1105 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Pixelisation_OilPaint::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Pixelisation_OilPaint_get_material_m1106 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaint::Start()
extern TypeInfo* CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral253;
extern "C" void CameraFilterPack_Pixelisation_OilPaint_Start_m1107 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(130);
		_stringLiteral253 = il2cpp_codegen_string_literal_from_index(253);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Pixelisation_OilPaint_t171_StaticFields*)CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral253, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void CameraFilterPack_Pixelisation_OilPaint_OnRenderImage_m1108 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Pixelisation_OilPaint_get_material_m1106(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Pixelisation_OilPaint_get_material_m1106(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Pixelisation_OilPaint_get_material_m1106(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value_6);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral2, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_Pixelisation_OilPaint_get_material_m1106(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnValidate()
extern TypeInfo* CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixelisation_OilPaint_OnValidate_m1109 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(130);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Pixelisation_OilPaint_t171_StaticFields*)CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaint::Update()
extern TypeInfo* CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixelisation_OilPaint_Update_m1110 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(130);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Pixelisation_OilPaint_t171_StaticFields*)CameraFilterPack_Pixelisation_OilPaint_t171_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnDisable()
extern "C" void CameraFilterPack_Pixelisation_OilPaint_OnDisable_m1111 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Pixelisation_OilPaintHQ
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaintHQ.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Pixelisation_OilPaintHQ
#include "AssemblyU2DCSharp_CameraFilterPack_Pixelisation_OilPaintHQMethodDeclarations.h"



// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::.ctor()
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ__ctor_m1112 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (2.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Pixelisation_OilPaintHQ::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Pixelisation_OilPaintHQ_get_material_m1113 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::Start()
extern TypeInfo* CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral254;
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_Start_m1114 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		_stringLiteral254 = il2cpp_codegen_string_literal_from_index(254);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Pixelisation_OilPaintHQ_t172_StaticFields*)CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral254, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_OnRenderImage_m1115 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Pixelisation_OilPaintHQ_get_material_m1113(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Pixelisation_OilPaintHQ_get_material_m1113(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Pixelisation_OilPaintHQ_get_material_m1113(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value_6);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral2, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_Pixelisation_OilPaintHQ_get_material_m1113(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnValidate()
extern TypeInfo* CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_OnValidate_m1116 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Pixelisation_OilPaintHQ_t172_StaticFields*)CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::Update()
extern TypeInfo* CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_Update_m1117 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(131);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Pixelisation_OilPaintHQ_t172_StaticFields*)CameraFilterPack_Pixelisation_OilPaintHQ_t172_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnDisable()
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_OnDisable_m1118 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Sharpen_Sharpen
#include "AssemblyU2DCSharp_CameraFilterPack_Sharpen_Sharpen.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Sharpen_Sharpen
#include "AssemblyU2DCSharp_CameraFilterPack_Sharpen_SharpenMethodDeclarations.h"



// System.Void CameraFilterPack_Sharpen_Sharpen::.ctor()
extern "C" void CameraFilterPack_Sharpen_Sharpen__ctor_m1119 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method)
{
	{
		__this->___Value_3 = (4.0f);
		__this->___TimeX_4 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Sharpen_Sharpen::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Sharpen_Sharpen_get_material_m1120 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_Sharpen_Sharpen::Start()
extern TypeInfo* CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral255;
extern "C" void CameraFilterPack_Sharpen_Sharpen_Start_m1121 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(132);
		_stringLiteral255 = il2cpp_codegen_string_literal_from_index(255);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_3);
		((CameraFilterPack_Sharpen_Sharpen_t173_StaticFields*)CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral255, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_Sharpen_Sharpen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void CameraFilterPack_Sharpen_Sharpen_OnRenderImage_m1122 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Sharpen_Sharpen_get_material_m1120(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Sharpen_Sharpen_get_material_m1120(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Sharpen_Sharpen_get_material_m1120(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value_3);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral2, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_Sharpen_Sharpen_get_material_m1120(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_Sharpen_Sharpen::OnValidate()
extern TypeInfo* CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Sharpen_Sharpen_OnValidate_m1123 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(132);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_3);
		((CameraFilterPack_Sharpen_Sharpen_t173_StaticFields*)CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_Sharpen_Sharpen::Update()
extern TypeInfo* CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Sharpen_Sharpen_Update_m1124 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(132);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Sharpen_Sharpen_t173_StaticFields*)CameraFilterPack_Sharpen_Sharpen_t173_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_7;
		__this->___Value_3 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_Sharpen_Sharpen::OnDisable()
extern "C" void CameraFilterPack_Sharpen_Sharpen_OnDisable_m1125 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Special_Bubble
#include "AssemblyU2DCSharp_CameraFilterPack_Special_Bubble.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Special_Bubble
#include "AssemblyU2DCSharp_CameraFilterPack_Special_BubbleMethodDeclarations.h"



// System.Void CameraFilterPack_Special_Bubble::.ctor()
extern "C" void CameraFilterPack_Special_Bubble__ctor_m1126 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___X_6 = (0.5f);
		__this->___Y_7 = (0.5f);
		__this->___Rate_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Special_Bubble::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Special_Bubble_get_material_m1127 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Special_Bubble::Start()
extern TypeInfo* CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral256;
extern "C" void CameraFilterPack_Special_Bubble_Start_m1128 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(133);
		_stringLiteral256 = il2cpp_codegen_string_literal_from_index(256);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___X_6);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Y_7);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Rate_8);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral256, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Special_Bubble::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Special_Bubble_OnRenderImage_m1129 (CameraFilterPack_Special_Bubble_t174 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Special_Bubble_get_material_m1127(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Special_Bubble_get_material_m1127(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___X_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Special_Bubble_get_material_m1127(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Y_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Special_Bubble_get_material_m1127(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Rate_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Special_Bubble_get_material_m1127(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Special_Bubble_get_material_m1127(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Special_Bubble_get_material_m1127(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Special_Bubble::OnValidate()
extern TypeInfo* CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Special_Bubble_OnValidate_m1130 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(133);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___X_6);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Y_7);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Rate_8);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Special_Bubble::Update()
extern TypeInfo* CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Special_Bubble_Update_m1131 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(133);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___X_6 = L_1;
		float L_2 = ((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Y_7 = L_2;
		float L_3 = ((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Rate_8 = L_3;
		float L_4 = ((CameraFilterPack_Special_Bubble_t174_StaticFields*)CameraFilterPack_Special_Bubble_t174_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Special_Bubble::OnDisable()
extern "C" void CameraFilterPack_Special_Bubble_OnDisable_m1132 (CameraFilterPack_Special_Bubble_t174 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_50
#include "AssemblyU2DCSharp_CameraFilterPack_TV_50.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_50
#include "AssemblyU2DCSharp_CameraFilterPack_TV_50MethodDeclarations.h"



// System.Void CameraFilterPack_TV_50::.ctor()
extern "C" void CameraFilterPack_TV_50__ctor_m1133 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_50::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_50_get_material_m1134 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_50::Start()
extern Il2CppCodeGenString* _stringLiteral257;
extern "C" void CameraFilterPack_TV_50_Start_m1135 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral257 = il2cpp_codegen_string_literal_from_index(257);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral257, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_50::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_50_OnRenderImage_m1136 (CameraFilterPack_TV_50_t175 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_50_get_material_m1134(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_50_get_material_m1134(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_TV_50_get_material_m1134(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_50::Update()
extern "C" void CameraFilterPack_TV_50_Update_m1137 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_50::OnDisable()
extern "C" void CameraFilterPack_TV_50_OnDisable_m1138 (CameraFilterPack_TV_50_t175 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_80
#include "AssemblyU2DCSharp_CameraFilterPack_TV_80.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_80
#include "AssemblyU2DCSharp_CameraFilterPack_TV_80MethodDeclarations.h"



// System.Void CameraFilterPack_TV_80::.ctor()
extern "C" void CameraFilterPack_TV_80__ctor_m1139 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_80::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_80_get_material_m1140 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_80::Start()
extern Il2CppCodeGenString* _stringLiteral258;
extern "C" void CameraFilterPack_TV_80_Start_m1141 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral258 = il2cpp_codegen_string_literal_from_index(258);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral258, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_80::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_80_OnRenderImage_m1142 (CameraFilterPack_TV_80_t176 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_80_get_material_m1140(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_80_get_material_m1140(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_TV_80_get_material_m1140(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_80::Update()
extern "C" void CameraFilterPack_TV_80_Update_m1143 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_80::OnDisable()
extern "C" void CameraFilterPack_TV_80_OnDisable_m1144 (CameraFilterPack_TV_80_t176 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_ARCADE
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_ARCADE
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADEMethodDeclarations.h"



// System.Void CameraFilterPack_TV_ARCADE::.ctor()
extern "C" void CameraFilterPack_TV_ARCADE__ctor_m1145 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_ARCADE::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_ARCADE_get_material_m1146 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_ARCADE::Start()
extern Il2CppCodeGenString* _stringLiteral259;
extern "C" void CameraFilterPack_TV_ARCADE_Start_m1147 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral259 = il2cpp_codegen_string_literal_from_index(259);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral259, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_ARCADE::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_ARCADE_OnRenderImage_m1148 (CameraFilterPack_TV_ARCADE_t177 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_ARCADE_get_material_m1146(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_ARCADE_get_material_m1146(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_TV_ARCADE_get_material_m1146(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_ARCADE::Update()
extern "C" void CameraFilterPack_TV_ARCADE_Update_m1149 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_ARCADE::OnDisable()
extern "C" void CameraFilterPack_TV_ARCADE_OnDisable_m1150 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_ARCADE_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE_2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_ARCADE_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ARCADE_2MethodDeclarations.h"



// System.Void CameraFilterPack_TV_ARCADE_2::.ctor()
extern "C" void CameraFilterPack_TV_ARCADE_2__ctor_m1151 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Interferance_Size_6 = (1.0f);
		__this->___Interferance_Speed_7 = (0.5f);
		__this->___Contrast_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_ARCADE_2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_ARCADE_2_get_material_m1152 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_ARCADE_2::Start()
extern TypeInfo* CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral260;
extern "C" void CameraFilterPack_TV_ARCADE_2_Start_m1153 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(134);
		_stringLiteral260 = il2cpp_codegen_string_literal_from_index(260);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Interferance_Size_6);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Interferance_Speed_7);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Contrast_8);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral260, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_ARCADE_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_ARCADE_2_OnRenderImage_m1154 (CameraFilterPack_TV_ARCADE_2_t178 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_ARCADE_2_get_material_m1152(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_ARCADE_2_get_material_m1152(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Interferance_Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_ARCADE_2_get_material_m1152(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Interferance_Speed_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_ARCADE_2_get_material_m1152(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Contrast_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_ARCADE_2_get_material_m1152(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_ARCADE_2_get_material_m1152(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_ARCADE_2_get_material_m1152(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_ARCADE_2::OnValidate()
extern TypeInfo* CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_ARCADE_2_OnValidate_m1155 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(134);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Interferance_Size_6);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Interferance_Speed_7);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Contrast_8);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_ARCADE_2::Update()
extern TypeInfo* CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_ARCADE_2_Update_m1156 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(134);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Interferance_Size_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Interferance_Speed_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Contrast_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_ARCADE_2_t178_StaticFields*)CameraFilterPack_TV_ARCADE_2_t178_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_ARCADE_2::OnDisable()
extern "C" void CameraFilterPack_TV_ARCADE_2_OnDisable_m1157 (CameraFilterPack_TV_ARCADE_2_t178 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Artefact
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Artefact.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Artefact
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ArtefactMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Artefact::.ctor()
extern "C" void CameraFilterPack_TV_Artefact__ctor_m1158 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_4 = (1.0f);
		__this->___Colorisation_5 = (1.0f);
		__this->___Parasite_6 = (1.0f);
		__this->___Noise_7 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Artefact::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Artefact_get_material_m1159 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_8 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_8);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_8);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Artefact::Start()
extern Il2CppCodeGenString* _stringLiteral261;
extern "C" void CameraFilterPack_TV_Artefact_Start_m1160 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral261 = il2cpp_codegen_string_literal_from_index(261);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral261, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Artefact::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral262;
extern Il2CppCodeGenString* _stringLiteral263;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Artefact_OnRenderImage_m1161 (CameraFilterPack_TV_Artefact_t179 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral262 = il2cpp_codegen_string_literal_from_index(262);
		_stringLiteral263 = il2cpp_codegen_string_literal_from_index(263);
		_stringLiteral93 = il2cpp_codegen_string_literal_from_index(93);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d5;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Artefact_get_material_m1159(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Artefact_get_material_m1159(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Colorisation_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral262, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_Artefact_get_material_m1159(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Parasite_6);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral263, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_Artefact_get_material_m1159(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Noise_7);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral93, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_Artefact_get_material_m1159(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_width_m2726(L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_height_m2727(L_16, /*hidden argument*/NULL);
		Vector4_t5  L_18 = {0};
		Vector4__ctor_m2728(&L_18, (((float)L_15)), (((float)L_17)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Material_SetVector_m2729(L_13, _stringLiteral11, L_18, /*hidden argument*/NULL);
		RenderTexture_t15 * L_19 = ___sourceTexture;
		RenderTexture_t15 * L_20 = ___destTexture;
		Material_t2 * L_21 = CameraFilterPack_TV_Artefact_get_material_m1159(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		goto IL_00dc;
	}

IL_00d5:
	{
		RenderTexture_t15 * L_22 = ___sourceTexture;
		RenderTexture_t15 * L_23 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Artefact::Update()
extern "C" void CameraFilterPack_TV_Artefact_Update_m1162 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Artefact::OnDisable()
extern "C" void CameraFilterPack_TV_Artefact_OnDisable_m1163 (CameraFilterPack_TV_Artefact_t179 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_8);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_8);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_BrokenGlass
#include "AssemblyU2DCSharp_CameraFilterPack_TV_BrokenGlass.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_BrokenGlass
#include "AssemblyU2DCSharp_CameraFilterPack_TV_BrokenGlassMethodDeclarations.h"



// System.Void CameraFilterPack_TV_BrokenGlass::.ctor()
extern "C" void CameraFilterPack_TV_BrokenGlass__ctor_m1164 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Broken_Big_7 = (1.0f);
		__this->___LightReflect_8 = (0.002f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_BrokenGlass::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_BrokenGlass_get_material_m1165 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_9);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_9 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_9);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_9);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_BrokenGlass::Start()
extern TypeInfo* Texture2D_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral264;
extern Il2CppCodeGenString* _stringLiteral265;
extern "C" void CameraFilterPack_TV_BrokenGlass_Start_m1166 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral264 = il2cpp_codegen_string_literal_from_index(264);
		_stringLiteral265 = il2cpp_codegen_string_literal_from_index(265);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t473 * L_0 = Resources_Load_m2735(NULL /*static, unused*/, _stringLiteral264, /*hidden argument*/NULL);
		__this->___Texture2_10 = ((Texture2D_t9 *)IsInst(L_0, Texture2D_t9_il2cpp_TypeInfo_var));
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral265, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0037;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0037:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_BrokenGlass::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral28;
extern Il2CppCodeGenString* _stringLiteral20;
extern "C" void CameraFilterPack_TV_BrokenGlass_OnRenderImage_m1167 (CameraFilterPack_TV_BrokenGlass_t180 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral28 = il2cpp_codegen_string_literal_from_index(28);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ea;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___LightReflect_8);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Broken_Small_4);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Broken_Medium_5);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Broken_High_6);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		float L_16 = (__this->___Broken_Big_7);
		NullCheck(L_15);
		Material_SetFloat_m2724(L_15, _stringLiteral28, L_16, /*hidden argument*/NULL);
		Material_t2 * L_17 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		Texture2D_t9 * L_18 = (__this->___Texture2_10);
		NullCheck(L_17);
		Material_SetTexture_m2736(L_17, _stringLiteral20, L_18, /*hidden argument*/NULL);
		RenderTexture_t15 * L_19 = ___sourceTexture;
		RenderTexture_t15 * L_20 = ___destTexture;
		Material_t2 * L_21 = CameraFilterPack_TV_BrokenGlass_get_material_m1165(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_19, L_20, L_21, /*hidden argument*/NULL);
		goto IL_00f1;
	}

IL_00ea:
	{
		RenderTexture_t15 * L_22 = ___sourceTexture;
		RenderTexture_t15 * L_23 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_BrokenGlass::Update()
extern "C" void CameraFilterPack_TV_BrokenGlass_Update_m1168 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_BrokenGlass::OnDisable()
extern "C" void CameraFilterPack_TV_BrokenGlass_OnDisable_m1169 (CameraFilterPack_TV_BrokenGlass_t180 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_9);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_9);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Chromatical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Chromatical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_ChromaticalMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Chromatical::.ctor()
extern "C" void CameraFilterPack_TV_Chromatical__ctor_m1170 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Chromatical::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Chromatical_get_material_m1171 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Chromatical::Start()
extern Il2CppCodeGenString* _stringLiteral266;
extern "C" void CameraFilterPack_TV_Chromatical_Start_m1172 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral266, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Chromatical::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Chromatical_OnRenderImage_m1173 (CameraFilterPack_TV_Chromatical_t181 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0092;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)((float)((float)L_3*(float)(2.0f)))));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_0044:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Chromatical_get_material_m1171(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Chromatical_get_material_m1171(__this, /*hidden argument*/NULL);
		int32_t L_8 = Screen_get_width_m2737(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_9 = Screen_get_height_m2738(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t7  L_10 = {0};
		Vector2__ctor_m2714(&L_10, (((float)L_8)), (((float)L_9)), /*hidden argument*/NULL);
		Vector4_t5  L_11 = Vector4_op_Implicit_m2739(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_11, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Material_t2 * L_14 = CameraFilterPack_TV_Chromatical_get_material_m1171(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0092:
	{
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Chromatical::Update()
extern "C" void CameraFilterPack_TV_Chromatical_Update_m1174 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Chromatical::OnDisable()
extern "C" void CameraFilterPack_TV_Chromatical_OnDisable_m1175 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Chromatical2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Chromatical2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Chromatical2MethodDeclarations.h"



// System.Void CameraFilterPack_TV_Chromatical2::.ctor()
extern "C" void CameraFilterPack_TV_Chromatical2__ctor_m1176 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Aberration_6 = (2.0f);
		__this->___Value2_7 = (1.0f);
		__this->___Value3_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Chromatical2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Chromatical2_get_material_m1177 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Chromatical2::Start()
extern TypeInfo* CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral267;
extern "C" void CameraFilterPack_TV_Chromatical2_Start_m1178 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(135);
		_stringLiteral267 = il2cpp_codegen_string_literal_from_index(267);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Aberration_6);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral267, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Chromatical2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Chromatical2_OnRenderImage_m1179 (CameraFilterPack_TV_Chromatical2_t182 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Chromatical2_get_material_m1177(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Chromatical2_get_material_m1177(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Aberration_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_Chromatical2_get_material_m1177(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Value2_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_Chromatical2_get_material_m1177(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Value3_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_Chromatical2_get_material_m1177(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_Chromatical2_get_material_m1177(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_Chromatical2_get_material_m1177(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Chromatical2::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Chromatical2_OnValidate_m1180 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(135);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Aberration_6);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Value3_8);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_Chromatical2::Update()
extern TypeInfo* CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Chromatical2_Update_m1181 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(135);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Aberration_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Value2_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Value3_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_Chromatical2_t182_StaticFields*)CameraFilterPack_TV_Chromatical2_t182_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Chromatical2::OnDisable()
extern "C" void CameraFilterPack_TV_Chromatical2_OnDisable_m1182 (CameraFilterPack_TV_Chromatical2_t182 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_CompressionFX
#include "AssemblyU2DCSharp_CameraFilterPack_TV_CompressionFX.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_CompressionFX
#include "AssemblyU2DCSharp_CameraFilterPack_TV_CompressionFXMethodDeclarations.h"



// System.Void CameraFilterPack_TV_CompressionFX::.ctor()
extern "C" void CameraFilterPack_TV_CompressionFX__ctor_m1183 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_4 = (1.0f);
		__this->___Parasite_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_CompressionFX::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_CompressionFX_get_material_m1184 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_CompressionFX::Start()
extern Il2CppCodeGenString* _stringLiteral268;
extern "C" void CameraFilterPack_TV_CompressionFX_Start_m1185 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral268, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_CompressionFX::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral263;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_CompressionFX_OnRenderImage_m1186 (CameraFilterPack_TV_CompressionFX_t183 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral263 = il2cpp_codegen_string_literal_from_index(263);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_CompressionFX_get_material_m1184(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_CompressionFX_get_material_m1184(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Parasite_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral263, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_CompressionFX_get_material_m1184(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_TV_CompressionFX_get_material_m1184(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_CompressionFX::Update()
extern "C" void CameraFilterPack_TV_CompressionFX_Update_m1187 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_CompressionFX::OnDisable()
extern "C" void CameraFilterPack_TV_CompressionFX_OnDisable_m1188 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Distorted
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Distorted.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Distorted
#include "AssemblyU2DCSharp_CameraFilterPack_TV_DistortedMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Distorted::.ctor()
extern "C" void CameraFilterPack_TV_Distorted__ctor_m1189 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Distortion_4 = (1.0f);
		__this->___RGB_5 = (0.002f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Distorted::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Distorted_get_material_m1190 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Distorted::Start()
extern TypeInfo* CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral269;
extern "C" void CameraFilterPack_TV_Distorted_Start_m1191 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		_stringLiteral269 = il2cpp_codegen_string_literal_from_index(269);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Distorted_t184_StaticFields*)CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral269, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Distorted::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral270;
extern "C" void CameraFilterPack_TV_Distorted_OnRenderImage_m1192 (CameraFilterPack_TV_Distorted_t184 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral270 = il2cpp_codegen_string_literal_from_index(270);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0092;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Distorted_get_material_m1190(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Distorted_get_material_m1190(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_Distorted_get_material_m1190(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___RGB_5);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral270, L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_11 = ___sourceTexture;
		RenderTexture_t15 * L_12 = ___destTexture;
		Material_t2 * L_13 = CameraFilterPack_TV_Distorted_get_material_m1190(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_0092:
	{
		RenderTexture_t15 * L_14 = ___sourceTexture;
		RenderTexture_t15 * L_15 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0099:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Distorted::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Distorted_OnValidate_m1193 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Distorted_t184_StaticFields*)CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_TV_Distorted::Update()
extern TypeInfo* CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Distorted_Update_m1194 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Distorted_t184_StaticFields*)CameraFilterPack_TV_Distorted_t184_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_7;
		__this->___Distortion_4 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Distorted::OnDisable()
extern "C" void CameraFilterPack_TV_Distorted_OnDisable_m1195 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_LED
#include "AssemblyU2DCSharp_CameraFilterPack_TV_LED.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_LED
#include "AssemblyU2DCSharp_CameraFilterPack_TV_LEDMethodDeclarations.h"



// System.Void CameraFilterPack_TV_LED::.ctor()
extern "C" void CameraFilterPack_TV_LED__ctor_m1196 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_4 = (1.0f);
		__this->___Distortion_5 = (1.0f);
		__this->___Size_6 = 5;
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_LED::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_LED_get_material_m1197 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_7 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_7);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_7);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_LED::Start()
extern Il2CppCodeGenString* _stringLiteral271;
extern "C" void CameraFilterPack_TV_LED_Start_m1198 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral271 = il2cpp_codegen_string_literal_from_index(271);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral271, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_LED::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral73;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_LED_OnRenderImage_m1199 (CameraFilterPack_TV_LED_t185 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral73 = il2cpp_codegen_string_literal_from_index(73);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00c0;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_LED_get_material_m1197(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_LED_get_material_m1197(__this, /*hidden argument*/NULL);
		int32_t L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral73, (((float)L_8)), /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_LED_get_material_m1197(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Distortion_5);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral16, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_LED_get_material_m1197(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_width_m2726(L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_14 = ___sourceTexture;
		NullCheck(L_14);
		int32_t L_15 = RenderTexture_get_height_m2727(L_14, /*hidden argument*/NULL);
		Vector4_t5  L_16 = {0};
		Vector4__ctor_m2728(&L_16, (((float)L_13)), (((float)L_15)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_SetVector_m2729(L_11, _stringLiteral11, L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_17 = ___sourceTexture;
		RenderTexture_t15 * L_18 = ___destTexture;
		Material_t2 * L_19 = CameraFilterPack_TV_LED_get_material_m1197(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		goto IL_00c7;
	}

IL_00c0:
	{
		RenderTexture_t15 * L_20 = ___sourceTexture;
		RenderTexture_t15 * L_21 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_LED::Update()
extern "C" void CameraFilterPack_TV_LED_Update_m1200 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_LED::OnDisable()
extern "C" void CameraFilterPack_TV_LED_OnDisable_m1201 (CameraFilterPack_TV_LED_t185 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_7);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_7);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Noise.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Noise
#include "AssemblyU2DCSharp_CameraFilterPack_TV_NoiseMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Noise::.ctor()
extern "C" void CameraFilterPack_TV_Noise__ctor_m1202 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (0.01f);
		__this->___LightBackGround_7 = (0.5f);
		__this->___Speed_8 = (1.0f);
		__this->___Size2_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Noise::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Noise_get_material_m1203 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Noise::Start()
extern TypeInfo* CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral272;
extern "C" void CameraFilterPack_TV_Noise_Start_m1204 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(137);
		_stringLiteral272 = il2cpp_codegen_string_literal_from_index(272);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___LightBackGround_7);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Speed_8);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Size2_9);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral272, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Noise_OnRenderImage_m1205 (CameraFilterPack_TV_Noise_t186 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Noise_get_material_m1203(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Noise_get_material_m1203(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_Noise_get_material_m1203(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___LightBackGround_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_Noise_get_material_m1203(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Speed_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_Noise_get_material_m1203(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Size2_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_Noise_get_material_m1203(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_Noise_get_material_m1203(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Noise::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Noise_OnValidate_m1206 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(137);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___LightBackGround_7);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Speed_8);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Size2_9);
		((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_Noise::Update()
extern TypeInfo* CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Noise_Update_m1207 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(137);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Size_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___LightBackGround_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Speed_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_Noise_t186_StaticFields*)CameraFilterPack_TV_Noise_t186_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Size2_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Noise::OnDisable()
extern "C" void CameraFilterPack_TV_Noise_OnDisable_m1208 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Old
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Old
#include "AssemblyU2DCSharp_CameraFilterPack_TV_OldMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Old::.ctor()
extern "C" void CameraFilterPack_TV_Old__ctor_m1209 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Distortion_4 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Old::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Old_get_material_m1210 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Old::Start()
extern TypeInfo* CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral273;
extern "C" void CameraFilterPack_TV_Old_Start_m1211 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(138);
		_stringLiteral273 = il2cpp_codegen_string_literal_from_index(273);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Old_t187_StaticFields*)CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral273, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_TV_Old_OnRenderImage_m1212 (CameraFilterPack_TV_Old_t187 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Old_get_material_m1210(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Old_get_material_m1210(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_9 = ___sourceTexture;
		RenderTexture_t15 * L_10 = ___destTexture;
		Material_t2 * L_11 = CameraFilterPack_TV_Old_get_material_m1210(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007c:
	{
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Old_OnValidate_m1213 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(138);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Old_t187_StaticFields*)CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_TV_Old::Update()
extern TypeInfo* CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Old_Update_m1214 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(138);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Old_t187_StaticFields*)CameraFilterPack_TV_Old_t187_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6;
		__this->___Distortion_4 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old::OnDisable()
extern "C" void CameraFilterPack_TV_Old_OnDisable_m1215 (CameraFilterPack_TV_Old_t187 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Old_Movie
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Old_Movie
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_MovieMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Old_Movie::.ctor()
extern "C" void CameraFilterPack_TV_Old_Movie__ctor_m1216 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Distortion_4 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Old_Movie::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Old_Movie_get_material_m1217 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie::Start()
extern TypeInfo* CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral274;
extern "C" void CameraFilterPack_TV_Old_Movie_Start_m1218 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		_stringLiteral274 = il2cpp_codegen_string_literal_from_index(274);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Old_Movie_t188_StaticFields*)CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral274, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_TV_Old_Movie_OnRenderImage_m1219 (CameraFilterPack_TV_Old_Movie_t188 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Old_Movie_get_material_m1217(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Old_Movie_get_material_m1217(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_9 = ___sourceTexture;
		RenderTexture_t15 * L_10 = ___destTexture;
		Material_t2 * L_11 = CameraFilterPack_TV_Old_Movie_get_material_m1217(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007c:
	{
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Old_Movie_OnValidate_m1220 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Old_Movie_t188_StaticFields*)CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie::Update()
extern TypeInfo* CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Old_Movie_Update_m1221 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(139);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Old_Movie_t188_StaticFields*)CameraFilterPack_TV_Old_Movie_t188_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6;
		__this->___Distortion_4 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie::OnDisable()
extern "C" void CameraFilterPack_TV_Old_Movie_OnDisable_m1222 (CameraFilterPack_TV_Old_Movie_t188 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Old_Movie_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie_2.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Old_Movie_2
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Old_Movie_2MethodDeclarations.h"



// System.Void CameraFilterPack_TV_Old_Movie_2::.ctor()
extern "C" void CameraFilterPack_TV_Old_Movie_2__ctor_m1223 (CameraFilterPack_TV_Old_Movie_2_t189 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___FramePerSecond_6 = (15.0f);
		__this->___Contrast_7 = (1.0f);
		__this->___SceneCut_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Old_Movie_2::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Old_Movie_2_get_material_m1224 (CameraFilterPack_TV_Old_Movie_2_t189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie_2::Start()
extern TypeInfo* CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral275;
extern "C" void CameraFilterPack_TV_Old_Movie_2_Start_m1225 (CameraFilterPack_TV_Old_Movie_2_t189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___FramePerSecond_6);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Contrast_7);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Burn_8);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___SceneCut_9);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral275, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie_2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Old_Movie_2_OnRenderImage_m1226 (CameraFilterPack_TV_Old_Movie_2_t189 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Old_Movie_2_get_material_m1224(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Old_Movie_2_get_material_m1224(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___FramePerSecond_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_Old_Movie_2_get_material_m1224(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Contrast_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_Old_Movie_2_get_material_m1224(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Burn_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_Old_Movie_2_get_material_m1224(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___SceneCut_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_Old_Movie_2_get_material_m1224(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_Old_Movie_2_get_material_m1224(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie_2::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Old_Movie_2_OnValidate_m1227 (CameraFilterPack_TV_Old_Movie_2_t189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___FramePerSecond_6);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Contrast_7);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Burn_8);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___SceneCut_9);
		((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie_2::Update()
extern TypeInfo* CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Old_Movie_2_Update_m1228 (CameraFilterPack_TV_Old_Movie_2_t189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___FramePerSecond_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Contrast_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Burn_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_Old_Movie_2_t189_StaticFields*)CameraFilterPack_TV_Old_Movie_2_t189_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___SceneCut_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Old_Movie_2::OnDisable()
extern "C" void CameraFilterPack_TV_Old_Movie_2_OnDisable_m1229 (CameraFilterPack_TV_Old_Movie_2_t189 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_PlanetMars
#include "AssemblyU2DCSharp_CameraFilterPack_TV_PlanetMars.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_PlanetMars
#include "AssemblyU2DCSharp_CameraFilterPack_TV_PlanetMarsMethodDeclarations.h"



// System.Void CameraFilterPack_TV_PlanetMars::.ctor()
extern "C" void CameraFilterPack_TV_PlanetMars__ctor_m1230 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_4 = (1.0f);
		__this->___Distortion_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_PlanetMars::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_PlanetMars_get_material_m1231 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_PlanetMars::Start()
extern Il2CppCodeGenString* _stringLiteral276;
extern "C" void CameraFilterPack_TV_PlanetMars_Start_m1232 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral276 = il2cpp_codegen_string_literal_from_index(276);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral276, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_PlanetMars::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_PlanetMars_OnRenderImage_m1233 (CameraFilterPack_TV_PlanetMars_t190 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_PlanetMars_get_material_m1231(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_PlanetMars_get_material_m1231(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_PlanetMars_get_material_m1231(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_TV_PlanetMars_get_material_m1231(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_PlanetMars::Update()
extern "C" void CameraFilterPack_TV_PlanetMars_Update_m1234 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_PlanetMars::OnDisable()
extern "C" void CameraFilterPack_TV_PlanetMars_OnDisable_m1235 (CameraFilterPack_TV_PlanetMars_t190 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Posterize
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Posterize.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Posterize
#include "AssemblyU2DCSharp_CameraFilterPack_TV_PosterizeMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Posterize::.ctor()
extern "C" void CameraFilterPack_TV_Posterize__ctor_m1236 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Posterize_4 = (64.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Posterize::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Posterize_get_material_m1237 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Posterize::Start()
extern TypeInfo* CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral277;
extern "C" void CameraFilterPack_TV_Posterize_Start_m1238 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		_stringLiteral277 = il2cpp_codegen_string_literal_from_index(277);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Posterize_4);
		((CameraFilterPack_TV_Posterize_t191_StaticFields*)CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var->static_fields)->___ChangePosterize_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral277, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Posterize::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_TV_Posterize_OnRenderImage_m1239 (CameraFilterPack_TV_Posterize_t191 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Posterize_get_material_m1237(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Posterize_get_material_m1237(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Posterize_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_9 = ___sourceTexture;
		RenderTexture_t15 * L_10 = ___destTexture;
		Material_t2 * L_11 = CameraFilterPack_TV_Posterize_get_material_m1237(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007c:
	{
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Posterize::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Posterize_OnValidate_m1240 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Posterize_4);
		((CameraFilterPack_TV_Posterize_t191_StaticFields*)CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var->static_fields)->___ChangePosterize_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_TV_Posterize::Update()
extern TypeInfo* CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Posterize_Update_m1241 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Posterize_t191_StaticFields*)CameraFilterPack_TV_Posterize_t191_il2cpp_TypeInfo_var->static_fields)->___ChangePosterize_6;
		__this->___Posterize_4 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Posterize::OnDisable()
extern "C" void CameraFilterPack_TV_Posterize_OnDisable_m1242 (CameraFilterPack_TV_Posterize_t191 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Rgb
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Rgb.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Rgb
#include "AssemblyU2DCSharp_CameraFilterPack_TV_RgbMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Rgb::.ctor()
extern "C" void CameraFilterPack_TV_Rgb__ctor_m1243 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_4 = (1.0f);
		__this->___Distortion_5 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Rgb::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Rgb_get_material_m1244 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_6 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_6);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_6);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Rgb::Start()
extern Il2CppCodeGenString* _stringLiteral278;
extern "C" void CameraFilterPack_TV_Rgb_Start_m1245 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral278 = il2cpp_codegen_string_literal_from_index(278);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral278, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Rgb::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Rgb_OnRenderImage_m1246 (CameraFilterPack_TV_Rgb_t192 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_4);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_4 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_4);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_4 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Rgb_get_material_m1244(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_4);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Rgb_get_material_m1244(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_5);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_Rgb_get_material_m1244(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_TV_Rgb_get_material_m1244(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Rgb::Update()
extern "C" void CameraFilterPack_TV_Rgb_Update_m1247 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Rgb::OnDisable()
extern "C" void CameraFilterPack_TV_Rgb_OnDisable_m1248 (CameraFilterPack_TV_Rgb_t192 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_6);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_6);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Tiles
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Tiles.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Tiles
#include "AssemblyU2DCSharp_CameraFilterPack_TV_TilesMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Tiles::.ctor()
extern "C" void CameraFilterPack_TV_Tiles__ctor_m1249 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (1.0f);
		__this->___Intensity_7 = (4.0f);
		__this->___StretchX_8 = (0.6f);
		__this->___StretchY_9 = (0.4f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Tiles::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Tiles_get_material_m1250 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Tiles::Start()
extern TypeInfo* CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral279;
extern "C" void CameraFilterPack_TV_Tiles_Start_m1251 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		_stringLiteral279 = il2cpp_codegen_string_literal_from_index(279);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Intensity_7);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral279, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Tiles::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Tiles_OnRenderImage_m1252 (CameraFilterPack_TV_Tiles_t193 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Tiles_get_material_m1250(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Tiles_get_material_m1250(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_Tiles_get_material_m1250(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Intensity_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_Tiles_get_material_m1250(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___StretchX_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_Tiles_get_material_m1250(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___StretchY_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_Tiles_get_material_m1250(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_Tiles_get_material_m1250(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Tiles::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Tiles_OnValidate_m1253 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Intensity_7);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_Tiles::Update()
extern TypeInfo* CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Tiles_Update_m1254 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Size_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Intensity_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___StretchX_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_Tiles_t193_StaticFields*)CameraFilterPack_TV_Tiles_t193_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___StretchY_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Tiles::OnDisable()
extern "C" void CameraFilterPack_TV_Tiles_OnDisable_m1255 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_VHS
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_VHS
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHSMethodDeclarations.h"



// System.Void CameraFilterPack_TV_VHS::.ctor()
extern "C" void CameraFilterPack_TV_VHS__ctor_m1256 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Cryptage_6 = (64.0f);
		__this->___Parasite_7 = (32.0f);
		__this->___WhiteParasite_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_VHS::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_VHS_get_material_m1257 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_VHS::Start()
extern TypeInfo* CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral280;
extern "C" void CameraFilterPack_TV_VHS_Start_m1258 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(143);
		_stringLiteral280 = il2cpp_codegen_string_literal_from_index(280);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Cryptage_6);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Parasite_7);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Calibrage_8);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___WhiteParasite_9);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral280, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_VHS_OnRenderImage_m1259 (CameraFilterPack_TV_VHS_t194 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_VHS_get_material_m1257(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_VHS_get_material_m1257(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Cryptage_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_VHS_get_material_m1257(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Parasite_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_VHS_get_material_m1257(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Calibrage_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_VHS_get_material_m1257(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___WhiteParasite_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_VHS_get_material_m1257(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_VHS_get_material_m1257(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS::OnValidate()
extern TypeInfo* CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_VHS_OnValidate_m1260 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(143);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Cryptage_6);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Parasite_7);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Calibrage_8);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___WhiteParasite_9);
		((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS::Update()
extern TypeInfo* CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_VHS_Update_m1261 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(143);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Cryptage_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Parasite_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Calibrage_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_VHS_t194_StaticFields*)CameraFilterPack_TV_VHS_t194_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___WhiteParasite_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS::OnDisable()
extern "C" void CameraFilterPack_TV_VHS_OnDisable_m1262 (CameraFilterPack_TV_VHS_t194 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_VHS_Rewind
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS_Rewind.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_VHS_Rewind
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VHS_RewindMethodDeclarations.h"



// System.Void CameraFilterPack_TV_VHS_Rewind::.ctor()
extern "C" void CameraFilterPack_TV_VHS_Rewind__ctor_m1263 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Cryptage_6 = (1.0f);
		__this->___Parasite_7 = (9.0f);
		__this->___Parasite2_8 = (12.0f);
		__this->___WhiteParasite_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_VHS_Rewind::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_VHS_Rewind_get_material_m1264 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_VHS_Rewind::Start()
extern TypeInfo* CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral281;
extern "C" void CameraFilterPack_TV_VHS_Rewind_Start_m1265 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		_stringLiteral281 = il2cpp_codegen_string_literal_from_index(281);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Cryptage_6);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Parasite_7);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Parasite2_8);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___WhiteParasite_9);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral281, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS_Rewind::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_VHS_Rewind_OnRenderImage_m1266 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_VHS_Rewind_get_material_m1264(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_VHS_Rewind_get_material_m1264(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Cryptage_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_VHS_Rewind_get_material_m1264(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Parasite_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_VHS_Rewind_get_material_m1264(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Parasite2_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_VHS_Rewind_get_material_m1264(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___WhiteParasite_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_VHS_Rewind_get_material_m1264(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_VHS_Rewind_get_material_m1264(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS_Rewind::OnValidate()
extern TypeInfo* CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_VHS_Rewind_OnValidate_m1267 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Cryptage_6);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Parasite_7);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Parasite2_8);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___WhiteParasite_9);
		((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS_Rewind::Update()
extern TypeInfo* CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_VHS_Rewind_Update_m1268 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(144);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Cryptage_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Parasite_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Parasite2_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_VHS_Rewind_t195_StaticFields*)CameraFilterPack_TV_VHS_Rewind_t195_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___WhiteParasite_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_VHS_Rewind::OnDisable()
extern "C" void CameraFilterPack_TV_VHS_Rewind_OnDisable_m1269 (CameraFilterPack_TV_VHS_Rewind_t195 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Vcr
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Vcr.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Vcr
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VcrMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Vcr::.ctor()
extern "C" void CameraFilterPack_TV_Vcr__ctor_m1270 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Distortion_4 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Vcr::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Vcr_get_material_m1271 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Vcr::Start()
extern TypeInfo* CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral282;
extern "C" void CameraFilterPack_TV_Vcr_Start_m1272 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		_stringLiteral282 = il2cpp_codegen_string_literal_from_index(282);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Vcr_t196_StaticFields*)CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral282, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Vcr::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_TV_Vcr_OnRenderImage_m1273 (CameraFilterPack_TV_Vcr_t196 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Vcr_get_material_m1271(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Vcr_get_material_m1271(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_9 = ___sourceTexture;
		RenderTexture_t15 * L_10 = ___destTexture;
		Material_t2 * L_11 = CameraFilterPack_TV_Vcr_get_material_m1271(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007c:
	{
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Vcr::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Vcr_OnValidate_m1274 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Vcr_t196_StaticFields*)CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_TV_Vcr::Update()
extern TypeInfo* CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Vcr_Update_m1275 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(145);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Vcr_t196_StaticFields*)CameraFilterPack_TV_Vcr_t196_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6;
		__this->___Distortion_4 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Vcr::OnDisable()
extern "C" void CameraFilterPack_TV_Vcr_OnDisable_m1276 (CameraFilterPack_TV_Vcr_t196 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Video3D
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Video3D.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Video3D
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Video3DMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Video3D::.ctor()
extern "C" void CameraFilterPack_TV_Video3D__ctor_m1277 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Video3D::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Video3D_get_material_m1278 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Video3D::Start()
extern Il2CppCodeGenString* _stringLiteral283;
extern "C" void CameraFilterPack_TV_Video3D_Start_m1279 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral283 = il2cpp_codegen_string_literal_from_index(283);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral283, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Video3D::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Video3D_OnRenderImage_m1280 (CameraFilterPack_TV_Video3D_t197 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Video3D_get_material_m1278(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Video3D_get_material_m1278(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_TV_Video3D_get_material_m1278(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Video3D::Update()
extern "C" void CameraFilterPack_TV_Video3D_Update_m1281 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Video3D::OnDisable()
extern "C" void CameraFilterPack_TV_Video3D_OnDisable_m1282 (CameraFilterPack_TV_Video3D_t197 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Videoflip
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Videoflip.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Videoflip
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VideoflipMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Videoflip::.ctor()
extern "C" void CameraFilterPack_TV_Videoflip__ctor_m1283 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Videoflip::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Videoflip_get_material_m1284 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Videoflip::Start()
extern Il2CppCodeGenString* _stringLiteral284;
extern "C" void CameraFilterPack_TV_Videoflip_Start_m1285 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral284 = il2cpp_codegen_string_literal_from_index(284);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral284, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_0;
		bool L_1 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Videoflip::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_Videoflip_OnRenderImage_m1286 (CameraFilterPack_TV_Videoflip_t198 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0093;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Videoflip_get_material_m1284(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Videoflip_get_material_m1284(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_8 = ___sourceTexture;
		NullCheck(L_8);
		int32_t L_9 = RenderTexture_get_width_m2726(L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_height_m2727(L_10, /*hidden argument*/NULL);
		Vector4_t5  L_12 = {0};
		Vector4__ctor_m2728(&L_12, (((float)L_9)), (((float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_7);
		Material_SetVector_m2729(L_7, _stringLiteral11, L_12, /*hidden argument*/NULL);
		RenderTexture_t15 * L_13 = ___sourceTexture;
		RenderTexture_t15 * L_14 = ___destTexture;
		Material_t2 * L_15 = CameraFilterPack_TV_Videoflip_get_material_m1284(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0093:
	{
		RenderTexture_t15 * L_16 = ___sourceTexture;
		RenderTexture_t15 * L_17 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Videoflip::Update()
extern "C" void CameraFilterPack_TV_Videoflip_Update_m1287 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Videoflip::OnDisable()
extern "C" void CameraFilterPack_TV_Videoflip_OnDisable_m1288 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_Vintage
#include "AssemblyU2DCSharp_CameraFilterPack_TV_Vintage.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_Vintage
#include "AssemblyU2DCSharp_CameraFilterPack_TV_VintageMethodDeclarations.h"



// System.Void CameraFilterPack_TV_Vintage::.ctor()
extern "C" void CameraFilterPack_TV_Vintage__ctor_m1289 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Distortion_4 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_Vintage::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_Vintage_get_material_m1290 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_Vintage::Start()
extern TypeInfo* CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral285;
extern "C" void CameraFilterPack_TV_Vintage_Start_m1291 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(146);
		_stringLiteral285 = il2cpp_codegen_string_literal_from_index(285);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Vintage_t199_StaticFields*)CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral285, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Vintage::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral16;
extern "C" void CameraFilterPack_TV_Vintage_OnRenderImage_m1292 (CameraFilterPack_TV_Vintage_t199 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007c;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_Vintage_get_material_m1290(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_Vintage_get_material_m1290(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Distortion_4);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral16, L_8, /*hidden argument*/NULL);
		RenderTexture_t15 * L_9 = ___sourceTexture;
		RenderTexture_t15 * L_10 = ___destTexture;
		Material_t2 * L_11 = CameraFilterPack_TV_Vintage_get_material_m1290(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0083;
	}

IL_007c:
	{
		RenderTexture_t15 * L_12 = ___sourceTexture;
		RenderTexture_t15 * L_13 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
	}

IL_0083:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Vintage::OnValidate()
extern TypeInfo* CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Vintage_OnValidate_m1293 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(146);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distortion_4);
		((CameraFilterPack_TV_Vintage_t199_StaticFields*)CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_TV_Vintage::Update()
extern TypeInfo* CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_Vintage_Update_m1294 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(146);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_Vintage_t199_StaticFields*)CameraFilterPack_TV_Vintage_t199_il2cpp_TypeInfo_var->static_fields)->___ChangeDistortion_6;
		__this->___Distortion_4 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_Vintage::OnDisable()
extern "C" void CameraFilterPack_TV_Vintage_OnDisable_m1295 (CameraFilterPack_TV_Vintage_t199 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_WideScreenCircle
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenCircle.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_WideScreenCircle
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenCircleMethodDeclarations.h"



// System.Void CameraFilterPack_TV_WideScreenCircle::.ctor()
extern "C" void CameraFilterPack_TV_WideScreenCircle__ctor_m1296 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (0.55f);
		__this->___Smooth_7 = (0.01f);
		__this->___StretchX_8 = (1.0f);
		__this->___StretchY_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_WideScreenCircle::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_WideScreenCircle_get_material_m1297 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_WideScreenCircle::Start()
extern TypeInfo* CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral286;
extern "C" void CameraFilterPack_TV_WideScreenCircle_Start_m1298 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		_stringLiteral286 = il2cpp_codegen_string_literal_from_index(286);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral286, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenCircle::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_WideScreenCircle_OnRenderImage_m1299 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_WideScreenCircle_get_material_m1297(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_WideScreenCircle_get_material_m1297(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_WideScreenCircle_get_material_m1297(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Smooth_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_WideScreenCircle_get_material_m1297(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___StretchX_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_WideScreenCircle_get_material_m1297(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___StretchY_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_WideScreenCircle_get_material_m1297(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_WideScreenCircle_get_material_m1297(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenCircle::OnValidate()
extern TypeInfo* CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenCircle_OnValidate_m1300 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenCircle::Update()
extern TypeInfo* CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenCircle_Update_m1301 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Size_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Smooth_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___StretchX_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_WideScreenCircle_t200_StaticFields*)CameraFilterPack_TV_WideScreenCircle_t200_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___StretchY_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenCircle::OnDisable()
extern "C" void CameraFilterPack_TV_WideScreenCircle_OnDisable_m1302 (CameraFilterPack_TV_WideScreenCircle_t200 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_WideScreenHV
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHV.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_WideScreenHV
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHVMethodDeclarations.h"



// System.Void CameraFilterPack_TV_WideScreenHV::.ctor()
extern "C" void CameraFilterPack_TV_WideScreenHV__ctor_m1303 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (0.55f);
		__this->___Smooth_7 = (0.01f);
		__this->___StretchX_8 = (1.0f);
		__this->___StretchY_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_WideScreenHV::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_WideScreenHV_get_material_m1304 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHV::Start()
extern TypeInfo* CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral287;
extern "C" void CameraFilterPack_TV_WideScreenHV_Start_m1305 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		_stringLiteral287 = il2cpp_codegen_string_literal_from_index(287);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral287, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHV::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_WideScreenHV_OnRenderImage_m1306 (CameraFilterPack_TV_WideScreenHV_t201 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_WideScreenHV_get_material_m1304(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_WideScreenHV_get_material_m1304(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_WideScreenHV_get_material_m1304(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Smooth_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_WideScreenHV_get_material_m1304(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___StretchX_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_WideScreenHV_get_material_m1304(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___StretchY_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_WideScreenHV_get_material_m1304(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_WideScreenHV_get_material_m1304(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHV::OnValidate()
extern TypeInfo* CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenHV_OnValidate_m1307 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHV::Update()
extern TypeInfo* CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenHV_Update_m1308 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Size_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Smooth_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___StretchX_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_WideScreenHV_t201_StaticFields*)CameraFilterPack_TV_WideScreenHV_t201_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___StretchY_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHV::OnDisable()
extern "C" void CameraFilterPack_TV_WideScreenHV_OnDisable_m1309 (CameraFilterPack_TV_WideScreenHV_t201 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_WideScreenHorizontal
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHorizontal.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_WideScreenHorizontal
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenHorizontalMethodDeclarations.h"



// System.Void CameraFilterPack_TV_WideScreenHorizontal::.ctor()
extern "C" void CameraFilterPack_TV_WideScreenHorizontal__ctor_m1310 (CameraFilterPack_TV_WideScreenHorizontal_t202 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (0.55f);
		__this->___Smooth_7 = (0.01f);
		__this->___StretchX_8 = (1.0f);
		__this->___StretchY_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_WideScreenHorizontal::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311 (CameraFilterPack_TV_WideScreenHorizontal_t202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHorizontal::Start()
extern TypeInfo* CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral288;
extern "C" void CameraFilterPack_TV_WideScreenHorizontal_Start_m1312 (CameraFilterPack_TV_WideScreenHorizontal_t202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral288, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHorizontal::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_WideScreenHorizontal_OnRenderImage_m1313 (CameraFilterPack_TV_WideScreenHorizontal_t202 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Smooth_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___StretchX_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___StretchY_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_WideScreenHorizontal_get_material_m1311(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHorizontal::OnValidate()
extern TypeInfo* CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenHorizontal_OnValidate_m1314 (CameraFilterPack_TV_WideScreenHorizontal_t202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHorizontal::Update()
extern TypeInfo* CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenHorizontal_Update_m1315 (CameraFilterPack_TV_WideScreenHorizontal_t202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(149);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Size_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Smooth_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___StretchX_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields*)CameraFilterPack_TV_WideScreenHorizontal_t202_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___StretchY_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenHorizontal::OnDisable()
extern "C" void CameraFilterPack_TV_WideScreenHorizontal_OnDisable_m1316 (CameraFilterPack_TV_WideScreenHorizontal_t202 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_TV_WideScreenVertical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenVertical.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_TV_WideScreenVertical
#include "AssemblyU2DCSharp_CameraFilterPack_TV_WideScreenVerticalMethodDeclarations.h"



// System.Void CameraFilterPack_TV_WideScreenVertical::.ctor()
extern "C" void CameraFilterPack_TV_WideScreenVertical__ctor_m1317 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Size_6 = (0.55f);
		__this->___Smooth_7 = (0.01f);
		__this->___StretchX_8 = (1.0f);
		__this->___StretchY_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_TV_WideScreenVertical::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_TV_WideScreenVertical_get_material_m1318 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_TV_WideScreenVertical::Start()
extern TypeInfo* CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral289;
extern "C" void CameraFilterPack_TV_WideScreenVertical_Start_m1319 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(150);
		_stringLiteral289 = il2cpp_codegen_string_literal_from_index(289);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral289, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenVertical::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_TV_WideScreenVertical_OnRenderImage_m1320 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_TV_WideScreenVertical_get_material_m1318(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_TV_WideScreenVertical_get_material_m1318(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Size_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_TV_WideScreenVertical_get_material_m1318(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Smooth_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_TV_WideScreenVertical_get_material_m1318(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___StretchX_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_TV_WideScreenVertical_get_material_m1318(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___StretchY_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_TV_WideScreenVertical_get_material_m1318(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_TV_WideScreenVertical_get_material_m1318(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenVertical::OnValidate()
extern TypeInfo* CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenVertical_OnValidate_m1321 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(150);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Size_6);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Smooth_7);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___StretchX_8);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___StretchY_9);
		((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenVertical::Update()
extern TypeInfo* CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_TV_WideScreenVertical_Update_m1322 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(150);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Size_6 = L_1;
		float L_2 = ((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Smooth_7 = L_2;
		float L_3 = ((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___StretchX_8 = L_3;
		float L_4 = ((CameraFilterPack_TV_WideScreenVertical_t203_StaticFields*)CameraFilterPack_TV_WideScreenVertical_t203_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___StretchY_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_TV_WideScreenVertical::OnDisable()
extern "C" void CameraFilterPack_TV_WideScreenVertical_OnDisable_m1323 (CameraFilterPack_TV_WideScreenVertical_t203 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_VHS_Tracking
#include "AssemblyU2DCSharp_CameraFilterPack_VHS_Tracking.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_VHS_Tracking
#include "AssemblyU2DCSharp_CameraFilterPack_VHS_TrackingMethodDeclarations.h"



// System.Void CameraFilterPack_VHS_Tracking::.ctor()
extern "C" void CameraFilterPack_VHS_Tracking__ctor_m1324 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Tracking_6 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_VHS_Tracking::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_VHS_Tracking_get_material_m1325 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_VHS_Tracking::Start()
extern TypeInfo* CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral290;
extern "C" void CameraFilterPack_VHS_Tracking_Start_m1326 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(151);
		_stringLiteral290 = il2cpp_codegen_string_literal_from_index(290);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Tracking_6);
		((CameraFilterPack_VHS_Tracking_t204_StaticFields*)CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var->static_fields)->___ChangeTracking_7 = L_0;
		Shader_t1 * L_1 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral290, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_1;
		bool L_2 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_002d;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		return;
	}
}
// System.Void CameraFilterPack_VHS_Tracking::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_VHS_Tracking_OnRenderImage_m1327 (CameraFilterPack_VHS_Tracking_t204 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00a9;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_VHS_Tracking_get_material_m1325(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_VHS_Tracking_get_material_m1325(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Tracking_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_VHS_Tracking_get_material_m1325(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_10 = ___sourceTexture;
		NullCheck(L_10);
		int32_t L_11 = RenderTexture_get_width_m2726(L_10, /*hidden argument*/NULL);
		RenderTexture_t15 * L_12 = ___sourceTexture;
		NullCheck(L_12);
		int32_t L_13 = RenderTexture_get_height_m2727(L_12, /*hidden argument*/NULL);
		Vector4_t5  L_14 = {0};
		Vector4__ctor_m2728(&L_14, (((float)L_11)), (((float)L_13)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		Material_SetVector_m2729(L_9, _stringLiteral11, L_14, /*hidden argument*/NULL);
		RenderTexture_t15 * L_15 = ___sourceTexture;
		RenderTexture_t15 * L_16 = ___destTexture;
		Material_t2 * L_17 = CameraFilterPack_VHS_Tracking_get_material_m1325(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		goto IL_00b0;
	}

IL_00a9:
	{
		RenderTexture_t15 * L_18 = ___sourceTexture;
		RenderTexture_t15 * L_19 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		return;
	}
}
// System.Void CameraFilterPack_VHS_Tracking::OnValidate()
extern TypeInfo* CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_VHS_Tracking_OnValidate_m1328 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(151);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Tracking_6);
		((CameraFilterPack_VHS_Tracking_t204_StaticFields*)CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var->static_fields)->___ChangeTracking_7 = L_0;
		return;
	}
}
// System.Void CameraFilterPack_VHS_Tracking::Update()
extern TypeInfo* CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_VHS_Tracking_Update_m1329 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(151);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = ((CameraFilterPack_VHS_Tracking_t204_StaticFields*)CameraFilterPack_VHS_Tracking_t204_il2cpp_TypeInfo_var->static_fields)->___ChangeTracking_7;
		__this->___Tracking_6 = L_1;
	}

IL_0015:
	{
		return;
	}
}
// System.Void CameraFilterPack_VHS_Tracking::OnDisable()
extern "C" void CameraFilterPack_VHS_Tracking_OnDisable_m1330 (CameraFilterPack_VHS_Tracking_t204 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Vision_Blood
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Blood.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Vision_Blood
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_BloodMethodDeclarations.h"



// System.Void CameraFilterPack_Vision_Blood::.ctor()
extern "C" void CameraFilterPack_Vision_Blood__ctor_m1331 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___HoleSize_6 = (0.6f);
		__this->___HoleSmooth_7 = (0.3f);
		__this->___Color1_8 = (0.2f);
		__this->___Color2_9 = (0.9f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Vision_Blood::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Vision_Blood_get_material_m1332 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Vision_Blood::Start()
extern TypeInfo* CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral291;
extern "C" void CameraFilterPack_Vision_Blood_Start_m1333 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		_stringLiteral291 = il2cpp_codegen_string_literal_from_index(291);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___HoleSize_6);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___HoleSmooth_7);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Color1_8);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Color2_9);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral291, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Blood::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Vision_Blood_OnRenderImage_m1334 (CameraFilterPack_Vision_Blood_t205 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Vision_Blood_get_material_m1332(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Vision_Blood_get_material_m1332(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___HoleSize_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Vision_Blood_get_material_m1332(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___HoleSmooth_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Vision_Blood_get_material_m1332(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Color1_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Vision_Blood_get_material_m1332(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Color2_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Vision_Blood_get_material_m1332(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Vision_Blood_get_material_m1332(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Blood::OnValidate()
extern TypeInfo* CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Blood_OnValidate_m1335 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___HoleSize_6);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___HoleSmooth_7);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Color1_8);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Color2_9);
		((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Vision_Blood::Update()
extern TypeInfo* CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Blood_Update_m1336 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(152);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___HoleSize_6 = L_1;
		float L_2 = ((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___HoleSmooth_7 = L_2;
		float L_3 = ((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Color1_8 = L_3;
		float L_4 = ((CameraFilterPack_Vision_Blood_t205_StaticFields*)CameraFilterPack_Vision_Blood_t205_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Color2_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Blood::OnDisable()
extern "C" void CameraFilterPack_Vision_Blood_OnDisable_m1337 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Vision_Crystal
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Crystal.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Vision_Crystal
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_CrystalMethodDeclarations.h"



// System.Void CameraFilterPack_Vision_Crystal::.ctor()
extern "C" void CameraFilterPack_Vision_Crystal__ctor_m1338 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (1.0f);
		__this->___X_7 = (1.0f);
		__this->___Y_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Vision_Crystal::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Vision_Crystal_get_material_m1339 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Vision_Crystal::Start()
extern TypeInfo* CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral292;
extern "C" void CameraFilterPack_Vision_Crystal_Start_m1340 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		_stringLiteral292 = il2cpp_codegen_string_literal_from_index(292);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___X_7);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Y_8);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral292, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Crystal::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Vision_Crystal_OnRenderImage_m1341 (CameraFilterPack_Vision_Crystal_t206 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Vision_Crystal_get_material_m1339(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Vision_Crystal_get_material_m1339(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Vision_Crystal_get_material_m1339(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___X_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Vision_Crystal_get_material_m1339(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Y_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Vision_Crystal_get_material_m1339(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Vision_Crystal_get_material_m1339(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Vision_Crystal_get_material_m1339(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Crystal::OnValidate()
extern TypeInfo* CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Crystal_OnValidate_m1342 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___X_7);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Y_8);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Vision_Crystal::Update()
extern TypeInfo* CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Crystal_Update_m1343 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Value_6 = L_1;
		float L_2 = ((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___X_7 = L_2;
		float L_3 = ((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Y_8 = L_3;
		float L_4 = ((CameraFilterPack_Vision_Crystal_t206_StaticFields*)CameraFilterPack_Vision_Crystal_t206_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Crystal::OnDisable()
extern "C" void CameraFilterPack_Vision_Crystal_OnDisable_m1344 (CameraFilterPack_Vision_Crystal_t206 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Vision_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Plasma.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Vision_Plasma
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_PlasmaMethodDeclarations.h"



// System.Void CameraFilterPack_Vision_Plasma::.ctor()
extern "C" void CameraFilterPack_Vision_Plasma__ctor_m1345 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (0.6f);
		__this->___Value2_7 = (0.2f);
		__this->___Intensity_8 = (15.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Vision_Plasma::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Vision_Plasma_get_material_m1346 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Vision_Plasma::Start()
extern TypeInfo* CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral293;
extern "C" void CameraFilterPack_Vision_Plasma_Start_m1347 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		_stringLiteral293 = il2cpp_codegen_string_literal_from_index(293);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Intensity_8);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral293, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Plasma::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Vision_Plasma_OnRenderImage_m1348 (CameraFilterPack_Vision_Plasma_t207 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Vision_Plasma_get_material_m1346(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Vision_Plasma_get_material_m1346(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Vision_Plasma_get_material_m1346(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Value2_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Vision_Plasma_get_material_m1346(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Intensity_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Vision_Plasma_get_material_m1346(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Vision_Plasma_get_material_m1346(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Vision_Plasma_get_material_m1346(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Plasma::OnValidate()
extern TypeInfo* CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Plasma_OnValidate_m1349 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Intensity_8);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Vision_Plasma::Update()
extern TypeInfo* CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Plasma_Update_m1350 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(154);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Value_6 = L_1;
		float L_2 = ((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Value2_7 = L_2;
		float L_3 = ((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Intensity_8 = L_3;
		float L_4 = ((CameraFilterPack_Vision_Plasma_t207_StaticFields*)CameraFilterPack_Vision_Plasma_t207_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Plasma::OnDisable()
extern "C" void CameraFilterPack_Vision_Plasma_OnDisable_m1351 (CameraFilterPack_Vision_Plasma_t207 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Vision_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Psycho.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Vision_Psycho
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_PsychoMethodDeclarations.h"



// System.Void CameraFilterPack_Vision_Psycho::.ctor()
extern "C" void CameraFilterPack_Vision_Psycho__ctor_m1352 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___HoleSize_6 = (0.6f);
		__this->___HoleSmooth_7 = (0.3f);
		__this->___Color1_8 = (0.2f);
		__this->___Color2_9 = (0.9f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Vision_Psycho::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Vision_Psycho_get_material_m1353 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Vision_Psycho::Start()
extern TypeInfo* CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral294;
extern "C" void CameraFilterPack_Vision_Psycho_Start_m1354 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		_stringLiteral294 = il2cpp_codegen_string_literal_from_index(294);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___HoleSize_6);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___HoleSmooth_7);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Color1_8);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Color2_9);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral294, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Psycho::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Vision_Psycho_OnRenderImage_m1355 (CameraFilterPack_Vision_Psycho_t208 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Vision_Psycho_get_material_m1353(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Vision_Psycho_get_material_m1353(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___HoleSize_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Vision_Psycho_get_material_m1353(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___HoleSmooth_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Vision_Psycho_get_material_m1353(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Color1_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Vision_Psycho_get_material_m1353(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Color2_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Vision_Psycho_get_material_m1353(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Vision_Psycho_get_material_m1353(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Psycho::OnValidate()
extern TypeInfo* CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Psycho_OnValidate_m1356 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___HoleSize_6);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___HoleSmooth_7);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Color1_8);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Color2_9);
		((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Vision_Psycho::Update()
extern TypeInfo* CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Psycho_Update_m1357 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(155);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___HoleSize_6 = L_1;
		float L_2 = ((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___HoleSmooth_7 = L_2;
		float L_3 = ((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Color1_8 = L_3;
		float L_4 = ((CameraFilterPack_Vision_Psycho_t208_StaticFields*)CameraFilterPack_Vision_Psycho_t208_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Color2_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Psycho::OnDisable()
extern "C" void CameraFilterPack_Vision_Psycho_OnDisable_m1358 (CameraFilterPack_Vision_Psycho_t208 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// CameraFilterPack_Vision_Tunnel
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_Tunnel.h"
#ifndef _MSC_VER
#else
#endif
// CameraFilterPack_Vision_Tunnel
#include "AssemblyU2DCSharp_CameraFilterPack_Vision_TunnelMethodDeclarations.h"



// System.Void CameraFilterPack_Vision_Tunnel::.ctor()
extern "C" void CameraFilterPack_Vision_Tunnel__ctor_m1359 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method)
{
	{
		__this->___TimeX_3 = (1.0f);
		__this->___Value_6 = (0.6f);
		__this->___Value2_7 = (0.4f);
		__this->___Intensity_8 = (1.0f);
		__this->___Value4_9 = (1.0f);
		MonoBehaviour__ctor_m2715(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material CameraFilterPack_Vision_Tunnel::get_material()
extern TypeInfo* Material_t2_il2cpp_TypeInfo_var;
extern "C" Material_t2 * CameraFilterPack_Vision_Tunnel_get_material_m1360 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Equality_m2716(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		Shader_t1 * L_2 = (__this->___SCShader_2);
		Material_t2 * L_3 = (Material_t2 *)il2cpp_codegen_object_new (Material_t2_il2cpp_TypeInfo_var);
		Material__ctor_m2717(L_3, L_2, /*hidden argument*/NULL);
		__this->___SCMaterial_5 = L_3;
		Material_t2 * L_4 = (__this->___SCMaterial_5);
		NullCheck(L_4);
		Object_set_hideFlags_m2718(L_4, ((int32_t)61), /*hidden argument*/NULL);
	}

IL_002f:
	{
		Material_t2 * L_5 = (__this->___SCMaterial_5);
		return L_5;
	}
}
// System.Void CameraFilterPack_Vision_Tunnel::Start()
extern TypeInfo* CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral295;
extern "C" void CameraFilterPack_Vision_Tunnel_Start_m1361 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(156);
		_stringLiteral295 = il2cpp_codegen_string_literal_from_index(295);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Intensity_8);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		Shader_t1 * L_4 = Shader_Find_m2719(NULL /*static, unused*/, _stringLiteral295, /*hidden argument*/NULL);
		__this->___SCShader_2 = L_4;
		bool L_5 = SystemInfo_get_supportsImageEffects_m2720(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004e;
		}
	}
	{
		Behaviour_set_enabled_m2721(__this, 0, /*hidden argument*/NULL);
		return;
	}

IL_004e:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Tunnel::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void CameraFilterPack_Vision_Tunnel_OnRenderImage_m1362 (CameraFilterPack_Vision_Tunnel_t209 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		Shader_t1 * L_0 = (__this->___SCShader_2);
		bool L_1 = Object_op_Inequality_m2722(NULL /*static, unused*/, L_0, (Object_t473 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00eb;
		}
	}
	{
		float L_2 = (__this->___TimeX_3);
		float L_3 = Time_get_deltaTime_m2723(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___TimeX_3 = ((float)((float)L_2+(float)L_3));
		float L_4 = (__this->___TimeX_3);
		if ((!(((float)L_4) > ((float)(100.0f)))))
		{
			goto IL_003e;
		}
	}
	{
		__this->___TimeX_3 = (0.0f);
	}

IL_003e:
	{
		Material_t2 * L_5 = CameraFilterPack_Vision_Tunnel_get_material_m1360(__this, /*hidden argument*/NULL);
		float L_6 = (__this->___TimeX_3);
		NullCheck(L_5);
		Material_SetFloat_m2724(L_5, _stringLiteral1, L_6, /*hidden argument*/NULL);
		Material_t2 * L_7 = CameraFilterPack_Vision_Tunnel_get_material_m1360(__this, /*hidden argument*/NULL);
		float L_8 = (__this->___Value_6);
		NullCheck(L_7);
		Material_SetFloat_m2724(L_7, _stringLiteral2, L_8, /*hidden argument*/NULL);
		Material_t2 * L_9 = CameraFilterPack_Vision_Tunnel_get_material_m1360(__this, /*hidden argument*/NULL);
		float L_10 = (__this->___Value2_7);
		NullCheck(L_9);
		Material_SetFloat_m2724(L_9, _stringLiteral3, L_10, /*hidden argument*/NULL);
		Material_t2 * L_11 = CameraFilterPack_Vision_Tunnel_get_material_m1360(__this, /*hidden argument*/NULL);
		float L_12 = (__this->___Intensity_8);
		NullCheck(L_11);
		Material_SetFloat_m2724(L_11, _stringLiteral22, L_12, /*hidden argument*/NULL);
		Material_t2 * L_13 = CameraFilterPack_Vision_Tunnel_get_material_m1360(__this, /*hidden argument*/NULL);
		float L_14 = (__this->___Value4_9);
		NullCheck(L_13);
		Material_SetFloat_m2724(L_13, _stringLiteral23, L_14, /*hidden argument*/NULL);
		Material_t2 * L_15 = CameraFilterPack_Vision_Tunnel_get_material_m1360(__this, /*hidden argument*/NULL);
		RenderTexture_t15 * L_16 = ___sourceTexture;
		NullCheck(L_16);
		int32_t L_17 = RenderTexture_get_width_m2726(L_16, /*hidden argument*/NULL);
		RenderTexture_t15 * L_18 = ___sourceTexture;
		NullCheck(L_18);
		int32_t L_19 = RenderTexture_get_height_m2727(L_18, /*hidden argument*/NULL);
		Vector4_t5  L_20 = {0};
		Vector4__ctor_m2728(&L_20, (((float)L_17)), (((float)L_19)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_15);
		Material_SetVector_m2729(L_15, _stringLiteral11, L_20, /*hidden argument*/NULL);
		RenderTexture_t15 * L_21 = ___sourceTexture;
		RenderTexture_t15 * L_22 = ___destTexture;
		Material_t2 * L_23 = CameraFilterPack_Vision_Tunnel_get_material_m1360(__this, /*hidden argument*/NULL);
		Graphics_Blit_m2730(NULL /*static, unused*/, L_21, L_22, L_23, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00eb:
	{
		RenderTexture_t15 * L_24 = ___sourceTexture;
		RenderTexture_t15 * L_25 = ___destTexture;
		Graphics_Blit_m2731(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Tunnel::OnValidate()
extern TypeInfo* CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Tunnel_OnValidate_m1363 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(156);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Value_6);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10 = L_0;
		float L_1 = (__this->___Value2_7);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11 = L_1;
		float L_2 = (__this->___Intensity_8);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12 = L_2;
		float L_3 = (__this->___Value4_9);
		((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13 = L_3;
		return;
	}
}
// System.Void CameraFilterPack_Vision_Tunnel::Update()
extern TypeInfo* CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var;
extern "C" void CameraFilterPack_Vision_Tunnel_Update_m1364 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(156);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m2732(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		float L_1 = ((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue_10;
		__this->___Value_6 = L_1;
		float L_2 = ((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue2_11;
		__this->___Value2_7 = L_2;
		float L_3 = ((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue3_12;
		__this->___Intensity_8 = L_3;
		float L_4 = ((CameraFilterPack_Vision_Tunnel_t209_StaticFields*)CameraFilterPack_Vision_Tunnel_t209_il2cpp_TypeInfo_var->static_fields)->___ChangeValue4_13;
		__this->___Value4_9 = L_4;
	}

IL_0036:
	{
		return;
	}
}
// System.Void CameraFilterPack_Vision_Tunnel::OnDisable()
extern "C" void CameraFilterPack_Vision_Tunnel_OnDisable_m1365 (CameraFilterPack_Vision_Tunnel_t209 * __this, const MethodInfo* method)
{
	{
		Material_t2 * L_0 = (__this->___SCMaterial_5);
		bool L_1 = Object_op_Implicit_m2733(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Material_t2 * L_2 = (__this->___SCMaterial_5);
		Object_DestroyImmediate_m2734(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
