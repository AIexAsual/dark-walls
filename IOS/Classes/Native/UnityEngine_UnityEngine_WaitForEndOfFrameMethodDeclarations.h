﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t484;

// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m2816 (WaitForEndOfFrame_t484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
