﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Regular
struct CameraFilterPack_Blur_Regular_t58;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Regular::.ctor()
extern "C" void CameraFilterPack_Blur_Regular__ctor_m359 (CameraFilterPack_Blur_Regular_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Regular::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Regular_get_material_m360 (CameraFilterPack_Blur_Regular_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::Start()
extern "C" void CameraFilterPack_Blur_Regular_Start_m361 (CameraFilterPack_Blur_Regular_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Regular_OnRenderImage_m362 (CameraFilterPack_Blur_Regular_t58 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::OnValidate()
extern "C" void CameraFilterPack_Blur_Regular_OnValidate_m363 (CameraFilterPack_Blur_Regular_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::Update()
extern "C" void CameraFilterPack_Blur_Regular_Update_m364 (CameraFilterPack_Blur_Regular_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Regular::OnDisable()
extern "C" void CameraFilterPack_Blur_Regular_OnDisable_m365 (CameraFilterPack_Blur_Regular_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
