﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTween/<TweenDelay>c__Iterator1A
struct U3CTweenDelayU3Ec__Iterator1A_t433;
// System.Object
struct Object_t;

// System.Void iTween/<TweenDelay>c__Iterator1A::.ctor()
extern "C" void U3CTweenDelayU3Ec__Iterator1A__ctor_m2415 (U3CTweenDelayU3Ec__Iterator1A_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__Iterator1A::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CTweenDelayU3Ec__Iterator1A_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2416 (U3CTweenDelayU3Ec__Iterator1A_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenDelay>c__Iterator1A::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CTweenDelayU3Ec__Iterator1A_System_Collections_IEnumerator_get_Current_m2417 (U3CTweenDelayU3Ec__Iterator1A_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenDelay>c__Iterator1A::MoveNext()
extern "C" bool U3CTweenDelayU3Ec__Iterator1A_MoveNext_m2418 (U3CTweenDelayU3Ec__Iterator1A_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__Iterator1A::Dispose()
extern "C" void U3CTweenDelayU3Ec__Iterator1A_Dispose_m2419 (U3CTweenDelayU3Ec__Iterator1A_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenDelay>c__Iterator1A::Reset()
extern "C" void U3CTweenDelayU3Ec__Iterator1A_Reset_m2420 (U3CTweenDelayU3Ec__Iterator1A_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
