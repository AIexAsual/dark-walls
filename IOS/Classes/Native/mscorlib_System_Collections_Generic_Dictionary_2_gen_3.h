﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t269;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2315;
// System.String[]
struct StringU5BU5D_t398;
// iTweenPath[]
struct iTweenPathU5BU5D_t455;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t2320;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,iTweenPath,System.Collections.DictionaryEntry>
struct Transform_1_t2483;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2<System.String,iTweenPath>
struct  Dictionary_2_t458  : public Object_t
{
	// System.Int32[] System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::table
	Int32U5BU5D_t269* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::linkSlots
	LinkU5BU5D_t2315* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::keySlots
	StringU5BU5D_t398* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::valueSlots
	iTweenPathU5BU5D_t455* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::hcp
	Object_t* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::serialization_info
	SerializationInfo_t1104 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::generation
	int32_t ___generation_14;
};
struct Dictionary_2_t458_StaticFields{
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2<System.String,iTweenPath>::<>f__am$cacheB
	Transform_1_t2483 * ___U3CU3Ef__amU24cacheB_15;
};
