﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// FPS
struct FPS_t213;

// System.Void FPS::.ctor()
extern "C" void FPS__ctor_m1380 (FPS_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPS::Awake()
extern "C" void FPS_Awake_m1381 (FPS_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPS::LateUpdate()
extern "C" void FPS_LateUpdate_m1382 (FPS_t213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
