﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNPopup
struct MNPopup_t278;
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"

// System.Void MNPopup::.ctor()
extern "C" void MNPopup__ctor_m1733 (MNPopup_t278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNPopup::<OnComplete>m__3(MNDialogResult)
extern "C" void MNPopup_U3COnCompleteU3Em__3_m1734 (Object_t * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
