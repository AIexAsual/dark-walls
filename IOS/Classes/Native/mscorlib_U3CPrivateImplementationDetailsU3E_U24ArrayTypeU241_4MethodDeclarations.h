﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$1024
struct U24ArrayTypeU241024_t2168;
struct U24ArrayTypeU241024_t2168_marshaled;

void U24ArrayTypeU241024_t2168_marshal(const U24ArrayTypeU241024_t2168& unmarshaled, U24ArrayTypeU241024_t2168_marshaled& marshaled);
void U24ArrayTypeU241024_t2168_marshal_back(const U24ArrayTypeU241024_t2168_marshaled& marshaled, U24ArrayTypeU241024_t2168& unmarshaled);
void U24ArrayTypeU241024_t2168_marshal_cleanup(U24ArrayTypeU241024_t2168_marshaled& marshaled);
