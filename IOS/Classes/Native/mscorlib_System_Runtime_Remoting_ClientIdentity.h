﻿#pragma once
#include <stdint.h>
// System.WeakReference
struct WeakReference_t1905;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// System.Runtime.Remoting.ClientIdentity
struct  ClientIdentity_t1906  : public Identity_t1896
{
	// System.WeakReference System.Runtime.Remoting.ClientIdentity::_proxyReference
	WeakReference_t1905 * ____proxyReference_5;
};
