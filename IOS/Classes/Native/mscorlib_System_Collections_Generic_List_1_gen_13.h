﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t453;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct  List_1_t585  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.AudioSource>::_items
	AudioSourceU5BU5D_t453* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::_version
	int32_t ____version_3;
};
struct List_1_t585_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.AudioSource>::EmptyArray
	AudioSourceU5BU5D_t453* ___EmptyArray_4;
};
