﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t1003;
// System.Type
struct Type_t;

// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern "C" void MonoPInvokeCallbackAttribute__ctor_m6147 (MonoPInvokeCallbackAttribute_t1003 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
