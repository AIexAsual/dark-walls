﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t307;
// UnityEngine.Animator
struct Animator_t321;
// AICharacterControl
struct AICharacterControl_t308;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t323;
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InteractiveMama
struct  InteractiveMama_t330  : public MonoBehaviour_t4
{
	// UnityEngine.NavMeshAgent InteractiveMama::navMeshAgent
	NavMeshAgent_t307 * ___navMeshAgent_3;
	// UnityEngine.Animator InteractiveMama::anim
	Animator_t321 * ___anim_4;
	// System.Boolean InteractiveMama::goToPos
	bool ___goToPos_5;
	// AICharacterControl InteractiveMama::aiControl
	AICharacterControl_t308 * ___aiControl_6;
	// UnityEngine.GameObject[] InteractiveMama::mama_skin
	GameObjectU5BU5D_t323* ___mama_skin_7;
	// UnityEngine.GameObject InteractiveMama::head
	GameObject_t256 * ___head_8;
	// UnityEngine.Transform InteractiveMama::targetLook
	Transform_t243 * ___targetLook_9;
	// UnityEngine.Transform InteractiveMama::myTransform
	Transform_t243 * ___myTransform_10;
};
struct InteractiveMama_t330_StaticFields{
	// UnityEngine.GameObject InteractiveMama::targetPos
	GameObject_t256 * ___targetPos_2;
};
