﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GameManager/<ObjectSpawner>c__IteratorD
struct U3CObjectSpawnerU3Ec__IteratorD_t352;
// System.Object
struct Object_t;

// System.Void GameManager/<ObjectSpawner>c__IteratorD::.ctor()
extern "C" void U3CObjectSpawnerU3Ec__IteratorD__ctor_m2047 (U3CObjectSpawnerU3Ec__IteratorD_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<ObjectSpawner>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CObjectSpawnerU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2048 (U3CObjectSpawnerU3Ec__IteratorD_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<ObjectSpawner>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CObjectSpawnerU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m2049 (U3CObjectSpawnerU3Ec__IteratorD_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<ObjectSpawner>c__IteratorD::MoveNext()
extern "C" bool U3CObjectSpawnerU3Ec__IteratorD_MoveNext_m2050 (U3CObjectSpawnerU3Ec__IteratorD_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<ObjectSpawner>c__IteratorD::Dispose()
extern "C" void U3CObjectSpawnerU3Ec__IteratorD_Dispose_m2051 (U3CObjectSpawnerU3Ec__IteratorD_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<ObjectSpawner>c__IteratorD::Reset()
extern "C" void U3CObjectSpawnerU3Ec__IteratorD_Reset_m2052 (U3CObjectSpawnerU3Ec__IteratorD_t352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
