﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<RagdollHelper/BodyPart>
struct List_1_t401;
// UnityEngine.Animator
struct Animator_t321;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// RagdollHelper/RagdollState
#include "AssemblyU2DCSharp_RagdollHelper_RagdollState.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// RagdollHelper
struct  RagdollHelper_t402  : public MonoBehaviour_t4
{
	// RagdollHelper/RagdollState RagdollHelper::state
	int32_t ___state_2;
	// System.Single RagdollHelper::ragdollToMecanimBlendTime
	float ___ragdollToMecanimBlendTime_3;
	// System.Single RagdollHelper::mecanimToGetUpTransitionTime
	float ___mecanimToGetUpTransitionTime_4;
	// System.Single RagdollHelper::ragdollingEndTime
	float ___ragdollingEndTime_5;
	// UnityEngine.Vector3 RagdollHelper::ragdolledHipPosition
	Vector3_t215  ___ragdolledHipPosition_6;
	// UnityEngine.Vector3 RagdollHelper::ragdolledHeadPosition
	Vector3_t215  ___ragdolledHeadPosition_7;
	// UnityEngine.Vector3 RagdollHelper::ragdolledFeetPosition
	Vector3_t215  ___ragdolledFeetPosition_8;
	// System.Collections.Generic.List`1<RagdollHelper/BodyPart> RagdollHelper::bodyParts
	List_1_t401 * ___bodyParts_9;
	// UnityEngine.Animator RagdollHelper::anim
	Animator_t321 * ___anim_10;
};
