﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_Artefact
struct  CameraFilterPack_TV_Artefact_t179  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_Artefact::SCShader
	Shader_t1 * ___SCShader_2;
	// UnityEngine.Vector4 CameraFilterPack_TV_Artefact::ScreenResolution
	Vector4_t5  ___ScreenResolution_3;
	// System.Single CameraFilterPack_TV_Artefact::TimeX
	float ___TimeX_4;
	// System.Single CameraFilterPack_TV_Artefact::Colorisation
	float ___Colorisation_5;
	// System.Single CameraFilterPack_TV_Artefact::Parasite
	float ___Parasite_6;
	// System.Single CameraFilterPack_TV_Artefact::Noise
	float ___Noise_7;
	// UnityEngine.Material CameraFilterPack_TV_Artefact::SCMaterial
	Material_t2 * ___SCMaterial_8;
};
