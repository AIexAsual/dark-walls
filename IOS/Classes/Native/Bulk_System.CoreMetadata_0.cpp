﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1171_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CModuleU3E_t1171_0_0_0;
extern const Il2CppType U3CModuleU3E_t1171_1_0_0;
struct U3CModuleU3E_t1171;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1171_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t1171_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t1171_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1171_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1171_1_0_0/* this_arg */
	, &U3CModuleU3E_t1171_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1171)/* instance_size */
	, sizeof (U3CModuleU3E_t1171)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern TypeInfo ExtensionAttribute_t1172_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
static const EncodedMethodIndex ExtensionAttribute_t1172_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t1172_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType ExtensionAttribute_t1172_0_0_0;
extern const Il2CppType ExtensionAttribute_t1172_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct ExtensionAttribute_t1172;
const Il2CppTypeDefinitionMetadata ExtensionAttribute_t1172_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtensionAttribute_t1172_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ExtensionAttribute_t1172_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6126/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExtensionAttribute_t1172_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &ExtensionAttribute_t1172_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2275/* custom_attributes_cache */
	, &ExtensionAttribute_t1172_0_0_0/* byval_arg */
	, &ExtensionAttribute_t1172_1_0_0/* this_arg */
	, &ExtensionAttribute_t1172_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t1172)/* instance_size */
	, sizeof (ExtensionAttribute_t1172)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Locale
#include "System_Core_Locale.h"
// Metadata Definition Locale
extern TypeInfo Locale_t1173_il2cpp_TypeInfo;
// Locale
#include "System_Core_LocaleMethodDeclarations.h"
static const EncodedMethodIndex Locale_t1173_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Locale_t1173_0_0_0;
extern const Il2CppType Locale_t1173_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Locale_t1173;
const Il2CppTypeDefinitionMetadata Locale_t1173_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Locale_t1173_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6127/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Locale_t1173_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Locale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Locale_t1173_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Locale_t1173_0_0_0/* byval_arg */
	, &Locale_t1173_1_0_0/* this_arg */
	, &Locale_t1173_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Locale_t1173)/* instance_size */
	, sizeof (Locale_t1173)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.KeyBuilder
#include "System_Core_Mono_Security_Cryptography_KeyBuilder.h"
// Metadata Definition Mono.Security.Cryptography.KeyBuilder
extern TypeInfo KeyBuilder_t1175_il2cpp_TypeInfo;
// Mono.Security.Cryptography.KeyBuilder
#include "System_Core_Mono_Security_Cryptography_KeyBuilderMethodDeclarations.h"
static const EncodedMethodIndex KeyBuilder_t1175_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType KeyBuilder_t1175_0_0_0;
extern const Il2CppType KeyBuilder_t1175_1_0_0;
struct KeyBuilder_t1175;
const Il2CppTypeDefinitionMetadata KeyBuilder_t1175_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyBuilder_t1175_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5022/* fieldStart */
	, 6129/* methodStart */
	, -1/* eventStart */
	, 1198/* propertyStart */

};
TypeInfo KeyBuilder_t1175_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyBuilder"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyBuilder_t1175_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyBuilder_t1175_0_0_0/* byval_arg */
	, &KeyBuilder_t1175_1_0_0/* this_arg */
	, &KeyBuilder_t1175_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyBuilder_t1175)/* instance_size */
	, sizeof (KeyBuilder_t1175)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyBuilder_t1175_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.SymmetricTransform
#include "System_Core_Mono_Security_Cryptography_SymmetricTransform.h"
// Metadata Definition Mono.Security.Cryptography.SymmetricTransform
extern TypeInfo SymmetricTransform_t1177_il2cpp_TypeInfo;
// Mono.Security.Cryptography.SymmetricTransform
#include "System_Core_Mono_Security_Cryptography_SymmetricTransformMethodDeclarations.h"
static const EncodedMethodIndex SymmetricTransform_t1177_VTable[18] = 
{
	626,
	1600,
	627,
	628,
	1601,
	1602,
	1603,
	1604,
	1605,
	1602,
	1606,
	0,
	1607,
	1608,
	1609,
	1610,
	1603,
	1604,
};
extern const Il2CppType IDisposable_t538_0_0_0;
extern const Il2CppType ICryptoTransform_t1188_0_0_0;
static const Il2CppType* SymmetricTransform_t1177_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ICryptoTransform_t1188_0_0_0,
};
static Il2CppInterfaceOffsetPair SymmetricTransform_t1177_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType SymmetricTransform_t1177_0_0_0;
extern const Il2CppType SymmetricTransform_t1177_1_0_0;
struct SymmetricTransform_t1177;
const Il2CppTypeDefinitionMetadata SymmetricTransform_t1177_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SymmetricTransform_t1177_InterfacesTypeInfos/* implementedInterfaces */
	, SymmetricTransform_t1177_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SymmetricTransform_t1177_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5023/* fieldStart */
	, 6132/* methodStart */
	, -1/* eventStart */
	, 1199/* propertyStart */

};
TypeInfo SymmetricTransform_t1177_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "SymmetricTransform"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SymmetricTransform_t1177_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SymmetricTransform_t1177_0_0_0/* byval_arg */
	, &SymmetricTransform_t1177_1_0_0/* this_arg */
	, &SymmetricTransform_t1177_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SymmetricTransform_t1177)/* instance_size */
	, sizeof (SymmetricTransform_t1177)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 20/* method_count */
	, 2/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
// Metadata Definition System.Linq.Check
extern TypeInfo Check_t1178_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"
static const EncodedMethodIndex Check_t1178_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Check_t1178_0_0_0;
extern const Il2CppType Check_t1178_1_0_0;
struct Check_t1178;
const Il2CppTypeDefinitionMetadata Check_t1178_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Check_t1178_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6152/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Check_t1178_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Check"/* name */
	, "System.Linq"/* namespaze */
	, NULL/* methods */
	, &Check_t1178_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Check_t1178_0_0_0/* byval_arg */
	, &Check_t1178_1_0_0/* this_arg */
	, &Check_t1178_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Check_t1178)/* instance_size */
	, sizeof (Check_t1178)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
// Metadata Definition System.Linq.Enumerable
extern TypeInfo Enumerable_t512_il2cpp_TypeInfo;
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
extern const Il2CppType ICollection_1_t3693_0_0_0;
extern const Il2CppType IEnumerable_1_t3694_0_0_0;
extern const Il2CppRGCTXDefinition Enumerable_Any_m24292_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6713 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4296 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType IEnumerable_1_t3695_0_0_0;
extern const Il2CppType IEnumerator_1_t3696_0_0_0;
extern const Il2CppRGCTXDefinition Enumerable_First_m24293_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4298 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6714 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5492 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType PredicateOf_1_t3697_0_0_0;
extern const Il2CppRGCTXDefinition Enumerable_FirstOrDefault_m24294_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6715 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5493 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Enumerable_FirstOrDefault_m24295_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5494 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Enumerable_Repeat_m24296_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5495 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3698_0_0_0;
extern const Il2CppRGCTXDefinition Enumerable_CreateRepeatIterator_m24297_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6716 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5496 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Enumerable_Select_m24298_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5497 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3699_0_0_0;
extern const Il2CppRGCTXDefinition Enumerable_CreateSelectIterator_m24299_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6717 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5498 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType ICollection_1_t3700_0_0_0;
extern const Il2CppType TSourceU5BU5D_t3701_0_0_0;
extern const Il2CppType List_1_t3702_0_0_0;
extern const Il2CppRGCTXDefinition Enumerable_ToArray_m24300_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6718 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4323 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6719 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5499 }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5500 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppRGCTXDefinition Enumerable_Where_m24301_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5501 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3703_0_0_0;
extern const Il2CppRGCTXDefinition Enumerable_CreateWhereIterator_m24302_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6720 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5502 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType Fallback_t1179_0_0_0;
extern const Il2CppType PredicateOf_1_t3419_0_0_0;
extern const Il2CppType U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_0_0_0;
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_0_0_0;
static const Il2CppType* Enumerable_t512_il2cpp_TypeInfo__nestedTypes[5] =
{
	&Fallback_t1179_0_0_0,
	&PredicateOf_1_t3419_0_0_0,
	&U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_0_0_0,
	&U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_0_0_0,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_0_0_0,
};
static const EncodedMethodIndex Enumerable_t512_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Enumerable_t512_0_0_0;
extern const Il2CppType Enumerable_t512_1_0_0;
struct Enumerable_t512;
const Il2CppTypeDefinitionMetadata Enumerable_t512_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Enumerable_t512_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerable_t512_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6155/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Enumerable_t512_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerable"/* name */
	, "System.Linq"/* namespaze */
	, NULL/* methods */
	, &Enumerable_t512_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2277/* custom_attributes_cache */
	, &Enumerable_t512_0_0_0/* byval_arg */
	, &Enumerable_t512_1_0_0/* this_arg */
	, &Enumerable_t512_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerable_t512)/* instance_size */
	, sizeof (Enumerable_t512)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_Fallback.h"
// Metadata Definition System.Linq.Enumerable/Fallback
extern TypeInfo Fallback_t1179_il2cpp_TypeInfo;
// System.Linq.Enumerable/Fallback
#include "System_Core_System_Linq_Enumerable_FallbackMethodDeclarations.h"
static const EncodedMethodIndex Fallback_t1179_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair Fallback_t1179_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Fallback_t1179_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Fallback_t1179_DefinitionMetadata = 
{
	&Enumerable_t512_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Fallback_t1179_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Fallback_t1179_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5035/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Fallback_t1179_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Fallback"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Fallback_t1179_0_0_0/* byval_arg */
	, &Fallback_t1179_1_0_0/* this_arg */
	, &Fallback_t1179_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Fallback_t1179)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Fallback_t1179)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/PredicateOf`1
extern TypeInfo PredicateOf_1_t3419_il2cpp_TypeInfo;
static const EncodedMethodIndex PredicateOf_1_t3419_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern const Il2CppType PredicateOf_1_t3704_0_0_0;
extern const Il2CppType Func_2_t3705_0_0_0;
extern const Il2CppRGCTXDefinition PredicateOf_1_t3419_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 6728 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5503 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6729 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5504 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType PredicateOf_1_t3419_1_0_0;
struct PredicateOf_1_t3419;
const Il2CppTypeDefinitionMetadata PredicateOf_1_t3419_DefinitionMetadata = 
{
	&Enumerable_t512_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PredicateOf_1_t3419_VTable/* vtableMethods */
	, PredicateOf_1_t3419_RGCTXData/* rgctxDefinition */
	, 5038/* fieldStart */
	, 6166/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PredicateOf_1_t3419_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "PredicateOf`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PredicateOf_1_t3419_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PredicateOf_1_t3419_0_0_0/* byval_arg */
	, &PredicateOf_1_t3419_1_0_0/* this_arg */
	, &PredicateOf_1_t3419_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 70/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1
extern TypeInfo U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_VTable[10] = 
{
	626,
	601,
	627,
	628,
	1611,
	1612,
	1613,
	1614,
	1615,
	1616,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType IEnumerable_t1097_0_0_0;
extern const Il2CppType IEnumerable_1_t3706_0_0_0;
extern const Il2CppType IEnumerator_1_t3707_0_0_0;
static const Il2CppType* U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerable_t1097_0_0_0,
	&IEnumerable_1_t3706_0_0_0,
	&IEnumerator_1_t3707_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerable_t1097_0_0_0, 7},
	{ &IEnumerable_1_t3706_0_0_0, 8},
	{ &IEnumerator_1_t3707_0_0_0, 9},
};
extern const Il2CppType U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_gp_0_0_0_0;
extern const Il2CppType U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3709_0_0_0;
extern const Il2CppRGCTXDefinition U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4334 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5505 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6732 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5506 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_1_0_0;
struct U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420;
const Il2CppTypeDefinitionMetadata U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_DefinitionMetadata = 
{
	&Enumerable_t512_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_VTable/* vtableMethods */
	, U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_RGCTXData/* rgctxDefinition */
	, 5040/* fieldStart */
	, 6168/* methodStart */
	, -1/* eventStart */
	, 1201/* propertyStart */

};
TypeInfo U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateRepeatIterator>c__IteratorE`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2290/* custom_attributes_cache */
	, &U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_0_0_0/* byval_arg */
	, &U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_1_0_0/* this_arg */
	, &U3CCreateRepeatIteratorU3Ec__IteratorE_1_t3420_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 71/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2
extern TypeInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_VTable[10] = 
{
	626,
	601,
	627,
	628,
	1617,
	1618,
	1619,
	1620,
	1621,
	1622,
};
extern const Il2CppType IEnumerable_1_t3710_0_0_0;
extern const Il2CppType IEnumerator_1_t3711_0_0_0;
static const Il2CppType* U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerable_t1097_0_0_0,
	&IEnumerable_1_t3710_0_0_0,
	&IEnumerator_1_t3711_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerable_t1097_0_0_0, 7},
	{ &IEnumerable_1_t3710_0_0_0, 8},
	{ &IEnumerator_1_t3711_0_0_0, 9},
};
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_gp_1_0_0_0;
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3713_0_0_0;
extern const Il2CppType IEnumerable_1_t3714_0_0_0;
extern const Il2CppType IEnumerator_1_t3715_0_0_0;
extern const Il2CppRGCTXDefinition U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4337 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5507 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6735 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5508 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6736 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6737 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5509 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_1_0_0;
struct U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421;
const Il2CppTypeDefinitionMetadata U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_DefinitionMetadata = 
{
	&Enumerable_t512_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_VTable/* vtableMethods */
	, U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_RGCTXData/* rgctxDefinition */
	, 5047/* fieldStart */
	, 6175/* methodStart */
	, -1/* eventStart */
	, 1203/* propertyStart */

};
TypeInfo U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateSelectIterator>c__Iterator10`2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2296/* custom_attributes_cache */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_0_0_0/* byval_arg */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_1_0_0/* this_arg */
	, &U3CCreateSelectIteratorU3Ec__Iterator10_2_t3421_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 72/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_il2cpp_TypeInfo;
static const EncodedMethodIndex U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_VTable[10] = 
{
	626,
	601,
	627,
	628,
	1623,
	1624,
	1625,
	1626,
	1627,
	1628,
};
extern const Il2CppType IEnumerable_1_t3716_0_0_0;
extern const Il2CppType IEnumerator_1_t3717_0_0_0;
static const Il2CppType* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&IEnumerable_t1097_0_0_0,
	&IEnumerable_1_t3716_0_0_0,
	&IEnumerator_1_t3717_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &IEnumerable_t1097_0_0_0, 7},
	{ &IEnumerable_1_t3716_0_0_0, 8},
	{ &IEnumerator_1_t3717_0_0_0, 9},
};
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3719_0_0_0;
extern const Il2CppRGCTXDefinition U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 4345 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5510 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6740 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5511 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 6739 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, 4346 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5512 }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_1_0_0;
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422;
const Il2CppTypeDefinitionMetadata U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_DefinitionMetadata = 
{
	&Enumerable_t512_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_VTable/* vtableMethods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_RGCTXData/* rgctxDefinition */
	, 5055/* fieldStart */
	, 6182/* methodStart */
	, -1/* eventStart */
	, 1205/* propertyStart */

};
TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateWhereIterator>c__Iterator1D`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2302/* custom_attributes_cache */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_0_0_0/* byval_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_1_0_0/* this_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3422_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 73/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Security.Cryptography.Aes
#include "System_Core_System_Security_Cryptography_Aes.h"
// Metadata Definition System.Security.Cryptography.Aes
extern TypeInfo Aes_t1180_il2cpp_TypeInfo;
// System.Security.Cryptography.Aes
#include "System_Core_System_Security_Cryptography_AesMethodDeclarations.h"
static const EncodedMethodIndex Aes_t1180_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	1637,
	1638,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	0,
	1647,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair Aes_t1180_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Aes_t1180_0_0_0;
extern const Il2CppType Aes_t1180_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t1176_0_0_0;
struct Aes_t1180;
const Il2CppTypeDefinitionMetadata Aes_t1180_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Aes_t1180_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t1176_0_0_0/* parent */
	, Aes_t1180_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6189/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Aes_t1180_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Aes"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Aes_t1180_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Aes_t1180_0_0_0/* byval_arg */
	, &Aes_t1180_1_0_0/* this_arg */
	, &Aes_t1180_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Aes_t1180)/* instance_size */
	, sizeof (Aes_t1180)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.AesManaged
#include "System_Core_System_Security_Cryptography_AesManaged.h"
// Metadata Definition System.Security.Cryptography.AesManaged
extern TypeInfo AesManaged_t1181_il2cpp_TypeInfo;
// System.Security.Cryptography.AesManaged
#include "System_Core_System_Security_Cryptography_AesManagedMethodDeclarations.h"
static const EncodedMethodIndex AesManaged_t1181_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1648,
	1632,
	1633,
	1634,
	1649,
	1650,
	1651,
	1652,
	1653,
	1654,
	1641,
	1642,
	1643,
	1644,
	1645,
	1655,
	1656,
	1657,
	1658,
	1659,
	1660,
};
static Il2CppInterfaceOffsetPair AesManaged_t1181_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType AesManaged_t1181_0_0_0;
extern const Il2CppType AesManaged_t1181_1_0_0;
struct AesManaged_t1181;
const Il2CppTypeDefinitionMetadata AesManaged_t1181_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AesManaged_t1181_InterfacesOffsets/* interfaceOffsets */
	, &Aes_t1180_0_0_0/* parent */
	, AesManaged_t1181_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6190/* methodStart */
	, -1/* eventStart */
	, 1207/* propertyStart */

};
TypeInfo AesManaged_t1181_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "AesManaged"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AesManaged_t1181_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AesManaged_t1181_0_0_0/* byval_arg */
	, &AesManaged_t1181_1_0_0/* this_arg */
	, &AesManaged_t1181_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AesManaged_t1181)/* instance_size */
	, sizeof (AesManaged_t1181)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.AesTransform
#include "System_Core_System_Security_Cryptography_AesTransform.h"
// Metadata Definition System.Security.Cryptography.AesTransform
extern TypeInfo AesTransform_t1183_il2cpp_TypeInfo;
// System.Security.Cryptography.AesTransform
#include "System_Core_System_Security_Cryptography_AesTransformMethodDeclarations.h"
static const EncodedMethodIndex AesTransform_t1183_VTable[18] = 
{
	626,
	1600,
	627,
	628,
	1601,
	1602,
	1603,
	1604,
	1605,
	1602,
	1606,
	1661,
	1607,
	1608,
	1609,
	1610,
	1603,
	1604,
};
static Il2CppInterfaceOffsetPair AesTransform_t1183_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType AesTransform_t1183_0_0_0;
extern const Il2CppType AesTransform_t1183_1_0_0;
struct AesTransform_t1183;
const Il2CppTypeDefinitionMetadata AesTransform_t1183_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AesTransform_t1183_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t1177_0_0_0/* parent */
	, AesTransform_t1183_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5063/* fieldStart */
	, 6204/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AesTransform_t1183_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "AesTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &AesTransform_t1183_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AesTransform_t1183_0_0_0/* byval_arg */
	, &AesTransform_t1183_1_0_0/* this_arg */
	, &AesTransform_t1183_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AesTransform_t1183)/* instance_size */
	, sizeof (AesTransform_t1183)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AesTransform_t1183_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Action
#include "System_Core_System_Action.h"
// Metadata Definition System.Action
extern TypeInfo Action_t238_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
static const EncodedMethodIndex Action_t238_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1662,
	1663,
	1664,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair Action_t238_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Action_t238_0_0_0;
extern const Il2CppType Action_t238_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct Action_t238;
const Il2CppTypeDefinitionMetadata Action_t238_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_t238_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Action_t238_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6210/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Action_t238_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Action_t238_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_t238_0_0_0/* byval_arg */
	, &Action_t238_1_0_0/* this_arg */
	, &Action_t238_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_Action_t238/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_t238)/* instance_size */
	, sizeof (Action_t238)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Func`2
extern TypeInfo Func_2_t3423_il2cpp_TypeInfo;
static const EncodedMethodIndex Func_2_t3423_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1665,
	1666,
	1667,
};
static Il2CppInterfaceOffsetPair Func_2_t3423_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Func_2_t3423_0_0_0;
extern const Il2CppType Func_2_t3423_1_0_0;
struct Func_2_t3423;
const Il2CppTypeDefinitionMetadata Func_2_t3423_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Func_2_t3423_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Func_2_t3423_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6214/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Func_2_t3423_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Func_2_t3423_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Func_2_t3423_0_0_0/* byval_arg */
	, &Func_2_t3423_1_0_0/* this_arg */
	, &Func_2_t3423_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 74/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1187_il2cpp_TypeInfo;
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
extern const Il2CppType U24ArrayTypeU24120_t1184_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t1185_0_0_0;
extern const Il2CppType U24ArrayTypeU241024_t1186_0_0_0;
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1187_il2cpp_TypeInfo__nestedTypes[3] =
{
	&U24ArrayTypeU24120_t1184_0_0_0,
	&U24ArrayTypeU24256_t1185_0_0_0,
	&U24ArrayTypeU241024_t1186_0_0_0,
};
static const EncodedMethodIndex U3CPrivateImplementationDetailsU3E_t1187_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1187_0_0_0;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1187_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1187;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1187_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1187_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1187_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5077/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1187_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CPrivateImplementationDetailsU3E_t1187_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2308/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1187_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1187_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1187_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1187)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1187)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1187_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$120
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
extern TypeInfo U24ArrayTypeU24120_t1184_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$120
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeUMethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24120_t1184_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU24120_t1184_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24120_t1184_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1187_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24120_t1184_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24120_t1184_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$120"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24120_t1184_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24120_t1184_0_0_0/* byval_arg */
	, &U24ArrayTypeU24120_t1184_1_0_0/* this_arg */
	, &U24ArrayTypeU24120_t1184_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24120_t1184_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t1184_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t1184_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24120_t1184)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24120_t1184)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24120_t1184_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t1185_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24256_t1185_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t1185_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t1185_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1187_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24256_t1185_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24256_t1185_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24256_t1185_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t1185_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t1185_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t1185_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t1185_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1185_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1185_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t1185)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t1185)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t1185_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$1024
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
extern TypeInfo U24ArrayTypeU241024_t1186_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$1024
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU_1MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU241024_t1186_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU241024_t1186_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU241024_t1186_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1187_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU241024_t1186_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU241024_t1186_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$1024"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU241024_t1186_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU241024_t1186_0_0_0/* byval_arg */
	, &U24ArrayTypeU241024_t1186_1_0_0/* this_arg */
	, &U24ArrayTypeU241024_t1186_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU241024_t1186_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t1186_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t1186_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU241024_t1186)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU241024_t1186)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU241024_t1186_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
