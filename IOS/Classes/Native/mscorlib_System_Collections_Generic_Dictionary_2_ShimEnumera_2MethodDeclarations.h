﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t2524;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2513;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17606_gshared (ShimEnumerator_t2524 * __this, Dictionary_2_t2513 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17606(__this, ___host, method) (( void (*) (ShimEnumerator_t2524 *, Dictionary_2_t2513 *, const MethodInfo*))ShimEnumerator__ctor_m17606_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17607_gshared (ShimEnumerator_t2524 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17607(__this, method) (( bool (*) (ShimEnumerator_t2524 *, const MethodInfo*))ShimEnumerator_MoveNext_m17607_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m17608_gshared (ShimEnumerator_t2524 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17608(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2524 *, const MethodInfo*))ShimEnumerator_get_Entry_m17608_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17609_gshared (ShimEnumerator_t2524 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17609(__this, method) (( Object_t * (*) (ShimEnumerator_t2524 *, const MethodInfo*))ShimEnumerator_get_Key_m17609_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17610_gshared (ShimEnumerator_t2524 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17610(__this, method) (( Object_t * (*) (ShimEnumerator_t2524 *, const MethodInfo*))ShimEnumerator_get_Value_m17610_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17611_gshared (ShimEnumerator_t2524 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17611(__this, method) (( Object_t * (*) (ShimEnumerator_t2524 *, const MethodInfo*))ShimEnumerator_get_Current_m17611_gshared)(__this, method)
