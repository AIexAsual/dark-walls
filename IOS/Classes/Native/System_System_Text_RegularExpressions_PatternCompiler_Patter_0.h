﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
struct  PatternLinkStack_t1476  : public LinkStack_t1477
{
	// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::link
	Link_t1475  ___link_1;
};
