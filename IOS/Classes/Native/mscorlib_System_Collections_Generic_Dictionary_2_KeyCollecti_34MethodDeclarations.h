﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>
struct Enumerator_t2880;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2876;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22422_gshared (Enumerator_t2880 * __this, Dictionary_2_t2876 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22422(__this, ___host, method) (( void (*) (Enumerator_t2880 *, Dictionary_2_t2876 *, const MethodInfo*))Enumerator__ctor_m22422_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22423_gshared (Enumerator_t2880 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22423(__this, method) (( Object_t * (*) (Enumerator_t2880 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22423_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m22424_gshared (Enumerator_t2880 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22424(__this, method) (( void (*) (Enumerator_t2880 *, const MethodInfo*))Enumerator_Dispose_m22424_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22425_gshared (Enumerator_t2880 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22425(__this, method) (( bool (*) (Enumerator_t2880 *, const MethodInfo*))Enumerator_MoveNext_m22425_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m22426_gshared (Enumerator_t2880 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22426(__this, method) (( Object_t * (*) (Enumerator_t2880 *, const MethodInfo*))Enumerator_get_Current_m22426_gshared)(__this, method)
