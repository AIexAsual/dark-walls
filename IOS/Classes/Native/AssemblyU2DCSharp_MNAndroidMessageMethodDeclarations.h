﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNAndroidMessage
struct MNAndroidMessage_t286;
// System.String
struct String_t;

// System.Void MNAndroidMessage::.ctor()
extern "C" void MNAndroidMessage__ctor_m1769 (MNAndroidMessage_t286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidMessage MNAndroidMessage::Create(System.String,System.String)
extern "C" MNAndroidMessage_t286 * MNAndroidMessage_Create_m1770 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNAndroidMessage MNAndroidMessage::Create(System.String,System.String,System.String)
extern "C" MNAndroidMessage_t286 * MNAndroidMessage_Create_m1771 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidMessage::init()
extern "C" void MNAndroidMessage_init_m1772 (MNAndroidMessage_t286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNAndroidMessage::onPopUpCallBack(System.String)
extern "C" void MNAndroidMessage_onPopUpCallBack_m1773 (MNAndroidMessage_t286 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
