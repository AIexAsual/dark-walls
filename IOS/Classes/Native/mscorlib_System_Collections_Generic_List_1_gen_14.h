﻿#pragma once
#include <stdint.h>
// ArrayIndexes[]
struct ArrayIndexesU5BU5D_t454;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<ArrayIndexes>
struct  List_1_t586  : public Object_t
{
	// T[] System.Collections.Generic.List`1<ArrayIndexes>::_items
	ArrayIndexesU5BU5D_t454* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::_version
	int32_t ____version_3;
};
struct List_1_t586_StaticFields{
	// T[] System.Collections.Generic.List`1<ArrayIndexes>::EmptyArray
	ArrayIndexesU5BU5D_t454* ___EmptyArray_4;
};
