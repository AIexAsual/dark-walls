﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1158;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t2997;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Type>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m6464(__this, method) (( void (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1__ctor_m14012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m21662(__this, method) (( bool (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m14013_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m21663(__this, method) (( Object_t * (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m14014_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m21664(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t1158 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m14015_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21665(__this, method) (( Object_t* (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14016_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m21666(__this, method) (( Object_t * (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m14017_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Peek()
#define Stack_1_Peek_m21667(__this, method) (( Type_t * (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_Peek_m14018_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m6466(__this, method) (( Type_t * (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_Pop_m14019_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(T)
#define Stack_1_Push_m6465(__this, ___t, method) (( void (*) (Stack_1_t1158 *, Type_t *, const MethodInfo*))Stack_1_Push_m14020_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m21668(__this, method) (( int32_t (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_get_Count_m14021_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Type>::GetEnumerator()
#define Stack_1_GetEnumerator_m21669(__this, method) (( Enumerator_t2811  (*) (Stack_1_t1158 *, const MethodInfo*))Stack_1_GetEnumerator_m14022_gshared)(__this, method)
