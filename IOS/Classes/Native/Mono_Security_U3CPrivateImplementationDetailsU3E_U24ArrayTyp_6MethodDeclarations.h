﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$16
struct U24ArrayTypeU2416_t1330;
struct U24ArrayTypeU2416_t1330_marshaled;

void U24ArrayTypeU2416_t1330_marshal(const U24ArrayTypeU2416_t1330& unmarshaled, U24ArrayTypeU2416_t1330_marshaled& marshaled);
void U24ArrayTypeU2416_t1330_marshal_back(const U24ArrayTypeU2416_t1330_marshaled& marshaled, U24ArrayTypeU2416_t1330& unmarshaled);
void U24ArrayTypeU2416_t1330_marshal_cleanup(U24ArrayTypeU2416_t1330_marshaled& marshaled);
