﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GetFPS
struct GetFPS_t314;

// System.Void GetFPS::.ctor()
extern "C" void GetFPS__ctor_m1886 (GetFPS_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetFPS::Update()
extern "C" void GetFPS_Update_m1887 (GetFPS_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetFPS::OnGUI()
extern "C" void GetFPS_OnGUI_m1888 (GetFPS_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
