﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_Stripe
struct CameraFilterPack_Gradients_Stripe_t152;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_Stripe::.ctor()
extern "C" void CameraFilterPack_Gradients_Stripe__ctor_m983 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Stripe::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_Stripe_get_material_m984 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::Start()
extern "C" void CameraFilterPack_Gradients_Stripe_Start_m985 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_Stripe_OnRenderImage_m986 (CameraFilterPack_Gradients_Stripe_t152 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::Update()
extern "C" void CameraFilterPack_Gradients_Stripe_Update_m987 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Stripe::OnDisable()
extern "C" void CameraFilterPack_Gradients_Stripe_OnDisable_m988 (CameraFilterPack_Gradients_Stripe_t152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
