﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_CellShading
struct CameraFilterPack_Drawing_CellShading_t95;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_CellShading::.ctor()
extern "C" void CameraFilterPack_Drawing_CellShading__ctor_m602 (CameraFilterPack_Drawing_CellShading_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_CellShading::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_CellShading_get_material_m603 (CameraFilterPack_Drawing_CellShading_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::Start()
extern "C" void CameraFilterPack_Drawing_CellShading_Start_m604 (CameraFilterPack_Drawing_CellShading_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_CellShading_OnRenderImage_m605 (CameraFilterPack_Drawing_CellShading_t95 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::OnValidate()
extern "C" void CameraFilterPack_Drawing_CellShading_OnValidate_m606 (CameraFilterPack_Drawing_CellShading_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::Update()
extern "C" void CameraFilterPack_Drawing_CellShading_Update_m607 (CameraFilterPack_Drawing_CellShading_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_CellShading::OnDisable()
extern "C" void CameraFilterPack_Drawing_CellShading_OnDisable_m608 (CameraFilterPack_Drawing_CellShading_t95 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
