﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.InAttribute
struct InAttribute_t1557;

// System.Void System.Runtime.InteropServices.InAttribute::.ctor()
extern "C" void InAttribute__ctor_m9350 (InAttribute_t1557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
