﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Single>
struct Enumerator_t2400;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t576;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15686_gshared (Enumerator_t2400 * __this, List_1_t576 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15686(__this, ___l, method) (( void (*) (Enumerator_t2400 *, List_1_t576 *, const MethodInfo*))Enumerator__ctor_m15686_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15687_gshared (Enumerator_t2400 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15687(__this, method) (( Object_t * (*) (Enumerator_t2400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15687_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::Dispose()
extern "C" void Enumerator_Dispose_m15688_gshared (Enumerator_t2400 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15688(__this, method) (( void (*) (Enumerator_t2400 *, const MethodInfo*))Enumerator_Dispose_m15688_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Single>::VerifyState()
extern "C" void Enumerator_VerifyState_m15689_gshared (Enumerator_t2400 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15689(__this, method) (( void (*) (Enumerator_t2400 *, const MethodInfo*))Enumerator_VerifyState_m15689_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15690_gshared (Enumerator_t2400 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15690(__this, method) (( bool (*) (Enumerator_t2400 *, const MethodInfo*))Enumerator_MoveNext_m15690_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
extern "C" float Enumerator_get_Current_m15691_gshared (Enumerator_t2400 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15691(__this, method) (( float (*) (Enumerator_t2400 *, const MethodInfo*))Enumerator_get_Current_m15691_gshared)(__this, method)
