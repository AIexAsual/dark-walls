﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct KeyValuePair_2_t2350;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m14739_gshared (KeyValuePair_2_t2350 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m14739(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2350 *, Object_t *, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m14739_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m14741_gshared (KeyValuePair_2_t2350 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m14741(__this, method) (( Object_t * (*) (KeyValuePair_2_t2350 *, const MethodInfo*))KeyValuePair_2_get_Key_m14741_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m14743_gshared (KeyValuePair_2_t2350 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m14743(__this, ___value, method) (( void (*) (KeyValuePair_2_t2350 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m14743_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m14745_gshared (KeyValuePair_2_t2350 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m14745(__this, method) (( Object_t * (*) (KeyValuePair_2_t2350 *, const MethodInfo*))KeyValuePair_2_get_Value_m14745_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m14747_gshared (KeyValuePair_2_t2350 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m14747(__this, ___value, method) (( void (*) (KeyValuePair_2_t2350 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m14747_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m14749_gshared (KeyValuePair_2_t2350 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m14749(__this, method) (( String_t* (*) (KeyValuePair_2_t2350 *, const MethodInfo*))KeyValuePair_2_ToString_m14749_gshared)(__this, method)
