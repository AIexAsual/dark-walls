﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_BlackHole
struct CameraFilterPack_Distortion_BlackHole_t81;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_BlackHole::.ctor()
extern "C" void CameraFilterPack_Distortion_BlackHole__ctor_m503 (CameraFilterPack_Distortion_BlackHole_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_BlackHole::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_BlackHole_get_material_m504 (CameraFilterPack_Distortion_BlackHole_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::Start()
extern "C" void CameraFilterPack_Distortion_BlackHole_Start_m505 (CameraFilterPack_Distortion_BlackHole_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_BlackHole_OnRenderImage_m506 (CameraFilterPack_Distortion_BlackHole_t81 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::OnValidate()
extern "C" void CameraFilterPack_Distortion_BlackHole_OnValidate_m507 (CameraFilterPack_Distortion_BlackHole_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::Update()
extern "C" void CameraFilterPack_Distortion_BlackHole_Update_m508 (CameraFilterPack_Distortion_BlackHole_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_BlackHole::OnDisable()
extern "C" void CameraFilterPack_Distortion_BlackHole_OnDisable_m509 (CameraFilterPack_Distortion_BlackHole_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
