﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Drawing_NewCellShading
struct  CameraFilterPack_Drawing_NewCellShading_t108  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Drawing_NewCellShading::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_NewCellShading::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Drawing_NewCellShading::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Drawing_NewCellShading::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Drawing_NewCellShading::Threshold
	float ___Threshold_6;
};
