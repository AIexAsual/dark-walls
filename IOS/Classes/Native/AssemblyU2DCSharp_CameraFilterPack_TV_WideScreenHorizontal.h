﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_WideScreenHorizontal
struct  CameraFilterPack_TV_WideScreenHorizontal_t202  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_WideScreenHorizontal::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_TV_WideScreenHorizontal::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_TV_WideScreenHorizontal::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::Size
	float ___Size_6;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::Smooth
	float ___Smooth_7;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::StretchX
	float ___StretchX_8;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::StretchY
	float ___StretchY_9;
};
struct CameraFilterPack_TV_WideScreenHorizontal_t202_StaticFields{
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_TV_WideScreenHorizontal::ChangeValue4
	float ___ChangeValue4_13;
};
