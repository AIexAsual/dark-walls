﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Pixelisation_OilPaint
struct CameraFilterPack_Pixelisation_OilPaint_t171;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Pixelisation_OilPaint::.ctor()
extern "C" void CameraFilterPack_Pixelisation_OilPaint__ctor_m1105 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixelisation_OilPaint::get_material()
extern "C" Material_t2 * CameraFilterPack_Pixelisation_OilPaint_get_material_m1106 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::Start()
extern "C" void CameraFilterPack_Pixelisation_OilPaint_Start_m1107 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Pixelisation_OilPaint_OnRenderImage_m1108 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnValidate()
extern "C" void CameraFilterPack_Pixelisation_OilPaint_OnValidate_m1109 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::Update()
extern "C" void CameraFilterPack_Pixelisation_OilPaint_Update_m1110 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaint::OnDisable()
extern "C" void CameraFilterPack_Pixelisation_OilPaint_OnDisable_m1111 (CameraFilterPack_Pixelisation_OilPaint_t171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
