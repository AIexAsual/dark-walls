﻿#pragma once
#include <stdint.h>
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t516;
// System.Object
#include "mscorlib_System_Object.h"
// System.Linq.Enumerable/PredicateOf`1<System.Object>
struct  PredicateOf_1_t2283  : public Object_t
{
};
struct PredicateOf_1_t2283_StaticFields{
	// System.Func`2<T,System.Boolean> System.Linq.Enumerable/PredicateOf`1<System.Object>::Always
	Func_2_t516 * ___Always_0;
	// System.Func`2<T,System.Boolean> System.Linq.Enumerable/PredicateOf`1<System.Object>::<>f__am$cache1
	Func_2_t516 * ___U3CU3Ef__amU24cache1_1;
};
