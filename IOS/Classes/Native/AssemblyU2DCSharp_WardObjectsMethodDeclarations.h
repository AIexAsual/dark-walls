﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WardObjects
struct WardObjects_t373;

// System.Void WardObjects::.ctor()
extern "C" void WardObjects__ctor_m2160 (WardObjects_t373 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WardObjects::.cctor()
extern "C" void WardObjects__cctor_m2161 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
