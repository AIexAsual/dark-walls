﻿#pragma once
#include <stdint.h>
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// UnityEngine.SpaceAttribute
struct  SpaceAttribute_t1048  : public PropertyAttribute_t1046
{
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;
};
