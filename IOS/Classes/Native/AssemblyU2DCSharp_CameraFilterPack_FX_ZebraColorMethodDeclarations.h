﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_ZebraColor
struct CameraFilterPack_FX_ZebraColor_t141;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_ZebraColor::.ctor()
extern "C" void CameraFilterPack_FX_ZebraColor__ctor_m914 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_ZebraColor::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_ZebraColor_get_material_m915 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::Start()
extern "C" void CameraFilterPack_FX_ZebraColor_Start_m916 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_ZebraColor_OnRenderImage_m917 (CameraFilterPack_FX_ZebraColor_t141 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::OnValidate()
extern "C" void CameraFilterPack_FX_ZebraColor_OnValidate_m918 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::Update()
extern "C" void CameraFilterPack_FX_ZebraColor_Update_m919 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_ZebraColor::OnDisable()
extern "C" void CameraFilterPack_FX_ZebraColor_OnDisable_m920 (CameraFilterPack_FX_ZebraColor_t141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
