﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ArrayIndexes
struct ArrayIndexes_t441;

// System.Void ArrayIndexes::.ctor()
extern "C" void ArrayIndexes__ctor_m2687 (ArrayIndexes_t441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
