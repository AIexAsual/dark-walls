﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<iTween/LoopType>
struct IList_1_t570;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>
struct  ReadOnlyCollection_1_t2459  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<iTween/LoopType>::list
	Object_t* ___list_0;
};
