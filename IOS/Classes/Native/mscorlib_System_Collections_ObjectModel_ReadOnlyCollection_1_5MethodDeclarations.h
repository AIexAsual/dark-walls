﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>
struct ReadOnlyCollection_1_t2291;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<System.Char>
struct IList_1_t2290;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Char[]
struct CharU5BU5D_t530;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t2174;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m14392_gshared (ReadOnlyCollection_1_t2291 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m14392(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2291 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m14392_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14393_gshared (ReadOnlyCollection_1_t2291 * __this, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14393(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2291 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14393_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14394_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14394(__this, method) (( void (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14394_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14395_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14395(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2291 *, int32_t, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14395_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14396_gshared (ReadOnlyCollection_1_t2291 * __this, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14396(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2291 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14396_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14397_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14397(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14397_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14398_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14398(__this, ___index, method) (( uint16_t (*) (ReadOnlyCollection_1_t2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14398_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14399_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14399(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2291 *, int32_t, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14399_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14400_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14400(__this, method) (( bool (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14400_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14401_gshared (ReadOnlyCollection_1_t2291 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14401(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2291 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14401_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14402_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14402(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14402_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m14403_gshared (ReadOnlyCollection_1_t2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m14403(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m14403_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m14404_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m14404(__this, method) (( void (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m14404_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m14405_gshared (ReadOnlyCollection_1_t2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m14405(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m14405_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14406_gshared (ReadOnlyCollection_1_t2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14406(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14406_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m14407_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m14407(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2291 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m14407_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m14408_gshared (ReadOnlyCollection_1_t2291 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m14408(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2291 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m14408_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14409_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14409(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14409_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14410_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14410(__this, method) (( bool (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14410_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14411_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14411(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14411_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14412_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14412(__this, method) (( bool (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14412_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14413_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14413(__this, method) (( bool (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14413_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m14414_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m14414(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m14414_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m14415_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m14415(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2291 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m14415_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m14416_gshared (ReadOnlyCollection_1_t2291 * __this, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m14416(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2291 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m14416_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m14417_gshared (ReadOnlyCollection_1_t2291 * __this, CharU5BU5D_t530* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m14417(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2291 *, CharU5BU5D_t530*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m14417_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m14418_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m14418(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m14418_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m14419_gshared (ReadOnlyCollection_1_t2291 * __this, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m14419(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2291 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m14419_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m14420_gshared (ReadOnlyCollection_1_t2291 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m14420(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2291 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m14420_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>::get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_get_Item_m14421_gshared (ReadOnlyCollection_1_t2291 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m14421(__this, ___index, method) (( uint16_t (*) (ReadOnlyCollection_1_t2291 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m14421_gshared)(__this, ___index, method)
