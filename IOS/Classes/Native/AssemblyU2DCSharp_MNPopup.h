﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Action`1<MNDialogResult>
struct Action_1_t277;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// MNPopup
struct  MNPopup_t278  : public MonoBehaviour_t4
{
	// System.String MNPopup::title
	String_t* ___title_2;
	// System.String MNPopup::message
	String_t* ___message_3;
	// System.Action`1<MNDialogResult> MNPopup::OnComplete
	Action_1_t277 * ___OnComplete_4;
};
struct MNPopup_t278_StaticFields{
	// System.Action`1<MNDialogResult> MNPopup::<>f__am$cache3
	Action_1_t277 * ___U3CU3Ef__amU24cache3_5;
};
