﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct Enumerator_t2891;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1412;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__25MethodDeclarations.h"
#define Enumerator__ctor_m22518(__this, ___dictionary, method) (( void (*) (Enumerator_t2891 *, Dictionary_2_t1412 *, const MethodInfo*))Enumerator__ctor_m22427_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22519(__this, method) (( Object_t * (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22428_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22520(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22429_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22521(__this, method) (( Object_t * (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22430_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22522(__this, method) (( Object_t * (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22431_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m22523(__this, method) (( bool (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_MoveNext_m22432_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m22524(__this, method) (( KeyValuePair_2_t2888  (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_get_Current_m22433_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22525(__this, method) (( String_t* (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_get_CurrentKey_m22434_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22526(__this, method) (( bool (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_get_CurrentValue_m22435_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m22527(__this, method) (( void (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_VerifyState_m22436_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22528(__this, method) (( void (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_VerifyCurrent_m22437_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m22529(__this, method) (( void (*) (Enumerator_t2891 *, const MethodInfo*))Enumerator_Dispose_m22438_gshared)(__this, method)
