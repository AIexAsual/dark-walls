﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.GlobalProxySelection
struct GlobalProxySelection_t1404;
// System.Net.IWebProxy
struct IWebProxy_t1399;

// System.Net.IWebProxy System.Net.GlobalProxySelection::get_Select()
extern "C" Object_t * GlobalProxySelection_get_Select_m7670 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
