﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Colors_Adjust_ColorRGB
struct  CameraFilterPack_Colors_Adjust_ColorRGB_t69  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Colors_Adjust_ColorRGB::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Colors_Adjust_ColorRGB::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Colors_Adjust_ColorRGB::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::Red
	float ___Red_6;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::Green
	float ___Green_7;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::Blue
	float ___Blue_8;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::Brightness
	float ___Brightness_9;
};
struct CameraFilterPack_Colors_Adjust_ColorRGB_t69_StaticFields{
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Colors_Adjust_ColorRGB::ChangeValue4
	float ___ChangeValue4_13;
};
