﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<iTweenEvent,System.Boolean>
struct Func_2_t559;
// System.Object
struct Object_t;
// iTweenEvent
struct iTweenEvent_t443;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<iTweenEvent,System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Boolean>
#include "System_Core_System_Func_2_gen_5MethodDeclarations.h"
#define Func_2__ctor_m3386(__this, ___object, ___method, method) (( void (*) (Func_2_t559 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m14265_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<iTweenEvent,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m15499(__this, ___arg1, method) (( bool (*) (Func_2_t559 *, iTweenEvent_t443 *, const MethodInfo*))Func_2_Invoke_m14267_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<iTweenEvent,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m15500(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t559 *, iTweenEvent_t443 *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m14269_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<iTweenEvent,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m15501(__this, ___result, method) (( bool (*) (Func_2_t559 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m14271_gshared)(__this, ___result, method)
