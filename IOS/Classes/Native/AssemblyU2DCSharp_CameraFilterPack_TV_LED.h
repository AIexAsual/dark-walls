﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_LED
struct  CameraFilterPack_TV_LED_t185  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_LED::SCShader
	Shader_t1 * ___SCShader_2;
	// UnityEngine.Vector4 CameraFilterPack_TV_LED::ScreenResolution
	Vector4_t5  ___ScreenResolution_3;
	// System.Single CameraFilterPack_TV_LED::TimeX
	float ___TimeX_4;
	// System.Single CameraFilterPack_TV_LED::Distortion
	float ___Distortion_5;
	// System.Int32 CameraFilterPack_TV_LED::Size
	int32_t ___Size_6;
	// UnityEngine.Material CameraFilterPack_TV_LED::SCMaterial
	Material_t2 * ___SCMaterial_7;
};
