﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Light_Rainbow2
struct  CameraFilterPack_Light_Rainbow2_t156  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Light_Rainbow2::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Light_Rainbow2::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Light_Rainbow2::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Light_Rainbow2::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Light_Rainbow2::Value
	float ___Value_6;
};
struct CameraFilterPack_Light_Rainbow2_t156_StaticFields{
	// System.Single CameraFilterPack_Light_Rainbow2::ChangeValue
	float ___ChangeValue_7;
};
