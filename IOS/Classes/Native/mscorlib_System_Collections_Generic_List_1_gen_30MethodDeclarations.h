﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t779;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t515;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2280;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1094;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2228;
// System.Object[]
struct ObjectU5BU5D_t470;
// System.Predicate`1<System.Object>
struct Predicate_1_t2233;
// System.Comparison`1<System.Object>
struct Comparison_1_t2239;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m6426_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1__ctor_m6426(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m13490_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m13490(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m13492_gshared (List_1_t779 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m13492(__this, ___capacity, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m13494_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m13494(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496(__this, method) (( Object_t* (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m13498_gshared (List_1_t779 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m13498(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t779 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13500(__this, method) (( Object_t * (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m13502_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m13502(__this, ___item, method) (( int32_t (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m13504_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m13504(__this, ___item, method) (( bool (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m13506_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m13506(__this, ___item, method) (( int32_t (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m13508_gshared (List_1_t779 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m13508(__this, ___index, ___item, method) (( void (*) (List_1_t779 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m13510_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m13510(__this, ___item, method) (( void (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13514(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m13516(__this, method) (( Object_t * (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m13518(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m13520(__this, method) (( bool (*) (List_1_t779 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m13522_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m13522(__this, ___index, method) (( Object_t * (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m13524_gshared (List_1_t779 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m13524(__this, ___index, ___value, method) (( void (*) (List_1_t779 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m13526_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m13526(__this, ___item, method) (( void (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m13528_gshared (List_1_t779 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m13528(__this, ___newCount, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m13530_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m13530(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m13532_gshared (List_1_t779 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m13532(__this, ___enumerable, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m13534_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m13534(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2228 * List_1_AsReadOnly_m13536_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m13536(__this, method) (( ReadOnlyCollection_1_t2228 * (*) (List_1_t779 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m13538_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_Clear_m13538(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m13540_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m13540(__this, ___item, method) (( bool (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m13542_gshared (List_1_t779 * __this, ObjectU5BU5D_t470* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m13542(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t779 *, ObjectU5BU5D_t470*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m13544_gshared (List_1_t779 * __this, Predicate_1_t2233 * ___match, const MethodInfo* method);
#define List_1_Find_m13544(__this, ___match, method) (( Object_t * (*) (List_1_t779 *, Predicate_1_t2233 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m13546_gshared (Object_t * __this /* static, unused */, Predicate_1_t2233 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m13546(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2233 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m13548_gshared (List_1_t779 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2233 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m13548(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t779 *, int32_t, int32_t, Predicate_1_t2233 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2227  List_1_GetEnumerator_m13550_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m13550(__this, method) (( Enumerator_t2227  (*) (List_1_t779 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m13552_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m13552(__this, ___item, method) (( int32_t (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m13554_gshared (List_1_t779 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m13554(__this, ___start, ___delta, method) (( void (*) (List_1_t779 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m13556_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m13556(__this, ___index, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m13558_gshared (List_1_t779 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m13558(__this, ___index, ___item, method) (( void (*) (List_1_t779 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m13560_gshared (List_1_t779 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m13560(__this, ___collection, method) (( void (*) (List_1_t779 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m13562_gshared (List_1_t779 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m13562(__this, ___item, method) (( bool (*) (List_1_t779 *, Object_t *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m13564_gshared (List_1_t779 * __this, Predicate_1_t2233 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m13564(__this, ___match, method) (( int32_t (*) (List_1_t779 *, Predicate_1_t2233 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m13566_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m13566(__this, ___index, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m13568_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_Reverse_m13568(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m13570_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_Sort_m13570(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m13572_gshared (List_1_t779 * __this, Comparison_1_t2239 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m13572(__this, ___comparison, method) (( void (*) (List_1_t779 *, Comparison_1_t2239 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t470* List_1_ToArray_m13573_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_ToArray_m13573(__this, method) (( ObjectU5BU5D_t470* (*) (List_1_t779 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m13575_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m13575(__this, method) (( void (*) (List_1_t779 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m13577_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m13577(__this, method) (( int32_t (*) (List_1_t779 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m13579_gshared (List_1_t779 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m13579(__this, ___value, method) (( void (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m13581_gshared (List_1_t779 * __this, const MethodInfo* method);
#define List_1_get_Count_m13581(__this, method) (( int32_t (*) (List_1_t779 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m13583_gshared (List_1_t779 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m13583(__this, ___index, method) (( Object_t * (*) (List_1_t779 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m13585_gshared (List_1_t779 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m13585(__this, ___index, ___value, method) (( void (*) (List_1_t779 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
