﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blur_Radial
struct  CameraFilterPack_Blur_Radial_t56  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Blur_Radial::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Radial::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Radial::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Radial::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Blur_Radial::Intensity
	float ___Intensity_6;
	// System.Single CameraFilterPack_Blur_Radial::MovX
	float ___MovX_7;
	// System.Single CameraFilterPack_Blur_Radial::MovY
	float ___MovY_8;
	// System.Single CameraFilterPack_Blur_Radial::blurWidth
	float ___blurWidth_9;
};
struct CameraFilterPack_Blur_Radial_t56_StaticFields{
	// System.Single CameraFilterPack_Blur_Radial::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Blur_Radial::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Blur_Radial::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Blur_Radial::ChangeValue4
	float ___ChangeValue4_13;
};
