﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19
struct U3CBlendLookWeightU3Ec__Iterator19_t414;
// System.Object
struct Object_t;

// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::.ctor()
extern "C" void U3CBlendLookWeightU3Ec__Iterator19__ctor_m2361 (U3CBlendLookWeightU3Ec__Iterator19_t414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CBlendLookWeightU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2362 (U3CBlendLookWeightU3Ec__Iterator19_t414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CBlendLookWeightU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m2363 (U3CBlendLookWeightU3Ec__Iterator19_t414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::MoveNext()
extern "C" bool U3CBlendLookWeightU3Ec__Iterator19_MoveNext_m2364 (U3CBlendLookWeightU3Ec__Iterator19_t414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::Dispose()
extern "C" void U3CBlendLookWeightU3Ec__Iterator19_Dispose_m2365 (U3CBlendLookWeightU3Ec__Iterator19_t414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::Reset()
extern "C" void U3CBlendLookWeightU3Ec__Iterator19_Reset_m2366 (U3CBlendLookWeightU3Ec__Iterator19_t414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
