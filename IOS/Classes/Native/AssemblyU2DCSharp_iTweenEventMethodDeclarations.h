﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTweenEvent
struct iTweenEvent_t443;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t456;
// UnityEngine.GameObject
struct GameObject_t256;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void iTweenEvent::.ctor()
extern "C" void iTweenEvent__ctor_m2696 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// iTweenEvent iTweenEvent::GetEvent(UnityEngine.GameObject,System.String)
extern "C" iTweenEvent_t443 * iTweenEvent_GetEvent_m2697 (Object_t * __this /* static, unused */, GameObject_t256 * ___obj, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.Object> iTweenEvent::get_Values()
extern "C" Dictionary_2_t456 * iTweenEvent_get_Values_m2698 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent::set_Values(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" void iTweenEvent_set_Values_m2699 (iTweenEvent_t443 * __this, Dictionary_2_t456 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent::Start()
extern "C" void iTweenEvent_Start_m2700 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent::Play()
extern "C" void iTweenEvent_Play_m2701 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent::Stop()
extern "C" void iTweenEvent_Stop_m2702 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent::OnDrawGizmos()
extern "C" void iTweenEvent_OnDrawGizmos_m2703 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator iTweenEvent::StartEvent()
extern "C" Object_t * iTweenEvent_StartEvent_m2704 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent::SerializeValues()
extern "C" void iTweenEvent_SerializeValues_m2705 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTweenEvent::DeserializeValues()
extern "C" void iTweenEvent_DeserializeValues_m2706 (iTweenEvent_t443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
