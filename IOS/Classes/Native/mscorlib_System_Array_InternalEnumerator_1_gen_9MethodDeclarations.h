﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct InternalEnumerator_1_t2248;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13807_gshared (InternalEnumerator_1_t2248 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13807(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2248 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13807_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13808_gshared (InternalEnumerator_1_t2248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13808(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2248 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13808_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13809_gshared (InternalEnumerator_1_t2248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13809(__this, method) (( void (*) (InternalEnumerator_1_t2248 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13809_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13810_gshared (InternalEnumerator_1_t2248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13810(__this, method) (( bool (*) (InternalEnumerator_1_t2248 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13810_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t511  InternalEnumerator_1_get_Current_m13811_gshared (InternalEnumerator_1_t2248 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13811(__this, method) (( RaycastResult_t511  (*) (InternalEnumerator_1_t2248 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13811_gshared)(__this, method)
