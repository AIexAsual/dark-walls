﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_EarthQuake
struct CameraFilterPack_FX_EarthQuake_t126;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_EarthQuake::.ctor()
extern "C" void CameraFilterPack_FX_EarthQuake__ctor_m813 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_EarthQuake::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_EarthQuake_get_material_m814 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::Start()
extern "C" void CameraFilterPack_FX_EarthQuake_Start_m815 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_EarthQuake_OnRenderImage_m816 (CameraFilterPack_FX_EarthQuake_t126 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::OnValidate()
extern "C" void CameraFilterPack_FX_EarthQuake_OnValidate_m817 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::Update()
extern "C" void CameraFilterPack_FX_EarthQuake_Update_m818 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_EarthQuake::OnDisable()
extern "C" void CameraFilterPack_FX_EarthQuake_OnDisable_m819 (CameraFilterPack_FX_EarthQuake_t126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
