﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t398;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.AppDomainInitializer
struct  AppDomainInitializer_t2078  : public MulticastDelegate_t219
{
};
