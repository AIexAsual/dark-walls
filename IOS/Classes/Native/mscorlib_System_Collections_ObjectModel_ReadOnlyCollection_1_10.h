﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t566;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct  ReadOnlyCollection_1_t2423  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>::list
	Object_t* ___list_0;
};
