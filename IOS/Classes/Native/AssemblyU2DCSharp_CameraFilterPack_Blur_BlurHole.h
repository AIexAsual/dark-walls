﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blur_BlurHole
struct  CameraFilterPack_Blur_BlurHole_t48  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Blur_BlurHole::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_BlurHole::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Blur_BlurHole::Size
	float ___Size_4;
	// System.Single CameraFilterPack_Blur_BlurHole::_Radius
	float ____Radius_5;
	// System.Single CameraFilterPack_Blur_BlurHole::_SpotSize
	float ____SpotSize_6;
	// System.Single CameraFilterPack_Blur_BlurHole::_CenterX
	float ____CenterX_7;
	// System.Single CameraFilterPack_Blur_BlurHole::_CenterY
	float ____CenterY_8;
	// System.Single CameraFilterPack_Blur_BlurHole::_AlphaBlur
	float ____AlphaBlur_9;
	// System.Single CameraFilterPack_Blur_BlurHole::_AlphaBlurInside
	float ____AlphaBlurInside_10;
	// UnityEngine.Vector4 CameraFilterPack_Blur_BlurHole::ScreenResolution
	Vector4_t5  ___ScreenResolution_11;
	// UnityEngine.Material CameraFilterPack_Blur_BlurHole::SCMaterial
	Material_t2 * ___SCMaterial_12;
};
struct CameraFilterPack_Blur_BlurHole_t48_StaticFields{
	// System.Single CameraFilterPack_Blur_BlurHole::ChangeSize
	float ___ChangeSize_13;
};
