﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
struct Enumerator_t2477;
// System.Object
struct Object_t;
// UnityEngine.AudioSource
struct AudioSource_t339;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t585;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
#define Enumerator__ctor_m16908(__this, ___l, method) (( void (*) (Enumerator_t2477 *, List_1_t585 *, const MethodInfo*))Enumerator__ctor_m13586_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16909(__this, method) (( Object_t * (*) (Enumerator_t2477 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13587_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::Dispose()
#define Enumerator_Dispose_m16910(__this, method) (( void (*) (Enumerator_t2477 *, const MethodInfo*))Enumerator_Dispose_m13588_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::VerifyState()
#define Enumerator_VerifyState_m16911(__this, method) (( void (*) (Enumerator_t2477 *, const MethodInfo*))Enumerator_VerifyState_m13589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::MoveNext()
#define Enumerator_MoveNext_m16912(__this, method) (( bool (*) (Enumerator_t2477 *, const MethodInfo*))Enumerator_MoveNext_m13590_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>::get_Current()
#define Enumerator_get_Current_m16913(__this, method) (( AudioSource_t339 * (*) (Enumerator_t2477 *, const MethodInfo*))Enumerator_get_Current_m13591_gshared)(__this, method)
