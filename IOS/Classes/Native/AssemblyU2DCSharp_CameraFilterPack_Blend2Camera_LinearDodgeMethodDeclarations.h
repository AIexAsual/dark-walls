﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_LinearDodge
struct CameraFilterPack_Blend2Camera_LinearDodge_t33;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_LinearDodge::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_LinearDodge__ctor_m177 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_LinearDodge::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_LinearDodge_get_material_m178 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::Start()
extern "C" void CameraFilterPack_Blend2Camera_LinearDodge_Start_m179 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_LinearDodge_OnRenderImage_m180 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_LinearDodge_OnValidate_m181 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::Update()
extern "C" void CameraFilterPack_Blend2Camera_LinearDodge_Update_m182 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_LinearDodge_OnEnable_m183 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LinearDodge::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_LinearDodge_OnDisable_m184 (CameraFilterPack_Blend2Camera_LinearDodge_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
