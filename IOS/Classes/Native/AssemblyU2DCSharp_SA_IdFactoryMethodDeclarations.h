﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SA_IdFactory
struct SA_IdFactory_t299;
// System.String
struct String_t;

// System.Void SA_IdFactory::.ctor()
extern "C" void SA_IdFactory__ctor_m1832 (SA_IdFactory_t299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SA_IdFactory::get_NextId()
extern "C" int32_t SA_IdFactory_get_NextId_m1833 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SA_IdFactory::get_RandomString()
extern "C" String_t* SA_IdFactory_get_RandomString_m1834 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
