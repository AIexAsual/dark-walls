﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Colors_HSV
struct  CameraFilterPack_Colors_HSV_t76  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Colors_HSV::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Colors_HSV::_HueShift
	float ____HueShift_3;
	// System.Single CameraFilterPack_Colors_HSV::_Saturation
	float ____Saturation_4;
	// System.Single CameraFilterPack_Colors_HSV::_ValueBrightness
	float ____ValueBrightness_5;
	// UnityEngine.Material CameraFilterPack_Colors_HSV::SCMaterial
	Material_t2 * ___SCMaterial_6;
};
struct CameraFilterPack_Colors_HSV_t76_StaticFields{
	// System.Single CameraFilterPack_Colors_HSV::ChangeHueShift
	float ___ChangeHueShift_7;
	// System.Single CameraFilterPack_Colors_HSV::ChangeSaturation
	float ___ChangeSaturation_8;
	// System.Single CameraFilterPack_Colors_HSV::ChangeValueBrightness
	float ___ChangeValueBrightness_9;
};
