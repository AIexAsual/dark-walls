﻿#pragma once
#include <stdint.h>
// System.Single[]
struct SingleU5BU5D_t72;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Single>
struct  List_1_t576  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Single>::_items
	SingleU5BU5D_t72* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Single>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Single>::_version
	int32_t ____version_3;
};
struct List_1_t576_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Single>::EmptyArray
	SingleU5BU5D_t72* ___EmptyArray_4;
};
