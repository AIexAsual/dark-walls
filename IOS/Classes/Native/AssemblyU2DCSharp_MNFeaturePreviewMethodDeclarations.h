﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNFeaturePreview
struct MNFeaturePreview_t274;

// System.Void MNFeaturePreview::.ctor()
extern "C" void MNFeaturePreview__ctor_m1719 (MNFeaturePreview_t274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNFeaturePreview::InitStyles()
extern "C" void MNFeaturePreview_InitStyles_m1720 (MNFeaturePreview_t274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNFeaturePreview::Start()
extern "C" void MNFeaturePreview_Start_m1721 (MNFeaturePreview_t274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNFeaturePreview::UpdateToStartPos()
extern "C" void MNFeaturePreview_UpdateToStartPos_m1722 (MNFeaturePreview_t274 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
