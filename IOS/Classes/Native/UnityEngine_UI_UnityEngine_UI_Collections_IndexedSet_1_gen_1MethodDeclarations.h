﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t2535;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2280;
// System.Object[]
struct ObjectU5BU5D_t470;
// System.Predicate`1<System.Object>
struct Predicate_1_t2233;
// System.Comparison`1<System.Object>
struct Comparison_1_t2239;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m17771_gshared (IndexedSet_1_t2535 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m17771(__this, method) (( void (*) (IndexedSet_1_t2535 *, const MethodInfo*))IndexedSet_1__ctor_m17771_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17773_gshared (IndexedSet_1_t2535 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17773(__this, method) (( Object_t * (*) (IndexedSet_1_t2535 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17773_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m17775_gshared (IndexedSet_1_t2535 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m17775(__this, ___item, method) (( void (*) (IndexedSet_1_t2535 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m17775_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m17777_gshared (IndexedSet_1_t2535 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m17777(__this, ___item, method) (( bool (*) (IndexedSet_1_t2535 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m17777_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m17779_gshared (IndexedSet_1_t2535 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m17779(__this, method) (( Object_t* (*) (IndexedSet_1_t2535 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m17779_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m17781_gshared (IndexedSet_1_t2535 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m17781(__this, method) (( void (*) (IndexedSet_1_t2535 *, const MethodInfo*))IndexedSet_1_Clear_m17781_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m17783_gshared (IndexedSet_1_t2535 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m17783(__this, ___item, method) (( bool (*) (IndexedSet_1_t2535 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m17783_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m17785_gshared (IndexedSet_1_t2535 * __this, ObjectU5BU5D_t470* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m17785(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t2535 *, ObjectU5BU5D_t470*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m17785_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m17787_gshared (IndexedSet_1_t2535 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m17787(__this, method) (( int32_t (*) (IndexedSet_1_t2535 *, const MethodInfo*))IndexedSet_1_get_Count_m17787_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m17789_gshared (IndexedSet_1_t2535 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m17789(__this, method) (( bool (*) (IndexedSet_1_t2535 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m17789_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m17791_gshared (IndexedSet_1_t2535 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m17791(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t2535 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m17791_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m17793_gshared (IndexedSet_1_t2535 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m17793(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t2535 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m17793_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m17795_gshared (IndexedSet_1_t2535 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m17795(__this, ___index, method) (( void (*) (IndexedSet_1_t2535 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m17795_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m17797_gshared (IndexedSet_1_t2535 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m17797(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t2535 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m17797_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m17799_gshared (IndexedSet_1_t2535 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m17799(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t2535 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m17799_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m17800_gshared (IndexedSet_1_t2535 * __this, Predicate_1_t2233 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m17800(__this, ___match, method) (( void (*) (IndexedSet_1_t2535 *, Predicate_1_t2233 *, const MethodInfo*))IndexedSet_1_RemoveAll_m17800_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m17801_gshared (IndexedSet_1_t2535 * __this, Comparison_1_t2239 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m17801(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t2535 *, Comparison_1_t2239 *, const MethodInfo*))IndexedSet_1_Sort_m17801_gshared)(__this, ___sortLayoutFunction, method)
