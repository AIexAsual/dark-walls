﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TestEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t387;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// testClass
#include "AssemblyU2DCSharp_testClass.h"

// System.Void TestEventManager/OnStateChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void OnStateChangeHandler__ctor_m2204 (OnStateChangeHandler_t387 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager/OnStateChangeHandler::Invoke(testClass)
extern "C" void OnStateChangeHandler_Invoke_m2205 (OnStateChangeHandler_t387 * __this, int32_t ___tc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnStateChangeHandler_t387(Il2CppObject* delegate, int32_t ___tc);
// System.IAsyncResult TestEventManager/OnStateChangeHandler::BeginInvoke(testClass,System.AsyncCallback,System.Object)
extern "C" Object_t * OnStateChangeHandler_BeginInvoke_m2206 (OnStateChangeHandler_t387 * __this, int32_t ___tc, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestEventManager/OnStateChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void OnStateChangeHandler_EndInvoke_m2207 (OnStateChangeHandler_t387 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
