﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_ARCADE
struct CameraFilterPack_TV_ARCADE_t177;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_ARCADE::.ctor()
extern "C" void CameraFilterPack_TV_ARCADE__ctor_m1145 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_ARCADE::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_ARCADE_get_material_m1146 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE::Start()
extern "C" void CameraFilterPack_TV_ARCADE_Start_m1147 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_ARCADE_OnRenderImage_m1148 (CameraFilterPack_TV_ARCADE_t177 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE::Update()
extern "C" void CameraFilterPack_TV_ARCADE_Update_m1149 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_ARCADE::OnDisable()
extern "C" void CameraFilterPack_TV_ARCADE_OnDisable_m1150 (CameraFilterPack_TV_ARCADE_t177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
