﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Hexagon_Black
struct CameraFilterPack_FX_Hexagon_Black_t133;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Hexagon_Black::.ctor()
extern "C" void CameraFilterPack_FX_Hexagon_Black__ctor_m860 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Hexagon_Black::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Hexagon_Black_get_material_m861 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon_Black::Start()
extern "C" void CameraFilterPack_FX_Hexagon_Black_Start_m862 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon_Black::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Hexagon_Black_OnRenderImage_m863 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon_Black::OnValidate()
extern "C" void CameraFilterPack_FX_Hexagon_Black_OnValidate_m864 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon_Black::Update()
extern "C" void CameraFilterPack_FX_Hexagon_Black_Update_m865 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hexagon_Black::OnDisable()
extern "C" void CameraFilterPack_FX_Hexagon_Black_OnDisable_m866 (CameraFilterPack_FX_Hexagon_Black_t133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
