﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNIOSRateUsPopUp
struct MNIOSRateUsPopUp_t290;
// System.String
struct String_t;

// System.Void MNIOSRateUsPopUp::.ctor()
extern "C" void MNIOSRateUsPopUp__ctor_m1789 (MNIOSRateUsPopUp_t290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create()
extern "C" MNIOSRateUsPopUp_t290 * MNIOSRateUsPopUp_Create_m1790 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create(System.String,System.String)
extern "C" MNIOSRateUsPopUp_t290 * MNIOSRateUsPopUp_Create_m1791 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MNIOSRateUsPopUp MNIOSRateUsPopUp::Create(System.String,System.String,System.String,System.String,System.String)
extern "C" MNIOSRateUsPopUp_t290 * MNIOSRateUsPopUp_Create_m1792 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, String_t* ___rate, String_t* ___remind, String_t* ___declined, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSRateUsPopUp::init()
extern "C" void MNIOSRateUsPopUp_init_m1793 (MNIOSRateUsPopUp_t290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNIOSRateUsPopUp::onPopUpCallBack(System.String)
extern "C" void MNIOSRateUsPopUp_onPopUpCallBack_m1794 (MNIOSRateUsPopUp_t290 * __this, String_t* ___buttonIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
