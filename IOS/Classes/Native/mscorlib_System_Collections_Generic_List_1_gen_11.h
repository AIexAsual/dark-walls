﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform[]
struct TransformU5BU5D_t423;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct  List_1_t583  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Transform>::_items
	TransformU5BU5D_t423* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::_version
	int32_t ____version_3;
};
struct List_1_t583_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Transform>::EmptyArray
	TransformU5BU5D_t423* ___EmptyArray_4;
};
