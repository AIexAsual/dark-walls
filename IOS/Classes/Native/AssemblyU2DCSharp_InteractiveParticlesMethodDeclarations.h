﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveParticles
struct InteractiveParticles_t334;

// System.Void InteractiveParticles::.ctor()
extern "C" void InteractiveParticles__ctor_m1971 (InteractiveParticles_t334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveParticles::Start()
extern "C" void InteractiveParticles_Start_m1972 (InteractiveParticles_t334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveParticles::Update()
extern "C" void InteractiveParticles_Update_m1973 (InteractiveParticles_t334 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveParticles::SetGazedAt(System.Boolean)
extern "C" void InteractiveParticles_SetGazedAt_m1974 (InteractiveParticles_t334 * __this, bool ___gazeAt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveParticles::Emitter(System.Boolean)
extern "C" void InteractiveParticles_Emitter_m1975 (InteractiveParticles_t334 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
