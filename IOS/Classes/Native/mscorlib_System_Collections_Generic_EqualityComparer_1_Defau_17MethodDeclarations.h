﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>
struct DefaultComparer_t2838;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C" void DefaultComparer__ctor_m21970_gshared (DefaultComparer_t2838 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m21970(__this, method) (( void (*) (DefaultComparer_t2838 *, const MethodInfo*))DefaultComparer__ctor_m21970_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m21971_gshared (DefaultComparer_t2838 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m21971(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2838 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m21971_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.TextEditor/TextEditOp>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m21972_gshared (DefaultComparer_t2838 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m21972(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2838 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m21972_gshared)(__this, ___x, ___y, method)
