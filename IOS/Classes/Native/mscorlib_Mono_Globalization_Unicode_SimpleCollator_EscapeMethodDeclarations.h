﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t1593;
struct Escape_t1593_marshaled;

void Escape_t1593_marshal(const Escape_t1593& unmarshaled, Escape_t1593_marshaled& marshaled);
void Escape_t1593_marshal_back(const Escape_t1593_marshaled& marshaled, Escape_t1593& unmarshaled);
void Escape_t1593_marshal_cleanup(Escape_t1593_marshaled& marshaled);
