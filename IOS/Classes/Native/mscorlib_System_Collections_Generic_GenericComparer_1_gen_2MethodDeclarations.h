﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t2221;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m13472_gshared (GenericComparer_1_t2221 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m13472(__this, method) (( void (*) (GenericComparer_1_t2221 *, const MethodInfo*))GenericComparer_1__ctor_m13472_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m23040_gshared (GenericComparer_1_t2221 * __this, TimeSpan_t1437  ___x, TimeSpan_t1437  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m23040(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2221 *, TimeSpan_t1437 , TimeSpan_t1437 , const MethodInfo*))GenericComparer_1_Compare_m23040_gshared)(__this, ___x, ___y, method)
