﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$56
struct U24ArrayTypeU2456_t2151;
struct U24ArrayTypeU2456_t2151_marshaled;

void U24ArrayTypeU2456_t2151_marshal(const U24ArrayTypeU2456_t2151& unmarshaled, U24ArrayTypeU2456_t2151_marshaled& marshaled);
void U24ArrayTypeU2456_t2151_marshal_back(const U24ArrayTypeU2456_t2151_marshaled& marshaled, U24ArrayTypeU2456_t2151& unmarshaled);
void U24ArrayTypeU2456_t2151_marshal_cleanup(U24ArrayTypeU2456_t2151_marshaled& marshaled);
