﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Blurry
struct CameraFilterPack_Blur_Blurry_t49;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Blurry::.ctor()
extern "C" void CameraFilterPack_Blur_Blurry__ctor_m296 (CameraFilterPack_Blur_Blurry_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Blurry::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Blurry_get_material_m297 (CameraFilterPack_Blur_Blurry_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::Start()
extern "C" void CameraFilterPack_Blur_Blurry_Start_m298 (CameraFilterPack_Blur_Blurry_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Blurry_OnRenderImage_m299 (CameraFilterPack_Blur_Blurry_t49 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::OnValidate()
extern "C" void CameraFilterPack_Blur_Blurry_OnValidate_m300 (CameraFilterPack_Blur_Blurry_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::Update()
extern "C" void CameraFilterPack_Blur_Blurry_Update_m301 (CameraFilterPack_Blur_Blurry_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Blurry::OnDisable()
extern "C" void CameraFilterPack_Blur_Blurry_OnDisable_m302 (CameraFilterPack_Blur_Blurry_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
