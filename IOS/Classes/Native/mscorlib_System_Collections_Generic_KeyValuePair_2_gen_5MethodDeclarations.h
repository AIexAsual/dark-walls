﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>
struct KeyValuePair_2_t2328;
// System.Object
struct Object_t;
// System.String
struct String_t;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m14835_gshared (KeyValuePair_2_t2328 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m14835(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2328 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m14835_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m14836_gshared (KeyValuePair_2_t2328 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m14836(__this, method) (( int32_t (*) (KeyValuePair_2_t2328 *, const MethodInfo*))KeyValuePair_2_get_Key_m14836_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m14837_gshared (KeyValuePair_2_t2328 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m14837(__this, ___value, method) (( void (*) (KeyValuePair_2_t2328 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m14837_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m14838_gshared (KeyValuePair_2_t2328 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m14838(__this, method) (( Object_t * (*) (KeyValuePair_2_t2328 *, const MethodInfo*))KeyValuePair_2_get_Value_m14838_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m14839_gshared (KeyValuePair_2_t2328 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m14839(__this, ___value, method) (( void (*) (KeyValuePair_2_t2328 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m14839_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m14840_gshared (KeyValuePair_2_t2328 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m14840(__this, method) (( String_t* (*) (KeyValuePair_2_t2328 *, const MethodInfo*))KeyValuePair_2_ToString_m14840_gshared)(__this, method)
