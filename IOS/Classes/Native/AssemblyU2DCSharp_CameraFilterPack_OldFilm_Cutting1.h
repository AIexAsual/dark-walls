﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.Texture2D
struct Texture2D_t9;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_OldFilm_Cutting1
struct  CameraFilterPack_OldFilm_Cutting1_t167  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_OldFilm_Cutting1::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_OldFilm_Cutting1::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_OldFilm_Cutting1::Speed
	float ___Speed_4;
	// System.Single CameraFilterPack_OldFilm_Cutting1::Luminosity
	float ___Luminosity_5;
	// System.Single CameraFilterPack_OldFilm_Cutting1::Vignette
	float ___Vignette_6;
	// System.Single CameraFilterPack_OldFilm_Cutting1::Negative
	float ___Negative_7;
	// UnityEngine.Material CameraFilterPack_OldFilm_Cutting1::SCMaterial
	Material_t2 * ___SCMaterial_8;
	// UnityEngine.Texture2D CameraFilterPack_OldFilm_Cutting1::Texture2
	Texture2D_t9 * ___Texture2_9;
};
