﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t2887;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2876;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22474_gshared (ShimEnumerator_t2887 * __this, Dictionary_2_t2876 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22474(__this, ___host, method) (( void (*) (ShimEnumerator_t2887 *, Dictionary_2_t2876 *, const MethodInfo*))ShimEnumerator__ctor_m22474_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22475_gshared (ShimEnumerator_t2887 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22475(__this, method) (( bool (*) (ShimEnumerator_t2887 *, const MethodInfo*))ShimEnumerator_MoveNext_m22475_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t552  ShimEnumerator_get_Entry_m22476_gshared (ShimEnumerator_t2887 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22476(__this, method) (( DictionaryEntry_t552  (*) (ShimEnumerator_t2887 *, const MethodInfo*))ShimEnumerator_get_Entry_m22476_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22477_gshared (ShimEnumerator_t2887 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22477(__this, method) (( Object_t * (*) (ShimEnumerator_t2887 *, const MethodInfo*))ShimEnumerator_get_Key_m22477_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22478_gshared (ShimEnumerator_t2887 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22478(__this, method) (( Object_t * (*) (ShimEnumerator_t2887 *, const MethodInfo*))ShimEnumerator_get_Value_m22478_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22479_gshared (ShimEnumerator_t2887 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22479(__this, method) (( Object_t * (*) (ShimEnumerator_t2887 *, const MethodInfo*))ShimEnumerator_get_Current_m22479_gshared)(__this, method)
