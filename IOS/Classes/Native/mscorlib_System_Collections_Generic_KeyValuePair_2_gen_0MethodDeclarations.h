﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct KeyValuePair_2_t784;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t257;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17616(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t784 *, int32_t, PointerEventData_t257 *, const MethodInfo*))KeyValuePair_2__ctor_m17534_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Key()
#define KeyValuePair_2_get_Key_m4623(__this, method) (( int32_t (*) (KeyValuePair_2_t784 *, const MethodInfo*))KeyValuePair_2_get_Key_m17535_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17617(__this, ___value, method) (( void (*) (KeyValuePair_2_t784 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m17536_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Value()
#define KeyValuePair_2_get_Value_m4622(__this, method) (( PointerEventData_t257 * (*) (KeyValuePair_2_t784 *, const MethodInfo*))KeyValuePair_2_get_Value_m17537_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17618(__this, ___value, method) (( void (*) (KeyValuePair_2_t784 *, PointerEventData_t257 *, const MethodInfo*))KeyValuePair_2_set_Value_m17538_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::ToString()
#define KeyValuePair_2_ToString_m4637(__this, method) (( String_t* (*) (KeyValuePair_2_t784 *, const MethodInfo*))KeyValuePair_2_ToString_m17539_gshared)(__this, method)
