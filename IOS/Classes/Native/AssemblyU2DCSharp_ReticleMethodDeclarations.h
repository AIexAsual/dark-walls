﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Reticle
struct Reticle_t406;

// System.Void Reticle::.ctor()
extern "C" void Reticle__ctor_m2315 (Reticle_t406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reticle::Start()
extern "C" void Reticle_Start_m2316 (Reticle_t406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Reticle::Update()
extern "C" void Reticle_Update_m2317 (Reticle_t406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
