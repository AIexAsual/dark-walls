﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Vision_Plasma
struct  CameraFilterPack_Vision_Plasma_t207  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Vision_Plasma::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Vision_Plasma::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Vision_Plasma::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Vision_Plasma::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Vision_Plasma::Value
	float ___Value_6;
	// System.Single CameraFilterPack_Vision_Plasma::Value2
	float ___Value2_7;
	// System.Single CameraFilterPack_Vision_Plasma::Intensity
	float ___Intensity_8;
	// System.Single CameraFilterPack_Vision_Plasma::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Vision_Plasma_t207_StaticFields{
	// System.Single CameraFilterPack_Vision_Plasma::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Vision_Plasma::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Vision_Plasma::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Vision_Plasma::ChangeValue4
	float ___ChangeValue4_13;
};
