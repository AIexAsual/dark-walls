﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<iTween/EaseType>
struct  Predicate_1_t2454  : public MulticastDelegate_t219
{
};
