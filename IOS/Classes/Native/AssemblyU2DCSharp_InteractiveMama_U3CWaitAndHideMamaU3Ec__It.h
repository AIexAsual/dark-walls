﻿#pragma once
#include <stdint.h>
// UnityEngine.Collider
struct Collider_t319;
// System.Object
struct Object_t;
// InteractiveMama
struct InteractiveMama_t330;
// System.Object
#include "mscorlib_System_Object.h"
// InteractiveMama/<WaitAndHideMama>c__Iterator9
struct  U3CWaitAndHideMamaU3Ec__Iterator9_t333  : public Object_t
{
	// System.Single InteractiveMama/<WaitAndHideMama>c__Iterator9::delay
	float ___delay_0;
	// System.Int32 InteractiveMama/<WaitAndHideMama>c__Iterator9::<i>__0
	int32_t ___U3CiU3E__0_1;
	// System.Boolean InteractiveMama/<WaitAndHideMama>c__Iterator9::val
	bool ___val_2;
	// UnityEngine.Collider InteractiveMama/<WaitAndHideMama>c__Iterator9::<col>__1
	Collider_t319 * ___U3CcolU3E__1_3;
	// System.Int32 InteractiveMama/<WaitAndHideMama>c__Iterator9::$PC
	int32_t ___U24PC_4;
	// System.Object InteractiveMama/<WaitAndHideMama>c__Iterator9::$current
	Object_t * ___U24current_5;
	// System.Single InteractiveMama/<WaitAndHideMama>c__Iterator9::<$>delay
	float ___U3CU24U3Edelay_6;
	// System.Boolean InteractiveMama/<WaitAndHideMama>c__Iterator9::<$>val
	bool ___U3CU24U3Eval_7;
	// InteractiveMama InteractiveMama/<WaitAndHideMama>c__Iterator9::<>f__this
	InteractiveMama_t330 * ___U3CU3Ef__this_8;
};
