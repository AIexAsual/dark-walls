﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t212;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// FPS
struct  FPS_t213  : public MonoBehaviour_t4
{
	// UnityEngine.UI.Text FPS::text
	Text_t212 * ___text_2;
	// System.Single FPS::fps
	float ___fps_3;
};
