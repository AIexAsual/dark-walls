﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2218;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m13466_gshared (GenericEqualityComparer_1_t2218 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m13466(__this, method) (( void (*) (GenericEqualityComparer_1_t2218 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m13466_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m23009_gshared (GenericEqualityComparer_1_t2218 * __this, DateTimeOffset_t1148  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m23009(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2218 *, DateTimeOffset_t1148 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m23009_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m23010_gshared (GenericEqualityComparer_1_t2218 * __this, DateTimeOffset_t1148  ___x, DateTimeOffset_t1148  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m23010(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2218 *, DateTimeOffset_t1148 , DateTimeOffset_t1148 , const MethodInfo*))GenericEqualityComparer_1_Equals_m23010_gshared)(__this, ___x, ___y, method)
