﻿#pragma once
#include <stdint.h>
// InteractiveObjectManager
struct InteractiveObjectManager_t366;
// InteractiveObject
struct InteractiveObject_t328;
// InteractiveDroppingObject
struct InteractiveDroppingObject_t327;
// InteractiveFlashlight
struct InteractiveFlashlight_t329;
// InteractiveRadio
struct InteractiveRadio_t340;
// InteractiveMama
struct InteractiveMama_t330;
// InteractiveBaby
struct InteractiveBaby_t324;
// InteractiveBurnMama
struct InteractiveBurnMama_t326;
// GoTo
struct GoTo_t315;
// Teddy
struct Teddy_t367;
// InteractiveParticles
struct InteractiveParticles_t334;
// TextAppearance
struct TextAppearance_t368;
// InteractivePlasticBag
struct InteractivePlasticBag_t335;
// FadeTexture
struct FadeTexture_t313;
// System.Object
#include "mscorlib_System_Object.h"
// InteractiveObjectManager
struct  InteractiveObjectManager_t366  : public Object_t
{
	// InteractiveObject InteractiveObjectManager::interactiveObjects
	InteractiveObject_t328 * ___interactiveObjects_1;
	// InteractiveDroppingObject InteractiveObjectManager::interactiveDroppingObject
	InteractiveDroppingObject_t327 * ___interactiveDroppingObject_2;
	// InteractiveFlashlight InteractiveObjectManager::interactiveFlashlight
	InteractiveFlashlight_t329 * ___interactiveFlashlight_3;
	// InteractiveRadio InteractiveObjectManager::interactiveRadio
	InteractiveRadio_t340 * ___interactiveRadio_4;
	// InteractiveMama InteractiveObjectManager::interactiveMama
	InteractiveMama_t330 * ___interactiveMama_5;
	// InteractiveBaby InteractiveObjectManager::interactiveBaby
	InteractiveBaby_t324 * ___interactiveBaby_6;
	// InteractiveBurnMama InteractiveObjectManager::interactiveBurnMama
	InteractiveBurnMama_t326 * ___interactiveBurnMama_7;
	// GoTo InteractiveObjectManager::goTo
	GoTo_t315 * ___goTo_8;
	// Teddy InteractiveObjectManager::interactiveTeddy
	Teddy_t367 * ___interactiveTeddy_9;
	// InteractiveParticles InteractiveObjectManager::particle
	InteractiveParticles_t334 * ___particle_10;
	// TextAppearance InteractiveObjectManager::textApp
	TextAppearance_t368 * ___textApp_11;
	// InteractivePlasticBag InteractiveObjectManager::interactivePlasticBag
	InteractivePlasticBag_t335 * ___interactivePlasticBag_12;
	// FadeTexture InteractiveObjectManager::fadeTexture
	FadeTexture_t313 * ___fadeTexture_13;
};
struct InteractiveObjectManager_t366_StaticFields{
	// InteractiveObjectManager InteractiveObjectManager::_instance
	InteractiveObjectManager_t366 * ____instance_0;
};
