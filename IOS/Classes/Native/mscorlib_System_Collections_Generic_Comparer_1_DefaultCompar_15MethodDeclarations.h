﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t2965;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m23028_gshared (DefaultComparer_t2965 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m23028(__this, method) (( void (*) (DefaultComparer_t2965 *, const MethodInfo*))DefaultComparer__ctor_m23028_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m23029_gshared (DefaultComparer_t2965 * __this, Guid_t555  ___x, Guid_t555  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m23029(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2965 *, Guid_t555 , Guid_t555 , const MethodInfo*))DefaultComparer_Compare_m23029_gshared)(__this, ___x, ___y, method)
