﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t273;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// MNFeaturePreview
struct  MNFeaturePreview_t274  : public MonoBehaviour_t4
{
	// UnityEngine.GUIStyle MNFeaturePreview::style
	GUIStyle_t273 * ___style_2;
	// System.Int32 MNFeaturePreview::buttonWidth
	int32_t ___buttonWidth_3;
	// System.Int32 MNFeaturePreview::buttonHeight
	int32_t ___buttonHeight_4;
	// System.Single MNFeaturePreview::StartY
	float ___StartY_5;
	// System.Single MNFeaturePreview::StartX
	float ___StartX_6;
	// System.Single MNFeaturePreview::XStartPos
	float ___XStartPos_7;
	// System.Single MNFeaturePreview::YStartPos
	float ___YStartPos_8;
	// System.Single MNFeaturePreview::XButtonStep
	float ___XButtonStep_9;
	// System.Single MNFeaturePreview::YButtonStep
	float ___YButtonStep_10;
	// System.Single MNFeaturePreview::YLableStep
	float ___YLableStep_11;
};
