﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Oculus_ThermaVision
struct  CameraFilterPack_Oculus_ThermaVision_t166  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Oculus_ThermaVision::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Oculus_ThermaVision::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Oculus_ThermaVision::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Oculus_ThermaVision::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Oculus_ThermaVision::Therma_Variation
	float ___Therma_Variation_6;
	// System.Single CameraFilterPack_Oculus_ThermaVision::Contrast
	float ___Contrast_7;
	// System.Single CameraFilterPack_Oculus_ThermaVision::Burn
	float ___Burn_8;
	// System.Single CameraFilterPack_Oculus_ThermaVision::SceneCut
	float ___SceneCut_9;
};
struct CameraFilterPack_Oculus_ThermaVision_t166_StaticFields{
	// System.Single CameraFilterPack_Oculus_ThermaVision::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Oculus_ThermaVision::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Oculus_ThermaVision::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Oculus_ThermaVision::ChangeValue4
	float ___ChangeValue4_13;
};
