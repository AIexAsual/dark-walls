﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Space>
struct DefaultComparer_t2444;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Space>::.ctor()
extern "C" void DefaultComparer__ctor_m16341_gshared (DefaultComparer_t2444 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16341(__this, method) (( void (*) (DefaultComparer_t2444 *, const MethodInfo*))DefaultComparer__ctor_m16341_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Space>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m16342_gshared (DefaultComparer_t2444 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m16342(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2444 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m16342_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Space>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m16343_gshared (DefaultComparer_t2444 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m16343(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2444 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m16343_gshared)(__this, ___x, ___y, method)
