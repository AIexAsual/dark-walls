﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t530;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct  Enum_t554  : public ValueType_t1540
{
};
struct Enum_t554_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t530* ___split_char_0;
};
