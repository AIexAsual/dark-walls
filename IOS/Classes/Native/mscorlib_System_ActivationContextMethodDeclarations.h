﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ActivationContext
struct ActivationContext_t2070;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ActivationContext::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m12602 (ActivationContext_t2070 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Finalize()
extern "C" void ActivationContext_Finalize_m12603 (ActivationContext_t2070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose()
extern "C" void ActivationContext_Dispose_m12604 (ActivationContext_t2070 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ActivationContext::Dispose(System.Boolean)
extern "C" void ActivationContext_Dispose_m12605 (ActivationContext_t2070 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
