﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct KeyValuePair_2_t2580;
// UnityEngine.Canvas
struct Canvas_t650;
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t803;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m18570(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2580 *, Canvas_t650 *, IndexedSet_1_t803 *, const MethodInfo*))KeyValuePair_2__ctor_m14739_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Key()
#define KeyValuePair_2_get_Key_m18571(__this, method) (( Canvas_t650 * (*) (KeyValuePair_2_t2580 *, const MethodInfo*))KeyValuePair_2_get_Key_m14741_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m18572(__this, ___value, method) (( void (*) (KeyValuePair_2_t2580 *, Canvas_t650 *, const MethodInfo*))KeyValuePair_2_set_Key_m14743_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Value()
#define KeyValuePair_2_get_Value_m18573(__this, method) (( IndexedSet_1_t803 * (*) (KeyValuePair_2_t2580 *, const MethodInfo*))KeyValuePair_2_get_Value_m14745_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m18574(__this, ___value, method) (( void (*) (KeyValuePair_2_t2580 *, IndexedSet_1_t803 *, const MethodInfo*))KeyValuePair_2_set_Value_m14747_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::ToString()
#define KeyValuePair_2_ToString_m18575(__this, method) (( String_t* (*) (KeyValuePair_2_t2580 *, const MethodInfo*))KeyValuePair_2_ToString_m14749_gshared)(__this, method)
