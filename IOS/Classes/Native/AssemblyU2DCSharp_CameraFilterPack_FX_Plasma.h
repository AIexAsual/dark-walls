﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_FX_Plasma
struct  CameraFilterPack_FX_Plasma_t137  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_Plasma::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Plasma::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_Plasma::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_Plasma::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_FX_Plasma::Value
	float ___Value_6;
};
struct CameraFilterPack_FX_Plasma_t137_StaticFields{
	// System.Single CameraFilterPack_FX_Plasma::ChangeValue
	float ___ChangeValue_7;
};
