﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct ObjectPool_1_t753;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t754;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t775;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m4965(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t753 *, UnityAction_1_t754 *, UnityAction_1_t754 *, const MethodInfo*))ObjectPool_1__ctor_m13999_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m19268(__this, method) (( int32_t (*) (ObjectPool_1_t753 *, const MethodInfo*))ObjectPool_1_get_countAll_m14001_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m19269(__this, ___value, method) (( void (*) (ObjectPool_1_t753 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m14003_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m19270(__this, method) (( int32_t (*) (ObjectPool_1_t753 *, const MethodInfo*))ObjectPool_1_get_countActive_m14005_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m19271(__this, method) (( int32_t (*) (ObjectPool_1_t753 *, const MethodInfo*))ObjectPool_1_get_countInactive_m14007_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m4966(__this, method) (( List_1_t775 * (*) (ObjectPool_1_t753 *, const MethodInfo*))ObjectPool_1_Get_m14009_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m4967(__this, ___element, method) (( void (*) (ObjectPool_1_t753 *, List_1_t775 *, const MethodInfo*))ObjectPool_1_Release_m14011_gshared)(__this, ___element, method)
