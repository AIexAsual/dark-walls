﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct  Getter_2_t2941  : public MulticastDelegate_t219
{
};
