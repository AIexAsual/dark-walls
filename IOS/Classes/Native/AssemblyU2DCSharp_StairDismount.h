﻿#pragma once
#include <stdint.h>
// UnityEngine.Rigidbody
struct Rigidbody_t325;
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// StairDismount
struct  StairDismount_t403  : public MonoBehaviour_t4
{
	// System.Single StairDismount::impactEndTime
	float ___impactEndTime_2;
	// UnityEngine.Rigidbody StairDismount::impactTarget
	Rigidbody_t325 * ___impactTarget_3;
	// UnityEngine.Vector3 StairDismount::impact
	Vector3_t215  ___impact_4;
	// System.Int32 StairDismount::score
	int32_t ___score_5;
	// UnityEngine.GameObject StairDismount::scoreTextTemplate
	GameObject_t256 * ___scoreTextTemplate_6;
};
