﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Runtime
struct Runtime_t1646;
// System.String
struct String_t;

// System.String Mono.Runtime::GetDisplayName()
extern "C" String_t* Runtime_GetDisplayName_m9865 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
