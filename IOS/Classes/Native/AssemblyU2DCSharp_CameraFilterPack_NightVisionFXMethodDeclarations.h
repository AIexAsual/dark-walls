﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_NightVisionFX
struct CameraFilterPack_NightVisionFX_t160;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_NightVisionFX::.ctor()
extern "C" void CameraFilterPack_NightVisionFX__ctor_m1029 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_NightVisionFX::get_material()
extern "C" Material_t2 * CameraFilterPack_NightVisionFX_get_material_m1030 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::Start()
extern "C" void CameraFilterPack_NightVisionFX_Start_m1031 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_NightVisionFX_OnRenderImage_m1032 (CameraFilterPack_NightVisionFX_t160 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::Update()
extern "C" void CameraFilterPack_NightVisionFX_Update_m1033 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_NightVisionFX::OnDisable()
extern "C" void CameraFilterPack_NightVisionFX_OnDisable_m1034 (CameraFilterPack_NightVisionFX_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
