﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>
struct Enumerator_t2431;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t578;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m16122_gshared (Enumerator_t2431 * __this, List_1_t578 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m16122(__this, ___l, method) (( void (*) (Enumerator_t2431 *, List_1_t578 *, const MethodInfo*))Enumerator__ctor_m16122_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16123_gshared (Enumerator_t2431 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16123(__this, method) (( Object_t * (*) (Enumerator_t2431 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16123_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::Dispose()
extern "C" void Enumerator_Dispose_m16124_gshared (Enumerator_t2431 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16124(__this, method) (( void (*) (Enumerator_t2431 *, const MethodInfo*))Enumerator_Dispose_m16124_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::VerifyState()
extern "C" void Enumerator_VerifyState_m16125_gshared (Enumerator_t2431 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16125(__this, method) (( void (*) (Enumerator_t2431 *, const MethodInfo*))Enumerator_VerifyState_m16125_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16126_gshared (Enumerator_t2431 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16126(__this, method) (( bool (*) (Enumerator_t2431 *, const MethodInfo*))Enumerator_MoveNext_m16126_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>::get_Current()
extern "C" Color_t6  Enumerator_get_Current_m16127_gshared (Enumerator_t2431 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16127(__this, method) (( Color_t6  (*) (Enumerator_t2431 *, const MethodInfo*))Enumerator_get_Current_m16127_gshared)(__this, method)
