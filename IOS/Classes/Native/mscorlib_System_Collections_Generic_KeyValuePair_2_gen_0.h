﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t257;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>
struct  KeyValuePair_2_t784 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::value
	PointerEventData_t257 * ___value_1;
};
