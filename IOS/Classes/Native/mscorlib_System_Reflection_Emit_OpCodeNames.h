﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t398;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Emit.OpCodeNames
struct  OpCodeNames_t1756  : public Object_t
{
};
struct OpCodeNames_t1756_StaticFields{
	// System.String[] System.Reflection.Emit.OpCodeNames::names
	StringU5BU5D_t398* ___names_0;
};
