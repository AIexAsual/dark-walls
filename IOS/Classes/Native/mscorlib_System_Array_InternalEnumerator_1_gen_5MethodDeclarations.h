﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Char>
struct InternalEnumerator_1_t2238;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13691_gshared (InternalEnumerator_1_t2238 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13691(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2238 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13691_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13692_gshared (InternalEnumerator_1_t2238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13692(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2238 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13692_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13693_gshared (InternalEnumerator_1_t2238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13693(__this, method) (( void (*) (InternalEnumerator_1_t2238 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13693_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13694_gshared (InternalEnumerator_1_t2238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13694(__this, method) (( bool (*) (InternalEnumerator_1_t2238 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13694_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m13695_gshared (InternalEnumerator_1_t2238 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13695(__this, method) (( uint16_t (*) (InternalEnumerator_1_t2238 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13695_gshared)(__this, method)
