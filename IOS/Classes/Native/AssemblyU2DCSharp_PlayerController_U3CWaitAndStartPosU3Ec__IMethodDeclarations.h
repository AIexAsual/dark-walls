﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController/<WaitAndStartPos>c__Iterator10
struct U3CWaitAndStartPosU3Ec__Iterator10_t390;
// System.Object
struct Object_t;

// System.Void PlayerController/<WaitAndStartPos>c__Iterator10::.ctor()
extern "C" void U3CWaitAndStartPosU3Ec__Iterator10__ctor_m2230 (U3CWaitAndStartPosU3Ec__Iterator10_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndStartPos>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndStartPosU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2231 (U3CWaitAndStartPosU3Ec__Iterator10_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndStartPos>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndStartPosU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m2232 (U3CWaitAndStartPosU3Ec__Iterator10_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController/<WaitAndStartPos>c__Iterator10::MoveNext()
extern "C" bool U3CWaitAndStartPosU3Ec__Iterator10_MoveNext_m2233 (U3CWaitAndStartPosU3Ec__Iterator10_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndStartPos>c__Iterator10::Dispose()
extern "C" void U3CWaitAndStartPosU3Ec__Iterator10_Dispose_m2234 (U3CWaitAndStartPosU3Ec__Iterator10_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndStartPos>c__Iterator10::Reset()
extern "C" void U3CWaitAndStartPosU3Ec__Iterator10_Reset_m2235 (U3CWaitAndStartPosU3Ec__Iterator10_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
