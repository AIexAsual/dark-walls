﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Half_Sphere
struct  CameraFilterPack_Distortion_Half_Sphere_t88  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Half_Sphere::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Half_Sphere::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Half_Sphere::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::SphereSize
	float ___SphereSize_6;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::SpherePositionX
	float ___SpherePositionX_7;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::SpherePositionY
	float ___SpherePositionY_8;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::Strength
	float ___Strength_9;
};
struct CameraFilterPack_Distortion_Half_Sphere_t88_StaticFields{
	// System.Single CameraFilterPack_Distortion_Half_Sphere::ChangeSphereSize
	float ___ChangeSphereSize_10;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::ChangeSpherePositionX
	float ___ChangeSpherePositionX_11;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::ChangeSpherePositionY
	float ___ChangeSpherePositionY_12;
	// System.Single CameraFilterPack_Distortion_Half_Sphere::ChangeStrength
	float ___ChangeStrength_13;
};
