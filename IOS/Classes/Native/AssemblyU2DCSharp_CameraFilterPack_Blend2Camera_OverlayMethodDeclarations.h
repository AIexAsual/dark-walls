﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Overlay
struct CameraFilterPack_Blend2Camera_Overlay_t37;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Overlay::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Overlay__ctor_m209 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Overlay::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Overlay_get_material_m210 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::Start()
extern "C" void CameraFilterPack_Blend2Camera_Overlay_Start_m211 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Overlay_OnRenderImage_m212 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Overlay_OnValidate_m213 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::Update()
extern "C" void CameraFilterPack_Blend2Camera_Overlay_Update_m214 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Overlay_OnEnable_m215 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Overlay::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Overlay_OnDisable_m216 (CameraFilterPack_Blend2Camera_Overlay_t37 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
