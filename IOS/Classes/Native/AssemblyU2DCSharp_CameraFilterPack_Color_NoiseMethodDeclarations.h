﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_Noise
struct CameraFilterPack_Color_Noise_t65;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_Noise::.ctor()
extern "C" void CameraFilterPack_Color_Noise__ctor_m406 (CameraFilterPack_Color_Noise_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Noise::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_Noise_get_material_m407 (CameraFilterPack_Color_Noise_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::Start()
extern "C" void CameraFilterPack_Color_Noise_Start_m408 (CameraFilterPack_Color_Noise_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_Noise_OnRenderImage_m409 (CameraFilterPack_Color_Noise_t65 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::OnValidate()
extern "C" void CameraFilterPack_Color_Noise_OnValidate_m410 (CameraFilterPack_Color_Noise_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::Update()
extern "C" void CameraFilterPack_Color_Noise_Update_m411 (CameraFilterPack_Color_Noise_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Noise::OnDisable()
extern "C" void CameraFilterPack_Color_Noise_OnDisable_m412 (CameraFilterPack_Color_Noise_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
