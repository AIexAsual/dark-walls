﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Film_ColorPerfection
struct  CameraFilterPack_Film_ColorPerfection_t143  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Film_ColorPerfection::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Film_ColorPerfection::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Film_ColorPerfection::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Film_ColorPerfection::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Film_ColorPerfection::Gamma
	float ___Gamma_6;
	// System.Single CameraFilterPack_Film_ColorPerfection::Value2
	float ___Value2_7;
	// System.Single CameraFilterPack_Film_ColorPerfection::Value3
	float ___Value3_8;
	// System.Single CameraFilterPack_Film_ColorPerfection::Value4
	float ___Value4_9;
};
struct CameraFilterPack_Film_ColorPerfection_t143_StaticFields{
	// System.Single CameraFilterPack_Film_ColorPerfection::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Film_ColorPerfection::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Film_ColorPerfection::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Film_ColorPerfection::ChangeValue4
	float ___ChangeValue4_13;
};
