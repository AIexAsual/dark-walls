﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t609;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.RaycasterManager
struct  RaycasterManager_t610  : public Object_t
{
};
struct RaycasterManager_t610_StaticFields{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t609 * ___s_Raycasters_0;
};
