﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Mesh
struct Mesh_t245;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t437;
// UnityEngine.Color[]
struct ColorU5BU5D_t449;
// System.Int32[]
struct Int32U5BU5D_t269;

// System.Void UnityEngine.Mesh::.ctor()
extern "C" void Mesh__ctor_m2913 (Mesh_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C" void Mesh_Internal_Create_m5065 (Object_t * __this /* static, unused */, Mesh_t245 * ___mono, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C" void Mesh_set_vertices_m2914 (Mesh_t245 * __this, Vector3U5BU5D_t317* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C" void Mesh_set_uv_m2915 (Mesh_t245 * __this, Vector2U5BU5D_t437* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
extern "C" void Mesh_set_colors_m2916 (Mesh_t245 * __this, ColorU5BU5D_t449* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Optimize()
extern "C" void Mesh_Optimize_m2918 (Mesh_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C" void Mesh_set_triangles_m2917 (Mesh_t245 * __this, Int32U5BU5D_t269* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::UploadMeshData(System.Boolean)
extern "C" void Mesh_UploadMeshData_m2919 (Mesh_t245 * __this, bool ___markNoLogerReadable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
