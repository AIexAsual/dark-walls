﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.MethodReturnDictionary
struct MethodReturnDictionary_t1882;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t2184;

// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern "C" void MethodReturnDictionary__ctor_m11449 (MethodReturnDictionary_t1882 * __this, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
extern "C" void MethodReturnDictionary__cctor_m11450 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
