﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyConfigurationAttribute
struct  AssemblyConfigurationAttribute_t1770  : public Attribute_t903
{
	// System.String System.Reflection.AssemblyConfigurationAttribute::name
	String_t* ___name_0;
};
