﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter
struct ThirdPersonCharacter_t413;
// System.Object
#include "mscorlib_System_Object.h"
// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19
struct  U3CBlendLookWeightU3Ec__Iterator19_t414  : public Object_t
{
	// System.Single UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::<t>__0
	float ___U3CtU3E__0_0;
	// System.Int32 UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::$PC
	int32_t ___U24PC_1;
	// System.Object UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::$current
	Object_t * ___U24current_2;
	// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/<BlendLookWeight>c__Iterator19::<>f__this
	ThirdPersonCharacter_t413 * ___U3CU3Ef__this_3;
};
