﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t675;

// System.Void UnityEngine.UI.InputField/SubmitEvent::.ctor()
extern "C" void SubmitEvent__ctor_m3862 (SubmitEvent_t675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
