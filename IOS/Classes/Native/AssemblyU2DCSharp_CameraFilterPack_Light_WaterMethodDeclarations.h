﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Light_Water
struct CameraFilterPack_Light_Water_t157;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Light_Water::.ctor()
extern "C" void CameraFilterPack_Light_Water__ctor_m1015 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Light_Water::get_material()
extern "C" Material_t2 * CameraFilterPack_Light_Water_get_material_m1016 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::Start()
extern "C" void CameraFilterPack_Light_Water_Start_m1017 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Light_Water_OnRenderImage_m1018 (CameraFilterPack_Light_Water_t157 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::OnValidate()
extern "C" void CameraFilterPack_Light_Water_OnValidate_m1019 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::Update()
extern "C" void CameraFilterPack_Light_Water_Update_m1020 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Light_Water::OnDisable()
extern "C" void CameraFilterPack_Light_Water_OnDisable_m1021 (CameraFilterPack_Light_Water_t157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
