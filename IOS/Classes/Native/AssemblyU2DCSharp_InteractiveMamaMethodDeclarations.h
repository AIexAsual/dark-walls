﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveMama
struct InteractiveMama_t330;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void InteractiveMama::.ctor()
extern "C" void InteractiveMama__ctor_m1953 (InteractiveMama_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::Start()
extern "C" void InteractiveMama_Start_m1954 (InteractiveMama_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::Update()
extern "C" void InteractiveMama_Update_m1955 (InteractiveMama_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::MamaHeadTurn(System.Single,System.Single,System.Single)
extern "C" void InteractiveMama_MamaHeadTurn_m1956 (InteractiveMama_t330 * __this, float ___time, float ___delay, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::EnableMama(System.Single,System.Boolean)
extern "C" void InteractiveMama_EnableMama_m1957 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InteractiveMama::WaitAndEnableMama(System.Single,System.Boolean)
extern "C" Object_t * InteractiveMama_WaitAndEnableMama_m1958 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::MamaAction(System.String)
extern "C" void InteractiveMama_MamaAction_m1959 (InteractiveMama_t330 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::MamaGoto(System.String)
extern "C" void InteractiveMama_MamaGoto_m1960 (InteractiveMama_t330 * __this, String_t* ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::MamaPos(System.Single,UnityEngine.Vector3)
extern "C" void InteractiveMama_MamaPos_m1961 (InteractiveMama_t330 * __this, float ___delay, Vector3_t215  ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InteractiveMama::WaitAndMamaPos(System.Single,UnityEngine.Vector3)
extern "C" Object_t * InteractiveMama_WaitAndMamaPos_m1962 (InteractiveMama_t330 * __this, float ___delay, Vector3_t215  ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::MamaScream()
extern "C" void InteractiveMama_MamaScream_m1963 (InteractiveMama_t330 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::MamaLookAt(UnityEngine.Transform)
extern "C" void InteractiveMama_MamaLookAt_m1964 (InteractiveMama_t330 * __this, Transform_t243 * ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveMama::HideMama(System.Single,System.Boolean)
extern "C" void InteractiveMama_HideMama_m1965 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator InteractiveMama::WaitAndHideMama(System.Single,System.Boolean)
extern "C" Object_t * InteractiveMama_WaitAndHideMama_m1966 (InteractiveMama_t330 * __this, float ___delay, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
