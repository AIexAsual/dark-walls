﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_AAA_SuperComputer
struct CameraFilterPack_AAA_SuperComputer_t3;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_AAA_SuperComputer::.ctor()
extern "C" void CameraFilterPack_AAA_SuperComputer__ctor_m0 (CameraFilterPack_AAA_SuperComputer_t3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::.cctor()
extern "C" void CameraFilterPack_AAA_SuperComputer__cctor_m1 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_AAA_SuperComputer::get_material()
extern "C" Material_t2 * CameraFilterPack_AAA_SuperComputer_get_material_m2 (CameraFilterPack_AAA_SuperComputer_t3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::Start()
extern "C" void CameraFilterPack_AAA_SuperComputer_Start_m3 (CameraFilterPack_AAA_SuperComputer_t3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_AAA_SuperComputer_OnRenderImage_m4 (CameraFilterPack_AAA_SuperComputer_t3 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::OnValidate()
extern "C" void CameraFilterPack_AAA_SuperComputer_OnValidate_m5 (CameraFilterPack_AAA_SuperComputer_t3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::Update()
extern "C" void CameraFilterPack_AAA_SuperComputer_Update_m6 (CameraFilterPack_AAA_SuperComputer_t3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_AAA_SuperComputer::OnDisable()
extern "C" void CameraFilterPack_AAA_SuperComputer_OnDisable_m7 (CameraFilterPack_AAA_SuperComputer_t3 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
