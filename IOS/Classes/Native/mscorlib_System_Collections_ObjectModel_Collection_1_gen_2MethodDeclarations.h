﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t2394;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Int32[]
struct Int32U5BU5D_t269;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t2986;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t561;

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m15586_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15586(__this, method) (( void (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1__ctor_m15586_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15587_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15587(__this, method) (( bool (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15587_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15588_gshared (Collection_1_t2394 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15588(__this, ___array, ___index, method) (( void (*) (Collection_1_t2394 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15588_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15589_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15589(__this, method) (( Object_t * (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15589_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15590_gshared (Collection_1_t2394 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15590(__this, ___value, method) (( int32_t (*) (Collection_1_t2394 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15590_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15591_gshared (Collection_1_t2394 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15591(__this, ___value, method) (( bool (*) (Collection_1_t2394 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15591_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15592_gshared (Collection_1_t2394 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15592(__this, ___value, method) (( int32_t (*) (Collection_1_t2394 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15592_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15593_gshared (Collection_1_t2394 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15593(__this, ___index, ___value, method) (( void (*) (Collection_1_t2394 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15593_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15594_gshared (Collection_1_t2394 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15594(__this, ___value, method) (( void (*) (Collection_1_t2394 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15594_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15595_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15595(__this, method) (( bool (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15595_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15596_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15596(__this, method) (( Object_t * (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15596_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15597_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15597(__this, method) (( bool (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15597_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15598_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15598(__this, method) (( bool (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15598_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15599_gshared (Collection_1_t2394 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15599(__this, ___index, method) (( Object_t * (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15599_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15600_gshared (Collection_1_t2394 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15600(__this, ___index, ___value, method) (( void (*) (Collection_1_t2394 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15600_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m15601_gshared (Collection_1_t2394 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m15601(__this, ___item, method) (( void (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_Add_m15601_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m15602_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15602(__this, method) (( void (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_Clear_m15602_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m15603_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15603(__this, method) (( void (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_ClearItems_m15603_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m15604_gshared (Collection_1_t2394 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m15604(__this, ___item, method) (( bool (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_Contains_m15604_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15605_gshared (Collection_1_t2394 * __this, Int32U5BU5D_t269* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15605(__this, ___array, ___index, method) (( void (*) (Collection_1_t2394 *, Int32U5BU5D_t269*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15605_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15606_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15606(__this, method) (( Object_t* (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_GetEnumerator_m15606_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15607_gshared (Collection_1_t2394 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15607(__this, ___item, method) (( int32_t (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m15607_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15608_gshared (Collection_1_t2394 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m15608(__this, ___index, ___item, method) (( void (*) (Collection_1_t2394 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m15608_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15609_gshared (Collection_1_t2394 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15609(__this, ___index, ___item, method) (( void (*) (Collection_1_t2394 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m15609_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m15610_gshared (Collection_1_t2394 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m15610(__this, ___item, method) (( bool (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_Remove_m15610_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15611_gshared (Collection_1_t2394 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15611(__this, ___index, method) (( void (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15611_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15612_gshared (Collection_1_t2394 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15612(__this, ___index, method) (( void (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15612_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15613_gshared (Collection_1_t2394 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15613(__this, method) (( int32_t (*) (Collection_1_t2394 *, const MethodInfo*))Collection_1_get_Count_m15613_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m15614_gshared (Collection_1_t2394 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15614(__this, ___index, method) (( int32_t (*) (Collection_1_t2394 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15614_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15615_gshared (Collection_1_t2394 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15615(__this, ___index, ___value, method) (( void (*) (Collection_1_t2394 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m15615_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15616_gshared (Collection_1_t2394 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15616(__this, ___index, ___item, method) (( void (*) (Collection_1_t2394 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m15616_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15617_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15617(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15617_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m15618_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15618(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15618_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15619_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15619(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15619_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15620_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15620(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15620_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15621_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15621(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15621_gshared)(__this /* static, unused */, ___list, method)
