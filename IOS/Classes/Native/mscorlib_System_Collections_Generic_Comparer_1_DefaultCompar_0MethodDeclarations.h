﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t2257;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m13900_gshared (DefaultComparer_t2257 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m13900(__this, method) (( void (*) (DefaultComparer_t2257 *, const MethodInfo*))DefaultComparer__ctor_m13900_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m13901_gshared (DefaultComparer_t2257 * __this, RaycastResult_t511  ___x, RaycastResult_t511  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m13901(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2257 *, RaycastResult_t511 , RaycastResult_t511 , const MethodInfo*))DefaultComparer_Compare_m13901_gshared)(__this, ___x, ___y, method)
