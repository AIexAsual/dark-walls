﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t2833;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2824;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21942_gshared (Enumerator_t2833 * __this, Dictionary_2_t2824 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21942(__this, ___host, method) (( void (*) (Enumerator_t2833 *, Dictionary_2_t2824 *, const MethodInfo*))Enumerator__ctor_m21942_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21943_gshared (Enumerator_t2833 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21943(__this, method) (( Object_t * (*) (Enumerator_t2833 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21943_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m21944_gshared (Enumerator_t2833 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21944(__this, method) (( void (*) (Enumerator_t2833 *, const MethodInfo*))Enumerator_Dispose_m21944_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21945_gshared (Enumerator_t2833 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21945(__this, method) (( bool (*) (Enumerator_t2833 *, const MethodInfo*))Enumerator_MoveNext_m21945_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t Enumerator_get_Current_m21946_gshared (Enumerator_t2833 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21946(__this, method) (( int32_t (*) (Enumerator_t2833 *, const MethodInfo*))Enumerator_get_Current_m21946_gshared)(__this, method)
