﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// MovingObjects
struct MovingObjects_t375;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// MovingObjects/<WaitAndSpawnHere>c__IteratorF
struct  U3CWaitAndSpawnHereU3Ec__IteratorF_t389  : public Object_t
{
	// System.Single MovingObjects/<WaitAndSpawnHere>c__IteratorF::delay
	float ___delay_0;
	// UnityEngine.Vector3 MovingObjects/<WaitAndSpawnHere>c__IteratorF::val
	Vector3_t215  ___val_1;
	// System.Int32 MovingObjects/<WaitAndSpawnHere>c__IteratorF::$PC
	int32_t ___U24PC_2;
	// System.Object MovingObjects/<WaitAndSpawnHere>c__IteratorF::$current
	Object_t * ___U24current_3;
	// System.Single MovingObjects/<WaitAndSpawnHere>c__IteratorF::<$>delay
	float ___U3CU24U3Edelay_4;
	// UnityEngine.Vector3 MovingObjects/<WaitAndSpawnHere>c__IteratorF::<$>val
	Vector3_t215  ___U3CU24U3Eval_5;
	// MovingObjects MovingObjects/<WaitAndSpawnHere>c__IteratorF::<>f__this
	MovingObjects_t375 * ___U3CU3Ef__this_6;
};
