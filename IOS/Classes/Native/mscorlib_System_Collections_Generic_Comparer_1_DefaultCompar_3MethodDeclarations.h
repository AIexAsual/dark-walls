﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Single>
struct DefaultComparer_t2409;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Single>::.ctor()
extern "C" void DefaultComparer__ctor_m15779_gshared (DefaultComparer_t2409 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15779(__this, method) (( void (*) (DefaultComparer_t2409 *, const MethodInfo*))DefaultComparer__ctor_m15779_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Single>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m15780_gshared (DefaultComparer_t2409 * __this, float ___x, float ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m15780(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2409 *, float, float, const MethodInfo*))DefaultComparer_Compare_m15780_gshared)(__this, ___x, ___y, method)
