﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t469;
// Mono.Security.X509.X509Extension
#include "Mono_Security_Mono_Security_X509_X509Extension.h"
// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
struct  AuthorityKeyIdentifierExtension_t1245  : public X509Extension_t1241
{
	// System.Byte[] Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::aki
	ByteU5BU5D_t469* ___aki_3;
};
