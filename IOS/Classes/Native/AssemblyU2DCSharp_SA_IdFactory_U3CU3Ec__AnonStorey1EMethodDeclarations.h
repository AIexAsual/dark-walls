﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SA_IdFactory/<>c__AnonStorey1E
struct U3CU3Ec__AnonStorey1E_t298;
// System.String
struct String_t;

// System.Void SA_IdFactory/<>c__AnonStorey1E::.ctor()
extern "C" void U3CU3Ec__AnonStorey1E__ctor_m1830 (U3CU3Ec__AnonStorey1E_t298 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char SA_IdFactory/<>c__AnonStorey1E::<>m__7(System.String)
extern "C" uint16_t U3CU3Ec__AnonStorey1E_U3CU3Em__7_m1831 (U3CU3Ec__AnonStorey1E_t298 * __this, String_t* ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
