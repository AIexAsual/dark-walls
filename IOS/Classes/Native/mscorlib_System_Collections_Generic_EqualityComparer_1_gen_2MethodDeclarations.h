﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>
struct EqualityComparer_1_t2341;
// System.Object
struct Object_t;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>::.ctor()
extern "C" void EqualityComparer_1__ctor_m14923_gshared (EqualityComparer_1_t2341 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m14923(__this, method) (( void (*) (EqualityComparer_1_t2341 *, const MethodInfo*))EqualityComparer_1__ctor_m14923_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>::.cctor()
extern "C" void EqualityComparer_1__cctor_m14924_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m14924(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m14924_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14925_gshared (EqualityComparer_1_t2341 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14925(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2341 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14925_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14926_gshared (EqualityComparer_1_t2341 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14926(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2341 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14926_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<iTweenEvent/TweenType>::get_Default()
extern "C" EqualityComparer_1_t2341 * EqualityComparer_1_get_Default_m14927_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m14927(__this /* static, unused */, method) (( EqualityComparer_1_t2341 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m14927_gshared)(__this /* static, unused */, method)
