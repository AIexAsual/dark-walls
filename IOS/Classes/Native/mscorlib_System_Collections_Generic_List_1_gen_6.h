﻿#pragma once
#include <stdint.h>
// UnityEngine.Color[]
struct ColorU5BU5D_t449;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.Color>
struct  List_1_t578  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Color>::_items
	ColorU5BU5D_t449* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Color>::_version
	int32_t ____version_3;
};
struct List_1_t578_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Color>::EmptyArray
	ColorU5BU5D_t449* ___EmptyArray_4;
};
