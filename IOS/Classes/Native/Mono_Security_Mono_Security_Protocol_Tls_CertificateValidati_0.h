﻿#pragma once
#include <stdint.h>
// Mono.Security.Protocol.Tls.ValidationResult
struct ValidationResult_t1295;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t1229;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct  CertificateValidationCallback2_t1297  : public MulticastDelegate_t219
{
};
