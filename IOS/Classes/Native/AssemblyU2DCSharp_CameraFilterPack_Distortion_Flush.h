﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Distortion_Flush
struct  CameraFilterPack_Distortion_Flush_t87  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Flush::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Flush::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Distortion_Flush::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Flush::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Distortion_Flush::Size
	float ___Size_6;
	// System.Single CameraFilterPack_Distortion_Flush::LightBackGround
	float ___LightBackGround_7;
	// System.Single CameraFilterPack_Distortion_Flush::Speed
	float ___Speed_8;
	// System.Single CameraFilterPack_Distortion_Flush::Size2
	float ___Size2_9;
};
struct CameraFilterPack_Distortion_Flush_t87_StaticFields{
	// System.Single CameraFilterPack_Distortion_Flush::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Distortion_Flush::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Distortion_Flush::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Distortion_Flush::ChangeValue4
	float ___ChangeValue4_13;
};
