﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t2564;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m18260_gshared (U3CStartU3Ec__Iterator0_t2564 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m18260(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2564 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m18260_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m18261_gshared (U3CStartU3Ec__Iterator0_t2564 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m18261(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2564 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m18261_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18262_gshared (U3CStartU3Ec__Iterator0_t2564 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18262(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2564 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18262_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m18263_gshared (U3CStartU3Ec__Iterator0_t2564 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m18263(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t2564 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m18263_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m18264_gshared (U3CStartU3Ec__Iterator0_t2564 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m18264(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2564 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m18264_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m18265_gshared (U3CStartU3Ec__Iterator0_t2564 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m18265(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2564 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m18265_gshared)(__this, method)
