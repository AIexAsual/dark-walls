﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Enumerator_t2346;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct Dictionary_2_t420;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"
#define Enumerator__ctor_m15068(__this, ___dictionary, method) (( void (*) (Enumerator_t2346 *, Dictionary_2_t420 *, const MethodInfo*))Enumerator__ctor_m14865_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15069(__this, method) (( Object_t * (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14866_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15070(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14867_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15071(__this, method) (( Object_t * (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14868_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15072(__this, method) (( Object_t * (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14869_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::MoveNext()
#define Enumerator_MoveNext_m15073(__this, method) (( bool (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_MoveNext_m14870_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Current()
#define Enumerator_get_Current_m15074(__this, method) (( KeyValuePair_2_t2343  (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_get_Current_m14871_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15075(__this, method) (( int32_t (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_get_CurrentKey_m14872_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15076(__this, method) (( Dictionary_2_t545 * (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_get_CurrentValue_m14873_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::VerifyState()
#define Enumerator_VerifyState_m15077(__this, method) (( void (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_VerifyState_m14874_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15078(__this, method) (( void (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_VerifyCurrent_m14875_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::Dispose()
#define Enumerator_Dispose_m15079(__this, method) (( void (*) (Enumerator_t2346 *, const MethodInfo*))Enumerator_Dispose_m14876_gshared)(__this, method)
