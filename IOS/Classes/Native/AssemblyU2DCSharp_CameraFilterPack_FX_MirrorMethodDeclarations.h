﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Mirror
struct CameraFilterPack_FX_Mirror_t136;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Mirror::.ctor()
extern "C" void CameraFilterPack_FX_Mirror__ctor_m880 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Mirror::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Mirror_get_material_m881 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::Start()
extern "C" void CameraFilterPack_FX_Mirror_Start_m882 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Mirror_OnRenderImage_m883 (CameraFilterPack_FX_Mirror_t136 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::Update()
extern "C" void CameraFilterPack_FX_Mirror_Update_m884 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Mirror::OnDisable()
extern "C" void CameraFilterPack_FX_Mirror_OnDisable_m885 (CameraFilterPack_FX_Mirror_t136 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
