﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2030;
// System.Byte[]
struct ByteU5BU5D_t469;

// System.Void System.Text.DecoderFallbackBuffer::.ctor()
extern "C" void DecoderFallbackBuffer__ctor_m12288 (DecoderFallbackBuffer_t2030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallbackBuffer::Reset()
extern "C" void DecoderFallbackBuffer_Reset_m12289 (DecoderFallbackBuffer_t2030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
