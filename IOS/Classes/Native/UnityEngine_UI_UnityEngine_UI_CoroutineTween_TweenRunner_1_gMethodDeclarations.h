﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t652;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m4679_gshared (TweenRunner_1_t652 * __this, const MethodInfo* method);
#define TweenRunner_1__ctor_m4679(__this, method) (( void (*) (TweenRunner_1_t652 *, const MethodInfo*))TweenRunner_1__ctor_m4679_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m18259_gshared (Object_t * __this /* static, unused */, ColorTween_t630  ___tweenInfo, const MethodInfo* method);
#define TweenRunner_1_Start_m18259(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, ColorTween_t630 , const MethodInfo*))TweenRunner_1_Start_m18259_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m4680_gshared (TweenRunner_1_t652 * __this, MonoBehaviour_t4 * ___coroutineContainer, const MethodInfo* method);
#define TweenRunner_1_Init_m4680(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t652 *, MonoBehaviour_t4 *, const MethodInfo*))TweenRunner_1_Init_m4680_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m4704_gshared (TweenRunner_1_t652 * __this, ColorTween_t630  ___info, const MethodInfo* method);
#define TweenRunner_1_StartTween_m4704(__this, ___info, method) (( void (*) (TweenRunner_1_t652 *, ColorTween_t630 , const MethodInfo*))TweenRunner_1_StartTween_m4704_gshared)(__this, ___info, method)
