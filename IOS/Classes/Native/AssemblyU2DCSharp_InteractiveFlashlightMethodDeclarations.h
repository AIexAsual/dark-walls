﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveFlashlight
struct InteractiveFlashlight_t329;

// System.Void InteractiveFlashlight::.ctor()
extern "C" void InteractiveFlashlight__ctor_m1929 (InteractiveFlashlight_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveFlashlight::Start()
extern "C" void InteractiveFlashlight_Start_m1930 (InteractiveFlashlight_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveFlashlight::SetGazedAt(System.Boolean)
extern "C" void InteractiveFlashlight_SetGazedAt_m1931 (InteractiveFlashlight_t329 * __this, bool ___gazeAt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveFlashlight::LightsOn()
extern "C" void InteractiveFlashlight_LightsOn_m1932 (InteractiveFlashlight_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveFlashlight::LightsOff()
extern "C" void InteractiveFlashlight_LightsOff_m1933 (InteractiveFlashlight_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveFlashlight::GetFlashlight()
extern "C" void InteractiveFlashlight_GetFlashlight_m1934 (InteractiveFlashlight_t329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
