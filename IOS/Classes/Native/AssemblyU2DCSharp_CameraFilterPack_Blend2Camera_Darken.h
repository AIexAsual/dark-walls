﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blend2Camera_Darken
struct  CameraFilterPack_Blend2Camera_Darken_t21  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_Blend2Camera_Darken::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_Darken::SCShader
	Shader_t1 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_Darken::Camera2
	Camera_t14 * ___Camera2_4;
	// System.Single CameraFilterPack_Blend2Camera_Darken::TimeX
	float ___TimeX_5;
	// UnityEngine.Vector4 CameraFilterPack_Blend2Camera_Darken::ScreenResolution
	Vector4_t5  ___ScreenResolution_6;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_Darken::SCMaterial
	Material_t2 * ___SCMaterial_7;
	// System.Single CameraFilterPack_Blend2Camera_Darken::SwitchCameraToCamera2
	float ___SwitchCameraToCamera2_8;
	// System.Single CameraFilterPack_Blend2Camera_Darken::BlendFX
	float ___BlendFX_9;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_Darken::Camera2tex
	RenderTexture_t15 * ___Camera2tex_12;
};
struct CameraFilterPack_Blend2Camera_Darken_t21_StaticFields{
	// System.Single CameraFilterPack_Blend2Camera_Darken::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Blend2Camera_Darken::ChangeValue2
	float ___ChangeValue2_11;
};
