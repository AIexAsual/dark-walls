﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// MamaActionTriggers
struct  MamaActionTriggers_t365  : public Object_t
{
};
struct MamaActionTriggers_t365_StaticFields{
	// System.String MamaActionTriggers::IDLE
	String_t* ___IDLE_0;
	// System.String MamaActionTriggers::WALK
	String_t* ___WALK_1;
	// System.String MamaActionTriggers::SCREAM
	String_t* ___SCREAM_2;
	// System.String MamaActionTriggers::CRAWL
	String_t* ___CRAWL_3;
	// System.String MamaActionTriggers::CRY
	String_t* ___CRY_4;
	// System.String MamaActionTriggers::ATTACK
	String_t* ___ATTACK_5;
	// System.String MamaActionTriggers::BITE
	String_t* ___BITE_6;
	// System.String MamaActionTriggers::APPROACH
	String_t* ___APPROACH_7;
};
