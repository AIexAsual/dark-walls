﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Distortion_Dream
struct  CameraFilterPack_Distortion_Dream_t83  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Distortion_Dream::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Distortion_Dream::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_Distortion_Dream::Distortion
	float ___Distortion_4;
	// UnityEngine.Material CameraFilterPack_Distortion_Dream::SCMaterial
	Material_t2 * ___SCMaterial_5;
};
struct CameraFilterPack_Distortion_Dream_t83_StaticFields{
	// System.Single CameraFilterPack_Distortion_Dream::ChangeDistortion
	float ___ChangeDistortion_6;
};
