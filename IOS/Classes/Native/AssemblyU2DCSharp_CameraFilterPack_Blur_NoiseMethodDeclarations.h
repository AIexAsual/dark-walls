﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Noise
struct CameraFilterPack_Blur_Noise_t55;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Noise::.ctor()
extern "C" void CameraFilterPack_Blur_Noise__ctor_m338 (CameraFilterPack_Blur_Noise_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Noise::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Noise_get_material_m339 (CameraFilterPack_Blur_Noise_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::Start()
extern "C" void CameraFilterPack_Blur_Noise_Start_m340 (CameraFilterPack_Blur_Noise_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Noise_OnRenderImage_m341 (CameraFilterPack_Blur_Noise_t55 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::OnValidate()
extern "C" void CameraFilterPack_Blur_Noise_OnValidate_m342 (CameraFilterPack_Blur_Noise_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::Update()
extern "C" void CameraFilterPack_Blur_Noise_Update_m343 (CameraFilterPack_Blur_Noise_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Noise::OnDisable()
extern "C" void CameraFilterPack_Blur_Noise_OnDisable_m344 (CameraFilterPack_Blur_Noise_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
