﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// StairDismount
struct StairDismount_t403;

// System.Void StairDismount::.ctor()
extern "C" void StairDismount__ctor_m2344 (StairDismount_t403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StairDismount::Start()
extern "C" void StairDismount_Start_m2345 (StairDismount_t403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StairDismount::Update()
extern "C" void StairDismount_Update_m2346 (StairDismount_t403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
