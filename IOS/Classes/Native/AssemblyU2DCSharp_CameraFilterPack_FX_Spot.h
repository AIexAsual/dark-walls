﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// CameraFilterPack_FX_Spot
struct  CameraFilterPack_FX_Spot_t140  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_Spot::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Spot::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_Spot::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_Spot::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// UnityEngine.Vector2 CameraFilterPack_FX_Spot::center
	Vector2_t7  ___center_6;
	// System.Single CameraFilterPack_FX_Spot::Radius
	float ___Radius_7;
};
struct CameraFilterPack_FX_Spot_t140_StaticFields{
	// UnityEngine.Vector2 CameraFilterPack_FX_Spot::Changecenter
	Vector2_t7  ___Changecenter_8;
	// System.Single CameraFilterPack_FX_Spot::ChangeRadius
	float ___ChangeRadius_9;
};
