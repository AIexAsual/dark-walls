﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// LightManager
struct LightManager_t370;
// System.String
struct String_t;

// System.Void LightManager::.ctor()
extern "C" void LightManager__ctor_m2142 (LightManager_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::.cctor()
extern "C" void LightManager__cctor_m2143 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LightManager LightManager::get_Instance()
extern "C" LightManager_t370 * LightManager_get_Instance_m2144 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::Start()
extern "C" void LightManager_Start_m2145 (LightManager_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::OffGameLight(System.String)
extern "C" void LightManager_OffGameLight_m2146 (LightManager_t370 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::LightFlicker(System.String)
extern "C" void LightManager_LightFlicker_m2147 (LightManager_t370 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::FlickerLights(System.String,System.Boolean,System.Single,System.Single)
extern "C" void LightManager_FlickerLights_m2148 (LightManager_t370 * __this, String_t* ___name, bool ___val, float ___time, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::FlashlightsOn()
extern "C" void LightManager_FlashlightsOn_m2149 (LightManager_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::FlashlightOff()
extern "C" void LightManager_FlashlightOff_m2150 (LightManager_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::GetFlashlight()
extern "C" void LightManager_GetFlashlight_m2151 (LightManager_t370 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::LightsOn(System.String)
extern "C" void LightManager_LightsOn_m2152 (LightManager_t370 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::LightsOff(System.String)
extern "C" void LightManager_LightsOff_m2153 (LightManager_t370 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::LightFade(System.String,System.Boolean,System.Single,System.Single,System.String)
extern "C" void LightManager_LightFade_m2154 (LightManager_t370 * __this, String_t* ___name, bool ___val, float ___delay, float ___speed, String_t* ___fadeState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LightManager::FogState(System.String,System.Single)
extern "C" void LightManager_FogState_m2155 (LightManager_t370 * __this, String_t* ___name, float ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
