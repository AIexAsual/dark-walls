﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController/<WaitAndTalk>c__Iterator14
struct U3CWaitAndTalkU3Ec__Iterator14_t394;
// System.Object
struct Object_t;

// System.Void PlayerController/<WaitAndTalk>c__Iterator14::.ctor()
extern "C" void U3CWaitAndTalkU3Ec__Iterator14__ctor_m2254 (U3CWaitAndTalkU3Ec__Iterator14_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndTalk>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndTalkU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2255 (U3CWaitAndTalkU3Ec__Iterator14_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndTalk>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndTalkU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m2256 (U3CWaitAndTalkU3Ec__Iterator14_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController/<WaitAndTalk>c__Iterator14::MoveNext()
extern "C" bool U3CWaitAndTalkU3Ec__Iterator14_MoveNext_m2257 (U3CWaitAndTalkU3Ec__Iterator14_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndTalk>c__Iterator14::Dispose()
extern "C" void U3CWaitAndTalkU3Ec__Iterator14_Dispose_m2258 (U3CWaitAndTalkU3Ec__Iterator14_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndTalk>c__Iterator14::Reset()
extern "C" void U3CWaitAndTalkU3Ec__Iterator14_Reset_m2259 (U3CWaitAndTalkU3Ec__Iterator14_t394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
