﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t2553;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct IEnumerator_1_t3106;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t687;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_2.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m18150(__this, method) (( void (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1__ctor_m14012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m18151(__this, method) (( bool (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m14013_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m18152(__this, method) (( Object_t * (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m14014_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m18153(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2553 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m14015_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18154(__this, method) (( Object_t* (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14016_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m18155(__this, method) (( Object_t * (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m14017_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m18156(__this, method) (( List_1_t687 * (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_Peek_m14018_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m18157(__this, method) (( List_1_t687 * (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_Pop_m14019_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m18158(__this, ___t, method) (( void (*) (Stack_1_t2553 *, List_1_t687 *, const MethodInfo*))Stack_1_Push_m14020_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m18159(__this, method) (( int32_t (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_get_Count_m14021_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m18160(__this, method) (( Enumerator_t3107  (*) (Stack_1_t2553 *, const MethodInfo*))Stack_1_GetEnumerator_m14022_gshared)(__this, method)
