﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t1334;
struct DSAParameters_t1334_marshaled;

void DSAParameters_t1334_marshal(const DSAParameters_t1334& unmarshaled, DSAParameters_t1334_marshaled& marshaled);
void DSAParameters_t1334_marshal_back(const DSAParameters_t1334_marshaled& marshaled, DSAParameters_t1334& unmarshaled);
void DSAParameters_t1334_marshal_cleanup(DSAParameters_t1334_marshaled& marshaled);
