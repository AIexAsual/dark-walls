﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_OldFilm_Cutting1
struct CameraFilterPack_OldFilm_Cutting1_t167;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_OldFilm_Cutting1::.ctor()
extern "C" void CameraFilterPack_OldFilm_Cutting1__ctor_m1079 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_OldFilm_Cutting1::get_material()
extern "C" Material_t2 * CameraFilterPack_OldFilm_Cutting1_get_material_m1080 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::Start()
extern "C" void CameraFilterPack_OldFilm_Cutting1_Start_m1081 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_OldFilm_Cutting1_OnRenderImage_m1082 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::Update()
extern "C" void CameraFilterPack_OldFilm_Cutting1_Update_m1083 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_OldFilm_Cutting1::OnDisable()
extern "C" void CameraFilterPack_OldFilm_Cutting1_OnDisable_m1084 (CameraFilterPack_OldFilm_Cutting1_t167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
