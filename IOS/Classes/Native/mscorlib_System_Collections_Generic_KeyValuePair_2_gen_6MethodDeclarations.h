﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>
struct KeyValuePair_2_t2343;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t545;
// System.String
struct String_t;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15034(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2343 *, int32_t, Dictionary_2_t545 *, const MethodInfo*))KeyValuePair_2__ctor_m14835_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Key()
#define KeyValuePair_2_get_Key_m15035(__this, method) (( int32_t (*) (KeyValuePair_2_t2343 *, const MethodInfo*))KeyValuePair_2_get_Key_m14836_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15036(__this, ___value, method) (( void (*) (KeyValuePair_2_t2343 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m14837_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::get_Value()
#define KeyValuePair_2_get_Value_m15037(__this, method) (( Dictionary_2_t545 * (*) (KeyValuePair_2_t2343 *, const MethodInfo*))KeyValuePair_2_get_Value_m14838_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15038(__this, ___value, method) (( void (*) (KeyValuePair_2_t2343 *, Dictionary_2_t545 *, const MethodInfo*))KeyValuePair_2_set_Value_m14839_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Collections.Generic.Dictionary`2<System.String,System.Type>>::ToString()
#define KeyValuePair_2_ToString_m15039(__this, method) (( String_t* (*) (KeyValuePair_2_t2343 *, const MethodInfo*))KeyValuePair_2_ToString_m14840_gshared)(__this, method)
