﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Noise
struct CameraFilterPack_TV_Noise_t186;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Noise::.ctor()
extern "C" void CameraFilterPack_TV_Noise__ctor_m1202 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Noise::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Noise_get_material_m1203 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::Start()
extern "C" void CameraFilterPack_TV_Noise_Start_m1204 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Noise_OnRenderImage_m1205 (CameraFilterPack_TV_Noise_t186 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::OnValidate()
extern "C" void CameraFilterPack_TV_Noise_OnValidate_m1206 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::Update()
extern "C" void CameraFilterPack_TV_Noise_Update_m1207 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Noise::OnDisable()
extern "C" void CameraFilterPack_TV_Noise_OnDisable_m1208 (CameraFilterPack_TV_Noise_t186 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
