﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// InteractiveObject
struct  InteractiveObject_t328  : public MonoBehaviour_t4
{
	// UnityEngine.Material InteractiveObject::objectMaterial
	Material_t2 * ___objectMaterial_2;
	// System.Boolean InteractiveObject::lookedAt
	bool ___lookedAt_3;
};
