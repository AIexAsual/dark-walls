﻿#pragma once
#include <stdint.h>
// System.String[]
struct StringU5BU5D_t398;
// UnityEngine.Transform
struct Transform_t243;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vector3OrTransform
struct  Vector3OrTransform_t422  : public Object_t
{
	// System.Int32 Vector3OrTransform::selected
	int32_t ___selected_3;
	// UnityEngine.Vector3 Vector3OrTransform::vector
	Vector3_t215  ___vector_4;
	// UnityEngine.Transform Vector3OrTransform::transform
	Transform_t243 * ___transform_5;
};
struct Vector3OrTransform_t422_StaticFields{
	// System.String[] Vector3OrTransform::choices
	StringU5BU5D_t398* ___choices_0;
	// System.Int32 Vector3OrTransform::vector3Selected
	int32_t ___vector3Selected_1;
	// System.Int32 Vector3OrTransform::transformSelected
	int32_t ___transformSelected_2;
};
