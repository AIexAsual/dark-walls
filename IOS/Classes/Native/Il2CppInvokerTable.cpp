﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
void* RuntimeInvoker_Void_t1548 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.Boolean
#include "mscorlib_System_Boolean.h"
void* RuntimeInvoker_Boolean_t536 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.SByte
#include "mscorlib_System_SByte.h"
void* RuntimeInvoker_Void_t1548_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
void* RuntimeInvoker_Void_t1548_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
// Cardboard/DistortionCorrectionMethod
#include "AssemblyU2DCSharp_Cardboard_DistortionCorrectionMethod.h"
void* RuntimeInvoker_DistortionCorrectionMethod_t228 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
void* RuntimeInvoker_Void_t1548_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

struct Object_t;
// Cardboard/BackButtonModes
#include "AssemblyU2DCSharp_Cardboard_BackButtonModes.h"
void* RuntimeInvoker_BackButtonModes_t229 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
void* RuntimeInvoker_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

struct Object_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
void* RuntimeInvoker_Matrix4x4_t242_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
void* RuntimeInvoker_Rect_t225_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
void* RuntimeInvoker_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
void* RuntimeInvoker_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
void* RuntimeInvoker_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t225_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Matrix4x4U26_t3505 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t242 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t242 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Rect_t225_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, Rect_t225  p1, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
void* RuntimeInvoker_Ray_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t465  (*Func)(void* obj, const MethodInfo* method);
	Ray_t465  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Vector3U5BU5D_t317;
#include "UnityEngine_ArrayTypes.h"
struct Vector2U5BU5D_t437;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_SByte_t1135_Vector3U5BU5DU26_t3506_Vector2U5BU5DU26_t3507 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Vector3U5BU5D_t317** p4, Vector2U5BU5D_t437** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Vector3U5BU5D_t317**)args[3], (Vector2U5BU5D_t437**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t225_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// CardboardProfile/Distortion
#include "AssemblyU2DCSharp_CardboardProfile_Distortion.h"
void* RuntimeInvoker_Distortion_t251_Single_t531_Single_t531_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Distortion_t251  (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	Distortion_t251  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Distortion_t251_Distortion_t251_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Distortion_t251  (*Func)(void* obj, Distortion_t251  p1, float p2, int32_t p3, const MethodInfo* method);
	Distortion_t251  ret = ((Func)method->method)(obj, *((Distortion_t251 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t7_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, Quaternion_t261  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Quaternion_t261 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Matrix4x4_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t242  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Matrix4x4_t242 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t261  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Matrix4x4U26_t3505_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Matrix4x4_t242 * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Matrix4x4_t242 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
// BaseVRDevice/DisplayMetrics
#include "AssemblyU2DCSharp_BaseVRDevice_DisplayMetrics.h"
void* RuntimeInvoker_DisplayMetrics_t271 (const MethodInfo* method, void* obj, void** args)
{
	typedef DisplayMetrics_t271  (*Func)(void* obj, const MethodInfo* method);
	DisplayMetrics_t271  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Single_t531_Single_t531_Single_t531_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Char
#include "mscorlib_System_Char.h"
struct Object_t;
void* RuntimeInvoker_Char_t526_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Vector3_t215  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t215 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Vector3_t215  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t215 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t1135_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, float p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((float*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_Single_t531_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, float p2, float p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Single_t531_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Single_t531_Single_t531_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215  p2, float p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t215 *)args[1]), *((float*)args[2]), *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, float p4, float p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t215 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Single_t531_Single_t531_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, float p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Vector3_t215_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, Vector3_t215  p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t215 *)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Vector3_t215_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Vector3_t215  p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t215 *)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_SByte_t1135_SByte_t1135_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, int8_t p2, int8_t p3, Vector3_t215  p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((Vector3_t215 *)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
void* RuntimeInvoker_Void_t1548_Object_t_Color_t6_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t6  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t6 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Rect_t225_Rect_t225_Rect_t225_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, Rect_t225  p1, Rect_t225  p2, float p3, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), *((Rect_t225 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Vector3_t215_Vector3_t215_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, float p3, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t7_Vector2_t7_Vector2_t7_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, float p3, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t6  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t215_Object_t_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t6 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Color_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t6  p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color_t6 *)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Single_t531_Single_t531_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
void* RuntimeInvoker_Int32_t478_RaycastResult_t511_RaycastResult_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t511  p1, RaycastResult_t511  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), *((RaycastResult_t511 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventSystems.MoveDirection
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
void* RuntimeInvoker_MoveDirection_t608 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastResult_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t511  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t511  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_RaycastResult_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t511  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
void* RuntimeInvoker_InputButton_t613 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RaycastResult_t511_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t511  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t511  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t608_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MoveDirection_t608_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

struct Object_t;
struct PointerEventData_t257;
// UnityEngine.EventSystems.PointerEventData
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
void* RuntimeInvoker_Boolean_t536_Int32_t478_PointerEventDataU26_t3508_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t257 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t257 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
void* RuntimeInvoker_Object_t_Touch_t768_BooleanU26_t3509_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t768  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t768 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
// UnityEngine.EventSystems.PointerEventData/FramePressState
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
void* RuntimeInvoker_FramePressState_t614_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector2_t7_Vector2_t7_Single_t531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventSystems.StandaloneInputModule/InputMode
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
void* RuntimeInvoker_InputMode_t621 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
void* RuntimeInvoker_LayerMask_t241 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t241  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t241  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_LayerMask_t241 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t241  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t241 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
void* RuntimeInvoker_Int32_t478_RaycastHit_t482_RaycastHit_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t482  p1, RaycastHit_t482  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t482 *)args[0]), *((RaycastHit_t482 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
void* RuntimeInvoker_ColorTweenMode_t627 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
void* RuntimeInvoker_ColorBlock_t642 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t642  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t642  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
void* RuntimeInvoker_FontStyle_t889 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
void* RuntimeInvoker_TextAnchor_t819 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
void* RuntimeInvoker_HorizontalWrapMode_t953 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
void* RuntimeInvoker_VerticalWrapMode_t954 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector2_t7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t7  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t7_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Color_t6_Single_t531_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Color_t6_Single_t531_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color_t6_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.GraphicRaycaster/BlockingObjects
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
void* RuntimeInvoker_BlockingObjects_t655 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Vector2_t7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t7  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t7 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
void* RuntimeInvoker_Type_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
void* RuntimeInvoker_FillMethod_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
void* RuntimeInvoker_Vector4_t5_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"
void* RuntimeInvoker_Void_t1548_Object_t_UIVertex_t685_Vector2_t7_Vector2_t7_Vector2_t7_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UIVertex_t685  p2, Vector2_t7  p3, Vector2_t7  p4, Vector2_t7  p5, Vector2_t7  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t685 *)args[1]), *((Vector2_t7 *)args[2]), *((Vector2_t7 *)args[3]), *((Vector2_t7 *)args[4]), *((Vector2_t7 *)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5_Vector4_t5_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, Vector4_t5  p1, Rect_t225  p2, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), *((Rect_t225 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Single_t531_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Single_t531_Single_t531_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector2_t7_Vector2_t7_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Vector2_t7  p1, Rect_t225  p2, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Rect_t225 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
void* RuntimeInvoker_ContentType_t671 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
void* RuntimeInvoker_LineType_t674 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
void* RuntimeInvoker_InputType_t672 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
void* RuntimeInvoker_TouchScreenKeyboardType_t805 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
void* RuntimeInvoker_CharacterValidation_t673 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t526 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Int16
#include "mscorlib_System_Int16.h"
void* RuntimeInvoker_Void_t1548_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Vector2_t7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t7  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
struct Object_t;
void* RuntimeInvoker_EditState_t678_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t7  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t7 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t526_Object_t_Int32_t478_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Int16_t1136_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
void* RuntimeInvoker_Mode_t689 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
void* RuntimeInvoker_Navigation_t690 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t690  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t690  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
void* RuntimeInvoker_Direction_t692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
void* RuntimeInvoker_Axis_t695 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ScrollRect/MovementType
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
void* RuntimeInvoker_MovementType_t699 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
void* RuntimeInvoker_Bounds_t703 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t703  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t703  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Navigation_t690 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t690  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t690 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
void* RuntimeInvoker_Transition_t704 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ColorBlock_t642 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t642  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t642 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
void* RuntimeInvoker_SpriteState_t708 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t708  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t708  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SpriteState_t708 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t708  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t708 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
void* RuntimeInvoker_SelectionState_t705 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t215_Object_t_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Object_t * p1, Vector2_t7  p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t7 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Color_t6_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_ColorU26_t3511_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t6 * p1, Color_t6  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t6 *)args[0], *((Color_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
void* RuntimeInvoker_Direction_t710 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
void* RuntimeInvoker_Axis_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
void* RuntimeInvoker_TextGenerationSettings_t773_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t773  (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	TextGenerationSettings_t773  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t7_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
void* RuntimeInvoker_AspectMode_t725 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
void* RuntimeInvoker_ScaleMode_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
void* RuntimeInvoker_ScreenMatchMode_t728 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
void* RuntimeInvoker_Unit_t729 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
void* RuntimeInvoker_FitMode_t731 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
void* RuntimeInvoker_Corner_t733 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
void* RuntimeInvoker_Axis_t734 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
void* RuntimeInvoker_Constraint_t735 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Int32_t478_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
void* RuntimeInvoker_Boolean_t536_LayoutRebuilder_t745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t745  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t745 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_Object_t_Object_t_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ILayoutElement_t776;
void* RuntimeInvoker_Single_t531_Object_t_Object_t_Single_t531_ILayoutElementU26_t3512 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
void* RuntimeInvoker_Void_t1548_Object_t_Color32_t778_Int32_t478_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t778  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t778 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_LayerMask_t241 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t241  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t241 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LayerMask_t241_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t241  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t241  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.Double
#include "mscorlib_System_Double.h"
void* RuntimeInvoker_Void_t1548_Object_t_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Int64
#include "mscorlib_System_Int64.h"
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
void* RuntimeInvoker_Void_t1548_GcAchievementDescriptionData_t1019_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t1019  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t1019 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
void* RuntimeInvoker_Void_t1548_GcUserProfileData_t1018_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t1018  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t1018 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Double_t553_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct UserProfileU5BU5D_t845;
struct Object_t;
void* RuntimeInvoker_Void_t1548_UserProfileU5BU5DU26_t3513_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t845** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t845**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct UserProfileU5BU5D_t845;
void* RuntimeInvoker_Void_t1548_UserProfileU5BU5DU26_t3513_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t845** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t845**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
void* RuntimeInvoker_Void_t1548_GcScoreData_t1021 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t1021  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t1021 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
// UnityEngine.ColorSpace
#include "UnityEngine_UnityEngine_ColorSpace.h"
void* RuntimeInvoker_ColorSpace_t1025 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.BoneWeight
#include "UnityEngine_UnityEngine_BoneWeight.h"
void* RuntimeInvoker_Boolean_t536_BoneWeight_t854_BoneWeight_t854 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t854  p1, BoneWeight_t854  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t854 *)args[0]), *((BoneWeight_t854 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215  p2, Quaternion_t261  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t215 *)args[1]), *((Quaternion_t261 *)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Quaternion_t261_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215  p2, Quaternion_t261  p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t215 *)args[1]), *((Quaternion_t261 *)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514_QuaternionU26_t3515_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, Quaternion_t261 * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], (Quaternion_t261 *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Rect_t225_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, Object_t * p2, Rect_t225  p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], *((Rect_t225 *)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
// UnityEngine.InternalDrawTextureArguments
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
void* RuntimeInvoker_Void_t1548_InternalDrawTextureArgumentsU26_t3516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, InternalDrawTextureArguments_t856 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (InternalDrawTextureArguments_t856 *)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t6  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t6 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Color_t6_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t6  p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((Color_t6 *)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_ColorU26_t3511_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, Color_t6 * p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Color_t6 *)args[2], *((float*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color_t6_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_RectU26_t3517_Int32_t478_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t225 * p2, int32_t p3, int32_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t225 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ColorU26_t3511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t6 *)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Color_t6_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
void* RuntimeInvoker_Void_t1548_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t871  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t871 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_RectU26_t3517_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t225 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Int32_t478_SByte_t1135_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225  p1, Object_t * p2, int32_t p3, int8_t p4, float p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), *((float*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Rect_t225_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t225  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Rect_t225_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t225  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Rect_t225_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t225  p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_RectU26_t3517_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t225 * p1, Object_t * p2, IntPtr_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Rect_t225 *)args[0], (Object_t *)args[1], *((IntPtr_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_Int32_t478_Single_t531_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t225  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t225 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_RectU26_t3517 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t225 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t225 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t225_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t225_Object_t_RectU26_t3517 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, Object_t * p1, Rect_t225 * p2, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Rect_t225 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.ImagePosition
#include "UnityEngine_UnityEngine_ImagePosition.h"
void* RuntimeInvoker_ImagePosition_t890 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t7_Rect_t225_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Rect_t225  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Rect_t225_Object_t_Int32_t478_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t225  p2, Object_t * p3, int32_t p4, Vector2_t7 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t225 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t7 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_RectU26_t3517_Object_t_Int32_t478_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t225 * p2, Object_t * p3, int32_t p4, Vector2_t7 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t225 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t7 *)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Object_t_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t7 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t7 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_Object_t_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_IntPtr_t_Object_t_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SingleU26_t3519_SingleU26_t3519 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float* p2, float* p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (float*)args[1], (float*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Object_t_SingleU26_t3519_SingleU26_t3519 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float* p3, float* p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (float*)args[2], (float*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t3520_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t891 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t891 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

struct Object_t;
// UnityEngine.EventType
#include "UnityEngine_UnityEngine_EventType.h"
void* RuntimeInvoker_EventType_t893 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t7 *)args[0], method);
	return NULL;
}

struct Object_t;
// UnityEngine.EventModifiers
#include "UnityEngine_UnityEngine_EventModifiers.h"
void* RuntimeInvoker_EventModifiers_t894 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
void* RuntimeInvoker_KeyCode_t892 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3U26_t3514_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215 * p1, Vector3_t215 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t215 *)args[0], (Vector3_t215 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3U26_t3514_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215 * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t215 *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Vector2_t7_Vector2_t7_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Vector2_t7_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector2_t7_Vector2_t7_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Vector2_t7  p1, float p2, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector2_t7_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Vector3_t215_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Vector3_t215_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Vector3_t215_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Vector3_t215  p1, float p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Single_t531_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, float p1, Vector3_t215  p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector3_t215_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t6_Color_t6_Color_t6_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, Color_t6  p1, Color_t6  p2, float p3, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), *((Color_t6 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t6_Color_t6_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, Color_t6  p1, float p2, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, Color_t6  p1, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t6_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, Vector4_t5  p1, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Color32_t778_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t778  (*Func)(void* obj, Color_t6  p1, const MethodInfo* method);
	Color32_t778  ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t6_Color32_t778 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, Color32_t778  p1, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, *((Color32_t778 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Quaternion_t261_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t261  p1, Quaternion_t261  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), *((Quaternion_t261 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Vector3_t215_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Vector3U26_t3514_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Vector3_t215 * p1, Vector3_t215 * p2, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, (Vector3_t215 *)args[0], (Vector3_t215 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Quaternion_t261_Quaternion_t261_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Quaternion_t261  p1, Quaternion_t261  p2, float p3, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), *((Quaternion_t261 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_QuaternionU26_t3515_QuaternionU26_t3515_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Quaternion_t261 * p1, Quaternion_t261 * p2, float p3, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, (Quaternion_t261 *)args[0], (Quaternion_t261 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Quaternion_t261  p1, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_QuaternionU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Quaternion_t261 * p1, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, (Quaternion_t261 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Quaternion_t261  p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_QuaternionU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Quaternion_t261 * p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, (Quaternion_t261 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Vector3_t215 * p1, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, (Vector3_t215 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Quaternion_t261_Quaternion_t261_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t261  (*Func)(void* obj, Quaternion_t261  p1, Quaternion_t261  p2, const MethodInfo* method);
	Quaternion_t261  ret = ((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), *((Quaternion_t261 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Quaternion_t261_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Quaternion_t261  p1, Vector3_t215  p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Quaternion_t261_Quaternion_t261 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t261  p1, Quaternion_t261  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t261 *)args[0]), *((Quaternion_t261 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Rect_t225_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t225  p1, Rect_t225  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), *((Rect_t225 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Matrix4x4_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, Matrix4x4_t242  p1, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((Matrix4x4_t242 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Matrix4x4U26_t3505 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, Matrix4x4_t242 * p1, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, (Matrix4x4_t242 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Matrix4x4_t242_Matrix4x4U26_t3505 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t242  p1, Matrix4x4_t242 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t242 *)args[0]), (Matrix4x4_t242 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Matrix4x4U26_t3505_Matrix4x4U26_t3505 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t242 * p1, Matrix4x4_t242 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t242 *)args[0], (Matrix4x4_t242 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t5  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t5 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Quaternion_t261_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, Quaternion_t261  p2, Vector3_t215  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Quaternion_t261 *)args[1]), *((Vector3_t215 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Vector3_t215_Quaternion_t261_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, Vector3_t215  p1, Quaternion_t261  p2, Vector3_t215  p3, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Quaternion_t261 *)args[1]), *((Vector3_t215 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Vector3U26_t3514_QuaternionU26_t3515_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, Vector3_t215 * p1, Quaternion_t261 * p2, Vector3_t215 * p3, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, (Vector3_t215 *)args[0], (Quaternion_t261 *)args[1], (Vector3_t215 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Single_t531_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Matrix4x4_t242_Matrix4x4_t242_Matrix4x4_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t242  (*Func)(void* obj, Matrix4x4_t242  p1, Matrix4x4_t242  p2, const MethodInfo* method);
	Matrix4x4_t242  ret = ((Func)method->method)(obj, *((Matrix4x4_t242 *)args[0]), *((Matrix4x4_t242 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5_Matrix4x4_t242_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, Matrix4x4_t242  p1, Vector4_t5  p2, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((Matrix4x4_t242 *)args[0]), *((Vector4_t5 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Matrix4x4_t242_Matrix4x4_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t242  p1, Matrix4x4_t242  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t242 *)args[0]), *((Matrix4x4_t242 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Bounds_t703 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t703  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t703 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Bounds_t703 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t703  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t703 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Bounds_t703_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t703  p1, Vector3_t215  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t703 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_BoundsU26_t3521_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t703 * p1, Vector3_t215 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t703 *)args[0], (Vector3_t215 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Bounds_t703_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t703  p1, Vector3_t215  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t703 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_BoundsU26_t3521_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t703 * p1, Vector3_t215 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t703 *)args[0], (Vector3_t215 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_RayU26_t3522_BoundsU26_t3521_SingleU26_t3519 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t465 * p1, Bounds_t703 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t465 *)args[0], (Bounds_t703 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Ray_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t465  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Ray_t465_SingleU26_t3519 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t465  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_BoundsU26_t3521_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Bounds_t703 * p1, Vector3_t215 * p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, (Bounds_t703 *)args[0], (Vector3_t215 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Bounds_t703_Bounds_t703 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t703  p1, Bounds_t703  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t703 *)args[0]), *((Bounds_t703 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Vector4_t5_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t5  p1, Vector4_t5  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), *((Vector4_t5 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t5  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5_Vector4_t5_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, Vector4_t5  p1, Vector4_t5  p2, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), *((Vector4_t5 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5_Vector4_t5_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, Vector4_t5  p1, float p2, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector4_t5_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t5  p1, Vector4_t5  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), *((Vector4_t5 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector3_t215_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Vector4_t5  p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, *((Vector4_t5 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Vector4_t5_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Single_t531_Single_t531_SingleU26_t3519_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Single_t531_Single_t531_SingleU26_t3519_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_RectU26_t3517 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t225 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t225 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t6  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color_t6 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_ColorU26_t3511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color_t6 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Color_t6 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector4_t5 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector4_t5  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector4_t5 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Matrix4x4_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Matrix4x4_t242  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Matrix4x4_t242 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Matrix4x4_t242 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Matrix4x4_t242  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Matrix4x4_t242 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Matrix4x4U26_t3505 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Matrix4x4_t242 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Matrix4x4_t242 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_ColorU26_t3511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Color_t6 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Color_t6 *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Color_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Color_t6_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t6  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color_t6  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t7 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Vector2_t7 *)args[2], method);
	return NULL;
}

struct Object_t;
// UnityEngine.Rendering.SphericalHarmonicsL2
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
void* RuntimeInvoker_Void_t1548_SphericalHarmonicsL2U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t905 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t905 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Color_t6_SphericalHarmonicsL2U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6  p1, SphericalHarmonicsL2_t905 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t6 *)args[0]), (SphericalHarmonicsL2_t905 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ColorU26_t3511_SphericalHarmonicsL2U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t6 * p1, SphericalHarmonicsL2_t905 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t6 *)args[0], (SphericalHarmonicsL2_t905 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Color_t6_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, Color_t6  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Color_t6 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Color_t6_SphericalHarmonicsL2U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, Color_t6  p2, SphericalHarmonicsL2_t905 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Color_t6 *)args[1]), (SphericalHarmonicsL2_t905 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3U26_t3514_ColorU26_t3511_SphericalHarmonicsL2U26_t3523 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215 * p1, Color_t6 * p2, SphericalHarmonicsL2_t905 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t215 *)args[0], (Color_t6 *)args[1], (SphericalHarmonicsL2_t905 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t905  (*Func)(void* obj, SphericalHarmonicsL2_t905  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t905  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t905 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t905_Single_t531_SphericalHarmonicsL2_t905 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t905  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t905  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t905  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t905 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t905  (*Func)(void* obj, SphericalHarmonicsL2_t905  p1, SphericalHarmonicsL2_t905  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t905  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t905 *)args[0]), *((SphericalHarmonicsL2_t905 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t905  p1, SphericalHarmonicsL2_t905  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t905 *)args[0]), *((SphericalHarmonicsL2_t905 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector4U26_t3524 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t5 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4_t5 *)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector4_t5_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t5  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t5  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t7 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t7 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
void* RuntimeInvoker_RuntimePlatform_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
void* RuntimeInvoker_CameraClearFlags_t1024 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t215_Object_t_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Ray_t465_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t465  (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	Ray_t465  ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t465_Object_t_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t465  (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, const MethodInfo* method);
	Ray_t465  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t465_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t465  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_RayU26_t3522_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t465 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t465 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
void* RuntimeInvoker_RenderBuffer_t1023 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t1023  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t1023  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_RenderBufferU26_t3525_RenderBufferU26_t3525 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t1023 * p2, RenderBuffer_t1023 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t1023 *)args[1], (RenderBuffer_t1023 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
void* RuntimeInvoker_TouchPhase_t919 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t215 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Touch_t768_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t768  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t768  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_QuaternionU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t261 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t261 *)args[0], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector3_t215_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t215  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t215 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, Vector3_t215 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], (Vector3_t215 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector3_t215_Vector3_t215_RaycastHitU26_t3526_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, RaycastHit_t482 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), (RaycastHit_t482 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector3U26_t3514_Vector3U26_t3514_RaycastHitU26_t3526_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t215 * p1, Vector3_t215 * p2, RaycastHit_t482 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t215 *)args[0], (Vector3_t215 *)args[1], (RaycastHit_t482 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector3_t215_Vector3_t215_Single_t531_Vector3_t215_RaycastHitU26_t3526_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, float p3, Vector3_t215  p4, RaycastHit_t482 * p5, float p6, int32_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), *((float*)args[2]), *((Vector3_t215 *)args[3]), (RaycastHit_t482 *)args[4], *((float*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector3U26_t3514_Vector3U26_t3514_Single_t531_Vector3U26_t3514_RaycastHitU26_t3526_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t215 * p1, Vector3_t215 * p2, float p3, Vector3_t215 * p4, RaycastHit_t482 * p5, float p6, int32_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t215 *)args[0], (Vector3_t215 *)args[1], *((float*)args[2]), (Vector3_t215 *)args[3], (RaycastHit_t482 *)args[4], *((float*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Ray_t465_RaycastHitU26_t3526 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t465  p1, RaycastHit_t482 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), (RaycastHit_t482 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Ray_t465_RaycastHitU26_t3526_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t465  p1, RaycastHit_t482 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), (RaycastHit_t482 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Ray_t465 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t465  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t215_Vector3_t215_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3U26_t3514_Vector3U26_t3514_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t215 * p1, Vector3_t215 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t215 *)args[0], (Vector3_t215 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Ray_t465_Single_t531_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t465  p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_QuaternionU26_t3515 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Quaternion_t261 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Quaternion_t261 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Ray_t465_RaycastHitU26_t3526_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t465  p2, RaycastHit_t482 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Ray_t465 *)args[1]), (RaycastHit_t482 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_RayU26_t3522_RaycastHitU26_t3526_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Ray_t465 * p2, RaycastHit_t482 * p3, float p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t465 *)args[1], (RaycastHit_t482 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Ray_t465_RaycastHitU26_t3526_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t465  p1, RaycastHit_t482 * p2, float p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t465 *)args[0]), (RaycastHit_t482 *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2U26_t3518_Object_t_Vector2_t7_Vector3_t215_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7 * p1, Object_t * p2, Vector2_t7  p3, Vector3_t215  p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t7 *)args[0], (Object_t *)args[1], *((Vector2_t7 *)args[2]), *((Vector3_t215 *)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2U26_t3518_Object_t_Vector2U26_t3518_Vector3U26_t3514_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7 * p1, Object_t * p2, Vector2_t7 * p3, Vector3_t215 * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t7 *)args[0], (Object_t *)args[1], (Vector2_t7 *)args[2], (Vector3_t215 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
void* RuntimeInvoker_Void_t1548_Vector2_t7_Vector2_t7_Single_t531_Int32_t478_Single_t531_Single_t531_RaycastHit2DU26_t3527 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t789 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t789 *)args[6], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2U26_t3518_Vector2U26_t3518_Single_t531_Int32_t478_Single_t531_Single_t531_RaycastHit2DU26_t3527 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7 * p1, Vector2_t7 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t789 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t7 *)args[0], (Vector2_t7 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t789 *)args[6], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t789_Vector2_t7_Vector2_t7_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t789  (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t789  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t789_Vector2_t7_Vector2_t7_Single_t531_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t789  (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t789  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t7_Vector2_t7_Single_t531_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t7  p1, Vector2_t7  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), *((Vector2_t7 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2U26_t3518_Vector2U26_t3518_Single_t531_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t7 * p1, Vector2_t7 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t7 *)args[0], (Vector2_t7 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector3_t215 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t215 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
void* RuntimeInvoker_SendMessageOptions_t832 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
void* RuntimeInvoker_AnimatorStateInfo_t542 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t542  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t542  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
void* RuntimeInvoker_AnimatorClipInfo_t942 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t942  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t942  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t215  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t215 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Vector3_t215 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Vector3_t215 *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_AnimatorStateInfo_t542_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t542  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	AnimatorStateInfo_t542  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Color_t6_Int32_t478_Single_t531_Single_t531_Int32_t478_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_Vector2_t7_Vector2_t7_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t6  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t7  p16, Vector2_t7  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t6 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t7 *)args[15]), *((Vector2_t7 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Color_t6_Int32_t478_Single_t531_Single_t531_Int32_t478_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_Single_t531_Single_t531_Single_t531_Single_t531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t6  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t6 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_ColorU26_t3511_Int32_t478_Single_t531_Single_t531_Int32_t478_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_Single_t531_Single_t531_Single_t531_Single_t531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t6 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t6 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextGenerationSettings_t773_TextGenerationSettings_t773 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t773  (*Func)(void* obj, TextGenerationSettings_t773  p1, const MethodInfo* method);
	TextGenerationSettings_t773  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t773 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_Object_t_TextGenerationSettings_t773 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t773  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t773 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_TextGenerationSettings_t773 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t773  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t773 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.RenderMode
#include "UnityEngine_UnityEngine_RenderMode.h"
void* RuntimeInvoker_RenderMode_t959 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_ColorU26_t3511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t6 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t6 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Vector2_t7_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t7  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t7 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Vector2U26_t3518_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t7 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t7 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector2_t7_Vector2_t7_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t7  (*Func)(void* obj, Vector2_t7  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t7  ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2_t7_Object_t_Object_t_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7  p1, Object_t * p2, Object_t * p3, Vector2_t7 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t7 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t7 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2U26_t3518_Object_t_Object_t_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7 * p1, Object_t * p2, Object_t * p3, Vector2_t7 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t7 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t7 *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Rect_t225_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t225  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t225  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Vector2_t7_Object_t_Vector3U26_t3514 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t7  p2, Object_t * p3, Vector3_t215 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t7 *)args[1]), (Object_t *)args[2], (Vector3_t215 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Vector2_t7_Object_t_Vector2U26_t3518 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t7  p2, Object_t * p3, Vector2_t7 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t7 *)args[1]), (Object_t *)args[2], (Vector2_t7 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Ray_t465_Object_t_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t465  (*Func)(void* obj, Object_t * p1, Vector2_t7  p2, const MethodInfo* method);
	Ray_t465  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t7 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
void* RuntimeInvoker_SourceID_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
void* RuntimeInvoker_AppID_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt64
#include "mscorlib_System_UInt64.h"
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UInt32
#include "mscorlib_System_UInt32.h"
void* RuntimeInvoker_UInt32_t1124 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
void* RuntimeInvoker_NetworkID_t979 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt64_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
void* RuntimeInvoker_NodeID_t980 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt16_t1122 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1134_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1134_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1134_UInt16_t1122_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, uint16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t446 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t446  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t446 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t446 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t446  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t446 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t3510_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (bool*)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
void* RuntimeInvoker_UserState_t1043 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Double_t553_SByte_t1135_SByte_t1135_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t871  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t871 *)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Object_t_DateTime_t871_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t871  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t871 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
void* RuntimeInvoker_UserScope_t1044 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
void* RuntimeInvoker_Range_t1038 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t1038  (*Func)(void* obj, const MethodInfo* method);
	Range_t1038  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Range_t1038 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t1038  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t1038 *)args[0]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
void* RuntimeInvoker_TimeScope_t1045 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
void* RuntimeInvoker_Void_t1548_Int32_t478_HitInfo_t1039 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t1039  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t1039 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_HitInfo_t1039_HitInfo_t1039 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t1039  p1, HitInfo_t1039  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t1039 *)args[0]), *((HitInfo_t1039 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_HitInfo_t1039 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t1039  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t1039 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
// System.String
#include "mscorlib_System_String.h"
struct String_t;
void* RuntimeInvoker_Void_t1548_Object_t_StringU26_t3529_StringU26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
void* RuntimeInvoker_Void_t1548_Object_t_StreamingContext_t1105 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t1105  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1105 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_AnimatorStateInfo_t542_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, AnimatorStateInfo_t542  p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((AnimatorStateInfo_t542 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Color_t6_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t6  p1, Color_t6  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), *((Color_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_TextGenerationSettings_t773 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t773  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t773 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
void* RuntimeInvoker_PersistentListenerMode_t1059 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1198_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1202 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.Byte
#include "mscorlib_System_Byte.h"
void* RuntimeInvoker_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t469;
#include "mscorlib_ArrayTypes.h"
void* RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_ByteU26_t3530_Int32U26_t3510_ByteU5BU5DU26_t3531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t469** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t469**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t871_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
// System.Security.Cryptography.DSAParameters
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t1334 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t1334  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t1334 *)args[1]), method);
	return ret;
}

struct Object_t;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
void* RuntimeInvoker_RSAParameters_t1307_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1307  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t1307  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_RSAParameters_t1307 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t1307  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t1307 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1334_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1334  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t1334  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t871  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
void* RuntimeInvoker_X509ChainStatusFlags_t1238 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Byte_t1117_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
void* RuntimeInvoker_AlertLevel_t1257 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
void* RuntimeInvoker_AlertDescription_t1258 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int16_t1136_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Int16_t1136_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
void* RuntimeInvoker_CipherAlgorithmType_t1260 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
void* RuntimeInvoker_HashAlgorithmType_t1278 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
void* RuntimeInvoker_ExchangeAlgorithmType_t1276 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
void* RuntimeInvoker_CipherMode_t1190 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct ByteU5BU5D_t469;
struct ByteU5BU5D_t469;
void* RuntimeInvoker_Void_t1548_Object_t_ByteU5BU5DU26_t3531_ByteU5BU5DU26_t3531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t469** p2, ByteU5BU5D_t469** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t469**)args[1], (ByteU5BU5D_t469**)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t1117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Int16_t1136_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
void* RuntimeInvoker_SecurityProtocolType_t1293 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
void* RuntimeInvoker_SecurityCompressionType_t1292 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
void* RuntimeInvoker_HandshakeType_t1309 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
void* RuntimeInvoker_HandshakeState_t1277 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SecurityProtocolType_t1293_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t1117_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t1117_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Byte_t1117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t1117_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Int64_t1131_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Byte_t1117_Byte_t1117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RSAParameters_t1307 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t1307  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t1307  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Byte_t1117_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Byte_t1117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
void* RuntimeInvoker_ContentType_t1271 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DictionaryNode_t1380;
// System.Collections.Specialized.ListDictionary/DictionaryNode
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t3532 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t1380 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t1380 **)args[1], method);
	return ret;
}

struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
void* RuntimeInvoker_DictionaryEntry_t552 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"
void* RuntimeInvoker_EditorBrowsableState_t1391 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPAddress_t1408;
// System.Net.IPAddress
#include "System_System_Net_IPAddress.h"
void* RuntimeInvoker_Boolean_t536_Object_t_IPAddressU26_t3533 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t1408 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t1408 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"
void* RuntimeInvoker_AddressFamily_t1396 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct IPv6Address_t1409;
// System.Net.IPv6Address
#include "System_System_Net_IPv6Address.h"
void* RuntimeInvoker_Boolean_t536_Object_t_IPv6AddressU26_t3534 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t1409 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t1409 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Net.SecurityProtocolType
#include "System_System_Net_SecurityProtocolType.h"
void* RuntimeInvoker_SecurityProtocolType_t1410 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.AsnDecodeStatus
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
struct Object_t;
void* RuntimeInvoker_AsnDecodeStatus_t1450_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1438_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1438_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1438_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatusFlags_t1438 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationFlag
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
void* RuntimeInvoker_X509RevocationFlag_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509RevocationMode
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
void* RuntimeInvoker_X509RevocationMode_t1446 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509VerificationFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
void* RuntimeInvoker_X509VerificationFlags_t1449 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
void* RuntimeInvoker_X509KeyUsageFlags_t1443 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509KeyUsageFlags_t1443_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Int16_t1136_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.RegexOptions
#include "System_System_Text_RegularExpressions_RegexOptions.h"
void* RuntimeInvoker_RegexOptions_t1464 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"
struct Object_t;
void* RuntimeInvoker_Category_t1471_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UInt16_t1122_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int16_t1136_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt16_t1122_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int16_t1136_Int16_t1136_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int16_t1136_Object_t_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_UInt16_t1122_UInt16_t1122 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.OpFlags
#include "System_System_Text_RegularExpressions_OpFlags.h"
void* RuntimeInvoker_OpFlags_t1466_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt16_t1122_UInt16_t1122 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Int32U26_t3510_Int32U26_t3510_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UInt16_t1122_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Int32_t478_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.Interval
#include "System_System_Text_RegularExpressions_Interval.h"
void* RuntimeInvoker_Interval_t1486 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1486  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1486  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Interval_t1486 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1486  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1486 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Interval_t1486 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1486  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1486 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Interval_t1486_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1486  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1486  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Interval_t1486 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1486  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1486 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Interval_t1486_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1486  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1486 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t553_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RegexOptionsU26_t3535 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_RegexOptionsU26_t3535_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32U26_t3510_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Category_t1471 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int16_t1136_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t526_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt16_t1122_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int16_t1136_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_UInt16_t1122 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

struct Object_t;
// System.Text.RegularExpressions.Position
#include "System_System_Text_RegularExpressions_Position.h"
void* RuntimeInvoker_Position_t1467 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UriHostNameType
#include "System_System_UriHostNameType.h"
struct Object_t;
void* RuntimeInvoker_UriHostNameType_t1519_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t1548_StringU26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t526_Object_t_Int32U26_t3510_CharU26_t3536 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct UriFormatException_t1518;
// System.UriFormatException
#include "System_System_UriFormatException.h"
void* RuntimeInvoker_Void_t1548_Object_t_UriFormatExceptionU26_t3537 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1518 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1518 **)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t470;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_ObjectU5BU5DU26_t3538 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t470** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t470**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t470;
void* RuntimeInvoker_Int32_t478_Object_t_ObjectU5BU5DU26_t3538 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t470** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t470**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t1117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Decimal
#include "mscorlib_System_Decimal.h"
struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t1136_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1131_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t520;
// System.Exception
#include "mscorlib_System_Exception.h"
void* RuntimeInvoker_Boolean_t536_SByte_t1135_Object_t_Int32_t478_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t520 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t520 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_Int32U26_t3510_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t520 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t520 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Int32_t478_SByte_t1135_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t520 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t520 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Int32U26_t3510_Object_t_SByte_t1135_SByte_t1135_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t520 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t520 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32U26_t3510_Object_t_Object_t_BooleanU26_t3509_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32U26_t3510_Object_t_Object_t_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Int32U26_t3510_Object_t_Int32U26_t3510_SByte_t1135_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t520 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t520 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32U26_t3510_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int16_t1136_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_Int32U26_t3510_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t520 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t520 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
void* RuntimeInvoker_TypeCode_t2140 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_Int64U26_t3540_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t520 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t520 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1131_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_Int64U26_t3540_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t520 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t520 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1131_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int64U26_t3540 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_Int64U26_t3540 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_UInt32U26_t3541_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t520 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t520 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_UInt32U26_t3541_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t520 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t520 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_UInt32U26_t3541 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_UInt32U26_t3541 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_UInt64U26_t3542_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t520 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t520 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_UInt64U26_t3542 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t1117_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Byte_t1117_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_ByteU26_t3530 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_ByteU26_t3530 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_SByteU26_t3543_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t520 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t520 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_SByte_t1135_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_SByteU26_t3543 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_Int16U26_t3544_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t520 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t520 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t1136_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t1136_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int16U26_t3544 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_UInt16U26_t3545 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_UInt16U26_t3545 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ByteU2AU26_t3546_ByteU2AU26_t3546_DoubleU2AU26_t3547_UInt16U2AU26_t3548_UInt16U2AU26_t3548_UInt16U2AU26_t3548_UInt16U2AU26_t3548 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

struct Object_t;
// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"
void* RuntimeInvoker_UnicodeCategory_t1693_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t526_Int16_t1136_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int16_t1136_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Char_t526_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int16_t1136_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_Int32U26_t3510_Int32U26_t3510_BooleanU26_t3509_StringU26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Single_t531_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t553_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t553_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_DoubleU26_t3549_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t520 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t520 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_DoubleU26_t3549 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_DoubleU26_t3549 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, double* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (double*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Decimal_t1133_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, Decimal_t1133  p1, Decimal_t1133  p2, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), *((Decimal_t1133 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Decimal_t1133_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1133  p1, Decimal_t1133  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), *((Decimal_t1133 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Decimal_t1133_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133  p1, Decimal_t1133  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), *((Decimal_t1133 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Int32U26_t3510_BooleanU26_t3509_BooleanU26_t3509_Int32U26_t3510_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_DecimalU26_t3550_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t1133 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t1133 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DecimalU26_t3550_UInt64U26_t3542 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1133 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DecimalU26_t3550_Int64U26_t3540 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1133 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DecimalU26_t3550_DecimalU26_t3550 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133 * p1, Decimal_t1133 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1133 *)args[0], (Decimal_t1133 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_DecimalU26_t3550_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1133 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DecimalU26_t3550_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1133 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_DecimalU26_t3550 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1133 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t1133 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_DecimalU26_t3550_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1133 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t1133 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DecimalU26_t3550_DecimalU26_t3550_DecimalU26_t3550 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1133 * p1, Decimal_t1133 * p2, Decimal_t1133 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1133 *)args[0], (Decimal_t1133 *)args[1], (Decimal_t1133 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_IntPtr_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"
void* RuntimeInvoker_UIntPtr_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIntPtr_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct MulticastDelegate_t219;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t3551 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t219 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t219 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1131_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Object_t_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
void* RuntimeInvoker_TypeAttributes_t1812 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
void* RuntimeInvoker_MemberTypes_t1791 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
void* RuntimeInvoker_RuntimeTypeHandle_t1550 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1550  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1550  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeCode_t2140_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1550 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1550  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1550 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_RuntimeTypeHandle_t1550_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1550  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1550  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
void* RuntimeInvoker_Void_t1548_Object_t_RuntimeFieldHandle_t1551 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1551  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1551 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ContractionU5BU5D_t1597;
struct Level2MapU5BU5D_t1598;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_ContractionU5BU5DU26_t3552_Level2MapU5BU5DU26_t3553 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1597** p3, Level2MapU5BU5D_t1598** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1597**)args[2], (Level2MapU5BU5D_t1598**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct CodePointIndexer_t1582;
// Mono.Globalization.Unicode.CodePointIndexer
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
struct CodePointIndexer_t1582;
void* RuntimeInvoker_Void_t1548_Object_t_CodePointIndexerU26_t3554_ByteU2AU26_t3546_ByteU2AU26_t3546_CodePointIndexerU26_t3554_ByteU2AU26_t3546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1582 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1582 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1582 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1582 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
void* RuntimeInvoker_ExtenderType_t1594_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_BooleanU26_t3509_BooleanU26_t3509_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// Mono.Globalization.Unicode.SimpleCollator/Context
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_BooleanU26_t3509_BooleanU26_t3509_SByte_t1135_SByte_t1135_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1591 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1591 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_Int32_t478_SByte_t1135_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1591 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1591 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int16_t1136_Int32_t478_SByte_t1135_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1591 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1591 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_Object_t_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1591 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1591 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1591 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1591 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Object_t_SByte_t1135_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1591 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1591 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1584;
// Mono.Globalization.Unicode.Contraction
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
void* RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Object_t_SByte_t1135_Int32_t478_ContractionU26_t3556_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1584 ** p8, Context_t1591 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1584 **)args[7], (Context_t1591 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Int32_t478_Object_t_SByte_t1135_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1591 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1591 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Contraction_t1584;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Int32_t478_Object_t_SByte_t1135_Int32_t478_ContractionU26_t3556_ContextU26_t3555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1584 ** p9, Context_t1591 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1584 **)args[8], (Context_t1591 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

struct Object_t;
struct ByteU5BU5D_t469;
void* RuntimeInvoker_Void_t1548_SByte_t1135_ByteU5BU5DU26_t3531_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t469** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t469**)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.Prime.ConfidenceFactor
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
void* RuntimeInvoker_ConfidenceFactor_t1603 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Math.BigInteger/Sign
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Sign_t1605_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DSAParameters_t1334_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t1334  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t1334  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_DSAParameters_t1334 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t1334  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t1334 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int16_t1136_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Double_t553_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Single_t531_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Single_t531_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct MethodBase_t1163;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
struct String_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_SByte_t1135_MethodBaseU26_t3557_Int32U26_t3510_Int32U26_t3510_StringU26_t3529_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t1163 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t1163 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t871  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
void* RuntimeInvoker_DayOfWeek_t2091_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t871  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32U26_t3510_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2091_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32U26_t3510_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
void* RuntimeInvoker_Void_t1548_DateTime_t871_DateTime_t871_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t871  p1, DateTime_t871  p2, TimeSpan_t1437  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((DateTime_t871 *)args[1]), *((TimeSpan_t1437 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1133  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t1133  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_SByte_t1135_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_IntPtr_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"
void* RuntimeInvoker_Boolean_t536_Object_t_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"
struct Object_t;
void* RuntimeInvoker_FileAttributes_t1703_Object_t_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"
void* RuntimeInvoker_MonoFileType_t1712_IntPtr_t_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
// System.IO.MonoIOStat
#include "mscorlib_System_IO_MonoIOStat.h"
void* RuntimeInvoker_Boolean_t536_Object_t_MonoIOStatU26_t3559_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1711 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1711 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_IntPtr_t_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_IntPtr_t_Object_t_Int32_t478_Int32_t478_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_IntPtr_t_Int64_t1131_Int32_t478_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_IntPtr_t_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_IntPtr_t_Int64_t1131_MonoIOErrorU26_t3558 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
void* RuntimeInvoker_CallingConventions_t1786 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
void* RuntimeInvoker_RuntimeMethodHandle_t2132 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t2132  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t2132  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
void* RuntimeInvoker_MethodAttributes_t1792 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.Emit.MethodToken
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
void* RuntimeInvoker_MethodToken_t1751 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1751  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1751  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
void* RuntimeInvoker_FieldAttributes_t1789 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RuntimeFieldHandle_t1551 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1551  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1551  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.OpCode
#include "mscorlib_System_Reflection_Emit_OpCode.h"
void* RuntimeInvoker_Void_t1548_OpCode_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1755  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1755 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_OpCode_t1755_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1755  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1755 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.StackBehaviour
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
void* RuntimeInvoker_StackBehaviour_t1760 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
void* RuntimeInvoker_PropertyAttributes_t1808 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Module_t1748;
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t3510_ModuleU26_t3560 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1748 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1748 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct Object_t;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
void* RuntimeInvoker_AssemblyNameFlags_t1780 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t470;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_ObjectU5BU5DU26_t3538_Object_t_Object_t_Object_t_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t470** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t470**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

struct Object_t;
struct ObjectU5BU5D_t470;
struct Object_t;
void* RuntimeInvoker_Void_t1548_ObjectU5BU5DU26_t3538_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t470** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t470**)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct ObjectU5BU5D_t470;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_ObjectU5BU5DU26_t3538_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t470** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t470**)args[1], (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
void* RuntimeInvoker_EventAttributes_t1787 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1551 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1551  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1551 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1105 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1105  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1105 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t2132 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t2132  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t2132 *)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
void* RuntimeInvoker_Void_t1548_Object_t_MonoEventInfoU26_t3561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1797 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1797 *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_MonoEventInfo_t1797_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1797  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1797  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
void* RuntimeInvoker_Void_t1548_IntPtr_t_MonoMethodInfoU26_t3562 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1800 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1800 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_MonoMethodInfo_t1800_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1800  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1800  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_MethodAttributes_t1792_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_CallingConventions_t1786_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t520 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t520 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
void* RuntimeInvoker_Void_t1548_Object_t_MonoPropertyInfoU26_t3563_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1801 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1801 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
void* RuntimeInvoker_ParameterAttributes_t1804 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
struct Object_t;
void* RuntimeInvoker_GCHandle_t1833_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1833  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1833  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_IntPtr_t_Object_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t1437  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1105_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1105  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1105 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct ISurrogateSelector_t1885;
void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t1105_ISurrogateSelectorU26_t3564 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t1105  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t1105 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct String_t;
void* RuntimeInvoker_Object_t_StringU26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct String_t;
struct String_t;
void* RuntimeInvoker_Boolean_t536_Object_t_StringU26_t3529_StringU26_t3529 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Remoting.WellKnownObjectMode
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
void* RuntimeInvoker_WellKnownObjectMode_t1926 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_StreamingContext_t1105 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t1105  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t1105  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.Formatters.TypeFilterLevel
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
void* RuntimeInvoker_TypeFilterLevel_t1942 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t1117_Object_t_SByte_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Byte_t1117_Object_t_SByte_t1135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2147;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_ObjectU26_t3528_HeaderU5BU5DU26_t3565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t2147** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t2147**)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct HeaderU5BU5D_t2147;
void* RuntimeInvoker_Void_t1548_Byte_t1117_Object_t_SByte_t1135_ObjectU26_t3528_HeaderU5BU5DU26_t3565 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t2147** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t2147**)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Byte_t1117_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1104;
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
void* RuntimeInvoker_Void_t1548_Byte_t1117_Object_t_Int64U26_t3540_ObjectU26_t3528_SerializationInfoU26_t3566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t1104 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t1104 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1104;
void* RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_SByte_t1135_Int64U26_t3540_ObjectU26_t3528_SerializationInfoU26_t3566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t1104 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t1104 **)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1104;
void* RuntimeInvoker_Void_t1548_Object_t_Int64U26_t3540_ObjectU26_t3528_SerializationInfoU26_t3566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t1104 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t1104 **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct SerializationInfo_t1104;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64_t1131_ObjectU26_t3528_SerializationInfoU26_t3566 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t1104 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t1104 **)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Object_t_Int64_t1131_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int64U26_t3540_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64U26_t3540_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64_t1131_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Int64_t1131_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int64_t1131_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Int32_t478_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Object_t_Int64_t1131_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_SByte_t1135_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_StreamingContext_t1105 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1105  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1105 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_StreamingContext_t1105 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t1105  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t1105 *)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_StreamingContext_t1105 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t1105  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t1105 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_StreamingContext_t1105_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t1105  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t1105 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t871  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t871 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Runtime.Serialization.SerializationEntry
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
void* RuntimeInvoker_SerializationEntry_t1959 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1959  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1959  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Runtime.Serialization.StreamingContextStates
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
void* RuntimeInvoker_StreamingContextStates_t1962 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.CspProviderFlags
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
void* RuntimeInvoker_CspProviderFlags_t1966 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt32U26_t3541_Int32_t478_UInt32U26_t3541_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Int64_t1131_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Int64_t1131_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Security.Cryptography.PaddingMode
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
void* RuntimeInvoker_PaddingMode_t1193 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct StringBuilder_t780;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
void* RuntimeInvoker_Void_t1548_StringBuilderU26_t3567_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t780 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t780 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct EncoderFallbackBuffer_t2039;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
struct CharU5BU5D_t530;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_EncoderFallbackBufferU26_t3568_CharU5BU5DU26_t3569 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t2039 ** p6, CharU5BU5D_t530** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t2039 **)args[5], (CharU5BU5D_t530**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2030;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_DecoderFallbackBufferU26_t3570 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t2030 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t2030 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int16_t1136_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int16_t1136_Int16_t1136_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int16_t1136_Int16_t1136_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int32_t478_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135_Int32U26_t3510_BooleanU26_t3509_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_CharU26_t3536_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_CharU26_t3536_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_CharU26_t3536_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_CharU26_t3536_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2030;
struct ByteU5BU5D_t469;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t2030 ** p7, ByteU5BU5D_t469** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t2030 **)args[6], (ByteU5BU5D_t469**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2030;
struct ByteU5BU5D_t469;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t2030 ** p6, ByteU5BU5D_t469** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t2030 **)args[5], (ByteU5BU5D_t469**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2030;
struct ByteU5BU5D_t469;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_Object_t_Int64_t1131_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2030 ** p2, ByteU5BU5D_t469** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2030 **)args[1], (ByteU5BU5D_t469**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2030;
struct ByteU5BU5D_t469;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_Object_t_Int64_t1131_Int32_t478_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t2030 ** p2, ByteU5BU5D_t469** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t2030 **)args[1], (ByteU5BU5D_t469**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2030;
struct ByteU5BU5D_t469;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_UInt32U26_t3541_UInt32U26_t3541_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t2030 ** p9, ByteU5BU5D_t469** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t2030 **)args[8], (ByteU5BU5D_t469**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct DecoderFallbackBuffer_t2030;
struct ByteU5BU5D_t469;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_UInt32U26_t3541_UInt32U26_t3541_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t2030 ** p8, ByteU5BU5D_t469** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t2030 **)args[7], (ByteU5BU5D_t469**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t1135_Object_t_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_IntPtr_t_SByte_t1135_SByte_t1135_Object_t_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_TimeSpan_t1437_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1437  p1, TimeSpan_t1437  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int64_t1131_Int64_t1131_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_IntPtr_t_Int32_t478_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1131_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Byte_t1117_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t526_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t526_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Char_t526_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Char_t526_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t871_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int16_t1136_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_SByte_t1135_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Single_t531_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt16_t1122_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt32_t1124_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UInt64_t1134_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SByte_t1135_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t1437  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_DayOfWeek_t2091 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
void* RuntimeInvoker_DateTimeKind_t2089 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, TimeSpan_t1437  p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DateTime_t871_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t871  p1, DateTime_t871  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((DateTime_t871 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_DateTime_t871_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, DateTime_t871  p1, int32_t p2, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t871_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_DateTimeU26_t3571_DateTimeOffsetU26_t3572_SByte_t1135_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t871 * p4, DateTimeOffset_t1148 * p5, int8_t p6, Exception_t520 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t871 *)args[3], (DateTimeOffset_t1148 *)args[4], *((int8_t*)args[5]), (Exception_t520 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t520 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t520 **)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Object_t_SByte_t1135_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Int32_t478_Object_t_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Int32_t478_Object_t_SByte_t1135_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_SByte_t1135_DateTimeU26_t3571_DateTimeOffsetU26_t3572_Object_t_Int32_t478_SByte_t1135_BooleanU26_t3509_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t871 * p5, DateTimeOffset_t1148 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t871 *)args[4], (DateTimeOffset_t1148 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DateTime_t871_Object_t_Object_t_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Exception_t520;
void* RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Int32_t478_DateTimeU26_t3571_SByte_t1135_BooleanU26_t3509_SByte_t1135_ExceptionU26_t3539 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t871 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t520 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t871 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t520 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_DateTime_t871_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, DateTime_t871  p1, TimeSpan_t1437  p2, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_DateTime_t871_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t871  p1, DateTime_t871  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((DateTime_t871 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_DateTime_t871_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t871  p1, TimeSpan_t1437  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int64_t1131_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t1437  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DateTimeOffset_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1148  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1148 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_DateTimeOffset_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1148  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1148 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTimeOffset_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTimeOffset_t1148  (*Func)(void* obj, const MethodInfo* method);
	DateTimeOffset_t1148  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_Object_t_BooleanU26_t3509_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_Object_t_BooleanU26_t3509_BooleanU26_t3509_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t871_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t871  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen_0.h"
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_DateTime_t871_Nullable_1_t2187_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t871  p1, Nullable_1_t2187  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((Nullable_1_t2187 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
void* RuntimeInvoker_Void_t1548_MonoEnumInfo_t2102 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t2102  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t2102 *)args[0]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_MonoEnumInfoU26_t3573 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t2102 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t2102 *)args[1], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int16_t1136_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
void* RuntimeInvoker_PlatformID_t2129 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int16_t1136_Int16_t1136_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

struct Object_t;
// System.Guid
#include "mscorlib_System_Guid.h"
void* RuntimeInvoker_Int32_t478_Guid_t555 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t555  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t555 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Guid_t555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t555  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t555 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Guid_t555 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t555  (*Func)(void* obj, const MethodInfo* method);
	Guid_t555  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t1135_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Double_t553_Double_t553_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TypeAttributes_t1812_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt64U2AU26_t3574_Int32U2AU26_t3575_CharU2AU26_t3576_CharU2AU26_t3576_Int64U2AU26_t3577_Int32U2AU26_t3575 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Double_t553_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t1133  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1133 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int16_t1136_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t1131_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Double_t553_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Decimal_t1133_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t1133  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1133 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Double_t553_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_BooleanU26_t3509_SByte_t1135_Int32U26_t3510_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Object_t_SByte_t1135_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int64_t1131_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, TimeSpan_t1437  p1, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_TimeSpan_t1437_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1437  p1, TimeSpan_t1437  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t1437  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t1437  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_Double_t553_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_TimeSpan_t1437_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, TimeSpan_t1437  p1, TimeSpan_t1437  p2, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, *((TimeSpan_t1437 *)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, DateTime_t871  p1, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_DateTime_t871_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t871  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DateTime_t871_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t871  (*Func)(void* obj, DateTime_t871  p1, const MethodInfo* method);
	DateTime_t871  ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_DateTime_t871_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, DateTime_t871  p1, TimeSpan_t1437  p2, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, *((DateTime_t871 *)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int64U5BU5D_t2176;
struct StringU5BU5D_t398;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Int64U5BU5DU26_t3578_StringU5BU5DU26_t3579 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t2176** p2, StringU5BU5D_t398** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t2176**)args[1], (StringU5BU5D_t398**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_KeyValuePair_2_t446 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, KeyValuePair_2_t446  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((KeyValuePair_2_t446 *)args[4]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_KeyValuePair_2_t446_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, KeyValuePair_2_t446  p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((KeyValuePair_2_t446 *)args[4]), (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_ObjectU26_t3528_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_ObjectU26_t3528_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2350  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2350 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2350  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2350 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_0.h"
void* RuntimeInvoker_Enumerator_t2872 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2872  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2872  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t2269 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2269  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2269  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t470;
void* RuntimeInvoker_Void_t1548_ObjectU5BU5DU26_t3538_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t470** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t470**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct ObjectU5BU5D_t470;
void* RuntimeInvoker_Void_t1548_ObjectU5BU5DU26_t3538_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t470** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t470**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2350_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2350  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2350  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
void* RuntimeInvoker_Enumerator_t2354 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2354  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2354  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2350  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2350  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5.h"
void* RuntimeInvoker_Enumerator_t2353 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2353  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2353  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9.h"
void* RuntimeInvoker_Enumerator_t2357 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2357  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2357  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"
void* RuntimeInvoker_Enumerator_t2227 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2227  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2227  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_15.h"
void* RuntimeInvoker_Enumerator_t2521 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2521  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2521  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__9.h"
void* RuntimeInvoker_Enumerator_t2518 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2518  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2518  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
void* RuntimeInvoker_KeyValuePair_2_t2514 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2514  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2514  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
void* RuntimeInvoker_Void_t1548_ColorTween_t630 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t630  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t630 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_TypeU26_t3580_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_BooleanU26_t3509_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_FillMethodU26_t3581_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_SingleU26_t3519_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_ContentTypeU26_t3582_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_LineTypeU26_t3583_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_InputTypeU26_t3584_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_TouchScreenKeyboardTypeU26_t3585_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_CharacterValidationU26_t3586_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_CharU26_t3536_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_DirectionU26_t3587_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_NavigationU26_t3588_Navigation_t690 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t690 * p1, Navigation_t690  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t690 *)args[0], *((Navigation_t690 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_TransitionU26_t3589_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_ColorBlockU26_t3590_ColorBlock_t642 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t642 * p1, ColorBlock_t642  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t642 *)args[0], *((ColorBlock_t642 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_SpriteStateU26_t3591_SpriteState_t708 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t708 * p1, SpriteState_t708  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t708 *)args[0], *((SpriteState_t708 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_DirectionU26_t3592_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_AspectModeU26_t3593_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_FitModeU26_t3594_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_CornerU26_t3595_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_AxisU26_t3596_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Vector2U26_t3518_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t7 * p1, Vector2_t7  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t7 *)args[0], *((Vector2_t7 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ConstraintU26_t3597_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_SingleU26_t3519_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_BooleanU26_t3509_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_TextAnchorU26_t3598_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Double_t553 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t215  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t7  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Vector2_t7 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t7  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t7 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t6  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color_t6  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastResult_t511_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t511  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t511  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_RaycastResult_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t511  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_RaycastResult_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t511  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_RaycastResult_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t511  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t511 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2247;
#include "UnityEngine.UI_ArrayTypes.h"
void* RuntimeInvoker_Void_t1548_RaycastResultU5BU5DU26_t3599_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2247** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2247**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct RaycastResultU5BU5D_t2247;
void* RuntimeInvoker_Void_t1548_RaycastResultU5BU5DU26_t3599_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t2247** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t2247**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_RaycastResult_t511_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t511  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t511 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_RaycastResult_t511_RaycastResult_t511_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t511  p1, RaycastResult_t511  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), *((RaycastResult_t511 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct CharU5BU5D_t530;
void* RuntimeInvoker_Void_t1548_CharU5BU5DU26_t3569_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CharU5BU5D_t530** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CharU5BU5D_t530**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct CharU5BU5D_t530;
void* RuntimeInvoker_Void_t1548_CharU5BU5DU26_t3569_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CharU5BU5D_t530** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CharU5BU5D_t530**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int16_t1136_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int16_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Int16_t1136_Int16_t1136_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit_t482_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t482  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t482  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_RaycastHit_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t482  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t482 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_RaycastHit_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t482  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t482 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_RaycastHit_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t482  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t482 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_RaycastHit_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t482  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t482 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
void* RuntimeInvoker_KeyValuePair_2_t2328_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2328  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2328  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2328 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2328  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2328 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2328 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2328  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2328 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2328 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2328  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2328 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2328 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2328  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2328 *)args[1]), method);
	return NULL;
}

struct Object_t;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"
void* RuntimeInvoker_TweenType_t442_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_DictionaryEntry_t552 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t552  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t552 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_DictionaryEntry_t552 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t552  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t552 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DictionaryEntry_t552 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t552  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t552 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_DictionaryEntry_t552 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t552  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t552 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2350_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2350  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2350  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2350  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2350 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2350  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2350 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t225  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Rect_t225 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Rect_t225  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Rect_t225 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8.h"
void* RuntimeInvoker_KeyValuePair_2_t2364_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2364  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2364  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2364 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2364  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2364 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2364 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2364  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2364 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2364  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2364 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2364 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2364  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2364 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
void* RuntimeInvoker_Space_t546_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
void* RuntimeInvoker_EaseType_t425_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"
void* RuntimeInvoker_LoopType_t426_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Int32U5BU5D_t269;
void* RuntimeInvoker_Void_t1548_Int32U5BU5DU26_t3600_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t269** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t269**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Int32U5BU5D_t269;
void* RuntimeInvoker_Void_t1548_Int32U5BU5DU26_t3600_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t269** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t269**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct SingleU5BU5D_t72;
void* RuntimeInvoker_Void_t1548_SingleU5BU5DU26_t3601_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SingleU5BU5D_t72** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (SingleU5BU5D_t72**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct SingleU5BU5D_t72;
void* RuntimeInvoker_Void_t1548_SingleU5BU5DU26_t3601_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SingleU5BU5D_t72** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (SingleU5BU5D_t72**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Single_t531_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, float p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Single_t531_Single_t531_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct BooleanU5BU5D_t448;
void* RuntimeInvoker_Void_t1548_BooleanU5BU5DU26_t3602_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, BooleanU5BU5D_t448** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (BooleanU5BU5D_t448**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct BooleanU5BU5D_t448;
void* RuntimeInvoker_Void_t1548_BooleanU5BU5DU26_t3602_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, BooleanU5BU5D_t448** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (BooleanU5BU5D_t448**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_SByte_t1135_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_SByte_t1135_SByte_t1135_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Vector3U5BU5D_t317;
void* RuntimeInvoker_Void_t1548_Vector3U5BU5DU26_t3506_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t317** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t317**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct Vector3U5BU5D_t317;
void* RuntimeInvoker_Void_t1548_Vector3U5BU5DU26_t3506_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t317** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t317**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Vector3_t215_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t215  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t215 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Vector3_t215_Vector3_t215_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct ColorU5BU5D_t449;
void* RuntimeInvoker_Void_t1548_ColorU5BU5DU26_t3603_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorU5BU5D_t449** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ColorU5BU5D_t449**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct ColorU5BU5D_t449;
void* RuntimeInvoker_Void_t1548_ColorU5BU5DU26_t3603_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorU5BU5D_t449** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ColorU5BU5D_t449**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Color_t6_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Color_t6  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Color_t6 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Color_t6_Color_t6_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color_t6  p1, Color_t6  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), *((Color_t6 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct SpaceU5BU5D_t450;
void* RuntimeInvoker_Void_t1548_SpaceU5BU5DU26_t3604_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpaceU5BU5D_t450** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (SpaceU5BU5D_t450**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct SpaceU5BU5D_t450;
void* RuntimeInvoker_Void_t1548_SpaceU5BU5DU26_t3604_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpaceU5BU5D_t450** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (SpaceU5BU5D_t450**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct EaseTypeU5BU5D_t451;
#include "Assembly-CSharp_ArrayTypes.h"
void* RuntimeInvoker_Void_t1548_EaseTypeU5BU5DU26_t3605_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, EaseTypeU5BU5D_t451** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (EaseTypeU5BU5D_t451**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct EaseTypeU5BU5D_t451;
void* RuntimeInvoker_Void_t1548_EaseTypeU5BU5DU26_t3605_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, EaseTypeU5BU5D_t451** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (EaseTypeU5BU5D_t451**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct LoopTypeU5BU5D_t452;
void* RuntimeInvoker_Void_t1548_LoopTypeU5BU5DU26_t3606_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LoopTypeU5BU5D_t452** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (LoopTypeU5BU5D_t452**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct LoopTypeU5BU5D_t452;
void* RuntimeInvoker_Void_t1548_LoopTypeU5BU5DU26_t3606_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LoopTypeU5BU5D_t452** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (LoopTypeU5BU5D_t452**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2514_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2514  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2514  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2514  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2514 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2514 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2514  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2514 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2514 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2514  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2514 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2514 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2514  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2514 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t789_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t789  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t789  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_RaycastHit2D_t789 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t789  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t789 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_RaycastHit2D_t789 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t789  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t789 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_RaycastHit2D_t789 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t789  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t789 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_RaycastHit2D_t789 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t789  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t789 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t684;
void* RuntimeInvoker_Void_t1548_UIVertexU5BU5DU26_t3607_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t684** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t684**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UIVertexU5BU5D_t684;
void* RuntimeInvoker_Void_t1548_UIVertexU5BU5DU26_t3607_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t684** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t684**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_UIVertex_t685_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t685  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t685 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_UIVertex_t685_UIVertex_t685_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t685  p1, UIVertex_t685  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), *((UIVertex_t685 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ContentType_t671_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
void* RuntimeInvoker_UILineInfo_t809_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t809  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t809  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UILineInfo_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t809  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UILineInfo_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t809  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UILineInfo_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t809  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_UILineInfo_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t809  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t809 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"
void* RuntimeInvoker_UICharInfo_t811_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t811  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t811  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UICharInfo_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t811  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UICharInfo_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t811  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UICharInfo_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t811  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_UICharInfo_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t811  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t811 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
void* RuntimeInvoker_GcAchievementData_t1020_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t1020  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t1020  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_GcAchievementData_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t1020  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t1020 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_GcAchievementData_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t1020  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t1020 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_GcAchievementData_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t1020  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t1020 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_GcAchievementData_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t1020  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t1020 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t1021_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t1021  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t1021  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_GcScoreData_t1021 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t1021  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t1021 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_GcScoreData_t1021 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t1021  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t1021 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_GcScoreData_t1021 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t1021  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t1021 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.ContactPoint
#include "UnityEngine_UnityEngine_ContactPoint.h"
void* RuntimeInvoker_ContactPoint_t930_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t930  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint_t930  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ContactPoint_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint_t930  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint_t930 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_ContactPoint_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint_t930  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint_t930 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_ContactPoint_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint_t930  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint_t930 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_ContactPoint_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint_t930  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint_t930 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"
void* RuntimeInvoker_Keyframe_t943_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t943  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t943  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Keyframe_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t943  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t943 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Keyframe_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t943  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t943 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Keyframe_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t943  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t943 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Keyframe_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t943  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t943 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1086;
void* RuntimeInvoker_Void_t1548_UICharInfoU5BU5DU26_t3608_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1086** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1086**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UICharInfoU5BU5D_t1086;
void* RuntimeInvoker_Void_t1548_UICharInfoU5BU5DU26_t3608_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t1086** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t1086**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_UICharInfo_t811_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t811  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t811 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_UICharInfo_t811_UICharInfo_t811_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t811  p1, UICharInfo_t811  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), *((UICharInfo_t811 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct UILineInfoU5BU5D_t1087;
void* RuntimeInvoker_Void_t1548_UILineInfoU5BU5DU26_t3609_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1087** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1087**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct Object_t;
struct UILineInfoU5BU5D_t1087;
void* RuntimeInvoker_Void_t1548_UILineInfoU5BU5DU26_t3609_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t1087** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t1087**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_UILineInfo_t809_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t809  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t809 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_UILineInfo_t809_UILineInfo_t809_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t809  p1, UILineInfo_t809  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), *((UILineInfo_t809 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
void* RuntimeInvoker_KeyValuePair_2_t2706_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2706  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2706  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2706 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2706  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2706 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2706 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2706  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2706 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2706 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2706  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2706 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2706 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2706  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2706 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
void* RuntimeInvoker_KeyValuePair_2_t2743_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2743  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2743  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2743  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2743 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2743  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2743 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2743  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2743 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2743  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2743 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_NetworkID_t979_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UInt64_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UInt64_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_UInt64_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint64_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
void* RuntimeInvoker_KeyValuePair_2_t2762_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2762  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2762  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2762 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2762  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2762 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2762 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2762  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2762 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2762 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2762  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2762 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2762 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2762  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2762 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
void* RuntimeInvoker_ParameterModifier_t1805_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1805  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1805  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ParameterModifier_t1805 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1805  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1805 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_ParameterModifier_t1805 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1805  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1805 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_ParameterModifier_t1805 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1805  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1805 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_ParameterModifier_t1805 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1805  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1805 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t1039_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t1039  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t1039  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_HitInfo_t1039 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t1039  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t1039 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_HitInfo_t1039 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t1039  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t1039 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
void* RuntimeInvoker_KeyValuePair_2_t2825_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2825  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2825  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2825  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2825 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2825  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2825 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2825  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2825 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2825  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2825 *)args[1]), method);
	return NULL;
}

struct Object_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
void* RuntimeInvoker_TextEditOp_t1057_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
void* RuntimeInvoker_ClientCertificateType_t1308_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
void* RuntimeInvoker_KeyValuePair_2_t2877_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2877  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2877 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2877  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2877 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2877 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2877  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2877 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2877 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2877  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2877 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2877 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2877  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2877 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Security.Cryptography.X509Certificates.X509ChainStatus
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
void* RuntimeInvoker_X509ChainStatus_t1432_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1432  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t1432  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_X509ChainStatus_t1432 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t1432  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t1432 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_X509ChainStatus_t1432 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t1432  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t1432 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_X509ChainStatus_t1432 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t1432  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t1432 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_X509ChainStatus_t1432 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t1432  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t1432 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_31.h"
void* RuntimeInvoker_KeyValuePair_2_t2896_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2896  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2896 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2896  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2896 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2896  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2896 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2896  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2896 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Text.RegularExpressions.Mark
#include "System_System_Text_RegularExpressions_Mark.h"
void* RuntimeInvoker_Mark_t1479_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1479  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1479  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Mark_t1479 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1479  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1479 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Mark_t1479 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1479  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1479 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Mark_t1479 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1479  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1479 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Mark_t1479 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1479  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1479 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"
void* RuntimeInvoker_UriScheme_t1516_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1516  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1516  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UriScheme_t1516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1516  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1516 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UriScheme_t1516 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1516  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1516 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UriScheme_t1516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1516  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1516 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_UriScheme_t1516 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1516  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1516 *)args[1]), method);
	return NULL;
}

struct Object_t;
// Mono.Globalization.Unicode.CodePointIndexer/TableRange
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
void* RuntimeInvoker_TableRange_t1580_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1580  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1580  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_TableRange_t1580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1580  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1580 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_TableRange_t1580 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1580  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1580 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_TableRange_t1580 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1580  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1580 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_TableRange_t1580 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1580  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1580 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Hashtable/Slot
#include "mscorlib_System_Collections_Hashtable_Slot.h"
void* RuntimeInvoker_Slot_t1657_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1657  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1657  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Slot_t1657 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1657  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1657 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Slot_t1657 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1657  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Slot_t1657 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1657  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1657 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Slot_t1657 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1657  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1657 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.SortedList/Slot
#include "mscorlib_System_Collections_SortedList_Slot.h"
void* RuntimeInvoker_Slot_t1664_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1664  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1664  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Slot_t1664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1664  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1664 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Slot_t1664 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1664  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Slot_t1664 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1664  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Slot_t1664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1664  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1664 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILTokenInfo
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
void* RuntimeInvoker_ILTokenInfo_t1742_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1742  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1742  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_ILTokenInfo_t1742 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1742  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1742 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_ILTokenInfo_t1742 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1742  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1742 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_ILTokenInfo_t1742 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1742  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1742 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_ILTokenInfo_t1742 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1742  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1742 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelData
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
void* RuntimeInvoker_LabelData_t1744_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1744  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1744  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_LabelData_t1744 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1744  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1744 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_LabelData_t1744 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1744  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1744 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_LabelData_t1744 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1744  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1744 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_LabelData_t1744 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1744  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1744 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Reflection.Emit.ILGenerator/LabelFixup
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
void* RuntimeInvoker_LabelFixup_t1743_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1743  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1743  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_LabelFixup_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1743  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1743 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_LabelFixup_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1743  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1743 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_LabelFixup_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1743  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1743 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_LabelFixup_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1743  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1743 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_DateTime_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t871  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t871 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1133  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t1133 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Decimal_t1133 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t1133  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t1133 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_TimeSpan_t1437_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t1437  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t1437  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_TimeSpan_t1437 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t1437  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t1437 *)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
void* RuntimeInvoker_TypeTag_t1930_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_Byte_t1117 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
void* RuntimeInvoker_Enumerator_t2249 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2249  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2249  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_RaycastResult_t511_RaycastResult_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t511  p1, RaycastResult_t511  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), *((RaycastResult_t511 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t511_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t511  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastResult_t511_RaycastResult_t511_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t511  p1, RaycastResult_t511  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t511 *)args[0]), *((RaycastResult_t511 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Queue`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen.h"
void* RuntimeInvoker_Enumerator_t2284 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2284  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2284  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Char>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"
void* RuntimeInvoker_Enumerator_t2289 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2289  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2289  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int16_t1136_Int16_t1136 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Int16_t1136_Int16_t1136_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_RaycastHit_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t482  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t482  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2328_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2328  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2328  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TweenType_t442_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TweenType_t442_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
void* RuntimeInvoker_Enumerator_t2333 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2333  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2333  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2328 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2328  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2328  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TweenType_t442 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_2.h"
void* RuntimeInvoker_Enumerator_t2332 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2332  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2332  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<iTweenEvent/TweenType,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_6.h"
void* RuntimeInvoker_Enumerator_t2336 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2336  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2336  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2328_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2328  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2328  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2350_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2350  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2350  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2364_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2364  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2364  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
void* RuntimeInvoker_Enumerator_t2368 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2368  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2368  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2364 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2364  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2364  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_7.h"
void* RuntimeInvoker_Enumerator_t2367 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2367  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2367  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11.h"
void* RuntimeInvoker_Enumerator_t2371 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2371  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2371  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2364_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2364  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2364  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Nullable`1<UnityEngine.Vector3>
#include "mscorlib_System_Nullable_1_gen.h"
void* RuntimeInvoker_Boolean_t536_Nullable_1_t550 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t550  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t550 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Space_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_EaseType_t425 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LoopType_t426 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"
void* RuntimeInvoker_Enumerator_t2392 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2392  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2392  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Single>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"
void* RuntimeInvoker_Enumerator_t2400 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2400  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2400  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Single_t531_Single_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Single_t531_Single_t531_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<System.Boolean>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"
void* RuntimeInvoker_Enumerator_t2411 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2411  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2411  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_SByte_t1135_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_SByte_t1135_SByte_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Vector3_t215_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t215  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector3_t215  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"
void* RuntimeInvoker_Enumerator_t2422 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2422  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2422  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t215_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t215  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Vector3_t215_Vector3_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector3_t215_Vector3_t215_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t215  p1, Vector3_t215  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t215 *)args[0]), *((Vector3_t215 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Color>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
void* RuntimeInvoker_Enumerator_t2431 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2431  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2431  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t6  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Color_t6_Color_t6 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color_t6  p1, Color_t6  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), *((Color_t6 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Color_t6_Color_t6_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t6  p1, Color_t6  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t6 *)args[0]), *((Color_t6 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Space_t546_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"
void* RuntimeInvoker_Enumerator_t2440 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2440  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2440  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_EaseType_t425_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<iTween/EaseType>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"
void* RuntimeInvoker_Enumerator_t2449 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2449  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2449  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_LoopType_t426_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<iTween/LoopType>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"
void* RuntimeInvoker_Enumerator_t2458 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2458  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2458  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2514_Int32_t478_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2514  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2514  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_11.h"
void* RuntimeInvoker_Enumerator_t2517 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2517  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2517  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2514_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2514  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2514  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_RaycastHit2D_t789 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t789  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t789  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_RaycastHit_t482_RaycastHit_t482_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t482  p1, RaycastHit_t482  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t482 *)args[0]), *((RaycastHit_t482 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_UIVertex_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t685  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UIVertex_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t685  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UIVertex_t685_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t685  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t685  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27.h"
void* RuntimeInvoker_Enumerator_t2556 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2556  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2556  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UIVertex_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t685  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Void_t1548_Int32_t478_UIVertex_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t685  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t685 *)args[1]), method);
	return NULL;
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t685_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t685  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t685  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UIVertex_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t685  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t685  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UIVertex_t685_UIVertex_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t685  p1, UIVertex_t685  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), *((UIVertex_t685 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t685_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t685  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UIVertex_t685_UIVertex_t685 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t685  p1, UIVertex_t685  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), *((UIVertex_t685 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UIVertex_t685_UIVertex_t685_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t685  p1, UIVertex_t685  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t685 *)args[0]), *((UIVertex_t685 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_ColorTween_t630 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t630  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t630 *)args[0]), method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_UILineInfo_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t809  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t809  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UICharInfo_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t811  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t811  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Vector2_t7_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t7  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t7 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_GcAchievementData_t1020 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t1020  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t1020  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_GcScoreData_t1021 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t1021  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t1021  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ContactPoint_t930 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t930  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint_t930  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Keyframe_t943 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t943  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t943  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UICharInfo_t811_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t811  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t811  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_37.h"
void* RuntimeInvoker_Enumerator_t2685 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2685  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2685  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UICharInfo_t811_UICharInfo_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t811  p1, UICharInfo_t811  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), *((UICharInfo_t811 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t811_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t811  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UICharInfo_t811_UICharInfo_t811 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t811  p1, UICharInfo_t811  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), *((UICharInfo_t811 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UICharInfo_t811_UICharInfo_t811_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t811  p1, UICharInfo_t811  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t811 *)args[0]), *((UICharInfo_t811 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_UILineInfo_t809_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t809  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t809  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_38.h"
void* RuntimeInvoker_Enumerator_t2694 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2694  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2694  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UILineInfo_t809_UILineInfo_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t809  p1, UILineInfo_t809  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), *((UILineInfo_t809 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t809_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t809  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_UILineInfo_t809_UILineInfo_t809 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t809  p1, UILineInfo_t809  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), *((UILineInfo_t809 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UILineInfo_t809_UILineInfo_t809_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t809  p1, UILineInfo_t809  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t809 *)args[0]), *((UILineInfo_t809 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2706_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2706  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	KeyValuePair_2_t2706  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Int64_t1131_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16.h"
void* RuntimeInvoker_Enumerator_t2711 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2711  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2711  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Object_t_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2706 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2706  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2706  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_21.h"
void* RuntimeInvoker_Enumerator_t2710 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2710  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2710  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_Int64_t1131_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_23.h"
void* RuntimeInvoker_Enumerator_t2714 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2714  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2714  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2706_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2706  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2706  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int64_t1131_Int64_t1131 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_UInt64_t1134_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2743_UInt64_t1134_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2743  (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2743  ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_NetworkID_t979_UInt64_t1134_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_UInt64_t1134_ObjectU26_t3528 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_NetworkID_t979_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
void* RuntimeInvoker_Enumerator_t2748 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2748  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2748  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_UInt64_t1134_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2743  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2743  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_24.h"
void* RuntimeInvoker_Enumerator_t2747 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2747  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2747  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_UInt64_t1134_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_26.h"
void* RuntimeInvoker_Enumerator_t2751 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2751  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2751  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2743_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2743  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2743  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_UInt64_t1134_UInt64_t1134 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, uint64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2762 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2762  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2762  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Void_t1548_Object_t_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2350  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2350 *)args[1]), method);
	return NULL;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2762_Object_t_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2762  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2350  p2, const MethodInfo* method);
	KeyValuePair_2_t2762  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2350  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2350 *)args[1]), method);
	return ret;
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2350_Object_t_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2350  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2350  p2, const MethodInfo* method);
	KeyValuePair_2_t2350  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_KeyValuePair_2U26_t3610 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2350 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (KeyValuePair_2_t2350 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
void* RuntimeInvoker_Enumerator_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2791  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2791  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Object_t_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2350  p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_28.h"
void* RuntimeInvoker_Enumerator_t2790 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2790  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2790  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2350_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t2350  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t2350 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_30.h"
void* RuntimeInvoker_Enumerator_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2794  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2794  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2762_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2762  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2762  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2350_KeyValuePair_2_t2350 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2350  p1, KeyValuePair_2_t2350  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2350 *)args[0]), *((KeyValuePair_2_t2350 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ParameterModifier_t1805 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1805  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1805  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_HitInfo_t1039 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t1039  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t1039  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TextEditOp_t1057_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2825_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2825  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2825  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_TextEditOp_t1057_Object_t_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_TextEditOpU26_t3611 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23.h"
void* RuntimeInvoker_Enumerator_t2830 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2830  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2830  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2825 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2825  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2825  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TextEditOp_t1057 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_31.h"
void* RuntimeInvoker_Enumerator_t2829 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2829  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2829  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_33.h"
void* RuntimeInvoker_Enumerator_t2833 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2833  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2833  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2825_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2825  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2825  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ClientCertificateType_t1308 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2877_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t2877  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_Boolean_t536_Object_t_BooleanU26_t3509 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__25.h"
void* RuntimeInvoker_Enumerator_t2881 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2881  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2881  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Object_t_SByte_t1135 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2877 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2877  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_34.h"
void* RuntimeInvoker_Enumerator_t2880 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2880  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2880  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
struct Object_t;
void* RuntimeInvoker_Object_t_Object_t_SByte_t1135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_36.h"
void* RuntimeInvoker_Enumerator_t2884 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2884  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2884  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2877_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2877  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2877  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_X509ChainStatus_t1432 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t1432  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t1432  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2896_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Int32_t478_Int32U26_t3510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
void* RuntimeInvoker_Enumerator_t2900 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2900  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2900  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_DictionaryEntry_t552_Int32_t478_Int32_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t552  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t552  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2896 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_37.h"
void* RuntimeInvoker_Enumerator_t2899 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2899  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2899  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_39.h"
void* RuntimeInvoker_Enumerator_t2903 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2903  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2903  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
struct Object_t;
void* RuntimeInvoker_KeyValuePair_2_t2896_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2896  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2896  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Mark_t1479 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1479  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1479  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_UriScheme_t1516 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1516  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1516  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TableRange_t1580 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1580  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1580  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1657 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1657  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1657  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Slot_t1664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1664  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1664  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_ILTokenInfo_t1742 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1742  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1742  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelData_t1744 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1744  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1744  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_LabelFixup_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1743  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1743  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_TypeTag_t1930 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_DateTimeOffset_t1148_DateTimeOffset_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1148  p1, DateTimeOffset_t1148  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1148 *)args[0]), *((DateTimeOffset_t1148 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_DateTimeOffset_t1148_DateTimeOffset_t1148 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1148  p1, DateTimeOffset_t1148  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1148 *)args[0]), *((DateTimeOffset_t1148 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Nullable_1_t2187 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t2187  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t2187 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Int32_t478_Guid_t555_Guid_t555 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t555  p1, Guid_t555  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t555 *)args[0]), *((Guid_t555 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

struct Object_t;
void* RuntimeInvoker_Boolean_t536_Guid_t555_Guid_t555 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t555  p1, Guid_t555  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t555 *)args[0]), *((Guid_t555 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1660] = 
{
	RuntimeInvoker_Void_t1548,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536,
	RuntimeInvoker_Void_t1548_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_DistortionCorrectionMethod_t228,
	RuntimeInvoker_Void_t1548_Int32_t478,
	RuntimeInvoker_BackButtonModes_t229,
	RuntimeInvoker_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531,
	RuntimeInvoker_Object_t_Int32_t478,
	RuntimeInvoker_Matrix4x4_t242_Int32_t478_Int32_t478,
	RuntimeInvoker_Rect_t225_Int32_t478_Int32_t478,
	RuntimeInvoker_Vector2_t7,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478,
	RuntimeInvoker_Matrix4x4_t242,
	RuntimeInvoker_Quaternion_t261,
	RuntimeInvoker_Vector3_t215,
	RuntimeInvoker_Matrix4x4_t242_Int32_t478,
	RuntimeInvoker_Vector3_t215_Int32_t478,
	RuntimeInvoker_Rect_t225_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Matrix4x4U26_t3505,
	RuntimeInvoker_Rect_t225_Rect_t225,
	RuntimeInvoker_Void_t1548_Object_t_Single_t531_Single_t531,
	RuntimeInvoker_Ray_t465,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_SByte_t1135_Vector3U5BU5DU26_t3506_Vector2U5BU5DU26_t3507,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478_SByte_t1135,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Rect_t225_Object_t,
	RuntimeInvoker_Single_t531_Object_t,
	RuntimeInvoker_Distortion_t251_Single_t531_Single_t531_Single_t531_Int32_t478,
	RuntimeInvoker_Distortion_t251_Distortion_t251_Single_t531_Int32_t478,
	RuntimeInvoker_Single_t531_Single_t531,
	RuntimeInvoker_Boolean_t536_Int32_t478,
	RuntimeInvoker_Vector2_t7_Vector3_t215,
	RuntimeInvoker_Void_t1548_Vector3_t215_Quaternion_t261,
	RuntimeInvoker_Void_t1548_Matrix4x4_t242,
	RuntimeInvoker_Void_t1548_Vector3_t215,
	RuntimeInvoker_Void_t1548_Quaternion_t261,
	RuntimeInvoker_Vector3_t215_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_Matrix4x4U26_t3505_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_DisplayMetrics_t271,
	RuntimeInvoker_Matrix4x4_t242_Single_t531_Single_t531_Single_t531_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t478,
	RuntimeInvoker_Char_t526_Object_t,
	RuntimeInvoker_Object_t_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Object_t,
	RuntimeInvoker_Void_t1548_Single_t531_SByte_t1135,
	RuntimeInvoker_Object_t_Single_t531_SByte_t1135,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531_Vector3_t215,
	RuntimeInvoker_Object_t_Single_t531_Vector3_t215,
	RuntimeInvoker_Object_t_SByte_t1135_Single_t531,
	RuntimeInvoker_Void_t1548_SByte_t1135_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_SByte_t1135_Single_t531_Single_t531_Object_t,
	RuntimeInvoker_Void_t1548_Int32_t478_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Single_t531_Single_t531_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Single_t531_Single_t531_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Single_t531,
	RuntimeInvoker_Void_t1548_Vector3_t215_Single_t531_Single_t531_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Vector3_t215_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531_Vector3_t215_SByte_t1135,
	RuntimeInvoker_Object_t_Single_t531_Vector3_t215_SByte_t1135,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531,
	RuntimeInvoker_Object_t_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531_Int32_t478,
	RuntimeInvoker_Object_t_Single_t531_Int32_t478,
	RuntimeInvoker_Void_t1548_SByte_t1135_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Object_t,
	RuntimeInvoker_Void_t1548_Vector3_t215_SByte_t1135_SByte_t1135_Vector3_t215,
	RuntimeInvoker_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Color_t6_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Single_t531,
	RuntimeInvoker_Rect_t225_Rect_t225_Rect_t225_Single_t531,
	RuntimeInvoker_Vector3_t215_Vector3_t215_Vector3_t215_Single_t531,
	RuntimeInvoker_Vector2_t7_Vector2_t7_Vector2_t7_Single_t531,
	RuntimeInvoker_Single_t531_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Object_t_Color_t6,
	RuntimeInvoker_Vector3_t215_Object_t_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_Color_t6,
	RuntimeInvoker_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Color_t6_Object_t,
	RuntimeInvoker_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Vector3_t215_Single_t531,
	RuntimeInvoker_Object_t_Single_t531_Single_t531_Single_t531_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_RaycastResult_t511_RaycastResult_t511,
	RuntimeInvoker_Void_t1548_Vector2_t7,
	RuntimeInvoker_MoveDirection_t608,
	RuntimeInvoker_RaycastResult_t511,
	RuntimeInvoker_Void_t1548_RaycastResult_t511,
	RuntimeInvoker_InputButton_t613,
	RuntimeInvoker_RaycastResult_t511_Object_t,
	RuntimeInvoker_MoveDirection_t608_Single_t531_Single_t531,
	RuntimeInvoker_MoveDirection_t608_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Object_t_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Boolean_t536_Int32_t478_PointerEventDataU26_t3508_SByte_t1135,
	RuntimeInvoker_Object_t_Touch_t768_BooleanU26_t3509_BooleanU26_t3509,
	RuntimeInvoker_FramePressState_t614_Int32_t478,
	RuntimeInvoker_Boolean_t536_Vector2_t7_Vector2_t7_Single_t531_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_InputMode_t621,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_SByte_t1135,
	RuntimeInvoker_LayerMask_t241,
	RuntimeInvoker_Void_t1548_LayerMask_t241,
	RuntimeInvoker_Int32_t478_RaycastHit_t482_RaycastHit_t482,
	RuntimeInvoker_Color_t6,
	RuntimeInvoker_Void_t1548_Color_t6,
	RuntimeInvoker_ColorTweenMode_t627,
	RuntimeInvoker_ColorBlock_t642,
	RuntimeInvoker_FontStyle_t889,
	RuntimeInvoker_TextAnchor_t819,
	RuntimeInvoker_HorizontalWrapMode_t953,
	RuntimeInvoker_VerticalWrapMode_t954,
	RuntimeInvoker_Boolean_t536_Vector2_t7_Object_t,
	RuntimeInvoker_Vector2_t7_Vector2_t7,
	RuntimeInvoker_Rect_t225,
	RuntimeInvoker_Void_t1548_Color_t6_Single_t531_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Color_t6_Single_t531_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Color_t6_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_SByte_t1135,
	RuntimeInvoker_BlockingObjects_t655,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Vector2_t7_Object_t,
	RuntimeInvoker_Type_t661,
	RuntimeInvoker_FillMethod_t662,
	RuntimeInvoker_Vector4_t5_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_UIVertex_t685_Vector2_t7_Vector2_t7_Vector2_t7_Vector2_t7,
	RuntimeInvoker_Vector4_t5_Vector4_t5_Rect_t225,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Single_t531_SByte_t1135_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Single_t531_Single_t531_SByte_t1135_Int32_t478,
	RuntimeInvoker_Vector2_t7_Vector2_t7_Rect_t225,
	RuntimeInvoker_ContentType_t671,
	RuntimeInvoker_LineType_t674,
	RuntimeInvoker_InputType_t672,
	RuntimeInvoker_TouchScreenKeyboardType_t805,
	RuntimeInvoker_CharacterValidation_t673,
	RuntimeInvoker_Char_t526,
	RuntimeInvoker_Void_t1548_Int16_t1136,
	RuntimeInvoker_Void_t1548_Int32U26_t3510,
	RuntimeInvoker_Int32_t478_Vector2_t7_Object_t,
	RuntimeInvoker_Int32_t478_Vector2_t7,
	RuntimeInvoker_EditState_t678_Object_t,
	RuntimeInvoker_Boolean_t536_Int16_t1136,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Int32_t478_Int32_t478_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_Void_t1548_Object_t_Vector2_t7,
	RuntimeInvoker_Single_t531_Int32_t478_Object_t,
	RuntimeInvoker_Char_t526_Object_t_Int32_t478_Int16_t1136,
	RuntimeInvoker_Void_t1548_Int32_t478_SByte_t1135,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Int16_t1136_Object_t_Object_t,
	RuntimeInvoker_Mode_t689,
	RuntimeInvoker_Navigation_t690,
	RuntimeInvoker_Void_t1548_Rect_t225,
	RuntimeInvoker_Direction_t692,
	RuntimeInvoker_Axis_t695,
	RuntimeInvoker_MovementType_t699,
	RuntimeInvoker_Bounds_t703,
	RuntimeInvoker_Void_t1548_Navigation_t690,
	RuntimeInvoker_Transition_t704,
	RuntimeInvoker_Void_t1548_ColorBlock_t642,
	RuntimeInvoker_SpriteState_t708,
	RuntimeInvoker_Void_t1548_SpriteState_t708,
	RuntimeInvoker_SelectionState_t705,
	RuntimeInvoker_Object_t_Vector3_t215,
	RuntimeInvoker_Vector3_t215_Object_t_Vector2_t7,
	RuntimeInvoker_Void_t1548_Color_t6_SByte_t1135,
	RuntimeInvoker_Boolean_t536_ColorU26_t3511_Color_t6,
	RuntimeInvoker_Direction_t710,
	RuntimeInvoker_Axis_t712,
	RuntimeInvoker_TextGenerationSettings_t773_Vector2_t7,
	RuntimeInvoker_Vector2_t7_Int32_t478,
	RuntimeInvoker_AspectMode_t725,
	RuntimeInvoker_Single_t531_Single_t531_Int32_t478,
	RuntimeInvoker_ScaleMode_t727,
	RuntimeInvoker_ScreenMatchMode_t728,
	RuntimeInvoker_Unit_t729,
	RuntimeInvoker_FitMode_t731,
	RuntimeInvoker_Corner_t733,
	RuntimeInvoker_Axis_t734,
	RuntimeInvoker_Constraint_t735,
	RuntimeInvoker_Single_t531_Int32_t478,
	RuntimeInvoker_Single_t531_Int32_t478_Single_t531,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Boolean_t536_LayoutRebuilder_t745,
	RuntimeInvoker_Single_t531_Object_t_Int32_t478,
	RuntimeInvoker_Single_t531_Object_t_Object_t_Single_t531,
	RuntimeInvoker_Single_t531_Object_t_Object_t_Single_t531_ILayoutElementU26_t3512,
	RuntimeInvoker_Void_t1548_Object_t_Color32_t778_Int32_t478_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Int32_t478_LayerMask_t241,
	RuntimeInvoker_LayerMask_t241_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Double_t553,
	RuntimeInvoker_Void_t1548_Int64_t1131_Object_t,
	RuntimeInvoker_Void_t1548_GcAchievementDescriptionData_t1019_Int32_t478,
	RuntimeInvoker_Void_t1548_GcUserProfileData_t1018_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Double_t553_Object_t,
	RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_UserProfileU5BU5DU26_t3513_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_UserProfileU5BU5DU26_t3513_Int32_t478,
	RuntimeInvoker_Void_t1548_GcScoreData_t1021,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_ColorSpace_t1025,
	RuntimeInvoker_Boolean_t536_BoneWeight_t854_BoneWeight_t854,
	RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Quaternion_t261,
	RuntimeInvoker_Void_t1548_Object_t_Vector3_t215_Quaternion_t261_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514_QuaternionU26_t3515_Int32_t478,
	RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Rect_t225_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Void_t1548_InternalDrawTextureArgumentsU26_t3516,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Color_t6,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Color_t6_Single_t531,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_ColorU26_t3511_Single_t531,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_IntPtr_t,
	RuntimeInvoker_Color_t6_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Rect_t225_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_RectU26_t3517_Int32_t478_Int32_t478_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_ColorU26_t3511,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t3514,
	RuntimeInvoker_Void_t1548_Color_t6_Single_t531,
	RuntimeInvoker_Void_t1548_DateTime_t871,
	RuntimeInvoker_Void_t1548_Rect_t225_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1548_RectU26_t3517_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Rect_t225_Object_t_Int32_t478_SByte_t1135_Single_t531,
	RuntimeInvoker_Boolean_t536_Rect_t225_Object_t,
	RuntimeInvoker_Boolean_t536_Rect_t225_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Rect_t225_Object_t_IntPtr_t,
	RuntimeInvoker_Boolean_t536_RectU26_t3517_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_Int32_t478_Single_t531_Single_t531_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Int32_t478_Rect_t225,
	RuntimeInvoker_Void_t1548_Int32_t478_RectU26_t3517,
	RuntimeInvoker_Rect_t225_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531_Object_t,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531_Object_t_Object_t,
	RuntimeInvoker_Rect_t225_Object_t_RectU26_t3517,
	RuntimeInvoker_IntPtr_t_Int32_t478,
	RuntimeInvoker_ImagePosition_t890,
	RuntimeInvoker_Single_t531_IntPtr_t,
	RuntimeInvoker_Vector2_t7_Rect_t225_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_IntPtr_t_Rect_t225_Object_t_Int32_t478_Vector2U26_t3518,
	RuntimeInvoker_Void_t1548_IntPtr_t_RectU26_t3517_Object_t_Int32_t478_Vector2U26_t3518,
	RuntimeInvoker_Vector2_t7_Object_t,
	RuntimeInvoker_Void_t1548_IntPtr_t_Object_t_Vector2U26_t3518,
	RuntimeInvoker_Single_t531_Object_t_Single_t531,
	RuntimeInvoker_Single_t531_IntPtr_t_Object_t_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_SingleU26_t3519_SingleU26_t3519,
	RuntimeInvoker_Void_t1548_IntPtr_t_Object_t_SingleU26_t3519_SingleU26_t3519,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Object_t,
	RuntimeInvoker_Void_t1548_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t3520_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Object_t,
	RuntimeInvoker_EventType_t893,
	RuntimeInvoker_Void_t1548_Vector2U26_t3518,
	RuntimeInvoker_EventModifiers_t894,
	RuntimeInvoker_KeyCode_t892,
	RuntimeInvoker_Void_t1548_IntPtr_t,
	RuntimeInvoker_Void_t1548_Vector3_t215_Vector3_t215,
	RuntimeInvoker_Void_t1548_Vector3U26_t3514_Vector3U26_t3514,
	RuntimeInvoker_Void_t1548_Vector3_t215_Object_t,
	RuntimeInvoker_Void_t1548_Vector3U26_t3514_Object_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int32_t478_Single_t531,
	RuntimeInvoker_Vector2_t7_Vector2_t7_Vector2_t7,
	RuntimeInvoker_Single_t531_Vector2_t7_Vector2_t7,
	RuntimeInvoker_Single_t531_Vector2_t7,
	RuntimeInvoker_Vector2_t7_Vector2_t7_Single_t531,
	RuntimeInvoker_Boolean_t536_Vector2_t7_Vector2_t7,
	RuntimeInvoker_Vector3_t215_Vector2_t7,
	RuntimeInvoker_Vector3_t215_Vector3_t215_Vector3_t215,
	RuntimeInvoker_Vector3_t215_Vector3_t215,
	RuntimeInvoker_Single_t531_Vector3_t215_Vector3_t215,
	RuntimeInvoker_Single_t531_Vector3_t215,
	RuntimeInvoker_Vector3_t215_Vector3_t215_Single_t531,
	RuntimeInvoker_Vector3_t215_Single_t531_Vector3_t215,
	RuntimeInvoker_Boolean_t536_Vector3_t215_Vector3_t215,
	RuntimeInvoker_Color_t6_Color_t6_Color_t6_Single_t531,
	RuntimeInvoker_Color_t6_Color_t6_Single_t531,
	RuntimeInvoker_Vector4_t5_Color_t6,
	RuntimeInvoker_Color_t6_Vector4_t5,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Color32_t778_Color_t6,
	RuntimeInvoker_Color_t6_Color32_t778,
	RuntimeInvoker_Single_t531_Quaternion_t261_Quaternion_t261,
	RuntimeInvoker_Quaternion_t261_Vector3_t215_Vector3_t215,
	RuntimeInvoker_Quaternion_t261_Vector3U26_t3514_Vector3U26_t3514,
	RuntimeInvoker_Quaternion_t261_Quaternion_t261_Quaternion_t261_Single_t531,
	RuntimeInvoker_Quaternion_t261_QuaternionU26_t3515_QuaternionU26_t3515_Single_t531,
	RuntimeInvoker_Quaternion_t261_Quaternion_t261,
	RuntimeInvoker_Quaternion_t261_QuaternionU26_t3515,
	RuntimeInvoker_Quaternion_t261_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Quaternion_t261_Vector3_t215,
	RuntimeInvoker_Vector3_t215_Quaternion_t261,
	RuntimeInvoker_Vector3_t215_QuaternionU26_t3515,
	RuntimeInvoker_Quaternion_t261_Vector3U26_t3514,
	RuntimeInvoker_Quaternion_t261_Quaternion_t261_Quaternion_t261,
	RuntimeInvoker_Vector3_t215_Quaternion_t261_Vector3_t215,
	RuntimeInvoker_Boolean_t536_Quaternion_t261_Quaternion_t261,
	RuntimeInvoker_Boolean_t536_Vector3_t215,
	RuntimeInvoker_Boolean_t536_Rect_t225_Rect_t225,
	RuntimeInvoker_Single_t531_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Single_t531,
	RuntimeInvoker_Matrix4x4_t242_Matrix4x4_t242,
	RuntimeInvoker_Matrix4x4_t242_Matrix4x4U26_t3505,
	RuntimeInvoker_Boolean_t536_Matrix4x4_t242_Matrix4x4U26_t3505,
	RuntimeInvoker_Boolean_t536_Matrix4x4U26_t3505_Matrix4x4U26_t3505,
	RuntimeInvoker_Vector4_t5_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Vector4_t5,
	RuntimeInvoker_Matrix4x4_t242_Vector3_t215,
	RuntimeInvoker_Void_t1548_Vector3_t215_Quaternion_t261_Vector3_t215,
	RuntimeInvoker_Matrix4x4_t242_Vector3_t215_Quaternion_t261_Vector3_t215,
	RuntimeInvoker_Matrix4x4_t242_Vector3U26_t3514_QuaternionU26_t3515_Vector3U26_t3514,
	RuntimeInvoker_Matrix4x4_t242_Single_t531_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Matrix4x4_t242_Matrix4x4_t242_Matrix4x4_t242,
	RuntimeInvoker_Vector4_t5_Matrix4x4_t242_Vector4_t5,
	RuntimeInvoker_Boolean_t536_Matrix4x4_t242_Matrix4x4_t242,
	RuntimeInvoker_Void_t1548_Bounds_t703,
	RuntimeInvoker_Boolean_t536_Bounds_t703,
	RuntimeInvoker_Boolean_t536_Bounds_t703_Vector3_t215,
	RuntimeInvoker_Boolean_t536_BoundsU26_t3521_Vector3U26_t3514,
	RuntimeInvoker_Single_t531_Bounds_t703_Vector3_t215,
	RuntimeInvoker_Single_t531_BoundsU26_t3521_Vector3U26_t3514,
	RuntimeInvoker_Boolean_t536_RayU26_t3522_BoundsU26_t3521_SingleU26_t3519,
	RuntimeInvoker_Boolean_t536_Ray_t465,
	RuntimeInvoker_Boolean_t536_Ray_t465_SingleU26_t3519,
	RuntimeInvoker_Vector3_t215_BoundsU26_t3521_Vector3U26_t3514,
	RuntimeInvoker_Boolean_t536_Bounds_t703_Bounds_t703,
	RuntimeInvoker_Single_t531_Vector4_t5_Vector4_t5,
	RuntimeInvoker_Single_t531_Vector4_t5,
	RuntimeInvoker_Vector4_t5,
	RuntimeInvoker_Vector4_t5_Vector4_t5_Vector4_t5,
	RuntimeInvoker_Vector4_t5_Vector4_t5_Single_t531,
	RuntimeInvoker_Boolean_t536_Vector4_t5_Vector4_t5,
	RuntimeInvoker_Vector3_t215_Vector4_t5,
	RuntimeInvoker_Vector4_t5_Vector2_t7,
	RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Single_t531,
	RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Single_t531_Single_t531,
	RuntimeInvoker_Single_t531_Single_t531_Single_t531_SingleU26_t3519_Single_t531,
	RuntimeInvoker_Single_t531_Single_t531_Single_t531_SingleU26_t3519_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_RectU26_t3517,
	RuntimeInvoker_Void_t1548_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Void_t1548_Int32_t478_Color_t6,
	RuntimeInvoker_Void_t1548_Int32_t478_ColorU26_t3511,
	RuntimeInvoker_Void_t1548_Object_t_Vector4_t5,
	RuntimeInvoker_Void_t1548_Object_t_Matrix4x4_t242,
	RuntimeInvoker_Void_t1548_Int32_t478_Matrix4x4_t242,
	RuntimeInvoker_Void_t1548_Int32_t478_Matrix4x4U26_t3505,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_ColorU26_t3511,
	RuntimeInvoker_Color_t6_Object_t,
	RuntimeInvoker_Color_t6_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Vector2U26_t3518,
	RuntimeInvoker_Void_t1548_SphericalHarmonicsL2U26_t3523,
	RuntimeInvoker_Void_t1548_Color_t6_SphericalHarmonicsL2U26_t3523,
	RuntimeInvoker_Void_t1548_ColorU26_t3511_SphericalHarmonicsL2U26_t3523,
	RuntimeInvoker_Void_t1548_Vector3_t215_Color_t6_Single_t531,
	RuntimeInvoker_Void_t1548_Vector3_t215_Color_t6_SphericalHarmonicsL2U26_t3523,
	RuntimeInvoker_Void_t1548_Vector3U26_t3514_ColorU26_t3511_SphericalHarmonicsL2U26_t3523,
	RuntimeInvoker_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905_Single_t531,
	RuntimeInvoker_SphericalHarmonicsL2_t905_Single_t531_SphericalHarmonicsL2_t905,
	RuntimeInvoker_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905,
	RuntimeInvoker_Boolean_t536_SphericalHarmonicsL2_t905_SphericalHarmonicsL2_t905,
	RuntimeInvoker_Void_t1548_Vector4U26_t3524,
	RuntimeInvoker_Vector4_t5_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Vector2U26_t3518,
	RuntimeInvoker_Object_t_SByte_t1135,
	RuntimeInvoker_Object_t_SByte_t1135_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135_Object_t_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135,
	RuntimeInvoker_RuntimePlatform_t833,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_CameraClearFlags_t1024,
	RuntimeInvoker_Vector3_t215_Object_t_Vector3U26_t3514,
	RuntimeInvoker_Ray_t465_Vector3_t215,
	RuntimeInvoker_Ray_t465_Object_t_Vector3U26_t3514,
	RuntimeInvoker_Object_t_Ray_t465_Single_t531_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_RayU26_t3522_Single_t531_Int32_t478,
	RuntimeInvoker_RenderBuffer_t1023,
	RuntimeInvoker_Void_t1548_IntPtr_t_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_Void_t1548_IntPtr_t_RenderBufferU26_t3525_RenderBufferU26_t3525,
	RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_TouchPhase_t919,
	RuntimeInvoker_Void_t1548_Vector3U26_t3514,
	RuntimeInvoker_Touch_t768_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Object_t,
	RuntimeInvoker_Void_t1548_QuaternionU26_t3515,
	RuntimeInvoker_Void_t1548_Vector3_t215_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Vector3_t215,
	RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514_Vector3U26_t3514,
	RuntimeInvoker_Boolean_t536_Vector3_t215_Vector3_t215_RaycastHitU26_t3526_Single_t531_Int32_t478,
	RuntimeInvoker_Boolean_t536_Vector3U26_t3514_Vector3U26_t3514_RaycastHitU26_t3526_Single_t531_Int32_t478,
	RuntimeInvoker_Boolean_t536_Vector3_t215_Vector3_t215_Single_t531_Vector3_t215_RaycastHitU26_t3526_Single_t531_Int32_t478,
	RuntimeInvoker_Boolean_t536_Vector3U26_t3514_Vector3U26_t3514_Single_t531_Vector3U26_t3514_RaycastHitU26_t3526_Single_t531_Int32_t478,
	RuntimeInvoker_Boolean_t536_Ray_t465_RaycastHitU26_t3526,
	RuntimeInvoker_Boolean_t536_Ray_t465_RaycastHitU26_t3526_Single_t531_Int32_t478,
	RuntimeInvoker_Object_t_Ray_t465,
	RuntimeInvoker_Object_t_Vector3_t215_Vector3_t215_Single_t531_Int32_t478,
	RuntimeInvoker_Object_t_Vector3U26_t3514_Vector3U26_t3514_Single_t531_Int32_t478,
	RuntimeInvoker_Boolean_t536_Ray_t465_Single_t531_Single_t531_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Vector3U26_t3514,
	RuntimeInvoker_Void_t1548_Object_t_QuaternionU26_t3515,
	RuntimeInvoker_Boolean_t536_Object_t_Ray_t465_RaycastHitU26_t3526_Single_t531,
	RuntimeInvoker_Boolean_t536_Object_t_RayU26_t3522_RaycastHitU26_t3526_Single_t531,
	RuntimeInvoker_Boolean_t536_Ray_t465_RaycastHitU26_t3526_Single_t531,
	RuntimeInvoker_Void_t1548_Vector2U26_t3518_Object_t_Vector2_t7_Vector3_t215_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Vector2U26_t3518_Object_t_Vector2U26_t3518_Vector3U26_t3514_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Vector2_t7_Vector2_t7_Single_t531_Int32_t478_Single_t531_Single_t531_RaycastHit2DU26_t3527,
	RuntimeInvoker_Void_t1548_Vector2U26_t3518_Vector2U26_t3518_Single_t531_Int32_t478_Single_t531_Single_t531_RaycastHit2DU26_t3527,
	RuntimeInvoker_RaycastHit2D_t789_Vector2_t7_Vector2_t7_Single_t531_Int32_t478,
	RuntimeInvoker_RaycastHit2D_t789_Vector2_t7_Vector2_t7_Single_t531_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Object_t_Vector2_t7_Vector2_t7_Single_t531_Int32_t478,
	RuntimeInvoker_Object_t_Vector2U26_t3518_Vector2U26_t3518_Single_t531_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Boolean_t536_Object_t_Vector3U26_t3514,
	RuntimeInvoker_Object_t_SByte_t1135_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Int64_t1131,
	RuntimeInvoker_SendMessageOptions_t832,
	RuntimeInvoker_AnimatorStateInfo_t542,
	RuntimeInvoker_AnimatorClipInfo_t942,
	RuntimeInvoker_Void_t1548_Int32_t478_Vector3_t215,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Vector3U26_t3514,
	RuntimeInvoker_Void_t1548_Single_t531_Single_t531_Single_t531_Single_t531_Single_t531,
	RuntimeInvoker_AnimatorStateInfo_t542_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Color_t6_Int32_t478_Single_t531_Single_t531_Int32_t478_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_Vector2_t7_Vector2_t7_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Color_t6_Int32_t478_Single_t531_Single_t531_Int32_t478_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_Single_t531_Single_t531_Single_t531_Single_t531_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_ColorU26_t3511_Int32_t478_Single_t531_Single_t531_Int32_t478_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_Single_t531_Single_t531_Single_t531_Single_t531_SByte_t1135,
	RuntimeInvoker_TextGenerationSettings_t773_TextGenerationSettings_t773,
	RuntimeInvoker_Single_t531_Object_t_TextGenerationSettings_t773,
	RuntimeInvoker_Boolean_t536_Object_t_TextGenerationSettings_t773,
	RuntimeInvoker_RenderMode_t959,
	RuntimeInvoker_Void_t1548_Object_t_ColorU26_t3511,
	RuntimeInvoker_Boolean_t536_Object_t_Vector2_t7_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Vector2U26_t3518_Object_t,
	RuntimeInvoker_Vector2_t7_Vector2_t7_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Vector2_t7_Object_t_Object_t_Vector2U26_t3518,
	RuntimeInvoker_Void_t1548_Vector2U26_t3518_Object_t_Object_t_Vector2U26_t3518,
	RuntimeInvoker_Rect_t225_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Vector2_t7_Object_t_Vector3U26_t3514,
	RuntimeInvoker_Boolean_t536_Object_t_Vector2_t7_Object_t_Vector2U26_t3518,
	RuntimeInvoker_Ray_t465_Object_t_Vector2_t7,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_SourceID_t978,
	RuntimeInvoker_AppID_t977,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt16_t1122_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt64_t1134_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt32_t1124,
	RuntimeInvoker_NetworkID_t979,
	RuntimeInvoker_Void_t1548_UInt64_t1134,
	RuntimeInvoker_NodeID_t980,
	RuntimeInvoker_Void_t1548_UInt16_t1122,
	RuntimeInvoker_Object_t_UInt64_t1134,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_Object_t_Object_t,
	RuntimeInvoker_Object_t_UInt64_t1134_Object_t_Object_t,
	RuntimeInvoker_Object_t_UInt64_t1134_Object_t,
	RuntimeInvoker_Object_t_UInt64_t1134_UInt16_t1122_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_ObjectU26_t3528,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t446,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t446,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t3510_BooleanU26_t3509,
	RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510,
	RuntimeInvoker_Int32_t478_Object_t_Int32U26_t3510,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_Int32_t478_Object_t,
	RuntimeInvoker_UserState_t1043,
	RuntimeInvoker_Void_t1548_Object_t_Double_t553_SByte_t1135_SByte_t1135_DateTime_t871,
	RuntimeInvoker_Double_t553,
	RuntimeInvoker_Void_t1548_Double_t553,
	RuntimeInvoker_DateTime_t871,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1135_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int64_t1131,
	RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Object_t_DateTime_t871_Object_t_Int32_t478,
	RuntimeInvoker_Int64_t1131,
	RuntimeInvoker_UserScope_t1044,
	RuntimeInvoker_Range_t1038,
	RuntimeInvoker_Void_t1548_Range_t1038,
	RuntimeInvoker_TimeScope_t1045,
	RuntimeInvoker_Void_t1548_Int32_t478_HitInfo_t1039,
	RuntimeInvoker_Boolean_t536_HitInfo_t1039_HitInfo_t1039,
	RuntimeInvoker_Boolean_t536_HitInfo_t1039,
	RuntimeInvoker_Void_t1548_Object_t_StringU26_t3529_StringU26_t3529,
	RuntimeInvoker_Void_t1548_Object_t_StreamingContext_t1105,
	RuntimeInvoker_Void_t1548_Object_t_AnimatorStateInfo_t542_Int32_t478,
	RuntimeInvoker_Boolean_t536_Color_t6_Color_t6,
	RuntimeInvoker_Boolean_t536_TextGenerationSettings_t773,
	RuntimeInvoker_PersistentListenerMode_t1059,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_Object_t_Object_t,
	RuntimeInvoker_UInt32_t1124_Int32_t478,
	RuntimeInvoker_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_UInt32_t1124_Object_t_Int32_t478,
	RuntimeInvoker_Sign_t1198_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_ConfidenceFactor_t1202,
	RuntimeInvoker_Void_t1548_SByte_t1135_Object_t,
	RuntimeInvoker_Byte_t1117,
	RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_ByteU26_t3530_Int32U26_t3510_ByteU5BU5DU26_t3531,
	RuntimeInvoker_DateTime_t871_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t1334,
	RuntimeInvoker_RSAParameters_t1307_SByte_t1135,
	RuntimeInvoker_Void_t1548_RSAParameters_t1307,
	RuntimeInvoker_DSAParameters_t1334_BooleanU26_t3509,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_Object_t_SByte_t1135,
	RuntimeInvoker_Boolean_t536_DateTime_t871,
	RuntimeInvoker_X509ChainStatusFlags_t1238,
	RuntimeInvoker_Void_t1548_Byte_t1117,
	RuntimeInvoker_Void_t1548_Byte_t1117_Byte_t1117,
	RuntimeInvoker_AlertLevel_t1257,
	RuntimeInvoker_AlertDescription_t1258,
	RuntimeInvoker_Object_t_Byte_t1117,
	RuntimeInvoker_Void_t1548_Int16_t1136_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Int16_t1136_SByte_t1135_SByte_t1135,
	RuntimeInvoker_CipherAlgorithmType_t1260,
	RuntimeInvoker_HashAlgorithmType_t1278,
	RuntimeInvoker_ExchangeAlgorithmType_t1276,
	RuntimeInvoker_CipherMode_t1190,
	RuntimeInvoker_Int16_t1136,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int16_t1136,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int64_t1131,
	RuntimeInvoker_Void_t1548_Object_t_ByteU5BU5DU26_t3531_ByteU5BU5DU26_t3531,
	RuntimeInvoker_Object_t_Byte_t1117_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Object_t_Int16_t1136,
	RuntimeInvoker_Int32_t478_Int16_t1136,
	RuntimeInvoker_Object_t_Int16_t1136_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_Int16_t1136_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t1293,
	RuntimeInvoker_SecurityCompressionType_t1292,
	RuntimeInvoker_HandshakeType_t1309,
	RuntimeInvoker_HandshakeState_t1277,
	RuntimeInvoker_UInt64_t1134,
	RuntimeInvoker_SecurityProtocolType_t1293_Int16_t1136,
	RuntimeInvoker_Object_t_Byte_t1117_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t1117_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Byte_t1117_Object_t,
	RuntimeInvoker_Object_t_Byte_t1117_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Int64_t1131_Int64_t1131_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Byte_t1117_Byte_t1117_Object_t,
	RuntimeInvoker_RSAParameters_t1307,
	RuntimeInvoker_Void_t1548_Object_t_Byte_t1117,
	RuntimeInvoker_Void_t1548_Object_t_Byte_t1117_Byte_t1117,
	RuntimeInvoker_Void_t1548_Object_t_Byte_t1117_Object_t,
	RuntimeInvoker_ContentType_t1271,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t3532,
	RuntimeInvoker_DictionaryEntry_t552,
	RuntimeInvoker_EditorBrowsableState_t1391,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Int16_t1136_Int16_t1136,
	RuntimeInvoker_Boolean_t536_Object_t_IPAddressU26_t3533,
	RuntimeInvoker_AddressFamily_t1396,
	RuntimeInvoker_Object_t_Int64_t1131,
	RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510,
	RuntimeInvoker_Boolean_t536_Object_t_IPv6AddressU26_t3534,
	RuntimeInvoker_UInt16_t1122_Int16_t1136,
	RuntimeInvoker_SecurityProtocolType_t1410,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Int32_t478_SByte_t1135,
	RuntimeInvoker_AsnDecodeStatus_t1450_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_SByte_t1135,
	RuntimeInvoker_X509ChainStatusFlags_t1438_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t1438_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_X509ChainStatusFlags_t1438_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_X509ChainStatusFlags_t1438,
	RuntimeInvoker_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_Int32_t478_Int32_t478,
	RuntimeInvoker_X509RevocationFlag_t1445,
	RuntimeInvoker_X509RevocationMode_t1446,
	RuntimeInvoker_X509VerificationFlags_t1449,
	RuntimeInvoker_X509KeyUsageFlags_t1443,
	RuntimeInvoker_X509KeyUsageFlags_t1443_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Byte_t1117_Int16_t1136,
	RuntimeInvoker_Byte_t1117_Int16_t1136_Int16_t1136,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_RegexOptions_t1464,
	RuntimeInvoker_Category_t1471_Object_t,
	RuntimeInvoker_Boolean_t536_UInt16_t1122_Int16_t1136,
	RuntimeInvoker_Boolean_t536_Int32_t478_Int16_t1136,
	RuntimeInvoker_Void_t1548_Int16_t1136_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_UInt16_t1122_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int16_t1136_Int16_t1136_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int16_t1136_Object_t_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_SByte_t1135_Object_t,
	RuntimeInvoker_Void_t1548_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_SByte_t1135_Int32_t478_Object_t,
	RuntimeInvoker_UInt16_t1122_UInt16_t1122_UInt16_t1122,
	RuntimeInvoker_OpFlags_t1466_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_UInt16_t1122_UInt16_t1122,
	RuntimeInvoker_Boolean_t536_Int32_t478_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Boolean_t536_Int32_t478_Int32U26_t3510_Int32U26_t3510_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Boolean_t536_UInt16_t1122_Int32_t478,
	RuntimeInvoker_Boolean_t536_Int32_t478_Int32_t478_SByte_t1135_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_SByte_t1135_Int32_t478,
	RuntimeInvoker_Interval_t1486,
	RuntimeInvoker_Boolean_t536_Interval_t1486,
	RuntimeInvoker_Void_t1548_Interval_t1486,
	RuntimeInvoker_Interval_t1486_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Double_t553_Interval_t1486,
	RuntimeInvoker_Object_t_Interval_t1486_Object_t_Object_t,
	RuntimeInvoker_Double_t553_Object_t,
	RuntimeInvoker_Int32_t478_Object_t_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t3510,
	RuntimeInvoker_Object_t_RegexOptionsU26_t3535,
	RuntimeInvoker_Void_t1548_RegexOptionsU26_t3535_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Int32U26_t3510_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Category_t1471,
	RuntimeInvoker_Int32_t478_Int16_t1136_Int32_t478_Int32_t478,
	RuntimeInvoker_Char_t526_Int16_t1136,
	RuntimeInvoker_Int32_t478_Int32U26_t3510,
	RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_UInt16_t1122_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int16_t1136_Int16_t1136,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_UInt16_t1122,
	RuntimeInvoker_Position_t1467,
	RuntimeInvoker_UriHostNameType_t1519_Object_t,
	RuntimeInvoker_Void_t1548_StringU26_t3529,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Char_t526_Object_t_Int32U26_t3510_CharU26_t3536,
	RuntimeInvoker_Void_t1548_Object_t_UriFormatExceptionU26_t3537,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_ObjectU5BU5DU26_t3538,
	RuntimeInvoker_Int32_t478_Object_t_ObjectU5BU5DU26_t3538,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Byte_t1117_Object_t,
	RuntimeInvoker_Decimal_t1133_Object_t,
	RuntimeInvoker_Int16_t1136_Object_t,
	RuntimeInvoker_Int64_t1131_Object_t,
	RuntimeInvoker_SByte_t1135_Object_t,
	RuntimeInvoker_UInt16_t1122_Object_t,
	RuntimeInvoker_UInt32_t1124_Object_t,
	RuntimeInvoker_UInt64_t1134_Object_t,
	RuntimeInvoker_Boolean_t536_SByte_t1135_Object_t_Int32_t478_ExceptionU26_t3539,
	RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_Int32U26_t3510_ExceptionU26_t3539,
	RuntimeInvoker_Boolean_t536_Int32_t478_SByte_t1135_ExceptionU26_t3539,
	RuntimeInvoker_Boolean_t536_Int32U26_t3510_Object_t_SByte_t1135_SByte_t1135_ExceptionU26_t3539,
	RuntimeInvoker_Void_t1548_Int32U26_t3510_Object_t_Object_t_BooleanU26_t3509_BooleanU26_t3509,
	RuntimeInvoker_Void_t1548_Int32U26_t3510_Object_t_Object_t_BooleanU26_t3509,
	RuntimeInvoker_Boolean_t536_Int32U26_t3510_Object_t_Int32U26_t3510_SByte_t1135_ExceptionU26_t3539,
	RuntimeInvoker_Boolean_t536_Int32U26_t3510_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Int16_t1136_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_Int32U26_t3510_ExceptionU26_t3539,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_Int32U26_t3510,
	RuntimeInvoker_TypeCode_t2140,
	RuntimeInvoker_Int32_t478_Int64_t1131,
	RuntimeInvoker_Boolean_t536_Int64_t1131,
	RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_Int64U26_t3540_ExceptionU26_t3539,
	RuntimeInvoker_Int64_t1131_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_Int64U26_t3540_ExceptionU26_t3539,
	RuntimeInvoker_Int64_t1131_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Int64U26_t3540,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_Int64U26_t3540,
	RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_UInt32U26_t3541_ExceptionU26_t3539,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_UInt32U26_t3541_ExceptionU26_t3539,
	RuntimeInvoker_UInt32_t1124_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_UInt32_t1124_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_UInt32U26_t3541,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_UInt32U26_t3541,
	RuntimeInvoker_UInt64_t1134_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_UInt64U26_t3542_ExceptionU26_t3539,
	RuntimeInvoker_UInt64_t1134_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_UInt64U26_t3542,
	RuntimeInvoker_Int32_t478_SByte_t1135,
	RuntimeInvoker_Boolean_t536_SByte_t1135,
	RuntimeInvoker_Byte_t1117_Object_t_Object_t,
	RuntimeInvoker_Byte_t1117_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_ByteU26_t3530,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_ByteU26_t3530,
	RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_SByteU26_t3543_ExceptionU26_t3539,
	RuntimeInvoker_SByte_t1135_Object_t_Object_t,
	RuntimeInvoker_SByte_t1135_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_SByteU26_t3543,
	RuntimeInvoker_Boolean_t536_Object_t_SByte_t1135_Int16U26_t3544_ExceptionU26_t3539,
	RuntimeInvoker_Int16_t1136_Object_t_Object_t,
	RuntimeInvoker_Int16_t1136_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Int16U26_t3544,
	RuntimeInvoker_UInt16_t1122_Object_t_Object_t,
	RuntimeInvoker_UInt16_t1122_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_UInt16U26_t3545,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_UInt16U26_t3545,
	RuntimeInvoker_Void_t1548_ByteU2AU26_t3546_ByteU2AU26_t3546_DoubleU2AU26_t3547_UInt16U2AU26_t3548_UInt16U2AU26_t3548_UInt16U2AU26_t3548_UInt16U2AU26_t3548,
	RuntimeInvoker_UnicodeCategory_t1693_Int16_t1136,
	RuntimeInvoker_Char_t526_Int16_t1136_Object_t,
	RuntimeInvoker_Void_t1548_Int16_t1136_Int32_t478,
	RuntimeInvoker_Char_t526_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_SByte_t1135_Object_t,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478_SByte_t1135_Object_t,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Int16_t1136_Int32_t478,
	RuntimeInvoker_Object_t_Int32_t478_Int16_t1136,
	RuntimeInvoker_Object_t_Int16_t1136_Int16_t1136,
	RuntimeInvoker_Void_t1548_Object_t_Int32U26_t3510_Int32U26_t3510_Int32U26_t3510_BooleanU26_t3509_StringU26_t3529,
	RuntimeInvoker_Void_t1548_Int32_t478_Int16_t1136,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Object_t_Int16_t1136_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Single_t531,
	RuntimeInvoker_Single_t531_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_Double_t553,
	RuntimeInvoker_Boolean_t536_Double_t553,
	RuntimeInvoker_Double_t553_Object_t_Object_t,
	RuntimeInvoker_Double_t553_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_DoubleU26_t3549_ExceptionU26_t3539,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_DoubleU26_t3549,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_DoubleU26_t3549,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Object_t_Decimal_t1133,
	RuntimeInvoker_Decimal_t1133_Decimal_t1133_Decimal_t1133,
	RuntimeInvoker_UInt64_t1134_Decimal_t1133,
	RuntimeInvoker_Int64_t1131_Decimal_t1133,
	RuntimeInvoker_Boolean_t536_Decimal_t1133_Decimal_t1133,
	RuntimeInvoker_Decimal_t1133_Decimal_t1133,
	RuntimeInvoker_Int32_t478_Decimal_t1133_Decimal_t1133,
	RuntimeInvoker_Int32_t478_Decimal_t1133,
	RuntimeInvoker_Boolean_t536_Decimal_t1133,
	RuntimeInvoker_Decimal_t1133_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Int32U26_t3510_BooleanU26_t3509_BooleanU26_t3509_Int32U26_t3510_SByte_t1135,
	RuntimeInvoker_Decimal_t1133_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_DecimalU26_t3550_SByte_t1135,
	RuntimeInvoker_Int32_t478_DecimalU26_t3550_UInt64U26_t3542,
	RuntimeInvoker_Int32_t478_DecimalU26_t3550_Int64U26_t3540,
	RuntimeInvoker_Int32_t478_DecimalU26_t3550_DecimalU26_t3550,
	RuntimeInvoker_Int32_t478_DecimalU26_t3550_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_DecimalU26_t3550_Int32_t478,
	RuntimeInvoker_Double_t553_DecimalU26_t3550,
	RuntimeInvoker_Void_t1548_DecimalU26_t3550_Int32_t478,
	RuntimeInvoker_Int32_t478_DecimalU26_t3550_DecimalU26_t3550_DecimalU26_t3550,
	RuntimeInvoker_Byte_t1117_Decimal_t1133,
	RuntimeInvoker_SByte_t1135_Decimal_t1133,
	RuntimeInvoker_Int16_t1136_Decimal_t1133,
	RuntimeInvoker_UInt16_t1122_Decimal_t1133,
	RuntimeInvoker_UInt32_t1124_Decimal_t1133,
	RuntimeInvoker_Decimal_t1133_SByte_t1135,
	RuntimeInvoker_Decimal_t1133_Int16_t1136,
	RuntimeInvoker_Decimal_t1133_Int32_t478,
	RuntimeInvoker_Decimal_t1133_Int64_t1131,
	RuntimeInvoker_Decimal_t1133_Single_t531,
	RuntimeInvoker_Decimal_t1133_Double_t553,
	RuntimeInvoker_Single_t531_Decimal_t1133,
	RuntimeInvoker_Double_t553_Decimal_t1133,
	RuntimeInvoker_Boolean_t536_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int64_t1131,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t478_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt64_t1134_IntPtr_t,
	RuntimeInvoker_UInt32_t1124_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t1131,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t3551,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_UInt64_t1134_Object_t_Int32_t478,
	RuntimeInvoker_Object_t_Object_t_Int16_t1136,
	RuntimeInvoker_Object_t_Object_t_Int64_t1131,
	RuntimeInvoker_Int64_t1131_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Object_t_Int64_t1131_Int64_t1131,
	RuntimeInvoker_Object_t_Int64_t1131_Int64_t1131_Int64_t1131,
	RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Int64_t1131,
	RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Int64_t1131_Int64_t1131,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Object_t_Int64_t1131_Int64_t1131,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64_t1131,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_TypeAttributes_t1812,
	RuntimeInvoker_MemberTypes_t1791,
	RuntimeInvoker_RuntimeTypeHandle_t1550,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_SByte_t1135,
	RuntimeInvoker_TypeCode_t2140_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1550,
	RuntimeInvoker_RuntimeTypeHandle_t1550_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_RuntimeFieldHandle_t1551,
	RuntimeInvoker_Void_t1548_IntPtr_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_ContractionU5BU5DU26_t3552_Level2MapU5BU5DU26_t3553,
	RuntimeInvoker_Void_t1548_Object_t_CodePointIndexerU26_t3554_ByteU2AU26_t3546_ByteU2AU26_t3546_CodePointIndexerU26_t3554_ByteU2AU26_t3546,
	RuntimeInvoker_Byte_t1117_Int32_t478,
	RuntimeInvoker_Boolean_t536_Int32_t478_SByte_t1135,
	RuntimeInvoker_Byte_t1117_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Int32_t478_Int32_t478,
	RuntimeInvoker_ExtenderType_t1594_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_BooleanU26_t3509_BooleanU26_t3509_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32_t478_BooleanU26_t3509_BooleanU26_t3509_SByte_t1135_SByte_t1135_ContextU26_t3555,
	RuntimeInvoker_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_Int32_t478_SByte_t1135_ContextU26_t3555,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_BooleanU26_t3509,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int16_t1136_Int32_t478_SByte_t1135_ContextU26_t3555,
	RuntimeInvoker_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_Object_t_ContextU26_t3555,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135_ContextU26_t3555,
	RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Object_t_SByte_t1135_ContextU26_t3555,
	RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Object_t_SByte_t1135_Int32_t478_ContractionU26_t3556_ContextU26_t3555,
	RuntimeInvoker_Boolean_t536_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Int32_t478_Object_t_SByte_t1135_ContextU26_t3555,
	RuntimeInvoker_Boolean_t536_Object_t_Int32U26_t3510_Int32_t478_Int32_t478_Int32_t478_Object_t_SByte_t1135_Int32_t478_ContractionU26_t3556_ContextU26_t3555,
	RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Object_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Object_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_SByte_t1135_ByteU5BU5DU26_t3531_Int32U26_t3510,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_ConfidenceFactor_t1603,
	RuntimeInvoker_Sign_t1605_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t1334_SByte_t1135,
	RuntimeInvoker_Void_t1548_DSAParameters_t1334,
	RuntimeInvoker_Int16_t1136_Object_t_Int32_t478,
	RuntimeInvoker_Double_t553_Object_t_Int32_t478,
	RuntimeInvoker_Object_t_Int16_t1136_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int32_t478_Single_t531_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Single_t531_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Int32_t478_Single_t531_Object_t,
	RuntimeInvoker_Boolean_t536_Int32_t478_SByte_t1135_MethodBaseU26_t3557_Int32U26_t3510_Int32U26_t3510_StringU26_t3529_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Int32_t478_DateTime_t871,
	RuntimeInvoker_DayOfWeek_t2091_DateTime_t871,
	RuntimeInvoker_Int32_t478_Int32U26_t3510_Int32_t478_Int32_t478,
	RuntimeInvoker_DayOfWeek_t2091_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32U26_t3510_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Void_t1548_DateTime_t871_DateTime_t871_TimeSpan_t1437,
	RuntimeInvoker_TimeSpan_t1437,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Object_t_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32U26_t3510,
	RuntimeInvoker_Decimal_t1133,
	RuntimeInvoker_SByte_t1135,
	RuntimeInvoker_UInt16_t1122,
	RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_SByte_t1135_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_Int32_t478,
	RuntimeInvoker_Int32_t478_IntPtr_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_MonoIOErrorU26_t3558,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t478_Int32_t478_MonoIOErrorU26_t3558,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t3558,
	RuntimeInvoker_FileAttributes_t1703_Object_t_MonoIOErrorU26_t3558,
	RuntimeInvoker_MonoFileType_t1712_IntPtr_t_MonoIOErrorU26_t3558,
	RuntimeInvoker_Boolean_t536_Object_t_MonoIOStatU26_t3559_MonoIOErrorU26_t3558,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_MonoIOErrorU26_t3558,
	RuntimeInvoker_Boolean_t536_IntPtr_t_MonoIOErrorU26_t3558,
	RuntimeInvoker_Int32_t478_IntPtr_t_Object_t_Int32_t478_Int32_t478_MonoIOErrorU26_t3558,
	RuntimeInvoker_Int64_t1131_IntPtr_t_Int64_t1131_Int32_t478_MonoIOErrorU26_t3558,
	RuntimeInvoker_Int64_t1131_IntPtr_t_MonoIOErrorU26_t3558,
	RuntimeInvoker_Boolean_t536_IntPtr_t_Int64_t1131_MonoIOErrorU26_t3558,
	RuntimeInvoker_Void_t1548_Object_t_Int32_t478_Int32_t478_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t1786,
	RuntimeInvoker_RuntimeMethodHandle_t2132,
	RuntimeInvoker_MethodAttributes_t1792,
	RuntimeInvoker_MethodToken_t1751,
	RuntimeInvoker_FieldAttributes_t1789,
	RuntimeInvoker_RuntimeFieldHandle_t1551,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_OpCode_t1755,
	RuntimeInvoker_Void_t1548_OpCode_t1755_Object_t,
	RuntimeInvoker_StackBehaviour_t1760,
	RuntimeInvoker_PropertyAttributes_t1808,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int32_t478_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Int32_t478_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_SByte_t1135_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t3510_ModuleU26_t3560,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1135_SByte_t1135,
	RuntimeInvoker_AssemblyNameFlags_t1780,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_ObjectU5BU5DU26_t3538_Object_t_Object_t_Object_t_ObjectU26_t3528,
	RuntimeInvoker_Void_t1548_ObjectU5BU5DU26_t3538_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_ObjectU5BU5DU26_t3538_Object_t,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_EventAttributes_t1787,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1551,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t1105,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t2132,
	RuntimeInvoker_Void_t1548_Object_t_MonoEventInfoU26_t3561,
	RuntimeInvoker_MonoEventInfo_t1797_Object_t,
	RuntimeInvoker_Void_t1548_IntPtr_t_MonoMethodInfoU26_t3562,
	RuntimeInvoker_MonoMethodInfo_t1800_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t1792_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1786_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t3539,
	RuntimeInvoker_Void_t1548_Object_t_MonoPropertyInfoU26_t3563_Int32_t478,
	RuntimeInvoker_ParameterAttributes_t1804,
	RuntimeInvoker_GCHandle_t1833_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_IntPtr_t_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_IntPtr_t_Object_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_BooleanU26_t3509,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t3529,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t3529,
	RuntimeInvoker_Void_t1548_TimeSpan_t1437,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_SByte_t1135_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t1105_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t1105_ISurrogateSelectorU26_t3564,
	RuntimeInvoker_Void_t1548_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t1437_Object_t,
	RuntimeInvoker_Object_t_StringU26_t3529,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t3528,
	RuntimeInvoker_Boolean_t536_Object_t_StringU26_t3529_StringU26_t3529,
	RuntimeInvoker_WellKnownObjectMode_t1926,
	RuntimeInvoker_StreamingContext_t1105,
	RuntimeInvoker_TypeFilterLevel_t1942,
	RuntimeInvoker_Void_t1548_Object_t_BooleanU26_t3509,
	RuntimeInvoker_Object_t_Byte_t1117_Object_t_SByte_t1135_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t1117_Object_t_SByte_t1135_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_ObjectU26_t3528_HeaderU5BU5DU26_t3565,
	RuntimeInvoker_Void_t1548_Byte_t1117_Object_t_SByte_t1135_ObjectU26_t3528_HeaderU5BU5DU26_t3565,
	RuntimeInvoker_Boolean_t536_Byte_t1117_Object_t,
	RuntimeInvoker_Void_t1548_Byte_t1117_Object_t_Int64U26_t3540_ObjectU26_t3528_SerializationInfoU26_t3566,
	RuntimeInvoker_Void_t1548_Object_t_SByte_t1135_SByte_t1135_Int64U26_t3540_ObjectU26_t3528_SerializationInfoU26_t3566,
	RuntimeInvoker_Void_t1548_Object_t_Int64U26_t3540_ObjectU26_t3528_SerializationInfoU26_t3566,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64_t1131_ObjectU26_t3528_SerializationInfoU26_t3566,
	RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Object_t_Int64_t1131_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Int64U26_t3540_ObjectU26_t3528,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64U26_t3540_ObjectU26_t3528,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Int64_t1131_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Int64_t1131_Int64_t1131_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t1131_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t1117,
	RuntimeInvoker_Void_t1548_Int64_t1131_Int32_t478_Int64_t1131,
	RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Int64_t1131,
	RuntimeInvoker_Void_t1548_Object_t_Int64_t1131_Object_t_Int64_t1131_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_SByte_t1135_Object_t_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_StreamingContext_t1105,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_StreamingContext_t1105,
	RuntimeInvoker_Void_t1548_StreamingContext_t1105,
	RuntimeInvoker_Object_t_StreamingContext_t1105_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Int16_t1136,
	RuntimeInvoker_Void_t1548_Object_t_DateTime_t871,
	RuntimeInvoker_SerializationEntry_t1959,
	RuntimeInvoker_StreamingContextStates_t1962,
	RuntimeInvoker_CspProviderFlags_t1966,
	RuntimeInvoker_UInt32_t1124_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_SByte_t1135,
	RuntimeInvoker_Void_t1548_Int64_t1131_Object_t_Int32_t478,
	RuntimeInvoker_UInt32_t1124_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_UInt32U26_t3541_Int32_t478_UInt32U26_t3541_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t1548_Int64_t1131_Int64_t1131,
	RuntimeInvoker_UInt64_t1134_Int64_t1131_Int32_t478,
	RuntimeInvoker_UInt64_t1134_Int64_t1131_Int64_t1131_Int64_t1131,
	RuntimeInvoker_UInt64_t1134_Int64_t1131,
	RuntimeInvoker_PaddingMode_t1193,
	RuntimeInvoker_Void_t1548_StringBuilderU26_t3567_Int32_t478,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_EncoderFallbackBufferU26_t3568_CharU5BU5DU26_t3569,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_DecoderFallbackBufferU26_t3570,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Boolean_t536_Int16_t1136_Int32_t478,
	RuntimeInvoker_Boolean_t536_Int16_t1136_Int16_t1136_Int32_t478,
	RuntimeInvoker_Void_t1548_Int16_t1136_Int16_t1136_Int32_t478,
	RuntimeInvoker_Object_t_Int32U26_t3510,
	RuntimeInvoker_Object_t_Int32_t478_Object_t_Int32_t478,
	RuntimeInvoker_Void_t1548_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_SByte_t1135_Int32_t478_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_SByte_t1135_Int32U26_t3510_BooleanU26_t3509_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_Int32U26_t3510,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_CharU26_t3536_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_CharU26_t3536_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_CharU26_t3536_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_CharU26_t3536_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_Object_t_Int64_t1131_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_Object_t_Int64_t1131_Int32_t478_Object_t_Int32U26_t3510,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Object_t_Int32_t478_UInt32U26_t3541_UInt32U26_t3541_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Int32_t478_UInt32U26_t3541_UInt32U26_t3541_Object_t_DecoderFallbackBufferU26_t3570_ByteU5BU5DU26_t3531_SByte_t1135,
	RuntimeInvoker_Void_t1548_SByte_t1135_Int32_t478,
	RuntimeInvoker_IntPtr_t_SByte_t1135_Object_t_BooleanU26_t3509,
	RuntimeInvoker_Boolean_t536_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t1135_SByte_t1135_Object_t_BooleanU26_t3509,
	RuntimeInvoker_Boolean_t536_TimeSpan_t1437_TimeSpan_t1437,
	RuntimeInvoker_Boolean_t536_Int64_t1131_Int64_t1131_SByte_t1135,
	RuntimeInvoker_Boolean_t536_IntPtr_t_Int32_t478_SByte_t1135,
	RuntimeInvoker_Int64_t1131_Double_t553,
	RuntimeInvoker_Object_t_Double_t553,
	RuntimeInvoker_Int64_t1131_Object_t_Int32_t478,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t478_Int32_t478,
	RuntimeInvoker_Byte_t1117_SByte_t1135,
	RuntimeInvoker_Byte_t1117_Double_t553,
	RuntimeInvoker_Byte_t1117_Single_t531,
	RuntimeInvoker_Byte_t1117_Int64_t1131,
	RuntimeInvoker_Char_t526_SByte_t1135,
	RuntimeInvoker_Char_t526_Int64_t1131,
	RuntimeInvoker_Char_t526_Single_t531,
	RuntimeInvoker_Char_t526_Object_t_Object_t,
	RuntimeInvoker_DateTime_t871_Object_t_Object_t,
	RuntimeInvoker_DateTime_t871_Int16_t1136,
	RuntimeInvoker_DateTime_t871_Int32_t478,
	RuntimeInvoker_DateTime_t871_Int64_t1131,
	RuntimeInvoker_DateTime_t871_Single_t531,
	RuntimeInvoker_DateTime_t871_SByte_t1135,
	RuntimeInvoker_Double_t553_SByte_t1135,
	RuntimeInvoker_Double_t553_Double_t553,
	RuntimeInvoker_Double_t553_Single_t531,
	RuntimeInvoker_Double_t553_Int32_t478,
	RuntimeInvoker_Double_t553_Int64_t1131,
	RuntimeInvoker_Double_t553_Int16_t1136,
	RuntimeInvoker_Int16_t1136_SByte_t1135,
	RuntimeInvoker_Int16_t1136_Double_t553,
	RuntimeInvoker_Int16_t1136_Single_t531,
	RuntimeInvoker_Int16_t1136_Int32_t478,
	RuntimeInvoker_Int16_t1136_Int64_t1131,
	RuntimeInvoker_Int64_t1131_SByte_t1135,
	RuntimeInvoker_Int64_t1131_Int16_t1136,
	RuntimeInvoker_Int64_t1131_Single_t531,
	RuntimeInvoker_Int64_t1131_Int64_t1131,
	RuntimeInvoker_SByte_t1135_SByte_t1135,
	RuntimeInvoker_SByte_t1135_Int16_t1136,
	RuntimeInvoker_SByte_t1135_Double_t553,
	RuntimeInvoker_SByte_t1135_Single_t531,
	RuntimeInvoker_SByte_t1135_Int32_t478,
	RuntimeInvoker_SByte_t1135_Int64_t1131,
	RuntimeInvoker_Single_t531_SByte_t1135,
	RuntimeInvoker_Single_t531_Double_t553,
	RuntimeInvoker_Single_t531_Int64_t1131,
	RuntimeInvoker_Single_t531_Int16_t1136,
	RuntimeInvoker_UInt16_t1122_SByte_t1135,
	RuntimeInvoker_UInt16_t1122_Double_t553,
	RuntimeInvoker_UInt16_t1122_Single_t531,
	RuntimeInvoker_UInt16_t1122_Int32_t478,
	RuntimeInvoker_UInt16_t1122_Int64_t1131,
	RuntimeInvoker_UInt32_t1124_SByte_t1135,
	RuntimeInvoker_UInt32_t1124_Int16_t1136,
	RuntimeInvoker_UInt32_t1124_Double_t553,
	RuntimeInvoker_UInt32_t1124_Single_t531,
	RuntimeInvoker_UInt32_t1124_Int64_t1131,
	RuntimeInvoker_UInt64_t1134_SByte_t1135,
	RuntimeInvoker_UInt64_t1134_Int16_t1136,
	RuntimeInvoker_UInt64_t1134_Double_t553,
	RuntimeInvoker_UInt64_t1134_Single_t531,
	RuntimeInvoker_UInt64_t1134_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_SByte_t1135_TimeSpan_t1437,
	RuntimeInvoker_Void_t1548_Int64_t1131_Int32_t478,
	RuntimeInvoker_DayOfWeek_t2091,
	RuntimeInvoker_DateTimeKind_t2089,
	RuntimeInvoker_DateTime_t871_TimeSpan_t1437,
	RuntimeInvoker_DateTime_t871_Double_t553,
	RuntimeInvoker_Int32_t478_DateTime_t871_DateTime_t871,
	RuntimeInvoker_DateTime_t871_DateTime_t871_Int32_t478,
	RuntimeInvoker_DateTime_t871_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Int32_t478_DateTimeU26_t3571_DateTimeOffsetU26_t3572_SByte_t1135_ExceptionU26_t3539,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_ExceptionU26_t3539,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_SByte_t1135_SByte_t1135_Int32U26_t3510,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Object_t_Object_t_SByte_t1135_Int32U26_t3510,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Int32_t478_Object_t_Int32U26_t3510,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Int32_t478_Object_t_SByte_t1135_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_Boolean_t536_Object_t_Int32_t478_Object_t_SByte_t1135_Int32U26_t3510,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_SByte_t1135_DateTimeU26_t3571_DateTimeOffsetU26_t3572_Object_t_Int32_t478_SByte_t1135_BooleanU26_t3509_BooleanU26_t3509,
	RuntimeInvoker_DateTime_t871_Object_t_Object_t_Object_t_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_Object_t_Object_t_Int32_t478_DateTimeU26_t3571_SByte_t1135_BooleanU26_t3509_SByte_t1135_ExceptionU26_t3539,
	RuntimeInvoker_DateTime_t871_DateTime_t871_TimeSpan_t1437,
	RuntimeInvoker_Boolean_t536_DateTime_t871_DateTime_t871,
	RuntimeInvoker_Void_t1548_DateTime_t871_TimeSpan_t1437,
	RuntimeInvoker_Void_t1548_Int64_t1131_TimeSpan_t1437,
	RuntimeInvoker_Int32_t478_DateTimeOffset_t1148,
	RuntimeInvoker_Boolean_t536_DateTimeOffset_t1148,
	RuntimeInvoker_DateTimeOffset_t1148,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int16_t1136,
	RuntimeInvoker_Object_t_Int16_t1136_Object_t_BooleanU26_t3509_BooleanU26_t3509,
	RuntimeInvoker_Object_t_Int16_t1136_Object_t_BooleanU26_t3509_BooleanU26_t3509_SByte_t1135,
	RuntimeInvoker_Object_t_DateTime_t871_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t871_Nullable_1_t2187_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_MonoEnumInfo_t2102,
	RuntimeInvoker_Void_t1548_Object_t_MonoEnumInfoU26_t3573,
	RuntimeInvoker_Int32_t478_Int16_t1136_Int16_t1136,
	RuntimeInvoker_Int32_t478_Int64_t1131_Int64_t1131,
	RuntimeInvoker_PlatformID_t2129,
	RuntimeInvoker_Void_t1548_Int32_t478_Int16_t1136_Int16_t1136_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Int32_t478_Guid_t555,
	RuntimeInvoker_Boolean_t536_Guid_t555,
	RuntimeInvoker_Guid_t555,
	RuntimeInvoker_Object_t_SByte_t1135_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Double_t553_Double_t553_Double_t553,
	RuntimeInvoker_TypeAttributes_t1812_Object_t,
	RuntimeInvoker_Object_t_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Void_t1548_UInt64U2AU26_t3574_Int32U2AU26_t3575_CharU2AU26_t3576_CharU2AU26_t3576_Int64U2AU26_t3577_Int32U2AU26_t3575,
	RuntimeInvoker_Void_t1548_Int32_t478_Int64_t1131,
	RuntimeInvoker_Void_t1548_Object_t_Double_t553_Int32_t478,
	RuntimeInvoker_Void_t1548_Object_t_Decimal_t1133,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t1136_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t1131_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t531_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t553_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t1133_Object_t,
	RuntimeInvoker_Object_t_Single_t531_Object_t,
	RuntimeInvoker_Object_t_Double_t553_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_BooleanU26_t3509_SByte_t1135_Int32U26_t3510_Int32U26_t3510,
	RuntimeInvoker_Object_t_Object_t_Int32_t478_Int32_t478_Object_t_SByte_t1135_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_Int64_t1131_Int32_t478_Int32_t478_Int32_t478_Int32_t478_Int32_t478,
	RuntimeInvoker_TimeSpan_t1437_TimeSpan_t1437,
	RuntimeInvoker_Int32_t478_TimeSpan_t1437_TimeSpan_t1437,
	RuntimeInvoker_Int32_t478_TimeSpan_t1437,
	RuntimeInvoker_Boolean_t536_TimeSpan_t1437,
	RuntimeInvoker_TimeSpan_t1437_Double_t553,
	RuntimeInvoker_TimeSpan_t1437_Double_t553_Int64_t1131,
	RuntimeInvoker_TimeSpan_t1437_TimeSpan_t1437_TimeSpan_t1437,
	RuntimeInvoker_TimeSpan_t1437_DateTime_t871,
	RuntimeInvoker_Boolean_t536_DateTime_t871_Object_t,
	RuntimeInvoker_DateTime_t871_DateTime_t871,
	RuntimeInvoker_TimeSpan_t1437_DateTime_t871_TimeSpan_t1437,
	RuntimeInvoker_Boolean_t536_Int32_t478_Int64U5BU5DU26_t3578_StringU5BU5DU26_t3579,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_KeyValuePair_2_t446,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_KeyValuePair_2_t446_Object_t,
	RuntimeInvoker_Void_t1548_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t536_ObjectU26_t3528_Object_t,
	RuntimeInvoker_Void_t1548_ObjectU26_t3528_Object_t,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2350,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2350,
	RuntimeInvoker_Enumerator_t2872,
	RuntimeInvoker_Enumerator_t2269,
	RuntimeInvoker_Void_t1548_Int32_t478_ObjectU26_t3528,
	RuntimeInvoker_Void_t1548_ObjectU5BU5DU26_t3538_Int32_t478,
	RuntimeInvoker_Void_t1548_ObjectU5BU5DU26_t3538_Int32_t478_Int32_t478,
	RuntimeInvoker_KeyValuePair_2_t2350_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2354,
	RuntimeInvoker_DictionaryEntry_t552_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2350,
	RuntimeInvoker_Enumerator_t2353,
	RuntimeInvoker_Enumerator_t2357,
	RuntimeInvoker_Int32_t478_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Enumerator_t2227,
	RuntimeInvoker_Enumerator_t2521,
	RuntimeInvoker_Enumerator_t2518,
	RuntimeInvoker_KeyValuePair_2_t2514,
	RuntimeInvoker_Void_t1548_ColorTween_t630,
	RuntimeInvoker_Boolean_t536_TypeU26_t3580_Int32_t478,
	RuntimeInvoker_Boolean_t536_BooleanU26_t3509_SByte_t1135,
	RuntimeInvoker_Boolean_t536_FillMethodU26_t3581_Int32_t478,
	RuntimeInvoker_Boolean_t536_SingleU26_t3519_Single_t531,
	RuntimeInvoker_Boolean_t536_ContentTypeU26_t3582_Int32_t478,
	RuntimeInvoker_Boolean_t536_LineTypeU26_t3583_Int32_t478,
	RuntimeInvoker_Boolean_t536_InputTypeU26_t3584_Int32_t478,
	RuntimeInvoker_Boolean_t536_TouchScreenKeyboardTypeU26_t3585_Int32_t478,
	RuntimeInvoker_Boolean_t536_CharacterValidationU26_t3586_Int32_t478,
	RuntimeInvoker_Boolean_t536_CharU26_t3536_Int16_t1136,
	RuntimeInvoker_Boolean_t536_DirectionU26_t3587_Int32_t478,
	RuntimeInvoker_Boolean_t536_NavigationU26_t3588_Navigation_t690,
	RuntimeInvoker_Boolean_t536_TransitionU26_t3589_Int32_t478,
	RuntimeInvoker_Boolean_t536_ColorBlockU26_t3590_ColorBlock_t642,
	RuntimeInvoker_Boolean_t536_SpriteStateU26_t3591_SpriteState_t708,
	RuntimeInvoker_Boolean_t536_DirectionU26_t3592_Int32_t478,
	RuntimeInvoker_Boolean_t536_AspectModeU26_t3593_Int32_t478,
	RuntimeInvoker_Boolean_t536_FitModeU26_t3594_Int32_t478,
	RuntimeInvoker_Void_t1548_CornerU26_t3595_Int32_t478,
	RuntimeInvoker_Void_t1548_AxisU26_t3596_Int32_t478,
	RuntimeInvoker_Void_t1548_Vector2U26_t3518_Vector2_t7,
	RuntimeInvoker_Void_t1548_ConstraintU26_t3597_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32U26_t3510_Int32_t478,
	RuntimeInvoker_Void_t1548_SingleU26_t3519_Single_t531,
	RuntimeInvoker_Void_t1548_BooleanU26_t3509_SByte_t1135,
	RuntimeInvoker_Void_t1548_TextAnchorU26_t3598_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_Double_t553,
	RuntimeInvoker_Int32_t478_Vector3_t215,
	RuntimeInvoker_Boolean_t536_Vector2_t7,
	RuntimeInvoker_Void_t1548_Int32_t478_Vector2_t7,
	RuntimeInvoker_Boolean_t536_Color_t6,
	RuntimeInvoker_Int32_t478_Color_t6,
	RuntimeInvoker_RaycastResult_t511_Int32_t478,
	RuntimeInvoker_Boolean_t536_RaycastResult_t511,
	RuntimeInvoker_Int32_t478_RaycastResult_t511,
	RuntimeInvoker_Void_t1548_Int32_t478_RaycastResult_t511,
	RuntimeInvoker_Void_t1548_RaycastResultU5BU5DU26_t3599_Int32_t478,
	RuntimeInvoker_Void_t1548_RaycastResultU5BU5DU26_t3599_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_RaycastResult_t511_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_RaycastResult_t511_RaycastResult_t511_Object_t,
	RuntimeInvoker_Void_t1548_CharU5BU5DU26_t3569_Int32_t478,
	RuntimeInvoker_Void_t1548_CharU5BU5DU26_t3569_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Int16_t1136_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Int16_t1136_Int16_t1136_Object_t,
	RuntimeInvoker_RaycastHit_t482_Int32_t478,
	RuntimeInvoker_Void_t1548_RaycastHit_t482,
	RuntimeInvoker_Boolean_t536_RaycastHit_t482,
	RuntimeInvoker_Int32_t478_RaycastHit_t482,
	RuntimeInvoker_Void_t1548_Int32_t478_RaycastHit_t482,
	RuntimeInvoker_KeyValuePair_2_t2328_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2328,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2328,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2328,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2328,
	RuntimeInvoker_TweenType_t442_Int32_t478,
	RuntimeInvoker_DictionaryEntry_t552_Int32_t478,
	RuntimeInvoker_Void_t1548_DictionaryEntry_t552,
	RuntimeInvoker_Boolean_t536_DictionaryEntry_t552,
	RuntimeInvoker_Int32_t478_DictionaryEntry_t552,
	RuntimeInvoker_Void_t1548_Int32_t478_DictionaryEntry_t552,
	RuntimeInvoker_KeyValuePair_2_t2350_Int32_t478,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2350,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2350,
	RuntimeInvoker_Boolean_t536_Rect_t225,
	RuntimeInvoker_Int32_t478_Rect_t225,
	RuntimeInvoker_KeyValuePair_2_t2364_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2364,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2364,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2364,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2364,
	RuntimeInvoker_Space_t546_Int32_t478,
	RuntimeInvoker_EaseType_t425_Int32_t478,
	RuntimeInvoker_LoopType_t426_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32U5BU5DU26_t3600_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32U5BU5DU26_t3600_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_SingleU5BU5DU26_t3601_Int32_t478,
	RuntimeInvoker_Void_t1548_SingleU5BU5DU26_t3601_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Single_t531_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Single_t531_Single_t531_Object_t,
	RuntimeInvoker_Void_t1548_BooleanU5BU5DU26_t3602_Int32_t478,
	RuntimeInvoker_Void_t1548_BooleanU5BU5DU26_t3602_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_SByte_t1135_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_SByte_t1135_SByte_t1135_Object_t,
	RuntimeInvoker_Void_t1548_Vector3U5BU5DU26_t3506_Int32_t478,
	RuntimeInvoker_Void_t1548_Vector3U5BU5DU26_t3506_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Vector3_t215_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Vector3_t215_Vector3_t215_Object_t,
	RuntimeInvoker_Void_t1548_ColorU5BU5DU26_t3603_Int32_t478,
	RuntimeInvoker_Void_t1548_ColorU5BU5DU26_t3603_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_Color_t6_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Color_t6_Color_t6_Object_t,
	RuntimeInvoker_Void_t1548_SpaceU5BU5DU26_t3604_Int32_t478,
	RuntimeInvoker_Void_t1548_SpaceU5BU5DU26_t3604_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_EaseTypeU5BU5DU26_t3605_Int32_t478,
	RuntimeInvoker_Void_t1548_EaseTypeU5BU5DU26_t3605_Int32_t478_Int32_t478,
	RuntimeInvoker_Void_t1548_LoopTypeU5BU5DU26_t3606_Int32_t478,
	RuntimeInvoker_Void_t1548_LoopTypeU5BU5DU26_t3606_Int32_t478_Int32_t478,
	RuntimeInvoker_KeyValuePair_2_t2514_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2514,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2514,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2514,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2514,
	RuntimeInvoker_RaycastHit2D_t789_Int32_t478,
	RuntimeInvoker_Void_t1548_RaycastHit2D_t789,
	RuntimeInvoker_Boolean_t536_RaycastHit2D_t789,
	RuntimeInvoker_Int32_t478_RaycastHit2D_t789,
	RuntimeInvoker_Void_t1548_Int32_t478_RaycastHit2D_t789,
	RuntimeInvoker_Void_t1548_UIVertexU5BU5DU26_t3607_Int32_t478,
	RuntimeInvoker_Void_t1548_UIVertexU5BU5DU26_t3607_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_UIVertex_t685_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_UIVertex_t685_UIVertex_t685_Object_t,
	RuntimeInvoker_ContentType_t671_Int32_t478,
	RuntimeInvoker_UILineInfo_t809_Int32_t478,
	RuntimeInvoker_Void_t1548_UILineInfo_t809,
	RuntimeInvoker_Boolean_t536_UILineInfo_t809,
	RuntimeInvoker_Int32_t478_UILineInfo_t809,
	RuntimeInvoker_Void_t1548_Int32_t478_UILineInfo_t809,
	RuntimeInvoker_UICharInfo_t811_Int32_t478,
	RuntimeInvoker_Void_t1548_UICharInfo_t811,
	RuntimeInvoker_Boolean_t536_UICharInfo_t811,
	RuntimeInvoker_Int32_t478_UICharInfo_t811,
	RuntimeInvoker_Void_t1548_Int32_t478_UICharInfo_t811,
	RuntimeInvoker_GcAchievementData_t1020_Int32_t478,
	RuntimeInvoker_Void_t1548_GcAchievementData_t1020,
	RuntimeInvoker_Boolean_t536_GcAchievementData_t1020,
	RuntimeInvoker_Int32_t478_GcAchievementData_t1020,
	RuntimeInvoker_Void_t1548_Int32_t478_GcAchievementData_t1020,
	RuntimeInvoker_GcScoreData_t1021_Int32_t478,
	RuntimeInvoker_Boolean_t536_GcScoreData_t1021,
	RuntimeInvoker_Int32_t478_GcScoreData_t1021,
	RuntimeInvoker_Void_t1548_Int32_t478_GcScoreData_t1021,
	RuntimeInvoker_Void_t1548_Int32_t478_IntPtr_t,
	RuntimeInvoker_ContactPoint_t930_Int32_t478,
	RuntimeInvoker_Void_t1548_ContactPoint_t930,
	RuntimeInvoker_Boolean_t536_ContactPoint_t930,
	RuntimeInvoker_Int32_t478_ContactPoint_t930,
	RuntimeInvoker_Void_t1548_Int32_t478_ContactPoint_t930,
	RuntimeInvoker_Keyframe_t943_Int32_t478,
	RuntimeInvoker_Void_t1548_Keyframe_t943,
	RuntimeInvoker_Boolean_t536_Keyframe_t943,
	RuntimeInvoker_Int32_t478_Keyframe_t943,
	RuntimeInvoker_Void_t1548_Int32_t478_Keyframe_t943,
	RuntimeInvoker_Void_t1548_UICharInfoU5BU5DU26_t3608_Int32_t478,
	RuntimeInvoker_Void_t1548_UICharInfoU5BU5DU26_t3608_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_UICharInfo_t811_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_UICharInfo_t811_UICharInfo_t811_Object_t,
	RuntimeInvoker_Void_t1548_UILineInfoU5BU5DU26_t3609_Int32_t478,
	RuntimeInvoker_Void_t1548_UILineInfoU5BU5DU26_t3609_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_Object_t_UILineInfo_t809_Int32_t478_Int32_t478,
	RuntimeInvoker_Int32_t478_UILineInfo_t809_UILineInfo_t809_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2706_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2706,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2706,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2706,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2706,
	RuntimeInvoker_KeyValuePair_2_t2743_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2743,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2743,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2743,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2743,
	RuntimeInvoker_NetworkID_t979_Int32_t478,
	RuntimeInvoker_Boolean_t536_UInt64_t1134,
	RuntimeInvoker_Int32_t478_UInt64_t1134,
	RuntimeInvoker_Void_t1548_Int32_t478_UInt64_t1134,
	RuntimeInvoker_KeyValuePair_2_t2762_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2762,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2762,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2762,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2762,
	RuntimeInvoker_ParameterModifier_t1805_Int32_t478,
	RuntimeInvoker_Void_t1548_ParameterModifier_t1805,
	RuntimeInvoker_Boolean_t536_ParameterModifier_t1805,
	RuntimeInvoker_Int32_t478_ParameterModifier_t1805,
	RuntimeInvoker_Void_t1548_Int32_t478_ParameterModifier_t1805,
	RuntimeInvoker_HitInfo_t1039_Int32_t478,
	RuntimeInvoker_Void_t1548_HitInfo_t1039,
	RuntimeInvoker_Int32_t478_HitInfo_t1039,
	RuntimeInvoker_KeyValuePair_2_t2825_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2825,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2825,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2825,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2825,
	RuntimeInvoker_TextEditOp_t1057_Int32_t478,
	RuntimeInvoker_ClientCertificateType_t1308_Int32_t478,
	RuntimeInvoker_KeyValuePair_2_t2877_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2877,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2877,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2877,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2877,
	RuntimeInvoker_X509ChainStatus_t1432_Int32_t478,
	RuntimeInvoker_Void_t1548_X509ChainStatus_t1432,
	RuntimeInvoker_Boolean_t536_X509ChainStatus_t1432,
	RuntimeInvoker_Int32_t478_X509ChainStatus_t1432,
	RuntimeInvoker_Void_t1548_Int32_t478_X509ChainStatus_t1432,
	RuntimeInvoker_KeyValuePair_2_t2896_Int32_t478,
	RuntimeInvoker_Void_t1548_KeyValuePair_2_t2896,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2896,
	RuntimeInvoker_Int32_t478_KeyValuePair_2_t2896,
	RuntimeInvoker_Void_t1548_Int32_t478_KeyValuePair_2_t2896,
	RuntimeInvoker_Int32_t478_Object_t_Int32_t478_Int32_t478_Int32_t478_Object_t,
	RuntimeInvoker_Mark_t1479_Int32_t478,
	RuntimeInvoker_Void_t1548_Mark_t1479,
	RuntimeInvoker_Boolean_t536_Mark_t1479,
	RuntimeInvoker_Int32_t478_Mark_t1479,
	RuntimeInvoker_Void_t1548_Int32_t478_Mark_t1479,
	RuntimeInvoker_UriScheme_t1516_Int32_t478,
	RuntimeInvoker_Void_t1548_UriScheme_t1516,
	RuntimeInvoker_Boolean_t536_UriScheme_t1516,
	RuntimeInvoker_Int32_t478_UriScheme_t1516,
	RuntimeInvoker_Void_t1548_Int32_t478_UriScheme_t1516,
	RuntimeInvoker_TableRange_t1580_Int32_t478,
	RuntimeInvoker_Void_t1548_TableRange_t1580,
	RuntimeInvoker_Boolean_t536_TableRange_t1580,
	RuntimeInvoker_Int32_t478_TableRange_t1580,
	RuntimeInvoker_Void_t1548_Int32_t478_TableRange_t1580,
	RuntimeInvoker_Slot_t1657_Int32_t478,
	RuntimeInvoker_Void_t1548_Slot_t1657,
	RuntimeInvoker_Boolean_t536_Slot_t1657,
	RuntimeInvoker_Int32_t478_Slot_t1657,
	RuntimeInvoker_Void_t1548_Int32_t478_Slot_t1657,
	RuntimeInvoker_Slot_t1664_Int32_t478,
	RuntimeInvoker_Void_t1548_Slot_t1664,
	RuntimeInvoker_Boolean_t536_Slot_t1664,
	RuntimeInvoker_Int32_t478_Slot_t1664,
	RuntimeInvoker_Void_t1548_Int32_t478_Slot_t1664,
	RuntimeInvoker_ILTokenInfo_t1742_Int32_t478,
	RuntimeInvoker_Void_t1548_ILTokenInfo_t1742,
	RuntimeInvoker_Boolean_t536_ILTokenInfo_t1742,
	RuntimeInvoker_Int32_t478_ILTokenInfo_t1742,
	RuntimeInvoker_Void_t1548_Int32_t478_ILTokenInfo_t1742,
	RuntimeInvoker_LabelData_t1744_Int32_t478,
	RuntimeInvoker_Void_t1548_LabelData_t1744,
	RuntimeInvoker_Boolean_t536_LabelData_t1744,
	RuntimeInvoker_Int32_t478_LabelData_t1744,
	RuntimeInvoker_Void_t1548_Int32_t478_LabelData_t1744,
	RuntimeInvoker_LabelFixup_t1743_Int32_t478,
	RuntimeInvoker_Void_t1548_LabelFixup_t1743,
	RuntimeInvoker_Boolean_t536_LabelFixup_t1743,
	RuntimeInvoker_Int32_t478_LabelFixup_t1743,
	RuntimeInvoker_Void_t1548_Int32_t478_LabelFixup_t1743,
	RuntimeInvoker_Void_t1548_Int32_t478_DateTime_t871,
	RuntimeInvoker_Void_t1548_Decimal_t1133,
	RuntimeInvoker_Void_t1548_Int32_t478_Decimal_t1133,
	RuntimeInvoker_TimeSpan_t1437_Int32_t478,
	RuntimeInvoker_Void_t1548_Int32_t478_TimeSpan_t1437,
	RuntimeInvoker_TypeTag_t1930_Int32_t478,
	RuntimeInvoker_Boolean_t536_Byte_t1117,
	RuntimeInvoker_Int32_t478_Byte_t1117,
	RuntimeInvoker_Void_t1548_Int32_t478_Byte_t1117,
	RuntimeInvoker_Enumerator_t2249,
	RuntimeInvoker_Boolean_t536_RaycastResult_t511_RaycastResult_t511,
	RuntimeInvoker_Object_t_RaycastResult_t511_Object_t_Object_t,
	RuntimeInvoker_Object_t_RaycastResult_t511_RaycastResult_t511_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2284,
	RuntimeInvoker_Enumerator_t2289,
	RuntimeInvoker_Boolean_t536_Int16_t1136_Int16_t1136,
	RuntimeInvoker_Object_t_Int16_t1136_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int16_t1136_Int16_t1136_Object_t_Object_t,
	RuntimeInvoker_RaycastHit_t482,
	RuntimeInvoker_KeyValuePair_2_t2328_Int32_t478_Object_t,
	RuntimeInvoker_TweenType_t442_Int32_t478_Object_t,
	RuntimeInvoker_Boolean_t536_Int32_t478_ObjectU26_t3528,
	RuntimeInvoker_TweenType_t442_Object_t,
	RuntimeInvoker_Enumerator_t2333,
	RuntimeInvoker_DictionaryEntry_t552_Int32_t478_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2328,
	RuntimeInvoker_TweenType_t442,
	RuntimeInvoker_Enumerator_t2332,
	RuntimeInvoker_Enumerator_t2336,
	RuntimeInvoker_DictionaryEntry_t552_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2328_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2350_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2364_Object_t_Int32_t478,
	RuntimeInvoker_Enumerator_t2368,
	RuntimeInvoker_DictionaryEntry_t552_Object_t_Int32_t478,
	RuntimeInvoker_KeyValuePair_2_t2364,
	RuntimeInvoker_Enumerator_t2367,
	RuntimeInvoker_Enumerator_t2371,
	RuntimeInvoker_KeyValuePair_2_t2364_Object_t,
	RuntimeInvoker_Boolean_t536_Nullable_1_t550,
	RuntimeInvoker_Space_t546,
	RuntimeInvoker_EaseType_t425,
	RuntimeInvoker_LoopType_t426,
	RuntimeInvoker_Enumerator_t2392,
	RuntimeInvoker_Enumerator_t2400,
	RuntimeInvoker_Object_t_Single_t531_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_Single_t531_Single_t531,
	RuntimeInvoker_Object_t_Single_t531_Single_t531_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2411,
	RuntimeInvoker_Boolean_t536_SByte_t1135_SByte_t1135,
	RuntimeInvoker_Object_t_SByte_t1135_SByte_t1135_Object_t_Object_t,
	RuntimeInvoker_Vector3_t215_Object_t,
	RuntimeInvoker_Enumerator_t2422,
	RuntimeInvoker_Object_t_Vector3_t215_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_Vector3_t215_Vector3_t215,
	RuntimeInvoker_Object_t_Vector3_t215_Vector3_t215_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2431,
	RuntimeInvoker_Object_t_Color_t6_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_Color_t6_Color_t6,
	RuntimeInvoker_Object_t_Color_t6_Color_t6_Object_t_Object_t,
	RuntimeInvoker_Space_t546_Object_t,
	RuntimeInvoker_Enumerator_t2440,
	RuntimeInvoker_EaseType_t425_Object_t,
	RuntimeInvoker_Enumerator_t2449,
	RuntimeInvoker_LoopType_t426_Object_t,
	RuntimeInvoker_Enumerator_t2458,
	RuntimeInvoker_KeyValuePair_2_t2514_Int32_t478_Object_t,
	RuntimeInvoker_Enumerator_t2517,
	RuntimeInvoker_KeyValuePair_2_t2514_Object_t,
	RuntimeInvoker_RaycastHit2D_t789,
	RuntimeInvoker_Object_t_RaycastHit_t482_RaycastHit_t482_Object_t_Object_t,
	RuntimeInvoker_Void_t1548_UIVertex_t685,
	RuntimeInvoker_Boolean_t536_UIVertex_t685,
	RuntimeInvoker_UIVertex_t685_Object_t,
	RuntimeInvoker_Enumerator_t2556,
	RuntimeInvoker_Int32_t478_UIVertex_t685,
	RuntimeInvoker_Void_t1548_Int32_t478_UIVertex_t685,
	RuntimeInvoker_UIVertex_t685_Int32_t478,
	RuntimeInvoker_UIVertex_t685,
	RuntimeInvoker_Boolean_t536_UIVertex_t685_UIVertex_t685,
	RuntimeInvoker_Object_t_UIVertex_t685_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_UIVertex_t685_UIVertex_t685,
	RuntimeInvoker_Object_t_UIVertex_t685_UIVertex_t685_Object_t_Object_t,
	RuntimeInvoker_Object_t_ColorTween_t630,
	RuntimeInvoker_UILineInfo_t809,
	RuntimeInvoker_UICharInfo_t811,
	RuntimeInvoker_Object_t_Vector2_t7_Object_t_Object_t,
	RuntimeInvoker_GcAchievementData_t1020,
	RuntimeInvoker_GcScoreData_t1021,
	RuntimeInvoker_ContactPoint_t930,
	RuntimeInvoker_Keyframe_t943,
	RuntimeInvoker_UICharInfo_t811_Object_t,
	RuntimeInvoker_Enumerator_t2685,
	RuntimeInvoker_Boolean_t536_UICharInfo_t811_UICharInfo_t811,
	RuntimeInvoker_Object_t_UICharInfo_t811_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_UICharInfo_t811_UICharInfo_t811,
	RuntimeInvoker_Object_t_UICharInfo_t811_UICharInfo_t811_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t809_Object_t,
	RuntimeInvoker_Enumerator_t2694,
	RuntimeInvoker_Boolean_t536_UILineInfo_t809_UILineInfo_t809,
	RuntimeInvoker_Object_t_UILineInfo_t809_Object_t_Object_t,
	RuntimeInvoker_Int32_t478_UILineInfo_t809_UILineInfo_t809,
	RuntimeInvoker_Object_t_UILineInfo_t809_UILineInfo_t809_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2706_Object_t_Int64_t1131,
	RuntimeInvoker_Int64_t1131_Object_t_Int64_t1131,
	RuntimeInvoker_Enumerator_t2711,
	RuntimeInvoker_DictionaryEntry_t552_Object_t_Int64_t1131,
	RuntimeInvoker_KeyValuePair_2_t2706,
	RuntimeInvoker_Enumerator_t2710,
	RuntimeInvoker_Object_t_Object_t_Int64_t1131_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2714,
	RuntimeInvoker_KeyValuePair_2_t2706_Object_t,
	RuntimeInvoker_Boolean_t536_Int64_t1131_Int64_t1131,
	RuntimeInvoker_Void_t1548_UInt64_t1134_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2743_UInt64_t1134_Object_t,
	RuntimeInvoker_NetworkID_t979_UInt64_t1134_Object_t,
	RuntimeInvoker_Boolean_t536_UInt64_t1134_ObjectU26_t3528,
	RuntimeInvoker_NetworkID_t979_Object_t,
	RuntimeInvoker_Enumerator_t2748,
	RuntimeInvoker_DictionaryEntry_t552_UInt64_t1134_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2743,
	RuntimeInvoker_Enumerator_t2747,
	RuntimeInvoker_Object_t_UInt64_t1134_Object_t_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2751,
	RuntimeInvoker_KeyValuePair_2_t2743_Object_t,
	RuntimeInvoker_Boolean_t536_UInt64_t1134_UInt64_t1134,
	RuntimeInvoker_KeyValuePair_2_t2762,
	RuntimeInvoker_Void_t1548_Object_t_KeyValuePair_2_t2350,
	RuntimeInvoker_KeyValuePair_2_t2762_Object_t_KeyValuePair_2_t2350,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2350,
	RuntimeInvoker_KeyValuePair_2_t2350_Object_t_KeyValuePair_2_t2350,
	RuntimeInvoker_Boolean_t536_Object_t_KeyValuePair_2U26_t3610,
	RuntimeInvoker_Enumerator_t2791,
	RuntimeInvoker_DictionaryEntry_t552_Object_t_KeyValuePair_2_t2350,
	RuntimeInvoker_Enumerator_t2790,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t2350_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2794,
	RuntimeInvoker_KeyValuePair_2_t2762_Object_t,
	RuntimeInvoker_Boolean_t536_KeyValuePair_2_t2350_KeyValuePair_2_t2350,
	RuntimeInvoker_ParameterModifier_t1805,
	RuntimeInvoker_HitInfo_t1039,
	RuntimeInvoker_TextEditOp_t1057_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2825_Object_t_Int32_t478,
	RuntimeInvoker_TextEditOp_t1057_Object_t_Int32_t478,
	RuntimeInvoker_Boolean_t536_Object_t_TextEditOpU26_t3611,
	RuntimeInvoker_Enumerator_t2830,
	RuntimeInvoker_KeyValuePair_2_t2825,
	RuntimeInvoker_TextEditOp_t1057,
	RuntimeInvoker_Enumerator_t2829,
	RuntimeInvoker_Enumerator_t2833,
	RuntimeInvoker_KeyValuePair_2_t2825_Object_t,
	RuntimeInvoker_ClientCertificateType_t1308,
	RuntimeInvoker_KeyValuePair_2_t2877_Object_t_SByte_t1135,
	RuntimeInvoker_Boolean_t536_Object_t_BooleanU26_t3509,
	RuntimeInvoker_Enumerator_t2881,
	RuntimeInvoker_DictionaryEntry_t552_Object_t_SByte_t1135,
	RuntimeInvoker_KeyValuePair_2_t2877,
	RuntimeInvoker_Enumerator_t2880,
	RuntimeInvoker_Object_t_Object_t_SByte_t1135_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2884,
	RuntimeInvoker_KeyValuePair_2_t2877_Object_t,
	RuntimeInvoker_X509ChainStatus_t1432,
	RuntimeInvoker_KeyValuePair_2_t2896_Int32_t478_Int32_t478,
	RuntimeInvoker_Boolean_t536_Int32_t478_Int32U26_t3510,
	RuntimeInvoker_Enumerator_t2900,
	RuntimeInvoker_DictionaryEntry_t552_Int32_t478_Int32_t478,
	RuntimeInvoker_KeyValuePair_2_t2896,
	RuntimeInvoker_Enumerator_t2899,
	RuntimeInvoker_Enumerator_t2903,
	RuntimeInvoker_KeyValuePair_2_t2896_Object_t,
	RuntimeInvoker_Mark_t1479,
	RuntimeInvoker_UriScheme_t1516,
	RuntimeInvoker_TableRange_t1580,
	RuntimeInvoker_Slot_t1657,
	RuntimeInvoker_Slot_t1664,
	RuntimeInvoker_ILTokenInfo_t1742,
	RuntimeInvoker_LabelData_t1744,
	RuntimeInvoker_LabelFixup_t1743,
	RuntimeInvoker_TypeTag_t1930,
	RuntimeInvoker_Int32_t478_DateTimeOffset_t1148_DateTimeOffset_t1148,
	RuntimeInvoker_Boolean_t536_DateTimeOffset_t1148_DateTimeOffset_t1148,
	RuntimeInvoker_Boolean_t536_Nullable_1_t2187,
	RuntimeInvoker_Int32_t478_Guid_t555_Guid_t555,
	RuntimeInvoker_Boolean_t536_Guid_t555_Guid_t555,
};
