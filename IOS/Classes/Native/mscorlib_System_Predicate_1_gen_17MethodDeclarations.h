﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<iTween/LoopType>
struct Predicate_1_t2463;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Predicate`1<iTween/LoopType>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m16628_gshared (Predicate_1_t2463 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m16628(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2463 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m16628_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<iTween/LoopType>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m16629_gshared (Predicate_1_t2463 * __this, int32_t ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m16629(__this, ___obj, method) (( bool (*) (Predicate_1_t2463 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m16629_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<iTween/LoopType>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m16630_gshared (Predicate_1_t2463 * __this, int32_t ___obj, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m16630(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2463 *, int32_t, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m16630_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<iTween/LoopType>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m16631_gshared (Predicate_1_t2463 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m16631(__this, ___result, method) (( bool (*) (Predicate_1_t2463 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m16631_gshared)(__this, ___result, method)
