﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_PhotoshopFilters
struct CameraFilterPack_Blend2Camera_PhotoshopFilters_t39;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters__ctor_m217 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_PhotoshopFilters::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_PhotoshopFilters_get_material_m218 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::ChangeFilters()
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters_ChangeFilters_m219 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::Start()
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters_Start_m220 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnRenderImage_m221 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnValidate_m222 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::Update()
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters_Update_m223 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnEnable_m224 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PhotoshopFilters::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_PhotoshopFilters_OnDisable_m225 (CameraFilterPack_Blend2Camera_PhotoshopFilters_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
