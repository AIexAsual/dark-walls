﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t221;
// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UI.RawImage
struct  RawImage_t691  : public MaskableGraphic_t670
{
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t221 * ___m_Texture_23;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t225  ___m_UVRect_24;
};
