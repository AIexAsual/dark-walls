﻿#pragma once
#include <stdint.h>
// System.Delegate
struct Delegate_t480;
// System.Object
#include "mscorlib_System_Object.h"
// System.DelegateSerializationHolder
struct  DelegateSerializationHolder_t2093  : public Object_t
{
	// System.Delegate System.DelegateSerializationHolder::_delegate
	Delegate_t480 * ____delegate_0;
};
