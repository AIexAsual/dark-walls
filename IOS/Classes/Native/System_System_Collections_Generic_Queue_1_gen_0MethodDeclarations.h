﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2871;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2280;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Object[]
struct ObjectU5BU5D_t470;
// System.Collections.Generic.Queue`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Queue_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C" void Queue_1__ctor_m22276_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1__ctor_m22276(__this, method) (( void (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1__ctor_m22276_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Queue_1_System_Collections_ICollection_CopyTo_m22277_gshared (Queue_1_t2871 * __this, Array_t * ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_CopyTo_m22277(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2871 *, Array_t *, int32_t, const MethodInfo*))Queue_1_System_Collections_ICollection_CopyTo_m22277_gshared)(__this, ___array, ___idx, method)
// System.Boolean System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Queue_1_System_Collections_ICollection_get_IsSynchronized_m22278_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_IsSynchronized_m22278(__this, method) (( bool (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_IsSynchronized_m22278_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Queue_1_System_Collections_ICollection_get_SyncRoot_m22279_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_ICollection_get_SyncRoot_m22279(__this, method) (( Object_t * (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1_System_Collections_ICollection_get_SyncRoot_m22279_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Queue`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22280_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22280(__this, method) (( Object_t* (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22280_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Queue`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Queue_1_System_Collections_IEnumerable_GetEnumerator_m22281_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1_System_Collections_IEnumerable_GetEnumerator_m22281(__this, method) (( Object_t * (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1_System_Collections_IEnumerable_GetEnumerator_m22281_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Clear()
extern "C" void Queue_1_Clear_m22282_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1_Clear_m22282(__this, method) (( void (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1_Clear_m22282_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Queue_1_CopyTo_m22283_gshared (Queue_1_t2871 * __this, ObjectU5BU5D_t470* ___array, int32_t ___idx, const MethodInfo* method);
#define Queue_1_CopyTo_m22283(__this, ___array, ___idx, method) (( void (*) (Queue_1_t2871 *, ObjectU5BU5D_t470*, int32_t, const MethodInfo*))Queue_1_CopyTo_m22283_gshared)(__this, ___array, ___idx, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(T)
extern "C" void Queue_1_Enqueue_m22284_gshared (Queue_1_t2871 * __this, Object_t * ___item, const MethodInfo* method);
#define Queue_1_Enqueue_m22284(__this, ___item, method) (( void (*) (Queue_1_t2871 *, Object_t *, const MethodInfo*))Queue_1_Enqueue_m22284_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Queue`1<System.Object>::SetCapacity(System.Int32)
extern "C" void Queue_1_SetCapacity_m22285_gshared (Queue_1_t2871 * __this, int32_t ___new_size, const MethodInfo* method);
#define Queue_1_SetCapacity_m22285(__this, ___new_size, method) (( void (*) (Queue_1_t2871 *, int32_t, const MethodInfo*))Queue_1_SetCapacity_m22285_gshared)(__this, ___new_size, method)
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C" int32_t Queue_1_get_Count_m22286_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1_get_Count_m22286(__this, method) (( int32_t (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1_get_Count_m22286_gshared)(__this, method)
// System.Collections.Generic.Queue`1/Enumerator<T> System.Collections.Generic.Queue`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t2872  Queue_1_GetEnumerator_m22287_gshared (Queue_1_t2871 * __this, const MethodInfo* method);
#define Queue_1_GetEnumerator_m22287(__this, method) (( Enumerator_t2872  (*) (Queue_1_t2871 *, const MethodInfo*))Queue_1_GetEnumerator_m22287_gshared)(__this, method)
