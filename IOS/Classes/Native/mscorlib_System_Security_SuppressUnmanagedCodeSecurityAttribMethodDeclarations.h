﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SuppressUnmanagedCodeSecurityAttribute
struct SuppressUnmanagedCodeSecurityAttribute_t2026;

// System.Void System.Security.SuppressUnmanagedCodeSecurityAttribute::.ctor()
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m12254 (SuppressUnmanagedCodeSecurityAttribute_t2026 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
