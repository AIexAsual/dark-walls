﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_VHS_Rewind
struct  CameraFilterPack_TV_VHS_Rewind_t195  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_VHS_Rewind::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_VHS_Rewind::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_TV_VHS_Rewind::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_TV_VHS_Rewind::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_TV_VHS_Rewind::Cryptage
	float ___Cryptage_6;
	// System.Single CameraFilterPack_TV_VHS_Rewind::Parasite
	float ___Parasite_7;
	// System.Single CameraFilterPack_TV_VHS_Rewind::Parasite2
	float ___Parasite2_8;
	// System.Single CameraFilterPack_TV_VHS_Rewind::WhiteParasite
	float ___WhiteParasite_9;
};
struct CameraFilterPack_TV_VHS_Rewind_t195_StaticFields{
	// System.Single CameraFilterPack_TV_VHS_Rewind::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_TV_VHS_Rewind::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_TV_VHS_Rewind::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_TV_VHS_Rewind::ChangeValue4
	float ___ChangeValue4_13;
};
