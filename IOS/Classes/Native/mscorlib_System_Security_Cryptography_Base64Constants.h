﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t469;
// System.Object
#include "mscorlib_System_Object.h"
// System.Security.Cryptography.Base64Constants
struct  Base64Constants_t1965  : public Object_t
{
};
struct Base64Constants_t1965_StaticFields{
	// System.Byte[] System.Security.Cryptography.Base64Constants::EncodeTable
	ByteU5BU5D_t469* ___EncodeTable_0;
	// System.Byte[] System.Security.Cryptography.Base64Constants::DecodeTable
	ByteU5BU5D_t469* ___DecodeTable_1;
};
