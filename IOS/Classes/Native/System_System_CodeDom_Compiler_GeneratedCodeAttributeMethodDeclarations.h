﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.CodeDom.Compiler.GeneratedCodeAttribute
struct GeneratedCodeAttribute_t1377;
// System.String
struct String_t;

// System.Void System.CodeDom.Compiler.GeneratedCodeAttribute::.ctor(System.String,System.String)
extern "C" void GeneratedCodeAttribute__ctor_m7565 (GeneratedCodeAttribute_t1377 * __this, String_t* ___tool, String_t* ___version, const MethodInfo* method) IL2CPP_METHOD_ATTR;
