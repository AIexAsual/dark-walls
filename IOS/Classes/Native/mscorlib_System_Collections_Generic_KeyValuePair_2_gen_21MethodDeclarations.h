﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>
struct KeyValuePair_2_t2757;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t981;
// System.String
struct String_t;
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20964(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2757 *, uint64_t, NetworkAccessToken_t981 *, const MethodInfo*))KeyValuePair_2__ctor_m20869_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Key()
#define KeyValuePair_2_get_Key_m20965(__this, method) (( uint64_t (*) (KeyValuePair_2_t2757 *, const MethodInfo*))KeyValuePair_2_get_Key_m20870_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20966(__this, ___value, method) (( void (*) (KeyValuePair_2_t2757 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m20871_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Value()
#define KeyValuePair_2_get_Value_m20967(__this, method) (( NetworkAccessToken_t981 * (*) (KeyValuePair_2_t2757 *, const MethodInfo*))KeyValuePair_2_get_Value_m20872_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20968(__this, ___value, method) (( void (*) (KeyValuePair_2_t2757 *, NetworkAccessToken_t981 *, const MethodInfo*))KeyValuePair_2_set_Value_m20873_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::ToString()
#define KeyValuePair_2_ToString_m20969(__this, method) (( String_t* (*) (KeyValuePair_2_t2757 *, const MethodInfo*))KeyValuePair_2_ToString_m20874_gshared)(__this, method)
