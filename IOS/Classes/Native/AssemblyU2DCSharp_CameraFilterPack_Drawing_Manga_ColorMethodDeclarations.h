﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Manga_Color
struct CameraFilterPack_Drawing_Manga_Color_t107;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Manga_Color::.ctor()
extern "C" void CameraFilterPack_Drawing_Manga_Color__ctor_m685 (CameraFilterPack_Drawing_Manga_Color_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Manga_Color::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Manga_Color_get_material_m686 (CameraFilterPack_Drawing_Manga_Color_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::Start()
extern "C" void CameraFilterPack_Drawing_Manga_Color_Start_m687 (CameraFilterPack_Drawing_Manga_Color_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Manga_Color_OnRenderImage_m688 (CameraFilterPack_Drawing_Manga_Color_t107 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::OnValidate()
extern "C" void CameraFilterPack_Drawing_Manga_Color_OnValidate_m689 (CameraFilterPack_Drawing_Manga_Color_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::Update()
extern "C" void CameraFilterPack_Drawing_Manga_Color_Update_m690 (CameraFilterPack_Drawing_Manga_Color_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Manga_Color::OnDisable()
extern "C" void CameraFilterPack_Drawing_Manga_Color_OnDisable_m691 (CameraFilterPack_Drawing_Manga_Color_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
