﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<iTweenEvent/TweenType>
struct InternalEnumerator_1_t2330;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// iTweenEvent/TweenType
#include "AssemblyU2DCSharp_iTweenEvent_TweenType.h"

// System.Void System.Array/InternalEnumerator`1<iTweenEvent/TweenType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14841_gshared (InternalEnumerator_1_t2330 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14841(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2330 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14841_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<iTweenEvent/TweenType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14842_gshared (InternalEnumerator_1_t2330 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14842(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2330 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14842_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<iTweenEvent/TweenType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14843_gshared (InternalEnumerator_1_t2330 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14843(__this, method) (( void (*) (InternalEnumerator_1_t2330 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14843_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<iTweenEvent/TweenType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14844_gshared (InternalEnumerator_1_t2330 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14844(__this, method) (( bool (*) (InternalEnumerator_1_t2330 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14844_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<iTweenEvent/TweenType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m14845_gshared (InternalEnumerator_1_t2330 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14845(__this, method) (( int32_t (*) (InternalEnumerator_1_t2330 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14845_gshared)(__this, method)
