﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2876;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1094;
// System.Collections.Generic.ICollection`1<System.Boolean>
struct ICollection_1_t3029;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t2879;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t2883;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t2347;
// System.Collections.Generic.IDictionary`2<System.Object,System.Boolean>
struct IDictionary_2_t3265;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1104;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t3266;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t3267;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1527;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__25.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m22299_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m22299(__this, method) (( void (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2__ctor_m22299_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22300_gshared (Dictionary_2_t2876 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22300(__this, ___comparer, method) (( void (*) (Dictionary_2_t2876 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22300_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m22302_gshared (Dictionary_2_t2876 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m22302(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2876 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22302_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22304_gshared (Dictionary_2_t2876 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m22304(__this, ___capacity, method) (( void (*) (Dictionary_2_t2876 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22304_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22306_gshared (Dictionary_2_t2876 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22306(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2876 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22306_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22308_gshared (Dictionary_2_t2876 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m22308(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2876 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2__ctor_m22308_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22310_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22310(__this, method) (( Object_t* (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22310_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22312_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22312(__this, method) (( Object_t* (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22312_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22314_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22314(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22314_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22316_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22316(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2876 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22316_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22318_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22318(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2876 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22318_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22320_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22320(__this, ___key, method) (( bool (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22320_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22322_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22322(__this, ___key, method) (( void (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22322_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22324_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22324(__this, method) (( bool (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22324_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22326_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22326(__this, method) (( Object_t * (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22326_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22328_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22328(__this, method) (( bool (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22328_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22330_gshared (Dictionary_2_t2876 * __this, KeyValuePair_2_t2877  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22330(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2876 *, KeyValuePair_2_t2877 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22330_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22332_gshared (Dictionary_2_t2876 * __this, KeyValuePair_2_t2877  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22332(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2876 *, KeyValuePair_2_t2877 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22332_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22334_gshared (Dictionary_2_t2876 * __this, KeyValuePair_2U5BU5D_t3266* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22334(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2876 *, KeyValuePair_2U5BU5D_t3266*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22334_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22336_gshared (Dictionary_2_t2876 * __this, KeyValuePair_2_t2877  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22336(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2876 *, KeyValuePair_2_t2877 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22336_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22338_gshared (Dictionary_2_t2876 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22338(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2876 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22338_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22340_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22340(__this, method) (( Object_t * (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22340_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22342_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22342(__this, method) (( Object_t* (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22342_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22344_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22344(__this, method) (( Object_t * (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22344_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22346_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22346(__this, method) (( int32_t (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_get_Count_m22346_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m22348_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22348(__this, ___key, method) (( bool (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m22348_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22350_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22350(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2876 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m22350_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22352_gshared (Dictionary_2_t2876 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22352(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2876 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22352_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22354_gshared (Dictionary_2_t2876 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22354(__this, ___size, method) (( void (*) (Dictionary_2_t2876 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22354_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22356_gshared (Dictionary_2_t2876 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22356(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2876 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22356_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2877  Dictionary_2_make_pair_m22358_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22358(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2877  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m22358_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m22360_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22360(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_key_m22360_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_value(TKey,TValue)
extern "C" bool Dictionary_2_pick_value_m22362_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22362(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_value_m22362_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22364_gshared (Dictionary_2_t2876 * __this, KeyValuePair_2U5BU5D_t3266* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22364(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2876 *, KeyValuePair_2U5BU5D_t3266*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22364_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m22366_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22366(__this, method) (( void (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_Resize_m22366_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22368_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22368(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2876 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m22368_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m22370_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22370(__this, method) (( void (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_Clear_m22370_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22372_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22372(__this, ___key, method) (( bool (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m22372_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22374_gshared (Dictionary_2_t2876 * __this, bool ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22374(__this, ___value, method) (( bool (*) (Dictionary_2_t2876 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m22374_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22376_gshared (Dictionary_2_t2876 * __this, SerializationInfo_t1104 * ___info, StreamingContext_t1105  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22376(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2876 *, SerializationInfo_t1104 *, StreamingContext_t1105 , const MethodInfo*))Dictionary_2_GetObjectData_m22376_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22378_gshared (Dictionary_2_t2876 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22378(__this, ___sender, method) (( void (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22378_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22380_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22380(__this, ___key, method) (( bool (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m22380_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22382_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22382(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2876 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m22382_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Keys()
extern "C" KeyCollection_t2879 * Dictionary_2_get_Keys_m22384_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22384(__this, method) (( KeyCollection_t2879 * (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_get_Keys_m22384_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Values()
extern "C" ValueCollection_t2883 * Dictionary_2_get_Values_m22386_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22386(__this, method) (( ValueCollection_t2883 * (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_get_Values_m22386_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m22388_gshared (Dictionary_2_t2876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22388(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22388_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m22390_gshared (Dictionary_2_t2876 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22390(__this, ___value, method) (( bool (*) (Dictionary_2_t2876 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22390_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22392_gshared (Dictionary_2_t2876 * __this, KeyValuePair_2_t2877  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22392(__this, ___pair, method) (( bool (*) (Dictionary_2_t2876 *, KeyValuePair_2_t2877 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22392_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t2881  Dictionary_2_GetEnumerator_m22394_gshared (Dictionary_2_t2876 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22394(__this, method) (( Enumerator_t2881  (*) (Dictionary_2_t2876 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22394_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t552  Dictionary_2_U3CCopyToU3Em__0_m22396_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22396(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t552  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22396_gshared)(__this /* static, unused */, ___key, ___value, method)
