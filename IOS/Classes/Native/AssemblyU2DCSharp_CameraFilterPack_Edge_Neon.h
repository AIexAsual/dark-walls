﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Edge_Neon
struct  CameraFilterPack_Edge_Neon_t115  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Edge_Neon::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Edge_Neon::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Edge_Neon::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Edge_Neon::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Edge_Neon::EdgeWeight
	float ___EdgeWeight_6;
};
struct CameraFilterPack_Edge_Neon_t115_StaticFields{
	// System.Single CameraFilterPack_Edge_Neon::ChangeEdgeWeight
	float ___ChangeEdgeWeight_7;
};
