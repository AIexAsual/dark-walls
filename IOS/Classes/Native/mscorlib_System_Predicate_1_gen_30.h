﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Selectable
struct Selectable_t636;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Selectable>
struct  Predicate_1_t2602  : public MulticastDelegate_t219
{
};
