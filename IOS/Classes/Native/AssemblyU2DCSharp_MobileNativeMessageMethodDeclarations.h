﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MobileNativeMessage
struct MobileNativeMessage_t280;
// System.String
struct String_t;
// MNDialogResult
#include "AssemblyU2DCSharp_MNDialogResult.h"

// System.Void MobileNativeMessage::.ctor(System.String,System.String)
extern "C" void MobileNativeMessage__ctor_m1740 (MobileNativeMessage_t280 * __this, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::.ctor(System.String,System.String,System.String)
extern "C" void MobileNativeMessage__ctor_m1741 (MobileNativeMessage_t280 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::init(System.String,System.String,System.String)
extern "C" void MobileNativeMessage_init_m1742 (MobileNativeMessage_t280 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::OnCompleteListener(MNDialogResult)
extern "C" void MobileNativeMessage_OnCompleteListener_m1743 (MobileNativeMessage_t280 * __this, int32_t ___res, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MobileNativeMessage::<OnComplete>m__5()
extern "C" void MobileNativeMessage_U3COnCompleteU3Em__5_m1744 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
