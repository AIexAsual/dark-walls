﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Vision_Warp
struct CameraFilterPack_Vision_Warp_t210;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Vision_Warp::.ctor()
extern "C" void CameraFilterPack_Vision_Warp__ctor_m1366 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Warp::get_material()
extern "C" Material_t2 * CameraFilterPack_Vision_Warp_get_material_m1367 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp::Start()
extern "C" void CameraFilterPack_Vision_Warp_Start_m1368 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Vision_Warp_OnRenderImage_m1369 (CameraFilterPack_Vision_Warp_t210 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp::OnValidate()
extern "C" void CameraFilterPack_Vision_Warp_OnValidate_m1370 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp::Update()
extern "C" void CameraFilterPack_Vision_Warp_Update_m1371 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Warp::OnDisable()
extern "C" void CameraFilterPack_Vision_Warp_OnDisable_m1372 (CameraFilterPack_Vision_Warp_t210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
