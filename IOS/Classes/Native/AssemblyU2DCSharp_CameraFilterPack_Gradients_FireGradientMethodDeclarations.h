﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_FireGradient
struct CameraFilterPack_Gradients_FireGradient_t148;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_FireGradient::.ctor()
extern "C" void CameraFilterPack_Gradients_FireGradient__ctor_m959 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_FireGradient::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_FireGradient_get_material_m960 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::Start()
extern "C" void CameraFilterPack_Gradients_FireGradient_Start_m961 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_FireGradient_OnRenderImage_m962 (CameraFilterPack_Gradients_FireGradient_t148 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::Update()
extern "C" void CameraFilterPack_Gradients_FireGradient_Update_m963 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_FireGradient::OnDisable()
extern "C" void CameraFilterPack_Gradients_FireGradient_OnDisable_m964 (CameraFilterPack_Gradients_FireGradient_t148 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
