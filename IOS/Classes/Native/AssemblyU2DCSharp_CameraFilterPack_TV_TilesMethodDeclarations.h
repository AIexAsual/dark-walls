﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Tiles
struct CameraFilterPack_TV_Tiles_t193;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Tiles::.ctor()
extern "C" void CameraFilterPack_TV_Tiles__ctor_m1249 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Tiles::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Tiles_get_material_m1250 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::Start()
extern "C" void CameraFilterPack_TV_Tiles_Start_m1251 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Tiles_OnRenderImage_m1252 (CameraFilterPack_TV_Tiles_t193 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::OnValidate()
extern "C" void CameraFilterPack_TV_Tiles_OnValidate_m1253 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::Update()
extern "C" void CameraFilterPack_TV_Tiles_Update_m1254 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Tiles::OnDisable()
extern "C" void CameraFilterPack_TV_Tiles_OnDisable_m1255 (CameraFilterPack_TV_Tiles_t193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
