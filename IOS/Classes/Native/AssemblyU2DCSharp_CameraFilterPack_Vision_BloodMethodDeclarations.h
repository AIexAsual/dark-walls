﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Vision_Blood
struct CameraFilterPack_Vision_Blood_t205;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Vision_Blood::.ctor()
extern "C" void CameraFilterPack_Vision_Blood__ctor_m1331 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Vision_Blood::get_material()
extern "C" Material_t2 * CameraFilterPack_Vision_Blood_get_material_m1332 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::Start()
extern "C" void CameraFilterPack_Vision_Blood_Start_m1333 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Vision_Blood_OnRenderImage_m1334 (CameraFilterPack_Vision_Blood_t205 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::OnValidate()
extern "C" void CameraFilterPack_Vision_Blood_OnValidate_m1335 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::Update()
extern "C" void CameraFilterPack_Vision_Blood_Update_m1336 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Vision_Blood::OnDisable()
extern "C" void CameraFilterPack_Vision_Blood_OnDisable_m1337 (CameraFilterPack_Vision_Blood_t205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
