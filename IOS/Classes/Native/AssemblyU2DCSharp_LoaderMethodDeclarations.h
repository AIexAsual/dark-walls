﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Loader
struct Loader_t346;

// System.Void Loader::.ctor()
extern "C" void Loader__ctor_m2026 (Loader_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader::Start()
extern "C" void Loader_Start_m2027 (Loader_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader::Update()
extern "C" void Loader_Update_m2028 (Loader_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader::Unload()
extern "C" void Loader_Unload_m2029 (Loader_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader::Load()
extern "C" void Loader_Load_m2030 (Loader_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader::changeVal(System.Single)
extern "C" void Loader_changeVal_m2031 (Loader_t346 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader::doneLoading()
extern "C" void Loader_doneLoading_m2032 (Loader_t346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
