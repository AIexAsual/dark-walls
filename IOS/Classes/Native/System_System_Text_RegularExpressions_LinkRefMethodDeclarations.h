﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkRef
struct LinkRef_t1473;

// System.Void System.Text.RegularExpressions.LinkRef::.ctor()
extern "C" void LinkRef__ctor_m8082 (LinkRef_t1473 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
