﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.Object,System.Char>
struct Func_2_t529;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.Object,System.Char>::.ctor(System.Object,System.IntPtr)
extern "C" void Func_2__ctor_m14322_gshared (Func_2_t529 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Func_2__ctor_m14322(__this, ___object, ___method, method) (( void (*) (Func_2_t529 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m14322_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.Object,System.Char>::Invoke(T)
extern "C" uint16_t Func_2_Invoke_m14324_gshared (Func_2_t529 * __this, Object_t * ___arg1, const MethodInfo* method);
#define Func_2_Invoke_m14324(__this, ___arg1, method) (( uint16_t (*) (Func_2_t529 *, Object_t *, const MethodInfo*))Func_2_Invoke_m14324_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.Object,System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Func_2_BeginInvoke_m14326_gshared (Func_2_t529 * __this, Object_t * ___arg1, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Func_2_BeginInvoke_m14326(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t529 *, Object_t *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m14326_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.Object,System.Char>::EndInvoke(System.IAsyncResult)
extern "C" uint16_t Func_2_EndInvoke_m14328_gshared (Func_2_t529 * __this, Object_t * ___result, const MethodInfo* method);
#define Func_2_EndInvoke_m14328(__this, ___result, method) (( uint16_t (*) (Func_2_t529 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m14328_gshared)(__this, ___result, method)
