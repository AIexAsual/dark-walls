﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Distorted
struct CameraFilterPack_TV_Distorted_t184;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Distorted::.ctor()
extern "C" void CameraFilterPack_TV_Distorted__ctor_m1189 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Distorted::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Distorted_get_material_m1190 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::Start()
extern "C" void CameraFilterPack_TV_Distorted_Start_m1191 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Distorted_OnRenderImage_m1192 (CameraFilterPack_TV_Distorted_t184 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::OnValidate()
extern "C" void CameraFilterPack_TV_Distorted_OnValidate_m1193 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::Update()
extern "C" void CameraFilterPack_TV_Distorted_Update_m1194 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Distorted::OnDisable()
extern "C" void CameraFilterPack_TV_Distorted_OnDisable_m1195 (CameraFilterPack_TV_Distorted_t184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
