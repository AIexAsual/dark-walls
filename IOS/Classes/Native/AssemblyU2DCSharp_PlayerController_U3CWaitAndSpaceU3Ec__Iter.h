﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// PlayerController
struct PlayerController_t356;
// System.Object
#include "mscorlib_System_Object.h"
// PlayerController/<WaitAndSpace>c__Iterator15
struct  U3CWaitAndSpaceU3Ec__Iterator15_t395  : public Object_t
{
	// System.Single PlayerController/<WaitAndSpace>c__Iterator15::time
	float ___time_0;
	// System.Int32 PlayerController/<WaitAndSpace>c__Iterator15::$PC
	int32_t ___U24PC_1;
	// System.Object PlayerController/<WaitAndSpace>c__Iterator15::$current
	Object_t * ___U24current_2;
	// System.Single PlayerController/<WaitAndSpace>c__Iterator15::<$>time
	float ___U3CU24U3Etime_3;
	// PlayerController PlayerController/<WaitAndSpace>c__Iterator15::<>f__this
	PlayerController_t356 * ___U3CU3Ef__this_4;
};
