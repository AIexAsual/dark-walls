﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t611;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>
struct  Predicate_1_t2497  : public MulticastDelegate_t219
{
};
