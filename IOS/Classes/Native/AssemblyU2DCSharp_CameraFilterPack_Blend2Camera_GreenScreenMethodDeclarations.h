﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_GreenScreen
struct CameraFilterPack_Blend2Camera_GreenScreen_t26;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_GreenScreen::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_GreenScreen__ctor_m122 (CameraFilterPack_Blend2Camera_GreenScreen_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_GreenScreen::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_GreenScreen_get_material_m123 (CameraFilterPack_Blend2Camera_GreenScreen_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::Start()
extern "C" void CameraFilterPack_Blend2Camera_GreenScreen_Start_m124 (CameraFilterPack_Blend2Camera_GreenScreen_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_GreenScreen_OnRenderImage_m125 (CameraFilterPack_Blend2Camera_GreenScreen_t26 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::Update()
extern "C" void CameraFilterPack_Blend2Camera_GreenScreen_Update_m126 (CameraFilterPack_Blend2Camera_GreenScreen_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_GreenScreen_OnEnable_m127 (CameraFilterPack_Blend2Camera_GreenScreen_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_GreenScreen::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_GreenScreen_OnDisable_m128 (CameraFilterPack_Blend2Camera_GreenScreen_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
