﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AICharacterControl
struct AICharacterControl_t308;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t307;
// UnityEngine.Transform
struct Transform_t243;

// System.Void AICharacterControl::.ctor()
extern "C" void AICharacterControl__ctor_m1857 (AICharacterControl_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NavMeshAgent AICharacterControl::get_agent()
extern "C" NavMeshAgent_t307 * AICharacterControl_get_agent_m1858 (AICharacterControl_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICharacterControl::set_agent(UnityEngine.NavMeshAgent)
extern "C" void AICharacterControl_set_agent_m1859 (AICharacterControl_t308 * __this, NavMeshAgent_t307 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICharacterControl::Start()
extern "C" void AICharacterControl_Start_m1860 (AICharacterControl_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICharacterControl::Update()
extern "C" void AICharacterControl_Update_m1861 (AICharacterControl_t308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AICharacterControl::SetTarget(UnityEngine.Transform)
extern "C" void AICharacterControl_SetTarget_m1862 (AICharacterControl_t308 * __this, Transform_t243 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
