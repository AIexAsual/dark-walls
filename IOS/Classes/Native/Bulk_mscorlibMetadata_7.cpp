﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
// Metadata Definition System.Threading.Thread
extern TypeInfo Thread_t1859_il2cpp_TypeInfo;
// System.Threading.Thread
#include "mscorlib_System_Threading_ThreadMethodDeclarations.h"
static const EncodedMethodIndex Thread_t1859_VTable[4] = 
{
	626,
	4215,
	4216,
	628,
};
extern const Il2CppType _Thread_t3487_0_0_0;
static const Il2CppType* Thread_t1859_InterfacesTypeInfos[] = 
{
	&_Thread_t3487_0_0_0,
};
static Il2CppInterfaceOffsetPair Thread_t1859_InterfacesOffsets[] = 
{
	{ &_Thread_t3487_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Thread_t1859_0_0_0;
extern const Il2CppType Thread_t1859_1_0_0;
extern const Il2CppType CriticalFinalizerObject_t1823_0_0_0;
struct Thread_t1859;
const Il2CppTypeDefinitionMetadata Thread_t1859_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Thread_t1859_InterfacesTypeInfos/* implementedInterfaces */
	, Thread_t1859_InterfacesOffsets/* interfaceOffsets */
	, &CriticalFinalizerObject_t1823_0_0_0/* parent */
	, Thread_t1859_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8954/* fieldStart */
	, 13017/* methodStart */
	, -1/* eventStart */
	, 2554/* propertyStart */

};
TypeInfo Thread_t1859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Thread"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Thread_t1859_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3197/* custom_attributes_cache */
	, &Thread_t1859_0_0_0/* byval_arg */
	, &Thread_t1859_1_0_0/* this_arg */
	, &Thread_t1859_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Thread_t1859)/* instance_size */
	, sizeof (Thread_t1859)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Thread_t1859_StaticFields)/* static_fields_size */
	, sizeof(Thread_t1859_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 30/* method_count */
	, 8/* property_count */
	, 52/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.ThreadAbortException
#include "mscorlib_System_Threading_ThreadAbortException.h"
// Metadata Definition System.Threading.ThreadAbortException
extern TypeInfo ThreadAbortException_t2061_il2cpp_TypeInfo;
// System.Threading.ThreadAbortException
#include "mscorlib_System_Threading_ThreadAbortExceptionMethodDeclarations.h"
static const EncodedMethodIndex ThreadAbortException_t2061_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair ThreadAbortException_t2061_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadAbortException_t2061_0_0_0;
extern const Il2CppType ThreadAbortException_t2061_1_0_0;
extern const Il2CppType SystemException_t1536_0_0_0;
struct ThreadAbortException_t2061;
const Il2CppTypeDefinitionMetadata ThreadAbortException_t2061_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadAbortException_t2061_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, ThreadAbortException_t2061_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13047/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadAbortException_t2061_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadAbortException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadAbortException_t2061_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3207/* custom_attributes_cache */
	, &ThreadAbortException_t2061_0_0_0/* byval_arg */
	, &ThreadAbortException_t2061_1_0_0/* this_arg */
	, &ThreadAbortException_t2061_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadAbortException_t2061)/* instance_size */
	, sizeof (ThreadAbortException_t2061)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.ThreadInterruptedException
#include "mscorlib_System_Threading_ThreadInterruptedException.h"
// Metadata Definition System.Threading.ThreadInterruptedException
extern TypeInfo ThreadInterruptedException_t2062_il2cpp_TypeInfo;
// System.Threading.ThreadInterruptedException
#include "mscorlib_System_Threading_ThreadInterruptedExceptionMethodDeclarations.h"
static const EncodedMethodIndex ThreadInterruptedException_t2062_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair ThreadInterruptedException_t2062_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadInterruptedException_t2062_0_0_0;
extern const Il2CppType ThreadInterruptedException_t2062_1_0_0;
struct ThreadInterruptedException_t2062;
const Il2CppTypeDefinitionMetadata ThreadInterruptedException_t2062_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadInterruptedException_t2062_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, ThreadInterruptedException_t2062_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13049/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadInterruptedException_t2062_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadInterruptedException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadInterruptedException_t2062_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3208/* custom_attributes_cache */
	, &ThreadInterruptedException_t2062_0_0_0/* byval_arg */
	, &ThreadInterruptedException_t2062_1_0_0/* this_arg */
	, &ThreadInterruptedException_t2062_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadInterruptedException_t2062)/* instance_size */
	, sizeof (ThreadInterruptedException_t2062)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.ThreadPool
#include "mscorlib_System_Threading_ThreadPool.h"
// Metadata Definition System.Threading.ThreadPool
extern TypeInfo ThreadPool_t2063_il2cpp_TypeInfo;
// System.Threading.ThreadPool
#include "mscorlib_System_Threading_ThreadPoolMethodDeclarations.h"
static const EncodedMethodIndex ThreadPool_t2063_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadPool_t2063_0_0_0;
extern const Il2CppType ThreadPool_t2063_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct ThreadPool_t2063;
const Il2CppTypeDefinitionMetadata ThreadPool_t2063_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ThreadPool_t2063_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13051/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadPool_t2063_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadPool"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadPool_t2063_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThreadPool_t2063_0_0_0/* byval_arg */
	, &ThreadPool_t2063_1_0_0/* this_arg */
	, &ThreadPool_t2063_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadPool_t2063)/* instance_size */
	, sizeof (ThreadPool_t2063)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.ThreadState
#include "mscorlib_System_Threading_ThreadState.h"
// Metadata Definition System.Threading.ThreadState
extern TypeInfo ThreadState_t2064_il2cpp_TypeInfo;
// System.Threading.ThreadState
#include "mscorlib_System_Threading_ThreadStateMethodDeclarations.h"
static const EncodedMethodIndex ThreadState_t2064_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair ThreadState_t2064_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadState_t2064_0_0_0;
extern const Il2CppType ThreadState_t2064_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata ThreadState_t2064_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadState_t2064_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ThreadState_t2064_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9006/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadState_t2064_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadState"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3209/* custom_attributes_cache */
	, &ThreadState_t2064_0_0_0/* byval_arg */
	, &ThreadState_t2064_1_0_0/* this_arg */
	, &ThreadState_t2064_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadState_t2064)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ThreadState_t2064)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Threading.ThreadStateException
#include "mscorlib_System_Threading_ThreadStateException.h"
// Metadata Definition System.Threading.ThreadStateException
extern TypeInfo ThreadStateException_t2065_il2cpp_TypeInfo;
// System.Threading.ThreadStateException
#include "mscorlib_System_Threading_ThreadStateExceptionMethodDeclarations.h"
static const EncodedMethodIndex ThreadStateException_t2065_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair ThreadStateException_t2065_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStateException_t2065_0_0_0;
extern const Il2CppType ThreadStateException_t2065_1_0_0;
struct ThreadStateException_t2065;
const Il2CppTypeDefinitionMetadata ThreadStateException_t2065_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStateException_t2065_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, ThreadStateException_t2065_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13052/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadStateException_t2065_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStateException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ThreadStateException_t2065_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3210/* custom_attributes_cache */
	, &ThreadStateException_t2065_0_0_0/* byval_arg */
	, &ThreadStateException_t2065_1_0_0/* this_arg */
	, &ThreadStateException_t2065_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStateException_t2065)/* instance_size */
	, sizeof (ThreadStateException_t2065)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Threading.Timer
#include "mscorlib_System_Threading_Timer.h"
// Metadata Definition System.Threading.Timer
extern TypeInfo Timer_t1861_il2cpp_TypeInfo;
// System.Threading.Timer
#include "mscorlib_System_Threading_TimerMethodDeclarations.h"
extern const Il2CppType TimerComparer_t2066_0_0_0;
extern const Il2CppType Scheduler_t2067_0_0_0;
static const Il2CppType* Timer_t1861_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TimerComparer_t2066_0_0_0,
	&Scheduler_t2067_0_0_0,
};
static const EncodedMethodIndex Timer_t1861_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4217,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* Timer_t1861_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair Timer_t1861_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Timer_t1861_0_0_0;
extern const Il2CppType Timer_t1861_1_0_0;
extern const Il2CppType MarshalByRefObject_t1415_0_0_0;
struct Timer_t1861;
const Il2CppTypeDefinitionMetadata Timer_t1861_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Timer_t1861_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Timer_t1861_InterfacesTypeInfos/* implementedInterfaces */
	, Timer_t1861_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, Timer_t1861_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9017/* fieldStart */
	, 13054/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Timer_t1861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Timer"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Timer_t1861_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3211/* custom_attributes_cache */
	, &Timer_t1861_0_0_0/* byval_arg */
	, &Timer_t1861_1_0_0/* this_arg */
	, &Timer_t1861_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Timer_t1861)/* instance_size */
	, sizeof (Timer_t1861)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Timer_t1861_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Timer/TimerComparer
#include "mscorlib_System_Threading_Timer_TimerComparer.h"
// Metadata Definition System.Threading.Timer/TimerComparer
extern TypeInfo TimerComparer_t2066_il2cpp_TypeInfo;
// System.Threading.Timer/TimerComparer
#include "mscorlib_System_Threading_Timer_TimerComparerMethodDeclarations.h"
static const EncodedMethodIndex TimerComparer_t2066_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4218,
};
extern const Il2CppType IComparer_t416_0_0_0;
static const Il2CppType* TimerComparer_t2066_InterfacesTypeInfos[] = 
{
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair TimerComparer_t2066_InterfacesOffsets[] = 
{
	{ &IComparer_t416_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimerComparer_t2066_1_0_0;
struct TimerComparer_t2066;
const Il2CppTypeDefinitionMetadata TimerComparer_t2066_DefinitionMetadata = 
{
	&Timer_t1861_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, TimerComparer_t2066_InterfacesTypeInfos/* implementedInterfaces */
	, TimerComparer_t2066_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimerComparer_t2066_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13058/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TimerComparer_t2066_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimerComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &TimerComparer_t2066_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimerComparer_t2066_0_0_0/* byval_arg */
	, &TimerComparer_t2066_1_0_0/* this_arg */
	, &TimerComparer_t2066_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimerComparer_t2066)/* instance_size */
	, sizeof (TimerComparer_t2066)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Timer/Scheduler
#include "mscorlib_System_Threading_Timer_Scheduler.h"
// Metadata Definition System.Threading.Timer/Scheduler
extern TypeInfo Scheduler_t2067_il2cpp_TypeInfo;
// System.Threading.Timer/Scheduler
#include "mscorlib_System_Threading_Timer_SchedulerMethodDeclarations.h"
static const EncodedMethodIndex Scheduler_t2067_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Scheduler_t2067_1_0_0;
struct Scheduler_t2067;
const Il2CppTypeDefinitionMetadata Scheduler_t2067_DefinitionMetadata = 
{
	&Timer_t1861_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Scheduler_t2067_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9024/* fieldStart */
	, 13060/* methodStart */
	, -1/* eventStart */
	, 2562/* propertyStart */

};
TypeInfo Scheduler_t2067_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Scheduler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Scheduler_t2067_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Scheduler_t2067_0_0_0/* byval_arg */
	, &Scheduler_t2067_1_0_0/* this_arg */
	, &Scheduler_t2067_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Scheduler_t2067)/* instance_size */
	, sizeof (Scheduler_t2067)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Scheduler_t2067_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandle.h"
// Metadata Definition System.Threading.WaitHandle
extern TypeInfo WaitHandle_t1335_il2cpp_TypeInfo;
// System.Threading.WaitHandle
#include "mscorlib_System_Threading_WaitHandleMethodDeclarations.h"
static const EncodedMethodIndex WaitHandle_t1335_VTable[10] = 
{
	626,
	4207,
	627,
	628,
	4208,
	4209,
	4210,
	4211,
	4212,
	4213,
};
static const Il2CppType* WaitHandle_t1335_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair WaitHandle_t1335_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WaitHandle_t1335_0_0_0;
extern const Il2CppType WaitHandle_t1335_1_0_0;
struct WaitHandle_t1335;
const Il2CppTypeDefinitionMetadata WaitHandle_t1335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WaitHandle_t1335_InterfacesTypeInfos/* implementedInterfaces */
	, WaitHandle_t1335_InterfacesOffsets/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, WaitHandle_t1335_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9026/* fieldStart */
	, 13069/* methodStart */
	, -1/* eventStart */
	, 2563/* propertyStart */

};
TypeInfo WaitHandle_t1335_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WaitHandle"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &WaitHandle_t1335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3212/* custom_attributes_cache */
	, &WaitHandle_t1335_0_0_0/* byval_arg */
	, &WaitHandle_t1335_1_0_0/* this_arg */
	, &WaitHandle_t1335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WaitHandle_t1335)/* instance_size */
	, sizeof (WaitHandle_t1335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WaitHandle_t1335_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.AccessViolationException
#include "mscorlib_System_AccessViolationException.h"
// Metadata Definition System.AccessViolationException
extern TypeInfo AccessViolationException_t2069_il2cpp_TypeInfo;
// System.AccessViolationException
#include "mscorlib_System_AccessViolationExceptionMethodDeclarations.h"
static const EncodedMethodIndex AccessViolationException_t2069_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair AccessViolationException_t2069_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AccessViolationException_t2069_0_0_0;
extern const Il2CppType AccessViolationException_t2069_1_0_0;
struct AccessViolationException_t2069;
const Il2CppTypeDefinitionMetadata AccessViolationException_t2069_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AccessViolationException_t2069_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, AccessViolationException_t2069_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13080/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AccessViolationException_t2069_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AccessViolationException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AccessViolationException_t2069_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3214/* custom_attributes_cache */
	, &AccessViolationException_t2069_0_0_0/* byval_arg */
	, &AccessViolationException_t2069_1_0_0/* this_arg */
	, &AccessViolationException_t2069_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AccessViolationException_t2069)/* instance_size */
	, sizeof (AccessViolationException_t2069)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ActivationContext
#include "mscorlib_System_ActivationContext.h"
// Metadata Definition System.ActivationContext
extern TypeInfo ActivationContext_t2070_il2cpp_TypeInfo;
// System.ActivationContext
#include "mscorlib_System_ActivationContextMethodDeclarations.h"
static const EncodedMethodIndex ActivationContext_t2070_VTable[6] = 
{
	626,
	4219,
	627,
	628,
	4220,
	4221,
};
static const Il2CppType* ActivationContext_t2070_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair ActivationContext_t2070_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationContext_t2070_0_0_0;
extern const Il2CppType ActivationContext_t2070_1_0_0;
struct ActivationContext_t2070;
const Il2CppTypeDefinitionMetadata ActivationContext_t2070_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ActivationContext_t2070_InterfacesTypeInfos/* implementedInterfaces */
	, ActivationContext_t2070_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationContext_t2070_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9030/* fieldStart */
	, 13082/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ActivationContext_t2070_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationContext"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ActivationContext_t2070_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3215/* custom_attributes_cache */
	, &ActivationContext_t2070_0_0_0/* byval_arg */
	, &ActivationContext_t2070_1_0_0/* this_arg */
	, &ActivationContext_t2070_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationContext_t2070)/* instance_size */
	, sizeof (ActivationContext_t2070)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Activator
#include "mscorlib_System_Activator.h"
// Metadata Definition System.Activator
extern TypeInfo Activator_t2071_il2cpp_TypeInfo;
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
extern const Il2CppType Activator_CreateInstance_m25054_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition Activator_CreateInstance_m25054_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, 5468 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5468 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
static const EncodedMethodIndex Activator_t2071_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern const Il2CppType _Activator_t3486_0_0_0;
static const Il2CppType* Activator_t2071_InterfacesTypeInfos[] = 
{
	&_Activator_t3486_0_0_0,
};
static Il2CppInterfaceOffsetPair Activator_t2071_InterfacesOffsets[] = 
{
	{ &_Activator_t3486_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Activator_t2071_0_0_0;
extern const Il2CppType Activator_t2071_1_0_0;
struct Activator_t2071;
const Il2CppTypeDefinitionMetadata Activator_t2071_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Activator_t2071_InterfacesTypeInfos/* implementedInterfaces */
	, Activator_t2071_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Activator_t2071_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13086/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Activator_t2071_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Activator"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Activator_t2071_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3217/* custom_attributes_cache */
	, &Activator_t2071_0_0_0/* byval_arg */
	, &Activator_t2071_1_0_0/* this_arg */
	, &Activator_t2071_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Activator_t2071)/* instance_size */
	, sizeof (Activator_t2071)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.AppDomain
#include "mscorlib_System_AppDomain.h"
// Metadata Definition System.AppDomain
extern TypeInfo AppDomain_t2021_il2cpp_TypeInfo;
// System.AppDomain
#include "mscorlib_System_AppDomainMethodDeclarations.h"
static const EncodedMethodIndex AppDomain_t2021_VTable[4] = 
{
	626,
	601,
	627,
	4222,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomain_t2021_0_0_0;
extern const Il2CppType AppDomain_t2021_1_0_0;
struct AppDomain_t2021;
const Il2CppTypeDefinitionMetadata AppDomain_t2021_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, AppDomain_t2021_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9031/* fieldStart */
	, 13095/* methodStart */
	, -1/* eventStart */
	, 2564/* propertyStart */

};
TypeInfo AppDomain_t2021_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomain"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AppDomain_t2021_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3219/* custom_attributes_cache */
	, &AppDomain_t2021_0_0_0/* byval_arg */
	, &AppDomain_t2021_1_0_0/* this_arg */
	, &AppDomain_t2021_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomain_t2021)/* instance_size */
	, sizeof (AppDomain_t2021)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AppDomain_t2021_StaticFields)/* static_fields_size */
	, sizeof(AppDomain_t2021_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AppDomainManager
#include "mscorlib_System_AppDomainManager.h"
// Metadata Definition System.AppDomainManager
extern TypeInfo AppDomainManager_t2072_il2cpp_TypeInfo;
// System.AppDomainManager
#include "mscorlib_System_AppDomainManagerMethodDeclarations.h"
static const EncodedMethodIndex AppDomainManager_t2072_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainManager_t2072_0_0_0;
extern const Il2CppType AppDomainManager_t2072_1_0_0;
struct AppDomainManager_t2072;
const Il2CppTypeDefinitionMetadata AppDomainManager_t2072_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, AppDomainManager_t2072_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppDomainManager_t2072_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainManager"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AppDomainManager_t2072_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3224/* custom_attributes_cache */
	, &AppDomainManager_t2072_0_0_0/* byval_arg */
	, &AppDomainManager_t2072_1_0_0/* this_arg */
	, &AppDomainManager_t2072_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainManager_t2072)/* instance_size */
	, sizeof (AppDomainManager_t2072)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AppDomainSetup
#include "mscorlib_System_AppDomainSetup.h"
// Metadata Definition System.AppDomainSetup
extern TypeInfo AppDomainSetup_t2079_il2cpp_TypeInfo;
// System.AppDomainSetup
#include "mscorlib_System_AppDomainSetupMethodDeclarations.h"
static const EncodedMethodIndex AppDomainSetup_t2079_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainSetup_t2079_0_0_0;
extern const Il2CppType AppDomainSetup_t2079_1_0_0;
struct AppDomainSetup_t2079;
const Il2CppTypeDefinitionMetadata AppDomainSetup_t2079_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AppDomainSetup_t2079_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9052/* fieldStart */
	, 13107/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AppDomainSetup_t2079_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainSetup"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AppDomainSetup_t2079_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3225/* custom_attributes_cache */
	, &AppDomainSetup_t2079_0_0_0/* byval_arg */
	, &AppDomainSetup_t2079_1_0_0/* this_arg */
	, &AppDomainSetup_t2079_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainSetup_t2079)/* instance_size */
	, sizeof (AppDomainSetup_t2079)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ApplicationException
#include "mscorlib_System_ApplicationException.h"
// Metadata Definition System.ApplicationException
extern TypeInfo ApplicationException_t2080_il2cpp_TypeInfo;
// System.ApplicationException
#include "mscorlib_System_ApplicationExceptionMethodDeclarations.h"
static const EncodedMethodIndex ApplicationException_t2080_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair ApplicationException_t2080_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ApplicationException_t2080_0_0_0;
extern const Il2CppType ApplicationException_t2080_1_0_0;
extern const Il2CppType Exception_t520_0_0_0;
struct ApplicationException_t2080;
const Il2CppTypeDefinitionMetadata ApplicationException_t2080_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ApplicationException_t2080_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, ApplicationException_t2080_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13108/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ApplicationException_t2080_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ApplicationException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ApplicationException_t2080_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3226/* custom_attributes_cache */
	, &ApplicationException_t2080_0_0_0/* byval_arg */
	, &ApplicationException_t2080_1_0_0/* this_arg */
	, &ApplicationException_t2080_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ApplicationException_t2080)/* instance_size */
	, sizeof (ApplicationException_t2080)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ApplicationIdentity
#include "mscorlib_System_ApplicationIdentity.h"
// Metadata Definition System.ApplicationIdentity
extern TypeInfo ApplicationIdentity_t2073_il2cpp_TypeInfo;
// System.ApplicationIdentity
#include "mscorlib_System_ApplicationIdentityMethodDeclarations.h"
static const EncodedMethodIndex ApplicationIdentity_t2073_VTable[5] = 
{
	626,
	601,
	627,
	4223,
	4224,
};
static const Il2CppType* ApplicationIdentity_t2073_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair ApplicationIdentity_t2073_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ApplicationIdentity_t2073_0_0_0;
extern const Il2CppType ApplicationIdentity_t2073_1_0_0;
struct ApplicationIdentity_t2073;
const Il2CppTypeDefinitionMetadata ApplicationIdentity_t2073_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ApplicationIdentity_t2073_InterfacesTypeInfos/* implementedInterfaces */
	, ApplicationIdentity_t2073_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ApplicationIdentity_t2073_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9074/* fieldStart */
	, 13111/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ApplicationIdentity_t2073_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ApplicationIdentity"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ApplicationIdentity_t2073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3227/* custom_attributes_cache */
	, &ApplicationIdentity_t2073_0_0_0/* byval_arg */
	, &ApplicationIdentity_t2073_1_0_0/* this_arg */
	, &ApplicationIdentity_t2073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ApplicationIdentity_t2073)/* instance_size */
	, sizeof (ApplicationIdentity_t2073)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// Metadata Definition System.ArgumentException
extern TypeInfo ArgumentException_t556_il2cpp_TypeInfo;
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArgumentException_t556_VTable[12] = 
{
	626,
	601,
	627,
	1316,
	4098,
	1318,
	4099,
	1320,
	1321,
	4098,
	1322,
	4100,
};
static Il2CppInterfaceOffsetPair ArgumentException_t556_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgumentException_t556_0_0_0;
extern const Il2CppType ArgumentException_t556_1_0_0;
struct ArgumentException_t556;
const Il2CppTypeDefinitionMetadata ArgumentException_t556_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgumentException_t556_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, ArgumentException_t556_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9075/* fieldStart */
	, 13113/* methodStart */
	, -1/* eventStart */
	, 2565/* propertyStart */

};
TypeInfo ArgumentException_t556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgumentException_t556_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3229/* custom_attributes_cache */
	, &ArgumentException_t556_0_0_0/* byval_arg */
	, &ArgumentException_t556_1_0_0/* this_arg */
	, &ArgumentException_t556_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentException_t556)/* instance_size */
	, sizeof (ArgumentException_t556)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// Metadata Definition System.ArgumentNullException
extern TypeInfo ArgumentNullException_t1128_il2cpp_TypeInfo;
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArgumentNullException_t1128_VTable[12] = 
{
	626,
	601,
	627,
	1316,
	4098,
	1318,
	4099,
	1320,
	1321,
	4098,
	1322,
	4100,
};
static Il2CppInterfaceOffsetPair ArgumentNullException_t1128_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgumentNullException_t1128_0_0_0;
extern const Il2CppType ArgumentNullException_t1128_1_0_0;
struct ArgumentNullException_t1128;
const Il2CppTypeDefinitionMetadata ArgumentNullException_t1128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgumentNullException_t1128_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t556_0_0_0/* parent */
	, ArgumentNullException_t1128_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9077/* fieldStart */
	, 13122/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArgumentNullException_t1128_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentNullException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgumentNullException_t1128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3230/* custom_attributes_cache */
	, &ArgumentNullException_t1128_0_0_0/* byval_arg */
	, &ArgumentNullException_t1128_1_0_0/* this_arg */
	, &ArgumentNullException_t1128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentNullException_t1128)/* instance_size */
	, sizeof (ArgumentNullException_t1128)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Metadata Definition System.ArgumentOutOfRangeException
extern TypeInfo ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArgumentOutOfRangeException_t1130_VTable[12] = 
{
	626,
	601,
	627,
	1316,
	4225,
	1318,
	4226,
	1320,
	1321,
	4225,
	1322,
	4100,
};
static Il2CppInterfaceOffsetPair ArgumentOutOfRangeException_t1130_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArgumentOutOfRangeException_t1130_0_0_0;
extern const Il2CppType ArgumentOutOfRangeException_t1130_1_0_0;
struct ArgumentOutOfRangeException_t1130;
const Il2CppTypeDefinitionMetadata ArgumentOutOfRangeException_t1130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArgumentOutOfRangeException_t1130_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t556_0_0_0/* parent */
	, ArgumentOutOfRangeException_t1130_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9078/* fieldStart */
	, 13126/* methodStart */
	, -1/* eventStart */
	, 2567/* propertyStart */

};
TypeInfo ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentOutOfRangeException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArgumentOutOfRangeException_t1130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3231/* custom_attributes_cache */
	, &ArgumentOutOfRangeException_t1130_0_0_0/* byval_arg */
	, &ArgumentOutOfRangeException_t1130_1_0_0/* this_arg */
	, &ArgumentOutOfRangeException_t1130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentOutOfRangeException_t1130)/* instance_size */
	, sizeof (ArgumentOutOfRangeException_t1130)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArithmeticException
#include "mscorlib_System_ArithmeticException.h"
// Metadata Definition System.ArithmeticException
extern TypeInfo ArithmeticException_t1336_il2cpp_TypeInfo;
// System.ArithmeticException
#include "mscorlib_System_ArithmeticExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArithmeticException_t1336_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair ArithmeticException_t1336_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArithmeticException_t1336_0_0_0;
extern const Il2CppType ArithmeticException_t1336_1_0_0;
struct ArithmeticException_t1336;
const Il2CppTypeDefinitionMetadata ArithmeticException_t1336_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArithmeticException_t1336_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, ArithmeticException_t1336_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13133/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArithmeticException_t1336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArithmeticException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArithmeticException_t1336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3232/* custom_attributes_cache */
	, &ArithmeticException_t1336_0_0_0/* byval_arg */
	, &ArithmeticException_t1336_1_0_0/* this_arg */
	, &ArithmeticException_t1336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArithmeticException_t1336)/* instance_size */
	, sizeof (ArithmeticException_t1336)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ArrayTypeMismatchException
#include "mscorlib_System_ArrayTypeMismatchException.h"
// Metadata Definition System.ArrayTypeMismatchException
extern TypeInfo ArrayTypeMismatchException_t2081_il2cpp_TypeInfo;
// System.ArrayTypeMismatchException
#include "mscorlib_System_ArrayTypeMismatchExceptionMethodDeclarations.h"
static const EncodedMethodIndex ArrayTypeMismatchException_t2081_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair ArrayTypeMismatchException_t2081_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ArrayTypeMismatchException_t2081_0_0_0;
extern const Il2CppType ArrayTypeMismatchException_t2081_1_0_0;
struct ArrayTypeMismatchException_t2081;
const Il2CppTypeDefinitionMetadata ArrayTypeMismatchException_t2081_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ArrayTypeMismatchException_t2081_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, ArrayTypeMismatchException_t2081_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9079/* fieldStart */
	, 13136/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ArrayTypeMismatchException_t2081_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArrayTypeMismatchException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ArrayTypeMismatchException_t2081_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3233/* custom_attributes_cache */
	, &ArrayTypeMismatchException_t2081_0_0_0/* byval_arg */
	, &ArrayTypeMismatchException_t2081_1_0_0/* this_arg */
	, &ArrayTypeMismatchException_t2081_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArrayTypeMismatchException_t2081)/* instance_size */
	, sizeof (ArrayTypeMismatchException_t2081)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AssemblyLoadEventArgs
#include "mscorlib_System_AssemblyLoadEventArgs.h"
// Metadata Definition System.AssemblyLoadEventArgs
extern TypeInfo AssemblyLoadEventArgs_t2082_il2cpp_TypeInfo;
// System.AssemblyLoadEventArgs
#include "mscorlib_System_AssemblyLoadEventArgsMethodDeclarations.h"
static const EncodedMethodIndex AssemblyLoadEventArgs_t2082_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyLoadEventArgs_t2082_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2082_1_0_0;
extern const Il2CppType EventArgs_t1223_0_0_0;
struct AssemblyLoadEventArgs_t2082;
const Il2CppTypeDefinitionMetadata AssemblyLoadEventArgs_t2082_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1223_0_0_0/* parent */
	, AssemblyLoadEventArgs_t2082_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyLoadEventArgs_t2082_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyLoadEventArgs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &AssemblyLoadEventArgs_t2082_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3234/* custom_attributes_cache */
	, &AssemblyLoadEventArgs_t2082_0_0_0/* byval_arg */
	, &AssemblyLoadEventArgs_t2082_1_0_0/* this_arg */
	, &AssemblyLoadEventArgs_t2082_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyLoadEventArgs_t2082)/* instance_size */
	, sizeof (AssemblyLoadEventArgs_t2082)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"
// Metadata Definition System.AttributeTargets
extern TypeInfo AttributeTargets_t2083_il2cpp_TypeInfo;
// System.AttributeTargets
#include "mscorlib_System_AttributeTargetsMethodDeclarations.h"
static const EncodedMethodIndex AttributeTargets_t2083_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AttributeTargets_t2083_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeTargets_t2083_0_0_0;
extern const Il2CppType AttributeTargets_t2083_1_0_0;
const Il2CppTypeDefinitionMetadata AttributeTargets_t2083_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AttributeTargets_t2083_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AttributeTargets_t2083_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9080/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AttributeTargets_t2083_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeTargets"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3235/* custom_attributes_cache */
	, &AttributeTargets_t2083_0_0_0/* byval_arg */
	, &AttributeTargets_t2083_1_0_0/* this_arg */
	, &AttributeTargets_t2083_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeTargets_t2083)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AttributeTargets_t2083)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.BitConverter
#include "mscorlib_System_BitConverter.h"
// Metadata Definition System.BitConverter
extern TypeInfo BitConverter_t1338_il2cpp_TypeInfo;
// System.BitConverter
#include "mscorlib_System_BitConverterMethodDeclarations.h"
static const EncodedMethodIndex BitConverter_t1338_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BitConverter_t1338_0_0_0;
extern const Il2CppType BitConverter_t1338_1_0_0;
struct BitConverter_t1338;
const Il2CppTypeDefinitionMetadata BitConverter_t1338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitConverter_t1338_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9097/* fieldStart */
	, 13139/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BitConverter_t1338_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitConverter"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &BitConverter_t1338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BitConverter_t1338_0_0_0/* byval_arg */
	, &BitConverter_t1338_1_0_0/* this_arg */
	, &BitConverter_t1338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitConverter_t1338)/* instance_size */
	, sizeof (BitConverter_t1338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BitConverter_t1338_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Buffer
#include "mscorlib_System_Buffer.h"
// Metadata Definition System.Buffer
extern TypeInfo Buffer_t2084_il2cpp_TypeInfo;
// System.Buffer
#include "mscorlib_System_BufferMethodDeclarations.h"
static const EncodedMethodIndex Buffer_t2084_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Buffer_t2084_0_0_0;
extern const Il2CppType Buffer_t2084_1_0_0;
struct Buffer_t2084;
const Il2CppTypeDefinitionMetadata Buffer_t2084_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Buffer_t2084_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13149/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Buffer_t2084_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Buffer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Buffer_t2084_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3236/* custom_attributes_cache */
	, &Buffer_t2084_0_0_0/* byval_arg */
	, &Buffer_t2084_1_0_0/* this_arg */
	, &Buffer_t2084_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Buffer_t2084)/* instance_size */
	, sizeof (Buffer_t2084)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CharEnumerator
#include "mscorlib_System_CharEnumerator.h"
// Metadata Definition System.CharEnumerator
extern TypeInfo CharEnumerator_t2085_il2cpp_TypeInfo;
// System.CharEnumerator
#include "mscorlib_System_CharEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex CharEnumerator_t2085_VTable[8] = 
{
	626,
	601,
	627,
	628,
	4227,
	4228,
	4229,
	4230,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType IEnumerator_1_t2174_0_0_0;
static const Il2CppType* CharEnumerator_t2085_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
	&IDisposable_t538_0_0_0,
	&ICloneable_t3433_0_0_0,
	&IEnumerator_1_t2174_0_0_0,
};
static Il2CppInterfaceOffsetPair CharEnumerator_t2085_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
	{ &IDisposable_t538_0_0_0, 6},
	{ &ICloneable_t3433_0_0_0, 7},
	{ &IEnumerator_1_t2174_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CharEnumerator_t2085_0_0_0;
extern const Il2CppType CharEnumerator_t2085_1_0_0;
struct CharEnumerator_t2085;
const Il2CppTypeDefinitionMetadata CharEnumerator_t2085_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CharEnumerator_t2085_InterfacesTypeInfos/* implementedInterfaces */
	, CharEnumerator_t2085_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CharEnumerator_t2085_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9099/* fieldStart */
	, 13153/* methodStart */
	, -1/* eventStart */
	, 2568/* propertyStart */

};
TypeInfo CharEnumerator_t2085_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharEnumerator"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CharEnumerator_t2085_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3237/* custom_attributes_cache */
	, &CharEnumerator_t2085_0_0_0/* byval_arg */
	, &CharEnumerator_t2085_1_0_0/* this_arg */
	, &CharEnumerator_t2085_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharEnumerator_t2085)/* instance_size */
	, sizeof (CharEnumerator_t2085)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Console
#include "mscorlib_System_Console.h"
// Metadata Definition System.Console
extern TypeInfo Console_t1537_il2cpp_TypeInfo;
// System.Console
#include "mscorlib_System_ConsoleMethodDeclarations.h"
static const EncodedMethodIndex Console_t1537_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Console_t1537_0_0_0;
extern const Il2CppType Console_t1537_1_0_0;
struct Console_t1537;
const Il2CppTypeDefinitionMetadata Console_t1537_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Console_t1537_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9102/* fieldStart */
	, 13158/* methodStart */
	, -1/* eventStart */
	, 2570/* propertyStart */

};
TypeInfo Console_t1537_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Console"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Console_t1537_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Console_t1537_0_0_0/* byval_arg */
	, &Console_t1537_1_0_0/* this_arg */
	, &Console_t1537_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Console_t1537)/* instance_size */
	, sizeof (Console_t1537)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Console_t1537_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 385/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ContextBoundObject
#include "mscorlib_System_ContextBoundObject.h"
// Metadata Definition System.ContextBoundObject
extern TypeInfo ContextBoundObject_t2086_il2cpp_TypeInfo;
// System.ContextBoundObject
#include "mscorlib_System_ContextBoundObjectMethodDeclarations.h"
static const EncodedMethodIndex ContextBoundObject_t2086_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ContextBoundObject_t2086_0_0_0;
extern const Il2CppType ContextBoundObject_t2086_1_0_0;
struct ContextBoundObject_t2086;
const Il2CppTypeDefinitionMetadata ContextBoundObject_t2086_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MarshalByRefObject_t1415_0_0_0/* parent */
	, ContextBoundObject_t2086_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13165/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContextBoundObject_t2086_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContextBoundObject"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ContextBoundObject_t2086_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3238/* custom_attributes_cache */
	, &ContextBoundObject_t2086_0_0_0/* byval_arg */
	, &ContextBoundObject_t2086_1_0_0/* this_arg */
	, &ContextBoundObject_t2086_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContextBoundObject_t2086)/* instance_size */
	, sizeof (ContextBoundObject_t2086)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Convert
#include "mscorlib_System_Convert.h"
// Metadata Definition System.Convert
extern TypeInfo Convert_t524_il2cpp_TypeInfo;
// System.Convert
#include "mscorlib_System_ConvertMethodDeclarations.h"
static const EncodedMethodIndex Convert_t524_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Convert_t524_0_0_0;
extern const Il2CppType Convert_t524_1_0_0;
struct Convert_t524;
const Il2CppTypeDefinitionMetadata Convert_t524_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Convert_t524_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9107/* fieldStart */
	, 13166/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Convert_t524_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Convert"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Convert_t524_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Convert_t524_0_0_0/* byval_arg */
	, &Convert_t524_1_0_0/* this_arg */
	, &Convert_t524_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Convert_t524)/* instance_size */
	, sizeof (Convert_t524)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Convert_t524_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 208/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DBNull
#include "mscorlib_System_DBNull.h"
// Metadata Definition System.DBNull
extern TypeInfo DBNull_t2087_il2cpp_TypeInfo;
// System.DBNull
#include "mscorlib_System_DBNullMethodDeclarations.h"
static const EncodedMethodIndex DBNull_t2087_VTable[21] = 
{
	626,
	601,
	627,
	4231,
	4232,
	4233,
	4234,
	4235,
	4236,
	4237,
	4238,
	4239,
	4240,
	4241,
	4242,
	4243,
	4244,
	4245,
	4246,
	4247,
	4248,
};
static const Il2CppType* DBNull_t2087_InterfacesTypeInfos[] = 
{
	&IConvertible_t2193_0_0_0,
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair DBNull_t2087_InterfacesOffsets[] = 
{
	{ &IConvertible_t2193_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 20},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DBNull_t2087_0_0_0;
extern const Il2CppType DBNull_t2087_1_0_0;
struct DBNull_t2087;
const Il2CppTypeDefinitionMetadata DBNull_t2087_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DBNull_t2087_InterfacesTypeInfos/* implementedInterfaces */
	, DBNull_t2087_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DBNull_t2087_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9109/* fieldStart */
	, 13374/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DBNull_t2087_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DBNull"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DBNull_t2087_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3338/* custom_attributes_cache */
	, &DBNull_t2087_0_0_0/* byval_arg */
	, &DBNull_t2087_1_0_0/* this_arg */
	, &DBNull_t2087_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DBNull_t2087)/* instance_size */
	, sizeof (DBNull_t2087)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DBNull_t2087_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 21/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.DateTime
#include "mscorlib_System_DateTime.h"
// Metadata Definition System.DateTime
extern TypeInfo DateTime_t871_il2cpp_TypeInfo;
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
extern const Il2CppType Which_t2088_0_0_0;
static const Il2CppType* DateTime_t871_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Which_t2088_0_0_0,
};
static const EncodedMethodIndex DateTime_t871_VTable[24] = 
{
	4249,
	601,
	4250,
	4251,
	4252,
	4253,
	4254,
	4255,
	4256,
	4257,
	4258,
	4259,
	4260,
	4261,
	4262,
	4263,
	4264,
	4265,
	4266,
	4267,
	4268,
	4269,
	4270,
	4271,
};
extern const Il2CppType IComparable_1_t3890_0_0_0;
extern const Il2CppType IEquatable_1_t3891_0_0_0;
static const Il2CppType* DateTime_t871_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IConvertible_t2193_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3890_0_0_0,
	&IEquatable_1_t3891_0_0_0,
};
static Il2CppInterfaceOffsetPair DateTime_t871_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
	{ &IComparable_1_t3890_0_0_0, 22},
	{ &IEquatable_1_t3891_0_0_0, 23},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTime_t871_0_0_0;
extern const Il2CppType DateTime_t871_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata DateTime_t871_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DateTime_t871_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, DateTime_t871_InterfacesTypeInfos/* implementedInterfaces */
	, DateTime_t871_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, DateTime_t871_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9110/* fieldStart */
	, 13395/* methodStart */
	, -1/* eventStart */
	, 2571/* propertyStart */

};
TypeInfo DateTime_t871_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTime"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DateTime_t871_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DateTime_t871_0_0_0/* byval_arg */
	, &DateTime_t871_1_0_0/* this_arg */
	, &DateTime_t871_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTime_t871)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTime_t871)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DateTime_t871_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 82/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 24/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.DateTime/Which
#include "mscorlib_System_DateTime_Which.h"
// Metadata Definition System.DateTime/Which
extern TypeInfo Which_t2088_il2cpp_TypeInfo;
// System.DateTime/Which
#include "mscorlib_System_DateTime_WhichMethodDeclarations.h"
static const EncodedMethodIndex Which_t2088_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Which_t2088_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Which_t2088_1_0_0;
const Il2CppTypeDefinitionMetadata Which_t2088_DefinitionMetadata = 
{
	&DateTime_t871_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Which_t2088_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Which_t2088_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9125/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Which_t2088_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Which"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Which_t2088_0_0_0/* byval_arg */
	, &Which_t2088_1_0_0/* this_arg */
	, &Which_t2088_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Which_t2088)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Which_t2088)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
// Metadata Definition System.DateTimeKind
extern TypeInfo DateTimeKind_t2089_il2cpp_TypeInfo;
// System.DateTimeKind
#include "mscorlib_System_DateTimeKindMethodDeclarations.h"
static const EncodedMethodIndex DateTimeKind_t2089_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DateTimeKind_t2089_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeKind_t2089_0_0_0;
extern const Il2CppType DateTimeKind_t2089_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeKind_t2089_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DateTimeKind_t2089_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DateTimeKind_t2089_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9130/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeKind_t2089_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeKind"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3339/* custom_attributes_cache */
	, &DateTimeKind_t2089_0_0_0/* byval_arg */
	, &DateTimeKind_t2089_1_0_0/* this_arg */
	, &DateTimeKind_t2089_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeKind_t2089)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeKind_t2089)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"
// Metadata Definition System.DateTimeOffset
extern TypeInfo DateTimeOffset_t1148_il2cpp_TypeInfo;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffsetMethodDeclarations.h"
static const EncodedMethodIndex DateTimeOffset_t1148_VTable[10] = 
{
	4272,
	601,
	4273,
	4274,
	4275,
	4276,
	4277,
	4278,
	4279,
	4280,
};
extern const Il2CppType IComparable_1_t3892_0_0_0;
extern const Il2CppType IEquatable_1_t3893_0_0_0;
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
static const Il2CppType* DateTimeOffset_t1148_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IComparable_t2192_0_0_0,
	&ISerializable_t2210_0_0_0,
	&IComparable_1_t3892_0_0_0,
	&IEquatable_1_t3893_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair DateTimeOffset_t1148_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IComparable_t2192_0_0_0, 5},
	{ &ISerializable_t2210_0_0_0, 6},
	{ &IComparable_1_t3892_0_0_0, 7},
	{ &IEquatable_1_t3893_0_0_0, 8},
	{ &IDeserializationCallback_t2213_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeOffset_t1148_0_0_0;
extern const Il2CppType DateTimeOffset_t1148_1_0_0;
const Il2CppTypeDefinitionMetadata DateTimeOffset_t1148_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, DateTimeOffset_t1148_InterfacesTypeInfos/* implementedInterfaces */
	, DateTimeOffset_t1148_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, DateTimeOffset_t1148_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9134/* fieldStart */
	, 13477/* methodStart */
	, -1/* eventStart */
	, 2583/* propertyStart */

};
TypeInfo DateTimeOffset_t1148_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeOffset"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DateTimeOffset_t1148_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DateTimeOffset_t1148_0_0_0/* byval_arg */
	, &DateTimeOffset_t1148_1_0_0/* this_arg */
	, &DateTimeOffset_t1148_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeOffset_t1148)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DateTimeOffset_t1148)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DateTimeOffset_t1148_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
// System.DateTimeUtils
#include "mscorlib_System_DateTimeUtils.h"
// Metadata Definition System.DateTimeUtils
extern TypeInfo DateTimeUtils_t2090_il2cpp_TypeInfo;
// System.DateTimeUtils
#include "mscorlib_System_DateTimeUtilsMethodDeclarations.h"
static const EncodedMethodIndex DateTimeUtils_t2090_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DateTimeUtils_t2090_0_0_0;
extern const Il2CppType DateTimeUtils_t2090_1_0_0;
struct DateTimeUtils_t2090;
const Il2CppTypeDefinitionMetadata DateTimeUtils_t2090_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DateTimeUtils_t2090_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13495/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DateTimeUtils_t2090_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DateTimeUtils"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DateTimeUtils_t2090_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DateTimeUtils_t2090_0_0_0/* byval_arg */
	, &DateTimeUtils_t2090_1_0_0/* this_arg */
	, &DateTimeUtils_t2090_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DateTimeUtils_t2090)/* instance_size */
	, sizeof (DateTimeUtils_t2090)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"
// Metadata Definition System.DayOfWeek
extern TypeInfo DayOfWeek_t2091_il2cpp_TypeInfo;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeekMethodDeclarations.h"
static const EncodedMethodIndex DayOfWeek_t2091_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair DayOfWeek_t2091_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DayOfWeek_t2091_0_0_0;
extern const Il2CppType DayOfWeek_t2091_1_0_0;
const Il2CppTypeDefinitionMetadata DayOfWeek_t2091_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DayOfWeek_t2091_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, DayOfWeek_t2091_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9138/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DayOfWeek_t2091_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DayOfWeek"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3341/* custom_attributes_cache */
	, &DayOfWeek_t2091_0_0_0/* byval_arg */
	, &DayOfWeek_t2091_1_0_0/* this_arg */
	, &DayOfWeek_t2091_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DayOfWeek_t2091)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DayOfWeek_t2091)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.DelegateData
#include "mscorlib_System_DelegateData.h"
// Metadata Definition System.DelegateData
extern TypeInfo DelegateData_t1545_il2cpp_TypeInfo;
// System.DelegateData
#include "mscorlib_System_DelegateDataMethodDeclarations.h"
static const EncodedMethodIndex DelegateData_t1545_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelegateData_t1545_0_0_0;
extern const Il2CppType DelegateData_t1545_1_0_0;
struct DelegateData_t1545;
const Il2CppTypeDefinitionMetadata DelegateData_t1545_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DelegateData_t1545_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9146/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelegateData_t1545_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelegateData"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DelegateData_t1545_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelegateData_t1545_0_0_0/* byval_arg */
	, &DelegateData_t1545_1_0_0/* this_arg */
	, &DelegateData_t1545_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelegateData_t1545)/* instance_size */
	, sizeof (DelegateData_t1545)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DelegateSerializationHolder
#include "mscorlib_System_DelegateSerializationHolder.h"
// Metadata Definition System.DelegateSerializationHolder
extern TypeInfo DelegateSerializationHolder_t2093_il2cpp_TypeInfo;
// System.DelegateSerializationHolder
#include "mscorlib_System_DelegateSerializationHolderMethodDeclarations.h"
extern const Il2CppType DelegateEntry_t2092_0_0_0;
static const Il2CppType* DelegateSerializationHolder_t2093_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DelegateEntry_t2092_0_0_0,
};
static const EncodedMethodIndex DelegateSerializationHolder_t2093_VTable[6] = 
{
	626,
	601,
	627,
	628,
	4281,
	4282,
};
extern const Il2CppType IObjectReference_t2211_0_0_0;
static const Il2CppType* DelegateSerializationHolder_t2093_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IObjectReference_t2211_0_0_0,
};
static Il2CppInterfaceOffsetPair DelegateSerializationHolder_t2093_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IObjectReference_t2211_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelegateSerializationHolder_t2093_0_0_0;
extern const Il2CppType DelegateSerializationHolder_t2093_1_0_0;
struct DelegateSerializationHolder_t2093;
const Il2CppTypeDefinitionMetadata DelegateSerializationHolder_t2093_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DelegateSerializationHolder_t2093_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, DelegateSerializationHolder_t2093_InterfacesTypeInfos/* implementedInterfaces */
	, DelegateSerializationHolder_t2093_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DelegateSerializationHolder_t2093_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9148/* fieldStart */
	, 13502/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelegateSerializationHolder_t2093_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelegateSerializationHolder"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DelegateSerializationHolder_t2093_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelegateSerializationHolder_t2093_0_0_0/* byval_arg */
	, &DelegateSerializationHolder_t2093_1_0_0/* this_arg */
	, &DelegateSerializationHolder_t2093_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelegateSerializationHolder_t2093)/* instance_size */
	, sizeof (DelegateSerializationHolder_t2093)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.DelegateSerializationHolder/DelegateEntry
#include "mscorlib_System_DelegateSerializationHolder_DelegateEntry.h"
// Metadata Definition System.DelegateSerializationHolder/DelegateEntry
extern TypeInfo DelegateEntry_t2092_il2cpp_TypeInfo;
// System.DelegateSerializationHolder/DelegateEntry
#include "mscorlib_System_DelegateSerializationHolder_DelegateEntryMethodDeclarations.h"
static const EncodedMethodIndex DelegateEntry_t2092_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DelegateEntry_t2092_1_0_0;
struct DelegateEntry_t2092;
const Il2CppTypeDefinitionMetadata DelegateEntry_t2092_DefinitionMetadata = 
{
	&DelegateSerializationHolder_t2093_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DelegateEntry_t2092_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9149/* fieldStart */
	, 13506/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DelegateEntry_t2092_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DelegateEntry"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DelegateEntry_t2092_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DelegateEntry_t2092_0_0_0/* byval_arg */
	, &DelegateEntry_t2092_1_0_0/* this_arg */
	, &DelegateEntry_t2092_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DelegateEntry_t2092)/* instance_size */
	, sizeof (DelegateEntry_t2092)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.DivideByZeroException
#include "mscorlib_System_DivideByZeroException.h"
// Metadata Definition System.DivideByZeroException
extern TypeInfo DivideByZeroException_t2094_il2cpp_TypeInfo;
// System.DivideByZeroException
#include "mscorlib_System_DivideByZeroExceptionMethodDeclarations.h"
static const EncodedMethodIndex DivideByZeroException_t2094_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair DivideByZeroException_t2094_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DivideByZeroException_t2094_0_0_0;
extern const Il2CppType DivideByZeroException_t2094_1_0_0;
struct DivideByZeroException_t2094;
const Il2CppTypeDefinitionMetadata DivideByZeroException_t2094_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DivideByZeroException_t2094_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t1336_0_0_0/* parent */
	, DivideByZeroException_t2094_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13508/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DivideByZeroException_t2094_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DivideByZeroException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DivideByZeroException_t2094_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3342/* custom_attributes_cache */
	, &DivideByZeroException_t2094_0_0_0/* byval_arg */
	, &DivideByZeroException_t2094_1_0_0/* this_arg */
	, &DivideByZeroException_t2094_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DivideByZeroException_t2094)/* instance_size */
	, sizeof (DivideByZeroException_t2094)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.DllNotFoundException
#include "mscorlib_System_DllNotFoundException.h"
// Metadata Definition System.DllNotFoundException
extern TypeInfo DllNotFoundException_t2095_il2cpp_TypeInfo;
// System.DllNotFoundException
#include "mscorlib_System_DllNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex DllNotFoundException_t2095_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4283,
	1318,
	4284,
	1320,
	1321,
	4283,
	1322,
};
static Il2CppInterfaceOffsetPair DllNotFoundException_t2095_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DllNotFoundException_t2095_0_0_0;
extern const Il2CppType DllNotFoundException_t2095_1_0_0;
extern const Il2CppType TypeLoadException_t2096_0_0_0;
struct DllNotFoundException_t2095;
const Il2CppTypeDefinitionMetadata DllNotFoundException_t2095_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DllNotFoundException_t2095_InterfacesOffsets/* interfaceOffsets */
	, &TypeLoadException_t2096_0_0_0/* parent */
	, DllNotFoundException_t2095_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9156/* fieldStart */
	, 13510/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DllNotFoundException_t2095_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DllNotFoundException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &DllNotFoundException_t2095_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3343/* custom_attributes_cache */
	, &DllNotFoundException_t2095_0_0_0/* byval_arg */
	, &DllNotFoundException_t2095_1_0_0/* this_arg */
	, &DllNotFoundException_t2095_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DllNotFoundException_t2095)/* instance_size */
	, sizeof (DllNotFoundException_t2095)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.EntryPointNotFoundException
#include "mscorlib_System_EntryPointNotFoundException.h"
// Metadata Definition System.EntryPointNotFoundException
extern TypeInfo EntryPointNotFoundException_t2097_il2cpp_TypeInfo;
// System.EntryPointNotFoundException
#include "mscorlib_System_EntryPointNotFoundExceptionMethodDeclarations.h"
static const EncodedMethodIndex EntryPointNotFoundException_t2097_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4283,
	1318,
	4284,
	1320,
	1321,
	4283,
	1322,
};
static Il2CppInterfaceOffsetPair EntryPointNotFoundException_t2097_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EntryPointNotFoundException_t2097_0_0_0;
extern const Il2CppType EntryPointNotFoundException_t2097_1_0_0;
struct EntryPointNotFoundException_t2097;
const Il2CppTypeDefinitionMetadata EntryPointNotFoundException_t2097_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EntryPointNotFoundException_t2097_InterfacesOffsets/* interfaceOffsets */
	, &TypeLoadException_t2096_0_0_0/* parent */
	, EntryPointNotFoundException_t2097_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9157/* fieldStart */
	, 13512/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EntryPointNotFoundException_t2097_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EntryPointNotFoundException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &EntryPointNotFoundException_t2097_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3344/* custom_attributes_cache */
	, &EntryPointNotFoundException_t2097_0_0_0/* byval_arg */
	, &EntryPointNotFoundException_t2097_1_0_0/* this_arg */
	, &EntryPointNotFoundException_t2097_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EntryPointNotFoundException_t2097)/* instance_size */
	, sizeof (EntryPointNotFoundException_t2097)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfo.h"
// Metadata Definition System.MonoEnumInfo
extern TypeInfo MonoEnumInfo_t2102_il2cpp_TypeInfo;
// System.MonoEnumInfo
#include "mscorlib_System_MonoEnumInfoMethodDeclarations.h"
extern const Il2CppType SByteComparer_t2098_0_0_0;
extern const Il2CppType ShortComparer_t2099_0_0_0;
extern const Il2CppType IntComparer_t2100_0_0_0;
extern const Il2CppType LongComparer_t2101_0_0_0;
static const Il2CppType* MonoEnumInfo_t2102_il2cpp_TypeInfo__nestedTypes[4] =
{
	&SByteComparer_t2098_0_0_0,
	&ShortComparer_t2099_0_0_0,
	&IntComparer_t2100_0_0_0,
	&LongComparer_t2101_0_0_0,
};
static const EncodedMethodIndex MonoEnumInfo_t2102_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoEnumInfo_t2102_0_0_0;
extern const Il2CppType MonoEnumInfo_t2102_1_0_0;
const Il2CppTypeDefinitionMetadata MonoEnumInfo_t2102_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoEnumInfo_t2102_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MonoEnumInfo_t2102_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9158/* fieldStart */
	, 13514/* methodStart */
	, -1/* eventStart */
	, 2586/* propertyStart */

};
TypeInfo MonoEnumInfo_t2102_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEnumInfo"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoEnumInfo_t2102_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEnumInfo_t2102_0_0_0/* byval_arg */
	, &MonoEnumInfo_t2102_1_0_0/* this_arg */
	, &MonoEnumInfo_t2102_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEnumInfo_t2102)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoEnumInfo_t2102)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoEnumInfo_t2102_StaticFields)/* static_fields_size */
	, sizeof(MonoEnumInfo_t2102_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 264/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 4/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoEnumInfo/SByteComparer
#include "mscorlib_System_MonoEnumInfo_SByteComparer.h"
// Metadata Definition System.MonoEnumInfo/SByteComparer
extern TypeInfo SByteComparer_t2098_il2cpp_TypeInfo;
// System.MonoEnumInfo/SByteComparer
#include "mscorlib_System_MonoEnumInfo_SByteComparerMethodDeclarations.h"
static const EncodedMethodIndex SByteComparer_t2098_VTable[6] = 
{
	626,
	601,
	627,
	628,
	4285,
	4286,
};
extern const Il2CppType IComparer_1_t3894_0_0_0;
static const Il2CppType* SByteComparer_t2098_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3894_0_0_0,
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair SByteComparer_t2098_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3894_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SByteComparer_t2098_1_0_0;
struct SByteComparer_t2098;
const Il2CppTypeDefinitionMetadata SByteComparer_t2098_DefinitionMetadata = 
{
	&MonoEnumInfo_t2102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, SByteComparer_t2098_InterfacesTypeInfos/* implementedInterfaces */
	, SByteComparer_t2098_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SByteComparer_t2098_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13519/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SByteComparer_t2098_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SByteComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SByteComparer_t2098_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SByteComparer_t2098_0_0_0/* byval_arg */
	, &SByteComparer_t2098_1_0_0/* this_arg */
	, &SByteComparer_t2098_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SByteComparer_t2098)/* instance_size */
	, sizeof (SByteComparer_t2098)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo/ShortComparer
#include "mscorlib_System_MonoEnumInfo_ShortComparer.h"
// Metadata Definition System.MonoEnumInfo/ShortComparer
extern TypeInfo ShortComparer_t2099_il2cpp_TypeInfo;
// System.MonoEnumInfo/ShortComparer
#include "mscorlib_System_MonoEnumInfo_ShortComparerMethodDeclarations.h"
static const EncodedMethodIndex ShortComparer_t2099_VTable[6] = 
{
	626,
	601,
	627,
	628,
	4287,
	4288,
};
extern const Il2CppType IComparer_1_t3895_0_0_0;
static const Il2CppType* ShortComparer_t2099_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3895_0_0_0,
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair ShortComparer_t2099_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3895_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ShortComparer_t2099_1_0_0;
struct ShortComparer_t2099;
const Il2CppTypeDefinitionMetadata ShortComparer_t2099_DefinitionMetadata = 
{
	&MonoEnumInfo_t2102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ShortComparer_t2099_InterfacesTypeInfos/* implementedInterfaces */
	, ShortComparer_t2099_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ShortComparer_t2099_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13522/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ShortComparer_t2099_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShortComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ShortComparer_t2099_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ShortComparer_t2099_0_0_0/* byval_arg */
	, &ShortComparer_t2099_1_0_0/* this_arg */
	, &ShortComparer_t2099_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShortComparer_t2099)/* instance_size */
	, sizeof (ShortComparer_t2099)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo/IntComparer
#include "mscorlib_System_MonoEnumInfo_IntComparer.h"
// Metadata Definition System.MonoEnumInfo/IntComparer
extern TypeInfo IntComparer_t2100_il2cpp_TypeInfo;
// System.MonoEnumInfo/IntComparer
#include "mscorlib_System_MonoEnumInfo_IntComparerMethodDeclarations.h"
static const EncodedMethodIndex IntComparer_t2100_VTable[6] = 
{
	626,
	601,
	627,
	628,
	4289,
	4290,
};
extern const Il2CppType IComparer_1_t3351_0_0_0;
static const Il2CppType* IntComparer_t2100_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3351_0_0_0,
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair IntComparer_t2100_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3351_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IntComparer_t2100_1_0_0;
struct IntComparer_t2100;
const Il2CppTypeDefinitionMetadata IntComparer_t2100_DefinitionMetadata = 
{
	&MonoEnumInfo_t2102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, IntComparer_t2100_InterfacesTypeInfos/* implementedInterfaces */
	, IntComparer_t2100_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IntComparer_t2100_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13525/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IntComparer_t2100_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &IntComparer_t2100_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntComparer_t2100_0_0_0/* byval_arg */
	, &IntComparer_t2100_1_0_0/* this_arg */
	, &IntComparer_t2100_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntComparer_t2100)/* instance_size */
	, sizeof (IntComparer_t2100)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoEnumInfo/LongComparer
#include "mscorlib_System_MonoEnumInfo_LongComparer.h"
// Metadata Definition System.MonoEnumInfo/LongComparer
extern TypeInfo LongComparer_t2101_il2cpp_TypeInfo;
// System.MonoEnumInfo/LongComparer
#include "mscorlib_System_MonoEnumInfo_LongComparerMethodDeclarations.h"
static const EncodedMethodIndex LongComparer_t2101_VTable[6] = 
{
	626,
	601,
	627,
	628,
	4291,
	4292,
};
extern const Il2CppType IComparer_1_t3896_0_0_0;
static const Il2CppType* LongComparer_t2101_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3896_0_0_0,
	&IComparer_t416_0_0_0,
};
static Il2CppInterfaceOffsetPair LongComparer_t2101_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3896_0_0_0, 4},
	{ &IComparer_t416_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LongComparer_t2101_1_0_0;
struct LongComparer_t2101;
const Il2CppTypeDefinitionMetadata LongComparer_t2101_DefinitionMetadata = 
{
	&MonoEnumInfo_t2102_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, LongComparer_t2101_InterfacesTypeInfos/* implementedInterfaces */
	, LongComparer_t2101_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LongComparer_t2101_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13528/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LongComparer_t2101_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LongComparer"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &LongComparer_t2101_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LongComparer_t2101_0_0_0/* byval_arg */
	, &LongComparer_t2101_1_0_0/* this_arg */
	, &LongComparer_t2101_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LongComparer_t2101)/* instance_size */
	, sizeof (LongComparer_t2101)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Environment
#include "mscorlib_System_Environment.h"
// Metadata Definition System.Environment
extern TypeInfo Environment_t2105_il2cpp_TypeInfo;
// System.Environment
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
extern const Il2CppType SpecialFolder_t2103_0_0_0;
static const Il2CppType* Environment_t2105_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SpecialFolder_t2103_0_0_0,
};
static const EncodedMethodIndex Environment_t2105_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Environment_t2105_0_0_0;
extern const Il2CppType Environment_t2105_1_0_0;
struct Environment_t2105;
const Il2CppTypeDefinitionMetadata Environment_t2105_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Environment_t2105_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Environment_t2105_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9169/* fieldStart */
	, 13531/* methodStart */
	, -1/* eventStart */
	, 2587/* propertyStart */

};
TypeInfo Environment_t2105_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Environment"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Environment_t2105_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3346/* custom_attributes_cache */
	, &Environment_t2105_0_0_0/* byval_arg */
	, &Environment_t2105_1_0_0/* this_arg */
	, &Environment_t2105_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Environment_t2105)/* instance_size */
	, sizeof (Environment_t2105)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Environment_t2105_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 6/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Environment/SpecialFolder
#include "mscorlib_System_Environment_SpecialFolder.h"
// Metadata Definition System.Environment/SpecialFolder
extern TypeInfo SpecialFolder_t2103_il2cpp_TypeInfo;
// System.Environment/SpecialFolder
#include "mscorlib_System_Environment_SpecialFolderMethodDeclarations.h"
static const EncodedMethodIndex SpecialFolder_t2103_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SpecialFolder_t2103_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SpecialFolder_t2103_1_0_0;
const Il2CppTypeDefinitionMetadata SpecialFolder_t2103_DefinitionMetadata = 
{
	&Environment_t2105_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpecialFolder_t2103_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SpecialFolder_t2103_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9170/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SpecialFolder_t2103_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpecialFolder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3347/* custom_attributes_cache */
	, &SpecialFolder_t2103_0_0_0/* byval_arg */
	, &SpecialFolder_t2103_1_0_0/* this_arg */
	, &SpecialFolder_t2103_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpecialFolder_t2103)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpecialFolder_t2103)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.EventArgs
#include "mscorlib_System_EventArgs.h"
// Metadata Definition System.EventArgs
extern TypeInfo EventArgs_t1223_il2cpp_TypeInfo;
// System.EventArgs
#include "mscorlib_System_EventArgsMethodDeclarations.h"
static const EncodedMethodIndex EventArgs_t1223_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventArgs_t1223_1_0_0;
struct EventArgs_t1223;
const Il2CppTypeDefinitionMetadata EventArgs_t1223_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EventArgs_t1223_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9194/* fieldStart */
	, 13546/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventArgs_t1223_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventArgs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &EventArgs_t1223_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3348/* custom_attributes_cache */
	, &EventArgs_t1223_0_0_0/* byval_arg */
	, &EventArgs_t1223_1_0_0/* this_arg */
	, &EventArgs_t1223_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventArgs_t1223)/* instance_size */
	, sizeof (EventArgs_t1223)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EventArgs_t1223_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineException.h"
// Metadata Definition System.ExecutionEngineException
extern TypeInfo ExecutionEngineException_t2106_il2cpp_TypeInfo;
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineExceptionMethodDeclarations.h"
static const EncodedMethodIndex ExecutionEngineException_t2106_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair ExecutionEngineException_t2106_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExecutionEngineException_t2106_0_0_0;
extern const Il2CppType ExecutionEngineException_t2106_1_0_0;
struct ExecutionEngineException_t2106;
const Il2CppTypeDefinitionMetadata ExecutionEngineException_t2106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecutionEngineException_t2106_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, ExecutionEngineException_t2106_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13548/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExecutionEngineException_t2106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecutionEngineException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ExecutionEngineException_t2106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3349/* custom_attributes_cache */
	, &ExecutionEngineException_t2106_0_0_0/* byval_arg */
	, &ExecutionEngineException_t2106_1_0_0/* this_arg */
	, &ExecutionEngineException_t2106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecutionEngineException_t2106)/* instance_size */
	, sizeof (ExecutionEngineException_t2106)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FieldAccessException
#include "mscorlib_System_FieldAccessException.h"
// Metadata Definition System.FieldAccessException
extern TypeInfo FieldAccessException_t2107_il2cpp_TypeInfo;
// System.FieldAccessException
#include "mscorlib_System_FieldAccessExceptionMethodDeclarations.h"
static const EncodedMethodIndex FieldAccessException_t2107_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair FieldAccessException_t2107_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAccessException_t2107_0_0_0;
extern const Il2CppType FieldAccessException_t2107_1_0_0;
extern const Il2CppType MemberAccessException_t2108_0_0_0;
struct FieldAccessException_t2107;
const Il2CppTypeDefinitionMetadata FieldAccessException_t2107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAccessException_t2107_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2108_0_0_0/* parent */
	, FieldAccessException_t2107_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13550/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FieldAccessException_t2107_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAccessException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &FieldAccessException_t2107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3350/* custom_attributes_cache */
	, &FieldAccessException_t2107_0_0_0/* byval_arg */
	, &FieldAccessException_t2107_1_0_0/* this_arg */
	, &FieldAccessException_t2107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAccessException_t2107)/* instance_size */
	, sizeof (FieldAccessException_t2107)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// Metadata Definition System.FlagsAttribute
extern TypeInfo FlagsAttribute_t2109_il2cpp_TypeInfo;
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
static const EncodedMethodIndex FlagsAttribute_t2109_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair FlagsAttribute_t2109_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FlagsAttribute_t2109_0_0_0;
extern const Il2CppType FlagsAttribute_t2109_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct FlagsAttribute_t2109;
const Il2CppTypeDefinitionMetadata FlagsAttribute_t2109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FlagsAttribute_t2109_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, FlagsAttribute_t2109_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13553/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FlagsAttribute_t2109_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FlagsAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &FlagsAttribute_t2109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3351/* custom_attributes_cache */
	, &FlagsAttribute_t2109_0_0_0/* byval_arg */
	, &FlagsAttribute_t2109_1_0_0/* this_arg */
	, &FlagsAttribute_t2109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FlagsAttribute_t2109)/* instance_size */
	, sizeof (FlagsAttribute_t2109)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.FormatException
#include "mscorlib_System_FormatException.h"
// Metadata Definition System.FormatException
extern TypeInfo FormatException_t1123_il2cpp_TypeInfo;
// System.FormatException
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
static const EncodedMethodIndex FormatException_t1123_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair FormatException_t1123_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatException_t1123_0_0_0;
extern const Il2CppType FormatException_t1123_1_0_0;
struct FormatException_t1123;
const Il2CppTypeDefinitionMetadata FormatException_t1123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatException_t1123_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, FormatException_t1123_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9195/* fieldStart */
	, 13554/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FormatException_t1123_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &FormatException_t1123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3352/* custom_attributes_cache */
	, &FormatException_t1123_0_0_0/* byval_arg */
	, &FormatException_t1123_1_0_0/* this_arg */
	, &FormatException_t1123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatException_t1123)/* instance_size */
	, sizeof (FormatException_t1123)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.GC
#include "mscorlib_System_GC.h"
// Metadata Definition System.GC
extern TypeInfo GC_t2110_il2cpp_TypeInfo;
// System.GC
#include "mscorlib_System_GCMethodDeclarations.h"
static const EncodedMethodIndex GC_t2110_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GC_t2110_0_0_0;
extern const Il2CppType GC_t2110_1_0_0;
struct GC_t2110;
const Il2CppTypeDefinitionMetadata GC_t2110_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GC_t2110_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13557/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GC_t2110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GC"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &GC_t2110_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GC_t2110_0_0_0/* byval_arg */
	, &GC_t2110_1_0_0/* this_arg */
	, &GC_t2110_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GC_t2110)/* instance_size */
	, sizeof (GC_t2110)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Guid
#include "mscorlib_System_Guid.h"
// Metadata Definition System.Guid
extern TypeInfo Guid_t555_il2cpp_TypeInfo;
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
static const EncodedMethodIndex Guid_t555_VTable[8] = 
{
	4293,
	601,
	4294,
	4295,
	4296,
	4297,
	4298,
	4299,
};
extern const Il2CppType IComparable_1_t3897_0_0_0;
extern const Il2CppType IEquatable_1_t3898_0_0_0;
static const Il2CppType* Guid_t555_InterfacesTypeInfos[] = 
{
	&IFormattable_t2190_0_0_0,
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3897_0_0_0,
	&IEquatable_1_t3898_0_0_0,
};
static Il2CppInterfaceOffsetPair Guid_t555_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IComparable_t2192_0_0_0, 5},
	{ &IComparable_1_t3897_0_0_0, 6},
	{ &IEquatable_1_t3898_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Guid_t555_0_0_0;
extern const Il2CppType Guid_t555_1_0_0;
const Il2CppTypeDefinitionMetadata Guid_t555_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Guid_t555_InterfacesTypeInfos/* implementedInterfaces */
	, Guid_t555_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, Guid_t555_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9196/* fieldStart */
	, 13558/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Guid_t555_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Guid"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Guid_t555_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3354/* custom_attributes_cache */
	, &Guid_t555_0_0_0/* byval_arg */
	, &Guid_t555_1_0_0/* this_arg */
	, &Guid_t555_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Guid_t555)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Guid_t555)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Guid_t555 )/* native_size */
	, sizeof(Guid_t555_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.ICustomFormatter
extern TypeInfo ICustomFormatter_t2189_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICustomFormatter_t2189_0_0_0;
extern const Il2CppType ICustomFormatter_t2189_1_0_0;
struct ICustomFormatter_t2189;
const Il2CppTypeDefinitionMetadata ICustomFormatter_t2189_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13579/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ICustomFormatter_t2189_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICustomFormatter"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ICustomFormatter_t2189_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3355/* custom_attributes_cache */
	, &ICustomFormatter_t2189_0_0_0/* byval_arg */
	, &ICustomFormatter_t2189_1_0_0/* this_arg */
	, &ICustomFormatter_t2189_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IFormatProvider
extern TypeInfo IFormatProvider_t2173_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatProvider_t2173_0_0_0;
extern const Il2CppType IFormatProvider_t2173_1_0_0;
struct IFormatProvider_t2173;
const Il2CppTypeDefinitionMetadata IFormatProvider_t2173_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13580/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IFormatProvider_t2173_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatProvider"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IFormatProvider_t2173_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3356/* custom_attributes_cache */
	, &IFormatProvider_t2173_0_0_0/* byval_arg */
	, &IFormatProvider_t2173_1_0_0/* this_arg */
	, &IFormatProvider_t2173_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeException.h"
// Metadata Definition System.IndexOutOfRangeException
extern TypeInfo IndexOutOfRangeException_t1116_il2cpp_TypeInfo;
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
static const EncodedMethodIndex IndexOutOfRangeException_t1116_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair IndexOutOfRangeException_t1116_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IndexOutOfRangeException_t1116_0_0_0;
extern const Il2CppType IndexOutOfRangeException_t1116_1_0_0;
struct IndexOutOfRangeException_t1116;
const Il2CppTypeDefinitionMetadata IndexOutOfRangeException_t1116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IndexOutOfRangeException_t1116_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, IndexOutOfRangeException_t1116_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13581/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IndexOutOfRangeException_t1116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexOutOfRangeException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &IndexOutOfRangeException_t1116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3357/* custom_attributes_cache */
	, &IndexOutOfRangeException_t1116_0_0_0/* byval_arg */
	, &IndexOutOfRangeException_t1116_1_0_0/* this_arg */
	, &IndexOutOfRangeException_t1116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IndexOutOfRangeException_t1116)/* instance_size */
	, sizeof (IndexOutOfRangeException_t1116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidCastException
#include "mscorlib_System_InvalidCastException.h"
// Metadata Definition System.InvalidCastException
extern TypeInfo InvalidCastException_t2111_il2cpp_TypeInfo;
// System.InvalidCastException
#include "mscorlib_System_InvalidCastExceptionMethodDeclarations.h"
static const EncodedMethodIndex InvalidCastException_t2111_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair InvalidCastException_t2111_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidCastException_t2111_0_0_0;
extern const Il2CppType InvalidCastException_t2111_1_0_0;
struct InvalidCastException_t2111;
const Il2CppTypeDefinitionMetadata InvalidCastException_t2111_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidCastException_t2111_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, InvalidCastException_t2111_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9210/* fieldStart */
	, 13584/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvalidCastException_t2111_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidCastException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &InvalidCastException_t2111_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3358/* custom_attributes_cache */
	, &InvalidCastException_t2111_0_0_0/* byval_arg */
	, &InvalidCastException_t2111_1_0_0/* this_arg */
	, &InvalidCastException_t2111_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidCastException_t2111)/* instance_size */
	, sizeof (InvalidCastException_t2111)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// Metadata Definition System.InvalidOperationException
extern TypeInfo InvalidOperationException_t1359_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
static const EncodedMethodIndex InvalidOperationException_t1359_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair InvalidOperationException_t1359_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidOperationException_t1359_0_0_0;
extern const Il2CppType InvalidOperationException_t1359_1_0_0;
struct InvalidOperationException_t1359;
const Il2CppTypeDefinitionMetadata InvalidOperationException_t1359_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidOperationException_t1359_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, InvalidOperationException_t1359_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9211/* fieldStart */
	, 13587/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InvalidOperationException_t1359_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidOperationException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &InvalidOperationException_t1359_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3359/* custom_attributes_cache */
	, &InvalidOperationException_t1359_0_0_0/* byval_arg */
	, &InvalidOperationException_t1359_1_0_0/* this_arg */
	, &InvalidOperationException_t1359_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidOperationException_t1359)/* instance_size */
	, sizeof (InvalidOperationException_t1359)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"
// Metadata Definition System.LoaderOptimization
extern TypeInfo LoaderOptimization_t2112_il2cpp_TypeInfo;
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimizationMethodDeclarations.h"
static const EncodedMethodIndex LoaderOptimization_t2112_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair LoaderOptimization_t2112_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoaderOptimization_t2112_0_0_0;
extern const Il2CppType LoaderOptimization_t2112_1_0_0;
const Il2CppTypeDefinitionMetadata LoaderOptimization_t2112_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoaderOptimization_t2112_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, LoaderOptimization_t2112_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9212/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LoaderOptimization_t2112_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoaderOptimization"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3360/* custom_attributes_cache */
	, &LoaderOptimization_t2112_0_0_0/* byval_arg */
	, &LoaderOptimization_t2112_1_0_0/* this_arg */
	, &LoaderOptimization_t2112_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoaderOptimization_t2112)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoaderOptimization_t2112)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Math
#include "mscorlib_System_Math.h"
// Metadata Definition System.Math
extern TypeInfo Math_t2113_il2cpp_TypeInfo;
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
static const EncodedMethodIndex Math_t2113_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Math_t2113_0_0_0;
extern const Il2CppType Math_t2113_1_0_0;
struct Math_t2113;
const Il2CppTypeDefinitionMetadata Math_t2113_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Math_t2113_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13591/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Math_t2113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Math"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Math_t2113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Math_t2113_0_0_0/* byval_arg */
	, &Math_t2113_1_0_0/* this_arg */
	, &Math_t2113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Math_t2113)/* instance_size */
	, sizeof (Math_t2113)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 22/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MemberAccessException
#include "mscorlib_System_MemberAccessException.h"
// Metadata Definition System.MemberAccessException
extern TypeInfo MemberAccessException_t2108_il2cpp_TypeInfo;
// System.MemberAccessException
#include "mscorlib_System_MemberAccessExceptionMethodDeclarations.h"
static const EncodedMethodIndex MemberAccessException_t2108_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair MemberAccessException_t2108_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberAccessException_t2108_1_0_0;
struct MemberAccessException_t2108;
const Il2CppTypeDefinitionMetadata MemberAccessException_t2108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberAccessException_t2108_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, MemberAccessException_t2108_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13613/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MemberAccessException_t2108_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberAccessException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MemberAccessException_t2108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3368/* custom_attributes_cache */
	, &MemberAccessException_t2108_0_0_0/* byval_arg */
	, &MemberAccessException_t2108_1_0_0/* this_arg */
	, &MemberAccessException_t2108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberAccessException_t2108)/* instance_size */
	, sizeof (MemberAccessException_t2108)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MethodAccessException
#include "mscorlib_System_MethodAccessException.h"
// Metadata Definition System.MethodAccessException
extern TypeInfo MethodAccessException_t2114_il2cpp_TypeInfo;
// System.MethodAccessException
#include "mscorlib_System_MethodAccessExceptionMethodDeclarations.h"
static const EncodedMethodIndex MethodAccessException_t2114_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair MethodAccessException_t2114_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAccessException_t2114_0_0_0;
extern const Il2CppType MethodAccessException_t2114_1_0_0;
struct MethodAccessException_t2114;
const Il2CppTypeDefinitionMetadata MethodAccessException_t2114_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAccessException_t2114_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2108_0_0_0/* parent */
	, MethodAccessException_t2114_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13616/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodAccessException_t2114_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAccessException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MethodAccessException_t2114_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3369/* custom_attributes_cache */
	, &MethodAccessException_t2114_0_0_0/* byval_arg */
	, &MethodAccessException_t2114_1_0_0/* this_arg */
	, &MethodAccessException_t2114_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAccessException_t2114)/* instance_size */
	, sizeof (MethodAccessException_t2114)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingFieldException
#include "mscorlib_System_MissingFieldException.h"
// Metadata Definition System.MissingFieldException
extern TypeInfo MissingFieldException_t2115_il2cpp_TypeInfo;
// System.MissingFieldException
#include "mscorlib_System_MissingFieldExceptionMethodDeclarations.h"
static const EncodedMethodIndex MissingFieldException_t2115_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4300,
	1318,
	4301,
	1320,
	1321,
	4300,
	1322,
};
static Il2CppInterfaceOffsetPair MissingFieldException_t2115_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingFieldException_t2115_0_0_0;
extern const Il2CppType MissingFieldException_t2115_1_0_0;
extern const Il2CppType MissingMemberException_t2116_0_0_0;
struct MissingFieldException_t2115;
const Il2CppTypeDefinitionMetadata MissingFieldException_t2115_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingFieldException_t2115_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t2116_0_0_0/* parent */
	, MissingFieldException_t2115_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13618/* methodStart */
	, -1/* eventStart */
	, 2593/* propertyStart */

};
TypeInfo MissingFieldException_t2115_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingFieldException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MissingFieldException_t2115_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3370/* custom_attributes_cache */
	, &MissingFieldException_t2115_0_0_0/* byval_arg */
	, &MissingFieldException_t2115_1_0_0/* this_arg */
	, &MissingFieldException_t2115_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingFieldException_t2115)/* instance_size */
	, sizeof (MissingFieldException_t2115)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMemberException
#include "mscorlib_System_MissingMemberException.h"
// Metadata Definition System.MissingMemberException
extern TypeInfo MissingMemberException_t2116_il2cpp_TypeInfo;
// System.MissingMemberException
#include "mscorlib_System_MissingMemberExceptionMethodDeclarations.h"
static const EncodedMethodIndex MissingMemberException_t2116_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4300,
	1318,
	4302,
	1320,
	1321,
	4300,
	1322,
};
static Il2CppInterfaceOffsetPair MissingMemberException_t2116_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMemberException_t2116_1_0_0;
struct MissingMemberException_t2116;
const Il2CppTypeDefinitionMetadata MissingMemberException_t2116_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMemberException_t2116_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2108_0_0_0/* parent */
	, MissingMemberException_t2116_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9219/* fieldStart */
	, 13622/* methodStart */
	, -1/* eventStart */
	, 2594/* propertyStart */

};
TypeInfo MissingMemberException_t2116_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMemberException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MissingMemberException_t2116_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3371/* custom_attributes_cache */
	, &MissingMemberException_t2116_0_0_0/* byval_arg */
	, &MissingMemberException_t2116_1_0_0/* this_arg */
	, &MissingMemberException_t2116_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMemberException_t2116)/* instance_size */
	, sizeof (MissingMemberException_t2116)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMethodException
#include "mscorlib_System_MissingMethodException.h"
// Metadata Definition System.MissingMethodException
extern TypeInfo MissingMethodException_t2117_il2cpp_TypeInfo;
// System.MissingMethodException
#include "mscorlib_System_MissingMethodExceptionMethodDeclarations.h"
static const EncodedMethodIndex MissingMethodException_t2117_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4300,
	1318,
	4303,
	1320,
	1321,
	4300,
	1322,
};
static Il2CppInterfaceOffsetPair MissingMethodException_t2117_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMethodException_t2117_0_0_0;
extern const Il2CppType MissingMethodException_t2117_1_0_0;
struct MissingMethodException_t2117;
const Il2CppTypeDefinitionMetadata MissingMethodException_t2117_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMethodException_t2117_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t2116_0_0_0/* parent */
	, MissingMethodException_t2117_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9222/* fieldStart */
	, 13628/* methodStart */
	, -1/* eventStart */
	, 2595/* propertyStart */

};
TypeInfo MissingMethodException_t2117_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMethodException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MissingMethodException_t2117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3372/* custom_attributes_cache */
	, &MissingMethodException_t2117_0_0_0/* byval_arg */
	, &MissingMethodException_t2117_1_0_0/* this_arg */
	, &MissingMethodException_t2117_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMethodException_t2117)/* instance_size */
	, sizeof (MissingMethodException_t2117)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCall.h"
// Metadata Definition System.MonoAsyncCall
extern TypeInfo MonoAsyncCall_t2118_il2cpp_TypeInfo;
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCallMethodDeclarations.h"
static const EncodedMethodIndex MonoAsyncCall_t2118_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoAsyncCall_t2118_0_0_0;
extern const Il2CppType MonoAsyncCall_t2118_1_0_0;
struct MonoAsyncCall_t2118;
const Il2CppTypeDefinitionMetadata MonoAsyncCall_t2118_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoAsyncCall_t2118_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9223/* fieldStart */
	, 13633/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoAsyncCall_t2118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoAsyncCall"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoAsyncCall_t2118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoAsyncCall_t2118_0_0_0/* byval_arg */
	, &MonoAsyncCall_t2118_1_0_0/* this_arg */
	, &MonoAsyncCall_t2118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoAsyncCall_t2118)/* instance_size */
	, sizeof (MonoAsyncCall_t2118)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrs.h"
// Metadata Definition System.MonoCustomAttrs
extern TypeInfo MonoCustomAttrs_t2120_il2cpp_TypeInfo;
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrsMethodDeclarations.h"
extern const Il2CppType AttributeInfo_t2119_0_0_0;
static const Il2CppType* MonoCustomAttrs_t2120_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AttributeInfo_t2119_0_0_0,
};
static const EncodedMethodIndex MonoCustomAttrs_t2120_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCustomAttrs_t2120_0_0_0;
extern const Il2CppType MonoCustomAttrs_t2120_1_0_0;
struct MonoCustomAttrs_t2120;
const Il2CppTypeDefinitionMetadata MonoCustomAttrs_t2120_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoCustomAttrs_t2120_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoCustomAttrs_t2120_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9230/* fieldStart */
	, 13634/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoCustomAttrs_t2120_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCustomAttrs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoCustomAttrs_t2120_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCustomAttrs_t2120_0_0_0/* byval_arg */
	, &MonoCustomAttrs_t2120_1_0_0/* this_arg */
	, &MonoCustomAttrs_t2120_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCustomAttrs_t2120)/* instance_size */
	, sizeof (MonoCustomAttrs_t2120)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoCustomAttrs_t2120_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfo.h"
// Metadata Definition System.MonoCustomAttrs/AttributeInfo
extern TypeInfo AttributeInfo_t2119_il2cpp_TypeInfo;
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfoMethodDeclarations.h"
static const EncodedMethodIndex AttributeInfo_t2119_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeInfo_t2119_1_0_0;
struct AttributeInfo_t2119;
const Il2CppTypeDefinitionMetadata AttributeInfo_t2119_DefinitionMetadata = 
{
	&MonoCustomAttrs_t2120_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeInfo_t2119_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9233/* fieldStart */
	, 13647/* methodStart */
	, -1/* eventStart */
	, 2596/* propertyStart */

};
TypeInfo AttributeInfo_t2119_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AttributeInfo_t2119_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeInfo_t2119_0_0_0/* byval_arg */
	, &AttributeInfo_t2119_1_0_0/* this_arg */
	, &AttributeInfo_t2119_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeInfo_t2119)/* instance_size */
	, sizeof (AttributeInfo_t2119)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelper.h"
// Metadata Definition System.MonoTouchAOTHelper
extern TypeInfo MonoTouchAOTHelper_t2121_il2cpp_TypeInfo;
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelperMethodDeclarations.h"
static const EncodedMethodIndex MonoTouchAOTHelper_t2121_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTouchAOTHelper_t2121_0_0_0;
extern const Il2CppType MonoTouchAOTHelper_t2121_1_0_0;
struct MonoTouchAOTHelper_t2121;
const Il2CppTypeDefinitionMetadata MonoTouchAOTHelper_t2121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTouchAOTHelper_t2121_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9235/* fieldStart */
	, 13650/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTouchAOTHelper_t2121_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTouchAOTHelper"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTouchAOTHelper_t2121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTouchAOTHelper_t2121_0_0_0/* byval_arg */
	, &MonoTouchAOTHelper_t2121_1_0_0/* this_arg */
	, &MonoTouchAOTHelper_t2121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTouchAOTHelper_t2121)/* instance_size */
	, sizeof (MonoTouchAOTHelper_t2121)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoTouchAOTHelper_t2121_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfo.h"
// Metadata Definition System.MonoTypeInfo
extern TypeInfo MonoTypeInfo_t2122_il2cpp_TypeInfo;
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfoMethodDeclarations.h"
static const EncodedMethodIndex MonoTypeInfo_t2122_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTypeInfo_t2122_0_0_0;
extern const Il2CppType MonoTypeInfo_t2122_1_0_0;
struct MonoTypeInfo_t2122;
const Il2CppTypeDefinitionMetadata MonoTypeInfo_t2122_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTypeInfo_t2122_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9236/* fieldStart */
	, 13651/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoTypeInfo_t2122_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTypeInfo"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoTypeInfo_t2122_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTypeInfo_t2122_0_0_0/* byval_arg */
	, &MonoTypeInfo_t2122_1_0_0/* this_arg */
	, &MonoTypeInfo_t2122_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTypeInfo_t2122)/* instance_size */
	, sizeof (MonoTypeInfo_t2122)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoType
#include "mscorlib_System_MonoType.h"
// Metadata Definition System.MonoType
extern TypeInfo MonoType_t_il2cpp_TypeInfo;
// System.MonoType
#include "mscorlib_System_MonoTypeMethodDeclarations.h"
static const EncodedMethodIndex MonoType_t_VTable[82] = 
{
	2626,
	601,
	2627,
	4304,
	4305,
	4306,
	4307,
	4308,
	4309,
	4310,
	4311,
	4306,
	4312,
	4305,
	4313,
	4314,
	2634,
	4315,
	4316,
	2635,
	2636,
	2637,
	2638,
	2639,
	2640,
	2641,
	2642,
	2643,
	2644,
	2645,
	2646,
	2647,
	2648,
	2649,
	4317,
	4318,
	4319,
	2651,
	4320,
	4321,
	2653,
	2654,
	4322,
	4323,
	4324,
	4325,
	2655,
	2656,
	2657,
	2658,
	4326,
	4327,
	4328,
	2659,
	2660,
	2661,
	2662,
	4329,
	4330,
	4331,
	4332,
	4333,
	4334,
	4335,
	4336,
	2663,
	2664,
	2665,
	2666,
	2667,
	2668,
	2669,
	4337,
	4338,
	4339,
	4340,
	2672,
	4341,
	2674,
	2675,
	4342,
	4343,
};
static const Il2CppType* MonoType_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
extern const Il2CppType IReflect_t3442_0_0_0;
extern const Il2CppType _Type_t3440_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t2188_0_0_0;
extern const Il2CppType _MemberInfo_t3441_0_0_0;
static Il2CppInterfaceOffsetPair MonoType_t_InterfacesOffsets[] = 
{
	{ &IReflect_t3442_0_0_0, 14},
	{ &_Type_t3440_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &ISerializable_t2210_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType MonoType_t_1_0_0;
extern const Il2CppType Type_t_0_0_0;
struct MonoType_t;
const Il2CppTypeDefinitionMetadata MonoType_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoType_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoType_t_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, MonoType_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9238/* fieldStart */
	, 13652/* methodStart */
	, -1/* eventStart */
	, 2598/* propertyStart */

};
TypeInfo MonoType_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoType"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MonoType_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoType_t_0_0_0/* byval_arg */
	, &MonoType_t_1_0_0/* this_arg */
	, &MonoType_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoType_t)/* instance_size */
	, sizeof (MonoType_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 14/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 82/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedException.h"
// Metadata Definition System.MulticastNotSupportedException
extern TypeInfo MulticastNotSupportedException_t2123_il2cpp_TypeInfo;
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedExceptionMethodDeclarations.h"
static const EncodedMethodIndex MulticastNotSupportedException_t2123_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair MulticastNotSupportedException_t2123_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MulticastNotSupportedException_t2123_0_0_0;
extern const Il2CppType MulticastNotSupportedException_t2123_1_0_0;
struct MulticastNotSupportedException_t2123;
const Il2CppTypeDefinitionMetadata MulticastNotSupportedException_t2123_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MulticastNotSupportedException_t2123_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, MulticastNotSupportedException_t2123_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13702/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MulticastNotSupportedException_t2123_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MulticastNotSupportedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &MulticastNotSupportedException_t2123_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3373/* custom_attributes_cache */
	, &MulticastNotSupportedException_t2123_0_0_0/* byval_arg */
	, &MulticastNotSupportedException_t2123_1_0_0/* this_arg */
	, &MulticastNotSupportedException_t2123_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MulticastNotSupportedException_t2123)/* instance_size */
	, sizeof (MulticastNotSupportedException_t2123)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"
// Metadata Definition System.NonSerializedAttribute
extern TypeInfo NonSerializedAttribute_t2124_il2cpp_TypeInfo;
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttributeMethodDeclarations.h"
static const EncodedMethodIndex NonSerializedAttribute_t2124_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair NonSerializedAttribute_t2124_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NonSerializedAttribute_t2124_0_0_0;
extern const Il2CppType NonSerializedAttribute_t2124_1_0_0;
struct NonSerializedAttribute_t2124;
const Il2CppTypeDefinitionMetadata NonSerializedAttribute_t2124_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NonSerializedAttribute_t2124_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, NonSerializedAttribute_t2124_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13705/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NonSerializedAttribute_t2124_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonSerializedAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NonSerializedAttribute_t2124_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3374/* custom_attributes_cache */
	, &NonSerializedAttribute_t2124_0_0_0/* byval_arg */
	, &NonSerializedAttribute_t2124_1_0_0/* this_arg */
	, &NonSerializedAttribute_t2124_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonSerializedAttribute_t2124)/* instance_size */
	, sizeof (NonSerializedAttribute_t2124)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// Metadata Definition System.NotImplementedException
extern TypeInfo NotImplementedException_t1191_il2cpp_TypeInfo;
// System.NotImplementedException
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
static const EncodedMethodIndex NotImplementedException_t1191_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair NotImplementedException_t1191_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotImplementedException_t1191_0_0_0;
extern const Il2CppType NotImplementedException_t1191_1_0_0;
struct NotImplementedException_t1191;
const Il2CppTypeDefinitionMetadata NotImplementedException_t1191_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotImplementedException_t1191_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, NotImplementedException_t1191_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13706/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NotImplementedException_t1191_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotImplementedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NotImplementedException_t1191_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3375/* custom_attributes_cache */
	, &NotImplementedException_t1191_0_0_0/* byval_arg */
	, &NotImplementedException_t1191_1_0_0/* this_arg */
	, &NotImplementedException_t1191_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotImplementedException_t1191)/* instance_size */
	, sizeof (NotImplementedException_t1191)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// Metadata Definition System.NotSupportedException
extern TypeInfo NotSupportedException_t476_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
static const EncodedMethodIndex NotSupportedException_t476_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair NotSupportedException_t476_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotSupportedException_t476_0_0_0;
extern const Il2CppType NotSupportedException_t476_1_0_0;
struct NotSupportedException_t476;
const Il2CppTypeDefinitionMetadata NotSupportedException_t476_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotSupportedException_t476_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, NotSupportedException_t476_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9239/* fieldStart */
	, 13709/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NotSupportedException_t476_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotSupportedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NotSupportedException_t476_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3376/* custom_attributes_cache */
	, &NotSupportedException_t476_0_0_0/* byval_arg */
	, &NotSupportedException_t476_1_0_0/* this_arg */
	, &NotSupportedException_t476_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotSupportedException_t476)/* instance_size */
	, sizeof (NotSupportedException_t476)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// Metadata Definition System.NullReferenceException
extern TypeInfo NullReferenceException_t1106_il2cpp_TypeInfo;
// System.NullReferenceException
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
static const EncodedMethodIndex NullReferenceException_t1106_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair NullReferenceException_t1106_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullReferenceException_t1106_0_0_0;
extern const Il2CppType NullReferenceException_t1106_1_0_0;
struct NullReferenceException_t1106;
const Il2CppTypeDefinitionMetadata NullReferenceException_t1106_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullReferenceException_t1106_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, NullReferenceException_t1106_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9240/* fieldStart */
	, 13712/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NullReferenceException_t1106_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullReferenceException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NullReferenceException_t1106_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3377/* custom_attributes_cache */
	, &NullReferenceException_t1106_0_0_0/* byval_arg */
	, &NullReferenceException_t1106_1_0_0/* this_arg */
	, &NullReferenceException_t1106_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullReferenceException_t1106)/* instance_size */
	, sizeof (NullReferenceException_t1106)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NumberFormatter
#include "mscorlib_System_NumberFormatter.h"
// Metadata Definition System.NumberFormatter
extern TypeInfo NumberFormatter_t2126_il2cpp_TypeInfo;
// System.NumberFormatter
#include "mscorlib_System_NumberFormatterMethodDeclarations.h"
extern const Il2CppType CustomInfo_t2125_0_0_0;
static const Il2CppType* NumberFormatter_t2126_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CustomInfo_t2125_0_0_0,
};
static const EncodedMethodIndex NumberFormatter_t2126_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatter_t2126_0_0_0;
extern const Il2CppType NumberFormatter_t2126_1_0_0;
struct NumberFormatter_t2126;
const Il2CppTypeDefinitionMetadata NumberFormatter_t2126_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NumberFormatter_t2126_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatter_t2126_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9241/* fieldStart */
	, 13715/* methodStart */
	, -1/* eventStart */
	, 2612/* propertyStart */

};
TypeInfo NumberFormatter_t2126_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatter"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &NumberFormatter_t2126_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NumberFormatter_t2126_0_0_0/* byval_arg */
	, &NumberFormatter_t2126_1_0_0/* this_arg */
	, &NumberFormatter_t2126_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatter_t2126)/* instance_size */
	, sizeof (NumberFormatter_t2126)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatter_t2126_StaticFields)/* static_fields_size */
	, sizeof(NumberFormatter_t2126_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 94/* method_count */
	, 6/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfo.h"
// Metadata Definition System.NumberFormatter/CustomInfo
extern TypeInfo CustomInfo_t2125_il2cpp_TypeInfo;
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfoMethodDeclarations.h"
static const EncodedMethodIndex CustomInfo_t2125_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CustomInfo_t2125_1_0_0;
struct CustomInfo_t2125;
const Il2CppTypeDefinitionMetadata CustomInfo_t2125_DefinitionMetadata = 
{
	&NumberFormatter_t2126_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CustomInfo_t2125_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9267/* fieldStart */
	, 13809/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CustomInfo_t2125_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CustomInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &CustomInfo_t2125_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CustomInfo_t2125_0_0_0/* byval_arg */
	, &CustomInfo_t2125_1_0_0/* this_arg */
	, &CustomInfo_t2125_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CustomInfo_t2125)/* instance_size */
	, sizeof (CustomInfo_t2125)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// Metadata Definition System.ObjectDisposedException
extern TypeInfo ObjectDisposedException_t1192_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
static const EncodedMethodIndex ObjectDisposedException_t1192_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	4344,
	1318,
	4345,
	1320,
	1321,
	4344,
	1322,
};
static Il2CppInterfaceOffsetPair ObjectDisposedException_t1192_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectDisposedException_t1192_0_0_0;
extern const Il2CppType ObjectDisposedException_t1192_1_0_0;
struct ObjectDisposedException_t1192;
const Il2CppTypeDefinitionMetadata ObjectDisposedException_t1192_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectDisposedException_t1192_InterfacesOffsets/* interfaceOffsets */
	, &InvalidOperationException_t1359_0_0_0/* parent */
	, ObjectDisposedException_t1192_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9281/* fieldStart */
	, 13813/* methodStart */
	, -1/* eventStart */
	, 2618/* propertyStart */

};
TypeInfo ObjectDisposedException_t1192_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectDisposedException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ObjectDisposedException_t1192_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3379/* custom_attributes_cache */
	, &ObjectDisposedException_t1192_0_0_0/* byval_arg */
	, &ObjectDisposedException_t1192_1_0_0/* this_arg */
	, &ObjectDisposedException_t1192_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectDisposedException_t1192)/* instance_size */
	, sizeof (ObjectDisposedException_t1192)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OperatingSystem
#include "mscorlib_System_OperatingSystem.h"
// Metadata Definition System.OperatingSystem
extern TypeInfo OperatingSystem_t2104_il2cpp_TypeInfo;
// System.OperatingSystem
#include "mscorlib_System_OperatingSystemMethodDeclarations.h"
static const EncodedMethodIndex OperatingSystem_t2104_VTable[5] = 
{
	626,
	601,
	627,
	4346,
	4347,
};
static const Il2CppType* OperatingSystem_t2104_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair OperatingSystem_t2104_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OperatingSystem_t2104_0_0_0;
extern const Il2CppType OperatingSystem_t2104_1_0_0;
struct OperatingSystem_t2104;
const Il2CppTypeDefinitionMetadata OperatingSystem_t2104_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OperatingSystem_t2104_InterfacesTypeInfos/* implementedInterfaces */
	, OperatingSystem_t2104_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OperatingSystem_t2104_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9283/* fieldStart */
	, 13818/* methodStart */
	, -1/* eventStart */
	, 2619/* propertyStart */

};
TypeInfo OperatingSystem_t2104_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OperatingSystem"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OperatingSystem_t2104_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3380/* custom_attributes_cache */
	, &OperatingSystem_t2104_0_0_0/* byval_arg */
	, &OperatingSystem_t2104_1_0_0/* this_arg */
	, &OperatingSystem_t2104_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OperatingSystem_t2104)/* instance_size */
	, sizeof (OperatingSystem_t2104)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryException.h"
// Metadata Definition System.OutOfMemoryException
extern TypeInfo OutOfMemoryException_t2127_il2cpp_TypeInfo;
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
static const EncodedMethodIndex OutOfMemoryException_t2127_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair OutOfMemoryException_t2127_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutOfMemoryException_t2127_0_0_0;
extern const Il2CppType OutOfMemoryException_t2127_1_0_0;
struct OutOfMemoryException_t2127;
const Il2CppTypeDefinitionMetadata OutOfMemoryException_t2127_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutOfMemoryException_t2127_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, OutOfMemoryException_t2127_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9286/* fieldStart */
	, 13822/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OutOfMemoryException_t2127_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutOfMemoryException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OutOfMemoryException_t2127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3381/* custom_attributes_cache */
	, &OutOfMemoryException_t2127_0_0_0/* byval_arg */
	, &OutOfMemoryException_t2127_1_0_0/* this_arg */
	, &OutOfMemoryException_t2127_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutOfMemoryException_t2127)/* instance_size */
	, sizeof (OutOfMemoryException_t2127)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OverflowException
#include "mscorlib_System_OverflowException.h"
// Metadata Definition System.OverflowException
extern TypeInfo OverflowException_t2128_il2cpp_TypeInfo;
// System.OverflowException
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
static const EncodedMethodIndex OverflowException_t2128_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair OverflowException_t2128_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OverflowException_t2128_0_0_0;
extern const Il2CppType OverflowException_t2128_1_0_0;
struct OverflowException_t2128;
const Il2CppTypeDefinitionMetadata OverflowException_t2128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OverflowException_t2128_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t1336_0_0_0/* parent */
	, OverflowException_t2128_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9287/* fieldStart */
	, 13824/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OverflowException_t2128_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OverflowException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OverflowException_t2128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3382/* custom_attributes_cache */
	, &OverflowException_t2128_0_0_0/* byval_arg */
	, &OverflowException_t2128_1_0_0/* this_arg */
	, &OverflowException_t2128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OverflowException_t2128)/* instance_size */
	, sizeof (OverflowException_t2128)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
// Metadata Definition System.PlatformID
extern TypeInfo PlatformID_t2129_il2cpp_TypeInfo;
// System.PlatformID
#include "mscorlib_System_PlatformIDMethodDeclarations.h"
static const EncodedMethodIndex PlatformID_t2129_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair PlatformID_t2129_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PlatformID_t2129_0_0_0;
extern const Il2CppType PlatformID_t2129_1_0_0;
const Il2CppTypeDefinitionMetadata PlatformID_t2129_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlatformID_t2129_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, PlatformID_t2129_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9288/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PlatformID_t2129_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformID"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3383/* custom_attributes_cache */
	, &PlatformID_t2129_0_0_0/* byval_arg */
	, &PlatformID_t2129_1_0_0/* this_arg */
	, &PlatformID_t2129_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformID_t2129)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PlatformID_t2129)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Random
#include "mscorlib_System_Random.h"
// Metadata Definition System.Random
extern TypeInfo Random_t297_il2cpp_TypeInfo;
// System.Random
#include "mscorlib_System_RandomMethodDeclarations.h"
static const EncodedMethodIndex Random_t297_VTable[6] = 
{
	626,
	601,
	627,
	628,
	4348,
	4349,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Random_t297_0_0_0;
extern const Il2CppType Random_t297_1_0_0;
struct Random_t297;
const Il2CppTypeDefinitionMetadata Random_t297_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t297_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9296/* fieldStart */
	, 13827/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Random_t297_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Random_t297_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3384/* custom_attributes_cache */
	, &Random_t297_0_0_0/* byval_arg */
	, &Random_t297_1_0_0/* this_arg */
	, &Random_t297_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t297)/* instance_size */
	, sizeof (Random_t297)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RankException
#include "mscorlib_System_RankException.h"
// Metadata Definition System.RankException
extern TypeInfo RankException_t2130_il2cpp_TypeInfo;
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
static const EncodedMethodIndex RankException_t2130_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair RankException_t2130_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RankException_t2130_0_0_0;
extern const Il2CppType RankException_t2130_1_0_0;
struct RankException_t2130;
const Il2CppTypeDefinitionMetadata RankException_t2130_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RankException_t2130_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, RankException_t2130_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13831/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RankException_t2130_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RankException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RankException_t2130_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3385/* custom_attributes_cache */
	, &RankException_t2130_0_0_0/* byval_arg */
	, &RankException_t2130_1_0_0/* this_arg */
	, &RankException_t2130_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RankException_t2130)/* instance_size */
	, sizeof (RankException_t2130)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgs.h"
// Metadata Definition System.ResolveEventArgs
extern TypeInfo ResolveEventArgs_t2131_il2cpp_TypeInfo;
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgsMethodDeclarations.h"
static const EncodedMethodIndex ResolveEventArgs_t2131_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventArgs_t2131_0_0_0;
extern const Il2CppType ResolveEventArgs_t2131_1_0_0;
struct ResolveEventArgs_t2131;
const Il2CppTypeDefinitionMetadata ResolveEventArgs_t2131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1223_0_0_0/* parent */
	, ResolveEventArgs_t2131_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9299/* fieldStart */
	, 13834/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResolveEventArgs_t2131_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventArgs"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ResolveEventArgs_t2131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3386/* custom_attributes_cache */
	, &ResolveEventArgs_t2131_0_0_0/* byval_arg */
	, &ResolveEventArgs_t2131_1_0_0/* this_arg */
	, &ResolveEventArgs_t2131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventArgs_t2131)/* instance_size */
	, sizeof (ResolveEventArgs_t2131)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
// Metadata Definition System.RuntimeMethodHandle
extern TypeInfo RuntimeMethodHandle_t2132_il2cpp_TypeInfo;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandleMethodDeclarations.h"
static const EncodedMethodIndex RuntimeMethodHandle_t2132_VTable[5] = 
{
	4350,
	601,
	4351,
	654,
	4352,
};
static const Il2CppType* RuntimeMethodHandle_t2132_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeMethodHandle_t2132_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeMethodHandle_t2132_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t2132_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeMethodHandle_t2132_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeMethodHandle_t2132_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeMethodHandle_t2132_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RuntimeMethodHandle_t2132_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9300/* fieldStart */
	, 13835/* methodStart */
	, -1/* eventStart */
	, 2620/* propertyStart */

};
TypeInfo RuntimeMethodHandle_t2132_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeMethodHandle"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &RuntimeMethodHandle_t2132_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3387/* custom_attributes_cache */
	, &RuntimeMethodHandle_t2132_0_0_0/* byval_arg */
	, &RuntimeMethodHandle_t2132_1_0_0/* this_arg */
	, &RuntimeMethodHandle_t2132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeMethodHandle_t2132)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeMethodHandle_t2132)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeMethodHandle_t2132 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// Metadata Definition System.StringComparer
extern TypeInfo StringComparer_t1113_il2cpp_TypeInfo;
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
static const EncodedMethodIndex StringComparer_t1113_VTable[13] = 
{
	626,
	601,
	627,
	628,
	4353,
	4354,
	4355,
	4356,
	4357,
	4358,
	0,
	0,
	0,
};
extern const Il2CppType IComparer_1_t3899_0_0_0;
extern const Il2CppType IEqualityComparer_1_t2320_0_0_0;
extern const Il2CppType IEqualityComparer_t1387_0_0_0;
static const Il2CppType* StringComparer_t1113_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3899_0_0_0,
	&IEqualityComparer_1_t2320_0_0_0,
	&IComparer_t416_0_0_0,
	&IEqualityComparer_t1387_0_0_0,
};
static Il2CppInterfaceOffsetPair StringComparer_t1113_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3899_0_0_0, 4},
	{ &IEqualityComparer_1_t2320_0_0_0, 5},
	{ &IComparer_t416_0_0_0, 7},
	{ &IEqualityComparer_t1387_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparer_t1113_0_0_0;
extern const Il2CppType StringComparer_t1113_1_0_0;
struct StringComparer_t1113;
const Il2CppTypeDefinitionMetadata StringComparer_t1113_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringComparer_t1113_InterfacesTypeInfos/* implementedInterfaces */
	, StringComparer_t1113_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringComparer_t1113_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9301/* fieldStart */
	, 13841/* methodStart */
	, -1/* eventStart */
	, 2621/* propertyStart */

};
TypeInfo StringComparer_t1113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &StringComparer_t1113_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3389/* custom_attributes_cache */
	, &StringComparer_t1113_0_0_0/* byval_arg */
	, &StringComparer_t1113_1_0_0/* this_arg */
	, &StringComparer_t1113_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparer_t1113)/* instance_size */
	, sizeof (StringComparer_t1113)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringComparer_t1113_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparer.h"
// Metadata Definition System.CultureAwareComparer
extern TypeInfo CultureAwareComparer_t2133_il2cpp_TypeInfo;
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparerMethodDeclarations.h"
static const EncodedMethodIndex CultureAwareComparer_t2133_VTable[13] = 
{
	626,
	601,
	627,
	628,
	4359,
	4360,
	4361,
	4356,
	4357,
	4358,
	4359,
	4360,
	4361,
};
static Il2CppInterfaceOffsetPair CultureAwareComparer_t2133_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3899_0_0_0, 4},
	{ &IEqualityComparer_1_t2320_0_0_0, 5},
	{ &IComparer_t416_0_0_0, 7},
	{ &IEqualityComparer_t1387_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureAwareComparer_t2133_0_0_0;
extern const Il2CppType CultureAwareComparer_t2133_1_0_0;
struct CultureAwareComparer_t2133;
const Il2CppTypeDefinitionMetadata CultureAwareComparer_t2133_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CultureAwareComparer_t2133_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1113_0_0_0/* parent */
	, CultureAwareComparer_t2133_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9305/* fieldStart */
	, 13851/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CultureAwareComparer_t2133_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureAwareComparer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &CultureAwareComparer_t2133_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CultureAwareComparer_t2133_0_0_0/* byval_arg */
	, &CultureAwareComparer_t2133_1_0_0/* this_arg */
	, &CultureAwareComparer_t2133_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureAwareComparer_t2133)/* instance_size */
	, sizeof (CultureAwareComparer_t2133)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparer.h"
// Metadata Definition System.OrdinalComparer
extern TypeInfo OrdinalComparer_t2134_il2cpp_TypeInfo;
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparerMethodDeclarations.h"
static const EncodedMethodIndex OrdinalComparer_t2134_VTable[13] = 
{
	626,
	601,
	627,
	628,
	4362,
	4363,
	4364,
	4356,
	4357,
	4358,
	4362,
	4363,
	4364,
};
static Il2CppInterfaceOffsetPair OrdinalComparer_t2134_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3899_0_0_0, 4},
	{ &IEqualityComparer_1_t2320_0_0_0, 5},
	{ &IComparer_t416_0_0_0, 7},
	{ &IEqualityComparer_t1387_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OrdinalComparer_t2134_0_0_0;
extern const Il2CppType OrdinalComparer_t2134_1_0_0;
struct OrdinalComparer_t2134;
const Il2CppTypeDefinitionMetadata OrdinalComparer_t2134_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OrdinalComparer_t2134_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1113_0_0_0/* parent */
	, OrdinalComparer_t2134_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9307/* fieldStart */
	, 13855/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo OrdinalComparer_t2134_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrdinalComparer"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &OrdinalComparer_t2134_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrdinalComparer_t2134_0_0_0/* byval_arg */
	, &OrdinalComparer_t2134_1_0_0/* this_arg */
	, &OrdinalComparer_t2134_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrdinalComparer_t2134)/* instance_size */
	, sizeof (OrdinalComparer_t2134)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// Metadata Definition System.StringComparison
extern TypeInfo StringComparison_t2135_il2cpp_TypeInfo;
// System.StringComparison
#include "mscorlib_System_StringComparisonMethodDeclarations.h"
static const EncodedMethodIndex StringComparison_t2135_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair StringComparison_t2135_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparison_t2135_0_0_0;
extern const Il2CppType StringComparison_t2135_1_0_0;
const Il2CppTypeDefinitionMetadata StringComparison_t2135_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringComparison_t2135_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, StringComparison_t2135_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9308/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StringComparison_t2135_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparison"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3390/* custom_attributes_cache */
	, &StringComparison_t2135_0_0_0/* byval_arg */
	, &StringComparison_t2135_1_0_0/* this_arg */
	, &StringComparison_t2135_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparison_t2135)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringComparison_t2135)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
// Metadata Definition System.StringSplitOptions
extern TypeInfo StringSplitOptions_t2136_il2cpp_TypeInfo;
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptionsMethodDeclarations.h"
static const EncodedMethodIndex StringSplitOptions_t2136_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair StringSplitOptions_t2136_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringSplitOptions_t2136_0_0_0;
extern const Il2CppType StringSplitOptions_t2136_1_0_0;
const Il2CppTypeDefinitionMetadata StringSplitOptions_t2136_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringSplitOptions_t2136_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, StringSplitOptions_t2136_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9315/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StringSplitOptions_t2136_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringSplitOptions"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3391/* custom_attributes_cache */
	, &StringSplitOptions_t2136_0_0_0/* byval_arg */
	, &StringSplitOptions_t2136_1_0_0/* this_arg */
	, &StringSplitOptions_t2136_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringSplitOptions_t2136)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringSplitOptions_t2136)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.SystemException
#include "mscorlib_System_SystemException.h"
// Metadata Definition System.SystemException
extern TypeInfo SystemException_t1536_il2cpp_TypeInfo;
// System.SystemException
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
static const EncodedMethodIndex SystemException_t1536_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair SystemException_t1536_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SystemException_t1536_1_0_0;
struct SystemException_t1536;
const Il2CppTypeDefinitionMetadata SystemException_t1536_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SystemException_t1536_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, SystemException_t1536_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13859/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SystemException_t1536_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemException"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &SystemException_t1536_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3392/* custom_attributes_cache */
	, &SystemException_t1536_0_0_0/* byval_arg */
	, &SystemException_t1536_1_0_0/* this_arg */
	, &SystemException_t1536_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemException_t1536)/* instance_size */
	, sizeof (SystemException_t1536)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// Metadata Definition System.ThreadStaticAttribute
extern TypeInfo ThreadStaticAttribute_t2137_il2cpp_TypeInfo;
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
static const EncodedMethodIndex ThreadStaticAttribute_t2137_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ThreadStaticAttribute_t2137_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStaticAttribute_t2137_0_0_0;
extern const Il2CppType ThreadStaticAttribute_t2137_1_0_0;
struct ThreadStaticAttribute_t2137;
const Il2CppTypeDefinitionMetadata ThreadStaticAttribute_t2137_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStaticAttribute_t2137_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ThreadStaticAttribute_t2137_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13863/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ThreadStaticAttribute_t2137_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStaticAttribute"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &ThreadStaticAttribute_t2137_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3393/* custom_attributes_cache */
	, &ThreadStaticAttribute_t2137_0_0_0/* byval_arg */
	, &ThreadStaticAttribute_t2137_1_0_0/* this_arg */
	, &ThreadStaticAttribute_t2137_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStaticAttribute_t2137)/* instance_size */
	, sizeof (ThreadStaticAttribute_t2137)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// Metadata Definition System.TimeSpan
extern TypeInfo TimeSpan_t1437_il2cpp_TypeInfo;
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
static const EncodedMethodIndex TimeSpan_t1437_VTable[7] = 
{
	4365,
	601,
	4366,
	4367,
	4368,
	4369,
	4370,
};
extern const Il2CppType IComparable_1_t3900_0_0_0;
extern const Il2CppType IEquatable_1_t3901_0_0_0;
static const Il2CppType* TimeSpan_t1437_InterfacesTypeInfos[] = 
{
	&IComparable_t2192_0_0_0,
	&IComparable_1_t3900_0_0_0,
	&IEquatable_1_t3901_0_0_0,
};
static Il2CppInterfaceOffsetPair TimeSpan_t1437_InterfacesOffsets[] = 
{
	{ &IComparable_t2192_0_0_0, 4},
	{ &IComparable_1_t3900_0_0_0, 5},
	{ &IEquatable_1_t3901_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeSpan_t1437_0_0_0;
extern const Il2CppType TimeSpan_t1437_1_0_0;
const Il2CppTypeDefinitionMetadata TimeSpan_t1437_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TimeSpan_t1437_InterfacesTypeInfos/* implementedInterfaces */
	, TimeSpan_t1437_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, TimeSpan_t1437_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 9318/* fieldStart */
	, 13864/* methodStart */
	, -1/* eventStart */
	, 2623/* propertyStart */

};
TypeInfo TimeSpan_t1437_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeSpan"/* name */
	, "System"/* namespaze */
	, NULL/* methods */
	, &TimeSpan_t1437_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3394/* custom_attributes_cache */
	, &TimeSpan_t1437_0_0_0/* byval_arg */
	, &TimeSpan_t1437_1_0_0/* this_arg */
	, &TimeSpan_t1437_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeSpan_t1437)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeSpan_t1437)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TimeSpan_t1437 )/* native_size */
	, sizeof(TimeSpan_t1437_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 41/* method_count */
	, 11/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
