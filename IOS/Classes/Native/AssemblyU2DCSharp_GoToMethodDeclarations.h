﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GoTo
struct GoTo_t315;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void GoTo::.ctor()
extern "C" void GoTo__ctor_m1895 (GoTo_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoTo::.cctor()
extern "C" void GoTo__cctor_m1896 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GoTo GoTo::get_Instance()
extern "C" GoTo_t315 * GoTo_get_Instance_m1897 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoTo::Start()
extern "C" void GoTo_Start_m1898 (GoTo_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoTo::GotoPos()
extern "C" void GoTo_GotoPos_m1899 (GoTo_t315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoTo::GoHere(System.Single,System.Boolean)
extern "C" void GoTo_GoHere_m1900 (GoTo_t315 * __this, float ___time, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GoTo::WaitAndEnable(System.Single,System.Boolean)
extern "C" Object_t * GoTo_WaitAndEnable_m1901 (GoTo_t315 * __this, float ___time, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoTo::myValue(System.Single)
extern "C" void GoTo_myValue_m1902 (GoTo_t315 * __this, float ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
