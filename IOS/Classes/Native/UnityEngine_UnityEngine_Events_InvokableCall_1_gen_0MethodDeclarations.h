﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
struct InvokableCall_1_t2533;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t769;
// System.Object[]
struct ObjectU5BU5D_t470;

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m17767_gshared (InvokableCall_1_t2533 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m17767(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2533 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m17767_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m17768_gshared (InvokableCall_1_t2533 * __this, UnityAction_1_t769 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m17768(__this, ___callback, method) (( void (*) (InvokableCall_1_t2533 *, UnityAction_1_t769 *, const MethodInfo*))InvokableCall_1__ctor_m17768_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m17769_gshared (InvokableCall_1_t2533 * __this, ObjectU5BU5D_t470* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m17769(__this, ___args, method) (( void (*) (InvokableCall_1_t2533 *, ObjectU5BU5D_t470*, const MethodInfo*))InvokableCall_1_Invoke_m17769_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Color>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m17770_gshared (InvokableCall_1_t2533 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m17770(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2533 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m17770_gshared)(__this, ___targetObj, ___method, method)
