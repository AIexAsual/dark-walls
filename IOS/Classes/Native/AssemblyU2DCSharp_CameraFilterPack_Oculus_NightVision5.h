﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// System.Single[]
struct SingleU5BU5D_t72;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Oculus_NightVision5
struct  CameraFilterPack_Oculus_NightVision5_t165  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_Oculus_NightVision5::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Oculus_NightVision5::SCShader
	Shader_t1 * ___SCShader_3;
	// System.Single CameraFilterPack_Oculus_NightVision5::FadeFX
	float ___FadeFX_4;
	// System.Single CameraFilterPack_Oculus_NightVision5::_Size
	float ____Size_5;
	// System.Single CameraFilterPack_Oculus_NightVision5::_Smooth
	float ____Smooth_6;
	// System.Single CameraFilterPack_Oculus_NightVision5::_Dist
	float ____Dist_7;
	// System.Single CameraFilterPack_Oculus_NightVision5::TimeX
	float ___TimeX_8;
	// UnityEngine.Vector4 CameraFilterPack_Oculus_NightVision5::ScreenResolution
	Vector4_t5  ___ScreenResolution_9;
	// UnityEngine.Material CameraFilterPack_Oculus_NightVision5::SCMaterial
	Material_t2 * ___SCMaterial_10;
	// System.Single[] CameraFilterPack_Oculus_NightVision5::Matrix9
	SingleU5BU5D_t72* ___Matrix9_11;
	// System.Single CameraFilterPack_Oculus_NightVision5::Brightness
	float ___Brightness_12;
};
