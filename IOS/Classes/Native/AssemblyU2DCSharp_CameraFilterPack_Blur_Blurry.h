﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blur_Blurry
struct  CameraFilterPack_Blur_Blurry_t49  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Blur_Blurry::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Blurry::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Blurry::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Blurry::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Blur_Blurry::Amount
	float ___Amount_6;
	// System.Int32 CameraFilterPack_Blur_Blurry::FastFilter
	int32_t ___FastFilter_7;
};
struct CameraFilterPack_Blur_Blurry_t49_StaticFields{
	// System.Single CameraFilterPack_Blur_Blurry::ChangeAmount
	float ___ChangeAmount_8;
	// System.Int32 CameraFilterPack_Blur_Blurry::ChangeFastFilter
	int32_t ___ChangeFastFilter_9;
};
