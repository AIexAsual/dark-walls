﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WayPoints
struct WayPoints_t364;

// System.Void WayPoints::.ctor()
extern "C" void WayPoints__ctor_m2108 (WayPoints_t364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WayPoints::.cctor()
extern "C" void WayPoints__cctor_m2109 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
