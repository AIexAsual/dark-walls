﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// PlayerController/<WaitAndSpace>c__Iterator15
struct U3CWaitAndSpaceU3Ec__Iterator15_t395;
// System.Object
struct Object_t;

// System.Void PlayerController/<WaitAndSpace>c__Iterator15::.ctor()
extern "C" void U3CWaitAndSpaceU3Ec__Iterator15__ctor_m2260 (U3CWaitAndSpaceU3Ec__Iterator15_t395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndSpace>c__Iterator15::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CWaitAndSpaceU3Ec__Iterator15_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2261 (U3CWaitAndSpaceU3Ec__Iterator15_t395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerController/<WaitAndSpace>c__Iterator15::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitAndSpaceU3Ec__Iterator15_System_Collections_IEnumerator_get_Current_m2262 (U3CWaitAndSpaceU3Ec__Iterator15_t395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerController/<WaitAndSpace>c__Iterator15::MoveNext()
extern "C" bool U3CWaitAndSpaceU3Ec__Iterator15_MoveNext_m2263 (U3CWaitAndSpaceU3Ec__Iterator15_t395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndSpace>c__Iterator15::Dispose()
extern "C" void U3CWaitAndSpaceU3Ec__Iterator15_Dispose_m2264 (U3CWaitAndSpaceU3Ec__Iterator15_t395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController/<WaitAndSpace>c__Iterator15::Reset()
extern "C" void U3CWaitAndSpaceU3Ec__Iterator15_Reset_m2265 (U3CWaitAndSpaceU3Ec__Iterator15_t395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
