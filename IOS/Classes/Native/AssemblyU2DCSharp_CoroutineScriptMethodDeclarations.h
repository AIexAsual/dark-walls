﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CoroutineScript
struct CoroutineScript_t310;
// System.Collections.IEnumerator
struct IEnumerator_t464;

// System.Void CoroutineScript::.ctor()
extern "C" void CoroutineScript__ctor_m1869 (CoroutineScript_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CoroutineScript::get_CoroutineTime()
extern "C" float CoroutineScript_get_CoroutineTime_m1870 (CoroutineScript_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineScript::set_CoroutineTime(System.Single)
extern "C" void CoroutineScript_set_CoroutineTime_m1871 (CoroutineScript_t310 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CoroutineScript::testCor(System.Single)
extern "C" Object_t * CoroutineScript_testCor_m1872 (CoroutineScript_t310 * __this, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineScript::Start()
extern "C" void CoroutineScript_Start_m1873 (CoroutineScript_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineScript::Update()
extern "C" void CoroutineScript_Update_m1874 (CoroutineScript_t310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
