﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t698;
// UnityEngine.Object
struct Object_t473;
struct Object_t473_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t648;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m4852 (DrivenRectTransformTracker_t698 * __this, Object_t473 * ___driver, RectTransform_t648 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m4850 (DrivenRectTransformTracker_t698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
