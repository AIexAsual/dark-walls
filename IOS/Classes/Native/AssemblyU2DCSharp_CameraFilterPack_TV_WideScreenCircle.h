﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_WideScreenCircle
struct  CameraFilterPack_TV_WideScreenCircle_t200  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_WideScreenCircle::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_WideScreenCircle::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_TV_WideScreenCircle::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_TV_WideScreenCircle::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_TV_WideScreenCircle::Size
	float ___Size_6;
	// System.Single CameraFilterPack_TV_WideScreenCircle::Smooth
	float ___Smooth_7;
	// System.Single CameraFilterPack_TV_WideScreenCircle::StretchX
	float ___StretchX_8;
	// System.Single CameraFilterPack_TV_WideScreenCircle::StretchY
	float ___StretchY_9;
};
struct CameraFilterPack_TV_WideScreenCircle_t200_StaticFields{
	// System.Single CameraFilterPack_TV_WideScreenCircle::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_TV_WideScreenCircle::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_TV_WideScreenCircle::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_TV_WideScreenCircle::ChangeValue4
	float ___ChangeValue4_13;
};
