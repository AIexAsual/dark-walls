﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Space>
struct Collection_1_t2442;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Space[]
struct SpaceU5BU5D_t450;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Space>
struct IEnumerator_1_t3037;
// System.Collections.Generic.IList`1<UnityEngine.Space>
struct IList_1_t568;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::.ctor()
extern "C" void Collection_1__ctor_m16300_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16300(__this, method) (( void (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1__ctor_m16300_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16301_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16301(__this, method) (( bool (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16301_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16302_gshared (Collection_1_t2442 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16302(__this, ___array, ___index, method) (( void (*) (Collection_1_t2442 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16302_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16303_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16303(__this, method) (( Object_t * (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16303_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16304_gshared (Collection_1_t2442 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16304(__this, ___value, method) (( int32_t (*) (Collection_1_t2442 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16304_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16305_gshared (Collection_1_t2442 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16305(__this, ___value, method) (( bool (*) (Collection_1_t2442 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16305_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16306_gshared (Collection_1_t2442 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16306(__this, ___value, method) (( int32_t (*) (Collection_1_t2442 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16306_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16307_gshared (Collection_1_t2442 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16307(__this, ___index, ___value, method) (( void (*) (Collection_1_t2442 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16307_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16308_gshared (Collection_1_t2442 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16308(__this, ___value, method) (( void (*) (Collection_1_t2442 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16308_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16309_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16309(__this, method) (( bool (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16309_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16310_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16310(__this, method) (( Object_t * (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16310_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16311_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16311(__this, method) (( bool (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16311_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16312_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16312(__this, method) (( bool (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16312_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16313_gshared (Collection_1_t2442 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16313(__this, ___index, method) (( Object_t * (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16313_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16314_gshared (Collection_1_t2442 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16314(__this, ___index, ___value, method) (( void (*) (Collection_1_t2442 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16314_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::Add(T)
extern "C" void Collection_1_Add_m16315_gshared (Collection_1_t2442 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m16315(__this, ___item, method) (( void (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_Add_m16315_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::Clear()
extern "C" void Collection_1_Clear_m16316_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16316(__this, method) (( void (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_Clear_m16316_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::ClearItems()
extern "C" void Collection_1_ClearItems_m16317_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16317(__this, method) (( void (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_ClearItems_m16317_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::Contains(T)
extern "C" bool Collection_1_Contains_m16318_gshared (Collection_1_t2442 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m16318(__this, ___item, method) (( bool (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_Contains_m16318_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16319_gshared (Collection_1_t2442 * __this, SpaceU5BU5D_t450* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16319(__this, ___array, ___index, method) (( void (*) (Collection_1_t2442 *, SpaceU5BU5D_t450*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16319_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16320_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16320(__this, method) (( Object_t* (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_GetEnumerator_m16320_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16321_gshared (Collection_1_t2442 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16321(__this, ___item, method) (( int32_t (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m16321_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16322_gshared (Collection_1_t2442 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m16322(__this, ___index, ___item, method) (( void (*) (Collection_1_t2442 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m16322_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16323_gshared (Collection_1_t2442 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16323(__this, ___index, ___item, method) (( void (*) (Collection_1_t2442 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m16323_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::Remove(T)
extern "C" bool Collection_1_Remove_m16324_gshared (Collection_1_t2442 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m16324(__this, ___item, method) (( bool (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_Remove_m16324_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16325_gshared (Collection_1_t2442 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16325(__this, ___index, method) (( void (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16325_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16326_gshared (Collection_1_t2442 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16326(__this, ___index, method) (( void (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16326_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16327_gshared (Collection_1_t2442 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16327(__this, method) (( int32_t (*) (Collection_1_t2442 *, const MethodInfo*))Collection_1_get_Count_m16327_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m16328_gshared (Collection_1_t2442 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16328(__this, ___index, method) (( int32_t (*) (Collection_1_t2442 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16328_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16329_gshared (Collection_1_t2442 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16329(__this, ___index, ___value, method) (( void (*) (Collection_1_t2442 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m16329_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16330_gshared (Collection_1_t2442 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16330(__this, ___index, ___item, method) (( void (*) (Collection_1_t2442 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m16330_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16331_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16331(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16331_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m16332_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16332(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16332_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16333_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16333(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16333_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16334_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16334(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16334_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Space>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16335_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16335(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16335_gshared)(__this /* static, unused */, ___list, method)
