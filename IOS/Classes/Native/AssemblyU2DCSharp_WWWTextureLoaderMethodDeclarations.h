﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WWWTextureLoader
struct WWWTextureLoader_t305;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t302;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Texture2D
struct Texture2D_t9;

// System.Void WWWTextureLoader::.ctor()
extern "C" void WWWTextureLoader__ctor_m1850 (WWWTextureLoader_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::add_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern "C" void WWWTextureLoader_add_OnLoad_m1851 (WWWTextureLoader_t305 * __this, Action_1_t302 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::remove_OnLoad(System.Action`1<UnityEngine.Texture2D>)
extern "C" void WWWTextureLoader_remove_OnLoad_m1852 (WWWTextureLoader_t305 * __this, Action_1_t302 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// WWWTextureLoader WWWTextureLoader::Create()
extern "C" WWWTextureLoader_t305 * WWWTextureLoader_Create_m1853 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::LoadTexture(System.String)
extern "C" void WWWTextureLoader_LoadTexture_m1854 (WWWTextureLoader_t305 * __this, String_t* ___url, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator WWWTextureLoader::LoadCoroutin()
extern "C" Object_t * WWWTextureLoader_LoadCoroutin_m1855 (WWWTextureLoader_t305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WWWTextureLoader::<OnLoad>m__8(UnityEngine.Texture2D)
extern "C" void WWWTextureLoader_U3COnLoadU3Em__8_m1856 (Object_t * __this /* static, unused */, Texture2D_t9 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
