﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// System.Single[]
struct SingleU5BU5D_t72;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_NightVision_4
struct  CameraFilterPack_NightVision_4_t161  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_NightVision_4::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_NightVision_4::SCShader
	Shader_t1 * ___SCShader_3;
	// System.Single CameraFilterPack_NightVision_4::FadeFX
	float ___FadeFX_4;
	// System.Single CameraFilterPack_NightVision_4::TimeX
	float ___TimeX_5;
	// UnityEngine.Vector4 CameraFilterPack_NightVision_4::ScreenResolution
	Vector4_t5  ___ScreenResolution_6;
	// UnityEngine.Material CameraFilterPack_NightVision_4::SCMaterial
	Material_t2 * ___SCMaterial_7;
	// System.Single[] CameraFilterPack_NightVision_4::Matrix9
	SingleU5BU5D_t72* ___Matrix9_8;
	// System.Single CameraFilterPack_NightVision_4::Brightness
	float ___Brightness_9;
};
