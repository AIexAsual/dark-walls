﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MNP
struct MNP_t276;
// System.String
struct String_t;

// System.Void MNP::.ctor()
extern "C" void MNP__ctor_m1730 (MNP_t276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNP::ShowPreloader(System.String,System.String)
extern "C" void MNP_ShowPreloader_m1731 (Object_t * __this /* static, unused */, String_t* ___title, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MNP::HidePreloader()
extern "C" void MNP_HidePreloader_m1732 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
