﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// CoroutineScript/<testCor>c__Iterator5
struct  U3CtestCorU3Ec__Iterator5_t309  : public Object_t
{
	// System.Single CoroutineScript/<testCor>c__Iterator5::time
	float ___time_0;
	// System.Int32 CoroutineScript/<testCor>c__Iterator5::$PC
	int32_t ___U24PC_1;
	// System.Object CoroutineScript/<testCor>c__Iterator5::$current
	Object_t * ___U24current_2;
	// System.Single CoroutineScript/<testCor>c__Iterator5::<$>time
	float ___U3CU24U3Etime_3;
};
