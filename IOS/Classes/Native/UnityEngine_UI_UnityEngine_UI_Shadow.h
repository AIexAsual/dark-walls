﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.UI.Shadow
struct  Shadow_t758  : public BaseVertexEffect_t756
{
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t6  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t7  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;
};
