﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Dream
struct CameraFilterPack_Distortion_Dream_t83;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Dream::.ctor()
extern "C" void CameraFilterPack_Distortion_Dream__ctor_m517 (CameraFilterPack_Distortion_Dream_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Dream::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Dream_get_material_m518 (CameraFilterPack_Distortion_Dream_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::Start()
extern "C" void CameraFilterPack_Distortion_Dream_Start_m519 (CameraFilterPack_Distortion_Dream_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Dream_OnRenderImage_m520 (CameraFilterPack_Distortion_Dream_t83 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::OnValidate()
extern "C" void CameraFilterPack_Distortion_Dream_OnValidate_m521 (CameraFilterPack_Distortion_Dream_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::Update()
extern "C" void CameraFilterPack_Distortion_Dream_Update_m522 (CameraFilterPack_Distortion_Dream_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream::OnDisable()
extern "C" void CameraFilterPack_Distortion_Dream_OnDisable_m523 (CameraFilterPack_Distortion_Dream_t83 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
