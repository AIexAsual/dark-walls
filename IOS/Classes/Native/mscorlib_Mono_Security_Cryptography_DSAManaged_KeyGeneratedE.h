﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1223;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Cryptography.DSAManaged/KeyGeneratedEventHandler
struct  KeyGeneratedEventHandler_t1612  : public MulticastDelegate_t219
{
};
