﻿#pragma once
#include <stdint.h>
// PlayerController
struct PlayerController_t356;
// UnityEngine.TextMesh
struct TextMesh_t397;
// Loader
struct Loader_t346;
// GoTo
struct GoTo_t315;
// UnityEngine.Light
struct Light_t318;
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.Rigidbody
struct Rigidbody_t325;
// UnityEngine.Collider
struct Collider_t319;
// UnityEngine.AudioSource
struct AudioSource_t339;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t398;
// System.Collections.Hashtable
struct Hashtable_t348;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// PlayerController
struct  PlayerController_t356  : public MonoBehaviour_t4
{
	// UnityEngine.TextMesh PlayerController::ScreenText
	TextMesh_t397 * ___ScreenText_3;
	// System.Single PlayerController::angleTurn
	float ___angleTurn_4;
	// Loader PlayerController::loader
	Loader_t346 * ___loader_5;
	// GoTo PlayerController::GoToPos
	GoTo_t315 * ___GoToPos_6;
	// UnityEngine.Light PlayerController::myLight
	Light_t318 * ___myLight_7;
	// UnityEngine.GameObject PlayerController::userSpotlight
	GameObject_t256 * ___userSpotlight_8;
	// UnityEngine.GameObject PlayerController::lightings
	GameObject_t256 * ___lightings_9;
	// UnityEngine.GameObject PlayerController::_loader
	GameObject_t256 * ____loader_10;
	// UnityEngine.GameObject PlayerController::goToPos
	GameObject_t256 * ___goToPos_11;
	// UnityEngine.GameObject PlayerController::playerHead
	GameObject_t256 * ___playerHead_12;
	// UnityEngine.GameObject PlayerController::CardBoardMain
	GameObject_t256 * ___CardBoardMain_13;
	// UnityEngine.Transform PlayerController::oldPos
	Transform_t243 * ___oldPos_14;
	// UnityEngine.Rigidbody PlayerController::rb
	Rigidbody_t325 * ___rb_15;
	// UnityEngine.Collider PlayerController::col
	Collider_t319 * ___col_16;
	// UnityEngine.Vector3 PlayerController::targetPos
	Vector3_t215  ___targetPos_17;
	// UnityEngine.AudioSource PlayerController::maleVoice
	AudioSource_t339 * ___maleVoice_18;
	// System.Int32 PlayerController::changeDialog
	int32_t ___changeDialog_19;
	// System.Single PlayerController::t
	float ___t_20;
	// System.Single PlayerController::t2
	float ___t2_21;
	// System.Int32 PlayerController::i
	int32_t ___i_22;
	// System.Int32 PlayerController::tempI
	int32_t ___tempI_23;
	// System.Boolean PlayerController::isDestinationReady
	bool ___isDestinationReady_24;
	// System.Boolean PlayerController::readytoGO
	bool ___readytoGO_25;
	// System.Boolean PlayerController::FLFlicker
	bool ___FLFlicker_26;
	// System.String PlayerController::MyDestination
	String_t* ___MyDestination_27;
	// System.String PlayerController::MyCurrentPosition
	String_t* ___MyCurrentPosition_28;
	// System.String PlayerController::MyTempDestination
	String_t* ___MyTempDestination_29;
	// System.String PlayerController::LookingAt
	String_t* ___LookingAt_30;
	// System.String[] PlayerController::monologues
	StringU5BU5D_t398* ___monologues_31;
	// System.Collections.Hashtable PlayerController::moveCounts
	Hashtable_t348 * ___moveCounts_32;
	// System.Collections.Hashtable PlayerController::lookAt
	Hashtable_t348 * ___lookAt_33;
	// System.Collections.Hashtable PlayerController::nextMove
	Hashtable_t348 * ___nextMove_34;
};
struct PlayerController_t356_StaticFields{
	// PlayerController PlayerController::_instance
	PlayerController_t356 * ____instance_2;
};
