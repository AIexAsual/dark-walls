﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_NightVisionFX/preset
#include "AssemblyU2DCSharp_CameraFilterPack_NightVisionFX_preset.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_NightVisionFX
struct  CameraFilterPack_NightVisionFX_t160  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_NightVisionFX::SCShader
	Shader_t1 * ___SCShader_2;
	// CameraFilterPack_NightVisionFX/preset CameraFilterPack_NightVisionFX::Preset
	int32_t ___Preset_3;
	// CameraFilterPack_NightVisionFX/preset CameraFilterPack_NightVisionFX::PresetMemo
	int32_t ___PresetMemo_4;
	// System.Single CameraFilterPack_NightVisionFX::TimeX
	float ___TimeX_5;
	// UnityEngine.Vector4 CameraFilterPack_NightVisionFX::ScreenResolution
	Vector4_t5  ___ScreenResolution_6;
	// UnityEngine.Material CameraFilterPack_NightVisionFX::SCMaterial
	Material_t2 * ___SCMaterial_7;
	// System.Single CameraFilterPack_NightVisionFX::Greenness
	float ___Greenness_8;
	// System.Single CameraFilterPack_NightVisionFX::Vignette
	float ___Vignette_9;
	// System.Single CameraFilterPack_NightVisionFX::Vignette_Alpha
	float ___Vignette_Alpha_10;
	// System.Single CameraFilterPack_NightVisionFX::Distortion
	float ___Distortion_11;
	// System.Single CameraFilterPack_NightVisionFX::Noise
	float ___Noise_12;
	// System.Single CameraFilterPack_NightVisionFX::Intensity
	float ___Intensity_13;
	// System.Single CameraFilterPack_NightVisionFX::Light
	float ___Light_14;
	// System.Single CameraFilterPack_NightVisionFX::Light2
	float ___Light2_15;
	// System.Single CameraFilterPack_NightVisionFX::Line
	float ___Line_16;
	// System.Single CameraFilterPack_NightVisionFX::Color_R
	float ___Color_R_17;
	// System.Single CameraFilterPack_NightVisionFX::Color_G
	float ___Color_G_18;
	// System.Single CameraFilterPack_NightVisionFX::Color_B
	float ___Color_B_19;
	// System.Single CameraFilterPack_NightVisionFX::_Binocular_Size
	float ____Binocular_Size_20;
	// System.Single CameraFilterPack_NightVisionFX::_Binocular_Smooth
	float ____Binocular_Smooth_21;
	// System.Single CameraFilterPack_NightVisionFX::_Binocular_Dist
	float ____Binocular_Dist_22;
};
