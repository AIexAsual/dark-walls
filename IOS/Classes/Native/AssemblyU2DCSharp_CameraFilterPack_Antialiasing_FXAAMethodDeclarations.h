﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Antialiasing_FXAA
struct CameraFilterPack_Antialiasing_FXAA_t12;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Antialiasing_FXAA::.ctor()
extern "C" void CameraFilterPack_Antialiasing_FXAA__ctor_m30 (CameraFilterPack_Antialiasing_FXAA_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Antialiasing_FXAA::get_material()
extern "C" Material_t2 * CameraFilterPack_Antialiasing_FXAA_get_material_m31 (CameraFilterPack_Antialiasing_FXAA_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::Start()
extern "C" void CameraFilterPack_Antialiasing_FXAA_Start_m32 (CameraFilterPack_Antialiasing_FXAA_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Antialiasing_FXAA_OnRenderImage_m33 (CameraFilterPack_Antialiasing_FXAA_t12 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::Update()
extern "C" void CameraFilterPack_Antialiasing_FXAA_Update_m34 (CameraFilterPack_Antialiasing_FXAA_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Antialiasing_FXAA::OnDisable()
extern "C" void CameraFilterPack_Antialiasing_FXAA_OnDisable_m35 (CameraFilterPack_Antialiasing_FXAA_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
