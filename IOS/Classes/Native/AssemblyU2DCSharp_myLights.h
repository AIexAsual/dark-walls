﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// myLights
struct  myLights_t369  : public Object_t
{
};
struct myLights_t369_StaticFields{
	// System.String myLights::FlashLight
	String_t* ___FlashLight_0;
	// System.String myLights::ER_LIGHTS
	String_t* ___ER_LIGHTS_1;
	// System.String myLights::HW_LIGHTS1
	String_t* ___HW_LIGHTS1_2;
	// System.String myLights::HW_LIGHTS2
	String_t* ___HW_LIGHTS2_3;
	// System.String myLights::HW_LIGHTS3
	String_t* ___HW_LIGHTS3_4;
	// System.String myLights::N_LIGHTS
	String_t* ___N_LIGHTS_5;
	// System.String myLights::MORGUE_LIGHTS1
	String_t* ___MORGUE_LIGHTS1_6;
	// System.String myLights::MORGUE_LIGHTS2
	String_t* ___MORGUE_LIGHTS2_7;
	// System.String myLights::WARD_LIGHT
	String_t* ___WARD_LIGHT_8;
};
