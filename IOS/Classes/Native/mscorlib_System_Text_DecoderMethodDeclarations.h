﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.Decoder
struct Decoder_t1695;
// System.Text.DecoderFallback
struct DecoderFallback_t2029;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2030;
// System.Byte[]
struct ByteU5BU5D_t469;
// System.Char[]
struct CharU5BU5D_t530;

// System.Void System.Text.Decoder::.ctor()
extern "C" void Decoder__ctor_m12272 (Decoder_t1695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::set_Fallback(System.Text.DecoderFallback)
extern "C" void Decoder_set_Fallback_m12273 (Decoder_t1695 * __this, DecoderFallback_t2029 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.Decoder::get_FallbackBuffer()
extern "C" DecoderFallbackBuffer_t2030 * Decoder_get_FallbackBuffer_m12274 (Decoder_t1695 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
