﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Uri
struct Uri_t237;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Networking.Match.NetworkMatch
struct  NetworkMatch_t984  : public MonoBehaviour_t4
{
	// System.Uri UnityEngine.Networking.Match.NetworkMatch::m_BaseUri
	Uri_t237 * ___m_BaseUri_3;
};
