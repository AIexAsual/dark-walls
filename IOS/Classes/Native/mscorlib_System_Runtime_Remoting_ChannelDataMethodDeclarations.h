﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelData
struct ChannelData_t1912;
// System.Collections.ArrayList
struct ArrayList_t436;
// System.Collections.Hashtable
struct Hashtable_t348;

// System.Void System.Runtime.Remoting.ChannelData::.ctor()
extern "C" void ChannelData__ctor_m11604 (ChannelData_t1912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Runtime.Remoting.ChannelData::get_ServerProviders()
extern "C" ArrayList_t436 * ChannelData_get_ServerProviders_m11605 (ChannelData_t1912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ArrayList System.Runtime.Remoting.ChannelData::get_ClientProviders()
extern "C" ArrayList_t436 * ChannelData_get_ClientProviders_m11606 (ChannelData_t1912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Hashtable System.Runtime.Remoting.ChannelData::get_CustomProperties()
extern "C" Hashtable_t348 * ChannelData_get_CustomProperties_m11607 (ChannelData_t1912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ChannelData::CopyFrom(System.Runtime.Remoting.ChannelData)
extern "C" void ChannelData_CopyFrom_m11608 (ChannelData_t1912 * __this, ChannelData_t1912 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
