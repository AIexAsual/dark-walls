﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Single>
struct List_1_t576;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Single>
struct  Enumerator_t2400 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Single>::l
	List_1_t576 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Single>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Single>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Single>::current
	float ___current_3;
};
