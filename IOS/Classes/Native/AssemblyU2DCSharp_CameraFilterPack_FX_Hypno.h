﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_FX_Hypno
struct  CameraFilterPack_FX_Hypno_t134  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_Hypno::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Hypno::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_Hypno::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_Hypno::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_FX_Hypno::Speed
	float ___Speed_6;
	// System.Single CameraFilterPack_FX_Hypno::Red
	float ___Red_7;
	// System.Single CameraFilterPack_FX_Hypno::Green
	float ___Green_8;
	// System.Single CameraFilterPack_FX_Hypno::Blue
	float ___Blue_9;
};
struct CameraFilterPack_FX_Hypno_t134_StaticFields{
	// System.Single CameraFilterPack_FX_Hypno::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_FX_Hypno::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_FX_Hypno::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_FX_Hypno::ChangeValue4
	float ___ChangeValue4_13;
};
