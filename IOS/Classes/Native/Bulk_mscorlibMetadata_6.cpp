﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Security.Cryptography.RSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RSACryptoServiceProvid.h"
// Metadata Definition System.Security.Cryptography.RSACryptoServiceProvider
extern TypeInfo RSACryptoServiceProvider_t1340_il2cpp_TypeInfo;
// System.Security.Cryptography.RSACryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_RSACryptoServiceProvidMethodDeclarations.h"
static const EncodedMethodIndex RSACryptoServiceProvider_t1340_VTable[14] = 
{
	626,
	3983,
	627,
	628,
	1704,
	3984,
	1706,
	3985,
	1708,
	3982,
	3986,
	3987,
	3988,
	3989,
};
extern const Il2CppType ICspAsymmetricAlgorithm_t3493_0_0_0;
static const Il2CppType* RSACryptoServiceProvider_t1340_InterfacesTypeInfos[] = 
{
	&ICspAsymmetricAlgorithm_t3493_0_0_0,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static Il2CppInterfaceOffsetPair RSACryptoServiceProvider_t1340_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICspAsymmetricAlgorithm_t3493_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSACryptoServiceProvider_t1340_0_0_0;
extern const Il2CppType RSACryptoServiceProvider_t1340_1_0_0;
extern const Il2CppType RSA_t1226_0_0_0;
struct RSACryptoServiceProvider_t1340;
const Il2CppTypeDefinitionMetadata RSACryptoServiceProvider_t1340_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RSACryptoServiceProvider_t1340_InterfacesTypeInfos/* implementedInterfaces */
	, RSACryptoServiceProvider_t1340_InterfacesOffsets/* interfaceOffsets */
	, &RSA_t1226_0_0_0/* parent */
	, RSACryptoServiceProvider_t1340_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8687/* fieldStart */
	, 12410/* methodStart */
	, -1/* eventStart */
	, 2469/* propertyStart */

};
TypeInfo RSACryptoServiceProvider_t1340_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSACryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSACryptoServiceProvider_t1340_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3086/* custom_attributes_cache */
	, &RSACryptoServiceProvider_t1340_0_0_0/* byval_arg */
	, &RSACryptoServiceProvider_t1340_1_0_0/* this_arg */
	, &RSACryptoServiceProvider_t1340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSACryptoServiceProvider_t1340)/* instance_size */
	, sizeof (RSACryptoServiceProvider_t1340)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RSACryptoServiceProvider_t1340_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 2/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyExchangeFor.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
extern TypeInfo RSAPKCS1KeyExchangeFormatter_t1369_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1KeyExchangeFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1KeyExchangeForMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1KeyExchangeFormatter_t1369_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3990,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1KeyExchangeFormatter_t1369_0_0_0;
extern const Il2CppType RSAPKCS1KeyExchangeFormatter_t1369_1_0_0;
extern const Il2CppType AsymmetricKeyExchangeFormatter_t1964_0_0_0;
struct RSAPKCS1KeyExchangeFormatter_t1369;
const Il2CppTypeDefinitionMetadata RSAPKCS1KeyExchangeFormatter_t1369_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricKeyExchangeFormatter_t1964_0_0_0/* parent */
	, RSAPKCS1KeyExchangeFormatter_t1369_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8694/* fieldStart */
	, 12424/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1KeyExchangeFormatter_t1369_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1KeyExchangeFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1KeyExchangeFormatter_t1369_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3088/* custom_attributes_cache */
	, &RSAPKCS1KeyExchangeFormatter_t1369_0_0_0/* byval_arg */
	, &RSAPKCS1KeyExchangeFormatter_t1369_1_0_0/* this_arg */
	, &RSAPKCS1KeyExchangeFormatter_t1369_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1KeyExchangeFormatter_t1369)/* instance_size */
	, sizeof (RSAPKCS1KeyExchangeFormatter_t1369)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureDefor.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1SignatureDeformatter
extern TypeInfo RSAPKCS1SignatureDeformatter_t1348_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureDeforMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1SignatureDeformatter_t1348_VTable[7] = 
{
	626,
	601,
	627,
	628,
	3991,
	3992,
	3993,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1SignatureDeformatter_t1348_0_0_0;
extern const Il2CppType RSAPKCS1SignatureDeformatter_t1348_1_0_0;
extern const Il2CppType AsymmetricSignatureDeformatter_t1289_0_0_0;
struct RSAPKCS1SignatureDeformatter_t1348;
const Il2CppTypeDefinitionMetadata RSAPKCS1SignatureDeformatter_t1348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureDeformatter_t1289_0_0_0/* parent */
	, RSAPKCS1SignatureDeformatter_t1348_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8696/* fieldStart */
	, 12427/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1SignatureDeformatter_t1348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1SignatureDeformatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1SignatureDeformatter_t1348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3089/* custom_attributes_cache */
	, &RSAPKCS1SignatureDeformatter_t1348_0_0_0/* byval_arg */
	, &RSAPKCS1SignatureDeformatter_t1348_1_0_0/* this_arg */
	, &RSAPKCS1SignatureDeformatter_t1348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1SignatureDeformatter_t1348)/* instance_size */
	, sizeof (RSAPKCS1SignatureDeformatter_t1348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSAPKCS1SignatureFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureForma.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1SignatureFormatter
extern TypeInfo RSAPKCS1SignatureFormatter_t1983_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1SignatureFormatter
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SignatureFormaMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1SignatureFormatter_t1983_VTable[7] = 
{
	626,
	601,
	627,
	628,
	3994,
	3995,
	3996,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1SignatureFormatter_t1983_0_0_0;
extern const Il2CppType RSAPKCS1SignatureFormatter_t1983_1_0_0;
extern const Il2CppType AsymmetricSignatureFormatter_t1291_0_0_0;
struct RSAPKCS1SignatureFormatter_t1983;
const Il2CppTypeDefinitionMetadata RSAPKCS1SignatureFormatter_t1983_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureFormatter_t1291_0_0_0/* parent */
	, RSAPKCS1SignatureFormatter_t1983_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8698/* fieldStart */
	, 12432/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1SignatureFormatter_t1983_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1SignatureFormatter"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1SignatureFormatter_t1983_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3090/* custom_attributes_cache */
	, &RSAPKCS1SignatureFormatter_t1983_0_0_0/* byval_arg */
	, &RSAPKCS1SignatureFormatter_t1983_1_0_0/* this_arg */
	, &RSAPKCS1SignatureFormatter_t1983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1SignatureFormatter_t1983)/* instance_size */
	, sizeof (RSAPKCS1SignatureFormatter_t1983)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
// Metadata Definition System.Security.Cryptography.RSAParameters
extern TypeInfo RSAParameters_t1307_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAParameters
#include "mscorlib_System_Security_Cryptography_RSAParametersMethodDeclarations.h"
static const EncodedMethodIndex RSAParameters_t1307_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAParameters_t1307_0_0_0;
extern const Il2CppType RSAParameters_t1307_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata RSAParameters_t1307_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RSAParameters_t1307_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8700/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAParameters_t1307_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAParameters"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAParameters_t1307_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3091/* custom_attributes_cache */
	, &RSAParameters_t1307_0_0_0/* byval_arg */
	, &RSAParameters_t1307_1_0_0/* this_arg */
	, &RSAParameters_t1307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)RSAParameters_t1307_marshal/* marshal_to_native_func */
	, (methodPointerType)RSAParameters_t1307_marshal_back/* marshal_from_native_func */
	, (methodPointerType)RSAParameters_t1307_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (RSAParameters_t1307)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RSAParameters_t1307)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RSAParameters_t1307_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RandomNumberGenerator
#include "mscorlib_System_Security_Cryptography_RandomNumberGenerator.h"
// Metadata Definition System.Security.Cryptography.RandomNumberGenerator
extern TypeInfo RandomNumberGenerator_t1174_il2cpp_TypeInfo;
// System.Security.Cryptography.RandomNumberGenerator
#include "mscorlib_System_Security_Cryptography_RandomNumberGeneratorMethodDeclarations.h"
static const EncodedMethodIndex RandomNumberGenerator_t1174_VTable[6] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RandomNumberGenerator_t1174_0_0_0;
extern const Il2CppType RandomNumberGenerator_t1174_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct RandomNumberGenerator_t1174;
const Il2CppTypeDefinitionMetadata RandomNumberGenerator_t1174_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RandomNumberGenerator_t1174_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12436/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RandomNumberGenerator_t1174_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RandomNumberGenerator"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RandomNumberGenerator_t1174_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RandomNumberGenerator_t1174_0_0_0/* byval_arg */
	, &RandomNumberGenerator_t1174_1_0_0/* this_arg */
	, &RandomNumberGenerator_t1174_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RandomNumberGenerator_t1174)/* instance_size */
	, sizeof (RandomNumberGenerator_t1174)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_Rijndael.h"
// Metadata Definition System.Security.Cryptography.Rijndael
extern TypeInfo Rijndael_t1357_il2cpp_TypeInfo;
// System.Security.Cryptography.Rijndael
#include "mscorlib_System_Security_Cryptography_RijndaelMethodDeclarations.h"
static const EncodedMethodIndex Rijndael_t1357_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	1637,
	1638,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	0,
	1647,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair Rijndael_t1357_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Rijndael_t1357_0_0_0;
extern const Il2CppType Rijndael_t1357_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t1176_0_0_0;
struct Rijndael_t1357;
const Il2CppTypeDefinitionMetadata Rijndael_t1357_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Rijndael_t1357_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t1176_0_0_0/* parent */
	, Rijndael_t1357_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12441/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Rijndael_t1357_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Rijndael"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &Rijndael_t1357_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3092/* custom_attributes_cache */
	, &Rijndael_t1357_0_0_0/* byval_arg */
	, &Rijndael_t1357_1_0_0/* this_arg */
	, &Rijndael_t1357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Rijndael_t1357)/* instance_size */
	, sizeof (Rijndael_t1357)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RijndaelManaged
#include "mscorlib_System_Security_Cryptography_RijndaelManaged.h"
// Metadata Definition System.Security.Cryptography.RijndaelManaged
extern TypeInfo RijndaelManaged_t1984_il2cpp_TypeInfo;
// System.Security.Cryptography.RijndaelManaged
#include "mscorlib_System_Security_Cryptography_RijndaelManagedMethodDeclarations.h"
static const EncodedMethodIndex RijndaelManaged_t1984_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	1637,
	1638,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	3997,
	1647,
	3998,
	3999,
	4000,
};
static Il2CppInterfaceOffsetPair RijndaelManaged_t1984_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RijndaelManaged_t1984_0_0_0;
extern const Il2CppType RijndaelManaged_t1984_1_0_0;
struct RijndaelManaged_t1984;
const Il2CppTypeDefinitionMetadata RijndaelManaged_t1984_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RijndaelManaged_t1984_InterfacesOffsets/* interfaceOffsets */
	, &Rijndael_t1357_0_0_0/* parent */
	, RijndaelManaged_t1984_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12444/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RijndaelManaged_t1984_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RijndaelManaged"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RijndaelManaged_t1984_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3093/* custom_attributes_cache */
	, &RijndaelManaged_t1984_0_0_0/* byval_arg */
	, &RijndaelManaged_t1984_1_0_0/* this_arg */
	, &RijndaelManaged_t1984_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RijndaelManaged_t1984)/* instance_size */
	, sizeof (RijndaelManaged_t1984)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.RijndaelTransform
#include "mscorlib_System_Security_Cryptography_RijndaelTransform.h"
// Metadata Definition System.Security.Cryptography.RijndaelTransform
extern TypeInfo RijndaelTransform_t1985_il2cpp_TypeInfo;
// System.Security.Cryptography.RijndaelTransform
#include "mscorlib_System_Security_Cryptography_RijndaelTransformMethodDeclarations.h"
static const EncodedMethodIndex RijndaelTransform_t1985_VTable[18] = 
{
	626,
	2740,
	627,
	628,
	2741,
	2742,
	2743,
	2744,
	2745,
	2742,
	2746,
	4001,
	2747,
	2748,
	2749,
	2750,
	2743,
	2744,
};
extern const Il2CppType ICryptoTransform_t1188_0_0_0;
static Il2CppInterfaceOffsetPair RijndaelTransform_t1985_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RijndaelTransform_t1985_0_0_0;
extern const Il2CppType RijndaelTransform_t1985_1_0_0;
extern const Il2CppType SymmetricTransform_t1622_0_0_0;
struct RijndaelTransform_t1985;
const Il2CppTypeDefinitionMetadata RijndaelTransform_t1985_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RijndaelTransform_t1985_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t1622_0_0_0/* parent */
	, RijndaelTransform_t1985_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8708/* fieldStart */
	, 12449/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RijndaelTransform_t1985_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RijndaelTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RijndaelTransform_t1985_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RijndaelTransform_t1985_0_0_0/* byval_arg */
	, &RijndaelTransform_t1985_1_0_0/* this_arg */
	, &RijndaelTransform_t1985_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RijndaelTransform_t1985)/* instance_size */
	, sizeof (RijndaelTransform_t1985)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RijndaelTransform_t1985_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.RijndaelManagedTransform
#include "mscorlib_System_Security_Cryptography_RijndaelManagedTransfo.h"
// Metadata Definition System.Security.Cryptography.RijndaelManagedTransform
extern TypeInfo RijndaelManagedTransform_t1986_il2cpp_TypeInfo;
// System.Security.Cryptography.RijndaelManagedTransform
#include "mscorlib_System_Security_Cryptography_RijndaelManagedTransfoMethodDeclarations.h"
static const EncodedMethodIndex RijndaelManagedTransform_t1986_VTable[8] = 
{
	626,
	601,
	627,
	628,
	4002,
	4003,
	4004,
	4005,
};
static const Il2CppType* RijndaelManagedTransform_t1986_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ICryptoTransform_t1188_0_0_0,
};
static Il2CppInterfaceOffsetPair RijndaelManagedTransform_t1986_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RijndaelManagedTransform_t1986_0_0_0;
extern const Il2CppType RijndaelManagedTransform_t1986_1_0_0;
struct RijndaelManagedTransform_t1986;
const Il2CppTypeDefinitionMetadata RijndaelManagedTransform_t1986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RijndaelManagedTransform_t1986_InterfacesTypeInfos/* implementedInterfaces */
	, RijndaelManagedTransform_t1986_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RijndaelManagedTransform_t1986_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8723/* fieldStart */
	, 12460/* methodStart */
	, -1/* eventStart */
	, 2471/* propertyStart */

};
TypeInfo RijndaelManagedTransform_t1986_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RijndaelManagedTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RijndaelManagedTransform_t1986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3094/* custom_attributes_cache */
	, &RijndaelManagedTransform_t1986_0_0_0/* byval_arg */
	, &RijndaelManagedTransform_t1986_1_0_0/* this_arg */
	, &RijndaelManagedTransform_t1986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RijndaelManagedTransform_t1986)/* instance_size */
	, sizeof (RijndaelManagedTransform_t1986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1.h"
// Metadata Definition System.Security.Cryptography.SHA1
extern TypeInfo SHA1_t1345_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1MethodDeclarations.h"
static const EncodedMethodIndex SHA1_t1345_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static Il2CppInterfaceOffsetPair SHA1_t1345_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1_t1345_0_0_0;
extern const Il2CppType SHA1_t1345_1_0_0;
extern const Il2CppType HashAlgorithm_t1217_0_0_0;
struct SHA1_t1345;
const Il2CppTypeDefinitionMetadata SHA1_t1345_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA1_t1345_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, SHA1_t1345_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12465/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1_t1345_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1_t1345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3095/* custom_attributes_cache */
	, &SHA1_t1345_0_0_0/* byval_arg */
	, &SHA1_t1345_1_0_0/* this_arg */
	, &SHA1_t1345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1_t1345)/* instance_size */
	, sizeof (SHA1_t1345)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1Internal
#include "mscorlib_System_Security_Cryptography_SHA1Internal.h"
// Metadata Definition System.Security.Cryptography.SHA1Internal
extern TypeInfo SHA1Internal_t1987_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1Internal
#include "mscorlib_System_Security_Cryptography_SHA1InternalMethodDeclarations.h"
static const EncodedMethodIndex SHA1Internal_t1987_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1Internal_t1987_0_0_0;
extern const Il2CppType SHA1Internal_t1987_1_0_0;
struct SHA1Internal_t1987;
const Il2CppTypeDefinitionMetadata SHA1Internal_t1987_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SHA1Internal_t1987_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8725/* fieldStart */
	, 12468/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1Internal_t1987_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1Internal"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1Internal_t1987_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SHA1Internal_t1987_0_0_0/* byval_arg */
	, &SHA1Internal_t1987_1_0_0/* this_arg */
	, &SHA1Internal_t1987_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1Internal_t1987)/* instance_size */
	, sizeof (SHA1Internal_t1987)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_SHA1CryptoServiceProvi.h"
// Metadata Definition System.Security.Cryptography.SHA1CryptoServiceProvider
extern TypeInfo SHA1CryptoServiceProvider_t1988_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1CryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_SHA1CryptoServiceProviMethodDeclarations.h"
static const EncodedMethodIndex SHA1CryptoServiceProvider_t1988_VTable[15] = 
{
	626,
	4006,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	4007,
	4008,
	1698,
	4009,
	4010,
};
static Il2CppInterfaceOffsetPair SHA1CryptoServiceProvider_t1988_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1CryptoServiceProvider_t1988_0_0_0;
extern const Il2CppType SHA1CryptoServiceProvider_t1988_1_0_0;
struct SHA1CryptoServiceProvider_t1988;
const Il2CppTypeDefinitionMetadata SHA1CryptoServiceProvider_t1988_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA1CryptoServiceProvider_t1988_InterfacesOffsets/* interfaceOffsets */
	, &SHA1_t1345_0_0_0/* parent */
	, SHA1CryptoServiceProvider_t1988_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8730/* fieldStart */
	, 12477/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1CryptoServiceProvider_t1988_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1CryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1CryptoServiceProvider_t1988_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3096/* custom_attributes_cache */
	, &SHA1CryptoServiceProvider_t1988_0_0_0/* byval_arg */
	, &SHA1CryptoServiceProvider_t1988_1_0_0/* this_arg */
	, &SHA1CryptoServiceProvider_t1988_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1CryptoServiceProvider_t1988)/* instance_size */
	, sizeof (SHA1CryptoServiceProvider_t1988)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA1Managed
#include "mscorlib_System_Security_Cryptography_SHA1Managed.h"
// Metadata Definition System.Security.Cryptography.SHA1Managed
extern TypeInfo SHA1Managed_t1989_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA1Managed
#include "mscorlib_System_Security_Cryptography_SHA1ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA1Managed_t1989_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	4011,
	4012,
	1698,
	4013,
	1699,
};
static Il2CppInterfaceOffsetPair SHA1Managed_t1989_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA1Managed_t1989_0_0_0;
extern const Il2CppType SHA1Managed_t1989_1_0_0;
struct SHA1Managed_t1989;
const Il2CppTypeDefinitionMetadata SHA1Managed_t1989_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA1Managed_t1989_InterfacesOffsets/* interfaceOffsets */
	, &SHA1_t1345_0_0_0/* parent */
	, SHA1Managed_t1989_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8731/* fieldStart */
	, 12483/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA1Managed_t1989_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA1Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA1Managed_t1989_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3097/* custom_attributes_cache */
	, &SHA1Managed_t1989_0_0_0/* byval_arg */
	, &SHA1Managed_t1989_1_0_0/* this_arg */
	, &SHA1Managed_t1989_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA1Managed_t1989)/* instance_size */
	, sizeof (SHA1Managed_t1989)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA256
#include "mscorlib_System_Security_Cryptography_SHA256.h"
// Metadata Definition System.Security.Cryptography.SHA256
extern TypeInfo SHA256_t1346_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA256
#include "mscorlib_System_Security_Cryptography_SHA256MethodDeclarations.h"
static const EncodedMethodIndex SHA256_t1346_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static Il2CppInterfaceOffsetPair SHA256_t1346_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA256_t1346_0_0_0;
extern const Il2CppType SHA256_t1346_1_0_0;
struct SHA256_t1346;
const Il2CppTypeDefinitionMetadata SHA256_t1346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA256_t1346_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, SHA256_t1346_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12487/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA256_t1346_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA256"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA256_t1346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3098/* custom_attributes_cache */
	, &SHA256_t1346_0_0_0/* byval_arg */
	, &SHA256_t1346_1_0_0/* this_arg */
	, &SHA256_t1346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA256_t1346)/* instance_size */
	, sizeof (SHA256_t1346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA256Managed
#include "mscorlib_System_Security_Cryptography_SHA256Managed.h"
// Metadata Definition System.Security.Cryptography.SHA256Managed
extern TypeInfo SHA256Managed_t1990_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA256Managed
#include "mscorlib_System_Security_Cryptography_SHA256ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA256Managed_t1990_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	4014,
	4015,
	1698,
	4016,
	1699,
};
static Il2CppInterfaceOffsetPair SHA256Managed_t1990_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA256Managed_t1990_0_0_0;
extern const Il2CppType SHA256Managed_t1990_1_0_0;
struct SHA256Managed_t1990;
const Il2CppTypeDefinitionMetadata SHA256Managed_t1990_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA256Managed_t1990_InterfacesOffsets/* interfaceOffsets */
	, &SHA256_t1346_0_0_0/* parent */
	, SHA256Managed_t1990_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8732/* fieldStart */
	, 12490/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA256Managed_t1990_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA256Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA256Managed_t1990_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3099/* custom_attributes_cache */
	, &SHA256Managed_t1990_0_0_0/* byval_arg */
	, &SHA256Managed_t1990_1_0_0/* this_arg */
	, &SHA256Managed_t1990_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA256Managed_t1990)/* instance_size */
	, sizeof (SHA256Managed_t1990)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA384
#include "mscorlib_System_Security_Cryptography_SHA384.h"
// Metadata Definition System.Security.Cryptography.SHA384
extern TypeInfo SHA384_t1991_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA384
#include "mscorlib_System_Security_Cryptography_SHA384MethodDeclarations.h"
static const EncodedMethodIndex SHA384_t1991_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static Il2CppInterfaceOffsetPair SHA384_t1991_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA384_t1991_0_0_0;
extern const Il2CppType SHA384_t1991_1_0_0;
struct SHA384_t1991;
const Il2CppTypeDefinitionMetadata SHA384_t1991_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA384_t1991_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, SHA384_t1991_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12497/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA384_t1991_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA384"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA384_t1991_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3100/* custom_attributes_cache */
	, &SHA384_t1991_0_0_0/* byval_arg */
	, &SHA384_t1991_1_0_0/* this_arg */
	, &SHA384_t1991_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA384_t1991)/* instance_size */
	, sizeof (SHA384_t1991)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA384Managed
#include "mscorlib_System_Security_Cryptography_SHA384Managed.h"
// Metadata Definition System.Security.Cryptography.SHA384Managed
extern TypeInfo SHA384Managed_t1993_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA384Managed
#include "mscorlib_System_Security_Cryptography_SHA384ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA384Managed_t1993_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	4017,
	4018,
	1698,
	4019,
	1699,
};
static Il2CppInterfaceOffsetPair SHA384Managed_t1993_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA384Managed_t1993_0_0_0;
extern const Il2CppType SHA384Managed_t1993_1_0_0;
struct SHA384Managed_t1993;
const Il2CppTypeDefinitionMetadata SHA384Managed_t1993_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA384Managed_t1993_InterfacesOffsets/* interfaceOffsets */
	, &SHA384_t1991_0_0_0/* parent */
	, SHA384Managed_t1993_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8737/* fieldStart */
	, 12498/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA384Managed_t1993_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA384Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA384Managed_t1993_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3101/* custom_attributes_cache */
	, &SHA384Managed_t1993_0_0_0/* byval_arg */
	, &SHA384Managed_t1993_1_0_0/* this_arg */
	, &SHA384Managed_t1993_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA384Managed_t1993)/* instance_size */
	, sizeof (SHA384Managed_t1993)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA512
#include "mscorlib_System_Security_Cryptography_SHA512.h"
// Metadata Definition System.Security.Cryptography.SHA512
extern TypeInfo SHA512_t1994_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA512
#include "mscorlib_System_Security_Cryptography_SHA512MethodDeclarations.h"
static const EncodedMethodIndex SHA512_t1994_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static Il2CppInterfaceOffsetPair SHA512_t1994_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA512_t1994_0_0_0;
extern const Il2CppType SHA512_t1994_1_0_0;
struct SHA512_t1994;
const Il2CppTypeDefinitionMetadata SHA512_t1994_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA512_t1994_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, SHA512_t1994_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12509/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA512_t1994_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA512"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA512_t1994_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3102/* custom_attributes_cache */
	, &SHA512_t1994_0_0_0/* byval_arg */
	, &SHA512_t1994_1_0_0/* this_arg */
	, &SHA512_t1994_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA512_t1994)/* instance_size */
	, sizeof (SHA512_t1994)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHA512Managed
#include "mscorlib_System_Security_Cryptography_SHA512Managed.h"
// Metadata Definition System.Security.Cryptography.SHA512Managed
extern TypeInfo SHA512Managed_t1995_il2cpp_TypeInfo;
// System.Security.Cryptography.SHA512Managed
#include "mscorlib_System_Security_Cryptography_SHA512ManagedMethodDeclarations.h"
static const EncodedMethodIndex SHA512Managed_t1995_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	4020,
	4021,
	1698,
	4022,
	1699,
};
static Il2CppInterfaceOffsetPair SHA512Managed_t1995_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHA512Managed_t1995_0_0_0;
extern const Il2CppType SHA512Managed_t1995_1_0_0;
struct SHA512Managed_t1995;
const Il2CppTypeDefinitionMetadata SHA512Managed_t1995_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SHA512Managed_t1995_InterfacesOffsets/* interfaceOffsets */
	, &SHA512_t1994_0_0_0/* parent */
	, SHA512Managed_t1995_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8751/* fieldStart */
	, 12510/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHA512Managed_t1995_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHA512Managed"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHA512Managed_t1995_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3103/* custom_attributes_cache */
	, &SHA512Managed_t1995_0_0_0/* byval_arg */
	, &SHA512Managed_t1995_1_0_0/* this_arg */
	, &SHA512Managed_t1995_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHA512Managed_t1995)/* instance_size */
	, sizeof (SHA512Managed_t1995)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.SHAConstants
#include "mscorlib_System_Security_Cryptography_SHAConstants.h"
// Metadata Definition System.Security.Cryptography.SHAConstants
extern TypeInfo SHAConstants_t1996_il2cpp_TypeInfo;
// System.Security.Cryptography.SHAConstants
#include "mscorlib_System_Security_Cryptography_SHAConstantsMethodDeclarations.h"
static const EncodedMethodIndex SHAConstants_t1996_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SHAConstants_t1996_0_0_0;
extern const Il2CppType SHAConstants_t1996_1_0_0;
struct SHAConstants_t1996;
const Il2CppTypeDefinitionMetadata SHAConstants_t1996_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SHAConstants_t1996_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8765/* fieldStart */
	, 12528/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SHAConstants_t1996_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SHAConstants"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SHAConstants_t1996_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SHAConstants_t1996_0_0_0/* byval_arg */
	, &SHAConstants_t1996_1_0_0/* this_arg */
	, &SHAConstants_t1996_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SHAConstants_t1996)/* instance_size */
	, sizeof (SHAConstants_t1996)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SHAConstants_t1996_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.SignatureDescription
#include "mscorlib_System_Security_Cryptography_SignatureDescription.h"
// Metadata Definition System.Security.Cryptography.SignatureDescription
extern TypeInfo SignatureDescription_t1997_il2cpp_TypeInfo;
// System.Security.Cryptography.SignatureDescription
#include "mscorlib_System_Security_Cryptography_SignatureDescriptionMethodDeclarations.h"
static const EncodedMethodIndex SignatureDescription_t1997_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SignatureDescription_t1997_0_0_0;
extern const Il2CppType SignatureDescription_t1997_1_0_0;
struct SignatureDescription_t1997;
const Il2CppTypeDefinitionMetadata SignatureDescription_t1997_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SignatureDescription_t1997_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8767/* fieldStart */
	, 12529/* methodStart */
	, -1/* eventStart */
	, 2472/* propertyStart */

};
TypeInfo SignatureDescription_t1997_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SignatureDescription"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SignatureDescription_t1997_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3104/* custom_attributes_cache */
	, &SignatureDescription_t1997_0_0_0/* byval_arg */
	, &SignatureDescription_t1997_1_0_0/* this_arg */
	, &SignatureDescription_t1997_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SignatureDescription_t1997)/* instance_size */
	, sizeof (SignatureDescription_t1997)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.DSASignatureDescription
#include "mscorlib_System_Security_Cryptography_DSASignatureDescriptio.h"
// Metadata Definition System.Security.Cryptography.DSASignatureDescription
extern TypeInfo DSASignatureDescription_t1998_il2cpp_TypeInfo;
// System.Security.Cryptography.DSASignatureDescription
#include "mscorlib_System_Security_Cryptography_DSASignatureDescriptioMethodDeclarations.h"
static const EncodedMethodIndex DSASignatureDescription_t1998_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DSASignatureDescription_t1998_0_0_0;
extern const Il2CppType DSASignatureDescription_t1998_1_0_0;
struct DSASignatureDescription_t1998;
const Il2CppTypeDefinitionMetadata DSASignatureDescription_t1998_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SignatureDescription_t1997_0_0_0/* parent */
	, DSASignatureDescription_t1998_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12534/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DSASignatureDescription_t1998_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DSASignatureDescription"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &DSASignatureDescription_t1998_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DSASignatureDescription_t1998_0_0_0/* byval_arg */
	, &DSASignatureDescription_t1998_1_0_0/* this_arg */
	, &DSASignatureDescription_t1998_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DSASignatureDescription_t1998)/* instance_size */
	, sizeof (DSASignatureDescription_t1998)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA1SignatureD.h"
// Metadata Definition System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
extern TypeInfo RSAPKCS1SHA1SignatureDescription_t1999_il2cpp_TypeInfo;
// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
#include "mscorlib_System_Security_Cryptography_RSAPKCS1SHA1SignatureDMethodDeclarations.h"
static const EncodedMethodIndex RSAPKCS1SHA1SignatureDescription_t1999_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RSAPKCS1SHA1SignatureDescription_t1999_0_0_0;
extern const Il2CppType RSAPKCS1SHA1SignatureDescription_t1999_1_0_0;
struct RSAPKCS1SHA1SignatureDescription_t1999;
const Il2CppTypeDefinitionMetadata RSAPKCS1SHA1SignatureDescription_t1999_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &SignatureDescription_t1997_0_0_0/* parent */
	, RSAPKCS1SHA1SignatureDescription_t1999_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12535/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSAPKCS1SHA1SignatureDescription_t1999_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAPKCS1SHA1SignatureDescription"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAPKCS1SHA1SignatureDescription_t1999_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RSAPKCS1SHA1SignatureDescription_t1999_0_0_0/* byval_arg */
	, &RSAPKCS1SHA1SignatureDescription_t1999_1_0_0/* this_arg */
	, &RSAPKCS1SHA1SignatureDescription_t1999_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAPKCS1SHA1SignatureDescription_t1999)/* instance_size */
	, sizeof (RSAPKCS1SHA1SignatureDescription_t1999)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
// Metadata Definition System.Security.Cryptography.SymmetricAlgorithm
extern TypeInfo SymmetricAlgorithm_t1176_il2cpp_TypeInfo;
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithmMethodDeclarations.h"
static const EncodedMethodIndex SymmetricAlgorithm_t1176_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	1637,
	1638,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	0,
	1647,
	0,
	0,
	0,
};
static const Il2CppType* SymmetricAlgorithm_t1176_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair SymmetricAlgorithm_t1176_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SymmetricAlgorithm_t1176_1_0_0;
struct SymmetricAlgorithm_t1176;
const Il2CppTypeDefinitionMetadata SymmetricAlgorithm_t1176_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SymmetricAlgorithm_t1176_InterfacesTypeInfos/* implementedInterfaces */
	, SymmetricAlgorithm_t1176_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SymmetricAlgorithm_t1176_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8771/* fieldStart */
	, 12536/* methodStart */
	, -1/* eventStart */
	, 2476/* propertyStart */

};
TypeInfo SymmetricAlgorithm_t1176_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SymmetricAlgorithm"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &SymmetricAlgorithm_t1176_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3105/* custom_attributes_cache */
	, &SymmetricAlgorithm_t1176_0_0_0/* byval_arg */
	, &SymmetricAlgorithm_t1176_1_0_0/* this_arg */
	, &SymmetricAlgorithm_t1176_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SymmetricAlgorithm_t1176)/* instance_size */
	, sizeof (SymmetricAlgorithm_t1176)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 8/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.ToBase64Transform
#include "mscorlib_System_Security_Cryptography_ToBase64Transform.h"
// Metadata Definition System.Security.Cryptography.ToBase64Transform
extern TypeInfo ToBase64Transform_t2000_il2cpp_TypeInfo;
// System.Security.Cryptography.ToBase64Transform
#include "mscorlib_System_Security_Cryptography_ToBase64TransformMethodDeclarations.h"
static const EncodedMethodIndex ToBase64Transform_t2000_VTable[12] = 
{
	626,
	4023,
	627,
	628,
	4024,
	4025,
	4026,
	4027,
	4025,
	4028,
	4029,
	4030,
};
static const Il2CppType* ToBase64Transform_t2000_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ICryptoTransform_t1188_0_0_0,
};
static Il2CppInterfaceOffsetPair ToBase64Transform_t2000_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ToBase64Transform_t2000_0_0_0;
extern const Il2CppType ToBase64Transform_t2000_1_0_0;
struct ToBase64Transform_t2000;
const Il2CppTypeDefinitionMetadata ToBase64Transform_t2000_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ToBase64Transform_t2000_InterfacesTypeInfos/* implementedInterfaces */
	, ToBase64Transform_t2000_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ToBase64Transform_t2000_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8781/* fieldStart */
	, 12562/* methodStart */
	, -1/* eventStart */
	, 2484/* propertyStart */

};
TypeInfo ToBase64Transform_t2000_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToBase64Transform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &ToBase64Transform_t2000_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3106/* custom_attributes_cache */
	, &ToBase64Transform_t2000_0_0_0/* byval_arg */
	, &ToBase64Transform_t2000_1_0_0/* this_arg */
	, &ToBase64Transform_t2000_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToBase64Transform_t2000)/* instance_size */
	, sizeof (ToBase64Transform_t2000)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Cryptography.TripleDES
#include "mscorlib_System_Security_Cryptography_TripleDES.h"
// Metadata Definition System.Security.Cryptography.TripleDES
extern TypeInfo TripleDES_t1356_il2cpp_TypeInfo;
// System.Security.Cryptography.TripleDES
#include "mscorlib_System_Security_Cryptography_TripleDESMethodDeclarations.h"
static const EncodedMethodIndex TripleDES_t1356_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	4031,
	4032,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	0,
	1647,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair TripleDES_t1356_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TripleDES_t1356_0_0_0;
extern const Il2CppType TripleDES_t1356_1_0_0;
struct TripleDES_t1356;
const Il2CppTypeDefinitionMetadata TripleDES_t1356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TripleDES_t1356_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t1176_0_0_0/* parent */
	, TripleDES_t1356_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12572/* methodStart */
	, -1/* eventStart */
	, 2487/* propertyStart */

};
TypeInfo TripleDES_t1356_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TripleDES"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &TripleDES_t1356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3107/* custom_attributes_cache */
	, &TripleDES_t1356_0_0_0/* byval_arg */
	, &TripleDES_t1356_1_0_0/* this_arg */
	, &TripleDES_t1356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TripleDES_t1356)/* instance_size */
	, sizeof (TripleDES_t1356)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.TripleDESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_TripleDESCryptoService.h"
// Metadata Definition System.Security.Cryptography.TripleDESCryptoServiceProvider
extern TypeInfo TripleDESCryptoServiceProvider_t2001_il2cpp_TypeInfo;
// System.Security.Cryptography.TripleDESCryptoServiceProvider
#include "mscorlib_System_Security_Cryptography_TripleDESCryptoServiceMethodDeclarations.h"
static const EncodedMethodIndex TripleDESCryptoServiceProvider_t2001_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1635,
	1636,
	4031,
	4032,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	4033,
	1647,
	4034,
	4035,
	4036,
};
static Il2CppInterfaceOffsetPair TripleDESCryptoServiceProvider_t2001_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TripleDESCryptoServiceProvider_t2001_0_0_0;
extern const Il2CppType TripleDESCryptoServiceProvider_t2001_1_0_0;
struct TripleDESCryptoServiceProvider_t2001;
const Il2CppTypeDefinitionMetadata TripleDESCryptoServiceProvider_t2001_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TripleDESCryptoServiceProvider_t2001_InterfacesOffsets/* interfaceOffsets */
	, &TripleDES_t1356_0_0_0/* parent */
	, TripleDESCryptoServiceProvider_t2001_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12578/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TripleDESCryptoServiceProvider_t2001_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TripleDESCryptoServiceProvider"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &TripleDESCryptoServiceProvider_t2001_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3108/* custom_attributes_cache */
	, &TripleDESCryptoServiceProvider_t2001_0_0_0/* byval_arg */
	, &TripleDESCryptoServiceProvider_t2001_1_0_0/* this_arg */
	, &TripleDESCryptoServiceProvider_t2001_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TripleDESCryptoServiceProvider_t2001)/* instance_size */
	, sizeof (TripleDESCryptoServiceProvider_t2001)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Cryptography.TripleDESTransform
#include "mscorlib_System_Security_Cryptography_TripleDESTransform.h"
// Metadata Definition System.Security.Cryptography.TripleDESTransform
extern TypeInfo TripleDESTransform_t2002_il2cpp_TypeInfo;
// System.Security.Cryptography.TripleDESTransform
#include "mscorlib_System_Security_Cryptography_TripleDESTransformMethodDeclarations.h"
static const EncodedMethodIndex TripleDESTransform_t2002_VTable[18] = 
{
	626,
	2740,
	627,
	628,
	2741,
	2742,
	2743,
	2744,
	2745,
	2742,
	2746,
	4037,
	2747,
	2748,
	2749,
	2750,
	2743,
	2744,
};
static Il2CppInterfaceOffsetPair TripleDESTransform_t2002_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TripleDESTransform_t2002_0_0_0;
extern const Il2CppType TripleDESTransform_t2002_1_0_0;
struct TripleDESTransform_t2002;
const Il2CppTypeDefinitionMetadata TripleDESTransform_t2002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TripleDESTransform_t2002_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricTransform_t1622_0_0_0/* parent */
	, TripleDESTransform_t2002_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8782/* fieldStart */
	, 12583/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TripleDESTransform_t2002_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TripleDESTransform"/* name */
	, "System.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &TripleDESTransform_t2002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TripleDESTransform_t2002_0_0_0/* byval_arg */
	, &TripleDESTransform_t2002_1_0_0/* this_arg */
	, &TripleDESTransform_t2002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TripleDESTransform_t2002)/* instance_size */
	, sizeof (TripleDESTransform_t2002)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Permissions.IBuiltInPermission
extern TypeInfo IBuiltInPermission_t3494_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IBuiltInPermission_t3494_0_0_0;
extern const Il2CppType IBuiltInPermission_t3494_1_0_0;
struct IBuiltInPermission_t3494;
const Il2CppTypeDefinitionMetadata IBuiltInPermission_t3494_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IBuiltInPermission_t3494_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IBuiltInPermission"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &IBuiltInPermission_t3494_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IBuiltInPermission_t3494_0_0_0/* byval_arg */
	, &IBuiltInPermission_t3494_1_0_0/* this_arg */
	, &IBuiltInPermission_t3494_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Permissions.IUnrestrictedPermission
extern TypeInfo IUnrestrictedPermission_t3495_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IUnrestrictedPermission_t3495_0_0_0;
extern const Il2CppType IUnrestrictedPermission_t3495_1_0_0;
struct IUnrestrictedPermission_t3495;
const Il2CppTypeDefinitionMetadata IUnrestrictedPermission_t3495_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IUnrestrictedPermission_t3495_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUnrestrictedPermission"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &IUnrestrictedPermission_t3495_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3109/* custom_attributes_cache */
	, &IUnrestrictedPermission_t3495_0_0_0/* byval_arg */
	, &IUnrestrictedPermission_t3495_1_0_0/* this_arg */
	, &IUnrestrictedPermission_t3495_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Permissions.SecurityPermission
#include "mscorlib_System_Security_Permissions_SecurityPermission.h"
// Metadata Definition System.Security.Permissions.SecurityPermission
extern TypeInfo SecurityPermission_t2003_il2cpp_TypeInfo;
// System.Security.Permissions.SecurityPermission
#include "mscorlib_System_Security_Permissions_SecurityPermissionMethodDeclarations.h"
static const EncodedMethodIndex SecurityPermission_t2003_VTable[7] = 
{
	4038,
	601,
	4039,
	4040,
	4041,
	4042,
	4043,
};
static const Il2CppType* SecurityPermission_t2003_InterfacesTypeInfos[] = 
{
	&IBuiltInPermission_t3494_0_0_0,
	&IUnrestrictedPermission_t3495_0_0_0,
};
extern const Il2CppType IPermission_t2018_0_0_0;
extern const Il2CppType ISecurityEncodable_t3499_0_0_0;
extern const Il2CppType IStackWalk_t3500_0_0_0;
static Il2CppInterfaceOffsetPair SecurityPermission_t2003_InterfacesOffsets[] = 
{
	{ &IPermission_t2018_0_0_0, 4},
	{ &ISecurityEncodable_t3499_0_0_0, 4},
	{ &IStackWalk_t3500_0_0_0, 4},
	{ &IBuiltInPermission_t3494_0_0_0, 6},
	{ &IUnrestrictedPermission_t3495_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityPermission_t2003_0_0_0;
extern const Il2CppType SecurityPermission_t2003_1_0_0;
extern const Il2CppType CodeAccessPermission_t2004_0_0_0;
struct SecurityPermission_t2003;
const Il2CppTypeDefinitionMetadata SecurityPermission_t2003_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SecurityPermission_t2003_InterfacesTypeInfos/* implementedInterfaces */
	, SecurityPermission_t2003_InterfacesOffsets/* interfaceOffsets */
	, &CodeAccessPermission_t2004_0_0_0/* parent */
	, SecurityPermission_t2003_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8788/* fieldStart */
	, 12586/* methodStart */
	, -1/* eventStart */
	, 2488/* propertyStart */

};
TypeInfo SecurityPermission_t2003_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityPermission"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &SecurityPermission_t2003_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3110/* custom_attributes_cache */
	, &SecurityPermission_t2003_0_0_0/* byval_arg */
	, &SecurityPermission_t2003_1_0_0/* this_arg */
	, &SecurityPermission_t2003_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityPermission_t2003)/* instance_size */
	, sizeof (SecurityPermission_t2003)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 2/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Security.Permissions.SecurityPermissionFlag
#include "mscorlib_System_Security_Permissions_SecurityPermissionFlag.h"
// Metadata Definition System.Security.Permissions.SecurityPermissionFlag
extern TypeInfo SecurityPermissionFlag_t2005_il2cpp_TypeInfo;
// System.Security.Permissions.SecurityPermissionFlag
#include "mscorlib_System_Security_Permissions_SecurityPermissionFlagMethodDeclarations.h"
static const EncodedMethodIndex SecurityPermissionFlag_t2005_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair SecurityPermissionFlag_t2005_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityPermissionFlag_t2005_0_0_0;
extern const Il2CppType SecurityPermissionFlag_t2005_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata SecurityPermissionFlag_t2005_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityPermissionFlag_t2005_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SecurityPermissionFlag_t2005_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8789/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityPermissionFlag_t2005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityPermissionFlag"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3111/* custom_attributes_cache */
	, &SecurityPermissionFlag_t2005_0_0_0/* byval_arg */
	, &SecurityPermissionFlag_t2005_1_0_0/* this_arg */
	, &SecurityPermissionFlag_t2005_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityPermissionFlag_t2005)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityPermissionFlag_t2005)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Permissions.StrongNamePublicKeyBlob
#include "mscorlib_System_Security_Permissions_StrongNamePublicKeyBlob.h"
// Metadata Definition System.Security.Permissions.StrongNamePublicKeyBlob
extern TypeInfo StrongNamePublicKeyBlob_t2006_il2cpp_TypeInfo;
// System.Security.Permissions.StrongNamePublicKeyBlob
#include "mscorlib_System_Security_Permissions_StrongNamePublicKeyBlobMethodDeclarations.h"
static const EncodedMethodIndex StrongNamePublicKeyBlob_t2006_VTable[4] = 
{
	4044,
	601,
	4045,
	4046,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongNamePublicKeyBlob_t2006_0_0_0;
extern const Il2CppType StrongNamePublicKeyBlob_t2006_1_0_0;
struct StrongNamePublicKeyBlob_t2006;
const Il2CppTypeDefinitionMetadata StrongNamePublicKeyBlob_t2006_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongNamePublicKeyBlob_t2006_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8806/* fieldStart */
	, 12593/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StrongNamePublicKeyBlob_t2006_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongNamePublicKeyBlob"/* name */
	, "System.Security.Permissions"/* namespaze */
	, NULL/* methods */
	, &StrongNamePublicKeyBlob_t2006_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3112/* custom_attributes_cache */
	, &StrongNamePublicKeyBlob_t2006_0_0_0/* byval_arg */
	, &StrongNamePublicKeyBlob_t2006_1_0_0/* this_arg */
	, &StrongNamePublicKeyBlob_t2006_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongNamePublicKeyBlob_t2006)/* instance_size */
	, sizeof (StrongNamePublicKeyBlob_t2006)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Policy.ApplicationTrust
#include "mscorlib_System_Security_Policy_ApplicationTrust.h"
// Metadata Definition System.Security.Policy.ApplicationTrust
extern TypeInfo ApplicationTrust_t2008_il2cpp_TypeInfo;
// System.Security.Policy.ApplicationTrust
#include "mscorlib_System_Security_Policy_ApplicationTrustMethodDeclarations.h"
static const EncodedMethodIndex ApplicationTrust_t2008_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
static const Il2CppType* ApplicationTrust_t2008_InterfacesTypeInfos[] = 
{
	&ISecurityEncodable_t3499_0_0_0,
};
static Il2CppInterfaceOffsetPair ApplicationTrust_t2008_InterfacesOffsets[] = 
{
	{ &ISecurityEncodable_t3499_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ApplicationTrust_t2008_0_0_0;
extern const Il2CppType ApplicationTrust_t2008_1_0_0;
struct ApplicationTrust_t2008;
const Il2CppTypeDefinitionMetadata ApplicationTrust_t2008_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ApplicationTrust_t2008_InterfacesTypeInfos/* implementedInterfaces */
	, ApplicationTrust_t2008_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ApplicationTrust_t2008_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8807/* fieldStart */
	, 12596/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ApplicationTrust_t2008_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ApplicationTrust"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &ApplicationTrust_t2008_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3113/* custom_attributes_cache */
	, &ApplicationTrust_t2008_0_0_0/* byval_arg */
	, &ApplicationTrust_t2008_1_0_0/* this_arg */
	, &ApplicationTrust_t2008_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ApplicationTrust_t2008)/* instance_size */
	, sizeof (ApplicationTrust_t2008)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Policy.Evidence
#include "mscorlib_System_Security_Policy_Evidence.h"
// Metadata Definition System.Security.Policy.Evidence
extern TypeInfo Evidence_t1767_il2cpp_TypeInfo;
// System.Security.Policy.Evidence
#include "mscorlib_System_Security_Policy_EvidenceMethodDeclarations.h"
extern const Il2CppType EvidenceEnumerator_t2009_0_0_0;
static const Il2CppType* Evidence_t1767_il2cpp_TypeInfo__nestedTypes[1] =
{
	&EvidenceEnumerator_t2009_0_0_0,
};
static const EncodedMethodIndex Evidence_t1767_VTable[9] = 
{
	4047,
	601,
	4048,
	628,
	4049,
	4050,
	4051,
	4052,
	4053,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
extern const Il2CppType ICollection_t1528_0_0_0;
static const Il2CppType* Evidence_t1767_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
};
static Il2CppInterfaceOffsetPair Evidence_t1767_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Evidence_t1767_0_0_0;
extern const Il2CppType Evidence_t1767_1_0_0;
struct Evidence_t1767;
const Il2CppTypeDefinitionMetadata Evidence_t1767_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Evidence_t1767_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Evidence_t1767_InterfacesTypeInfos/* implementedInterfaces */
	, Evidence_t1767_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Evidence_t1767_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8808/* fieldStart */
	, 12597/* methodStart */
	, -1/* eventStart */
	, 2489/* propertyStart */

};
TypeInfo Evidence_t1767_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Evidence"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &Evidence_t1767_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3114/* custom_attributes_cache */
	, &Evidence_t1767_0_0_0/* byval_arg */
	, &Evidence_t1767_1_0_0/* this_arg */
	, &Evidence_t1767_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Evidence_t1767)/* instance_size */
	, sizeof (Evidence_t1767)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.Policy.Evidence/EvidenceEnumerator
#include "mscorlib_System_Security_Policy_Evidence_EvidenceEnumerator.h"
// Metadata Definition System.Security.Policy.Evidence/EvidenceEnumerator
extern TypeInfo EvidenceEnumerator_t2009_il2cpp_TypeInfo;
// System.Security.Policy.Evidence/EvidenceEnumerator
#include "mscorlib_System_Security_Policy_Evidence_EvidenceEnumeratorMethodDeclarations.h"
static const EncodedMethodIndex EvidenceEnumerator_t2009_VTable[6] = 
{
	626,
	601,
	627,
	628,
	4054,
	4055,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
static const Il2CppType* EvidenceEnumerator_t2009_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair EvidenceEnumerator_t2009_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EvidenceEnumerator_t2009_1_0_0;
struct EvidenceEnumerator_t2009;
const Il2CppTypeDefinitionMetadata EvidenceEnumerator_t2009_DefinitionMetadata = 
{
	&Evidence_t1767_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, EvidenceEnumerator_t2009_InterfacesTypeInfos/* implementedInterfaces */
	, EvidenceEnumerator_t2009_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EvidenceEnumerator_t2009_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8811/* fieldStart */
	, 12607/* methodStart */
	, -1/* eventStart */
	, 2494/* propertyStart */

};
TypeInfo EvidenceEnumerator_t2009_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EvidenceEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EvidenceEnumerator_t2009_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EvidenceEnumerator_t2009_0_0_0/* byval_arg */
	, &EvidenceEnumerator_t2009_1_0_0/* this_arg */
	, &EvidenceEnumerator_t2009_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EvidenceEnumerator_t2009)/* instance_size */
	, sizeof (EvidenceEnumerator_t2009)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.Policy.Hash
#include "mscorlib_System_Security_Policy_Hash.h"
// Metadata Definition System.Security.Policy.Hash
extern TypeInfo Hash_t2010_il2cpp_TypeInfo;
// System.Security.Policy.Hash
#include "mscorlib_System_Security_Policy_HashMethodDeclarations.h"
static const EncodedMethodIndex Hash_t2010_VTable[5] = 
{
	626,
	601,
	627,
	4056,
	4057,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
extern const Il2CppType IBuiltInEvidence_t3496_0_0_0;
static const Il2CppType* Hash_t2010_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IBuiltInEvidence_t3496_0_0_0,
};
static Il2CppInterfaceOffsetPair Hash_t2010_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IBuiltInEvidence_t3496_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Hash_t2010_0_0_0;
extern const Il2CppType Hash_t2010_1_0_0;
struct Hash_t2010;
const Il2CppTypeDefinitionMetadata Hash_t2010_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Hash_t2010_InterfacesTypeInfos/* implementedInterfaces */
	, Hash_t2010_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Hash_t2010_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8814/* fieldStart */
	, 12610/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Hash_t2010_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Hash"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &Hash_t2010_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3117/* custom_attributes_cache */
	, &Hash_t2010_0_0_0/* byval_arg */
	, &Hash_t2010_1_0_0/* this_arg */
	, &Hash_t2010_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Hash_t2010)/* instance_size */
	, sizeof (Hash_t2010)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Policy.IBuiltInEvidence
extern TypeInfo IBuiltInEvidence_t3496_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IBuiltInEvidence_t3496_1_0_0;
struct IBuiltInEvidence_t3496;
const Il2CppTypeDefinitionMetadata IBuiltInEvidence_t3496_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IBuiltInEvidence_t3496_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IBuiltInEvidence"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &IBuiltInEvidence_t3496_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IBuiltInEvidence_t3496_0_0_0/* byval_arg */
	, &IBuiltInEvidence_t3496_1_0_0/* this_arg */
	, &IBuiltInEvidence_t3496_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 160/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Policy.IIdentityPermissionFactory
extern TypeInfo IIdentityPermissionFactory_t3497_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IIdentityPermissionFactory_t3497_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t3497_1_0_0;
struct IIdentityPermissionFactory_t3497;
const Il2CppTypeDefinitionMetadata IIdentityPermissionFactory_t3497_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IIdentityPermissionFactory_t3497_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IIdentityPermissionFactory"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &IIdentityPermissionFactory_t3497_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3118/* custom_attributes_cache */
	, &IIdentityPermissionFactory_t3497_0_0_0/* byval_arg */
	, &IIdentityPermissionFactory_t3497_1_0_0/* this_arg */
	, &IIdentityPermissionFactory_t3497_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Policy.StrongName
#include "mscorlib_System_Security_Policy_StrongName.h"
// Metadata Definition System.Security.Policy.StrongName
extern TypeInfo StrongName_t2011_il2cpp_TypeInfo;
// System.Security.Policy.StrongName
#include "mscorlib_System_Security_Policy_StrongNameMethodDeclarations.h"
static const EncodedMethodIndex StrongName_t2011_VTable[4] = 
{
	4058,
	601,
	4059,
	4060,
};
static const Il2CppType* StrongName_t2011_InterfacesTypeInfos[] = 
{
	&IBuiltInEvidence_t3496_0_0_0,
	&IIdentityPermissionFactory_t3497_0_0_0,
};
static Il2CppInterfaceOffsetPair StrongName_t2011_InterfacesOffsets[] = 
{
	{ &IBuiltInEvidence_t3496_0_0_0, 4},
	{ &IIdentityPermissionFactory_t3497_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongName_t2011_0_0_0;
extern const Il2CppType StrongName_t2011_1_0_0;
struct StrongName_t2011;
const Il2CppTypeDefinitionMetadata StrongName_t2011_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StrongName_t2011_InterfacesTypeInfos/* implementedInterfaces */
	, StrongName_t2011_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongName_t2011_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8816/* fieldStart */
	, 12615/* methodStart */
	, -1/* eventStart */
	, 2495/* propertyStart */

};
TypeInfo StrongName_t2011_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongName"/* name */
	, "System.Security.Policy"/* namespaze */
	, NULL/* methods */
	, &StrongName_t2011_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3119/* custom_attributes_cache */
	, &StrongName_t2011_0_0_0/* byval_arg */
	, &StrongName_t2011_1_0_0/* this_arg */
	, &StrongName_t2011_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongName_t2011)/* instance_size */
	, sizeof (StrongName_t2011)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Security.Principal.IIdentity
extern TypeInfo IIdentity_t3498_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IIdentity_t3498_0_0_0;
extern const Il2CppType IIdentity_t3498_1_0_0;
struct IIdentity_t3498;
const Il2CppTypeDefinitionMetadata IIdentity_t3498_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IIdentity_t3498_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IIdentity"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &IIdentity_t3498_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3120/* custom_attributes_cache */
	, &IIdentity_t3498_0_0_0/* byval_arg */
	, &IIdentity_t3498_1_0_0/* this_arg */
	, &IIdentity_t3498_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.Principal.IPrincipal
extern TypeInfo IPrincipal_t2060_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IPrincipal_t2060_0_0_0;
extern const Il2CppType IPrincipal_t2060_1_0_0;
struct IPrincipal_t2060;
const Il2CppTypeDefinitionMetadata IPrincipal_t2060_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPrincipal_t2060_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPrincipal"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &IPrincipal_t2060_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3121/* custom_attributes_cache */
	, &IPrincipal_t2060_0_0_0/* byval_arg */
	, &IPrincipal_t2060_1_0_0/* this_arg */
	, &IPrincipal_t2060_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.Principal.PrincipalPolicy
#include "mscorlib_System_Security_Principal_PrincipalPolicy.h"
// Metadata Definition System.Security.Principal.PrincipalPolicy
extern TypeInfo PrincipalPolicy_t2012_il2cpp_TypeInfo;
// System.Security.Principal.PrincipalPolicy
#include "mscorlib_System_Security_Principal_PrincipalPolicyMethodDeclarations.h"
static const EncodedMethodIndex PrincipalPolicy_t2012_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair PrincipalPolicy_t2012_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrincipalPolicy_t2012_0_0_0;
extern const Il2CppType PrincipalPolicy_t2012_1_0_0;
const Il2CppTypeDefinitionMetadata PrincipalPolicy_t2012_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrincipalPolicy_t2012_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, PrincipalPolicy_t2012_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8819/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrincipalPolicy_t2012_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrincipalPolicy"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3122/* custom_attributes_cache */
	, &PrincipalPolicy_t2012_0_0_0/* byval_arg */
	, &PrincipalPolicy_t2012_1_0_0/* this_arg */
	, &PrincipalPolicy_t2012_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrincipalPolicy_t2012)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PrincipalPolicy_t2012)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Principal.WindowsAccountType
#include "mscorlib_System_Security_Principal_WindowsAccountType.h"
// Metadata Definition System.Security.Principal.WindowsAccountType
extern TypeInfo WindowsAccountType_t2013_il2cpp_TypeInfo;
// System.Security.Principal.WindowsAccountType
#include "mscorlib_System_Security_Principal_WindowsAccountTypeMethodDeclarations.h"
static const EncodedMethodIndex WindowsAccountType_t2013_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair WindowsAccountType_t2013_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WindowsAccountType_t2013_0_0_0;
extern const Il2CppType WindowsAccountType_t2013_1_0_0;
const Il2CppTypeDefinitionMetadata WindowsAccountType_t2013_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WindowsAccountType_t2013_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, WindowsAccountType_t2013_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8823/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WindowsAccountType_t2013_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WindowsAccountType"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3123/* custom_attributes_cache */
	, &WindowsAccountType_t2013_0_0_0/* byval_arg */
	, &WindowsAccountType_t2013_1_0_0/* this_arg */
	, &WindowsAccountType_t2013_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WindowsAccountType_t2013)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WindowsAccountType_t2013)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Security.Principal.WindowsIdentity
#include "mscorlib_System_Security_Principal_WindowsIdentity.h"
// Metadata Definition System.Security.Principal.WindowsIdentity
extern TypeInfo WindowsIdentity_t2014_il2cpp_TypeInfo;
// System.Security.Principal.WindowsIdentity
#include "mscorlib_System_Security_Principal_WindowsIdentityMethodDeclarations.h"
static const EncodedMethodIndex WindowsIdentity_t2014_VTable[7] = 
{
	626,
	601,
	627,
	628,
	4061,
	4062,
	4063,
};
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
static const Il2CppType* WindowsIdentity_t2014_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ISerializable_t2210_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
	&IIdentity_t3498_0_0_0,
};
static Il2CppInterfaceOffsetPair WindowsIdentity_t2014_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 5},
	{ &IDeserializationCallback_t2213_0_0_0, 6},
	{ &IIdentity_t3498_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WindowsIdentity_t2014_0_0_0;
extern const Il2CppType WindowsIdentity_t2014_1_0_0;
struct WindowsIdentity_t2014;
const Il2CppTypeDefinitionMetadata WindowsIdentity_t2014_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WindowsIdentity_t2014_InterfacesTypeInfos/* implementedInterfaces */
	, WindowsIdentity_t2014_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WindowsIdentity_t2014_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8828/* fieldStart */
	, 12621/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo WindowsIdentity_t2014_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WindowsIdentity"/* name */
	, "System.Security.Principal"/* namespaze */
	, NULL/* methods */
	, &WindowsIdentity_t2014_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3124/* custom_attributes_cache */
	, &WindowsIdentity_t2014_0_0_0/* byval_arg */
	, &WindowsIdentity_t2014_1_0_0/* this_arg */
	, &WindowsIdentity_t2014_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WindowsIdentity_t2014)/* instance_size */
	, sizeof (WindowsIdentity_t2014)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WindowsIdentity_t2014_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Security.CodeAccessPermission
#include "mscorlib_System_Security_CodeAccessPermission.h"
// Metadata Definition System.Security.CodeAccessPermission
extern TypeInfo CodeAccessPermission_t2004_il2cpp_TypeInfo;
// System.Security.CodeAccessPermission
#include "mscorlib_System_Security_CodeAccessPermissionMethodDeclarations.h"
static const EncodedMethodIndex CodeAccessPermission_t2004_VTable[6] = 
{
	4038,
	601,
	4039,
	4040,
	0,
	0,
};
static const Il2CppType* CodeAccessPermission_t2004_InterfacesTypeInfos[] = 
{
	&IPermission_t2018_0_0_0,
	&ISecurityEncodable_t3499_0_0_0,
	&IStackWalk_t3500_0_0_0,
};
static Il2CppInterfaceOffsetPair CodeAccessPermission_t2004_InterfacesOffsets[] = 
{
	{ &IPermission_t2018_0_0_0, 4},
	{ &ISecurityEncodable_t3499_0_0_0, 4},
	{ &IStackWalk_t3500_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CodeAccessPermission_t2004_1_0_0;
struct CodeAccessPermission_t2004;
const Il2CppTypeDefinitionMetadata CodeAccessPermission_t2004_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CodeAccessPermission_t2004_InterfacesTypeInfos/* implementedInterfaces */
	, CodeAccessPermission_t2004_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CodeAccessPermission_t2004_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12628/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CodeAccessPermission_t2004_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CodeAccessPermission"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &CodeAccessPermission_t2004_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3126/* custom_attributes_cache */
	, &CodeAccessPermission_t2004_0_0_0/* byval_arg */
	, &CodeAccessPermission_t2004_1_0_0/* this_arg */
	, &CodeAccessPermission_t2004_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CodeAccessPermission_t2004)/* instance_size */
	, sizeof (CodeAccessPermission_t2004)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Security.IPermission
extern TypeInfo IPermission_t2018_il2cpp_TypeInfo;
static const Il2CppType* IPermission_t2018_InterfacesTypeInfos[] = 
{
	&ISecurityEncodable_t3499_0_0_0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IPermission_t2018_1_0_0;
struct IPermission_t2018;
const Il2CppTypeDefinitionMetadata IPermission_t2018_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IPermission_t2018_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IPermission_t2018_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IPermission"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &IPermission_t2018_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3129/* custom_attributes_cache */
	, &IPermission_t2018_0_0_0/* byval_arg */
	, &IPermission_t2018_1_0_0/* this_arg */
	, &IPermission_t2018_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.ISecurityEncodable
extern TypeInfo ISecurityEncodable_t3499_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ISecurityEncodable_t3499_1_0_0;
struct ISecurityEncodable_t3499;
const Il2CppTypeDefinitionMetadata ISecurityEncodable_t3499_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ISecurityEncodable_t3499_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISecurityEncodable"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &ISecurityEncodable_t3499_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3130/* custom_attributes_cache */
	, &ISecurityEncodable_t3499_0_0_0/* byval_arg */
	, &ISecurityEncodable_t3499_1_0_0/* this_arg */
	, &ISecurityEncodable_t3499_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Security.IStackWalk
extern TypeInfo IStackWalk_t3500_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IStackWalk_t3500_1_0_0;
struct IStackWalk_t3500;
const Il2CppTypeDefinitionMetadata IStackWalk_t3500_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IStackWalk_t3500_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IStackWalk"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &IStackWalk_t3500_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3131/* custom_attributes_cache */
	, &IStackWalk_t3500_0_0_0/* byval_arg */
	, &IStackWalk_t3500_1_0_0/* this_arg */
	, &IStackWalk_t3500_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.PermissionSet
#include "mscorlib_System_Security_PermissionSet.h"
// Metadata Definition System.Security.PermissionSet
extern TypeInfo PermissionSet_t1768_il2cpp_TypeInfo;
// System.Security.PermissionSet
#include "mscorlib_System_Security_PermissionSetMethodDeclarations.h"
static const EncodedMethodIndex PermissionSet_t1768_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PermissionSet_t1768_0_0_0;
extern const Il2CppType PermissionSet_t1768_1_0_0;
struct PermissionSet_t1768;
const Il2CppTypeDefinitionMetadata PermissionSet_t1768_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PermissionSet_t1768_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8835/* fieldStart */
	, 12636/* methodStart */
	, -1/* eventStart */
	, 2498/* propertyStart */

};
TypeInfo PermissionSet_t1768_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PermissionSet"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &PermissionSet_t1768_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PermissionSet_t1768_0_0_0/* byval_arg */
	, &PermissionSet_t1768_1_0_0/* this_arg */
	, &PermissionSet_t1768_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PermissionSet_t1768)/* instance_size */
	, sizeof (PermissionSet_t1768)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityContext
#include "mscorlib_System_Security_SecurityContext.h"
// Metadata Definition System.Security.SecurityContext
extern TypeInfo SecurityContext_t2016_il2cpp_TypeInfo;
// System.Security.SecurityContext
#include "mscorlib_System_Security_SecurityContextMethodDeclarations.h"
static const EncodedMethodIndex SecurityContext_t2016_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityContext_t2016_0_0_0;
extern const Il2CppType SecurityContext_t2016_1_0_0;
struct SecurityContext_t2016;
const Il2CppTypeDefinitionMetadata SecurityContext_t2016_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityContext_t2016_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8836/* fieldStart */
	, 12640/* methodStart */
	, -1/* eventStart */
	, 2499/* propertyStart */

};
TypeInfo SecurityContext_t2016_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityContext"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityContext_t2016_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityContext_t2016_0_0_0/* byval_arg */
	, &SecurityContext_t2016_1_0_0/* this_arg */
	, &SecurityContext_t2016_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityContext_t2016)/* instance_size */
	, sizeof (SecurityContext_t2016)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityElement
#include "mscorlib_System_Security_SecurityElement.h"
// Metadata Definition System.Security.SecurityElement
extern TypeInfo SecurityElement_t1639_il2cpp_TypeInfo;
// System.Security.SecurityElement
#include "mscorlib_System_Security_SecurityElementMethodDeclarations.h"
extern const Il2CppType SecurityAttribute_t2017_0_0_0;
static const Il2CppType* SecurityElement_t1639_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SecurityAttribute_t2017_0_0_0,
};
static const EncodedMethodIndex SecurityElement_t1639_VTable[4] = 
{
	626,
	601,
	627,
	4064,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityElement_t1639_0_0_0;
extern const Il2CppType SecurityElement_t1639_1_0_0;
struct SecurityElement_t1639;
const Il2CppTypeDefinitionMetadata SecurityElement_t1639_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SecurityElement_t1639_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityElement_t1639_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8840/* fieldStart */
	, 12645/* methodStart */
	, -1/* eventStart */
	, 2501/* propertyStart */

};
TypeInfo SecurityElement_t1639_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityElement"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityElement_t1639_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3134/* custom_attributes_cache */
	, &SecurityElement_t1639_0_0_0/* byval_arg */
	, &SecurityElement_t1639_1_0_0/* this_arg */
	, &SecurityElement_t1639_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityElement_t1639)/* instance_size */
	, sizeof (SecurityElement_t1639)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SecurityElement_t1639_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 3/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityElement/SecurityAttribute
#include "mscorlib_System_Security_SecurityElement_SecurityAttribute.h"
// Metadata Definition System.Security.SecurityElement/SecurityAttribute
extern TypeInfo SecurityAttribute_t2017_il2cpp_TypeInfo;
// System.Security.SecurityElement/SecurityAttribute
#include "mscorlib_System_Security_SecurityElement_SecurityAttributeMethodDeclarations.h"
static const EncodedMethodIndex SecurityAttribute_t2017_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityAttribute_t2017_1_0_0;
struct SecurityAttribute_t2017;
const Il2CppTypeDefinitionMetadata SecurityAttribute_t2017_DefinitionMetadata = 
{
	&SecurityElement_t1639_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityAttribute_t2017_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8849/* fieldStart */
	, 12663/* methodStart */
	, -1/* eventStart */
	, 2504/* propertyStart */

};
TypeInfo SecurityAttribute_t2017_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityAttribute"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SecurityAttribute_t2017_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityAttribute_t2017_0_0_0/* byval_arg */
	, &SecurityAttribute_t2017_1_0_0/* this_arg */
	, &SecurityAttribute_t2017_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityAttribute_t2017)/* instance_size */
	, sizeof (SecurityAttribute_t2017)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityException
#include "mscorlib_System_Security_SecurityException.h"
// Metadata Definition System.Security.SecurityException
extern TypeInfo SecurityException_t2019_il2cpp_TypeInfo;
// System.Security.SecurityException
#include "mscorlib_System_Security_SecurityExceptionMethodDeclarations.h"
static const EncodedMethodIndex SecurityException_t2019_VTable[11] = 
{
	626,
	601,
	627,
	4065,
	4066,
	1318,
	1319,
	1320,
	1321,
	4066,
	1322,
};
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair SecurityException_t2019_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityException_t2019_0_0_0;
extern const Il2CppType SecurityException_t2019_1_0_0;
extern const Il2CppType SystemException_t1536_0_0_0;
struct SecurityException_t2019;
const Il2CppTypeDefinitionMetadata SecurityException_t2019_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityException_t2019_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, SecurityException_t2019_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8851/* fieldStart */
	, 12666/* methodStart */
	, -1/* eventStart */
	, 2506/* propertyStart */

};
TypeInfo SecurityException_t2019_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityException"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityException_t2019_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3135/* custom_attributes_cache */
	, &SecurityException_t2019_0_0_0/* byval_arg */
	, &SecurityException_t2019_1_0_0/* this_arg */
	, &SecurityException_t2019_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityException_t2019)/* instance_size */
	, sizeof (SecurityException_t2019)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 6/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Security.RuntimeDeclSecurityEntry
#include "mscorlib_System_Security_RuntimeDeclSecurityEntry.h"
// Metadata Definition System.Security.RuntimeDeclSecurityEntry
extern TypeInfo RuntimeDeclSecurityEntry_t2020_il2cpp_TypeInfo;
// System.Security.RuntimeDeclSecurityEntry
#include "mscorlib_System_Security_RuntimeDeclSecurityEntryMethodDeclarations.h"
static const EncodedMethodIndex RuntimeDeclSecurityEntry_t2020_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeDeclSecurityEntry_t2020_0_0_0;
extern const Il2CppType RuntimeDeclSecurityEntry_t2020_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeDeclSecurityEntry_t2020_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, RuntimeDeclSecurityEntry_t2020_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8859/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimeDeclSecurityEntry_t2020_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeDeclSecurityEntry"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &RuntimeDeclSecurityEntry_t2020_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimeDeclSecurityEntry_t2020_0_0_0/* byval_arg */
	, &RuntimeDeclSecurityEntry_t2020_1_0_0/* this_arg */
	, &RuntimeDeclSecurityEntry_t2020_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeDeclSecurityEntry_t2020)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeDeclSecurityEntry_t2020)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeDeclSecurityEntry_t2020 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.RuntimeSecurityFrame
#include "mscorlib_System_Security_RuntimeSecurityFrame.h"
// Metadata Definition System.Security.RuntimeSecurityFrame
extern TypeInfo RuntimeSecurityFrame_t2022_il2cpp_TypeInfo;
// System.Security.RuntimeSecurityFrame
#include "mscorlib_System_Security_RuntimeSecurityFrameMethodDeclarations.h"
static const EncodedMethodIndex RuntimeSecurityFrame_t2022_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeSecurityFrame_t2022_0_0_0;
extern const Il2CppType RuntimeSecurityFrame_t2022_1_0_0;
struct RuntimeSecurityFrame_t2022;
const Il2CppTypeDefinitionMetadata RuntimeSecurityFrame_t2022_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RuntimeSecurityFrame_t2022_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8862/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RuntimeSecurityFrame_t2022_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeSecurityFrame"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &RuntimeSecurityFrame_t2022_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RuntimeSecurityFrame_t2022_0_0_0/* byval_arg */
	, &RuntimeSecurityFrame_t2022_1_0_0/* this_arg */
	, &RuntimeSecurityFrame_t2022_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeSecurityFrame_t2022)/* instance_size */
	, sizeof (RuntimeSecurityFrame_t2022)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityFrame
#include "mscorlib_System_Security_SecurityFrame.h"
// Metadata Definition System.Security.SecurityFrame
extern TypeInfo SecurityFrame_t2023_il2cpp_TypeInfo;
// System.Security.SecurityFrame
#include "mscorlib_System_Security_SecurityFrameMethodDeclarations.h"
static const EncodedMethodIndex SecurityFrame_t2023_VTable[4] = 
{
	652,
	601,
	653,
	4067,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityFrame_t2023_0_0_0;
extern const Il2CppType SecurityFrame_t2023_1_0_0;
const Il2CppTypeDefinitionMetadata SecurityFrame_t2023_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, SecurityFrame_t2023_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8867/* fieldStart */
	, 12677/* methodStart */
	, -1/* eventStart */
	, 2512/* propertyStart */

};
TypeInfo SecurityFrame_t2023_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityFrame"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityFrame_t2023_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityFrame_t2023_0_0_0/* byval_arg */
	, &SecurityFrame_t2023_1_0_0/* this_arg */
	, &SecurityFrame_t2023_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityFrame_t2023)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityFrame_t2023)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecurityManager
#include "mscorlib_System_Security_SecurityManager.h"
// Metadata Definition System.Security.SecurityManager
extern TypeInfo SecurityManager_t2024_il2cpp_TypeInfo;
// System.Security.SecurityManager
#include "mscorlib_System_Security_SecurityManagerMethodDeclarations.h"
static const EncodedMethodIndex SecurityManager_t2024_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecurityManager_t2024_0_0_0;
extern const Il2CppType SecurityManager_t2024_1_0_0;
struct SecurityManager_t2024;
const Il2CppTypeDefinitionMetadata SecurityManager_t2024_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityManager_t2024_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8872/* fieldStart */
	, 12684/* methodStart */
	, -1/* eventStart */
	, 2514/* propertyStart */

};
TypeInfo SecurityManager_t2024_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityManager"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecurityManager_t2024_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3137/* custom_attributes_cache */
	, &SecurityManager_t2024_0_0_0/* byval_arg */
	, &SecurityManager_t2024_1_0_0/* this_arg */
	, &SecurityManager_t2024_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityManager_t2024)/* instance_size */
	, sizeof (SecurityManager_t2024)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SecurityManager_t2024_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 385/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// Metadata Definition System.Security.SecuritySafeCriticalAttribute
extern TypeInfo SecuritySafeCriticalAttribute_t2025_il2cpp_TypeInfo;
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
static const EncodedMethodIndex SecuritySafeCriticalAttribute_t2025_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair SecuritySafeCriticalAttribute_t2025_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SecuritySafeCriticalAttribute_t2025_0_0_0;
extern const Il2CppType SecuritySafeCriticalAttribute_t2025_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct SecuritySafeCriticalAttribute_t2025;
const Il2CppTypeDefinitionMetadata SecuritySafeCriticalAttribute_t2025_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecuritySafeCriticalAttribute_t2025_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SecuritySafeCriticalAttribute_t2025_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12688/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecuritySafeCriticalAttribute_t2025_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecuritySafeCriticalAttribute"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SecuritySafeCriticalAttribute_t2025_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3139/* custom_attributes_cache */
	, &SecuritySafeCriticalAttribute_t2025_0_0_0/* byval_arg */
	, &SecuritySafeCriticalAttribute_t2025_1_0_0/* this_arg */
	, &SecuritySafeCriticalAttribute_t2025_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecuritySafeCriticalAttribute_t2025)/* instance_size */
	, sizeof (SecuritySafeCriticalAttribute_t2025)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttrib.h"
// Metadata Definition System.Security.SuppressUnmanagedCodeSecurityAttribute
extern TypeInfo SuppressUnmanagedCodeSecurityAttribute_t2026_il2cpp_TypeInfo;
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttribMethodDeclarations.h"
static const EncodedMethodIndex SuppressUnmanagedCodeSecurityAttribute_t2026_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair SuppressUnmanagedCodeSecurityAttribute_t2026_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SuppressUnmanagedCodeSecurityAttribute_t2026_0_0_0;
extern const Il2CppType SuppressUnmanagedCodeSecurityAttribute_t2026_1_0_0;
struct SuppressUnmanagedCodeSecurityAttribute_t2026;
const Il2CppTypeDefinitionMetadata SuppressUnmanagedCodeSecurityAttribute_t2026_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SuppressUnmanagedCodeSecurityAttribute_t2026_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SuppressUnmanagedCodeSecurityAttribute_t2026_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12689/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SuppressUnmanagedCodeSecurityAttribute_t2026_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SuppressUnmanagedCodeSecurityAttribute"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &SuppressUnmanagedCodeSecurityAttribute_t2026_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3140/* custom_attributes_cache */
	, &SuppressUnmanagedCodeSecurityAttribute_t2026_0_0_0/* byval_arg */
	, &SuppressUnmanagedCodeSecurityAttribute_t2026_1_0_0/* this_arg */
	, &SuppressUnmanagedCodeSecurityAttribute_t2026_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SuppressUnmanagedCodeSecurityAttribute_t2026)/* instance_size */
	, sizeof (SuppressUnmanagedCodeSecurityAttribute_t2026)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Security.UnverifiableCodeAttribute
#include "mscorlib_System_Security_UnverifiableCodeAttribute.h"
// Metadata Definition System.Security.UnverifiableCodeAttribute
extern TypeInfo UnverifiableCodeAttribute_t2027_il2cpp_TypeInfo;
// System.Security.UnverifiableCodeAttribute
#include "mscorlib_System_Security_UnverifiableCodeAttributeMethodDeclarations.h"
static const EncodedMethodIndex UnverifiableCodeAttribute_t2027_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair UnverifiableCodeAttribute_t2027_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnverifiableCodeAttribute_t2027_0_0_0;
extern const Il2CppType UnverifiableCodeAttribute_t2027_1_0_0;
struct UnverifiableCodeAttribute_t2027;
const Il2CppTypeDefinitionMetadata UnverifiableCodeAttribute_t2027_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnverifiableCodeAttribute_t2027_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, UnverifiableCodeAttribute_t2027_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12690/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnverifiableCodeAttribute_t2027_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnverifiableCodeAttribute"/* name */
	, "System.Security"/* namespaze */
	, NULL/* methods */
	, &UnverifiableCodeAttribute_t2027_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3141/* custom_attributes_cache */
	, &UnverifiableCodeAttribute_t2027_0_0_0/* byval_arg */
	, &UnverifiableCodeAttribute_t2027_1_0_0/* this_arg */
	, &UnverifiableCodeAttribute_t2027_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnverifiableCodeAttribute_t2027)/* instance_size */
	, sizeof (UnverifiableCodeAttribute_t2027)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.ASCIIEncoding
#include "mscorlib_System_Text_ASCIIEncoding.h"
// Metadata Definition System.Text.ASCIIEncoding
extern TypeInfo ASCIIEncoding_t2028_il2cpp_TypeInfo;
// System.Text.ASCIIEncoding
#include "mscorlib_System_Text_ASCIIEncodingMethodDeclarations.h"
static const EncodedMethodIndex ASCIIEncoding_t2028_VTable[25] = 
{
	4068,
	601,
	4069,
	628,
	4070,
	4071,
	4072,
	4073,
	4074,
	4075,
	4076,
	4077,
	4078,
	4079,
	4080,
	4081,
	4082,
	4083,
	4084,
	4085,
	4086,
	4087,
	4088,
	4089,
	4090,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
static Il2CppInterfaceOffsetPair ASCIIEncoding_t2028_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ASCIIEncoding_t2028_0_0_0;
extern const Il2CppType ASCIIEncoding_t2028_1_0_0;
extern const Il2CppType Encoding_t519_0_0_0;
struct ASCIIEncoding_t2028;
const Il2CppTypeDefinitionMetadata ASCIIEncoding_t2028_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ASCIIEncoding_t2028_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t519_0_0_0/* parent */
	, ASCIIEncoding_t2028_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12691/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ASCIIEncoding_t2028_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASCIIEncoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &ASCIIEncoding_t2028_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3142/* custom_attributes_cache */
	, &ASCIIEncoding_t2028_0_0_0/* byval_arg */
	, &ASCIIEncoding_t2028_1_0_0/* this_arg */
	, &ASCIIEncoding_t2028_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASCIIEncoding_t2028)/* instance_size */
	, sizeof (ASCIIEncoding_t2028)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// Metadata Definition System.Text.Decoder
extern TypeInfo Decoder_t1695_il2cpp_TypeInfo;
// System.Text.Decoder
#include "mscorlib_System_Text_DecoderMethodDeclarations.h"
static const EncodedMethodIndex Decoder_t1695_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Decoder_t1695_0_0_0;
extern const Il2CppType Decoder_t1695_1_0_0;
struct Decoder_t1695;
const Il2CppTypeDefinitionMetadata Decoder_t1695_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Decoder_t1695_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8875/* fieldStart */
	, 12707/* methodStart */
	, -1/* eventStart */
	, 2515/* propertyStart */

};
TypeInfo Decoder_t1695_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Decoder"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &Decoder_t1695_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3146/* custom_attributes_cache */
	, &Decoder_t1695_0_0_0/* byval_arg */
	, &Decoder_t1695_1_0_0/* this_arg */
	, &Decoder_t1695_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Decoder_t1695)/* instance_size */
	, sizeof (Decoder_t1695)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderExceptionFallback
#include "mscorlib_System_Text_DecoderExceptionFallback.h"
// Metadata Definition System.Text.DecoderExceptionFallback
extern TypeInfo DecoderExceptionFallback_t2031_il2cpp_TypeInfo;
// System.Text.DecoderExceptionFallback
#include "mscorlib_System_Text_DecoderExceptionFallbackMethodDeclarations.h"
static const EncodedMethodIndex DecoderExceptionFallback_t2031_VTable[5] = 
{
	4091,
	601,
	4092,
	628,
	4093,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderExceptionFallback_t2031_0_0_0;
extern const Il2CppType DecoderExceptionFallback_t2031_1_0_0;
extern const Il2CppType DecoderFallback_t2029_0_0_0;
struct DecoderExceptionFallback_t2031;
const Il2CppTypeDefinitionMetadata DecoderExceptionFallback_t2031_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallback_t2029_0_0_0/* parent */
	, DecoderExceptionFallback_t2031_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12711/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DecoderExceptionFallback_t2031_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderExceptionFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderExceptionFallback_t2031_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderExceptionFallback_t2031_0_0_0/* byval_arg */
	, &DecoderExceptionFallback_t2031_1_0_0/* this_arg */
	, &DecoderExceptionFallback_t2031_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderExceptionFallback_t2031)/* instance_size */
	, sizeof (DecoderExceptionFallback_t2031)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderExceptionFallbackBuffer
#include "mscorlib_System_Text_DecoderExceptionFallbackBuffer.h"
// Metadata Definition System.Text.DecoderExceptionFallbackBuffer
extern TypeInfo DecoderExceptionFallbackBuffer_t2032_il2cpp_TypeInfo;
// System.Text.DecoderExceptionFallbackBuffer
#include "mscorlib_System_Text_DecoderExceptionFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex DecoderExceptionFallbackBuffer_t2032_VTable[8] = 
{
	626,
	601,
	627,
	628,
	4094,
	4095,
	4096,
	4097,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderExceptionFallbackBuffer_t2032_0_0_0;
extern const Il2CppType DecoderExceptionFallbackBuffer_t2032_1_0_0;
extern const Il2CppType DecoderFallbackBuffer_t2030_0_0_0;
struct DecoderExceptionFallbackBuffer_t2032;
const Il2CppTypeDefinitionMetadata DecoderExceptionFallbackBuffer_t2032_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallbackBuffer_t2030_0_0_0/* parent */
	, DecoderExceptionFallbackBuffer_t2032_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12715/* methodStart */
	, -1/* eventStart */
	, 2517/* propertyStart */

};
TypeInfo DecoderExceptionFallbackBuffer_t2032_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderExceptionFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderExceptionFallbackBuffer_t2032_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderExceptionFallbackBuffer_t2032_0_0_0/* byval_arg */
	, &DecoderExceptionFallbackBuffer_t2032_1_0_0/* this_arg */
	, &DecoderExceptionFallbackBuffer_t2032_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderExceptionFallbackBuffer_t2032)/* instance_size */
	, sizeof (DecoderExceptionFallbackBuffer_t2032)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderFallback
#include "mscorlib_System_Text_DecoderFallback.h"
// Metadata Definition System.Text.DecoderFallback
extern TypeInfo DecoderFallback_t2029_il2cpp_TypeInfo;
// System.Text.DecoderFallback
#include "mscorlib_System_Text_DecoderFallbackMethodDeclarations.h"
static const EncodedMethodIndex DecoderFallback_t2029_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderFallback_t2029_1_0_0;
struct DecoderFallback_t2029;
const Il2CppTypeDefinitionMetadata DecoderFallback_t2029_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DecoderFallback_t2029_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8877/* fieldStart */
	, 12719/* methodStart */
	, -1/* eventStart */
	, 2518/* propertyStart */

};
TypeInfo DecoderFallback_t2029_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderFallback_t2029_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderFallback_t2029_0_0_0/* byval_arg */
	, &DecoderFallback_t2029_1_0_0/* this_arg */
	, &DecoderFallback_t2029_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderFallback_t2029)/* instance_size */
	, sizeof (DecoderFallback_t2029)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DecoderFallback_t2029_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
// Metadata Definition System.Text.DecoderFallbackBuffer
extern TypeInfo DecoderFallbackBuffer_t2030_il2cpp_TypeInfo;
// System.Text.DecoderFallbackBuffer
#include "mscorlib_System_Text_DecoderFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex DecoderFallbackBuffer_t2030_VTable[8] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
	4097,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderFallbackBuffer_t2030_1_0_0;
struct DecoderFallbackBuffer_t2030;
const Il2CppTypeDefinitionMetadata DecoderFallbackBuffer_t2030_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DecoderFallbackBuffer_t2030_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12725/* methodStart */
	, -1/* eventStart */
	, 2521/* propertyStart */

};
TypeInfo DecoderFallbackBuffer_t2030_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderFallbackBuffer_t2030_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderFallbackBuffer_t2030_0_0_0/* byval_arg */
	, &DecoderFallbackBuffer_t2030_1_0_0/* this_arg */
	, &DecoderFallbackBuffer_t2030_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderFallbackBuffer_t2030)/* instance_size */
	, sizeof (DecoderFallbackBuffer_t2030)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderFallbackException
#include "mscorlib_System_Text_DecoderFallbackException.h"
// Metadata Definition System.Text.DecoderFallbackException
extern TypeInfo DecoderFallbackException_t2033_il2cpp_TypeInfo;
// System.Text.DecoderFallbackException
#include "mscorlib_System_Text_DecoderFallbackExceptionMethodDeclarations.h"
static const EncodedMethodIndex DecoderFallbackException_t2033_VTable[12] = 
{
	626,
	601,
	627,
	1316,
	4098,
	1318,
	4099,
	1320,
	1321,
	4098,
	1322,
	4100,
};
static Il2CppInterfaceOffsetPair DecoderFallbackException_t2033_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderFallbackException_t2033_0_0_0;
extern const Il2CppType DecoderFallbackException_t2033_1_0_0;
extern const Il2CppType ArgumentException_t556_0_0_0;
struct DecoderFallbackException_t2033;
const Il2CppTypeDefinitionMetadata DecoderFallbackException_t2033_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DecoderFallbackException_t2033_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t556_0_0_0/* parent */
	, DecoderFallbackException_t2033_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8880/* fieldStart */
	, 12730/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DecoderFallbackException_t2033_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderFallbackException"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderFallbackException_t2033_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderFallbackException_t2033_0_0_0/* byval_arg */
	, &DecoderFallbackException_t2033_1_0_0/* this_arg */
	, &DecoderFallbackException_t2033_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderFallbackException_t2033)/* instance_size */
	, sizeof (DecoderFallbackException_t2033)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.DecoderReplacementFallback
#include "mscorlib_System_Text_DecoderReplacementFallback.h"
// Metadata Definition System.Text.DecoderReplacementFallback
extern TypeInfo DecoderReplacementFallback_t2034_il2cpp_TypeInfo;
// System.Text.DecoderReplacementFallback
#include "mscorlib_System_Text_DecoderReplacementFallbackMethodDeclarations.h"
static const EncodedMethodIndex DecoderReplacementFallback_t2034_VTable[5] = 
{
	4101,
	601,
	4102,
	628,
	4103,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderReplacementFallback_t2034_0_0_0;
extern const Il2CppType DecoderReplacementFallback_t2034_1_0_0;
struct DecoderReplacementFallback_t2034;
const Il2CppTypeDefinitionMetadata DecoderReplacementFallback_t2034_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallback_t2029_0_0_0/* parent */
	, DecoderReplacementFallback_t2034_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8882/* fieldStart */
	, 12733/* methodStart */
	, -1/* eventStart */
	, 2522/* propertyStart */

};
TypeInfo DecoderReplacementFallback_t2034_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderReplacementFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderReplacementFallback_t2034_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderReplacementFallback_t2034_0_0_0/* byval_arg */
	, &DecoderReplacementFallback_t2034_1_0_0/* this_arg */
	, &DecoderReplacementFallback_t2034_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderReplacementFallback_t2034)/* instance_size */
	, sizeof (DecoderReplacementFallback_t2034)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.DecoderReplacementFallbackBuffer
#include "mscorlib_System_Text_DecoderReplacementFallbackBuffer.h"
// Metadata Definition System.Text.DecoderReplacementFallbackBuffer
extern TypeInfo DecoderReplacementFallbackBuffer_t2035_il2cpp_TypeInfo;
// System.Text.DecoderReplacementFallbackBuffer
#include "mscorlib_System_Text_DecoderReplacementFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex DecoderReplacementFallbackBuffer_t2035_VTable[8] = 
{
	626,
	601,
	627,
	628,
	4104,
	4105,
	4106,
	4107,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DecoderReplacementFallbackBuffer_t2035_0_0_0;
extern const Il2CppType DecoderReplacementFallbackBuffer_t2035_1_0_0;
struct DecoderReplacementFallbackBuffer_t2035;
const Il2CppTypeDefinitionMetadata DecoderReplacementFallbackBuffer_t2035_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &DecoderFallbackBuffer_t2030_0_0_0/* parent */
	, DecoderReplacementFallbackBuffer_t2035_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8883/* fieldStart */
	, 12739/* methodStart */
	, -1/* eventStart */
	, 2523/* propertyStart */

};
TypeInfo DecoderReplacementFallbackBuffer_t2035_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DecoderReplacementFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &DecoderReplacementFallbackBuffer_t2035_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DecoderReplacementFallbackBuffer_t2035_0_0_0/* byval_arg */
	, &DecoderReplacementFallbackBuffer_t2035_1_0_0/* this_arg */
	, &DecoderReplacementFallbackBuffer_t2035_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DecoderReplacementFallbackBuffer_t2035)/* instance_size */
	, sizeof (DecoderReplacementFallbackBuffer_t2035)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderExceptionFallback
#include "mscorlib_System_Text_EncoderExceptionFallback.h"
// Metadata Definition System.Text.EncoderExceptionFallback
extern TypeInfo EncoderExceptionFallback_t2036_il2cpp_TypeInfo;
// System.Text.EncoderExceptionFallback
#include "mscorlib_System_Text_EncoderExceptionFallbackMethodDeclarations.h"
static const EncodedMethodIndex EncoderExceptionFallback_t2036_VTable[5] = 
{
	4108,
	601,
	4109,
	628,
	4110,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderExceptionFallback_t2036_0_0_0;
extern const Il2CppType EncoderExceptionFallback_t2036_1_0_0;
extern const Il2CppType EncoderFallback_t2037_0_0_0;
struct EncoderExceptionFallback_t2036;
const Il2CppTypeDefinitionMetadata EncoderExceptionFallback_t2036_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallback_t2037_0_0_0/* parent */
	, EncoderExceptionFallback_t2036_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12744/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EncoderExceptionFallback_t2036_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderExceptionFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderExceptionFallback_t2036_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderExceptionFallback_t2036_0_0_0/* byval_arg */
	, &EncoderExceptionFallback_t2036_1_0_0/* this_arg */
	, &EncoderExceptionFallback_t2036_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderExceptionFallback_t2036)/* instance_size */
	, sizeof (EncoderExceptionFallback_t2036)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderExceptionFallbackBuffer
#include "mscorlib_System_Text_EncoderExceptionFallbackBuffer.h"
// Metadata Definition System.Text.EncoderExceptionFallbackBuffer
extern TypeInfo EncoderExceptionFallbackBuffer_t2038_il2cpp_TypeInfo;
// System.Text.EncoderExceptionFallbackBuffer
#include "mscorlib_System_Text_EncoderExceptionFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex EncoderExceptionFallbackBuffer_t2038_VTable[8] = 
{
	626,
	601,
	627,
	628,
	4111,
	4112,
	4113,
	4114,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderExceptionFallbackBuffer_t2038_0_0_0;
extern const Il2CppType EncoderExceptionFallbackBuffer_t2038_1_0_0;
extern const Il2CppType EncoderFallbackBuffer_t2039_0_0_0;
struct EncoderExceptionFallbackBuffer_t2038;
const Il2CppTypeDefinitionMetadata EncoderExceptionFallbackBuffer_t2038_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallbackBuffer_t2039_0_0_0/* parent */
	, EncoderExceptionFallbackBuffer_t2038_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12748/* methodStart */
	, -1/* eventStart */
	, 2524/* propertyStart */

};
TypeInfo EncoderExceptionFallbackBuffer_t2038_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderExceptionFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderExceptionFallbackBuffer_t2038_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderExceptionFallbackBuffer_t2038_0_0_0/* byval_arg */
	, &EncoderExceptionFallbackBuffer_t2038_1_0_0/* this_arg */
	, &EncoderExceptionFallbackBuffer_t2038_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderExceptionFallbackBuffer_t2038)/* instance_size */
	, sizeof (EncoderExceptionFallbackBuffer_t2038)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderFallback
#include "mscorlib_System_Text_EncoderFallback.h"
// Metadata Definition System.Text.EncoderFallback
extern TypeInfo EncoderFallback_t2037_il2cpp_TypeInfo;
// System.Text.EncoderFallback
#include "mscorlib_System_Text_EncoderFallbackMethodDeclarations.h"
static const EncodedMethodIndex EncoderFallback_t2037_VTable[5] = 
{
	626,
	601,
	627,
	628,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderFallback_t2037_1_0_0;
struct EncoderFallback_t2037;
const Il2CppTypeDefinitionMetadata EncoderFallback_t2037_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncoderFallback_t2037_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8886/* fieldStart */
	, 12753/* methodStart */
	, -1/* eventStart */
	, 2525/* propertyStart */

};
TypeInfo EncoderFallback_t2037_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderFallback_t2037_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderFallback_t2037_0_0_0/* byval_arg */
	, &EncoderFallback_t2037_1_0_0/* this_arg */
	, &EncoderFallback_t2037_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderFallback_t2037)/* instance_size */
	, sizeof (EncoderFallback_t2037)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(EncoderFallback_t2037_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
// Metadata Definition System.Text.EncoderFallbackBuffer
extern TypeInfo EncoderFallbackBuffer_t2039_il2cpp_TypeInfo;
// System.Text.EncoderFallbackBuffer
#include "mscorlib_System_Text_EncoderFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex EncoderFallbackBuffer_t2039_VTable[8] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderFallbackBuffer_t2039_1_0_0;
struct EncoderFallbackBuffer_t2039;
const Il2CppTypeDefinitionMetadata EncoderFallbackBuffer_t2039_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncoderFallbackBuffer_t2039_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12759/* methodStart */
	, -1/* eventStart */
	, 2528/* propertyStart */

};
TypeInfo EncoderFallbackBuffer_t2039_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderFallbackBuffer_t2039_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderFallbackBuffer_t2039_0_0_0/* byval_arg */
	, &EncoderFallbackBuffer_t2039_1_0_0/* this_arg */
	, &EncoderFallbackBuffer_t2039_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderFallbackBuffer_t2039)/* instance_size */
	, sizeof (EncoderFallbackBuffer_t2039)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderFallbackException
#include "mscorlib_System_Text_EncoderFallbackException.h"
// Metadata Definition System.Text.EncoderFallbackException
extern TypeInfo EncoderFallbackException_t2040_il2cpp_TypeInfo;
// System.Text.EncoderFallbackException
#include "mscorlib_System_Text_EncoderFallbackExceptionMethodDeclarations.h"
static const EncodedMethodIndex EncoderFallbackException_t2040_VTable[12] = 
{
	626,
	601,
	627,
	1316,
	4098,
	1318,
	4099,
	1320,
	1321,
	4098,
	1322,
	4100,
};
static Il2CppInterfaceOffsetPair EncoderFallbackException_t2040_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderFallbackException_t2040_0_0_0;
extern const Il2CppType EncoderFallbackException_t2040_1_0_0;
struct EncoderFallbackException_t2040;
const Il2CppTypeDefinitionMetadata EncoderFallbackException_t2040_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EncoderFallbackException_t2040_InterfacesOffsets/* interfaceOffsets */
	, &ArgumentException_t556_0_0_0/* parent */
	, EncoderFallbackException_t2040_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8889/* fieldStart */
	, 12764/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EncoderFallbackException_t2040_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderFallbackException"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderFallbackException_t2040_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderFallbackException_t2040_0_0_0/* byval_arg */
	, &EncoderFallbackException_t2040_1_0_0/* this_arg */
	, &EncoderFallbackException_t2040_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderFallbackException_t2040)/* instance_size */
	, sizeof (EncoderFallbackException_t2040)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Text.EncoderReplacementFallback
#include "mscorlib_System_Text_EncoderReplacementFallback.h"
// Metadata Definition System.Text.EncoderReplacementFallback
extern TypeInfo EncoderReplacementFallback_t2041_il2cpp_TypeInfo;
// System.Text.EncoderReplacementFallback
#include "mscorlib_System_Text_EncoderReplacementFallbackMethodDeclarations.h"
static const EncodedMethodIndex EncoderReplacementFallback_t2041_VTable[5] = 
{
	4115,
	601,
	4116,
	628,
	4117,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderReplacementFallback_t2041_0_0_0;
extern const Il2CppType EncoderReplacementFallback_t2041_1_0_0;
struct EncoderReplacementFallback_t2041;
const Il2CppTypeDefinitionMetadata EncoderReplacementFallback_t2041_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallback_t2037_0_0_0/* parent */
	, EncoderReplacementFallback_t2041_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8893/* fieldStart */
	, 12768/* methodStart */
	, -1/* eventStart */
	, 2529/* propertyStart */

};
TypeInfo EncoderReplacementFallback_t2041_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderReplacementFallback"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderReplacementFallback_t2041_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderReplacementFallback_t2041_0_0_0/* byval_arg */
	, &EncoderReplacementFallback_t2041_1_0_0/* this_arg */
	, &EncoderReplacementFallback_t2041_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderReplacementFallback_t2041)/* instance_size */
	, sizeof (EncoderReplacementFallback_t2041)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.EncoderReplacementFallbackBuffer
#include "mscorlib_System_Text_EncoderReplacementFallbackBuffer.h"
// Metadata Definition System.Text.EncoderReplacementFallbackBuffer
extern TypeInfo EncoderReplacementFallbackBuffer_t2042_il2cpp_TypeInfo;
// System.Text.EncoderReplacementFallbackBuffer
#include "mscorlib_System_Text_EncoderReplacementFallbackBufferMethodDeclarations.h"
static const EncodedMethodIndex EncoderReplacementFallbackBuffer_t2042_VTable[8] = 
{
	626,
	601,
	627,
	628,
	4118,
	4119,
	4120,
	4121,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EncoderReplacementFallbackBuffer_t2042_0_0_0;
extern const Il2CppType EncoderReplacementFallbackBuffer_t2042_1_0_0;
struct EncoderReplacementFallbackBuffer_t2042;
const Il2CppTypeDefinitionMetadata EncoderReplacementFallbackBuffer_t2042_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EncoderFallbackBuffer_t2039_0_0_0/* parent */
	, EncoderReplacementFallbackBuffer_t2042_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8894/* fieldStart */
	, 12774/* methodStart */
	, -1/* eventStart */
	, 2530/* propertyStart */

};
TypeInfo EncoderReplacementFallbackBuffer_t2042_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncoderReplacementFallbackBuffer"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &EncoderReplacementFallbackBuffer_t2042_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncoderReplacementFallbackBuffer_t2042_0_0_0/* byval_arg */
	, &EncoderReplacementFallbackBuffer_t2042_1_0_0/* this_arg */
	, &EncoderReplacementFallbackBuffer_t2042_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncoderReplacementFallbackBuffer_t2042)/* instance_size */
	, sizeof (EncoderReplacementFallbackBuffer_t2042)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// Metadata Definition System.Text.Encoding
extern TypeInfo Encoding_t519_il2cpp_TypeInfo;
// System.Text.Encoding
#include "mscorlib_System_Text_EncodingMethodDeclarations.h"
extern const Il2CppType ForwardingDecoder_t2043_0_0_0;
static const Il2CppType* Encoding_t519_il2cpp_TypeInfo__nestedTypes[1] =
{
	&ForwardingDecoder_t2043_0_0_0,
};
static const EncodedMethodIndex Encoding_t519_VTable[25] = 
{
	4068,
	601,
	4069,
	628,
	0,
	4122,
	4072,
	0,
	4123,
	4075,
	4076,
	4077,
	0,
	0,
	4080,
	4124,
	0,
	0,
	4084,
	4125,
	4086,
	4087,
	4088,
	4126,
	4127,
};
static const Il2CppType* Encoding_t519_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair Encoding_t519_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Encoding_t519_1_0_0;
struct Encoding_t519;
const Il2CppTypeDefinitionMetadata Encoding_t519_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Encoding_t519_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Encoding_t519_InterfacesTypeInfos/* implementedInterfaces */
	, Encoding_t519_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Encoding_t519_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8897/* fieldStart */
	, 12780/* methodStart */
	, -1/* eventStart */
	, 2531/* propertyStart */

};
TypeInfo Encoding_t519_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &Encoding_t519_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3151/* custom_attributes_cache */
	, &Encoding_t519_0_0_0/* byval_arg */
	, &Encoding_t519_1_0_0/* this_arg */
	, &Encoding_t519_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Encoding_t519)/* instance_size */
	, sizeof (Encoding_t519)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Encoding_t519_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 16/* property_count */
	, 28/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.Encoding/ForwardingDecoder
#include "mscorlib_System_Text_Encoding_ForwardingDecoder.h"
// Metadata Definition System.Text.Encoding/ForwardingDecoder
extern TypeInfo ForwardingDecoder_t2043_il2cpp_TypeInfo;
// System.Text.Encoding/ForwardingDecoder
#include "mscorlib_System_Text_Encoding_ForwardingDecoderMethodDeclarations.h"
static const EncodedMethodIndex ForwardingDecoder_t2043_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4128,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ForwardingDecoder_t2043_1_0_0;
struct ForwardingDecoder_t2043;
const Il2CppTypeDefinitionMetadata ForwardingDecoder_t2043_DefinitionMetadata = 
{
	&Encoding_t519_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t1695_0_0_0/* parent */
	, ForwardingDecoder_t2043_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8925/* fieldStart */
	, 12827/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ForwardingDecoder_t2043_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ForwardingDecoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ForwardingDecoder_t2043_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ForwardingDecoder_t2043_0_0_0/* byval_arg */
	, &ForwardingDecoder_t2043_1_0_0/* this_arg */
	, &ForwardingDecoder_t2043_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ForwardingDecoder_t2043)/* instance_size */
	, sizeof (ForwardingDecoder_t2043)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.Latin1Encoding
#include "mscorlib_System_Text_Latin1Encoding.h"
// Metadata Definition System.Text.Latin1Encoding
extern TypeInfo Latin1Encoding_t2044_il2cpp_TypeInfo;
// System.Text.Latin1Encoding
#include "mscorlib_System_Text_Latin1EncodingMethodDeclarations.h"
static const EncodedMethodIndex Latin1Encoding_t2044_VTable[25] = 
{
	4068,
	601,
	4069,
	628,
	4129,
	4130,
	4072,
	4131,
	4132,
	4075,
	4076,
	4077,
	4133,
	4134,
	4080,
	4124,
	4135,
	4136,
	4084,
	4137,
	4138,
	4139,
	4140,
	4126,
	4127,
};
static Il2CppInterfaceOffsetPair Latin1Encoding_t2044_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Latin1Encoding_t2044_0_0_0;
extern const Il2CppType Latin1Encoding_t2044_1_0_0;
struct Latin1Encoding_t2044;
const Il2CppTypeDefinitionMetadata Latin1Encoding_t2044_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Latin1Encoding_t2044_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t519_0_0_0/* parent */
	, Latin1Encoding_t2044_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12829/* methodStart */
	, -1/* eventStart */
	, 2547/* propertyStart */

};
TypeInfo Latin1Encoding_t2044_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Latin1Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &Latin1Encoding_t2044_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Latin1Encoding_t2044_0_0_0/* byval_arg */
	, &Latin1Encoding_t2044_1_0_0/* this_arg */
	, &Latin1Encoding_t2044_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Latin1Encoding_t2044)/* instance_size */
	, sizeof (Latin1Encoding_t2044)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// Metadata Definition System.Text.StringBuilder
extern TypeInfo StringBuilder_t780_il2cpp_TypeInfo;
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
static const EncodedMethodIndex StringBuilder_t780_VTable[5] = 
{
	626,
	601,
	627,
	4141,
	4142,
};
static const Il2CppType* StringBuilder_t780_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair StringBuilder_t780_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringBuilder_t780_0_0_0;
extern const Il2CppType StringBuilder_t780_1_0_0;
struct StringBuilder_t780;
const Il2CppTypeDefinitionMetadata StringBuilder_t780_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringBuilder_t780_InterfacesTypeInfos/* implementedInterfaces */
	, StringBuilder_t780_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringBuilder_t780_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8926/* fieldStart */
	, 12844/* methodStart */
	, -1/* eventStart */
	, 2549/* propertyStart */

};
TypeInfo StringBuilder_t780_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringBuilder"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &StringBuilder_t780_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3158/* custom_attributes_cache */
	, &StringBuilder_t780_0_0_0/* byval_arg */
	, &StringBuilder_t780_1_0_0/* this_arg */
	, &StringBuilder_t780_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringBuilder_t780)/* instance_size */
	, sizeof (StringBuilder_t780)/* actualSize */
	, 0/* element_size */
	, sizeof(char*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 38/* method_count */
	, 3/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF32Encoding
#include "mscorlib_System_Text_UTF32Encoding.h"
// Metadata Definition System.Text.UTF32Encoding
extern TypeInfo UTF32Encoding_t2046_il2cpp_TypeInfo;
// System.Text.UTF32Encoding
#include "mscorlib_System_Text_UTF32EncodingMethodDeclarations.h"
extern const Il2CppType UTF32Decoder_t2045_0_0_0;
static const Il2CppType* UTF32Encoding_t2046_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UTF32Decoder_t2045_0_0_0,
};
static const EncodedMethodIndex UTF32Encoding_t2046_VTable[25] = 
{
	4143,
	601,
	4144,
	628,
	4145,
	4146,
	4072,
	4147,
	4148,
	4075,
	4076,
	4077,
	4149,
	4150,
	4080,
	4151,
	4152,
	4153,
	4154,
	4155,
	4086,
	4087,
	4088,
	4156,
	4157,
};
static Il2CppInterfaceOffsetPair UTF32Encoding_t2046_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF32Encoding_t2046_0_0_0;
extern const Il2CppType UTF32Encoding_t2046_1_0_0;
struct UTF32Encoding_t2046;
const Il2CppTypeDefinitionMetadata UTF32Encoding_t2046_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UTF32Encoding_t2046_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UTF32Encoding_t2046_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t519_0_0_0/* parent */
	, UTF32Encoding_t2046_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8931/* fieldStart */
	, 12882/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF32Encoding_t2046_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF32Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UTF32Encoding_t2046_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF32Encoding_t2046_0_0_0/* byval_arg */
	, &UTF32Encoding_t2046_1_0_0/* this_arg */
	, &UTF32Encoding_t2046_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF32Encoding_t2046)/* instance_size */
	, sizeof (UTF32Encoding_t2046)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF32Encoding/UTF32Decoder
#include "mscorlib_System_Text_UTF32Encoding_UTF32Decoder.h"
// Metadata Definition System.Text.UTF32Encoding/UTF32Decoder
extern TypeInfo UTF32Decoder_t2045_il2cpp_TypeInfo;
// System.Text.UTF32Encoding/UTF32Decoder
#include "mscorlib_System_Text_UTF32Encoding_UTF32DecoderMethodDeclarations.h"
static const EncodedMethodIndex UTF32Decoder_t2045_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4158,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF32Decoder_t2045_1_0_0;
struct UTF32Decoder_t2045;
const Il2CppTypeDefinitionMetadata UTF32Decoder_t2045_DefinitionMetadata = 
{
	&UTF32Encoding_t2046_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t1695_0_0_0/* parent */
	, UTF32Decoder_t2045_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8933/* fieldStart */
	, 12900/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF32Decoder_t2045_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF32Decoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UTF32Decoder_t2045_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF32Decoder_t2045_0_0_0/* byval_arg */
	, &UTF32Decoder_t2045_1_0_0/* this_arg */
	, &UTF32Decoder_t2045_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF32Decoder_t2045)/* instance_size */
	, sizeof (UTF32Decoder_t2045)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.UTF7Encoding
#include "mscorlib_System_Text_UTF7Encoding.h"
// Metadata Definition System.Text.UTF7Encoding
extern TypeInfo UTF7Encoding_t2049_il2cpp_TypeInfo;
// System.Text.UTF7Encoding
#include "mscorlib_System_Text_UTF7EncodingMethodDeclarations.h"
extern const Il2CppType UTF7Decoder_t2047_0_0_0;
static const Il2CppType* UTF7Encoding_t2049_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UTF7Decoder_t2047_0_0_0,
};
static const EncodedMethodIndex UTF7Encoding_t2049_VTable[25] = 
{
	4159,
	601,
	4160,
	628,
	4161,
	4162,
	4072,
	4163,
	4164,
	4075,
	4076,
	4077,
	4165,
	4166,
	4080,
	4167,
	4168,
	4169,
	4084,
	4170,
	4086,
	4087,
	4088,
	4171,
	4172,
};
static Il2CppInterfaceOffsetPair UTF7Encoding_t2049_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF7Encoding_t2049_0_0_0;
extern const Il2CppType UTF7Encoding_t2049_1_0_0;
struct UTF7Encoding_t2049;
const Il2CppTypeDefinitionMetadata UTF7Encoding_t2049_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UTF7Encoding_t2049_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UTF7Encoding_t2049_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t519_0_0_0/* parent */
	, UTF7Encoding_t2049_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8936/* fieldStart */
	, 12902/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF7Encoding_t2049_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF7Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UTF7Encoding_t2049_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3167/* custom_attributes_cache */
	, &UTF7Encoding_t2049_0_0_0/* byval_arg */
	, &UTF7Encoding_t2049_1_0_0/* this_arg */
	, &UTF7Encoding_t2049_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF7Encoding_t2049)/* instance_size */
	, sizeof (UTF7Encoding_t2049)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(UTF7Encoding_t2049_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF7Encoding/UTF7Decoder
#include "mscorlib_System_Text_UTF7Encoding_UTF7Decoder.h"
// Metadata Definition System.Text.UTF7Encoding/UTF7Decoder
extern TypeInfo UTF7Decoder_t2047_il2cpp_TypeInfo;
// System.Text.UTF7Encoding/UTF7Decoder
#include "mscorlib_System_Text_UTF7Encoding_UTF7DecoderMethodDeclarations.h"
static const EncodedMethodIndex UTF7Decoder_t2047_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4173,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF7Decoder_t2047_1_0_0;
struct UTF7Decoder_t2047;
const Il2CppTypeDefinitionMetadata UTF7Decoder_t2047_DefinitionMetadata = 
{
	&UTF7Encoding_t2049_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t1695_0_0_0/* parent */
	, UTF7Decoder_t2047_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8939/* fieldStart */
	, 12923/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF7Decoder_t2047_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF7Decoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UTF7Decoder_t2047_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF7Decoder_t2047_0_0_0/* byval_arg */
	, &UTF7Decoder_t2047_1_0_0/* this_arg */
	, &UTF7Decoder_t2047_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF7Decoder_t2047)/* instance_size */
	, sizeof (UTF7Decoder_t2047)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.UTF8Encoding
#include "mscorlib_System_Text_UTF8Encoding.h"
// Metadata Definition System.Text.UTF8Encoding
extern TypeInfo UTF8Encoding_t2051_il2cpp_TypeInfo;
// System.Text.UTF8Encoding
#include "mscorlib_System_Text_UTF8EncodingMethodDeclarations.h"
extern const Il2CppType UTF8Decoder_t2050_0_0_0;
static const Il2CppType* UTF8Encoding_t2051_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UTF8Decoder_t2050_0_0_0,
};
static const EncodedMethodIndex UTF8Encoding_t2051_VTable[25] = 
{
	4174,
	601,
	4175,
	628,
	4176,
	4177,
	4072,
	4178,
	4179,
	4075,
	4076,
	4077,
	4180,
	4181,
	4080,
	4182,
	4183,
	4184,
	4185,
	4186,
	4086,
	4087,
	4088,
	4187,
	4188,
};
static Il2CppInterfaceOffsetPair UTF8Encoding_t2051_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF8Encoding_t2051_0_0_0;
extern const Il2CppType UTF8Encoding_t2051_1_0_0;
struct UTF8Encoding_t2051;
const Il2CppTypeDefinitionMetadata UTF8Encoding_t2051_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UTF8Encoding_t2051_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UTF8Encoding_t2051_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t519_0_0_0/* parent */
	, UTF8Encoding_t2051_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8940/* fieldStart */
	, 12925/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF8Encoding_t2051_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF8Encoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UTF8Encoding_t2051_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3175/* custom_attributes_cache */
	, &UTF8Encoding_t2051_0_0_0/* byval_arg */
	, &UTF8Encoding_t2051_1_0_0/* this_arg */
	, &UTF8Encoding_t2051_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF8Encoding_t2051)/* instance_size */
	, sizeof (UTF8Encoding_t2051)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UTF8Encoding/UTF8Decoder
#include "mscorlib_System_Text_UTF8Encoding_UTF8Decoder.h"
// Metadata Definition System.Text.UTF8Encoding/UTF8Decoder
extern TypeInfo UTF8Decoder_t2050_il2cpp_TypeInfo;
// System.Text.UTF8Encoding/UTF8Decoder
#include "mscorlib_System_Text_UTF8Encoding_UTF8DecoderMethodDeclarations.h"
static const EncodedMethodIndex UTF8Decoder_t2050_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4189,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UTF8Decoder_t2050_1_0_0;
struct UTF8Decoder_t2050;
const Il2CppTypeDefinitionMetadata UTF8Decoder_t2050_DefinitionMetadata = 
{
	&UTF8Encoding_t2051_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t1695_0_0_0/* parent */
	, UTF8Decoder_t2050_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8941/* fieldStart */
	, 12953/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UTF8Decoder_t2050_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UTF8Decoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UTF8Decoder_t2050_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UTF8Decoder_t2050_0_0_0/* byval_arg */
	, &UTF8Decoder_t2050_1_0_0/* this_arg */
	, &UTF8Decoder_t2050_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UTF8Decoder_t2050)/* instance_size */
	, sizeof (UTF8Decoder_t2050)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056771/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Text.UnicodeEncoding
#include "mscorlib_System_Text_UnicodeEncoding.h"
// Metadata Definition System.Text.UnicodeEncoding
extern TypeInfo UnicodeEncoding_t2053_il2cpp_TypeInfo;
// System.Text.UnicodeEncoding
#include "mscorlib_System_Text_UnicodeEncodingMethodDeclarations.h"
extern const Il2CppType UnicodeDecoder_t2052_0_0_0;
static const Il2CppType* UnicodeEncoding_t2053_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UnicodeDecoder_t2052_0_0_0,
};
static const EncodedMethodIndex UnicodeEncoding_t2053_VTable[25] = 
{
	4190,
	601,
	4191,
	628,
	4192,
	4193,
	4072,
	4194,
	4195,
	4075,
	4076,
	4077,
	4196,
	4197,
	4080,
	4198,
	4199,
	4200,
	4201,
	4202,
	4086,
	4087,
	4088,
	4203,
	4204,
};
static Il2CppInterfaceOffsetPair UnicodeEncoding_t2053_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnicodeEncoding_t2053_0_0_0;
extern const Il2CppType UnicodeEncoding_t2053_1_0_0;
struct UnicodeEncoding_t2053;
const Il2CppTypeDefinitionMetadata UnicodeEncoding_t2053_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UnicodeEncoding_t2053_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnicodeEncoding_t2053_InterfacesOffsets/* interfaceOffsets */
	, &Encoding_t519_0_0_0/* parent */
	, UnicodeEncoding_t2053_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8943/* fieldStart */
	, 12955/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnicodeEncoding_t2053_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnicodeEncoding"/* name */
	, "System.Text"/* namespaze */
	, NULL/* methods */
	, &UnicodeEncoding_t2053_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3179/* custom_attributes_cache */
	, &UnicodeEncoding_t2053_0_0_0/* byval_arg */
	, &UnicodeEncoding_t2053_1_0_0/* this_arg */
	, &UnicodeEncoding_t2053_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnicodeEncoding_t2053)/* instance_size */
	, sizeof (UnicodeEncoding_t2053)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 25/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Text.UnicodeEncoding/UnicodeDecoder
#include "mscorlib_System_Text_UnicodeEncoding_UnicodeDecoder.h"
// Metadata Definition System.Text.UnicodeEncoding/UnicodeDecoder
extern TypeInfo UnicodeDecoder_t2052_il2cpp_TypeInfo;
// System.Text.UnicodeEncoding/UnicodeDecoder
#include "mscorlib_System_Text_UnicodeEncoding_UnicodeDecoderMethodDeclarations.h"
static const EncodedMethodIndex UnicodeDecoder_t2052_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4205,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnicodeDecoder_t2052_1_0_0;
struct UnicodeDecoder_t2052;
const Il2CppTypeDefinitionMetadata UnicodeDecoder_t2052_DefinitionMetadata = 
{
	&UnicodeEncoding_t2053_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Decoder_t1695_0_0_0/* parent */
	, UnicodeDecoder_t2052_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8945/* fieldStart */
	, 12976/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnicodeDecoder_t2052_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnicodeDecoder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &UnicodeDecoder_t2052_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnicodeDecoder_t2052_0_0_0/* byval_arg */
	, &UnicodeDecoder_t2052_1_0_0/* this_arg */
	, &UnicodeDecoder_t2052_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnicodeDecoder_t2052)/* instance_size */
	, sizeof (UnicodeDecoder_t2052)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.CompressedStack
#include "mscorlib_System_Threading_CompressedStack.h"
// Metadata Definition System.Threading.CompressedStack
extern TypeInfo CompressedStack_t2015_il2cpp_TypeInfo;
// System.Threading.CompressedStack
#include "mscorlib_System_Threading_CompressedStackMethodDeclarations.h"
static const EncodedMethodIndex CompressedStack_t2015_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4206,
};
static const Il2CppType* CompressedStack_t2015_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair CompressedStack_t2015_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompressedStack_t2015_0_0_0;
extern const Il2CppType CompressedStack_t2015_1_0_0;
struct CompressedStack_t2015;
const Il2CppTypeDefinitionMetadata CompressedStack_t2015_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CompressedStack_t2015_InterfacesTypeInfos/* implementedInterfaces */
	, CompressedStack_t2015_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CompressedStack_t2015_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8947/* fieldStart */
	, 12978/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompressedStack_t2015_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompressedStack"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &CompressedStack_t2015_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CompressedStack_t2015_0_0_0/* byval_arg */
	, &CompressedStack_t2015_1_0_0/* this_arg */
	, &CompressedStack_t2015_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompressedStack_t2015)/* instance_size */
	, sizeof (CompressedStack_t2015)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.EventResetMode
#include "mscorlib_System_Threading_EventResetMode.h"
// Metadata Definition System.Threading.EventResetMode
extern TypeInfo EventResetMode_t2054_il2cpp_TypeInfo;
// System.Threading.EventResetMode
#include "mscorlib_System_Threading_EventResetModeMethodDeclarations.h"
static const EncodedMethodIndex EventResetMode_t2054_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EventResetMode_t2054_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventResetMode_t2054_0_0_0;
extern const Il2CppType EventResetMode_t2054_1_0_0;
const Il2CppTypeDefinitionMetadata EventResetMode_t2054_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventResetMode_t2054_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EventResetMode_t2054_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8948/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventResetMode_t2054_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventResetMode"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3185/* custom_attributes_cache */
	, &EventResetMode_t2054_0_0_0/* byval_arg */
	, &EventResetMode_t2054_1_0_0/* this_arg */
	, &EventResetMode_t2054_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventResetMode_t2054)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventResetMode_t2054)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandle.h"
// Metadata Definition System.Threading.EventWaitHandle
extern TypeInfo EventWaitHandle_t2055_il2cpp_TypeInfo;
// System.Threading.EventWaitHandle
#include "mscorlib_System_Threading_EventWaitHandleMethodDeclarations.h"
static const EncodedMethodIndex EventWaitHandle_t2055_VTable[10] = 
{
	626,
	4207,
	627,
	628,
	4208,
	4209,
	4210,
	4211,
	4212,
	4213,
};
static Il2CppInterfaceOffsetPair EventWaitHandle_t2055_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventWaitHandle_t2055_0_0_0;
extern const Il2CppType EventWaitHandle_t2055_1_0_0;
extern const Il2CppType WaitHandle_t1335_0_0_0;
struct EventWaitHandle_t2055;
const Il2CppTypeDefinitionMetadata EventWaitHandle_t2055_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventWaitHandle_t2055_InterfacesOffsets/* interfaceOffsets */
	, &WaitHandle_t1335_0_0_0/* parent */
	, EventWaitHandle_t2055_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12984/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventWaitHandle_t2055_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventWaitHandle"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &EventWaitHandle_t2055_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3186/* custom_attributes_cache */
	, &EventWaitHandle_t2055_0_0_0/* byval_arg */
	, &EventWaitHandle_t2055_1_0_0/* this_arg */
	, &EventWaitHandle_t2055_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventWaitHandle_t2055)/* instance_size */
	, sizeof (EventWaitHandle_t2055)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.ExecutionContext
#include "mscorlib_System_Threading_ExecutionContext.h"
// Metadata Definition System.Threading.ExecutionContext
extern TypeInfo ExecutionContext_t1866_il2cpp_TypeInfo;
// System.Threading.ExecutionContext
#include "mscorlib_System_Threading_ExecutionContextMethodDeclarations.h"
static const EncodedMethodIndex ExecutionContext_t1866_VTable[5] = 
{
	626,
	601,
	627,
	628,
	4214,
};
static const Il2CppType* ExecutionContext_t1866_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair ExecutionContext_t1866_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExecutionContext_t1866_0_0_0;
extern const Il2CppType ExecutionContext_t1866_1_0_0;
struct ExecutionContext_t1866;
const Il2CppTypeDefinitionMetadata ExecutionContext_t1866_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ExecutionContext_t1866_InterfacesTypeInfos/* implementedInterfaces */
	, ExecutionContext_t1866_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ExecutionContext_t1866_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8951/* fieldStart */
	, 12988/* methodStart */
	, -1/* eventStart */
	, 2552/* propertyStart */

};
TypeInfo ExecutionContext_t1866_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecutionContext"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ExecutionContext_t1866_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecutionContext_t1866_0_0_0/* byval_arg */
	, &ExecutionContext_t1866_1_0_0/* this_arg */
	, &ExecutionContext_t1866_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecutionContext_t1866)/* instance_size */
	, sizeof (ExecutionContext_t1866)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Interlocked
#include "mscorlib_System_Threading_Interlocked.h"
// Metadata Definition System.Threading.Interlocked
extern TypeInfo Interlocked_t2056_il2cpp_TypeInfo;
// System.Threading.Interlocked
#include "mscorlib_System_Threading_InterlockedMethodDeclarations.h"
static const EncodedMethodIndex Interlocked_t2056_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Interlocked_t2056_0_0_0;
extern const Il2CppType Interlocked_t2056_1_0_0;
struct Interlocked_t2056;
const Il2CppTypeDefinitionMetadata Interlocked_t2056_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Interlocked_t2056_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12997/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Interlocked_t2056_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Interlocked"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Interlocked_t2056_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Interlocked_t2056_0_0_0/* byval_arg */
	, &Interlocked_t2056_1_0_0/* this_arg */
	, &Interlocked_t2056_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Interlocked_t2056)/* instance_size */
	, sizeof (Interlocked_t2056)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEvent.h"
// Metadata Definition System.Threading.ManualResetEvent
extern TypeInfo ManualResetEvent_t1283_il2cpp_TypeInfo;
// System.Threading.ManualResetEvent
#include "mscorlib_System_Threading_ManualResetEventMethodDeclarations.h"
static const EncodedMethodIndex ManualResetEvent_t1283_VTable[10] = 
{
	626,
	4207,
	627,
	628,
	4208,
	4209,
	4210,
	4211,
	4212,
	4213,
};
static Il2CppInterfaceOffsetPair ManualResetEvent_t1283_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ManualResetEvent_t1283_0_0_0;
extern const Il2CppType ManualResetEvent_t1283_1_0_0;
struct ManualResetEvent_t1283;
const Il2CppTypeDefinitionMetadata ManualResetEvent_t1283_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ManualResetEvent_t1283_InterfacesOffsets/* interfaceOffsets */
	, &EventWaitHandle_t2055_0_0_0/* parent */
	, ManualResetEvent_t1283_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12998/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ManualResetEvent_t1283_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ManualResetEvent"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &ManualResetEvent_t1283_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3190/* custom_attributes_cache */
	, &ManualResetEvent_t1283_0_0_0/* byval_arg */
	, &ManualResetEvent_t1283_1_0_0/* this_arg */
	, &ManualResetEvent_t1283_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ManualResetEvent_t1283)/* instance_size */
	, sizeof (ManualResetEvent_t1283)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.Monitor
#include "mscorlib_System_Threading_Monitor.h"
// Metadata Definition System.Threading.Monitor
extern TypeInfo Monitor_t2057_il2cpp_TypeInfo;
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
static const EncodedMethodIndex Monitor_t2057_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Monitor_t2057_0_0_0;
extern const Il2CppType Monitor_t2057_1_0_0;
struct Monitor_t2057;
const Il2CppTypeDefinitionMetadata Monitor_t2057_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Monitor_t2057_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 12999/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Monitor_t2057_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Monitor"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Monitor_t2057_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3191/* custom_attributes_cache */
	, &Monitor_t2057_0_0_0/* byval_arg */
	, &Monitor_t2057_1_0_0/* this_arg */
	, &Monitor_t2057_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Monitor_t2057)/* instance_size */
	, sizeof (Monitor_t2057)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.Mutex
#include "mscorlib_System_Threading_Mutex.h"
// Metadata Definition System.Threading.Mutex
extern TypeInfo Mutex_t1858_il2cpp_TypeInfo;
// System.Threading.Mutex
#include "mscorlib_System_Threading_MutexMethodDeclarations.h"
static const EncodedMethodIndex Mutex_t1858_VTable[10] = 
{
	626,
	4207,
	627,
	628,
	4208,
	4209,
	4210,
	4211,
	4212,
	4213,
};
static Il2CppInterfaceOffsetPair Mutex_t1858_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Mutex_t1858_0_0_0;
extern const Il2CppType Mutex_t1858_1_0_0;
struct Mutex_t1858;
const Il2CppTypeDefinitionMetadata Mutex_t1858_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Mutex_t1858_InterfacesOffsets/* interfaceOffsets */
	, &WaitHandle_t1335_0_0_0/* parent */
	, Mutex_t1858_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13006/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Mutex_t1858_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mutex"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &Mutex_t1858_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3193/* custom_attributes_cache */
	, &Mutex_t1858_0_0_0/* byval_arg */
	, &Mutex_t1858_1_0_0/* this_arg */
	, &Mutex_t1858_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mutex_t1858)/* instance_size */
	, sizeof (Mutex_t1858)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Threading.NativeEventCalls
#include "mscorlib_System_Threading_NativeEventCalls.h"
// Metadata Definition System.Threading.NativeEventCalls
extern TypeInfo NativeEventCalls_t2058_il2cpp_TypeInfo;
// System.Threading.NativeEventCalls
#include "mscorlib_System_Threading_NativeEventCallsMethodDeclarations.h"
static const EncodedMethodIndex NativeEventCalls_t2058_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NativeEventCalls_t2058_0_0_0;
extern const Il2CppType NativeEventCalls_t2058_1_0_0;
struct NativeEventCalls_t2058;
const Il2CppTypeDefinitionMetadata NativeEventCalls_t2058_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NativeEventCalls_t2058_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13010/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NativeEventCalls_t2058_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NativeEventCalls"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &NativeEventCalls_t2058_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NativeEventCalls_t2058_0_0_0/* byval_arg */
	, &NativeEventCalls_t2058_1_0_0/* this_arg */
	, &NativeEventCalls_t2058_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NativeEventCalls_t2058)/* instance_size */
	, sizeof (NativeEventCalls_t2058)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Threading.SynchronizationLockException
#include "mscorlib_System_Threading_SynchronizationLockException.h"
// Metadata Definition System.Threading.SynchronizationLockException
extern TypeInfo SynchronizationLockException_t2059_il2cpp_TypeInfo;
// System.Threading.SynchronizationLockException
#include "mscorlib_System_Threading_SynchronizationLockExceptionMethodDeclarations.h"
static const EncodedMethodIndex SynchronizationLockException_t2059_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair SynchronizationLockException_t2059_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SynchronizationLockException_t2059_0_0_0;
extern const Il2CppType SynchronizationLockException_t2059_1_0_0;
struct SynchronizationLockException_t2059;
const Il2CppTypeDefinitionMetadata SynchronizationLockException_t2059_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SynchronizationLockException_t2059_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, SynchronizationLockException_t2059_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 13014/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SynchronizationLockException_t2059_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SynchronizationLockException"/* name */
	, "System.Threading"/* namespaze */
	, NULL/* methods */
	, &SynchronizationLockException_t2059_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3196/* custom_attributes_cache */
	, &SynchronizationLockException_t2059_0_0_0/* byval_arg */
	, &SynchronizationLockException_t2059_1_0_0/* this_arg */
	, &SynchronizationLockException_t2059_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SynchronizationLockException_t2059)/* instance_size */
	, sizeof (SynchronizationLockException_t2059)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
