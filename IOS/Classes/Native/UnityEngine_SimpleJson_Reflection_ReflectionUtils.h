﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t470;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t1002  : public Object_t
{
};
struct ReflectionUtils_t1002_StaticFields{
	// System.Object[] SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t470* ___EmptyObjects_0;
};
