﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_FishEye
struct CameraFilterPack_Distortion_FishEye_t85;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_FishEye::.ctor()
extern "C" void CameraFilterPack_Distortion_FishEye__ctor_m531 (CameraFilterPack_Distortion_FishEye_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::.cctor()
extern "C" void CameraFilterPack_Distortion_FishEye__cctor_m532 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_FishEye::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_FishEye_get_material_m533 (CameraFilterPack_Distortion_FishEye_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::Start()
extern "C" void CameraFilterPack_Distortion_FishEye_Start_m534 (CameraFilterPack_Distortion_FishEye_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_FishEye_OnRenderImage_m535 (CameraFilterPack_Distortion_FishEye_t85 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::OnValidate()
extern "C" void CameraFilterPack_Distortion_FishEye_OnValidate_m536 (CameraFilterPack_Distortion_FishEye_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::Update()
extern "C" void CameraFilterPack_Distortion_FishEye_Update_m537 (CameraFilterPack_Distortion_FishEye_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_FishEye::OnDisable()
extern "C" void CameraFilterPack_Distortion_FishEye_OnDisable_m538 (CameraFilterPack_Distortion_FishEye_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
