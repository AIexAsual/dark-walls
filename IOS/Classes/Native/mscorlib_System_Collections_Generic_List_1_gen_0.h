﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t2247;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct  List_1_t510  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::_items
	RaycastResultU5BU5D_t2247* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::_version
	int32_t ____version_3;
};
struct List_1_t510_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::EmptyArray
	RaycastResultU5BU5D_t2247* ___EmptyArray_4;
};
