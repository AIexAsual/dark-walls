﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$20
struct U24ArrayTypeU2420_t2156;
struct U24ArrayTypeU2420_t2156_marshaled;

void U24ArrayTypeU2420_t2156_marshal(const U24ArrayTypeU2420_t2156& unmarshaled, U24ArrayTypeU2420_t2156_marshaled& marshaled);
void U24ArrayTypeU2420_t2156_marshal_back(const U24ArrayTypeU2420_t2156_marshaled& marshaled, U24ArrayTypeU2420_t2156& unmarshaled);
void U24ArrayTypeU2420_t2156_marshal_cleanup(U24ArrayTypeU2420_t2156_marshaled& marshaled);
