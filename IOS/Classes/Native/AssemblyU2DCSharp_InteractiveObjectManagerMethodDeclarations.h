﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveObjectManager
struct InteractiveObjectManager_t366;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void InteractiveObjectManager::.ctor()
extern "C" void InteractiveObjectManager__ctor_m2112 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::.cctor()
extern "C" void InteractiveObjectManager__cctor_m2113 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// InteractiveObjectManager InteractiveObjectManager::get_Instance()
extern "C" InteractiveObjectManager_t366 * InteractiveObjectManager_get_Instance_m2114 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::Start()
extern "C" void InteractiveObjectManager_Start_m2115 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::Update()
extern "C" void InteractiveObjectManager_Update_m2116 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::GoHere(System.Single,System.Boolean)
extern "C" void InteractiveObjectManager_GoHere_m2117 (InteractiveObjectManager_t366 * __this, float ___time, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::DropObject(System.String)
extern "C" void InteractiveObjectManager_DropObject_m2118 (InteractiveObjectManager_t366 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::RadioChange(System.Int32)
extern "C" void InteractiveObjectManager_RadioChange_m2119 (InteractiveObjectManager_t366 * __this, int32_t ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::SetRagdoll()
extern "C" void InteractiveObjectManager_SetRagdoll_m2120 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::MamaAction(System.String)
extern "C" void InteractiveObjectManager_MamaAction_m2121 (InteractiveObjectManager_t366 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::MamaGoto(System.String)
extern "C" void InteractiveObjectManager_MamaGoto_m2122 (InteractiveObjectManager_t366 * __this, String_t* ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::MoveMama(System.Single,UnityEngine.Vector3)
extern "C" void InteractiveObjectManager_MoveMama_m2123 (InteractiveObjectManager_t366 * __this, float ___delay, Vector3_t215  ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::MamaLookAt(UnityEngine.Transform)
extern "C" void InteractiveObjectManager_MamaLookAt_m2124 (InteractiveObjectManager_t366 * __this, Transform_t243 * ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::MamaScream()
extern "C" void InteractiveObjectManager_MamaScream_m2125 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::EnableMama(System.Single,System.Boolean)
extern "C" void InteractiveObjectManager_EnableMama_m2126 (InteractiveObjectManager_t366 * __this, float ___delay, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::HideMama(System.Single,System.Boolean)
extern "C" void InteractiveObjectManager_HideMama_m2127 (InteractiveObjectManager_t366 * __this, float ___delay, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::LookAtMeMom()
extern "C" void InteractiveObjectManager_LookAtMeMom_m2128 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::TextFade()
extern "C" void InteractiveObjectManager_TextFade_m2129 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::HideBaby(System.Boolean)
extern "C" void InteractiveObjectManager_HideBaby_m2130 (InteractiveObjectManager_t366 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::BabyWalk()
extern "C" void InteractiveObjectManager_BabyWalk_m2131 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::MoveBaby(UnityEngine.Vector3)
extern "C" void InteractiveObjectManager_MoveBaby_m2132 (InteractiveObjectManager_t366 * __this, Vector3_t215  ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::BabyScream()
extern "C" void InteractiveObjectManager_BabyScream_m2133 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::EnableBaby(System.Boolean)
extern "C" void InteractiveObjectManager_EnableBaby_m2134 (InteractiveObjectManager_t366 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::OpenFaucet(System.Boolean)
extern "C" void InteractiveObjectManager_OpenFaucet_m2135 (InteractiveObjectManager_t366 * __this, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::TextEffects(System.String,System.Single,System.Single)
extern "C" void InteractiveObjectManager_TextEffects_m2136 (InteractiveObjectManager_t366 * __this, String_t* ___name, float ___val, float ___delay, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::TextEnable(System.String,System.Boolean)
extern "C" void InteractiveObjectManager_TextEnable_m2137 (InteractiveObjectManager_t366 * __this, String_t* ___name, bool ___val, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::Wiggle()
extern "C" void InteractiveObjectManager_Wiggle_m2138 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveObjectManager::StopWiggle()
extern "C" void InteractiveObjectManager_StopWiggle_m2139 (InteractiveObjectManager_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
