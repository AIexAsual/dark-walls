﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_t1565;

// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
extern "C" void DebuggerHiddenAttribute__ctor_m9359 (DebuggerHiddenAttribute_t1565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
