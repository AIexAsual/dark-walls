﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RagdollHelper
struct RagdollHelper_t402;

// System.Void RagdollHelper::.ctor()
extern "C" void RagdollHelper__ctor_m2299 (RagdollHelper_t402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RagdollHelper::get_ragdolled()
extern "C" bool RagdollHelper_get_ragdolled_m2300 (RagdollHelper_t402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollHelper::set_ragdolled(System.Boolean)
extern "C" void RagdollHelper_set_ragdolled_m2301 (RagdollHelper_t402 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollHelper::setKinematic(System.Boolean)
extern "C" void RagdollHelper_setKinematic_m2302 (RagdollHelper_t402 * __this, bool ___newValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollHelper::Start()
extern "C" void RagdollHelper_Start_m2303 (RagdollHelper_t402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollHelper::Update()
extern "C" void RagdollHelper_Update_m2304 (RagdollHelper_t402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RagdollHelper::LateUpdate()
extern "C" void RagdollHelper_LateUpdate_m2305 (RagdollHelper_t402 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
