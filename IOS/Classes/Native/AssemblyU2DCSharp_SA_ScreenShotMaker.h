﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t302;
// SA_Singleton`1<SA_ScreenShotMaker>
#include "AssemblyU2DCSharp_SA_Singleton_1_gen.h"
// SA_ScreenShotMaker
struct  SA_ScreenShotMaker_t300  : public SA_Singleton_1_t303
{
	// System.Action`1<UnityEngine.Texture2D> SA_ScreenShotMaker::OnScreenshotReady
	Action_1_t302 * ___OnScreenshotReady_4;
};
