﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t468;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t525;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t2972;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1093;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
struct ReadOnlyCollection_1_t2240;
// System.String[]
struct StringU5BU5D_t398;
// System.Predicate`1<System.String>
struct Predicate_1_t2241;
// System.Comparison`1<System.String>
struct Comparison_1_t2243;
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m2831(__this, method) (( void (*) (List_1_t468 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m13489(__this, ___collection, method) (( void (*) (List_1_t468 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Int32)
#define List_1__ctor_m13491(__this, ___capacity, method) (( void (*) (List_1_t468 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.String>::.cctor()
#define List_1__cctor_m13493(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.String>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13495(__this, method) (( Object_t* (*) (List_1_t468 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m13497(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t468 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13499(__this, method) (( Object_t * (*) (List_1_t468 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m13501(__this, ___item, method) (( int32_t (*) (List_1_t468 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m13503(__this, ___item, method) (( bool (*) (List_1_t468 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m13505(__this, ___item, method) (( int32_t (*) (List_1_t468 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m13507(__this, ___index, ___item, method) (( void (*) (List_1_t468 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m13509(__this, ___item, method) (( void (*) (List_1_t468 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13511(__this, method) (( bool (*) (List_1_t468 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13513(__this, method) (( bool (*) (List_1_t468 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m13515(__this, method) (( Object_t * (*) (List_1_t468 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m13517(__this, method) (( bool (*) (List_1_t468 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m13519(__this, method) (( bool (*) (List_1_t468 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m13521(__this, ___index, method) (( Object_t * (*) (List_1_t468 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m13523(__this, ___index, ___value, method) (( void (*) (List_1_t468 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
#define List_1_Add_m13525(__this, ___item, method) (( void (*) (List_1_t468 *, String_t*, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m13527(__this, ___newCount, method) (( void (*) (List_1_t468 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m13529(__this, ___collection, method) (( void (*) (List_1_t468 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m13531(__this, ___enumerable, method) (( void (*) (List_1_t468 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m13533(__this, ___collection, method) (( void (*) (List_1_t468 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.String>::AsReadOnly()
#define List_1_AsReadOnly_m13535(__this, method) (( ReadOnlyCollection_1_t2240 * (*) (List_1_t468 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
#define List_1_Clear_m13537(__this, method) (( void (*) (List_1_t468 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(T)
#define List_1_Contains_m13539(__this, ___item, method) (( bool (*) (List_1_t468 *, String_t*, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m13541(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t468 *, StringU5BU5D_t398*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.String>::Find(System.Predicate`1<T>)
#define List_1_Find_m13543(__this, ___match, method) (( String_t* (*) (List_1_t468 *, Predicate_1_t2241 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m13545(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2241 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m13547(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t468 *, int32_t, int32_t, Predicate_1_t2241 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.String>::GetEnumerator()
#define List_1_GetEnumerator_m13549(__this, method) (( Enumerator_t2242  (*) (List_1_t468 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(T)
#define List_1_IndexOf_m13551(__this, ___item, method) (( int32_t (*) (List_1_t468 *, String_t*, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m13553(__this, ___start, ___delta, method) (( void (*) (List_1_t468 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m13555(__this, ___index, method) (( void (*) (List_1_t468 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Insert(System.Int32,T)
#define List_1_Insert_m13557(__this, ___index, ___item, method) (( void (*) (List_1_t468 *, int32_t, String_t*, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m13559(__this, ___collection, method) (( void (*) (List_1_t468 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Remove(T)
#define List_1_Remove_m13561(__this, ___item, method) (( bool (*) (List_1_t468 *, String_t*, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m13563(__this, ___match, method) (( int32_t (*) (List_1_t468 *, Predicate_1_t2241 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m13565(__this, ___index, method) (( void (*) (List_1_t468 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Reverse()
#define List_1_Reverse_m13567(__this, method) (( void (*) (List_1_t468 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Sort()
#define List_1_Sort_m13569(__this, method) (( void (*) (List_1_t468 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m13571(__this, ___comparison, method) (( void (*) (List_1_t468 *, Comparison_1_t2243 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.String>::ToArray()
#define List_1_ToArray_m2832(__this, method) (( StringU5BU5D_t398* (*) (List_1_t468 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::TrimExcess()
#define List_1_TrimExcess_m13574(__this, method) (( void (*) (List_1_t468 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Capacity()
#define List_1_get_Capacity_m13576(__this, method) (( int32_t (*) (List_1_t468 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m13578(__this, ___value, method) (( void (*) (List_1_t468 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m13580(__this, method) (( int32_t (*) (List_1_t468 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m13582(__this, ___index, method) (( String_t* (*) (List_1_t468 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Item(System.Int32,T)
#define List_1_set_Item_m13584(__this, ___index, ___value, method) (( void (*) (List_1_t468 *, int32_t, String_t*, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
