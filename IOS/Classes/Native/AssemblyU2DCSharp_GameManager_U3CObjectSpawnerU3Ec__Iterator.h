﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t256;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// GameManager/<ObjectSpawner>c__IteratorD
struct  U3CObjectSpawnerU3Ec__IteratorD_t352  : public Object_t
{
	// System.String GameManager/<ObjectSpawner>c__IteratorD::name
	String_t* ___name_0;
	// UnityEngine.GameObject GameManager/<ObjectSpawner>c__IteratorD::<instance>__0
	GameObject_t256 * ___U3CinstanceU3E__0_1;
	// UnityEngine.GameObject GameManager/<ObjectSpawner>c__IteratorD::go
	GameObject_t256 * ___go_2;
	// System.Int32 GameManager/<ObjectSpawner>c__IteratorD::$PC
	int32_t ___U24PC_3;
	// System.Object GameManager/<ObjectSpawner>c__IteratorD::$current
	Object_t * ___U24current_4;
	// System.String GameManager/<ObjectSpawner>c__IteratorD::<$>name
	String_t* ___U3CU24U3Ename_5;
	// UnityEngine.GameObject GameManager/<ObjectSpawner>c__IteratorD::<$>go
	GameObject_t256 * ___U3CU24U3Ego_6;
};
