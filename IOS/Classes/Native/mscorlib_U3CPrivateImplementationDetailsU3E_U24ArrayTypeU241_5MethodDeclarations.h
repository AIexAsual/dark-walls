﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$128
struct U24ArrayTypeU24128_t2170;
struct U24ArrayTypeU24128_t2170_marshaled;

void U24ArrayTypeU24128_t2170_marshal(const U24ArrayTypeU24128_t2170& unmarshaled, U24ArrayTypeU24128_t2170_marshaled& marshaled);
void U24ArrayTypeU24128_t2170_marshal_back(const U24ArrayTypeU24128_t2170_marshaled& marshaled, U24ArrayTypeU24128_t2170& unmarshaled);
void U24ArrayTypeU24128_t2170_marshal_cleanup(U24ArrayTypeU24128_t2170_marshaled& marshaled);
