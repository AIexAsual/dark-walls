﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Enumerator_t2751;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2742;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20929_gshared (Enumerator_t2751 * __this, Dictionary_2_t2742 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m20929(__this, ___host, method) (( void (*) (Enumerator_t2751 *, Dictionary_2_t2742 *, const MethodInfo*))Enumerator__ctor_m20929_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20930_gshared (Enumerator_t2751 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20930(__this, method) (( Object_t * (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20930_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m20931_gshared (Enumerator_t2751 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20931(__this, method) (( void (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_Dispose_m20931_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20932_gshared (Enumerator_t2751 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20932(__this, method) (( bool (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_MoveNext_m20932_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m20933_gshared (Enumerator_t2751 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20933(__this, method) (( Object_t * (*) (Enumerator_t2751 *, const MethodInfo*))Enumerator_get_Current_m20933_gshared)(__this, method)
