﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_PinLight
struct CameraFilterPack_Blend2Camera_PinLight_t40;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_PinLight::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_PinLight__ctor_m226 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_PinLight::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_PinLight_get_material_m227 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::Start()
extern "C" void CameraFilterPack_Blend2Camera_PinLight_Start_m228 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_PinLight_OnRenderImage_m229 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_PinLight_OnValidate_m230 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::Update()
extern "C" void CameraFilterPack_Blend2Camera_PinLight_Update_m231 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_PinLight_OnEnable_m232 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_PinLight::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_PinLight_OnDisable_m233 (CameraFilterPack_Blend2Camera_PinLight_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
