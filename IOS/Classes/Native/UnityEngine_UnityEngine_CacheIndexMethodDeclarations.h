﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t910;
struct CacheIndex_t910_marshaled;

void CacheIndex_t910_marshal(const CacheIndex_t910& unmarshaled, CacheIndex_t910_marshaled& marshaled);
void CacheIndex_t910_marshal_back(const CacheIndex_t910_marshaled& marshaled, CacheIndex_t910& unmarshaled);
void CacheIndex_t910_marshal_cleanup(CacheIndex_t910_marshaled& marshaled);
