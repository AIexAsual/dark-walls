﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// InteractiveShadowMan
struct InteractiveShadowMan_t341;

// System.Void InteractiveShadowMan::.ctor()
extern "C" void InteractiveShadowMan__ctor_m1993 (InteractiveShadowMan_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveShadowMan::Awake()
extern "C" void InteractiveShadowMan_Awake_m1994 (InteractiveShadowMan_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveShadowMan::SetGazedAt(System.Boolean)
extern "C" void InteractiveShadowMan_SetGazedAt_m1995 (InteractiveShadowMan_t341 * __this, bool ___gazeAt, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InteractiveShadowMan::Update()
extern "C" void InteractiveShadowMan_Update_m1996 (InteractiveShadowMan_t341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
