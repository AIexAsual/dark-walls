﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SA_Singleton`1<SA_ScreenShotMaker>
struct SA_Singleton_1_t303;
// SA_ScreenShotMaker
struct SA_ScreenShotMaker_t300;

// System.Void SA_Singleton`1<SA_ScreenShotMaker>::.ctor()
// SA_Singleton`1<System.Object>
#include "AssemblyU2DCSharp_SA_Singleton_1_gen_0MethodDeclarations.h"
#define SA_Singleton_1__ctor_m3103(__this, method) (( void (*) (SA_Singleton_1_t303 *, const MethodInfo*))SA_Singleton_1__ctor_m14485_gshared)(__this, method)
// System.Void SA_Singleton`1<SA_ScreenShotMaker>::.cctor()
#define SA_Singleton_1__cctor_m14486(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1__cctor_m14487_gshared)(__this /* static, unused */, method)
// T SA_Singleton`1<SA_ScreenShotMaker>::get_instance()
#define SA_Singleton_1_get_instance_m14488(__this /* static, unused */, method) (( SA_ScreenShotMaker_t300 * (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_instance_m14489_gshared)(__this /* static, unused */, method)
// T SA_Singleton`1<SA_ScreenShotMaker>::get_Instance()
#define SA_Singleton_1_get_Instance_m14490(__this /* static, unused */, method) (( SA_ScreenShotMaker_t300 * (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_Instance_m14491_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton`1<SA_ScreenShotMaker>::get_HasInstance()
#define SA_Singleton_1_get_HasInstance_m14492(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_HasInstance_m14493_gshared)(__this /* static, unused */, method)
// System.Boolean SA_Singleton`1<SA_ScreenShotMaker>::get_IsDestroyed()
#define SA_Singleton_1_get_IsDestroyed_m14494(__this /* static, unused */, method) (( bool (*) (Object_t * /* static, unused */, const MethodInfo*))SA_Singleton_1_get_IsDestroyed_m14495_gshared)(__this /* static, unused */, method)
// System.Void SA_Singleton`1<SA_ScreenShotMaker>::OnDestroy()
#define SA_Singleton_1_OnDestroy_m14496(__this, method) (( void (*) (SA_Singleton_1_t303 *, const MethodInfo*))SA_Singleton_1_OnDestroy_m14497_gshared)(__this, method)
// System.Void SA_Singleton`1<SA_ScreenShotMaker>::OnApplicationQuit()
#define SA_Singleton_1_OnApplicationQuit_m14498(__this, method) (( void (*) (SA_Singleton_1_t303 *, const MethodInfo*))SA_Singleton_1_OnApplicationQuit_m14499_gshared)(__this, method)
