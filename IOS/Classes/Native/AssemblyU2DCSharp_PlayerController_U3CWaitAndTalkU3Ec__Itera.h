﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// PlayerController
struct PlayerController_t356;
// System.Object
#include "mscorlib_System_Object.h"
// PlayerController/<WaitAndTalk>c__Iterator14
struct  U3CWaitAndTalkU3Ec__Iterator14_t394  : public Object_t
{
	// System.Single PlayerController/<WaitAndTalk>c__Iterator14::time
	float ___time_0;
	// System.Int32 PlayerController/<WaitAndTalk>c__Iterator14::val
	int32_t ___val_1;
	// System.Int32 PlayerController/<WaitAndTalk>c__Iterator14::$PC
	int32_t ___U24PC_2;
	// System.Object PlayerController/<WaitAndTalk>c__Iterator14::$current
	Object_t * ___U24current_3;
	// System.Single PlayerController/<WaitAndTalk>c__Iterator14::<$>time
	float ___U3CU24U3Etime_4;
	// System.Int32 PlayerController/<WaitAndTalk>c__Iterator14::<$>val
	int32_t ___U3CU24U3Eval_5;
	// PlayerController PlayerController/<WaitAndTalk>c__Iterator14::<>f__this
	PlayerController_t356 * ___U3CU3Ef__this_6;
};
