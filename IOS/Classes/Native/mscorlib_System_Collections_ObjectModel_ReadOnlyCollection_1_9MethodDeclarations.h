﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>
struct ReadOnlyCollection_1_t2412;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<System.Boolean>
struct IList_1_t563;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Boolean[]
struct BooleanU5BU5D_t448;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t3028;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m15839_gshared (ReadOnlyCollection_1_t2412 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m15839(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2412 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15839_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15840_gshared (ReadOnlyCollection_1_t2412 * __this, bool ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15840(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2412 *, bool, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15840_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15841_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15841(__this, method) (( void (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15841_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15842_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, bool ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15842(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2412 *, int32_t, bool, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15842_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15843_gshared (ReadOnlyCollection_1_t2412 * __this, bool ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15843(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, bool, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15843_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15844_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15844(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2412 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15844_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15845_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15845(__this, ___index, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15845_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15846_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, bool ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15846(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2412 *, int32_t, bool, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15846_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15847_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15847(__this, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15847_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15848_gshared (ReadOnlyCollection_1_t2412 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15848(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2412 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15848_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15849_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15849(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15849_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m15850_gshared (ReadOnlyCollection_1_t2412 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m15850(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2412 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15850_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m15851_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m15851(__this, method) (( void (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15851_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m15852_gshared (ReadOnlyCollection_1_t2412 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m15852(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15852_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15853_gshared (ReadOnlyCollection_1_t2412 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15853(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2412 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15853_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m15854_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m15854(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2412 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15854_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m15855_gshared (ReadOnlyCollection_1_t2412 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m15855(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2412 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15855_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15856_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15856(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2412 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15856_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15857_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15857(__this, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15858_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15858(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15858_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15859_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15859(__this, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15859_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15860_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15860(__this, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15860_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m15861_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m15861(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2412 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15861_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m15862_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m15862(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2412 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15862_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m15863_gshared (ReadOnlyCollection_1_t2412 * __this, bool ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m15863(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, bool, const MethodInfo*))ReadOnlyCollection_1_Contains_m15863_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m15864_gshared (ReadOnlyCollection_1_t2412 * __this, BooleanU5BU5D_t448* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m15864(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2412 *, BooleanU5BU5D_t448*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15864_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m15865_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m15865(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15865_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m15866_gshared (ReadOnlyCollection_1_t2412 * __this, bool ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m15866(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2412 *, bool, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15866_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m15867_gshared (ReadOnlyCollection_1_t2412 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m15867(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2412 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15867_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Boolean>::get_Item(System.Int32)
extern "C" bool ReadOnlyCollection_1_get_Item_m15868_gshared (ReadOnlyCollection_1_t2412 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m15868(__this, ___index, method) (( bool (*) (ReadOnlyCollection_1_t2412 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15868_gshared)(__this, ___index, method)
