﻿#pragma once
#include <stdint.h>
// CardboardEye
struct CardboardEye_t240;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<CardboardEye,System.Boolean>
struct  Func_2_t514  : public MulticastDelegate_t219
{
};
