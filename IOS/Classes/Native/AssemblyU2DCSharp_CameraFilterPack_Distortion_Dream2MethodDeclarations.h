﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Dream2
struct CameraFilterPack_Distortion_Dream2_t84;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Dream2::.ctor()
extern "C" void CameraFilterPack_Distortion_Dream2__ctor_m524 (CameraFilterPack_Distortion_Dream2_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Dream2::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Dream2_get_material_m525 (CameraFilterPack_Distortion_Dream2_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::Start()
extern "C" void CameraFilterPack_Distortion_Dream2_Start_m526 (CameraFilterPack_Distortion_Dream2_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Dream2_OnRenderImage_m527 (CameraFilterPack_Distortion_Dream2_t84 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::OnValidate()
extern "C" void CameraFilterPack_Distortion_Dream2_OnValidate_m528 (CameraFilterPack_Distortion_Dream2_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::Update()
extern "C" void CameraFilterPack_Distortion_Dream2_Update_m529 (CameraFilterPack_Distortion_Dream2_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Dream2::OnDisable()
extern "C" void CameraFilterPack_Distortion_Dream2_OnDisable_m530 (CameraFilterPack_Distortion_Dream2_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
