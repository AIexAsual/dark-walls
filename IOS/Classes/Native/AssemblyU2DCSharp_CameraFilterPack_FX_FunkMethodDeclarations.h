﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Funk
struct CameraFilterPack_FX_Funk_t127;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Funk::.ctor()
extern "C" void CameraFilterPack_FX_Funk__ctor_m820 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Funk::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Funk_get_material_m821 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::Start()
extern "C" void CameraFilterPack_FX_Funk_Start_m822 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Funk_OnRenderImage_m823 (CameraFilterPack_FX_Funk_t127 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::Update()
extern "C" void CameraFilterPack_FX_Funk_Update_m824 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Funk::OnDisable()
extern "C" void CameraFilterPack_FX_Funk_OnDisable_m825 (CameraFilterPack_FX_Funk_t127 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
