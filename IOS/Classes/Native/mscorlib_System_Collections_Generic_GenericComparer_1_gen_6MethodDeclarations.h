﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Boolean>
struct GenericComparer_1_t2419;

// System.Void System.Collections.Generic.GenericComparer`1<System.Boolean>::.ctor()
extern "C" void GenericComparer_1__ctor_m15924_gshared (GenericComparer_1_t2419 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m15924(__this, method) (( void (*) (GenericComparer_1_t2419 *, const MethodInfo*))GenericComparer_1__ctor_m15924_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Boolean>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m15925_gshared (GenericComparer_1_t2419 * __this, bool ___x, bool ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m15925(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2419 *, bool, bool, const MethodInfo*))GenericComparer_1_Compare_m15925_gshared)(__this, ___x, ___y, method)
