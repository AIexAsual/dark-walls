﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct KeyValuePair_2_t2800;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21581(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2800 *, String_t*, KeyValuePair_2_t1147 , const MethodInfo*))KeyValuePair_2__ctor_m21045_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m21582(__this, method) (( String_t* (*) (KeyValuePair_2_t2800 *, const MethodInfo*))KeyValuePair_2_get_Key_m21046_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21583(__this, ___value, method) (( void (*) (KeyValuePair_2_t2800 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m21047_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m21584(__this, method) (( KeyValuePair_2_t1147  (*) (KeyValuePair_2_t2800 *, const MethodInfo*))KeyValuePair_2_get_Value_m21048_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21585(__this, ___value, method) (( void (*) (KeyValuePair_2_t2800 *, KeyValuePair_2_t1147 , const MethodInfo*))KeyValuePair_2_set_Value_m21049_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m21586(__this, method) (( String_t* (*) (KeyValuePair_2_t2800 *, const MethodInfo*))KeyValuePair_2_ToString_m21050_gshared)(__this, method)
