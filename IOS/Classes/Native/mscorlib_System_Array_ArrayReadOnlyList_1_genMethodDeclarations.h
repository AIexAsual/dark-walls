﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2913;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t470;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2280;
// System.Exception
struct Exception_t520;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m22708_gshared (ArrayReadOnlyList_1_t2913 * __this, ObjectU5BU5D_t470* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m22708(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t2913 *, ObjectU5BU5D_t470*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m22708_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22709_gshared (ArrayReadOnlyList_1_t2913 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22709(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2913 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m22709_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m22710_gshared (ArrayReadOnlyList_1_t2913 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m22710(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2913 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m22710_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m22711_gshared (ArrayReadOnlyList_1_t2913 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m22711(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t2913 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m22711_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m22712_gshared (ArrayReadOnlyList_1_t2913 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m22712(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t2913 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m22712_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m22713_gshared (ArrayReadOnlyList_1_t2913 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m22713(__this, method) (( bool (*) (ArrayReadOnlyList_1_t2913 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m22713_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m22714_gshared (ArrayReadOnlyList_1_t2913 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m22714(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2913 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m22714_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m22715_gshared (ArrayReadOnlyList_1_t2913 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m22715(__this, method) (( void (*) (ArrayReadOnlyList_1_t2913 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m22715_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m22716_gshared (ArrayReadOnlyList_1_t2913 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m22716(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2913 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m22716_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m22717_gshared (ArrayReadOnlyList_1_t2913 * __this, ObjectU5BU5D_t470* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m22717(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2913 *, ObjectU5BU5D_t470*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m22717_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m22718_gshared (ArrayReadOnlyList_1_t2913 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m22718(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t2913 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m22718_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m22719_gshared (ArrayReadOnlyList_1_t2913 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m22719(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t2913 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m22719_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m22720_gshared (ArrayReadOnlyList_1_t2913 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m22720(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2913 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m22720_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m22721_gshared (ArrayReadOnlyList_1_t2913 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m22721(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2913 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m22721_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m22722_gshared (ArrayReadOnlyList_1_t2913 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m22722(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2913 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m22722_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t520 * ArrayReadOnlyList_1_ReadOnlyError_m22723_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m22723(__this /* static, unused */, method) (( Exception_t520 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m22723_gshared)(__this /* static, unused */, method)
