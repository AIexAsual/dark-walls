﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Oculus_NightVision2
struct CameraFilterPack_Oculus_NightVision2_t163;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Oculus_NightVision2::.ctor()
extern "C" void CameraFilterPack_Oculus_NightVision2__ctor_m1049 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Oculus_NightVision2::get_material()
extern "C" Material_t2 * CameraFilterPack_Oculus_NightVision2_get_material_m1050 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::ChangeFilters()
extern "C" void CameraFilterPack_Oculus_NightVision2_ChangeFilters_m1051 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::Start()
extern "C" void CameraFilterPack_Oculus_NightVision2_Start_m1052 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Oculus_NightVision2_OnRenderImage_m1053 (CameraFilterPack_Oculus_NightVision2_t163 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::OnValidate()
extern "C" void CameraFilterPack_Oculus_NightVision2_OnValidate_m1054 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::Update()
extern "C" void CameraFilterPack_Oculus_NightVision2_Update_m1055 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Oculus_NightVision2::OnDisable()
extern "C" void CameraFilterPack_Oculus_NightVision2_OnDisable_m1056 (CameraFilterPack_Oculus_NightVision2_t163 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
