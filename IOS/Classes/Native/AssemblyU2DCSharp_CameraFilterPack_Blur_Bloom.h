﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blur_Bloom
struct  CameraFilterPack_Blur_Bloom_t47  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Blur_Bloom::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Blur_Bloom::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Blur_Bloom::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Blur_Bloom::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Blur_Bloom::Amount
	float ___Amount_6;
	// System.Single CameraFilterPack_Blur_Bloom::Glow
	float ___Glow_7;
};
struct CameraFilterPack_Blur_Bloom_t47_StaticFields{
	// System.Single CameraFilterPack_Blur_Bloom::ChangeAmount
	float ___ChangeAmount_8;
	// System.Single CameraFilterPack_Blur_Bloom::ChangeGlow
	float ___ChangeGlow_9;
};
