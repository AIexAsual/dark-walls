﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Boolean>
struct DefaultComparer_t2420;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Boolean>::.ctor()
extern "C" void DefaultComparer__ctor_m15926_gshared (DefaultComparer_t2420 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15926(__this, method) (( void (*) (DefaultComparer_t2420 *, const MethodInfo*))DefaultComparer__ctor_m15926_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Boolean>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m15927_gshared (DefaultComparer_t2420 * __this, bool ___x, bool ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m15927(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2420 *, bool, bool, const MethodInfo*))DefaultComparer_Compare_m15927_gshared)(__this, ___x, ___y, method)
