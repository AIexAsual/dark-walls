﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CoroutineScript/<testCor>c__Iterator5
struct U3CtestCorU3Ec__Iterator5_t309;
// System.Object
struct Object_t;

// System.Void CoroutineScript/<testCor>c__Iterator5::.ctor()
extern "C" void U3CtestCorU3Ec__Iterator5__ctor_m1863 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CoroutineScript/<testCor>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CtestCorU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1864 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CoroutineScript/<testCor>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CtestCorU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1865 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CoroutineScript/<testCor>c__Iterator5::MoveNext()
extern "C" bool U3CtestCorU3Ec__Iterator5_MoveNext_m1866 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineScript/<testCor>c__Iterator5::Dispose()
extern "C" void U3CtestCorU3Ec__Iterator5_Dispose_m1867 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoroutineScript/<testCor>c__Iterator5::Reset()
extern "C" void U3CtestCorU3Ec__Iterator5_Reset_m1868 (U3CtestCorU3Ec__Iterator5_t309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
