﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CardboardPreRender
struct  CardboardPreRender_t247  : public MonoBehaviour_t4
{
	// UnityEngine.Camera CardboardPreRender::<camera>k__BackingField
	Camera_t14 * ___U3CcameraU3Ek__BackingField_2;
};
