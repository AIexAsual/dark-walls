﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Func`2<System.String,System.Char>
struct Func_2_t528;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Func`2<System.String,System.Char>::.ctor(System.Object,System.IntPtr)
// System.Func`2<System.Object,System.Char>
#include "System_Core_System_Func_2_gen_7MethodDeclarations.h"
#define Func_2__ctor_m3097(__this, ___object, ___method, method) (( void (*) (Func_2_t528 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m14322_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<System.String,System.Char>::Invoke(T)
#define Func_2_Invoke_m14323(__this, ___arg1, method) (( uint16_t (*) (Func_2_t528 *, String_t*, const MethodInfo*))Func_2_Invoke_m14324_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<System.String,System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m14325(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t528 *, String_t*, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m14326_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<System.String,System.Char>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m14327(__this, ___result, method) (( uint16_t (*) (Func_2_t528 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m14328_gshared)(__this, ___result, method)
