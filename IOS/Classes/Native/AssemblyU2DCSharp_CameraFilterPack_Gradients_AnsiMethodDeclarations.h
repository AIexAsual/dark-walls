﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Gradients_Ansi
struct CameraFilterPack_Gradients_Ansi_t145;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Gradients_Ansi::.ctor()
extern "C" void CameraFilterPack_Gradients_Ansi__ctor_m941 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Gradients_Ansi::get_material()
extern "C" Material_t2 * CameraFilterPack_Gradients_Ansi_get_material_m942 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::Start()
extern "C" void CameraFilterPack_Gradients_Ansi_Start_m943 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Gradients_Ansi_OnRenderImage_m944 (CameraFilterPack_Gradients_Ansi_t145 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::Update()
extern "C" void CameraFilterPack_Gradients_Ansi_Update_m945 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Gradients_Ansi::OnDisable()
extern "C" void CameraFilterPack_Gradients_Ansi_OnDisable_m946 (CameraFilterPack_Gradients_Ansi_t145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
