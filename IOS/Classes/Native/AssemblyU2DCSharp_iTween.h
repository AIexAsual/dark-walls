﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t436;
// UnityEngine.GameObject
struct GameObject_t256;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t348;
// iTween/EasingFunction
struct EasingFunction_t430;
// iTween/ApplyTween
struct ApplyTween_t431;
// UnityEngine.AudioSource
struct AudioSource_t339;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t317;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t437;
// UnityEngine.Color[,]
struct ColorU5BU2CU5D_t438;
// System.Single[]
struct SingleU5BU5D_t72;
// UnityEngine.Rect[]
struct RectU5BU5D_t439;
// iTween/CRSpline
struct CRSpline_t429;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t440;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// iTween/EaseType
#include "AssemblyU2DCSharp_iTween_EaseType.h"
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// iTween/NamedValueColor
#include "AssemblyU2DCSharp_iTween_NamedValueColor.h"
// iTween
struct  iTween_t432  : public MonoBehaviour_t4
{
	// System.String iTween::id
	String_t* ___id_4;
	// System.String iTween::type
	String_t* ___type_5;
	// System.String iTween::method
	String_t* ___method_6;
	// iTween/EaseType iTween::easeType
	int32_t ___easeType_7;
	// System.Single iTween::time
	float ___time_8;
	// System.Single iTween::delay
	float ___delay_9;
	// iTween/LoopType iTween::loopType
	int32_t ___loopType_10;
	// System.Boolean iTween::isRunning
	bool ___isRunning_11;
	// System.Boolean iTween::isPaused
	bool ___isPaused_12;
	// System.String iTween::_name
	String_t* ____name_13;
	// System.Single iTween::runningTime
	float ___runningTime_14;
	// System.Single iTween::percentage
	float ___percentage_15;
	// System.Single iTween::delayStarted
	float ___delayStarted_16;
	// System.Boolean iTween::kinematic
	bool ___kinematic_17;
	// System.Boolean iTween::isLocal
	bool ___isLocal_18;
	// System.Boolean iTween::loop
	bool ___loop_19;
	// System.Boolean iTween::reverse
	bool ___reverse_20;
	// System.Boolean iTween::wasPaused
	bool ___wasPaused_21;
	// System.Boolean iTween::physics
	bool ___physics_22;
	// System.Collections.Hashtable iTween::tweenArguments
	Hashtable_t348 * ___tweenArguments_23;
	// UnityEngine.Space iTween::space
	int32_t ___space_24;
	// iTween/EasingFunction iTween::ease
	EasingFunction_t430 * ___ease_25;
	// iTween/ApplyTween iTween::apply
	ApplyTween_t431 * ___apply_26;
	// UnityEngine.AudioSource iTween::audioSource
	AudioSource_t339 * ___audioSource_27;
	// UnityEngine.Vector3[] iTween::vector3s
	Vector3U5BU5D_t317* ___vector3s_28;
	// UnityEngine.Vector2[] iTween::vector2s
	Vector2U5BU5D_t437* ___vector2s_29;
	// UnityEngine.Color[,] iTween::colors
	ColorU5BU2CU5D_t438* ___colors_30;
	// System.Single[] iTween::floats
	SingleU5BU5D_t72* ___floats_31;
	// UnityEngine.Rect[] iTween::rects
	RectU5BU5D_t439* ___rects_32;
	// iTween/CRSpline iTween::path
	CRSpline_t429 * ___path_33;
	// UnityEngine.Vector3 iTween::preUpdate
	Vector3_t215  ___preUpdate_34;
	// UnityEngine.Vector3 iTween::postUpdate
	Vector3_t215  ___postUpdate_35;
	// iTween/NamedValueColor iTween::namedcolorvalue
	int32_t ___namedcolorvalue_36;
	// System.Single iTween::lastRealTime
	float ___lastRealTime_37;
	// System.Boolean iTween::useRealTime
	bool ___useRealTime_38;
};
struct iTween_t432_StaticFields{
	// System.Collections.ArrayList iTween::tweens
	ArrayList_t436 * ___tweens_2;
	// UnityEngine.GameObject iTween::cameraFade
	GameObject_t256 * ___cameraFade_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map0
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map0_39;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map1
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map1_40;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map2
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map2_41;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map3
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map3_42;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map4
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map4_43;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map5
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map5_44;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map6
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map6_45;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map7
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map7_46;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map8
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map8_47;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$map9
	Dictionary_2_t440 * ___U3CU3Ef__switchU24map9_48;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$mapA
	Dictionary_2_t440 * ___U3CU3Ef__switchU24mapA_49;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$mapB
	Dictionary_2_t440 * ___U3CU3Ef__switchU24mapB_50;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> iTween::<>f__switch$mapC
	Dictionary_2_t440 * ___U3CU3Ef__switchU24mapC_51;
};
