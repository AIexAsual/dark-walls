﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// MovingGameObjects
struct MovingGameObjects_t372;

// System.Void MovingGameObjects::.ctor()
extern "C" void MovingGameObjects__ctor_m2158 (MovingGameObjects_t372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingGameObjects::.cctor()
extern "C" void MovingGameObjects__cctor_m2159 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
