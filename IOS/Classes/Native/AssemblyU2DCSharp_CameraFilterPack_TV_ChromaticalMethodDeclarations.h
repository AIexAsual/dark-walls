﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Chromatical
struct CameraFilterPack_TV_Chromatical_t181;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Chromatical::.ctor()
extern "C" void CameraFilterPack_TV_Chromatical__ctor_m1170 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Chromatical::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Chromatical_get_material_m1171 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::Start()
extern "C" void CameraFilterPack_TV_Chromatical_Start_m1172 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Chromatical_OnRenderImage_m1173 (CameraFilterPack_TV_Chromatical_t181 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::Update()
extern "C" void CameraFilterPack_TV_Chromatical_Update_m1174 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Chromatical::OnDisable()
extern "C" void CameraFilterPack_TV_Chromatical_OnDisable_m1175 (CameraFilterPack_TV_Chromatical_t181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
