﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Sharpen_Sharpen
struct  CameraFilterPack_Sharpen_Sharpen_t173  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Sharpen_Sharpen::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Sharpen_Sharpen::Value
	float ___Value_3;
	// System.Single CameraFilterPack_Sharpen_Sharpen::TimeX
	float ___TimeX_4;
	// UnityEngine.Vector4 CameraFilterPack_Sharpen_Sharpen::ScreenResolution
	Vector4_t5  ___ScreenResolution_5;
	// UnityEngine.Material CameraFilterPack_Sharpen_Sharpen::SCMaterial
	Material_t2 * ___SCMaterial_6;
};
struct CameraFilterPack_Sharpen_Sharpen_t173_StaticFields{
	// System.Single CameraFilterPack_Sharpen_Sharpen::ChangeValue
	float ___ChangeValue_7;
};
