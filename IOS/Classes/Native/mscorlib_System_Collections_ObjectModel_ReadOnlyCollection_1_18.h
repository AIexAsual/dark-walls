﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<ArrayIndexes>
struct IList_1_t575;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<ArrayIndexes>
struct  ReadOnlyCollection_1_t2479  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<ArrayIndexes>::list
	Object_t* ___list_0;
};
