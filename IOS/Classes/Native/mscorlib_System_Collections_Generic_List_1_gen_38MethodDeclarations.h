﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>
struct List_1_t975;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.MatchDesc
struct MatchDesc_t974;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Networking.Match.MatchDesc>
struct IEnumerable_1_t3195;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Networking.Match.MatchDesc>
struct IEnumerator_1_t3196;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.Match.MatchDesc>
struct ICollection_1_t3197;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Networking.Match.MatchDesc>
struct ReadOnlyCollection_1_t2733;
// UnityEngine.Networking.Match.MatchDesc[]
struct MatchDescU5BU5D_t2731;
// System.Predicate`1<UnityEngine.Networking.Match.MatchDesc>
struct Predicate_1_t2734;
// System.Comparison`1<UnityEngine.Networking.Match.MatchDesc>
struct Comparison_1_t2736;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Networking.Match.MatchDesc>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_40.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m20671(__this, method) (( void (*) (List_1_t975 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20672(__this, ___collection, method) (( void (*) (List_1_t975 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::.ctor(System.Int32)
#define List_1__ctor_m20673(__this, ___capacity, method) (( void (*) (List_1_t975 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::.cctor()
#define List_1__cctor_m20674(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20675(__this, method) (( Object_t* (*) (List_1_t975 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20676(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t975 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20677(__this, method) (( Object_t * (*) (List_1_t975 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20678(__this, ___item, method) (( int32_t (*) (List_1_t975 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20679(__this, ___item, method) (( bool (*) (List_1_t975 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20680(__this, ___item, method) (( int32_t (*) (List_1_t975 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20681(__this, ___index, ___item, method) (( void (*) (List_1_t975 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20682(__this, ___item, method) (( void (*) (List_1_t975 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20683(__this, method) (( bool (*) (List_1_t975 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20684(__this, method) (( bool (*) (List_1_t975 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20685(__this, method) (( Object_t * (*) (List_1_t975 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20686(__this, method) (( bool (*) (List_1_t975 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20687(__this, method) (( bool (*) (List_1_t975 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20688(__this, ___index, method) (( Object_t * (*) (List_1_t975 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20689(__this, ___index, ___value, method) (( void (*) (List_1_t975 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Add(T)
#define List_1_Add_m20690(__this, ___item, method) (( void (*) (List_1_t975 *, MatchDesc_t974 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20691(__this, ___newCount, method) (( void (*) (List_1_t975 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20692(__this, ___collection, method) (( void (*) (List_1_t975 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20693(__this, ___enumerable, method) (( void (*) (List_1_t975 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20694(__this, ___collection, method) (( void (*) (List_1_t975 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::AsReadOnly()
#define List_1_AsReadOnly_m20695(__this, method) (( ReadOnlyCollection_1_t2733 * (*) (List_1_t975 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Clear()
#define List_1_Clear_m20696(__this, method) (( void (*) (List_1_t975 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Contains(T)
#define List_1_Contains_m20697(__this, ___item, method) (( bool (*) (List_1_t975 *, MatchDesc_t974 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20698(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t975 *, MatchDescU5BU5D_t2731*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Find(System.Predicate`1<T>)
#define List_1_Find_m20699(__this, ___match, method) (( MatchDesc_t974 * (*) (List_1_t975 *, Predicate_1_t2734 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20700(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2734 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20701(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t975 *, int32_t, int32_t, Predicate_1_t2734 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::GetEnumerator()
#define List_1_GetEnumerator_m20702(__this, method) (( Enumerator_t2735  (*) (List_1_t975 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::IndexOf(T)
#define List_1_IndexOf_m20703(__this, ___item, method) (( int32_t (*) (List_1_t975 *, MatchDesc_t974 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20704(__this, ___start, ___delta, method) (( void (*) (List_1_t975 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20705(__this, ___index, method) (( void (*) (List_1_t975 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Insert(System.Int32,T)
#define List_1_Insert_m20706(__this, ___index, ___item, method) (( void (*) (List_1_t975 *, int32_t, MatchDesc_t974 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20707(__this, ___collection, method) (( void (*) (List_1_t975 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Remove(T)
#define List_1_Remove_m20708(__this, ___item, method) (( bool (*) (List_1_t975 *, MatchDesc_t974 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20709(__this, ___match, method) (( int32_t (*) (List_1_t975 *, Predicate_1_t2734 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20710(__this, ___index, method) (( void (*) (List_1_t975 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Reverse()
#define List_1_Reverse_m20711(__this, method) (( void (*) (List_1_t975 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Sort()
#define List_1_Sort_m20712(__this, method) (( void (*) (List_1_t975 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20713(__this, ___comparison, method) (( void (*) (List_1_t975 *, Comparison_1_t2736 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::ToArray()
#define List_1_ToArray_m20714(__this, method) (( MatchDescU5BU5D_t2731* (*) (List_1_t975 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::TrimExcess()
#define List_1_TrimExcess_m20715(__this, method) (( void (*) (List_1_t975 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::get_Capacity()
#define List_1_get_Capacity_m20716(__this, method) (( int32_t (*) (List_1_t975 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20717(__this, ___value, method) (( void (*) (List_1_t975 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::get_Count()
#define List_1_get_Count_m20718(__this, method) (( int32_t (*) (List_1_t975 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::get_Item(System.Int32)
#define List_1_get_Item_m20719(__this, ___index, method) (( MatchDesc_t974 * (*) (List_1_t975 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::set_Item(System.Int32,T)
#define List_1_set_Item_m20720(__this, ___index, ___value, method) (( void (*) (List_1_t975 *, int32_t, MatchDesc_t974 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
