﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_Difference
struct CameraFilterPack_Blend2Camera_Difference_t23;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_Difference::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_Difference__ctor_m98 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_Difference::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_Difference_get_material_m99 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::Start()
extern "C" void CameraFilterPack_Blend2Camera_Difference_Start_m100 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_Difference_OnRenderImage_m101 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_Difference_OnValidate_m102 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::Update()
extern "C" void CameraFilterPack_Blend2Camera_Difference_Update_m103 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_Difference_OnEnable_m104 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_Difference::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_Difference_OnDisable_m105 (CameraFilterPack_Blend2Camera_Difference_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
