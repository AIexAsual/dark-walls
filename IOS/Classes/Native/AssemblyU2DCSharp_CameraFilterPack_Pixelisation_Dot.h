﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Pixelisation_Dot
struct  CameraFilterPack_Pixelisation_Dot_t170  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Pixelisation_Dot::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Pixelisation_Dot::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Pixelisation_Dot::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Pixelisation_Dot::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Pixelisation_Dot::Size
	float ___Size_6;
	// System.Single CameraFilterPack_Pixelisation_Dot::LightBackGround
	float ___LightBackGround_7;
	// System.Single CameraFilterPack_Pixelisation_Dot::Speed
	float ___Speed_8;
	// System.Single CameraFilterPack_Pixelisation_Dot::Size2
	float ___Size2_9;
};
struct CameraFilterPack_Pixelisation_Dot_t170_StaticFields{
	// System.Single CameraFilterPack_Pixelisation_Dot::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_Pixelisation_Dot::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_Pixelisation_Dot::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_Pixelisation_Dot::ChangeValue4
	float ___ChangeValue4_13;
};
