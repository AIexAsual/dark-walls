﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Single>
struct EqualityComparer_1_t2403;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Single>
struct  EqualityComparer_1_t2403  : public Object_t
{
};
struct EqualityComparer_1_t2403_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Single>::_default
	EqualityComparer_1_t2403 * ____default_0;
};
