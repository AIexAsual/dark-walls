﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t2573;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t654;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.UI.Graphic>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
#define Predicate_1__ctor_m18438(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2573 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m13671_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Graphic>::Invoke(T)
#define Predicate_1_Invoke_m18439(__this, ___obj, method) (( bool (*) (Predicate_1_t2573 *, Graphic_t654 *, const MethodInfo*))Predicate_1_Invoke_m13672_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UI.Graphic>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m18440(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2573 *, Graphic_t654 *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m13673_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Graphic>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m18441(__this, ___result, method) (( bool (*) (Predicate_1_t2573 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m13674_gshared)(__this, ___result, method)
