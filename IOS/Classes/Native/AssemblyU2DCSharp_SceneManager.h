﻿#pragma once
#include <stdint.h>
// SceneManager
struct SceneManager_t381;
// SceneLoader
struct SceneLoader_t382;
// SceneManager/OnStateChangeHandler
struct OnStateChangeHandler_t380;
// System.Object
#include "mscorlib_System_Object.h"
// SceneEvent
#include "AssemblyU2DCSharp_SceneEvent.h"
// SceneManager
struct  SceneManager_t381  : public Object_t
{
	// SceneEvent SceneManager::sceneState
	int32_t ___sceneState_1;
	// SceneLoader SceneManager::sceneLoader
	SceneLoader_t382 * ___sceneLoader_2;
	// SceneManager/OnStateChangeHandler SceneManager::OnStateChangeEvent
	OnStateChangeHandler_t380 * ___OnStateChangeEvent_3;
};
struct SceneManager_t381_StaticFields{
	// SceneManager SceneManager::_instance
	SceneManager_t381 * ____instance_0;
};
