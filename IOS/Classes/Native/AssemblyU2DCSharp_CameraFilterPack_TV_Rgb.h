﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_TV_Rgb
struct  CameraFilterPack_TV_Rgb_t192  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_Rgb::SCShader
	Shader_t1 * ___SCShader_2;
	// UnityEngine.Vector4 CameraFilterPack_TV_Rgb::ScreenResolution
	Vector4_t5  ___ScreenResolution_3;
	// System.Single CameraFilterPack_TV_Rgb::TimeX
	float ___TimeX_4;
	// System.Single CameraFilterPack_TV_Rgb::Distortion
	float ___Distortion_5;
	// UnityEngine.Material CameraFilterPack_TV_Rgb::SCMaterial
	Material_t2 * ___SCMaterial_6;
};
