﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t2138;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct  TimeZone_t2138  : public Object_t
{
};
struct TimeZone_t2138_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t2138 * ___currentTimeZone_0;
};
