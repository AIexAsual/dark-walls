﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// <Module>
#include "Mono_Security_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1196_il2cpp_TypeInfo;
// <Module>
#include "Mono_Security_U3CModuleU3EMethodDeclarations.h"
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U3CModuleU3E_t1196_0_0_0;
extern const Il2CppType U3CModuleU3E_t1196_1_0_0;
struct U3CModuleU3E_t1196;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1196_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CModuleU3E_t1196_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CModuleU3E_t1196_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1196_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1196_1_0_0/* this_arg */
	, &U3CModuleU3E_t1196_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1196)/* instance_size */
	, sizeof (U3CModuleU3E_t1196)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Locale
#include "Mono_Security_Locale.h"
// Metadata Definition Locale
extern TypeInfo Locale_t1197_il2cpp_TypeInfo;
// Locale
#include "Mono_Security_LocaleMethodDeclarations.h"
static const EncodedMethodIndex Locale_t1197_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType Locale_t1197_0_0_0;
extern const Il2CppType Locale_t1197_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct Locale_t1197;
const Il2CppTypeDefinitionMetadata Locale_t1197_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Locale_t1197_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6218/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Locale_t1197_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "Locale"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Locale_t1197_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Locale_t1197_0_0_0/* byval_arg */
	, &Locale_t1197_1_0_0/* this_arg */
	, &Locale_t1197_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Locale_t1197)/* instance_size */
	, sizeof (Locale_t1197)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger
#include "Mono_Security_Mono_Math_BigInteger.h"
// Metadata Definition Mono.Math.BigInteger
extern TypeInfo BigInteger_t1199_il2cpp_TypeInfo;
// Mono.Math.BigInteger
#include "Mono_Security_Mono_Math_BigIntegerMethodDeclarations.h"
extern const Il2CppType Sign_t1198_0_0_0;
extern const Il2CppType ModulusRing_t1200_0_0_0;
extern const Il2CppType Kernel_t1201_0_0_0;
static const Il2CppType* BigInteger_t1199_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Sign_t1198_0_0_0,
	&ModulusRing_t1200_0_0_0,
	&Kernel_t1201_0_0_0,
};
static const EncodedMethodIndex BigInteger_t1199_VTable[4] = 
{
	1668,
	601,
	1669,
	1670,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType BigInteger_t1199_0_0_0;
extern const Il2CppType BigInteger_t1199_1_0_0;
struct BigInteger_t1199;
const Il2CppTypeDefinitionMetadata BigInteger_t1199_DefinitionMetadata = 
{
	NULL/* declaringType */
	, BigInteger_t1199_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BigInteger_t1199_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5088/* fieldStart */
	, 6219/* methodStart */
	, -1/* eventStart */
	, 1210/* propertyStart */

};
TypeInfo BigInteger_t1199_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "BigInteger"/* name */
	, "Mono.Math"/* namespaze */
	, NULL/* methods */
	, &BigInteger_t1199_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BigInteger_t1199_0_0_0/* byval_arg */
	, &BigInteger_t1199_1_0_0/* this_arg */
	, &BigInteger_t1199_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BigInteger_t1199)/* instance_size */
	, sizeof (BigInteger_t1199)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(BigInteger_t1199_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 44/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
// Metadata Definition Mono.Math.BigInteger/Sign
extern TypeInfo Sign_t1198_il2cpp_TypeInfo;
// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_SignMethodDeclarations.h"
static const EncodedMethodIndex Sign_t1198_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair Sign_t1198_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType Sign_t1198_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Sign_t1198_DefinitionMetadata = 
{
	&BigInteger_t1199_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Sign_t1198_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Sign_t1198_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5092/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Sign_t1198_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sign"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sign_t1198_0_0_0/* byval_arg */
	, &Sign_t1198_1_0_0/* this_arg */
	, &Sign_t1198_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sign_t1198)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Sign_t1198)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Math.BigInteger/ModulusRing
#include "Mono_Security_Mono_Math_BigInteger_ModulusRing.h"
// Metadata Definition Mono.Math.BigInteger/ModulusRing
extern TypeInfo ModulusRing_t1200_il2cpp_TypeInfo;
// Mono.Math.BigInteger/ModulusRing
#include "Mono_Security_Mono_Math_BigInteger_ModulusRingMethodDeclarations.h"
static const EncodedMethodIndex ModulusRing_t1200_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ModulusRing_t1200_1_0_0;
struct ModulusRing_t1200;
const Il2CppTypeDefinitionMetadata ModulusRing_t1200_DefinitionMetadata = 
{
	&BigInteger_t1199_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ModulusRing_t1200_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5096/* fieldStart */
	, 6263/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ModulusRing_t1200_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ModulusRing"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ModulusRing_t1200_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ModulusRing_t1200_0_0_0/* byval_arg */
	, &ModulusRing_t1200_1_0_0/* this_arg */
	, &ModulusRing_t1200_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ModulusRing_t1200)/* instance_size */
	, sizeof (ModulusRing_t1200)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048834/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.BigInteger/Kernel
#include "Mono_Security_Mono_Math_BigInteger_Kernel.h"
// Metadata Definition Mono.Math.BigInteger/Kernel
extern TypeInfo Kernel_t1201_il2cpp_TypeInfo;
// Mono.Math.BigInteger/Kernel
#include "Mono_Security_Mono_Math_BigInteger_KernelMethodDeclarations.h"
static const EncodedMethodIndex Kernel_t1201_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType Kernel_t1201_1_0_0;
struct Kernel_t1201;
const Il2CppTypeDefinitionMetadata Kernel_t1201_DefinitionMetadata = 
{
	&BigInteger_t1199_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Kernel_t1201_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6269/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Kernel_t1201_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "Kernel"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Kernel_t1201_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Kernel_t1201_0_0_0/* byval_arg */
	, &Kernel_t1201_1_0_0/* this_arg */
	, &Kernel_t1201_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Kernel_t1201)/* instance_size */
	, sizeof (Kernel_t1201)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
// Metadata Definition Mono.Math.Prime.ConfidenceFactor
extern TypeInfo ConfidenceFactor_t1202_il2cpp_TypeInfo;
// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactorMethodDeclarations.h"
static const EncodedMethodIndex ConfidenceFactor_t1202_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ConfidenceFactor_t1202_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ConfidenceFactor_t1202_0_0_0;
extern const Il2CppType ConfidenceFactor_t1202_1_0_0;
const Il2CppTypeDefinitionMetadata ConfidenceFactor_t1202_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConfidenceFactor_t1202_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ConfidenceFactor_t1202_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5098/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ConfidenceFactor_t1202_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConfidenceFactor"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConfidenceFactor_t1202_0_0_0/* byval_arg */
	, &ConfidenceFactor_t1202_1_0_0/* this_arg */
	, &ConfidenceFactor_t1202_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConfidenceFactor_t1202)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ConfidenceFactor_t1202)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTests
#include "Mono_Security_Mono_Math_Prime_PrimalityTests.h"
// Metadata Definition Mono.Math.Prime.PrimalityTests
extern TypeInfo PrimalityTests_t1203_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTests
#include "Mono_Security_Mono_Math_Prime_PrimalityTestsMethodDeclarations.h"
static const EncodedMethodIndex PrimalityTests_t1203_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrimalityTests_t1203_0_0_0;
extern const Il2CppType PrimalityTests_t1203_1_0_0;
struct PrimalityTests_t1203;
const Il2CppTypeDefinitionMetadata PrimalityTests_t1203_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimalityTests_t1203_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6284/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrimalityTests_t1203_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTests"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &PrimalityTests_t1203_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTests_t1203_0_0_0/* byval_arg */
	, &PrimalityTests_t1203_1_0_0/* this_arg */
	, &PrimalityTests_t1203_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTests_t1203)/* instance_size */
	, sizeof (PrimalityTests_t1203)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGeneratorBase.h"
// Metadata Definition Mono.Math.Prime.Generator.PrimeGeneratorBase
extern TypeInfo PrimeGeneratorBase_t1204_il2cpp_TypeInfo;
// Mono.Math.Prime.Generator.PrimeGeneratorBase
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGeneratorBaseMethodDeclarations.h"
static const EncodedMethodIndex PrimeGeneratorBase_t1204_VTable[8] = 
{
	626,
	601,
	627,
	628,
	1671,
	1672,
	1673,
	0,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrimeGeneratorBase_t1204_0_0_0;
extern const Il2CppType PrimeGeneratorBase_t1204_1_0_0;
struct PrimeGeneratorBase_t1204;
const Il2CppTypeDefinitionMetadata PrimeGeneratorBase_t1204_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimeGeneratorBase_t1204_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6286/* methodStart */
	, -1/* eventStart */
	, 1211/* propertyStart */

};
TypeInfo PrimeGeneratorBase_t1204_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimeGeneratorBase"/* name */
	, "Mono.Math.Prime.Generator"/* namespaze */
	, NULL/* methods */
	, &PrimeGeneratorBase_t1204_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimeGeneratorBase_t1204_0_0_0/* byval_arg */
	, &PrimeGeneratorBase_t1204_1_0_0/* this_arg */
	, &PrimeGeneratorBase_t1204_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimeGeneratorBase_t1204)/* instance_size */
	, sizeof (PrimeGeneratorBase_t1204)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "Mono_Security_Mono_Math_Prime_Generator_SequentialSearchPrim.h"
// Metadata Definition Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
extern TypeInfo SequentialSearchPrimeGeneratorBase_t1205_il2cpp_TypeInfo;
// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
#include "Mono_Security_Mono_Math_Prime_Generator_SequentialSearchPrimMethodDeclarations.h"
static const EncodedMethodIndex SequentialSearchPrimeGeneratorBase_t1205_VTable[11] = 
{
	626,
	601,
	627,
	628,
	1671,
	1672,
	1673,
	1674,
	1675,
	1676,
	1677,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SequentialSearchPrimeGeneratorBase_t1205_0_0_0;
extern const Il2CppType SequentialSearchPrimeGeneratorBase_t1205_1_0_0;
struct SequentialSearchPrimeGeneratorBase_t1205;
const Il2CppTypeDefinitionMetadata SequentialSearchPrimeGeneratorBase_t1205_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &PrimeGeneratorBase_t1204_0_0_0/* parent */
	, SequentialSearchPrimeGeneratorBase_t1205_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6291/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SequentialSearchPrimeGeneratorBase_t1205_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SequentialSearchPrimeGeneratorBase"/* name */
	, "Mono.Math.Prime.Generator"/* namespaze */
	, NULL/* methods */
	, &SequentialSearchPrimeGeneratorBase_t1205_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SequentialSearchPrimeGeneratorBase_t1205_0_0_0/* byval_arg */
	, &SequentialSearchPrimeGeneratorBase_t1205_1_0_0/* this_arg */
	, &SequentialSearchPrimeGeneratorBase_t1205_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SequentialSearchPrimeGeneratorBase_t1205)/* instance_size */
	, sizeof (SequentialSearchPrimeGeneratorBase_t1205)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.ASN1
#include "Mono_Security_Mono_Security_ASN1.h"
// Metadata Definition Mono.Security.ASN1
extern TypeInfo ASN1_t1206_il2cpp_TypeInfo;
// Mono.Security.ASN1
#include "Mono_Security_Mono_Security_ASN1MethodDeclarations.h"
static const EncodedMethodIndex ASN1_t1206_VTable[5] = 
{
	626,
	601,
	627,
	1678,
	1679,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ASN1_t1206_0_0_0;
extern const Il2CppType ASN1_t1206_1_0_0;
struct ASN1_t1206;
const Il2CppTypeDefinitionMetadata ASN1_t1206_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ASN1_t1206_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5105/* fieldStart */
	, 6296/* methodStart */
	, -1/* eventStart */
	, 1214/* propertyStart */

};
TypeInfo ASN1_t1206_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASN1"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &ASN1_t1206_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2322/* custom_attributes_cache */
	, &ASN1_t1206_0_0_0/* byval_arg */
	, &ASN1_t1206_1_0_0/* this_arg */
	, &ASN1_t1206_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASN1_t1206)/* instance_size */
	, sizeof (ASN1_t1206)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.ASN1Convert
#include "Mono_Security_Mono_Security_ASN1Convert.h"
// Metadata Definition Mono.Security.ASN1Convert
extern TypeInfo ASN1Convert_t1207_il2cpp_TypeInfo;
// Mono.Security.ASN1Convert
#include "Mono_Security_Mono_Security_ASN1ConvertMethodDeclarations.h"
static const EncodedMethodIndex ASN1Convert_t1207_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ASN1Convert_t1207_0_0_0;
extern const Il2CppType ASN1Convert_t1207_1_0_0;
struct ASN1Convert_t1207;
const Il2CppTypeDefinitionMetadata ASN1Convert_t1207_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ASN1Convert_t1207_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6313/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ASN1Convert_t1207_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ASN1Convert"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &ASN1Convert_t1207_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ASN1Convert_t1207_0_0_0/* byval_arg */
	, &ASN1Convert_t1207_1_0_0/* this_arg */
	, &ASN1Convert_t1207_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ASN1Convert_t1207)/* instance_size */
	, sizeof (ASN1Convert_t1207)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.BitConverterLE
#include "Mono_Security_Mono_Security_BitConverterLE.h"
// Metadata Definition Mono.Security.BitConverterLE
extern TypeInfo BitConverterLE_t1208_il2cpp_TypeInfo;
// Mono.Security.BitConverterLE
#include "Mono_Security_Mono_Security_BitConverterLEMethodDeclarations.h"
static const EncodedMethodIndex BitConverterLE_t1208_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType BitConverterLE_t1208_0_0_0;
extern const Il2CppType BitConverterLE_t1208_1_0_0;
struct BitConverterLE_t1208;
const Il2CppTypeDefinitionMetadata BitConverterLE_t1208_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BitConverterLE_t1208_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6318/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BitConverterLE_t1208_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "BitConverterLE"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &BitConverterLE_t1208_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BitConverterLE_t1208_0_0_0/* byval_arg */
	, &BitConverterLE_t1208_1_0_0/* this_arg */
	, &BitConverterLE_t1208_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BitConverterLE_t1208)/* instance_size */
	, sizeof (BitConverterLE_t1208)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7
#include "Mono_Security_Mono_Security_PKCS7.h"
// Metadata Definition Mono.Security.PKCS7
extern TypeInfo PKCS7_t1211_il2cpp_TypeInfo;
// Mono.Security.PKCS7
#include "Mono_Security_Mono_Security_PKCS7MethodDeclarations.h"
extern const Il2CppType ContentInfo_t1209_0_0_0;
extern const Il2CppType EncryptedData_t1210_0_0_0;
static const Il2CppType* PKCS7_t1211_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ContentInfo_t1209_0_0_0,
	&EncryptedData_t1210_0_0_0,
};
static const EncodedMethodIndex PKCS7_t1211_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PKCS7_t1211_0_0_0;
extern const Il2CppType PKCS7_t1211_1_0_0;
struct PKCS7_t1211;
const Il2CppTypeDefinitionMetadata PKCS7_t1211_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS7_t1211_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS7_t1211_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS7_t1211_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS7"/* name */
	, "Mono.Security"/* namespaze */
	, NULL/* methods */
	, &PKCS7_t1211_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS7_t1211_0_0_0/* byval_arg */
	, &PKCS7_t1211_1_0_0/* this_arg */
	, &PKCS7_t1211_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS7_t1211)/* instance_size */
	, sizeof (PKCS7_t1211)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7/ContentInfo
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo.h"
// Metadata Definition Mono.Security.PKCS7/ContentInfo
extern TypeInfo ContentInfo_t1209_il2cpp_TypeInfo;
// Mono.Security.PKCS7/ContentInfo
#include "Mono_Security_Mono_Security_PKCS7_ContentInfoMethodDeclarations.h"
static const EncodedMethodIndex ContentInfo_t1209_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ContentInfo_t1209_1_0_0;
struct ContentInfo_t1209;
const Il2CppTypeDefinitionMetadata ContentInfo_t1209_DefinitionMetadata = 
{
	&PKCS7_t1211_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ContentInfo_t1209_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5108/* fieldStart */
	, 6320/* methodStart */
	, -1/* eventStart */
	, 1219/* propertyStart */

};
TypeInfo ContentInfo_t1209_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ContentInfo_t1209_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContentInfo_t1209_0_0_0/* byval_arg */
	, &ContentInfo_t1209_1_0_0/* this_arg */
	, &ContentInfo_t1209_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentInfo_t1209)/* instance_size */
	, sizeof (ContentInfo_t1209)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.PKCS7/EncryptedData
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData.h"
// Metadata Definition Mono.Security.PKCS7/EncryptedData
extern TypeInfo EncryptedData_t1210_il2cpp_TypeInfo;
// Mono.Security.PKCS7/EncryptedData
#include "Mono_Security_Mono_Security_PKCS7_EncryptedDataMethodDeclarations.h"
static const EncodedMethodIndex EncryptedData_t1210_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType EncryptedData_t1210_1_0_0;
struct EncryptedData_t1210;
const Il2CppTypeDefinitionMetadata EncryptedData_t1210_DefinitionMetadata = 
{
	&PKCS7_t1211_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncryptedData_t1210_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5110/* fieldStart */
	, 6327/* methodStart */
	, -1/* eventStart */
	, 1221/* propertyStart */

};
TypeInfo EncryptedData_t1210_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncryptedData"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EncryptedData_t1210_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncryptedData_t1210_0_0_0/* byval_arg */
	, &EncryptedData_t1210_1_0_0/* this_arg */
	, &EncryptedData_t1210_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncryptedData_t1210)/* instance_size */
	, sizeof (EncryptedData_t1210)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.ARC4Managed
#include "Mono_Security_Mono_Security_Cryptography_ARC4Managed.h"
// Metadata Definition Mono.Security.Cryptography.ARC4Managed
extern TypeInfo ARC4Managed_t1212_il2cpp_TypeInfo;
// Mono.Security.Cryptography.ARC4Managed
#include "Mono_Security_Mono_Security_Cryptography_ARC4ManagedMethodDeclarations.h"
static const EncodedMethodIndex ARC4Managed_t1212_VTable[29] = 
{
	626,
	1680,
	627,
	628,
	1630,
	1681,
	1632,
	1633,
	1634,
	1682,
	1683,
	1684,
	1685,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	1686,
	1647,
	1687,
	1688,
	1689,
	1690,
	1691,
	1692,
};
extern const Il2CppType IDisposable_t538_0_0_0;
extern const Il2CppType ICryptoTransform_t1188_0_0_0;
static const Il2CppType* ARC4Managed_t1212_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
	&ICryptoTransform_t1188_0_0_0,
};
static Il2CppInterfaceOffsetPair ARC4Managed_t1212_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 26},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ARC4Managed_t1212_0_0_0;
extern const Il2CppType ARC4Managed_t1212_1_0_0;
extern const Il2CppType RC4_t1213_0_0_0;
struct ARC4Managed_t1212;
const Il2CppTypeDefinitionMetadata ARC4Managed_t1212_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ARC4Managed_t1212_InterfacesTypeInfos/* implementedInterfaces */
	, ARC4Managed_t1212_InterfacesOffsets/* interfaceOffsets */
	, &RC4_t1213_0_0_0/* parent */
	, ARC4Managed_t1212_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5114/* fieldStart */
	, 6331/* methodStart */
	, -1/* eventStart */
	, 1223/* propertyStart */

};
TypeInfo ARC4Managed_t1212_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ARC4Managed"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &ARC4Managed_t1212_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ARC4Managed_t1212_0_0_0/* byval_arg */
	, &ARC4Managed_t1212_1_0_0/* this_arg */
	, &ARC4Managed_t1212_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ARC4Managed_t1212)/* instance_size */
	, sizeof (ARC4Managed_t1212)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.CryptoConvert
#include "Mono_Security_Mono_Security_Cryptography_CryptoConvert.h"
// Metadata Definition Mono.Security.Cryptography.CryptoConvert
extern TypeInfo CryptoConvert_t1214_il2cpp_TypeInfo;
// Mono.Security.Cryptography.CryptoConvert
#include "Mono_Security_Mono_Security_Cryptography_CryptoConvertMethodDeclarations.h"
static const EncodedMethodIndex CryptoConvert_t1214_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CryptoConvert_t1214_0_0_0;
extern const Il2CppType CryptoConvert_t1214_1_0_0;
struct CryptoConvert_t1214;
const Il2CppTypeDefinitionMetadata CryptoConvert_t1214_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CryptoConvert_t1214_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6346/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CryptoConvert_t1214_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CryptoConvert"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &CryptoConvert_t1214_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CryptoConvert_t1214_0_0_0/* byval_arg */
	, &CryptoConvert_t1214_1_0_0/* this_arg */
	, &CryptoConvert_t1214_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CryptoConvert_t1214)/* instance_size */
	, sizeof (CryptoConvert_t1214)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.KeyBuilder
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilder.h"
// Metadata Definition Mono.Security.Cryptography.KeyBuilder
extern TypeInfo KeyBuilder_t1215_il2cpp_TypeInfo;
// Mono.Security.Cryptography.KeyBuilder
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilderMethodDeclarations.h"
static const EncodedMethodIndex KeyBuilder_t1215_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType KeyBuilder_t1215_0_0_0;
extern const Il2CppType KeyBuilder_t1215_1_0_0;
struct KeyBuilder_t1215;
const Il2CppTypeDefinitionMetadata KeyBuilder_t1215_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, KeyBuilder_t1215_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5119/* fieldStart */
	, 6347/* methodStart */
	, -1/* eventStart */
	, 1225/* propertyStart */

};
TypeInfo KeyBuilder_t1215_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyBuilder"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &KeyBuilder_t1215_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyBuilder_t1215_0_0_0/* byval_arg */
	, &KeyBuilder_t1215_1_0_0/* this_arg */
	, &KeyBuilder_t1215_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyBuilder_t1215)/* instance_size */
	, sizeof (KeyBuilder_t1215)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(KeyBuilder_t1215_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.MD2
#include "Mono_Security_Mono_Security_Cryptography_MD2.h"
// Metadata Definition Mono.Security.Cryptography.MD2
extern TypeInfo MD2_t1216_il2cpp_TypeInfo;
// Mono.Security.Cryptography.MD2
#include "Mono_Security_Mono_Security_Cryptography_MD2MethodDeclarations.h"
static const EncodedMethodIndex MD2_t1216_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	0,
	0,
	1698,
	0,
	1699,
};
static Il2CppInterfaceOffsetPair MD2_t1216_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType MD2_t1216_0_0_0;
extern const Il2CppType MD2_t1216_1_0_0;
extern const Il2CppType HashAlgorithm_t1217_0_0_0;
struct MD2_t1216;
const Il2CppTypeDefinitionMetadata MD2_t1216_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MD2_t1216_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, MD2_t1216_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6349/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MD2_t1216_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "MD2"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MD2_t1216_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MD2_t1216_0_0_0/* byval_arg */
	, &MD2_t1216_1_0_0/* this_arg */
	, &MD2_t1216_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MD2_t1216)/* instance_size */
	, sizeof (MD2_t1216)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.MD2Managed
#include "Mono_Security_Mono_Security_Cryptography_MD2Managed.h"
// Metadata Definition Mono.Security.Cryptography.MD2Managed
extern TypeInfo MD2Managed_t1218_il2cpp_TypeInfo;
// Mono.Security.Cryptography.MD2Managed
#include "Mono_Security_Mono_Security_Cryptography_MD2ManagedMethodDeclarations.h"
static const EncodedMethodIndex MD2Managed_t1218_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	1700,
	1701,
	1698,
	1702,
	1699,
};
static Il2CppInterfaceOffsetPair MD2Managed_t1218_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType MD2Managed_t1218_0_0_0;
extern const Il2CppType MD2Managed_t1218_1_0_0;
struct MD2Managed_t1218;
const Il2CppTypeDefinitionMetadata MD2Managed_t1218_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MD2Managed_t1218_InterfacesOffsets/* interfaceOffsets */
	, &MD2_t1216_0_0_0/* parent */
	, MD2Managed_t1218_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5120/* fieldStart */
	, 6352/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MD2Managed_t1218_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "MD2Managed"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MD2Managed_t1218_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MD2Managed_t1218_0_0_0/* byval_arg */
	, &MD2Managed_t1218_1_0_0/* this_arg */
	, &MD2Managed_t1218_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MD2Managed_t1218)/* instance_size */
	, sizeof (MD2Managed_t1218)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MD2Managed_t1218_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS1
#include "Mono_Security_Mono_Security_Cryptography_PKCS1.h"
// Metadata Definition Mono.Security.Cryptography.PKCS1
extern TypeInfo PKCS1_t1219_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS1
#include "Mono_Security_Mono_Security_Cryptography_PKCS1MethodDeclarations.h"
static const EncodedMethodIndex PKCS1_t1219_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PKCS1_t1219_0_0_0;
extern const Il2CppType PKCS1_t1219_1_0_0;
struct PKCS1_t1219;
const Il2CppTypeDefinitionMetadata PKCS1_t1219_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS1_t1219_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5126/* fieldStart */
	, 6359/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS1_t1219_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS1"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &PKCS1_t1219_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS1_t1219_0_0_0/* byval_arg */
	, &PKCS1_t1219_1_0_0/* this_arg */
	, &PKCS1_t1219_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS1_t1219)/* instance_size */
	, sizeof (PKCS1_t1219)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PKCS1_t1219_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8
#include "Mono_Security_Mono_Security_Cryptography_PKCS8.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8
extern TypeInfo PKCS8_t1222_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8
#include "Mono_Security_Mono_Security_Cryptography_PKCS8MethodDeclarations.h"
extern const Il2CppType PrivateKeyInfo_t1220_0_0_0;
extern const Il2CppType EncryptedPrivateKeyInfo_t1221_0_0_0;
static const Il2CppType* PKCS8_t1222_il2cpp_TypeInfo__nestedTypes[2] =
{
	&PrivateKeyInfo_t1220_0_0_0,
	&EncryptedPrivateKeyInfo_t1221_0_0_0,
};
static const EncodedMethodIndex PKCS8_t1222_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PKCS8_t1222_0_0_0;
extern const Il2CppType PKCS8_t1222_1_0_0;
struct PKCS8_t1222;
const Il2CppTypeDefinitionMetadata PKCS8_t1222_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS8_t1222_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS8_t1222_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PKCS8_t1222_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS8"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &PKCS8_t1222_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS8_t1222_0_0_0/* byval_arg */
	, &PKCS8_t1222_1_0_0/* this_arg */
	, &PKCS8_t1222_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS8_t1222)/* instance_size */
	, sizeof (PKCS8_t1222)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_PrivateKeyInf.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
extern TypeInfo PrivateKeyInfo_t1220_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8/PrivateKeyInfo
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_PrivateKeyInfMethodDeclarations.h"
static const EncodedMethodIndex PrivateKeyInfo_t1220_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrivateKeyInfo_t1220_1_0_0;
struct PrivateKeyInfo_t1220;
const Il2CppTypeDefinitionMetadata PrivateKeyInfo_t1220_DefinitionMetadata = 
{
	&PKCS8_t1222_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrivateKeyInfo_t1220_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5130/* fieldStart */
	, 6369/* methodStart */
	, -1/* eventStart */
	, 1226/* propertyStart */

};
TypeInfo PrivateKeyInfo_t1220_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeyInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &PrivateKeyInfo_t1220_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeyInfo_t1220_0_0_0/* byval_arg */
	, &PrivateKeyInfo_t1220_1_0_0/* this_arg */
	, &PrivateKeyInfo_t1220_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeyInfo_t1220)/* instance_size */
	, sizeof (PrivateKeyInfo_t1220)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_EncryptedPriv.h"
// Metadata Definition Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
extern TypeInfo EncryptedPrivateKeyInfo_t1221_il2cpp_TypeInfo;
// Mono.Security.Cryptography.PKCS8/EncryptedPrivateKeyInfo
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_EncryptedPrivMethodDeclarations.h"
static const EncodedMethodIndex EncryptedPrivateKeyInfo_t1221_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType EncryptedPrivateKeyInfo_t1221_1_0_0;
struct EncryptedPrivateKeyInfo_t1221;
const Il2CppTypeDefinitionMetadata EncryptedPrivateKeyInfo_t1221_DefinitionMetadata = 
{
	&PKCS8_t1222_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EncryptedPrivateKeyInfo_t1221_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5134/* fieldStart */
	, 6377/* methodStart */
	, -1/* eventStart */
	, 1227/* propertyStart */

};
TypeInfo EncryptedPrivateKeyInfo_t1221_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "EncryptedPrivateKeyInfo"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &EncryptedPrivateKeyInfo_t1221_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EncryptedPrivateKeyInfo_t1221_0_0_0/* byval_arg */
	, &EncryptedPrivateKeyInfo_t1221_1_0_0/* this_arg */
	, &EncryptedPrivateKeyInfo_t1221_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EncryptedPrivateKeyInfo_t1221)/* instance_size */
	, sizeof (EncryptedPrivateKeyInfo_t1221)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 4/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.RC4
#include "Mono_Security_Mono_Security_Cryptography_RC4.h"
// Metadata Definition Mono.Security.Cryptography.RC4
extern TypeInfo RC4_t1213_il2cpp_TypeInfo;
// Mono.Security.Cryptography.RC4
#include "Mono_Security_Mono_Security_Cryptography_RC4MethodDeclarations.h"
static const EncodedMethodIndex RC4_t1213_VTable[26] = 
{
	626,
	1629,
	627,
	628,
	1630,
	1631,
	1632,
	1633,
	1634,
	1682,
	1683,
	1637,
	1638,
	1639,
	1640,
	1641,
	1642,
	1643,
	1644,
	1645,
	1646,
	0,
	1647,
	0,
	0,
	0,
};
static Il2CppInterfaceOffsetPair RC4_t1213_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType RC4_t1213_1_0_0;
extern const Il2CppType SymmetricAlgorithm_t1176_0_0_0;
struct RC4_t1213;
const Il2CppTypeDefinitionMetadata RC4_t1213_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RC4_t1213_InterfacesOffsets/* interfaceOffsets */
	, &SymmetricAlgorithm_t1176_0_0_0/* parent */
	, RC4_t1213_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5138/* fieldStart */
	, 6384/* methodStart */
	, -1/* eventStart */
	, 1231/* propertyStart */

};
TypeInfo RC4_t1213_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "RC4"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RC4_t1213_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RC4_t1213_0_0_0/* byval_arg */
	, &RC4_t1213_1_0_0/* this_arg */
	, &RC4_t1213_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RC4_t1213)/* instance_size */
	, sizeof (RC4_t1213)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RC4_t1213_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 26/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Cryptography.RSAManaged
#include "Mono_Security_Mono_Security_Cryptography_RSAManaged.h"
// Metadata Definition Mono.Security.Cryptography.RSAManaged
extern TypeInfo RSAManaged_t1225_il2cpp_TypeInfo;
// Mono.Security.Cryptography.RSAManaged
#include "Mono_Security_Mono_Security_Cryptography_RSAManagedMethodDeclarations.h"
extern const Il2CppType KeyGeneratedEventHandler_t1224_0_0_0;
static const Il2CppType* RSAManaged_t1225_il2cpp_TypeInfo__nestedTypes[1] =
{
	&KeyGeneratedEventHandler_t1224_0_0_0,
};
static const EncodedMethodIndex RSAManaged_t1225_VTable[14] = 
{
	626,
	1703,
	627,
	628,
	1704,
	1705,
	1706,
	1707,
	1708,
	1709,
	1710,
	1711,
	1712,
	1713,
};
static Il2CppInterfaceOffsetPair RSAManaged_t1225_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType RSAManaged_t1225_0_0_0;
extern const Il2CppType RSAManaged_t1225_1_0_0;
extern const Il2CppType RSA_t1226_0_0_0;
struct RSAManaged_t1225;
const Il2CppTypeDefinitionMetadata RSAManaged_t1225_DefinitionMetadata = 
{
	NULL/* declaringType */
	, RSAManaged_t1225_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RSAManaged_t1225_InterfacesOffsets/* interfaceOffsets */
	, &RSA_t1226_0_0_0/* parent */
	, RSAManaged_t1225_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5140/* fieldStart */
	, 6388/* methodStart */
	, -1/* eventStart */
	, 1232/* propertyStart */

};
TypeInfo RSAManaged_t1225_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSAManaged"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &RSAManaged_t1225_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RSAManaged_t1225_0_0_0/* byval_arg */
	, &RSAManaged_t1225_1_0_0/* this_arg */
	, &RSAManaged_t1225_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSAManaged_t1225)/* instance_size */
	, sizeof (RSAManaged_t1225)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 2/* property_count */
	, 13/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 14/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "Mono_Security_Mono_Security_Cryptography_RSAManaged_KeyGener.h"
// Metadata Definition Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
extern TypeInfo KeyGeneratedEventHandler_t1224_il2cpp_TypeInfo;
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
#include "Mono_Security_Mono_Security_Cryptography_RSAManaged_KeyGenerMethodDeclarations.h"
static const EncodedMethodIndex KeyGeneratedEventHandler_t1224_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1714,
	1715,
	1716,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair KeyGeneratedEventHandler_t1224_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType KeyGeneratedEventHandler_t1224_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct KeyGeneratedEventHandler_t1224;
const Il2CppTypeDefinitionMetadata KeyGeneratedEventHandler_t1224_DefinitionMetadata = 
{
	&RSAManaged_t1225_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyGeneratedEventHandler_t1224_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, KeyGeneratedEventHandler_t1224_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6401/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyGeneratedEventHandler_t1224_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyGeneratedEventHandler"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &KeyGeneratedEventHandler_t1224_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyGeneratedEventHandler_t1224_0_0_0/* byval_arg */
	, &KeyGeneratedEventHandler_t1224_1_0_0/* this_arg */
	, &KeyGeneratedEventHandler_t1224_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1224/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyGeneratedEventHandler_t1224)/* instance_size */
	, sizeof (KeyGeneratedEventHandler_t1224)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.X509.SafeBag
#include "Mono_Security_Mono_Security_X509_SafeBag.h"
// Metadata Definition Mono.Security.X509.SafeBag
extern TypeInfo SafeBag_t1227_il2cpp_TypeInfo;
// Mono.Security.X509.SafeBag
#include "Mono_Security_Mono_Security_X509_SafeBagMethodDeclarations.h"
static const EncodedMethodIndex SafeBag_t1227_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SafeBag_t1227_0_0_0;
extern const Il2CppType SafeBag_t1227_1_0_0;
struct SafeBag_t1227;
const Il2CppTypeDefinitionMetadata SafeBag_t1227_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SafeBag_t1227_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5153/* fieldStart */
	, 6405/* methodStart */
	, -1/* eventStart */
	, 1234/* propertyStart */

};
TypeInfo SafeBag_t1227_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeBag"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &SafeBag_t1227_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeBag_t1227_0_0_0/* byval_arg */
	, &SafeBag_t1227_1_0_0/* this_arg */
	, &SafeBag_t1227_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeBag_t1227)/* instance_size */
	, sizeof (SafeBag_t1227)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.PKCS12
#include "Mono_Security_Mono_Security_X509_PKCS12.h"
// Metadata Definition Mono.Security.X509.PKCS12
extern TypeInfo PKCS12_t1230_il2cpp_TypeInfo;
// Mono.Security.X509.PKCS12
#include "Mono_Security_Mono_Security_X509_PKCS12MethodDeclarations.h"
extern const Il2CppType DeriveBytes_t1228_0_0_0;
static const Il2CppType* PKCS12_t1230_il2cpp_TypeInfo__nestedTypes[1] =
{
	&DeriveBytes_t1228_0_0_0,
};
static const EncodedMethodIndex PKCS12_t1230_VTable[4] = 
{
	626,
	1717,
	627,
	628,
};
static const Il2CppType* PKCS12_t1230_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
};
static Il2CppInterfaceOffsetPair PKCS12_t1230_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PKCS12_t1230_0_0_0;
extern const Il2CppType PKCS12_t1230_1_0_0;
struct PKCS12_t1230;
const Il2CppTypeDefinitionMetadata PKCS12_t1230_DefinitionMetadata = 
{
	NULL/* declaringType */
	, PKCS12_t1230_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, PKCS12_t1230_InterfacesTypeInfos/* implementedInterfaces */
	, PKCS12_t1230_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PKCS12_t1230_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5155/* fieldStart */
	, 6408/* methodStart */
	, -1/* eventStart */
	, 1236/* propertyStart */

};
TypeInfo PKCS12_t1230_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PKCS12"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &PKCS12_t1230_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PKCS12_t1230_0_0_0/* byval_arg */
	, &PKCS12_t1230_1_0_0/* this_arg */
	, &PKCS12_t1230_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PKCS12_t1230)/* instance_size */
	, sizeof (PKCS12_t1230)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PKCS12_t1230_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 4/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.PKCS12/DeriveBytes
#include "Mono_Security_Mono_Security_X509_PKCS12_DeriveBytes.h"
// Metadata Definition Mono.Security.X509.PKCS12/DeriveBytes
extern TypeInfo DeriveBytes_t1228_il2cpp_TypeInfo;
// Mono.Security.X509.PKCS12/DeriveBytes
#include "Mono_Security_Mono_Security_X509_PKCS12_DeriveBytesMethodDeclarations.h"
static const EncodedMethodIndex DeriveBytes_t1228_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType DeriveBytes_t1228_1_0_0;
struct DeriveBytes_t1228;
const Il2CppTypeDefinitionMetadata DeriveBytes_t1228_DefinitionMetadata = 
{
	&PKCS12_t1230_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DeriveBytes_t1228_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5170/* fieldStart */
	, 6426/* methodStart */
	, -1/* eventStart */
	, 1240/* propertyStart */

};
TypeInfo DeriveBytes_t1228_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "DeriveBytes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &DeriveBytes_t1228_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DeriveBytes_t1228_0_0_0/* byval_arg */
	, &DeriveBytes_t1228_1_0_0/* this_arg */
	, &DeriveBytes_t1228_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DeriveBytes_t1228)/* instance_size */
	, sizeof (DeriveBytes_t1228)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DeriveBytes_t1228_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X501
#include "Mono_Security_Mono_Security_X509_X501.h"
// Metadata Definition Mono.Security.X509.X501
extern TypeInfo X501_t1231_il2cpp_TypeInfo;
// Mono.Security.X509.X501
#include "Mono_Security_Mono_Security_X509_X501MethodDeclarations.h"
static const EncodedMethodIndex X501_t1231_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X501_t1231_0_0_0;
extern const Il2CppType X501_t1231_1_0_0;
struct X501_t1231;
const Il2CppTypeDefinitionMetadata X501_t1231_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X501_t1231_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5177/* fieldStart */
	, 6437/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X501_t1231_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X501"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X501_t1231_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X501_t1231_0_0_0/* byval_arg */
	, &X501_t1231_1_0_0/* this_arg */
	, &X501_t1231_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X501_t1231)/* instance_size */
	, sizeof (X501_t1231)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X501_t1231_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509Certificate
#include "Mono_Security_Mono_Security_X509_X509Certificate.h"
// Metadata Definition Mono.Security.X509.X509Certificate
extern TypeInfo X509Certificate_t1234_il2cpp_TypeInfo;
// Mono.Security.X509.X509Certificate
#include "Mono_Security_Mono_Security_X509_X509CertificateMethodDeclarations.h"
static const EncodedMethodIndex X509Certificate_t1234_VTable[20] = 
{
	626,
	601,
	627,
	628,
	1718,
	1719,
	1720,
	1721,
	1722,
	1723,
	1724,
	1725,
	1726,
	1727,
	1728,
	1729,
	1730,
	1731,
	1732,
	1718,
};
static const Il2CppType* X509Certificate_t1234_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair X509Certificate_t1234_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509Certificate_t1234_0_0_0;
extern const Il2CppType X509Certificate_t1234_1_0_0;
struct X509Certificate_t1234;
const Il2CppTypeDefinitionMetadata X509Certificate_t1234_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509Certificate_t1234_InterfacesTypeInfos/* implementedInterfaces */
	, X509Certificate_t1234_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Certificate_t1234_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5192/* fieldStart */
	, 6441/* methodStart */
	, -1/* eventStart */
	, 1244/* propertyStart */

};
TypeInfo X509Certificate_t1234_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Certificate"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Certificate_t1234_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Certificate_t1234_0_0_0/* byval_arg */
	, &X509Certificate_t1234_1_0_0/* this_arg */
	, &X509Certificate_t1234_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Certificate_t1234)/* instance_size */
	, sizeof (X509Certificate_t1234)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Certificate_t1234_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 33/* method_count */
	, 18/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.X509CertificateCollection
#include "Mono_Security_Mono_Security_X509_X509CertificateCollection.h"
// Metadata Definition Mono.Security.X509.X509CertificateCollection
extern TypeInfo X509CertificateCollection_t1229_il2cpp_TypeInfo;
// Mono.Security.X509.X509CertificateCollection
#include "Mono_Security_Mono_Security_X509_X509CertificateCollectionMethodDeclarations.h"
extern const Il2CppType X509CertificateEnumerator_t1235_0_0_0;
static const Il2CppType* X509CertificateCollection_t1229_il2cpp_TypeInfo__nestedTypes[1] =
{
	&X509CertificateEnumerator_t1235_0_0_0,
};
static const EncodedMethodIndex X509CertificateCollection_t1229_VTable[29] = 
{
	626,
	601,
	1733,
	628,
	1734,
	1735,
	1736,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
};
extern const Il2CppType IEnumerable_t1097_0_0_0;
static const Il2CppType* X509CertificateCollection_t1229_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
};
extern const Il2CppType ICollection_t1528_0_0_0;
extern const Il2CppType IList_t1487_0_0_0;
static Il2CppInterfaceOffsetPair X509CertificateCollection_t1229_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509CertificateCollection_t1229_0_0_0;
extern const Il2CppType X509CertificateCollection_t1229_1_0_0;
extern const Il2CppType CollectionBase_t1236_0_0_0;
struct X509CertificateCollection_t1229;
const Il2CppTypeDefinitionMetadata X509CertificateCollection_t1229_DefinitionMetadata = 
{
	NULL/* declaringType */
	, X509CertificateCollection_t1229_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, X509CertificateCollection_t1229_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateCollection_t1229_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1236_0_0_0/* parent */
	, X509CertificateCollection_t1229_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6474/* methodStart */
	, -1/* eventStart */
	, 1262/* propertyStart */

};
TypeInfo X509CertificateCollection_t1229_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateCollection"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509CertificateCollection_t1229_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2330/* custom_attributes_cache */
	, &X509CertificateCollection_t1229_0_0_0/* byval_arg */
	, &X509CertificateCollection_t1229_1_0_0/* this_arg */
	, &X509CertificateCollection_t1229_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateCollection_t1229)/* instance_size */
	, sizeof (X509CertificateCollection_t1229)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "Mono_Security_Mono_Security_X509_X509CertificateCollection_X.h"
// Metadata Definition Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
extern TypeInfo X509CertificateEnumerator_t1235_il2cpp_TypeInfo;
// Mono.Security.X509.X509CertificateCollection/X509CertificateEnumerator
#include "Mono_Security_Mono_Security_X509_X509CertificateCollection_XMethodDeclarations.h"
static const EncodedMethodIndex X509CertificateEnumerator_t1235_VTable[7] = 
{
	626,
	601,
	627,
	628,
	1759,
	1760,
	1761,
};
extern const Il2CppType IEnumerator_t464_0_0_0;
static const Il2CppType* X509CertificateEnumerator_t1235_InterfacesTypeInfos[] = 
{
	&IEnumerator_t464_0_0_0,
};
static Il2CppInterfaceOffsetPair X509CertificateEnumerator_t1235_InterfacesOffsets[] = 
{
	{ &IEnumerator_t464_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509CertificateEnumerator_t1235_1_0_0;
struct X509CertificateEnumerator_t1235;
const Il2CppTypeDefinitionMetadata X509CertificateEnumerator_t1235_DefinitionMetadata = 
{
	&X509CertificateCollection_t1229_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, X509CertificateEnumerator_t1235_InterfacesTypeInfos/* implementedInterfaces */
	, X509CertificateEnumerator_t1235_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509CertificateEnumerator_t1235_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5218/* fieldStart */
	, 6486/* methodStart */
	, -1/* eventStart */
	, 1263/* propertyStart */

};
TypeInfo X509CertificateEnumerator_t1235_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CertificateEnumerator"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &X509CertificateEnumerator_t1235_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509CertificateEnumerator_t1235_0_0_0/* byval_arg */
	, &X509CertificateEnumerator_t1235_1_0_0/* this_arg */
	, &X509CertificateEnumerator_t1235_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CertificateEnumerator_t1235)/* instance_size */
	, sizeof (X509CertificateEnumerator_t1235)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.X509.X509Chain
#include "Mono_Security_Mono_Security_X509_X509Chain.h"
// Metadata Definition Mono.Security.X509.X509Chain
extern TypeInfo X509Chain_t1237_il2cpp_TypeInfo;
// Mono.Security.X509.X509Chain
#include "Mono_Security_Mono_Security_X509_X509ChainMethodDeclarations.h"
static const EncodedMethodIndex X509Chain_t1237_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509Chain_t1237_0_0_0;
extern const Il2CppType X509Chain_t1237_1_0_0;
struct X509Chain_t1237;
const Il2CppTypeDefinitionMetadata X509Chain_t1237_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Chain_t1237_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5219/* fieldStart */
	, 6491/* methodStart */
	, -1/* eventStart */
	, 1265/* propertyStart */

};
TypeInfo X509Chain_t1237_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Chain"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Chain_t1237_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Chain_t1237_0_0_0/* byval_arg */
	, &X509Chain_t1237_1_0_0/* this_arg */
	, &X509Chain_t1237_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Chain_t1237)/* instance_size */
	, sizeof (X509Chain_t1237)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
// Metadata Definition Mono.Security.X509.X509ChainStatusFlags
extern TypeInfo X509ChainStatusFlags_t1238_il2cpp_TypeInfo;
// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlagsMethodDeclarations.h"
static const EncodedMethodIndex X509ChainStatusFlags_t1238_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair X509ChainStatusFlags_t1238_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509ChainStatusFlags_t1238_0_0_0;
extern const Il2CppType X509ChainStatusFlags_t1238_1_0_0;
const Il2CppTypeDefinitionMetadata X509ChainStatusFlags_t1238_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, X509ChainStatusFlags_t1238_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, X509ChainStatusFlags_t1238_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5224/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo X509ChainStatusFlags_t1238_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ChainStatusFlags"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2331/* custom_attributes_cache */
	, &X509ChainStatusFlags_t1238_0_0_0/* byval_arg */
	, &X509ChainStatusFlags_t1238_1_0_0/* this_arg */
	, &X509ChainStatusFlags_t1238_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ChainStatusFlags_t1238)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (X509ChainStatusFlags_t1238)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.X509.X509Crl
#include "Mono_Security_Mono_Security_X509_X509Crl.h"
// Metadata Definition Mono.Security.X509.X509Crl
extern TypeInfo X509Crl_t1240_il2cpp_TypeInfo;
// Mono.Security.X509.X509Crl
#include "Mono_Security_Mono_Security_X509_X509CrlMethodDeclarations.h"
extern const Il2CppType X509CrlEntry_t1239_0_0_0;
static const Il2CppType* X509Crl_t1240_il2cpp_TypeInfo__nestedTypes[1] =
{
	&X509CrlEntry_t1239_0_0_0,
};
static const EncodedMethodIndex X509Crl_t1240_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509Crl_t1240_0_0_0;
extern const Il2CppType X509Crl_t1240_1_0_0;
struct X509Crl_t1240;
const Il2CppTypeDefinitionMetadata X509Crl_t1240_DefinitionMetadata = 
{
	NULL/* declaringType */
	, X509Crl_t1240_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Crl_t1240_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5232/* fieldStart */
	, 6501/* methodStart */
	, -1/* eventStart */
	, 1267/* propertyStart */

};
TypeInfo X509Crl_t1240_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Crl"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Crl_t1240_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2332/* custom_attributes_cache */
	, &X509Crl_t1240_0_0_0/* byval_arg */
	, &X509Crl_t1240_1_0_0/* this_arg */
	, &X509Crl_t1240_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Crl_t1240)/* instance_size */
	, sizeof (X509Crl_t1240)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509Crl_t1240_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 4/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509Crl/X509CrlEntry
#include "Mono_Security_Mono_Security_X509_X509Crl_X509CrlEntry.h"
// Metadata Definition Mono.Security.X509.X509Crl/X509CrlEntry
extern TypeInfo X509CrlEntry_t1239_il2cpp_TypeInfo;
// Mono.Security.X509.X509Crl/X509CrlEntry
#include "Mono_Security_Mono_Security_X509_X509Crl_X509CrlEntryMethodDeclarations.h"
static const EncodedMethodIndex X509CrlEntry_t1239_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509CrlEntry_t1239_1_0_0;
struct X509CrlEntry_t1239;
const Il2CppTypeDefinitionMetadata X509CrlEntry_t1239_DefinitionMetadata = 
{
	&X509Crl_t1240_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509CrlEntry_t1239_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5243/* fieldStart */
	, 6514/* methodStart */
	, -1/* eventStart */
	, 1271/* propertyStart */

};
TypeInfo X509CrlEntry_t1239_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509CrlEntry"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &X509CrlEntry_t1239_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509CrlEntry_t1239_0_0_0/* byval_arg */
	, &X509CrlEntry_t1239_1_0_0/* this_arg */
	, &X509CrlEntry_t1239_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509CrlEntry_t1239)/* instance_size */
	, sizeof (X509CrlEntry_t1239)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048578/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509Extension
#include "Mono_Security_Mono_Security_X509_X509Extension.h"
// Metadata Definition Mono.Security.X509.X509Extension
extern TypeInfo X509Extension_t1241_il2cpp_TypeInfo;
// Mono.Security.X509.X509Extension
#include "Mono_Security_Mono_Security_X509_X509ExtensionMethodDeclarations.h"
static const EncodedMethodIndex X509Extension_t1241_VTable[6] = 
{
	1762,
	601,
	1763,
	1764,
	1765,
	1766,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509Extension_t1241_0_0_0;
extern const Il2CppType X509Extension_t1241_1_0_0;
struct X509Extension_t1241;
const Il2CppTypeDefinitionMetadata X509Extension_t1241_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Extension_t1241_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5246/* fieldStart */
	, 6518/* methodStart */
	, -1/* eventStart */
	, 1274/* propertyStart */

};
TypeInfo X509Extension_t1241_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Extension"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Extension_t1241_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Extension_t1241_0_0_0/* byval_arg */
	, &X509Extension_t1241_1_0_0/* this_arg */
	, &X509Extension_t1241_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Extension_t1241)/* instance_size */
	, sizeof (X509Extension_t1241)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509ExtensionCollection
#include "Mono_Security_Mono_Security_X509_X509ExtensionCollection.h"
// Metadata Definition Mono.Security.X509.X509ExtensionCollection
extern TypeInfo X509ExtensionCollection_t1233_il2cpp_TypeInfo;
// Mono.Security.X509.X509ExtensionCollection
#include "Mono_Security_Mono_Security_X509_X509ExtensionCollectionMethodDeclarations.h"
static const EncodedMethodIndex X509ExtensionCollection_t1233_VTable[29] = 
{
	626,
	601,
	627,
	628,
	1767,
	1735,
	1736,
	1737,
	1738,
	1739,
	1740,
	1741,
	1742,
	1743,
	1744,
	1745,
	1746,
	1747,
	1748,
	1749,
	1750,
	1751,
	1752,
	1753,
	1754,
	1755,
	1756,
	1757,
	1758,
};
static const Il2CppType* X509ExtensionCollection_t1233_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
};
static Il2CppInterfaceOffsetPair X509ExtensionCollection_t1233_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509ExtensionCollection_t1233_0_0_0;
extern const Il2CppType X509ExtensionCollection_t1233_1_0_0;
struct X509ExtensionCollection_t1233;
const Il2CppTypeDefinitionMetadata X509ExtensionCollection_t1233_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, X509ExtensionCollection_t1233_InterfacesTypeInfos/* implementedInterfaces */
	, X509ExtensionCollection_t1233_InterfacesOffsets/* interfaceOffsets */
	, &CollectionBase_t1236_0_0_0/* parent */
	, X509ExtensionCollection_t1233_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5249/* fieldStart */
	, 6529/* methodStart */
	, -1/* eventStart */
	, 1277/* propertyStart */

};
TypeInfo X509ExtensionCollection_t1233_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509ExtensionCollection"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509ExtensionCollection_t1233_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2334/* custom_attributes_cache */
	, &X509ExtensionCollection_t1233_0_0_0/* byval_arg */
	, &X509ExtensionCollection_t1233_1_0_0/* this_arg */
	, &X509ExtensionCollection_t1233_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509ExtensionCollection_t1233)/* instance_size */
	, sizeof (X509ExtensionCollection_t1233)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.X509.X509Store
#include "Mono_Security_Mono_Security_X509_X509Store.h"
// Metadata Definition Mono.Security.X509.X509Store
extern TypeInfo X509Store_t1242_il2cpp_TypeInfo;
// Mono.Security.X509.X509Store
#include "Mono_Security_Mono_Security_X509_X509StoreMethodDeclarations.h"
static const EncodedMethodIndex X509Store_t1242_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509Store_t1242_0_0_0;
extern const Il2CppType X509Store_t1242_1_0_0;
struct X509Store_t1242;
const Il2CppTypeDefinitionMetadata X509Store_t1242_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Store_t1242_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5250/* fieldStart */
	, 6534/* methodStart */
	, -1/* eventStart */
	, 1278/* propertyStart */

};
TypeInfo X509Store_t1242_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Store"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Store_t1242_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Store_t1242_0_0_0/* byval_arg */
	, &X509Store_t1242_1_0_0/* this_arg */
	, &X509Store_t1242_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Store_t1242)/* instance_size */
	, sizeof (X509Store_t1242)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509StoreManager
#include "Mono_Security_Mono_Security_X509_X509StoreManager.h"
// Metadata Definition Mono.Security.X509.X509StoreManager
extern TypeInfo X509StoreManager_t1244_il2cpp_TypeInfo;
// Mono.Security.X509.X509StoreManager
#include "Mono_Security_Mono_Security_X509_X509StoreManagerMethodDeclarations.h"
static const EncodedMethodIndex X509StoreManager_t1244_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509StoreManager_t1244_0_0_0;
extern const Il2CppType X509StoreManager_t1244_1_0_0;
struct X509StoreManager_t1244;
const Il2CppTypeDefinitionMetadata X509StoreManager_t1244_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509StoreManager_t1244_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5254/* fieldStart */
	, 6543/* methodStart */
	, -1/* eventStart */
	, 1280/* propertyStart */

};
TypeInfo X509StoreManager_t1244_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509StoreManager"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509StoreManager_t1244_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509StoreManager_t1244_0_0_0/* byval_arg */
	, &X509StoreManager_t1244_1_0_0/* this_arg */
	, &X509StoreManager_t1244_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509StoreManager_t1244)/* instance_size */
	, sizeof (X509StoreManager_t1244)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(X509StoreManager_t1244_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.X509Stores
#include "Mono_Security_Mono_Security_X509_X509Stores.h"
// Metadata Definition Mono.Security.X509.X509Stores
extern TypeInfo X509Stores_t1243_il2cpp_TypeInfo;
// Mono.Security.X509.X509Stores
#include "Mono_Security_Mono_Security_X509_X509StoresMethodDeclarations.h"
static const EncodedMethodIndex X509Stores_t1243_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType X509Stores_t1243_0_0_0;
extern const Il2CppType X509Stores_t1243_1_0_0;
struct X509Stores_t1243;
const Il2CppTypeDefinitionMetadata X509Stores_t1243_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, X509Stores_t1243_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5256/* fieldStart */
	, 6546/* methodStart */
	, -1/* eventStart */
	, 1283/* propertyStart */

};
TypeInfo X509Stores_t1243_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "X509Stores"/* name */
	, "Mono.Security.X509"/* namespaze */
	, NULL/* methods */
	, &X509Stores_t1243_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &X509Stores_t1243_0_0_0/* byval_arg */
	, &X509Stores_t1243_1_0_0/* this_arg */
	, &X509Stores_t1243_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (X509Stores_t1243)/* instance_size */
	, sizeof (X509Stores_t1243)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
#include "Mono_Security_Mono_Security_X509_Extensions_AuthorityKeyIden.h"
// Metadata Definition Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
extern TypeInfo AuthorityKeyIdentifierExtension_t1245_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
#include "Mono_Security_Mono_Security_X509_Extensions_AuthorityKeyIdenMethodDeclarations.h"
static const EncodedMethodIndex AuthorityKeyIdentifierExtension_t1245_VTable[6] = 
{
	1762,
	601,
	1763,
	1768,
	1769,
	1766,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType AuthorityKeyIdentifierExtension_t1245_0_0_0;
extern const Il2CppType AuthorityKeyIdentifierExtension_t1245_1_0_0;
struct AuthorityKeyIdentifierExtension_t1245;
const Il2CppTypeDefinitionMetadata AuthorityKeyIdentifierExtension_t1245_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1241_0_0_0/* parent */
	, AuthorityKeyIdentifierExtension_t1245_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5258/* fieldStart */
	, 6549/* methodStart */
	, -1/* eventStart */
	, 1284/* propertyStart */

};
TypeInfo AuthorityKeyIdentifierExtension_t1245_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "AuthorityKeyIdentifierExtension"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &AuthorityKeyIdentifierExtension_t1245_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AuthorityKeyIdentifierExtension_t1245_0_0_0/* byval_arg */
	, &AuthorityKeyIdentifierExtension_t1245_1_0_0/* this_arg */
	, &AuthorityKeyIdentifierExtension_t1245_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AuthorityKeyIdentifierExtension_t1245)/* instance_size */
	, sizeof (AuthorityKeyIdentifierExtension_t1245)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.BasicConstraintsExtension
#include "Mono_Security_Mono_Security_X509_Extensions_BasicConstraints.h"
// Metadata Definition Mono.Security.X509.Extensions.BasicConstraintsExtension
extern TypeInfo BasicConstraintsExtension_t1246_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.BasicConstraintsExtension
#include "Mono_Security_Mono_Security_X509_Extensions_BasicConstraintsMethodDeclarations.h"
static const EncodedMethodIndex BasicConstraintsExtension_t1246_VTable[6] = 
{
	1762,
	601,
	1763,
	1770,
	1771,
	1772,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType BasicConstraintsExtension_t1246_0_0_0;
extern const Il2CppType BasicConstraintsExtension_t1246_1_0_0;
struct BasicConstraintsExtension_t1246;
const Il2CppTypeDefinitionMetadata BasicConstraintsExtension_t1246_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1241_0_0_0/* parent */
	, BasicConstraintsExtension_t1246_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5259/* fieldStart */
	, 6553/* methodStart */
	, -1/* eventStart */
	, 1285/* propertyStart */

};
TypeInfo BasicConstraintsExtension_t1246_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "BasicConstraintsExtension"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &BasicConstraintsExtension_t1246_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BasicConstraintsExtension_t1246_0_0_0/* byval_arg */
	, &BasicConstraintsExtension_t1246_1_0_0/* this_arg */
	, &BasicConstraintsExtension_t1246_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BasicConstraintsExtension_t1246)/* instance_size */
	, sizeof (BasicConstraintsExtension_t1246)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
#include "Mono_Security_Mono_Security_X509_Extensions_ExtendedKeyUsage.h"
// Metadata Definition Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
extern TypeInfo ExtendedKeyUsageExtension_t1247_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
#include "Mono_Security_Mono_Security_X509_Extensions_ExtendedKeyUsageMethodDeclarations.h"
static const EncodedMethodIndex ExtendedKeyUsageExtension_t1247_VTable[6] = 
{
	1762,
	601,
	1763,
	1773,
	1774,
	1775,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ExtendedKeyUsageExtension_t1247_0_0_0;
extern const Il2CppType ExtendedKeyUsageExtension_t1247_1_0_0;
struct ExtendedKeyUsageExtension_t1247;
const Il2CppTypeDefinitionMetadata ExtendedKeyUsageExtension_t1247_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1241_0_0_0/* parent */
	, ExtendedKeyUsageExtension_t1247_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5261/* fieldStart */
	, 6558/* methodStart */
	, -1/* eventStart */
	, 1286/* propertyStart */

};
TypeInfo ExtendedKeyUsageExtension_t1247_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtendedKeyUsageExtension"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &ExtendedKeyUsageExtension_t1247_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExtendedKeyUsageExtension_t1247_0_0_0/* byval_arg */
	, &ExtendedKeyUsageExtension_t1247_1_0_0/* this_arg */
	, &ExtendedKeyUsageExtension_t1247_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtendedKeyUsageExtension_t1247)/* instance_size */
	, sizeof (ExtendedKeyUsageExtension_t1247)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ExtendedKeyUsageExtension_t1247_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.GeneralNames
#include "Mono_Security_Mono_Security_X509_Extensions_GeneralNames.h"
// Metadata Definition Mono.Security.X509.Extensions.GeneralNames
extern TypeInfo GeneralNames_t1248_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.GeneralNames
#include "Mono_Security_Mono_Security_X509_Extensions_GeneralNamesMethodDeclarations.h"
static const EncodedMethodIndex GeneralNames_t1248_VTable[4] = 
{
	626,
	601,
	627,
	1776,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType GeneralNames_t1248_0_0_0;
extern const Il2CppType GeneralNames_t1248_1_0_0;
struct GeneralNames_t1248;
const Il2CppTypeDefinitionMetadata GeneralNames_t1248_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GeneralNames_t1248_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5263/* fieldStart */
	, 6563/* methodStart */
	, -1/* eventStart */
	, 1287/* propertyStart */

};
TypeInfo GeneralNames_t1248_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "GeneralNames"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &GeneralNames_t1248_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GeneralNames_t1248_0_0_0/* byval_arg */
	, &GeneralNames_t1248_1_0_0/* this_arg */
	, &GeneralNames_t1248_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GeneralNames_t1248)/* instance_size */
	, sizeof (GeneralNames_t1248)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.KeyUsages
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsages.h"
// Metadata Definition Mono.Security.X509.Extensions.KeyUsages
extern TypeInfo KeyUsages_t1249_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.KeyUsages
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsagesMethodDeclarations.h"
static const EncodedMethodIndex KeyUsages_t1249_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair KeyUsages_t1249_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType KeyUsages_t1249_0_0_0;
extern const Il2CppType KeyUsages_t1249_1_0_0;
const Il2CppTypeDefinitionMetadata KeyUsages_t1249_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, KeyUsages_t1249_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, KeyUsages_t1249_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5268/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyUsages_t1249_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyUsages"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2336/* custom_attributes_cache */
	, &KeyUsages_t1249_0_0_0/* byval_arg */
	, &KeyUsages_t1249_1_0_0/* this_arg */
	, &KeyUsages_t1249_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyUsages_t1249)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (KeyUsages_t1249)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.KeyUsageExtension
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsageExtensio.h"
// Metadata Definition Mono.Security.X509.Extensions.KeyUsageExtension
extern TypeInfo KeyUsageExtension_t1250_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.KeyUsageExtension
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsageExtensioMethodDeclarations.h"
static const EncodedMethodIndex KeyUsageExtension_t1250_VTable[6] = 
{
	1762,
	601,
	1763,
	1777,
	1778,
	1779,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType KeyUsageExtension_t1250_0_0_0;
extern const Il2CppType KeyUsageExtension_t1250_1_0_0;
struct KeyUsageExtension_t1250;
const Il2CppTypeDefinitionMetadata KeyUsageExtension_t1250_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1241_0_0_0/* parent */
	, KeyUsageExtension_t1250_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5279/* fieldStart */
	, 6567/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo KeyUsageExtension_t1250_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyUsageExtension"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &KeyUsageExtension_t1250_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &KeyUsageExtension_t1250_0_0_0/* byval_arg */
	, &KeyUsageExtension_t1250_1_0_0/* this_arg */
	, &KeyUsageExtension_t1250_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeyUsageExtension_t1250)/* instance_size */
	, sizeof (KeyUsageExtension_t1250)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertType_0.h"
// Metadata Definition Mono.Security.X509.Extensions.NetscapeCertTypeExtension
extern TypeInfo NetscapeCertTypeExtension_t1252_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertType_0MethodDeclarations.h"
extern const Il2CppType CertTypes_t1251_0_0_0;
static const Il2CppType* NetscapeCertTypeExtension_t1252_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CertTypes_t1251_0_0_0,
};
static const EncodedMethodIndex NetscapeCertTypeExtension_t1252_VTable[6] = 
{
	1762,
	601,
	1763,
	1780,
	1781,
	1766,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType NetscapeCertTypeExtension_t1252_0_0_0;
extern const Il2CppType NetscapeCertTypeExtension_t1252_1_0_0;
struct NetscapeCertTypeExtension_t1252;
const Il2CppTypeDefinitionMetadata NetscapeCertTypeExtension_t1252_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NetscapeCertTypeExtension_t1252_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1241_0_0_0/* parent */
	, NetscapeCertTypeExtension_t1252_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5280/* fieldStart */
	, 6572/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NetscapeCertTypeExtension_t1252_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "NetscapeCertTypeExtension"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &NetscapeCertTypeExtension_t1252_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NetscapeCertTypeExtension_t1252_0_0_0/* byval_arg */
	, &NetscapeCertTypeExtension_t1252_1_0_0/* this_arg */
	, &NetscapeCertTypeExtension_t1252_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NetscapeCertTypeExtension_t1252)/* instance_size */
	, sizeof (NetscapeCertTypeExtension_t1252)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertType.h"
// Metadata Definition Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
extern TypeInfo CertTypes_t1251_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertTypeMethodDeclarations.h"
static const EncodedMethodIndex CertTypes_t1251_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CertTypes_t1251_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertTypes_t1251_1_0_0;
const Il2CppTypeDefinitionMetadata CertTypes_t1251_DefinitionMetadata = 
{
	&NetscapeCertTypeExtension_t1252_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertTypes_t1251_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CertTypes_t1251_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5281/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CertTypes_t1251_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertTypes"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2337/* custom_attributes_cache */
	, &CertTypes_t1251_0_0_0/* byval_arg */
	, &CertTypes_t1251_1_0_0/* this_arg */
	, &CertTypes_t1251_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertTypes_t1251)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CertTypes_t1251)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.X509.Extensions.SubjectAltNameExtension
#include "Mono_Security_Mono_Security_X509_Extensions_SubjectAltNameEx.h"
// Metadata Definition Mono.Security.X509.Extensions.SubjectAltNameExtension
extern TypeInfo SubjectAltNameExtension_t1253_il2cpp_TypeInfo;
// Mono.Security.X509.Extensions.SubjectAltNameExtension
#include "Mono_Security_Mono_Security_X509_Extensions_SubjectAltNameExMethodDeclarations.h"
static const EncodedMethodIndex SubjectAltNameExtension_t1253_VTable[6] = 
{
	1762,
	601,
	1763,
	1782,
	1783,
	1766,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SubjectAltNameExtension_t1253_0_0_0;
extern const Il2CppType SubjectAltNameExtension_t1253_1_0_0;
struct SubjectAltNameExtension_t1253;
const Il2CppTypeDefinitionMetadata SubjectAltNameExtension_t1253_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &X509Extension_t1241_0_0_0/* parent */
	, SubjectAltNameExtension_t1253_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5289/* fieldStart */
	, 6576/* methodStart */
	, -1/* eventStart */
	, 1289/* propertyStart */

};
TypeInfo SubjectAltNameExtension_t1253_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SubjectAltNameExtension"/* name */
	, "Mono.Security.X509.Extensions"/* namespaze */
	, NULL/* methods */
	, &SubjectAltNameExtension_t1253_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SubjectAltNameExtension_t1253_0_0_0/* byval_arg */
	, &SubjectAltNameExtension_t1253_1_0_0/* this_arg */
	, &SubjectAltNameExtension_t1253_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SubjectAltNameExtension_t1253)/* instance_size */
	, sizeof (SubjectAltNameExtension_t1253)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Cryptography.HMAC
#include "Mono_Security_Mono_Security_Cryptography_HMAC.h"
// Metadata Definition Mono.Security.Cryptography.HMAC
extern TypeInfo HMAC_t1254_il2cpp_TypeInfo;
// Mono.Security.Cryptography.HMAC
#include "Mono_Security_Mono_Security_Cryptography_HMACMethodDeclarations.h"
static const EncodedMethodIndex HMAC_t1254_VTable[17] = 
{
	626,
	1784,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	1785,
	1786,
	1698,
	1787,
	1788,
	1789,
	1790,
};
static Il2CppInterfaceOffsetPair HMAC_t1254_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType HMAC_t1254_0_0_0;
extern const Il2CppType HMAC_t1254_1_0_0;
extern const Il2CppType KeyedHashAlgorithm_t1255_0_0_0;
struct HMAC_t1254;
const Il2CppTypeDefinitionMetadata HMAC_t1254_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HMAC_t1254_InterfacesOffsets/* interfaceOffsets */
	, &KeyedHashAlgorithm_t1255_0_0_0/* parent */
	, HMAC_t1254_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5290/* fieldStart */
	, 6581/* methodStart */
	, -1/* eventStart */
	, 1291/* propertyStart */

};
TypeInfo HMAC_t1254_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "HMAC"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &HMAC_t1254_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HMAC_t1254_0_0_0/* byval_arg */
	, &HMAC_t1254_1_0_0/* this_arg */
	, &HMAC_t1254_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HMAC_t1254)/* instance_size */
	, sizeof (HMAC_t1254)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 17/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Cryptography.MD5SHA1
#include "Mono_Security_Mono_Security_Cryptography_MD5SHA1.h"
// Metadata Definition Mono.Security.Cryptography.MD5SHA1
extern TypeInfo MD5SHA1_t1256_il2cpp_TypeInfo;
// Mono.Security.Cryptography.MD5SHA1
#include "Mono_Security_Mono_Security_Cryptography_MD5SHA1MethodDeclarations.h"
static const EncodedMethodIndex MD5SHA1_t1256_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	1791,
	1792,
	1698,
	1793,
	1699,
};
static Il2CppInterfaceOffsetPair MD5SHA1_t1256_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType MD5SHA1_t1256_0_0_0;
extern const Il2CppType MD5SHA1_t1256_1_0_0;
struct MD5SHA1_t1256;
const Il2CppTypeDefinitionMetadata MD5SHA1_t1256_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MD5SHA1_t1256_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, MD5SHA1_t1256_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5294/* fieldStart */
	, 6588/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MD5SHA1_t1256_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "MD5SHA1"/* name */
	, "Mono.Security.Cryptography"/* namespaze */
	, NULL/* methods */
	, &MD5SHA1_t1256_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MD5SHA1_t1256_0_0_0/* byval_arg */
	, &MD5SHA1_t1256_1_0_0/* this_arg */
	, &MD5SHA1_t1256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MD5SHA1_t1256)/* instance_size */
	, sizeof (MD5SHA1_t1256)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
// Metadata Definition Mono.Security.Protocol.Tls.AlertLevel
extern TypeInfo AlertLevel_t1257_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevelMethodDeclarations.h"
static const EncodedMethodIndex AlertLevel_t1257_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AlertLevel_t1257_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType AlertLevel_t1257_0_0_0;
extern const Il2CppType AlertLevel_t1257_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t1117_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata AlertLevel_t1257_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AlertLevel_t1257_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AlertLevel_t1257_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5297/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AlertLevel_t1257_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "AlertLevel"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AlertLevel_t1257_0_0_0/* byval_arg */
	, &AlertLevel_t1257_1_0_0/* this_arg */
	, &AlertLevel_t1257_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AlertLevel_t1257)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AlertLevel_t1257)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8448/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
// Metadata Definition Mono.Security.Protocol.Tls.AlertDescription
extern TypeInfo AlertDescription_t1258_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescriptionMethodDeclarations.h"
static const EncodedMethodIndex AlertDescription_t1258_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AlertDescription_t1258_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType AlertDescription_t1258_0_0_0;
extern const Il2CppType AlertDescription_t1258_1_0_0;
const Il2CppTypeDefinitionMetadata AlertDescription_t1258_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AlertDescription_t1258_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AlertDescription_t1258_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5300/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AlertDescription_t1258_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "AlertDescription"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AlertDescription_t1258_0_0_0/* byval_arg */
	, &AlertDescription_t1258_1_0_0/* this_arg */
	, &AlertDescription_t1258_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AlertDescription_t1258)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AlertDescription_t1258)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8448/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 25/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Alert
#include "Mono_Security_Mono_Security_Protocol_Tls_Alert.h"
// Metadata Definition Mono.Security.Protocol.Tls.Alert
extern TypeInfo Alert_t1259_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Alert
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertMethodDeclarations.h"
static const EncodedMethodIndex Alert_t1259_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType Alert_t1259_0_0_0;
extern const Il2CppType Alert_t1259_1_0_0;
struct Alert_t1259;
const Il2CppTypeDefinitionMetadata Alert_t1259_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Alert_t1259_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5325/* fieldStart */
	, 6594/* methodStart */
	, -1/* eventStart */
	, 1292/* propertyStart */

};
TypeInfo Alert_t1259_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "Alert"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Alert_t1259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Alert_t1259_0_0_0/* byval_arg */
	, &Alert_t1259_1_0_0/* this_arg */
	, &Alert_t1259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Alert_t1259)/* instance_size */
	, sizeof (Alert_t1259)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
// Metadata Definition Mono.Security.Protocol.Tls.CipherAlgorithmType
extern TypeInfo CipherAlgorithmType_t1260_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmTypeMethodDeclarations.h"
static const EncodedMethodIndex CipherAlgorithmType_t1260_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CipherAlgorithmType_t1260_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CipherAlgorithmType_t1260_0_0_0;
extern const Il2CppType CipherAlgorithmType_t1260_1_0_0;
const Il2CppTypeDefinitionMetadata CipherAlgorithmType_t1260_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CipherAlgorithmType_t1260_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CipherAlgorithmType_t1260_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5327/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CipherAlgorithmType_t1260_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CipherAlgorithmType"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CipherAlgorithmType_t1260_0_0_0/* byval_arg */
	, &CipherAlgorithmType_t1260_1_0_0/* this_arg */
	, &CipherAlgorithmType_t1260_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CipherAlgorithmType_t1260)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CipherAlgorithmType_t1260)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuite.h"
// Metadata Definition Mono.Security.Protocol.Tls.CipherSuite
extern TypeInfo CipherSuite_t1262_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuiteMethodDeclarations.h"
static const EncodedMethodIndex CipherSuite_t1262_VTable[8] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
	0,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CipherSuite_t1262_0_0_0;
extern const Il2CppType CipherSuite_t1262_1_0_0;
struct CipherSuite_t1262;
const Il2CppTypeDefinitionMetadata CipherSuite_t1262_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CipherSuite_t1262_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5335/* fieldStart */
	, 6602/* methodStart */
	, -1/* eventStart */
	, 1296/* propertyStart */

};
TypeInfo CipherSuite_t1262_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CipherSuite"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &CipherSuite_t1262_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CipherSuite_t1262_0_0_0/* byval_arg */
	, &CipherSuite_t1262_1_0_0/* this_arg */
	, &CipherSuite_t1262_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CipherSuite_t1262)/* instance_size */
	, sizeof (CipherSuite_t1262)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CipherSuite_t1262_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 36/* method_count */
	, 19/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CipherSuiteCollection
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuiteCollecti.h"
// Metadata Definition Mono.Security.Protocol.Tls.CipherSuiteCollection
extern TypeInfo CipherSuiteCollection_t1263_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CipherSuiteCollection
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuiteCollectiMethodDeclarations.h"
static const EncodedMethodIndex CipherSuiteCollection_t1263_VTable[20] = 
{
	626,
	601,
	627,
	628,
	1794,
	1795,
	1796,
	1797,
	1798,
	1799,
	1800,
	1801,
	1802,
	1803,
	1804,
	1805,
	1806,
	1807,
	1808,
	1809,
};
static const Il2CppType* CipherSuiteCollection_t1263_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1097_0_0_0,
	&ICollection_t1528_0_0_0,
	&IList_t1487_0_0_0,
};
static Il2CppInterfaceOffsetPair CipherSuiteCollection_t1263_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1097_0_0_0, 4},
	{ &ICollection_t1528_0_0_0, 5},
	{ &IList_t1487_0_0_0, 9},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CipherSuiteCollection_t1263_0_0_0;
extern const Il2CppType CipherSuiteCollection_t1263_1_0_0;
struct CipherSuiteCollection_t1263;
const Il2CppTypeDefinitionMetadata CipherSuiteCollection_t1263_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CipherSuiteCollection_t1263_InterfacesTypeInfos/* implementedInterfaces */
	, CipherSuiteCollection_t1263_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CipherSuiteCollection_t1263_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5356/* fieldStart */
	, 6638/* methodStart */
	, -1/* eventStart */
	, 1315/* propertyStart */

};
TypeInfo CipherSuiteCollection_t1263_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CipherSuiteCollection"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &CipherSuiteCollection_t1263_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2338/* custom_attributes_cache */
	, &CipherSuiteCollection_t1263_0_0_0/* byval_arg */
	, &CipherSuiteCollection_t1263_1_0_0/* this_arg */
	, &CipherSuiteCollection_t1263_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CipherSuiteCollection_t1263)/* instance_size */
	, sizeof (CipherSuiteCollection_t1263)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 27/* method_count */
	, 9/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CipherSuiteFactory
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuiteFactory.h"
// Metadata Definition Mono.Security.Protocol.Tls.CipherSuiteFactory
extern TypeInfo CipherSuiteFactory_t1264_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CipherSuiteFactory
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSuiteFactoryMethodDeclarations.h"
static const EncodedMethodIndex CipherSuiteFactory_t1264_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CipherSuiteFactory_t1264_0_0_0;
extern const Il2CppType CipherSuiteFactory_t1264_1_0_0;
struct CipherSuiteFactory_t1264;
const Il2CppTypeDefinitionMetadata CipherSuiteFactory_t1264_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CipherSuiteFactory_t1264_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6665/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CipherSuiteFactory_t1264_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CipherSuiteFactory"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &CipherSuiteFactory_t1264_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CipherSuiteFactory_t1264_0_0_0/* byval_arg */
	, &CipherSuiteFactory_t1264_1_0_0/* this_arg */
	, &CipherSuiteFactory_t1264_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CipherSuiteFactory_t1264)/* instance_size */
	, sizeof (CipherSuiteFactory_t1264)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ClientContext
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientContext.h"
// Metadata Definition Mono.Security.Protocol.Tls.ClientContext
extern TypeInfo ClientContext_t1266_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ClientContext
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientContextMethodDeclarations.h"
static const EncodedMethodIndex ClientContext_t1266_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1810,
	1811,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ClientContext_t1266_0_0_0;
extern const Il2CppType ClientContext_t1266_1_0_0;
extern const Il2CppType Context_t1261_0_0_0;
struct ClientContext_t1266;
const Il2CppTypeDefinitionMetadata ClientContext_t1266_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Context_t1261_0_0_0/* parent */
	, ClientContext_t1266_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5358/* fieldStart */
	, 6668/* methodStart */
	, -1/* eventStart */
	, 1324/* propertyStart */

};
TypeInfo ClientContext_t1266_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientContext"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &ClientContext_t1266_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientContext_t1266_0_0_0/* byval_arg */
	, &ClientContext_t1266_1_0_0/* this_arg */
	, &ClientContext_t1266_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientContext_t1266)/* instance_size */
	, sizeof (ClientContext_t1266)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ClientRecordProtocol
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRecordProtoco.h"
// Metadata Definition Mono.Security.Protocol.Tls.ClientRecordProtocol
extern TypeInfo ClientRecordProtocol_t1267_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ClientRecordProtocol
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRecordProtocoMethodDeclarations.h"
static const EncodedMethodIndex ClientRecordProtocol_t1267_VTable[8] = 
{
	626,
	601,
	627,
	628,
	1812,
	1813,
	1814,
	1815,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ClientRecordProtocol_t1267_0_0_0;
extern const Il2CppType ClientRecordProtocol_t1267_1_0_0;
extern const Il2CppType RecordProtocol_t1268_0_0_0;
struct ClientRecordProtocol_t1267;
const Il2CppTypeDefinitionMetadata ClientRecordProtocol_t1267_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &RecordProtocol_t1268_0_0_0/* parent */
	, ClientRecordProtocol_t1267_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 6673/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ClientRecordProtocol_t1267_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientRecordProtocol"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &ClientRecordProtocol_t1267_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientRecordProtocol_t1267_0_0_0/* byval_arg */
	, &ClientRecordProtocol_t1267_1_0_0/* this_arg */
	, &ClientRecordProtocol_t1267_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientRecordProtocol_t1267)/* instance_size */
	, sizeof (ClientRecordProtocol_t1267)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ClientSessionInfo
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSessionInfo.h"
// Metadata Definition Mono.Security.Protocol.Tls.ClientSessionInfo
extern TypeInfo ClientSessionInfo_t1269_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ClientSessionInfo
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSessionInfoMethodDeclarations.h"
static const EncodedMethodIndex ClientSessionInfo_t1269_VTable[5] = 
{
	626,
	1816,
	627,
	628,
	1817,
};
static const Il2CppType* ClientSessionInfo_t1269_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair ClientSessionInfo_t1269_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ClientSessionInfo_t1269_0_0_0;
extern const Il2CppType ClientSessionInfo_t1269_1_0_0;
struct ClientSessionInfo_t1269;
const Il2CppTypeDefinitionMetadata ClientSessionInfo_t1269_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ClientSessionInfo_t1269_InterfacesTypeInfos/* implementedInterfaces */
	, ClientSessionInfo_t1269_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ClientSessionInfo_t1269_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5360/* fieldStart */
	, 6678/* methodStart */
	, -1/* eventStart */
	, 1326/* propertyStart */

};
TypeInfo ClientSessionInfo_t1269_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientSessionInfo"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &ClientSessionInfo_t1269_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientSessionInfo_t1269_0_0_0/* byval_arg */
	, &ClientSessionInfo_t1269_1_0_0/* this_arg */
	, &ClientSessionInfo_t1269_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientSessionInfo_t1269)/* instance_size */
	, sizeof (ClientSessionInfo_t1269)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ClientSessionInfo_t1269_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 3/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ClientSessionCache
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSessionCache.h"
// Metadata Definition Mono.Security.Protocol.Tls.ClientSessionCache
extern TypeInfo ClientSessionCache_t1270_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ClientSessionCache
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSessionCacheMethodDeclarations.h"
static const EncodedMethodIndex ClientSessionCache_t1270_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ClientSessionCache_t1270_0_0_0;
extern const Il2CppType ClientSessionCache_t1270_1_0_0;
struct ClientSessionCache_t1270;
const Il2CppTypeDefinitionMetadata ClientSessionCache_t1270_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ClientSessionCache_t1270_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5366/* fieldStart */
	, 6690/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ClientSessionCache_t1270_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientSessionCache"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &ClientSessionCache_t1270_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientSessionCache_t1270_0_0_0/* byval_arg */
	, &ClientSessionCache_t1270_1_0_0/* this_arg */
	, &ClientSessionCache_t1270_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientSessionCache_t1270)/* instance_size */
	, sizeof (ClientSessionCache_t1270)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ClientSessionCache_t1270_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
// Metadata Definition Mono.Security.Protocol.Tls.ContentType
extern TypeInfo ContentType_t1271_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTypeMethodDeclarations.h"
static const EncodedMethodIndex ContentType_t1271_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ContentType_t1271_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ContentType_t1271_0_0_0;
extern const Il2CppType ContentType_t1271_1_0_0;
const Il2CppTypeDefinitionMetadata ContentType_t1271_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ContentType_t1271_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ContentType_t1271_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5368/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ContentType_t1271_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentType"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ContentType_t1271_0_0_0/* byval_arg */
	, &ContentType_t1271_1_0_0/* this_arg */
	, &ContentType_t1271_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentType_t1271)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ContentType_t1271)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8448/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Context
#include "Mono_Security_Mono_Security_Protocol_Tls_Context.h"
// Metadata Definition Mono.Security.Protocol.Tls.Context
extern TypeInfo Context_t1261_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Context
#include "Mono_Security_Mono_Security_Protocol_Tls_ContextMethodDeclarations.h"
static const EncodedMethodIndex Context_t1261_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1818,
	1811,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType Context_t1261_1_0_0;
struct Context_t1261;
const Il2CppTypeDefinitionMetadata Context_t1261_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Context_t1261_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5373/* fieldStart */
	, 6696/* methodStart */
	, -1/* eventStart */
	, 1329/* propertyStart */

};
TypeInfo Context_t1261_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "Context"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Context_t1261_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Context_t1261_0_0_0/* byval_arg */
	, &Context_t1261_1_0_0/* this_arg */
	, &Context_t1261_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Context_t1261)/* instance_size */
	, sizeof (Context_t1261)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 62/* method_count */
	, 31/* property_count */
	, 30/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
// Metadata Definition Mono.Security.Protocol.Tls.ExchangeAlgorithmType
extern TypeInfo ExchangeAlgorithmType_t1276_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTyMethodDeclarations.h"
static const EncodedMethodIndex ExchangeAlgorithmType_t1276_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ExchangeAlgorithmType_t1276_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ExchangeAlgorithmType_t1276_0_0_0;
extern const Il2CppType ExchangeAlgorithmType_t1276_1_0_0;
const Il2CppTypeDefinitionMetadata ExchangeAlgorithmType_t1276_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExchangeAlgorithmType_t1276_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ExchangeAlgorithmType_t1276_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5403/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ExchangeAlgorithmType_t1276_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExchangeAlgorithmType"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExchangeAlgorithmType_t1276_0_0_0/* byval_arg */
	, &ExchangeAlgorithmType_t1276_1_0_0/* this_arg */
	, &ExchangeAlgorithmType_t1276_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExchangeAlgorithmType_t1276)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ExchangeAlgorithmType_t1276)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
// Metadata Definition Mono.Security.Protocol.Tls.HandshakeState
extern TypeInfo HandshakeState_t1277_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeStateMethodDeclarations.h"
static const EncodedMethodIndex HandshakeState_t1277_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair HandshakeState_t1277_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType HandshakeState_t1277_0_0_0;
extern const Il2CppType HandshakeState_t1277_1_0_0;
const Il2CppTypeDefinitionMetadata HandshakeState_t1277_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HandshakeState_t1277_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, HandshakeState_t1277_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5409/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HandshakeState_t1277_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "HandshakeState"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HandshakeState_t1277_0_0_0/* byval_arg */
	, &HandshakeState_t1277_1_0_0/* this_arg */
	, &HandshakeState_t1277_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HandshakeState_t1277)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HandshakeState_t1277)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8448/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
// Metadata Definition Mono.Security.Protocol.Tls.HashAlgorithmType
extern TypeInfo HashAlgorithmType_t1278_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmTypeMethodDeclarations.h"
static const EncodedMethodIndex HashAlgorithmType_t1278_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair HashAlgorithmType_t1278_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType HashAlgorithmType_t1278_0_0_0;
extern const Il2CppType HashAlgorithmType_t1278_1_0_0;
const Il2CppTypeDefinitionMetadata HashAlgorithmType_t1278_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HashAlgorithmType_t1278_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, HashAlgorithmType_t1278_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5413/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HashAlgorithmType_t1278_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashAlgorithmType"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HashAlgorithmType_t1278_0_0_0/* byval_arg */
	, &HashAlgorithmType_t1278_1_0_0/* this_arg */
	, &HashAlgorithmType_t1278_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HashAlgorithmType_t1278)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HashAlgorithmType_t1278)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.HttpsClientStream
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClientStream.h"
// Metadata Definition Mono.Security.Protocol.Tls.HttpsClientStream
extern TypeInfo HttpsClientStream_t1282_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.HttpsClientStream
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClientStreamMethodDeclarations.h"
static const EncodedMethodIndex HttpsClientStream_t1282_VTable[33] = 
{
	626,
	1819,
	627,
	628,
	1820,
	1821,
	1822,
	1823,
	1824,
	1825,
	1826,
	1827,
	1828,
	1829,
	1830,
	1831,
	1832,
	1833,
	1834,
	1835,
	1836,
	1837,
	1838,
	1839,
	1840,
	1841,
	1842,
	1843,
	1844,
	1845,
	1846,
	1847,
	1848,
};
static Il2CppInterfaceOffsetPair HttpsClientStream_t1282_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType HttpsClientStream_t1282_0_0_0;
extern const Il2CppType HttpsClientStream_t1282_1_0_0;
extern const Il2CppType SslClientStream_t1265_0_0_0;
struct HttpsClientStream_t1282;
const Il2CppTypeDefinitionMetadata HttpsClientStream_t1282_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HttpsClientStream_t1282_InterfacesOffsets/* interfaceOffsets */
	, &SslClientStream_t1265_0_0_0/* parent */
	, HttpsClientStream_t1282_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5417/* fieldStart */
	, 6758/* methodStart */
	, -1/* eventStart */
	, 1360/* propertyStart */

};
TypeInfo HttpsClientStream_t1282_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "HttpsClientStream"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &HttpsClientStream_t1282_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HttpsClientStream_t1282_0_0_0/* byval_arg */
	, &HttpsClientStream_t1282_1_0_0/* this_arg */
	, &HttpsClientStream_t1282_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HttpsClientStream_t1282)/* instance_size */
	, sizeof (HttpsClientStream_t1282)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(HttpsClientStream_t1282_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.RecordProtocol
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol.h"
// Metadata Definition Mono.Security.Protocol.Tls.RecordProtocol
extern TypeInfo RecordProtocol_t1268_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.RecordProtocol
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocolMethodDeclarations.h"
extern const Il2CppType ReceiveRecordAsyncResult_t1285_0_0_0;
extern const Il2CppType SendRecordAsyncResult_t1287_0_0_0;
static const Il2CppType* RecordProtocol_t1268_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ReceiveRecordAsyncResult_t1285_0_0_0,
	&SendRecordAsyncResult_t1287_0_0_0,
};
static const EncodedMethodIndex RecordProtocol_t1268_VTable[8] = 
{
	626,
	601,
	627,
	628,
	1812,
	0,
	1814,
	1849,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType RecordProtocol_t1268_1_0_0;
struct RecordProtocol_t1268;
const Il2CppTypeDefinitionMetadata RecordProtocol_t1268_DefinitionMetadata = 
{
	NULL/* declaringType */
	, RecordProtocol_t1268_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, RecordProtocol_t1268_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5421/* fieldStart */
	, 6763/* methodStart */
	, -1/* eventStart */
	, 1361/* propertyStart */

};
TypeInfo RecordProtocol_t1268_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "RecordProtocol"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &RecordProtocol_t1268_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RecordProtocol_t1268_0_0_0/* byval_arg */
	, &RecordProtocol_t1268_1_0_0/* this_arg */
	, &RecordProtocol_t1268_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RecordProtocol_t1268)/* instance_size */
	, sizeof (RecordProtocol_t1268)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RecordProtocol_t1268_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol_Rece.h"
// Metadata Definition Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
extern TypeInfo ReceiveRecordAsyncResult_t1285_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol_ReceMethodDeclarations.h"
static const EncodedMethodIndex ReceiveRecordAsyncResult_t1285_VTable[7] = 
{
	626,
	601,
	627,
	628,
	1850,
	1851,
	1852,
};
extern const Il2CppType IAsyncResult_t216_0_0_0;
static const Il2CppType* ReceiveRecordAsyncResult_t1285_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t216_0_0_0,
};
static Il2CppInterfaceOffsetPair ReceiveRecordAsyncResult_t1285_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t216_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ReceiveRecordAsyncResult_t1285_1_0_0;
struct ReceiveRecordAsyncResult_t1285;
const Il2CppTypeDefinitionMetadata ReceiveRecordAsyncResult_t1285_DefinitionMetadata = 
{
	&RecordProtocol_t1268_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ReceiveRecordAsyncResult_t1285_InterfacesTypeInfos/* implementedInterfaces */
	, ReceiveRecordAsyncResult_t1285_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReceiveRecordAsyncResult_t1285_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5424/* fieldStart */
	, 6794/* methodStart */
	, -1/* eventStart */
	, 1362/* propertyStart */

};
TypeInfo ReceiveRecordAsyncResult_t1285_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReceiveRecordAsyncResult"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ReceiveRecordAsyncResult_t1285_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReceiveRecordAsyncResult_t1285_0_0_0/* byval_arg */
	, &ReceiveRecordAsyncResult_t1285_1_0_0/* this_arg */
	, &ReceiveRecordAsyncResult_t1285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReceiveRecordAsyncResult_t1285)/* instance_size */
	, sizeof (ReceiveRecordAsyncResult_t1285)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 8/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol_Send.h"
// Metadata Definition Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult
extern TypeInfo SendRecordAsyncResult_t1287_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.RecordProtocol/SendRecordAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordProtocol_SendMethodDeclarations.h"
static const EncodedMethodIndex SendRecordAsyncResult_t1287_VTable[7] = 
{
	626,
	601,
	627,
	628,
	1853,
	1854,
	1855,
};
static const Il2CppType* SendRecordAsyncResult_t1287_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t216_0_0_0,
};
static Il2CppInterfaceOffsetPair SendRecordAsyncResult_t1287_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t216_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SendRecordAsyncResult_t1287_1_0_0;
struct SendRecordAsyncResult_t1287;
const Il2CppTypeDefinitionMetadata SendRecordAsyncResult_t1287_DefinitionMetadata = 
{
	&RecordProtocol_t1268_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, SendRecordAsyncResult_t1287_InterfacesTypeInfos/* implementedInterfaces */
	, SendRecordAsyncResult_t1287_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendRecordAsyncResult_t1287_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5433/* fieldStart */
	, 6806/* methodStart */
	, -1/* eventStart */
	, 1370/* propertyStart */

};
TypeInfo SendRecordAsyncResult_t1287_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendRecordAsyncResult"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &SendRecordAsyncResult_t1287_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendRecordAsyncResult_t1287_0_0_0/* byval_arg */
	, &SendRecordAsyncResult_t1287_1_0_0/* this_arg */
	, &SendRecordAsyncResult_t1287_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendRecordAsyncResult_t1287)/* instance_size */
	, sizeof (SendRecordAsyncResult_t1287)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.RSASslSignatureDeformatter
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSignatureDefo.h"
// Metadata Definition Mono.Security.Protocol.Tls.RSASslSignatureDeformatter
extern TypeInfo RSASslSignatureDeformatter_t1288_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.RSASslSignatureDeformatter
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSignatureDefoMethodDeclarations.h"
static const EncodedMethodIndex RSASslSignatureDeformatter_t1288_VTable[7] = 
{
	626,
	601,
	627,
	628,
	1856,
	1857,
	1858,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType RSASslSignatureDeformatter_t1288_0_0_0;
extern const Il2CppType RSASslSignatureDeformatter_t1288_1_0_0;
extern const Il2CppType AsymmetricSignatureDeformatter_t1289_0_0_0;
struct RSASslSignatureDeformatter_t1288;
const Il2CppTypeDefinitionMetadata RSASslSignatureDeformatter_t1288_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureDeformatter_t1289_0_0_0/* parent */
	, RSASslSignatureDeformatter_t1288_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5440/* fieldStart */
	, 6815/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSASslSignatureDeformatter_t1288_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSASslSignatureDeformatter"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &RSASslSignatureDeformatter_t1288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RSASslSignatureDeformatter_t1288_0_0_0/* byval_arg */
	, &RSASslSignatureDeformatter_t1288_1_0_0/* this_arg */
	, &RSASslSignatureDeformatter_t1288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSASslSignatureDeformatter_t1288)/* instance_size */
	, sizeof (RSASslSignatureDeformatter_t1288)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RSASslSignatureDeformatter_t1288_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.RSASslSignatureFormatter
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSignatureForm.h"
// Metadata Definition Mono.Security.Protocol.Tls.RSASslSignatureFormatter
extern TypeInfo RSASslSignatureFormatter_t1290_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.RSASslSignatureFormatter
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSignatureFormMethodDeclarations.h"
static const EncodedMethodIndex RSASslSignatureFormatter_t1290_VTable[7] = 
{
	626,
	601,
	627,
	628,
	1859,
	1860,
	1861,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType RSASslSignatureFormatter_t1290_0_0_0;
extern const Il2CppType RSASslSignatureFormatter_t1290_1_0_0;
extern const Il2CppType AsymmetricSignatureFormatter_t1291_0_0_0;
struct RSASslSignatureFormatter_t1290;
const Il2CppTypeDefinitionMetadata RSASslSignatureFormatter_t1290_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &AsymmetricSignatureFormatter_t1291_0_0_0/* parent */
	, RSASslSignatureFormatter_t1290_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5443/* fieldStart */
	, 6819/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo RSASslSignatureFormatter_t1290_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "RSASslSignatureFormatter"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &RSASslSignatureFormatter_t1290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RSASslSignatureFormatter_t1290_0_0_0/* byval_arg */
	, &RSASslSignatureFormatter_t1290_1_0_0/* this_arg */
	, &RSASslSignatureFormatter_t1290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RSASslSignatureFormatter_t1290)/* instance_size */
	, sizeof (RSASslSignatureFormatter_t1290)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RSASslSignatureFormatter_t1290_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
// Metadata Definition Mono.Security.Protocol.Tls.SecurityCompressionType
extern TypeInfo SecurityCompressionType_t1292_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompressionMethodDeclarations.h"
static const EncodedMethodIndex SecurityCompressionType_t1292_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SecurityCompressionType_t1292_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SecurityCompressionType_t1292_0_0_0;
extern const Il2CppType SecurityCompressionType_t1292_1_0_0;
const Il2CppTypeDefinitionMetadata SecurityCompressionType_t1292_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityCompressionType_t1292_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SecurityCompressionType_t1292_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5446/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityCompressionType_t1292_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityCompressionType"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityCompressionType_t1292_0_0_0/* byval_arg */
	, &SecurityCompressionType_t1292_1_0_0/* this_arg */
	, &SecurityCompressionType_t1292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityCompressionType_t1292)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityCompressionType_t1292)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SecurityParameters
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityParameters.h"
// Metadata Definition Mono.Security.Protocol.Tls.SecurityParameters
extern TypeInfo SecurityParameters_t1274_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SecurityParameters
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityParametersMethodDeclarations.h"
static const EncodedMethodIndex SecurityParameters_t1274_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SecurityParameters_t1274_0_0_0;
extern const Il2CppType SecurityParameters_t1274_1_0_0;
struct SecurityParameters_t1274;
const Il2CppTypeDefinitionMetadata SecurityParameters_t1274_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SecurityParameters_t1274_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5449/* fieldStart */
	, 6823/* methodStart */
	, -1/* eventStart */
	, 1376/* propertyStart */

};
TypeInfo SecurityParameters_t1274_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityParameters"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &SecurityParameters_t1274_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SecurityParameters_t1274_0_0_0/* byval_arg */
	, &SecurityParameters_t1274_1_0_0/* this_arg */
	, &SecurityParameters_t1274_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityParameters_t1274)/* instance_size */
	, sizeof (SecurityParameters_t1274)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
// Metadata Definition Mono.Security.Protocol.Tls.SecurityProtocolType
extern TypeInfo SecurityProtocolType_t1293_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTypMethodDeclarations.h"
static const EncodedMethodIndex SecurityProtocolType_t1293_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair SecurityProtocolType_t1293_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SecurityProtocolType_t1293_0_0_0;
extern const Il2CppType SecurityProtocolType_t1293_1_0_0;
const Il2CppTypeDefinitionMetadata SecurityProtocolType_t1293_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SecurityProtocolType_t1293_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, SecurityProtocolType_t1293_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5452/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SecurityProtocolType_t1293_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SecurityProtocolType"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2345/* custom_attributes_cache */
	, &SecurityProtocolType_t1293_0_0_0/* byval_arg */
	, &SecurityProtocolType_t1293_1_0_0/* this_arg */
	, &SecurityProtocolType_t1293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SecurityProtocolType_t1293)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SecurityProtocolType_t1293)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ServerContext
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerContext.h"
// Metadata Definition Mono.Security.Protocol.Tls.ServerContext
extern TypeInfo ServerContext_t1294_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ServerContext
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerContextMethodDeclarations.h"
static const EncodedMethodIndex ServerContext_t1294_VTable[6] = 
{
	626,
	601,
	627,
	628,
	1818,
	1811,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ServerContext_t1294_0_0_0;
extern const Il2CppType ServerContext_t1294_1_0_0;
struct ServerContext_t1294;
const Il2CppTypeDefinitionMetadata ServerContext_t1294_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Context_t1261_0_0_0/* parent */
	, ServerContext_t1294_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ServerContext_t1294_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ServerContext"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &ServerContext_t1294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ServerContext_t1294_0_0_0/* byval_arg */
	, &ServerContext_t1294_1_0_0/* this_arg */
	, &ServerContext_t1294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ServerContext_t1294)/* instance_size */
	, sizeof (ServerContext_t1294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.ValidationResult
#include "Mono_Security_Mono_Security_Protocol_Tls_ValidationResult.h"
// Metadata Definition Mono.Security.Protocol.Tls.ValidationResult
extern TypeInfo ValidationResult_t1295_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.ValidationResult
#include "Mono_Security_Mono_Security_Protocol_Tls_ValidationResultMethodDeclarations.h"
static const EncodedMethodIndex ValidationResult_t1295_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ValidationResult_t1295_0_0_0;
extern const Il2CppType ValidationResult_t1295_1_0_0;
struct ValidationResult_t1295;
const Il2CppTypeDefinitionMetadata ValidationResult_t1295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ValidationResult_t1295_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5457/* fieldStart */
	, 6831/* methodStart */
	, -1/* eventStart */
	, 1379/* propertyStart */

};
TypeInfo ValidationResult_t1295_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ValidationResult"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &ValidationResult_t1295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ValidationResult_t1295_0_0_0/* byval_arg */
	, &ValidationResult_t1295_1_0_0/* this_arg */
	, &ValidationResult_t1295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ValidationResult_t1295)/* instance_size */
	, sizeof (ValidationResult_t1295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SslClientStream
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClientStream.h"
// Metadata Definition Mono.Security.Protocol.Tls.SslClientStream
extern TypeInfo SslClientStream_t1265_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SslClientStream
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClientStreamMethodDeclarations.h"
static const EncodedMethodIndex SslClientStream_t1265_VTable[33] = 
{
	626,
	1819,
	627,
	628,
	1820,
	1821,
	1822,
	1823,
	1824,
	1825,
	1826,
	1827,
	1828,
	1829,
	1830,
	1831,
	1832,
	1833,
	1834,
	1835,
	1836,
	1837,
	1838,
	1839,
	1840,
	1841,
	1842,
	1843,
	1844,
	1845,
	1846,
	1862,
	1848,
};
static Il2CppInterfaceOffsetPair SslClientStream_t1265_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SslClientStream_t1265_1_0_0;
extern const Il2CppType SslStreamBase_t1298_0_0_0;
struct SslClientStream_t1265;
const Il2CppTypeDefinitionMetadata SslClientStream_t1265_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SslClientStream_t1265_InterfacesOffsets/* interfaceOffsets */
	, &SslStreamBase_t1298_0_0_0/* parent */
	, SslClientStream_t1265_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5459/* fieldStart */
	, 6833/* methodStart */
	, 17/* eventStart */
	, 1381/* propertyStart */

};
TypeInfo SslClientStream_t1265_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SslClientStream"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &SslClientStream_t1265_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SslClientStream_t1265_0_0_0/* byval_arg */
	, &SslClientStream_t1265_1_0_0/* this_arg */
	, &SslClientStream_t1265_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SslClientStream_t1265)/* instance_size */
	, sizeof (SslClientStream_t1265)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 36/* method_count */
	, 7/* property_count */
	, 4/* field_count */
	, 4/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SslCipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipherSuite.h"
// Metadata Definition Mono.Security.Protocol.Tls.SslCipherSuite
extern TypeInfo SslCipherSuite_t1299_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SslCipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipherSuiteMethodDeclarations.h"
static const EncodedMethodIndex SslCipherSuite_t1299_VTable[8] = 
{
	626,
	601,
	627,
	628,
	1863,
	1864,
	1865,
	1866,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SslCipherSuite_t1299_0_0_0;
extern const Il2CppType SslCipherSuite_t1299_1_0_0;
struct SslCipherSuite_t1299;
const Il2CppTypeDefinitionMetadata SslCipherSuite_t1299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CipherSuite_t1262_0_0_0/* parent */
	, SslCipherSuite_t1299_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5463/* fieldStart */
	, 6869/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SslCipherSuite_t1299_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SslCipherSuite"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &SslCipherSuite_t1299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SslCipherSuite_t1299_0_0_0/* byval_arg */
	, &SslCipherSuite_t1299_1_0_0/* this_arg */
	, &SslCipherSuite_t1299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SslCipherSuite_t1299)/* instance_size */
	, sizeof (SslCipherSuite_t1299)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SslHandshakeHash
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandshakeHash.h"
// Metadata Definition Mono.Security.Protocol.Tls.SslHandshakeHash
extern TypeInfo SslHandshakeHash_t1300_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SslHandshakeHash
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandshakeHashMethodDeclarations.h"
static const EncodedMethodIndex SslHandshakeHash_t1300_VTable[15] = 
{
	626,
	601,
	627,
	628,
	1693,
	1694,
	1695,
	1696,
	1694,
	1697,
	1867,
	1868,
	1698,
	1869,
	1699,
};
static Il2CppInterfaceOffsetPair SslHandshakeHash_t1300_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
	{ &ICryptoTransform_t1188_0_0_0, 5},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SslHandshakeHash_t1300_0_0_0;
extern const Il2CppType SslHandshakeHash_t1300_1_0_0;
struct SslHandshakeHash_t1300;
const Il2CppTypeDefinitionMetadata SslHandshakeHash_t1300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SslHandshakeHash_t1300_InterfacesOffsets/* interfaceOffsets */
	, &HashAlgorithm_t1217_0_0_0/* parent */
	, SslHandshakeHash_t1300_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5466/* fieldStart */
	, 6875/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SslHandshakeHash_t1300_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SslHandshakeHash"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &SslHandshakeHash_t1300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SslHandshakeHash_t1300_0_0_0/* byval_arg */
	, &SslHandshakeHash_t1300_1_0_0/* this_arg */
	, &SslHandshakeHash_t1300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SslHandshakeHash_t1300)/* instance_size */
	, sizeof (SslHandshakeHash_t1300)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SslStreamBase
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamBase.h"
// Metadata Definition Mono.Security.Protocol.Tls.SslStreamBase
extern TypeInfo SslStreamBase_t1298_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SslStreamBase
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamBaseMethodDeclarations.h"
extern const Il2CppType InternalAsyncResult_t1301_0_0_0;
static const Il2CppType* SslStreamBase_t1298_il2cpp_TypeInfo__nestedTypes[1] =
{
	&InternalAsyncResult_t1301_0_0_0,
};
static const EncodedMethodIndex SslStreamBase_t1298_VTable[31] = 
{
	626,
	1870,
	627,
	628,
	1820,
	1821,
	1822,
	1823,
	1824,
	1825,
	1826,
	1871,
	1828,
	1829,
	1830,
	1831,
	1832,
	1833,
	1834,
	1835,
	1836,
	1837,
	1838,
	1839,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
static const Il2CppType* SslStreamBase_t1298_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair SslStreamBase_t1298_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType SslStreamBase_t1298_1_0_0;
extern const Il2CppType Stream_t1284_0_0_0;
struct SslStreamBase_t1298;
const Il2CppTypeDefinitionMetadata SslStreamBase_t1298_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SslStreamBase_t1298_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, SslStreamBase_t1298_InterfacesTypeInfos/* implementedInterfaces */
	, SslStreamBase_t1298_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t1284_0_0_0/* parent */
	, SslStreamBase_t1298_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5474/* fieldStart */
	, 6881/* methodStart */
	, -1/* eventStart */
	, 1388/* propertyStart */

};
TypeInfo SslStreamBase_t1298_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "SslStreamBase"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &SslStreamBase_t1298_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SslStreamBase_t1298_0_0_0/* byval_arg */
	, &SslStreamBase_t1298_1_0_0/* this_arg */
	, &SslStreamBase_t1298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SslStreamBase_t1298)/* instance_size */
	, sizeof (SslStreamBase_t1298)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SslStreamBase_t1298_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 55/* method_count */
	, 17/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 31/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamBase_Inter.h"
// Metadata Definition Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
extern TypeInfo InternalAsyncResult_t1301_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.SslStreamBase/InternalAsyncResult
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStreamBase_InterMethodDeclarations.h"
static const EncodedMethodIndex InternalAsyncResult_t1301_VTable[7] = 
{
	626,
	601,
	627,
	628,
	1872,
	1873,
	1874,
};
static const Il2CppType* InternalAsyncResult_t1301_InterfacesTypeInfos[] = 
{
	&IAsyncResult_t216_0_0_0,
};
static Il2CppInterfaceOffsetPair InternalAsyncResult_t1301_InterfacesOffsets[] = 
{
	{ &IAsyncResult_t216_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType InternalAsyncResult_t1301_1_0_0;
struct InternalAsyncResult_t1301;
const Il2CppTypeDefinitionMetadata InternalAsyncResult_t1301_DefinitionMetadata = 
{
	&SslStreamBase_t1298_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, InternalAsyncResult_t1301_InterfacesTypeInfos/* implementedInterfaces */
	, InternalAsyncResult_t1301_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InternalAsyncResult_t1301_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5489/* fieldStart */
	, 6936/* methodStart */
	, -1/* eventStart */
	, 1405/* propertyStart */

};
TypeInfo InternalAsyncResult_t1301_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalAsyncResult"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &InternalAsyncResult_t1301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InternalAsyncResult_t1301_0_0_0/* byval_arg */
	, &InternalAsyncResult_t1301_1_0_0/* this_arg */
	, &InternalAsyncResult_t1301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalAsyncResult_t1301)/* instance_size */
	, sizeof (InternalAsyncResult_t1301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 11/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.TlsCipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipherSuite.h"
// Metadata Definition Mono.Security.Protocol.Tls.TlsCipherSuite
extern TypeInfo TlsCipherSuite_t1302_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.TlsCipherSuite
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipherSuiteMethodDeclarations.h"
static const EncodedMethodIndex TlsCipherSuite_t1302_VTable[8] = 
{
	626,
	601,
	627,
	628,
	1875,
	1876,
	1877,
	1878,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsCipherSuite_t1302_0_0_0;
extern const Il2CppType TlsCipherSuite_t1302_1_0_0;
struct TlsCipherSuite_t1302;
const Il2CppTypeDefinitionMetadata TlsCipherSuite_t1302_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &CipherSuite_t1262_0_0_0/* parent */
	, TlsCipherSuite_t1302_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5501/* fieldStart */
	, 6952/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsCipherSuite_t1302_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsCipherSuite"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &TlsCipherSuite_t1302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsCipherSuite_t1302_0_0_0/* byval_arg */
	, &TlsCipherSuite_t1302_1_0_0/* this_arg */
	, &TlsCipherSuite_t1302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsCipherSuite_t1302)/* instance_size */
	, sizeof (TlsCipherSuite_t1302)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.TlsClientSettings
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClientSettings.h"
// Metadata Definition Mono.Security.Protocol.Tls.TlsClientSettings
extern TypeInfo TlsClientSettings_t1273_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.TlsClientSettings
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClientSettingsMethodDeclarations.h"
static const EncodedMethodIndex TlsClientSettings_t1273_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsClientSettings_t1273_0_0_0;
extern const Il2CppType TlsClientSettings_t1273_1_0_0;
struct TlsClientSettings_t1273;
const Il2CppTypeDefinitionMetadata TlsClientSettings_t1273_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TlsClientSettings_t1273_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5503/* fieldStart */
	, 6957/* methodStart */
	, -1/* eventStart */
	, 1416/* propertyStart */

};
TypeInfo TlsClientSettings_t1273_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsClientSettings"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &TlsClientSettings_t1273_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsClientSettings_t1273_0_0_0/* byval_arg */
	, &TlsClientSettings_t1273_1_0_0/* this_arg */
	, &TlsClientSettings_t1273_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsClientSettings_t1273)/* instance_size */
	, sizeof (TlsClientSettings_t1273)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048832/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.TlsException
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsException.h"
// Metadata Definition Mono.Security.Protocol.Tls.TlsException
extern TypeInfo TlsException_t1305_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.TlsException
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExceptionMethodDeclarations.h"
static const EncodedMethodIndex TlsException_t1305_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair TlsException_t1305_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsException_t1305_0_0_0;
extern const Il2CppType TlsException_t1305_1_0_0;
extern const Il2CppType Exception_t520_0_0_0;
struct TlsException_t1305;
const Il2CppTypeDefinitionMetadata TlsException_t1305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsException_t1305_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, TlsException_t1305_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5507/* fieldStart */
	, 6965/* methodStart */
	, -1/* eventStart */
	, 1419/* propertyStart */

};
TypeInfo TlsException_t1305_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsException"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &TlsException_t1305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsException_t1305_0_0_0/* byval_arg */
	, &TlsException_t1305_1_0_0/* this_arg */
	, &TlsException_t1305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsException_t1305)/* instance_size */
	, sizeof (TlsException_t1305)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.TlsServerSettings
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServerSettings.h"
// Metadata Definition Mono.Security.Protocol.Tls.TlsServerSettings
extern TypeInfo TlsServerSettings_t1272_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.TlsServerSettings
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServerSettingsMethodDeclarations.h"
static const EncodedMethodIndex TlsServerSettings_t1272_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerSettings_t1272_0_0_0;
extern const Il2CppType TlsServerSettings_t1272_1_0_0;
struct TlsServerSettings_t1272;
const Il2CppTypeDefinitionMetadata TlsServerSettings_t1272_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TlsServerSettings_t1272_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5508/* fieldStart */
	, 6972/* methodStart */
	, -1/* eventStart */
	, 1420/* propertyStart */

};
TypeInfo TlsServerSettings_t1272_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerSettings"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &TlsServerSettings_t1272_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerSettings_t1272_0_0_0/* byval_arg */
	, &TlsServerSettings_t1272_1_0_0/* this_arg */
	, &TlsServerSettings_t1272_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerSettings_t1272)/* instance_size */
	, sizeof (TlsServerSettings_t1272)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 8/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.TlsStream
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream.h"
// Metadata Definition Mono.Security.Protocol.Tls.TlsStream
extern TypeInfo TlsStream_t1275_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.TlsStream
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStreamMethodDeclarations.h"
static const EncodedMethodIndex TlsStream_t1275_VTable[24] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
};
static Il2CppInterfaceOffsetPair TlsStream_t1275_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsStream_t1275_0_0_0;
extern const Il2CppType TlsStream_t1275_1_0_0;
struct TlsStream_t1275;
const Il2CppTypeDefinitionMetadata TlsStream_t1275_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsStream_t1275_InterfacesOffsets/* interfaceOffsets */
	, &Stream_t1284_0_0_0/* parent */
	, TlsStream_t1275_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5516/* fieldStart */
	, 6986/* methodStart */
	, -1/* eventStart */
	, 1428/* propertyStart */

};
TypeInfo TlsStream_t1275_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsStream"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &TlsStream_t1275_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsStream_t1275_0_0_0/* byval_arg */
	, &TlsStream_t1275_1_0_0/* this_arg */
	, &TlsStream_t1275_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsStream_t1275)/* instance_size */
	, sizeof (TlsStream_t1275)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 6/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
extern TypeInfo ClientCertificateType_t1308_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCerMethodDeclarations.h"
static const EncodedMethodIndex ClientCertificateType_t1308_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ClientCertificateType_t1308_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType ClientCertificateType_t1308_0_0_0;
extern const Il2CppType ClientCertificateType_t1308_1_0_0;
const Il2CppTypeDefinitionMetadata ClientCertificateType_t1308_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ClientCertificateType_t1308_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ClientCertificateType_t1308_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5520/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ClientCertificateType_t1308_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClientCertificateType"/* name */
	, "Mono.Security.Protocol.Tls.Handshake"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ClientCertificateType_t1308_0_0_0/* byval_arg */
	, &ClientCertificateType_t1308_1_0_0/* this_arg */
	, &ClientCertificateType_t1308_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClientCertificateType_t1308)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ClientCertificateType_t1308)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8448/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
extern TypeInfo HandshakeMessage_t1286_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake_0MethodDeclarations.h"
static const EncodedMethodIndex HandshakeMessage_t1286_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	0,
	0,
	1896,
	1897,
};
static Il2CppInterfaceOffsetPair HandshakeMessage_t1286_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType HandshakeMessage_t1286_0_0_0;
extern const Il2CppType HandshakeMessage_t1286_1_0_0;
struct HandshakeMessage_t1286;
const Il2CppTypeDefinitionMetadata HandshakeMessage_t1286_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HandshakeMessage_t1286_InterfacesOffsets/* interfaceOffsets */
	, &TlsStream_t1275_0_0_0/* parent */
	, HandshakeMessage_t1286_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5526/* fieldStart */
	, 7012/* methodStart */
	, -1/* eventStart */
	, 1434/* propertyStart */

};
TypeInfo HandshakeMessage_t1286_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "HandshakeMessage"/* name */
	, "Mono.Security.Protocol.Tls.Handshake"/* namespaze */
	, NULL/* methods */
	, &HandshakeMessage_t1286_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HandshakeMessage_t1286_0_0_0/* byval_arg */
	, &HandshakeMessage_t1286_1_0_0/* this_arg */
	, &HandshakeMessage_t1286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HandshakeMessage_t1286)/* instance_size */
	, sizeof (HandshakeMessage_t1286)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.HandshakeType
extern TypeInfo HandshakeType_t1309_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_HandshakeMethodDeclarations.h"
static const EncodedMethodIndex HandshakeType_t1309_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair HandshakeType_t1309_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType HandshakeType_t1309_0_0_0;
extern const Il2CppType HandshakeType_t1309_1_0_0;
const Il2CppTypeDefinitionMetadata HandshakeType_t1309_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HandshakeType_t1309_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, HandshakeType_t1309_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5530/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo HandshakeType_t1309_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "HandshakeType"/* name */
	, "Mono.Security.Protocol.Tls.Handshake"/* namespaze */
	, NULL/* methods */
	, &Byte_t1117_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HandshakeType_t1309_0_0_0/* byval_arg */
	, &HandshakeType_t1309_1_0_0/* this_arg */
	, &HandshakeType_t1309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HandshakeType_t1309)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HandshakeType_t1309)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8448/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificate
extern TypeInfo TlsClientCertificate_t1310_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_TlMethodDeclarations.h"
static const EncodedMethodIndex TlsClientCertificate_t1310_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1898,
	1899,
	1900,
	1897,
};
static Il2CppInterfaceOffsetPair TlsClientCertificate_t1310_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsClientCertificate_t1310_0_0_0;
extern const Il2CppType TlsClientCertificate_t1310_1_0_0;
struct TlsClientCertificate_t1310;
const Il2CppTypeDefinitionMetadata TlsClientCertificate_t1310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsClientCertificate_t1310_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsClientCertificate_t1310_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5542/* fieldStart */
	, 7024/* methodStart */
	, -1/* eventStart */
	, 1437/* propertyStart */

};
TypeInfo TlsClientCertificate_t1310_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsClientCertificate"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsClientCertificate_t1310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsClientCertificate_t1310_0_0_0/* byval_arg */
	, &TlsClientCertificate_t1310_1_0_0/* this_arg */
	, &TlsClientCertificate_t1310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsClientCertificate_t1310)/* instance_size */
	, sizeof (TlsClientCertificate_t1310)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
extern TypeInfo TlsClientCertificateVerify_t1311_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_0MethodDeclarations.h"
static const EncodedMethodIndex TlsClientCertificateVerify_t1311_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1901,
	1902,
	1903,
	1897,
};
static Il2CppInterfaceOffsetPair TlsClientCertificateVerify_t1311_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsClientCertificateVerify_t1311_0_0_0;
extern const Il2CppType TlsClientCertificateVerify_t1311_1_0_0;
struct TlsClientCertificateVerify_t1311;
const Il2CppTypeDefinitionMetadata TlsClientCertificateVerify_t1311_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsClientCertificateVerify_t1311_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsClientCertificateVerify_t1311_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7032/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsClientCertificateVerify_t1311_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsClientCertificateVerify"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsClientCertificateVerify_t1311_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsClientCertificateVerify_t1311_0_0_0/* byval_arg */
	, &TlsClientCertificateVerify_t1311_1_0_0/* this_arg */
	, &TlsClientCertificateVerify_t1311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsClientCertificateVerify_t1311)/* instance_size */
	, sizeof (TlsClientCertificateVerify_t1311)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_1.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
extern TypeInfo TlsClientFinished_t1312_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_1MethodDeclarations.h"
static const EncodedMethodIndex TlsClientFinished_t1312_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1904,
	1905,
	1906,
	1897,
};
static Il2CppInterfaceOffsetPair TlsClientFinished_t1312_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsClientFinished_t1312_0_0_0;
extern const Il2CppType TlsClientFinished_t1312_1_0_0;
struct TlsClientFinished_t1312;
const Il2CppTypeDefinitionMetadata TlsClientFinished_t1312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsClientFinished_t1312_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsClientFinished_t1312_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5544/* fieldStart */
	, 7038/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsClientFinished_t1312_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsClientFinished"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsClientFinished_t1312_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsClientFinished_t1312_0_0_0/* byval_arg */
	, &TlsClientFinished_t1312_1_0_0/* this_arg */
	, &TlsClientFinished_t1312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsClientFinished_t1312)/* instance_size */
	, sizeof (TlsClientFinished_t1312)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TlsClientFinished_t1312_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_2.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello
extern TypeInfo TlsClientHello_t1313_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_2MethodDeclarations.h"
static const EncodedMethodIndex TlsClientHello_t1313_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1907,
	1908,
	1909,
	1897,
};
static Il2CppInterfaceOffsetPair TlsClientHello_t1313_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsClientHello_t1313_0_0_0;
extern const Il2CppType TlsClientHello_t1313_1_0_0;
struct TlsClientHello_t1313;
const Il2CppTypeDefinitionMetadata TlsClientHello_t1313_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsClientHello_t1313_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsClientHello_t1313_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5545/* fieldStart */
	, 7043/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsClientHello_t1313_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsClientHello"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsClientHello_t1313_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsClientHello_t1313_0_0_0/* byval_arg */
	, &TlsClientHello_t1313_1_0_0/* this_arg */
	, &TlsClientHello_t1313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsClientHello_t1313)/* instance_size */
	, sizeof (TlsClientHello_t1313)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_3.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsClientKeyExchange
extern TypeInfo TlsClientKeyExchange_t1314_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_3MethodDeclarations.h"
static const EncodedMethodIndex TlsClientKeyExchange_t1314_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1910,
	1911,
	1896,
	1897,
};
static Il2CppInterfaceOffsetPair TlsClientKeyExchange_t1314_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsClientKeyExchange_t1314_0_0_0;
extern const Il2CppType TlsClientKeyExchange_t1314_1_0_0;
struct TlsClientKeyExchange_t1314;
const Il2CppTypeDefinitionMetadata TlsClientKeyExchange_t1314_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsClientKeyExchange_t1314_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsClientKeyExchange_t1314_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7047/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsClientKeyExchange_t1314_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsClientKeyExchange"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsClientKeyExchange_t1314_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsClientKeyExchange_t1314_0_0_0/* byval_arg */
	, &TlsClientKeyExchange_t1314_1_0_0/* this_arg */
	, &TlsClientKeyExchange_t1314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsClientKeyExchange_t1314)/* instance_size */
	, sizeof (TlsClientKeyExchange_t1314)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
