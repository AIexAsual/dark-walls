﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// iTweenPath
struct iTweenPath_t459;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>
struct  KeyValuePair_2_t2484 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,iTweenPath>::value
	iTweenPath_t459 * ___value_1;
};
