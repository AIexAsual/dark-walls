﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RIPEMD160
struct RIPEMD160_t1980;

// System.Void System.Security.Cryptography.RIPEMD160::.ctor()
extern "C" void RIPEMD160__ctor_m11965 (RIPEMD160_t1980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
