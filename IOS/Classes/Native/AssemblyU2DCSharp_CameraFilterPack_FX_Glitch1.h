﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_FX_Glitch1
struct  CameraFilterPack_FX_Glitch1_t128  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_Glitch1::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Glitch1::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_Glitch1::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_Glitch1::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_FX_Glitch1::Value
	float ___Value_6;
};
struct CameraFilterPack_FX_Glitch1_t128_StaticFields{
	// System.Single CameraFilterPack_FX_Glitch1::ChangeValue
	float ___ChangeValue_7;
};
