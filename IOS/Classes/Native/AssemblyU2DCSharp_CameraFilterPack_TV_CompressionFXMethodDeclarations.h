﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_CompressionFX
struct CameraFilterPack_TV_CompressionFX_t183;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_CompressionFX::.ctor()
extern "C" void CameraFilterPack_TV_CompressionFX__ctor_m1183 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_CompressionFX::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_CompressionFX_get_material_m1184 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_CompressionFX::Start()
extern "C" void CameraFilterPack_TV_CompressionFX_Start_m1185 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_CompressionFX::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_CompressionFX_OnRenderImage_m1186 (CameraFilterPack_TV_CompressionFX_t183 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_CompressionFX::Update()
extern "C" void CameraFilterPack_TV_CompressionFX_Update_m1187 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_CompressionFX::OnDisable()
extern "C" void CameraFilterPack_TV_CompressionFX_OnDisable_m1188 (CameraFilterPack_TV_CompressionFX_t183 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
