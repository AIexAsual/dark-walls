﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t742;
// UnityEngine.RectTransform
struct RectTransform_t648;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>
struct  Enumerator_t2628 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::l
	List_1_t742 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::current
	RectTransform_t648 * ___current_3;
};
