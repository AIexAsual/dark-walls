﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct Enumerator_t2249;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t510;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m13812_gshared (Enumerator_t2249 * __this, List_1_t510 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m13812(__this, ___l, method) (( void (*) (Enumerator_t2249 *, List_1_t510 *, const MethodInfo*))Enumerator__ctor_m13812_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m13813_gshared (Enumerator_t2249 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m13813(__this, method) (( Object_t * (*) (Enumerator_t2249 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m13813_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m13814_gshared (Enumerator_t2249 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m13814(__this, method) (( void (*) (Enumerator_t2249 *, const MethodInfo*))Enumerator_Dispose_m13814_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m13815_gshared (Enumerator_t2249 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m13815(__this, method) (( void (*) (Enumerator_t2249 *, const MethodInfo*))Enumerator_VerifyState_m13815_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m13816_gshared (Enumerator_t2249 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m13816(__this, method) (( bool (*) (Enumerator_t2249 *, const MethodInfo*))Enumerator_MoveNext_m13816_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t511  Enumerator_get_Current_m13817_gshared (Enumerator_t2249 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m13817(__this, method) (( RaycastResult_t511  (*) (Enumerator_t2249 *, const MethodInfo*))Enumerator_get_Current_m13817_gshared)(__this, method)
