﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$120
struct U24ArrayTypeU24120_t2154;
struct U24ArrayTypeU24120_t2154_marshaled;

void U24ArrayTypeU24120_t2154_marshal(const U24ArrayTypeU24120_t2154& unmarshaled, U24ArrayTypeU24120_t2154_marshaled& marshaled);
void U24ArrayTypeU24120_t2154_marshal_back(const U24ArrayTypeU24120_t2154_marshaled& marshaled, U24ArrayTypeU24120_t2154& unmarshaled);
void U24ArrayTypeU24120_t2154_marshal_cleanup(U24ArrayTypeU24120_t2154_marshaled& marshaled);
