﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ScoreTextScript
struct ScoreTextScript_t409;

// System.Void ScoreTextScript::.ctor()
extern "C" void ScoreTextScript__ctor_m2341 (ScoreTextScript_t409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreTextScript::Start()
extern "C" void ScoreTextScript_Start_m2342 (ScoreTextScript_t409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreTextScript::Update()
extern "C" void ScoreTextScript_Update_m2343 (ScoreTextScript_t409 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
