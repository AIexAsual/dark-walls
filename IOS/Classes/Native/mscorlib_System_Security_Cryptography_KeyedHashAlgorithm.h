﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t469;
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_t1255  : public HashAlgorithm_t1217
{
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t469* ___KeyValue_4;
};
