﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Oculus_NightVision3
struct  CameraFilterPack_Oculus_NightVision3_t164  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Oculus_NightVision3::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Oculus_NightVision3::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Oculus_NightVision3::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Oculus_NightVision3::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Oculus_NightVision3::Greenness
	float ___Greenness_6;
};
struct CameraFilterPack_Oculus_NightVision3_t164_StaticFields{
	// System.Single CameraFilterPack_Oculus_NightVision3::ChangeBinocularSize
	float ___ChangeBinocularSize_7;
	// System.Single CameraFilterPack_Oculus_NightVision3::ChangeBinocularDistance
	float ___ChangeBinocularDistance_8;
	// System.Single CameraFilterPack_Oculus_NightVision3::ChangeGreenness
	float ___ChangeGreenness_9;
};
