﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t949;
struct SkeletonBone_t949_marshaled;

void SkeletonBone_t949_marshal(const SkeletonBone_t949& unmarshaled, SkeletonBone_t949_marshaled& marshaled);
void SkeletonBone_t949_marshal_back(const SkeletonBone_t949_marshaled& marshaled, SkeletonBone_t949& unmarshaled);
void SkeletonBone_t949_marshal_cleanup(SkeletonBone_t949_marshaled& marshaled);
