﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Vector3>
struct Comparer_1_t2428;
// System.Object
struct Object_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.ctor()
extern "C" void Comparer_1__ctor_m16064_gshared (Comparer_1_t2428 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m16064(__this, method) (( void (*) (Comparer_1_t2428 *, const MethodInfo*))Comparer_1__ctor_m16064_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::.cctor()
extern "C" void Comparer_1__cctor_m16065_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m16065(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m16065_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m16066_gshared (Comparer_1_t2428 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m16066(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2428 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m16066_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Vector3>::get_Default()
extern "C" Comparer_1_t2428 * Comparer_1_get_Default_m16067_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m16067(__this /* static, unused */, method) (( Comparer_1_t2428 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m16067_gshared)(__this /* static, unused */, method)
