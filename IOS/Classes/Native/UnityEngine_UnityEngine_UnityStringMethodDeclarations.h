﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UnityString
struct UnityString_t911;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t470;

// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C" String_t* UnityString_Format_m5568 (Object_t * __this /* static, unused */, String_t* ___fmt, ObjectU5BU5D_t470* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
