﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<iTween/LoopType>
struct InternalEnumerator_1_t2387;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"

// System.Void System.Array/InternalEnumerator`1<iTween/LoopType>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15474_gshared (InternalEnumerator_1_t2387 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15474(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2387 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15474_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<iTween/LoopType>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15475_gshared (InternalEnumerator_1_t2387 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15475(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2387 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15475_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<iTween/LoopType>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15476_gshared (InternalEnumerator_1_t2387 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15476(__this, method) (( void (*) (InternalEnumerator_1_t2387 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15476_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<iTween/LoopType>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15477_gshared (InternalEnumerator_1_t2387 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15477(__this, method) (( bool (*) (InternalEnumerator_1_t2387 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15477_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<iTween/LoopType>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m15478_gshared (InternalEnumerator_1_t2387 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15478(__this, method) (( int32_t (*) (InternalEnumerator_1_t2387 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15478_gshared)(__this, method)
