﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_Contrast
struct CameraFilterPack_Color_Contrast_t62;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_Contrast::.ctor()
extern "C" void CameraFilterPack_Color_Contrast__ctor_m387 (CameraFilterPack_Color_Contrast_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Contrast::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_Contrast_get_material_m388 (CameraFilterPack_Color_Contrast_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::Start()
extern "C" void CameraFilterPack_Color_Contrast_Start_m389 (CameraFilterPack_Color_Contrast_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_Contrast_OnRenderImage_m390 (CameraFilterPack_Color_Contrast_t62 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::OnValidate()
extern "C" void CameraFilterPack_Color_Contrast_OnValidate_m391 (CameraFilterPack_Color_Contrast_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::Update()
extern "C" void CameraFilterPack_Color_Contrast_Update_m392 (CameraFilterPack_Color_Contrast_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Contrast::OnDisable()
extern "C" void CameraFilterPack_Color_Contrast_OnDisable_m393 (CameraFilterPack_Color_Contrast_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
