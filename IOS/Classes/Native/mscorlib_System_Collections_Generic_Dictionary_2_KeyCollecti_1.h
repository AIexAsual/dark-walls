﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<iTweenEvent/TweenType,System.Object>
struct Dictionary_2_t2327;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>
struct  KeyCollection_t2331  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<iTweenEvent/TweenType,System.Object>::dictionary
	Dictionary_2_t2327 * ___dictionary_0;
};
