﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.Emit.StackBehaviour
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
// Metadata Definition System.Reflection.Emit.StackBehaviour
extern TypeInfo StackBehaviour_t1760_il2cpp_TypeInfo;
// System.Reflection.Emit.StackBehaviour
#include "mscorlib_System_Reflection_Emit_StackBehaviourMethodDeclarations.h"
static const EncodedMethodIndex StackBehaviour_t1760_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
extern const Il2CppType IFormattable_t2190_0_0_0;
extern const Il2CppType IConvertible_t2193_0_0_0;
extern const Il2CppType IComparable_t2192_0_0_0;
static Il2CppInterfaceOffsetPair StackBehaviour_t1760_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StackBehaviour_t1760_0_0_0;
extern const Il2CppType StackBehaviour_t1760_1_0_0;
extern const Il2CppType Enum_t554_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t478_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata StackBehaviour_t1760_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StackBehaviour_t1760_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, StackBehaviour_t1760_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7745/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StackBehaviour_t1760_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackBehaviour"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2803/* custom_attributes_cache */
	, &StackBehaviour_t1760_0_0_0/* byval_arg */
	, &StackBehaviour_t1760_1_0_0/* this_arg */
	, &StackBehaviour_t1760_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackBehaviour_t1760)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StackBehaviour_t1760)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 30/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.Emit.TypeBuilder
#include "mscorlib_System_Reflection_Emit_TypeBuilder.h"
// Metadata Definition System.Reflection.Emit.TypeBuilder
extern TypeInfo TypeBuilder_t1733_il2cpp_TypeInfo;
// System.Reflection.Emit.TypeBuilder
#include "mscorlib_System_Reflection_Emit_TypeBuilderMethodDeclarations.h"
static const EncodedMethodIndex TypeBuilder_t1733_VTable[81] = 
{
	2626,
	601,
	2627,
	3566,
	3567,
	3568,
	3569,
	2632,
	3570,
	3571,
	3572,
	3568,
	3573,
	3567,
	3574,
	3575,
	2634,
	3576,
	3577,
	2635,
	2636,
	2637,
	2638,
	2639,
	2640,
	2641,
	2642,
	2643,
	2644,
	2645,
	2646,
	2647,
	2648,
	2649,
	3578,
	3579,
	3580,
	2651,
	3581,
	3582,
	3583,
	2654,
	3584,
	3585,
	3586,
	3587,
	2655,
	2656,
	2657,
	2658,
	3588,
	3589,
	3590,
	2659,
	2660,
	2661,
	2662,
	3591,
	3592,
	3593,
	3594,
	3595,
	3596,
	3597,
	3598,
	3599,
	2664,
	2665,
	2666,
	2667,
	2668,
	2669,
	3600,
	3601,
	3602,
	3603,
	3604,
	3605,
	3606,
	3607,
	3608,
};
extern const Il2CppType _TypeBuilder_t3475_0_0_0;
static const Il2CppType* TypeBuilder_t1733_InterfacesTypeInfos[] = 
{
	&_TypeBuilder_t3475_0_0_0,
};
extern const Il2CppType IReflect_t3442_0_0_0;
extern const Il2CppType _Type_t3440_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t2188_0_0_0;
extern const Il2CppType _MemberInfo_t3441_0_0_0;
static Il2CppInterfaceOffsetPair TypeBuilder_t1733_InterfacesOffsets[] = 
{
	{ &IReflect_t3442_0_0_0, 14},
	{ &_Type_t3440_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_TypeBuilder_t3475_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeBuilder_t1733_0_0_0;
extern const Il2CppType TypeBuilder_t1733_1_0_0;
extern const Il2CppType Type_t_0_0_0;
struct TypeBuilder_t1733;
const Il2CppTypeDefinitionMetadata TypeBuilder_t1733_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TypeBuilder_t1733_InterfacesTypeInfos/* implementedInterfaces */
	, TypeBuilder_t1733_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, TypeBuilder_t1733_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7775/* fieldStart */
	, 11127/* methodStart */
	, -1/* eventStart */
	, 2168/* propertyStart */

};
TypeInfo TypeBuilder_t1733_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeBuilder"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &TypeBuilder_t1733_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2804/* custom_attributes_cache */
	, &TypeBuilder_t1733_0_0_0/* byval_arg */
	, &TypeBuilder_t1733_1_0_0/* this_arg */
	, &TypeBuilder_t1733_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeBuilder_t1733)/* instance_size */
	, sizeof (TypeBuilder_t1733)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 62/* method_count */
	, 17/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 81/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.Emit.UnmanagedMarshal
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshal.h"
// Metadata Definition System.Reflection.Emit.UnmanagedMarshal
extern TypeInfo UnmanagedMarshal_t1738_il2cpp_TypeInfo;
// System.Reflection.Emit.UnmanagedMarshal
#include "mscorlib_System_Reflection_Emit_UnmanagedMarshalMethodDeclarations.h"
static const EncodedMethodIndex UnmanagedMarshal_t1738_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnmanagedMarshal_t1738_0_0_0;
extern const Il2CppType UnmanagedMarshal_t1738_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct UnmanagedMarshal_t1738;
const Il2CppTypeDefinitionMetadata UnmanagedMarshal_t1738_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnmanagedMarshal_t1738_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7792/* fieldStart */
	, 11189/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnmanagedMarshal_t1738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnmanagedMarshal"/* name */
	, "System.Reflection.Emit"/* namespaze */
	, NULL/* methods */
	, &UnmanagedMarshal_t1738_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2814/* custom_attributes_cache */
	, &UnmanagedMarshal_t1738_0_0_0/* byval_arg */
	, &UnmanagedMarshal_t1738_1_0_0/* this_arg */
	, &UnmanagedMarshal_t1738_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnmanagedMarshal_t1738)/* instance_size */
	, sizeof (UnmanagedMarshal_t1738)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.AmbiguousMatchException
#include "mscorlib_System_Reflection_AmbiguousMatchException.h"
// Metadata Definition System.Reflection.AmbiguousMatchException
extern TypeInfo AmbiguousMatchException_t1765_il2cpp_TypeInfo;
// System.Reflection.AmbiguousMatchException
#include "mscorlib_System_Reflection_AmbiguousMatchExceptionMethodDeclarations.h"
static const EncodedMethodIndex AmbiguousMatchException_t1765_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
extern const Il2CppType ISerializable_t2210_0_0_0;
extern const Il2CppType _Exception_t3443_0_0_0;
static Il2CppInterfaceOffsetPair AmbiguousMatchException_t1765_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AmbiguousMatchException_t1765_0_0_0;
extern const Il2CppType AmbiguousMatchException_t1765_1_0_0;
extern const Il2CppType SystemException_t1536_0_0_0;
struct AmbiguousMatchException_t1765;
const Il2CppTypeDefinitionMetadata AmbiguousMatchException_t1765_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AmbiguousMatchException_t1765_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, AmbiguousMatchException_t1765_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11190/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AmbiguousMatchException_t1765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AmbiguousMatchException"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AmbiguousMatchException_t1765_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2815/* custom_attributes_cache */
	, &AmbiguousMatchException_t1765_0_0_0/* byval_arg */
	, &AmbiguousMatchException_t1765_1_0_0/* this_arg */
	, &AmbiguousMatchException_t1765_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AmbiguousMatchException_t1765)/* instance_size */
	, sizeof (AmbiguousMatchException_t1765)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.Assembly
#include "mscorlib_System_Reflection_Assembly.h"
// Metadata Definition System.Reflection.Assembly
extern TypeInfo Assembly_t1535_il2cpp_TypeInfo;
// System.Reflection.Assembly
#include "mscorlib_System_Reflection_AssemblyMethodDeclarations.h"
extern const Il2CppType ResolveEventHolder_t1766_0_0_0;
static const Il2CppType* Assembly_t1535_il2cpp_TypeInfo__nestedTypes[1] =
{
	&ResolveEventHolder_t1766_0_0_0,
};
static const EncodedMethodIndex Assembly_t1535_VTable[21] = 
{
	626,
	601,
	627,
	3361,
	3362,
	3363,
	3364,
	3609,
	3363,
	3362,
	3610,
	3367,
	3368,
	3369,
	3370,
	3371,
	3372,
	3611,
	3374,
	3612,
	3376,
};
extern const Il2CppType _Assembly_t3476_0_0_0;
static const Il2CppType* Assembly_t1535_InterfacesTypeInfos[] = 
{
	&ICustomAttributeProvider_t2188_0_0_0,
	&_Assembly_t3476_0_0_0,
};
static Il2CppInterfaceOffsetPair Assembly_t1535_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_Assembly_t3476_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Assembly_t1535_0_0_0;
extern const Il2CppType Assembly_t1535_1_0_0;
struct Assembly_t1535;
const Il2CppTypeDefinitionMetadata Assembly_t1535_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Assembly_t1535_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Assembly_t1535_InterfacesTypeInfos/* implementedInterfaces */
	, Assembly_t1535_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Assembly_t1535_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7801/* fieldStart */
	, 11193/* methodStart */
	, -1/* eventStart */
	, 2185/* propertyStart */

};
TypeInfo Assembly_t1535_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Assembly"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Assembly_t1535_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2816/* custom_attributes_cache */
	, &Assembly_t1535_0_0_0/* byval_arg */
	, &Assembly_t1535_1_0_0/* this_arg */
	, &Assembly_t1535_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Assembly_t1535)/* instance_size */
	, sizeof (Assembly_t1535)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 21/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.Assembly/ResolveEventHolder
#include "mscorlib_System_Reflection_Assembly_ResolveEventHolder.h"
// Metadata Definition System.Reflection.Assembly/ResolveEventHolder
extern TypeInfo ResolveEventHolder_t1766_il2cpp_TypeInfo;
// System.Reflection.Assembly/ResolveEventHolder
#include "mscorlib_System_Reflection_Assembly_ResolveEventHolderMethodDeclarations.h"
static const EncodedMethodIndex ResolveEventHolder_t1766_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventHolder_t1766_1_0_0;
struct ResolveEventHolder_t1766;
const Il2CppTypeDefinitionMetadata ResolveEventHolder_t1766_DefinitionMetadata = 
{
	&Assembly_t1535_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ResolveEventHolder_t1766_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11219/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ResolveEventHolder_t1766_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventHolder"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &ResolveEventHolder_t1766_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ResolveEventHolder_t1766_0_0_0/* byval_arg */
	, &ResolveEventHolder_t1766_1_0_0/* this_arg */
	, &ResolveEventHolder_t1766_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventHolder_t1766)/* instance_size */
	, sizeof (ResolveEventHolder_t1766)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048581/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// Metadata Definition System.Reflection.AssemblyCompanyAttribute
extern TypeInfo AssemblyCompanyAttribute_t1769_il2cpp_TypeInfo;
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyCompanyAttribute_t1769_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
extern const Il2CppType _Attribute_t3429_0_0_0;
static Il2CppInterfaceOffsetPair AssemblyCompanyAttribute_t1769_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyCompanyAttribute_t1769_0_0_0;
extern const Il2CppType AssemblyCompanyAttribute_t1769_1_0_0;
extern const Il2CppType Attribute_t903_0_0_0;
struct AssemblyCompanyAttribute_t1769;
const Il2CppTypeDefinitionMetadata AssemblyCompanyAttribute_t1769_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyCompanyAttribute_t1769_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyCompanyAttribute_t1769_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7811/* fieldStart */
	, 11220/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyCompanyAttribute_t1769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyCompanyAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyCompanyAttribute_t1769_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2818/* custom_attributes_cache */
	, &AssemblyCompanyAttribute_t1769_0_0_0/* byval_arg */
	, &AssemblyCompanyAttribute_t1769_1_0_0/* this_arg */
	, &AssemblyCompanyAttribute_t1769_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyCompanyAttribute_t1769)/* instance_size */
	, sizeof (AssemblyCompanyAttribute_t1769)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// Metadata Definition System.Reflection.AssemblyConfigurationAttribute
extern TypeInfo AssemblyConfigurationAttribute_t1770_il2cpp_TypeInfo;
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyConfigurationAttribute_t1770_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyConfigurationAttribute_t1770_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyConfigurationAttribute_t1770_0_0_0;
extern const Il2CppType AssemblyConfigurationAttribute_t1770_1_0_0;
struct AssemblyConfigurationAttribute_t1770;
const Il2CppTypeDefinitionMetadata AssemblyConfigurationAttribute_t1770_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyConfigurationAttribute_t1770_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyConfigurationAttribute_t1770_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7812/* fieldStart */
	, 11221/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyConfigurationAttribute_t1770_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyConfigurationAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyConfigurationAttribute_t1770_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2819/* custom_attributes_cache */
	, &AssemblyConfigurationAttribute_t1770_0_0_0/* byval_arg */
	, &AssemblyConfigurationAttribute_t1770_1_0_0/* this_arg */
	, &AssemblyConfigurationAttribute_t1770_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyConfigurationAttribute_t1770)/* instance_size */
	, sizeof (AssemblyConfigurationAttribute_t1770)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// Metadata Definition System.Reflection.AssemblyCopyrightAttribute
extern TypeInfo AssemblyCopyrightAttribute_t1771_il2cpp_TypeInfo;
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyCopyrightAttribute_t1771_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyCopyrightAttribute_t1771_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyCopyrightAttribute_t1771_0_0_0;
extern const Il2CppType AssemblyCopyrightAttribute_t1771_1_0_0;
struct AssemblyCopyrightAttribute_t1771;
const Il2CppTypeDefinitionMetadata AssemblyCopyrightAttribute_t1771_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyCopyrightAttribute_t1771_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyCopyrightAttribute_t1771_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7813/* fieldStart */
	, 11222/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyCopyrightAttribute_t1771_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyCopyrightAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyCopyrightAttribute_t1771_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2820/* custom_attributes_cache */
	, &AssemblyCopyrightAttribute_t1771_0_0_0/* byval_arg */
	, &AssemblyCopyrightAttribute_t1771_1_0_0/* this_arg */
	, &AssemblyCopyrightAttribute_t1771_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyCopyrightAttribute_t1771)/* instance_size */
	, sizeof (AssemblyCopyrightAttribute_t1771)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// Metadata Definition System.Reflection.AssemblyDefaultAliasAttribute
extern TypeInfo AssemblyDefaultAliasAttribute_t1772_il2cpp_TypeInfo;
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyDefaultAliasAttribute_t1772_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyDefaultAliasAttribute_t1772_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyDefaultAliasAttribute_t1772_0_0_0;
extern const Il2CppType AssemblyDefaultAliasAttribute_t1772_1_0_0;
struct AssemblyDefaultAliasAttribute_t1772;
const Il2CppTypeDefinitionMetadata AssemblyDefaultAliasAttribute_t1772_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyDefaultAliasAttribute_t1772_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyDefaultAliasAttribute_t1772_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7814/* fieldStart */
	, 11223/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyDefaultAliasAttribute_t1772_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyDefaultAliasAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyDefaultAliasAttribute_t1772_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2821/* custom_attributes_cache */
	, &AssemblyDefaultAliasAttribute_t1772_0_0_0/* byval_arg */
	, &AssemblyDefaultAliasAttribute_t1772_1_0_0/* this_arg */
	, &AssemblyDefaultAliasAttribute_t1772_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyDefaultAliasAttribute_t1772)/* instance_size */
	, sizeof (AssemblyDefaultAliasAttribute_t1772)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// Metadata Definition System.Reflection.AssemblyDelaySignAttribute
extern TypeInfo AssemblyDelaySignAttribute_t1773_il2cpp_TypeInfo;
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyDelaySignAttribute_t1773_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyDelaySignAttribute_t1773_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyDelaySignAttribute_t1773_0_0_0;
extern const Il2CppType AssemblyDelaySignAttribute_t1773_1_0_0;
struct AssemblyDelaySignAttribute_t1773;
const Il2CppTypeDefinitionMetadata AssemblyDelaySignAttribute_t1773_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyDelaySignAttribute_t1773_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyDelaySignAttribute_t1773_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7815/* fieldStart */
	, 11224/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyDelaySignAttribute_t1773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyDelaySignAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyDelaySignAttribute_t1773_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2822/* custom_attributes_cache */
	, &AssemblyDelaySignAttribute_t1773_0_0_0/* byval_arg */
	, &AssemblyDelaySignAttribute_t1773_1_0_0/* this_arg */
	, &AssemblyDelaySignAttribute_t1773_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyDelaySignAttribute_t1773)/* instance_size */
	, sizeof (AssemblyDelaySignAttribute_t1773)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// Metadata Definition System.Reflection.AssemblyDescriptionAttribute
extern TypeInfo AssemblyDescriptionAttribute_t1774_il2cpp_TypeInfo;
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyDescriptionAttribute_t1774_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyDescriptionAttribute_t1774_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyDescriptionAttribute_t1774_0_0_0;
extern const Il2CppType AssemblyDescriptionAttribute_t1774_1_0_0;
struct AssemblyDescriptionAttribute_t1774;
const Il2CppTypeDefinitionMetadata AssemblyDescriptionAttribute_t1774_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyDescriptionAttribute_t1774_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyDescriptionAttribute_t1774_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7816/* fieldStart */
	, 11225/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyDescriptionAttribute_t1774_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyDescriptionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyDescriptionAttribute_t1774_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2823/* custom_attributes_cache */
	, &AssemblyDescriptionAttribute_t1774_0_0_0/* byval_arg */
	, &AssemblyDescriptionAttribute_t1774_1_0_0/* this_arg */
	, &AssemblyDescriptionAttribute_t1774_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyDescriptionAttribute_t1774)/* instance_size */
	, sizeof (AssemblyDescriptionAttribute_t1774)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// Metadata Definition System.Reflection.AssemblyFileVersionAttribute
extern TypeInfo AssemblyFileVersionAttribute_t1775_il2cpp_TypeInfo;
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyFileVersionAttribute_t1775_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyFileVersionAttribute_t1775_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyFileVersionAttribute_t1775_0_0_0;
extern const Il2CppType AssemblyFileVersionAttribute_t1775_1_0_0;
struct AssemblyFileVersionAttribute_t1775;
const Il2CppTypeDefinitionMetadata AssemblyFileVersionAttribute_t1775_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyFileVersionAttribute_t1775_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyFileVersionAttribute_t1775_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7817/* fieldStart */
	, 11226/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyFileVersionAttribute_t1775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyFileVersionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyFileVersionAttribute_t1775_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2824/* custom_attributes_cache */
	, &AssemblyFileVersionAttribute_t1775_0_0_0/* byval_arg */
	, &AssemblyFileVersionAttribute_t1775_1_0_0/* this_arg */
	, &AssemblyFileVersionAttribute_t1775_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyFileVersionAttribute_t1775)/* instance_size */
	, sizeof (AssemblyFileVersionAttribute_t1775)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// Metadata Definition System.Reflection.AssemblyInformationalVersionAttribute
extern TypeInfo AssemblyInformationalVersionAttribute_t1776_il2cpp_TypeInfo;
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
static const EncodedMethodIndex AssemblyInformationalVersionAttribute_t1776_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyInformationalVersionAttribute_t1776_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyInformationalVersionAttribute_t1776_0_0_0;
extern const Il2CppType AssemblyInformationalVersionAttribute_t1776_1_0_0;
struct AssemblyInformationalVersionAttribute_t1776;
const Il2CppTypeDefinitionMetadata AssemblyInformationalVersionAttribute_t1776_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyInformationalVersionAttribute_t1776_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyInformationalVersionAttribute_t1776_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7818/* fieldStart */
	, 11227/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyInformationalVersionAttribute_t1776_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyInformationalVersionAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyInformationalVersionAttribute_t1776_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2825/* custom_attributes_cache */
	, &AssemblyInformationalVersionAttribute_t1776_0_0_0/* byval_arg */
	, &AssemblyInformationalVersionAttribute_t1776_1_0_0/* this_arg */
	, &AssemblyInformationalVersionAttribute_t1776_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyInformationalVersionAttribute_t1776)/* instance_size */
	, sizeof (AssemblyInformationalVersionAttribute_t1776)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// Metadata Definition System.Reflection.AssemblyKeyFileAttribute
extern TypeInfo AssemblyKeyFileAttribute_t1777_il2cpp_TypeInfo;
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyKeyFileAttribute_t1777_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyKeyFileAttribute_t1777_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyKeyFileAttribute_t1777_0_0_0;
extern const Il2CppType AssemblyKeyFileAttribute_t1777_1_0_0;
struct AssemblyKeyFileAttribute_t1777;
const Il2CppTypeDefinitionMetadata AssemblyKeyFileAttribute_t1777_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyKeyFileAttribute_t1777_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyKeyFileAttribute_t1777_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7819/* fieldStart */
	, 11228/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyKeyFileAttribute_t1777_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyKeyFileAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyKeyFileAttribute_t1777_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2826/* custom_attributes_cache */
	, &AssemblyKeyFileAttribute_t1777_0_0_0/* byval_arg */
	, &AssemblyKeyFileAttribute_t1777_1_0_0/* this_arg */
	, &AssemblyKeyFileAttribute_t1777_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyKeyFileAttribute_t1777)/* instance_size */
	, sizeof (AssemblyKeyFileAttribute_t1777)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
// Metadata Definition System.Reflection.AssemblyName
extern TypeInfo AssemblyName_t1779_il2cpp_TypeInfo;
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyNameMethodDeclarations.h"
static const EncodedMethodIndex AssemblyName_t1779_VTable[6] = 
{
	626,
	601,
	627,
	3613,
	3614,
	3615,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType _AssemblyName_t3477_0_0_0;
extern const Il2CppType IDeserializationCallback_t2213_0_0_0;
static const Il2CppType* AssemblyName_t1779_InterfacesTypeInfos[] = 
{
	&ICloneable_t3433_0_0_0,
	&ISerializable_t2210_0_0_0,
	&_AssemblyName_t3477_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair AssemblyName_t1779_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_AssemblyName_t3477_0_0_0, 5},
	{ &IDeserializationCallback_t2213_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyName_t1779_0_0_0;
extern const Il2CppType AssemblyName_t1779_1_0_0;
struct AssemblyName_t1779;
const Il2CppTypeDefinitionMetadata AssemblyName_t1779_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AssemblyName_t1779_InterfacesTypeInfos/* implementedInterfaces */
	, AssemblyName_t1779_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AssemblyName_t1779_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7820/* fieldStart */
	, 11229/* methodStart */
	, -1/* eventStart */
	, 2187/* propertyStart */

};
TypeInfo AssemblyName_t1779_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyName"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyName_t1779_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2827/* custom_attributes_cache */
	, &AssemblyName_t1779_0_0_0/* byval_arg */
	, &AssemblyName_t1779_1_0_0/* this_arg */
	, &AssemblyName_t1779_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyName_t1779)/* instance_size */
	, sizeof (AssemblyName_t1779)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 5/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
// Metadata Definition System.Reflection.AssemblyNameFlags
extern TypeInfo AssemblyNameFlags_t1780_il2cpp_TypeInfo;
// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlagsMethodDeclarations.h"
static const EncodedMethodIndex AssemblyNameFlags_t1780_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair AssemblyNameFlags_t1780_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyNameFlags_t1780_0_0_0;
extern const Il2CppType AssemblyNameFlags_t1780_1_0_0;
const Il2CppTypeDefinitionMetadata AssemblyNameFlags_t1780_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyNameFlags_t1780_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, AssemblyNameFlags_t1780_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7835/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyNameFlags_t1780_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyNameFlags"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2828/* custom_attributes_cache */
	, &AssemblyNameFlags_t1780_0_0_0/* byval_arg */
	, &AssemblyNameFlags_t1780_1_0_0/* this_arg */
	, &AssemblyNameFlags_t1780_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyNameFlags_t1780)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AssemblyNameFlags_t1780)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// Metadata Definition System.Reflection.AssemblyProductAttribute
extern TypeInfo AssemblyProductAttribute_t1781_il2cpp_TypeInfo;
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyProductAttribute_t1781_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyProductAttribute_t1781_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyProductAttribute_t1781_0_0_0;
extern const Il2CppType AssemblyProductAttribute_t1781_1_0_0;
struct AssemblyProductAttribute_t1781;
const Il2CppTypeDefinitionMetadata AssemblyProductAttribute_t1781_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyProductAttribute_t1781_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyProductAttribute_t1781_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7841/* fieldStart */
	, 11244/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyProductAttribute_t1781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyProductAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyProductAttribute_t1781_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2829/* custom_attributes_cache */
	, &AssemblyProductAttribute_t1781_0_0_0/* byval_arg */
	, &AssemblyProductAttribute_t1781_1_0_0/* this_arg */
	, &AssemblyProductAttribute_t1781_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyProductAttribute_t1781)/* instance_size */
	, sizeof (AssemblyProductAttribute_t1781)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// Metadata Definition System.Reflection.AssemblyTitleAttribute
extern TypeInfo AssemblyTitleAttribute_t1782_il2cpp_TypeInfo;
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyTitleAttribute_t1782_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyTitleAttribute_t1782_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyTitleAttribute_t1782_0_0_0;
extern const Il2CppType AssemblyTitleAttribute_t1782_1_0_0;
struct AssemblyTitleAttribute_t1782;
const Il2CppTypeDefinitionMetadata AssemblyTitleAttribute_t1782_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyTitleAttribute_t1782_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyTitleAttribute_t1782_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7842/* fieldStart */
	, 11245/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyTitleAttribute_t1782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyTitleAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyTitleAttribute_t1782_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2830/* custom_attributes_cache */
	, &AssemblyTitleAttribute_t1782_0_0_0/* byval_arg */
	, &AssemblyTitleAttribute_t1782_1_0_0/* this_arg */
	, &AssemblyTitleAttribute_t1782_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyTitleAttribute_t1782)/* instance_size */
	, sizeof (AssemblyTitleAttribute_t1782)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// Metadata Definition System.Reflection.AssemblyTrademarkAttribute
extern TypeInfo AssemblyTrademarkAttribute_t1783_il2cpp_TypeInfo;
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
static const EncodedMethodIndex AssemblyTrademarkAttribute_t1783_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair AssemblyTrademarkAttribute_t1783_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyTrademarkAttribute_t1783_0_0_0;
extern const Il2CppType AssemblyTrademarkAttribute_t1783_1_0_0;
struct AssemblyTrademarkAttribute_t1783;
const Il2CppTypeDefinitionMetadata AssemblyTrademarkAttribute_t1783_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyTrademarkAttribute_t1783_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, AssemblyTrademarkAttribute_t1783_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7843/* fieldStart */
	, 11246/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AssemblyTrademarkAttribute_t1783_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyTrademarkAttribute"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &AssemblyTrademarkAttribute_t1783_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2831/* custom_attributes_cache */
	, &AssemblyTrademarkAttribute_t1783_0_0_0/* byval_arg */
	, &AssemblyTrademarkAttribute_t1783_1_0_0/* this_arg */
	, &AssemblyTrademarkAttribute_t1783_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyTrademarkAttribute_t1783)/* instance_size */
	, sizeof (AssemblyTrademarkAttribute_t1783)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Binder
#include "mscorlib_System_Reflection_Binder.h"
// Metadata Definition System.Reflection.Binder
extern TypeInfo Binder_t1160_il2cpp_TypeInfo;
// System.Reflection.Binder
#include "mscorlib_System_Reflection_BinderMethodDeclarations.h"
extern const Il2CppType Default_t1784_0_0_0;
static const Il2CppType* Binder_t1160_il2cpp_TypeInfo__nestedTypes[1] =
{
	&Default_t1784_0_0_0,
};
static const EncodedMethodIndex Binder_t1160_VTable[9] = 
{
	626,
	601,
	627,
	628,
	0,
	0,
	0,
	0,
	0,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Binder_t1160_0_0_0;
extern const Il2CppType Binder_t1160_1_0_0;
struct Binder_t1160;
const Il2CppTypeDefinitionMetadata Binder_t1160_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Binder_t1160_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Binder_t1160_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7844/* fieldStart */
	, 11247/* methodStart */
	, -1/* eventStart */
	, 2192/* propertyStart */

};
TypeInfo Binder_t1160_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Binder"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Binder_t1160_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2832/* custom_attributes_cache */
	, &Binder_t1160_0_0_0/* byval_arg */
	, &Binder_t1160_1_0_0/* this_arg */
	, &Binder_t1160_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Binder_t1160)/* instance_size */
	, sizeof (Binder_t1160)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Binder_t1160_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Binder/Default
#include "mscorlib_System_Reflection_Binder_Default.h"
// Metadata Definition System.Reflection.Binder/Default
extern TypeInfo Default_t1784_il2cpp_TypeInfo;
// System.Reflection.Binder/Default
#include "mscorlib_System_Reflection_Binder_DefaultMethodDeclarations.h"
static const EncodedMethodIndex Default_t1784_VTable[9] = 
{
	626,
	601,
	627,
	628,
	3616,
	3617,
	3618,
	3619,
	3620,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Default_t1784_1_0_0;
struct Default_t1784;
const Il2CppTypeDefinitionMetadata Default_t1784_DefinitionMetadata = 
{
	&Binder_t1160_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Binder_t1160_0_0_0/* parent */
	, Default_t1784_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11258/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Default_t1784_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Default"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Default_t1784_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Default_t1784_0_0_0/* byval_arg */
	, &Default_t1784_1_0_0/* this_arg */
	, &Default_t1784_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Default_t1784)/* instance_size */
	, sizeof (Default_t1784)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// Metadata Definition System.Reflection.BindingFlags
extern TypeInfo BindingFlags_t1785_il2cpp_TypeInfo;
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlagsMethodDeclarations.h"
static const EncodedMethodIndex BindingFlags_t1785_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair BindingFlags_t1785_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType BindingFlags_t1785_0_0_0;
extern const Il2CppType BindingFlags_t1785_1_0_0;
const Il2CppTypeDefinitionMetadata BindingFlags_t1785_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, BindingFlags_t1785_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, BindingFlags_t1785_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7845/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo BindingFlags_t1785_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "BindingFlags"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2834/* custom_attributes_cache */
	, &BindingFlags_t1785_0_0_0/* byval_arg */
	, &BindingFlags_t1785_1_0_0/* this_arg */
	, &BindingFlags_t1785_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BindingFlags_t1785)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (BindingFlags_t1785)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 21/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"
// Metadata Definition System.Reflection.CallingConventions
extern TypeInfo CallingConventions_t1786_il2cpp_TypeInfo;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventionsMethodDeclarations.h"
static const EncodedMethodIndex CallingConventions_t1786_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CallingConventions_t1786_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallingConventions_t1786_0_0_0;
extern const Il2CppType CallingConventions_t1786_1_0_0;
const Il2CppTypeDefinitionMetadata CallingConventions_t1786_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallingConventions_t1786_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CallingConventions_t1786_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7866/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CallingConventions_t1786_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallingConventions"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2835/* custom_attributes_cache */
	, &CallingConventions_t1786_0_0_0/* byval_arg */
	, &CallingConventions_t1786_1_0_0/* this_arg */
	, &CallingConventions_t1786_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallingConventions_t1786)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CallingConventions_t1786)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
// Metadata Definition System.Reflection.ConstructorInfo
extern TypeInfo ConstructorInfo_t996_il2cpp_TypeInfo;
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
static const EncodedMethodIndex ConstructorInfo_t996_VTable[31] = 
{
	626,
	601,
	627,
	628,
	2629,
	2630,
	0,
	3381,
	0,
	0,
	2677,
	0,
	0,
	0,
	0,
	3621,
	3388,
	0,
	0,
	0,
	3622,
	3393,
	3394,
	3395,
	3396,
	3623,
	3398,
	3399,
	3400,
	3401,
	0,
};
extern const Il2CppType _ConstructorInfo_t3478_0_0_0;
static const Il2CppType* ConstructorInfo_t996_InterfacesTypeInfos[] = 
{
	&_ConstructorInfo_t3478_0_0_0,
};
extern const Il2CppType _MethodBase_t3481_0_0_0;
static Il2CppInterfaceOffsetPair ConstructorInfo_t996_InterfacesOffsets[] = 
{
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_ConstructorInfo_t3478_0_0_0, 30},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ConstructorInfo_t996_0_0_0;
extern const Il2CppType ConstructorInfo_t996_1_0_0;
extern const Il2CppType MethodBase_t1163_0_0_0;
struct ConstructorInfo_t996;
const Il2CppTypeDefinitionMetadata ConstructorInfo_t996_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ConstructorInfo_t996_InterfacesTypeInfos/* implementedInterfaces */
	, ConstructorInfo_t996_InterfacesOffsets/* interfaceOffsets */
	, &MethodBase_t1163_0_0_0/* parent */
	, ConstructorInfo_t996_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7872/* fieldStart */
	, 11273/* methodStart */
	, -1/* eventStart */
	, 2193/* propertyStart */

};
TypeInfo ConstructorInfo_t996_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &ConstructorInfo_t996_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2836/* custom_attributes_cache */
	, &ConstructorInfo_t996_0_0_0/* byval_arg */
	, &ConstructorInfo_t996_1_0_0/* this_arg */
	, &ConstructorInfo_t996_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorInfo_t996)/* instance_size */
	, sizeof (ConstructorInfo_t996)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ConstructorInfo_t996_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 31/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributes.h"
// Metadata Definition System.Reflection.EventAttributes
extern TypeInfo EventAttributes_t1787_il2cpp_TypeInfo;
// System.Reflection.EventAttributes
#include "mscorlib_System_Reflection_EventAttributesMethodDeclarations.h"
static const EncodedMethodIndex EventAttributes_t1787_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair EventAttributes_t1787_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventAttributes_t1787_0_0_0;
extern const Il2CppType EventAttributes_t1787_1_0_0;
const Il2CppTypeDefinitionMetadata EventAttributes_t1787_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventAttributes_t1787_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, EventAttributes_t1787_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7874/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo EventAttributes_t1787_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2841/* custom_attributes_cache */
	, &EventAttributes_t1787_0_0_0/* byval_arg */
	, &EventAttributes_t1787_1_0_0/* this_arg */
	, &EventAttributes_t1787_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventAttributes_t1787)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (EventAttributes_t1787)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
// Metadata Definition System.Reflection.EventInfo
extern TypeInfo EventInfo_t_il2cpp_TypeInfo;
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfoMethodDeclarations.h"
extern const Il2CppType AddEventAdapter_t1788_0_0_0;
static const Il2CppType* EventInfo_t_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AddEventAdapter_t1788_0_0_0,
};
static const EncodedMethodIndex EventInfo_t_VTable[17] = 
{
	626,
	601,
	627,
	628,
	2629,
	2630,
	0,
	3624,
	0,
	0,
	2677,
	0,
	0,
	0,
	0,
	3625,
	0,
};
extern const Il2CppType _EventInfo_t3479_0_0_0;
static const Il2CppType* EventInfo_t_InterfacesTypeInfos[] = 
{
	&_EventInfo_t3479_0_0_0,
};
static Il2CppInterfaceOffsetPair EventInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_EventInfo_t3479_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventInfo_t_0_0_0;
extern const Il2CppType EventInfo_t_1_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
struct EventInfo_t;
const Il2CppTypeDefinitionMetadata EventInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, EventInfo_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, EventInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, EventInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, EventInfo_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7879/* fieldStart */
	, 11278/* methodStart */
	, -1/* eventStart */
	, 2194/* propertyStart */

};
TypeInfo EventInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &EventInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2842/* custom_attributes_cache */
	, &EventInfo_t_0_0_0/* byval_arg */
	, &EventInfo_t_1_0_0/* this_arg */
	, &EventInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventInfo_t)/* instance_size */
	, sizeof (EventInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 17/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.EventInfo/AddEventAdapter
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapter.h"
// Metadata Definition System.Reflection.EventInfo/AddEventAdapter
extern TypeInfo AddEventAdapter_t1788_il2cpp_TypeInfo;
// System.Reflection.EventInfo/AddEventAdapter
#include "mscorlib_System_Reflection_EventInfo_AddEventAdapterMethodDeclarations.h"
static const EncodedMethodIndex AddEventAdapter_t1788_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	3626,
	3627,
	3628,
};
static Il2CppInterfaceOffsetPair AddEventAdapter_t1788_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AddEventAdapter_t1788_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct AddEventAdapter_t1788;
const Il2CppTypeDefinitionMetadata AddEventAdapter_t1788_DefinitionMetadata = 
{
	&EventInfo_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddEventAdapter_t1788_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, AddEventAdapter_t1788_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11283/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo AddEventAdapter_t1788_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddEventAdapter"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &AddEventAdapter_t1788_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddEventAdapter_t1788_0_0_0/* byval_arg */
	, &AddEventAdapter_t1788_1_0_0/* this_arg */
	, &AddEventAdapter_t1788_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AddEventAdapter_t1788/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddEventAdapter_t1788)/* instance_size */
	, sizeof (AddEventAdapter_t1788)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributes.h"
// Metadata Definition System.Reflection.FieldAttributes
extern TypeInfo FieldAttributes_t1789_il2cpp_TypeInfo;
// System.Reflection.FieldAttributes
#include "mscorlib_System_Reflection_FieldAttributesMethodDeclarations.h"
static const EncodedMethodIndex FieldAttributes_t1789_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair FieldAttributes_t1789_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAttributes_t1789_0_0_0;
extern const Il2CppType FieldAttributes_t1789_1_0_0;
const Il2CppTypeDefinitionMetadata FieldAttributes_t1789_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAttributes_t1789_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, FieldAttributes_t1789_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7880/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo FieldAttributes_t1789_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2843/* custom_attributes_cache */
	, &FieldAttributes_t1789_0_0_0/* byval_arg */
	, &FieldAttributes_t1789_1_0_0/* this_arg */
	, &FieldAttributes_t1789_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAttributes_t1789)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FieldAttributes_t1789)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
// Metadata Definition System.Reflection.FieldInfo
extern TypeInfo FieldInfo_t_il2cpp_TypeInfo;
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfoMethodDeclarations.h"
static const EncodedMethodIndex FieldInfo_t_VTable[27] = 
{
	626,
	601,
	627,
	628,
	2629,
	2630,
	0,
	3439,
	0,
	0,
	2677,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3448,
	3449,
	3450,
	3451,
	3452,
	0,
	3454,
	3629,
	3630,
};
extern const Il2CppType _FieldInfo_t3480_0_0_0;
static const Il2CppType* FieldInfo_t_InterfacesTypeInfos[] = 
{
	&_FieldInfo_t3480_0_0_0,
};
static Il2CppInterfaceOffsetPair FieldInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_FieldInfo_t3480_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType FieldInfo_t_1_0_0;
struct FieldInfo_t;
const Il2CppTypeDefinitionMetadata FieldInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, FieldInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, FieldInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, FieldInfo_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11287/* methodStart */
	, -1/* eventStart */
	, 2197/* propertyStart */

};
TypeInfo FieldInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &FieldInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2844/* custom_attributes_cache */
	, &FieldInfo_t_0_0_0/* byval_arg */
	, &FieldInfo_t_1_0_0/* this_arg */
	, &FieldInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldInfo_t)/* instance_size */
	, sizeof (FieldInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 10/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 27/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MemberInfoSerializationHolder
#include "mscorlib_System_Reflection_MemberInfoSerializationHolder.h"
// Metadata Definition System.Reflection.MemberInfoSerializationHolder
extern TypeInfo MemberInfoSerializationHolder_t1790_il2cpp_TypeInfo;
// System.Reflection.MemberInfoSerializationHolder
#include "mscorlib_System_Reflection_MemberInfoSerializationHolderMethodDeclarations.h"
static const EncodedMethodIndex MemberInfoSerializationHolder_t1790_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3631,
	3632,
};
extern const Il2CppType IObjectReference_t2211_0_0_0;
static const Il2CppType* MemberInfoSerializationHolder_t1790_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IObjectReference_t2211_0_0_0,
};
static Il2CppInterfaceOffsetPair MemberInfoSerializationHolder_t1790_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IObjectReference_t2211_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberInfoSerializationHolder_t1790_0_0_0;
extern const Il2CppType MemberInfoSerializationHolder_t1790_1_0_0;
struct MemberInfoSerializationHolder_t1790;
const Il2CppTypeDefinitionMetadata MemberInfoSerializationHolder_t1790_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MemberInfoSerializationHolder_t1790_InterfacesTypeInfos/* implementedInterfaces */
	, MemberInfoSerializationHolder_t1790_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MemberInfoSerializationHolder_t1790_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7900/* fieldStart */
	, 11306/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MemberInfoSerializationHolder_t1790_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberInfoSerializationHolder"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MemberInfoSerializationHolder_t1790_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MemberInfoSerializationHolder_t1790_0_0_0/* byval_arg */
	, &MemberInfoSerializationHolder_t1790_1_0_0/* this_arg */
	, &MemberInfoSerializationHolder_t1790_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberInfoSerializationHolder_t1790)/* instance_size */
	, sizeof (MemberInfoSerializationHolder_t1790)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypes.h"
// Metadata Definition System.Reflection.MemberTypes
extern TypeInfo MemberTypes_t1791_il2cpp_TypeInfo;
// System.Reflection.MemberTypes
#include "mscorlib_System_Reflection_MemberTypesMethodDeclarations.h"
static const EncodedMethodIndex MemberTypes_t1791_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MemberTypes_t1791_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberTypes_t1791_0_0_0;
extern const Il2CppType MemberTypes_t1791_1_0_0;
const Il2CppTypeDefinitionMetadata MemberTypes_t1791_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberTypes_t1791_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MemberTypes_t1791_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7905/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MemberTypes_t1791_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberTypes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2846/* custom_attributes_cache */
	, &MemberTypes_t1791_0_0_0/* byval_arg */
	, &MemberTypes_t1791_1_0_0/* this_arg */
	, &MemberTypes_t1791_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberTypes_t1791)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MemberTypes_t1791)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributes.h"
// Metadata Definition System.Reflection.MethodAttributes
extern TypeInfo MethodAttributes_t1792_il2cpp_TypeInfo;
// System.Reflection.MethodAttributes
#include "mscorlib_System_Reflection_MethodAttributesMethodDeclarations.h"
static const EncodedMethodIndex MethodAttributes_t1792_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MethodAttributes_t1792_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAttributes_t1792_0_0_0;
extern const Il2CppType MethodAttributes_t1792_1_0_0;
const Il2CppTypeDefinitionMetadata MethodAttributes_t1792_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAttributes_t1792_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MethodAttributes_t1792_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7915/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodAttributes_t1792_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2847/* custom_attributes_cache */
	, &MethodAttributes_t1792_0_0_0/* byval_arg */
	, &MethodAttributes_t1792_1_0_0/* this_arg */
	, &MethodAttributes_t1792_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAttributes_t1792)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodAttributes_t1792)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 25/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
// Metadata Definition System.Reflection.MethodBase
extern TypeInfo MethodBase_t1163_il2cpp_TypeInfo;
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
static const EncodedMethodIndex MethodBase_t1163_VTable[30] = 
{
	626,
	601,
	627,
	628,
	2629,
	2630,
	0,
	0,
	0,
	0,
	2677,
	0,
	0,
	0,
	0,
	3621,
	3388,
	0,
	0,
	0,
	3622,
	3393,
	3394,
	3395,
	3396,
	3623,
	3398,
	3399,
	3400,
	3401,
};
static const Il2CppType* MethodBase_t1163_InterfacesTypeInfos[] = 
{
	&_MethodBase_t3481_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodBase_t1163_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_MethodBase_t3481_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodBase_t1163_1_0_0;
struct MethodBase_t1163;
const Il2CppTypeDefinitionMetadata MethodBase_t1163_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodBase_t1163_InterfacesTypeInfos/* implementedInterfaces */
	, MethodBase_t1163_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, MethodBase_t1163_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11311/* methodStart */
	, -1/* eventStart */
	, 2207/* propertyStart */

};
TypeInfo MethodBase_t1163_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodBase"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MethodBase_t1163_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2848/* custom_attributes_cache */
	, &MethodBase_t1163_0_0_0/* byval_arg */
	, &MethodBase_t1163_1_0_0/* this_arg */
	, &MethodBase_t1163_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodBase_t1163)/* instance_size */
	, sizeof (MethodBase_t1163)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 10/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 30/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributes.h"
// Metadata Definition System.Reflection.MethodImplAttributes
extern TypeInfo MethodImplAttributes_t1793_il2cpp_TypeInfo;
// System.Reflection.MethodImplAttributes
#include "mscorlib_System_Reflection_MethodImplAttributesMethodDeclarations.h"
static const EncodedMethodIndex MethodImplAttributes_t1793_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair MethodImplAttributes_t1793_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodImplAttributes_t1793_0_0_0;
extern const Il2CppType MethodImplAttributes_t1793_1_0_0;
const Il2CppTypeDefinitionMetadata MethodImplAttributes_t1793_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodImplAttributes_t1793_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, MethodImplAttributes_t1793_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7940/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MethodImplAttributes_t1793_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodImplAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2851/* custom_attributes_cache */
	, &MethodImplAttributes_t1793_0_0_0/* byval_arg */
	, &MethodImplAttributes_t1793_1_0_0/* this_arg */
	, &MethodImplAttributes_t1793_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodImplAttributes_t1793)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MethodImplAttributes_t1793)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// Metadata Definition System.Reflection.MethodInfo
extern TypeInfo MethodInfo_t_il2cpp_TypeInfo;
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfoMethodDeclarations.h"
static const EncodedMethodIndex MethodInfo_t_VTable[33] = 
{
	626,
	601,
	627,
	628,
	2629,
	2630,
	0,
	3511,
	0,
	0,
	2677,
	0,
	0,
	0,
	0,
	3621,
	3388,
	0,
	0,
	0,
	3622,
	3393,
	3394,
	3395,
	3396,
	3623,
	3633,
	3634,
	3635,
	3636,
	0,
	3637,
	3638,
};
extern const Il2CppType _MethodInfo_t3482_0_0_0;
static const Il2CppType* MethodInfo_t_InterfacesTypeInfos[] = 
{
	&_MethodInfo_t3482_0_0_0,
};
static Il2CppInterfaceOffsetPair MethodInfo_t_InterfacesOffsets[] = 
{
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_MethodInfo_t3482_0_0_0, 30},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType MethodInfo_t_1_0_0;
struct MethodInfo_t;
const Il2CppTypeDefinitionMetadata MethodInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MethodInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, MethodInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MethodBase_t1163_0_0_0/* parent */
	, MethodInfo_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11332/* methodStart */
	, -1/* eventStart */
	, 2217/* propertyStart */

};
TypeInfo MethodInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MethodInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2852/* custom_attributes_cache */
	, &MethodInfo_t_0_0_0/* byval_arg */
	, &MethodInfo_t_1_0_0/* this_arg */
	, &MethodInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodInfo_t)/* instance_size */
	, sizeof (MethodInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.Missing
#include "mscorlib_System_Reflection_Missing.h"
// Metadata Definition System.Reflection.Missing
extern TypeInfo Missing_t1794_il2cpp_TypeInfo;
// System.Reflection.Missing
#include "mscorlib_System_Reflection_MissingMethodDeclarations.h"
static const EncodedMethodIndex Missing_t1794_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3639,
};
static const Il2CppType* Missing_t1794_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair Missing_t1794_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Missing_t1794_0_0_0;
extern const Il2CppType Missing_t1794_1_0_0;
struct Missing_t1794;
const Il2CppTypeDefinitionMetadata Missing_t1794_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Missing_t1794_InterfacesTypeInfos/* implementedInterfaces */
	, Missing_t1794_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Missing_t1794_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7955/* fieldStart */
	, 11341/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Missing_t1794_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Missing"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Missing_t1794_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2855/* custom_attributes_cache */
	, &Missing_t1794_0_0_0/* byval_arg */
	, &Missing_t1794_1_0_0/* this_arg */
	, &Missing_t1794_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Missing_t1794)/* instance_size */
	, sizeof (Missing_t1794)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Missing_t1794_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
// Metadata Definition System.Reflection.Module
extern TypeInfo Module_t1748_il2cpp_TypeInfo;
// System.Reflection.Module
#include "mscorlib_System_Reflection_ModuleMethodDeclarations.h"
static const EncodedMethodIndex Module_t1748_VTable[11] = 
{
	626,
	601,
	627,
	3532,
	3533,
	3534,
	3535,
	3534,
	3533,
	3640,
	3535,
};
extern const Il2CppType _Module_t3483_0_0_0;
static const Il2CppType* Module_t1748_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&ICustomAttributeProvider_t2188_0_0_0,
	&_Module_t3483_0_0_0,
};
static Il2CppInterfaceOffsetPair Module_t1748_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &ICustomAttributeProvider_t2188_0_0_0, 5},
	{ &_Module_t3483_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Module_t1748_0_0_0;
extern const Il2CppType Module_t1748_1_0_0;
struct Module_t1748;
const Il2CppTypeDefinitionMetadata Module_t1748_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Module_t1748_InterfacesTypeInfos/* implementedInterfaces */
	, Module_t1748_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Module_t1748_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7956/* fieldStart */
	, 11344/* methodStart */
	, -1/* eventStart */
	, 2222/* propertyStart */

};
TypeInfo Module_t1748_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Module"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Module_t1748_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2857/* custom_attributes_cache */
	, &Module_t1748_0_0_0/* byval_arg */
	, &Module_t1748_1_0_0/* this_arg */
	, &Module_t1748_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Module_t1748)/* instance_size */
	, sizeof (Module_t1748)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Module_t1748_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8193/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 2/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"
// Metadata Definition System.Reflection.MonoEventInfo
extern TypeInfo MonoEventInfo_t1797_il2cpp_TypeInfo;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfoMethodDeclarations.h"
static const EncodedMethodIndex MonoEventInfo_t1797_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoEventInfo_t1797_0_0_0;
extern const Il2CppType MonoEventInfo_t1797_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata MonoEventInfo_t1797_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MonoEventInfo_t1797_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7966/* fieldStart */
	, 11357/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoEventInfo_t1797_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEventInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoEventInfo_t1797_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEventInfo_t1797_0_0_0/* byval_arg */
	, &MonoEventInfo_t1797_1_0_0/* this_arg */
	, &MonoEventInfo_t1797_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEventInfo_t1797)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoEventInfo_t1797)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.MonoEvent
#include "mscorlib_System_Reflection_MonoEvent.h"
// Metadata Definition System.Reflection.MonoEvent
extern TypeInfo MonoEvent_t_il2cpp_TypeInfo;
// System.Reflection.MonoEvent
#include "mscorlib_System_Reflection_MonoEventMethodDeclarations.h"
static const EncodedMethodIndex MonoEvent_t_VTable[18] = 
{
	626,
	601,
	627,
	3641,
	3642,
	3643,
	3644,
	3624,
	3645,
	3646,
	2677,
	3643,
	3647,
	3642,
	3648,
	3625,
	3649,
	3650,
};
static const Il2CppType* MonoEvent_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoEvent_t_InterfacesOffsets[] = 
{
	{ &_EventInfo_t3479_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &ISerializable_t2210_0_0_0, 17},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoEvent_t_0_0_0;
extern const Il2CppType MonoEvent_t_1_0_0;
struct MonoEvent_t;
const Il2CppTypeDefinitionMetadata MonoEvent_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoEvent_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoEvent_t_InterfacesOffsets/* interfaceOffsets */
	, &EventInfo_t_0_0_0/* parent */
	, MonoEvent_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7974/* fieldStart */
	, 11359/* methodStart */
	, -1/* eventStart */
	, 2224/* propertyStart */

};
TypeInfo MonoEvent_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoEvent"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoEvent_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoEvent_t_0_0_0/* byval_arg */
	, &MonoEvent_t_1_0_0/* this_arg */
	, &MonoEvent_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoEvent_t)/* instance_size */
	, sizeof (MonoEvent_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 4/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MonoField
#include "mscorlib_System_Reflection_MonoField.h"
// Metadata Definition System.Reflection.MonoField
extern TypeInfo MonoField_t_il2cpp_TypeInfo;
// System.Reflection.MonoField
#include "mscorlib_System_Reflection_MonoFieldMethodDeclarations.h"
static const EncodedMethodIndex MonoField_t_VTable[28] = 
{
	626,
	601,
	627,
	3651,
	3652,
	3653,
	3654,
	3439,
	3655,
	3656,
	2677,
	3653,
	3657,
	3652,
	3658,
	3659,
	3660,
	3661,
	3448,
	3449,
	3450,
	3451,
	3452,
	3662,
	3454,
	3663,
	3630,
	3664,
};
static const Il2CppType* MonoField_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoField_t_InterfacesOffsets[] = 
{
	{ &_FieldInfo_t3480_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &ISerializable_t2210_0_0_0, 27},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoField_t_0_0_0;
extern const Il2CppType MonoField_t_1_0_0;
struct MonoField_t;
const Il2CppTypeDefinitionMetadata MonoField_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoField_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoField_t_InterfacesOffsets/* interfaceOffsets */
	, &FieldInfo_t_0_0_0/* parent */
	, MonoField_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7976/* fieldStart */
	, 11370/* methodStart */
	, -1/* eventStart */
	, 2228/* propertyStart */

};
TypeInfo MonoField_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoField"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoField_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoField_t_0_0_0/* byval_arg */
	, &MonoField_t_1_0_0/* this_arg */
	, &MonoField_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoField_t)/* instance_size */
	, sizeof (MonoField_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 6/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MonoGenericMethod
#include "mscorlib_System_Reflection_MonoGenericMethod.h"
// Metadata Definition System.Reflection.MonoGenericMethod
extern TypeInfo MonoGenericMethod_t_il2cpp_TypeInfo;
// System.Reflection.MonoGenericMethod
#include "mscorlib_System_Reflection_MonoGenericMethodMethodDeclarations.h"
static const EncodedMethodIndex MonoGenericMethod_t_VTable[34] = 
{
	626,
	601,
	627,
	3665,
	3666,
	3667,
	3668,
	3511,
	3669,
	3670,
	2677,
	3667,
	3671,
	3666,
	3672,
	3621,
	3388,
	3673,
	3674,
	3675,
	3676,
	3393,
	3394,
	3395,
	3396,
	3623,
	3677,
	3678,
	3679,
	3680,
	3681,
	3682,
	3683,
	3684,
};
static Il2CppInterfaceOffsetPair MonoGenericMethod_t_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 33},
	{ &_MethodInfo_t3482_0_0_0, 30},
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoGenericMethod_t_0_0_0;
extern const Il2CppType MonoGenericMethod_t_1_0_0;
extern const Il2CppType MonoMethod_t_0_0_0;
struct MonoGenericMethod_t;
const Il2CppTypeDefinitionMetadata MonoGenericMethod_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoGenericMethod_t_InterfacesOffsets/* interfaceOffsets */
	, &MonoMethod_t_0_0_0/* parent */
	, MonoGenericMethod_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11389/* methodStart */
	, -1/* eventStart */
	, 2234/* propertyStart */

};
TypeInfo MonoGenericMethod_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoGenericMethod"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoGenericMethod_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoGenericMethod_t_0_0_0/* byval_arg */
	, &MonoGenericMethod_t_1_0_0/* this_arg */
	, &MonoGenericMethod_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoGenericMethod_t)/* instance_size */
	, sizeof (MonoGenericMethod_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 0/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoGenericCMethod
#include "mscorlib_System_Reflection_MonoGenericCMethod.h"
// Metadata Definition System.Reflection.MonoGenericCMethod
extern TypeInfo MonoGenericCMethod_t1798_il2cpp_TypeInfo;
// System.Reflection.MonoGenericCMethod
#include "mscorlib_System_Reflection_MonoGenericCMethodMethodDeclarations.h"
static const EncodedMethodIndex MonoGenericCMethod_t1798_VTable[32] = 
{
	626,
	601,
	627,
	3685,
	3686,
	3687,
	3688,
	3381,
	3689,
	3690,
	2677,
	3687,
	3691,
	3686,
	3692,
	3621,
	3388,
	3693,
	3694,
	3695,
	3696,
	3393,
	3394,
	3395,
	3396,
	3623,
	3398,
	3399,
	3400,
	3401,
	3697,
	3698,
};
static Il2CppInterfaceOffsetPair MonoGenericCMethod_t1798_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 31},
	{ &_ConstructorInfo_t3478_0_0_0, 30},
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoGenericCMethod_t1798_0_0_0;
extern const Il2CppType MonoGenericCMethod_t1798_1_0_0;
extern const Il2CppType MonoCMethod_t1799_0_0_0;
struct MonoGenericCMethod_t1798;
const Il2CppTypeDefinitionMetadata MonoGenericCMethod_t1798_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoGenericCMethod_t1798_InterfacesOffsets/* interfaceOffsets */
	, &MonoCMethod_t1799_0_0_0/* parent */
	, MonoGenericCMethod_t1798_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11391/* methodStart */
	, -1/* eventStart */
	, 2235/* propertyStart */

};
TypeInfo MonoGenericCMethod_t1798_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoGenericCMethod"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoGenericCMethod_t1798_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoGenericCMethod_t1798_0_0_0/* byval_arg */
	, &MonoGenericCMethod_t1798_1_0_0/* this_arg */
	, &MonoGenericCMethod_t1798_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoGenericCMethod_t1798)/* instance_size */
	, sizeof (MonoGenericCMethod_t1798)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 0/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
// Metadata Definition System.Reflection.MonoMethodInfo
extern TypeInfo MonoMethodInfo_t1800_il2cpp_TypeInfo;
// System.Reflection.MonoMethodInfo
#include "mscorlib_System_Reflection_MonoMethodInfoMethodDeclarations.h"
static const EncodedMethodIndex MonoMethodInfo_t1800_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethodInfo_t1800_0_0_0;
extern const Il2CppType MonoMethodInfo_t1800_1_0_0;
const Il2CppTypeDefinitionMetadata MonoMethodInfo_t1800_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MonoMethodInfo_t1800_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7981/* fieldStart */
	, 11393/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoMethodInfo_t1800_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethodInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoMethodInfo_t1800_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethodInfo_t1800_0_0_0/* byval_arg */
	, &MonoMethodInfo_t1800_1_0_0/* this_arg */
	, &MonoMethodInfo_t1800_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethodInfo_t1800)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoMethodInfo_t1800)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.MonoMethod
#include "mscorlib_System_Reflection_MonoMethod.h"
// Metadata Definition System.Reflection.MonoMethod
extern TypeInfo MonoMethod_t_il2cpp_TypeInfo;
// System.Reflection.MonoMethod
#include "mscorlib_System_Reflection_MonoMethodMethodDeclarations.h"
static const EncodedMethodIndex MonoMethod_t_VTable[34] = 
{
	626,
	601,
	627,
	3665,
	3666,
	3667,
	3668,
	3511,
	3669,
	3699,
	2677,
	3667,
	3671,
	3666,
	3672,
	3621,
	3388,
	3673,
	3674,
	3675,
	3676,
	3393,
	3394,
	3395,
	3396,
	3623,
	3677,
	3678,
	3679,
	3680,
	3681,
	3682,
	3683,
	3684,
};
static const Il2CppType* MonoMethod_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoMethod_t_InterfacesOffsets[] = 
{
	{ &_MethodInfo_t3482_0_0_0, 30},
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &ISerializable_t2210_0_0_0, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoMethod_t_1_0_0;
struct MonoMethod_t;
const Il2CppTypeDefinitionMetadata MonoMethod_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoMethod_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoMethod_t_InterfacesOffsets/* interfaceOffsets */
	, &MethodInfo_t_0_0_0/* parent */
	, MonoMethod_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7986/* fieldStart */
	, 11401/* methodStart */
	, -1/* eventStart */
	, 2236/* propertyStart */

};
TypeInfo MonoMethod_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoMethod"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoMethod_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoMethod_t_0_0_0/* byval_arg */
	, &MonoMethod_t_1_0_0/* this_arg */
	, &MonoMethod_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoMethod_t)/* instance_size */
	, sizeof (MonoMethod_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 28/* method_count */
	, 10/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoCMethod
#include "mscorlib_System_Reflection_MonoCMethod.h"
// Metadata Definition System.Reflection.MonoCMethod
extern TypeInfo MonoCMethod_t1799_il2cpp_TypeInfo;
// System.Reflection.MonoCMethod
#include "mscorlib_System_Reflection_MonoCMethodMethodDeclarations.h"
static const EncodedMethodIndex MonoCMethod_t1799_VTable[32] = 
{
	626,
	601,
	627,
	3685,
	3686,
	3687,
	3688,
	3381,
	3689,
	3700,
	2677,
	3687,
	3691,
	3686,
	3692,
	3621,
	3388,
	3693,
	3694,
	3695,
	3696,
	3393,
	3394,
	3395,
	3396,
	3623,
	3398,
	3399,
	3400,
	3401,
	3697,
	3698,
};
static const Il2CppType* MonoCMethod_t1799_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair MonoCMethod_t1799_InterfacesOffsets[] = 
{
	{ &_ConstructorInfo_t3478_0_0_0, 30},
	{ &_MethodBase_t3481_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &ISerializable_t2210_0_0_0, 31},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCMethod_t1799_1_0_0;
struct MonoCMethod_t1799;
const Il2CppTypeDefinitionMetadata MonoCMethod_t1799_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoCMethod_t1799_InterfacesTypeInfos/* implementedInterfaces */
	, MonoCMethod_t1799_InterfacesOffsets/* interfaceOffsets */
	, &ConstructorInfo_t996_0_0_0/* parent */
	, MonoCMethod_t1799_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7989/* fieldStart */
	, 11429/* methodStart */
	, -1/* eventStart */
	, 2246/* propertyStart */

};
TypeInfo MonoCMethod_t1799_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCMethod"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoCMethod_t1799_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCMethod_t1799_0_0_0/* byval_arg */
	, &MonoCMethod_t1799_1_0_0/* this_arg */
	, &MonoCMethod_t1799_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCMethod_t1799)/* instance_size */
	, sizeof (MonoCMethod_t1799)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 6/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
// Metadata Definition System.Reflection.MonoPropertyInfo
extern TypeInfo MonoPropertyInfo_t1801_il2cpp_TypeInfo;
// System.Reflection.MonoPropertyInfo
#include "mscorlib_System_Reflection_MonoPropertyInfoMethodDeclarations.h"
static const EncodedMethodIndex MonoPropertyInfo_t1801_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoPropertyInfo_t1801_0_0_0;
extern const Il2CppType MonoPropertyInfo_t1801_1_0_0;
const Il2CppTypeDefinitionMetadata MonoPropertyInfo_t1801_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, MonoPropertyInfo_t1801_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7992/* fieldStart */
	, 11445/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MonoPropertyInfo_t1801_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoPropertyInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoPropertyInfo_t1801_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoPropertyInfo_t1801_0_0_0/* byval_arg */
	, &MonoPropertyInfo_t1801_1_0_0/* this_arg */
	, &MonoPropertyInfo_t1801_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoPropertyInfo_t1801)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (MonoPropertyInfo_t1801)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfo.h"
// Metadata Definition System.Reflection.PInfo
extern TypeInfo PInfo_t1802_il2cpp_TypeInfo;
// System.Reflection.PInfo
#include "mscorlib_System_Reflection_PInfoMethodDeclarations.h"
static const EncodedMethodIndex PInfo_t1802_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair PInfo_t1802_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PInfo_t1802_0_0_0;
extern const Il2CppType PInfo_t1802_1_0_0;
const Il2CppTypeDefinitionMetadata PInfo_t1802_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PInfo_t1802_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, PInfo_t1802_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 7997/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PInfo_t1802_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2858/* custom_attributes_cache */
	, &PInfo_t1802_0_0_0/* byval_arg */
	, &PInfo_t1802_1_0_0/* this_arg */
	, &PInfo_t1802_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PInfo_t1802)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PInfo_t1802)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoProperty.h"
// Metadata Definition System.Reflection.MonoProperty
extern TypeInfo MonoProperty_t_il2cpp_TypeInfo;
// System.Reflection.MonoProperty
#include "mscorlib_System_Reflection_MonoPropertyMethodDeclarations.h"
extern const Il2CppType MonoProperty_GetterAdapterFrame_m24939_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m24939_gp_1_0_0_0;
extern const Il2CppRGCTXDefinition MonoProperty_GetterAdapterFrame_m24939_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, 5246 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, 5716 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5247 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m24940_gp_0_0_0_0;
extern const Il2CppRGCTXDefinition MonoProperty_StaticGetterAdapterFrame_m24940_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, 5717 }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, 5249 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern const Il2CppType GetterAdapter_t1803_0_0_0;
extern const Il2CppType Getter_2_t2196_0_0_0;
extern const Il2CppType StaticGetter_1_t2195_0_0_0;
static const Il2CppType* MonoProperty_t_il2cpp_TypeInfo__nestedTypes[3] =
{
	&GetterAdapter_t1803_0_0_0,
	&Getter_2_t2196_0_0_0,
	&StaticGetter_1_t2195_0_0_0,
};
static const EncodedMethodIndex MonoProperty_t_VTable[29] = 
{
	626,
	601,
	627,
	3701,
	3702,
	3703,
	3704,
	3547,
	3705,
	3706,
	2677,
	3703,
	3707,
	3702,
	3708,
	3709,
	3710,
	3711,
	3712,
	3713,
	3714,
	3715,
	3716,
	3717,
	3718,
	3719,
	3720,
	3721,
	3722,
};
static const Il2CppType* MonoProperty_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
extern const Il2CppType _PropertyInfo_t3485_0_0_0;
static Il2CppInterfaceOffsetPair MonoProperty_t_InterfacesOffsets[] = 
{
	{ &_PropertyInfo_t3485_0_0_0, 14},
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &ISerializable_t2210_0_0_0, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoProperty_t_0_0_0;
extern const Il2CppType MonoProperty_t_1_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
struct MonoProperty_t;
const Il2CppTypeDefinitionMetadata MonoProperty_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoProperty_t_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, MonoProperty_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoProperty_t_InterfacesOffsets/* interfaceOffsets */
	, &PropertyInfo_t_0_0_0/* parent */
	, MonoProperty_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8004/* fieldStart */
	, 11447/* methodStart */
	, -1/* eventStart */
	, 2252/* propertyStart */

};
TypeInfo MonoProperty_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoProperty"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &MonoProperty_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoProperty_t_0_0_0/* byval_arg */
	, &MonoProperty_t_1_0_0/* this_arg */
	, &MonoProperty_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoProperty_t)/* instance_size */
	, sizeof (MonoProperty_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 26/* method_count */
	, 7/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 29/* vtable_count */
	, 1/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.Reflection.MonoProperty/GetterAdapter
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapter.h"
// Metadata Definition System.Reflection.MonoProperty/GetterAdapter
extern TypeInfo GetterAdapter_t1803_il2cpp_TypeInfo;
// System.Reflection.MonoProperty/GetterAdapter
#include "mscorlib_System_Reflection_MonoProperty_GetterAdapterMethodDeclarations.h"
static const EncodedMethodIndex GetterAdapter_t1803_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	3723,
	3724,
	3725,
};
static Il2CppInterfaceOffsetPair GetterAdapter_t1803_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GetterAdapter_t1803_1_0_0;
struct GetterAdapter_t1803;
const Il2CppTypeDefinitionMetadata GetterAdapter_t1803_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetterAdapter_t1803_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, GetterAdapter_t1803_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11473/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GetterAdapter_t1803_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetterAdapter"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &GetterAdapter_t1803_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetterAdapter_t1803_0_0_0/* byval_arg */
	, &GetterAdapter_t1803_1_0_0/* this_arg */
	, &GetterAdapter_t1803_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetterAdapter_t1803/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetterAdapter_t1803)/* instance_size */
	, sizeof (GetterAdapter_t1803)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.MonoProperty/Getter`2
extern TypeInfo Getter_2_t2196_il2cpp_TypeInfo;
static const EncodedMethodIndex Getter_2_t2196_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	3726,
	3727,
	3728,
};
static Il2CppInterfaceOffsetPair Getter_2_t2196_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Getter_2_t2196_1_0_0;
struct Getter_2_t2196;
const Il2CppTypeDefinitionMetadata Getter_2_t2196_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Getter_2_t2196_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, Getter_2_t2196_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11477/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Getter_2_t2196_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Getter`2"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &Getter_2_t2196_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Getter_2_t2196_0_0_0/* byval_arg */
	, &Getter_2_t2196_1_0_0/* this_arg */
	, &Getter_2_t2196_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 170/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Reflection.MonoProperty/StaticGetter`1
extern TypeInfo StaticGetter_1_t2195_il2cpp_TypeInfo;
static const EncodedMethodIndex StaticGetter_1_t2195_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	3729,
	3730,
	3731,
};
static Il2CppInterfaceOffsetPair StaticGetter_1_t2195_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StaticGetter_1_t2195_1_0_0;
struct StaticGetter_1_t2195;
const Il2CppTypeDefinitionMetadata StaticGetter_1_t2195_DefinitionMetadata = 
{
	&MonoProperty_t_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StaticGetter_1_t2195_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, StaticGetter_1_t2195_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11481/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StaticGetter_1_t2195_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StaticGetter`1"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &StaticGetter_1_t2195_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StaticGetter_1_t2195_0_0_0/* byval_arg */
	, &StaticGetter_1_t2195_1_0_0/* this_arg */
	, &StaticGetter_1_t2195_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, 171/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributes.h"
// Metadata Definition System.Reflection.ParameterAttributes
extern TypeInfo ParameterAttributes_t1804_il2cpp_TypeInfo;
// System.Reflection.ParameterAttributes
#include "mscorlib_System_Reflection_ParameterAttributesMethodDeclarations.h"
static const EncodedMethodIndex ParameterAttributes_t1804_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ParameterAttributes_t1804_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterAttributes_t1804_0_0_0;
extern const Il2CppType ParameterAttributes_t1804_1_0_0;
const Il2CppTypeDefinitionMetadata ParameterAttributes_t1804_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ParameterAttributes_t1804_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ParameterAttributes_t1804_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8009/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ParameterAttributes_t1804_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2859/* custom_attributes_cache */
	, &ParameterAttributes_t1804_0_0_0/* byval_arg */
	, &ParameterAttributes_t1804_1_0_0/* this_arg */
	, &ParameterAttributes_t1804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterAttributes_t1804)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ParameterAttributes_t1804)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
// Metadata Definition System.Reflection.ParameterInfo
extern TypeInfo ParameterInfo_t1154_il2cpp_TypeInfo;
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
static const EncodedMethodIndex ParameterInfo_t1154_VTable[13] = 
{
	626,
	601,
	627,
	3732,
	3733,
	3734,
	3735,
	3736,
	3737,
	3738,
	3739,
	3733,
	3734,
};
extern const Il2CppType _ParameterInfo_t3484_0_0_0;
static const Il2CppType* ParameterInfo_t1154_InterfacesTypeInfos[] = 
{
	&ICustomAttributeProvider_t2188_0_0_0,
	&_ParameterInfo_t3484_0_0_0,
};
static Il2CppInterfaceOffsetPair ParameterInfo_t1154_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_ParameterInfo_t3484_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterInfo_t1154_0_0_0;
extern const Il2CppType ParameterInfo_t1154_1_0_0;
struct ParameterInfo_t1154;
const Il2CppTypeDefinitionMetadata ParameterInfo_t1154_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ParameterInfo_t1154_InterfacesTypeInfos/* implementedInterfaces */
	, ParameterInfo_t1154_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ParameterInfo_t1154_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8021/* fieldStart */
	, 11485/* methodStart */
	, -1/* eventStart */
	, 2259/* propertyStart */

};
TypeInfo ParameterInfo_t1154_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &ParameterInfo_t1154_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2860/* custom_attributes_cache */
	, &ParameterInfo_t1154_0_0_0/* byval_arg */
	, &ParameterInfo_t1154_1_0_0/* this_arg */
	, &ParameterInfo_t1154_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ParameterInfo_t1154)/* instance_size */
	, sizeof (ParameterInfo_t1154)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 9/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
// Metadata Definition System.Reflection.ParameterModifier
extern TypeInfo ParameterModifier_t1805_il2cpp_TypeInfo;
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifierMethodDeclarations.h"
static const EncodedMethodIndex ParameterModifier_t1805_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ParameterModifier_t1805_0_0_0;
extern const Il2CppType ParameterModifier_t1805_1_0_0;
const Il2CppTypeDefinitionMetadata ParameterModifier_t1805_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, ParameterModifier_t1805_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8028/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ParameterModifier_t1805_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ParameterModifier"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &ParameterModifier_t1805_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2861/* custom_attributes_cache */
	, &ParameterModifier_t1805_0_0_0/* byval_arg */
	, &ParameterModifier_t1805_1_0_0/* this_arg */
	, &ParameterModifier_t1805_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)ParameterModifier_t1805_marshal/* marshal_to_native_func */
	, (methodPointerType)ParameterModifier_t1805_marshal_back/* marshal_from_native_func */
	, (methodPointerType)ParameterModifier_t1805_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (ParameterModifier_t1805)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ParameterModifier_t1805)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(ParameterModifier_t1805_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Reflection.Pointer
#include "mscorlib_System_Reflection_Pointer.h"
// Metadata Definition System.Reflection.Pointer
extern TypeInfo Pointer_t1806_il2cpp_TypeInfo;
// System.Reflection.Pointer
#include "mscorlib_System_Reflection_PointerMethodDeclarations.h"
static const EncodedMethodIndex Pointer_t1806_VTable[5] = 
{
	626,
	601,
	627,
	628,
	3740,
};
static const Il2CppType* Pointer_t1806_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
};
static Il2CppInterfaceOffsetPair Pointer_t1806_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Pointer_t1806_0_0_0;
extern const Il2CppType Pointer_t1806_1_0_0;
struct Pointer_t1806;
const Il2CppTypeDefinitionMetadata Pointer_t1806_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Pointer_t1806_InterfacesTypeInfos/* implementedInterfaces */
	, Pointer_t1806_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Pointer_t1806_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8029/* fieldStart */
	, 11501/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Pointer_t1806_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Pointer"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Pointer_t1806_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2862/* custom_attributes_cache */
	, &Pointer_t1806_0_0_0/* byval_arg */
	, &Pointer_t1806_1_0_0/* this_arg */
	, &Pointer_t1806_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Pointer_t1806)/* instance_size */
	, sizeof (Pointer_t1806)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitecture.h"
// Metadata Definition System.Reflection.ProcessorArchitecture
extern TypeInfo ProcessorArchitecture_t1807_il2cpp_TypeInfo;
// System.Reflection.ProcessorArchitecture
#include "mscorlib_System_Reflection_ProcessorArchitectureMethodDeclarations.h"
static const EncodedMethodIndex ProcessorArchitecture_t1807_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ProcessorArchitecture_t1807_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ProcessorArchitecture_t1807_0_0_0;
extern const Il2CppType ProcessorArchitecture_t1807_1_0_0;
const Il2CppTypeDefinitionMetadata ProcessorArchitecture_t1807_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ProcessorArchitecture_t1807_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ProcessorArchitecture_t1807_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8031/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ProcessorArchitecture_t1807_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProcessorArchitecture"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2863/* custom_attributes_cache */
	, &ProcessorArchitecture_t1807_0_0_0/* byval_arg */
	, &ProcessorArchitecture_t1807_1_0_0/* this_arg */
	, &ProcessorArchitecture_t1807_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ProcessorArchitecture_t1807)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ProcessorArchitecture_t1807)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"
// Metadata Definition System.Reflection.PropertyAttributes
extern TypeInfo PropertyAttributes_t1808_il2cpp_TypeInfo;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributesMethodDeclarations.h"
static const EncodedMethodIndex PropertyAttributes_t1808_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair PropertyAttributes_t1808_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyAttributes_t1808_0_0_0;
extern const Il2CppType PropertyAttributes_t1808_1_0_0;
const Il2CppTypeDefinitionMetadata PropertyAttributes_t1808_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttributes_t1808_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, PropertyAttributes_t1808_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8037/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PropertyAttributes_t1808_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2864/* custom_attributes_cache */
	, &PropertyAttributes_t1808_0_0_0/* byval_arg */
	, &PropertyAttributes_t1808_1_0_0/* this_arg */
	, &PropertyAttributes_t1808_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttributes_t1808)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PropertyAttributes_t1808)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 9/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
// Metadata Definition System.Reflection.PropertyInfo
extern TypeInfo PropertyInfo_t_il2cpp_TypeInfo;
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfoMethodDeclarations.h"
static const EncodedMethodIndex PropertyInfo_t_VTable[28] = 
{
	626,
	601,
	627,
	628,
	2629,
	2630,
	0,
	3547,
	0,
	0,
	2677,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	3741,
	0,
	3718,
	0,
	3564,
	3565,
};
static const Il2CppType* PropertyInfo_t_InterfacesTypeInfos[] = 
{
	&_PropertyInfo_t3485_0_0_0,
};
static Il2CppInterfaceOffsetPair PropertyInfo_t_InterfacesOffsets[] = 
{
	{ &ICustomAttributeProvider_t2188_0_0_0, 4},
	{ &_MemberInfo_t3441_0_0_0, 6},
	{ &_PropertyInfo_t3485_0_0_0, 14},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PropertyInfo_t_1_0_0;
struct PropertyInfo_t;
const Il2CppTypeDefinitionMetadata PropertyInfo_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PropertyInfo_t_InterfacesTypeInfos/* implementedInterfaces */
	, PropertyInfo_t_InterfacesOffsets/* interfaceOffsets */
	, &MemberInfo_t_0_0_0/* parent */
	, PropertyInfo_t_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11503/* methodStart */
	, -1/* eventStart */
	, 2268/* propertyStart */

};
TypeInfo PropertyInfo_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyInfo"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &PropertyInfo_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2865/* custom_attributes_cache */
	, &PropertyInfo_t_0_0_0/* byval_arg */
	, &PropertyInfo_t_1_0_0/* this_arg */
	, &PropertyInfo_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyInfo_t)/* instance_size */
	, sizeof (PropertyInfo_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 16/* method_count */
	, 5/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Reflection.StrongNameKeyPair
#include "mscorlib_System_Reflection_StrongNameKeyPair.h"
// Metadata Definition System.Reflection.StrongNameKeyPair
extern TypeInfo StrongNameKeyPair_t1778_il2cpp_TypeInfo;
// System.Reflection.StrongNameKeyPair
#include "mscorlib_System_Reflection_StrongNameKeyPairMethodDeclarations.h"
static const EncodedMethodIndex StrongNameKeyPair_t1778_VTable[6] = 
{
	626,
	601,
	627,
	628,
	3742,
	3743,
};
static const Il2CppType* StrongNameKeyPair_t1778_InterfacesTypeInfos[] = 
{
	&ISerializable_t2210_0_0_0,
	&IDeserializationCallback_t2213_0_0_0,
};
static Il2CppInterfaceOffsetPair StrongNameKeyPair_t1778_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &IDeserializationCallback_t2213_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StrongNameKeyPair_t1778_0_0_0;
extern const Il2CppType StrongNameKeyPair_t1778_1_0_0;
struct StrongNameKeyPair_t1778;
const Il2CppTypeDefinitionMetadata StrongNameKeyPair_t1778_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StrongNameKeyPair_t1778_InterfacesTypeInfos/* implementedInterfaces */
	, StrongNameKeyPair_t1778_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StrongNameKeyPair_t1778_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8046/* fieldStart */
	, 11519/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StrongNameKeyPair_t1778_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StrongNameKeyPair"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &StrongNameKeyPair_t1778_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2868/* custom_attributes_cache */
	, &StrongNameKeyPair_t1778_0_0_0/* byval_arg */
	, &StrongNameKeyPair_t1778_1_0_0/* this_arg */
	, &StrongNameKeyPair_t1778_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StrongNameKeyPair_t1778)/* instance_size */
	, sizeof (StrongNameKeyPair_t1778)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetException
#include "mscorlib_System_Reflection_TargetException.h"
// Metadata Definition System.Reflection.TargetException
extern TypeInfo TargetException_t1809_il2cpp_TypeInfo;
// System.Reflection.TargetException
#include "mscorlib_System_Reflection_TargetExceptionMethodDeclarations.h"
static const EncodedMethodIndex TargetException_t1809_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair TargetException_t1809_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetException_t1809_0_0_0;
extern const Il2CppType TargetException_t1809_1_0_0;
extern const Il2CppType Exception_t520_0_0_0;
struct TargetException_t1809;
const Il2CppTypeDefinitionMetadata TargetException_t1809_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetException_t1809_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, TargetException_t1809_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11522/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TargetException_t1809_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetException"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &TargetException_t1809_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2869/* custom_attributes_cache */
	, &TargetException_t1809_0_0_0/* byval_arg */
	, &TargetException_t1809_1_0_0/* this_arg */
	, &TargetException_t1809_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetException_t1809)/* instance_size */
	, sizeof (TargetException_t1809)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetInvocationException
#include "mscorlib_System_Reflection_TargetInvocationException.h"
// Metadata Definition System.Reflection.TargetInvocationException
extern TypeInfo TargetInvocationException_t1810_il2cpp_TypeInfo;
// System.Reflection.TargetInvocationException
#include "mscorlib_System_Reflection_TargetInvocationExceptionMethodDeclarations.h"
static const EncodedMethodIndex TargetInvocationException_t1810_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair TargetInvocationException_t1810_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetInvocationException_t1810_0_0_0;
extern const Il2CppType TargetInvocationException_t1810_1_0_0;
struct TargetInvocationException_t1810;
const Il2CppTypeDefinitionMetadata TargetInvocationException_t1810_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetInvocationException_t1810_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, TargetInvocationException_t1810_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11525/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TargetInvocationException_t1810_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetInvocationException"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &TargetInvocationException_t1810_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2870/* custom_attributes_cache */
	, &TargetInvocationException_t1810_0_0_0/* byval_arg */
	, &TargetInvocationException_t1810_1_0_0/* this_arg */
	, &TargetInvocationException_t1810_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetInvocationException_t1810)/* instance_size */
	, sizeof (TargetInvocationException_t1810)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TargetParameterCountException
#include "mscorlib_System_Reflection_TargetParameterCountException.h"
// Metadata Definition System.Reflection.TargetParameterCountException
extern TypeInfo TargetParameterCountException_t1811_il2cpp_TypeInfo;
// System.Reflection.TargetParameterCountException
#include "mscorlib_System_Reflection_TargetParameterCountExceptionMethodDeclarations.h"
static const EncodedMethodIndex TargetParameterCountException_t1811_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair TargetParameterCountException_t1811_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TargetParameterCountException_t1811_0_0_0;
extern const Il2CppType TargetParameterCountException_t1811_1_0_0;
struct TargetParameterCountException_t1811;
const Il2CppTypeDefinitionMetadata TargetParameterCountException_t1811_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TargetParameterCountException_t1811_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t520_0_0_0/* parent */
	, TargetParameterCountException_t1811_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11527/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TargetParameterCountException_t1811_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetParameterCountException"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &TargetParameterCountException_t1811_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2871/* custom_attributes_cache */
	, &TargetParameterCountException_t1811_0_0_0/* byval_arg */
	, &TargetParameterCountException_t1811_1_0_0/* this_arg */
	, &TargetParameterCountException_t1811_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TargetParameterCountException_t1811)/* instance_size */
	, sizeof (TargetParameterCountException_t1811)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"
// Metadata Definition System.Reflection.TypeAttributes
extern TypeInfo TypeAttributes_t1812_il2cpp_TypeInfo;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributesMethodDeclarations.h"
static const EncodedMethodIndex TypeAttributes_t1812_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair TypeAttributes_t1812_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeAttributes_t1812_0_0_0;
extern const Il2CppType TypeAttributes_t1812_1_0_0;
const Il2CppTypeDefinitionMetadata TypeAttributes_t1812_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeAttributes_t1812_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, TypeAttributes_t1812_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8050/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeAttributes_t1812_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeAttributes"/* name */
	, "System.Reflection"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2872/* custom_attributes_cache */
	, &TypeAttributes_t1812_0_0_0/* byval_arg */
	, &TypeAttributes_t1812_1_0_0/* this_arg */
	, &TypeAttributes_t1812_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeAttributes_t1812)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeAttributes_t1812)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 32/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// Metadata Definition System.Resources.NeutralResourcesLanguageAttribute
extern TypeInfo NeutralResourcesLanguageAttribute_t1813_il2cpp_TypeInfo;
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
static const EncodedMethodIndex NeutralResourcesLanguageAttribute_t1813_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair NeutralResourcesLanguageAttribute_t1813_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NeutralResourcesLanguageAttribute_t1813_0_0_0;
extern const Il2CppType NeutralResourcesLanguageAttribute_t1813_1_0_0;
struct NeutralResourcesLanguageAttribute_t1813;
const Il2CppTypeDefinitionMetadata NeutralResourcesLanguageAttribute_t1813_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NeutralResourcesLanguageAttribute_t1813_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, NeutralResourcesLanguageAttribute_t1813_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8082/* fieldStart */
	, 11530/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo NeutralResourcesLanguageAttribute_t1813_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NeutralResourcesLanguageAttribute"/* name */
	, "System.Resources"/* namespaze */
	, NULL/* methods */
	, &NeutralResourcesLanguageAttribute_t1813_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2873/* custom_attributes_cache */
	, &NeutralResourcesLanguageAttribute_t1813_0_0_0/* byval_arg */
	, &NeutralResourcesLanguageAttribute_t1813_1_0_0/* this_arg */
	, &NeutralResourcesLanguageAttribute_t1813_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NeutralResourcesLanguageAttribute_t1813)/* instance_size */
	, sizeof (NeutralResourcesLanguageAttribute_t1813)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// Metadata Definition System.Resources.SatelliteContractVersionAttribute
extern TypeInfo SatelliteContractVersionAttribute_t1814_il2cpp_TypeInfo;
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
static const EncodedMethodIndex SatelliteContractVersionAttribute_t1814_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair SatelliteContractVersionAttribute_t1814_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SatelliteContractVersionAttribute_t1814_0_0_0;
extern const Il2CppType SatelliteContractVersionAttribute_t1814_1_0_0;
struct SatelliteContractVersionAttribute_t1814;
const Il2CppTypeDefinitionMetadata SatelliteContractVersionAttribute_t1814_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SatelliteContractVersionAttribute_t1814_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, SatelliteContractVersionAttribute_t1814_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8083/* fieldStart */
	, 11531/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo SatelliteContractVersionAttribute_t1814_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SatelliteContractVersionAttribute"/* name */
	, "System.Resources"/* namespaze */
	, NULL/* methods */
	, &SatelliteContractVersionAttribute_t1814_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2874/* custom_attributes_cache */
	, &SatelliteContractVersionAttribute_t1814_0_0_0/* byval_arg */
	, &SatelliteContractVersionAttribute_t1814_1_0_0/* this_arg */
	, &SatelliteContractVersionAttribute_t1814_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SatelliteContractVersionAttribute_t1814)/* instance_size */
	, sizeof (SatelliteContractVersionAttribute_t1814)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxations
extern TypeInfo CompilationRelaxations_t1815_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxations
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
static const EncodedMethodIndex CompilationRelaxations_t1815_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CompilationRelaxations_t1815_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilationRelaxations_t1815_0_0_0;
extern const Il2CppType CompilationRelaxations_t1815_1_0_0;
const Il2CppTypeDefinitionMetadata CompilationRelaxations_t1815_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilationRelaxations_t1815_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CompilationRelaxations_t1815_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8084/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompilationRelaxations_t1815_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilationRelaxations"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2875/* custom_attributes_cache */
	, &CompilationRelaxations_t1815_0_0_0/* byval_arg */
	, &CompilationRelaxations_t1815_1_0_0/* this_arg */
	, &CompilationRelaxations_t1815_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilationRelaxations_t1815)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CompilationRelaxations_t1815)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0.h"
// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxationsAttribute
extern TypeInfo CompilationRelaxationsAttribute_t1816_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati_0MethodDeclarations.h"
static const EncodedMethodIndex CompilationRelaxationsAttribute_t1816_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair CompilationRelaxationsAttribute_t1816_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CompilationRelaxationsAttribute_t1816_0_0_0;
extern const Il2CppType CompilationRelaxationsAttribute_t1816_1_0_0;
struct CompilationRelaxationsAttribute_t1816;
const Il2CppTypeDefinitionMetadata CompilationRelaxationsAttribute_t1816_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CompilationRelaxationsAttribute_t1816_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, CompilationRelaxationsAttribute_t1816_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8086/* fieldStart */
	, 11532/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CompilationRelaxationsAttribute_t1816_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CompilationRelaxationsAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &CompilationRelaxationsAttribute_t1816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2876/* custom_attributes_cache */
	, &CompilationRelaxationsAttribute_t1816_0_0_0/* byval_arg */
	, &CompilationRelaxationsAttribute_t1816_1_0_0/* this_arg */
	, &CompilationRelaxationsAttribute_t1816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CompilationRelaxationsAttribute_t1816)/* instance_size */
	, sizeof (CompilationRelaxationsAttribute_t1816)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// Metadata Definition System.Runtime.CompilerServices.DefaultDependencyAttribute
extern TypeInfo DefaultDependencyAttribute_t1817_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
static const EncodedMethodIndex DefaultDependencyAttribute_t1817_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DefaultDependencyAttribute_t1817_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DefaultDependencyAttribute_t1817_0_0_0;
extern const Il2CppType DefaultDependencyAttribute_t1817_1_0_0;
struct DefaultDependencyAttribute_t1817;
const Il2CppTypeDefinitionMetadata DefaultDependencyAttribute_t1817_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultDependencyAttribute_t1817_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DefaultDependencyAttribute_t1817_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8087/* fieldStart */
	, 11533/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DefaultDependencyAttribute_t1817_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultDependencyAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &DefaultDependencyAttribute_t1817_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2877/* custom_attributes_cache */
	, &DefaultDependencyAttribute_t1817_0_0_0/* byval_arg */
	, &DefaultDependencyAttribute_t1817_1_0_0/* this_arg */
	, &DefaultDependencyAttribute_t1817_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultDependencyAttribute_t1817)/* instance_size */
	, sizeof (DefaultDependencyAttribute_t1817)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.CompilerServices.IsVolatile
#include "mscorlib_System_Runtime_CompilerServices_IsVolatile.h"
// Metadata Definition System.Runtime.CompilerServices.IsVolatile
extern TypeInfo IsVolatile_t1818_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.IsVolatile
#include "mscorlib_System_Runtime_CompilerServices_IsVolatileMethodDeclarations.h"
static const EncodedMethodIndex IsVolatile_t1818_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IsVolatile_t1818_0_0_0;
extern const Il2CppType IsVolatile_t1818_1_0_0;
struct IsVolatile_t1818;
const Il2CppTypeDefinitionMetadata IsVolatile_t1818_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IsVolatile_t1818_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo IsVolatile_t1818_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IsVolatile"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &IsVolatile_t1818_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2878/* custom_attributes_cache */
	, &IsVolatile_t1818_0_0_0/* byval_arg */
	, &IsVolatile_t1818_1_0_0/* this_arg */
	, &IsVolatile_t1818_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IsVolatile_t1818)/* instance_size */
	, sizeof (IsVolatile_t1818)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHint.h"
// Metadata Definition System.Runtime.CompilerServices.LoadHint
extern TypeInfo LoadHint_t1819_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.LoadHint
#include "mscorlib_System_Runtime_CompilerServices_LoadHintMethodDeclarations.h"
static const EncodedMethodIndex LoadHint_t1819_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair LoadHint_t1819_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoadHint_t1819_0_0_0;
extern const Il2CppType LoadHint_t1819_1_0_0;
const Il2CppTypeDefinitionMetadata LoadHint_t1819_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoadHint_t1819_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, LoadHint_t1819_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8088/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo LoadHint_t1819_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoadHint"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LoadHint_t1819_0_0_0/* byval_arg */
	, &LoadHint_t1819_1_0_0/* this_arg */
	, &LoadHint_t1819_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoadHint_t1819)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoadHint_t1819)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// Metadata Definition System.Runtime.CompilerServices.StringFreezingAttribute
extern TypeInfo StringFreezingAttribute_t1820_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
static const EncodedMethodIndex StringFreezingAttribute_t1820_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair StringFreezingAttribute_t1820_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringFreezingAttribute_t1820_0_0_0;
extern const Il2CppType StringFreezingAttribute_t1820_1_0_0;
struct StringFreezingAttribute_t1820;
const Il2CppTypeDefinitionMetadata StringFreezingAttribute_t1820_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringFreezingAttribute_t1820_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, StringFreezingAttribute_t1820_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11534/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo StringFreezingAttribute_t1820_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringFreezingAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, NULL/* methods */
	, &StringFreezingAttribute_t1820_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2879/* custom_attributes_cache */
	, &StringFreezingAttribute_t1820_0_0_0/* byval_arg */
	, &StringFreezingAttribute_t1820_1_0_0/* this_arg */
	, &StringFreezingAttribute_t1820_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringFreezingAttribute_t1820)/* instance_size */
	, sizeof (StringFreezingAttribute_t1820)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"
// Metadata Definition System.Runtime.ConstrainedExecution.Cer
extern TypeInfo Cer_t1821_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_CerMethodDeclarations.h"
static const EncodedMethodIndex Cer_t1821_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Cer_t1821_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Cer_t1821_0_0_0;
extern const Il2CppType Cer_t1821_1_0_0;
const Il2CppTypeDefinitionMetadata Cer_t1821_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Cer_t1821_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Cer_t1821_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8092/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Cer_t1821_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Cer"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Cer_t1821_0_0_0/* byval_arg */
	, &Cer_t1821_1_0_0/* this_arg */
	, &Cer_t1821_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Cer_t1821)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Cer_t1821)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_Consistency.h"
// Metadata Definition System.Runtime.ConstrainedExecution.Consistency
extern TypeInfo Consistency_t1822_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.Consistency
#include "mscorlib_System_Runtime_ConstrainedExecution_ConsistencyMethodDeclarations.h"
static const EncodedMethodIndex Consistency_t1822_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair Consistency_t1822_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Consistency_t1822_0_0_0;
extern const Il2CppType Consistency_t1822_1_0_0;
const Il2CppTypeDefinitionMetadata Consistency_t1822_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Consistency_t1822_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, Consistency_t1822_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8096/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Consistency_t1822_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Consistency"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Consistency_t1822_0_0_0/* byval_arg */
	, &Consistency_t1822_1_0_0/* this_arg */
	, &Consistency_t1822_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Consistency_t1822)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Consistency_t1822)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinaliz.h"
// Metadata Definition System.Runtime.ConstrainedExecution.CriticalFinalizerObject
extern TypeInfo CriticalFinalizerObject_t1823_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
#include "mscorlib_System_Runtime_ConstrainedExecution_CriticalFinalizMethodDeclarations.h"
static const EncodedMethodIndex CriticalFinalizerObject_t1823_VTable[4] = 
{
	626,
	3744,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CriticalFinalizerObject_t1823_0_0_0;
extern const Il2CppType CriticalFinalizerObject_t1823_1_0_0;
struct CriticalFinalizerObject_t1823;
const Il2CppTypeDefinitionMetadata CriticalFinalizerObject_t1823_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CriticalFinalizerObject_t1823_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11535/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CriticalFinalizerObject_t1823_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CriticalFinalizerObject"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, NULL/* methods */
	, &CriticalFinalizerObject_t1823_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2880/* custom_attributes_cache */
	, &CriticalFinalizerObject_t1823_0_0_0/* byval_arg */
	, &CriticalFinalizerObject_t1823_1_0_0/* this_arg */
	, &CriticalFinalizerObject_t1823_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CriticalFinalizerObject_t1823)/* instance_size */
	, sizeof (CriticalFinalizerObject_t1823)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// Metadata Definition System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
extern TypeInfo ReliabilityContractAttribute_t1824_il2cpp_TypeInfo;
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
static const EncodedMethodIndex ReliabilityContractAttribute_t1824_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ReliabilityContractAttribute_t1824_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ReliabilityContractAttribute_t1824_0_0_0;
extern const Il2CppType ReliabilityContractAttribute_t1824_1_0_0;
struct ReliabilityContractAttribute_t1824;
const Il2CppTypeDefinitionMetadata ReliabilityContractAttribute_t1824_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ReliabilityContractAttribute_t1824_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ReliabilityContractAttribute_t1824_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8101/* fieldStart */
	, 11537/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ReliabilityContractAttribute_t1824_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReliabilityContractAttribute"/* name */
	, "System.Runtime.ConstrainedExecution"/* namespaze */
	, NULL/* methods */
	, &ReliabilityContractAttribute_t1824_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2883/* custom_attributes_cache */
	, &ReliabilityContractAttribute_t1824_0_0_0/* byval_arg */
	, &ReliabilityContractAttribute_t1824_1_0_0/* this_arg */
	, &ReliabilityContractAttribute_t1824_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReliabilityContractAttribute_t1824)/* instance_size */
	, sizeof (ReliabilityContractAttribute_t1824)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.Hosting.ActivationArguments
#include "mscorlib_System_Runtime_Hosting_ActivationArguments.h"
// Metadata Definition System.Runtime.Hosting.ActivationArguments
extern TypeInfo ActivationArguments_t1825_il2cpp_TypeInfo;
// System.Runtime.Hosting.ActivationArguments
#include "mscorlib_System_Runtime_Hosting_ActivationArgumentsMethodDeclarations.h"
static const EncodedMethodIndex ActivationArguments_t1825_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ActivationArguments_t1825_0_0_0;
extern const Il2CppType ActivationArguments_t1825_1_0_0;
struct ActivationArguments_t1825;
const Il2CppTypeDefinitionMetadata ActivationArguments_t1825_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ActivationArguments_t1825_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ActivationArguments_t1825_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ActivationArguments"/* name */
	, "System.Runtime.Hosting"/* namespaze */
	, NULL/* methods */
	, &ActivationArguments_t1825_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2884/* custom_attributes_cache */
	, &ActivationArguments_t1825_0_0_0/* byval_arg */
	, &ActivationArguments_t1825_1_0_0/* this_arg */
	, &ActivationArguments_t1825_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ActivationArguments_t1825)/* instance_size */
	, sizeof (ActivationArguments_t1825)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConvention.h"
// Metadata Definition System.Runtime.InteropServices.CallingConvention
extern TypeInfo CallingConvention_t1826_il2cpp_TypeInfo;
// System.Runtime.InteropServices.CallingConvention
#include "mscorlib_System_Runtime_InteropServices_CallingConventionMethodDeclarations.h"
static const EncodedMethodIndex CallingConvention_t1826_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CallingConvention_t1826_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CallingConvention_t1826_0_0_0;
extern const Il2CppType CallingConvention_t1826_1_0_0;
const Il2CppTypeDefinitionMetadata CallingConvention_t1826_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CallingConvention_t1826_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CallingConvention_t1826_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8103/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CallingConvention_t1826_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CallingConvention"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2885/* custom_attributes_cache */
	, &CallingConvention_t1826_0_0_0/* byval_arg */
	, &CallingConvention_t1826_1_0_0/* this_arg */
	, &CallingConvention_t1826_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CallingConvention_t1826)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CallingConvention_t1826)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSet.h"
// Metadata Definition System.Runtime.InteropServices.CharSet
extern TypeInfo CharSet_t1827_il2cpp_TypeInfo;
// System.Runtime.InteropServices.CharSet
#include "mscorlib_System_Runtime_InteropServices_CharSetMethodDeclarations.h"
static const EncodedMethodIndex CharSet_t1827_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair CharSet_t1827_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CharSet_t1827_0_0_0;
extern const Il2CppType CharSet_t1827_1_0_0;
const Il2CppTypeDefinitionMetadata CharSet_t1827_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CharSet_t1827_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, CharSet_t1827_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8109/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CharSet_t1827_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CharSet"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2886/* custom_attributes_cache */
	, &CharSet_t1827_0_0_0/* byval_arg */
	, &CharSet_t1827_1_0_0/* this_arg */
	, &CharSet_t1827_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CharSet_t1827)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CharSet_t1827)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// Metadata Definition System.Runtime.InteropServices.ClassInterfaceAttribute
extern TypeInfo ClassInterfaceAttribute_t1828_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
static const EncodedMethodIndex ClassInterfaceAttribute_t1828_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ClassInterfaceAttribute_t1828_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClassInterfaceAttribute_t1828_0_0_0;
extern const Il2CppType ClassInterfaceAttribute_t1828_1_0_0;
struct ClassInterfaceAttribute_t1828;
const Il2CppTypeDefinitionMetadata ClassInterfaceAttribute_t1828_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ClassInterfaceAttribute_t1828_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ClassInterfaceAttribute_t1828_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8114/* fieldStart */
	, 11538/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ClassInterfaceAttribute_t1828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClassInterfaceAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &ClassInterfaceAttribute_t1828_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2887/* custom_attributes_cache */
	, &ClassInterfaceAttribute_t1828_0_0_0/* byval_arg */
	, &ClassInterfaceAttribute_t1828_1_0_0/* this_arg */
	, &ClassInterfaceAttribute_t1828_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClassInterfaceAttribute_t1828)/* instance_size */
	, sizeof (ClassInterfaceAttribute_t1828)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceType.h"
// Metadata Definition System.Runtime.InteropServices.ClassInterfaceType
extern TypeInfo ClassInterfaceType_t1829_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ClassInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceTypeMethodDeclarations.h"
static const EncodedMethodIndex ClassInterfaceType_t1829_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ClassInterfaceType_t1829_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ClassInterfaceType_t1829_0_0_0;
extern const Il2CppType ClassInterfaceType_t1829_1_0_0;
const Il2CppTypeDefinitionMetadata ClassInterfaceType_t1829_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ClassInterfaceType_t1829_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ClassInterfaceType_t1829_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8115/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ClassInterfaceType_t1829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ClassInterfaceType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2888/* custom_attributes_cache */
	, &ClassInterfaceType_t1829_0_0_0/* byval_arg */
	, &ClassInterfaceType_t1829_1_0_0/* this_arg */
	, &ClassInterfaceType_t1829_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ClassInterfaceType_t1829)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ClassInterfaceType_t1829)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// Metadata Definition System.Runtime.InteropServices.ComDefaultInterfaceAttribute
extern TypeInfo ComDefaultInterfaceAttribute_t1830_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
static const EncodedMethodIndex ComDefaultInterfaceAttribute_t1830_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair ComDefaultInterfaceAttribute_t1830_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComDefaultInterfaceAttribute_t1830_0_0_0;
extern const Il2CppType ComDefaultInterfaceAttribute_t1830_1_0_0;
struct ComDefaultInterfaceAttribute_t1830;
const Il2CppTypeDefinitionMetadata ComDefaultInterfaceAttribute_t1830_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComDefaultInterfaceAttribute_t1830_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, ComDefaultInterfaceAttribute_t1830_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8119/* fieldStart */
	, 11539/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ComDefaultInterfaceAttribute_t1830_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComDefaultInterfaceAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &ComDefaultInterfaceAttribute_t1830_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2889/* custom_attributes_cache */
	, &ComDefaultInterfaceAttribute_t1830_0_0_0/* byval_arg */
	, &ComDefaultInterfaceAttribute_t1830_1_0_0/* this_arg */
	, &ComDefaultInterfaceAttribute_t1830_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComDefaultInterfaceAttribute_t1830)/* instance_size */
	, sizeof (ComDefaultInterfaceAttribute_t1830)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceType.h"
// Metadata Definition System.Runtime.InteropServices.ComInterfaceType
extern TypeInfo ComInterfaceType_t1831_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComInterfaceType
#include "mscorlib_System_Runtime_InteropServices_ComInterfaceTypeMethodDeclarations.h"
static const EncodedMethodIndex ComInterfaceType_t1831_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair ComInterfaceType_t1831_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ComInterfaceType_t1831_0_0_0;
extern const Il2CppType ComInterfaceType_t1831_1_0_0;
const Il2CppTypeDefinitionMetadata ComInterfaceType_t1831_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ComInterfaceType_t1831_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, ComInterfaceType_t1831_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8120/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo ComInterfaceType_t1831_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComInterfaceType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2890/* custom_attributes_cache */
	, &ComInterfaceType_t1831_0_0_0/* byval_arg */
	, &ComInterfaceType_t1831_1_0_0/* this_arg */
	, &ComInterfaceType_t1831_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComInterfaceType_t1831)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ComInterfaceType_t1831)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// Metadata Definition System.Runtime.InteropServices.DispIdAttribute
extern TypeInfo DispIdAttribute_t1832_il2cpp_TypeInfo;
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
static const EncodedMethodIndex DispIdAttribute_t1832_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair DispIdAttribute_t1832_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType DispIdAttribute_t1832_0_0_0;
extern const Il2CppType DispIdAttribute_t1832_1_0_0;
struct DispIdAttribute_t1832;
const Il2CppTypeDefinitionMetadata DispIdAttribute_t1832_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DispIdAttribute_t1832_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, DispIdAttribute_t1832_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8124/* fieldStart */
	, 11540/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo DispIdAttribute_t1832_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "DispIdAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &DispIdAttribute_t1832_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2891/* custom_attributes_cache */
	, &DispIdAttribute_t1832_0_0_0/* byval_arg */
	, &DispIdAttribute_t1832_1_0_0/* this_arg */
	, &DispIdAttribute_t1832_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DispIdAttribute_t1832)/* instance_size */
	, sizeof (DispIdAttribute_t1832)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// Metadata Definition System.Runtime.InteropServices.GCHandle
extern TypeInfo GCHandle_t1833_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"
static const EncodedMethodIndex GCHandle_t1833_VTable[4] = 
{
	3745,
	601,
	3746,
	654,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandle_t1833_0_0_0;
extern const Il2CppType GCHandle_t1833_1_0_0;
const Il2CppTypeDefinitionMetadata GCHandle_t1833_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, GCHandle_t1833_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8125/* fieldStart */
	, 11541/* methodStart */
	, -1/* eventStart */
	, 2273/* propertyStart */

};
TypeInfo GCHandle_t1833_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &GCHandle_t1833_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2892/* custom_attributes_cache */
	, &GCHandle_t1833_0_0_0/* byval_arg */
	, &GCHandle_t1833_1_0_0/* this_arg */
	, &GCHandle_t1833_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandle_t1833)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandle_t1833)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GCHandle_t1833 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
// Metadata Definition System.Runtime.InteropServices.GCHandleType
extern TypeInfo GCHandleType_t1834_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleTypeMethodDeclarations.h"
static const EncodedMethodIndex GCHandleType_t1834_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair GCHandleType_t1834_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GCHandleType_t1834_0_0_0;
extern const Il2CppType GCHandleType_t1834_1_0_0;
const Il2CppTypeDefinitionMetadata GCHandleType_t1834_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GCHandleType_t1834_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, GCHandleType_t1834_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8126/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo GCHandleType_t1834_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GCHandleType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2893/* custom_attributes_cache */
	, &GCHandleType_t1834_0_0_0/* byval_arg */
	, &GCHandleType_t1834_1_0_0/* this_arg */
	, &GCHandleType_t1834_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GCHandleType_t1834)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GCHandleType_t1834)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// Metadata Definition System.Runtime.InteropServices.InterfaceTypeAttribute
extern TypeInfo InterfaceTypeAttribute_t1835_il2cpp_TypeInfo;
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
static const EncodedMethodIndex InterfaceTypeAttribute_t1835_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair InterfaceTypeAttribute_t1835_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InterfaceTypeAttribute_t1835_0_0_0;
extern const Il2CppType InterfaceTypeAttribute_t1835_1_0_0;
struct InterfaceTypeAttribute_t1835;
const Il2CppTypeDefinitionMetadata InterfaceTypeAttribute_t1835_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InterfaceTypeAttribute_t1835_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, InterfaceTypeAttribute_t1835_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8131/* fieldStart */
	, 11551/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo InterfaceTypeAttribute_t1835_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InterfaceTypeAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &InterfaceTypeAttribute_t1835_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2894/* custom_attributes_cache */
	, &InterfaceTypeAttribute_t1835_0_0_0/* byval_arg */
	, &InterfaceTypeAttribute_t1835_1_0_0/* this_arg */
	, &InterfaceTypeAttribute_t1835_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InterfaceTypeAttribute_t1835)/* instance_size */
	, sizeof (InterfaceTypeAttribute_t1835)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_Marshal.h"
// Metadata Definition System.Runtime.InteropServices.Marshal
extern TypeInfo Marshal_t1836_il2cpp_TypeInfo;
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
static const EncodedMethodIndex Marshal_t1836_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Marshal_t1836_0_0_0;
extern const Il2CppType Marshal_t1836_1_0_0;
struct Marshal_t1836;
const Il2CppTypeDefinitionMetadata Marshal_t1836_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Marshal_t1836_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8132/* fieldStart */
	, 11552/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo Marshal_t1836_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Marshal"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &Marshal_t1836_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2895/* custom_attributes_cache */
	, &Marshal_t1836_0_0_0/* byval_arg */
	, &Marshal_t1836_1_0_0/* this_arg */
	, &Marshal_t1836_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Marshal_t1836)/* instance_size */
	, sizeof (Marshal_t1836)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Marshal_t1836_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 262529/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExce.h"
// Metadata Definition System.Runtime.InteropServices.MarshalDirectiveException
extern TypeInfo MarshalDirectiveException_t1837_il2cpp_TypeInfo;
// System.Runtime.InteropServices.MarshalDirectiveException
#include "mscorlib_System_Runtime_InteropServices_MarshalDirectiveExceMethodDeclarations.h"
static const EncodedMethodIndex MarshalDirectiveException_t1837_VTable[11] = 
{
	626,
	601,
	627,
	1316,
	1317,
	1318,
	1319,
	1320,
	1321,
	1317,
	1322,
};
static Il2CppInterfaceOffsetPair MarshalDirectiveException_t1837_InterfacesOffsets[] = 
{
	{ &ISerializable_t2210_0_0_0, 4},
	{ &_Exception_t3443_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MarshalDirectiveException_t1837_0_0_0;
extern const Il2CppType MarshalDirectiveException_t1837_1_0_0;
struct MarshalDirectiveException_t1837;
const Il2CppTypeDefinitionMetadata MarshalDirectiveException_t1837_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MarshalDirectiveException_t1837_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t1536_0_0_0/* parent */
	, MarshalDirectiveException_t1837_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8134/* fieldStart */
	, 11556/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo MarshalDirectiveException_t1837_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarshalDirectiveException"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &MarshalDirectiveException_t1837_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2896/* custom_attributes_cache */
	, &MarshalDirectiveException_t1837_0_0_0/* byval_arg */
	, &MarshalDirectiveException_t1837_1_0_0/* this_arg */
	, &MarshalDirectiveException_t1837_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarshalDirectiveException_t1837)/* instance_size */
	, sizeof (MarshalDirectiveException_t1837)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttribute.h"
// Metadata Definition System.Runtime.InteropServices.PreserveSigAttribute
extern TypeInfo PreserveSigAttribute_t1838_il2cpp_TypeInfo;
// System.Runtime.InteropServices.PreserveSigAttribute
#include "mscorlib_System_Runtime_InteropServices_PreserveSigAttributeMethodDeclarations.h"
static const EncodedMethodIndex PreserveSigAttribute_t1838_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair PreserveSigAttribute_t1838_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PreserveSigAttribute_t1838_0_0_0;
extern const Il2CppType PreserveSigAttribute_t1838_1_0_0;
struct PreserveSigAttribute_t1838;
const Il2CppTypeDefinitionMetadata PreserveSigAttribute_t1838_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PreserveSigAttribute_t1838_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, PreserveSigAttribute_t1838_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 11558/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PreserveSigAttribute_t1838_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PreserveSigAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &PreserveSigAttribute_t1838_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2897/* custom_attributes_cache */
	, &PreserveSigAttribute_t1838_0_0_0/* byval_arg */
	, &PreserveSigAttribute_t1838_1_0_0/* this_arg */
	, &PreserveSigAttribute_t1838_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PreserveSigAttribute_t1838)/* instance_size */
	, sizeof (PreserveSigAttribute_t1838)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandle.h"
// Metadata Definition System.Runtime.InteropServices.SafeHandle
extern TypeInfo SafeHandle_t1578_il2cpp_TypeInfo;
// System.Runtime.InteropServices.SafeHandle
#include "mscorlib_System_Runtime_InteropServices_SafeHandleMethodDeclarations.h"
static const EncodedMethodIndex SafeHandle_t1578_VTable[8] = 
{
	626,
	2694,
	627,
	628,
	2695,
	2696,
	0,
	0,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static const Il2CppType* SafeHandle_t1578_InterfacesTypeInfos[] = 
{
	&IDisposable_t538_0_0_0,
};
static Il2CppInterfaceOffsetPair SafeHandle_t1578_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SafeHandle_t1578_0_0_0;
extern const Il2CppType SafeHandle_t1578_1_0_0;
struct SafeHandle_t1578;
const Il2CppTypeDefinitionMetadata SafeHandle_t1578_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, SafeHandle_t1578_InterfacesTypeInfos/* implementedInterfaces */
	, SafeHandle_t1578_InterfacesOffsets/* interfaceOffsets */
	, &CriticalFinalizerObject_t1823_0_0_0/* parent */
	, SafeHandle_t1578_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8135/* fieldStart */
	, 11559/* methodStart */
	, -1/* eventStart */
	, 2275/* propertyStart */

};
TypeInfo SafeHandle_t1578_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SafeHandle"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &SafeHandle_t1578_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SafeHandle_t1578_0_0_0/* byval_arg */
	, &SafeHandle_t1578_1_0_0/* this_arg */
	, &SafeHandle_t1578_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SafeHandle_t1578)/* instance_size */
	, sizeof (SafeHandle_t1578)/* actualSize */
	, 0/* element_size */
	, sizeof(void*)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibImportClassAttribute
extern TypeInfo TypeLibImportClassAttribute_t1839_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
static const EncodedMethodIndex TypeLibImportClassAttribute_t1839_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair TypeLibImportClassAttribute_t1839_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibImportClassAttribute_t1839_0_0_0;
extern const Il2CppType TypeLibImportClassAttribute_t1839_1_0_0;
struct TypeLibImportClassAttribute_t1839;
const Il2CppTypeDefinitionMetadata TypeLibImportClassAttribute_t1839_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibImportClassAttribute_t1839_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, TypeLibImportClassAttribute_t1839_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8139/* fieldStart */
	, 11570/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeLibImportClassAttribute_t1839_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibImportClassAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &TypeLibImportClassAttribute_t1839_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2908/* custom_attributes_cache */
	, &TypeLibImportClassAttribute_t1839_0_0_0/* byval_arg */
	, &TypeLibImportClassAttribute_t1839_1_0_0/* this_arg */
	, &TypeLibImportClassAttribute_t1839_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibImportClassAttribute_t1839)/* instance_size */
	, sizeof (TypeLibImportClassAttribute_t1839)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// Metadata Definition System.Runtime.InteropServices.TypeLibVersionAttribute
extern TypeInfo TypeLibVersionAttribute_t1840_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
static const EncodedMethodIndex TypeLibVersionAttribute_t1840_VTable[4] = 
{
	1366,
	601,
	1367,
	628,
};
static Il2CppInterfaceOffsetPair TypeLibVersionAttribute_t1840_InterfacesOffsets[] = 
{
	{ &_Attribute_t3429_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLibVersionAttribute_t1840_0_0_0;
extern const Il2CppType TypeLibVersionAttribute_t1840_1_0_0;
struct TypeLibVersionAttribute_t1840;
const Il2CppTypeDefinitionMetadata TypeLibVersionAttribute_t1840_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLibVersionAttribute_t1840_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t903_0_0_0/* parent */
	, TypeLibVersionAttribute_t1840_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8140/* fieldStart */
	, 11571/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TypeLibVersionAttribute_t1840_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLibVersionAttribute"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &TypeLibVersionAttribute_t1840_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2909/* custom_attributes_cache */
	, &TypeLibVersionAttribute_t1840_0_0_0/* byval_arg */
	, &TypeLibVersionAttribute_t1840_1_0_0/* this_arg */
	, &TypeLibVersionAttribute_t1840_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLibVersionAttribute_t1840)/* instance_size */
	, sizeof (TypeLibVersionAttribute_t1840)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedType.h"
// Metadata Definition System.Runtime.InteropServices.UnmanagedType
extern TypeInfo UnmanagedType_t1841_il2cpp_TypeInfo;
// System.Runtime.InteropServices.UnmanagedType
#include "mscorlib_System_Runtime_InteropServices_UnmanagedTypeMethodDeclarations.h"
static const EncodedMethodIndex UnmanagedType_t1841_VTable[23] = 
{
	604,
	601,
	605,
	606,
	607,
	608,
	609,
	610,
	611,
	612,
	613,
	614,
	615,
	616,
	617,
	618,
	619,
	620,
	621,
	622,
	623,
	624,
	625,
};
static Il2CppInterfaceOffsetPair UnmanagedType_t1841_InterfacesOffsets[] = 
{
	{ &IFormattable_t2190_0_0_0, 4},
	{ &IConvertible_t2193_0_0_0, 5},
	{ &IComparable_t2192_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnmanagedType_t1841_0_0_0;
extern const Il2CppType UnmanagedType_t1841_1_0_0;
const Il2CppTypeDefinitionMetadata UnmanagedType_t1841_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnmanagedType_t1841_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t554_0_0_0/* parent */
	, UnmanagedType_t1841_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 8142/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo UnmanagedType_t1841_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnmanagedType"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &Int32_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2910/* custom_attributes_cache */
	, &UnmanagedType_t1841_0_0_0/* byval_arg */
	, &UnmanagedType_t1841_1_0_0/* this_arg */
	, &UnmanagedType_t1841_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnmanagedType_t1841)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnmanagedType_t1841)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 36/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Activator
extern TypeInfo _Activator_t3486_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Activator_t3486_0_0_0;
extern const Il2CppType _Activator_t3486_1_0_0;
struct _Activator_t3486;
const Il2CppTypeDefinitionMetadata _Activator_t3486_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Activator_t3486_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Activator"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Activator_t3486_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2911/* custom_attributes_cache */
	, &_Activator_t3486_0_0_0/* byval_arg */
	, &_Activator_t3486_1_0_0/* this_arg */
	, &_Activator_t3486_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._Assembly
extern TypeInfo _Assembly_t3476_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _Assembly_t3476_1_0_0;
struct _Assembly_t3476;
const Il2CppTypeDefinitionMetadata _Assembly_t3476_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _Assembly_t3476_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_Assembly"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_Assembly_t3476_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2912/* custom_attributes_cache */
	, &_Assembly_t3476_0_0_0/* byval_arg */
	, &_Assembly_t3476_1_0_0/* this_arg */
	, &_Assembly_t3476_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyBuilder
extern TypeInfo _AssemblyBuilder_t3466_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyBuilder_t3466_0_0_0;
extern const Il2CppType _AssemblyBuilder_t3466_1_0_0;
struct _AssemblyBuilder_t3466;
const Il2CppTypeDefinitionMetadata _AssemblyBuilder_t3466_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _AssemblyBuilder_t3466_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_AssemblyBuilder_t3466_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2913/* custom_attributes_cache */
	, &_AssemblyBuilder_t3466_0_0_0/* byval_arg */
	, &_AssemblyBuilder_t3466_1_0_0/* this_arg */
	, &_AssemblyBuilder_t3466_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._AssemblyName
extern TypeInfo _AssemblyName_t3477_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _AssemblyName_t3477_1_0_0;
struct _AssemblyName_t3477;
const Il2CppTypeDefinitionMetadata _AssemblyName_t3477_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _AssemblyName_t3477_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_AssemblyName"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_AssemblyName_t3477_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2914/* custom_attributes_cache */
	, &_AssemblyName_t3477_0_0_0/* byval_arg */
	, &_AssemblyName_t3477_1_0_0/* this_arg */
	, &_AssemblyName_t3477_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorBuilder
extern TypeInfo _ConstructorBuilder_t3467_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorBuilder_t3467_0_0_0;
extern const Il2CppType _ConstructorBuilder_t3467_1_0_0;
struct _ConstructorBuilder_t3467;
const Il2CppTypeDefinitionMetadata _ConstructorBuilder_t3467_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ConstructorBuilder_t3467_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorBuilder"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ConstructorBuilder_t3467_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2915/* custom_attributes_cache */
	, &_ConstructorBuilder_t3467_0_0_0/* byval_arg */
	, &_ConstructorBuilder_t3467_1_0_0/* this_arg */
	, &_ConstructorBuilder_t3467_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Runtime.InteropServices._ConstructorInfo
extern TypeInfo _ConstructorInfo_t3478_il2cpp_TypeInfo;
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType _ConstructorInfo_t3478_1_0_0;
struct _ConstructorInfo_t3478;
const Il2CppTypeDefinitionMetadata _ConstructorInfo_t3478_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo _ConstructorInfo_t3478_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "_ConstructorInfo"/* name */
	, "System.Runtime.InteropServices"/* namespaze */
	, NULL/* methods */
	, &_ConstructorInfo_t3478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2916/* custom_attributes_cache */
	, &_ConstructorInfo_t3478_0_0_0/* byval_arg */
	, &_ConstructorInfo_t3478_1_0_0/* this_arg */
	, &_ConstructorInfo_t3478_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
