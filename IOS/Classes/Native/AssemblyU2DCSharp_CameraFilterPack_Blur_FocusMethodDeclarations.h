﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Focus
struct CameraFilterPack_Blur_Focus_t52;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Focus::.ctor()
extern "C" void CameraFilterPack_Blur_Focus__ctor_m317 (CameraFilterPack_Blur_Focus_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Focus::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Focus_get_material_m318 (CameraFilterPack_Blur_Focus_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::Start()
extern "C" void CameraFilterPack_Blur_Focus_Start_m319 (CameraFilterPack_Blur_Focus_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Focus_OnRenderImage_m320 (CameraFilterPack_Blur_Focus_t52 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::OnValidate()
extern "C" void CameraFilterPack_Blur_Focus_OnValidate_m321 (CameraFilterPack_Blur_Focus_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::Update()
extern "C" void CameraFilterPack_Blur_Focus_Update_m322 (CameraFilterPack_Blur_Focus_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Focus::OnDisable()
extern "C" void CameraFilterPack_Blur_Focus_OnDisable_m323 (CameraFilterPack_Blur_Focus_t52 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
