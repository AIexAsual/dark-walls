﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// System.Single[]
struct SingleU5BU5D_t72;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Oculus_NightVision2
struct  CameraFilterPack_Oculus_NightVision2_t163  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_Oculus_NightVision2::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Oculus_NightVision2::SCShader
	Shader_t1 * ___SCShader_3;
	// System.Single CameraFilterPack_Oculus_NightVision2::FadeFX
	float ___FadeFX_4;
	// System.Single CameraFilterPack_Oculus_NightVision2::TimeX
	float ___TimeX_5;
	// UnityEngine.Vector4 CameraFilterPack_Oculus_NightVision2::ScreenResolution
	Vector4_t5  ___ScreenResolution_6;
	// UnityEngine.Material CameraFilterPack_Oculus_NightVision2::SCMaterial
	Material_t2 * ___SCMaterial_7;
	// System.Single[] CameraFilterPack_Oculus_NightVision2::Matrix9
	SingleU5BU5D_t72* ___Matrix9_8;
	// System.Single CameraFilterPack_Oculus_NightVision2::Brightness
	float ___Brightness_9;
};
