﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.MatchCollection/Enumerator
struct Enumerator_t1459;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t1370;

// System.Void System.Text.RegularExpressions.MatchCollection/Enumerator::.ctor(System.Text.RegularExpressions.MatchCollection)
extern "C" void Enumerator__ctor_m8029 (Enumerator_t1459 * __this, MatchCollection_t1370 * ___coll, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m8030 (Enumerator_t1459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.MoveNext()
extern "C" bool Enumerator_System_Collections_IEnumerator_MoveNext_m8031 (Enumerator_t1459 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
