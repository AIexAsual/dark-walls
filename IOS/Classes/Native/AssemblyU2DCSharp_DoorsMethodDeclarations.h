﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Doors
struct Doors_t371;

// System.Void Doors::.ctor()
extern "C" void Doors__ctor_m2156 (Doors_t371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Doors::.cctor()
extern "C" void Doors__cctor_m2157 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
