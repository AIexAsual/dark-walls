﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t2230;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Object>
struct  EqualityComparer_1_t2230  : public Object_t
{
};
struct EqualityComparer_1_t2230_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::_default
	EqualityComparer_1_t2230 * ____default_0;
};
