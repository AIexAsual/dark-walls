﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// InteractiveMama
struct InteractiveMama_t330;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// InteractiveMama/<WaitAndMamaPos>c__Iterator8
struct  U3CWaitAndMamaPosU3Ec__Iterator8_t332  : public Object_t
{
	// System.Single InteractiveMama/<WaitAndMamaPos>c__Iterator8::delay
	float ___delay_0;
	// UnityEngine.Vector3 InteractiveMama/<WaitAndMamaPos>c__Iterator8::val
	Vector3_t215  ___val_1;
	// System.Int32 InteractiveMama/<WaitAndMamaPos>c__Iterator8::$PC
	int32_t ___U24PC_2;
	// System.Object InteractiveMama/<WaitAndMamaPos>c__Iterator8::$current
	Object_t * ___U24current_3;
	// System.Single InteractiveMama/<WaitAndMamaPos>c__Iterator8::<$>delay
	float ___U3CU24U3Edelay_4;
	// UnityEngine.Vector3 InteractiveMama/<WaitAndMamaPos>c__Iterator8::<$>val
	Vector3_t215  ___U3CU24U3Eval_5;
	// InteractiveMama InteractiveMama/<WaitAndMamaPos>c__Iterator8::<>f__this
	InteractiveMama_t330 * ___U3CU3Ef__this_6;
};
