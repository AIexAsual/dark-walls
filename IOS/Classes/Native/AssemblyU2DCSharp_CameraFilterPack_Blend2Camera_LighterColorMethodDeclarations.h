﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_LighterColor
struct CameraFilterPack_Blend2Camera_LighterColor_t31;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_LighterColor::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_LighterColor__ctor_m161 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_LighterColor::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_LighterColor_get_material_m162 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::Start()
extern "C" void CameraFilterPack_Blend2Camera_LighterColor_Start_m163 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_LighterColor_OnRenderImage_m164 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_LighterColor_OnValidate_m165 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::Update()
extern "C" void CameraFilterPack_Blend2Camera_LighterColor_Update_m166 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_LighterColor_OnEnable_m167 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_LighterColor::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_LighterColor_OnDisable_m168 (CameraFilterPack_Blend2Camera_LighterColor_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
