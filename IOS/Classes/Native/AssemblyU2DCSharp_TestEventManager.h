﻿#pragma once
#include <stdint.h>
// TestEventManager
struct TestEventManager_t388;
// TestEventManager/OnStateChangeHandler
struct OnStateChangeHandler_t387;
// System.Object
#include "mscorlib_System_Object.h"
// testClass
#include "AssemblyU2DCSharp_testClass.h"
// TestEventManager
struct  TestEventManager_t388  : public Object_t
{
	// testClass TestEventManager::testState
	int32_t ___testState_1;
	// TestEventManager/OnStateChangeHandler TestEventManager::OnStateChangeEvent
	OnStateChangeHandler_t387 * ___OnStateChangeEvent_2;
};
struct TestEventManager_t388_StaticFields{
	// TestEventManager TestEventManager::_instance
	TestEventManager_t388 * ____instance_0;
};
