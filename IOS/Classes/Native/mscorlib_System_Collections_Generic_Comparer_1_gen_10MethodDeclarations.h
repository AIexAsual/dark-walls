﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
struct Comparer_1_t2562;
// System.Object
struct Object_t;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Comparer_1__ctor_m18253_gshared (Comparer_1_t2562 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m18253(__this, method) (( void (*) (Comparer_1_t2562 *, const MethodInfo*))Comparer_1__ctor_m18253_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::.cctor()
extern "C" void Comparer_1__cctor_m18254_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m18254(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m18254_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m18255_gshared (Comparer_1_t2562 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m18255(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2562 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m18255_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>::get_Default()
extern "C" Comparer_1_t2562 * Comparer_1_get_Default_m18256_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m18256(__this /* static, unused */, method) (( Comparer_1_t2562 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m18256_gshared)(__this /* static, unused */, method)
