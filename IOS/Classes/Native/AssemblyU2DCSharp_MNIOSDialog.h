﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// MNPopup
#include "AssemblyU2DCSharp_MNPopup.h"
// MNIOSDialog
struct  MNIOSDialog_t288  : public MNPopup_t278
{
	// System.String MNIOSDialog::yes
	String_t* ___yes_6;
	// System.String MNIOSDialog::no
	String_t* ___no_7;
};
