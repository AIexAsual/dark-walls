﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Hypno
struct CameraFilterPack_FX_Hypno_t134;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Hypno::.ctor()
extern "C" void CameraFilterPack_FX_Hypno__ctor_m867 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Hypno::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Hypno_get_material_m868 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::Start()
extern "C" void CameraFilterPack_FX_Hypno_Start_m869 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Hypno_OnRenderImage_m870 (CameraFilterPack_FX_Hypno_t134 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::OnValidate()
extern "C" void CameraFilterPack_FX_Hypno_OnValidate_m871 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::Update()
extern "C" void CameraFilterPack_FX_Hypno_Update_m872 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Hypno::OnDisable()
extern "C" void CameraFilterPack_FX_Hypno_OnDisable_m873 (CameraFilterPack_FX_Hypno_t134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
