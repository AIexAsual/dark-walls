﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_Invert
struct CameraFilterPack_Color_Invert_t64;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_Invert::.ctor()
extern "C" void CameraFilterPack_Color_Invert__ctor_m400 (CameraFilterPack_Color_Invert_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_Invert::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_Invert_get_material_m401 (CameraFilterPack_Color_Invert_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Invert::Start()
extern "C" void CameraFilterPack_Color_Invert_Start_m402 (CameraFilterPack_Color_Invert_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Invert::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_Invert_OnRenderImage_m403 (CameraFilterPack_Color_Invert_t64 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Invert::Update()
extern "C" void CameraFilterPack_Color_Invert_Update_m404 (CameraFilterPack_Color_Invert_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_Invert::OnDisable()
extern "C" void CameraFilterPack_Color_Invert_OnDisable_m405 (CameraFilterPack_Color_Invert_t64 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
