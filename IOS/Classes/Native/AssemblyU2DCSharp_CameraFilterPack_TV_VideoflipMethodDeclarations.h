﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_TV_Videoflip
struct CameraFilterPack_TV_Videoflip_t198;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_TV_Videoflip::.ctor()
extern "C" void CameraFilterPack_TV_Videoflip__ctor_m1283 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_TV_Videoflip::get_material()
extern "C" Material_t2 * CameraFilterPack_TV_Videoflip_get_material_m1284 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::Start()
extern "C" void CameraFilterPack_TV_Videoflip_Start_m1285 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_TV_Videoflip_OnRenderImage_m1286 (CameraFilterPack_TV_Videoflip_t198 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::Update()
extern "C" void CameraFilterPack_TV_Videoflip_Update_m1287 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_TV_Videoflip::OnDisable()
extern "C" void CameraFilterPack_TV_Videoflip_OnDisable_m1288 (CameraFilterPack_TV_Videoflip_t198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
