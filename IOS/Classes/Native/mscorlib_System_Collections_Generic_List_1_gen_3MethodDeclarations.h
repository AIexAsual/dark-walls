﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Single>
struct List_1_t576;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3024;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t3025;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<System.Single>
struct ICollection_1_t3026;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Single>
struct ReadOnlyCollection_1_t2401;
// System.Single[]
struct SingleU5BU5D_t72;
// System.Predicate`1<System.Single>
struct Predicate_1_t2406;
// System.Comparison`1<System.Single>
struct Comparison_1_t2410;
// System.Collections.Generic.List`1/Enumerator<System.Single>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
extern "C" void List_1__ctor_m3390_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1__ctor_m3390(__this, method) (( void (*) (List_1_t576 *, const MethodInfo*))List_1__ctor_m3390_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15638_gshared (List_1_t576 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m15638(__this, ___collection, method) (( void (*) (List_1_t576 *, Object_t*, const MethodInfo*))List_1__ctor_m15638_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15639_gshared (List_1_t576 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m15639(__this, ___capacity, method) (( void (*) (List_1_t576 *, int32_t, const MethodInfo*))List_1__ctor_m15639_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Single>::.cctor()
extern "C" void List_1__cctor_m15640_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m15640(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15640_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Single>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15641_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15641(__this, method) (( Object_t* (*) (List_1_t576 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15641_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m15642_gshared (List_1_t576 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m15642(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t576 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m15642_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Single>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m15643_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15643(__this, method) (( Object_t * (*) (List_1_t576 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m15643_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m15644_gshared (List_1_t576 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m15644(__this, ___item, method) (( int32_t (*) (List_1_t576 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m15644_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m15645_gshared (List_1_t576 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m15645(__this, ___item, method) (( bool (*) (List_1_t576 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m15645_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m15646_gshared (List_1_t576 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m15646(__this, ___item, method) (( int32_t (*) (List_1_t576 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m15646_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m15647_gshared (List_1_t576 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m15647(__this, ___index, ___item, method) (( void (*) (List_1_t576 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m15647_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m15648_gshared (List_1_t576 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m15648(__this, ___item, method) (( void (*) (List_1_t576 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m15648_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15649_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15649(__this, method) (( bool (*) (List_1_t576 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15649_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m15650_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15650(__this, method) (( bool (*) (List_1_t576 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m15650_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Single>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m15651_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m15651(__this, method) (( Object_t * (*) (List_1_t576 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m15651_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m15652_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m15652(__this, method) (( bool (*) (List_1_t576 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m15652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m15653_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m15653(__this, method) (( bool (*) (List_1_t576 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m15653_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Single>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m15654_gshared (List_1_t576 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m15654(__this, ___index, method) (( Object_t * (*) (List_1_t576 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m15654_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Single>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m15655_gshared (List_1_t576 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m15655(__this, ___index, ___value, method) (( void (*) (List_1_t576 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m15655_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Add(T)
extern "C" void List_1_Add_m15656_gshared (List_1_t576 * __this, float ___item, const MethodInfo* method);
#define List_1_Add_m15656(__this, ___item, method) (( void (*) (List_1_t576 *, float, const MethodInfo*))List_1_Add_m15656_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15657_gshared (List_1_t576 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m15657(__this, ___newCount, method) (( void (*) (List_1_t576 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15657_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Single>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15658_gshared (List_1_t576 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m15658(__this, ___collection, method) (( void (*) (List_1_t576 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15658_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Single>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15659_gshared (List_1_t576 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m15659(__this, ___enumerable, method) (( void (*) (List_1_t576 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15659_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Single>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15660_gshared (List_1_t576 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m15660(__this, ___collection, method) (( void (*) (List_1_t576 *, Object_t*, const MethodInfo*))List_1_AddRange_m15660_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Single>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2401 * List_1_AsReadOnly_m15661_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m15661(__this, method) (( ReadOnlyCollection_1_t2401 * (*) (List_1_t576 *, const MethodInfo*))List_1_AsReadOnly_m15661_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Clear()
extern "C" void List_1_Clear_m15662_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_Clear_m15662(__this, method) (( void (*) (List_1_t576 *, const MethodInfo*))List_1_Clear_m15662_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::Contains(T)
extern "C" bool List_1_Contains_m15663_gshared (List_1_t576 * __this, float ___item, const MethodInfo* method);
#define List_1_Contains_m15663(__this, ___item, method) (( bool (*) (List_1_t576 *, float, const MethodInfo*))List_1_Contains_m15663_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m15664_gshared (List_1_t576 * __this, SingleU5BU5D_t72* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m15664(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t576 *, SingleU5BU5D_t72*, int32_t, const MethodInfo*))List_1_CopyTo_m15664_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Single>::Find(System.Predicate`1<T>)
extern "C" float List_1_Find_m15665_gshared (List_1_t576 * __this, Predicate_1_t2406 * ___match, const MethodInfo* method);
#define List_1_Find_m15665(__this, ___match, method) (( float (*) (List_1_t576 *, Predicate_1_t2406 *, const MethodInfo*))List_1_Find_m15665_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15666_gshared (Object_t * __this /* static, unused */, Predicate_1_t2406 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m15666(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2406 *, const MethodInfo*))List_1_CheckMatch_m15666_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15667_gshared (List_1_t576 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2406 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m15667(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t576 *, int32_t, int32_t, Predicate_1_t2406 *, const MethodInfo*))List_1_GetIndex_m15667_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Single>::GetEnumerator()
extern "C" Enumerator_t2400  List_1_GetEnumerator_m15668_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m15668(__this, method) (( Enumerator_t2400  (*) (List_1_t576 *, const MethodInfo*))List_1_GetEnumerator_m15668_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m15669_gshared (List_1_t576 * __this, float ___item, const MethodInfo* method);
#define List_1_IndexOf_m15669(__this, ___item, method) (( int32_t (*) (List_1_t576 *, float, const MethodInfo*))List_1_IndexOf_m15669_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15670_gshared (List_1_t576 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m15670(__this, ___start, ___delta, method) (( void (*) (List_1_t576 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15670_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15671_gshared (List_1_t576 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m15671(__this, ___index, method) (( void (*) (List_1_t576 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15671_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m15672_gshared (List_1_t576 * __this, int32_t ___index, float ___item, const MethodInfo* method);
#define List_1_Insert_m15672(__this, ___index, ___item, method) (( void (*) (List_1_t576 *, int32_t, float, const MethodInfo*))List_1_Insert_m15672_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Single>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15673_gshared (List_1_t576 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m15673(__this, ___collection, method) (( void (*) (List_1_t576 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15673_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Single>::Remove(T)
extern "C" bool List_1_Remove_m15674_gshared (List_1_t576 * __this, float ___item, const MethodInfo* method);
#define List_1_Remove_m15674(__this, ___item, method) (( bool (*) (List_1_t576 *, float, const MethodInfo*))List_1_Remove_m15674_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15675_gshared (List_1_t576 * __this, Predicate_1_t2406 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m15675(__this, ___match, method) (( int32_t (*) (List_1_t576 *, Predicate_1_t2406 *, const MethodInfo*))List_1_RemoveAll_m15675_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Single>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m15676_gshared (List_1_t576 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m15676(__this, ___index, method) (( void (*) (List_1_t576 *, int32_t, const MethodInfo*))List_1_RemoveAt_m15676_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Reverse()
extern "C" void List_1_Reverse_m15677_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_Reverse_m15677(__this, method) (( void (*) (List_1_t576 *, const MethodInfo*))List_1_Reverse_m15677_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Sort()
extern "C" void List_1_Sort_m15678_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_Sort_m15678(__this, method) (( void (*) (List_1_t576 *, const MethodInfo*))List_1_Sort_m15678_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m15679_gshared (List_1_t576 * __this, Comparison_1_t2410 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m15679(__this, ___comparison, method) (( void (*) (List_1_t576 *, Comparison_1_t2410 *, const MethodInfo*))List_1_Sort_m15679_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Single>::ToArray()
extern "C" SingleU5BU5D_t72* List_1_ToArray_m3403_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_ToArray_m3403(__this, method) (( SingleU5BU5D_t72* (*) (List_1_t576 *, const MethodInfo*))List_1_ToArray_m3403_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::TrimExcess()
extern "C" void List_1_TrimExcess_m15680_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m15680(__this, method) (( void (*) (List_1_t576 *, const MethodInfo*))List_1_TrimExcess_m15680_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15681_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m15681(__this, method) (( int32_t (*) (List_1_t576 *, const MethodInfo*))List_1_get_Capacity_m15681_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Single>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15682_gshared (List_1_t576 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m15682(__this, ___value, method) (( void (*) (List_1_t576 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15682_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Single>::get_Count()
extern "C" int32_t List_1_get_Count_m15683_gshared (List_1_t576 * __this, const MethodInfo* method);
#define List_1_get_Count_m15683(__this, method) (( int32_t (*) (List_1_t576 *, const MethodInfo*))List_1_get_Count_m15683_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32)
extern "C" float List_1_get_Item_m15684_gshared (List_1_t576 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m15684(__this, ___index, method) (( float (*) (List_1_t576 *, int32_t, const MethodInfo*))List_1_get_Item_m15684_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Single>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m15685_gshared (List_1_t576 * __this, int32_t ___index, float ___value, const MethodInfo* method);
#define List_1_set_Item_m15685(__this, ___index, ___value, method) (( void (*) (List_1_t576 *, int32_t, float, const MethodInfo*))List_1_set_Item_m15685_gshared)(__this, ___index, ___value, method)
