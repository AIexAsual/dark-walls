﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CardboardOnGUIMouse
struct CardboardOnGUIMouse_t222;

// System.Void CardboardOnGUIMouse::.ctor()
extern "C" void CardboardOnGUIMouse__ctor_m1405 (CardboardOnGUIMouse_t222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIMouse::LateUpdate()
extern "C" void CardboardOnGUIMouse_LateUpdate_m1406 (CardboardOnGUIMouse_t222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUIMouse::DrawPointerImage()
extern "C" void CardboardOnGUIMouse_DrawPointerImage_m1407 (CardboardOnGUIMouse_t222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
