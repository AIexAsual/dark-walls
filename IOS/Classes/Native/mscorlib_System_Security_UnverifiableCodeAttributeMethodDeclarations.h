﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.UnverifiableCodeAttribute
struct UnverifiableCodeAttribute_t2027;

// System.Void System.Security.UnverifiableCodeAttribute::.ctor()
extern "C" void UnverifiableCodeAttribute__ctor_m12255 (UnverifiableCodeAttribute_t2027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
