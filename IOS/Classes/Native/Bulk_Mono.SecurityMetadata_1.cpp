﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
extern TypeInfo TlsServerCertificate_t1315_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4MethodDeclarations.h"
static const EncodedMethodIndex TlsServerCertificate_t1315_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1912,
	1913,
	1914,
	1897,
};
extern const Il2CppType IDisposable_t538_0_0_0;
static Il2CppInterfaceOffsetPair TlsServerCertificate_t1315_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificate_t1315_0_0_0;
extern const Il2CppType TlsServerCertificate_t1315_1_0_0;
extern const Il2CppType HandshakeMessage_t1286_0_0_0;
struct TlsServerCertificate_t1315;
const Il2CppTypeDefinitionMetadata TlsServerCertificate_t1315_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificate_t1315_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsServerCertificate_t1315_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5546/* fieldStart */
	, 7051/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsServerCertificate_t1315_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificate"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsServerCertificate_t1315_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificate_t1315_0_0_0/* byval_arg */
	, &TlsServerCertificate_t1315_1_0_0/* this_arg */
	, &TlsServerCertificate_t1315_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificate_t1315)/* instance_size */
	, sizeof (TlsServerCertificate_t1315)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
extern TypeInfo TlsServerCertificateRequest_t1316_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5MethodDeclarations.h"
static const EncodedMethodIndex TlsServerCertificateRequest_t1316_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1915,
	1916,
	1917,
	1897,
};
static Il2CppInterfaceOffsetPair TlsServerCertificateRequest_t1316_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificateRequest_t1316_0_0_0;
extern const Il2CppType TlsServerCertificateRequest_t1316_1_0_0;
struct TlsServerCertificateRequest_t1316;
const Il2CppTypeDefinitionMetadata TlsServerCertificateRequest_t1316_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificateRequest_t1316_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsServerCertificateRequest_t1316_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5547/* fieldStart */
	, 7060/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsServerCertificateRequest_t1316_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificateRequest"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsServerCertificateRequest_t1316_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificateRequest_t1316_0_0_0/* byval_arg */
	, &TlsServerCertificateRequest_t1316_1_0_0/* this_arg */
	, &TlsServerCertificateRequest_t1316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificateRequest_t1316)/* instance_size */
	, sizeof (TlsServerCertificateRequest_t1316)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
extern TypeInfo TlsServerFinished_t1317_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6MethodDeclarations.h"
static const EncodedMethodIndex TlsServerFinished_t1317_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1918,
	1919,
	1920,
	1897,
};
static Il2CppInterfaceOffsetPair TlsServerFinished_t1317_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerFinished_t1317_0_0_0;
extern const Il2CppType TlsServerFinished_t1317_1_0_0;
struct TlsServerFinished_t1317;
const Il2CppTypeDefinitionMetadata TlsServerFinished_t1317_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerFinished_t1317_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsServerFinished_t1317_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5549/* fieldStart */
	, 7064/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsServerFinished_t1317_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerFinished"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsServerFinished_t1317_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerFinished_t1317_0_0_0/* byval_arg */
	, &TlsServerFinished_t1317_1_0_0/* this_arg */
	, &TlsServerFinished_t1317_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerFinished_t1317)/* instance_size */
	, sizeof (TlsServerFinished_t1317)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TlsServerFinished_t1317_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
extern TypeInfo TlsServerHello_t1318_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7MethodDeclarations.h"
static const EncodedMethodIndex TlsServerHello_t1318_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1921,
	1922,
	1923,
	1897,
};
static Il2CppInterfaceOffsetPair TlsServerHello_t1318_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHello_t1318_0_0_0;
extern const Il2CppType TlsServerHello_t1318_1_0_0;
struct TlsServerHello_t1318;
const Il2CppTypeDefinitionMetadata TlsServerHello_t1318_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHello_t1318_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsServerHello_t1318_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5550/* fieldStart */
	, 7069/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsServerHello_t1318_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHello"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsServerHello_t1318_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHello_t1318_0_0_0/* byval_arg */
	, &TlsServerHello_t1318_1_0_0/* this_arg */
	, &TlsServerHello_t1318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHello_t1318)/* instance_size */
	, sizeof (TlsServerHello_t1318)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
extern TypeInfo TlsServerHelloDone_t1319_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8MethodDeclarations.h"
static const EncodedMethodIndex TlsServerHelloDone_t1319_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1924,
	1925,
	1896,
	1897,
};
static Il2CppInterfaceOffsetPair TlsServerHelloDone_t1319_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHelloDone_t1319_0_0_0;
extern const Il2CppType TlsServerHelloDone_t1319_1_0_0;
struct TlsServerHelloDone_t1319;
const Il2CppTypeDefinitionMetadata TlsServerHelloDone_t1319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHelloDone_t1319_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsServerHelloDone_t1319_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7074/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsServerHelloDone_t1319_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHelloDone"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsServerHelloDone_t1319_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHelloDone_t1319_0_0_0/* byval_arg */
	, &TlsServerHelloDone_t1319_1_0_0/* this_arg */
	, &TlsServerHelloDone_t1319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHelloDone_t1319)/* instance_size */
	, sizeof (TlsServerHelloDone_t1319)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
extern TypeInfo TlsServerKeyExchange_t1320_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9MethodDeclarations.h"
static const EncodedMethodIndex TlsServerKeyExchange_t1320_VTable[28] = 
{
	626,
	601,
	627,
	628,
	1820,
	1879,
	1880,
	1881,
	1882,
	1883,
	1884,
	1885,
	1886,
	1887,
	1888,
	1831,
	1889,
	1890,
	1891,
	1835,
	1892,
	1893,
	1894,
	1895,
	1926,
	1927,
	1928,
	1897,
};
static Il2CppInterfaceOffsetPair TlsServerKeyExchange_t1320_InterfacesOffsets[] = 
{
	{ &IDisposable_t538_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerKeyExchange_t1320_0_0_0;
extern const Il2CppType TlsServerKeyExchange_t1320_1_0_0;
struct TlsServerKeyExchange_t1320;
const Il2CppTypeDefinitionMetadata TlsServerKeyExchange_t1320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerKeyExchange_t1320_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1286_0_0_0/* parent */
	, TlsServerKeyExchange_t1320_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5554/* fieldStart */
	, 7077/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo TlsServerKeyExchange_t1320_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerKeyExchange"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, NULL/* methods */
	, &TlsServerKeyExchange_t1320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerKeyExchange_t1320_0_0_0/* byval_arg */
	, &TlsServerKeyExchange_t1320_1_0_0/* this_arg */
	, &TlsServerKeyExchange_t1320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerKeyExchange_t1320)/* instance_size */
	, sizeof (TlsServerKeyExchange_t1320)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t1321_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
static const EncodedMethodIndex PrimalityTest_t1321_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1929,
	1930,
	1931,
};
extern const Il2CppType ICloneable_t3433_0_0_0;
extern const Il2CppType ISerializable_t2210_0_0_0;
static Il2CppInterfaceOffsetPair PrimalityTest_t1321_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrimalityTest_t1321_0_0_0;
extern const Il2CppType PrimalityTest_t1321_1_0_0;
extern const Il2CppType MulticastDelegate_t219_0_0_0;
struct PrimalityTest_t1321;
const Il2CppTypeDefinitionMetadata PrimalityTest_t1321_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t1321_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, PrimalityTest_t1321_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7082/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrimalityTest_t1321_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, NULL/* methods */
	, &PrimalityTest_t1321_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t1321_0_0_0/* byval_arg */
	, &PrimalityTest_t1321_1_0_0/* this_arg */
	, &PrimalityTest_t1321_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t1321/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t1321)/* instance_size */
	, sizeof (PrimalityTest_t1321)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback
extern TypeInfo CertificateValidationCallback_t1296_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidatiMethodDeclarations.h"
static const EncodedMethodIndex CertificateValidationCallback_t1296_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1932,
	1933,
	1934,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback_t1296_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback_t1296_0_0_0;
extern const Il2CppType CertificateValidationCallback_t1296_1_0_0;
struct CertificateValidationCallback_t1296;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback_t1296_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback_t1296_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, CertificateValidationCallback_t1296_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7086/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CertificateValidationCallback_t1296_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &CertificateValidationCallback_t1296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback_t1296_0_0_0/* byval_arg */
	, &CertificateValidationCallback_t1296_1_0_0/* this_arg */
	, &CertificateValidationCallback_t1296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback_t1296/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback_t1296)/* instance_size */
	, sizeof (CertificateValidationCallback_t1296)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback2
extern TypeInfo CertificateValidationCallback2_t1297_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0MethodDeclarations.h"
static const EncodedMethodIndex CertificateValidationCallback2_t1297_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1935,
	1936,
	1937,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback2_t1297_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback2_t1297_0_0_0;
extern const Il2CppType CertificateValidationCallback2_t1297_1_0_0;
struct CertificateValidationCallback2_t1297;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback2_t1297_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback2_t1297_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, CertificateValidationCallback2_t1297_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7090/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CertificateValidationCallback2_t1297_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback2"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &CertificateValidationCallback2_t1297_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback2_t1297_0_0_0/* byval_arg */
	, &CertificateValidationCallback2_t1297_1_0_0/* this_arg */
	, &CertificateValidationCallback2_t1297_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback2_t1297/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback2_t1297)/* instance_size */
	, sizeof (CertificateValidationCallback2_t1297)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectio.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateSelectionCallback
extern TypeInfo CertificateSelectionCallback_t1280_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectioMethodDeclarations.h"
static const EncodedMethodIndex CertificateSelectionCallback_t1280_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1938,
	1939,
	1940,
};
static Il2CppInterfaceOffsetPair CertificateSelectionCallback_t1280_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateSelectionCallback_t1280_0_0_0;
extern const Il2CppType CertificateSelectionCallback_t1280_1_0_0;
struct CertificateSelectionCallback_t1280;
const Il2CppTypeDefinitionMetadata CertificateSelectionCallback_t1280_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateSelectionCallback_t1280_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, CertificateSelectionCallback_t1280_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7094/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo CertificateSelectionCallback_t1280_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateSelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &CertificateSelectionCallback_t1280_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateSelectionCallback_t1280_0_0_0/* byval_arg */
	, &CertificateSelectionCallback_t1280_1_0_0/* this_arg */
	, &CertificateSelectionCallback_t1280_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateSelectionCallback_t1280/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateSelectionCallback_t1280)/* instance_size */
	, sizeof (CertificateSelectionCallback_t1280)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelection.h"
// Metadata Definition Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
extern TypeInfo PrivateKeySelectionCallback_t1281_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelectionMethodDeclarations.h"
static const EncodedMethodIndex PrivateKeySelectionCallback_t1281_VTable[13] = 
{
	634,
	601,
	635,
	628,
	636,
	637,
	636,
	638,
	639,
	640,
	1941,
	1942,
	1943,
};
static Il2CppInterfaceOffsetPair PrivateKeySelectionCallback_t1281_InterfacesOffsets[] = 
{
	{ &ICloneable_t3433_0_0_0, 4},
	{ &ISerializable_t2210_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrivateKeySelectionCallback_t1281_0_0_0;
extern const Il2CppType PrivateKeySelectionCallback_t1281_1_0_0;
struct PrivateKeySelectionCallback_t1281;
const Il2CppTypeDefinitionMetadata PrivateKeySelectionCallback_t1281_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrivateKeySelectionCallback_t1281_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t219_0_0_0/* parent */
	, PrivateKeySelectionCallback_t1281_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, 7098/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo PrivateKeySelectionCallback_t1281_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeySelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, NULL/* methods */
	, &PrivateKeySelectionCallback_t1281_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeySelectionCallback_t1281_0_0_0/* byval_arg */
	, &PrivateKeySelectionCallback_t1281_1_0_0/* this_arg */
	, &PrivateKeySelectionCallback_t1281_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1281/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeySelectionCallback_t1281)/* instance_size */
	, sizeof (PrivateKeySelectionCallback_t1281)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1332_il2cpp_TypeInfo;
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
extern const Il2CppType U24ArrayTypeU243132_t1323_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t1324_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t1325_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t1326_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t1327_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t1328_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1329_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t1330_0_0_0;
extern const Il2CppType U24ArrayTypeU244_t1331_0_0_0;
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1332_il2cpp_TypeInfo__nestedTypes[9] =
{
	&U24ArrayTypeU243132_t1323_0_0_0,
	&U24ArrayTypeU24256_t1324_0_0_0,
	&U24ArrayTypeU2420_t1325_0_0_0,
	&U24ArrayTypeU2432_t1326_0_0_0,
	&U24ArrayTypeU2448_t1327_0_0_0,
	&U24ArrayTypeU2464_t1328_0_0_0,
	&U24ArrayTypeU2412_t1329_0_0_0,
	&U24ArrayTypeU2416_t1330_0_0_0,
	&U24ArrayTypeU244_t1331_0_0_0,
};
static const EncodedMethodIndex U3CPrivateImplementationDetailsU3E_t1332_VTable[4] = 
{
	626,
	601,
	627,
	628,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1332_0_0_0;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1332_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct U3CPrivateImplementationDetailsU3E_t1332;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1332_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1332_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1332_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, 5556/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1332_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U3CPrivateImplementationDetailsU3E_t1332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2346/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1332_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1332)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1332_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 9/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t1323_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTypMethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU243132_t1323_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t1323_1_0_0;
extern const Il2CppType ValueType_t1540_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t1323_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU243132_t1323_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU243132_t1323_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU243132_t1323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t1323_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t1323_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t1323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t1323_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1323_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1323_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t1323)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t1323)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t1323_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t1324_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU24256_t1324_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t1324_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t1324_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU24256_t1324_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU24256_t1324_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU24256_t1324_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t1324_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t1324_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t1324_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t1324_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1324_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1324_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t1324)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t1324)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t1324_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t1325_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2420_t1325_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t1325_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t1325_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2420_t1325_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2420_t1325_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2420_t1325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t1325_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t1325_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t1325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t1325_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1325_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1325_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t1325)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t1325)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t1325_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t1326_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2432_t1326_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t1326_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t1326_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2432_t1326_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2432_t1326_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2432_t1326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t1326_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t1326_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t1326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t1326_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1326_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1326_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t1326)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t1326)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t1326_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t1327_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2448_t1327_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t1327_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t1327_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2448_t1327_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2448_t1327_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2448_t1327_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t1327_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t1327_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t1327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t1327_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1327_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1327_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t1327)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t1327)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t1327_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t1328_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2464_t1328_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t1328_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t1328_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2464_t1328_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2464_t1328_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2464_t1328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t1328_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t1328_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t1328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t1328_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1328_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1328_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t1328)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t1328)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t1328_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1329_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2412_t1329_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1329_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1329_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2412_t1329_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2412_t1329_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2412_t1329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1329_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1329_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1329_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1329_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1329_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1329)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1329)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1329_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t1330_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU2416_t1330_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t1330_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t1330_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU2416_t1330_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU2416_t1330_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU2416_t1330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t1330_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t1330_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t1330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t1330_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1330_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1330_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t1330)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t1330)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t1330_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$4
extern TypeInfo U24ArrayTypeU244_t1331_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7MethodDeclarations.h"
static const EncodedMethodIndex U24ArrayTypeU244_t1331_VTable[4] = 
{
	652,
	601,
	653,
	654,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU244_t1331_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU244_t1331_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1332_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t1540_0_0_0/* parent */
	, U24ArrayTypeU244_t1331_VTable/* vtableMethods */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */
	, -1/* methodStart */
	, -1/* eventStart */
	, -1/* propertyStart */

};
TypeInfo U24ArrayTypeU244_t1331_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$4"/* name */
	, ""/* namespaze */
	, NULL/* methods */
	, &U24ArrayTypeU244_t1331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU244_t1331_0_0_0/* byval_arg */
	, &U24ArrayTypeU244_t1331_1_0_0/* this_arg */
	, &U24ArrayTypeU244_t1331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, -1/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU244_t1331_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1331_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1331_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU244_t1331)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU244_t1331)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU244_t1331_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
