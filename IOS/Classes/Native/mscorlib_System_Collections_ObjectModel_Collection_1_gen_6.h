﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.Color>
struct IList_1_t567;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.Collection`1<UnityEngine.Color>
struct  Collection_1_t2433  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::list
	Object_t* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::syncRoot
	Object_t * ___syncRoot_1;
};
