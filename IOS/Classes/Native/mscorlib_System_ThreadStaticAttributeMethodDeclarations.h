﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ThreadStaticAttribute
struct ThreadStaticAttribute_t2137;

// System.Void System.ThreadStaticAttribute::.ctor()
extern "C" void ThreadStaticAttribute__ctor_m13307 (ThreadStaticAttribute_t2137 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
