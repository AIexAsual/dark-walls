﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// Doors
struct  Doors_t371  : public Object_t
{
};
struct Doors_t371_StaticFields{
	// System.String Doors::ER_DOOR1
	String_t* ___ER_DOOR1_0;
	// System.String Doors::CR_DOOR1
	String_t* ___CR_DOOR1_1;
	// System.String Doors::CUBICLE
	String_t* ___CUBICLE_2;
	// System.String Doors::HALLWAY
	String_t* ___HALLWAY_3;
	// System.String Doors::LOBBY_DOOR1
	String_t* ___LOBBY_DOOR1_4;
	// System.String Doors::LOBBY_DOOR2
	String_t* ___LOBBY_DOOR2_5;
	// System.String Doors::MORGUE_DOOR
	String_t* ___MORGUE_DOOR_6;
	// System.String Doors::MORGUE_DD1
	String_t* ___MORGUE_DD1_7;
	// System.String Doors::MORGUE_DD2
	String_t* ___MORGUE_DD2_8;
	// System.String Doors::WARD_DOOR1
	String_t* ___WARD_DOOR1_9;
	// System.String Doors::WARD_DOOR2
	String_t* ___WARD_DOOR2_10;
	// System.String Doors::ER_DOOR2
	String_t* ___ER_DOOR2_11;
	// System.String Doors::ER_DOOR3
	String_t* ___ER_DOOR3_12;
};
