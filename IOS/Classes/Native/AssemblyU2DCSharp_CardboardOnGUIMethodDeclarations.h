﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CardboardOnGUI
struct CardboardOnGUI_t220;
// CardboardOnGUI/OnGUICallback
struct OnGUICallback_t218;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4;

// System.Void CardboardOnGUI::.ctor()
extern "C" void CardboardOnGUI__ctor_m1393 (CardboardOnGUI_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::add_onGUICallback(CardboardOnGUI/OnGUICallback)
extern "C" void CardboardOnGUI_add_onGUICallback_m1394 (Object_t * __this /* static, unused */, OnGUICallback_t218 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::remove_onGUICallback(CardboardOnGUI/OnGUICallback)
extern "C" void CardboardOnGUI_remove_onGUICallback_m1395 (Object_t * __this /* static, unused */, OnGUICallback_t218 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardboardOnGUI::OKToDraw(UnityEngine.MonoBehaviour)
extern "C" bool CardboardOnGUI_OKToDraw_m1396 (Object_t * __this /* static, unused */, MonoBehaviour_t4 * ___mb, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardboardOnGUI::get_IsGUIVisible()
extern "C" bool CardboardOnGUI_get_IsGUIVisible_m1397 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::set_IsGUIVisible(System.Boolean)
extern "C" void CardboardOnGUI_set_IsGUIVisible_m1398 (Object_t * __this /* static, unused */, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardboardOnGUI::get_Triggered()
extern "C" bool CardboardOnGUI_get_Triggered_m1399 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::Awake()
extern "C" void CardboardOnGUI_Awake_m1400 (CardboardOnGUI_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::Start()
extern "C" void CardboardOnGUI_Start_m1401 (CardboardOnGUI_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::Create()
extern "C" void CardboardOnGUI_Create_m1402 (CardboardOnGUI_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::LateUpdate()
extern "C" void CardboardOnGUI_LateUpdate_m1403 (CardboardOnGUI_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardboardOnGUI::OnGUI()
extern "C" void CardboardOnGUI_OnGUI_m1404 (CardboardOnGUI_t220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
