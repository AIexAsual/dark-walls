﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_FX_Drunk2
struct CameraFilterPack_FX_Drunk2_t125;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_FX_Drunk2::.ctor()
extern "C" void CameraFilterPack_FX_Drunk2__ctor_m806 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_FX_Drunk2::get_material()
extern "C" Material_t2 * CameraFilterPack_FX_Drunk2_get_material_m807 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::Start()
extern "C" void CameraFilterPack_FX_Drunk2_Start_m808 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_FX_Drunk2_OnRenderImage_m809 (CameraFilterPack_FX_Drunk2_t125 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::OnValidate()
extern "C" void CameraFilterPack_FX_Drunk2_OnValidate_m810 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::Update()
extern "C" void CameraFilterPack_FX_Drunk2_Update_m811 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_FX_Drunk2::OnDisable()
extern "C" void CameraFilterPack_FX_Drunk2_OnDisable_m812 (CameraFilterPack_FX_Drunk2_t125 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
