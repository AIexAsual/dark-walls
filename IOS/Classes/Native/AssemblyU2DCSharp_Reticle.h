﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t256;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Reticle
struct  Reticle_t406  : public MonoBehaviour_t4
{
	// UnityEngine.GameObject Reticle::myCam
	GameObject_t256 * ___myCam_2;
	// UnityEngine.Camera Reticle::CameraFacing
	Camera_t14 * ___CameraFacing_3;
	// System.Boolean Reticle::gotCam
	bool ___gotCam_4;
	// UnityEngine.Vector3 Reticle::originalScale
	Vector3_t215  ___originalScale_5;
};
