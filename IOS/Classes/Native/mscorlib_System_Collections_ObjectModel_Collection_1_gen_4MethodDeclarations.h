﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Boolean>
struct Collection_1_t2413;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Boolean[]
struct BooleanU5BU5D_t448;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t3028;
// System.Collections.Generic.IList`1<System.Boolean>
struct IList_1_t563;

// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::.ctor()
extern "C" void Collection_1__ctor_m15869_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15869(__this, method) (( void (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1__ctor_m15869_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15870_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15870(__this, method) (( bool (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15870_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15871_gshared (Collection_1_t2413 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15871(__this, ___array, ___index, method) (( void (*) (Collection_1_t2413 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15871_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15872_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15872(__this, method) (( Object_t * (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15872_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15873_gshared (Collection_1_t2413 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15873(__this, ___value, method) (( int32_t (*) (Collection_1_t2413 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15873_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15874_gshared (Collection_1_t2413 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15874(__this, ___value, method) (( bool (*) (Collection_1_t2413 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15874_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15875_gshared (Collection_1_t2413 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15875(__this, ___value, method) (( int32_t (*) (Collection_1_t2413 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15875_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15876_gshared (Collection_1_t2413 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15876(__this, ___index, ___value, method) (( void (*) (Collection_1_t2413 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15876_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15877_gshared (Collection_1_t2413 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15877(__this, ___value, method) (( void (*) (Collection_1_t2413 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15877_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15878_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15878(__this, method) (( bool (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15878_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15879_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15879(__this, method) (( Object_t * (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15879_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15880_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15880(__this, method) (( bool (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15880_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15881_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15881(__this, method) (( bool (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15881_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15882_gshared (Collection_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15882(__this, ___index, method) (( Object_t * (*) (Collection_1_t2413 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15882_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15883_gshared (Collection_1_t2413 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15883(__this, ___index, ___value, method) (( void (*) (Collection_1_t2413 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15883_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::Add(T)
extern "C" void Collection_1_Add_m15884_gshared (Collection_1_t2413 * __this, bool ___item, const MethodInfo* method);
#define Collection_1_Add_m15884(__this, ___item, method) (( void (*) (Collection_1_t2413 *, bool, const MethodInfo*))Collection_1_Add_m15884_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::Clear()
extern "C" void Collection_1_Clear_m15885_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15885(__this, method) (( void (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_Clear_m15885_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::ClearItems()
extern "C" void Collection_1_ClearItems_m15886_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15886(__this, method) (( void (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_ClearItems_m15886_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::Contains(T)
extern "C" bool Collection_1_Contains_m15887_gshared (Collection_1_t2413 * __this, bool ___item, const MethodInfo* method);
#define Collection_1_Contains_m15887(__this, ___item, method) (( bool (*) (Collection_1_t2413 *, bool, const MethodInfo*))Collection_1_Contains_m15887_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15888_gshared (Collection_1_t2413 * __this, BooleanU5BU5D_t448* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15888(__this, ___array, ___index, method) (( void (*) (Collection_1_t2413 *, BooleanU5BU5D_t448*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15888_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Boolean>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15889_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15889(__this, method) (( Object_t* (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_GetEnumerator_m15889_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Boolean>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15890_gshared (Collection_1_t2413 * __this, bool ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15890(__this, ___item, method) (( int32_t (*) (Collection_1_t2413 *, bool, const MethodInfo*))Collection_1_IndexOf_m15890_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15891_gshared (Collection_1_t2413 * __this, int32_t ___index, bool ___item, const MethodInfo* method);
#define Collection_1_Insert_m15891(__this, ___index, ___item, method) (( void (*) (Collection_1_t2413 *, int32_t, bool, const MethodInfo*))Collection_1_Insert_m15891_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15892_gshared (Collection_1_t2413 * __this, int32_t ___index, bool ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15892(__this, ___index, ___item, method) (( void (*) (Collection_1_t2413 *, int32_t, bool, const MethodInfo*))Collection_1_InsertItem_m15892_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::Remove(T)
extern "C" bool Collection_1_Remove_m15893_gshared (Collection_1_t2413 * __this, bool ___item, const MethodInfo* method);
#define Collection_1_Remove_m15893(__this, ___item, method) (( bool (*) (Collection_1_t2413 *, bool, const MethodInfo*))Collection_1_Remove_m15893_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15894_gshared (Collection_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15894(__this, ___index, method) (( void (*) (Collection_1_t2413 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15894_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15895_gshared (Collection_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15895(__this, ___index, method) (( void (*) (Collection_1_t2413 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15895_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Boolean>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15896_gshared (Collection_1_t2413 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15896(__this, method) (( int32_t (*) (Collection_1_t2413 *, const MethodInfo*))Collection_1_get_Count_m15896_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Boolean>::get_Item(System.Int32)
extern "C" bool Collection_1_get_Item_m15897_gshared (Collection_1_t2413 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15897(__this, ___index, method) (( bool (*) (Collection_1_t2413 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15897_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15898_gshared (Collection_1_t2413 * __this, int32_t ___index, bool ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15898(__this, ___index, ___value, method) (( void (*) (Collection_1_t2413 *, int32_t, bool, const MethodInfo*))Collection_1_set_Item_m15898_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15899_gshared (Collection_1_t2413 * __this, int32_t ___index, bool ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15899(__this, ___index, ___item, method) (( void (*) (Collection_1_t2413 *, int32_t, bool, const MethodInfo*))Collection_1_SetItem_m15899_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15900_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15900(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15900_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Boolean>::ConvertItem(System.Object)
extern "C" bool Collection_1_ConvertItem_m15901_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15901(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15901_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Boolean>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15902_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15902(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15902_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15903_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15903(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15903_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Boolean>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15904_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15904(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15904_gshared)(__this /* static, unused */, ___list, method)
