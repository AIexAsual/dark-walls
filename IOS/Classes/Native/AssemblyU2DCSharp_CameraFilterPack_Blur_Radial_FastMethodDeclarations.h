﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_Radial_Fast
struct CameraFilterPack_Blur_Radial_Fast_t57;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_Radial_Fast::.ctor()
extern "C" void CameraFilterPack_Blur_Radial_Fast__ctor_m352 (CameraFilterPack_Blur_Radial_Fast_t57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_Radial_Fast::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_Radial_Fast_get_material_m353 (CameraFilterPack_Blur_Radial_Fast_t57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::Start()
extern "C" void CameraFilterPack_Blur_Radial_Fast_Start_m354 (CameraFilterPack_Blur_Radial_Fast_t57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_Radial_Fast_OnRenderImage_m355 (CameraFilterPack_Blur_Radial_Fast_t57 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::OnValidate()
extern "C" void CameraFilterPack_Blur_Radial_Fast_OnValidate_m356 (CameraFilterPack_Blur_Radial_Fast_t57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::Update()
extern "C" void CameraFilterPack_Blur_Radial_Fast_Update_m357 (CameraFilterPack_Blur_Radial_Fast_t57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_Radial_Fast::OnDisable()
extern "C" void CameraFilterPack_Blur_Radial_Fast_OnDisable_m358 (CameraFilterPack_Blur_Radial_Fast_t57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
