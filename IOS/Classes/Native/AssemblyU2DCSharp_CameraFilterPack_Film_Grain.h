﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Film_Grain
struct  CameraFilterPack_Film_Grain_t144  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Film_Grain::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Film_Grain::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_Film_Grain::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_Film_Grain::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_Film_Grain::Value
	float ___Value_6;
};
struct CameraFilterPack_Film_Grain_t144_StaticFields{
	// System.Single CameraFilterPack_Film_Grain::ChangeValue
	float ___ChangeValue_7;
};
