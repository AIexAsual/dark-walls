﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_NewCellShading
struct CameraFilterPack_Drawing_NewCellShading_t108;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_NewCellShading::.ctor()
extern "C" void CameraFilterPack_Drawing_NewCellShading__ctor_m692 (CameraFilterPack_Drawing_NewCellShading_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_NewCellShading::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_NewCellShading_get_material_m693 (CameraFilterPack_Drawing_NewCellShading_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::Start()
extern "C" void CameraFilterPack_Drawing_NewCellShading_Start_m694 (CameraFilterPack_Drawing_NewCellShading_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_NewCellShading_OnRenderImage_m695 (CameraFilterPack_Drawing_NewCellShading_t108 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::Update()
extern "C" void CameraFilterPack_Drawing_NewCellShading_Update_m696 (CameraFilterPack_Drawing_NewCellShading_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_NewCellShading::OnDisable()
extern "C" void CameraFilterPack_Drawing_NewCellShading_OnDisable_m697 (CameraFilterPack_Drawing_NewCellShading_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
