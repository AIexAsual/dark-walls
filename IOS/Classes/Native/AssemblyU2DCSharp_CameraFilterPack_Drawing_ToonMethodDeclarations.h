﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Toon
struct CameraFilterPack_Drawing_Toon_t109;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Toon::.ctor()
extern "C" void CameraFilterPack_Drawing_Toon__ctor_m698 (CameraFilterPack_Drawing_Toon_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Toon::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Toon_get_material_m699 (CameraFilterPack_Drawing_Toon_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::Start()
extern "C" void CameraFilterPack_Drawing_Toon_Start_m700 (CameraFilterPack_Drawing_Toon_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Toon_OnRenderImage_m701 (CameraFilterPack_Drawing_Toon_t109 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::OnValidate()
extern "C" void CameraFilterPack_Drawing_Toon_OnValidate_m702 (CameraFilterPack_Drawing_Toon_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::Update()
extern "C" void CameraFilterPack_Drawing_Toon_Update_m703 (CameraFilterPack_Drawing_Toon_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Toon::OnDisable()
extern "C" void CameraFilterPack_Drawing_Toon_OnDisable_m704 (CameraFilterPack_Drawing_Toon_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
