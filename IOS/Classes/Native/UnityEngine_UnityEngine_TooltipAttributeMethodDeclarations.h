﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TooltipAttribute
struct TooltipAttribute_t1047;
// System.String
struct String_t;

// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C" void TooltipAttribute__ctor_m6243 (TooltipAttribute_t1047 * __this, String_t* ___tooltip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
