﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct U24ArrayTypeU2464_t2159;
struct U24ArrayTypeU2464_t2159_marshaled;

void U24ArrayTypeU2464_t2159_marshal(const U24ArrayTypeU2464_t2159& unmarshaled, U24ArrayTypeU2464_t2159_marshaled& marshaled);
void U24ArrayTypeU2464_t2159_marshal_back(const U24ArrayTypeU2464_t2159_marshaled& marshaled, U24ArrayTypeU2464_t2159& unmarshaled);
void U24ArrayTypeU2464_t2159_marshal_cleanup(U24ArrayTypeU2464_t2159_marshaled& marshaled);
