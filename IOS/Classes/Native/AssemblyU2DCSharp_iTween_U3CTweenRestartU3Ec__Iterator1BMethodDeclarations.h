﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// iTween/<TweenRestart>c__Iterator1B
struct U3CTweenRestartU3Ec__Iterator1B_t434;
// System.Object
struct Object_t;

// System.Void iTween/<TweenRestart>c__Iterator1B::.ctor()
extern "C" void U3CTweenRestartU3Ec__Iterator1B__ctor_m2421 (U3CTweenRestartU3Ec__Iterator1B_t434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__Iterator1B::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CTweenRestartU3Ec__Iterator1B_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2422 (U3CTweenRestartU3Ec__Iterator1B_t434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object iTween/<TweenRestart>c__Iterator1B::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CTweenRestartU3Ec__Iterator1B_System_Collections_IEnumerator_get_Current_m2423 (U3CTweenRestartU3Ec__Iterator1B_t434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean iTween/<TweenRestart>c__Iterator1B::MoveNext()
extern "C" bool U3CTweenRestartU3Ec__Iterator1B_MoveNext_m2424 (U3CTweenRestartU3Ec__Iterator1B_t434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__Iterator1B::Dispose()
extern "C" void U3CTweenRestartU3Ec__Iterator1B_Dispose_m2425 (U3CTweenRestartU3Ec__Iterator1B_t434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iTween/<TweenRestart>c__Iterator1B::Reset()
extern "C" void U3CTweenRestartU3Ec__Iterator1B_Reset_m2426 (U3CTweenRestartU3Ec__Iterator1B_t434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
