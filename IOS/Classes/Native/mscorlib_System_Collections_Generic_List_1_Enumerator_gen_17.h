﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<iTween/LoopType>
struct List_1_t581;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// iTween/LoopType
#include "AssemblyU2DCSharp_iTween_LoopType.h"
// System.Collections.Generic.List`1/Enumerator<iTween/LoopType>
struct  Enumerator_t2458 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::l
	List_1_t581 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<iTween/LoopType>::current
	int32_t ___current_3;
};
