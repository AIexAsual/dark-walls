﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.Color>
struct Collection_1_t2433;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.Color[]
struct ColorU5BU5D_t449;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color>
struct IEnumerator_1_t3034;
// System.Collections.Generic.IList`1<UnityEngine.Color>
struct IList_1_t567;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::.ctor()
extern "C" void Collection_1__ctor_m16158_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16158(__this, method) (( void (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1__ctor_m16158_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16159_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16159(__this, method) (( bool (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16159_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16160_gshared (Collection_1_t2433 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16160(__this, ___array, ___index, method) (( void (*) (Collection_1_t2433 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16160_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16161_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16161(__this, method) (( Object_t * (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16161_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16162_gshared (Collection_1_t2433 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16162(__this, ___value, method) (( int32_t (*) (Collection_1_t2433 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16162_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16163_gshared (Collection_1_t2433 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16163(__this, ___value, method) (( bool (*) (Collection_1_t2433 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16163_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16164_gshared (Collection_1_t2433 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16164(__this, ___value, method) (( int32_t (*) (Collection_1_t2433 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16164_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16165_gshared (Collection_1_t2433 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16165(__this, ___index, ___value, method) (( void (*) (Collection_1_t2433 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16165_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16166_gshared (Collection_1_t2433 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16166(__this, ___value, method) (( void (*) (Collection_1_t2433 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16166_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16167_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16167(__this, method) (( bool (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16167_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16168_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16168(__this, method) (( Object_t * (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16168_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16169_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16169(__this, method) (( bool (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16169_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16170_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16170(__this, method) (( bool (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16170_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16171_gshared (Collection_1_t2433 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16171(__this, ___index, method) (( Object_t * (*) (Collection_1_t2433 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16171_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16172_gshared (Collection_1_t2433 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16172(__this, ___index, ___value, method) (( void (*) (Collection_1_t2433 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16172_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::Add(T)
extern "C" void Collection_1_Add_m16173_gshared (Collection_1_t2433 * __this, Color_t6  ___item, const MethodInfo* method);
#define Collection_1_Add_m16173(__this, ___item, method) (( void (*) (Collection_1_t2433 *, Color_t6 , const MethodInfo*))Collection_1_Add_m16173_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::Clear()
extern "C" void Collection_1_Clear_m16174_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16174(__this, method) (( void (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_Clear_m16174_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::ClearItems()
extern "C" void Collection_1_ClearItems_m16175_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16175(__this, method) (( void (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_ClearItems_m16175_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::Contains(T)
extern "C" bool Collection_1_Contains_m16176_gshared (Collection_1_t2433 * __this, Color_t6  ___item, const MethodInfo* method);
#define Collection_1_Contains_m16176(__this, ___item, method) (( bool (*) (Collection_1_t2433 *, Color_t6 , const MethodInfo*))Collection_1_Contains_m16176_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16177_gshared (Collection_1_t2433 * __this, ColorU5BU5D_t449* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16177(__this, ___array, ___index, method) (( void (*) (Collection_1_t2433 *, ColorU5BU5D_t449*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16177_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16178_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16178(__this, method) (( Object_t* (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_GetEnumerator_m16178_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16179_gshared (Collection_1_t2433 * __this, Color_t6  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16179(__this, ___item, method) (( int32_t (*) (Collection_1_t2433 *, Color_t6 , const MethodInfo*))Collection_1_IndexOf_m16179_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16180_gshared (Collection_1_t2433 * __this, int32_t ___index, Color_t6  ___item, const MethodInfo* method);
#define Collection_1_Insert_m16180(__this, ___index, ___item, method) (( void (*) (Collection_1_t2433 *, int32_t, Color_t6 , const MethodInfo*))Collection_1_Insert_m16180_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16181_gshared (Collection_1_t2433 * __this, int32_t ___index, Color_t6  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16181(__this, ___index, ___item, method) (( void (*) (Collection_1_t2433 *, int32_t, Color_t6 , const MethodInfo*))Collection_1_InsertItem_m16181_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::Remove(T)
extern "C" bool Collection_1_Remove_m16182_gshared (Collection_1_t2433 * __this, Color_t6  ___item, const MethodInfo* method);
#define Collection_1_Remove_m16182(__this, ___item, method) (( bool (*) (Collection_1_t2433 *, Color_t6 , const MethodInfo*))Collection_1_Remove_m16182_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16183_gshared (Collection_1_t2433 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16183(__this, ___index, method) (( void (*) (Collection_1_t2433 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16183_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16184_gshared (Collection_1_t2433 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16184(__this, ___index, method) (( void (*) (Collection_1_t2433 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16184_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16185_gshared (Collection_1_t2433 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16185(__this, method) (( int32_t (*) (Collection_1_t2433 *, const MethodInfo*))Collection_1_get_Count_m16185_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::get_Item(System.Int32)
extern "C" Color_t6  Collection_1_get_Item_m16186_gshared (Collection_1_t2433 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16186(__this, ___index, method) (( Color_t6  (*) (Collection_1_t2433 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16186_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16187_gshared (Collection_1_t2433 * __this, int32_t ___index, Color_t6  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16187(__this, ___index, ___value, method) (( void (*) (Collection_1_t2433 *, int32_t, Color_t6 , const MethodInfo*))Collection_1_set_Item_m16187_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16188_gshared (Collection_1_t2433 * __this, int32_t ___index, Color_t6  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16188(__this, ___index, ___item, method) (( void (*) (Collection_1_t2433 *, int32_t, Color_t6 , const MethodInfo*))Collection_1_SetItem_m16188_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16189_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16189(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16189_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::ConvertItem(System.Object)
extern "C" Color_t6  Collection_1_ConvertItem_m16190_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16190(__this /* static, unused */, ___item, method) (( Color_t6  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16190_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16191_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16191(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16191_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16192_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16192(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16192_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16193_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16193(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16193_gshared)(__this /* static, unused */, ___list, method)
