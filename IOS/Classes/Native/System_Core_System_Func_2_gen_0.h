﻿#pragma once
#include <stdint.h>
// CardboardHead
struct CardboardHead_t244;
// CardboardEye
struct CardboardEye_t240;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<CardboardEye,CardboardHead>
struct  Func_2_t266  : public MulticastDelegate_t219
{
};
