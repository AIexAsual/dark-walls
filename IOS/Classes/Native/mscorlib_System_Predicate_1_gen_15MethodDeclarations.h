﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Space>
struct Predicate_1_t2445;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Predicate`1<UnityEngine.Space>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m16344_gshared (Predicate_1_t2445 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m16344(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2445 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m16344_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Space>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m16345_gshared (Predicate_1_t2445 * __this, int32_t ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m16345(__this, ___obj, method) (( bool (*) (Predicate_1_t2445 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m16345_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Space>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m16346_gshared (Predicate_1_t2445 * __this, int32_t ___obj, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m16346(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2445 *, int32_t, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m16346_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Space>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m16347_gshared (Predicate_1_t2445 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m16347(__this, ___result, method) (( bool (*) (Predicate_1_t2445 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m16347_gshared)(__this, ___result, method)
