﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t585;
// System.Object
struct Object_t;
// UnityEngine.AudioSource
struct AudioSource_t339;
// System.Collections.Generic.IEnumerable`1<UnityEngine.AudioSource>
struct IEnumerable_1_t3051;
// System.Collections.Generic.IEnumerator`1<UnityEngine.AudioSource>
struct IEnumerator_1_t3052;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.AudioSource>
struct ICollection_1_t3053;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.AudioSource>
struct ReadOnlyCollection_1_t2475;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t453;
// System.Predicate`1<UnityEngine.AudioSource>
struct Predicate_1_t2476;
// System.Comparison`1<UnityEngine.AudioSource>
struct Comparison_1_t2478;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.AudioSource>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m3400(__this, method) (( void (*) (List_1_t585 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16826(__this, ___collection, method) (( void (*) (List_1_t585 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::.ctor(System.Int32)
#define List_1__ctor_m16827(__this, ___capacity, method) (( void (*) (List_1_t585 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::.cctor()
#define List_1__cctor_m16828(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16829(__this, method) (( Object_t* (*) (List_1_t585 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16830(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t585 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16831(__this, method) (( Object_t * (*) (List_1_t585 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16832(__this, ___item, method) (( int32_t (*) (List_1_t585 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16833(__this, ___item, method) (( bool (*) (List_1_t585 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16834(__this, ___item, method) (( int32_t (*) (List_1_t585 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16835(__this, ___index, ___item, method) (( void (*) (List_1_t585 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16836(__this, ___item, method) (( void (*) (List_1_t585 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16837(__this, method) (( bool (*) (List_1_t585 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16838(__this, method) (( bool (*) (List_1_t585 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16839(__this, method) (( Object_t * (*) (List_1_t585 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16840(__this, method) (( bool (*) (List_1_t585 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16841(__this, method) (( bool (*) (List_1_t585 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16842(__this, ___index, method) (( Object_t * (*) (List_1_t585 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16843(__this, ___index, ___value, method) (( void (*) (List_1_t585 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Add(T)
#define List_1_Add_m16844(__this, ___item, method) (( void (*) (List_1_t585 *, AudioSource_t339 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16845(__this, ___newCount, method) (( void (*) (List_1_t585 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16846(__this, ___collection, method) (( void (*) (List_1_t585 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16847(__this, ___enumerable, method) (( void (*) (List_1_t585 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16848(__this, ___collection, method) (( void (*) (List_1_t585 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.AudioSource>::AsReadOnly()
#define List_1_AsReadOnly_m16849(__this, method) (( ReadOnlyCollection_1_t2475 * (*) (List_1_t585 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Clear()
#define List_1_Clear_m16850(__this, method) (( void (*) (List_1_t585 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::Contains(T)
#define List_1_Contains_m16851(__this, ___item, method) (( bool (*) (List_1_t585 *, AudioSource_t339 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16852(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t585 *, AudioSourceU5BU5D_t453*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioSource>::Find(System.Predicate`1<T>)
#define List_1_Find_m16853(__this, ___match, method) (( AudioSource_t339 * (*) (List_1_t585 *, Predicate_1_t2476 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16854(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2476 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16855(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t585 *, int32_t, int32_t, Predicate_1_t2476 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.AudioSource>::GetEnumerator()
#define List_1_GetEnumerator_m16856(__this, method) (( Enumerator_t2477  (*) (List_1_t585 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::IndexOf(T)
#define List_1_IndexOf_m16857(__this, ___item, method) (( int32_t (*) (List_1_t585 *, AudioSource_t339 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16858(__this, ___start, ___delta, method) (( void (*) (List_1_t585 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16859(__this, ___index, method) (( void (*) (List_1_t585 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Insert(System.Int32,T)
#define List_1_Insert_m16860(__this, ___index, ___item, method) (( void (*) (List_1_t585 *, int32_t, AudioSource_t339 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16861(__this, ___collection, method) (( void (*) (List_1_t585 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.AudioSource>::Remove(T)
#define List_1_Remove_m16862(__this, ___item, method) (( bool (*) (List_1_t585 *, AudioSource_t339 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16863(__this, ___match, method) (( int32_t (*) (List_1_t585 *, Predicate_1_t2476 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16864(__this, ___index, method) (( void (*) (List_1_t585 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Reverse()
#define List_1_Reverse_m16865(__this, method) (( void (*) (List_1_t585 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Sort()
#define List_1_Sort_m16866(__this, method) (( void (*) (List_1_t585 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16867(__this, ___comparison, method) (( void (*) (List_1_t585 *, Comparison_1_t2478 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.AudioSource>::ToArray()
#define List_1_ToArray_m3413(__this, method) (( AudioSourceU5BU5D_t453* (*) (List_1_t585 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::TrimExcess()
#define List_1_TrimExcess_m16868(__this, method) (( void (*) (List_1_t585 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::get_Capacity()
#define List_1_get_Capacity_m16869(__this, method) (( int32_t (*) (List_1_t585 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16870(__this, ___value, method) (( void (*) (List_1_t585 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.AudioSource>::get_Count()
#define List_1_get_Count_m16871(__this, method) (( int32_t (*) (List_1_t585 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.AudioSource>::get_Item(System.Int32)
#define List_1_get_Item_m16872(__this, ___index, method) (( AudioSource_t339 * (*) (List_1_t585 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.AudioSource>::set_Item(System.Int32,T)
#define List_1_set_Item_m16873(__this, ___index, ___value, method) (( void (*) (List_1_t585 *, int32_t, AudioSource_t339 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
