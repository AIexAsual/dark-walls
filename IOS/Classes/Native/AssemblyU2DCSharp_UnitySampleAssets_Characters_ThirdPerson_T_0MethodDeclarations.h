﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/RayHitComparer
struct RayHitComparer_t412;
// System.Object
struct Object_t;

// System.Void UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/RayHitComparer::.ctor()
extern "C" void RayHitComparer__ctor_m2359 (RayHitComparer_t412 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnitySampleAssets.Characters.ThirdPerson.ThirdPersonCharacter/RayHitComparer::Compare(System.Object,System.Object)
extern "C" int32_t RayHitComparer_Compare_m2360 (RayHitComparer_t412 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
