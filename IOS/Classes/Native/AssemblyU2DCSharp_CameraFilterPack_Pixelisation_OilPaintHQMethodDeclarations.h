﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Pixelisation_OilPaintHQ
struct CameraFilterPack_Pixelisation_OilPaintHQ_t172;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::.ctor()
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ__ctor_m1112 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Pixelisation_OilPaintHQ::get_material()
extern "C" Material_t2 * CameraFilterPack_Pixelisation_OilPaintHQ_get_material_m1113 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::Start()
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_Start_m1114 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_OnRenderImage_m1115 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnValidate()
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_OnValidate_m1116 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::Update()
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_Update_m1117 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Pixelisation_OilPaintHQ::OnDisable()
extern "C" void CameraFilterPack_Pixelisation_OilPaintHQ_OnDisable_m1118 (CameraFilterPack_Pixelisation_OilPaintHQ_t172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
