﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Sharpen_Sharpen
struct CameraFilterPack_Sharpen_Sharpen_t173;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Sharpen_Sharpen::.ctor()
extern "C" void CameraFilterPack_Sharpen_Sharpen__ctor_m1119 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Sharpen_Sharpen::get_material()
extern "C" Material_t2 * CameraFilterPack_Sharpen_Sharpen_get_material_m1120 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::Start()
extern "C" void CameraFilterPack_Sharpen_Sharpen_Start_m1121 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Sharpen_Sharpen_OnRenderImage_m1122 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::OnValidate()
extern "C" void CameraFilterPack_Sharpen_Sharpen_OnValidate_m1123 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::Update()
extern "C" void CameraFilterPack_Sharpen_Sharpen_Update_m1124 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Sharpen_Sharpen::OnDisable()
extern "C" void CameraFilterPack_Sharpen_Sharpen_OnDisable_m1125 (CameraFilterPack_Sharpen_Sharpen_t173 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
