﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSource
struct AudioSource_t339;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.AudioSource>
struct  Comparison_1_t2478  : public MulticastDelegate_t219
{
};
