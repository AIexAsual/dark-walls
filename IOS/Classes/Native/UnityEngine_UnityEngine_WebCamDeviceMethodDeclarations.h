﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WebCamDevice
struct WebCamDevice_t938;
struct WebCamDevice_t938_marshaled;
// System.String
struct String_t;

// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m5786 (WebCamDevice_t938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m5787 (WebCamDevice_t938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void WebCamDevice_t938_marshal(const WebCamDevice_t938& unmarshaled, WebCamDevice_t938_marshaled& marshaled);
void WebCamDevice_t938_marshal_back(const WebCamDevice_t938_marshaled& marshaled, WebCamDevice_t938& unmarshaled);
void WebCamDevice_t938_marshal_cleanup(WebCamDevice_t938_marshaled& marshaled);
