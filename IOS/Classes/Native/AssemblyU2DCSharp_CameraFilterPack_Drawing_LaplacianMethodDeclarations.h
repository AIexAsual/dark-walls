﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Drawing_Laplacian
struct CameraFilterPack_Drawing_Laplacian_t101;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Drawing_Laplacian::.ctor()
extern "C" void CameraFilterPack_Drawing_Laplacian__ctor_m644 (CameraFilterPack_Drawing_Laplacian_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Drawing_Laplacian::get_material()
extern "C" Material_t2 * CameraFilterPack_Drawing_Laplacian_get_material_m645 (CameraFilterPack_Drawing_Laplacian_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::Start()
extern "C" void CameraFilterPack_Drawing_Laplacian_Start_m646 (CameraFilterPack_Drawing_Laplacian_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Drawing_Laplacian_OnRenderImage_m647 (CameraFilterPack_Drawing_Laplacian_t101 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::Update()
extern "C" void CameraFilterPack_Drawing_Laplacian_Update_m648 (CameraFilterPack_Drawing_Laplacian_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Drawing_Laplacian::OnDisable()
extern "C" void CameraFilterPack_Drawing_Laplacian_OnDisable_m649 (CameraFilterPack_Drawing_Laplacian_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
