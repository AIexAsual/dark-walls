﻿#pragma once
#include <stdint.h>
// GazeInputManager
struct GazeInputManager_t359;
// GazeInputManager/OnStateChangeHandler
struct OnStateChangeHandler_t358;
// System.Object
#include "mscorlib_System_Object.h"
// GazeInputEvent
#include "AssemblyU2DCSharp_GazeInputEvent.h"
// GazeInputManager
struct  GazeInputManager_t359  : public Object_t
{
	// GazeInputEvent GazeInputManager::gameState
	int32_t ___gameState_1;
	// GazeInputManager/OnStateChangeHandler GazeInputManager::OnStateChangeEvent
	OnStateChangeHandler_t358 * ___OnStateChangeEvent_2;
};
struct GazeInputManager_t359_StaticFields{
	// GazeInputManager GazeInputManager::_instance
	GazeInputManager_t359 * ____instance_0;
};
