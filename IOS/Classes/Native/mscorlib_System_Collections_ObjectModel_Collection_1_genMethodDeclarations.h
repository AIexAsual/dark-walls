﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2229;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Object[]
struct ObjectU5BU5D_t470;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2280;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t565;

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C" void Collection_1__ctor_m13622_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13622(__this, method) (( void (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1__ctor_m13622_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13623_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13623(__this, method) (( bool (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13623_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13624_gshared (Collection_1_t2229 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13624(__this, ___array, ___index, method) (( void (*) (Collection_1_t2229 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13624_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13625_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13625(__this, method) (( Object_t * (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13625_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13626_gshared (Collection_1_t2229 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13626(__this, ___value, method) (( int32_t (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13626_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13627_gshared (Collection_1_t2229 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13627(__this, ___value, method) (( bool (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13627_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13628_gshared (Collection_1_t2229 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13628(__this, ___value, method) (( int32_t (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13628_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13629_gshared (Collection_1_t2229 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13629(__this, ___index, ___value, method) (( void (*) (Collection_1_t2229 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13629_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13630_gshared (Collection_1_t2229 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13630(__this, ___value, method) (( void (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13630_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13631_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13631(__this, method) (( bool (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13631_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13632_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13632(__this, method) (( Object_t * (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13632_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13633_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13633(__this, method) (( bool (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13633_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13634_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13634(__this, method) (( bool (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13634_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13635_gshared (Collection_1_t2229 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13635(__this, ___index, method) (( Object_t * (*) (Collection_1_t2229 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13635_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13636_gshared (Collection_1_t2229 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13636(__this, ___index, ___value, method) (( void (*) (Collection_1_t2229 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13636_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C" void Collection_1_Add_m13637_gshared (Collection_1_t2229 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Add_m13637(__this, ___item, method) (( void (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_Add_m13637_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C" void Collection_1_Clear_m13638_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13638(__this, method) (( void (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_Clear_m13638_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C" void Collection_1_ClearItems_m13639_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13639(__this, method) (( void (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_ClearItems_m13639_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C" bool Collection_1_Contains_m13640_gshared (Collection_1_t2229 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Contains_m13640(__this, ___item, method) (( bool (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_Contains_m13640_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13641_gshared (Collection_1_t2229 * __this, ObjectU5BU5D_t470* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13641(__this, ___array, ___index, method) (( void (*) (Collection_1_t2229 *, ObjectU5BU5D_t470*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13641_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13642_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13642(__this, method) (( Object_t* (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_GetEnumerator_m13642_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13643_gshared (Collection_1_t2229 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13643(__this, ___item, method) (( int32_t (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_IndexOf_m13643_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13644_gshared (Collection_1_t2229 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Insert_m13644(__this, ___index, ___item, method) (( void (*) (Collection_1_t2229 *, int32_t, Object_t *, const MethodInfo*))Collection_1_Insert_m13644_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13645_gshared (Collection_1_t2229 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13645(__this, ___index, ___item, method) (( void (*) (Collection_1_t2229 *, int32_t, Object_t *, const MethodInfo*))Collection_1_InsertItem_m13645_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C" bool Collection_1_Remove_m13646_gshared (Collection_1_t2229 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Remove_m13646(__this, ___item, method) (( bool (*) (Collection_1_t2229 *, Object_t *, const MethodInfo*))Collection_1_Remove_m13646_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13647_gshared (Collection_1_t2229 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13647(__this, ___index, method) (( void (*) (Collection_1_t2229 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13647_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13648_gshared (Collection_1_t2229 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13648(__this, ___index, method) (( void (*) (Collection_1_t2229 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13648_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13649_gshared (Collection_1_t2229 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13649(__this, method) (( int32_t (*) (Collection_1_t2229 *, const MethodInfo*))Collection_1_get_Count_m13649_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * Collection_1_get_Item_m13650_gshared (Collection_1_t2229 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13650(__this, ___index, method) (( Object_t * (*) (Collection_1_t2229 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13650_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13651_gshared (Collection_1_t2229 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13651(__this, ___index, ___value, method) (( void (*) (Collection_1_t2229 *, int32_t, Object_t *, const MethodInfo*))Collection_1_set_Item_m13651_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13652_gshared (Collection_1_t2229 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13652(__this, ___index, ___item, method) (( void (*) (Collection_1_t2229 *, int32_t, Object_t *, const MethodInfo*))Collection_1_SetItem_m13652_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13653_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13653(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13653_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C" Object_t * Collection_1_ConvertItem_m13654_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13654(__this /* static, unused */, ___item, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13654_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13655_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13655(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13655_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13656_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13656(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13656_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13657_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13657(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13657_gshared)(__this /* static, unused */, ___list, method)
