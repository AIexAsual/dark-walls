﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t243;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CardboardHead
struct  CardboardHead_t244  : public MonoBehaviour_t4
{
	// System.Boolean CardboardHead::trackRotation
	bool ___trackRotation_2;
	// System.Boolean CardboardHead::trackPosition
	bool ___trackPosition_3;
	// UnityEngine.Transform CardboardHead::target
	Transform_t243 * ___target_4;
	// System.Boolean CardboardHead::updateEarly
	bool ___updateEarly_5;
	// System.Boolean CardboardHead::updated
	bool ___updated_6;
};
