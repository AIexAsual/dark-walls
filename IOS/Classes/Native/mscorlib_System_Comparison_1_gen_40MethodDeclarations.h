﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct Comparison_1_t2730;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t972;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m20667(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2730 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m13696_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::Invoke(T,T)
#define Comparison_1_Invoke_m20668(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2730 *, MatchDirectConnectInfo_t972 *, MatchDirectConnectInfo_t972 *, const MethodInfo*))Comparison_1_Invoke_m13697_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m20669(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2730 *, MatchDirectConnectInfo_t972 *, MatchDirectConnectInfo_t972 *, AsyncCallback_t217 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m13698_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m20670(__this, ___result, method) (( int32_t (*) (Comparison_1_t2730 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m13699_gshared)(__this, ___result, method)
