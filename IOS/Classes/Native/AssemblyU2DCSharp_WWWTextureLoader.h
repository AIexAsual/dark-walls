﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Action`1<UnityEngine.Texture2D>
struct Action_1_t302;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// WWWTextureLoader
struct  WWWTextureLoader_t305  : public MonoBehaviour_t4
{
	// System.String WWWTextureLoader::_url
	String_t* ____url_2;
	// System.Action`1<UnityEngine.Texture2D> WWWTextureLoader::OnLoad
	Action_1_t302 * ___OnLoad_3;
};
struct WWWTextureLoader_t305_StaticFields{
	// System.Action`1<UnityEngine.Texture2D> WWWTextureLoader::<>f__am$cache2
	Action_1_t302 * ___U3CU3Ef__amU24cache2_4;
};
