﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t1919;
// System.MarshalByRefObject
struct MarshalByRefObject_t1415;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern "C" MarshalByRefObject_t1415 * ClientActivatedIdentity_GetServerObject_m11643 (ClientActivatedIdentity_t1919 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
