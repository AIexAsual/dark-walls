﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blur_GaussianBlur
struct CameraFilterPack_Blur_GaussianBlur_t53;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blur_GaussianBlur::.ctor()
extern "C" void CameraFilterPack_Blur_GaussianBlur__ctor_m324 (CameraFilterPack_Blur_GaussianBlur_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blur_GaussianBlur::get_material()
extern "C" Material_t2 * CameraFilterPack_Blur_GaussianBlur_get_material_m325 (CameraFilterPack_Blur_GaussianBlur_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::Start()
extern "C" void CameraFilterPack_Blur_GaussianBlur_Start_m326 (CameraFilterPack_Blur_GaussianBlur_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blur_GaussianBlur_OnRenderImage_m327 (CameraFilterPack_Blur_GaussianBlur_t53 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::OnValidate()
extern "C" void CameraFilterPack_Blur_GaussianBlur_OnValidate_m328 (CameraFilterPack_Blur_GaussianBlur_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::Update()
extern "C" void CameraFilterPack_Blur_GaussianBlur_Update_m329 (CameraFilterPack_Blur_GaussianBlur_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blur_GaussianBlur::OnDisable()
extern "C" void CameraFilterPack_Blur_GaussianBlur_OnDisable_m330 (CameraFilterPack_Blur_GaussianBlur_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
