﻿#pragma once
#include <stdint.h>
// UnityEngine.Light
struct Light_t318;
// System.String
struct String_t;
// InteractiveFlashlight
struct InteractiveFlashlight_t329;
// System.Single[]
struct SingleU5BU5D_t72;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// LightController
struct  LightController_t343  : public MonoBehaviour_t4
{
	// UnityEngine.Light LightController::lights
	Light_t318 * ___lights_2;
	// System.Single LightController::myLightsIntensity
	float ___myLightsIntensity_3;
	// System.Single LightController::t
	float ___t_4;
	// System.Single LightController::t2
	float ___t2_5;
	// System.Single LightController::t3
	float ___t3_6;
	// System.Single LightController::timeSpeed
	float ___timeSpeed_7;
	// System.Single LightController::timeDelay
	float ___timeDelay_8;
	// System.Single LightController::fadeTime
	float ___fadeTime_9;
	// System.Boolean LightController::isFlicker
	bool ___isFlicker_10;
	// System.Boolean LightController::isFade
	bool ___isFade_11;
	// System.Boolean LightController::ExitFade
	bool ___ExitFade_12;
	// System.String LightController::fadeState
	String_t* ___fadeState_13;
	// InteractiveFlashlight LightController::flashLight
	InteractiveFlashlight_t329 * ___flashLight_14;
	// System.Single[] LightController::smoothing
	SingleU5BU5D_t72* ___smoothing_15;
};
