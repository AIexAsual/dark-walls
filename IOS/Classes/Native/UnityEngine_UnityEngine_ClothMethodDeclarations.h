﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Cloth
struct Cloth_t337;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.Cloth::set_externalAcceleration(UnityEngine.Vector3)
extern "C" void Cloth_set_externalAcceleration_m3147 (Cloth_t337 * __this, Vector3_t215  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::INTERNAL_set_externalAcceleration(UnityEngine.Vector3&)
extern "C" void Cloth_INTERNAL_set_externalAcceleration_m5758 (Cloth_t337 * __this, Vector3_t215 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_randomAcceleration(UnityEngine.Vector3)
extern "C" void Cloth_set_randomAcceleration_m3148 (Cloth_t337 * __this, Vector3_t215  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::INTERNAL_set_randomAcceleration(UnityEngine.Vector3&)
extern "C" void Cloth_INTERNAL_set_randomAcceleration_m5759 (Cloth_t337 * __this, Vector3_t215 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cloth::set_useGravity(System.Boolean)
extern "C" void Cloth_set_useGravity_m3146 (Cloth_t337 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
