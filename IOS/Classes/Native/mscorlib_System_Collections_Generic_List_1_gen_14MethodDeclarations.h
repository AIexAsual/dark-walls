﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<ArrayIndexes>
struct List_1_t586;
// System.Object
struct Object_t;
// ArrayIndexes
struct ArrayIndexes_t441;
// System.Collections.Generic.IEnumerable`1<ArrayIndexes>
struct IEnumerable_1_t3054;
// System.Collections.Generic.IEnumerator`1<ArrayIndexes>
struct IEnumerator_1_t3055;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<ArrayIndexes>
struct ICollection_1_t3056;
// System.Collections.ObjectModel.ReadOnlyCollection`1<ArrayIndexes>
struct ReadOnlyCollection_1_t2479;
// ArrayIndexes[]
struct ArrayIndexesU5BU5D_t454;
// System.Predicate`1<ArrayIndexes>
struct Predicate_1_t2480;
// System.Comparison`1<ArrayIndexes>
struct Comparison_1_t2482;
// System.Collections.Generic.List`1/Enumerator<ArrayIndexes>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<ArrayIndexes>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_30MethodDeclarations.h"
#define List_1__ctor_m3401(__this, method) (( void (*) (List_1_t586 *, const MethodInfo*))List_1__ctor_m6426_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m16918(__this, ___collection, method) (( void (*) (List_1_t586 *, Object_t*, const MethodInfo*))List_1__ctor_m13490_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::.ctor(System.Int32)
#define List_1__ctor_m16919(__this, ___capacity, method) (( void (*) (List_1_t586 *, int32_t, const MethodInfo*))List_1__ctor_m13492_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::.cctor()
#define List_1__cctor_m16920(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16921(__this, method) (( Object_t* (*) (List_1_t586 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16922(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t586 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13498_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16923(__this, method) (( Object_t * (*) (List_1_t586 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13500_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16924(__this, ___item, method) (( int32_t (*) (List_1_t586 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13502_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16925(__this, ___item, method) (( bool (*) (List_1_t586 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13504_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16926(__this, ___item, method) (( int32_t (*) (List_1_t586 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13506_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16927(__this, ___index, ___item, method) (( void (*) (List_1_t586 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13508_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16928(__this, ___item, method) (( void (*) (List_1_t586 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13510_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16929(__this, method) (( bool (*) (List_1_t586 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13512_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16930(__this, method) (( bool (*) (List_1_t586 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13514_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16931(__this, method) (( Object_t * (*) (List_1_t586 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16932(__this, method) (( bool (*) (List_1_t586 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16933(__this, method) (( bool (*) (List_1_t586 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13520_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16934(__this, ___index, method) (( Object_t * (*) (List_1_t586 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13522_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16935(__this, ___index, ___value, method) (( void (*) (List_1_t586 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13524_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::Add(T)
#define List_1_Add_m16936(__this, ___item, method) (( void (*) (List_1_t586 *, ArrayIndexes_t441 *, const MethodInfo*))List_1_Add_m13526_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16937(__this, ___newCount, method) (( void (*) (List_1_t586 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13528_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16938(__this, ___collection, method) (( void (*) (List_1_t586 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13530_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16939(__this, ___enumerable, method) (( void (*) (List_1_t586 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13532_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16940(__this, ___collection, method) (( void (*) (List_1_t586 *, Object_t*, const MethodInfo*))List_1_AddRange_m13534_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<ArrayIndexes>::AsReadOnly()
#define List_1_AsReadOnly_m16941(__this, method) (( ReadOnlyCollection_1_t2479 * (*) (List_1_t586 *, const MethodInfo*))List_1_AsReadOnly_m13536_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::Clear()
#define List_1_Clear_m16942(__this, method) (( void (*) (List_1_t586 *, const MethodInfo*))List_1_Clear_m13538_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<ArrayIndexes>::Contains(T)
#define List_1_Contains_m16943(__this, ___item, method) (( bool (*) (List_1_t586 *, ArrayIndexes_t441 *, const MethodInfo*))List_1_Contains_m13540_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16944(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t586 *, ArrayIndexesU5BU5D_t454*, int32_t, const MethodInfo*))List_1_CopyTo_m13542_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<ArrayIndexes>::Find(System.Predicate`1<T>)
#define List_1_Find_m16945(__this, ___match, method) (( ArrayIndexes_t441 * (*) (List_1_t586 *, Predicate_1_t2480 *, const MethodInfo*))List_1_Find_m13544_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16946(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2480 *, const MethodInfo*))List_1_CheckMatch_m13546_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16947(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t586 *, int32_t, int32_t, Predicate_1_t2480 *, const MethodInfo*))List_1_GetIndex_m13548_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<ArrayIndexes>::GetEnumerator()
#define List_1_GetEnumerator_m16948(__this, method) (( Enumerator_t2481  (*) (List_1_t586 *, const MethodInfo*))List_1_GetEnumerator_m13550_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::IndexOf(T)
#define List_1_IndexOf_m16949(__this, ___item, method) (( int32_t (*) (List_1_t586 *, ArrayIndexes_t441 *, const MethodInfo*))List_1_IndexOf_m13552_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16950(__this, ___start, ___delta, method) (( void (*) (List_1_t586 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13554_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16951(__this, ___index, method) (( void (*) (List_1_t586 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13556_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::Insert(System.Int32,T)
#define List_1_Insert_m16952(__this, ___index, ___item, method) (( void (*) (List_1_t586 *, int32_t, ArrayIndexes_t441 *, const MethodInfo*))List_1_Insert_m13558_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16953(__this, ___collection, method) (( void (*) (List_1_t586 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13560_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<ArrayIndexes>::Remove(T)
#define List_1_Remove_m16954(__this, ___item, method) (( bool (*) (List_1_t586 *, ArrayIndexes_t441 *, const MethodInfo*))List_1_Remove_m13562_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16955(__this, ___match, method) (( int32_t (*) (List_1_t586 *, Predicate_1_t2480 *, const MethodInfo*))List_1_RemoveAll_m13564_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16956(__this, ___index, method) (( void (*) (List_1_t586 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13566_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::Reverse()
#define List_1_Reverse_m16957(__this, method) (( void (*) (List_1_t586 *, const MethodInfo*))List_1_Reverse_m13568_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::Sort()
#define List_1_Sort_m16958(__this, method) (( void (*) (List_1_t586 *, const MethodInfo*))List_1_Sort_m13570_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16959(__this, ___comparison, method) (( void (*) (List_1_t586 *, Comparison_1_t2482 *, const MethodInfo*))List_1_Sort_m13572_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<ArrayIndexes>::ToArray()
#define List_1_ToArray_m3414(__this, method) (( ArrayIndexesU5BU5D_t454* (*) (List_1_t586 *, const MethodInfo*))List_1_ToArray_m13573_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::TrimExcess()
#define List_1_TrimExcess_m16960(__this, method) (( void (*) (List_1_t586 *, const MethodInfo*))List_1_TrimExcess_m13575_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::get_Capacity()
#define List_1_get_Capacity_m16961(__this, method) (( int32_t (*) (List_1_t586 *, const MethodInfo*))List_1_get_Capacity_m13577_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16962(__this, ___value, method) (( void (*) (List_1_t586 *, int32_t, const MethodInfo*))List_1_set_Capacity_m13579_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<ArrayIndexes>::get_Count()
#define List_1_get_Count_m16963(__this, method) (( int32_t (*) (List_1_t586 *, const MethodInfo*))List_1_get_Count_m13581_gshared)(__this, method)
// T System.Collections.Generic.List`1<ArrayIndexes>::get_Item(System.Int32)
#define List_1_get_Item_m16964(__this, ___index, method) (( ArrayIndexes_t441 * (*) (List_1_t586 *, int32_t, const MethodInfo*))List_1_get_Item_m13583_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<ArrayIndexes>::set_Item(System.Int32,T)
#define List_1_set_Item_m16965(__this, ___index, ___value, method) (( void (*) (List_1_t586 *, int32_t, ArrayIndexes_t441 *, const MethodInfo*))List_1_set_Item_m13585_gshared)(__this, ___index, ___value, method)
