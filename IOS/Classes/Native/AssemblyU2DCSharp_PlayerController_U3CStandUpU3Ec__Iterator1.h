﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// PlayerController
struct PlayerController_t356;
// System.Object
#include "mscorlib_System_Object.h"
// PlayerController/<StandUp>c__Iterator12
struct  U3CStandUpU3Ec__Iterator12_t392  : public Object_t
{
	// System.Single PlayerController/<StandUp>c__Iterator12::delay
	float ___delay_0;
	// System.Int32 PlayerController/<StandUp>c__Iterator12::$PC
	int32_t ___U24PC_1;
	// System.Object PlayerController/<StandUp>c__Iterator12::$current
	Object_t * ___U24current_2;
	// System.Single PlayerController/<StandUp>c__Iterator12::<$>delay
	float ___U3CU24U3Edelay_3;
	// PlayerController PlayerController/<StandUp>c__Iterator12::<>f__this
	PlayerController_t356 * ___U3CU3Ef__this_4;
};
