﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Color>
struct Predicate_1_t2436;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t216;
// System.AsyncCallback
struct AsyncCallback_t217;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Predicate`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m16202_gshared (Predicate_1_t2436 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m16202(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2436 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m16202_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Color>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m16203_gshared (Predicate_1_t2436 * __this, Color_t6  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m16203(__this, ___obj, method) (( bool (*) (Predicate_1_t2436 *, Color_t6 , const MethodInfo*))Predicate_1_Invoke_m16203_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m16204_gshared (Predicate_1_t2436 * __this, Color_t6  ___obj, AsyncCallback_t217 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m16204(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2436 *, Color_t6 , AsyncCallback_t217 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m16204_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m16205_gshared (Predicate_1_t2436 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m16205(__this, ___result, method) (( bool (*) (Predicate_1_t2436 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m16205_gshared)(__this, ___result, method)
