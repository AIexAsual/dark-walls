﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Camera
struct Camera_t14;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Blend2Camera_PhotoshopFilters/filters
#include "AssemblyU2DCSharp_CameraFilterPack_Blend2Camera_PhotoshopFil.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_Blend2Camera_PhotoshopFilters
struct  CameraFilterPack_Blend2Camera_PhotoshopFilters_t39  : public MonoBehaviour_t4
{
	// System.String CameraFilterPack_Blend2Camera_PhotoshopFilters::ShaderName
	String_t* ___ShaderName_2;
	// UnityEngine.Shader CameraFilterPack_Blend2Camera_PhotoshopFilters::SCShader
	Shader_t1 * ___SCShader_3;
	// UnityEngine.Camera CameraFilterPack_Blend2Camera_PhotoshopFilters::Camera2
	Camera_t14 * ___Camera2_4;
	// CameraFilterPack_Blend2Camera_PhotoshopFilters/filters CameraFilterPack_Blend2Camera_PhotoshopFilters::filterchoice
	int32_t ___filterchoice_5;
	// CameraFilterPack_Blend2Camera_PhotoshopFilters/filters CameraFilterPack_Blend2Camera_PhotoshopFilters::filterchoicememo
	int32_t ___filterchoicememo_6;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::TimeX
	float ___TimeX_7;
	// UnityEngine.Vector4 CameraFilterPack_Blend2Camera_PhotoshopFilters::ScreenResolution
	Vector4_t5  ___ScreenResolution_8;
	// UnityEngine.Material CameraFilterPack_Blend2Camera_PhotoshopFilters::SCMaterial
	Material_t2 * ___SCMaterial_9;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::SwitchCameraToCamera2
	float ___SwitchCameraToCamera2_10;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::BlendFX
	float ___BlendFX_11;
	// UnityEngine.RenderTexture CameraFilterPack_Blend2Camera_PhotoshopFilters::Camera2tex
	RenderTexture_t15 * ___Camera2tex_14;
};
struct CameraFilterPack_Blend2Camera_PhotoshopFilters_t39_StaticFields{
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::ChangeValue
	float ___ChangeValue_12;
	// System.Single CameraFilterPack_Blend2Camera_PhotoshopFilters::ChangeValue2
	float ___ChangeValue2_13;
};
