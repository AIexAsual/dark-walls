﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.Char>
struct Comparer_1_t2297;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<System.Char>::.ctor()
extern "C" void Comparer_1__ctor_m14473_gshared (Comparer_1_t2297 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m14473(__this, method) (( void (*) (Comparer_1_t2297 *, const MethodInfo*))Comparer_1__ctor_m14473_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.Char>::.cctor()
extern "C" void Comparer_1__cctor_m14474_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m14474(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m14474_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.Char>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m14475_gshared (Comparer_1_t2297 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m14475(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2297 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m14475_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Char>::get_Default()
extern "C" Comparer_1_t2297 * Comparer_1_get_Default_m14476_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m14476(__this /* static, unused */, method) (( Comparer_1_t2297 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m14476_gshared)(__this /* static, unused */, method)
