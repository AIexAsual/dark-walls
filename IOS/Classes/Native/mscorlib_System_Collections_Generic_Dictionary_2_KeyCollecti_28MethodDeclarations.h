﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t2790;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2788;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21511_gshared (Enumerator_t2790 * __this, Dictionary_2_t2788 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21511(__this, ___host, method) (( void (*) (Enumerator_t2790 *, Dictionary_2_t2788 *, const MethodInfo*))Enumerator__ctor_m21511_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21512_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21512(__this, method) (( Object_t * (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21512_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m21513_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21513(__this, method) (( void (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_Dispose_m21513_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21514_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21514(__this, method) (( bool (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_MoveNext_m21514_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m21515_gshared (Enumerator_t2790 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21515(__this, method) (( Object_t * (*) (Enumerator_t2790 *, const MethodInfo*))Enumerator_get_Current_m21515_gshared)(__this, method)
