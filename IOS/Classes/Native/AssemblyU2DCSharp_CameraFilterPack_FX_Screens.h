﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// CameraFilterPack_FX_Screens
struct  CameraFilterPack_FX_Screens_t139  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_FX_Screens::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_FX_Screens::TimeX
	float ___TimeX_3;
	// UnityEngine.Vector4 CameraFilterPack_FX_Screens::ScreenResolution
	Vector4_t5  ___ScreenResolution_4;
	// UnityEngine.Material CameraFilterPack_FX_Screens::SCMaterial
	Material_t2 * ___SCMaterial_5;
	// System.Single CameraFilterPack_FX_Screens::Tiles
	float ___Tiles_6;
	// System.Single CameraFilterPack_FX_Screens::Speed
	float ___Speed_7;
	// System.Single CameraFilterPack_FX_Screens::PosX
	float ___PosX_8;
	// System.Single CameraFilterPack_FX_Screens::PosY
	float ___PosY_9;
};
struct CameraFilterPack_FX_Screens_t139_StaticFields{
	// System.Single CameraFilterPack_FX_Screens::ChangeValue
	float ___ChangeValue_10;
	// System.Single CameraFilterPack_FX_Screens::ChangeValue2
	float ___ChangeValue2_11;
	// System.Single CameraFilterPack_FX_Screens::ChangeValue3
	float ___ChangeValue3_12;
	// System.Single CameraFilterPack_FX_Screens::ChangeValue4
	float ___ChangeValue4_13;
};
