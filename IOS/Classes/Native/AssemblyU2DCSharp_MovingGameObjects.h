﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Object
#include "mscorlib_System_Object.h"
// MovingGameObjects
struct  MovingGameObjects_t372  : public Object_t
{
};
struct MovingGameObjects_t372_StaticFields{
	// System.String MovingGameObjects::CABINET
	String_t* ___CABINET_0;
	// System.String MovingGameObjects::CURTAIN_MOVER
	String_t* ___CURTAIN_MOVER_1;
};
