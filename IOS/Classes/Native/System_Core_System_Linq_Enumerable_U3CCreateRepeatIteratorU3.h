﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>
struct  U3CCreateRepeatIteratorU3Ec__IteratorE_1_t2286  : public Object_t
{
	// System.Int32 System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::count
	int32_t ___count_1;
	// TResult System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::element
	Object_t * ___element_2;
	// System.Int32 System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::$PC
	int32_t ___U24PC_3;
	// TResult System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::$current
	Object_t * ___U24current_4;
	// System.Int32 System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::<$>count
	int32_t ___U3CU24U3Ecount_5;
	// TResult System.Linq.Enumerable/<CreateRepeatIterator>c__IteratorE`1<System.Object>::<$>element
	Object_t * ___U3CU24U3Eelement_6;
};
