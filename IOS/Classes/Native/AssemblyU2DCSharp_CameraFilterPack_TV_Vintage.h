﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_TV_Vintage
struct  CameraFilterPack_TV_Vintage_t199  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_TV_Vintage::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_TV_Vintage::TimeX
	float ___TimeX_3;
	// System.Single CameraFilterPack_TV_Vintage::Distortion
	float ___Distortion_4;
	// UnityEngine.Material CameraFilterPack_TV_Vintage::SCMaterial
	Material_t2 * ___SCMaterial_5;
};
struct CameraFilterPack_TV_Vintage_t199_StaticFields{
	// System.Single CameraFilterPack_TV_Vintage::ChangeDistortion
	float ___ChangeDistortion_6;
};
