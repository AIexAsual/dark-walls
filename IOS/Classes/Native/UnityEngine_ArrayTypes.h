﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct  MonoBehaviourU5BU5D_t3340  : public Array_t
{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct  BehaviourU5BU5D_t3341  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct  ComponentU5BU5D_t539  : public Array_t
{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct  ObjectU5BU5D_t1077  : public Array_t
{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct  Vector3U5BU5D_t317  : public Array_t
{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct  Vector2U5BU5D_t437  : public Array_t
{
};
// UnityEngine.Color[]
// UnityEngine.Color[]
struct  ColorU5BU5D_t449  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct  TransformU5BU5D_t423  : public Array_t
{
};
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct  GameObjectU5BU5D_t323  : public Array_t
{
};
// UnityEngine.Rigidbody[]
// UnityEngine.Rigidbody[]
struct  RigidbodyU5BU5D_t533  : public Array_t
{
};
// UnityEngine.AudioClip[]
// UnityEngine.AudioClip[]
struct  AudioClipU5BU5D_t338  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct  RaycastHitU5BU5D_t540  : public Array_t
{
};
// UnityEngine.Animator[]
// UnityEngine.Animator[]
struct  AnimatorU5BU5D_t541  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct  GUILayoutOptionU5BU5D_t544  : public Array_t
{
};
// UnityEngine.Color[,]
// UnityEngine.Color[,]
struct  ColorU5BU2CU5D_t438  : public Array_t
{
};
// UnityEngine.Rect[]
// UnityEngine.Rect[]
struct  RectU5BU5D_t439  : public Array_t
{
};
// UnityEngine.Material[]
// UnityEngine.Material[]
struct  MaterialU5BU5D_t551  : public Array_t
{
};
// UnityEngine.Space[]
// UnityEngine.Space[]
struct  SpaceU5BU5D_t450  : public Array_t
{
};
// UnityEngine.AudioSource[]
// UnityEngine.AudioSource[]
struct  AudioSourceU5BU5D_t453  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct  RaycastHit2DU5BU5D_t787  : public Array_t
{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct  FontU5BU5D_t2539  : public Array_t
{
};
struct FontU5BU5D_t2539_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct  UIVertexU5BU5D_t684  : public Array_t
{
};
struct UIVertexU5BU5D_t684_StaticFields{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct  CanvasU5BU5D_t2565  : public Array_t
{
};
struct CanvasU5BU5D_t2565_StaticFields{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct  UILineInfoU5BU5D_t1087  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct  UICharInfoU5BU5D_t1086  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct  CanvasGroupU5BU5D_t2605  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct  RectTransformU5BU5D_t2624  : public Array_t
{
};
struct RectTransformU5BU5D_t2624_StaticFields{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct  IAchievementDescriptionU5BU5D_t1107  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct  IAchievementU5BU5D_t1109  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct  IScoreU5BU5D_t1037  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct  IUserProfileU5BU5D_t1031  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct  AchievementDescriptionU5BU5D_t844  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct  UserProfileU5BU5D_t845  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct  GcLeaderboardU5BU5D_t2641  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct  GcAchievementDataU5BU5D_t1079  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct  AchievementU5BU5D_t1108  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct  GcScoreDataU5BU5D_t1080  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct  ScoreU5BU5D_t1110  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct  LayoutCacheU5BU5D_t2650  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct  GUILayoutEntryU5BU5D_t2656  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t2656_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct  GUIStyleU5BU5D_t887  : public Array_t
{
};
struct GUIStyleU5BU5D_t887_StaticFields{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct  CameraU5BU5D_t1041  : public Array_t
{
};
struct CameraU5BU5D_t1041_StaticFields{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct  DisplayU5BU5D_t917  : public Array_t
{
};
struct DisplayU5BU5D_t917_StaticFields{
};
// UnityEngine.ContactPoint[]
// UnityEngine.ContactPoint[]
struct  ContactPointU5BU5D_t931  : public Array_t
{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct  Rigidbody2DU5BU5D_t2678  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct  KeyframeU5BU5D_t1085  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct  MatchDirectConnectInfoU5BU5D_t2725  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDesc[]
// UnityEngine.Networking.Match.MatchDesc[]
struct  MatchDescU5BU5D_t2731  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkID[]
// UnityEngine.Networking.Types.NetworkID[]
struct  NetworkIDU5BU5D_t2737  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkAccessToken[]
// UnityEngine.Networking.Types.NetworkAccessToken[]
struct  NetworkAccessTokenU5BU5D_t2738  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
struct  ConstructorDelegateU5BU5D_t2767  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
struct  GetDelegateU5BU5D_t2779  : public Array_t
{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct  DisallowMultipleComponentU5BU5D_t1006  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct  ExecuteInEditModeU5BU5D_t1007  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct  RequireComponentU5BU5D_t1008  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct  HitInfoU5BU5D_t1040  : public Array_t
{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct  TextEditOpU5BU5D_t2820  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct  EventU5BU5D_t2819  : public Array_t
{
};
struct EventU5BU5D_t2819_StaticFields{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct  PersistentCallU5BU5D_t2853  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct  BaseInvokableCallU5BU5D_t2858  : public Array_t
{
};
