﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Distortion_Half_Sphere
struct CameraFilterPack_Distortion_Half_Sphere_t88;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Distortion_Half_Sphere::.ctor()
extern "C" void CameraFilterPack_Distortion_Half_Sphere__ctor_m553 (CameraFilterPack_Distortion_Half_Sphere_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Distortion_Half_Sphere::get_material()
extern "C" Material_t2 * CameraFilterPack_Distortion_Half_Sphere_get_material_m554 (CameraFilterPack_Distortion_Half_Sphere_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::Start()
extern "C" void CameraFilterPack_Distortion_Half_Sphere_Start_m555 (CameraFilterPack_Distortion_Half_Sphere_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Distortion_Half_Sphere_OnRenderImage_m556 (CameraFilterPack_Distortion_Half_Sphere_t88 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::OnValidate()
extern "C" void CameraFilterPack_Distortion_Half_Sphere_OnValidate_m557 (CameraFilterPack_Distortion_Half_Sphere_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::Update()
extern "C" void CameraFilterPack_Distortion_Half_Sphere_Update_m558 (CameraFilterPack_Distortion_Half_Sphere_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Distortion_Half_Sphere::OnDisable()
extern "C" void CameraFilterPack_Distortion_Half_Sphere_OnDisable_m559 (CameraFilterPack_Distortion_Half_Sphere_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
