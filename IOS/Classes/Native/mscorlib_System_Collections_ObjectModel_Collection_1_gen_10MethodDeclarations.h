﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t2559;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t684;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t3105;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t817;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m18209_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1__ctor_m18209(__this, method) (( void (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1__ctor_m18209_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18210_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18210(__this, method) (( bool (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18210_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m18211_gshared (Collection_1_t2559 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m18211(__this, ___array, ___index, method) (( void (*) (Collection_1_t2559 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m18211_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m18212_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m18212(__this, method) (( Object_t * (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m18212_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m18213_gshared (Collection_1_t2559 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m18213(__this, ___value, method) (( int32_t (*) (Collection_1_t2559 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m18213_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m18214_gshared (Collection_1_t2559 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m18214(__this, ___value, method) (( bool (*) (Collection_1_t2559 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m18214_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m18215_gshared (Collection_1_t2559 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m18215(__this, ___value, method) (( int32_t (*) (Collection_1_t2559 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m18215_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m18216_gshared (Collection_1_t2559 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m18216(__this, ___index, ___value, method) (( void (*) (Collection_1_t2559 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m18216_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m18217_gshared (Collection_1_t2559 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m18217(__this, ___value, method) (( void (*) (Collection_1_t2559 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m18217_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m18218_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m18218(__this, method) (( bool (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m18218_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m18219_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m18219(__this, method) (( Object_t * (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m18219_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m18220_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m18220(__this, method) (( bool (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m18220_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m18221_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m18221(__this, method) (( bool (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m18221_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m18222_gshared (Collection_1_t2559 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m18222(__this, ___index, method) (( Object_t * (*) (Collection_1_t2559 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m18222_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m18223_gshared (Collection_1_t2559 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m18223(__this, ___index, ___value, method) (( void (*) (Collection_1_t2559 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m18223_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m18224_gshared (Collection_1_t2559 * __this, UIVertex_t685  ___item, const MethodInfo* method);
#define Collection_1_Add_m18224(__this, ___item, method) (( void (*) (Collection_1_t2559 *, UIVertex_t685 , const MethodInfo*))Collection_1_Add_m18224_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m18225_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_Clear_m18225(__this, method) (( void (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_Clear_m18225_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m18226_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m18226(__this, method) (( void (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_ClearItems_m18226_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m18227_gshared (Collection_1_t2559 * __this, UIVertex_t685  ___item, const MethodInfo* method);
#define Collection_1_Contains_m18227(__this, ___item, method) (( bool (*) (Collection_1_t2559 *, UIVertex_t685 , const MethodInfo*))Collection_1_Contains_m18227_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m18228_gshared (Collection_1_t2559 * __this, UIVertexU5BU5D_t684* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m18228(__this, ___array, ___index, method) (( void (*) (Collection_1_t2559 *, UIVertexU5BU5D_t684*, int32_t, const MethodInfo*))Collection_1_CopyTo_m18228_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m18229_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m18229(__this, method) (( Object_t* (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_GetEnumerator_m18229_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m18230_gshared (Collection_1_t2559 * __this, UIVertex_t685  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m18230(__this, ___item, method) (( int32_t (*) (Collection_1_t2559 *, UIVertex_t685 , const MethodInfo*))Collection_1_IndexOf_m18230_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m18231_gshared (Collection_1_t2559 * __this, int32_t ___index, UIVertex_t685  ___item, const MethodInfo* method);
#define Collection_1_Insert_m18231(__this, ___index, ___item, method) (( void (*) (Collection_1_t2559 *, int32_t, UIVertex_t685 , const MethodInfo*))Collection_1_Insert_m18231_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m18232_gshared (Collection_1_t2559 * __this, int32_t ___index, UIVertex_t685  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m18232(__this, ___index, ___item, method) (( void (*) (Collection_1_t2559 *, int32_t, UIVertex_t685 , const MethodInfo*))Collection_1_InsertItem_m18232_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m18233_gshared (Collection_1_t2559 * __this, UIVertex_t685  ___item, const MethodInfo* method);
#define Collection_1_Remove_m18233(__this, ___item, method) (( bool (*) (Collection_1_t2559 *, UIVertex_t685 , const MethodInfo*))Collection_1_Remove_m18233_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m18234_gshared (Collection_1_t2559 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m18234(__this, ___index, method) (( void (*) (Collection_1_t2559 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m18234_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m18235_gshared (Collection_1_t2559 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m18235(__this, ___index, method) (( void (*) (Collection_1_t2559 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m18235_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m18236_gshared (Collection_1_t2559 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m18236(__this, method) (( int32_t (*) (Collection_1_t2559 *, const MethodInfo*))Collection_1_get_Count_m18236_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t685  Collection_1_get_Item_m18237_gshared (Collection_1_t2559 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m18237(__this, ___index, method) (( UIVertex_t685  (*) (Collection_1_t2559 *, int32_t, const MethodInfo*))Collection_1_get_Item_m18237_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m18238_gshared (Collection_1_t2559 * __this, int32_t ___index, UIVertex_t685  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m18238(__this, ___index, ___value, method) (( void (*) (Collection_1_t2559 *, int32_t, UIVertex_t685 , const MethodInfo*))Collection_1_set_Item_m18238_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m18239_gshared (Collection_1_t2559 * __this, int32_t ___index, UIVertex_t685  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m18239(__this, ___index, ___item, method) (( void (*) (Collection_1_t2559 *, int32_t, UIVertex_t685 , const MethodInfo*))Collection_1_SetItem_m18239_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m18240_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m18240(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m18240_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t685  Collection_1_ConvertItem_m18241_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m18241(__this /* static, unused */, ___item, method) (( UIVertex_t685  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m18241_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m18242_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m18242(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m18242_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m18243_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m18243(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m18243_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m18244_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m18244(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m18244_gshared)(__this /* static, unused */, ___list, method)
