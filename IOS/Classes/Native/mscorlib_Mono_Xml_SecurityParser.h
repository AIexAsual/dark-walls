﻿#pragma once
#include <stdint.h>
// System.Security.SecurityElement
struct SecurityElement_t1639;
// System.Collections.Stack
struct Stack_t1076;
// Mono.Xml.SmallXmlParser
#include "mscorlib_Mono_Xml_SmallXmlParser.h"
// Mono.Xml.SecurityParser
struct  SecurityParser_t1640  : public SmallXmlParser_t1641
{
	// System.Security.SecurityElement Mono.Xml.SecurityParser::root
	SecurityElement_t1639 * ___root_13;
	// System.Security.SecurityElement Mono.Xml.SecurityParser::current
	SecurityElement_t1639 * ___current_14;
	// System.Collections.Stack Mono.Xml.SecurityParser::stack
	Stack_t1076 * ___stack_15;
};
