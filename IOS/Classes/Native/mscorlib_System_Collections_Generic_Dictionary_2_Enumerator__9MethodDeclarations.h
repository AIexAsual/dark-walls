﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct Enumerator_t2518;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t2513;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17559_gshared (Enumerator_t2518 * __this, Dictionary_2_t2513 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17559(__this, ___dictionary, method) (( void (*) (Enumerator_t2518 *, Dictionary_2_t2513 *, const MethodInfo*))Enumerator__ctor_m17559_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17560_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17560(__this, method) (( Object_t * (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17560_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t552  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17561_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17561(__this, method) (( DictionaryEntry_t552  (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17562_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17562(__this, method) (( Object_t * (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17562_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17563_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17563(__this, method) (( Object_t * (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17563_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17564_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17564(__this, method) (( bool (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_MoveNext_m17564_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2514  Enumerator_get_Current_m17565_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17565(__this, method) (( KeyValuePair_2_t2514  (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_get_Current_m17565_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m17566_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17566(__this, method) (( int32_t (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_get_CurrentKey_m17566_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m17567_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17567(__this, method) (( Object_t * (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_get_CurrentValue_m17567_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m17568_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17568(__this, method) (( void (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_VerifyState_m17568_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17569_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17569(__this, method) (( void (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_VerifyCurrent_m17569_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m17570_gshared (Enumerator_t2518 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17570(__this, method) (( void (*) (Enumerator_t2518 *, const MethodInfo*))Enumerator_Dispose_m17570_gshared)(__this, method)
