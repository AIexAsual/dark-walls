﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>
struct InternalEnumerator_1_t2532;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17752_gshared (InternalEnumerator_1_t2532 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17752(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2532 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17752_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17753_gshared (InternalEnumerator_1_t2532 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17753(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2532 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17753_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17754_gshared (InternalEnumerator_1_t2532 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17754(__this, method) (( void (*) (InternalEnumerator_1_t2532 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17754_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17755_gshared (InternalEnumerator_1_t2532 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17755(__this, method) (( bool (*) (InternalEnumerator_1_t2532 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17755_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern "C" RaycastHit2D_t789  InternalEnumerator_1_get_Current_m17756_gshared (InternalEnumerator_1_t2532 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17756(__this, method) (( RaycastHit2D_t789  (*) (InternalEnumerator_1_t2532 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17756_gshared)(__this, method)
