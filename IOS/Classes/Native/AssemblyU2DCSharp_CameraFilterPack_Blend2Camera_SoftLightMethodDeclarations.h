﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Blend2Camera_SoftLight
struct CameraFilterPack_Blend2Camera_SoftLight_t43;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Blend2Camera_SoftLight::.ctor()
extern "C" void CameraFilterPack_Blend2Camera_SoftLight__ctor_m250 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Blend2Camera_SoftLight::get_material()
extern "C" Material_t2 * CameraFilterPack_Blend2Camera_SoftLight_get_material_m251 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::Start()
extern "C" void CameraFilterPack_Blend2Camera_SoftLight_Start_m252 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Blend2Camera_SoftLight_OnRenderImage_m253 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnValidate()
extern "C" void CameraFilterPack_Blend2Camera_SoftLight_OnValidate_m254 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::Update()
extern "C" void CameraFilterPack_Blend2Camera_SoftLight_Update_m255 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnEnable()
extern "C" void CameraFilterPack_Blend2Camera_SoftLight_OnEnable_m256 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Blend2Camera_SoftLight::OnDisable()
extern "C" void CameraFilterPack_Blend2Camera_SoftLight_OnDisable_m257 (CameraFilterPack_Blend2Camera_SoftLight_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
