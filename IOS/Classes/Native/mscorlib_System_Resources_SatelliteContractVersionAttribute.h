﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t522;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t1814  : public Attribute_t903
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t522 * ___ver_0;
};
