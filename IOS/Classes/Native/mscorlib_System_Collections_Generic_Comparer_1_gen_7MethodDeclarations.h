﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Space>
struct Comparer_1_t2446;
// System.Object
struct Object_t;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Space>::.ctor()
extern "C" void Comparer_1__ctor_m16348_gshared (Comparer_1_t2446 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m16348(__this, method) (( void (*) (Comparer_1_t2446 *, const MethodInfo*))Comparer_1__ctor_m16348_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Space>::.cctor()
extern "C" void Comparer_1__cctor_m16349_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m16349(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m16349_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Space>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m16350_gshared (Comparer_1_t2446 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m16350(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2446 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m16350_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Space>::get_Default()
extern "C" Comparer_1_t2446 * Comparer_1_get_Default_m16351_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m16351(__this /* static, unused */, method) (( Comparer_1_t2446 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m16351_gshared)(__this /* static, unused */, method)
