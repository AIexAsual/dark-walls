﻿#pragma once
#include <stdint.h>
// RagdollHelper/BodyPart[]
struct BodyPartU5BU5D_t2307;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<RagdollHelper/BodyPart>
struct  List_1_t401  : public Object_t
{
	// T[] System.Collections.Generic.List`1<RagdollHelper/BodyPart>::_items
	BodyPartU5BU5D_t2307* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<RagdollHelper/BodyPart>::_version
	int32_t ____version_3;
};
struct List_1_t401_StaticFields{
	// T[] System.Collections.Generic.List`1<RagdollHelper/BodyPart>::EmptyArray
	BodyPartU5BU5D_t2307* ___EmptyArray_4;
};
