﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Space>
struct List_1_t579;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>
struct  Enumerator_t2440 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::l
	List_1_t579 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Space>::current
	int32_t ___current_3;
};
