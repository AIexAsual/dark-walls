﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// CameraFilterPack_Color_GrayScale
struct CameraFilterPack_Color_GrayScale_t63;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.RenderTexture
struct RenderTexture_t15;

// System.Void CameraFilterPack_Color_GrayScale::.ctor()
extern "C" void CameraFilterPack_Color_GrayScale__ctor_m394 (CameraFilterPack_Color_GrayScale_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material CameraFilterPack_Color_GrayScale::get_material()
extern "C" Material_t2 * CameraFilterPack_Color_GrayScale_get_material_m395 (CameraFilterPack_Color_GrayScale_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_GrayScale::Start()
extern "C" void CameraFilterPack_Color_GrayScale_Start_m396 (CameraFilterPack_Color_GrayScale_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_GrayScale::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" void CameraFilterPack_Color_GrayScale_OnRenderImage_m397 (CameraFilterPack_Color_GrayScale_t63 * __this, RenderTexture_t15 * ___sourceTexture, RenderTexture_t15 * ___destTexture, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_GrayScale::Update()
extern "C" void CameraFilterPack_Color_GrayScale_Update_m398 (CameraFilterPack_Color_GrayScale_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraFilterPack_Color_GrayScale::OnDisable()
extern "C" void CameraFilterPack_Color_GrayScale_OnDisable_m399 (CameraFilterPack_Color_GrayScale_t63 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
