﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Space>
struct DefaultComparer_t2447;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Space>::.ctor()
extern "C" void DefaultComparer__ctor_m16352_gshared (DefaultComparer_t2447 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16352(__this, method) (( void (*) (DefaultComparer_t2447 *, const MethodInfo*))DefaultComparer__ctor_m16352_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Space>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m16353_gshared (DefaultComparer_t2447 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m16353(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2447 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Compare_m16353_gshared)(__this, ___x, ___y, method)
