﻿#pragma once
#include <stdint.h>
// UnityEngine.Shader
struct Shader_t1;
// UnityEngine.Material
struct Material_t2;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// CameraFilterPack_Drawing_Halftone
struct  CameraFilterPack_Drawing_Halftone_t100  : public MonoBehaviour_t4
{
	// UnityEngine.Shader CameraFilterPack_Drawing_Halftone::SCShader
	Shader_t1 * ___SCShader_2;
	// System.Single CameraFilterPack_Drawing_Halftone::TimeX
	float ___TimeX_3;
	// UnityEngine.Material CameraFilterPack_Drawing_Halftone::SCMaterial
	Material_t2 * ___SCMaterial_4;
	// System.Single CameraFilterPack_Drawing_Halftone::Threshold
	float ___Threshold_5;
	// System.Single CameraFilterPack_Drawing_Halftone::DotSize
	float ___DotSize_6;
};
struct CameraFilterPack_Drawing_Halftone_t100_StaticFields{
	// System.Single CameraFilterPack_Drawing_Halftone::ChangeThreshold
	float ___ChangeThreshold_7;
	// System.Single CameraFilterPack_Drawing_Halftone::ChangeDotSize
	float ___ChangeDotSize_8;
};
