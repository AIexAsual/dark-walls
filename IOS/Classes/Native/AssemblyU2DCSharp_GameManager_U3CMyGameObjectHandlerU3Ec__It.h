﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t256;
// System.Object
struct Object_t;
// System.Object
#include "mscorlib_System_Object.h"
// GameManager/<MyGameObjectHandler>c__IteratorE
struct  U3CMyGameObjectHandlerU3Ec__IteratorE_t353  : public Object_t
{
	// System.String GameManager/<MyGameObjectHandler>c__IteratorE::name
	String_t* ___name_0;
	// UnityEngine.GameObject GameManager/<MyGameObjectHandler>c__IteratorE::<go>__0
	GameObject_t256 * ___U3CgoU3E__0_1;
	// System.Boolean GameManager/<MyGameObjectHandler>c__IteratorE::val
	bool ___val_2;
	// System.Int32 GameManager/<MyGameObjectHandler>c__IteratorE::$PC
	int32_t ___U24PC_3;
	// System.Object GameManager/<MyGameObjectHandler>c__IteratorE::$current
	Object_t * ___U24current_4;
	// System.String GameManager/<MyGameObjectHandler>c__IteratorE::<$>name
	String_t* ___U3CU24U3Ename_5;
	// System.Boolean GameManager/<MyGameObjectHandler>c__IteratorE::<$>val
	bool ___U3CU24U3Eval_6;
};
