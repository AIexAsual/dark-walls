﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t958;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t3180;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t3181;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t464;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t813;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t2695;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1087;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t2699;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t2702;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_38.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m20185_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1__ctor_m20185(__this, method) (( void (*) (List_1_t958 *, const MethodInfo*))List_1__ctor_m20185_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m20186_gshared (List_1_t958 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m20186(__this, ___collection, method) (( void (*) (List_1_t958 *, Object_t*, const MethodInfo*))List_1__ctor_m20186_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m6401_gshared (List_1_t958 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m6401(__this, ___capacity, method) (( void (*) (List_1_t958 *, int32_t, const MethodInfo*))List_1__ctor_m6401_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m20187_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m20187(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m20187_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20188_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20188(__this, method) (( Object_t* (*) (List_1_t958 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20188_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m20189_gshared (List_1_t958 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m20189(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t958 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m20189_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m20190_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20190(__this, method) (( Object_t * (*) (List_1_t958 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m20190_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m20191_gshared (List_1_t958 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m20191(__this, ___item, method) (( int32_t (*) (List_1_t958 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m20191_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m20192_gshared (List_1_t958 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m20192(__this, ___item, method) (( bool (*) (List_1_t958 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m20192_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m20193_gshared (List_1_t958 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m20193(__this, ___item, method) (( int32_t (*) (List_1_t958 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m20193_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m20194_gshared (List_1_t958 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m20194(__this, ___index, ___item, method) (( void (*) (List_1_t958 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m20194_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m20195_gshared (List_1_t958 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m20195(__this, ___item, method) (( void (*) (List_1_t958 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m20195_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20196_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20196(__this, method) (( bool (*) (List_1_t958 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20196_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m20197_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20197(__this, method) (( bool (*) (List_1_t958 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m20197_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m20198_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m20198(__this, method) (( Object_t * (*) (List_1_t958 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m20198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m20199_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m20199(__this, method) (( bool (*) (List_1_t958 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m20199_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m20200_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m20200(__this, method) (( bool (*) (List_1_t958 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m20200_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m20201_gshared (List_1_t958 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m20201(__this, ___index, method) (( Object_t * (*) (List_1_t958 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m20201_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m20202_gshared (List_1_t958 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m20202(__this, ___index, ___value, method) (( void (*) (List_1_t958 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m20202_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m20203_gshared (List_1_t958 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define List_1_Add_m20203(__this, ___item, method) (( void (*) (List_1_t958 *, UILineInfo_t809 , const MethodInfo*))List_1_Add_m20203_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m20204_gshared (List_1_t958 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m20204(__this, ___newCount, method) (( void (*) (List_1_t958 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m20204_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m20205_gshared (List_1_t958 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m20205(__this, ___collection, method) (( void (*) (List_1_t958 *, Object_t*, const MethodInfo*))List_1_AddCollection_m20205_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m20206_gshared (List_1_t958 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m20206(__this, ___enumerable, method) (( void (*) (List_1_t958 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m20206_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m20207_gshared (List_1_t958 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m20207(__this, ___collection, method) (( void (*) (List_1_t958 *, Object_t*, const MethodInfo*))List_1_AddRange_m20207_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2695 * List_1_AsReadOnly_m20208_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m20208(__this, method) (( ReadOnlyCollection_1_t2695 * (*) (List_1_t958 *, const MethodInfo*))List_1_AsReadOnly_m20208_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m20209_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_Clear_m20209(__this, method) (( void (*) (List_1_t958 *, const MethodInfo*))List_1_Clear_m20209_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m20210_gshared (List_1_t958 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define List_1_Contains_m20210(__this, ___item, method) (( bool (*) (List_1_t958 *, UILineInfo_t809 , const MethodInfo*))List_1_Contains_m20210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m20211_gshared (List_1_t958 * __this, UILineInfoU5BU5D_t1087* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m20211(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t958 *, UILineInfoU5BU5D_t1087*, int32_t, const MethodInfo*))List_1_CopyTo_m20211_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Find(System.Predicate`1<T>)
extern "C" UILineInfo_t809  List_1_Find_m20212_gshared (List_1_t958 * __this, Predicate_1_t2699 * ___match, const MethodInfo* method);
#define List_1_Find_m20212(__this, ___match, method) (( UILineInfo_t809  (*) (List_1_t958 *, Predicate_1_t2699 *, const MethodInfo*))List_1_Find_m20212_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m20213_gshared (Object_t * __this /* static, unused */, Predicate_1_t2699 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m20213(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2699 *, const MethodInfo*))List_1_CheckMatch_m20213_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m20214_gshared (List_1_t958 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2699 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m20214(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t958 *, int32_t, int32_t, Predicate_1_t2699 *, const MethodInfo*))List_1_GetIndex_m20214_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t2694  List_1_GetEnumerator_m20215_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m20215(__this, method) (( Enumerator_t2694  (*) (List_1_t958 *, const MethodInfo*))List_1_GetEnumerator_m20215_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m20216_gshared (List_1_t958 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define List_1_IndexOf_m20216(__this, ___item, method) (( int32_t (*) (List_1_t958 *, UILineInfo_t809 , const MethodInfo*))List_1_IndexOf_m20216_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m20217_gshared (List_1_t958 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m20217(__this, ___start, ___delta, method) (( void (*) (List_1_t958 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m20217_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m20218_gshared (List_1_t958 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m20218(__this, ___index, method) (( void (*) (List_1_t958 *, int32_t, const MethodInfo*))List_1_CheckIndex_m20218_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m20219_gshared (List_1_t958 * __this, int32_t ___index, UILineInfo_t809  ___item, const MethodInfo* method);
#define List_1_Insert_m20219(__this, ___index, ___item, method) (( void (*) (List_1_t958 *, int32_t, UILineInfo_t809 , const MethodInfo*))List_1_Insert_m20219_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m20220_gshared (List_1_t958 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m20220(__this, ___collection, method) (( void (*) (List_1_t958 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m20220_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m20221_gshared (List_1_t958 * __this, UILineInfo_t809  ___item, const MethodInfo* method);
#define List_1_Remove_m20221(__this, ___item, method) (( bool (*) (List_1_t958 *, UILineInfo_t809 , const MethodInfo*))List_1_Remove_m20221_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m20222_gshared (List_1_t958 * __this, Predicate_1_t2699 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m20222(__this, ___match, method) (( int32_t (*) (List_1_t958 *, Predicate_1_t2699 *, const MethodInfo*))List_1_RemoveAll_m20222_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m20223_gshared (List_1_t958 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m20223(__this, ___index, method) (( void (*) (List_1_t958 *, int32_t, const MethodInfo*))List_1_RemoveAt_m20223_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse()
extern "C" void List_1_Reverse_m20224_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_Reverse_m20224(__this, method) (( void (*) (List_1_t958 *, const MethodInfo*))List_1_Reverse_m20224_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort()
extern "C" void List_1_Sort_m20225_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_Sort_m20225(__this, method) (( void (*) (List_1_t958 *, const MethodInfo*))List_1_Sort_m20225_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m20226_gshared (List_1_t958 * __this, Comparison_1_t2702 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m20226(__this, ___comparison, method) (( void (*) (List_1_t958 *, Comparison_1_t2702 *, const MethodInfo*))List_1_Sort_m20226_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t1087* List_1_ToArray_m20227_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_ToArray_m20227(__this, method) (( UILineInfoU5BU5D_t1087* (*) (List_1_t958 *, const MethodInfo*))List_1_ToArray_m20227_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m20228_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m20228(__this, method) (( void (*) (List_1_t958 *, const MethodInfo*))List_1_TrimExcess_m20228_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m20229_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m20229(__this, method) (( int32_t (*) (List_1_t958 *, const MethodInfo*))List_1_get_Capacity_m20229_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m20230_gshared (List_1_t958 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m20230(__this, ___value, method) (( void (*) (List_1_t958 *, int32_t, const MethodInfo*))List_1_set_Capacity_m20230_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m20231_gshared (List_1_t958 * __this, const MethodInfo* method);
#define List_1_get_Count_m20231(__this, method) (( int32_t (*) (List_1_t958 *, const MethodInfo*))List_1_get_Count_m20231_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t809  List_1_get_Item_m20232_gshared (List_1_t958 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m20232(__this, ___index, method) (( UILineInfo_t809  (*) (List_1_t958 *, int32_t, const MethodInfo*))List_1_get_Item_m20232_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m20233_gshared (List_1_t958 * __this, int32_t ___index, UILineInfo_t809  ___value, const MethodInfo* method);
#define List_1_set_Item_m20233(__this, ___index, ___value, method) (( void (*) (List_1_t958 *, int32_t, UILineInfo_t809 , const MethodInfo*))List_1_set_Item_m20233_gshared)(__this, ___index, ___value, method)
